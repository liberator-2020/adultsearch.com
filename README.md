# Adultsearch.com Legacy

## Installation on localhost

### PHP

Install PHP (tested with versions 5.6, 7.1 and 7.2)
Install and enable these PHP modules/extensions:
* gd
* pdo, pdo_mysql, mysqli
* shmop
* memcached
* redis (https://github.com/phpredis/phpredis/blob/develop/INSTALL.markdown)

Install php-fpm, start it and make sure it starts automatically
check/adjust configuraiton of php-fpm (/etc/php/php-fpm.d/www.conf or similar)
user = <your_user>
group = <your_group>
listen = /run/php-fpm/php-fpm.sock (this needsto match nginx conf)

### MySQL

Install MariaDB (MySQL) - tested with MariaDB v. 10.1.34

Start MariaDB server (make sure it runs on standard address 127.0.0.1 port 3306)

Download AS db dump from asdev@adultsearch.com

```
create database asdev;
create user 'asdev'@'localhost' identified by 'elanSestricky123';
grant all privileges on asdev.* to 'asdev'@'localhost';
UPDATE mysql.user SET Super_Priv='Y' WHERE user='asdev' AND host='localhost';
FLUSH PRIVILEGES;
```

```
mysql -u asdev -p asdev < asdev.20180710202656.sql
```

Disable SQL strict mode and ONLY_FULL_GROUP_BY mode.

### Sphinx

Install Sphinx search engine (http://sphinxsearch.com/) - tested with version 2.2.11

```
cp ./config/sphinx.conf /etc/sphinx/sphinx.conf
cp ./config/sphinx.dir.conf /etc/sphinx/sphinx.dir.conf
```

Adjust /etc/sphinx/sphinx.conf and /etc/sphinx/sphinx.dir.conf if necessary (paths)

Index at least index with indexer, for example "escorts" index:
```
indexer --config /etc/sphinx/sphinx.conf --rotate escorts
```

Start sphinx and make sure it runs automatically


### Memcached

Install Memcached (http://www.memcached.org/) - tested with version 1.5.8

Start Memcached and make sure it runs automatically


### Redis

Install Redis (https://redis.io/) - tested with version 4.0.11
* On Windows, you can try this: https://github.com/MicrosoftArchive/redis/releases
* ON Mac OS X: https://medium.com/@petehouston/install-and-config-redis-on-mac-os-x-via-homebrew-eb8df9a4f298

Configure to run on standard TCP port (127.0.0.1, port 6379), start Redis and make sure it runs automatically


### Nginx

Install Nginx - tested with version 1.15

```
cp ./config/install/dev.adultsearch.com.crt /etc/ssl/certs/dev.adultsearch.com.crt
cp ./config/install/dev.adultsearch.com.key /etc/ssl/private/dev.adultsearch.com.key
```

Use configuration file config/install/nginx_as.conf
- Change root /www/virtual/asdev/html directive to your webroot

Adjust/update /etc/nginx/nginx.conf:
```
error_log /var/log/nginx/error.log;
http {
	...
	client_max_body_size 10M;
	fastcgi_buffers 16 16k;
    fastcgi_buffer_size 32k;
}
```

Start nginx and make sure it is started automatically

### Browser

Install Certificate Authority into browser/OS keychain:

Chrome:
* Settings -> Advanced -> Privacy and Security -> Manage Certificates -> Authorities -> IMPORT -> Choose file ./config/install/1234CA.pem

Firefox:
* Preferences -> Privacy & Security -> Certificates -> View Certificates -> Authorities -> Import -> Choose file ./config/install/1234CA.pem -> Trust this CA to identify websites -> OK

### Libs

Download file https://adultsearch.com/inc/classes/bin/ip.bin and save it as html/inc/classes/bin/ip.bin


### Configuration

Copy configuration file and adjust settings inside
```
cp ./config/inc/config.php ./html/inc/config.php
```
and specify:
```
$config_db_host = 'mariadb';
```

Create log dir
```
mkdir ../data/log
```

Create safestore dir and copy dev safestore public & private key
```
mkdir ./data/safestore
cp ./config/data/safestore/* ./data/safestore/
```

Run commands:
```
vendor/bin/browscap-php browscap:fetch
vendor/bin/browscap-php browscap:convert
```


### Check

Try https://dev.adultsearch.com/monitor.php and enter password "snail", everything should be OK
Try https://dev.adultsearch.com, website should be working properly


### Crontab


    */2 * * * * /usr/bin/php /home/as/dev/cron/2mins.php >/dev/null 2>&1
    */5 * * * * /usr/bin/php /home/as/dev/cron/advertise.php >/dev/null 2>&1
    */15 * * * * /usr/bin/php /home/as/dev/cron/home_update.php >/dev/null 2>&1
    */30 * * * * /usr/bin/php /home/as/dev/cron/classifieds.php >/dev/null 2>&1
    */12 * * * * /usr/bin/php /home/as/dev/cron/capture.php >/dev/null 2>&1
    1 0 * * * /usr/bin/php /home/as/dev/cron/midnight.php >/dev/null 2>&1
    #2 2 * * * /usr/bin/php /home/as/dev/cron/businessowner.php >/dev/null 2>&1
    16 3 * * * /usr/bin/php /home/as/dev/cron/clear.php >/dev/null 2>&1
    7 */1 * * * /usr/bin/php /home/as/dev/cron/hour.php >/dev/null 2>&1
    7 7 * * sun /usr/bin/php /home/as/dev/cron/week.php >/dev/null 2>&1
    13 2 * * * /usr/bin/php /home/as/dev/cron/currency_update.php >/dev/null 2>&1
    11 4 * * * /usr/bin/php /home/as/dev/cron/sitemap.php >/dev/null 2>&1
	14 */4 * * * /usr/bin/php /home/as/prod/current/cron/process_waiting_list.php >/dev/null 2>&1

### Docker

Add alias to your hosts file:
```
127.0.0.1 dev.adultsearch.com
```

Copy config:

```bash
cp ./config/inc/config.docker.php ./html/inc/config.php
cp ./config/docker/.env.example ./config/docker/.env
```

Put to .env your UID (Ubuntu usually - 1000, MacOS - 501)

Import databases:

```bash
mkdir ./html/inc/classes/bin
wget https://adultsearch.com/inc/classes/bin/ip.bin -O ./html/inc/classes/bin/ip.bin
./config/scripts/db_prod_to_docker.sh
```

Run and rebuild docker:

```bash
./config/scripts/start.sh
```

Install composer:

```bash
cd ./config/docker/
docker-compose exec workspace composer install
```

### incron

Incron watches specific folders and files for changes. We're using it to produce optimized video files and thumbnails.

1. Install `incron` from repository

    apt install incron

2. Add permissions for webserver user to use incron. 

    sudo vi /etc/incron.allow

For user www-data - add nginx user name www-data and save the file.

3. Let's say for example that https://img.dev.adultsearch.com is set to point to `$config_image_path`.

   Ensure that folder `$config_image_path` + `/classifieds/uploaded/` folder exists and www-data has write permissions.

4. Open incrontab list editor for user www-data:

    sudo -u www-data incrontab -e

Copy the following line into the editor

    <value of $config_image_path>/classifieds/uploaded/ IN_CLOSE_WRITE /usr/bin/php /home/as/dev/incron/videoad.php $@ $#

Where the first path points to `$config_image_path` as set in config.php plus `/classifieds/uploaded/`

Save the file, you should receive "table updated" message


### Video conversion 

1. Install `ffmpeg`

2. Ensure you have `ffmpeg`, `qt-faststart`, `ffprobe` utilities available and webserver user has permissions to execute those scripts.

3. Create folder to store uploaded , but not processed videos with cleaned file name:

    $tgt_path = $config_image_path."/classifieds/uploaded/" (Set in class.vid.php, inc/common.php:1652 )

4. Create folder to store processed videos along with thumbnails

    $tgt_dir = $config_image_path . "/classifieds" (defined in inc/common.php:1653, adbuild_step3.tpl)


### Orphan file watcher

Cronjob orphan_check.php deletes old uploads in /classifieds/uploads/ and sends email to admin.


## Development worklow

We are using simple git branching model described here: https://gist.github.com/jbenet/ee6c9ac48068889b0912

Make sure you have access to GitLab repository: https://gitlab.com/slippery-snails/as-old

And that you have uploaded your SSH public key: https://gitlab.com/profile/keys

Set up your git email/username:
```
git config --global user.name "my_gitlab_username"
git config --global user.email "my_gitlab_account_email@example.com"
git config --global --list
```

Create a main directory:
```
mkdir work
cd work
```

Clone AS legacy repo:
```
git clone https://gitlab.com/slippery-snails/as-old.git
```

Adjust configuration to your machine and make sure website runs:
```
cd ~/work/as-old
cp ./config/inc/config.php ./html/inc/config.php 
```

### Developing new feature

Make sure we have up to date codebase
```
cd ~/work/as-old
git checkout master
git pull origin master
```

Create new branch for our new feature
```
git checkout -b new-edit-profile-page
```

Edit files, add new files (or remove files from repo)

Add changes to repo and commit on your machine
```
git add -p
git commit -m "added new profile edit page template and controller"
```

At least once per day (Every morning) or whenever necessary, update your branch with new updates in the master branch:

    git fetch origin
    git rebase origin/new-edit-profile-page
    git rebase origin/master

Push your branch to the server - this should be done at the end of every work day
```
git push origin new-edit-profile-page
```

When done with feature, rebase again interactive (you can squash multiple commits into one, etc...), push the branch to server for review
```
git rebase -i origin/master
git push --force origin new-edit-profile-page
```
Create merge request by going in web browser to https://gitlab.com/slippery-snails/as-old and click the button "Create merge request"

Start working on next feature

## Coding style

Mandatory rule:

**We use tabs for indentation. Do not use spaces for indenting in your code.**

Any good editor/IDE allows you to set width of the tab character (2, 4 or 8 spaces). If you are used to spaces, any good editor/IDE can automatically convert the spaces to tabs on save.

