<?php

set_time_limit(0);

require __DIR__.'./../html/inc/config.php';
require __DIR__.'./../html/inc/common.php';

$commandName       = $argv[1];
$commandsNamespace = 'App\\Command\\';

$commandClass = getCommandClass($commandName, $commandsNamespace);

/** @var \App\Command\AbstractCommand $command */
$command = new $commandClass(array_slice($argv, 2));

$command->execute();

/**
 * @param string $string
 *
 * @return mixed
 */
function dashesToCamelCase($string) {
	return str_replace('-', '', ucwords($string, '-'));
}

/**
 * @param string $commandName
 * @param string $commandsNamespace
 *
 * @return string
 */
function getCommandClass($commandName, $commandsNamespace) {
	$commandClass              = dashesToCamelCase($commandName);
	$commandClassWithNamespace = "{$commandsNamespace}{$commandClass}";
	if (!class_exists($commandClassWithNamespace)) {
		print("Command {$commandName} doesn't exists".PHP_EOL);

		exit;
	}

	return $commandClassWithNamespace;
}