#!/bin/bash

/usr/bin/indexer --config /etc/sphinxsearch/sphinx.conf  --rotate --all
/usr/bin/searchd --config /etc/sphinxsearch/sphinx.conf --nodetach
