# Installation of DEV Adultsearch on Ubuntu 18.04 LTS Desktop


Follow this article to install Ubuntu (dual-boot) on your computer:

https://www.tecmint.com/install-ubuntu-16-04-alongside-with-windows-10-or-8-in-dual-boot/

From https://www.ubuntu.com/#download we download Ubuntu desktop 18.04 LTS

English language (en-US)

Normal installation + Install 3rd party software (wifi drivers, ...)

After installation and reboot and logging in:

connect to internet

open terminal app

### Enable sudo without password
```
sudo visudo
```
At the bottom of the file put following line (replace your_username with your username):
```
your_username ALL=(ALL) NOPASSWD: ALL
```

### General stuff
```
sudo apt update
sudo apt install git composer
```

### Generate SSH key pair (based on https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-1804)

If you already have a ssh key pair you want to reuse, make sure public key is in file ~/.ssh/id_rsa.pub and privatekey in file  ~/.ssh/id_rsa, otherwise:
```
ssh-keygen
<Enter>
<Enter>
<Enter>
```

### Upload public ssh key to gitlab (based on https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html)

In browser, go to https://gitlab.com/users/sign_in and login

Click on profile icon in top right and go to "Settings", then pick "SSH Keys".

Into "Key" textarea paste contents from file ~/.ssh/id_rsa.pub (you need to enable "show hidden files" checkbox in settings of files ubuntu app to see .ssh folder)

Put anything into name and click "add key"

### Checkout codebase

Create project directory in your home directory:

```
mkdir -p ~/as/dev
cd ~/as/dev
```

Checkout master branch from gitlab (notice dot at the end of clone command ;): 

```
git config --global user.name "my_gitlab_username"
git config --global user.email "my_gitlab_account_email@example.com"
git clone git@gitlab.com:slippery-snails/as-old.git .
yes<Enter>
```

### Nginx (loosely based on https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04)

```
sudo apt install nginx
sudo ufw allow 'Nginx Full'
```

Verify in browser loading URL http://127.0.0.1/

### Mariadb
```
sudo apt install mariadb-server mariadb-client
sudo systemctl status mariadb
sudo mysql_secure_installation
<Enter>
<Enter>
mariadbpassword<Enter>
mariadbpassword<Enter>
<Enter>
<Enter>
<Enter>
<Enter>
```

### Create database
```
mysql -u root -p
mariadbpassword<Enter>
create database asdev;
create user 'asdev'@'localhost' identified by 'elanSestricky123';
grant all privileges on asdev.* to 'asdev'@'localhost';
UPDATE mysql.user SET Super_Priv='Y' WHERE user='asdev' AND host='localhost';
FLUSH PRIVILEGES;
exit
```

### Import mysql database

For automatic download and  import of production database, your public ssh key (in file ~/.ssh/id_rsa.pub) needs to be set up on server - send it to jay
```
cd ~/as/dev/
./config/scripts/db_prod_to_dev.sh
yes<Enter>
```

### Redis (based on https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-18-04)
```
sudo apt install redis-server
```

### Memcached (based on https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-memcached-on-ubuntu-16-04)
```
sudo apt install memcached
```

### PHP
```
sudo apt install php-fpm php-mysql php-curl php-mbstring php-simplexml php-memcached php-redis
```

### Sphinx
```
sudo apt install sphinxsearch
sudo cp /home/your_username/as/dev/config/intall/ubuntu/sphinx.* /etc/sphinxsearch/
```

Edit file /etc/default/sphinxsearch and change line "START=no" to "START=yes" :

```
sudo nano /etc/default/sphinxsearch
```

```
sudo systemctl restart sphinxsearch
sudo indexer --rotate --all
```

### Configure php-fpm and nginx
```
sudo cp ./config/install/ubuntu/nginx_as.conf /etc/nginx/sites-available/as.conf
sudo ln -s /etc/nginx/sites-available/as.conf /etc/nginx/sites-enabled/as.conf
```

Edit file /etc/nginx/sites-available/as.conf and update 2 root directives (change your_username to your username):
```
sudo nano /etc/nginx/sites-available/as.conf
```

Edit /etc/nginx/nginx.conf:
```
sudo nano /etc/nginx/nginx.conf
```

and put into http {} directive, on line 56 (above virtual hosts section) following lines:

```
client_max_body_size 10M;
fastcgi_buffers 16 16k;
fastcgi_buffer_size 32k;
```

Copy certificates:

```
sudo cp ./config/install/dev.adultsearch.com.crt /etc/ssl/certs/dev.adultsearch.com.crt
sudo cp ./config/install/dev.adultsearch.com.key /etc/ssl/private/dev.adultsearch.com.key
```

Backup original and copy over php-fpm config file:

```
sudo mv /etc/php/7.2/fpm/pool.d/www.conf /etc/php/7.2/fpm/pool.d/www.conf.bak
sudo cp ./config/install/ubuntu/php_fpm_www.conf /etc/php/7.2/fpm/pool.d/www.conf
```

Edit php-fpm conf file and replace all occurences of your_username with your username:
```
sudo nano /etc/php/7.2/fpm/pool.d/www.conf
```


```
sudo systemctl restart php7.2-fpm
sudo systemctl restart nginx
```

### Add DNS override for dev.adultsarch.com domain to point to your machine:
```
sudo echo -e "\n127.0.0.1 dev.adultsearch.com www.dev.adultsearch.com img.dev.adultsearch.com ca.dev.adultsearch.com uk.dev.adultsearch.com germany.dev.adultsearch.com thailand.dev.adultsearch.com philippines.adultsearch.com china.dev.adultsearch.com\n" >> /etc/hosts
```

### Import certification authority

in Mozilla Firefox:

* Preferences → Privacy & Security → Certificates → View Certificates... → Authorities → Import...
* select file ./config/install/1234CA.pem
* Check "Trust this CA to identify websites"
* Click OK

### Libs

Download file https://adultsearch.com/inc/classes/bin/ip.bin and save it as html/inc/classes/bin/ip.bin:

```
mkdir ~/as/dev/html/inc/classes/bin
wget -O ~/as/dev/html/inc/classes/bin/ip.bin https://adultsearch.com/inc/classes/bin/ip.bin
```

### Website Configuration

Copy configuration file, create logdir, safestoredir and copy safestore keys and install composer dependencies:

```
cd ~/as/dev
cp ./config/inc/config.php ./html/inc/config.php
mkdir ./html/tmp/smarty/templates_c
mkdir -p ./data/log
mkdir -p ./data/safestore
cp ./config/data/safestore* ./data/safestore/
composer install
```

Adjust settings in config file ./html/inc/config.php if needed (should not be necessary)

### Test website

In browser, go to URL:

http://dev.adultsearch.com/monitor.php

you should be redirected to https:// version and shown monitor passord page, enter "snail" and click on S button.

If everything is allright, test main website https://dev.adultsearch.com

### Mailhog

Dev is configured to send all emails to mailhog (https://github.com/mailhog/MailHog). Installation:

```
sudo wget -O /usr/local/bin/mailhog https://github.com/mailhog/MailHog/releases/download/v1.0.0/MailHog_linux_amd64
sudo chmod +x /usr/local/bin/mailhog
sudo cp /home/your_username/as/dev/config/install/ubuntu/mailhog.service /etc/systemd/system/mailhog.service
sudo systemctl start mailhog
sudo systemctl enable mailhog
```

You can view all outgoing emails in http://localhost:8025
