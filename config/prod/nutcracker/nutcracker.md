# Installation of nutcracker proxy on current AS machine

Homepage: https://github.com/twitter/twemproxy

## Install from source
(https://drive.google.com/drive/folders/0B6pVMMV5F5dfMUdJV25abllhUWM):

```
# cd
# tar xvzf ./nutcracker-0.4.1.tar.gz
# cd ./nutcracker-0.4.1
# ./configure
# make
# make install
```

## Configure

```
# mkdir /var/log/nutcracker
# chown nobody.nobody /var/log/nutcracker
# mkdir /var/run/nutcracker
# chown nobody.nobody /var/run/nutcracker
# vim /etc/sysconfig/nutcracker
# vim /etc/init.d/nutcracker
# vim /etc/nutcracker.yml
# service nutcracker start
# service nutcracker status
# telnet 127.0.0.1 26379
# chkconfig --add nutcracker
# chkconfig nutcracker on
```
