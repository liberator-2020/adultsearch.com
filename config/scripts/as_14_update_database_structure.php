<?php
chdir(__DIR__);
define('_CMS_FRONTEND', 1);

require_once('../../html/inc/config.php');
require_once('../../html/inc/common.php');

global $db, $config_image_path, $config_dev_server;

$time_start = microtime(true);
echo "Starting analyze...\n";

$handler = new TransferDataToPlace($db, $config_image_path, $config_dev_server, 'problem');
if ($handler->analyze()) {
	echo "Houston we have problem! ";
} else {
	echo "all ok. Start the transfer of data...\n";
	$handler->execute();
	echo "Done. ";
}

$time_end = microtime(true);
$time     = round($time_end - $time_start,2);
echo " time # {$time} sec\n";
exit();

class TransferDataToPlace {

	/**
	 * @var bool
	 */
	private $skip = false;

	/**
	 * @var bool
	 */
	private $problem = false;

	/**
	 * @var string
	 */
	private $sqlField = '
SELECT a.COLUMN_NAME, a.COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS a WHERE TABLE_SCHEMA=? AND TABLE_NAME=?;
';
	/**
	 * @var string
	 */
	private $sqlFieldMore = '
SELECT a.COLUMN_NAME, a.CHARACTER_MAXIMUM_LENGTH  FROM INFORMATION_SCHEMA.COLUMNS a WHERE TABLE_SCHEMA=? AND TABLE_NAME=?;
';

	/**
	 * @var string
	 */
	private $sqlAttributeField = '
SELECT a.`name`, a.`attribute_id` FROM `attribute` a ;
';

	/**
	 * @var string
	 */
	private $sqlMaxLength = '
SELECT MAX(LENGTH(:field)) FROM :table ;
';

	/**
	 * @var string
	 */
	private $sqlPlaceType = '
SELECT `place_type_id`, `url` FROM `place_type`
';

	private $sqlPlacePicture = '
SELECT pp.picture, pp.filename, pp.thumb 
FROM place_picture pp 
WHERE 
	pp.module = ? AND pp.id = ?
';

	private $sqlPlaceReview = '
SELECT pr.review_id
FROM place_review pr 
WHERE 
	pr.module = ? AND pr.id = ?
';

	private $sqlBusinessOwner ='
SELECT id
FROM account_businessowner
WHERE 
	section = ? AND place_id = ?
';
	/**
	 * @var string
	 */
	private $sqlEroticmassage = "SELECT e.*, GROUP_CONCAT(eta.tag_id) as masseuse, l.loc_name, lc.dir as country
FROM eroticmassage e
INNER JOIN location_location l ON e.loc_id = l.loc_id
INNER JOIN location_location lc ON l.country_id = lc.loc_id
LEFT JOIN eroticmassage_tag_assign eta on eta.id = e.id
GROUP BY e.id
ORDER BY e.id ASC";

	/**
	 * @var string
	 */
	private $sqlTable = "
SELECT p.*,  NULL as masseuse, l.loc_name, lc.dir as country 
FROM `%s` p
INNER JOIN location_location l ON p.loc_id = l.loc_id
INNER JOIN location_location lc ON l.country_id = lc.loc_id
ORDER BY id ASC
";

	/**
	 * @var string
	 */
	private $sqlPlaceTable = "
SELECT p.*,  NULL as masseuse, l.loc_name, lc.dir as country 
FROM `place` p
INNER JOIN location_location l ON p.loc_id = l.loc_id
INNER JOIN location_location lc ON l.country_id = lc.loc_id
ORDER BY place_id ASC
";

	/**
	 * @var string
	 */
	private $sqlPlaceExist = "SELECT place_id FROM place WHERE old_id = ? and place_type_id = ? and loc_id = ?";

	/**
	 * @var string
	 */
	private $sqlCheckLocation = "
SELECT DISTINCT t.id, t.loc_id FROM `%s` t LEFT JOIN `location_location` l ON t.loc_id = l.loc_id WHERE l.loc_id IS NULL;
";

	/**
	 * @var string
	 */
	private $sqlInsertMainAttribute = "
INSERT INTO `place_attribute` (`place_id`, `attribute_id`, `value`, `value_text`) VALUES 
";

	private $sqlLocationAttr = "
SELECT attribute_id FROM location_attribute WHERE location_id = ?
";

	private $sqlGetValuesAttr = "
SELECT CAST(%s AS UNSIGNED) as value FROM %s GROUP BY CAST(%s AS UNSIGNED);
";

	/**
	 * @var string
	 */
	private $sqlInsertLocationAttribute = "
INSERT INTO `location_attribute` (`location_id`, `attribute_id`) VALUES 
";

	/**
	 * @var string
	 */
	private $sqlSelectDb = "
SELECT database();
";

	/**
	 * @var string
	 */
	private $sqlUpdatePlacePicture = "
UPDATE place_picture SET id = ?, module = 'place',  filename = ?, thumb = ? WHERE picture = ? LIMIT 1
";

	private $sqlPlaceAttribute = "
SELECT value FROM place_attribute WHERE attribute_id=? AND place_id=?
";

	/**
	 * @var string
	 */
	private $dbName;

	/**
	 * @var string
	 */
	private $attr179Num = 179;


	/**
	 * @var string
	 */
	private $attr148Num = 148;
	/**
	 * @var array
	 */
	private $attrDancer = [
		1 => 'female',
		2 => 'female_and_male',
		3 => 'male',
		4 => 'male_gay',
		5 => 'ts_tg',
	];
	/**
	 * @var string
	 */
	private $mainTable = 'place';

	/**
	 * @var string
	 */
	private $mainTableAttribute = 'attribute';

	/**
	 * @var string[]
	 */
	private $tables = [
			'eroticmassage',
			'strip_club2',
			'adult_stores',
			'lingeriemodeling1on1',
			'gay',
			'gaybath',
			'brothel',
			'lifestyle',
		];

	/**
	 * @var array 
	 */
	private $pathFromTable = [
			'place'                => 'place',
			'eroticmassage'        => 'eroticmassage',
			'brothel'              => 'brothel',
			'gay'                  => 'gay',
			'gaybath'              => 'gaybath',
			'lingeriemodeling1on1' => 'lingeriemodeling1on1',
			'lifestyle'            => 'lifestyle',
			'adult_stores'         => 'adultstore',
			'strip_club2'          => 'stripclub',
			'strip_revues'         => 'stripclub/revues',
		];

	/**
	 * @var array 
	 */
	private $moduleFromTable = [
			'place'                => 'place',
			'eroticmassage'        => 'eroticmassage',
			'brothel'              => 'brothel',
			'gay'                  => 'gay',
			'gaybath'              => 'gaybath',
			'lingeriemodeling1on1' => 'lingeriemodeling1on1',
			'lifestyle'            => 'lifestyle',
			'adult_stores'         => 'adultstore',
			'strip_club2'          => 'stripclub',
			'strip_revues'         => 'stripclubrevue',
		];

	/**
	 * @var array 
	 */
	private $sectionFromTable = [
			'place'                => 'place',
			'eroticmassage'        => 'emp',
			'brothel'              => '',
			'gay'                  => '',
			'gaybath'              => '',
			'lingeriemodeling1on1' => 'lingerie',
			'lifestyle'            => '',
			'adult_stores'         => 'as',
			'strip_club2'          => 'sc',
			'strip_revues'         => '',
		];

	/**
	 * @var string[]
	 */
	private $fieldSkip = ['state_id', 'country_id', 'loc_name', 'state_name', 'state', 'neighbor', 'type'];

	/**
	 * @var string[]
	 */
	private $headers = ['Field', 'In table PLACE', 'In table attribute', 'move/ignore', 'COLUMN TYPE'];

	/**
	 * @var string[]
	 */
	private $attrsBooleanToBoolean3 = [
		'table_shower',
		'sauna',
		'jacuzzi',
		'fourhand',
		'private_parking',
		'vip',
		'gloryhole',
	];

	/**
	 * @var string[]
	 */
	private $attrsText = [
			'happy_hours',
			'facilities',
			'services',
			'dancers',
			'info',
			'cover',
		];

	/**
	 * @var string[]
	 */
	private $attrsBoolean3 = [
			'adultmovie',
			'adult_books',
			'adult_dvds',
			'adult_toys',
			'adult_video_arcade',
			'alcoholic',
			'cc',
			'cold_buffet',
			'coyote_dancers',
			'cruising_lounge',
			'double_ladies_drink',
			'fetish',
			'few_girls',
			'fishbowls',
			'fourhand',
			'free_food',
			'gloryhole',
			'have_bar_fine',
			'hot_buffet',
			'jacuzzi',
			'just_drinking_bar',
			'lgbt',
			'lingerie',
			'live_entertainment',
			'location_bar_hopping',
			'lots_of_girls',
			'mixture_girls_ladyboys',
			'nice_rooms_showers',
			'nude_topless_dancing',
			'offers_extra_services',
			'offers_sex',
			'open_holidays',
			'parking',
			'peep_shows',
			'pickup_dropoff',
			'privatedance',
			'private_parking',
			'private_room',
			'reservations',
			'sauna',
			'sexshow',
			'short_time_room',
			'soapy_massage',
			'table_shower',
			'take_out',
			'theaters',
			'toyshow',
			'vip',
			'vip_room',
		];
	
	/**
	 * @var array
	 */
	private $codeCountry = [16046, 16047, 41973];

	/**
	 * @var
	 */
	private $mainTableAttributeFields;

	/**
	 * @var
	 */
	private $mainTableAttributeFieldNames;

	/**
	 * @var
	 */
	private $mainTableFields;

	/**
	 * @var
	 */
	private $mainTableFieldNames;

	/**
	 * @var
	 */
	private $mainTableFieldsMore;

	/**
	 * @var array
	 */
	private $tableFields = [];

	/**
	 * @var
	 */
	private $placeType;

	/**
	 * @var \db
	 */
	private $db;

	/**
	 * @var
	 */
	private $configImagePath;

	/**
	 * @var
	 */
	private $configImagePathProd = 'http://img.adultsearch.com/';

	/**
	 * @var
	 */
	private $insertMainField = [];

	/**
	 * @var
	 */
	private $insertField = [];

	/**
	 * @var int
	 */
	private $countMoveRows = 0;

	public function __construct(\db $db, $config_image_path, $config_dev_server, $arg1 = '') {
		$this->db              = $db;
		$this->configImagePath = $config_image_path;
		if (substr($config_image_path, -1) != "/")
			$this->configImagePath = $config_image_path."/";
		$this->configDev       = $config_dev_server;
		if ($arg1 === 'problem') {
			$this->note('Output fields with problem only');
			$this->problem = true;
		}
		$this->initData();
	}

	/**
	 *
	 */
	public function execute() {

		// BUG some values were truncated on mysql insert (website from 255 to 100 chars, zipcode from 15 to 10 chars)
		// ALTER TABLE `place` CHANGE `website` `website` varchar(255) COLLATE 'utf8_general_ci' NULL AFTER `email`;
		// ALTER TABLE `place` CHANGE `zipcode` `zipcode` varchar(15) COLLATE 'utf8_general_ci' NULL AFTER `address2`;

		$this->checkAndCreatePathForTypes('place');

		$this->prepareLocationAttributes();

		$this->prepare179Attribute();

		$this->prepareExistImageForPlace();

		foreach ($this->tables as $table) {
			$time_start = microtime(true);

			$this->countMoveRows = 0;
			$interval            = 0;

			// for local test only
			$this->checkAndCreatePathForTypes($table);
			$this->defineRelationFields($table);

			if ($table === 'eroticmassage') {
				$res = $this->db->q($this->sqlEroticmassage, []);
			} else {
				$res = $this->db->q(sprintf($this->sqlTable, $table), []);
			}

			while ($row = $this->db->r($res, MYSQL_ASSOC)) {
//				$imageDir = $row['country']!=='guam'?$row['country']:'place';
//				$this->checkAndCreatePathForTypes($imageDir);
				$imageDir = 'place';
//				$this->checkAndCreatePathForTypes($imageDir);
				$interval++;
				$type_id       = isset($row['type']) ? $row['type'] : null;
				$place_type_id = $this->getTypeId($table, $type_id);
				if ($row_exist = $this->fetchAll($this->sqlPlaceExist, [$row['id'], $place_type_id, $row['loc_id']])) {
					if (!$this->configDev) {
						$this->note("Record already exist in `place` from {$table} old_id#{$row['id']} place_type_id#{$place_type_id} ");
					}
					continue;
				}
				if ($toId = $this->insertToMainTable($table, $row)) {
					$this->copyCoupon($table, $row, $toId, $imageDir);
					//BUG we should not copy images depending on whether we copied some extra attributes or not
					$this->insertToAttributeMainTable($toId, $table, $row);
					$this->copyImgs($table, $row, $toId, $imageDir);
					//BUG additional extra images for domestic places were copied from "place" imageDir instead of correct table imageDir
					$this->copyAdditionalImgs($table, $row['id'], $toId, $imageDir, true);
					$this->copyReview($table, $row['id'], $toId);
					$this->updateBusinessOwner($table, $row['id'], $toId);
					$this->countMoveRows++;
				}
				If ($interval > 100) {
					$interval = 0;
					$this->note("Move {$this->countMoveRows} rows for {$table}");
				}
//				If($this->countMoveRows > 10) break; // @todo for debug
			}
			$time_end = microtime(true);
			$time     = round($time_end - $time_start,2);
			$this->note("Done for {$table}: {$this->countMoveRows} rows # ".count($this->db->sqls)." requests # {$time} sec");
		}

	}

	/**
	 * @param $table
	 */
	private function defineRelationFields($table) {
		foreach (array_keys($this->tableFields[$table]) as $fieldOrg) {
			if (in_array($fieldOrg, $this->fieldSkip)) {
				continue;
			}
			$field = $this->ruleTransferredField($fieldOrg);

			if (in_array($field, $this->mainTableFieldNames)) {
				if ($this->tableFields[$table][$fieldOrg] == $this->mainTableFields[$field]) {
					// equal type
					$this->insertMainField[$field] = $fieldOrg;
				} else {
					// check
					$this->insertMainField[$field] = $fieldOrg;
				}
				$anyWhere = true;
			} else {
				if (in_array($field, $this->mainTableAttributeFieldNames)) {
					// attribute
					$this->insertField[$field] = $fieldOrg;
				}
			}

		}
	}

	/**
	 * @param $table
	 * @param $row
	 * @return null
	 */
	private function insertToMainTable($table, $row) {
		$insertMain = '';
		$paramsMain = [];
		foreach ($this->insertMainField as $mainName => $name) {
			$insertMain   .= $insertMain ? ',' : '';
			$insertMain   .= "`{$mainName}` = ?";
			$paramsMain[] = $this->prepareData($table, $row, $mainName, $name);
		}
		if ($insertMain) {
			$insertMain   .= ", place_type_id = ?";
			$type_id      = isset($row['type']) ? $row['type'] : null;
			$paramsMain[] = $this->getTypeId($table, $type_id);
			$res2         = $this->db->q("INSERT INTO `place` SET ".$insertMain, $paramsMain);

			return $this->db->insertid();
		}

		return null;
	}

	/**
	 * @param $toId
	 * @param $table
	 * @param $row
	 * @return mixed
	 */
	private function insertToAttributeMainTable($toId, $table, $row) {
		$insert = '';
		$params = [];
		foreach ($this->insertField as $nameAttribute => $name) {
			//BUG - we were inserting blank "place_attribute" values into place_attribute table
			$data = $this->prepareData($table, $row, $nameAttribute, $name);
			if (is_null($data) || $data === "")
				continue;

			$insert .= $insert ? ',' : '';
			if (in_array($nameAttribute, $this->attrsText)) {
				$insert .= "(?, ?, NULL, ?)";
			} else {
				$insert .= "(?, ?, ?, NULL)";
			}
			$params[] = $toId;
			$params[] = $this->mainTableAttributeFields[$nameAttribute];
			$params[] = $data;
		}
		$res3 = $this->db->q($this->sqlInsertMainAttribute.' '.$insert, $params);

		return $this->db->insertid();
	}

	/**
	 *
	 * @return int
	 */
	public function analyze() {
		$countProblemRows = 0;

		foreach ($this->tables as $table) {
			$nonExistLocations = $this->fetchAll(sprintf($this->sqlCheckLocation, $table), []);
			$nonExistLocations = array_column($nonExistLocations, 'loc_id', 'id');
			if ($nonExistLocations) {
				$this->error($table.' '.count($nonExistLocations).' rows with none exist locations');
				$this->outLocation($table, $nonExistLocations);
				$countProblemRows++;
			}
			$this->tableFields[$table] = $this->fetchAll($this->sqlField, [$this->dbName, $table]);
			$this->tableFields[$table] = array_column($this->tableFields[$table], 'COLUMN_TYPE', 'COLUMN_NAME');
			$rows                      = [];
			foreach (array_keys($this->tableFields[$table]) as $fromField) {

				$result           = $this->analyzeFromField($table, $fromField);
				$countProblemRows += $result->countProblemRows;

				if ($this->problem) {
					if ($result->isProblem) {
						$rows[] = $result->row;
					}
				} else {
					if (!$result->anyWhere || !$this->skip) {
						$rows[] = $result->row;
					}
				}
			}
			if ($rows) {
				$this->note($table);
				$this->table($this->headers, $rows);
			}
		}

		return $countProblemRows;
	}

	/**
	 * @param $table
	 * @param $fromField
	 * @return \StdClass
	 */
	private function analyzeFromField($table, $fromField) {

		$toField = $this->ruleTransferredField($fromField);

		$result = new StdClass();

		$result->row              = [0 => $toField !== $fromField ? $fromField.'#>'.$toField : $toField];
		$result->isProblem        = false;
		$result->anyWhere         = false;
		$result->countProblemRows = 0;

		if (in_array($toField, $this->mainTableFieldNames)) {
			$this->analyzeToMainField($table, $fromField, $result);
		} else {
			$this->analyzeToMainAttributeField($table, $fromField, $result);
		}

		return $result;
	}

	private function analyzeToMainField($table, $fromField, $result) {
		$toField = $this->ruleTransferredField($fromField);

		$result->row[1] = 'Yes';
		$result->row[2] = '-';
		$result->row[3] = 'move';
		if ($this->tableFields[$table][$fromField] == $this->mainTableFields[$toField]) {
			$result->row[4] = $this->mainTableFields[$toField];
		} else {
			$sqlMaxLengthTable = str_replace(':table', $table, $this->sqlMaxLength);
			$maxLength         = $this->fetchSingle($sqlMaxLengthTable, [':field' => $fromField]);
			$result->row[4]    = $this->tableFields[$table][$fromField].'#>'.$this->mainTableFields[$toField].' (max='
				.intval($maxLength)
				.')';
			if ($this->mainTableFieldsMore[$toField] > 0
				&& $this->mainTableFieldsMore[$toField] < $maxLength
			) {
				$result->row[4]    .= ' !!!!!!';
				$result->isProblem = true;
				$result->countProblemRows++;
			}
		}

		$result->anyWhere = true;
	}

	private function analyzeToMainAttributeField($table, $fromField, $result) {
		$toField = $this->ruleTransferredField($fromField);

		$result->row[1] = 'No';
		$result->row[4] = $this->tableFields[$table][$fromField];
		if (in_array($toField, $this->mainTableAttributeFieldNames)) {
			$result->row[2]   = 'Yes';
			if (in_array($toField, $this->attrsBoolean3)) {
				$result->row[2] .=  '#bool3:' . $this->getExistValues($table, $fromField);

			}
			$result->row[3]   = 'move';
			$result->anyWhere = true;
		} else {
			$result->row[2] = 'No';
			$result->row[3] = 'skip';
			if (!in_array($fromField, $this->fieldSkip)) {
				$result->row[3]    = '?';
				$result->isProblem = true;
				$result->countProblemRows++;
			}
		}
	}

	/**
	 * @param $table
	 * @param $fromField
	 * @return string
	 */
	private function getExistValues($table, $fromField) {
		$res       = '';
		$resValues = $this->fetchAll(sprintf($this->sqlGetValuesAttr, $fromField, $table, $fromField));
		if ($resValues) {
			$values = array_column($resValues, 'value');
			$res    = implode(',', $values);
		}

		return $res;
	}

	private function note($string = '') {
		if ($string) {
			echo $string, PHP_EOL;
		}
	}

	private function error($string = '') {
		if ($string) {
			echo 'ERR: '.$string, PHP_EOL;
		}
		die;
	}

	/**
	 * @param array $headers
	 * @param array $rows
	 */
	private function table($headers = [], $rows = []) {
		$maxLength = [];
		$table     = [$headers] + $rows;
		foreach (current($table) as $key => $column) {
			$maxLength[$key] = 1 + max(array_map(function ($item) {
					return strlen($item);
				}, array_column($table, $key)));
		}
		$allLength = array_sum($maxLength) + count($maxLength) + 1;

		echo PHP_EOL, str_pad('=', $allLength, '='), PHP_EOL;
		foreach ($headers as $key => $item) {
			echo str_pad($item, $maxLength[$key]);
		}
		echo PHP_EOL, str_pad('-', $allLength, '-'), PHP_EOL;
		foreach ($rows as $row) {
			ksort($row);
			foreach ($row as $key => $item) {
				echo str_pad($item, $maxLength[$key]);
			}
			echo PHP_EOL;
		}
		echo str_pad('-', $allLength, '-'), PHP_EOL, PHP_EOL;
	}

	private function initData() {

		$this->dbName = $this->fetchSingle($this->sqlSelectDb, [$this->mainTableAttribute]);

		$this->mainTableAttributeFields     = $this->fetchAll($this->sqlAttributeField, [$this->mainTableAttribute]);
		$this->mainTableAttributeFields     = array_column($this->mainTableAttributeFields, 'attribute_id', 'name');
		$this->mainTableAttributeFieldNames = array_keys($this->mainTableAttributeFields);

		$this->mainTableFields     = $this->fetchAll($this->sqlField, [$this->dbName, $this->mainTable]);
		$this->mainTableFields     = array_column($this->mainTableFields, 'COLUMN_TYPE', 'COLUMN_NAME');
		$this->mainTableFieldNames = array_keys($this->mainTableFields);

		$this->mainTableFieldsMore = $this->fetchAll($this->sqlFieldMore, [$this->dbName, $this->mainTable]);
		$this->mainTableFieldsMore = array_column($this->mainTableFieldsMore, 'CHARACTER_MAXIMUM_LENGTH',
			'COLUMN_NAME');

		$this->placeType = $this->fetchAll($this->sqlPlaceType, []);
		$this->placeType = array_column($this->placeType, 'place_type_id', 'url');
	}

	/**
	 * @param $fromField
	 * @return string
	 */
	private function ruleTransferredField($fromField) {
		switch ($fromField) {
			case 'id':
				$toField = 'old_id';
				break;
			case 'parking':
				$toField = 'parking3';
				break;
			case 'phone':
				$toField = 'phone1';
				break;
			case 'other_services':
				$toField = 'services';
				break;
			case 'gdescription':
				$toField = 'info';
				break;
			case 'major_cross_streets':
				$toField = 'location_detail';
				break;
			case 'suspend_reason':
				$toField = 'worker_comment';
				break;
//			case 'type':
//				$toField = 'place_type_id';
//				break;
			default:
				$toField = $fromField;
		}

		return $toField;
	}

	/**
	 * @param       $sql
	 * @param array $params
	 * @return array
	 */
	private function fetchAll($sql, $params = []) {
		$query  = $this->db->q($sql, $params);
		$result = $query->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

	/**
	 * @param       $sql
	 * @param array $params
	 * @return mixed
	 */
	private function fetchSingle($sql, $params = []) {
		$query  = $this->db->q($sql, $params);
		$result = $query->fetchColumn(0);

		return $result;
	}

	/**
	 * @param      $table
	 * @param null $type_id
	 * @return string
	 */
	private function getTypeId($table, $type_id = null) {
		$type = '';
		switch ($table) {
			case 'eroticmassage' :
				$type = $this->placeType['erotic-massage-parlor'];
				break;
			case 'strip_club2' :
				$type = $this->placeType['strip-club'];
				break;
			case 'adult_stores' :
				$type = $this->placeType['sex-shop'];
				break;
			case 'lingeriemodeling1on1' :
				$type = $this->placeType['lingerie-modeling-1on1'];
				break;
			case 'gay' :
				$type = $this->placeType['gay'];
				break;
			case 'gaybath' :
				$type = $this->placeType['gay-bath-house'];
				break;
			case 'brothel' :
				$type = $this->placeType['brothel'];
				break;
			case 'lifestyle' :
				switch ($type_id) {
					case 4:
						$type = $this->placeType['nudist-colony'];
						break;
					case 3:
						$type = $this->placeType['topless-pool'];
						break;
					case 2:
						$type = $this->placeType['bdsm'];
						break;
					case 1:
					case null:
					default :
						$type = $this->placeType['swingers-club'];
				}
				break;
		}

		return $type;
	}

	/**
	 * @param $table
	 * @param $row
	 * @param $mainName
	 * @param $name
	 * @return false|float|int|mixed|string|null
	 */
	private function prepareData($table, $row, $mainName, $name) {
		switch ($mainName) {
			case 'worker_comment':
			case 'suspend_reason':
				$data = isset($row['worker_comment']) ? $row['worker_comment'] : '';
				if (isset($row['suspend_reason'])) {
					$data .= $data ? ' ' : ''.'Suspend reason: '.$row['suspend_reason'];
				}
				break;
			case 'updated':
			case 'deleted':
				$data = is_null($row[$name]) ? null : strtotime($row[$name]);
				break;
			case 'last_review_id':
				$data = intval($row[$name]);
				break;
			case 'overall':
				$data = is_null($row[$name]) ? 0 : (float)$row[$name];
				break;
			case 'dancer':
				$data = isset($this->attrDancer[$row[$name]]) ? $this->attrDancer[$row[$name]] : null;
				break;
			//BUG some places had too many decimal places (double datatype) -> there was insert error
			case 'loc_lat':
			case 'loc_long':
				$data = number_format($row[$name], 6, ".", "");
				break;
			default:
				if (in_array($mainName, $this->attrsBooleanToBoolean3)) {
					$data = $this->transformBooleanToBoolean3($row[$name]);
				} else {
					$data = $row[$name];
				}
		}

		return $data;
	}

	/**
	 * @param $value
	 * @return int
	 */
	private function transformBooleanToBoolean3($value) {
		if ($value) {
			return 2;
		}
		return 0;
	}

	/**
	 * @param      $table
	 * @param      $row
	 * @param      $toId
	 * @param      $imageDir
	 * @param bool $renameImage
	 */
	private function copyImgs($table, $row, $toId, $imageDir, $renameImage = true) {
		$image = $thumb = $map_image = null;
		if ($row['image']) {
			$image = $this->copyImgItem($table, $row['image'], $toId, 'image', $imageDir, $renameImage);
		}
		if ($row['thumb']) {
			$thumb = $this->copyImgItem($table, $row['thumb'], $toId, 'thumb', $imageDir, $renameImage);
		}
		if ($row['map_image']) {
			$map_image = $this->copyImgItem($table, $row['map_image'], $toId, 'map_image', $imageDir, $renameImage);
		}
		if ($row['image'] != $image || $row['thumb'] != $thumb || $row['map_image'] != $map_image) {
			$res = $this->db->q("UPDATE place SET image = ?, thumb = ?, map_image = ? WHERE place_id = ? LIMIT 1",
				[$image, $thumb, $map_image, $toId]);
			$aff = $this->db->affected($res);
			if ($aff != 1) {
				$this->error("updating place #{$toId} with new filenames '{$image}' '{$thumb}' '{$map_image}' failed !");
			}
		}
	}

	/**
	 * @param      $table
	 * @param      $row
	 * @param      $toId
	 * @param      $imageDir
	 */
	private function copyCoupon($table, &$row, $toId, $imageDir ) {

		if ($row['coupon']) {
			$row['coupon'] = $this->copyImgItem($table, $row['coupon'], $toId, 'coupon', $imageDir, true);
		}
	}

	/**
	 * @param      $table
	 * @param      $image
	 * @param      $toId
	 * @param      $typeImage
	 * @param      $country
	 * @param bool $renameImage
	 * @return string|null
	 */
	private function copyImgItem($table, $image, $toId, $typeImage, $country, $renameImage = false) {
		if ($image) {
			//BUG: map images were being read from eroticmassage/maps instead of correct maps/eroticmassage
			$pathType      = $this->getPathType($typeImage, $renameImage?$table:$country, ($table!="place"));
			$pathTypePlace = $this->getPathType($typeImage, $this->mainTable);
			$image_src     = $this->configImagePath."{$pathType}/{$image}";
//			if ($this->configDev && !file_exists($image_src)) {
//				$img_from  = $this->configImagePath."test_img.jpg";
//				if(!file_exists($this->configImagePath."{$pathType}")) {
//					mkdir($this->configImagePath."{$pathType}", 0777, true);
//				}
//				$ret       = copy($img_from, $image_src );
//				if (!$ret) {
//					$this->error("test img error copy ! {$img_from}->{$image_src}");
//				}
//			}
			if (!file_exists($image_src)) {
				if (!$this->configDev) {
					$this->error("{$typeImage} file '{$image_src}' not found !");
				}

				//BUG - domestic place images were not copied at all
				return $image;
			}

			if ($renameImage) {
				if ($typeImage === 'map_image') {
					$image = "{$toId}.png";
				} else {
					$image = $this->getNotUsedFilename($this->configImagePath."{$pathTypePlace}", $toId);
				}
			}
			$image_tgt = $this->configImagePath."{$pathTypePlace}/{$image}";
			$ret       = copy($image_src, $image_tgt);
			if (!$ret) {
				$this->error("failed to copy {$typeImage} file '{$image_src}' -> '{$image_tgt}' !");
			}
		}
		return $image;
	}

	/**
	 * @param $table
	 * @param $fromId
	 * @param $toId
	 * @param $imageDir
	 * @param $renameImage
	 */
	private function copyAdditionalImgs($table, $fromId, $toId, $imageDir, $renameImage = false) {
		$module   = $this->moduleFromTable[$table];
		$res_pics = $this->fetchAll($this->sqlPlacePicture, [$module, $fromId]);
		foreach ($res_pics as $row_pics) {
			$image = $thumb = '';
			if ($row_pics['filename']) {
				$image = $this->copyImgItem($table, $row_pics['filename'], $toId, 'image', $imageDir, $renameImage);
			}
			if ($row_pics['thumb']) {
				$thumb = $this->copyImgItem($table, $row_pics['thumb'], $toId, 'thumb', $imageDir, $renameImage);
			}
			switch (true) {
				//BUG after moving extra images with renaming images for domestic places, the place_picture table entry was not updates
				case $row_pics['filename'] != $image:
				case $row_pics['thumb'] != $thumb:
					$res = $this->db->q($this->sqlUpdatePlacePicture, [$toId, $image, $thumb, $row_pics['picture']]);
					$aff = $this->db->affected();
					if ($aff != 1) {
						$this->error("updating place_picture #{$toId} with new filenames '{$image}' '{$thumb}' failed !");
					}
					break;
			}
		}
	}

	/**
	 * @param      $typeImage
	 * @param      $table
	 * @param bool $oldMapsPath
	 * @return string
	 */
	private function getPathType($typeImage, $table, $oldMapsPath = false) {
		if($table) {
			if (isset($this->pathFromTable[$table])) {
				$pathImage = $this->pathFromTable[$table];
			} else {
				$pathImage = $table;
			}
		} else {
			$pathImage = 'place';
		}
		switch ($typeImage) {
			case 'thumb':
				$pathType = "{$pathImage}/t";
				break;
			case 'coupon':
				$pathType = "{$pathImage}/c";
				break;
			case 'map_image':
				$pathType = $oldMapsPath?"maps/{$table}":"{$pathImage}/maps";
				break;
			case 'image':
			default:
				$pathType = "{$pathImage}";
		}

		return $pathType;
	}

	/**
	 * @param $dir
	 * @param $toId
	 * @return string
	 */
	private function getNotUsedFilename($dir, $toId) {

		do {
			$filename = system::_makeText(10)."_".$toId.".jpg";
			$filepath = $dir."/".$filename;
		} while (file_exists($filepath));

		return $filename;
	}

	/**
	 * @param $table
	 */
	private function checkAndCreatePathForTypes($table) {
		if($table) {
			if (isset($this->pathFromTable[$table])) {
				$pathImage = $this->pathFromTable[$table];
			} else {
				$pathImage = $table;
			}
		} else {
			$pathImage = 'place';
		}
		foreach (['image', 'thumb', 'map_image', 'coupon'] as $typeImage) {
			switch ($typeImage) {
				case 'thumb':
					$pathType = "{$pathImage}/t";
					break;
				case 'coupon':
					$pathType = "{$pathImage}/c";
					break;
				case 'map_image':
					$pathType = "{$pathImage}/maps";
					break;
				case 'image':
				default:
					$pathType = "{$pathImage}";
			}
			$dirname = $this->configImagePath."{$pathType}/";
			if (!file_exists($dirname)) {
				mkdir($dirname, 0777);
			}
		}
	}

	/**
	 * @param $table
	 * @param $nonExistLocations
	 */
	private function outLocation($table, $nonExistLocations) {
		$this->note($table);
		$rows = [];
		foreach ($nonExistLocations as $toId => $location_id) {
			$rows[] = [$toId, $location_id];
		}
		$this->table(['Place ID', 'Location ID'], $rows);
	}

	/**
	 * @param $table
	 * @param $fromId
	 * @param $toId
	 */
	private function copyReview($table, $fromId, $toId) {
		$module     = $this->moduleFromTable[$table];
		$res_review = $this->fetchAll($this->sqlPlaceReview, [$module, $fromId]);
		foreach ($res_review as $row_review) {
			if ($row_review['review_id']) {
				$res = $this->db->q("
UPDATE place_review SET id = ?, module = 'place' WHERE review_id = ? LIMIT 1
",
					[$toId, $row_review['review_id']]);
				$aff = $this->db->affected($res);
				if ($aff != 1) {
					$this->error("updating place_review #{$toId}(old={$fromId}) with new module failed !");
				}
			} else {
				$this->error("place_review #{$toId} no change module !");
			}
		}
		//BUG
		//update module and id in place_review_provider table
		$res2 = $this->db->q(
			"UPDATE place_review_provider SET module = 'place', place_id = ? WHERE module = ? AND place_id = ?", 
			[$toId, $module, $fromId]
			);
		$aff = $this->db->affected($res2);
	}

	/**
	 * @param $table
	 * @param $fromId
	 * @param $toId
	 */
	private function updateBusinessOwner($table, $fromId, $toId) {
		$sectionFrom     = $this->sectionFromTable[$table];
		if($sectionFrom) {
			$resBusinessOwner = $this->fetchAll($this->sqlBusinessOwner, [$sectionFrom, $fromId]);
			foreach ($resBusinessOwner as $rowBusinessOwner) {
				$res = $this->db->q("
UPDATE account_businessowner SET place_id = ?, section = 'place' WHERE id = ? LIMIT 1
",
					[$toId, $rowBusinessOwner['id']]);
				$aff = $this->db->affected($res);
				if ($aff != 1) {
					$this->error("updating account_businessowner #{$toId}(old={$fromId}) with new section failed !");
				}
			}
		}
	}

	/**
	 *  Defined access to the field for each country separately
	 */
	private function prepareLocationAttributes() {
		foreach ($this->codeCountry as $countryId) {
			$resLocationAttr = $this->fetchAll($this->sqlLocationAttr, [$countryId]);
			$resLocationAttr = array_column($resLocationAttr, 'attribute_id');
			$insert = '';
			$params = [];
			foreach ($this->mainTableAttributeFields as $fieldName => $fieldId) {
				if(!in_array($fieldId, $resLocationAttr)) {
					$insert .= $insert ? ',' : '';
					$insert .= "(?, ?)";
					$params[] = $countryId;
					$params[] = $fieldId;
				}
			}
			if($insert) {
				$result = $this->db->q($this->sqlInsertLocationAttribute.' '.$insert, $params);
			}
		}
	}

	/**
	 * Updated a multiselect attribute for types massages
	 */
	private function prepare179Attribute() {
		$resAttr    = $this->fetchSingle('SELECT options FROM attribute WHERE attribute_id=?', [$this->attr179Num]);
		$resOptions = $this->fetchSingle('
SELECT GROUP_CONCAT( CONCAT(tag_id,"@", tag_name)
ORDER BY  tag_name ASC SEPARATOR "|")
FROM eroticmassage_tag
');
		if ($resAttr !== $resOptions) {
			$res = $this->db->q("UPDATE attribute SET datatype = 'multi', options = ? WHERE attribute_id = ? LIMIT 1",
				[$resOptions, $this->attr179Num]);
			$aff = $this->db->affected();
			if ($aff != 1) {
				$this->error("updating attribute #{$this->attr179Num}(type=multi) failed !");
			}
		}
	}

	/**
	 * Moved all images for table place from {country} folder to `place` folder
	 */
	private function prepareExistImageForPlace() {
		$time_start      = microtime(true);
		$interval        = 0;
		$countMoveImages = 0;
		$res             = $this->db->q($this->sqlPlaceTable, []);
		while ($row = $this->db->r($res, MYSQL_ASSOC)) {
			$interval++;
			$this->copyImgs($this->mainTable, $row, $row['place_id'],  $row['country'], false);
			$this->copyAdditionalImgs($this->mainTable, $row['place_id'], $row['place_id'], $row['country'], false);
			$this->copyCouponImage($row['place_id'], $row['country']);
			$countMoveImages++;

			If ($interval > 100) {
				$interval = 0;
				$this->note("Copy images for {$countMoveImages} rows for {$this->mainTable}");
			}
			// @todo for debug
			If ($this->countMoveRows > 10) {
				break;
			}
		}
		$time_end = microtime(true);
		$time     = round($time_end - $time_start, 2);
		$this->note("Done preparing images for {$this->mainTable}: {$countMoveImages} rows with images # "
			. count($this->db->sqls) . " requests # {$time} sec");
	}

	/**
	 * Copy coupon to new place without renaming
	 *
	 * @param $place_id
	 * @param $country
	 */
	private function copyCouponImage($place_id, $country) {
		$image = $this->fetchSingle($this->sqlPlaceAttribute, [$this->attr148Num, $place_id]);
		$this->copyImgItem($this->mainTable, $image, $place_id, 'coupon', $country);
	}

}


