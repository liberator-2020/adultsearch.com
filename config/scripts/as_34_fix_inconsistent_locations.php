<?php
chdir(__DIR__);
define('_CMS_FRONTEND', 1);

require_once('../../html/inc/config.php');
require_once('../../html/inc/common.php');

global $db;

/**
 *
 */
$needFixing = [
	'eroticmassage' => [
		2709 => 2514,
	],
	'strip_club2'   => [
		5893 => 100461,
		//		5838 => 0,
	],
	'adult_stores'  => [
		8504  => 14260,
		8505  => 14403,
		8511  => 14402,
		8512  => 14402,
		8514  => 14402,
		8515  => 14402,
		8517  => 14402,
		8519  => 14402,
		8537  => 7139,
		8695  => 23980,
		11740 => 43065,
	],
	'gaybath'       => [
		182 => 99980,
		181 => 43063,
		180 => 43063,
		179 => 43063,
		178 => 43063,
		177 => 43063,
		176 => 43063,
		175 => 43063,
		165 => 43092,
		163 => 43092,
		164 => 43092,
		162 => 43092,
		161 => 43092,
		135 => 43100,
		134 => 43097,
		133 => 43097,
		130 => 100454,
	],
];

/**
 *
 */
foreach ($needFixing as $table => $items) {
	foreach ($items as $id => $loc_id) {
		$res = $db->q("
UPDATE `{$table}` SET `loc_id` = ?  WHERE `id` = ?;
", [$loc_id, $id]);

		if ($db->affected() != 1) {
			echo "Not updated table '{$table} row id={$id}", PHP_EOL;
		}
	}
	echo "Ended to update table '{$table}", PHP_EOL;
}

/**
 *
 */
$table_del = 'strip_club2';
$id_del    = 5838;
$res_del   = $db->q("
DELETE FROM `{$table_del}` WHERE `id`=?;
", [$id_del]);

if ($db->error()) {
	echo "NOT deleted from table '{$table_del} row id={$id_del}", PHP_EOL;
} else {
	echo "Deleted from table '{$table_del} row id={$id_del}", PHP_EOL;
}

die('All done.');