<?php
chdir(__DIR__);
define('_CMS_FRONTEND', 1);

require_once('../../html/inc/config.php');
require_once('../../html/inc/common.php');

global $db, $config_image_backup;

$time_start = microtime(true);
echo "Starting...\n";

$handler = new TransferGEOToPlaces($db, $config_image_backup);

$updated = $handler->execute();
echo "Updated {$updated} places\n";

echo "Done. ";

$time_end = microtime(true);
$time     = round($time_end - $time_start, 2);
echo " time # {$time} sec\n";
exit();


class TransferGEOToPlaces {

	/**
	 * @var string
	 */
	private $sqlExistCountry = "
SELECT lc.dir as country
FROM place p
INNER JOIN location_location l ON p.loc_id = l.loc_id
INNER JOIN location_location lc ON l.country_id = lc.loc_id
GROUP BY lc.dir 
ORDER BY lc.dir ASC
";

	/**
	 * @var string
	 */
	private $sqlGetPlace = "
SELECT place_id as id, lc.dir as module 
FROM place p
INNER JOIN location_location l ON p.loc_id = l.loc_id
INNER JOIN location_location lc ON l.country_id = lc.loc_id
WHERE p.loc_lat IS NULL OR p.loc_long  IS NULL OR (p.loc_lat=0 AND p.loc_long=0 );
";

	private $sqlGetTable = "
SELECT id , '%s' as module 
FROM `%s` p
INNER JOIN location_location l ON p.loc_id = l.loc_id
INNER JOIN location_location lc ON l.country_id = lc.loc_id 
WHERE p.loc_lat IS NULL OR p.loc_long  IS NULL OR (p.loc_lat=0 AND p.loc_long=0 );
";

	private $sqlImageBackup = "
SELECT id, filename, module FROM image_backup WHERE id=? and module=?
";
	/**
	 * @var string[]
	 */
	private $existCountry = [];

	/**
	 * @var string[]
	 */
	private $tables
		= [
			'place',
			'eroticmassage',
			'strip_club2',
			'adult_stores',
			'lingeriemodeling1on1',
			'gay',
			'gaybath',
			'brothel',
			'lifestyle',
		];

	/**
	 * @var \db
	 */
	private $db;

	/**
	 * @var string;
	 */
	private $configImageBackup;

	public function __construct(\db $db, $config_image_backup) {
		$this->db                = $db;
		$this->configImageBackup = $config_image_backup;
	}

	/**
	 * @return int
	 */
	public function execute() {
		$updatedPlaces = 0;
		foreach ($this->tables as $table) {
			$res = $this->getData($table);
			while ($row = $this->db->r($res, MYSQL_ASSOC)) {
				$files = $this->fetchAll($this->sqlImageBackup, [$row['id'], $row['module']]);
				$files = array_column($files, 'filename');
				if (count($files)) {
					foreach ($files as $fileName) {
						$file = $this->configImageBackup.'/'.$fileName;
//						copy($this->configImageBackup.'/test.jpg', $file);
						if (file_exists($file) && $geo_coords = $this->readGeoLocation($file)) {
							$colName = $table == 'place' ? 'place_id' : 'id';
							//$this->db->q("update {$table} set loc_lat='{$geo_coords['lat']}', loc_long='{$geo_coords['lng']}' where {$colName} = '{$row['id']}'");
							$this->db->q("update {$table} set loc_lat = ?, loc_long = ? where {$colName} = ? LIMIT 1", [$geo_coords['lat'], $geo_coords['lng'], $row['id']]);
							$updatedPlaces++;
							break;
						}
					}
				}
			}
		}
		return $updatedPlaces;
	}

	/**
	 * @param $table
	 * @return \PDOStatement
	 */
	private function getData($table) {
		if ($table === 'place') {
			$res = $this->db->q($this->sqlGetPlace, []);
		} else {
			$res = $this->db->q(sprintf($this->sqlGetTable, $this->getModuleFromTable($table), $table), []);
		}

		return $res;
	}

	/**
	 * Returns an array of latitude and longitude from the Image file
	 *
	 * @param files $file
	 * @return array|bool
	 */
	function readGeoLocation($file) {
		if (is_file($file)) {
			$info = exif_read_data($file);
			if (isset($info['GPSLatitude']) && isset($info['GPSLongitude'])
				&& isset($info['GPSLatitudeRef'])
				&& isset($info['GPSLongitudeRef'])
				&& in_array($info['GPSLatitudeRef'], array('E', 'W', 'N', 'S'))
				&& in_array($info['GPSLongitudeRef'], array('E', 'W', 'N', 'S'))
			) {

				$GPSLatitudeRef  = strtolower(trim($info['GPSLatitudeRef']));
				$GPSLongitudeRef = strtolower(trim($info['GPSLongitudeRef']));

				$lat_degrees_a = explode('/', $info['GPSLatitude'][0]);
				$lat_minutes_a = explode('/', $info['GPSLatitude'][1]);
				$lat_seconds_a = explode('/', $info['GPSLatitude'][2]);
				$lng_degrees_a = explode('/', $info['GPSLongitude'][0]);
				$lng_minutes_a = explode('/', $info['GPSLongitude'][1]);
				$lng_seconds_a = explode('/', $info['GPSLongitude'][2]);

				$lat_degrees = $lat_degrees_a[0] / $lat_degrees_a[1];
				$lat_minutes = $lat_minutes_a[0] / $lat_minutes_a[1];
				$lat_seconds = $lat_seconds_a[0] / $lat_seconds_a[1];
				$lng_degrees = $lng_degrees_a[0] / $lng_degrees_a[1];
				$lng_minutes = $lng_minutes_a[0] / $lng_minutes_a[1];
				$lng_seconds = $lng_seconds_a[0] / $lng_seconds_a[1];

				$lat = (float)$lat_degrees + ((($lat_minutes * 60) + ($lat_seconds)) / 3600);
				$lng = (float)$lng_degrees + ((($lng_minutes * 60) + ($lng_seconds)) / 3600);

				//If the latitude is South, make it negative.
				//If the longitude is west, make it negative
				$GPSLatitudeRef == 's' ? $lat *= -1 : '';
				$GPSLongitudeRef == 'w' ? $lng *= -1 : '';

				return array(
					'lat' => $lat,
					'lng' => $lng,
				);
			}
		}

		return false;
	}

	/**
	 * @param $table
	 * @return string
	 */
	function getModuleFromTable($table) {
		switch ($table) {
			case 'eroticmassage':
				return 'emp';
			case 'strip_club2':
				return 'sc';
			case 'adult_stores':
				return 'adultstore';
			case 'lingeriemodeling1on1':
				return 'lingeriemodeling1on1';
			case 'gay':
				return 'gay';
			case 'gaybath':
				return 'gaybath';
			case 'brothel':
				return 'brothel';
			case 'lifestyle':
				return 'lifestyle';
			default:
				return '';
		}
	}

	/**
	 * @param string $sql
	 * @param array $params
	 * @return array
	 */
	private function fetchAll($sql, $params = []) {
		$query  = $this->db->q($sql, $params);
		$result = $query->fetchAll(PDO::FETCH_ASSOC);

		return $result;
	}

}
