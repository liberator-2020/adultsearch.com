<?php
chdir(__DIR__);
define('_CMS_FRONTEND', 1);

require_once('../../html/inc/config.php');
require_once('../../html/inc/common.php');

global $db;

$sql_place = "
SELECT p.place_id, p.phone1, p.phone2, lc.s as country_code
FROM place p 
INNER JOIN location_location l ON p.loc_id = l.loc_id
INNER JOIN location_location lc ON l.country_id = lc.loc_id
WHERE p.deleted IS NULL AND ( p.phone1 IS NOT NULL OR p.phone2 IS NOT NULL)
";

$sql_update = "
UPDATE place p SET  p.phone1 = ?, p.phone2 = ? WHERE p.place_id = ?;
";

$needFixing = [
	1195 => ['+3699544218', null],
	2280 => ['+85516366610', '+85512366610'],
	3765 => ['6334-2628', '6338-013'],
	3862 => ['6327-1518', '9648-482'],
	9732 => ['+971 4 336 6607', '334 8777'],
	//	1762  => [null, null],
	//	1796  => [null, null],
	10839 => ['90471769', '97510272'],
	10792 => ['+43 (1) 595 52 55', '+43 (1) 595 52 75'],
	11134 => ['+65 97772176', null],
	11137 => ['(65) 96450877', '(65) 83182610'],
	11197 => ['975212241', '676489044'],
	11209 => ['0034 964 662 791', '676 045 876'],
	11266 => ['044 221 57 78', '044 221 62 51'],
	11272 => ['056-409-45-50', '097-11-62-314'],
	11274 => ['067-286-14-10', '050-548-32-82'],
	11306 => ['09063885575', '09997855575'],
	11330 => ['3409987575', null],
	11337 => [null, null],
	11584 => ['+38(099) 3807744', null],
	11585 => ['+38(067) 656 46 46', null],
	//	11601 => [null, null],
	11648 => ['(021) 385-2000', '385-2121'],
];

$time_start = microtime(true);
echo "Starting normalizy phones...\n";

// first correct
foreach ($needFixing as $place_id => $phones) {
	$res_correct = $db->q($sql_update, [$phones[0], $phones[1], $place_id]);

	if ($db->affected() != 1) {
		echo "Not updated phones '{$phones[0]}' '{$phones[1]}' place id={$place_id}\n";
	}
}

echo "Done direct correcting phones... (Stage 1)\n";
$res = $db->q($sql_place);

while ($row = $db->r($res, MYSQL_ASSOC)) {
	$phone1 = makeProperPhoneNumberE164(makeOnlyDigitPhoneNumberWithPlus(clearTail($row['phone1'])), $row['country_code'],
		$row['place_id']);
	$phone2 = makeProperPhoneNumberE164(makeOnlyDigitPhoneNumberWithPlus(clearTail($row['phone2'])), $row['country_code'],
		$row['place_id']);

	$res_upd = $db->q($sql_update, [$phone1, $phone2, $row['place_id']]);
	if ($db->affected() != 1 && ($phone1 !== $row['phone1'] || $phone2 !== $row['phone2'])) {
		echo "Not updated phone1#'{$row['phone1']}' or phone2#'{$row['phone2']}' row place_id={$row['place_id']}", PHP_EOL;
	}
}

$time_end = microtime(true);
$time     = round($time_end - $time_start, 2);
echo "All done.  time # {$time} sec\n";
exit();

/**
 * @param $phone
 * @param $country_code
 * @param $place_id
 *
 * @return string
 */
function makeProperPhoneNumberE164($phone, $country_code, $place_id) {

	if (!$phone) {
		return $phone;
	}

	if (!$country_code) {
		$country_code = 'US';
	}

	$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
	try {
		$phone_proto = $phoneUtil->parse($phone, $country_code);
	} catch (\libphonenumber\NumberParseException $e) {
		echo("Error : place_id={$place_id} phone '{$phone}'  country code '{$country_code}' : {$e->getCode()}#"
			.$e->getMessage()), PHP_EOL;

		return $phone;
	}


	return $phoneUtil->format($phone_proto, \libphonenumber\PhoneNumberFormat::E164);
}

/**
 * @param $phone
 * @return mixed
 */
function clearTail($phone) {
	$pos       = [];
	foreach ([',', '/', '|', 'or'] as $item) {
		$pos_tmp = strpos(strtolower($phone), $item, $item=='/'?8:0);
		if ($pos_tmp !== false) {
			$pos[] = $pos_tmp;
		}
	}
	if ($pos) {
		$phone = substr($phone, 0, min($pos));
	}

	return $phone;
}