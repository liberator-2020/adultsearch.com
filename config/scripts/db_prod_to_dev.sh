#!/bin/bash

# This shell script downloads newest latest DB from production and imports it into local db
# You need to have SSH public key set up in asdev account on adultsearch.com server

TO_HOST="127.0.0.1"
TO_DBNAME="asdev"
TO_DBUSER="asdev"
TO_DBPASS="elanSestricky123"

DUMP_FILE="as_latest.sql.gz"
DUMP_FILE_UNPACKED="as_latest.sql"

echo "Downloading latest db from asdev@test.adultsearch.com ..."
scp "asdev@test.adultsearch.com:~/as_latest.sql.gz" "./${DUMP_FILE}"
ret=$?
if [[ $ret -ne 0 ]]; then
	echo "Error downloading db over SSH, please make sure your private key is correctly set up in ./.ssh/config for domain adultsearch.com"
	exit 1
fi

echo "Unpacking ..."
gzip -d "$DUMP_FILE"
ret=$?
if [[ $ret -ne 0 ]]; then
	echo "Error unpacking file"
	exit 1
fi

echo "Importing to local db, this might take 2-3 minutes ..."
mysql -h $TO_HOST -u $TO_DBUSER --password=$TO_DBPASS $TO_DBNAME < "$DUMP_FILE_UNPACKED"
ret=$?
if [[ $ret -ne 0 ]]; then
	echo "db import failed !"
	exit 1
fi

rm "$DUMP_FILE_UNPACKED"

echo "Done."
exit 0
