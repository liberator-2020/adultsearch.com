#!/bin/bash

# This shell script downloads newest latest DB from production and imports it into local db
# You need to have SSH public key set up in asdev account on asescorts.com server

TO_HOST="127.0.0.1"
TO_DBNAME="asdev"
TO_DBUSER="asdev"
TO_DBPASS="elanSestricky123"


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DUMP_FILE="as_latest.sql.gz"
DUMP_FILE_UNPACKED="as_latest.sql"
PATH_TO_DOCKER="${DIR}/../docker"
PATH_TO_SCRIPTRS="${DIR}/../scripts"
URL="http://vpn-gate.lan/"

if [[ ! -f "${PATH_TO_DOCKER}/.env" ]]; then
	echo "Run commands first:"
	echo "cd ${PATH_TO_DOCKER}"
	echo "cp .env.example .env "
	exit 1
fi

echo "Downloading latest db from ${URL}${DUMP_FILE} to docker..."
wget "${URL}${DUMP_FILE}" -O "${PATH_TO_DOCKER}/mariadb/docker-entrypoint-initdb.d/${DUMP_FILE}"
ret=$?
if [[ $ret -ne 0 ]]; then
	echo "Error downloading db from LAN!!! Asq anybody why :)"
	exit 1
fi

cd "${PATH_TO_DOCKER}"
docker-compose down

cd ./mariadb
if [[  -f "./docker-entrypoint-initdb.d/${DUMP_FILE}" || -f "./docker-entrypoint-initdb.d/${DUMP_FILE_UNPACKED}" ]]; then
    find ./data/ ! -name '.gitignore' -type f -exec rm -f {} +
    find ./data/ ! -path './data/' -type d -exec rm -rf {} +
fi

cd ..
echo "Restarting docker"
#docker-compose up --build mariadb
${PATH_TO_SCRIPTRS}/docker_start.sh

#echo "Importing test data ..."
#mysql -h $TO_HOST -u $TO_DBUSER --password=$TO_DBPASS $TO_DBNAME < "../config/test/test_init.sql"

exit 0