<?php
chdir(__DIR__);
define('_CMS_FRONTEND', 1);

require_once('../../html/inc/config.php');
require_once('../../html/inc/common.php');

global $db, $sphinx;

echo "Starting checking...\n";

if (!is_object('SphinxClient')) {
	require_once(_CMS_ABS_PATH."/inc/classes/class.sphinxapi.php");
}

$host  = 'sphinx';
$ports = array(9306, 9312);

foreach ($ports as $port) {
	$connection = @fsockopen($host, $port);

	if (is_resource($connection)) {
		echo '#>'.$host.':'.$port.' '.'('.getservbyport($port, 'tcp').') is open.'."\n";

		fclose($connection);
	} else {
		echo '!!!#>'.$host.':'.$port.' is not responding.'."\n";
	}
}

$sS     = new SphinxClient();
$result = $sS->Status();
echo  'Status#';print_r($result);echo PHP_EOL;
$sS->SetConnectTimeout(1);
$sS->SetLimits(0, 3, 3);
$result = $sS->query("", 'dir');
if($result) {
	echo 'Index Dir#';
	print_r($result);
	echo PHP_EOL;
} else {
	var_dump($sS);
}

