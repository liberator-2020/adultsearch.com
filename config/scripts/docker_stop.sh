#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PATH_TO_DOCKER="${DIR}/../docker"

if [[ ! -f "${PATH_TO_DOCKER}/.env" ]]; then
	echo "Run commands first:"
	echo "cd ${PATH_TO_DOCKER}"
	echo "cp .env.example .env "
	exit 1
fi

cd "${PATH_TO_DOCKER}"
echo "Stop docker"
docker-compose down

exit 0