<?php
chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../../html/inc/config.php');
require_once('../../html/inc/common.php');

global $db;

$res = $db->q("
select email, username from account where deleted is null and banned = 0 and agency = 1 and email_confirmed = 1
", []);

while ($row = $db->r($res)){

    if (!$row['email'])
        continue;

    $params = [
        "username" => $row["username"]
    ];

    $subject = "Sticky Upgrades for Body Rubs";

    sendSupportEmail("classifieds/email/br_sticky_notification", $params, $subject, $row['email']);
}

die();