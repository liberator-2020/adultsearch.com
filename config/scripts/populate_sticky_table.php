<?php
chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../../html/inc/config.php');
require_once('../../html/inc/common.php');

global $db;

// get classifieds with sponsor enabled
$res = $db->q("
SELECT c.id, c.type, c.sponsor_position, UNIX_TIMESTAMP(c.date) as date, c.expires, c.expire_stamp, c.done, COUNT(cl.id) as loc_count, a.email
FROM classifieds c
INNER JOIN classifieds_loc cl on cl.post_id = c.id AND cl.done = 1
INNER JOIN account a on a.account_id = c.account_id
WHERE c.done = 1 
  and (c.sponsor = 1 or c.sponsor_mobile = 1) 
  and c.deleted IS NULL
GROUP BY c.id
", []);

$count = 0;
while ($row = $db->r($res)) {
	$clad_id = $row["id"];
	$email = $row["email"];
	$type = $row["type"];
	$sponsor_position = $row["sponsor_position"];
	$date_stamp = $row["date"];
	$expires = $row["expires"];
	$expire_stamp = $row["expires_stamp"];
	if (!$expire_stamp)
		$expire_stamp = strtotime($expires);
	$done = $row["done"];
	$loc_count = $row["loc_count"];

	//echo "{$clad_id},{$email},{$type},{$sponsor_position},{$date_stamp},{$expires},{$expire_stamp},{$done}, loc_count = {$loc_count}";

	if ($loc_count > 10) {
		//echo " => BIG ADVERTISER, skipping...\n";
		continue;
	}

	echo "\nImporting #{$clad_id} (of {$email})...\n";

	$res2 = $db->q("SELECT cl.loc_id FROM classifieds_loc cl WHERE cl.post_id = ? AND cl.done = 1", [$clad_id]);
	while ($row2 = $db->r($res2)) {
		$loc_id = $row2["loc_id"];
		echo "Loc #{$loc_id}: ";

		//INSERT
		$res3 = $db->q("
			INSERT INTO classified_sticky 
			(classified_id, loc_id, type, days, position, created_stamp, expire_stamp, done) 
			VALUES
			(?, ?, ?, ?, ?, ?, ?, 1)",
			[$clad_id, $loc_id, $type, 30, null, $date_stamp, $expire_stamp]
			);
		$id = $db->insertid($res3);
		if (!$id)
			die("Error - cant insert into classified_sticky, clad_id={$clad_id}, loc_id={$loc_id} !\n");
		echo "OK\n";
	}


	//UPDATE of classifieds entry
	$res2 = $db->q("UPDATE classifieds SET sponsor = 0, sponsor_mobile = 0, sponsor_position = 0 WHERE id = ? LIMIT 1", [$clad_id]);
	$aff = $db->insertid($res2);
	if (!$id)
		die("Error - cant update classifieds entry, clad_id={$clad_id} !\n");
	echo "UPDATE OK\n";
	
	$count++;
}

echo "Converted {$count} ads.\n";

die;

