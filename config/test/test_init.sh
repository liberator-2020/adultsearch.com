#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
echo "TEST init script, script_dir=${SCRIPT_DIR}"

#first parameter needs to be target deploy dir
DEPLOY_DIR="$1"
if [[ -z "$DEPLOY_DIR" ]] || [[ ! -d "$DEPLOY_DIR" ]]; then
	#DEPLOY DIR not specified, try to guess it
	DEPLOY_DIR="${SCRIPT_DIR}/../.."
	echo "Estimated DEPLOY_DIR='${DEPLOY_DIR}'"
	TEST=`find "${DEPLOY_DIR}" -maxdepth 1 -type d -name "html" | wc -l`
	if [[ $TEST -ne 1 ]]; then
		echo "Invalid/unspecified deploy directory '${DEPLOY_DIR}' !"
		exit 1
	fi
fi
echo "deploy_dir=${DEPLOY_DIR}"

#second parameter needs to be username
USERNAME="$2"
if [[ -z "$USERNAME" ]] || [[ ! -d "$USERNAME" ]]; then
	#USERNAME not specified, try to guess it
	USERNAME=`whoami`
fi
echo "username=${USERNAME}"

#get db credentials from parameters file
PARAMS_FILEPATH="${DEPLOY_DIR}/html/inc/config.php"
if [[ ! -f "$PARAMS_FILEPATH" ]]; then
	echo "Params file '${PARAMS_FILEPATH}' does not exist !"
	exit 2
fi

get_param() {
	param_name="$1"
	val=`grep "${param_name}" "${PARAMS_FILEPATH}" | cut -d '=' -f 2 | sed 's/^[ \t'\''"]*//;s/[ \t;'\''"]*$//'`
	echo $val
}

DB_HOST=$(get_param "config_db_host")
DB_USER=$(get_param "config_db_user")
DB_PASS=$(get_param "config_db_pass")
DB_NAME=$(get_param "config_db_name")
echo "DB credentials: host=${DB_HOST} user=${DB_USER}, database=${DB_NAME}"

#copy profile images
#cp -f "${SCRIPT_DIR}/lady_273.jpg" "${DEPLOY_DIR}/sf/web/images/t/lady_273.jpg"
#cp -f "${SCRIPT_DIR}/lady_300.jpg" "${DEPLOY_DIR}/sf/web/images/p/lady_300.jpg"

mysql -h "$DB_HOST" -u "$DB_USER" --password="$DB_PASS" "$DB_NAME" < "${SCRIPT_DIR}/test_init.sql"
ret=$?
if [[ $ret -ne 0 ]]; then
	echo "Test data import script failed."
	exit 3
fi

#update sphinx file with correct credentials
SPHINX_CONF_FILEPATH="${DEPLOY_DIR}/config/sphinx.conf"
if [[ ! -f "$SPHINX_CONF_FILEPATH" ]]; then
	echo "Sphinx conf file '${SPHINX_CONF_FILEPATH}' does not exist !"
	exit 2
fi
SPHINX_PORT=$(get_param "config_sphinx_port")
echo "Replacing values in sphinx conf: filepath='${SPHINX_CONF_FILEPATH}', port='${SPHINX_PORT}', username='${USERNAME}' ..."
replace_sphinx_directive() {
	sphinx_conf_filepath="$1"
	name="$2"
	value="$3"
	sed -i 's/\('$name'\s*=\s*\)[^[:space:]]*/\1'$value'/g' $sphinx_conf_filepath
}
replace_sphinx_directive "$SPHINX_CONF_FILEPATH" "sql_host" "$DB_HOST"
replace_sphinx_directive "$SPHINX_CONF_FILEPATH" "sql_user" "$DB_USER"
replace_sphinx_directive "$SPHINX_CONF_FILEPATH" "sql_pass" "$DB_PASS"
replace_sphinx_directive "$SPHINX_CONF_FILEPATH" "sql_db" "$DB_NAME"
sed -i 's/var\/lib\/sphinx/var\/lib\/sphinx_'$USERNAME'/g' "$SPHINX_CONF_FILEPATH"
sed -i 's/:9312/:'$SPHINX_PORT'/g' "$SPHINX_CONF_FILEPATH"
sed -i 's/9306:mysql41/1'$SPHINX_PORT':mysql41/g' "$SPHINX_CONF_FILEPATH"
sed -i 's/searchd\.log/searchd_'$USERNAME'.log/g' "$SPHINX_CONF_FILEPATH"
sed -i 's/query\.log/query_'$USERNAME'.log/g' "$SPHINX_CONF_FILEPATH"
sed -i 's/searchd\.pid/searchd_'$USERNAME'.pid/g' "$SPHINX_CONF_FILEPATH"

#restart searchd daemon
echo "Restarting searchd daemon ..."
sudo /usr/bin/systemctl restart "searchd_${USERNAME}"

echo "Done."
exit 0

