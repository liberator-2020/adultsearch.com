#add db entities for functional tests

#clear
DELETE FROM classifieds_loc WHERE post_id IN (10000001);
DELETE FROM classifieds WHERE id IN (10000001);
DELETE FROM phone_verification WHERE account_id IN (1000001,1000002,1000003,1000004,1000005);
DELETE FROM advertise_budget WHERE account_id IN (1000002);
DELETE FROM account_autologin WHERE account_id IN (1000001,1000002,1000003,1000004,1000005);
DELETE FROM account WHERE account_id IN (1000001,1000002,1000003,1000004,1000005);
DELETE FROM account WHERE email = 'testworker@adultsearch.com';

#normal user, email = 'testuser@adultsearch.com', password = 'testuser@adultsearch.com'
INSERT INTO account
(account_id, created_stamp, username, password, email, phone, phone_verified, approved, account_level)
VALUES
(1000001, 1556632109, 'testuser', '$2y$10$IWk2T9dxckfYWByeRgEo1ORs09TfxHSw6ef4sp6V7IrDrfYZVo7oO', 'testuser@adultsearch.com', '+17024721111', 1, 1, 0);
INSERT INTO phone_verification 
(account_id, phone, verified, sms_code, created_dts, verified_dts, attempts)
VALUES
(1000001, '+17024721111', 1, '1111', 1556632109, 1556632109, 1);
INSERT INTO account_autologin (account_id, secret, time) VALUES (1000001, 'tus1', NOW());

#agency, email = 'testagency@adultsearch.com', password = 'testagency@adultsearch.com'
INSERT INTO account
(account_id, created_stamp, username, password, email, phone, phone_verified, approved, account_level, agency)
VALUES
(1000002, 1556632109, 'testagency', '$2y$10$2Iiyk95Ghfwi0RxPLIv3kOMjOhXxCUYa02TYmSYs4zpkXwzp4kQQu', 'testagency@adultsearch.com', '+17024721112', 1, 1, 0, 1);
INSERT INTO phone_verification 
(account_id, phone, verified, sms_code, created_dts, verified_dts, attempts)
VALUES
(1000002, '+17024721112', 1, '1111', 1556632109, 1556632109, 1);
INSERT INTO account_autologin (account_id, secret, time) VALUES (1000002, 'tag1', NOW());
INSERT INTO advertise_budget (account_id, budget) VALUES (1000002, 1000.00);

#worker, email = 'testworker@adultsearch.com', password = 'testworker@adultsearch.com'
INSERT INTO account
(account_id, created_stamp, username, password, email, phone, phone_verified, approved, account_level, worker)
VALUES
(1000003, 1556632109, 'testworker', '$2y$10$qNxa0uO8g5BUqX1z4eSNzeEVaF2vLwzqxMr.D0ECSHUz36SB3ZNBO', 'testworker@adultsearch.com', '+17024721113', 1, 1, 1, 1);
INSERT INTO phone_verification 
(account_id, phone, verified, sms_code, created_dts, verified_dts, attempts)
VALUES
(1000003, '+17024721113', 1, '1111', 1556632109, 1556632109, 1);
INSERT INTO account_autologin (account_id, secret, time) VALUES (1000003, 'two1', NOW());

#admin, email = 'testadmin@adultsearch.com', password = 'testadmin@adultsearch.com'
INSERT INTO account
(account_id, created_stamp, username, password, email, phone, phone_verified, approved, account_level, worker)
VALUES
(1000004, 1556632109, 'testadmin', '$2y$10$IUTA.gp6kz8x7Ez6o8KRuOeYQmgYi/Q5/tvP2W.eApa4sCwNrQxSi', 'testadmin@adultsearch.com', '+17024721114', 1, 1, 3, 1);
INSERT INTO phone_verification 
(account_id, phone, verified, sms_code, created_dts, verified_dts, attempts)
VALUES
(1000004, '+17024721114', 1, '1111', 1556632109, 1556632109, 1);
INSERT INTO account_autologin (account_id, secret, time) VALUES (1000004, 'tad1', NOW());

#independent escort, email = 'testescort@adultsearch.com', password = 'testescort@adultsearch.com'
INSERT INTO account
(account_id, created_stamp, username, password, email, phone, phone_verified, approved, account_level)
VALUES
(1000005, 1556632109, 'testescort', '$2y$10$EyGKIfxYWjYSr/PA8jfYn.0DlHVeAioM/MGQ.rOY/GAhis91qbNGS', 'testescort@adultsearch.com', '+17024721115', 1, 1, 0);
INSERT INTO phone_verification 
(account_id, phone, verified, sms_code, created_dts, verified_dts, attempts)
VALUES
(1000005, '+17024721115', 1, '1111', 1556632109, 1556632109, 1);
INSERT INTO account_autologin (account_id, secret, time) VALUES (1000005, 'tes1', NOW());
INSERT INTO classifieds
(id, state_id, type, loc_id, loc_name, account_id, done
, expires, expire_stamp, reply, title, `date`, time_next, content
, incall, age, phone, total_loc, firstname, created)
VALUES
(10000001, 9, 1, 3693, 'Miami', 1000005, 1
, '2021-12-31 23:59:59', 1641009599, 2, 'kate', '2019-07-25 08:00:00', 1641009599, '<p>kate kate kate</p>'
, 1, 28, '+17024721115', 1, 'Kate', 1564056488);
INSERT INTO classifieds_loc 
(id, state_id, loc_id, neighbor_id, type, post_id, updated, done)
VALUES
(10000001, 9, 3693, 0, 1, 10000001, '2019-07-25 08:00:00', 1);

#rotate index
INSERT INTO cache_update (index_name, create_stamp) VALUES ('escorts', UNIX_TIMESTAMP(NOW()));
