<?php

chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../html/inc/config.php');
require_once('../html/inc/common.php');

global $db; 

$advertise = new advertise();
$now = time();


//-----------------------------------------------------------------------------------------------------------------------------
//expiring ZT (Zone / Time) type flat deals, if flat deal is active and to_stamp is not null and smaller than current timestamp
//$res = $db->q("SELECT fd.id, fd.account_id, fd.zone_id FROM advertise_flat_deal fd WHERE fd.type = 'ZT' AND fd.status = 1 AND fd.to_stamp IS NOT NULL AND fd.to_stamp < ?", array($now));
$res = $db->q("
	SELECT fd.id, fd.account_id, fd.zone_id, GROUP_CONCAT(adz.zone_id) as zone_ids
	FROM advertise_flat_deal fd 
	LEFT JOIN advertise_deal_zone adz on adz.deal_id = fd.id
	WHERE fd.type = 'ZT' AND fd.status = 1 AND fd.to_stamp IS NOT NULL AND fd.to_stamp < ?
	GROUP BY fd.id",
	[$now]
	);
while ($row = $db->r($res)) {
	$fd_id = $row["id"];
	$account_id = $row["account_id"];
//	$zone_id = null;
	$zone_ids = [];
	if ($row["zone_ids"]) {
		$zone_ids = explode(",", $row["zone_ids"]);
	} else if (intval($row["zone_id"])) {
		$zone_ids[] = intval($row["zone_id"]);
//		$zone_id = intval($row["zone_id"]);
	} else {
		continue;
	}

	file_log("advertise", "cron:advertise: Finishing flat deal #{$fd_id}...");
	reportAdmin("AS: cron:advertise: Flat deal #{$fd_id} finished.", "Check log/advertise.log and all the campaigns regarding this flat deal", array("fd_id" => $fd_id));

	$res2 = $db->q("UPDATE advertise_flat_deal SET finished_stamp = ?, status = 0 WHERE id = ? LIMIT 1", array($now, $fd_id));
	$aff = $db->affected($res2);
	if ($aff != 1) {
		file_log("advertise", "cron:advertise: Error: Initial update of advertise_flat_deal table failed!, fd_id={$fd_id}");
		reportAdmin("AS: cron:advertise: ERROR finishing flat deal", "Initial update of advertise_flat_deal table failed!", array("fd_id" => $fd_id));
		continue;
	}

	if (!$advertise->flatDealStatusUpdate($fd_id)) {
		file_log("advertise", "cron:advertise: Error: advertise::flatDealStatusUpdate() failed!, fd_id={$fd_id}");
		reportAdmin("AS: cron:advertise: ERROR finishing flat deal", "advertise::flatDealStatusUpdate() failed!", array("fd_id" => $fd_id));
		continue;
	}

	if (!$advertise->flatDealCampaigns($fd_id)) {
		file_log("advertise", "cron:advertise: Error: advertise::flatDealCampaigns() failed!, fd_id={$fd_id}");
		reportAdmin("AS: cron:advertise: ERROR finishing flat deal", "advertise::flatDealCampaigns() failed!", array("fd_id" => $fd_id));
		continue;
	}

	//if zone was exclusive for deal owner (which should be always with ZT deal), remove exclusivity ("putting the zone back into the system")
/*
	$res2 = $db->q("SELECT ads.type, ads.exclusive_access FROM advertise_section ads WHERE ads.id = ? LIMIT 1", array($zone_id));
	if ($db->numrows($res2) != 1) {
		file_log("advertise", "cron:advertise: Error: Can't find zone!, fd_id={$fd_id}, zone_id={$zone_id}");
		reportAdmin("AS: cron:advertise: ERROR finishing flat deal", "Can't find zone {$zone_id} in db!", array("fd_id" => $fd_id, "zone_id" => $zone_id));
		continue;
	}
	$row2 = $db->r($res2);
	$zone_type = $row2["type"];
	$exclusive_access = $row2["exclusive_access"];
	if ($zone_type != "A") {
		if ($exclusive_access != $account_id && !empty($exclusive_access)) {
			file_log("advertise", "cron:advertise: Error: Zone #{$zone_id} has no exclusive access for flat deal owner ?? fd_id={$fd_id}, zone_id={$zone_id}, exclusive_access='{$exclusive_access}'");
			reportAdmin("AS: cron:advertise: ERROR finishing flat deal", "Zone #{$zone_id} has no exclusive access for flat deal owner ??", 
				array("fd_id" => $fd_id, "zone_id" => $zone_id, "exclusive_access" => $exclusive_access));
		} else if ($exclusive_access == $account_id) {
			$res3 = $db->q("UPDATE advertise_section SET exclusive_access = NULL WHERE id = ? LIMIT 1", array($zone_id));
			$aff3 = $db->affected($res3);
			if ($aff3 != 1) {
				file_log("advertise", "cron:advertise: Error: Failed to update(remove) exclusive access on zone #{$zone_id}! fd_id={$fd_id}, zone_id={$zone_id}, exclusive_access='{$exclusive_access}'");
				reportAdmin("AS: cron:advertise: ERROR finishing flat deal", "Failed to update(remove) exclusive access on zone #{$zone_id}!", 
					array("fd_id" => $fd_id, "zone_id" => $zone_id, "exclusive_access" => $exclusive_access));
			}
		} else {
			//there was no exclusive access set up on this zone, even though it should be - mark this down and notify admin, but do not freak out from it
			file_log("advertise", "cron:advertise: Warning: There was no exclusive access set up on zone #{$zone_id}, even though it should have been. fd_id={$fd_id}, zone_id={$zone_id}, exclusive_access='{$exclusive_access}'");
			reportAdmin("AS: cron:advertise: Warning finishing flat deal", "There was no exclusive access set up on zone #{$zone_id}, even though it should have been.", 
					array("fd_id" => $fd_id, "zone_id" => $zone_id, "exclusive_access" => $exclusive_access));
		}
	}
*/
	$res2 = $db->q("SELECT ads.id as zone_id, ads.type, ads.exclusive_access FROM advertise_section ads WHERE ads.id IN ?", [$zone_ids]);
	if (!$db->numrows($res2)) {
		file_log("advertise", "cron:advertise: Error: Can't find zone!, fd_id={$fd_id}, zone_ids=".implode(",", $zone_ids));
		reportAdmin("AS: cron:advertise: ERROR finishing flat deal", "Can't find zone ids ".implode(",", $zone_ids)." in db!", array("fd_id" => $fd_id, "zone_ids" => implode(",", $zone_ids)));
		continue;
	}
	while ($row2 = $db->r($res2)) {
		$zone_id = $row2["zone_id"];
		$zone_type = $row2["type"];
		$exclusive_access = $row2["exclusive_access"];
		if ($zone_type != "A") {
			if ($exclusive_access != $account_id && !empty($exclusive_access)) {
				file_log("advertise", "cron:advertise: Error: Zone #{$zone_id} has no exclusive access for flat deal owner ?? fd_id={$fd_id}, zone_id={$zone_id}, exclusive_access='{$exclusive_access}'");
				reportAdmin("AS: cron:advertise: ERROR finishing flat deal", "Zone #{$zone_id} has no exclusive access for flat deal owner ??", 
					array("fd_id" => $fd_id, "zone_id" => $zone_id, "exclusive_access" => $exclusive_access));
			} else if ($exclusive_access == $account_id) {
				$res3 = $db->q("UPDATE advertise_section SET exclusive_access = NULL WHERE id = ? LIMIT 1", array($zone_id));
				$aff3 = $db->affected($res3);
				if ($aff3 != 1) {
					file_log("advertise", "cron:advertise: Error: Failed to update(remove) exclusive access on zone #{$zone_id}! fd_id={$fd_id}, zone_id={$zone_id}, exclusive_access='{$exclusive_access}'");
					reportAdmin("AS: cron:advertise: ERROR finishing flat deal", "Failed to update(remove) exclusive access on zone #{$zone_id}!", 
						array("fd_id" => $fd_id, "zone_id" => $zone_id, "exclusive_access" => $exclusive_access));
				}
			} else {
				//there was no exclusive access set up on this zone, even though it should be - mark this down and notify admin, but do not freak out from it
				file_log("advertise", "cron:advertise: Warning: There was no exclusive access set up on zone #{$zone_id}, even though it should have been. fd_id={$fd_id}, zone_id={$zone_id}, exclusive_access='{$exclusive_access}'");
				reportAdmin("AS: cron:advertise: Warning finishing flat deal", "There was no exclusive access set up on zone #{$zone_id}, even though it should have been.", 
						array("fd_id" => $fd_id, "zone_id" => $zone_id, "exclusive_access" => $exclusive_access));
			}
		}
	}

	rotate_index("advertise");

	file_log("advertise", "cron:advertise: Flat deal #{$fd_id} finished successfully.");
}


//-----------------------------------------------------------------------------------------------------------------------------
//starting flat deals that are not active, has not been started yet and current timestamp is bigger than from_stamp of the deal
$res = $db->q("SELECT fd.id, fd.type, fd.account_id, fd.zone_id, GROUP_CONCAT(adz.zone_id) as zone_ids
				FROM advertise_flat_deal fd 
				LEFT JOIN advertise_deal_zone adz on adz.deal_id = fd.id
				WHERE fd.status = 0 AND fd.started_stamp IS NULL AND fd.from_stamp IS NOT NULL AND fd.from_stamp < ?
				GROUP BY fd.id", 
				array($now));
while ($row = $db->r($res)) {
	$fd_id = $row["id"];
	$type = $row["type"];
	$account_id = $row["account_id"];
//	$zone_id = $row["zone_id"];
	$zone_ids = [];
    if ($row["zone_ids"]) {
        $zone_ids = explode(",", $row["zone_ids"]);
    } else if (intval($row["zone_id"])) {
        $zone_ids[] = intval($row["zone_id"]);
//      $zone_id = intval($row["zone_id"]);
    } else {
        continue;
    }

	file_log("advertise", "cron:advertise: Starting flat deal #{$fd_id}...");
	reportAdmin("AS: cron:advertise: Flat deal #{$fd_id} started.", 
		"Check log/advertise.log and all the campaigns regarding this flat deal and also exclusive access on zone if this is ZT flat deal", 
		array("fd_id" => $fd_id, "type" => $type, "account_id" => $account_id)
		);

	$res2 = $db->q("UPDATE advertise_flat_deal SET started_stamp = ?, status = 1 WHERE id = ? LIMIT 1", array($now, $fd_id));
	$aff = $db->affected($res2);
	if ($aff != 1) {
		file_log("advertise", "cron:advertise: Error: Initial update of advertise_flat_deal table failed!, fd_id={$fd_id}");
		reportAdmin("AS: cron:advertise: ERROR starting flat deal", "Initial update of advertise_flat_deal table failed!", array("fd_id" => $fd_id));
		continue;
	}

	if (!$advertise->flatDealStatusUpdate($fd_id)) {
		file_log("advertise", "cron:advertise: Error: advertise::flatDealStatusUpdate() failed!, fd_id={$fd_id}");
		reportAdmin("AS: cron:advertise: ERROR starting flat deal", "advertise::flatDealStatusUpdate() failed!", array("fd_id" => $fd_id));
		continue;
	}

	if (!$advertise->flatDealCampaigns($fd_id)) {
		file_log("advertise", "cron:advertise: Error: advertise::flatDealCampaigns() failed!, fd_id={$fd_id}");
		reportAdmin("AS: cron:advertise: ERROR starting flat deal", "advertise::flatDealCampaigns() failed!", array("fd_id" => $fd_id));
		continue;
	}

	//if this is ZT (Zone / Time) flat deal type, et up exclusive access on flat deal zone for owner of the flat deal
	if ($type == "ZT") {
		file_log("advertise", "cron:advertise: Setting exclusive access on zone ids # ".implode(",", $zone_ids)." for account_id #{$account_id}.");
		$res2 = $db->q("UPDATE advertise_section SET exclusive_access = ? WHERE id IN ?", [$account_id, $zone_ids]);
	}	

	rotate_index("advertise");

	file_log("advertise", "cron:advertise: Flat deal #{$fd_id} started successfully.");
}



//END
?>
ok
