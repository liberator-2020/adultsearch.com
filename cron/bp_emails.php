<?php
/**
 * This cron job periodically reads mailbox backpage@adultsearch.com
 * and stores incoming email addresses into bp_emails db table and forward emails from backpage to shad
 */

chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../html/inc/config.php');
require_once('../html/inc/common.php');
require_once('../html/inc/classes/class.backpage.php');

backpage::processMailbox();

?>
ok
