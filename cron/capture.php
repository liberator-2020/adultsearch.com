<?php

chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

include "../html/inc/config.php";
include "../html/inc/common.php";

global $db;
$system = new system;

$safeh = array('ashleymadison.com', 'amateurmatch.com', 'sexfinder.com', 'xxxmatch.com', 'arrangementfinders.com', 'sexsearched.com');

$hour = date("H");
if( $hour > 5 && $hour < 23 ) {
	$res = $db->q("SELECT s.id, c.ad_url
					FROM `advertise_camp_section` s
					INNER JOIN advertise_campaign c on s.c_id = c.id 
					WHERE s.section = 'popup' and s.`show` = 1 and s.approved = 0 limit 1");
	if( $db->numrows($res) ) {
		$row = $db->r($res); 

		$hx = strncmp($row["ad_url"], "http", 4) ? ("http://".$row["ad_url"]) : $row["ad_url"];
		$host = parse_url($hx, PHP_URL_HOST);
		$add = false; reset($safeh);
		foreach($safeh as $h) {
			if( stristr($host, $h) ) {
				$add = true; break;
			}
		}
		if( $add ) {
			$db->q("update advertise_camp_section set approved = 1 where id = '{$row[0]}'");
			reportAdmin("Popup auto approved", $host);
		} else {
			reportAdmin("Popup Approve Needed", $host);
		}
	}
}

//notify publishers if someone wants to advertise at their section
$res = $db->q("select a.email, n.account_id from advertise_notification n inner join account a using (account_id) group by account_id");
while($row=$db->r($res)) {
	$m = "Hello,<br><br>Thank you for being one of our publishers.<br><br>There is at least one campaign that wants to advertise on your network, please go to the your publisher account to accept or decline their requests. <br><br>You may reach your publisher account at <a href='http://adultsearch.com/publisher/'>http://adultsearch.com/publisher/</a>.<br><br>Sincerely,<br><br>adultsearch.com";
	sendEmail("support@adultsearch.com", "AdultSearch Publisher Notification", $m, $row["email"]);
	$db->q("delete from advertise_notification where account_id = '{$row["account_id"]}'");
}


//$db->q("delete from advertise_current where time < date_sub(now(), interval 240 minute)");
$db->q("delete from advertise_current where stamp < ?", array(time() - (4*3600)));

$db->q("delete from advertise_publisher_request where time < date_sub(now(), interval 2 day)");

echo "ads_current cleared\n";

return;

?>
