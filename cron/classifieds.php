<?php
chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../html/inc/config.php');
require_once('../html/inc/common.php');

function mlog($msg) {
	$h = fopen("/home/as/prod/current/cron/classifieds.log", "a");
	if (!$h)
		return;
	fputs($h, date("[Y-m-d H:i:s]")." CLlog: ".$msg."\n");
	fclose($h);
}

global $db, $account, $smarty; 
$sms = new swiftsms();
$now = time();
//echo "now={$now}\n";

// select unpublished (done -1) and requested to be removed (done -2) classifieds older than 24 hours
echo "###second section started...\n";
$res = $db->q("SELECT id, email, bp, done FROM `classifieds` WHERE done < 0 and date < date_sub(now(), interval 24 hour) and deleted IS NULL LIMIT 1000");
while($row = $db->r($res)) {
		$classified_id = $row["id"];

		$clad = clad::findOneById($classified_id);
    if ($clad->isExistsUpgrades()) {
    	  echo "Skip, classified #{$classified_id} has upgrades\n";
		    continue;
    }

    classifieds::remove($classified_id, "S-cron/classifieds");
		echo "removed: {$classified_id}\n";
}
echo "###second section ended\n";


//notify support about classifieds in waiting state
/*
echo "###third section started...\n";
$res = $db->q("SELECT id FROM `classifieds` WHERE done = 3 AND date < date_sub(now(), interval 60 minute)");
while($row = $db->r($res)) {
	$id = $row["id"];
	
	//send notification email to ross
	$s = "New classified ad #{$id} needs verification";
	$m = "There is new classified ad #{$id}, which member failed to do phone SMS verification. Please check this ad, its probably spam.<br />";
	$m .= "<a href=\"https://adultsearch.com/mng/classifieds?cid={$id}\">#{$id}</a><br />";
	$m .= "<br /><small>Sent from cron</small><br />";
	sendEmail("web@adultsearch.com", $s, $m, SUPPORT_EMAIL);  
}
echo "###third section ended\n";
*/


// -------------
// REPOSTING ADS
// -------------
//select published and not deleted ads, that are set to auto repost and time_next has been crossed
$res = $db->q("SELECT c.id, c.email, c.phone, c.account_id, c.firstname, c.loc_name
					, a.repost
				FROM classifieds c
				INNER JOIN account a on a.account_id = c.account_id
				WHERE c.done = 1 and (c.time_next < unix_timestamp(now())) and c.auto_repost > 0 AND c.deleted IS NULL");
$i = 0;
while($row=$db->r($res)) {
	$id = $row["id"];
	echo "Reposting ad #{$id}\n";
	classifieds::repost($id);
	$i++;

	//if we didnt run out of reposts, continue with next ad
	if ($row["repost"] > 1)
		continue;

	//client just ran out of top-up credits, notify him
	$phone = makeOnlyDigitPhoneNumber($row["phone"]);
	if (!empty($row["email"])) {
		$autologin = $account->createautologin($row['account_id'], 'https://adultsearch.com/classifieds/myposts');
		$m = "Hello {$row['firstname']},<br><br>
There are new clients looking for you in {$row['loc_name']}, but did you know that most guys would only call an entertainer from today's postings? Post your ad every day so you don't miss out on new clients!<br><br>
To post your ad at the top of today's listings just go to adultsearch.com and login with your email and password.<br><br>
For your convenience, here is 1-click to login link: <a href='{$autologin}'>{$autologin}</a><br><br>
If you have any questions or need special help please feel free to contact me anytime.  If I am not immediately available I will call you back as soon as possible.<br><br>
Have a great day!<br>support@adultsearch.com<br>";
		sendEmail(SUPPORT_EMAIL, "Your ad on AdultSearch hasnt been posted today!", $m, $row["email"]);
	} elseif( strlen($phone) == 10 ) {
		$text = "Your ad on adultsearch.com hasnt been posted today. Repost it now so you dont miss out on new business!";
		$ret = $sms->send($phone, $text);
	}
}
echo "Reposted {$i} ads.\n";

// -----------------
// EXPIRATION OF ADS
// -----------------
//select domestic, published ads that are NOT set to auto renew and that have been expired
//That means international ads are not expiring (requested by shad)
//eccie promo ads are also never expiring - 13.3.2015 miro
//miro 17.10.2015 - dont select deleted ads
$res = $db->q("SELECT c.id, c.firstname, c.bp, c.account_id, c.email, c.phone 
				FROM classifieds c
				INNER JOIN classifieds_loc cl on cl.post_id = c.id
				INNER JOIN location_location l on cl.loc_id = l.loc_id
				WHERE c.done > 0 
					AND c.expires < now() 
					AND (c.auto_repost IS NULL OR c.auto_repost = 0) 
					AND l.country_id IN (16046, 16047, 41973)
					AND c.deleted IS NULL");
//					AND c.eccie_promo <> 1
$emailx = $smsx = 0;
$i = 0;
while($row=$db->r($res)) {
	$id = $row["id"];

	if ($row["bp"] == 0) {

		//check if ad has not been referred by backpage last 24 hours, in this case prolong ad for 7 days and continue in cycle
		$res2 = $db->q("SELECT refer_id FROM classifieds_refer WHERE id = ? AND stamp > ? AND referer like '%backpage%' LIMIT 1",
			[$id, time()-86400]
			);
		if ($db->numrows($res2)) {
			if (classifieds::prolong($id, 7))
				audit::log("CLA", "Prolong", $id, "BP ref - 7 days", "S-cron/classifieds");
			continue;
		}
	
		if( !empty($row["email"]) ) {
			$autologin = $account->createautologin($row['account_id'], 'https://adultsearch.com/classifieds/myposts');
			$autologin .= "&mirsec=".$row["id"];	//miro secretly add classified id to find out whats going on
			$m = "Hello {$row['firstname']},<br><br>There are new clients looking for you on AdultSearch, but unfortunately your ad with us has expired.  Get back online now and don't miss out on any more phone calls from eager new clients. <br><br>To get your ad back up just go to adultsearch.com and login with your email and password. It's easy!<br><br>For your convenience, 1-click login link: <a href='{$autologin}'>{$autologin}</a><br><br>If you have any questions or need special help please feel free to contact me anytime.  If I am not immediately available I will call you back as soon as possible.<br><br>Have a great day!<br>support@adultsearch.com";
			sendEmail(SUPPORT_EMAIL, "A client is looking for you now - get back online!", $m, $row["email"]);
			$emailx++;
		}
		if( !empty($row["phone"]) ) {
			$phone = makeOnlyDigitPhoneNumber($row["phone"]);
			if( strlen($phone) == 10 ) {
				$text = "Your ad on adultsearch.com has expired. Post today so you don't miss out on new business!";
				$ret = $sms->send($phone, $text);
				$smsx++;
			}
		}
		audit::log("CLA", "Expire", $id, "", "S-cron/classifieds");
	}

	$db->q("update classifieds set done = 0 where id = '$id'");
	$db->q("update classifieds_loc set done = 0 where post_id = '$id'");
	$i++;
}
echo "Expired {$i} ads.\n";


//-------------------------------------------------------
//mark city thumbnail ads that just expired as expired (done=0)
$i = 0; $j = 0;
$res = $db->q("UPDATE classifieds_sponsor SET done = 0, notified = 0 WHERE done > 0 and expire_stamp < ?", [$now]);
$i = $db->affected($res);
$week_ago_stamp = $now - 7*86400;
$res = $db->q("SELECT * FROM classifieds_sponsor WHERE done < 1 and expire_stamp < ?", [$week_ago_stamp]);
while($row = $db->r($res)) {
	$id = $row['id'];
	if (empty($row['images']))
		classifieds::sponsorpicremove($row['post_id']);
	$db->q("DELETE FROM classifieds_sponsor WHERE id = ?", [$id]);
	$j++;
}
echo "Expired {$i} city thumbnails.\n";
echo "Finally deleted {$j} city thumbnail entries.\n";

//notify user that have expired featured ad and were not notified yet
$i = 0;
$res = $db->q("SELECT s.id, s.post_id, a.account_id, a.email 
				FROM `classifieds_sponsor` s 
				LEFT JOIN classifieds c on s.post_id = c.id 
				LEFT JOIN account a using (account_id) 
				WHERE s.done = 0 and s.notified = 0");
$ids = NULL;
while($row=$db->r($res)) { 
	if( !empty($row['account_id']) && !empty($row['email']) ) {
		$autologin_link = $account->createautologin($row["account_id"], "https://adultsearch.com/classifieds/sponsor?id={$row["post_id"]}");
		$smarty->assign("link", $autologin_link);
		$m = $smarty->fetch(_CMS_ABS_PATH.'/templates/email/home_thumbnail.tpl');
		sendEmail(SUPPORT_EMAIL, "AdultSearch.com - Homepage Thumbnail Expired", $m, $row['email']);
	}
	$idx[] = $row['id'];
	$i++;
}
if( is_array($idx) ) {
	$db->q("update classifieds_sponsor set notified = 1 where id in (".implode(',', $idx).")");
}
echo "Notified {$i} users about their expired city thumbnails.\n";


//-----------------------------------------------------------
//mark side sponsor ads that just expired as expired (done=0)
$i = 0; $j = 0;
$res = $db->q("UPDATE classifieds_side SET done = 0, notified = 0 WHERE done > 0 and expire_stamp < ?", [$now]);
$i = $db->affected($res);
echo "Expired {$i} side sponsors.\n";

$week_ago_stamp = $now - 7*86400;
$res = $db->q("DELETE FROM classifieds_side WHERE done < 1 and expire_stamp < ?", [$week_ago_stamp]);
$j = $db->affected($res);
echo "Finally deleted {$j} side sponsor entries.\n";


//mark sticky sponsor ads that just expired as expired (done=0)
$i = 0; $j = 0;
$res = $db->q("UPDATE classified_sticky SET done = 0, notified = 0 WHERE done > 0 and expire_stamp < ?", [$now]);
$i = $db->affected($res);
echo "Expired {$i} sticky sponsors.\n";

$res = $db->q("DELETE FROM classified_sticky WHERE done < 1 and expire_stamp < ?", [$week_ago_stamp]);
$j = $db->affected($res);
echo "Finally deleted {$j} sticky sponsor entries.\n";


//----------------------------------------------------------------
// rotate escorts index to propagate all changes into sphinx index
classifieds::rotateIndex();


if( $smsx || $emailx ) {
	//$idx = implode(",", $ids);
	//$m = "total sms sent: $smsx. total email sent: $emailx<br>$idx";
	//sendEmail(SUPPORT_EMAIL, "crontab log", $m, ADMIN_EMAIL);
}

?>

ok
