<?php
/**
 * This script runs at 3:16am from cron and it is (soft)deleting "old" expired ads (detailed conditions see below)
 */
chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../html/inc/config.php');
require_once('../html/inc/common.php');
require_once('../html/inc/classes/class.classifieds.php');

global $db; $classifieds = new classifieds;

$notify_limit = 3000;
$limit = 50000;

// select expired and not deleted ads, that have been posted more than 6 months ago, and that have not been referred in last 30 days
$res = $db->q("SELECT c.id
	FROM `classifieds` c 
	LEFT JOIN classifieds_refer r using (id) 
	WHERE c.done = 0 and c.deleted IS NULL and c.date < date_sub(now(), interval 6 month) 
	GROUP BY r.id 
	HAVING r.id is null or max(r.stamp) < ?
	ORDER BY c.id",
	[time() - 86400*30]
	);
$total = $db->numrows($res); 
if ($total > $notify_limit) {
	reportAdmin("AS: cron-cl-clear deleting more than {$notify_limit} ads", "Somethings wrong - in 1 day more than {$notify_limit} is supposed to be deleted ?", array("total" => $total));
	//return;
}

if ($total < $limit)
	$limit = $total;
$c = 1;
while($row = $db->r($res)) {
	echo "Removing {$row["id"]} - $c of $limit ...\n";
	$classifieds->remove($row["id"], "S-cron/clear");
	$db->q("insert into classifieds_removed (id, time, account_id, data) values ('{$row["id"]}', now(), '-1', 'expired ad no hit over 6 months')");
	$c++;
}

?>
