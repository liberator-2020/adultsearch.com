<?php

chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../html/inc/config.php');
require_once('../html/inc/common.php');
require_once('../html/inc/classes/class.curl.php');

global $db, $account;
$curl = new curl("_currency.cookie");

ob_end_flush();

$download = 1;

if( $download ) { 
	$buffer = $curl->get("http://www.xe.com/dfs/datafeed2.cgi?empmedia");
	$db->q("insert into currency_data (date, data) values (curdate(), ?)", [$buffer]);
} else {
	$res = $db->q("select * from currency_data order by date limit 1");
	if( $db->numrows($res) ) {
		$row = $db->r($res); 
		$buffer = $row['data'];
	}
}

if( empty($buffer) ) {
	reportAdmin('currency buffer is empty.');
	die('buffer is empty..\n');
}

$c = NULL;
preg_match_all('/"([^"]*)","([^"]*)","([^"]*)","([^"]*)"/', $buffer, $c);

print_r($c);
if( is_array($c) && isset($c[4]) ) {
	for ($i = 0; $i < count($c[1]); $i++) {
		$db->q("insert into currency (`code`, `name`, `to`, `from`) values (?, ?, ?, ?) on duplicate key update `to` = ?, `from` = ?",
			[$c[1][$i], $c[2][$i], $c[3][$i], $c[4][$i], $c[3][$i], $c[4][$i]]
			);
	}
}

?>
