<?php

chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../html/inc/config.php');
require_once('../html/inc/common.php');
require_once('../html/inc/classes/class.curl.php');

define('CURRENCY_URL', 'https://usd.fxexchangerate.com/rss.xml');
define('LAST_FILE', './currency_update_last.xml');


function download_rates() {
	//$curl = new curl("_currency_update.cookie");
	$curl = new curl();

	$feed = $curl->get(CURRENCY_URL);
	if (empty($feed)) {
		//error, could not download feed, notify admin
		reportAdmin("AS: cron/currency_update: Error - can't download feed", "", array());
		return false;
	}

	echo "Feed downloaded successfully - ".strlen($feed)." bytes.\n";

	//store downloaded RSS XML to file (for debug purposes)
	$h = fopen(LAST_FILE, "w");
	if ($h) {
		fwrite($h, $feed);
		fclose($h);
		echo "Saved into file '".LAST_FILE."'.\n";
	}

	return $feed;
}

function update_rate($curr_code, $curr_name, $rate) {
	global $db;

	$from = floatval(1 / floatval($rate));

	$res = $db->q(
		"INSERT INTO currency (`code`, `name`, `to`, `from`, `last_updated`) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `to` = ?, `from` = ?, `last_updated` = ?",
		array(
			$curr_code, 
			$curr_name, 
			number_format($rate, 4, ".", ""), 
			number_format($from, 4, ".", ""),
			time(),
			number_format($rate, 4, ".", ""), 
			number_format($from, 4, ".", ""),
			time()
		)
	);
	return true;
}



//loading from file - use only for debug purposes...
/*
$feed = file_get_contents(LAST_FILE);
if (!$feed) {
	echo "Error: Can't read feed from file '".LAST_FILE."' !\n";
	exit(1);
}
$xml = simplexml_load_file(LAST_FILE);
*/

$feed = download_rates();
if ($feed === false) {
	exit(1);
}

$xml = simplexml_load_string($feed);

if ($xml === false) {
	echo "Error: Can't load simplexml element!\n";
	reportAdmin("AS: cron/currency_update: Error - can't load simplexml elem", "", array());
	exit(1);
}

foreach ($xml->channel->item as $item) {
	$title = $item->title;
	//$ret = preg_match('/([A-Z]{3})\/USD/', $title, $matches);
	$ret = preg_match('/.*\(([A-Z]{3})\)$/', $title, $matches);
	if ($ret !== 1)
		continue;
	$curr_code = $matches[1];
	if (!$curr_code)
		continue;

	$description = $item->description;
	$ret = preg_match('/1 United States Dollar = ([0-9,\.]*) ([0-9_a-zA-Z \-]*)$/', $description, $matches);
	if ($ret !== 1)
		continue;
	$rate = preg_replace('/,/', '', $matches[1]);
	$curr_name = $matches[2];
	if (!$rate)
		continue;
	echo "Updating {$curr_code} ({$curr_name}): {$rate} ......";
	$ret = update_rate($curr_code, $curr_name, $rate);
	if ($ret)
		echo "OK\n";
	else
		echo " - ERROR updating !\n";
}

echo "Done.\n";
exit(0);

?>
