<?php
chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../html/inc/config.php');
require_once('../html/inc/common.php');

global $db;

// --------------------------------------------------------------------------------------------
// 1. check if there is no recurring subscription active for deleted or marked to be removed ad
$res = $db->q("
	SELECT DISTINCT p.id
	FROM payment p
	INNER JOIN payment_item pi on pi.payment_id = p.id
	INNER JOIN classifieds c on c.id = pi.classified_id
	WHERE (c.deleted IS NOT NULL OR c.done = -2) AND p.subscription_status = 1
	");
$payment_ids = [];
while ($row = $db->r($res)) {
	$payment_id = $row["id"];
	$payment_ids[] = $payment_id;
}
if (count($payment_ids)) {
	//this should not happen ,notify admin
	reportAdmin("AS: Data integrity check failed", "These ".count($payment_ids)." payments are with active recurring subscription, but they are for deleted or marked as removed ads:<br />\n".implode(",", $payment_ids)."\n<br />Fix it!");
}

// --------------------------------------------------------------------------------------------
// 2. check if there are no escort agencies with the same password or ip (duplicate accounts)
// disabled on 2018-11-06
/*
$res = $db->q("
	SELECT a.password, count(*) as cnt
	FROM account a
	WHERE a.deleted IS NULL AND a.banned = 0 AND a.agency = 1
	GROUP BY a.password
	HAVING count(*) > 1
	ORDER BY 2 desc
	");
$passwords = [];
while ($row = $db->r($res)) {
	$password = $row["password"];
	$count = $row["cnt"];
	$passwords[$password] = $count;
}
if (count($passwords)) {
	//this should not happen ,notify admin
	$html = "";
	foreach ($passwords as $password => $count) {
		$link = "https://adultsearch.com/mng/accounts?password=".urlencode($password)."&order=created_stamp&orderway=ASC";
		$html .= "Password '{$password}' : {$count} accounts : <a href=\"{$link}\">view these</a><br />\n";
	}
	reportAdmin("AS: Data integrity check failed", "There are multiple escort agencies with the same password, possible duplicate accounts:<br />{$html}");
}
$res = $db->q("
	SELECT a.ip_address, count(*) as cnt
	FROM account a
	WHERE a.deleted IS NULL AND a.banned = 0 AND a.agency = 1
	GROUP BY a.ip_address
	HAVING count(*) > 1
	ORDER BY 2 desc
	");
$ips = [];
while ($row = $db->r($res)) {
	$ip = $row["ip_address"];
	$count = $row["cnt"];
	$ips[$ip] = $count;
}
if (count($ips)) {
	//this should not happen ,notify admin
	$html = "";
	foreach ($ips as $ip => $count) {
		$link = "https://adultsearch.com/mng/accounts?ip=".urlencode($ip)."&order=created_stamp&orderway=ASC";
		$html .= "IP address '{$ip}' : {$count} accounts : <a href=\"{$link}\">view these</a><br />\n";
	}
	reportAdmin("AS: Data integrity check failed", "There are multiple escort agencies with the same ip, possible duplicate accounts:<br />{$html}");
}
*/

// --------------------------------------------------------------------------------------------
// 3. check if there is no drop in number of incoming visitors from eccie
//not used since eccie redirect to AS directly since dec.22.2018
/*
$eccie_minimum = 35000;
$eccie_visits = intval($db->single("SELECT count(*) FROM analytics WHERE ref like 'eccie%' AND day = SUBDATE(CURRENT_DATE, 1)"));
if ($eccie_visits < $eccie_minimum) {
	//this should not happen ,notify admin
	reportAdmin("AS: Drop in visitors from eccie", "Check if there is no drop in number of incoming visitors from eccie:<br />Yesterday visits: {$eccie_visits}, Minimum: {$eccie_minimum}<br />SELECT count(*) FROM analytics WHERE ref like 'eccie%' AND day = SUBDATE(CURRENT_DATE, 1);<br />SELECT day, count(*) FROM analytics WHERE ref like 'eccie%' group by day order by day desc");
}
*/


// --------------------------------------------------------------------------------------------
// 4. check if there is no duplicite location for one ad
$res = $db->q("
select cl.post_id, cl.loc_id, count(*) as cnt
from classifieds_loc cl
inner join classifieds c on cl.post_id = c.id
where c.deleted IS NULL
group by cl.post_id, cl.loc_id
having count(*) > 1
");
if ($db->numrows($res) > 0) {
	$html = "";
	while ($row = $db->r($res)) {
		$clad_id = $row["post_id"];
		$loc_id = $row["loc_id"];
		$cnt = $row["cnt"];
		$link = "https://adultsearch.com/mng/classifieds?cid={$clad_id}";
		$html .= "Cl.ad #{$clad_id} has multiple location placement in location #{$loc_id}: {$cnt} entries: <a href=\"{$link}\">view these</a><br />\n";
	}
	reportAdmin("AS: Data integrity check failed", "There are multiple clad placements:<br />{$html}");
}

// --------------------------------------------------------------------------------------------
// 5. check if there is no integrity error between classifieds.done and classifieds_loc.done
$res = $db->q("
SELECT c.id, c.done, cl.loc_id, cl.done as cl_done
FROM classifieds c
INNER JOIN  classifieds_loc cl on cl.post_id = c.id
WHERE c.done <> cl.done AND cl.done <> -1
");
if ($db->numrows($res) > 0) {
	$html = "";
	while ($row = $db->r($res)) {
		$c_done = $row["done"];
		$id = $row["id"];
		$loc_id = $row["loc_id"];
		$cl_done = $row["cl_done"];
		if ($c_done == -1) {
			//c.done = -1, but cl.done <> -1 ? not sure how this happens, but id does sometimes, for now update cl.done
			$db->q("UPDATE classifieds_loc SET done = ? WHERE post_id = ?", [$c_done, $id]);
			continue;
		}
		$html .= "{$id},{$c_done},{$loc_id},{$cl_done}<br />\n";
	}
	if ($html) {
		$html = "c.id, c.done, cl.loc_id, cl.done<br />\n{$html}";
		reportAdmin("AS: Data integrity check failed", "There are some ad placements with different done than classified:<br />{$html}");
	}
}

// --------------------------------------------------------------------------------------------
// 6. check duplicities in classifieds_sponsor table
$res = $db->q("
select post_id, loc_id, count(*) as cnt 
from classifieds_sponsor 
group by post_id, loc_id 
having count(*) > 1
");
if ($db->numrows($res) > 0) {
	$html = "";
	while ($row = $db->r($res)) {
		$clad_id = $row["post_id"];
		$loc_id = $row["loc_id"];
		$cnt = $row["cnt"];
		$res2 = $db->q("SELECT * FROM classifieds_sponsor WHERE post_id = ? AND loc_id = ?", [$clad_id, $loc_id]);
		$i = 0;
		while ($row2 = $db->r($res2)) {
			$id = $row2["id"];
			$done = $row2["done"];
			if ($done == -1) {
				$db->q("DELETE FROM classifieds_sponsor WHERE id = ? LIMIT 1", [$id]);
				continue;
			}
			$i++;
		}
		if ($i > 1)
			$html .= "{$clad_id},{$loc_id}, SELECT * FROM classifieds_sponsor WHERE post_id = {$clad_id} AND loc_id = {$loc_id};<br />\n";
	}
	if ($html) {
		$html = "clad_id, loc_id, SELECT<br />\n{$html}";
		reportAdmin("AS: Data integrity check failed", "There are some duplicities in classifieds_sponsor table:<br />{$html}");
	}
}


// --------------------------------------------------------------------------------------------
// 7. check inconsistency in classified_sticky table
$res = $db->q("
select cs1.classified_id, cs1.id as id_1, cs1.done as done_1, cs1.loc_id as loc_id_1, cs2.id as id_2, cs2.done as done_2, cs2.loc_id as loc_id_2
from classified_sticky cs1
inner join classified_sticky cs2 on cs1.classified_id = cs2.classified_id and cs1.done <> cs2.done
");
if ($db->numrows($res) > 0) {
	$html = "";
	while ($row = $db->r($res)) {
		$clad_id = $row["classified_id"];
		$id_1 = $row["id_1"];
		$id_2 = $row["id_2"];
		$loc_id_1 = $row["loc_id_1"];
		$loc_id_2 = $row["loc_id_2"];
		$done_1 = $row["done_1"];
		$done_2 = $row["done_2"];
		$html .= "{$clad_id},{$loc_id_1},{$done_1},{$loc_id_2},{$done_2}";
		if ($done_1 < 0 && $done_2 > 0)
			$html .= ", DELETE FROM classified_sticky WHERE id = '{$id_1}' LIMIT 1;";
		if ($done_2 < 0 && $done_1 > 0)
			$html .= ", DELETE FROM classified_sticky WHERE id = '{$id_2}' LIMIT 1;";
		$html .= "<br />\n";
	}
	if ($html) {
		$html = "classified_id, loc_id_1, done_1, loc_id_2, done_2, DELETE<br />\n{$html}";
		reportAdmin("AS: Data integrity check failed", "There is some inconsistency in classified_sticky table:<br />{$html}");
	}
}

//----------------------------------------------------------------------------------------
// 8. check inconsistency in classifieds_sponsor, classified_side, classified_sticky table
$res = $db->q("
select c.id, c.done as done_1, cs.done as done_2, cs.loc_id as loc_id
from classifieds_sponsor cs
inner join classifieds c on c.id = cs.post_id
where cs.done > 0 AND cs.done <> c.done
");
if ($db->numrows($res) > 0) {
	$html = "";
	while ($row = $db->r($res)) {
		$clad_id = $row["id"];
		$done_1 = $row["done_1"];
		$done_2 = $row["done_2"];
		$loc_id = $row["loc_id"];
		$html .= "{$clad_id},{$done_1},{$done_2},{$loc_id}";
		$html .= "<br />\n";
	}
	if ($html) {
		$html = "classified_id, c_done, cs_done, loc_id<br />\n{$html}";
		reportAdmin("AS: Data integrity check failed", "There is some inconsistency in classifieds_sponsor table:<br />{$html}");
	}
}
$res = $db->q("
select c.id, c.done as done_1, cs.done as done_2, cs.loc_id as loc_id
from classifieds_side cs
inner join classifieds c on c.id = cs.post_id
where cs.done > 0 AND cs.done <> c.done
");
if ($db->numrows($res) > 0) {
	$html = "";
	while ($row = $db->r($res)) {
		$clad_id = $row["id"];
		$done_1 = $row["done_1"];
		$done_2 = $row["done_2"];
		$loc_id = $row["loc_id"];
		$html .= "{$clad_id},{$done_1},{$done_2},{$loc_id}";
		$html .= "<br />\n";
	}
	if ($html) {
		$html = "classified_id, c_done, cs_done, loc_id<br />\n{$html}";
		reportAdmin("AS: Data integrity check failed", "There is some inconsistency in classifieds_side table:<br />{$html}");
	}
}
$res = $db->q("
select c.id, c.done as done_1, cs.done as done_2, cs.loc_id as loc_id
from classified_sticky cs
inner join classifieds c on c.id = cs.classified_id
where cs.done > 0 AND cs.done <> c.done
");
if ($db->numrows($res) > 0) {
	$html = "";
	while ($row = $db->r($res)) {
		$clad_id = $row["id"];
		$done_1 = $row["done_1"];
		$done_2 = $row["done_2"];
		$loc_id = $row["loc_id"];
		$html .= "{$clad_id},{$done_1},{$done_2},{$loc_id}";
		$html .= "<br />\n";
	}
	if ($html) {
		$html = "classified_id, c_done, cs_done, loc_id<br />\n{$html}";
		reportAdmin("AS: Data integrity check failed", "There is some inconsistency in classified_sticky table:<br />{$html}");
	}
}


?>

ok
