<?php
//TODO - obsolete cronjob, we dont use home and home_cache tables anymore, no need to update them -> delete this nd crontab entry

chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../html/inc/config.php');
require_once('../html/inc/common.php');

global $db;


//-------------------------------------
//-------------------------------------
echo "Updating table home ...\n\n";

/*
$res = $db->q("delete from home where what = 'frm'");
$res = $db->q("select * from forum_post");
$i = 0;
while($row=$db->r($res)) {
	$loc_id = $row["loc_id"];
	$state_id = $row["state_id"];
	$country_id = $row["country_id"];
	$db->q("insert into home (loc_id, what, cat, loc_parent) values ('$loc_id', 'frm', if({$row['forum_id']}<7,{$row['forum_id']},(select forum_parent from 
forum_forum  where forum_id = '{$row['forum_id']}' limit 1)), (select loc_parent from location_location where loc_id = '$loc_id' limit 1)) on duplicate key update `c` = 
`c` + 1; ");
}
*/

//return;


echo "Classifieds  . . . . . . . . ";
$res = $db->q("delete from home where what = 'cl'");
$res = $db->q("
	SELECT cl.* 
	FROM classifieds_loc cl 
	INNER JOIN classifieds c ON c.id = cl.post_id
	WHERE cl.done > 0 AND c.deleted IS NULL AND (c.sponsor OR c.sponsor_mobile <> 1) AND c.bp = 0
	");
$i = 0;
while($row=$db->r($res)) {
	$loc_id = $row["loc_id"];
	$state_id = $row["state_id"];
	$country_id = 0;

	$db->q("insert into home (loc_id, what, cat, loc_parent) values ('$loc_id', 'cl', '{$row["type"]}', '$state_id') on duplicate key update `c` = `c` + 1;");
	$db->q("insert into home (loc_id, what, cat, loc_parent) values ('$state_id', 'cl', '{$row["type"]}', (select loc_parent from location_location where loc_id = '$state_id')) on duplicate key update `c` = `c` + 1;");
	$i++;
}
echo "processed {$i} entries.\n\n";



//-------------------------------------
//-------------------------------------
echo "Updating table home_cache ...\n";

$res = $db->q("select * from home_cache limit 1");
$fields = [];
$row = $db->r($res, MYSQL_ASSOC);
foreach($row as $fieldname => $value) {
	if (is_numeric($fieldname[strlen($fieldname)-1]))
		$fields[$fieldname] = 0;
}

$current = time();
$res = $db->q("SELECT h.*, l.loc_name, l.loc_url, l.loc_lat, l.loc_long FROM `home` h left join location_location l on h.loc_id = l.loc_id WHERE 1");
while($row = $db->r($res)) {
	$state_id = $row["loc_id"];
	$loc_parent = $row["loc_parent"];
	$cat = $row["what"].$row["cat"];
	$c = $row["c"];
	$loc_name = $row['loc_name'];
	$loc_url = $row['loc_url'];
	$loc_lat = $row["loc_lat"];
	$loc_long = $row["loc_long"];

	if( !isset($sql[$state_id]) ) {
		$sql[$state_id] = array();
		$sql[$state_id]["state_id"] = $state_id;
		$sql[$state_id]["loc_parent"] = $loc_parent;
		$sql[$state_id]["loc_name"] = $loc_name;
		$sql[$state_id]["loc_url"] = $loc_url;
		$sql[$state_id]["loc_lat"] = $loc_lat;
		$sql[$state_id]["loc_long"] = $loc_long;
		$sql[$state_id]["updated"] = $current;
		foreach($fields as $key=>$f) {
			$sql[$state_id][$key] = 0;
		}
	}
	$sql[$state_id][$cat] = $c;

}

foreach($sql as $key=>$val) {
	$insert = $value = $value2 = "";
	$params = array();
	$add = 0;
	foreach($val as $k=>$v) {
		$insert .= $insert ? ", $k" : "$k";
		$value .= $value ? ", '$v'" : "'$v'";
		$value2 .= $value2 ? ", ?" : "?";
		if (is_numeric($v) && $v < 0)
			$v = 0;
		$params[] = $v;
		if( !$add && is_numeric($k[strlen($k)-1]) ) {
			if( $v > 0 ) $add = 1;
		}
	}

	$db->q("delete from home_cache where state_id = '$key'");
	if ($add) {
		$db->q("insert into home_cache ($insert) values ($value2)", $params);
	}
}

$db->q("delete from home_cache where updated < unix_timestamp(date_sub(now(), interval 2 hour))");


echo "Done.\n";

?>
ok
