<?php
chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../html/inc/config.php');
require_once('../html/inc/common.php');

global $db;

$db->q("DELETE LOW_PRIORITY FROM popup_ip WHERE stamp < ?", [time() - 3600*10]); 

#sending fake interest emails for recently posted ads
//disabling 2018-04-17
//classifieds::send_fake_interests();

?>
