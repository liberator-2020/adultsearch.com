<?php

/**
 * Script that clears various "tmp" tables and resets various counters each midnight
 */
chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../html/inc/config.php');
require_once('../html/inc/common.php');

global $db;

//re-setting daily budget limits
$db->q("UPDATE advertise_camp_section SET budget_available = 1 WHERE budget_available = 0");

//rotate advertise index
rotate_index("advertise");

//truncate some tables
$db->q("truncate table advertise_ip");
$db->q("truncate table classifieds_ip");

//do a snapshot of advertisers budgets
$res = $db->q("SELECT account_id, budget FROM advertise_budget WHERE budget > 0");
$day = date("Y-m-d");
$cnt = 0;
while ($row = $db->r($res)) {
	$db->q("INSERT INTO advertiser_budget_snapshot
			(account_id, day, budget, stamp)
			VALUES
			(?, ?, ?, ?)",
			array($row["account_id"], $day, $row["budget"], time())
		);
	if ($db->affected())
		$cnt++;
}
echo "Inserted {$cnt} budgets into advertiser_budget_snapshot table.\n";


//clear analytics - entries olde rthan 3 months
$db->q("DELETE FROM analytics where stamp < ?", [time() - 86400*30*3]);


//clear classifieds_refer - entries older than 1 year
//takes ~2 mins
$db->q("DELETE FROM classifieds_refer where stamp < ?", [time() - 86400*30*12]);


$res = $db->q("SELECT s.id, s.parent, sum(impression) i 
				FROM advertise_section s 
				LEFT JOIN advertise_publish_data d on (s.id=d.s_id and d.date>date_sub(curdate(), interval 2 day)) 
				WHERE s.external=1 and s.active = 1 and s.parent != 0 
				GROUP BY s.id");
while ($row=$db->r($res)) {
	if ($row["i"] < 10) {
		$db->q("update advertise_section set active = 0 where id = ?", array($row["id"]));
	}
}


//---------------------------------------------
//clear unfinished payments older than 3 months
$ago_3_months = time() - 3*30*86400;
//echo "ago_3_months={$ago_3_months}\n";
$res = $db->q("SELECT id FROM payment WHERE ( result = 'I' OR result = 'S' ) AND created_stamp < ? ORDER BY id ASC", [$ago_3_months]);
$p = $i = 0;
while ($row = $db->r($res)) {
    $payment_id = $row["id"];
    $res2 = $db->q("DELETE FROM payment_item WHERE payment_id = ?", [$payment_id]);
    $aff = $db->affected($res2);
    $res2 = $db->q("DELETE FROM payment WHERE id = ? LIMIT 1", [$payment_id]);
    $aff2 = $db->affected($res2);
    if ($aff2 != 1) {
        error_log("cron/midnight: Error: clear unfinished payments older than 3 months aff2={$aff2}, payment_id={$payment_id}");
		continue;
	}
    //echo "Deleted payment #{$payment_id}, {$aff} items too.\n";
    echo ".";
    if ($p % 100 == 0)
        echo "\n";
    $p++;
    $i += $aff;
}
echo "\nDone, deleted {$p} old unfinished payments and {$i} payment items.\n";


//----------------------------------------------------------------------------------
//scan error log and if there is some entry from last 3 days, send an email to admin
$error_log_filepath = _CMS_ABS_PATH."/../data/log/php_error_log";
$since = time() - 3*86400;
echo "Checking PHP error log file '{$error_log_filepath}' ...\n";
$line_count = exec("wc -l '{$error_log_filepath}' | cut -d ' ' -f 1", $output);
//echo "Line_count={$line_count}\n";
$h = fopen($error_log_filepath, "r");
if (!$h) {
	reportAdmin("AS: cron/midnight: Error", "Error: cant open php errror log file '{$error_log_filepath}' !");
} else {
	$i = 0;
	$sending = false;
	$message = null;
	while ($line = fgets($h)) {
		$i++;
		if ($i < ($line_count - 20))
			continue;
		//echo "line={$line}\n";
		if (preg_match('/^\[([0-9]{2})-([0-9]{2})-([0-9]{4}) [^\]]*\]/', $line, $matches)) {
			//echo "matches: ".print_r($matches, true)."\n";
			$stamp = mktime(0, 0, 0, $matches[2], $matches[1], $matches[3]);
			if ($stamp > $since) {
				$sending = true;		
			}
		}
		if ($sending) {
			//echo "{$line}\n";
			$message .= $line."<br />\n";
		}
	}
	if ($message) {
		echo "Found some recent errors, emailing out error messages.\n";
		reportAdmin("AS: cron/midnight: Errors in PHP error log", "Check & fix these errors:<br />\n---------------<br />\n{$message}<br />\n and/or check whole file '{$error_log_filepath}' for errors.");
	} else {
		echo "No recent errors found.\n";
	}
	fclose($h);
}

//check number of spooled emails and send warning if emails are waiting in the spool table
$spo_cnt = intval($db->single("SELECT count(*) FROM spool_gmail WHERE succeeded IS NULL", []));
if ($spo_cnt > 20) {
	echo "Found more than 20 emails waiting in spool, that is too much, sendinf warning to admin.\n";
	reportAdmin("AS: cron/midnight: Too many spooled emails", "Emails not being sent out of spool ? Check & fix.", ["spool_count" => $spo_cnt]);
}

?>
