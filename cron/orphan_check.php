<?php
/* 
 * 1. Watch for files in video upload that were not converted for some reasons.
 *
 * Run:
 *   /usr/bin/php /home/as/dev/incron/orphaned_check.php [delete]
 *
 * If you don't specify 'delete' as argument, files will not be actually deleted.
 * 
 * Logs result to vid.log
 * Sends email to admin
 *
 */
define('OLD_FILE_CRITERIA', 3600); // seconds
define('MAX_FILES', 5000);

////////////////////////////////////
$olddir = getcwd();

chdir( realpath(dirname(__FILE__). "/../html"));

define( '_CMS_FRONTEND', 1);

require_once('inc/config.php');
require_once('inc/common.php');

global $db;

// crude command line parsing
$source_path = $config_image_path . 'classifieds/uploaded/';

$is_dryrun = true;

if( isset($argv[1]) && strtolower($argv[1]) === 'delete') {
	$is_dryrun = false;
}


$now = time()-OLD_FILE_CRITERIA; // one hour ago;

file_log("vid", sprintf('Checking for videos in %s', $source_path.'*'));

/**************************************************************
 * Find old files in database that's still not converted
 **************************************************************/

file_log("vid", sprintf('Finding files older than %d that were not converted for some reason', OLD_FILE_CRITERIA));

$res = $db->q('
	SELECT id, filename, FROM_UNIXTIME(created_stamp) AS created
	FROM classified_video WHERE created_stamp<? AND converted=0',
	array($now)
);

$found_files1 = [];
$found_files2 = [];

while($row = $db->r($res)) {
	$found_files1[] = ['filename'=>$source_path.$row['filename'], 'created'=>$row['created'] ];
}


if( ($c = count($found_files1))>0) {
	file_log("vid", sprintf('Found %d videos',$c));
	reportAdmin("Video check found $c old non-converted files", "List of files below", 	$found_files1 );
} else {
	file_log("vid", 'No files found');
}

/**************************************************************
 * Find old not converted files not mentioned in database
 **************************************************************/

file_log("vid", sprintf('Find files not in the database'));

$files = glob($source_path.'*');


foreach($files as $file) {
	if(is_file($file)) {
		$mtime = filemtime($file);
		if ( $mtime < $now ) {
			$found_files2[] = ['filename'=>$file, 'modtime'=>$mtime ];
			file_log("vid", sprintf('File %s (%s) will be deleted', basename($file), date('Y-m-d H:i:s',$mtime)));
			if(!$is_dryrun) {
				@unlink($file);
			}

		}
    }
}
if( ($c = count($found_files2))>0) {
	//reportAdmin("Video check found $c orphaned files", "List of files below", 	$found_files2 );
} else {
	file_log("vid", 'No files found');
}


chdir($olddir);