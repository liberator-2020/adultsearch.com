<?php

namespace cron\processStickyWaitingList;

use App\Service\Classified\WaitingList;

chdir(__DIR__);
define('_CMS_FRONTEND', 1);

require_once('../html/inc/config.php');
require_once('../html/inc/common.php');

/**
 * Process waiting list and notify user about released sticky slot on locations (By SMS and Email)
 */
$stickyQueueService = new WaitingList();
$stickyQueueService->processWaitingList();
