<?php
/**
 * This script processes all rebills, it is supposed to be called from cron every day in the morning
 */
chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../html/inc/config.php');
require_once('../html/inc/common.php');
ini_set('max_execution_time', 0);

//maximum number of rebills to be processed (can be used to throttle or for testing), default null = no limit
$limit = intval($argv[1]);
if (!$limit)
	$limit = null;

echo "Processing recurring payments, limit={$limit}\n";
file_log("recurring", "Processing start, limit={$limit}");

payment::processRecurringPayments($limit);

file_log("recurring", "Processing finished.");
echo "Done.\n";

//END
