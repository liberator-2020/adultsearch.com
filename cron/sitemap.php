<?php
/**
 * Called from crontab each day at 4am to generate sitemap
 */

chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../html/inc/config.php');
require_once('../html/inc/common.php');
require_once('../html/inc/common-frontend.php');

$sitemap = new sitemap;
$sitemap->createsitemap();

echo "ok\n";

?>
