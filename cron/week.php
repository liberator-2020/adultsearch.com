<?php
chdir(__DIR__);
define( '_CMS_FRONTEND', 1);

require_once('../html/inc/config.php');
require_once('../html/inc/common.php');

global $db;

$db->q("delete LOW_PRIORITY from classifieds_ip where time < (unix_timestamp(now())-(60*60*24*7))");
$db->q("optimize table classifieds_ip");

?>
