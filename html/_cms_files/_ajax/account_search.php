<?php
/**
 * Account search jquery ui AJAX handler 
 * Used in cdvertise admin pages
 */

global $account, $db;

if (!$account->isadmin() && !permission::has("classified_edit") && !permission::has("classified_manage"))
	return;

$advertiser = "";
if ($_REQUEST["advertiser"] == 1)
	$advertiser = " AND advertiser = 1 ";

$term = $_REQUEST["term"];
if (is_numeric($term))
	$res = $db->q("SELECT * FROM account WHERE account_id LIKE ? {$advertiser} LIMIT 10", array($term));
else
	$res = $db->q("SELECT * FROM account WHERE ( username LIKE ? or email LIKE ? ) {$advertiser} LIMIT 10", array("%$term%", "%$term%"));

$response_array = array();
while ($row = $db->r($res)) {
	$acc = account::withRow($row);
	if (!$acc)
		continue;
	$response_array[] = array(
		"id" => $acc->getId(),
		"label" => "#{$acc->getId()} - {$acc->getUsername()} - {$acc->getEmail()}",
	);
}

header('Content-Type: application/json');
echo json_encode($response_array);
die;
?>
