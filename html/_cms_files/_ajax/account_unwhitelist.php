<?php
/**
 * Account unwhitelisting AJAX handler 
 * Used in classifieds manage admin page
 */

global $account, $db;

if (!permission::access("account_whitelist"))
	return;

$account_id = intval($_REQUEST["account_id"]);
$res = $db->q("UPDATE account SET whitelisted = 0 WHERE account_id = ?", array($account_id));
$aff = $db->affected($res);

if ($aff != 1) {
	echo "ERROR: Can't unwhitelist account #{$account_id} ! (maybe account is not whitelisted?)";
	die;
}

audit::log("ACC", "Unwhitelist", $account_id, "");

echo "OK";
die;
?>
