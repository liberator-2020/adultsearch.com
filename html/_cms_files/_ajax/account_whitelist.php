<?php
/**
 * Account whitelisting AJAX handler 
 * Used in classifieds manage admin page
 */

global $account, $db;

if (!permission::access("account_whitelist"))
	return;

$account_id = intval($_REQUEST["account_id"]);
$res = $db->q("UPDATE account SET whitelisted = 1 WHERE account_id = ?", array($account_id));
$aff = $db->affected($res);

if ($aff != 1) {
	echo "ERROR: Can't whitelist account #{$account_id} ! (maybe account is already whitelisted?)";
	die;
}

audit::log("ACC", "Whitelist", $account_id, "");

echo "OK";
die;
?>
