<?php
/* *
 * ajax call is used to add location on /mng/locations "Add location" page
 */

global $db, $account;

if (!permission::has("location_manage"))
    die("Invalid access");

//params
$loc_type = intval($_REQUEST["loc_type"]);
$loc_parent = intval($_REQUEST["loc_parent"]);
$country_id = intval($_REQUEST["country_id"]);
$country_sub = $_REQUEST["country_sub"];
$loc_name = $_REQUEST["loc_name"];
$s = $_REQUEST["s"];
$dir = $_REQUEST["dir"];
$loc_url = "/{$dir}/";
$loc_lat = $_REQUEST["loc_lat"];
$loc_long = $_REQUEST["loc_long"];

//first check if it doesnt exist yet
$res = $db->q("SELECT loc_id FROM location_location WHERE loc_type = ? AND loc_name like ? AND loc_parent = ?", array($loc_type, $loc_name, $loc_parent));
if ($db->numrows($res) > 0) {
	echo "-1";
	die;
}

//insert
$db->q("
	INSERT INTO location_location 
	(loc_type, loc_parent, country_id, county_id, country_sub, loc_name, loc_url, s, dir, loc_lat, loc_long)
	values
	(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
	array(
	$loc_type,
	$loc_parent,
	$country_id,
	0,
	$country_sub,
	$loc_name,
	$loc_url,
	$s,
	$dir,
	$loc_lat,
	$loc_long
	)
);
$loc_id = $db->insertid();

//insert failed
if (!$loc_id)
	die;

//success
echo $loc_id;
die;
?>
