<?php
global $db;

$name = trim($_REQUEST["search_string"]);
if (empty($name))
	die;

$res = $db->q("
	SELECT l.loc_name, l.loc_url, l.country_sub, l2.loc_name as ss
	FROM location_location l
	INNER JOIN location_location l2 on l.loc_parent = l2.loc_id
	WHERE l.loc_type = 3 AND l.has_place_or_ad = 1 AND l.loc_name like ?", 
	[$name."%"]
	);
while($row = $db->r($res)) {
	$link = location::getUrlStatic($row["country_sub"], $row["loc_url"]);
	$arr[] = array(
		"name" => "{$row['loc_name']}, {$row['ss']}",
		"url" => $link
	);
}

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json; charset=utf-8');
echo json_encode($arr);
die;
?>
