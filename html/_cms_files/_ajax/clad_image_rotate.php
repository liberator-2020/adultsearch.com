<?php
/**
 * Clad image rotation
 * Used in classifieds manage admin page
 */

global $db, $config_image_path;

if (!permission::access("classifieds_manage"))
	return;

require_once(_CMS_ABS_PATH."/inc/classes/SimpleImage.php");
$simpleimage = new \claviska\SimpleImage();

//get params
$image_id = intval($_REQUEST["id"]);
$direction = $_REQUEST["direction"];

//lookup image in DB
$res = $db->q("SELECT * FROM classifieds_image WHERE image_id = ? LIMIT 1", [$image_id]);
if ($db->numrows($res) != 1)
	die("ERROR: Can't find image with image_id={$image_id} in database !");
$row = $db->r($res);

//make sure file exists
$src_filename = $row["filename"];
$src_filepath = $config_image_path."/classifieds/{$src_filename}";
if (!file_exists($src_filepath))
	die("ERROR: Image file not found on disk '{$src_filepath}' !");

//create new filename
$pos = strrpos($src_filename, ".");
if ($pos) {
	$tgt_filename = substr($src_filename, 0, $pos)."_1".substr($src_filename, $pos);
} else { 
	$tgt_filename = $src_filename;
}
$tgt_filepath = $config_image_path."/classifieds/{$tgt_filename}";
$tgt_t_filepath = $config_image_path."/classifieds/t/{$tgt_filename}";

//get angle from direction
if ($direction == "right")
	$angle = 90;
else if ($direction == "left")
	$angle = 270;
else
	die("ERROR: Unknown direction '{$direction}' !");


try {
	$simpleimage
    	->fromFile($src_filepath)
		->rotate($angle)
    	->toFile($tgt_filepath);
	$simpleimage
    	->fromFile($src_filepath)
		->resize(200,null)
		->rotate($angle)
    	->toFile($tgt_t_filepath);
} catch(\Exception $e) {
	die("ERROR: {$e->getCode}: {$e->getMessage()} !");
}

$res = $db->q("UPDATE classifieds_image SET filename = ? WHERE image_id = ? LIMIT 1", [$tgt_filename, $image_id]);
$aff = $db->affected($res);
if ($aff != 1)
	die("ERROR: Can't updat eclassifieds_image entry: filename='{$tgt_filename}', image_id={$image_id}");

//delete old image file
unlink($src_filepath);

echo "OK";
die;
?>
