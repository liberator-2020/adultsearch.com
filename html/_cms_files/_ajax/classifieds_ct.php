<?php
/**
 * Adding/removing city thumbnail for classified ad
 * Used in classifieds manage admin page
 */

global $account, $db;

if (!permission::access("classifieds_manage")) {
	echo "Error - no permission for this!";
	return;
}

$id = intval($_REQUEST["id"]);
$action = $_REQUEST["action"];

$clad = clad::findOneById($id);
if (!$clad)
	json_response(["status" => "error", "reason" => "Incorrect clad id '{$id}' !"]);


if ($action == "ct_add") {

	$loc_id = intval($_REQUEST["loc_id"]);
	$until = $_REQUEST["until"];

	if (!$loc_id || !$until)
		json_response(["status" => "error", "reason" => "Missing parameter"]);

	$until = preg_replace('/_/', ' ', $until);
	if (($stamp = strtotime($until)) === false)
		json_response(["status" => "error", "reason" => "Incorrect timestamp '{$until}' !"]);

	$now = time();
	$datediff = $stamp - $now;
	if ($datediff < 0)
		json_response(["status" => "error", "reason" => "Until must be in future ({$until})!"]);

	$day = floor($datediff/(60*60*24));

	$loc = location::findOneById($loc_id);
	if (!$loc)
		json_response(["status" => "error", "reason" => "Invalid location"]);

	if (!classifieds::locationHasFreeSlotcityThumbnail($loc_id))
		json_response(["status" => "error", "reason" => "Location doesn't have free spot for city thumbnail"]);

	$res = $db->q("SELECT * FROM classifieds_sponsor WHERE post_id = ? AND loc_id = ?", array($id, $loc_id));
	if ($db->numrows($res) > 0)
		json_response(["status" => "error", "reason" => "City thumbnail already exists for post_id={$id} and loc_id={$loc_id} !"]);

	$res = $db->q("INSERT INTO classifieds_sponsor (loc_id, state_id, post_id, day, type, expire_stamp, done, notified, images)
					VALUES
					(?, ?, ?, ?, ?, ?, ?, ?, ?)
					", 
					array($loc_id, $loc->getLocParent(), $id, $day, $clad->getType(), $stamp, $clad->getDone(), 0, '')
				);
	$aff = $db->affected($res);
	if ($aff != 1)
		json_response(["status" => "error", "reason" => "Can't add this sponsor, please contact administrator"]);

	audit::log("CLA", "CityThumbAdd", $id, "Loc_id: {$loc_id}, Until: {$until}");

	json_response([
		"status" => "success",
		]);

} else if ($action == "ct_can") {
	//cancelling city thumbnail

	$ct_id = intval($_REQUEST["ct_id"]);

	if (!$ct_id)
		json_response(["status" => "error", "reason" => "Missing parameter"]);

	$res = $db->q("SELECT * FROM classifieds_sponsor WHERE id = ?", [$ct_id]);
	if ($db->numrows($res) != 1)
		json_response(["status" => "error", "reason" => "Can't find this city thumbnail's data in database, please contact administrator"]);
	$row = $db->r($res);
	$done = $row["done"];
	$clad_id = $row["post_id"];
	$loc_id = $row["loc_id"];

	if ($done != 1)
		json_response(["status" => "error", "reason" => "This city thumbnail upgrade is not live"]);

	//check if there is no payment for this upgrade
	$month_ago = time() - 86400 * 30;
	$res = $db->q("SELECT t.id, t.stamp, t.amount
		FROM transaction t 
		INNER JOIN payment p on p.id = t.payment_id
		INNER JOIN payment_item pi on pi.payment_id = p.id
		WHERE pi.classified_id = ? AND pi.loc_id = ? AND pi.sponsor = 1 AND t.stamp > ? AND t.amount > 0",
		[$clad_id, $loc_id, $month_ago]
		);
	if ($db->numrows($res) > 0) {
		$row = $db->r($res);
		json_response(["status" => "error", "reason" => "Non-refunded transaction #{$row["id"]} for this city thumbnail upgrade"]);
	}
	$res = $db->q("SELECT p.id, p.created_stamp
		FROM payment p
		INNER JOIN payment_item pi on pi.payment_id = p.id
		WHERE pi.classified_id = ? AND pi.loc_id = ? AND pi.sponsor = 1 AND p.subscription_status = 1",
		[$clad_id, $loc_id]
		);
	if ($db->numrows($res) > 0) {
		$row = $db->r($res);
		json_response(["status" => "error", "reason" => "Active recurring payment invoice #{$row["id"]} for this city thumbnail upgrade"]);
	}

	//cancel
	$res = $db->q("DELETE FROM classifieds_sponsor WHERE id = ? LIMIT 1", [$ct_id]);
	if ($db->affected($res) != 1)
		json_response(["status" => "error", "reason" => "Can't cancel this city thumbnail, please contact administrator"]);

	classifieds::rotateIndex();

	audit::log("CLA", "CityThumbnailCancel", $id, "");

	json_response([
		"status" => "success",
		]);

} else if ($action == "ct_chl") {

	$ct_id = intval($_REQUEST["ct_id"]);
	$new_loc_id = intval($_REQUEST["new_loc_id"]);

	if (!$ct_id || !$new_loc_id)
		json_response(["status" => "error", "reason" => "Missing parameter"]);

	$loc = location::findOneById($new_loc_id);
	if (!$loc)
		json_response(["status" => "error", "reason" => "Invalid location"]);

	if (!classifieds::locationHasFreeSlotCityThumbnail($new_loc_id))
		json_response(["status" => "error", "reason" => "Location doesn't have free spot for city thumbnail"]);

	$res = $db->q("SELECT * FROM classifieds_sponsor WHERE id = ?", [$ct_id]);
	if ($db->numrows($res) != 1)
		json_response(["status" => "error", "reason" => "Can't find this city thumbnail's data in database, please contact administrator"]);
	$row = $db->r($res);
	$until = date("m/d/Y", $row["expire_stamp"]);

	$res = $db->q("UPDATE classifieds_sponsor SET loc_id = ?, state_id = ? WHERE id = ? AND post_id = ?", [$loc->getId(), $loc->getLocParent(), $ct_id, $id]);
	if ($db->affected($res) != 1)
		json_response(["status" => "error", "reason" => "Can't update this city thumbnail's location, please contact administrator"]);

	classifieds::rotateIndex();

	audit::log("CLA", "CityThumbnailChangeLocation", $id, "New location: {$loc->getLabel()}");

	json_response([
		"status" => "success",
		"loc_name" => $loc->getLabel(),
		"until" => $until,
		]);
}


echo "Error: unknow action !";
die;

?>
