<?php
/**
 * Adding/removing side sponsor for classified ad
 * Used in classifieds manage admin page
 */

global $account, $db;

if (!permission::access("classifieds_manage")) {
	echo "Error - no permission for this!";
	return;
}

$id = intval($_REQUEST["id"]);
$action = $_REQUEST["action"];
$clad = clad::findOneById($id);

if (!$clad)
	json_response(["status" => "error", "reason" => "Incorrect clad id '{$id}' !"]);

if ($action == "ss_add") {

	$loc_id = intval($_REQUEST["loc_id"]);
	$until = $_REQUEST["until"];
	
	if (!$loc_id || !$until)
		json_response(["status" => "error", "reason" => "Missing parameter"]);

	$until = preg_replace('/_/', ' ', $until);
	if (($stamp = strtotime($until)) === false)
		json_response(["status" => "error", "reason" => "Incorrect timestamp '{$until}' !"]);

	$now = time();
	$datediff = $stamp - $now;
	if ($datediff < 0)
		json_response(["status" => "error", "reason" => "Until must be in future ({$until})!"]);

	$day = floor($datediff/(60*60*24));

	$loc = location::findOneById($loc_id);
	if (!$loc)
		json_response(["status" => "error", "reason" => "Invalid location"]);

	if (!classifieds::locationHasFreeSlotSideSponsor($loc_id, $clad->getType()))
		json_response(["status" => "error", "reason" => "Location doesn't have free spot for side sponsor"]);

	$res = $db->q("SELECT * FROM classifieds_side WHERE post_id = ? AND loc_id = ?", array($id, $loc_id));
	if ($db->numrows($res) > 0)
		json_response(["status" => "error", "reason" => "side sponsor already exists for post_id={$id} and loc_id={$loc_id} !"]);

	$res = $db->q("INSERT INTO classifieds_side 
					(loc_id, state_id, post_id, day, type, expire_stamp, done, notified)
					VALUES
					(?, ?, ?, ?, ?, ?, ?, ?)
					", 
					[$loc_id, $loc->getLocParent(), $id, $day, $clad->getType(), $stamp, $clad->getDone(), 0]
				);
	$aff = $db->affected($res);
	if ($aff != 1)
		json_response(["status" => "error", "reason" => "Can't add this sponsor, please contact administrator"]);

	audit::log("CLA", "SideSponsorAdd", $id, "Loc_id: {$loc_id}, Until: {$until}");

	json_response([
		"status" => "success",
		]);

} else if ($action == "ss_chl") {

	$ss_id = intval($_REQUEST["ss_id"]);
	$new_loc_id = intval($_REQUEST["new_loc_id"]);

	if (!$ss_id || !$new_loc_id)
		json_response(["status" => "error", "reason" => "Missing parameter"]);

	$loc = location::findOneById($new_loc_id);
	if (!$loc)
		json_response(["status" => "error", "reason" => "Invalid location"]);

	if (!classifieds::locationHasFreeSlotSideSponsor($new_loc_id, $clad->getType()))
		json_response(["status" => "error", "reason" => "Location doesn't have free spot for side sponsor"]);

	$res = $db->q("SELECT * FROM classifieds_side WHERE id = ?", [$ss_id]);
	if ($db->numrows($res) != 1)
		json_response(["status" => "error", "reason" => "Can't find this side sponsor's data in database, please contact administrator"]);
	$row = $db->r($res);
	$until = date("m/d/Y", $row["expire_stamp"]);

	$res = $db->q("UPDATE classifieds_side SET loc_id = ?, state_id = ? WHERE id = ? AND post_id = ?", [$loc->getId(), $loc->getLocParent(), $ss_id, $id]);
	if ($db->affected($res) != 1)
		json_response(["status" => "error", "reason" => "Can't update this side sponsor's location, please contact administrator"]);

	classifieds::rotateIndex();

	audit::log("CLA", "SideSponsorChangeLocation", $id, "New location: {$loc->getLabel()}");

	json_response([
		"status" => "success",
		"loc_name" => $loc->getLabel(),
		"until" => $until,
		]);
}

echo "Error: unknow action !";
die;

?>
