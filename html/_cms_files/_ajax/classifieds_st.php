<?php
/**
 * Adding/removing side sponsor for classified ad
 * Used in classifieds manage admin page
 */

global $account, $db;

if (!permission::access("classifieds_manage")) {
	echo "Error - no permission for this!";
	return;
}

$id = intval($_REQUEST["id"]);
$action = $_REQUEST["action"];
$clad = clad::findOneById($id);
$days = array_key_exists("days", $_REQUEST) ? intval($_REQUEST["days"]) : NULL;
$ad_type = $clad->getType();


if (!$clad)
	json_response(["status" => "error", "reason" => "Incorrect clad id '{$id}' !"]);

if ($action == "st_add") {

	$loc_id = intval($_REQUEST["loc_id"]);
	$until = $_REQUEST["until"];
	
	if (!$loc_id || !$until)
		json_response(["status" => "error", "reason" => "Missing parameter"]);

	$until = preg_replace('/_/', ' ', $until);
	if (($stamp = strtotime($until)) === false)
		json_response(["status" => "error", "reason" => "Incorrect timestamp '{$until}' !"]);

	$now = time();
	$datediff = $stamp - $now;
	if ($datediff < 0)
		json_response(["status" => "error", "reason" => "Until must be in future ({$until})!"]);

	$days = intval($_REQUEST["days"]);

	$loc = location::findOneById($loc_id);
	if (!$loc)
		json_response(["status" => "error", "reason" => "Invalid location"]);

	if (!classifieds::locationHasFreeSlotSticky($loc_id, $ad_type))
		json_response(["status" => "error", "reason" => "Location doesn't have free spot for sticky upgrade"]);

	$res = $db->q("SELECT * FROM classified_sticky WHERE classified_id = ? AND loc_id = ?", array($id, $loc_id));
	if ($db->numrows($res) > 0)
		json_response(["status" => "error", "reason" => "sticky sponsor already exists for classified_id={$id} and loc_id={$loc_id} !"]);

	$res = $db->q("INSERT INTO classified_sticky
					(loc_id, classified_id, days, type, expire_stamp, done, notified)
					VALUES
					(?, ?, ?, ?, ?, ?, ?)
					", 
					[$loc_id, $id, $days, $ad_type, $stamp, $clad->getDone(), 0]
				);
	$aff = $db->affected($res);
	if ($aff != 1)
		json_response(["status" => "error", "reason" => "Can't add this upgrade, please contact administrator"]);

	audit::log("CLA", "StickySponsorAdd", $id, "Loc_id: {$loc_id}, Until: {$until}");

	if($days != 30){
		$until_resp = new DateTime();
		$until_resp->setTimestamp($now + $days * 86400);
		$until = $until_resp->format('Y-m-d');
	}

	json_response([
		"status" => "success",
		"until" => $until
	]);

} else if ($action == "st_chl") {

	$st_id = intval($_REQUEST["st_id"]);
	$new_loc_id = intval($_REQUEST["new_loc_id"]);

	if (!$st_id || !$new_loc_id)
		json_response(["status" => "error", "reason" => "Missing parameter"]);

	$loc = location::findOneById($new_loc_id);
	if (!$loc)
		json_response(["status" => "error", "reason" => "Invalid location"]);

	//check if there is free sticky slot in this location
    if (!classifieds::locationHasFreeSlotSticky($new_loc_id, $ad_type))
        json_response(["status" => "error", "reason" => "There is no free slot for stickies in {$loc->getLabel()}"]);

	$res = $db->q("SELECT * FROM classified_sticky WHERE id = ?", [$st_id]);
	if ($db->numrows($res) != 1)
		json_response(["status" => "error", "reason" => "Can't find this sticky sponsor's data in database, please contact administrator"]);
	$row = $db->r($res);
	$until = date("m/d/Y", $row["expire_stamp"]);

	$res = $db->q("UPDATE classified_sticky SET loc_id = ? WHERE id = ? AND classified_id = ?", [$loc->getId(), $st_id, $id]);
	if ($db->affected($res) != 1)
		json_response(["status" => "error", "reason" => "Can't update this sticky sponsor's location, please contact administrator"]);

	classifieds::rotateIndex();

	audit::log("CLA", "StickySponsorChangeLocation", $id, "New location: {$loc->getLabel()}");

	json_response([
		"status" => "success",
		"loc_name" => $loc->getLabel(),
		"until" => $until
	]);
}

echo "Error: unknow action !";
die;

?>
