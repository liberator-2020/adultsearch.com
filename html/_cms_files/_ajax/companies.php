<?php

//disabling forum site-wide
die;

global $db;

$forum = new forum();

$forum_id = intval(GetGetParam("forum_id"));
$loc_id = intval(GetGetParam("loc_id"));

if ($forum_id == 0 || $loc_id == 0)
	die;

$companies = $forum->getCompanies($loc_id, $forum_id);
if (empty($companies))
	die;
$category_name = $forum->getSingular($forum->getForumNameById($forum_id));

$html = "<br /><span id=\"category_name\">Are you talking about specific {$category_name} ?</span><br />";
$html .= "<select name=\"item_id\">";
foreach ($companies as $k => $v) {
	$html .= "<option value=\"{$k}\">".htmlspecialchars($v)."</option>";
}
$html .="</select></span>";

header('Cache-Control: no-cache, must-revalidate');
echo $html;
die;

?>
