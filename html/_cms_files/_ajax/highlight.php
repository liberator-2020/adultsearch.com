<?php
/**
 * Admin highlighter
 */

global $account, $db;

if (!permission::access("access_admin"))
	die('{"status": "error", "reason": "no permission to highlight"}');

$current = intval($_REQUEST["current"]);
$module = $_REQUEST["module"];
$item_id = intval($_REQUEST["item_id"]);
if (($current != 0 && $current != 1) || !$module || !$item_id)
	die('{"status": "error", "reason": "invalid parameters"}');

$new = ($current == 1) ? 0 : 1;
if ($new == 1)
	$res = $db->q("INSERT INTO admin_highlight (module, item_id, created_stamp, created_by) VALUES (?, ?, ?, ?)", array($module, $item_id, time(), $account->getId()));
else
	$res = $db->q("DELETE FROM admin_highlight WHERE module = ? AND item_id = ?", array($module, $item_id));
$aff = $db->affected($res);

if ($aff == 0)
	die('{"status": "error", "reason": "DB operation failed"}');

die('{"status": "ok"}');
?>
