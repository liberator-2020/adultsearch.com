<?php
/* *
 * ajax call is used to bring list of cities with place or ad in specified state
 * Used on landing homepage
 */
global $db, $ctx;

$state_id = intval($_REQUEST["s"]);
if ($state_id == 0)
	die;

$res = $db->q("SELECT country_id, country_sub, loc_url, loc_name, s 
				FROM location_location 
				WHERE loc_type = 3 AND has_place_or_ad = 1 AND loc_parent = ? 
				ORDER BY loc_name ASC", array($state_id));
if ($db->numrows($res) < 1)
	die;

$link_base = "";

while($row = $db->r($res)) {
	if (empty($link_base)) {
		$link_base = "https://";
		$link_base .= ($row["country_id"] == 16046) ? "" : $row["country_sub"].".";
		$link_base .= $ctx->domain_base;
	}
	$url = $link_base.$row["loc_url"];
	$title = "{$row["loc_name"]}, {$row["s"]}";
	echo "<li><a href=\"{$url}\" title=\"{$title}\">{$row["loc_name"]}</a></li>\n";
}

die;

?>
