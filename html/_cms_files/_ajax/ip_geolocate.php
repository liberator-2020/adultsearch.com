<?php
global $account, $db;

$ip_address = preg_replace('/[^0-9\.]/', '', $_REQUEST["ip"]);
if (!$ip_address)
	die;

//die("ip_address='{$ip_address}'");
$location = geolocation::getLocationByIp($ip_address);
//_darr($location);
//die('loc='.$location);

$html = "";
if ($location["city_name"])
	$html .= $location["city_name"];

if ($location["state_name"])
	$html .= ", ".$location["state_name"];

if ($location["country_id"] || $location["country_sub"]) {
	$code = "";
	switch ($location["country_id"]) {
		case 16046: $code = "us"; break;
		case 16047: $code = "ca"; break;
		case 41973: $code = "gb"; break;
		default: break;
	}
	if (!$code && $location["country_sub"] && strlen($location["country_sub"]) == 2) 
		$code = strtolower($location["country_sub"]);
	if ($code)
		$html .= ", <img src=\"/images/flags/{$code}.gif\" />";
}

echo $html;
die;
?>
