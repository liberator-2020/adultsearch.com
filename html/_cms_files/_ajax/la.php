<?php

global $db;

$loc = explode(",", $_REQUEST["q"]);

$loc[0] = trim($loc[0]);
$loc[1] = trim($loc[1]);

$params = [];
$params[] = "{$loc[0]}%";
$additional_query = "";
if (isset($loc[1])) {
	$additional_query = " AND s like ? ";
	$params[] = "{$loc[1]}%";
}
$res = $db->q("
	SELECT loc_id id, CONCAT(loc_name, ', ', s, IF(loc_type=4,' (neighbor)','')) as name 
	FROM location_location 
	WHERE loc_type > 2 AND loc_name like ? {$additional_query}",
	$params
	);
$arr = NULL;
while ($row = $db->r($res)) {
	$arr[] = $row;
}

if (is_null($arr))
	die;

$json_response = json_encode($arr);
echo $json_response;
die;

?>
