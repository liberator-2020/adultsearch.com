<?php

global $db;

$_REQUEST["template"] = "blank.tpl";

$action		= GetGetParam('action');
$select_name	= GetGetParam('select_name');
$parent_name	= GetGetParam('parent_name');
$parent_id	= GetGetParam('parent_id');
$class = GetGetPAram('class');
$city_id = GetGetParam('city_id');

$jscript1 = "''";
$jscript2 = "''";

$city_select = isset($_GET["city_select"])  ? $_GET["city_select"] : "city_select";

$classselect = !empty($class) ? "class=\"$class\"" : "";
$classjs = !empty($class) ?  "&class=$class" : "";

$city_idselect = !empty($city_id) ? "id=\"$city_id\"" : "";
$city_idjs = !empty($city_id) ? "&city_id=$city_id" : "";

$parent_name = strtoupper($parent_name);
if( $select_name == "Country" ) {
	$loc_parent = 0;
} else {
	$res = $db->q("SELECT loc_id FROM location_location WHERE loc_id = '{$parent_name}'");
	if (!$db->numrows($res)) {
		echo "<select name=\"{$select_name}\"><option value=\"\">-- Select --</option></select>";
		die;
	}
	$SQLresult = $db->r($res);
	$parent_id = $SQLresult["loc_id"];

	if (empty($parent_id)) {
		die;
	}
}
if( $parent_id == 0 )
	$res = $db->q("SELECT loc_id, loc_name, loc_type FROM location_location WHERE loc_id in (16046, 16047, 41973) ORDER BY loc_name ASC");
else
	$res = $db->q("SELECT loc_id, loc_name, loc_type FROM location_location WHERE loc_parent = '{$parent_id}' ORDER BY loc_name ASC");
if ($db->numrows($res)) {
	$jscript1 = "[";
	$jscript2 = "[";
	$i = 0;
	while($SQLrow=$db->r($res)) {
		list($loc_id, $loc_name, $loc_type) = $SQLrow;
		if ($i != 0) {
			$jscript1 .= ",";
			$jscript2 .= ",";
		}
		$jscript1 .= "'".addslashes($loc_id)."'";
		$jscript2 .= "'".addslashes($loc_name)."'";
		$i++;
		$selected = (strtoupper($select_name) == "STATE" && $loc_id == $_GET["State"]) ? "selected=\"selected\"" : 
			((strtoupper($select_name) == "CITY" && $loc_id == $_GET["City"]) ? "selected=\"selected\"" : ((strtoupper($select_name) == "NEIGHBOR" && $loc_id == $_GET["Neighbor"])?"selected=\"selected\"":""));
		if( $select_name == "Country" && $_GET["Country"] == $loc_id ) $selected = "selected=\"selected\"";
		$option .= "<option value=\"{$loc_id}\" $selected>{$loc_name}</option>";
	}
	$alt_location =  $loc_type == 2 ? "-- states --" : "-- cities --";
	$option = "<option value=\"\">$alt_location</option>" . $option;
	$jscript1 .= "]";
	$jscript2 .= "]";
} else
	die;

$onChange = isset($_GET["onchange"]) ? "{$_GET["onchange"]}(this.options[this.selectedIndex].value);" : "";
$onChangeJs = $onChange ? "&onchange={$_GET["onchange"]}" : "";

if (strtoupper($select_name) == "COUNTRY") {
	if( isset($_GET["onChangeCity"]) )
		$onChangeCityJs = "&onChangeCity=".$_GET["onChangeCity"];
	else
		$onChangeCityJs = "";
	$state_select = "onchange=\"\$('#state_select').html(''); \$('#city_select').html('');  \$('#neighbor_select').html('');_ajax('get','/_ajax/loc', 'parent_name='+this.options[this.selectedIndex].value+'&select_name=State$classjs$city_idjs$onChangeCityJs$onChangeJs', 'state_select');$onChange\"";
	$city_idselect = "";	
} else if (strtoupper($select_name) == "STATE" || $loc_type == 2) {
	if( isset($_GET["onChangeCity"]) )
		$onChangeCityJs = "&onChangeCity=".$_GET["onChangeCity"];
	else
		$onChangeCityJs = "";
	$state_select = "onchange=\"$('#{$city_select}').html(''); _ajax('get','/_ajax/loc', 'parent_name='+this.options[this.selectedIndex].value+'&select_name=City$classjs$city_idjs$onChangeCityJs$onChangeJs', '{$city_select}');$onChange\"";
	$city_idselect = "";
} else {
	if( isset($_GET["onChangeCity"]) )
		$state_select = "onchange=\"".$_GET["onChangeCity"] ."(this.options[this.selectedIndex].value); $onChange;return false;\"";
	else
		$state_select = "onchange=\"$onChange return false;\"";

	if( isset($_GET['deep']) && $_GET['deep'] == 4 ) {
		$state_select = "onchange=\" _ajax('get','/_ajax/loc', 'parent_name='+this.options[this.selectedIndex].value+'&select_name=Neighbor$classjs$city_idjs$onChangeCityJs$onChangeJs', 'neighbor_select'); \"";
	}
}

$onChangeCity = ""; 
echo "<select name=\"{$select_name}\" id=\"{$select_name}\" $state_select $classselect $city_idselect $onChangeCity>{$option}</select>";
die;

?>
