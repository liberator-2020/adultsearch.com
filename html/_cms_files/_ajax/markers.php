<?php
/**
 * Get markers for map
 * Used in /_ajax/markers?type=15,74,72&loc_lat=&loc_long=
 */

global $account, $db;

//if (!$account->isadmin() && !permission::has("classified_edit") && !permission::has("classified_manage"))
//	return;

require_once(_CMS_ABS_PATH."/inc/classes/class.dir.php");
$dir = new dir();

$company = array();

if (!empty($_REQUEST["loc_lat"]) || !empty($_REQUEST["loc_long"])) {

	$map_limit  = 500;
	$map_radius = 30; // mile

	$loc_lat  = floatval($_REQUEST["loc_lat"]);
	$loc_long = floatval($_REQUEST["loc_long"]);

	$filter_types = array();

	if (!empty($_REQUEST["type"])) {
		$filter_types = array_map('intval', explode(',', $_REQUEST["type"]));
	}

	$res = $dir->getListMarkers($filter_types, $map_limit, $map_radius,
		array('loc_lat' => $loc_lat, 'loc_long' => $loc_long));

	while ($row = $db->r($res, MYSQL_ASSOC)) {
		if ($row['loc_lat'] > 0 || $row['loc_long'] > 0) { // @todo move to getListMarkers
			$id   = $row["place_id"];
			$link = $ctx->domain_link.$dir->getPlaceLink($row);

			$company[] = array(
				"id"           => $id,
				"name"         => $row["name"],
				"linkoverride" => $link,
				"longitude"    => $row['loc_long'],
				"latitude"     => $row['loc_lat'],
			);
		}
	}

}
header('Content-Type: application/json');
echo json_encode($company);
die;
?>