<?php
/**
 * used in adbuild on step3
 */
global $db, $account;

//params
$action = $_REQUEST["action"];
$classified_id = intval($_REQUEST["ad_id"]);
$phone = preg_replace('/[^0-9\+]/', '', $_REQUEST["phone"]);
$code = preg_replace('/[^0-9]/', '', $_REQUEST["code"]);

if ($action == "is_verified") {
	$isVerified = phone::isVerified($phone);
	die((string)intval($isVerified));
}

if ($action == "start_verification") {
	$ret = phone::startVerification($phone);
	if ($ret === false)
		die("0");
	else
		die("1");
}

if ($action == "code_check") {
	$ret = phone::updateVerification($code, $phone);
	if ($ret === true) {
		die("S");	//success
	} else if ($ret === false) {
		die("E");	//error
	} else if ($ret > 0) {
		die("F");	//failed, some attempts left
	} else {
		//failed, zero attempts left
		die("0");
	}
}

die("ERROR");
?>
