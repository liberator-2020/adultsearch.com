<?
ob_end_clean();

global $db, $account;

$zipcode = GetPostParam("zipcode");
$zipcode = preg_replace("/[^0-9a-z]/i", "", $zipcode);
$loc_name = GetPostParam("loc_name");
$zipcode = GetPostParam("zipcode");
$zipcode = preg_replace("/[^0-9a-z]/i", "", $zipcode);
$loc_name = GetPostParam("loc_name");
$where = urldecode(GetPostParam("where"));

if( strstr($where, "?") ) $where .= "&"; else $where .= "?";
if( $where[0] == "/" ) $where = substr($where, 1);

if( !empty($zipcode) ) {
	$res = $db->q("select l.loc_id, l.loc_parent, ll.loc_name from core_zip z inner join (location_location l inner join location_location ll on l.loc_parent = 
ll.loc_id) on z.loc_id = l.loc_id where z.zip = '$zipcode'");
	if( $db->numrows($res) ) {
		$row = $db->r($res);
		$account->setcookie("core_state_id", $row["loc_parent"]);
		$account->setcookie("core_state_name", $row["loc_name"]);
		header('Content-type: application/javascript');
		$where = preg_replace("/core\_state\_id=\d+/", "core_state_id=".$row["loc_parent"],$where);
		$where = preg_replace("/loc\_id=\d+/", "loc_id=".$row["loc_id"],$where);
		die("<script>window.location = '/$where';</script>");
	} else { die("<div class='error'>zip code $zipcode not found</div>"); }
} if( !empty($loc_name) ) {
	$res = $db->q("select l.loc_id, l.loc_name, l.loc_parent, l.s, ll.loc_name state from location_location l inner join location_location ll on l.loc_parent = 
ll.loc_id where l.loc_name like '$loc_name'");
	$total_city = $db->numrows($res);
	if( $total_city ) {
		if( $total_city == 1 ) {
			$row = $db->r($res);
			$account->setcookie("core_state_id", $row["loc_parent"]);
			$account->setcookie("core_state_name", $row["state"]);
			header('Content-type: application/javascript');
			$where = preg_replace("/core\_state\_id=\d+/", "core_state_id=".$row["loc_parent"],$where);
			$where = preg_replace("/loc\_id=\d+/", "loc_id=".$row["loc_id"],$where);
			die("<script>window.location = '/$where';</script>");
		}
		echo "<div style=\"color:#ff0000\">There are more than one cities found, Please make your selection below;</div><br/><ul>";
		$count = 1;
		while($row=$db->r($res)) {
			$where = preg_replace("/core\_state\_id=\d+/", "" ,$where);
			$where = preg_replace("/loc\_id=\d+/", "", $where);
			echo "<li style='list-style:disc;'><a class='' href='/{$where}core_state_id={$row["loc_parent"]}&loc_id={$row["loc_id"]}'>{$row["loc_name"]}, {$row["s"]}</a></li><br/>";
			$count++;
		}
		echo "</ul>";
		die;
	} else {
		die("<div class='error'>No city found with that name, please check your spelling</div>");
	}
} else die("<div class='error'>Please enter your zip code or the city name.</div>");

die;

?>
