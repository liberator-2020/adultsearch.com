<?php
/**
 * SMS Blast AJAX handler
 */

global $db, $config_dev_server;

$sms = new swiftsms();

$n = 10;

//get N messages to send from database
$res = $db->q("SELECT * FROM sms_blast WHERE sent_stamp IS NULL AND skip IS NULL ORDER BY id LIMIT {$n}");
if ($db->numrows($res) == 0) {
	echo "E-0-No messages to send.";
	die;
}

$successfully_sent = 0;
while($row = $db->r($res)) {
	$id = $row["id"];
	$phone = $row["phone"];
	$message = $row["message"];
	//on dev dont send actual sms message
	//if (!$config_dev_server && !$sms->send($phone, $message)) {
	if (!$config_dev_server) {
		$ret = $sms->send($phone, $message);
		if (!$ret) {
			//sending SMS failed
			$response = $sms->response;
			if ($sms->response == "CellNumber has opted-out." || $sms->response == "Invalid phone number.") {
				//set skip flag and continue
				$db->q("UPDATE sms_blast SET response = ?, skip = 1 WHERE id = ?", array($sms->response, $id));
				continue;
			}
			//$db->q("UPDATE sms_blast SET response = ? WHERE id = ?", array($response, $id));
			$db->q("UPDATE sms_blast SET response = ? WHERE id = ?", array("Error: ".$sms->error, $id));
			die("E-{$successfully_sent}-SMS Gateway error: {$sms->error}.");
		}
	}
	//update sent stamp in db
	//$response = $sms->response;
	//$db->q("UPDATE sms_blast SET sent_stamp = ?, response = ? WHERE id = ?", array(time(), $response, $id));
	$db->q("UPDATE sms_blast SET sent_stamp = ?, response = ? WHERE id = ?", array(time(), "sent ok", $id));
	$successfully_sent++;
}

echo "S-{$successfully_sent}";
die;

?>
