<?php
/**
 * SMS Verification of phone number
 * Used in adbuild step 4.5 when using new credit card to prevent credit card frauds
 * Also used in account/verify 
 */

global $db, $config_dev_server;

function get_sms_verification_code() {
	$code = rand(1,9).rand(0,9).rand(0,9).rand(0,9);
	return $code;
}

function get_country_prefix($country) {
	switch ($country) {
		case 'sk':
			return '+421'; break;
		case 'uk':
			return '+44'; break;
		default:
	}
	return '';
}

if (isset($_REQUEST["code"])) {
	$code = preg_replace('/[^0-9]/', '', $_REQUEST["code"]);
	if (isset($_SESSION["sms_verify_code"]) 
			&& isset($_SESSION["sms_verify_code"])
			&& strlen($code) == 4
			&& $code == $_SESSION["sms_verify_code"] 
			&& $_SESSION["sms_verify_cnt"] < 3) {
		//ok we entered correct sms verify code, generate check code
		$check = md5(get_sms_verification_code().time());
		debug_log("SMS verification OK, phone={$_SESSION["sms_verify_phone"]}, code={$_SESSION["sms_verify_code"]}, cnt={$_SESSION["sms_verify_cnt"]}, check = '{$check}'");
		unset($_SESSION["sms_verify_code"]);
		unset($_SESSION["sms_verify_cnt"]);
		$_SESSION["sms_verify_check"] = $check;
		die("OK {$check}");
	} else {
		if (intval($_SESSION["sms_verify_cnt"]) >= 2) {
			unset($_SESSION["sms_verify_phone"]);
			unset($_SESSION["sms_verify_code"]);
			unset($_SESSION["sms_verify_cnt"]);
			debug_log("SMS verification: wrong try #3, code = '{$code}'");
			//die("1Error: Wrong verification code. Please request new verification code.");
			die("OK wrong");
		} else {
			$_SESSION["sms_verify_cnt"] = intval($_SESSION["sms_verify_cnt"]) + 1;
			debug_log("SMS verification: wrong try #".$_SESSION["sms_verify_cnt"].", code = '{$code}'");
			die("Error: Wrong verification code. Please check your cell phone and enter correct verification code.");
		}
	}
}

//we want to generate new verify code
unset($_SESSION["sms_verify_phone"]);
unset($_SESSION["sms_verify_code"]);
unset($_SESSION["sms_verify_cnt"]);
unset($_SESSION["sms_verify_check"]);

$country = preg_replace('/[^a-z]/', '', $_REQUEST["country"]);
$phone = preg_replace('/[^0-9\+]/', '', $_REQUEST["phone"]);
$phone = ltrim($phone, '0');
$ad_id = intval($_REQUEST["ad_id"]);

if ($country != "us" && $country != "ca" && $country != "uk" && $country != "sk" && $country != "international") {
	die("Error: Unsupported country code, please select country from list above.".$country);
}

if ($phone == "") {
	die("Error: Phone number not entered. Please enter phone number in field above.");
}

if ($ad_id == 0) {
	die("Error: Unspecified error.");
}


$phone_util = \libphonenumber\PhoneNumberUtil::getInstance();

$min_digits = 100; $max_digits = -100;
switch ($country) {
	case 'sk':
		$min_digits = 9; $max_digits = 9; break;
	case 'international':
		$min_digits = 9; $max_digits = 15; break;
	case 'us':
	case 'ca':
	case 'uk':
	default:
		$min_digits = 10; $max_digits = 10; break;
}


if ($country == "international" && substr($phone, 0, 1) != "+") {
	die("Error: Wrong phone number - it is not in international format with leading +");
}

$digits = strlen($phone);
if ($digits < $min_digits || $digits > $max_digits) {
	die("Error: Wrong phone number. Please enter correct phone number.");
}

if ($country == "international")
	$full_phone = $phone;
else
	$full_phone = get_country_prefix($country).$phone;

//this check prevents sending 2 different codes to the same phone number
if ($_SESSION["sms_verify_phone"] == $full_phone && intval($_SESSION["sms_verify_try_cnt"]) > 0) {
	debug_log("SMS verification: Trying to send code to the same phone number '{$full_phone}', not doing anything...");
	die("OK");
}

//look up, if phone number was not used already to verify different account
$res = $db->q("SELECT count(*) as cnt FROM account WHERE phone_verified = 1 AND phone = ?", array($full_phone));
if ($db->numrows($res) != 1) {
	debug_log("SMS verification: Error - failed to get number of accounts with the same phone.");
	die("Error: Internal error, please contact administrator.");
}
$row = $db->r($res);
$cnt = intval($row["cnt"]);
if ($cnt > 0) {
	debug_log("SMS verification: Error - phone number '{$full_phone}' already used to verify {$cnt} accounts!");
	die("Error: This phone number was already used to verify different account. Please use only one account on our website.");
}

//this check id to ensure we have only 2 tries to generate & send sms code for one classified ad
if (!isset($_SESSION["sms_verify_try_adid"]) || $_SESSION["sms_verify_try_adid"] != $ad_id) {
	$_SESSION["sms_verify_try_adid"] = $ad_id;
	$_SESSION["sms_verify_try_cnt"] = 1;
} else {
	$_SESSION["sms_verify_try_cnt"] = intval($_SESSION["sms_verify_try_cnt"]) + 1;
}
if ($_SESSION["sms_verify_try_cnt"] > 2) {
	//this must be hack, were trying to generate new verify code more times than allowed limit
	die("Error: You can't verify this ad by phone anymore. Please create new ad.");
}


//ok we entered valid phone number, lets generate verufy code and send it via SMS
$code = get_sms_verification_code();
$message = "AS Verification code: {$code}";
debug_log("SMS verification: Sending code = '{$code}' to phone '{$full_phone}' for ad '{$ad_id}'");
$sms = new swiftsms();
if (!$config_dev_server && !$sms->send($full_phone, $message)) {
	//on dev dont send actual sms message
	debug_log("SMS verification: Error sending sms through swift sms gateway !!!");
	die("Error: Unable to send SMS message. Please contact administrator.");
}
// SMS successfully sent
$_SESSION["sms_verify_phone"] = $full_phone;
$_SESSION["sms_verify_code"] = $code;
$_SESSION["sms_verify_cnt"] = 0;

if ($_SESSION["sms_verify_try_cnt"] == 2)
	die("OKlast");
else
	die("OK");

?>
