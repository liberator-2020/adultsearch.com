<?php
/* *
 * ajax call is used to bring list of states for selected country on /mng/locations "Add location" page
 */
global $db;

$country_id = intval($_REQUEST["country_id"]);
if ($country_id == 0)
	die;

$res = $db->q("SELECT loc_id, loc_name, s FROM location_location WHERE loc_type = 2 AND loc_parent = ?", array($country_id));
if ($db->numrows($res) < 1)
	die;

echo "<option value=\"\">-- Select --</option>\n";
while($row = $db->r($res)) {
	echo "<option value=\"{$row["loc_id"]}\">{$row["loc_name"]} ({$row["s"]})</option>\n";
}

die;

?>
