<?php
/**
 * Advertise zone search for advertise admin panel
 * Used in _cms_files/advertise/deal.php
 */

global $account, $db;

if (!$account->isadmin())
	return;

$term = $_REQUEST["term"];
if (is_numeric($term))
	$res = $db->q("SELECT * FROM advertise_section WHERE status = 1 AND ( type = 'B' OR type = 'P' OR type = 'A' OR type = 'L') AND id LIKE ? LIMIT 10", array($term));
else
	$res = $db->q("SELECT * FROM advertise_section WHERE status = 1 AND ( type = 'B' OR type = 'P' OR type = 'A' OR type = 'L') AND ( name LIKE ? or nickname LIKE ? ) LIMIT 10", array("%$term%", "%$term%"));

$response_array = array();
while ($row = $db->r($res)) {
	if ($row["nickname"])
		$label = "#{$row["id"]} - {$row["nickname"]} ({$row["name"]})";
	else
		$label = "#{$row["id"]} - {$row["name"]}";
	$response_array[] = array(
		"id" => $row["id"],
		"label" => $label,
		"type" => $row["type"],
	);
}

header('Content-Type: application/json');
echo json_encode($response_array);
die;
?>
