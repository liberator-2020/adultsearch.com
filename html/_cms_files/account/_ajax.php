<?php
/**
 * Used as ajax call from /account/signup page to check if email and/or username are already in use
 */

global $account;

$u = GetGetParam("u");
$e = GetGetParam("e");

if (!empty($u)) {
	if ($account->username_taken($u))
		echo "This username is in use.";
} elseif (!empty($e)) {
	if ($account->email_inuse($e))
		echo "This email is in use.";
}

die;

?>
