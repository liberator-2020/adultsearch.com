<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $location, $config_image_path, $mobile;

if (!account::ensure_loggedin())
	die("Invalid access");

function process_delete() {
	global $smarty, $account, $db, $config_image_path;

	if (!$account->getAvatar()) {
		$smarty->assign("error", "You don't have avatar set");
		return false;
	}

	$avatar_filepath = $config_image_path."/avatar/".$account->getAvatar();
	if (file_exists($avatar_filepath)) {
		@unlink($avatar_filepath);
	}

	$account->setAvatar(null);
	$account->update();

	return true;
}

function process_submit() {
	global $smarty, $account, $db, $config_image_path;

	$handle = new upload($_FILES["avatar"]);
	if (!$handle->uploaded) {
		$smarty->assign("error", "Avatar file not uploaded");
		return false;
	}

	//if we already have avatar set up, delete it
	if ($account->getAvatar())
		process_delete();

	$handle->allowed = array("image/x-png", "image/x-tiff", "image/x-windows-bmp", "image/bmp", "image/gif", "image/jpeg", "image/pjpeg", "image/png", "image/x-png");
	$upload_dir = $config_image_path."/avatar/";
	$handle->file_new_name_body = time() ."_" . $account->getId();
	list($width, $height, $type, $attr) = getimagesize($handle->file_src_pathname);
	if ($width > 100) {
		$handle->image_convert = "jpg";
		$handle->image_resize = true;
		$handle->image_ratio_crop = "T";
		$handle->image_x = 100;
		$handle->image_y = 100;
	}
	$handle->process($upload_dir);
	if (!$handle->processed) {
		$smarty->assign("error", "Error processing avatar file");
		return false;
	}

	$db->q("UPDATE account SET avatar = ? WHERE account_id = ?", [$handle->file_dst_name, $account->getId()]);
	$handle->clean();

	return true;
}

if (isset($_POST["delavatar"])) {
	$ret = process_delete();
	if ($ret)
		return success_redirect("Your avatar has been deleted.", "/account/mypage");
}

if (isset($_FILES["avatar"])) {
	$ret = process_submit();
	if ($ret)
		return success_redirect("Your avatar has been changed.", "/account/mypage");
}

$smarty->assign("avatar", htmlspecialchars($account->getAvatar()));

escortsbiz::exportEbizLink();

$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/account/change_avatar.tpl");

?>
