<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $location, $config_image_path, $mobile;

if (!account::ensure_loggedin())
    die("Invalid access");

function process_submit() {
	global $smarty, $account, $db;

	$email = filter_var($_REQUEST["email"], FILTER_VALIDATE_EMAIL);

	//checks
	if (!$email) {
		$smarty->assign("error", "You need to enter new email, email needs to be in valid format");
		return false;
	}
    if ($account->email_inuse($email, true)) {
		$smarty->assign("error", "This email address is already in use. If you forgot your password, please <a href=\"/account/reset\">reset your password here</a>");
		return false;
	}
	if ($account->isBannedForLogin($email)) {
		$smarty->assign("error", "You may not use email address '".htmlspecialchars($email)."' on our site.");
		return false;
	}

	//update
	$res = $db->q("UPDATE account SET email = ? WHERE account_id = ? LIMIT 1", [$email, $account->getId()]);
	if ($db->numrows($res) != 1) {
		$smarty->assign("error", "Error updating your email, please contact our support");
		return false;
	}

	return true;
}

if (isset($_POST["email"])) {
	$ret = process_submit();
	if ($ret) {
		return success_redirect("Your email has been changed.", "/account/mypage");
	}
}

$smarty->assign("currentEmail", htmlspecialchars($account->getEmail()));

$smarty->assign("email", htmlspecialchars($_REQUEST["email"]));

escortsbiz::exportEbizLink();

$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/account/change_email.tpl");

?>
