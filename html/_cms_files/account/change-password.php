<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $location, $config_image_path, $mobile;

if (!account::ensure_loggedin())
    die("Invalid access");

function process_submit() {
	global $smarty, $account, $db;

	$p1 = $_REQUEST["p1"];
	$p2 = $_REQUEST["p2"];
	$p3 = $_REQUEST["p3"];

	//checks
	if (!$p1 || !$p2 || !$p3) {
		$smarty->assign("error", "You need to enter your current password and then twice our new password.");
		return false;
	}
	if (!password_verify($p1, $account->getPassword())) {
		$smarty->assign("error", "You typed your current password wrong!");
		return false;
	}
	if (strcmp($p2, $p3)) {
		$smarty->assign("error", "You typed your new password and password repeat different");
		return false;
	}

	//update
	$passwordEncrypted = password_hash($p2, PASSWORD_BCRYPT);
	$res = $db->q("UPDATE account SET password = ? WHERE account_id = ? LIMIT 1", [$passwordEncrypted, $account->getId()]);
	if ($db->numrows($res) != 1) {
		$smarty->assign("error", "Error updating your password, please contact our support");
		return false;
	}

	return true;
}

if (isset($_POST["p2"])) {
	$ret = process_submit();
	if ($ret) {
		return success_redirect("Your password has been changed.", "/account/mypage");
	}
}

escortsbiz::exportEbizLink();

$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/account/change_password.tpl");

?>
