<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $location, $config_image_path, $mobile;

if (!account::ensure_loggedin())
    die("Invalid access");

function process_submit() {
	global $smarty, $account, $db;

	$newcomment = ($_REQUEST["newcomment"] == "1") ? 1 : 0;
	$newcommentme = ($_REQUEST["newcommentme"] == "1") ? 1 : 0;

	//update
	$db->q("INSERT INTO account_opt (account_id, x, opt) VALUES (?, 'newcomment', ?)
			ON DUPLICATE KEY UPDATE opt = ?",
			[$account->getId(), $newcomment, $newcomment]
			);
	$db->q("INSERT INTO account_opt (account_id, x, opt) VALUES (?, 'newcommentme', ?)
			ON DUPLICATE KEY UPDATE opt = ?",
			[$account->getId(), $newcommentme, $newcommentme]
			);

	return true;
}

if (isset($_POST["submit"])) {
	$ret = process_submit();
	if ($ret) {
		return success_redirect("Your settings has been changed.", "/account/mypage");
	}
}

$opt = $account->accountOpt();
$smarty->assign("newcomment_checked", $opt["newcomment"] !== 0);
$smarty->assign("newcommentme_checked", $opt["newcommentme"] !== 0);

escortsbiz::exportEbizLink();

$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/account/change_settings.tpl");

?>
