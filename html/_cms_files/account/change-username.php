<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $location, $config_image_path, $mobile;

if (!account::ensure_loggedin())
    die("Invalid access");

function process_submit() {
	global $smarty, $account, $db;

	$username = preg_replace('/[^a-zA-Z0-9:\-\_\.]/', '', $_REQUEST["username"]);

	//checks
	if (!$username) {
		$smarty->assign("error", "You need to enter new username, please use only alphabetical characters in your username");
		return false;
	}
	if ($account->username_taken($username)) {
		$smarty->assign("error", "This username is already taken");
		return false;
	}

	//update
	$res = $db->q("UPDATE account SET username = ? WHERE account_id = ? LIMIT 1", [$username, $account->getId()]);
	if ($db->numrows($res) != 1) {
		$smarty->assign("error", "Error updating your username, please contact our support");
		return false;
	}

	return true;
}

if (isset($_POST["username"])) {
	$ret = process_submit();
	if ($ret) {
		return success_redirect("Your username has been changed.", "/account/mypage");
	}
}

$smarty->assign("currentUsername", htmlspecialchars($account->getUsername()));

$smarty->assign("username", htmlspecialchars($_REQUEST["username"]));

escortsbiz::exportEbizLink();

$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/account/change_username.tpl");

?>
