<?php
/*
 * Closing account controller
 */

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

$account_to_close = null;

//if we specify account_id in request param, we can close other account that ourselves, but this is only available to admins
$account_to_close_id = intval($_REQUEST["account_id"]);
if ($account_to_close_id) {
	if (!$account->isadmin())
		return error_redirect("You dont have permissions for this operation", "/");
	if ($account->getId() == $account_to_close_id)
		return error_redirect("You dont have permissions for this operation", "/");
	$account_to_close = account::findOneById($account_to_close_id);
	if (!$account_to_close)
		return error_redirect("Invalid account #", "/");
} else {
	$account_to_close = $account;
}

if ($account_to_close->getDeleted())
	return error_redirect("Account already closed", "/");

//$smarty->assign("error", "This account is not available");
//return $smarty->display(_CMS_ABS_PATH."/templates/account/close.tpl");

if ($_REQUEST["submit"] == "Close") {
	//close account
}

$res = $db->q("SELECT c.id, c.thumb, c.expires, l.loc_name, l.s
	FROM classifieds c 
	INNER JOIN classifieds_loc cl on cl.post_id = c.id
	INNER JOIN location_location l on l.loc_id = cl.loc_id
	WHERE c.account_id = ? AND c.done > 0
	GROUP BY c.id
	",
	[$account_to_close->getId()]
	);
$ads = [];
while ($row = $db->r($res)) {
	$ads[] = [
		"id" => $row["id"],
		"thumb" => $row["thumb"],
		"location" => $row["loc_name"].", ",$row["s"],
		"expires" => $row["expires"],
		];
}

$smarty->assign("ads", !isset($show_password_form)?true:false);
if ($account_to_close_id)
	$smarty->assign("account_id", $account_to_close_id);

$smarty->display(_CMS_ABS_PATH."/templates/account/close.tpl");

?>
