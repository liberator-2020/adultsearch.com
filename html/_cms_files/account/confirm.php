<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

$account_id = intval($_REQUEST["i"]);

if (isset($_GET["h"]) && isset($_GET["id"])) {
	$h = GetGetParam("h");
	$account_id = intval($_REQUEST["id"]);
	if(empty($h) || empty($account_id)) {
		echo "There is no user information for this link or email is already confirmed!";
		return;
	}

	file_log("account", "confirm: trying to confirm. id='{$id}', h='{$h}'");

	//----------
	//MAIN CHECK
	$res = $db->q("SELECT * FROM account_confirms WHERE account_id = ? and hash = ? LIMIT 1", array($account_id, $h));
	if (!$db->numrows($res)) {
		file_log("account", "confirm: cant find this unconfirmed account, perhaps hack or account already confirmed");
		echo "There is no user information for this link or email is already comfirmed!";
		return;
	}

	$db->q("UPDATE account SET email_confirmed = 1 WHERE account_id = ?", array($account_id));
	$db->q("DELETE FROM account_confirms WHERE account_id = ?", array($account_id));

	file_log("account", "confirm: account #{$id} confirmed successfully");

	//load user
	$user = account::findOneById($account_id);
	if (!$user) {
		echo "Error in system, cant find user with this id!";die;
	}

	//ban user with this password
	$ban = false;
	if (stristr($user->getPassword(), "cyberman") !== false) {
		if ($user->ban()) {
			audit::log("ACC", "Ban", $user->getId(), "Due to password '{$user->getPassword()}'", "S-account/confirm");
			$ban = true;
		}
	}

	//autologin user
	if (!$ban)
		$account->login($user->getEmail(), NULL, $error, 1);

	//redirect
	if ($user->isAdvertiser()) {
		system::go("/advertise/?account_confirmed", "Thank you for confirming your email address!");
	} else {

		//check if we by any chance did not start posting a classified ad, if yes then redirect to step 2
		$clad_id = classifieds::get_cl_owner();
		if ($clad_id)
			system::go("/adbuild/step2?ad_id={$clad_id}");

		file_log("account", "confirm: redirect to /?account_confirmed");
		system::go("/?account_confirmed", "Thank you for confirming your email address!");
	}
}

if (!$account_id)
	system::moved("/");

$res = $db->q("SELECT * FROM account WHERE account_id = ?", array($account_id));
if( !$db->numrows($res)  ) {
	echo "account could not found!";
	return;
}
$row = $db->r($res);

if ($row["email_confirmed"] == 1)
	system::go("/");

$smarty->assign("email", $row["email"]);

if( isset($_POST["captcha_string"]) && !empty($_POST["captcha_string"]) ) {
	if( !strcmp(strtolower($_POST["captcha_string"]), strtolower($_SESSION["captcha_str"])) ) {
		$account->sendConfirmation($account_id);
		$smarty->display(_CMS_ABS_PATH."/templates/account/account_infoconfirm.tpl");
		return;
	} else
		$smarty->assign("error", "You typed the verification code wrong");
} else if( isset($_POST["captcha_string"]) ) $smarty->assign("error", "You did not type the verification code");

$smarty->assign("time", time());
$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/account/account_confirm.tpl");

?>
