<?php

defined('_CMS_FRONTEND') or die('Restricted access');

if (!permission::access("account_impersonate"))
    die("Invalid access");

//account id to impersonate as
$id = intval($_REQUEST["id"]);
if (!$id)
	die("Missing param 'id'");

//url to go to after successful impersonation
$go = urldecode($_REQUEST["go"]);

$error = null;
$ret = account::impersonate($id, $error);
if (!$ret) {
	echo "Failed to impersonate: '{$error}' !<br />";
	die;
}

//impersonated successfully
//if go url is not specified go to my classifed ads page
if (!$go)
	$go = "/account/mypage";

system::go($go);
die;
