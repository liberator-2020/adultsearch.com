<?php

defined('_CMS_FRONTEND') or die('Restricted access');

if (!account::ensure_loggedin())
	die("Invalid access");

if (!account::is_impersonated())
	error_redirect("We are not impersonated");

$error = null;
$ret = account::impersonate_return($error);
if (!$ret) {
	echo "Failed to return to admin: '{$error}' !<br />";
	die;
}

//returned to admin successfully
system::go("/");

die;
