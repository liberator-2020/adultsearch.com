<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

if (!account::ensure_loggedin())
    die("Invalid access");

$res = $db->q("SELECT a.*, COUNT(distinct fp.post_id) as forum_post 
				FROM account a 
				LEFT JOIN forum_post fp on fp.account_id = a.account_id 
				WHERE a.account_id = ?
				GROUP BY a.account_id", [$account->getId()]);
if (!$db->numrows($res)) {
	echo "Error. Please Try Again.";
	return;
}
$rowme = $db->r($res);

$smarty->assign("account_id", $account->getId());
$smarty->assign("username", $rowme["username"]);
$smarty->assign("email", $rowme["email"]);
$smarty->assign("avatar", $rowme["avatar"]);
$smarty->assign("forum_post", intval($rowme["forum_post"]));

$res = $db->q("SELECT l.loc_name, lp.loc_name as lp_loc_name, c.done, c.id
	FROM classifieds c
	INNER JOIN classifieds_loc cl on cl.post_id = c.id
	INNER JOIN location_location l on l.loc_id = cl.loc_id
	INNER JOIN location_location lp on lp.loc_id = l.loc_parent
	WHERE c.account_id = ? AND c.deleted IS NULL
	ORDER BY c.done DESC, c.id ASC
	LIMIT 1",
	[$account->getId()]
	);
if ($db->numrows($res)) {
	$row = $db->r($res);
	$location = $row["loc_name"].", ".$row["lp_loc_name"];
	$smarty->assign("location", $location);
	//if ad is live (done=1), export classified id, so we can display change location button
	if ($row["done"] == 1)
		$smarty->assign("classified_id", $row["id"]);
}

$review_cnt = intval($db->single("SELECT count(*) as review_cnt FROM place_review WHERE account_id = ?", [$account->getId()]));
$smarty->assign("review_cnt", $review_cnt);

$opt = $account->accountOpt();

$smarty->assign("newcomment_checked", $opt["newcomment"] !== 0);
$smarty->assign("newcommentme_checked", $opt["newcommentme"] !== 0);

//we dont display credit cards anymore, since its no use to edit/delete them, we dont keep credit card details anymore since ccbill/payline

escortsbiz::exportEbizLink();

$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/account/account.tpl");

?>
