<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $smarty, $account;

if( !($account_id=$account->isloggedin()) ) {
	$account->asklogin();
	return;
}

escortsbiz::exportEbizLink();

$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/account/mywebsite.tpl");

?>
