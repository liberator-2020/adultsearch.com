<?

defined('_CMS_FRONTEND') or die('Restricted access');

require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");

global $db, $smarty, $account, $location;
$form = new form;

if( !($account_id=$account->isloggedin()) ) {
        $account->asklogin();
        return;
}

if( isset($_POST["Save"]) ) {
	$newcomment = isset($_POST["newcomment"])?1:0;
	$newcommentme = isset($_POST["newcommentme"])?1:0;

	$db->q("insert into account_opt (account_id, x, opt) values ('$account_id', 'newcomment', '$newcomment') on duplicate key update opt = '$newcomment'");
	$db->q("insert into account_opt (account_id, x, opt) values ('$account_id', 'newcommentme', '$newcommentme') on duplicate key update opt = '$newcommentme'");

	$smarty->assign("done", true);
}


$opt = $account->accountOpt($account_id);

$newcomment = $form->GetCheckBox("newcomment", $opt["newcomment"] !== 0 ? true : false, NULL);
$newcommentme = $form->GetCheckBox("newcommentme", $opt["newcommentme"] !== 0 ? true : false, NULL);

$smarty->assign("newcomment", $newcomment);
$smarty->assign("newcommentme", $newcommentme);
$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/account/account_notifications.tpl");


?>
