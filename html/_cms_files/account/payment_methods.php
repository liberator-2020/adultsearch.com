<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $mobile, $config_stripe_public_key;
$smarty->assign("nobanner", true);

if (!account::ensure_loggedin())
    die("Invalid access");


if ($_REQUEST["go_back"] == "1")
	system::go("/account/payment_methods");

$error = '';

// delete payment method
$ccid = intval($_REQUEST["delete_ccid"]);
if ($ccid) {
	$cre = creditcard::findOneById($ccid);
	if ($cre && !$cre->getDeletedStamp() && $account->getId() == $cre->getAccountId()){
		$cre->remove();
	}
}

if (!empty($_REQUEST['submitted'])) {
	$stripe = new stripe();
	$stripe->change_card($error);
}

$cards = creditcard::get_cards_active_subscription();
if (empty($cards)) {
	$error = 'No credit cards stored in your account';
}

$smarty->assign('cards', $cards);
$smarty->assign('account', $account);
$smarty->assign('stripe_public_key', $config_stripe_public_key);
$smarty->assign('error', $error);
return $smarty->display(_CMS_ABS_PATH."/templates/account/payment_methods.tpl");

?>
