<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $mobile;

if (!account::ensure_loggedin())
    die("Invalid access");


if ($_REQUEST["go_back"] == "1")
	system::go("/account/payments");


if ($_REQUEST["action"] == "subscription_cancel") {
	$payment_id = preg_replace('/[^0-9]/', '', $_REQUEST["payment_id"]);
	if (!$payment_id)
		return error_redirect("Invalid recurring payment #id !", "/account/payments");

	//fetch subscription from db
	$res = $db->q("
		SELECT p.*
		FROM payment p
		WHERE p.id = ?
		",
		[$payment_id]
		);
	if (!$db->numrows($res))
		return error_redirect("Subscription #{$payment_id} not found !", "/account/payments");
	$row = $db->r($res);
	$subscription_id = $row["subscription_id"];
	if (!$subscription_id)
		$subscription_id = $payment_id;

	//check if this subscription is ours
	if ($row["account_id"] != $account->getId())
		return error_redirect("Invalid access to subscription #{$payment_id} !", "/account/payments");

	//check if the subscription is active
	if ($row["subscription_status"] != "1")
		return error_redirect("Subscription #{$payment_id} is not active anymore !", "/account/payments");

	if ($_REQUEST["go_live"] != "1") {
		//not going live yet, show confirmation page
		$smarty->assign("payment_id", $payment_id);
		$smarty->assign("subscription_id", $subscription_id);

		//fetch subject of subscription being canceleed
		$clad_id = $message = null;
		$res = $db->q("
			SELECT p.id, p.next_renewal_date, p.subscription_status
				, pi.type as pi_type, pi.classified_id as pi_classified_id, pi.sponsor as pi_sponsor, pi.side as pi_side, pi.auto_renew as pi_auto_renew
			FROM payment p
			LEFT JOIN payment_item pi on pi.payment_id = p.id
			WHERE p.id = ?
			GROUP BY p.id",
			[$payment_id]
			);
		if (!$db->numrows($res))
			return error_redirect("Error fetching subscription #{$payment_id} details !", "/account/payments");
		$row = $db->r($res);
		if ($row["pi_type"]) {
			if ($row["pi_type"] == "classified")
				$clad_id = intval($row["pi_classified_id"]);
		}
		if ($clad_id) {
			$clad = clad::findOneById($clad_id);
			if (!$clad)
				return error_redirect("Error fetching classified ad #{$clad_id} details !", "/account/payments");
			$stamp = $clad->getExpireStamp();
			if (!$stamp)
				$stamp = strtotime($clad->getExpires());
			$message = "If you cancel this subscription, classified ad will expire on ".date("m/d/Y", $stamp).".";
			$smarty->assign("message", $message);
		}
		return $smarty->display(_CMS_ABS_PATH."/templates/account/subscription_cancel.tpl");
	}

	//going live!!!
	$error = null;
	$ret = payment::cancel($payment_id, "client_cancel", $error);

	if (!$ret) {
		reportAdmin("AS: client subscription cancel failed check", "please check why subscription cancellation failed, payment_id={$payment_id}, error='{$error}'");
		return error_redirect("Recurring payment #{$payment_id} cancellation failed, please contact us (support@adultsearch.com)", "/account/payments");
	}

	return success_redirect("Recurring payment #{$payment_id} was cancelled successfully", "/account/payments");
}


$res = $db->q("
	SELECT t.*
		, p.next_renewal_date, p.subscription_status
		, cc.firstname, cc.lastname, cc.cc, cc.type
		, pi.type as pi_type, pi.classified_id as pi_classified_id, pi.sponsor as pi_sponsor, pi.side as pi_side, pi.sticky as pi_sticky, pi.auto_renew as pi_auto_renew
		, pi.place_section, pi.place_id, pi.place_sponsor
	FROM transaction t
	INNER JOIN payment p on p.id = t.payment_id
	LEFT JOIN account_cc cc on cc.cc_id = p.cc_id
	LEFT JOIN payment_item pi on pi.payment_id = p.id
	WHERE p.account_id = ?
	GROUP BY t.id
	ORDER BY t.stamp DESC",
	[$account->getId()]
	);

$transactions = [];
$subscriptions = [];
while ($row = $db->r($res)) {
	$payment_id = $row["payment_id"];

	$subject = null;
	if ($row["pi_type"]) {
		if ($row["pi_type"] == "classified"){
			$subject = "Classified ad #".intval($row["pi_classified_id"]);
			if($row['pi_sticky'])
				$subject = "Sticky upgrade on ad #" . intval($row["pi_classified_id"]);
			if($row['pi_side'])
				$subject = "Side sponsor on ad #" . intval($row["pi_classified_id"]);
			if($row['pi_sponsor'])
				$subject = "City thumbnail on ad #" . intval($row["pi_classified_id"]);
		}
		elseif ($row["pi_type"] == "repost"){
			$subject = "Global repost credits: ".intval($row["pi_auto_renew"]);
		}
		elseif ($row["pi_type"] == "advertise"){
			$subject = "Advertising balance";
		}
		elseif ($row["pi_type"] == "businessowner"){
			$subject = businessowner::get_html_description($row);
		}
	}

	$card_label = null;
	if ($row["type"])
		$card_label .= creditcard::getTypeName($row["type"])." ";
	if ($row["cc"])
		$card_label .= substr($row["cc"], -5)." ";
	if ($row["firstname"])
		$card_label .= $row["firstname"]." ";
	if ($row["lastname"])
		$card_label .= $row["lastname"];

	$transaction = [
		"id" => $row["id"],
		"stamp" => $row["stamp"],
		"trans_id" => $row["trans_id"],
		"payment_id" => $row["payment_id"],
		"amount" => $row["amount"],
		"payment_id" => $row["payment_id"],
		"next_renewal_date" => $row["next_renewal_date"],
		"card_label" => $card_label,
		"subject" => $subject,
		];
	$transactions[] = $transaction;

	if (!array_key_exists($payment_id, $subscriptions) && $row["subscription_status"] == 1) {
		$subscription = [
			"payment_id" => $row["payment_id"],
			"amount" => $row["amount"],
			"next_renewal_date" => $row["next_renewal_date"],
			"card_label" => $card_label,
			"subject" => $subject,
			];
		$subscriptions[$payment_id] = $subscription;
	}
}

// check if account has stored credit cards
$cards = creditcard::get_cards();

escortsbiz::exportEbizLink();
$smarty->assign("nobanner", true);
$smarty->assign("cards", $cards);
$smarty->assign("transactions", $transactions);
$smarty->assign("subscriptions", $subscriptions);
$smarty->display(_CMS_ABS_PATH."/templates/account/payments.tpl");

?>
