<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

$system = new system;
$account_id = GetRequestParam("i");

if( isset($_POST["code"]) ) {
	$hash = GetPostParam("code");
	$account_id = GetPostParam("account_id");
	$password = GetPostParam("password");
	$password2 = GetPostParam("password2");
	if (empty($password) || empty($password2))
		$smarty->assign("error2", "You need to type both passwords");
	else if (strcmp($password, $password2))
		$smarty->assign("error2", "Password doesnt match");
	else if (strlen($password) > 30)
		$smarty->assign("error2", "Maximum length of the password is 30 characters");
	else {
		$res = $db->q("select account_id, email from account_confirms c inner join account using (account_id) where c.account_id = '$account_id' and hash = '$hash'");
		if( !$db->numrows($res) ) {
			$smarty->assign("error2", "Confirmation code could not find!");	
		} else {
			$row = $db->r($res);
			$password_encoded = password_hash($password, PASSWORD_BCRYPT);
	        if (!$password_encoded) {
    	        debug_log("account::reset Error: password_hash failed !");
				$smarty->assign("error2", "System error");
			} else {
				$db->q("UPDATE account SET password = ? WHERE account_id = ? LIMIT 1", [$password_encoded, $account_id]);
				$db->q("DELETE FROM account_confirms WHERE account_id = ? AND hash = ?", [$account_id, $hash]);
				$account->login($row["email"], "", $e, 1);
				$system->go("/", "You password has been updated.");
        	}	
		}
	}
}
if( isset($_POST["email"]) ) {
	$email = GetPostParam("email");
	$captcha_string = GetPostParam("captcha_string");
	if( empty($email) || empty($captcha_string) ) {
		$smarty->assign("error", "Please fill the form.");
	} else {
		if( strcmp(strtolower($_POST["captcha_string"]), strtolower($_SESSION["captcha_str"])) ) {
			$smarty->assign("error", "Please type the image code correctly");
		} else {
			$res = $db->q("select account_id, password, email_confirmed from account where email = ?", [$email]);
			if( !$db->numrows($res) ) {
				$smarty->assign("error", "There is no membership with this email address.");
			} else {
				$row = $db->r($res);
				if( $row["email_confirmed"] == 0 ) $system->moved("/account/confirm?i=".$row["account_id"]);
				$account_id = $row["account_id"];
				$hash = $account->_getCookieHash();
				$db->q("insert into account_confirms (account_id, hash, create_time) values ('$account_id', '$hash', curdate())");
				$mail_content = "Dear Adult Search User,<br><br>To reset your password, go <a href='https://adultsearch.com/account/reset?code={$hash}&id={$row["account_id"]}'>https://adultsearch.com/account/reset?code={$hash}&id={$row["account_id"]}</a><br />If the link above is not clickable, copy and past it to your browser.<br><br>If you DID NOT request to change your password, you can simple ignore this email<br>Best Regards,<br>adultsearch.com";
				sendEmail(SUPPORT_EMAIL, "Adult Search - Password Reset", $mail_content, $email);
				$smarty->assign("success", "Password reset e-mail has sent to your email address. Please check your email box. In some cases, email might be found in Spam/Junk email folders.");
				$show_password_form = true;
			}
		}
	}
	$smarty->assign("email", $email);
} elseif( isset($_GET["code"]) ) {
	$code = preg_replace('/[^a-zA-Z0-9]/', '', $_GET["code"]);
	$id = intval($_GET['id']);

	$res = $db->q("select * from account_confirms where account_id = ? and hash = ? limit 1", [$id, $code]);
	if (!$db->numrows($res)) {
		$smarty->assign("error", "Confirmation could not find.");
	} else {
		$show_password_form = true;
		$smarty->assign("code", $code);
		$smarty->assign("account_id", $id);
		$smarty->assign("show_password_form", true);
	}
}

$smarty->assign("show_form", !isset($show_password_form)?true:false);
$smarty->assign("time", time());
$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/account/account_reset.tpl");

?>
