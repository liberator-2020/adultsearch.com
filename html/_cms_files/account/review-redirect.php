<?php

global $db;

$review_id = intval($_GET["review_id"]);
if (!$review_id)
	system::moved('/');

$res = $db->q("select * from place_review where review_id = ?", array($review_id));
if (!$db->numrows($res))
	system::moved('/');
$row = $db->r($res);
$id = $row["id"];

$url = dir::getPlaceLinkById($id);

if (!$url)
	system::moved('/');

system::moved($url."#review{$review_id}");

?>
