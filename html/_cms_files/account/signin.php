<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;
$system = new system;

if (isset($_GET["redirect"]) && isset($_GET["nowait"])) {
	$redirect .= strstr($_GET["redirect"], "?") ? "&ref=signin" : "?ref=signin";
	$system->moved($_GET["redirect"].$redirect);
}

if ($account->isloggedin())
	$system->moved("/");

if (isset($_GET['origin']))
	$smarty->assign('origin', $_GET['origin']);

if (array_key_exists("message", $_REQUEST) && $_REQUEST["message"]) {
	$message_full = $message = $_REQUEST["message"];
	$message_full .= "<br />If you do not have an account, you can <a href=\"/account/signup\">register for free</a>.";
	$smarty->assign("message", $message);
	$smarty->assign("message_full", $message_full);
}

$smarty->assign("csrf", csrf::get_html_code("login"));
$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/account/account_signin.tpl");

?>
