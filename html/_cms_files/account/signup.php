<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $config_recaptcha_secret_key;

$gTitle = "Sign up on adultsearch.com";

if ($account->isloggedin())
	system::moved("/account/mypage");

$smarty->assign("clean_header", true);

function process_submission() {
	global $db, $smarty, $account, $config_recaptcha_secret_key, $config_domain;

	$email = preg_replace('/[^0-9A-Za-z_\-.@]/', '', $_REQUEST["email"]);
	$email = trim($email);
	$email_conf = preg_replace('/[^0-9A-Za-z_\-.@]/', '', $_REQUEST["email_conf"]);
	$email_conf = trim($email_conf);
	$username = preg_replace('/[^0-9A-Za-z_\-.@]/', '', $_REQUEST["username"]);
	$password = preg_replace('/[^0-9A-Za-z_\-.@$#:]/', '', $_REQUEST["password"]);
	$password_conf = preg_replace('/[^0-9A-Za-z_\-.@$#:]/', '', $_REQUEST["password_conf"]);

	//input fields validation
	if (empty($email)) {
		$smarty->assign("error", "You need to type your e-mail address.");
		return false;
	} 
	if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
		$smarty->assign("error", "Wrong e-mail address");
		return false;
	}
	if (strstr($username, ' ')) {
		$smarty->assign("error", "You may not have space in your user name");
		return false;
	}
	if (strcmp($email, $email_conf)) {
		$smarty->assign("error", "E-mail confirmation typed different than your e-mail");
		return false;
	}
	if ($account->email_inuse($email)) {
		$smarty->assign("error", "This email address is already in use.  If you forgot your password, please <a href=\"/account/reset\">click here</a> to get a reminder.");
		return false;
	}
	if (empty($username)) {
		$smarty->assign("error", "Please type a user name");
		return false;
	}
	if ($account->username_taken($username)) {
		$smarty->assign("error", "This user name($username) is already taken, please try a different one.");
		return false;
	}
	if (empty($password)) {
		$smarty->assign("error", "Please type a password");
		return false;
	}
	if (strcmp($password, $password_conf)) {
		$smarty->assign("error", "Passwords are not match.");
		return false;
	}

	/*
	//old captcha
	//$spam = preg_replace('/[^0-9A-Za-z]/', '', $_REQUEST["spam"]);
	if( !empty($spam) && !strcmp(strtolower($spam),strtolower($_SESSION["captcha_str"])) ) {
		$smarty->assign("captcha_ok", true); $smarty->assign("spam", $spam);
	}
	if (empty($spam) ) $smarty->assign("error", "Word Verification wasnt typed.");
	else if( strcmp(strtolower($spam),strtolower($_SESSION["captcha_str"])) ) $smarty->assign("error", "Word Verification typed wrong");
	*/

	//captcha validation
	$gRecaptchaResponse = $_REQUEST["g-recaptcha-response"];
	try {
		$recaptcha = new \ReCaptcha\ReCaptcha($config_recaptcha_secret_key);
		$resp = $recaptcha->setExpectedHostname($config_domain)
					->verify($gRecaptchaResponse, $_SERVER["REMOTE_ADDR"]);
	} catch (\Exception $e) {
		debug_log("account/signup: failed recaptcha: exception: {$e}");
		$smarty->assign("error", "Failed verification");
		return false;
	}
	if (!$resp->isSuccess()) {
	    $errors = $resp->getErrorCodes();
		debug_log("account/signup: failed recaptcha: ".implode(", ", $errors));
		$smarty->assign("error", "Failed verification");
		return false;
	}

	//spamcheck 1
	if (in_array(account::getUserIp(), array("68.231.101.181", "99.112.104.56"))) {
		$smarty->assign("error", "Passwords are not match.");
		return false;
	}
	//spamcheck 2
	if ($password == "x4ivygA51F") {
		$smarty->assign("error", "Invalid data."); 
		return false;
	}

	if ($account->isBannedForLogin($email)) {
		$smarty->assign("error", "You may not register on our site.");
		debug_log("Account: User banned for register: email={$email}, username={$username}, password={$password}");
		return false;
	}

	//everything successful
	$account_id = $account->register($email, $username, $password, 0, $_REQUEST["advertiser"] == 1);
	$smarty->assign("email", htmlspecialchars($email, ENT_QUOTES, $email));
	$smarty->assign("username", htmlspecialchars($email, ENT_QUOTES, $username));
	$smarty->assign("nobanner", true);
	$smarty->display(_CMS_ABS_PATH."/templates/account/account_infoconfirm.tpl");
	//unset($_SESSION["captcha_str"]);
	return true;
}

if (isset($_POST["submit"])) {
	if (process_submission())
		return;
}

$smarty->assign("email", htmlspecialchars($_REQUEST["email"], ENT_QUOTES, 'UTF-8'));
$smarty->assign("email_conf", htmlspecialchars($_REQUEST["email_conf"], ENT_QUOTES, 'UTF-8'));
$smarty->assign("username", htmlspecialchars($_REQUEST["username"], ENT_QUOTES, 'UTF-8')); 
$smarty->assign("password", htmlspecialchars($_REQUEST["password"], ENT_QUOTES, 'UTF-8'));
$smarty->assign("password_conf", htmlspecialchars($_REQUEST["password_conf"], ENT_QUOTES, 'UTF-8'));
$smarty->assign("advertiser", $_REQUEST["advertiser"] == 1);

$smarty->assign("time", time());
$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/account/signup.tpl");

?>
