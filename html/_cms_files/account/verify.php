<?php

use App\Service\PhoneVerification\PhoneCarrierVerification;

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle;
$gTitle = "Phone Verification";


if (!$account->isMember())
	return $account->asklogin("You need to be logged in verify your phone.");


$smarty->assign("nobanner", true);
$smarty->assign("account_id", $account->getId());

//origin is URL where we need to redirect after successful verification
$origin = "/";
if (array_key_exists("origin", $_REQUEST))
	$origin = $_REQUEST["origin"];
$smarty->assign("origin", $origin);

if ($account->isPhoneVerified())
	return system::go($origin);		//phone is already verified, redirect to origin

$smarty->assign('rand', rand(1000, 5000));
check();
$smarty->display(_CMS_ABS_PATH."/templates/account/verify.tpl");

/**
 * @return number of step
 */
function check() {
	global $db, $smarty, $account, $gTitle;

	$phoneCarrierVerification = new PhoneCarrierVerification();

	$step = (int)filter_input(INPUT_POST, 'step', FILTER_SANITIZE_NUMBER_INT, array('options'=>array('default'=>1)));
	$phone = $sms_check = false;

	// Step 1 request
	if($step===1) {
		if ($_SERVER['REQUEST_METHOD'] === 'GET') {
			$smarty->assign('step', 1);
			debug_log('get request');
			return 1;
		}
		// to advance we need correct phone number
		if(isset($_REQUEST['phone'])) {
			$phone = phone::checkPhoneNumber($_REQUEST['phone']);
		}

		// on POST stage received empty or invalid phone number - show error
		if( $phone === false ) {
			$smarty->assign('error_message', 'Phone number is invalid');
			$smarty->assign('step', 1);
			return 1; // show error, remain on step 1
		}

		// edge case: is phone number we've got was already verified outside of THE ACCOUNT?
		if (phone::isVerified($phone) && ( $account->getNormalizedPhone() !== $phone && !$account->getPhoneVerified() ) ) {

			//SMS verification successful
			debug_log("Phone verified in table, updating account_id={$account->getId()}");
			//update verified phone we have for this account
			$db->q("UPDATE account SET phone = ?, phone_verified = 1 WHERE account_id = ?", array($phone, $account->getId()));

			$smarty->assign('step', 3);
			return 3;
		}

		if (phone::usedInAnotherAccount($phone)) {
			$smarty->assign('error_message', 'Phone number was used for another account. Contact website administrators.');
			$smarty->assign('step', 1);
			return 1; // show error, remain on step 1
		}

		// Send SMS
		$token = phone::startVerification($phone);

		$smarty->assign('phone', $phone);
		$smarty->assign('token', $token);
		$smarty->assign('step', 2);
		return 2;
	}

	if($step===2) {
		// Step 2 - we need token, SMS code and correct phone number
		$phone = false;
		if(isset($_REQUEST['phone'])) {
			$phone = phone::checkPhoneNumber($_REQUEST['phone']);
		}
		if(isset($_REQUEST['token'])) {
			$token = filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING, array('options'=>array(FILTER_NULL_ON_FAILURE)));
		}

		if( $phone === false || empty($token)) {
			debug_log("Step 2 : invalid phone received");
			$smarty->assign('error_message', 'Phone is not correct. You must start again.');
			$smarty->assign('step', 1);
			return 1;
		}

		$sms_code = '';
		if(isset($_REQUEST['sms_code'])) {
			$sms_code = filter_input(INPUT_POST, 'sms_code', FILTER_SANITIZE_STRING, array( 'options'=>array(FILTER_NULL_ON_FAILURE)));
		}
		if(is_null($sms_code)) {
			$sms_code = '';
		}

		// Try to verify phone number
		$attempt = phone::updateVerification($sms_code, $phone);
		if( $attempt === false || $attempt === 0 ) {
			debug_log("Step 2 processing: 3 attempts expired");
			$smarty->assign('error_message', 'After 3 unsuccessful attempts to validate phone number you must start again.');
			$smarty->assign('step', 1);
			return 1;
		}

		if ($attempt === true && phone::isVerified($phone)) {
			if( $account->getNormalizedPhone() !== $phone && !$account->getPhoneVerified() ) {
				//SMS verification successful
				debug_log("Phone verified in step2, updating account_id={$account->getId()}");
				//update verified phone we have for this account
				$db->q("UPDATE account SET phone = ?, phone_verified = 1 WHERE account_id = ?", array($phone, $account->getId()));

				$phoneCarrierVerification->checkPhone($phone, $account);
			}

			unset($_SESSION["sms_verify_check"]);
			unset($_SESSION["sms_verify_phone"]);
			unset($_SESSION["sms_verify_try_adid"]);
			unset($_SESSION["sms_verify_try_cnt"]);

			$smarty->assign('step', 3);
			return 3;
		}

		$smarty->assign('error_message', "Invalid verification code. Try again. You have $attempt attempt(s) left.");
		$smarty->assign('step', 2);
		$smarty->assign('phone', $phone);
		$smarty->assign('token', $token);
		return 2;

	}

	if($step===3) {
		$smarty->assign('step',3);
		return 3;
	}

	return 1;
}

