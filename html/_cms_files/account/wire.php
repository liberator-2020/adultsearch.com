<?php
/*
 * New wire notification form for escort agencies and CPC/CPM advertisers
 */

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $config_site_url;

if (!account::ensure_loggedin())
	die("Invalid access");
$account_id = $account->getId();

if (!$account->isAgency())
	die("Invalid access");

global $error;
$error = "";

function processSubmit() {
	global $account, $error, $db, $config_site_url;

	$wireDir = _CMS_ABS_PATH."/../data/wire";

	$name = trim($_REQUEST["name"]);
	if (!$name) {
		$error = "Please fill out sender name";
		return false;
	}

	$accountId = $account->getId();
	if (!$accountId) {
		$error = "Invalid account #id";
		return false;
	}

	$amount = $_REQUEST["amount"];
	if (!$amount) {
		$error = "Please fill out amount";
		return false;
	}

	$bankName = $_REQUEST["bank_name"];
	if (!$bankName) {
		$error = "Please fill out bank name";
		return false;
	}
	
	file_log("advertise", "account/wire: submission: name={$name}, account_id={$accountId}, amount={$amount}, bankName={$bankName}");

	//image upload processing
	if (!is_dir($wireDir))
		mkdir($wireDir);
	if (!isset($_FILES["image"])) {
		$error = "Wire image not uploaded";
		return false;
	}
	$handle = new upload($_FILES["image"]);
	if (!$handle->uploaded) {
		$error = "Wire image not uploaded";
		return false;
	}
	//determine filename etc...
	$fileNameBody = date("Y-m-d-H-i")."_{$accountId}_".getRandomString(5);
	$fileNameExt = "jpg";
	$fileName = "{$fileNameBody}.{$fileNameExt}";
	$filePath = $wireDir."/".$fileName;
	//set upload class
	$handle->allowed = ["image/x-png", "image/gif", "image/jpeg", "image/pjpeg", "image/png"];
	$handle->image_convert = 'jpg';
	$handle->file_new_name_body = $fileNameBody;
	$handle->file_new_name_ext = $fileNameExt;
	$handle->file_safe_name = false;
	//process
	$handle->process($wireDir);
	if (!$handle->processed) {
		$error = "Failed to process wire image";
		return false;
	}
	//thumbnail
	$fileNameBody .= "_t";
	$thumbnail = "{$fileNameBody}.{$fileNameExt}";
	if ($handle->image_src_x > $handle->image_src_y)
		$handle->image_x = 100;
	else
		$handle->image_y = 100;
	$handle->image_resize = true;
	$handle->image_ratio = true;
	$handle->image_convert = 'jpg';
	$handle->file_new_name_body = $fileNameBody;
	$handle->file_new_name_ext = $fileNameExt;
	$handle->file_safe_name = false;
	$handle->process($wireDir);
	if (!$handle->processed) {
		$error = "Failed to process thumbnail of wire image: '{$handle->error}'";
		return false;
	}
	$handle->clean();
	file_log("advertise", "account/wire: submission: image uploaded successfully: filename={$fileName}");

	//duplicity check
	$res = $db->q("
		SELECT id, image
		FROM account_wire 
		WHERE name = ? AND account_id = ? AND amount = ? AND bank_name = ? AND status = 0 AND created_stamp > ?
		", 
		[$name, $accountId, $amount, $bankName, (time() - 86400)]
		);
	while ($row = $db->r($res)) {
		$id = $row["id"];
		$image = $row["image"];
		$imagePath = $wireDir."/".$image;
    	if (file_exists($imagePath) && filesize($imagePath) == filesize($filePath)) {
			//this submission is identical, ignore this submission and delete uploaded file
			file_log("advertise", "account/wire: submission is identical to wire id#{$id}, deleting newly uploaded file '{$filePath}'");
			@unlink($filePath);
			return true;		
		}
	}

	//insert to db
	$res = $db->q("INSERT INTO account_wire 
					(name, account_id, amount, bank_name, image, thumbnail, status, created_stamp)
					VALUES
					(?, ?, ?, ?, ?, ?, ?, ?)",
					[$name, $accountId, $amount, $bankName, $fileName, $thumbnail, 0, time()]
					);
	if (!($id = $db->insertid($res))) {
		$error = "Failed to insert your submission";
		reportAdmin("AS: account/wire: error", "Failed to insert wire:, name={$name}, account_id={$account_id}, amount={$amount}, bank_name={$bank_name}");
		return false;
	}

	file_log("advertise", "account/wire: submission: DB insert succeeded, wireId={$id}");
	//reportAdmin("AS: account/wire: new wire submitted", "Account #Id {$account_id} submitted new wire.<br /><a href=\"{$config_site_url}/advertise/wires\">Check wires</a><br />");

	return true;
}

if ($_REQUEST["submit"] == "Submit") {
	$ret = processSubmit();
	if ($ret) {
		//processing successful, show success message and redirect to account page
		system::go("/account/mypage", "Your wire has been submitted successfully. Please note we add your funds only after your wire arrives to our bank account, which might be few business days. Thanks");
	} else {
		//processing failed, display error message
		$smarty->assign("error", $error);
	}
}

$smarty->assign("name", htmlspecialchars($_REQUEST["name"]));
$smarty->assign("account_id", $account_id);
$smarty->assign("amount", htmlspecialchars($_REQUEST["amount"]));
$smarty->assign("bank_name", htmlspecialchars($_REQUEST["bank_name"]));
$smarty->assign("nobanner", true);

escortsbiz::exportEbizLink();
//check if we already have website
if ($account->getEbizId() && $account->getEbizSecret()) {
    $smarty->assign('ebiz_login_url', escortsbiz::getMyLoginUrl());
}

$smarty->display(_CMS_ABS_PATH."/templates/account/wire.tpl");

?>
