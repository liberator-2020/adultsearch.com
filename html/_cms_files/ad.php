<?php
/**
 * Internal quick ad system
 */
global $db;
$system = new system;

if (isset($_GET["go"])) {
	$id = intval($_GET["go"]);
	if (!$id)
		$system->moved("/");
	$res = $db->q("select link from ad where id = ?", array($id));
	if (!$db->numrows($res)) {
		reportAdmin("AS: /ad cant find ad in db", "_cms_files/ad.php<br />select link from ad where id = {$id}", array("id (go http param)" => $id));
		$system->moved("/");
	}
	$row = $db->r($res);
	$link = substr($row["link"], 0, 128);
	if (isset($_GET['source']))
		$source = substr(GetGetParam('source'), 0, 50);
	else
		$source = 'header';
	$db->q("insert into ad_click (day, id, source, target) values (curdate(), ?, ?, ?) on duplicate key update c=c+1", array($id, $source, $link));
	$system->moved($link);
}

if( isset($_GET["goo"]) ) {
	$host = substr(GetGetParam("h"), 0, 128);
	$link = GetGetParam("l");
	$source = 'out';
	$id = 1;
	$db->q("insert into ad_click (day, id, source, target) values (curdate(), ?, ?, ?) on duplicate key update c=c+1", array($id, $source, $host));
	$system->moved($link);
}

switch($_GET["ad"]) {
	case 'Webcams' :
		$db->q("insert into ad_click (day, id, source, target) 
				values (curdate(), 1, 'header', 'http://www.rabbitscams.com/?AFNO=1-261') 
				on duplicate key update c=c+1");
		$system->moved("http://www.rabbitscams.com/?AFNO=1-261");
		break;
	case 'LocalSluts':
		$address = "http://contact-secrets.com/site/mptextlink1.php?kw=mp1&ad=T02";
		$db->q("insert into ad_click (day, id, source, target) values (curdate(), 2, 'header', ?) on duplicate key update c=c+1", array($address));
		$system->moved($address);
		break;
	default:
		$system->moved("/");
}

?>
