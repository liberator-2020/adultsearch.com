<?php

global $smarty, $mobile;

if ($mobile != 1)
	include_html_head("css", "/css/adbuild.css?20190912");

include_html_head("js", "/js/adbuildUI.js");
$smarty->assign("adbuild", 1);
$smarty->assign("nobanner", true);

$classifieds = new classifieds;
$classifieds->adbuild_init();

if ($_REQUEST["error"])
	$smarty->assign("error", htmlspecialchars($_REQUEST["error"]));

$smarty->assign("csrf", csrf::get_html_code("adbuild_step1"));
$smarty->display(_CMS_ABS_PATH."/templates/adbuild/step1.tpl");

?>
