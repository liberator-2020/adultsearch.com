<?php

global $smarty;

$classifieds = new classifieds;
$classifieds->adbuild_init();
$result = $smarty->fetch(_CMS_ABS_PATH."/templates/adbuild/adbuild_summary_ajax.tpl");

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json; charset=utf-8');
echo json_encode($result);
die;

?>
