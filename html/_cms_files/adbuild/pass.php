<?php
defined('_CMS_FRONTEND') or die('Restricted access');
/**
 * Adbuild - pass ad to other user
 */

global $db, $smarty, $account;

if (!$account->isadmin() && !permission::has("classified_edit") && !permission::has("classified_manage"))
    return;

include_html_head("css", "/css/adbuild.css?7");
include_html_head("js", "/js/adbuildUI.js");
$smarty->assign("adbuild", 1);
$smarty->assign("nobanner", true);

$clad = clad::findOneById($_REQUEST["ad_id"]);
if (!$clad) {
	echo "Error - can't find ad #{$_REQUEST["ad_id"]} !";
	return;
}

$smarty->assign("ad_id", $clad->getId());

$passed = false;
$error = "";

if ($_REQUEST["submit"] == "Submit") {
	$new_account_id = intval($_REQUEST["new_account_id"]);
	if (!$new_account_id) {
		$error = "New Account #Id not specified !";
	} else {
		$acc = account::findOneById($new_account_id);
		if (!$acc) {
			$error = "Can't find accoutn with id #{$new_account_id} !";
		} else {
			if ($clad->getAccountId() == $new_account_id) {
				$error = "Ad #{$clad->getId()} is already owned by account #{$clad->getAccountId()} !";
			} else {
				$clad->setAccountId($new_account_id);
				$ret = $clad->update(true);
				if (!$ret) {	
					$error = "Can't update ad owner of ad #{$clad->getId()} !";
				} else {
					$passed = true;
					$smarty->assign("account_label", "#{$acc->getId()} - {$acc->getUsername()} - {$acc->getEmail()}");

					//if ad is in "Not posted yet status" and has already some location selected, show step 4 link so worker can copy that into email to the ad owner
					if ($clad->getDone() == -1) {
						$locations = $clad->getLocations();
 						if (!empty($locations)) {
							$smarty->assign("step4_link", "https://adultsearch.com/adbuild/step4?ad_id=".$clad->getId());
						}
					} else if ($clad->getDone() >= 0) {
						$smarty->assign("view_link", $clad->getUrl());
					}
				}
			}
		}
	}
}


$smarty->assign("passed", $passed);
$smarty->assign("error", $error);

$smarty->display(_CMS_ABS_PATH."/templates/adbuild/pass.tpl");

?>
