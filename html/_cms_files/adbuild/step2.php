<?php

global $db, $smarty, $account, $mobile;

if ($mobile != 1)
	include_html_head("css", "/css/adbuild.css?20190912");

include_html_head("js", "/js/adbuildUI.js");
$smarty->assign("adbuild", 1);
$smarty->assign("nobanner", true);

$classifieds = new classifieds;
$account_id = $account->isloggedin();

$classifieds->adbuild_step = 2;
$clad_id = $classifieds->adbuild_init(isset($_REQUEST['category'])?intval($_REQUEST['category']):NULL);

$clad = clad::findOneById($clad_id);
if ($clad->getDone() != -1) {
	//adbuild step 2 is only for new ads, old ads (live, expired, ...) cant change location here but need to use classifieds/move controller
	return system::go("/classifieds/myposts");
}

// list of the countries / states
$sel = classifieds::getStates();
$smarty->assign("sel", $sel);

// list of the cities
$hide = classifieds::getCities();
$smarty->assign("hide", $hide);

file_log("classifieds", "Adbuild step2: clad_id={$classifieds->adbuild_id}");

$smarty->assign("csrf", csrf::get_html_code("adbuild_step2"));
$smarty->display(_CMS_ABS_PATH."/templates/adbuild/step2.tpl");

?>
