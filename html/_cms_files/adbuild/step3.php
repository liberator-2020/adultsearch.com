<?php

use App\Service\Classified\WaitingList;
use App\Service\PhoneVerification\PhoneCarrierVerification;

require_once(_CMS_ABS_PATH."/_cms_files/classifieds/array.php");

global $db, $smarty, $account, $config_dev_server, $mobile;
$classifieds = new classifieds;

$phoneCarrierVerification = new PhoneCarrierVerification();

$smarty->assign("jquery_new", true);

if ($mobile != 1)
	include_html_head("css", "/css/adbuild.css?20190912");

include_html_head("js", "/js/adbuildUI.js");
include_html_head("js", "/js/ckeditor/ckeditor.js");
$smarty->assign("adbuild", 1);
$smarty->assign("nobanner", true);
$smarty->assign("jquery_file_upload", true);
$smarty->assign("flowplayer", true);
$smarty->assign("globals", $GLOBALS);

$form = new form;

$classifieds->adbuild_step = 3;
$ret = $classifieds->adbuild_init();

$account_id = $account->isloggedin();

if( count($classifieds->adbuild_locs) < 1 )
	system::moved("/adbuild/step2?ad_id={$classifieds->adbuild_id}");

$smarty->assign('ad_id', $classifieds->adbuild_id);

/* lets check if the ad is posted in international or in US to determine if we r using metric system or not */
$classifieds->adbuild_param_by_loc();

$last_image_stamp = NULL;
$res = $db->q("SELECT uploaded_stamp FROM classifieds_image WHERE id = ? ORDER BY uploaded_stamp DESC LIMIT 1", array($classifieds->adbuild_id));
if ($db->numrows($res) == 1) {
	$row = $db->r($res);
	$last_image_stamp = $row["uploaded_stamp"];
	$smarty->assign("last_image_stamp", $last_image_stamp);
}

$phone_util = \libphonenumber\PhoneNumberUtil::getInstance();

/* updating ... */
if( isset($_POST['updatethead']) ) {
	$update_sql = $error = $check = NULL;
	$error_availability = false;
	foreach($classifieds->adbuild_cat_questions[$classifieds->adbuild_type] as $input) {
		$val = $_POST[$input];
		if( $input == 'age' ) { 
			$check[] = $input;
			if (($val < 18 || $val > 100) && !$account->isrealadmin()) {
				$error[] = '<a href="#error-age" data-ajax="false">Choose correct age</a>';
				$smarty->assign('errorage', true);
			}
		}
		if( $input == 'incall' ) {
			$val = intval(GetPostParam('incall'));
			$val2 = intval(GetPostParam('outcall'));
			if( !$val && !$val2 ) { 
				$error[] = '<a href="#error-call" data-ajax="false">Select Incall or Outcall</a>';
				$smarty->assign('errorincall', true);
			}
		}
		if( $input == 'avl_men' || $input == 'avl_women' || $input == 'avl_couple' ) {
			$val = intval(GetPostParam($input));
			$val1 = GetPostParam('avl_men');
			$val2 = GetPostParam('avl_women');
			$val3 = GetPostParam('avl_couple');
			if (!$val1 && !$val2 && !$val3 && !in_array('Available To', $error) && !$account->isadmin() && !$error_availability) {
				$error[] = '<a href="#error-available" data-ajax="false">Your availability</a>';
				$smarty->assign('erroravl', true);
				$error_availability = true; // otherwise there will be 3 similar errors
			}
		}

		if( strstr($input,'visiting_') ) {
			//format check
			if ($val == "")
				$val = "0000-00-00";
			if ((preg_match('/^(\d{2})\/(\d{2})\/(\d{4})/i', $val) != 1) && (preg_match('/^(\d{4})-(\d{2})-(\d{2})/i', $val) != 1)) {
				//$error[] = htmlspecialchars($input, ENT_QUOTES, 'UTF-8');
				$error[] = '<a href="#error-visiting" data-ajax="false">Visiting date</a>';
				$smarty->assign('errorvisiting', true);
			} else {
				if (!$val)
					$val = '0000-00-00';
				else
					$val = preg_replace('/(\d{2})\/(\d{2})\/(\d{4})/', '$3-$1-$2', $val);
				if ($val != "0000-00-00") {
					//day/month/year valid check
					$tokens = explode("-", $val);
					if (count($tokens) != 3) {
						//$error[] = $input;
						$error[] = '<a href="#error-visiting" data-ajax="false">Visiting date</a>';
						$smarty->assign('errorvisiting', true);
					} else if (intval($tokens[0]) < 2014 || intval($tokens[0]) > 2099) {
						//$error[] = $input;
						$error[] = '<a href="#error-visiting" data-ajax="false">Visiting date</a>';
						$smarty->assign('errorvisiting', true);
					} else if (intval($tokens[1]) < 1 || intval($tokens[1]) > 12) {
						//$error[] = $input;
						$error[] = '<a href="#error-visiting" data-ajax="false">Visiting date</a>';
						$smarty->assign('errorvisiting', true);
					} else if (intval($tokens[2]) < 1 || intval($tokens[2]) > 31) {
						//$error[] = $input;
						$error[] = '<a href="#error-visiting" data-ajax="false">Visiting date</a>';
						$smarty->assign('errorvisiting', true);
					}
				}
			}
		}

		if ($input == "location")
			$val = preg_replace('/[^a-zA-Z0-9\-\'" _\(\)]/', '', $val);

		if (in_array($input, array("ethnicity", "incall_rate_hh", "incall_rate_h", "incall_rate_2h", "incall_rate_day", "incall", "outcall", "outcall_rate_hh", "outcall_rate_h", "outcall_rate_2h", "outcall_rate_day", "available", "age", "pornstar", "payment_visa", "payment_amex", "payment_dis", "review", "eyecolor", "haircolor", "pregnant", "visiting", "tantra", "fetish_dominant", "fetish_submissive", "fetish_swith", "height_feet", "height_inches", "weight", "build", "measure_1", "measure_2", "measure_3", "cupsize", "penis_size", "kitty", "gfe", "gfe_limited")))
			$val = intval($val);

		//in db tinyint unsigned
		if (in_array($input, array("measure_1", "measure_2", "measure_3")) && ($val > 255))
			$val = 255;
			

		$update_sql[$input] = $classifieds->adbuild_row[$input] = $val;

		if (GetPostParam($input))
			$smarty->assign($input, htmlspecialchars($val, ENT_QUOTES, 'UTF-8'));

	}

	if( is_null($error) ) {
		$classifieds->shouldAllow($account_id);
		if( !$classifieds->shouldAllowUser && 
			$classifieds->blockedContent($_POST['content'], $classifieds->adbuild_row['done']<1?'':$classifieds->adbuild_row['content'], $e)  ) {
				$error[] = '<a href="#error-adcontent" data-ajax="false">Content of ad is invalid</a>';
		}
	}

	$firstname = $classifieds->adbuild_row['firstname'] = substr(classifieds::noHtml(classifieds::onlyPrintableCharacters($_POST["firstname"])), 0, 50);
	$title = $classifieds->adbuild_row['title'] = substr(classifieds::noHtml(classifieds::onlyPrintableCharacters($_POST["title"])), 0, 200);
	$content = $classifieds->adbuild_row['content'] = strip_tags(classifieds::onlyPrintableCharacters(substr($_POST["content"], 0, 3000)), "<br><p><b><u><i><strong><em><ul><ol><li><a><img>");
	$email = $classifieds->adbuild_row['email'] = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
	$reply = $classifieds->adbuild_row['reply'] = GetPostParam('reply');



	// Validate phone number
	// phone validation can generate one of three possible errors:
	// 1. phone number is invalid
	// 2. phone requires SMS auth (in this case we'll show additional field in template
	// 3. attempted verification code is incorrect.
	$phone = substr(classifieds::onlyPrintableCharacters($_POST['phone_full']), 0, 18);

	$phone = phone::checkPhoneNumber($phone, $classifieds->adbuild_country);

	if (!$phone) {
		if ($account->isrealadmin()) {
			// Allows Jay to override invalid numbers
			$phone = substr(classifieds::onlyPrintableCharacters($_POST['phone_full']), 0, 18);
		} else {
			$error[] = '<a href="#error-phone" data-ajax="false">Invalid phone number</a>';
			$smarty->assign('errorphone', true);
		}
	} else if (!$account->isAgency()) {

		if (phone::usedInAnotherAccount($phone)) {
			$error[] = '<a href="#error-phone">Phone number was used for another account. Contact website administrators.</a>';
			$smarty->assign('errorphone', true);
		} else {
			$need_verification = !phone::isVerified($phone);

			if( $need_verification ) {
				if( !isset($_POST['sms_code'])) {
					$error[] = sprintf('<a href="#error-validation" data-ajax="false">Phone verification needed.
						A 4-digit code has just been sent to your phone "%s".
						It might take up to 1 minute for the code to arrive.
						Please enter this code.</a>'
									, $phone );

					$smarty->assign('phone_validation_needed', true);
					phone::startVerification($phone);
				} else {

					$sms_code = filter_var($_POST["sms_code"], FILTER_SANITIZE_STRING, array('options'=>array('default'=>'-1')));

					// Try to verify phone number
					$attempt = phone::updateVerification($sms_code, $phone);
					if( $attempt === false || $attempt <= 0 ) {
						debug_log("Step 2 processing: 3 attempts expired");
						$error[] = '<a href="#error-phone" data-ajax="false">After 3 unsuccessful attempts to validate phone number you must start again.</a>';
						$phone = '';
						$smarty->assign('phone_validation_error', true);
						$smarty->assign('phone_validation_needed', false);
						$smarty->assign('errorphone', true);

					} else {

						if (!phone::isVerified($phone)) {
							$error[] = '<a href="#error-validation" data-ajax="false">Phone verification error. Code is not correct. Enter correct code.</a>';
							$smarty->assign('phone_validation_needed', true);
							$smarty->assign('phone_validation_error', true);
						}
					}

					$phoneCarrierVerification->checkPhone($phone, $account);
					// otherwise do nothing
				}
			}
			// otherwise do nothing
		}
	}
	$classifieds->adbuild_row['phone'] = $phone;

	$website = $classifieds->adbuild_row['website'] = classifieds::sanitizeWebsite(classifieds::noHtml(classifieds::onlyPrintableCharacters($_POST["website"])));
	$payment_visa = $classifieds->adbuild_row['payment_visa'] = isset($_POST['payment_visa']) ? 1 : 0;
	$payment_amex = $classifieds->adbuild_row['payment_amex'] = isset($_POST['payment_amex']) ? 1 : 0;
	$payment_dis = $classifieds->adbuild_row['payment_dis'] = isset($_POST['payment_dis']) ? 1 : 0;
	//$bigdoggie = $classifieds->adbuild_row['bigdoggie'] = GetPostParam('bigdoggie');
	$ter = $classifieds->adbuild_row['ter'] = substr(preg_replace('/[^0-9]/', '', $_POST["ter"]), 0, 20);


	$facebook = $classifieds->adbuild_row['facebook'] = substr(preg_replace('/[^a-zA-Z0-9\.-_\/]/', '', $_POST["facebook"]), 0, 255);
	$twitter = $classifieds->adbuild_row['twitter'] = substr(preg_replace('/[^a-zA-Z0-9\.-_\/]/', '', $_POST["twitter"]), 0, 255);
	$google = $classifieds->adbuild_row['google'] = substr(preg_replace('/[^a-zA-Z0-9\.-_\/]/', '', $_POST["google"]), 0, 255);
	$instagram = $classifieds->adbuild_row['instagram'] = substr(preg_replace('/[^a-zA-Z0-9\.-_\/]/', '', $_POST["instagram"]), 0, 255);

	if (empty($firstname)  && !$account->isrealadmin()) { 
		$error[] = '<a href="#error-name" data-ajax="false">About you - Name</a>';
		$smarty->assign('errorname', true);
	}
	if( empty($title) ) {
		$error[] = '<a href="#error-introduction" data-ajax="false">About you - Introduction</a>';
		$smarty->assign('errortitle', true);
	}
	if( empty($content) ) {
		$error[] = '<a href="#error-adtext" data-ajax="false">About you - Ad text</a>';
		$smarty->assign('errorcontent', true);
	}
	if ($reply != 2 && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$error[] = '<a href="#error-email" data-ajax="false">Invalid email</a>';
		$smarty->assign('erroremail', true);
	}

	if (count($classifieds->adbuild_row["language"]) > 4) {
		$error[] = '<a href="#error-language" data-ajax="false">Language(s) Spoken</a>';
		$smarty->assign('errorlanguage', true);
	}

	$blacklisted = false;
	if (clbw::isBlacklisted($email, clbw::EMAIL)) {
		$error[] = '<a href="#error-email" data-ajax="false">Banned email address</a>';
		$blacklisted = true;
		$bwitem = clbw::findOneByField($email, clbw::EMAIL);
		if ($bwitem) {
			$bwitem->hit($classifieds->adbuild_id);
			debug_log("CLBW: BL hit, id={$bwitem->getId()}, email={$email}, ad_id={$classifieds->adbuild_id}");
		}
	}
	if (clbw::isBlacklisted($phone, clbw::PHONE)) {
		$error[] = '<a href="#error-phone" data-ajax="false">Banned phone number</a>';
		$blacklisted = true;
		$bwitem = clbw::findOneByField($phone, clbw::PHONE);
		if ($bwitem) {
			$bwitem->hit($classifieds->adbuild_id);
			debug_log("CLBW: BL hit, id={$bwitem->getId()}, phone={$phone}, ad_id={$classifieds->adbuild_id}");
		}
	}
	if (clbw::isBlacklisted($website, clbw::WEBSITE)) {
		$error[] = '<a href="#error-website" data-ajax="false">Banned website</a>';
		$blacklisted = true;
		$bwitem = clbw::findOneByField($website, clbw::WEBSITE);
		if ($bwitem) {
			$bwitem->hit($classifieds->adbuild_id);
			debug_log("CLBW: BL hit, id={$bwitem->getId()}, website={$website}, ad_id={$classifieds->adbuild_id}");
		}
	}
	if ($blacklisted) {
		$smarty->assign("spam", true);
	}

	if( is_null($error) ) {
		$sql = [];
		$params = array();
		$sql[] = "`firstname` = ?"; $params[] = $firstname;
		$sql[] = "`title` = ?"; $params[] = $title;
		$sql[] = "`content` = ?"; $params[] = $content;
		$sql[] = "`email` = ?"; $params[] = $email;
		$sql[] = "`reply` = ?"; $params[] = $reply;
		$sql[] = "`phone` = ?"; $params[] = $phone;
		$sql[] = "`website` = ?"; $params[] = $website;
		$sql[] = "`payment_visa` = ?"; $params[] = $payment_visa;
		$sql[] = "`payment_amex` = ?"; $params[] = $payment_amex;
		$sql[] = "`payment_dis` = ?"; $params[] = $payment_dis;
		//$sql[] = "`bigdoggie` = ?"; $params[] = $bigdoggie;
		$sql[] = "`ter` = ?"; $params[] = $ter;
		$sql[] = "`facebook` = ?"; $params[] = $facebook;
		$sql[] = "`twitter` = ?"; $params[] = $twitter;
		$sql[] = "`google` = ?"; $params[] = $google;
		$sql[] = "`instagram` = ?"; $params[] = $instagram;
		$sql[] = "`loc_id` = ?"; $params[] = $classifieds->adbuild_locs[0];

		$res = $db->q("select * from location_location where loc_id = ?", array($classifieds->adbuild_locs[0]));
		if( $db->numrows($res) ) {
			$rox = $db->r($res);
			$sql[] = "`state_id` = ?"; $params[] = $rox['loc_parent'];
			$sql[] = "`loc_name` = ?"; $params[] = $rox['loc_name'];
		}

		foreach($update_sql as $key => $value) {
			if (is_array($value)) {
				$sql[] = "`$key` = ?"; $params[] = implode(',', $value);
			} else {
				$sql[] = "`$key` = ?"; $params[] = $value;
			}
		}
		$params[] = $classifieds->adbuild_id;
		$db->q("UPDATE classifieds SET ".implode(',', $sql)." WHERE id = ?", $params);
		file_log("classifieds", "Adbuild step3: UPDATE: error='{$db->error()}', sql=".print_r($sql, true).", params=".print_r($params, true));

		if( isset($_POST['preview']) || isset($_POST['preview_x']) ) {
			system::moved("/adbuild/preview?ad_id={$classifieds->adbuild_id}");
			$smarty->assign('preview', true);
		} else {

			//make sure we have created thumbnail
			$res = $db->q("SELECT count(ci.image_id) as image_count, c.thumb
							FROM classifieds c 
							LEFT JOIN classifieds_image ci on c.id = ci.id 
							WHERE c.id = ?",
							[$classifieds->adbuild_id]
						);
			$row = $db->r($res);
			if (intval($row["image_count"]) > 0 && !$row["thumb"]) {
				file_log("classifieds", "Adbuild step3: Creating thumbnail, image count = {$row["image_count"]}");
				$ret = $classifieds->createThumb($classifieds->adbuild_id);
			}

			if (!$classifieds->adbuild_editonly) {
				file_log("classifieds", "Adbuild step3: Moving to step 4 (clad_id={$classifieds->adbuild_id})");
				system::moved("/adbuild/step4?ad_id={$classifieds->adbuild_id}");
			} else {
				//this was edit of already live ad

				//26.10.2015 - Dallas: I need to get emails when an ad has been edited. Spammer might put an ugly chick so we approve it & then edit it to a hot chick
				//6.11.2015 - Dallas email: Don't need edited photo emails anymore
				/*
				$old_last_image_stamp = intval($_REQUEST["last_image_stamp"]);
				if ($last_image_stamp && $old_last_image_stamp && $old_last_image_stamp < $last_image_stamp) {
					$clad = clad::findOneById($classifieds->adbuild_id);
					if ($clad) {
						debug_log("Adbuild step3: Sending edit admin notification for ad.id={$classifieds->adbuild_id}, new image(s) uploaded, stamp={$last_image_stamp}");
						$ret = $clad->emailAdminNotify(NULL, true);
						if (!$ret)
							reportAdmin("AS: Adbuild step3: unable to send admin notify email", "", array("clad_id" => $classifieds->adbuild_id));
					} else {
						reportAdmin("AS: Adbuild step3: Can't find clad by id", "", array("clad_id" => $classifieds->adbuild_id));
					}
				}
				*/

				if (isset($_REQUEST["ref"])) {
					system::go($_REQUEST["ref"]);
				}

				$smarty->assign('updated', true);
			}
		}
	} else {
		$smarty->assign('error', $error);
	}
} else {
	foreach($classifieds->adbuild_cat_questions[$classifieds->adbuild_type] as $input) {

		$val = $classifieds->adbuild_row[$input];

		/*
		if( strstr($input, 'visiting_') ) {
			$val = preg_replace('/(\d{4})\-(\d{2})\-(\d{2})/', '$2/$3/$1', $val);
			if( strstr($val, '00/00/0000') ) $val = '';
		}
		*/
		if ($input == "fetish")
			$classifieds->adbuild_row[$input] = explode(',', $val);
		if ($input == "language")
			$classifieds->adbuild_row[$input] = explode(',', $val);
		if ($input == "location")
			$val = preg_replace('/[^a-zA-Z0-9\-\'" _\(\)]/', '', $val);

		if ($val)
			$smarty->assign($input, htmlspecialchars($val, ENT_QUOTES));
	}
}

if( $classifieds->adbuild_metric ) {
	$adbuild_height_feet = $adbuild_height_feet_metric;
	$adbuild_height_inches = NULL;
	for($i=0;$i<100;$i++) $adbuild_height_inches[] = array("$i"=>"$i");
	$smarty->assign('metric', true);
}

$language_options = ["" => " - Please select -"] + $classifieds->adbuild_language;
$smarty->assign('language_options', $language_options);
$smarty->assign('language_options_html', join("", array_map( function($a, $b) { return "<option value=\"{$a}\">{$b}</option>"; }, array_keys($language_options), array_values($language_options))));
$smarty->assign('selected_languages', $classifieds->adbuild_row['language']);

$smarty->assign('fetish', $form->select_add_array($adbuild_fetish, $classifieds->adbuild_row['fetish']));
$smarty->assign('ethnicity', $form->select_add_select('---') . $form->select_add_array($adbuild_ethnicity, $classifieds->adbuild_row['ethnicity']));
$smarty->assign('height_feet', $form->select_add_select('---') . $form->select_add_array($adbuild_height_feet, $classifieds->adbuild_row['height_feet']));
$smarty->assign('height_inches', $form->select_add_select('---') . $form->select_add_array($adbuild_height_inches, $classifieds->adbuild_row['height_feet']?$classifieds->adbuild_row['height_inches']:''));
$smarty->assign('eyecolor', $form->select_add_select('---') . $form->select_add_array($adbuild_eyecolor, $classifieds->adbuild_row['eyecolor']));
$smarty->assign('haircolor', $form->select_add_select('---') . $form->select_add_array($adbuild_haircolor, $classifieds->adbuild_row['haircolor']));
$smarty->assign('build', $form->select_add_select('---') . $form->select_add_array($adbuild_build, $classifieds->adbuild_row['build']));
$smarty->assign('cupsize', $form->select_add_select('---') . $form->select_add_array($adbuild_cupsize, $classifieds->adbuild_row['cupsize']));
$smarty->assign('penis_size', $form->select_add_select('---') . $form->select_add_array($adbuild_penis_size, $classifieds->adbuild_row['penis_size']));
$smarty->assign('kitty', $form->select_add_select('---') . $form->select_add_array($adbuild_kitty, $classifieds->adbuild_row['kitty']));

/*
$weight_free_input = false;
if ($weight && !array_key_exists($weight, $adbuild_weight))
	$weight_free_input = true;
$smarty->assign("weight_free_input", $weight_free_input);
$smarty->assign("weight_options", array_merge(array("" => "---"), $adbuild_weight));
*/

$smarty->assign('firstname', htmlspecialchars($classifieds->adbuild_row['firstname'], ENT_QUOTES, 'UTF-8'));
$smarty->assign('title', htmlspecialchars($classifieds->adbuild_row['title'], ENT_QUOTES, 'UTF-8'));
$smarty->assign('content', htmlspecialchars($classifieds->adbuild_row['content'], ENT_QUOTES, 'UTF-8'));
$smarty->assign('email', htmlspecialchars($classifieds->adbuild_row['email'], ENT_QUOTES, 'UTF-8'));
$smarty->assign('reply', $classifieds->adbuild_row['reply']);

//
$phone = htmlspecialchars($classifieds->adbuild_row['phone'], ENT_QUOTES, 'UTF-8');
$country = $classifieds->adbuild_country;

$smarty->assign('phone1', $phone);
$smarty->assign('country_iso', $country);
if( $classifieds->adbuild_international ) {
	$smarty->assign('international', true);
} else {
	$smarty->assign('international', false);
}
$smarty->assign('website', htmlspecialchars($classifieds->adbuild_row['website'], ENT_QUOTES, 'UTF-8'));
$smarty->assign('payment_visa', htmlspecialchars($classifieds->adbuild_row['payment_visa'], ENT_QUOTES, 'UTF-8'));
$smarty->assign('payment_amex', htmlspecialchars($classifieds->adbuild_row['payment_amex'], ENT_QUOTES, 'UTF-8'));
$smarty->assign('payment_dis', htmlspecialchars($classifieds->adbuild_row['payment_dis'], ENT_QUOTES, 'UTF-8'));
//$smarty->assign('bigdoggie', htmlspecialchars($classifieds->adbuild_row['bigdoggie'], ENT_QUOTES, 'UTF-8'));
$smarty->assign('ter', htmlspecialchars($classifieds->adbuild_row['ter'], ENT_QUOTES, 'UTF-8'));

$smarty->assign('facebook', htmlspecialchars($classifieds->adbuild_row['facebook'], ENT_QUOTES, 'UTF-8'));
$smarty->assign('twitter', htmlspecialchars($classifieds->adbuild_row['twitter'], ENT_QUOTES, 'UTF-8'));
$smarty->assign('google', htmlspecialchars($classifieds->adbuild_row['google'], ENT_QUOTES, 'UTF-8'));
$smarty->assign('instagram', htmlspecialchars($classifieds->adbuild_row['instagram'], ENT_QUOTES, 'UTF-8'));

$smarty->assign("currency_sign", currency_convert::getPriceTagOnly($classifieds->adbuild_currency));

$age_options = array("" => "-");
for ($i = 21; $i <=70; $i++)
	$age_options[$i] = $i;
$smarty->assign("age_options", $age_options);


// Videos
$res = $db->q("SELECT cv.id, cv.filename, cv.thumbnail, cv.converted
				FROM classifieds AS c
				INNER JOIN classified_video AS cv on c.id = cv.classified_id
				WHERE cv.classified_id = ? AND cv.account_id=?",
				[$classifieds->adbuild_id, $account_id] );
$videos = [];

while($row = $db->r($res)) {
	$videos[] = array('id'=>$row['id'], 'filename'=>$row['filename'], 'thumbnail'=>$row['thumbnail'],'converted'=>$row['converted']);
}

$smarty->assign("videos", $videos);

// ------------------------------------------------------------------------------------
// ad is not live so lets show the purchase and upgrade options while building the ad..
if (!$classifieds->adbuild_editonly) {

	$now = time();

	$city_thumb_counts = $classifieds->getCityThumbCounts();
	$side_sponsor_counts = $classifieds->getSideSponsorCounts($classifieds->adbuild_type);
	$sticky_counts = $classifieds->getStickyCounts($classifieds->adbuild_type);

	//load city thumbnail info from db and fill smarty variables
	$res = $db->q("
		SELECT l.loc_id, l.loc_name, s.id, s.day
		FROM location_location l
		LEFT JOIN classifieds_sponsor s on (l.loc_id = s.loc_id AND s.post_id = ? AND s.done < 1 AND s.expire_stamp > ?)
		WHERE l.loc_id in (".implode(',', $classifieds->adbuild_locs).")",
		[$classifieds->adbuild_id, $now]
		);
	$total_loc = $db->numrows($res);
	$homesp = $cover = NULL;
	$available_locations = 0;
	while($row = $db->r($res)) {
		$day = $row['day'];
		$loc_id = $row["loc_id"];
		$not_available = false;
	    if (array_key_exists($loc_id, $city_thumb_counts) && $city_thumb_counts[$loc_id] >= classifieds::$city_thumbnail_count_limit) {
    	    $not_available = true;
	    } else {
    	    $available_locations++;
	    }
		$homesp[] = array(
			'loc_id'=>$row['loc_id'],
			'loc_name'=>$row['loc_name'],
			'checked'=>$row['id']?true:false,
			"price"=>$classifieds->gethomepricebyloc($row['loc_id']),
			"not_available" => $not_available,
			);
		if( !$cover && $row['id'] )
			$cover = true;
	}
	if( $total_loc < 11 && count($homesp) ) {
		$smarty->assign("homesp", $homesp);
		$smarty->assign("cover", $cover);
	}

	//load side sponsors info from db and fill smarty variables
	$res = $db->q("
		SELECT l.loc_id, l.loc_name, s.id, s.day
		FROM location_location l
		LEFT JOIN classifieds_side s on l.loc_id = s.loc_id and s.post_id = ? and s.done < 1 AND s.expire_stamp > ?
		WHERE l.loc_id in (".implode(',', $classifieds->adbuild_locs).")",
		[$classifieds->adbuild_id, $now]
		);
	$total_loc = $db->numrows($res);
	$sides = NULL;
	$available_locations = 0;
	while($row = $db->r($res)) {
		$day = $row['day'];
		$loc_id = $row["loc_id"];
		$not_available = false;
	    if (array_key_exists($loc_id, $side_sponsor_counts) && $side_sponsor_counts[$loc_id] >= classifieds::$side_sponsor_count_limit) {
    	    $not_available = true;
	    } else {
    	    $available_locations++;
	    }
		$sides[] = array(
			'loc_id' => $row['loc_id'],
			'loc_name' => $row['loc_name'],
			'checked' => $row['id']?true:false,
			"price" => classifieds::get_side_upgrade_price(),
			"not_available" => $not_available,
			);
	}
	$smarty->assign("sides", $sides);

	//load sticky sponsors info from db and fill smarty variables
	$res = $db->q("
		SELECT l.loc_id, l.loc_name, l.s, s.id, s.days
		FROM location_location l
		LEFT JOIN classified_sticky s on l.loc_id = s.loc_id and s.classified_id = ? and s.done < 1 AND s.expire_stamp > ?
		WHERE l.loc_id in (".implode(',', $classifieds->adbuild_locs).")",
		[$classifieds->adbuild_id, $now]
		);
	$total_loc = $db->numrows($res);
	$stickies = [];
	$available_locations = $sticky_min_monthly_price = 0;
	while($row = $db->r($res)) {
		$loc_id = $row["loc_id"];

		$not_available = false;
	    if (array_key_exists($loc_id, $sticky_counts) && $sticky_counts[$loc_id] >= classifieds::$sticky_count_limit) {
    	    $not_available = true;
	    } else {
    	    $available_locations++;
	    }

		$monthly_price = classifieds::get_sticky_upgrade_price($classifieds->adbuild_type, $row['loc_id'], 30);
		$weekly_price = classifieds::get_sticky_upgrade_price($classifieds->adbuild_type, $row['loc_id'], 7);

		if (!$sticky_min_monthly_price || $monthly_price < $sticky_min_monthly_price)
			$sticky_min_monthly_price = $monthly_price;

		$stickies[] = array(
			'loc_id' => $row['loc_id'],
			'loc_name' => $row['loc_name'],
			'checked' => (boolean)$row['id'],
			"price_monthly" => $monthly_price,
			"price_weekly" => $weekly_price,
			"not_available" => $not_available,
		);
	}
	$smarty->assign("stickies", $stickies);
	$smarty->assign("sticky_min_monthly_price", $sticky_min_monthly_price);

	$stickyWaitingListService = new WaitingList();
	$awaitingLocationsIds = $stickyWaitingListService->getActualAwaitingLocationsIdsByAccountId($account->getId());

	$smarty->assign('awaiting_locations_ids', $awaitingLocationsIds);

	//recurring and repost info
	if ($classifieds->adbuild_row['recurring'])
		$smarty->assign("recurring", true);

	$smarty->assign("repost", $classifieds->adbuild_row['recurring']||$classifieds->adbuild_row['auto_renew']?1:0);

	$auto_renew_fr_array = NULL;
	$auto_renew_fr_array[] = array(1=>"Every Day");
	$auto_renew_fr_array[] = array(2=>"2 days");
	$uplimit = $classifieds->adbuild_row['auto_renew_fr']>8?$classifieds->adbuild_row['auto_renew_fr']:8;
	if( $uplimit > 30 ) $uplimit = 8;
	for($i=3;$i<$uplimit;$i++) {
		if($i == 1 ) $auto_renew_fr_array[] = array($i=>"$i day");
		else $auto_renew_fr_array[] = array($i=>"$i days");
	}
	$auto_renew_fr = $form->select_add_array($auto_renew_fr_array, isset($_POST["auto_renew_fr"])?$_POST["auto_renew_fr"]:$classifieds->adbuild_row["auto_renew_fr"]);
	$smarty->assign("auto_renew_fr", $auto_renew_fr);

	$art_array = get_time_array();
	$smarty->assign("art_values", array_keys($art_array));
	$smarty->assign("art_names", array_values($art_array));
	$smarty->assign("auto_renew_time", isset($_POST["auto_renew_time"]) ? $_POST["auto_renew_time"] : $classifieds->adbuild_row['auto_renew_time']);

	$smarty->assign("auto_renew", 
		isset($_POST["title"]) ? 
			htmlspecialchars($_POST["auto_renew"], ENT_QUOTES, 'UTF-8') : 
			(isset($rowcl["auto_renew"]) ? $rowcl["auto_renew"] : "")
		);

	$smarty->assign("autoRepost", isset($_POST["autoRepost"]) ? htmlspecialchars($_POST["autoRepost"], ENT_QUOTES, 'UTF-8') : $classifieds->adbuild_row["auto_renew"]);

	$repost = NULL;
	for($i=5;$i<31;$i+=5) {
		$repost[] = array($i=>"$i Times for $".number_format($classifieds->price_per_repost*$i*count($classifieds->adbuild_locs), 2));
	}
	$autoRepostS = $form->select_add_array($repost, isset($_POST["autoRepost"])?$_POST["autoRepost"]:$classifieds->adbuild_row["auto_renew"]);
	$smarty->assign("autoRepostS", $autoRepostS);
}

include_html_head("js", "/js/tools/jquery.bbedit.min.js");
$smarty->assign("ad_id", $classifieds->adbuild_id);
$smarty->assign("bp", $classifieds->adbuild_row['bp']);
$smarty->assign("is_agency", $account->getAgency());
$smarty->assign("type", $classifieds->adbuild_type);
$smarty->assign("home_price_month", classifieds::get_city_thumbnail_upgrade_price());
$smarty->assign("price_side_sponsor", classifieds::get_side_upgrade_price());
$smarty->assign("csrf", csrf::get_html_code("adbuild_step3"));

if (isset($_REQUEST["ref"]))
	$smarty->assign("ref", $_REQUEST["ref"]);

$smarty->display(_CMS_ABS_PATH."/templates/adbuild/step3.tpl");

?>
