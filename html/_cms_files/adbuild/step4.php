<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $account, $smarty;
$classifieds = new classifieds;

if ($mobile != 1)
	include_html_head("css", "/css/adbuild.css?20161031");

include_html_head("js", "/js/adbuildUI.js");
include_html_head("js", "/js/ckeditor/ckeditor.js");
$smarty->assign("adbuild", 1);
$smarty->assign("nobanner", true);

$classifieds->adbuild_step = 4;
$classifieds->adbuild_init();

if (count($classifieds->adbuild_locs) < 1)
	system::moved("/adbuild/step2?ad_id={$classifieds->adbuild_id}");

//if we posting new ad (no edit, no renew), check if we are allowed to post new ad
if ($classifieds->adbuild_row['done'] < 0 && !$classifieds->posting_new_ad_allowed())
	system::moved("/adbuild/?error=".urlencode("You can't post another ad, please contact us at support@adultsearch.com"));

$now = time();
$ip_address = account::getUserIp();

$clad = clad::findOnebyId($classifieds->adbuild_id);
if (!$clad)
	system::moved("/adbuild/?error=".urlencode("Can't find classfied ad #{$classifieds->adbuild_id} in database, please contact us at support@adultsearch.com"));
$clad_account = $clad->getAccount();
if (!$clad_account && (classifieds::get_cl_owner() != $classifieds->adbuild_id))
	system::moved("/adbuild/?error=".urlencode("Can't find classfied ad #{$classifieds->adbuild_id} owner, please contact us at support@adultsearch.com"));


//----------------------------------
//----------------------------------
//----------------------------------

function process_register() {
	global $smarty, $account;

	$email = trim(preg_replace('/[^0-9A-Za-z_\-.@]/', '', $_REQUEST["account_email"]));
	$username = preg_replace('/[^0-9A-Za-z_\-.@]/', '', $_REQUEST["account_username"]);
	$password = preg_replace('/[^0-9A-Za-z_\-.@$#:]/', '', $_REQUEST["account_password"]);
	$password_conf = preg_replace('/[^0-9A-Za-z_\-.@$#:]/', '', $_REQUEST["account_password2"]);
	
	//input fields validation
	if (empty($email)) {
		$smarty->assign("error", "You need to type your e-mail address.");
		return false;
	}
	if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
		$smarty->assign("error", "Wrong e-mail address");
		return false;
	}
	if (strstr($username, ' ')) {
		$smarty->assign("error", "You may not have space in your user name");
		return false;
	}
	if ($account->email_inuse($email)) {
		$smarty->assign("error", "This email address is already in use.  If you forgot your password, please <a href=\"/account/reset\">click here</a> to get a reminder.");
		return false;
	}
	if (empty($username)) {
		$smarty->assign("error", "Please type a user name");
		return false;
	}
	if ($account->username_taken($username)) {
		$smarty->assign("error", "This user name($username) is already taken, please try a different one.");
		return false;
	}
	if (empty($password)) {
		$smarty->assign("error", "Please type a password");
		return false;
	}
	if (strcmp($password, $password_conf)) {
		$smarty->assign("error", "Passwords are not match.");
		return false;
	}
	if ($account->isBannedForLogin($email)) {
		$smarty->assign("error", "You may not register on our site.");
		return false;
	}

	$error = null;
	$newAccountId = (int)$account->auto_register($email, $username, $password, 1, $error);

	if ($error)
		$smarty->assign("error", $error);

	return $newAccountId;
}

//if we are not logged in, show log in / quick register page
if (!account::isLoggedIn()) {

	file_log("classifieds", "adbuild/step4: not logged in -> displaying login/register form page");

	$newAccountId = false;
	if ($_POST["account_email"])
		$newAccountId = process_register();
	
	if ($newAccountId)
		$account = account::findOneById($newAccountId);

	if (!$newAccountId || !$account) {
		$smarty->assign("login_email", htmlspecialchars($_POST["login_email"]));
		$smarty->assign("login_password", htmlspecialchars($_POST["login_password"]));
		$smarty->assign("account_email", htmlspecialchars($_POST["account_email"]));
		$smarty->assign("account_username", htmlspecialchars($_POST["account_username"]));
		$smarty->assign("account_password", htmlspecialchars($_POST["account_password"]));
		$smarty->assign("account_password2", htmlspecialchars($_POST["account_password2"]));
		$smarty->assign('ad_id', $classifieds->adbuild_id);
		return $smarty->display(_CMS_ABS_PATH."/templates/adbuild/login_register.tpl");
	}
}

file_log("classifieds", "adbuild/step4: logged in: account_id={$account->getId()}");

//----------------------------------
//----------------------------------
//----------------------------------

//make sure ad is correctly assigned to our account (we could have just logged in or registered)
if (!$clad->getAccount() || !$clad->getAccountId()) {
	file_log("classifieds", "adbuild/step4: assigning clad to account: clad_id={$clad->getId()}, account_id={$account->getId()}");
	$clad->setAccount($account);
	$clad->setAccountId($account->getId());
	$clad->update();
	$clad_account = $account;	
}

//make sure phone verification for this ad is assigned to our account
if (!phone::updateVerificationForClassified($clad)) {
	file_log("classifieds", "adbuild/step4: ERROR: failed to updateVerificationForClassified");
}

//create payment in database
$total = $classifieds->adbuild_totalcost;
$period = 30;
$res = $db->q("
	INSERT INTO payment 
	(created_stamp, account_id, author_id, amount, recurring_amount, recurring_period, email, result, ip_address)
	VALUES
	(?, ?, ?, ?, ?, ?, ?, ?, ?)",
	[$now, $clad->getAccountId(), $account->getId(), $total, $total, $period, $clad_account->getEmail(), "I", $ip_address]
	);
$payment_id = $db->insertid($res);
if (!$payment_id)
	system::moved("/adbuild/?error=".urlencode("Server error 1, please contact us at support@adultsearch.com"));

//create payment_items
$res = $db->q("
	SELECT owner, promo, promo_id, autorenew, auto_renew_fr, recurring, sponsor_city, sponsor_total, side_city, side_total, 
	auto_renew_time, timezone_time, time, upgradeonly
	FROM classifieds_payment
	WHERE post_id = ?
	LIMIT 1",
	[$clad->getId()]
	);
if (!$db->numrows($res))
	system::moved("/adbuild/?error=".urlencode("Server error 2, please contact us at support@adultsearch.com"));
$row = $db->r($res);
$promo_id = $row["promo_id"];
if ($clad->getPromo() && $clad->getPromoCode())
	$promo_id = $clad->getPromo();

$city_count = $row["sponsor_city"];	//total number of city thumbnail upgrades
$side_count = $row["side_city"];	//total number of side sponsor upgrades

foreach ($clad->getLocations() as $loc) {

	$city = $side = null;

	$res = $db->q("SELECT id FROM classifieds_sponsor WHERE post_id = ? AND loc_id = ?", [$clad->getId(), $loc->getId()]);
	if ($db->numrows($res) > 0 && $city_count > 0) {
		$city_count--;
		$city = 1;
	}

	$res = $db->q("SELECT id FROM classifieds_side WHERE post_id = ? AND loc_id = ?", [$clad->getId(), $loc->getId()]);
	if ($db->numrows($res) > 0 && $side_count > 0) {
		$side_count--;
		$side = 1;
	}

	$sticky = $sticky_position = $sticky_days = null;
	$res = $db->q("SELECT id FROM classified_sticky WHERE classified_id = ? AND loc_id = ?", [$clad->getId(), $loc->getId()]);
	if ($db->numrows($res) > 0) {
		$sticky = 1;
		$sticky_position = NULL;
		$sticky_days = 30;
	}

	file_log("classifieds", "adbuild/step4: payment_item insert: loc_id={$loc->getId()}, city_thumbnail={$city}, side={$side}, sticky={$sticky}");

	$classified_status = "N";
	if ($clad->getCreated() && ($clad->getCreated() < (time()-(86400*29))))
		$classified_status = "R";

	$res = $db->q("
		INSERT INTO payment_item
		(payment_id, type, classified_id, classified_status, loc_id, auto_renew, daily_repost
		, sponsor, side, sticky, sticky_position, sticky_days, sponsor_desktop, sponsor_mobile, created_stamp)
		VALUES
		(?, ?, ?, ?, ?, ?, ?
		, ?, ?, ?, ?, ?, ?, ?, ?)",
		[$payment_id, "classified", $clad->getId(), "N", $loc->getId(), $row["autorenew"], (($row["recurring"]) ? 1 : 0)
		, $city, $side, $sticky, $sticky_position, $sticky_days, null, null, $now]
		);
	$pi_id = $db->insertid($res);
	if (!$pi_id) {
		reportAdmin("AS: adbuild/step4 error", "Cant create payment classified xlink in db!");
		system::moved("/adbuild/?error=".urlencode("Server error 3, please contact us at support@adultsearch.com"));
	}
}

if ($promo_id)
	$db->q("UPDATE payment SET promocode_id = ? WHERE id = ? LIMIT 1", [$promo_id, $payment_id]);

return system::go("/payment/pay?id={$payment_id}");

?>
