<?php
defined('_CMS_FRONTEND') or die('Restricted access');
/**
 * Adbuild final step - displaying confirmation page with some links
 */

global $db, $smarty, $account;

if (!account::ensure_loggedin())
	die("Invalid access");

file_log("classifieds", "adbuild/step5: ad_id={$_REQUEST["ad_id"]}");

include_html_head("css", "/css/adbuild.css?20190912");
include_html_head("js", "/js/adbuildUI.js");
$smarty->assign("adbuild", 1);
$smarty->assign("nobanner", true);

$classifieds = new classifieds;
$classifieds->adbuild_step = 5;
$classifieds->adbuild_init();

$clad = clad::findOneById($classifieds->adbuild_id);

file_log("classifieds", "adbuild/step5: clad:{$classifieds->adbuild_id}, done={$clad->getDone()}");

if ($clad->getDone() == 1) {
	// wait for 1 second
	// this is to help process rotating sphinx index meanwhile we display this page to be sure the ad is accessible, when poster clicks on "view ad"
	sleep(1);
}

$smarty->assign("current", time());
$smarty->assign("ad_id", $classifieds->adbuild_id);
$smarty->assign("done", $clad->getDone());
$smarty->assign("thumb", $classifieds->adbuild_row['thumb']);

if (!$account->getEbizId() && !$classifieds->adbuild_row["website"])
	$smarty->assign("ebiz_show", true);

$smarty->display(_CMS_ABS_PATH."/templates/adbuild/step5.tpl");

?>
