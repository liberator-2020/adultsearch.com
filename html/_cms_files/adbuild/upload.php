<?php

global $db, $smarty;

include_html_head("css", "/css/adbuild.css?5");
include_html_head("js", "/js/adbuildUI.js");
$smarty->assign("adbuild", 1);
$smarty->assign("nobanner", true);

$form = new form; $system = new system;
$classifieds = new classifieds;

$classifieds->adbuild_step = 3;
$classifieds->adbuild_init();

if( isset($_GET['delete']) ) {
	$classifieds->adbuild_imgdel(intval($_GET['delete']));
	die;
}

if( isset($_FILES) && !empty($_FILES) ) {
	$_FILES = multiplef($_FILES);

	foreach($_FILES as $key => $file) {
		$return = $classifieds->adbuild_upload($key);
		echo json_encode(array($return));
	}
	die;
}

if( isset($_GET['frame']) ) {
	if( strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE') ) $smarty->assign('ie', true);
	$smarty->display(_CMS_ABS_PATH.'/templates/adbuild/adbuild_uploadimg.tpl');
	die;
}

echo json_encode($classifieds->adbuild_imgload());
die;

function multiplef(array $_files, $top = TRUE)
{
    $files = array();
    foreach($_files as $name=>$file){
        if($top) $sub_name = $file['name'];
        else    $sub_name = $name;
        
        if(is_array($sub_name)){
            foreach(array_keys($sub_name) as $key){
                $files[$name][$key] = array(
                    'name'     => $file['name'][$key],
                    'type'     => $file['type'][$key],
                    'tmp_name' => $file['tmp_name'][$key],
                    'error'    => $file['error'][$key],
                    'size'     => $file['size'][$key],
                );
                $files[$name] = multiplef($files[$name], FALSE);
            }
        }else{
            $files[$name] = $file;
        }
    }
    return $files;
}


?>
