<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;
$system = new system;

$system->go("http://adultsearch.com/advertise/edit");
return;
die;

/*
if( !($account_id = $account->isloggedin())) {
	$account->asklogin();
	return;
}

if (!$account->isrealadmin() && ($account_id != 46754)) {
	//jay or dena
	$smarty->display(_CMS_ABS_PATH."/templates/advertise/advertise_campain_add_new_hot.tpl");
	return;
}

$new = false;

if( !isset($_GET["id"]) )
	$new = true;
else { 
	$id = GetGetParam("id");
	$res = $db->q("select * from advertise_campaign where id = '$id' and account_id  = '$account_id'");
	if( !$db->numrows($res) ) $new = true;
	else {
		$row = $db->r($res); $new = false;
		$name = $name_orj = $row["name"];
		$ad_title = $ad_title_orj = $row["ad_title"];
		$ad_line1 = $ad_line1_orj = $row["ad_line1"];
		$ad_line2 = $ad_line2_orj = $row["ad_line2"];
		$ad_displayurl = $ad_displayurl_orj = $row["ad_durl"];
		$ad_prefix = $row["ad_prefix"];
		$ad_url = $ad_url_orj = $row["ad_url"];
		$budget = $budget_orj = $row["budget"];
		$dcpc = $dcpc_orj = $row["dcpc"];
		$external = $row["external"];

		$res = $db->q("select s_id, section, recursion, bid from advertise_camp_section where c_id = '$id' and `show` = 1");
		while($row=$db->r($res)) {
			$ad_cat[] = $row[0];
			if( $row["recursion"] == 1 ) $ad_recursion[] = $row[0];
			$dbs[$row[0]] = $row["bid"];
		}

		$res = $db->q("select d.loc, d.section, concat(l.loc_name,' ',l.s) loc_name from advertise_camp_section_dont d inner join location_location l on d.loc = 
l.loc_id where c_id = '$id' group by section, loc");
		$dont = NULL;
		while($row=$db->r($res)) {
			$loc_dont[] = array("loc"=>$row["loc"], "section"=>$row["section"], "loc_name"=>$row["loc_name"]);
			if( $row["section"] == "sc" ) $dont["sc"] .= ($dont["sc"] ? "|" : "") . $row["loc"];
			elseif( $row["section"] == "as" ) $dont["as"] .= ($dont["as"] ? "|" : "") . $row["loc"];
			elseif( $row["section"] == "emp" ) $dont["emp"] .= ($dont["emp"] ? "|" : "") . $row["loc"];
			elseif( $row["section"] == "cls" ) $dont["cls"] .= ($dont["cls"] ? "|" : "") . $row["loc"];
		}
		$smarty->assign("loc_dont", $loc_dont);
		$smarty->assign("sc_dont", $sc_dont);
		$smarty->assign("as_dont", $as_dont);
		$smarty->assign("emp_dont", $emp_dont);

		$res = $db->q("select loc from advertise_camp_section_dont_c where c_id = '$id'");
		while($row=$db->r($res)) {
			$geo[] = $row[0];
		}
	}
}

if( isset($_POST["name"]) ) {
	$name = GetPostParam("name");
	$ad_title = GetPostParam("ad_title");
	$ad_line1 = GetPostParam("ad_line1");
	$ad_line2 = GetPostParam("ad_line2");
	$ad_displayurl = GetPostParam("ad_displayurl");
	$ad_prefix = GetPostParam("ad_prefix");
	$ad_url = GetPostParam("ad_url");
	$budget = GetPostParam("budget");
	$dcpc = GetPostParam("dcpc");
	$external = isset($_POST["external"])?1:0;

	$linex = explode(" ", $ad_line1);
	foreach($linex as $l) {
		if( strlen($l) > 15 ) {
			$error = "Some of the words in Description line 1 too long. Please use shorter words.";
			break;
		}
	}

	$linex = explode(" ", $ad_line2);
	foreach($linex as $l) {
		if( strlen($l) > 15 ) {
			$error = "Some of the words in Description line 2 too long. Please use shorter words.";
			break;
		}
	}

	if( isset($error) )  $error = $error;
		else if( empty($name) ) $error = "Campaign name may not be empty";
	else if( empty($ad_title) ) $error = "Ad Title may not be empty";
	else if( empty($ad_line1) || empty($ad_line1) ) $error = "Ad Description Lines may not be empty";
	else if( empty($ad_displayurl) ) $error = "Display URL may not be empty";
	else if( empty($ad_url) ) $error = "Ad URL may not be empty";
	elseif( $dcpc < 0.01 ) $error = "Default CPC can not be less than $0.01";
	else { 
		$category = count($_POST["ad_cat"]);
		if( isset($_POST["selected15"]) && is_array($_POST["selected15"]) ) $category += count($_POST["selected15"]);
		if( isset($_POST["selected5"]) && is_array($_POST["selected5"]) ) $category += count($_POST["selected5"]);
		if( isset($_POST["selected81"]) && is_array($_POST["selected81"]) ) $category += count($_POST["selected81"]) ? count($_POST["selected81"]) - 1 : 0;
		if( $external ) $category += count($_POST["selectedexternal"]);
		$ad_https = $ad_prefix == "https://" ? 1 : 0;

		$status_budget = 0;
		$rex = $db->q("select budget from advertise_budget where account_id = '$account_id'");
		if( $db->numrows($rex) ) {
			$rox = $db->r($rex);
			if( $rox[0] > 0 ) $status_budget = 1; 
		}

		if( !$new ) {
			if( $budget > $budget_orj ) {
				$rex = $db->q("select sum(cost) from advertise_camp_section s left join advertise_data d on (s.id = d.s_id and d.date = curdate()) where 
s.c_id = '$id'");
				if( $db->numrows($rex) ) {
					$rox = $db->r($rex); $dbudget = number_format($rox[0]);
					if( $dbudget < $budget ) {
						$db->q("update advertise_camp_section set budget_available = 1 where c_id = '$id'");
						if( $db->affected ) { 
							$alert = "Some of your ads are now live with your new budget!";
						}
					}
				}
			}

			$db->q("update advertise_campaign set name = '$name', ad_title = '$ad_title', ad_line1 = '$ad_line1', ad_line2 = '$ad_line2', ad_durl = 
'$ad_displayurl', ad_https = '$ad_https', ad_url = '$ad_url', budget = '$budget', category = '$category', dcpc = '$dcpc', external = '$external' where id = '$id'");
			$c_id = $id;

			if( $ad_url == $ad_url_orj )
				$approved_dont_need = true;
			else {
				$rex = $db->q("select id from advertise_section where external = 0 and appneeded = 1");
				if( $db->numrows($rex) ) {
					while($rox=$db->r($rex)) {
						$idx[] = $rox[0];
					}
					$db->q("update advertise_camp_section set approved = 0 where c_id = '$id' and s_id in (".implode(',',$idx).")");
				}
			}

		} else {
			$db->q("insert into advertise_campaign (account_id, name, ad_title, ad_line1, ad_line2, ad_durl, ad_https, ad_url, budget, category, dcpc, 
external) values ('$account_id', '$name', '$ad_title', '$ad_line1', '$ad_line2', '$ad_displayurl', '$ad_https', '$ad_url', '$budget', '$category', '$dcpc', '$external')");
			$c_id = $db->insertid;

			if( is_array($_SESSION["advertise_exception"]) ) {
				foreach($_SESSION["advertise_exception"] as $ex)
					if( !empty($ex) )
						$db->q("insert into advertise_exception (c_id, exception) values (?, ?)", [$c_id, $ex]);
				$_SESSION["advertise_exception"] = NULL;
			}
		}

		if(!$c_id)
			$error = "Error, please try again.";
		else {
			$db->q("update advertise_camp_section set `show` = 0 where c_id = '$c_id'");
			if( is_array($_POST["ad_cat"]) ) { 
				foreach($_POST["ad_cat"] as $cat) {
					$add = 0; $cat = intval($cat);
					$rex = $db->q("select s.*, count(s2.id) p from advertise_section s left join advertise_section s2 on s.id = s2.parent where 
s.id = '$cat'");
					if( $db->numrows($rex) ) {
						$rox = $db->r($rex);
						$dcpc_ = isset($_POST["_db_{$cat}"])&&(float)$_POST["_db_{$cat}"]==$_POST["_db_{$cat}"]?(float)$_POST["_db_{$cat}"]:$dcpc;
						if( $rox["defaultbid"] > $dcpc_ ) $dcpc_ = $rox["defaultbid"];
						$dcpc_ = round($dcpc_, 2);
						$name_string = $rox["external"]==1?"n":"name";
						$p = $rox["p"] ? 1 : 0;
						$recursion = $rox["external"]&&isset($_POST["recursion".$cat])?1:0;
						$approved = !$rox['appneeded']?1:0;
						if( in_array($cat, $ad_cat) ) 
							$db->q("INSERT INTO advertise_camp_section 
									(c_id, s_id, status, section, name, bid, haschild, recursion) 
									values 
									('$c_id', '$cat', '$status_budget', '{$rox["section"]}', '{$rox[$name_string]}', '$dcpc_', '$p', '$recursion')
									on duplicate key
									update `show` = 1, name = '{$rox[$name_string]}', haschild = '$p', recursion = '$recursion', bid = '$dcpc_', approved = if(approved<0,0,approved)");
						else 
							$db->q("INSERT INTO advertise_camp_section 
									(c_id, s_id, status, section, name, bid, haschild, recursion, approved) 
									values
									('$c_id', '$cat', '$status_budget', '{$rox["section"]}', '{$rox[$name_string]}', '$dcpc_', '$p', '$recursion', '$approved') 
									on duplicate key 
									update `show` = 1, name = '{$rox[$name_string]}', haschild = '$p', recursion = '$recursion', bid = '$dcpc_'");
					}
				}
			} 

			$rex = $db->q("select s_id, approved from advertise_camp_section where c_id = '$c_id'");
			$oldies = array();
			while($rox=$db->r($rex)) {
				$oldies[$rox["s_id"]] = $rox["approved"];
			}

			reset($_POST);
			foreach($_POST as $key=>$value) {
				if( !strncmp($key, "selected", 8) && is_array($value) ) {
					$parent = intval(substr($key, 8));
					foreach($value as $val) {
						$val = intval($val);
						if( strstr($key, 'selectedexternal') )
							$rex = $db->q("select s2.defaultbid, s.section, s.external, s.appneeded, concat(s2.name,' - ',s.name) name, 
concat(s2.name,' - ',s.n) n from advertise_section s inner join advertise_section s2 on s.parent = s2.id where s.id = '$val'");
						else
							$rex = $db->q("select s2.defaultbid, s.section, s.external, s.appneeded, concat(s2.name,' - ',s.name) name, 
concat(s2.name,' - ',s.n) n from advertise_section s inner join advertise_section s2 on s.parent = s2.id where s.id = '$val' and s.parent = '$parent'");

						if( $db->numrows($rex) ) {
							$rox = $db->r($rex);
							$name_string = $rox["external"]==1?"n":"name";
							$approved = $rox["appneeded"] == 1 ? 0 : 1;
							if ($rox["appneeded"] == 0)
								$approved2 = 1;
							else
								$approved2 = isset($approved_dont_need) ? 1 : 0;
							//$defaultbid = $rox["defaultbid"];
							$defaultbid = $dcpc;
							//$bid = in_array($val, $ad_cat) ? "bid" : $dcpc;
							$bid = $dcpc;
							$name = substr($rox[$name_string], 0, 50);
							$db->q("INSERT INTO advertise_camp_section 
									(c_id, s_id, status, section, name, bid, approved) 
									values 
									(?, ?, ?, ?, ?, ?, ?) 
									on duplicate key UPDATE bid = ?, `show` = 1, name = ?, approved = ?", 
									array($c_id, $val, $status_budget, $rox["section"], $name, $defaultbid, $approved, $bid, $name, $approved2));

							if( $rox["external"] == 1 && $rox["appneeded"] ) {
								if( ($new && $approved == 0) ) $notify_owner[] = $val;
								elseif( !$new && ((isset($oldies[$val]) && $oldies[$val]==1 && $approved2 != "approved") || 
									(!isset($oldies[$val]) && $approved == 0) || ($approved2 == 1 && isset($oldies[$val]) && 
									$oldies[$val]==0 ) ) ) $notify_owner[] = $val;
							}
						}
					}
				}
			}

			$db->q("delete from advertise_camp_section_dont where c_id = '$c_id'");
			$x_dont = GetPostParam("sc_dont");
			if( !empty($x_dont) ) {
				$x_donts = explode("|", $x_dont);
				foreach($x_donts as $loc) {
					$loc = intval($loc);
					if( $loc ) $db->q("insert into advertise_camp_section_dont (id, c_id, section, loc) values ((select id from advertise_camp_section 
where c_id = '$c_id' and section = 'sc'), '$c_id', 'sc', '$loc')");
				}
			}
			$x_dont = GetPostParam("as_dont");
			if( !empty($x_dont) ) {
				$x_donts = explode("|", $x_dont);
				foreach($x_donts as $loc) {
					$loc = intval($loc);
					if( $loc ) $db->q("insert into advertise_camp_section_dont (id, c_id, section, loc) values ((select id from advertise_camp_section
where c_id = '$c_id' and section = 'as'), '$c_id', 'as', '$loc')");
				}
			}
			$x_dont = GetPostParam("emp_dont");
			if( !empty($x_dont) ) {
				$x_donts = explode("|", $x_dont);
				foreach($x_donts as $loc) {
					$loc = intval($loc);
					if( $loc ) $db->q("insert into advertise_camp_section_dont (id, c_id, section, loc) values ((select id from advertise_camp_section
where c_id = '$c_id' and section = 'emp'), '$c_id', 'emp', '$loc')");
				}
			}

			$x_dont = GetPostParam("cls_dont");
			if( !empty($x_dont) ) {
				$x_donts = explode("|", $x_dont);
				foreach($_POST['selected15'] as $s_idx) {
					$s_idx = intval($s_idx);
					foreach($x_donts as $loc) {
						$loc = intval($loc);
						if( $loc )
							$db->q("insert into advertise_camp_section_dont (id, c_id, section, loc) values ((select id from advertise_camp_section where c_id = '$c_id' and s_id = '$s_idx'), '$c_id', 'cls', '$loc')");
					}
				}
			}

			$db->q("delete from advertise_camp_section_dont_c where c_id = '$c_id'");
			$geo_dont = GetPostParam("_selectgeo");
			foreach($geo_dont as $key => $value) {
				$value = intval($value);
				if( $value ) $db->q("insert into advertise_camp_section_dont_c (c_id, loc) values ('$c_id', '$value')");
			}
		}

		if( isset($notify_owner) ) {
			foreach($notify_owner as $s_id) {
				$db->q("insert ignore into advertise_notification (account_id, s_id, what, time) values ((select account_id from advertise_section 
where id = '$s_id'), '$s_id', 'newadd', now())");
			}
		}

		$url = "/advertise/detail?id=".$db->insertid;
		if( $alert ) $system->go($url, $alert);
		else $system->moved($url);
	}
	if( isset($error) ) $smarty->assign("error", $error);
}

if( empty($_POST["ad_cat"]) && count($ad_cat) )
	$_POST["ad_cat"] = $ad_cat;
if( is_array($_POST["ad_cat"]) ) {
	foreach($_POST["ad_cat"] as $cat) {
		switch($cat) {
			case 'sc': $smarty->assign("sc", true); break;
			case 'as': $smarty->assign("as", true); break;
			case 'emp': $smarty->assign("emp", true); break;
			case 'aw': $smarty->assign("aw", true); break;
			case 'cambodia': $smarty->assign("cambodia", true); break;
			case 'philippines': $smarty->assign("philippines", true); break;
			case 'thailand': $smarty->assign("thailand", true); break;
			case 'all': $smarty->assign("all", true); break;
		}
	}
}

$sql = "SELECT s.*, group_concat(s2.id ORDER BY s2.orden) ids, group_concat(s2.name ORDER BY s2.orden) names, group_concat(s2.n ORDER BY s2.orden) ns, max(s2.appneeded) appneeded2 
		FROM advertise_section s
		LEFT JOIN advertise_section s2 on (s.id = s2.parent and (s2.external = 0 or s2.active = 1))
		WHERE s.parent = 0 group by s.id
		ORDER BY s.orden, s.name";
$rex = $db->q($sql);
$cat_ad = NULL;
$subex = $subex2 = NULL;

while($rox=$db->r($rex)) {
	$ids = $rox["ids"];
	$names = $rox["external"]==0?$rox["names"]:$rox["ns"];
	$sub = $sub2 = NULL;
	
	if( $rox['id'] == 5 )
		continue;

	if( !empty($ids) ) {
		$ids = explode(",", $ids);
		$names = explode(",", $names);
		for($i=0;$i<count($ids);$i++) {
			if( $rox["external"] == 0 ) {
				if( !in_array($ids[$i], $ad_cat) ) $sub[] = array("id"=>$ids[$i], "name"=>$names[$i]);
				else $sub2[] = array("id"=>$ids[$i], "name"=>$names[$i]);
			} else {
				if( !in_array($ids[$i], $ad_cat) ) {$subex[] = array("id"=>$ids[$i], "name"=>$names[$i], "parent"=>$rox["name"], "defaultbid"=>number_format($rox["defaultbid"],2)); }
				else {
					$sub2ex[] = array("id"=>$ids[$i], "name"=>$names[$i], "parent"=>$rox["name"], "defaultbid"=>number_format($rox["defaultbid"],2),);
				}
			}
		}
		if( count($sub) > 0 && count($sub2) == 0 ) {
			$sub2 = $sub; $sub = NULL;
		}
	}

	//email dena 12/05/2014 12:49 PM
	//please remove the ability to bid on those sections from the current AS CPC system so advertisers can only bid on shemale and fetish text ads
	if ($rox["name"] == "Erotic Services") {
		$sub3 = array();
		foreach ($sub2 as $key => $val) {
			if ($val["id"] == 16) {
				$fe_id = $key;
				continue;
			}
			if ($val["id"] == 18) {
				$br_id = $key;
				continue;
			}	
			$sub3[] = $val;
		}
		$sub2 = $sub3;
	}

	//remove AS section from pop-under category - from august 2013 until end of year - email dallas 28.8.2013 12:38
	if ($rox["name"] == "Pop-Under Advertising") {
		$as_id = NULL;
		foreach ($sub2 as $key => $val) {
			if ($val["id"] == 82) {
				$as_id = $key;
				break;
			}
		}
		if ($as_id != NULL)
			unset($sub2[$as_id]);
	}
	//remove EMP/BD section from pop-under category - from 13.9.2013 until 13.3.2014 - email dallas 13.9.2013 16:37
	if ($rox["name"] == "Pop-Under Advertising") {
		$as_id = NULL;
		foreach ($sub2 as $key => $val) {
			if ($val["id"] == 10058) {
				$as_id = $key;
				break;
			}
		}
		if ($as_id != NULL)
			unset($sub2[$as_id]);
	}
	if ($rox["name"] == "Pop-Under Advertising") {
		$as_id = NULL;
		foreach ($sub2 as $key => $val) {
			if ($val["id"] == 79) {
				$as_id = $key;
				break;
			}
		}
		if ($as_id != NULL)
			unset($sub2[$as_id]);
	}

	if( $rox["external"] == 0 )
		$cat_ad[] = array(
			"id"=>$rox["id"],
			"section"=>$rox["section"],
			"name"=>$rox["name"],
			"hasloc"=>$rox["hasloc"],
			"checked"=>isset($_POST["ad_cat"])&&is_array($_POST["ad_cat"])&&in_array($rox["id"], $_POST["ad_cat"])?true:false,
			"dont"=>isset($dont[$rox["section"]])?$dont[$rox["section"]]:"",
			"sub"=>$sub,
			"sub2"=>$sub2,
			"recursion"=>0,
			"bid"=>isset($dbs[$rox["id"]])?$dbs[$rox["id"]]:($rox['defaultbid']?$rox['defaultbid']:0),
			"defaultbid"=>$rox['defaultbid']!=0.2?$rox['defaultbid']:0,
			"d"=>!empty($rox["d"])?$rox["d"]:"",
			"appneeded"=>$rox['appneeded']
		);
	elseif( count($subex) || count($sub2ex) ) {
		if( $rox["appneeded2"] == 1 )  $appneeded2 = true;
		
		continue;
		$cat_ad_ex[] = array(
			"id"=>$rox["id"],
			"section"=>$rox["section"],
			"name"=>$rox["name"],
			"d"=>$rox["d"],
			"hasloc"=>$rox["hasloc"],
			"checked"=>isset($_POST["ad_cat"])&&is_array($_POST["ad_cat"])&&in_array($rox["id"], $_POST["ad_cat"])?true:false,
			"dont"=>isset($dont[$rox["section"]])?$dont[$rox["section"]]:"",
			"sub"=>$subex,
			"sub2"=>$sub2ex,
			"recursion"=>in_array($rox["id"], $ad_recursion)?1:0,
			"defaultbid"=>number_format($rox["defaultbid"],2),
			"sub_total"=>$sub_total,
			"sub2_total"=>$sub2_total,
		);
	}

}

if( $subex || $sub2ex ) {
	if( count($subex) > 0 && count($sub2ex) == 0 ) { $sub2ex = $subex; $subex = NULL; }
	$cat_ad_ex = array("sub"=>$subex, "sub2"=>$sub2ex, "sub_total"=>$sub_total, "sub2_total"=>$sub2_total);
}

$smarty->assign("cat_ad", $cat_ad);
$smarty->assign("cat_ad_ex", $cat_ad_ex);
if( isset($appneeded2) ) $smarty->assign("appneeded", true);

$res = $db->q("SELECT loc_id, loc_name FROM location_location WHERE loc_type = 1 ORDER BY loc_id ASC");
while($row=$db->r($res)) {
	if (in_array($row["country_id"], $geo))
		$cat_ad_geo[] = array("id"=>$row["loc_id"], "name"=>$row["loc_name"]);
	else
		$cat_ad_geo2[] = array("id"=>$row["loc_id"], "name"=>$row["loc_name"]);
}
$smarty->assign("cat_ad_geo", $cat_ad_geo);
$smarty->assign("cat_ad_geo2", $cat_ad_geo2);

$smarty->assign("name", $name);
$smarty->assign("ad_title", $ad_title);
$smarty->assign("ad_line1", $ad_line1);
$smarty->assign("ad_line2", $ad_line2);
$smarty->assign("ad_displayurl", $ad_displayurl);
$smarty->assign("ad_prefix", $ad_prefix);
$smarty->assign("ad_url", $ad_url);
$smarty->assign("budget", $budget);
$smarty->assign("dcpc", $dcpc);
$smarty->assign("external", $external);

$smarty->assign("h1title", $new ? "New Campaign" : "Edit Campaign: {$row["name"]}");
include_html_head("js", "/js/tools/jquery.autotab.js");
include_html_head("js", "/js/tools/jquery.textareaCounter.plugin.js");
$smarty->assign("new", $new);

if( !$new ) {
	$smarty->assign("id", $id);
}

if( !$new ) {
	$res = $db->q("select * from advertise_exception where c_id = '$id'");
	while($row=$db->r($res)) {
		$exception[] = array("x"=>$row["exception"]);
	}
} else {
	if( is_array($_SESSION["advertise_exception"]) ) {
		foreach($_SESSION["advertise_exception"] as $ex)
			$exception[] = array("x"=>$ex);
	}
}

$smarty->assign("exception", $exception);
$smarty->assign("nobanner", true);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/advertise_campain_add_new.tpl");
*/
?>
