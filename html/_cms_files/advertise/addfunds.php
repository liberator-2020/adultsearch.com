<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";
$smarty->assign("noshare", true);
$smarty->assign("nobanner", true);

if (!($account_id=$account->isLoggedIn())) {
	$account->asklogin();
	return;
}

$res = $db->q("SELECT budget FROM advertise_budget WHERE account_id = ?", [$account_id]);
$row = $db->r($res);
$smarty->assign("budget", number_format($row["budget"], 2));

if ($_REQUEST["error"] && $_REQUEST["reason"])
	$smarty->assign("error", $_REQUEST["reason"]);

//18.8.2017 Zack - disable upping advertiser balance with credit card -- disabled is also form in template
/*
if ($_REQUEST["submit"] == "Next") {
	//submitted deposit funds form

	//check if we dont already have max amount in advertising balance
	if ($row["budget"] > 10000 && !$account->isadmin()) {
		system::go("/advertise/", "You have reached the maximum allowable amount in your account. Please try again when you have less than $10,000 in your account.");
	}

	$amount = intval($_REQUEST["amount"]);

	if ($amount < 1 || $amount > 1000) {
		$smarty->assign("error", "Invalid amount !");
	} else {

		// create new payment and redirect to pay page
		$payment_id = advertise::create_payment($account_id, $account->getEmail(), $amount);
		if (!$payment_id) {
			$smarty->assign("error", "Error: failed to create payment in database, please contact support@adultsearch.com");
		} else {
			return system::go("/payment/pay?id={$payment_id}");
		}
	}
}
*/

$smarty->assign("account_id", $account_id);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/addfunds.tpl");

?>
