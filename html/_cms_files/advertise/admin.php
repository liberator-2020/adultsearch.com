<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!$account->isloggedin()) {
    $account->asklogin();
    return false;
}

if (!$account->isrealadmin()) {
    die("You have no privileges to display this page.");
    return false;
}

if (isset($_REQUEST["rotate"])) {
	rotate_index($_REQUEST["rotate"]);
	echo "<span class=\"alert alert-success\">Rotation request successfully added.</span><br />\n";
}

$last_rotation = "ERROR - no data !";
$res = $db->q("SELECT * FROM admin_operation_log WHERE code = 'last_cache_update' LIMIT 1");
if ($db->numrows($res)) {
	$row = $db->r($res);
	$last_rotation = "Index <strong>{$row["msg"]}</strong> at ".date("Y-m-d H:i:s T", $row["stamp"]);
}

?>
<h1>Advertise Admin</h1>
<?php echo "Last rotation: {$last_rotation}<br />\n"; ?>
<a href="/advertise/admin?rotate=advertise" class="btn btn-sm btn-info">Rotate advertise</a><br />
<br />
<a href="/advertise/admin?rotate=escorts" class="btn btn-sm btn-info">Rotate escorts</a><br />
