<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage"))
	die("Invalid access!");


//-----------------------
//new advertiser handling
if ($_REQUEST["action"] == "new") {
	if ($_REQUEST["submit"] == "Submit") {
		$errors = array();
		$username = preg_replace('/[^a-zA-Z0-9_\-.]/', '', $_REQUEST["username"]);
		$email = preg_replace('/[^a-zA-Z0-9_\-.@]/', '', $_REQUEST["email"]);
		$password = $_REQUEST["password"];
		$password2 = $_REQUEST["password2"];
		if (empty($username)) {
			$errors[] = "Username is mandatory!";
		}
		if (strlen($username) < 4) {
			$errors[] = "Username is too short!";
		}
		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			$errors[] = "Email format is invalid!";
		}
		if (!$password || !$password2) {
			$errors[] = "Please enter both passwords!";
		}
		if ($password != $password2) {
			$errors[] = "Passwords are different!";
		}
		if (strlen($password) < 6) {
			$errors[] = "Password is too short!";
		}
		$new_note = $_REQUEST["new_note"];

		if (empty($errors)) {
			$res = $db->q("INSERT INTO account 
							(username, email, email_register, password, approved, advertiser, created_stamp)
							VALUES 
							(?, ?, ?, ?, ?, ?, ?)",
							array($username, $email, $email, $password, 1, 1, time())
							);
			$account_id = $db->insertid($res);
			if (!$account_id) {
				$errors[] = "Error inserting new advertiser into database!";
			} else {
				$db->q("INSERT INTO advertise_budget (account_id, notes) VALUES (?, ?);", array($account_id, $new_note));
				return success_redirect("Advertiser #{$account_id} was succesfully saved.", "/advertise/advertiser?id={$account_id}");
			}
		}
		$smarty->assign("username", $username);
		$smarty->assign("email", $email);
		$smarty->assign("new_note", $new_note);
		$smarty->assign("errors", $errors);
	}

	$smarty->assign("new", true);
	$smarty->display(_CMS_ABS_PATH."/templates/advertise/advertiser_edit.tpl");
	return;
}

$account_id = intval($_REQUEST["id"]);
if (!$account_id) {
	echo "Advertiser account ID not specified!";
	return;
}

//-------------------------------
//get general info about advertiser
$res = $db->q("SELECT a.account_id, a.username, a.email, a.banned, a.ban_reason
					, ab.budget, ab.nickname, ab.name, ab.phone, ab.company, ab.contact, ab.notes
				FROM account a
				LEFT JOIN advertise_budget ab on ab.account_id = a.account_id
				WHERE a.account_id = ?", array($account_id));
if ($db->numrows($res) != 1) {
	echo "Advertiser account #{$account_id} not found!";
	return;
}
$row = $db->r($res);
$smarty->assign("advertiser", $row);
$email = $row["email"];
$banned = $row["banned"];
$ban_reason = $row["ban_reason"];
$notes = $row["notes"];
$budget = number_format($row["budget"], 2);
$has_ab_entry = false;		//has entry in advertise_budgte table ?
if ($row["budget"] != NULL)
	$has_ab_entry = true;


//------------------
//advertiser actions
if ($_REQUEST["action"] == "edit") {

	if ($banned)	
		return error_redirect("Account #{$account_id} is banned, you can't edit it", "/advertise/advertiser?id={$account_id}");

	if ($_REQUEST["submit"] == "Submit") {
		$name = $_REQUEST["name"];
		$phone = $_REQUEST["phone"];
		$company = $_REQUEST["company"];
		$nickname = $_REQUEST["nickname"];
		$new_note = trim($_REQUEST["new_note"]);

		if ($row["budget"] == NULL) {
			//entry in advertiser table not yet created, lets use insert
			$db->q("INSERT INTO advertise_budget (account_id, name, phone, company, nickname, notes) VALUES (?, ?, ?, ?, ?, ?)", 
				array($account_id, $name, $phone, $company, $nickname, $new_note));
		} else {
			//update
			if ($new_note)
				$notes = $notes."\n\n".$new_note;
			$db->q("UPDATE advertise_budget 
					SET name = ?, phone = ?, company = ?, nickname = ?, notes = ? 
					WHERE account_id = ?", 
					array($name, $phone, $company, $nickname, $notes, $account_id));
		}
		$aff = $db->affected();
		if ($aff == 1) {
			return success_redirect("Advertiser #{$account_id} was succesfully saved.", "/advertise/advertiser?id={$account_id}");
		} else {
			return error_redirect("Error while saving advertiser data. Please contact administrator.", "/advertise/advertiser?id={$account_id}");
		}
	} else {
		$smarty->display(_CMS_ABS_PATH."/templates/advertise/advertiser_edit.tpl");
		return;
	}
} else if ($_REQUEST["action"] == "addfunds") {

	//zach 2019005-08 remove this, let richlife add balances
	//if (!$account->isadmin())
	//	return error_redirect("You don't have permissions to modify advertiser's account balance", "/advertise/advertiser?id={$account_id}");

	if ($banned)	
		return error_redirect("Account #{$account_id} is banned, you can't add funds", "/advertise/advertiser?id={$account_id}");

	$wireId = $wireProcess = false;
	if (isset($_REQUEST["wire"])) {
		$wireId = $_REQUEST["wire"];
		$smarty->assign("wire_id", $wireId);
		$res = $db->q("SELECT aw.* FROM account_wire aw WHERE id = ? LIMIT 1", [$wireId]);
		if ($db->numrows($res) == 1) {
			$row_wire = $db->r($res);
			$smarty->assign("wire_row", $row_wire);
		}
		$wireId = $_REQUEST["wire"];
		if ($_REQUEST["wire_process"] == "1") {
			$wireProcess = true;
			$smarty->assign("wire_process", $wireProcess);
		}
	}

	$errors = array();
	if ($_REQUEST["submit"] == "Submit") {

		$amount = intval($_REQUEST["amount"]);
		$ads_limit = ($_REQUEST["ads_limit"] != "") ? intval($_REQUEST["ads_limit"]): null;
		$email_notify = intval($_REQUEST["email_notify"]);
		if ($amount) {
			if ($has_ab_entry) {
				$res = $db->q("UPDATE advertise_budget SET budget = budget + {$amount} WHERE account_id = ? LIMIT 1", array($account_id));
			} else {
				$res = $db->q("INSERT INTO advertise_budget (account_id, budget) VALUES (?, ?)", array($account_id, $amount));
			}
			$aff = $db->affected($res);
			if ($aff != 1) {
				reportAdmin("AS: advertise/advertiser/addfunds error", "Error updating budget for account_id '{$account_id}'", array("account_id" => $account_id, "amount" => $amount, "aff" => $aff));
				return error_redirect("Error updating account balance! Please contact administrator.", "/advertise/advertiser?id={$account_id}");
			} else {
				$now = time();

				//insert payment
				$res2 = $db->q("
					INSERT INTO payment 
					(created_stamp, account_id, author_id, amount, email, result, processor, trans_id)
					VALUES
					(?, ?, ?, ?, ?, ?, ?, ?)",
					[$now, $account_id, $account->getId(), $amount, null, "A", "manual", "manual"]
					);
				$payment_id = $db->insertid($res2);
				if (!$payment_id) {
					reportAdmin("AS: advertise/advertiser/addfunds error", "Error inserting payment into db", ["account_id" => $account_id, "amount" => $amount]);
					return error_redirect("Account balance successfully topped, but error inserting entry into payment table. Please contact administrator.", "/advertise/advertiser?id={$account_id}");
				}
				//insert payment item
				$res2 = $db->q("
						INSERT INTO payment_item
						(payment_id, type, created_stamp)
						VALUES
						(?, ?, ?)",
						[$payment_id, "advertise", $now]
						);
				$pi_id = $db->insertid($res2);
				if (!$pi_id) {
					reportAdmin("AS: advertise/advertiser/addfunds error", "Error inserting payment item into db", ["account_id" => $account_id, "amount" => $amount]);
					return error_redirect("Account balance successfully topped, but error inserting entry into payment item table. Please contact administrator.", "/advertise/advertiser?id={$account_id}");
				}
				//insert transaction
				$res2 = $db->q("
					INSERT INTO transaction
					(stamp, type, payment_id, trans_id, amount)
					VALUES
					(?, ?, ?, ?, ?)",
					[$now, "M", $payment_id, "manual", $amount]
					);
				$transaction_id = $db->insertid($res2);
				if (!$transaction_id) {
					reportAdmin("AS: advertise/advertiser/addfunds error", "Error inserting transaction into db", ["account_id" => $account_id, "amount" => $amount]);
					return error_redirect("Account balance successfully topped, but error inserting entry into transaction table. Please contact administrator.", "/advertise/advertiser?id={$account_id}");
				}
				file_log("advertise", "Addfunds: account_id={$account_id} amount={$amount} author_id={$account->getId()} payment_id={$payment_id} transaction_id={$transaction_id}");

				audit::log("ABU", "Add", $account_id, "Added {$amount}");

				$sql = "SELECT ab.budget FROM advertise_budget ab WHERE ab.account_id = '{$account_id}' LIMIT 1";
				$res = $db->q($sql);
				if ($db->numrows($res) != 1) {
					$errors[] = "Error: selecting budget information for account_id '{$account_id}'!";
					reportAdmin("AS: advertise/advertiser/addfunds error", "Error selecting budget information for account_id '{$account_id}'!", array("account_id" => $account_id, "amount" => $amount));
					return error_redirect("Account balance successfully topped, but error happened selecting new budget of advertiser. Please contact administrator.", "/advertise/advertiser?id={$account_id}");
				}

				$row = $db->r($res);
				$budget_new = number_format($row["budget"], 2);

				if ($amount > 0.01)
					advertise::budgetUpped($account_id);

				//changing ads_limit
				file_log("advertise", "Addfunds: Changing ads_limit to '{$ads_limit}' for account_id={$account_id}");
				$res = $db->q("UPDATE account SET ads_limit = ? WHERE account_id = ? LIMIT 1", [$ads_limit, $account_id]);

				if ($email_notify) {
					$ret = advertise::sendFundsAddedNotification($account_id, number_format($amount, 0, ".", ","), $email);
					if (!$ret) {
						//not critical, just send email to admin
						reportAdmin("AS: advertise/addfunds: error sending email notification", "Error sending addfunds email notification to client, account_id={$account_id}, amount={$amount}, email={$email}");
					}
				}

				//marking wire as processed
				if ($wireId && $wireProcess) {
					$res = $db->q("UPDATE account_wire SET status = 1 WHERE id = ? LIMIT 1", [$wireId]);
					if ($db->affected($res) != 1) {
						//not critical, just send email to admin
						reportAdmin("AS: advertise/addfunds: error marking wire as processed", "account_id={$account_id}, wire_id={$wireId}");
					}
				}

				return success_redirect("Balance of advertiser #{$account_id} was succesfully topped by \${$amount}, before budget was \${$budget}, new budget is \${$budget_new}.", "/advertise/advertiser?id={$account_id}");
			}
		} else {
			$errors[]= "You need to enter amount!";
		}
	}
	$smarty->assign("errors", $errors);
	$smarty->display(_CMS_ABS_PATH."/templates/advertise/advertiser_addfunds.tpl");
	return;
}

if ($_REQUEST["action"] == "budget_upped") {
	$affected_placements = advertise::budgetUpped($account_id);
	return success_redirect("budgetUpped affected {$affected_placements} placements.", "/advertise/advertiser?id={$account_id}");	
}

if ($_REQUEST["action"] == "budget_zero") {
	$affected_placements = advertise::budgetZero($account_id);
	return success_redirect("budgetZero affected {$affected_placements} placements.", "/advertise/advertiser?id={$account_id}");	
}


//-------------------------------
//get general info about advertiser
$res = $db->q("SELECT a.account_id, a.username, a.email, a.banned, a.ban_reason
					, ab.budget, ab.nickname, ab.name, ab.phone, ab.company, ab.contact, ab.notes
				FROM account a
				LEFT JOIN advertise_budget ab on ab.account_id = a.account_id
				WHERE a.account_id = ?", array($account_id));
$row = $db->r($res);
$smarty->assign("advertiser", $row);


//-----------------------------------------------
//get info about advertiser's budget changes
$budget_changes = array();
$res = $db->q("
	SELECT t.id, t.stamp, t.amount, t.trans_id 
    FROM transaction t
    INNER JOIN payment p on p.id = t.payment_id
    INNER JOIN payment_item pi on pi.payment_id = p.id
    WHERE p.account_id = ? AND pi.type = 'advertise' 
    ORDER BY t.stamp DESC 
    LIMIT 5
	",
	[$account_id]
	);
while ($row = $db->r($res)) {
	$stamp = $row["stamp"];
	$budget_changes[$stamp] = [
		"stamp" => $stamp,
		"total" => "\$".$row["amount"],
		"transaction_id" => $row["id"],
		];
}
$res = $db->q("SELECT * FROM advertiser_budget_snapshot WHERE account_id = ? ORDER BY day DESC LIMIT 5", array($account_id));
while ($row = $db->r($res)) {
	$stamp = strtotime($row["day"]);
	$budget_changes[$stamp] = array(
		"stamp" => $stamp,
		"balance" => "\$".number_format($row["budget"], 2),
		);
}
krsort($budget_changes);
//_darr($budget_changes);
$smarty->assign("budget_changes", $budget_changes);


//-----------------------------------------------
//get info about advertiser's campaigns
$res = $db->q("SELECT c.*, MAX(cs.flat_deal_status) as flat_deal_status, SUM(CASE WHEN cs.flat_deal_status = 1 THEN 1 ELSE 0 END) AS flat_deal_count
				FROM advertise_campaign c
				LEFT JOIN advertise_camp_section cs on cs.c_id = c.id
				WHERE c.account_id = ?
				GROUP BY c.id
				ORDER BY c.status DESC, c.id DESC",
				array($account_id)
				);
$paused = $live = $deleted = $total = 0;
$campaigns = array();
while ($row = $db->r($res)) {
	$total++;


	$campaign = array(
		"id" => $row["id"],
		"name" => $row["name"],
		"status" => $row["status"],
		"flat_deal_status" => $row["flat_deal_status"],
		"flat_deal_count" => $row["flat_deal_count"],
		);

	$campaigns[] = $campaign;

	if ($row["status"] == -1) {
		$deleted++;
		continue;
	} else if ($row["status"] == 0) {
		$paused++;
		continue;
	} else {
		$live++;
		continue;
	}
}

$smarty->assign("total", $total);
$smarty->assign("deleted", $deleted);
$smarty->assign("paused", $paused);
$smarty->assign("live", $live);
$smarty->assign("campaigns", $campaigns);


//-----------------------------------------------
//get info about advertiser's flat deal
$res = $db->q("SELECT fd.*, ase.nickname
				FROM advertise_flat_deal fd
				LEFT JOIN advertise_section ase on ase.id = fd.zone_id
				WHERE fd.account_id = ?
				ORDER BY fd.id DESC",
				[$account_id]
				);
$deals = [];
while ($row = $db->r($res)) {
	$deals[] = [
		"id" => $row["id"],
		"zone" => "#{$row["zone_id"]} - {$row["nickname"]}",
		"description" => $row["description"],
		"started" => $row["started_stamp"],
		"finished" => $row["finished_stamp"],
		];
}
$smarty->assign("deals", $deals);


include_html_head("js", "/js/advertise/jquery.flot.js");
//include_html_head("js", "/js/advertise/jquery.flot.pie.js");

$smarty->display(_CMS_ABS_PATH."/templates/advertise/advertiser.tpl");

?>
