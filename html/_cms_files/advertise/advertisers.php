<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $sphinx, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage"))
	die("Invalid access!");


//get sphinx advertise data
$sphinx->reset();
$sphinx->SetLimits(0, 1000, 1000);
$results = $sphinx->Query("", "advertise");
$total = $results["total_found"];
$result = $results["matches"];
$live_campaigns = array();
foreach ($results["matches"] as $match) {
	$account_id = $match["attrs"]["account_id"];
	$c_id = $match["attrs"]["c_id"];

	if (!array_key_exists($account_id, $live_campaigns)) {
		$live_campaigns[$account_id] = array($c_id => $c_id);
	} else {
		$lc_arr = $live_campaigns[$account_id];
		$lc_arr[$c_id] = $c_id;
		$live_campaigns[$account_id] = $lc_arr;
	}
}

//---------------------
//get advertisers

//filters
$where = "";
$params = array();
$account_id = intval($_REQUEST["account_id"]);
$type = $_REQUEST["type"];
if ($account_id) {
	$where .= " AND a.account_id = ?";
	$params[] = $account_id;
	$smarty->assign("account_id", $account_id);
}
if ($type) {
	if ($type == "agency") {
		$where .= " AND a.agency = 1";
	    $smarty->assign("type", "agency");
	} else if ($type == "advertiser") {
		$where .= " AND a.advertiser = 1";
	    $smarty->assign("type", "advertiser");
	}
}
if ($_REQUEST["username"]) {
	$username = preg_replace('/[^a-zA-Z0-9_\-]/', '', $_REQUEST["username"]);
	$where .= " AND a.username LIKE ?";
    $params[] = "%{$username}%";
    $smarty->assign("username", $username);
}
if ($_REQUEST["email"]) {
	$email = preg_replace('/[^a-zA-Z0-9_\-.@]/', '', $_REQUEST["email"]);
	$where .= " AND a.email LIKE ?";
    $params[] = "%{$email}%";
    $smarty->assign("email", $email);
}

$res = $db->q("SELECT a.*, ab.budget
				FROM account a
				LEFT JOIN advertise_budget ab on ab.account_id = a.account_id
				WHERE (a.advertiser = 1 OR a.agency  =1) AND a.deleted IS NULL {$where} 
				ORDER BY ab.budget DESC
				",
				$params);
$advertisers = array();
while ($row = $db->r($res)) {
	$account_id = $row["account_id"];
	$advertisers[] = array(
		"account_id" => $account_id,
		"agency" => $row["agency"],
		"advertiser" => $row["advertiser"],
		"username" => $row["username"],
		"email" => $row["email"],
		"password" => $row["password"],
		"budget" => number_format($row["budget"], 2),
		"live_campaigns" => intval(count($live_campaigns[$account_id])),
		);
}
$smarty->assign("advertisers", $advertisers);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/advertisers.tpl");

