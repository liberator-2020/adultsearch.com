<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;
$system = new system;

if( !($account_id = $account->isloggedin())) {
	$account->asklogin();
	return;
}

$id = intval($_GET["id"]);
if( !$id )
	$system->moved("/advertise/");

reportAdmin("AS: access to advertise/campaign", "", array());

$res = $db->q("select * from advertise_campaign where id = '$id' and account_id = '$account_id'");
if (!$db->numrows($res))
	$system->moved("/advertise/");
$row = $db->r($res);
$type = NULL;
if (in_array($row["type"], array("T", "B", "P"))) {
	$type = $row["type"];
	$smarty->assign("type", $row["type"]);
}

if (!empty($_POST)) {
	if ($type == "T" || $type == "P") {
		foreach($_POST as $key=>$value) {
			if (strncmp($key, "bid_", 4))
				continue;
			$s_id = substr($key, 4);
			$rex = $db->q("SELECT defaultbid FROM advertise_section WHERE id = ?", array($s_id));
			if ($db->numrows($rex)) {
				$rox = $db->r($rex);
				$defaultbid = $rox["defaultbid"];
			}
			if (!$defaultbid)
				$defaultbid = "0.2";
			if ($value < $defaultbid) {
				if (!isset($error))
					echo "<p class='error'>Mininum bid value must be \${$defaultbid} or higher (Your bid was \$$value)</p>";
				$error = true;
			} else {
				$db->q("UPDATE advertise_camp_section SET bid = ? WHERE c_id = ? AND s_id = ?", array($value, $id, $s_id));
				if ($db->affected() > 0 && !isset($done)) {
					echo "<p class='error'>Bids are updated.</p>";
					$done = true;
				}
			}
		}
	} else if ($type == "B") {
		foreach($_POST as $key=>$value) {
			if (strncmp($key, "cpm_", 4))
				continue;
			$s_id = substr($key, 4);
			$rex = $db->q("SELECT cpm_minimum FROM advertise_section WHERE id = ?", array($s_id));
			if ($db->numrows($rex)) {
				$rox = $db->r($rex);
				$cpm_minimum = $rox["cpm_minimum"];
			}
			if ($value < $cpm_minimum) {
				if (!isset($error))
					echo "<p class='error'>Mininum CPM value must be \${$cpm_minimum} or higher (Your bid was \$$value)</p>";
				$error = true;
			} else {
				$db->q("UPDATE advertise_camp_section SET cpm = ? WHERE c_id = ? AND s_id = ?", array($value, $id, $s_id));
				if ($db->affected > 0 && !isset($done)) {
					echo "<p class='error'>CPMs are updated.</p>";
					$done = true;
				}
			}
		}
	}

	if( isset($_POST["customurl"]) ) {
		$customurl = GetPostParam("customurl");
		$customurlid = intval($_POST["customurlid"]);
		$rex = $db->q("select id, name from advertise_camp_section where c_id = '$id' and id = '$customurlid'");
		if( !$db->numrows($rex) ) {
			reportAdmin("customurl update");
			die;
		}
		$rox = $db->r($rex);
		if( empty($customurl) ) {
			$db->q("update advertise_camp_section set customurl = '' where id = '$customurlid'");
			echo "<p class='error'>Custom URL for {$rox["name"]} is cleared</p>";	
		}
		else if( strlen($customurl) > 255 ) echo "<p class='error'>Custom URL has to have less than 256 characters</p>";
		else if( !strstr($customurl, "http://") && !strstr($customurl, "https://") ) echo "<p class='error'>Custom URL has to be valid. Please use either http:// 
or https:// protocols.</p>";
		else {
			$customurl_ = (($row["ad_https"]?"https://":"http://").$row["ad_url"]);
			$db->q("update advertise_camp_section set customurl = '$customurl' where id = '$customurlid'");
			echo "<p class='error'>Custom URL is updated for {$rox["name"]} to <b>$customurl</b></p>";
		}
	}

	if( isset($_POST["customurls"]) ) {
		$customurl = GetPostParam("customurls");
		if( !empty($customurl) && strlen($customurl) > 255 ) echo "<p class='error'>Custom URL has to have less than 256 characters</p>";
		else if( !empty($customurl) && (!strstr($customurl, "http://") && !strstr($customurl, "https://")) ) echo "<p class='error'>Custom URL has to be valid. 
Please use either http:// or https:// protocols. You typed: <b>$customurl</b></p>";
		else if( is_array($_POST["chk"]) ) {
			foreach($_POST["chk"] as $customurlid) {
				$rex = $db->q("select id, name from advertise_camp_section where c_id = '$id' and id = '$customurlid'");
				if( !$db->numrows($rex) ) {
					reportAdmin("customurl update");
					die;
				}
				$rox = $db->r($rex);
				if( empty($customurl) ) {
					$db->q("update advertise_camp_section set customurl = '' where id = '$customurlid'");
					echo "<p class='error'>Custom URL for {$rox["name"]} is cleared</p>";
				} else {
					$db->q("update advertise_camp_section set customurl = '$customurl' where id = '$customurlid'");
					echo "<p class='error'>Custom URL is updated for {$rox["name"]} to <b>$customurl</b></p>";
				}
			}
		}
	}
}

$res = $db->q("SELECT day(date), month(date), year(date)
				FROM advertise_camp_section s
				INNER JOIN `advertise_data` d on s.id = d.s_id
				WHERE s.c_id = '$id'
				ORDER BY date 
				LIMIT 1");
if( $db->numrows($res) ) {
	$row = $db->r($res);
	$dayFrom = $row[0];
	$monthFrom = $row[1];
	$yearFrom = $row[2];
} else {
	$dayFrom = date("d");
	$monthFrom = date("m");
	$yearFrom = date("Y");
}

$range_or_period = GetGetParam("range_or_period");
if( $range_or_period != 2 ) {
	$dayTo = date("d");
	$monthTo = date("m");
	$yearTo = date("Y");
	$period = $_GET["period"];

	$today_start = strtotime("00:00:00");
	$stamp_end = time() + 5;

	switch($period) {
		case 1:
			//last 7 days
			$time_sql = "and (date >= date_sub(curdate(), interval 6 day) or d.s_id is null)";
			$time_sql = "and date >= date_sub(curdate(), interval 6 day)";
			$stamp_start = strtotime("-6 day", $today_start);
			break;
		case 2:
			//yesterday
			$time_sql = "and date = date_sub(curdate(), interval 1 day)";
			$stamp_start = strtotime("-1 day", $today_start);
			$stamp_end = $today_start;
			break;
		case 3:
			//last 30 days
			$time_sql = "and date >= date_sub(curdate(), interval 29 day)";
			$stamp_start = strtotime("-29 day", $today_start);
			break;
		case 4:
			//this month
			getMonthRange($start, $end, 0);
			$time_sql = "and date >= '$start' and date <= '$end'";
			$stamp_start = mktime(0, 0, 0, date("n"), 1);
			$stamp_end = mktime(23, 59, 59, date("n"), date("t"));
			break;
		case 5:
			//last month
			getMonthRange($start, $end, 1);
			$time_sql = "and date >= '$start' and date <= '$end'";
			$stamp_start = strtotime("first day of previous month");
			$stamp_end = strtotime("last day of previous month") + 86400;
			break;
		default:
			//today
			$time_sql = "and date = curdate()";
			$stamp_start = $today_start;
			break;
	}
} else {
	$dayFrom = GetGetParam("dayFrom");
	$monthFrom = GetGetParam("monthFrom");
	$yearFrom = GetGetParam("yearFrom");

	$dayTo = GetGetParam("dayTo");
	$monthTo = GetGetParam("monthTo");
	$yearTo = GetGetParam("yearTo");

	$start_timer = mktime(0,0,1,$monthFrom,$dayFrom,$yearFrom);
	$end_timer = mktime(0,0,1, $monthTo, $dayTo, $yearTo);

	$start = date("Y-m-d", $start_timer);
	$end = date("Y-m-d", $end_timer);

	$time_sql = "and date >= '$start' and date <= '$end'";

	$stamp_start = $start_timer;
	$stamp_end = $end_timer;

	if( $start_timer > $end_timer )
		$smarty->assign("report_error", "Wrong date selection.");
}

$stamp_time_sql = "and stamp >= '{$stamp_start}' and stamp <= '{$stamp_end}'";

if ($type == "B") {
	$res = $db->q("SELECT s.*, 
					ads.name as section_name, ads.currentbid, ads.external, ads.defaultbid, ads.cpm_minimum, 
					(select sum(d.impression) from advertise_data d where s.id = d.s_id $time_sql) as impression,
					sum(bi.cost) as cost
				FROM advertise_camp_section s 
				INNER JOIN advertise_section ads on s.s_id = ads.id
				LEFT JOIN advertise_campaign c on s.c_id = c.id
				LEFT JOIN advertise_banner_impression bi on bi.cs_id = s.id {$stamp_time_sql}
				WHERE s.c_id = '$id' and s.`show` = 1 and s.haschild = 0
				GROUP BY s.id");
} else {
	$res = $db->q("SELECT s.*, 
					ads.name as section_name, ads.currentbid, ads.external, ads.defaultbid, ads.cpm_minimum, 
					sum(d.impression) impression, sum(d.click) as click, sum(d.cost) as cost
				FROM advertise_camp_section s 
				INNER JOIN advertise_section ads on s.s_id = ads.id
				LEFT JOIN advertise_campaign c on s.c_id = c.id
				LEFT JOIN advertise_data d on (s.id = d.s_id $time_sql)
				WHERE s.c_id = '$id' and s.`show` = 1 and s.haschild = 0
				GROUP BY s.id");
}

$total_i = $total_c = $total_cost = $total_a = 0;
while ($row = $db->r($res)) {
	$campaign[] = array(
		"id" => $row["id"],
		"s_id" => $row["s_id"],
		"section" => $row["section"],
		//"name" => $row["name"],
		"name" => $row["section_name"],
		"bid" => number_format($row["bid"], ($type == "P") ? 3 : 2),
		"cpm" => number_format($row["cpm"], 2),
		"impression" => number_format($row["impression"]),
		"click" => number_format($row["click"]),
		"cost" => number_format($row["cost"], 2),
		"acpc" => number_format($row["cost"]/$row["click"], strstr($row["section"],'popup')?3:2),
		"budget_available" => $row["budget_available"],
		"approved" => $row["approved"],
		"status" => $row["status"],
		"visible" => $row["currentbid"]>$row["bid"]?0:1,
		"mbid" => number_format($row["currentbid"], ($type == "P") ? 3 : 2),
		"customurl" => !empty($row["customurl"])?true:false,
		"reason" => $row["reason"],
		"external" => $row["external"],
		"defaultbid" => number_format($row["defaultbid"], ($type == "P") ? 3 : 2),
		"cpm_minimum" => number_format($row["cpm_minimum"], 2),
	);
	$total_i += $row["impression"];
	$total_c += $row["click"];
	$total_cost += $row["cost"];
}
$smarty->assign("campaign", $campaign);

$smarty->assign("total_i", number_format($total_i));
$smarty->assign("total_c", number_format($total_c));
$smarty->assign("total_cost", number_format($total_cost, 2));
$smarty->assign("total_a", number_format($total_cost/$total_c,2));

$smarty->assign("dayFrom", $dayFrom);
$smarty->assign("monthFrom", $monthFrom);
$smarty->assign("yearFrom", $yearFrom);
$smarty->assign("dayTo", $dayTo);
$smarty->assign("monthTo", $monthTo);
$smarty->assign("yearTo", $yearTo);
$smarty->assign("id", $id);
$smarty->assign("period", $_GET["period"]);
$smarty->assign("range_or_period", $_GET["range_or_period"]);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/detail.tpl");

$smarty->assign("nobanner", true);
global $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

?>
