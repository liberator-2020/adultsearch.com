<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!account::ensure_admin())
    die("Invalid access");

$c_id = intval($_REQUEST["id"]);
if (!$c_id) {
	echo "Campaign ID not specified!";
	return;
}

//----------------------------------------------
//get general info about campaign and advertiser
$campaign = campaign::findOneById($c_id);
if (!$campaign)
	die("Campaign #{$c_id} not found!");
/*
$res = $db->q("SELECT c.*, a.email 
				FROM advertise_campaign c 
				LEFT JOIN account a on c.account_id = a.account_id
				WHERE c.id = ?", array($c_id));
if ($db->numrows($res) != 1) {
	echo "Campaign #{$c_id} not found!";
	return;
}
$row = $db->r($res);
$smarty->assign("campaign", $row);
*/
$smarty->assign("campaign", $campaign);

//-----------------------------------------------
//get info about campaign placements in this zone
$res = $db->q("SELECT cs.id as cs_id, cs.bid, cs.cpm, cs.status, cs.approved, cs.budget_available, cs.flat_deal_status, 
					c.id as c_id, c.name as c_name, c.status as c_status, c.account_id,  
					s.id as s_id, s.name as s_name,
					a.email, 
					ab.budget
				FROM advertise_camp_section cs
				INNER JOIN advertise_campaign c on cs.c_id = c.id
				INNER JOIN advertise_section s on cs.s_id = s.id
				INNER JOIN account a on c.account_id = a.account_id
				LEFT JOIN advertise_budget ab on ab.account_id = c.account_id
				WHERE cs.c_id = ? AND cs.show = 1 AND c.status <> -1",
				array($c_id)
				);

$paused = $not_approved = $out_of_budget = $live = $total = 0;
$placements = array();
$cs_ids_live = array();
while ($row = $db->r($res)) {
	$total++;

	$placement = array(
		"cs_id" => $row["cs_id"],
		"cs_status" => $row["status"],
		"cs_approved" => $row["approved"],
		"cs_budget_available" => $row["budget_available"],
		"cs_flat_deal_status" => $row["flat_deal_status"],
		"c_id" => $row["c_id"],
		"c_name" => $row["c_name"],
		"s_id" => $row["s_id"],
		"s_name" => $row["s_name"],
		"account_id" => $row["account_id"],
		"email" => $row["email"],
		"test_link" => advertise::get_test_link($row["s_id"], $row["c_id"]),
		);

	$cpm = $row["cpm"];
	$budget = $row["budget"];
	$c_status = $row["c_status"];

	if (($cpm > $budget) && $c_status == 0) {
		$out_of_budget++;
	}

	$placements[] = $placement;
	$cs_ids_live[] = $row["cs_id"];
}

$smarty->assign("total", $total);
$smarty->assign("not_approved", $not_approved);
$smarty->assign("out_of_budget", $out_of_budget);
$smarty->assign("paused", $paused);
$smarty->assign("live", $live);
$smarty->assign("placements", $placements);

//-------------------
//submission handling
if ($_REQUEST["submit"] == "Submit") {

	$error = null;
	$new_campaign_name = $_REQUEST["new_campaign_name"];
	$status = intval($_REQUEST["status"]);

	//die("new_campaign_name={$new_campaign_name}, status={$status}");
	file_log("advertise", "campaign_clone: submission, c_id={$c_id}, new_campaign_name='{$new_campaign_name}, status={$status}");
	if ($new_campaign_name == $campaign->getName()) {
		$error = "New campaign name needs to be different than the original one";
	} else {

		$ret = $campaign->duplicate($new_campaign_name, $status, $error);
		if ($ret) {
			return success_redirect("Campaign #{$c_id} successfully cloned into campaign #{$ret}", "/advertise/campaign_detail?id={$ret}");
		} else {
			return error_redirect("Error while cloning campaign #{$c_id}: '{$error}'", "");
		}
	}
}

$smarty->assign("campaign", [
	"id" => $campaign->getId(),
	"name" =>$campaign->getName(),
	]);


$smarty->display(_CMS_ABS_PATH."/templates/advertise/campaign_clone.tpl");

?>
