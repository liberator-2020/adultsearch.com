<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage"))
	die("Invalid access!");


$c_id = intval($_REQUEST["id"]);
if (!$c_id) {
	echo "Campaign ID not specified!";
	return;
}

//----------------------------------------------
//get general info about campaign and advertiser
$res = $db->q("SELECT c.*, a.email 
				FROM advertise_campaign c 
				LEFT JOIN account a on c.account_id = a.account_id
				WHERE c.id = ?", array($c_id));
if ($db->numrows($res) != 1) {
	echo "Campaign #{$c_id} not found!";
	return;
}
$row = $db->r($res);
$smarty->assign("campaign", $row);


//-----------------------------------------------
//get info about campaign placements in this zone
$res = $db->q("SELECT cs.id as cs_id, cs.bid, cs.cpm, cs.status, cs.approved, cs.budget_available, cs.flat_deal_status, cs.cpm, 
					c.id as c_id, c.name as c_name, c.status as c_status, c.account_id,  
					s.id as s_id, s.name as s_name,
					a.email, 
					ab.budget
				FROM advertise_camp_section cs
				INNER JOIN advertise_campaign c on cs.c_id = c.id
				INNER JOIN advertise_section s on cs.s_id = s.id
				INNER JOIN account a on c.account_id = a.account_id
				LEFT JOIN advertise_budget ab on ab.account_id = c.account_id
				WHERE cs.c_id = ? AND cs.show = 1 AND c.status <> -1",
				array($c_id)
				);

$paused = $not_approved = $out_of_budget = $live = $total = 0;
$placements = array();
$cs_ids_live = array();
while ($row = $db->r($res)) {
	$total++;

	$placement = array(
		"cs_id" => $row["cs_id"],
		"cs_status" => $row["status"],
		"cs_approved" => $row["approved"],
		"cs_budget_available" => $row["budget_available"],
		"cs_flat_deal_status" => $row["flat_deal_status"],
		"c_id" => $row["c_id"],
		"c_name" => $row["c_name"],
		"s_id" => $row["s_id"],
		"s_name" => $row["s_name"],
		"account_id" => $row["account_id"],
		"email" => $row["email"],
		"bid" => $row["bid"],
		"cpm" => $row["cpm"],
		"test_link" => advertise::get_test_link($row["s_id"], $row["c_id"]),
		);

	$cpm = $row["cpm"];
	$budget = $row["budget"];
	$c_status = $row["c_status"];

	if (($cpm > $budget) && $c_status == 0) {
		$out_of_budget++;
	}

	$placements[] = $placement;
	$cs_ids_live[] = $row["cs_id"];
}

$smarty->assign("total", $total);
$smarty->assign("not_approved", $not_approved);
$smarty->assign("out_of_budget", $out_of_budget);
$smarty->assign("paused", $paused);
$smarty->assign("live", $live);
$smarty->assign("placements", $placements);


//-------------------
//get last week stats
$today_start = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
$week_ago_start = $today_start - (6 * 86400);
$month_ago_start = $today_start - (30 * 86400);
$week_ago_date = date("Y-m-d", $week_ago_start);
$month_ago_date = date("Y-m-d", $month_ago_start);
$day_indexes = array();
for($i = 0; $i < 31; $i++) {
	//$day_indexes[date("M-d", $week_ago_start+($i * 86400))] = $i;
	$day_indexes[date("M-d", $month_ago_start+($i * 86400))] = $i;
}
//_darr($day_indexes);
$smarty->assign("day_indexes", $day_indexes);
$smarty->assign("day_legend", array_flip($day_indexes));

$impressions = array();
$costs = array();
if (!empty($cs_ids_live)) {
	$res = $db->q("SELECT ad.s_id as cs_id, DATE_FORMAT(ad.`date`,'%b-%d') as day, SUM(ad.impression) as impression, ad.c_id, ac.name as c_name, ad.realsid, ads.name as s_name
					FROM advertise_data ad
					INNER JOIN advertise_campaign ac on ac.id = ad.c_id
					LEFT JOIN advertise_section ads on ads.id = ad.realsid
					WHERE ad.`date` >= ? AND ad.s_id IN (".implode(",", $cs_ids_live).")
					GROUP BY ad.s_id, DATE_FORMAT(ad.`date`,'%b-%d')",
					array($month_ago_date)
					);
	while ($row = $db->r($res)) {
		$cs_id = $row["cs_id"];
		$day = $row["day"];
		$impression = $row["impression"];
		$c_name = "#{$row["c_id"]} - {$row["c_name"]}";
		$s_name = "#{$row["realsid"]} - {$row["s_name"]}";
		if (array_key_exists($cs_id, $impressions)) {
			$a = $impressions[$cs_id]["data"];
			$a[$day] = $impression;
			$impressions[$cs_id]["data"] = $a;
		} else {
			$impressions[$cs_id] = array(
				"cs_id" => $cs_id, 
				"c_name" => $c_name, 
				"s_name" => $s_name, 
				"data" => array($day => $impression)
				);
		}
	}

	$res = $db->q("SELECT bi.cs_id, DATE_FORMAT(FROM_UNIXTIME(bi.stamp),'%b-%d') as day, SUM(bi.cost) as cost, bi.c_id, ac.name as c_name, acs.s_id, ads.name as s_name
					FROM advertise_banner_impression bi
					INNER JOIN advertise_campaign ac on ac.id = bi.c_id
					INNER JOIN advertise_camp_section acs on acs.id = bi.cs_id
					INNER JOIN advertise_section ads on ads.id = acs.s_id
					WHERE bi.stamp >= ? AND bi.cs_id IN (".implode(",", $cs_ids_live).")
					GROUP BY bi.cs_id, DATE_FORMAT(FROM_UNIXTIME(bi.stamp),'%b-%d')
					",
					array($month_ago_start)
					);
	while ($row = $db->r($res)) {
		$cs_id = $row["cs_id"];
		$day = $row["day"];
		$cost = $row["cost"];
		$c_name = "#{$row["c_id"]} - {$row["c_name"]}";
		$s_name = "#{$row["s_id"]} - {$row["s_name"]}";
		if (array_key_exists($cs_id, $costs)) {
			$a = $costs[$cs_id]["data"];
			$a[$day] = $cost;
			$costs[$cs_id]["data"] = $a;
		} else {
			$costs[$cs_id] = array(
				"cs_id" => $cs_id,
				"c_name" => $c_name,
				"s_name" => $s_name,
				"data" => array($day => $cost)
				);;
		}
	}
}
//_darr($impressions);
//_darr($costs);
$smarty->assign("impressions", $impressions);
$smarty->assign("costs", $costs);

include_html_head("js", "/js/advertise/jquery.flot.js");
//include_html_head("js", "/js/advertise/jquery.flot.pie.js");

$smarty->display(_CMS_ABS_PATH."/templates/advertise/campaign_detail.tpl");

?>
