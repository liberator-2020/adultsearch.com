<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";
$system = new system;

if (!$account->isloggedin()) {
	$account->asklogin();
	return false;
}

if (!$account->isadmin()) {
	die("You have no privileges to display this page.");
	return false;
}

$c_id = intval($_REQUEST["id"]);
if (!$c_id) {
	echo "Campaign ID not specified!";
	return;
}

$campaign = campaign::findOneById($c_id);
if (!$campaign) {
	echo "Advertise campaign #{$c_id} not found!";
	return;
}

if ($_REQUEST["submit"] == "Submit") {

	$errors = [];

	$status = intval($_REQUEST["status"]);
	if (!in_array($status, array(-1,0,1))) {
		$errors[] = "Campaign status not valid !";
	} else {
		$campaign->setStatus($status);
	}

	if (empty($errors)) {
		$ret = $campaign->update();
		if ($ret) {
			return success_redirect("Campaign #{$c_id} was succesfully saved.", "/advertise/campaign_detail?id={$c_id}");
		} else {
			return error_redirect("Error while saving campaign data. Please contact administrator.", "/advertise/campaign_detail?id={$c_id}");
		}
	} else {
		$smarty->assign("errors", $errors);
	}
}

$smarty->assign("campaign", array(
	"id" => $campaign->getId(),
	"name" => $campaign->getName(),
	"status" => $campaign->getStatus(),
	)
);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/campaign_edit.tpl");

?>
