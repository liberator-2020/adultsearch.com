<?php

function init() {
	global $smarty;
	$smarty->assign("noshare", true);
	$smarty->assign("nobanner", true);
}

function get_date_stamp_range($default_period = 0) {
	global $smarty;

	$range_or_period = intval($_REQUEST["range_or_period"]);
	if (!isset($_REQUEST["period"]))
		$period = $default_period;
	else
		$period = intval($_REQUEST["period"]);
	$dayFrom = intval($_REQUEST["dayFrom"]);
	$monthFrom = intval($_REQUEST["monthFrom"]);
	$yearFrom = intval($_REQUEST["yearFrom"]);
	$dayTo = intval($_REQUEST["dayTo"]);
	$monthTo = intval($_REQUEST["monthTo"]);
	$yearTo = intval($_REQUEST["yearTo"]);
	if ($dayFrom == "")
		$dayFrom = "1";
	if ($dayTo == "")
		$dayTo = date("j");
	if ($monthFrom == "")
		$monthFrom = date("n");
	if ($monthTo == "")
		$monthTo = date("n");
	if ($yearFrom == "")
		$yearFrom = date("Y");
	if ($yearTo == "")
		$yearTo = date("Y");

	$smarty->assign("dayFrom", $dayFrom);
	$smarty->assign("monthFrom", $monthFrom);
	$smarty->assign("yearFrom", $yearFrom);
	$smarty->assign("dayTo", $dayTo);
	$smarty->assign("monthTo", $monthTo);
	$smarty->assign("yearTo", $yearTo);
	$smarty->assign("id", $id);
	$smarty->assign("period", $period);
	$smarty->assign("range_or_period", $_REQUEST["range_or_period"]);

	$result = array();
	$today_start = strtotime("00:00:00");

	if ($range_or_period != 2) {
		switch($period) {
			case 1:
				//last 7 days
				$result["stamp_start"] = $today_start - (6*86400);
				break;
			case 2:
				//yesterday
				$result["stamp_start"] = $today_start - 86400;
				$result["stamp_end"] = $today_start - 1;
				break;
			case 3:
				//last 30 days
				$result["stamp_start"] = $today_start - (29*86400);
				break;
			case 4:
				//this month
				$result["stamp_start"] = mktime(0, 0, 0, date("n"), 1);
				$result["stamp_end"] = mktime(23, 59, 59, date("n"), date("t"));
				break;
			case 5:
				//last month
				$result["stamp_start"] = strtotime("first day of previous month");
				$result["stamp_end"] = strtotime("last day of previous month") + 86400;
				break;
			default:
				//today
				$result["stamp_start"] = $today_start;
				break;
		}

		if (!array_key_exists("stamp_end", $result))
			$result["stamp_end"] = time() + 1;

		$result["date_start"] = date("Y-m-d", $result["stamp_start"]);
		$result["date_end"] = date("Y-m-d", $result["stamp_end"]);

		return $result;
	}

	$result["stamp_start"] = mktime(0, 0, 0, $monthFrom, $dayFrom, $yearFrom);
	$result["stamp_end"] = mktime(23, 59, 59, $monthTo, $dayTo, $yearTo);
	if ($result["stamp_start"] > $result["stamp_end"])
		return false;
	$result["date_start"] = date("Y-m-d", $result["stamp_start"]);
	$result["date_end"] = date("Y-m-d", $result["stamp_end"]);

	return $result;
}


?>
