<?php

global $db, $account, $smarty;

if( !($account_id = $account->isloggedin())) {
	die("session is expired.");
}

$sid = intval($_GET["id"]);
if( !$sid ) die("Wrong Attempt");

$res = $db->q("
	SELECT s.customurl, s.name, c.ad_https, c.ad_url 
	FROM advertise_camp_section s 
	INNER JOIN advertise_campaign c on c.id = s.c_id 
	WHERE s.id = ? AND c.account_id = ?",
	[$sid, $account_id]
	);
if (!$db->numrows($res)) {
	reportAdmin("AS: customurl");
	die("Error! Our technical team has been notified!");
}
$row = $db->r($res); 

$customurl = $row["customurl"];
$orjurl = (($row["ad_https"]?"https://":"http://").$row["ad_url"]);

$smarty->assign("name", $row["name"]);
$smarty->assign("customurl", $customurl);
$smarty->assign("id", $sid);
$smarty->assign("orjurl", $orjurl);
$smarty->display(_CMS_ABS_PATH."/templates/advertise/advertise_customurl.tpl");
die;

?>
