<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage"))
	die("Invalid access!");


//filters
$where = "";
$params = [];
$zone_group_id = $zone_type = NULL;
if ($_REQUEST["zone_type"]) {
	$zone_type = $_REQUEST["zone_type"];
	$where .= " AND ads.type = ?";
	$params[] = $zone_type;
	$smarty->assign("zone_type", $zone_type);
}
if ($_REQUEST["zone_group_id"]) {
	$zone_group_id = intval($_REQUEST["zone_group_id"]);
	$where .= " AND ads.group_id = ?";
	$params[] = $zone_group_id;
	$smarty->assign("zone_group_id", $zone_group_id);
}

$res = $db->q("
	SELECT ads.id as zone_id, ads.type as zone_type, ads.nickname as zone_nickname, ads.name as zone_name
	FROM advertise_section ads
	LEFT JOIN advertise_zone_group g on g.id = ads.group_id
	WHERE ads.type IN ('B', 'P', 'A', 'L') AND ads.status > 0 {$where}
	ORDER BY g.orden ASC, ads.type ASC, ads.id ASC
	",
	$params
	);
$zones = array();
while ($row = $db->r($res)) {

	//zone
	$zone_id = $row["zone_id"];
	if ($row["zone_nickname"])
		$zone_label = $row["zone_nickname"];
	else
		$zone_label = $row["zone_name"];
	$zone = [
		"zone_id" => $row["zone_id"],
		"zone_type" => $row["zone_type"],
		"zone_label" => $zone_label,
		];


	//find deals on this zone
	$res2 = $db->q("
		SELECT fd.*
			, a.username, a.email, ab.nickname, ab.company
		FROM advertise_flat_deal fd
		LEFT JOIN account a on a.account_id = fd.account_id
		LEFT JOIN advertise_budget ab on ab.account_id = a.account_id
		LEFT JOIN advertise_deal_zone adz on adz.deal_id = fd.id
		WHERE fd.status = 1 AND ( fd.zone_id = ? OR adz.zone_id = ?)
		GROUP BY fd.id
		",
		[$zone_id, $zone_id]
		);
	if ($db->numrows($res2)) {
		//this zone has flat deal(s)
	
		while ($row2 = $db->r($res2)) {
			$deal = deal::withRow($row2);
			if (!$deal)
				die("Cant fetch deal!");

			$fd_id = $row2["id"];

			$occupied = "flat_deal";

			//advertiser
			$advertiser_id = $row2["account_id"];
			$advertiser = ($row2["nickname"]) ? $row2["nickname"] : $row2["email"];
			if ($row2["company"])
				$advertiser .= "<br />({$row2["company"]})";

			//deal start
			if ($row2["from_stamp"])
				$deal_start = date("M j", $row2["from_stamp"]);
			else if ($row2["created"])
				$deal_start = date("M j", $row2["created"]);

			//volume
			if ($row2["type"] == "ZI") {
				$volume = $row2["impressions_deal"];
				$volume_left = $row2["impressions_left"];
			} else if ($row2["type"] == "ZP") {
				$volume = $row2["pops_deal"];
				$volume_left = $row2["pops_left"];
			} else {
				$volume = "?";
				$volume_left = "?";
			}
			if (is_numeric($volume) && $volume > 1000000)
				$volume = ($volume/1000000)."M";
			else if (is_numeric($volume) && $volume > 1000)
				$volume = ($volume/1000)."K";

			//paid
			$paid = $row2["paid"];
			if (ctype_digit($paid) && $paid > 1000)
				$paid = ($paid/1000)."K";

			//deal_end
			$seconds_left = $deal->getSecondsLeft();
			$deal_end = date("M j", time() + $seconds_left);
			$time_left = time_to_human($seconds_left);

			if ($row2["next_deal_id"]) {
				$deal_end = $deal_end."<br />({$time_left})<br /><span style=\"color: green;\">next deal #{$row2["next_deal_id"]}</span>";
			} else {
				$left_style = "";
				if ($seconds_left < 3*86400)
					$left_style = "color: red; font-weight: bold;";
				else if ($seconds_left < 31*86400)
					$left_style = "color: orange; font-weight: bold;";
				$deal_end = $deal_end."<br /><span style=\"color: #aaa;\">(<span style=\"{$left_style}\">{$time_left}</span>)</span>";
			}

			$zone["occupied"] = "flat_deal";
			$zone["id"] = $fd_id;
			$zone["advertiser_id"] = $advertiser_id;
			$zone["advertiser"] = $advertiser;
			$zone["deal_type"] = $row2["type"];
			$zone["deal_start"] = $deal_start;
			$zone["volume"] = $volume;
			$zone["volume_left"] = $volume_left;
			$zone["paid"] = $paid;
			$zone["deal_end"] = $deal_end;
			$zone["next_deal_id"] = $row2["next_deal_id"];

			$zones[] = $zone;
		}

	} else {
		//this zone is in the bidding system
		$occupied = "system";

		$res2 = $db->q("SELECT COUNT(DISTINCT ac.account_id) as cnt_advertisers
				FROM advertise_data af
				INNER JOIN advertise_campaign ac on ac.id = af.c_id
				WHERE af.realsid = ? AND af.date >= ?",
				array($zone_id, date("Y-m-d"))
				);
		$row2 = $db->r($res2);
		$cnt_advertisers = $row2["cnt_advertisers"];

		$zone["occupied"] = "system";
		$zone["cnt_advertisers"] = $cnt_advertisers;
		
		$zones[] = $zone;
	}

}
//_darr($zones);
$smarty->assign("zones", $zones);

//zone groups
$zone_groups = array();
$res = $db->q("SELECT * FROM advertise_zone_group");
while ($row = $db->r($res)) {
		$zone_groups[$row["id"]] = $row["name"];
}
$smarty->assign("zone_groups", $zone_groups);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/dashboard.tpl");

?>

