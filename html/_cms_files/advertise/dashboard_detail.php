<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage"))
	die("Invalid access!");


//-------------------
//export or display ?
$export = false;
if ($_REQUEST["export"] == 1)
	$export = true;


//-------
//filters
$where = "";
$params = [];
$zone_group_id = $zone_type = NULL;


$fdfree = "";
//this filter allows us to select:
// - "free" - only free zones (zones with no advertising
// - "nfd" - no flat deals - zones with no active flat deals
// - "" - all zones - this is by default
if ($_REQUEST["fdfree"] == "free") {
	$fdfree = "free";
} else if ($_REQUEST["fdfree"] == "nfd") {
	$fdfree = "nfd";
}


if ($export) {
	$fp = fopen(_CMS_ABS_PATH.'/tmp/export/dashboard_detail.csv', 'w');
}


//pre-select all placements with flat_deal_status flag set, this will be later compared with active flat deals to make sure there is no mismatch (like non-active flat deal but placement is still set as flat deal)
$fdpza = [];	//fdpza stands for flat_deal_placements_zones_accounts
$res = $db->q("
	SELECT cs.id, c.account_id, cs.s_id
	FROM advertise_camp_section cs
	INNER JOIN advertise_campaign c on c.id = cs.c_id
	WHERE cs.flat_deal_status = 1", [] );
$nr = $db->numrows($res);
while ($row = $db->r($res)) {
	$cs_id = $row["id"];
	$s_id = $row["s_id"];
	$account_id = $row["account_id"];
	if (!array_key_exists($s_id, $fdpza)) {
		$fdpza[$s_id] = [$account_id];
	} else {
		$arr = $fdpza[$s_id];
		if (!in_array($account_id, $arr)) {
			$arr[] = $account_id;
			$fdpza[$s_id] = $arr;
		}
	}
}

$deals = array();
$res = $db->q("SELECT ads.id, ads.type, ads.name, ads.nickname, ads.cpm_minimum, ads.exclusive_access, ads.volume, ads.mobile, ads.backuphtml
					, ea.email as advertiser_email, eab.name as advertiser_name, eab.nickname as advertiser_nickname
					, ast.c as c_today, ast.backup as backup_today, asy.c as c_yesterday, asy.backup as backup_yesterday
				FROM advertise_section ads
				LEFT JOIN advertise_zone_group g on g.id = ads.group_id
				LEFT JOIN account ea on ea.account_id = ads.exclusive_access
				LEFT JOIN advertise_budget eab on eab.account_id = ea.account_id
				LEFT JOIN advertise_stat ast on ast.s_id = ads.id and ast.`date` = CURDATE()
				LEFT JOIN advertise_stat asy on asy.s_id = ads.id and asy.`date` = SUBDATE(CURDATE(), 1)
				WHERE ads.type IN ('B', 'P', 'A', 'L') AND ads.status > 0 {$where}
				ORDER BY g.orden ASC, ads.type ASC, ads.id ASC
				",
				$params);

while ($row = $db->r($res)) {

	$id = $row["id"];
	$type = $row["type"];
	$name = $row["name"];
	$nickname = $row["nickname"];
	$exclusive_access = $row["exclusive_access"];
	$backuphtml = $row["backuphtml"];
	$advertiser_email = $row["advertiser_email"];
	$advertiser_name = $row["advertiser_name"];
	$advertiser_nickname = $row["advertiser_nickname"];
	$c_today = $row["c_today"];
	$backup_today = $row["backup_today"];
	$c_yesterday = $row["c_yesterday"];
	$backup_yesterday = $row["backup_yesterday"];
	//...

	$system = $system_html = "";
	$free = true;
	$exclusive_access_account_ids = [];
	$flat_deal_account_ids = [];
	$warnings = "";
	//...

	//determine bidding system or exclusive access
	if ($type != "A") {
		if (!$exclusive_access) {
			$system = $system_html = "bidding system";
		} else if ($exclusive_access == intval($exclusive_access)) {
			//only one account in exclusive access, so we should have filled $advertiser_email and $advertiser_nickname variables properly
			$exclusive_access_account_ids[] = $exclusive_access;
			$system = $system_html = "reserved for: ";
			if ($advertiser_nickname) {
				$system_html .= "<span title=\"{$advertiser_email}\">{$advertiser_nickname}</span>";
			} else if ($advertiser_name) {
				$system_html .= "<span title=\"{$advertiser_email}\">{$advertiser_name}</span>";
			} else {
				$system_html .= $advertiser_email;
			}
			$system .= $advertiser_email;
			$system_html .= "<a href=\"/advertise/advertiser?action=edit&id={$exclusive_access}\" class=\"btn btn-default btn-xs\">edit</a>";
		} else if (($pos = strpos($exclusive_access, ",")) !== false) {
			//there is comma in exclusive access, probably multiple accounts in exclusive access
			$system = $system_html = "reserved for: ";
			$eas = explode(",", $exclusive_access);
			foreach ($eas as $ea) {
				if (!in_array($ea, $exclusive_access_account_ids))
					$exclusive_access_account_ids[] = $ea;
				$res2 = $db->q("
					SELECT ea.email, eab.name, eab.nickname 
					FROM account ea 
					LEFT JOIN advertise_budget eab on eab.account_id = ea.account_id
					WHERE ea.account_id = ?",
					[$ea]);
				if ($db->numrows($res2) != 1)
					die("Error: can't fetch advertiser for account_id = '{$ea}' !");
				$row2 = $db->r($res2);
				$system_html .= "<br />";
				if ($row2["nickname"])
					$system_html .= "<span title=\"{$row2["email"]}\">{$row2["nickname"]}</span>";
				else if ($row2["name"])
					$system_html .= "<span title=\"{$row2["email"]}\">{$row2["name"]}</span>";
				else
					$system_html .= $row2["email"];
				$system .= $row2["email"].", ";
				$system_html .= "<a href=\"/advertise/advertiser?action=edit&id={$ea}\" class=\"btn btn-default btn-xs\">edit</a>";
			} 
		}
	}

	//check flat deals on this zone
	$flat_deals = $flat_deals_html = "";
	$res2 = $db->q("SELECT fd.*
		FROM advertise_flat_deal fd
		INNER JOIN advertise_deal_zone adz on adz.deal_id = fd.id
		LEFT JOIN account a on a.account_id = fd.account_id
		LEFT JOIN advertise_budget ab on ab.account_id = a.account_id
		WHERE adz.zone_id = ? AND fd.status = 1",
		[$id]);
	if ($db->numrows($res2)) {
	   while ($row2 = $db->r($res2)) {
			$flat_deal_account_ids[] = $row2["account_id"];
			$free = false; 
			$flat_deals .= "#{$row2["id"]},";
			$flat_deals_html .= "#{$row2["id"]}<br />";
		}
	} else {
		$res2 = $db->q("SELECT fd.*
			FROM advertise_flat_deal fd
			LEFT JOIN account a on a.account_id = fd.account_id
			LEFT JOIN advertise_budget ab on ab.account_id = a.account_id
			WHERE fd.zone_id = ? AND fd.status = 1",
			[$id]);
		if ($db->numrows($res2)) {
			while ($row2 = $db->r($res2)) {
				$flat_deal_account_ids[] = $row2["account_id"];
				$free = false;
				$flat_deals .= "#{$row2["id"]},";
				$flat_deals_html .= "#{$row2["id"]}<br />";
			}
		} else {
			//no active flat deals on this zone
		}
	}

	if ($fdfree == "nfd" && count($flat_deal_account_ids) > 0)
		continue;

	//determine utilization
	$utilization_today = $utilization_yesterday = "";
	$total_today = intval($c_today + $backup_today);
	$total_yesterday = intval($c_yesterday + $backup_yesterday);
	if ($total_today) {
		$utilization_today = round($c_today / $total_today, 2);
	}
	if ($total_yesterday) {
		$utilization_yesterday = round($c_yesterday / $total_yesterday, 2);
	}
	//_d("id={$id}, total_today={$total_today}, utilization_today={$utilization_today}, total_yesterday={$total_yesterday}, utilization_yesterday={$utilization_yesterday}");

	if ($fdfree == "free") {
		if ($type == "A") {
			if (!$free)
				continue;
		} else {
			if ($utilization_today > 0.95)
				continue;
		}
	}

	//warnings
	//--------
	//making sure flat deals match exclusivity of zones
	if ($type != "A" && $id != 10106) {
		//sponsor ad zones are not accessbile in advertise create ad page
		//as login page banner zone (#10106) is special case and reserved to admin
		sort($exclusive_access_account_ids);
		sort($flat_deal_account_ids);
		if (empty($flat_deal_account_ids) && !empty($exclusive_access_account_ids)) {
			//zone is reserved for advertiser but he doesnt have flat deal
			//lets check if he has balance
			foreach($exclusive_access_account_ids as $aid) {
				$res2 = $db->q("SELECT ab.budget FROM advertise_budget ab WHERE ab.account_id = ?", [$aid]);
				$row2 = $db->r($res2);
				$budget = $row2["budget"];
				if ($budget <= 0) {
					$warnings .= "<div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-warning-sign\" aria-hidden=\"true\" style=\"color: red;\"></span> Zone reserved for account #{$aid}, but this account has no flat deal nor budget balance!</div>";
				}
			}
		} else if (!empty($flat_deal_account_ids) && empty($exclusive_access_account_ids)) {
			//zone has flat deals but no reservation - this is normal
		} else if ($exclusive_access_account_ids != $flat_deal_account_ids) {
			$warnings .= "<div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-warning-sign\" aria-hidden=\"true\" style=\"color: red;\"></span> Account(s) zone is reserved for mismatch flat deals!</div>";
		}
	}
	//making sure we have default pop url and backup banner HTML for each such zone
	if (!$backuphtml) {
		if ($type == "B" && $id != 10108 && $id != 10109)
			//zones #10108 and #10109 are special in list zones that dont have backup HTML
			$warnings .= "<div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-warning-sign\" aria-hidden=\"true\" style=\"color: red;\"></span> No backup banner HTML !</div>";
		else if ($type == "P")
			$warnings .= "<div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-warning-sign\" aria-hidden=\"true\" style=\"color: red;\"></span> No default popup URL !</div>";
	}
	//making sure there is no placement with flat_deal_status set for account, that does not have active flat deal on this zone
	foreach ($fdpza[$id] as $a) {
		if (!in_array($a, $exclusive_access_account_ids))
			$warnings .= "<div class=\"alert alert-danger\"><span class=\"glyphicon glyphicon-warning-sign\" aria-hidden=\"true\" style=\"color: red;\"></span> There is placement with flat_deal_status set, but no flat deal active for account #{$a} ! <span style=\"color: grey;\">SELECT cs.id FROM advertise_camp_section cs INNER JOIN advertise_campaign c on c.id = cs.c_id WHERE cs.s_id = {$id} and c.account_id = {$a} and cs.flat_deal_status = 1;</span></div>";
	}

	$zones[] = array(
		"id" => $id,
		"type" => $type,
		"label" => ($nickname) ? $nickname : $name,
		"system" => $system,
		"system_html" => $system_html,
		"flat_deals" => $flat_deals,
		"flat_deals_html" => $flat_deals_html,
		"utilization_today" => $utilization_today,
		"utilization_yesterday" => $utilization_yesterday,
		"free" => $free,
		"warnings" => $warnings,
		);
}

if ($nr && $export) {
	//header
	$fields = '"Zone Id#","Zone Type","Zone Name","System","Flat Deals","Utilization","Advertiser Info"'."\n";
	fputs($fp, $fields);

	//rows
	foreach ($zones as $z) {
		$fields = "\"{$z["id"]}\",";
		$type_label = "";
		switch($z["type"]) {
			case "A": $type_label = "Sponsored Ad"; break;
			case "T": $type_label = "Text Ad"; break;
			case "B": $type_label = "Banner"; break;
			case "P": $type_label = "Popunder"; break;
			case "L": $type_label = "Nav Link"; break;
		}
		$fields .= "\"{$type_label}\",";
		$fields .= "\"{$z["label"]}\",";
		$fields .= "\"{$z["system"]}\",";
		$fields .= "\"{$z["flat_deals"]}\",";
		$fields .= "\"".($z["utilization_today"]*100)."%\",";
		$fields .= "\"{$z["advertiser_info"]}\"";
		$fields .= "\n";
		fputs($fp, $fields);
	}

	fclose($fp);
	system::moved("http://adultsearch.com/tmp/export/dashboard_detail.csv");
}

$smarty->assign("fdfree", $fdfree);
$smarty->assign("zones", $zones);

//zone groups
$zone_groups = array();
$res = $db->q("SELECT * FROM advertise_zone_group");
while ($row = $db->r($res)) {
	$zone_groups[$row["id"]] = $row["name"];
}
$smarty->assign("zone_groups", $zone_groups);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/dashboard_detail.tpl");

?>

