<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";
$advertise = new advertise();

if (!permission::access("advertise_manage"))
    die("Invalid access!");


$id = intval($_REQUEST["id"]);

//-----------------------
//new/edit deal handling
if ($_REQUEST["submit"] == "Submit") {

	$new = false;
	if ($_REQUEST["action"] == "new")
		$new = true;
	else if (!$account->isadmin())
        return error_redirect("You don't have permissions to modify deal", "/advertise/deals");
		
	$errors = array();
	$now = time();

	$type = $_REQUEST["type"];
	$account_id = intval($_REQUEST["account_id"]);
	$zone_ids = $_REQUEST["zone_ids"];
	$status = ($_REQUEST["status"]) ? 1 : 0;
	$description = $_REQUEST["description"];
	$paid = $_REQUEST["paid"];
	$account_suggest = $zone_suggest = NULL;

//_d("'$type *$account_id *$zone_id *$status *$description *$paid *$account_suggest'");
	if ($type != "ZI" && $type != "ZP" && $type != "ZT") {
		$errors[] = "Flat deal type '{$type}' not supported!";
	}

	if (!$account_id) {
		$errors[] = "Advertiser account id is mandatory!";
	} else {
		$res = $db->q("SELECT account_id, username, email, advertiser FROM account a WHERE a.account_id = ? LIMIT 1", array($account_id));
		if ($db->numrows($res) != 1) {
			$errors[] = "Can't find advertiser!";
		} else {
			$row = $db->r($res);
			if ($row["advertiser"] != 1) {
				$errors[] = "Account #{$account_id} is not an advertiser!";
			} else {
				$account_email = $row["email"];
			}
			$account_suggest = "#{$row["account_id"]} - {$row["username"]} - {$row["email"]}";
		}
	}

	$zone_types = $zone_names = null;
	$zones = [];
	if (!is_array($zone_ids) || !count($zone_ids)) {
		$errors[] = "Zone id is mandatory!";
	} else {
		$res = $db->q("SELECT id, type, nickname, name FROM advertise_section ads WHERE ads.id IN ? LIMIT 1", array($zone_ids));
		if (!$db->numrows($res)) {
			$errors[] = "Can't find advertise zone(s)!";
		} else {
			while ($row = $db->r($res)) {
				if (!in_array($row["type"], $zone_types))
					$zone_types[] = $row["type"];
				$zone_names[] = $row["name"];
				if ($row["nickname"])	
					$zone_label = "#{$row["id"]} - {$row["nickname"]} ({$row["name"]})";
				else
					$zone_label = "#{$row["id"]} - {$row["name"]}";
				$zones[] = [
					"id" => $row["id"],
					"type" => $row["type"],
					"name" => $row["name"],
					"label" => $zone_label,
					];
			}
		}
	}
	if (count($zone_types) > 1) {
		$errors[] = "Zones must be of the same type!";
	}

	$impressions_deal = $impressions_left = $pops_deal = $pops_left = $to_stamp = $to_str = $clad_id = $clad_sponsor_position = $clad_sponsor_desktop = $clad_sponsor_mobile = NULL;
	$deal = intval($_REQUEST["deal"]);
	$to_date = $_REQUEST["to_date"];
	if ($type == "ZI" || $type == "ZP") {
		if ($deal <= 0) {
			$errors[] = "Deal count is mandatory!";
		}
	}
	if ($type == "ZI") {
		$impressions_deal = $impressions_left = $deal;
	} else if ($type == "ZP") {
		$pops_deal = $pops_left = $deal;
	} else if ($type == "ZT") {
		$arr = explode("-", $to_date);
		if (count($arr) == 3) {
			$to = new \Datetime("{$arr[0]}-{$arr[1]}-{$arr[2]} 23:59:00", new \DateTimeZone("America/Los_Angeles"));
			$to_stamp = $to->format("U");	//UTC timestamp
		}
	}

	if ($zone_types[0] == "A") {
		$clad_id = intval($_REQUEST["clad_id"]);
		$clad_sponsor_position = intval($_REQUEST["clad_sponsor_position"]);
		if (!$clad_id) {
			$errors[] = "Clad id# is mandatory!";
		}
		if (!$clad_sponsor_position) {
			$errors[] = "Clad sponsor position is mandatory!";
		}
		if ($clad_id && $clad_sponsor_position) {
			$clad = clad::findOneById($clad_id);
			if (!$clad) {
				$errors[] = "Can't find classified ad #{$clad_id} !";
			}
		}
		//determining, if this is desktop or mobile sponsor, and set clad_sponsor_column, which is name of column in classifieds table
		//this is a bit hardcoded, redo ?
		foreach ($zone_names as $zone_name) {
			if (stripos($zone_name, "desktop") !== false)
				$clad_sponsor_desktop = true;
			if (stripos($zone_name, "mobile") !== false)
				$clad_sponsor_mobile = true;
		}
		if (!$clad_sponsor_desktop && !$clad_sponsor_mobile)
			$errors[] = "Can't determine if advertise zone is desktop or mobile, contact administrator !";
	}

	$from_stamp = NULL;
	if ($status == 0) {
		$fs_date = $_REQUEST["fs_date"];
		$arr = explode("-", $fs_date);
		if (count($arr) == 3) {
			$fs = new \Datetime("{$arr[0]}-{$arr[1]}-{$arr[2]} 00:01:00", new \DateTimeZone("America/Los_Angeles"));
			$from_stamp = $fs->format("U");	//UTC timestamp
		}
	}

	$started_stamp = NULL;
	if ($status == 1 && $new)
		$started_stamp = $now;

	$smarty->assign("new", $new);
	$smarty->assign("type_options", array("ZI" => "Zone Impressions", "ZP" => "Zone Pops", "ZT" => "Zone Time"));
	$smarty->assign("type", $type);
	$smarty->assign("account_id", $account_id);
	$smarty->assign("account_suggest", $account_suggest);
//	$smarty->assign("zone_id", $zone_id);
//	$smarty->assign("zone_suggest", $zone_suggest);
	$smarty->assign("zones", $zones);
	$smarty->assign("deal", $deal);
	$smarty->assign("description", $description);
	$smarty->assign("paid", $paid);

	if (!empty($errors)) {
		//there were some errors, probably user didnt fill out form correctly
		//_darr($errors);
		$smarty->assign("errors", $errors);
		return $smarty->display(_CMS_ABS_PATH."/templates/advertise/deal.tpl");
	}

	$msg = "";
	if ($new) {
		$res = $db->q("
			INSERT INTO advertise_flat_deal
			(type, status, account_id, zone_id, from_stamp, impressions_deal, impressions_left, pops_deal, pops_left, to_stamp
				, clad_sponsor_position, clad_id, created, created_by, description, paid, started_stamp)
			VALUES 
			(?, ?, ?, ?, ?, ?, ?, ?, ?, ?
				, ?, ?, ?, ?, ?, ?, ?)",
			[$type, $status, $account_id, null, $from_stamp, $impressions_deal, $impressions_left, $pops_deal, $pops_left, $to_stamp
				, $clad_sponsor_position, $clad_id, $now, $account->getId(), $description, $paid, $started_stamp]
			);
		$id = $db->insertid($res);
		if (!$id) {
			$errors[] = "Error inserting new flat deal into database!";
		} else {

			foreach ($zone_ids as $zone_id) {
				$res = $db->q("INSERT INTO advertise_deal_zone (deal_id, zone_id) VALUES (?, ?)", [$id, $zone_id]);
				$adz_id = $db->insertid($res);
				if (!$adz_id)
					$errors[] = "Error inserting new flat deal into database - deal_id={$id}, zone_id={$zone_id} !";
			}

			if (empty($errors)) {
				$msg = "Flat deal #{$id} - ";
				if ($type == "ZI")
					$msg .= "{$deal} impressions";
				else if ($type == "ZP")
					$msg .= "{$deal} popunders";
				else if ($type == "ZT")
					$msg .= "deal until {$to_date}";
				$msg .= " for advertiser #{$account_id} {$account_email} and {$zone_types[0]} zones: #".implode(",", $zone_ids).", names: ".implode(",", $zone_names)." - was successfully saved.";
				file_log("advertise", "deal:new: Deal #{$id} successfully inserted.");
			}
		}
	} else {
		$res = $db->q("
			UPDATE advertise_flat_deal
			SET type = ?, status = ?, account_id = ?, zone_id = ?, from_stamp = ?, impressions_deal = ?, pops_deal = ?, to_stamp = ?, description = ?, paid = ?
			WHERE id = ?
			LIMIT 1",
			[$type, $status, $account_id, null, $from_stamp, $impressions_deal, $pops_deal, $to_stamp, $description, $paid, $id]
			);
		$aff = $db->affected();
		if ($aff != 1) {
			$errors[] = "Error updating flat deal in database!";
			file_log("advertise", "deal:edit: Error: Failed to update deal #{$id}.");
		} else {

			$db->q("DELETE FROM advertise_deal_zone WHERE deal_id = ?", [$id]);

			foreach ($zone_ids as $zone_id) {
				$res = $db->q("INSERT INTO advertise_deal_zone (deal_id, zone_id) VALUES (?, ?)", [$id, $zone_id]);
				$adz_id = $db->insertid($res);
				if (!$adz_id)
					$errors[] = "Error inserting new flat deal into database - deal_id={$id}, zone_id={$zone_id} !";
			}

			if (empty($errors)) {
				$msg = "Flat deal #{$id} was successfully updated - ";
				if ($type == "ZI")
					$msg .= "{$deal} impressions";
				else if ($type == "ZP")
					$msg .= "{$deal} popunders";
				else if ($type == "ZT")
					$msg .= "deal until {$to_date}";
				$msg .= " for advertiser #{$account_id} {$account_email} and {$zone_types[0]} zones: #".implode(",", $zone_ids).", names: ".implode(",", $zone_names);
				file_log("advertise", "deal:edit: Deal #{$id} successfully updated.");
			}
		}
	}

	if (!empty($errors)) {
		//there were some errors, main insert or update didnt go through
		//_darr($errors);
		$smarty->assign("errors", $errors);
		return $smarty->display(_CMS_ABS_PATH."/templates/advertise/deal.tpl");
	}

	//----------------------------------------------------------------------
	// additional changes that need to be done when deal is inserted/updated

	if ($zone_types[0] == "A") {
		//updating classified ad in case of sponsor ad zone type
		if ($clad_sponsor_desktop)
			$clad->setSponsor(1);
		if ($clad_sponsor_mobile)	
			$clad->setSponsorMobile(1);
		$clad->setSponsorPosition($clad_sponsor_position);
		$ret = $clad->update();
		if ($ret) {
			$msg .= " Classified ad #{$clad_id} was updated successfully.";
			rotate_index("escorts");
		} else {
			reportAdmin("AS: flat deal updated, but clad update failed", "Please check what is wrong !", [
				"flat_deal_id" => $id,
				"zone_ids" => print_r($zone_ids, true),
				"zone_names" => print_r($zone_names, true),
				"clad_id" => $clad->getId(), 
				"clad_sponsor_desktop" => intval($clad_sponsor_desktop), 
				"clad_sponsor_mobile" => intval($clad_sponsor_mobile), 
				"clad_sponsor_position" => $clad_sponsor_position
				]);
			$msg .= " ERROR: Classified ad #{$clad_id} was NOT updated correctly !";
		}
	}

	if ($status == 1) {
		//if we enabled deal - go to detail page where we can unpause campaign / fine tune flat_deal status on placements and make zone exclusive
		//TODO shouldnt we do this also when disabling the deal ?
		return success_redirect($msg, "/advertise/deal_detail?id={$id}");
	}

	$advertise->flatDealStatusUpdate($id);

	//lets not do this automatically here - set up explicit button for admin for this
	//$advertise->flatDealCampaigns($id);

	if ($status == 1 && $type == "ZT" && $zone_types[0] != "A") {
		//if this is ZT (Zone / Time) flat deal type, set up exclusive access on flat deal zone for owner of the flat deal
		//do not set exclusive access on A zones (classified sponsor ad zones)
		file_log("advertise", "deal:edit: Setting exclusive access on zones #: ".implode(",", $zone_ids)." for account_id #{$account_id}.");
		$res2 = $db->q("UPDATE advertise_section SET exclusive_access = ? WHERE id IN ? LIMIT 1", [$account_id, $zone_ids]);
		$msg .= " Zones # ".implode(",", $zone_ids)." were set up as exclusive for advertiser #{$account_id}.";
	}

	rotate_index("advertise");

	return success_redirect($msg, "/advertise/deals");
}

$new = ($_REQUEST["action"] == "new") ? true : false;
if (!$new && !$id) {
	echo "Flat deal ID not specified!";
	return;
}

if ($_REQUEST["action"] == "edit") {
	$res = $db->q("SELECT fd.*, a.email, a.username
						, z.nickname as zone_nickname, z.name as zone_name, z.type as zone_type
					FROM advertise_flat_deal fd
					LEFT JOIN account a on a.account_id = fd.account_id
					LEFT JOIN advertise_section z on z.id = fd.zone_id
					WHERE fd.id = ?
					LIMIT 1", 
					array($id));
	if ($db->numrows($res) != 1)
		die("Flat deal id not specified or invalid!");
	$row = $db->r($res);
	$account_suggest = "#{$row["account_id"]} - {$row["username"]} - {$row["email"]}";	$fs_date = NULL;
	if ($row["from_stamp"]) {
		$from = new \DateTime("@{$row["from_stamp"]}");
		$from->setTimezone(new \DateTimeZone("America/Los_Angeles"));
		$fs_date = $from->format("Y-m-d");
	}
	$to_date = NULL;
	if ($row["to_stamp"]) {
		$to = new \DateTime("@{$row["to_stamp"]}");
		$to->setTimezone(new \DateTimeZone("America/Los_Angeles"));
		$to_date = $to->format("Y-m-d");
	}

	$zones = [];
	if (intval($row["zone_id"])) {
		if ($row["zone_nickname"])	
			$zone_label = "#{$row["zone_id"]} - {$row["zone_nickname"]} ({$row["zone_name"]})";
		else
			$zone_label = "#{$row["zone_id"]} - {$row["zone_name"]}";
		$zones[] = [
			"id" => $row["zone_id"],
			"type" => $row["zone_type"],
			"name" => $row["zone_name"],
			"label" => $zone_label,
			];
	} else {
		$res2 = $db->q("
			SELECT ads.id, ads.type, ads.name, ads.nickname 
			FROM advertise_deal_zone adz
			INNER JOIN advertise_section ads on ads.id = adz.zone_id
			WHERE adz.deal_id = ?", 
			[$id]
			);
		while ($row2 = $db->r($res2)) {
			if ($row2["nickname"])	
				$zone_label = "#{$row2["id"]} - {$row2["nickname"]} ({$row2["name"]})";
			else
				$zone_label = "#{$row2["id"]} - {$row2["name"]}";
			$zones[] = [
				"id" => $row2["id"],
				"type" => $row2["type"],
				"name" => $row2["name"],
				"label" => $zone_label,
				];
		}
	}

	$smarty->assign("id", $id);
	$smarty->assign("type", $row["type"]);
	$smarty->assign("status", $row["status"]);
	$smarty->assign("account_id", $row["account_id"]);
	$smarty->assign("account_suggest", $account_suggest);
	$smarty->assign("zones", $zones);
	$smarty->assign("fs_date", $fs_date);
	$smarty->assign("deal", ($row["type"] == "ZI") ? $row["impressions_deal"] : $row["pops_deal"]);
	$smarty->assign("to_date", $to_date);
	$smarty->assign("clad_sponsor_position", $row["clad_sponsor_position"]);
	$smarty->assign("clad_id", $row["clad_id"]);
	$smarty->assign("description", $row["description"]);
	$smarty->assign("paid", $row["paid"]);
}


$smarty->assign("new", $new);
$smarty->assign("type_options", array("ZI" => "Zone Impressions", "ZP" => "Zone Pops", "ZT" => "Zone Time"));
$smarty->display(_CMS_ABS_PATH."/templates/advertise/deal.tpl");
?>
