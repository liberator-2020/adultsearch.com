<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage"))
    die("Invalid access!");


$id = intval($_REQUEST["id"]);
if (!$id)
	die("Deal id not specified!");

$res = $db->q("
	SELECT fd.id, fd.type, fd.status, fd.account_id, fd.zone_id, fd.to_stamp
		, a.email as account_email
		, GROUP_CONCAT(adz.zone_id) as zone_ids
	FROM advertise_flat_deal fd
	INNER JOIN account a on a.account_id = fd.account_id
	LEFT JOIN advertise_deal_zone adz on adz.deal_id = fd.id
	WHERE fd.id = ?
	GROUP BY fd.id
	",
	[$id]
	);
if ($db->numrows($res) != 1)
	die("Can't fetch flat deal from DB! id={$id}");
$row = $db->r($res);
$type = $row["type"];
$status = $row["status"];
$to_stamp = $row["to_stamp"];
$account_id = $row["account_id"];
$account_email = $row["account_email"];

//$zone_id = $row["zone_id"];
$zone_ids = [];
if ($row["zone_ids"]) {
	$zone_ids = explode(",", $row["zone_ids"]);
} else if (intval($row["zone_id"])) {
	$zone_ids[] = intval($row["zone_id"]);
} else {
	die("Error 1");
}


//-------------------
//submission handling
if ($_REQUEST["submit"] == "Submit") {

	$msg = "";

	//modifying zone's exclusive access
	foreach($_REQUEST as $key => $value) {
		$matches = null;
		if (!preg_match('/^z_([0-9]*)_exclusive_add$/', $key, $matches))
			continue;
		$zone_id = intval($matches[1]);
		$exclusive_add = intval($value);
		_d("exclusive_add for zone #{$zone_id} = '{$value}'");

		if (!$exclusive_add)
			continue;

		$res = $db->q("SELECT * FROM advertise_section WHERE id = ?", [$zone_id]);
		if (!$db->numrows($res))
			die("Error 4");
		$row = $db->r($res);
		$exclusive_access = $row["exclusive_access"];
		$exclusive_access_account_ids = [];
		if (trim($exclusive_access))
			$exclusive_access_account_ids = explode(",", $exclusive_access);

		if (!in_array($exclusive_add, $exclusive_access_account_ids)) {
			//_darr($exclusive_access_account_ids);
			$exclusive_access_account_ids[] = $exclusive_add;
			//_darr($exclusive_access_account_ids);
			$new_exclusive_access = implode(",", $exclusive_access_account_ids);
			//_d("new_exclusive_access='{$new_exclusive_access}'");
			//die;
			$db->q("UPDATE advertise_section SET exclusive_access = ? WHERE id = ?", [$new_exclusive_access, $zone_id]);
			$msg .= "Added account id #{$exclusive_add} to exclusive access for zone #{$zone_id}. ";
		}
	}

	//modidying flat deal statuses on placements
	foreach($_REQUEST as $key => $value) {
		if (substr($key, 0, 2) != "p_")
			continue;
		$cs_id = intval(substr($key, 2));
		if (!$cs_id)
			continue;
		//check if that placement is really for the same advertiser and the same zone
		$res = $db->q("
			SELECT ac.account_id as p_account_id, acs.s_id as p_zone_id, acs.flat_deal_status as p_flat_deal_status
			FROM advertise_camp_section acs
			INNER JOIN advertise_campaign ac on ac.id = acs.c_id
			WHERE acs.id = ?",
			[$cs_id]
			);
		if ($db->numrows($res) != 1)
			continue;
		$row = $db->r($res);
		if ($row["p_account_id"] != $account_id)
			continue;
		//check if we need to do change in flat deal status
		$value = intval($value) ? 1 : 0;
		if (intval($row["p_flat_deal_status"]) != $value) {
			//update placement - set flat deal status
			//_d("UPDATE advertise_camp_section SET flat_deal_status = ? WHERE id = ?, '{$value}', '{$cs_id}'");
			$res = $db->q("UPDATE advertise_camp_section SET flat_deal_status = ? WHERE id = ?", [$value, $cs_id]);
			$aff = $db->affected($res);
			if ($aff == 1) {
				$msg .= "Updated flat deal status to '{$value}'  on placement '{$cs_id}'. ";
			} else {
				die("Failed to update flat deal status to '{$value}' on placement #{$cs_id} !");
			}
		}
	}

	//rotate advertise index
	rotate_index("advertise");

	return success_redirect($msg, "/advertise/deals");
}

$zones = [];
foreach ($zone_ids as $zone_id) {

	$res = $db->q("SELECT * FROM advertise_section WHERE id = ?", [$zone_id]);
	if (!$db->numrows($res))
		die("Error 2");
	$row = $db->r($res);
	$zone_name = $row["name"];
	$zone_nickname = $row["nickname"];
	$zone_label = $zone_nickname ? $zone_nickname : $zone_name;
	$zone = [
		"zone_id" => $zone_id,
		"zone_name" => $zone_name,
		"zone_nickname" => $zone_nickname,
		"zone_label" => $zone_label,
		];

	$exclusive_access = $row["exclusive_access"];
	$exclusive_access_account_ids = [];
	if (trim($exclusive_access))
		$exclusive_access_account_ids = explode(",", $exclusive_access);

	//check exclusivity
	$exclusive_message = $exclusive_prompt = $exclusive_select = $exclusive_options = null;
	if ($status == 1 && !in_array($account_id, $exclusive_access_account_ids)) {
		$exclusive_message = "Flat deal #{$id} is active, but zone #{$zone_id} is not set as exclusive for account #{$account_id} ({$account_email}).";
		$exclusive_prompt = "Do you want to add account_id #{$account_id} to exclusive list for zone #{$zone_id} ?";
		$exclusive_select = "z_{$zone_id}_exclusive_add";
		$exclusive_options = ["{$account_id}" => "Yes", "" => "No"];
	}
	//_d("zone #{$zone_id} exclusive_options");_darr($exclusive_options);
	$zone["exclusive_access"] = $exclusive_access;
	$zone["exclusive_message"] = $exclusive_message;
	$zone["exclusive_prompt"] = $exclusive_prompt;
	$zone["exclusive_select"] = $exclusive_select;
	$zone["exclusive_options"] = $exclusive_options;

	//get campaigns and placements
	$all_live_campaigns_activated = true;
	$campaigns = [];
	$res = $db->q("
		SELECT ac.id as c_id, ac.status as c_status, ac.name as c_name
			, acs.id as cs_id, acs.flat_deal_status
		FROM advertise_camp_section acs
		INNER JOIN advertise_campaign ac ON ac.id = acs.c_id AND ac.status > -1
		WHERE acs.`show` = 1 AND ac.account_id = ? AND acs.s_id = ?
		",
		[$account_id, $zone_id]
		);
	while ($row = $db->r($res)) {
		$c_id = $row["c_id"];
		$c_status = $row["c_status"];
		$c_name = $row["c_name"];
		$cs_id = $row["cs_id"];
		$flat_deal_status = $row["flat_deal_status"];

		$camps = [
			"c{$c_id}" => [
				"id" => $c_id,
				"name" => $c_name,
				"status" => $c_status,
				"placement_id" => $cs_id,
				"flat_deal_status" => $flat_deal_status,
			]
		];
		$campaigns = array_merge_recursive($campaigns, $camps);
		if ($c_status != 1 || ($c_status == 1 && !$flat_deal_status))
			$all_live_campaigns_activated = false;
	}
	//_d("campaigns");_darr($campaigns);
	//$smarty->assign("campaigns", $campaigns);
	$zone["campaigns"] = $campaigns;
	$zone["all_live_campaigns_activated"] = $all_live_campaigns_activated;

	$type_label = "";
	if ($type == "ZT")
		$type_label = "ZT - until ".date("m/d/Y", $to_stamp);
	else
		$type_label = $type;
	$zone["type_label"] = $type_label;

	$zones[] = $zone;
}

$smarty->assign("id", $id);
$smarty->assign("type", $type);
$smarty->assign("status", $status);
$smarty->assign("account_id", $account_id);
$smarty->assign("account_email", $account_email);

$smarty->assign("zones", $zones);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/deal_detail.tpl");
?>
