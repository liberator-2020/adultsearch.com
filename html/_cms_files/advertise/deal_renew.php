<?php
/**
 * This controlelr handles renewing the deal - so it inserts new deal starting after current deal finishes
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";
$system = new system;
$advertise = new advertise();

if (!$account->isloggedin()) {
	$account->asklogin();
	return false;
}

if (!$account->isadmin()) {
	die("You have no privileges to display this page.");
	return false;
}

$id = intval($_REQUEST["id"]);

//-----------------------
//new/edit deal handling
if ($_REQUEST["submit"] == "Submit") {

	$old_deal = deal::findOneById($id);
	if (!$old_deal)
		die("Flat deal id not specified or invalid!");
	//for now only for ZT deals
	if ($old_deal->getType() != "ZT" && $old_deal->getType() != "ZI" && $old_deal->getType() != "ZP")
		die("Unsupported deal type: {$old_deal->getType()} !");

	$errors = array();
	$now = time();

	$paid = $_REQUEST["paid"];
	$length = $_REQUEST["length"];
	$end = $_REQUEST["end"];
	$volume = intval($_REQUEST["volume"]);
	$description =trim($_REQUEST["description"]);
	$to_stamp = NULL;

	if (!$paid) {
		$errors[] = "Paid amount not specified!";
	}
	if ($old_deal->getType() == "ZI" || $old_deal->getType() == "ZP") {
		if (!$volume)
			$errors[] = "Volume not specified!";
	}
	if ($old_deal->getType() == "ZT") {
		if (!$length) {
			$errors[] = "Length of new deal not specified!";
		}
		if ($length == "end") {
			$ret = preg_match('/^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})$/', $end, $matches);
			if (!$ret) {
				$errors[] = "Please select end date!";
			} else {
				$to_stamp = mktime(23, 59, 0, $matches[2], $matches[3], $matches[1]);		
			}
		}
	}
	if (!$description) {
		$errors[] = "Description is empty!";
	}

	if (empty($errors)) {

		$from_stamp = $old_deal->getToStamp();

		if ($old_deal->getType() == "ZT" && !$to_stamp) {
			$to = new \DateTime("@{$from_stamp}");
			if (in_array($length, ["1M", "2M", "3M", "6M", "12M"])) {
				$to->add(new \DateInterval("P{$length}"));
			} else {
				die("Unknown length of new deal. Contact administrator!");
			}
			$to_stamp = $to->format("U");
		}

		$new_deal = new deal();
		$new_deal->setType($old_deal->getType());
		$new_deal->setStatus(0);
		$new_deal->setAccountId($old_deal->getAccountId());
		$new_deal->setZoneId($old_deal->getZoneId());
		$new_deal->setFromStamp($from_stamp);
		if ($old_deal->getType() == "ZI") {
			$new_deal->setImpressionsDeal($volume);
			$new_deal->setImpressionsLeft($volume);
		} else if ($old_deal->getType() == "ZP") {
			$new_deal->setPopsDeal($volume);
			$new_deal->setPopsLeft($volume);
		} else if ($old_deal->getType() == "ZT") {
			$new_deal->setToStamp($to_stamp);
		}
		$new_deal->setCladSponsorPosition($old_deal->getCladSponsorPosition());
		$new_deal->setCladId($old_deal->getCladId());
		$new_deal->setPaid($paid);
		$new_deal->setCreated($now);
		$new_deal->setCreatedBy($account->getId());
		$new_deal->setDescription($description);
		$ret = $new_deal->add();
		if (!$ret) {
			reportAdmin("AS: advertise/deal_renew error 1", "Can't insert new deal into database", ["old_deal_id" => $old_deal->getId(), "from_stamp" => $from_stamp, "to_stamp" => $to_stamp, "paid" => $paid, "description" => $description]);
			file_log("advertise", "advertise/deal_renew error 1 - Can't insert new deal into database: old_deal_id={$old_deal->getId()}, from_stamp={$from_stamp}, to_stamp={$to_stamp}, paid={$paid}, description={$description}");
			return error_redirect("Can't insert new deal into database, please contact administrator", "/advertise/dashboard");
		}

		//copy zone assignment
		$zone_count = 0;
		$res = $db->q("SELECT zone_id FROM advertise_deal_zone WHERE deal_id = ?", [$old_deal->getId()]);
		while ($row = $db->r($res)) {
			$zone_id = $row["zone_id"];
			$res2 = $db->q("INSERT INTO advertise_deal_zone (deal_id, zone_id) VALUES (?, ?)", [$new_deal->getId(), $zone_id]);
			if (!$db->insertid($res2)) {
				file_log("advertise", "advertise/deal_renew error 1.5: cant insert deal_zone assignment: new_deal_id={$new_deal->getId()}, zone_id={$zone_id}");
				return error_redirect("Can't insert deal zone assignemnt into database, please contact administrator and check advertise.log", "/advertise/dashboard");
			}
			$zone_count++;
		}
		
		$old_deal->setNextDealId($new_deal->getId());
		$ret = $old_deal->update();
		if (!$ret) {
			reportAdmin("AS: advertise/deal_renew error 2", "Can't update old deal with next deal id", ["old_deal_id" => $old_deal->getId(), "new_deal_id" => $new_deal->getId()]);
			file_log("advertise", "advertise/deal_renew error 2 - Can't update old deal with next deal id: old_deal_id={$old_deal->getId()}, new_deal_id={$new_deal->getId()}");
			return error_redirect("Can't update old deal in database, please contact administrator", "/advertise/dashboard");
		}
		
		//success
		file_log("advertise", "Deal Renew success: old_deal_id={$old_deal->getId()}, new_deal_id={$new_deal->getId()}, length={$length}, paid={$paid}, zone_count={$zone_count}");
		return success_redirect("Deal successfully renewed. New deal id is #{$new_deal->getId()}. {$zone_count} zone assignments copied.", "/advertise/dashboard");		
	}

	$smarty->assign("errors", $errors);
	$smarty->assign("length", htmlspecialchars($length));
	$smarty->assign("paid", htmlspecialchars($paid));
	$smarty->assign("description", htmlspecialchars($description));
}

//fetch deal from DB
$deal = deal::findOneById($id);
if (!$deal)
	die("Flat deal id not specified or invalid!");

$ending = $length = "";
if ($deal->getType() == "ZT") {
	//compute length
	if ($deal->getStartedStamp() && $deal->getToStamp()) {
		$seconds = $deal->getToStamp() - $deal->getStartedStamp();
		$days = round($seconds / 86400);
		$months = round($days / 30);
		$length = "{$months} months ({$days} days) - from ".date("M j", $deal->getStartedStamp())." to ".date("M j", $deal->getToStamp()); 
	} else {
		$length = "<span style=\"color: red;\">Error: can't determine length of current deal, please contact administrator !</span>";
	}
} else if ($deal->getType() == "ZI") {
	$length = number_format($deal->getImpressionsDeal(), 0, "", ",")." impressions";
} else if ($deal->getType() == "ZP") {
	$length = number_format($deal->getPopsDeal(), 0, "", ",")." pops";
}

//compute ending time
$seconds_left = $deal->getSecondsLeft();
if ($seconds_left < 0) {
	$ending = "<span style=\"color: red; font-weight: bold;\">Deal already finished !</span>";
} else {
	$left_style = "";
	if ($seconds_left < 3*86400)
		$left_style = "color: red; font-weight: bold;";
	else if ($seconds_left < 31*86400)
		$left_style = "color: orange; font-weight: bold;";
	$ending = "in <span style=\"{$left_style}\">".time_to_human($seconds_left)."</span> (".date("M j", $seconds_left + time()).")";
}

$zones = $deal->getZones();
$zone_labels = "";
foreach ($zones as $zone) {
	$zone_labels .= "#".$zone->getId()." - ".$zone->getName()."<br />\n";
}

$smarty->assign("deal", array(
	"id" => $deal->getId(),
	"type" => $deal->getType(),
	"advertiser" => "#".$deal->getAccount()->getId()." - ".$deal->getAccount()->getEmail(),
	"zones" => $zone_labels,
	"to_stamp" => $deal->getToStamp(),
	"length" => $length,
	"paid" => $deal->getPaid(),
	"ending" => $ending,
	)	
	);
$smarty->assign("length_options", [
"" => "-", 
"1M" => "1 Month", 
"2M" => "2 Months", 
"3M" => "3 Months", 
"6M" => "6 Months", 
"12M" => "12 Months", 
"end" => "Other - specify end of the deal below", 
]);
$smarty->display(_CMS_ABS_PATH."/templates/advertise/deal_renew.tpl");
?>
