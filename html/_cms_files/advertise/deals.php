<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $sphinx, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage"))
    die("Invalid access!");


//filters
$where = "";
$account_id = NULL;
if ($_REQUEST["status"]) {
	switch ($_REQUEST["status"]) {
		case "all": 
			$smarty->assign("status", "all");
			break;
		case "active": 
			$where .= (empty($where)) ? " WHERE " : " AND ";
			$where .= "fd.status = 1";
			$smarty->assign("status", "active");
			break;
		case "not_active": 
			$where .= (empty($where)) ? " WHERE " : " AND ";
			$where .= "fd.status = 0";
			$smarty->assign("status", "not_active");
			break;
		default:
			break;
	}
} else {
	//default filter show only active deals
	$where .= (empty($where)) ? " WHERE " : " AND ";
	$where .= "fd.status = 1";
	$smarty->assign("status", "active");
}
if ($_REQUEST["advertiser"] && $_REQUEST["advertiser"] != "all") {
	$account_id = intval($_REQUEST["advertiser"]);
	$smarty->assign("advertiser", $account_id);
} else {
	$smarty->assign("advertiser", "all");
}

$res = $db->q("SELECT fd.*
					, a.username, a.email
					, GROUP_CONCAT(adz.zone_id) as zone_ids
					, a2.email as created_by_email, a2.username as created_by_username
				FROM advertise_flat_deal fd
				INNER JOIN account a on a.account_id = fd.account_id
				LEFT JOIN account a2 on a2.account_id = fd.created_by
				LEFT JOIN advertise_deal_zone adz on adz.deal_id = fd.id
				{$where}
				GROUP BY fd.id
				ORDER BY fd.id ASC
				");
$deals = $advertisers = array();
while ($row = $db->r($res)) {
	$fd_id = $row["id"];

	if (!array_key_exists($row["account_id"], $advertisers))
		$advertisers[$row["account_id"]] = $row["email"];
	if (!is_null($account_id) && $account_id != $row["account_id"])
		continue;

	$fd_account_id = $row["account_id"];

	$type_name = "N/A";
	if ($row["type"] == "ZI")
		$type_name = "Zone / Impressions";
	else if ($row["type"] == "ZP")
		$type_name = "Zone / Pops";
	else if ($row["type"] == "ZT")
		$type_name = "Zone / Time";
	else {
		echo "Unknown flat deal type '{$row["type"]}' !";
		return;
	}


	$time_until = NULL;	
	if ($row["type"] == "ZT") {
		$tu = new \DateTime("@".$row["to_stamp"], new \DateTimeZone("UTC"));
		$tu->setTimezone(new \DateTimeZone("America/Los_Angeles"));
		$time_until = $tu->format("Y&#8209;m&#8209;d H:i");	//&#8209; is non-breaking hyphen
	}

	$created = $row["created"];

	//compute how much time left in this deal
	if ($row["type"] == "ZI" || $row["type"] == "ZP") {
		//this is only APPROXIMATE !!!
		//TODO this does not take in account pauses in this deal or changes in volume - correctly it should count only impression/pop numbers from last period - REDO
		$seconds_spent = time() - $row["created"];
		$deal_spent = $row["impressions_deal"] - $row["impressions_left"];
		$deal_left = $row["impressions_left"];
		if ($row["type"] == "ZP") {
			$deal_spent = $row["pops_deal"] - $row["pops_left"];
			$deal_left = $row["pops_left"];
		}
		$deal_left_to_spent_ratio = $deal_left / $deal_spent;
		$seconds_left = $deal_left_to_spent_ratio * $seconds_spent;
	} else {
		$seconds_left = $row["to_stamp"] - time();
	}
	$time_left = time_to_human($seconds_left);

	$zones = [];
	$zone_ids = [];
    if ($row["zone_ids"]) {
        $zone_ids = explode(",", $row["zone_ids"]);
    } else if (intval($row["zone_id"])) {
        $zone_ids[] = intval($row["zone_id"]);
    } else {
        continue;
    }
	
	$campaigns = 0;
	foreach ($zone_ids as $zone_id) {
		$res2 = $db->q("
			SELECT ads.type as zone_type, ads.nickname, ads.name 
				, count(distinct pla.id) as campaigns
			FROM advertise_section ads 
			LEFT JOIN (
				SELECT DISTINCT acs.id, acs.s_id, c.account_id
				FROM advertise_camp_section acs
				INNER JOIN advertise_campaign c on c.id = acs.c_id
				WHERE acs.show = 1 and c.status = 1
				) pla on pla.s_id = ? and pla.account_id = ?
			WHERE ads.id = ? 
			LIMIT 1",
			[$zone_id, $fd_account_id, $zone_id]
			);
		if (!$db->numrows($res2))
			die("Error 1");
		$row2 = $db->r($res2);
		$zone = [
			"zone_id" => $zone_id,
			"zone_type" => $row2["zone_type"],
			"zone_name" => ($row2["nickname"]) ? $row2["nickname"] : $row2["name"],
			];
		$campaigns += intval($row2["campaigns"]);
		$zones[] = $zone;
	}

	$deals[] = array(
		"id" => $fd_id,
		"type" => $row["type"],
		"type_name" => $type_name,
		"status" => $row["status"],
		"account_id" => $row["account_id"],
		"username" => $row["username"],
		"email" => $row["email"],
		"zones" => $zones,
		"impressions_deal" => number_format($row["impressions_deal"]),
		"impressions_left" => number_format($row["impressions_left"]),
		"pops_deal" => number_format($row["pops_deal"]),
		"pops_left" => number_format($row["pops_left"]),
		"time_until" => $time_until,
		"time_left" => $time_left,
		"time_left_warning" => $seconds_left < 86400*3,
		"created" => $row["created"],
		"created_by" => $row["created_by"],
		"created_by_username" => $row["created_by_username"],
		"created_by_email" => $row["created_by_email"],
		"description" => $row["description"],
		"campaigns_advertising" => $campaigns,
		);
}
$smarty->assign("deals", $deals);
$smarty->assign("advertisers", $advertisers);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/deals.tpl");

?>
