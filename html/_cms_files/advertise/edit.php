<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $config_image_server;
$system = new system;

if (!$account->isloggedin()) {
	$account->asklogin();
	return;
}

$new = false;
$params = $cat_original = $cat_selected =array();
$type = NULL;
if (in_array($_REQUEST["type"], array("T", "B", "P", "L", "I"))) {
	$type = $_REQUEST["type"];
	$params["type"] = $type;
	$smarty->assign("type", $type);
}

if (!isset($_GET["id"])) {
	$new = true;
	$account_id = $account->getId();
} else { 
	$id = GetGetParam("id");
	$res = $db->q("select * from advertise_campaign where id = ?", array($id));
	if (!$db->numrows($res)) {
		$new = true;
		$account_id = $account->getId();
	} else {
		$row = $db->r($res);
		$account_id = $row["account_id"];
		if ($account_id != $account->getId() && !$account->isadmin()) {
			error_redirect("Access denied!", "/advertise/");
			die;
		}
		$params["type"] = $row["type"];
		$params["name"] = $row["name"];
		$params["ad_title"] = $row["ad_title"];
		$params["ad_line1"] = $row["ad_line1"];
		$params["ad_line2"] = $row["ad_line2"];
		$params["ad_displayurl"] = $row["ad_durl"];
		$params["ad_prefix"] = $row["ad_prefix"];
		$params["ad_url"] = $ad_url_orj = $row["ad_url"];
		$params["budget"] = $budget_orj = $row["budget"];
		$params["dcpc"] = $row["dcpc"];
		$params["external"] = $row["external"];
		$params["cpm"] = $row["cpm"];
		$params["banner_iframe_src"] = $row["banner_iframe_src"];
		$params["banner_a_href"] = $row["banner_a_href"];
		$params["banner_img_src"] = $row["banner_img_src"];
		$params["code"] = $row["code"];
		$params["locked"] = $row["locked"];

		$res = $db->q("select s_id, section, recursion, bid from advertise_camp_section where c_id = ? and `show` = 1", array($id));
		while ($row = $db->r($res)) {
			$cat_original[] = $row[0];
			$dbs[$row[0]] = $row["bid"];
		}

		$res = $db->q("select d.loc, d.section, concat(l.loc_name,' ',l.s) loc_name 
						from advertise_camp_section_dont d 
						inner join location_location l on d.loc = l.loc_id 
						where c_id = '$id' 
						group by section, loc");
		$dont = NULL;
		while($row=$db->r($res)) {
			$loc_dont[] = array("loc"=>$row["loc"], "section"=>$row["section"], "loc_name"=>$row["loc_name"]);
			if( $row["section"] == "sc" ) $dont["sc"] .= ($dont["sc"] ? "|" : "") . $row["loc"];
			elseif( $row["section"] == "as" ) $dont["as"] .= ($dont["as"] ? "|" : "") . $row["loc"];
			elseif( $row["section"] == "emp" ) $dont["emp"] .= ($dont["emp"] ? "|" : "") . $row["loc"];
			elseif( $row["section"] == "cls" ) $dont["cls"] .= ($dont["cls"] ? "|" : "") . $row["loc"];
		}
		$smarty->assign("loc_dont", $loc_dont);
	}
}

function ad_allowed_characters($input) {
	//no special unicode "icon" characters
	return trim(preg_replace('#[^a-zA-Z0-9 \-_\.\,\!\?\\\'\*\&\|\/]#', '', $input));
}

function validate_params(&$params, &$errors) {
	global $account;

	$errors = array();

	$params["name"] = ad_allowed_characters($_REQUEST["name"]);
	$params["ad_title"] = ad_allowed_characters($_REQUEST["ad_title"]);
	$params["ad_line1"] = ad_allowed_characters($_REQUEST["ad_line1"]);
	$params["ad_line2"] = ad_allowed_characters($_REQUEST["ad_line2"]);
	$params["ad_displayurl"] = GetPostParam("ad_displayurl");
	$params["ad_prefix"] = GetPostParam("ad_prefix");
	$params["budget"] = GetPostParam("budget");
	$params["dcpc"] = floatval(GetPostParam("dcpc"));
	$params["external"] = isset($_POST["external"])?1:0;
	$params["cpm"] = floatval($_REQUEST["cpm"]);
	$params["banner_iframe_src"] = preg_replace('/[\'"<>]/', '', $_REQUEST["banner_iframe_src"]);
	$params["banner_a_href"] = preg_replace('/[\'"<>]/', '', $_REQUEST["banner_a_href"]);
	$params["banner_img_src"] = preg_replace('/[\'"<>]/', '', $_REQUEST["banner_img_src"]);
	if ($account->isadmin())
		$params["code"] = $_REQUEST["code"];

	if (empty($params["name"]))
		$errors = "Campaign name may not be empty";

	//text ad params validation
	if ($params["type"] == "T") {
		$params["ad_url"] = GetPostParam("ad_url");

		$linex = explode(" ", $params["ad_line1"]);
		foreach($linex as $l) {
			if( strlen($l) > 15 ) {
				$errors[] = "Some of the words in Description line 1 too long. Please use shorter words.";
				break;
			}
		}

		$linex = explode(" ", $params["ad_line2"]);
		foreach($linex as $l) {
			if( strlen($l) > 15 ) {
				$errors[] = "Some of the words in Description line 2 too long. Please use shorter words.";
				break;
			}
		}

		if( empty($params["ad_title"]))
			$errors[] = "Ad Title may not be empty";
		
		if( empty($params["ad_line1"]) || empty($params["ad_line1"]) )
			$errors[] = "Ad Description Lines may not be empty";

		if( empty($params["ad_displayurl"]) )
			$errors[] = "Display URL may not be empty";

		if( empty($params["ad_url"]) )
			$errors[] = "Ad URL may not be empty";

		if( $params["dcpc"] < 0.01 )
			$errors[] = "Default CPC can not be less than $0.01";
	}

	//banner ad params validation
	if ($params["type"] == "B") {
		if ($params["cpm"] == 0)
			$errors[] = "CPM must be specified";
	}

	if ($params["type"] == "P") {
		$params["ad_url"] = GetPostParam("popup_url");
	}

	if ($params["type"] == "L") {
		$params["ad_title"] = ad_allowed_characters($_REQUEST["link_title"]);
		$params["ad_url"] = GetPostParam("link_url");
	}

	if (!empty($errors))
		return false;

	return true;
}


//----------
//submission
if (isset($_POST["name"]) ) {

	$ret = validate_params($params, $errors);

	if ($ret) { 

		$category = count($_POST["ad_cat"]);
		if (isset($_POST["selected15"]) && is_array($_POST["selected15"]))
			$category += count($_POST["selected15"]);
		if (isset($_POST["selected5"]) && is_array($_POST["selected5"]))
			$category += count($_POST["selected5"]);
		if (isset($_POST["selected81"]) && is_array($_POST["selected81"]))
			$category += count($_POST["selected81"]) ? count($_POST["selected81"]) - 1 : 0;
		if ($params["external"])
			$category += count($_POST["selectedexternal"]);

		$ad_https = $params["ad_prefix"] == "https://" ? 1 : 0;

		//get budget status
		$status_budget = 0;
		$rex = $db->q("select budget from advertise_budget where account_id = ?", array($account_id));
		if ($db->numrows($rex)) {
			$rox = $db->r($rex);
			if ($rox["budget"] > 0)
				$status_budget = 1; 
		}

		if (!$new) {
			//UPDATE

			//if we are increasing budget and we spent so far less than new budget, display notification, that some campaign placements might be live now
			if ($params["budget"] > $budget_orj ) {
				$rex = $db->q("SELECT sum(cost) as cost
								FROM advertise_camp_section s 
								LEFT JOIN advertise_data d on (s.id = d.s_id and d.date = curdate()) 
								WHERE s.c_id = ?", array($id));
				if( $db->numrows($rex) ) {
					$rox = $db->r($rex);
					$dbudget = number_format($rox["cost"]);
					if( $dbudget < $params["budget"] ) {
						$db->q("update advertise_camp_section set budget_available = 1 where c_id = ?", array($id));
						if ($db->affected) 
							$alert = "Some of your ads are now live with your new budget!";
					}
				}
			}

			//main update
			//CONTINUE
			/*
			$db->q("UPDATE advertise_campaign 
					SET name = ?, type = ?, ad_title = ?, ad_line1 = ?, ad_line2 = ?, ad_durl = ?, ad_https = ?, 
						ad_url = ?, budget = ?, category = ?, dcpc = ?, external = ?, 
						code = ?, cpm = ?, banner_iframe_src = ?, banner_a_href = ?, banner_img_src = ?
					WHERE id = ?",
					array($params["name"], $params["type"], $params["ad_title"], $params["ad_line1"], $params["ad_line2"], $params["ad_displayurl"], $ad_https, 
						$params["ad_url"], $params["budget"], $category, $params["dcpc"], $params["external"], 
						$params["code"], $params["cpm"], $params["banner_iframe_src"], $params["banner_a_href"], $params["banner_img_src"],
						$id)
				);
			*/
			$pars = [];
			$update = "name = ?, type = ?, ad_title = ?, ad_line1 = ?, ad_line2 = ?, ad_durl = ?, ad_https = ?";
			$update .= ", ad_url = ?, category = ?, external = ?, code = ?, banner_iframe_src = ?, banner_a_href = ?, banner_img_src = ?";
			$pars[] = $params["name"];
			$pars[] = $params["type"];
			$pars[] = $params["ad_title"];
			$pars[] = $params["ad_line1"];
			$pars[] = $params["ad_line2"];
			$pars[] = $params["ad_displayurl"];
			$pars[] = $ad_https;
			$pars[] = $params["ad_url"];
			$pars[] = $category;
			$pars[] = $params["external"];
			$pars[] = $params["code"];
			$pars[] = $params["banner_iframe_src"];
			$pars[] = $params["banner_a_href"];
			$pars[] = $params["banner_img_src"];
			if (!$params["locked"]) {
				$update .= ", budget = ?, dcpc = ?, cpm = ?";
				$pars[] = $params["budget"];
				$pars[] = $params["dcpc"];
				$pars[] = $params["cpm"];
			}
			$pars[] = $id;
			$db->q("UPDATE advertise_campaign SET {$update} WHERE id = ?", $pars);

			$c_id = $id;
			if ($account->isadmin())
				$db->q("UPDATE advertise_campaign SET code = ? WHERE id = ?", array($params["code"], $id));

			if ($ad_url == $ad_url_orj)
				$approved_dont_need = true;
			else {
				$rex = $db->q("select id from advertise_section where external = 0 and appneeded = 1");
				if ($db->numrows($rex)) {
					while($rox = $db->r($rex)) {
						$idx[] = $rox[0];
					}
					$db->q("update advertise_camp_section set approved = 0 where c_id = '$id' and s_id in (".implode(',',$idx).")");
				}
			}

			$cam = campaign::findOneById($id);
			if ($cam)
				$cam->notifyIfHasFlatDeal("edit");


		} else {
			$db->q("INSERT INTO advertise_campaign 
					(account_id, type, name, ad_title, ad_line1, ad_line2, ad_durl, ad_https, 
					ad_url, budget, category, dcpc, external,
					code, cpm, banner_iframe_src, banner_a_href, banner_img_src, 
					created_stamp) 
					values 
					(?, ?, ?, ?, ?, ?, ?, ?, 
					?, ?, ?, ?, ?,
					?, ?, ?, ?, ?, 
					?)",
					array($account->getId(), $params["type"], $params["name"], $params["ad_title"], $params["ad_line1"], $params["ad_line2"], $params["ad_displayurl"], $ad_https, 
						$params["ad_url"], $params["budget"], $category, $params["dcpc"], $params["external"],
						$params["code"], $params["cpm"], $params["banner_iframe_src"], $params["banner_a_href"], $params["banner_img_src"],
						time())
					);
			$c_id = $db->insertid;
			if ($account->isadmin())
				$db->q("UPDATE advertise_campaign SET code = ? WHERE id = ?", array($params["code"], $c_id));

			//TODO
			if( is_array($_SESSION["advertise_exception"]) ) {
				foreach($_SESSION["advertise_exception"] as $ex)
					if (!empty($ex))
						$db->q("INSERT INTO advertise_exception (c_id, exception) VALUES (?, ?)", array($c_id, $ex));
				$_SESSION["advertise_exception"] = NULL;
			}
		}

		if (!$c_id) {
			$error = "Error, please try again.";
		} else {
			//disable all old placements by setting show to 0
			$db->q("update advertise_camp_section set `show` = 0 where c_id = ?", array($c_id));

			//update placements
			if ($params["type"] == "T" && is_array($_POST["ad_cat"])) {
				foreach($_POST["ad_cat"] as $cat)
					$cat_selected[] = intval($cat);
			} else if ($params["type"] == "B" && is_array($_POST["cat_ban"])) {
				foreach($_POST["cat_ban"] as $cat) {
					$cat_selected[] = intval($cat);
				}
			} else if ($params["type"] == "P" && is_array($_POST["cat_pop"])) {
				foreach($_POST["cat_pop"] as $cat) {
					$cat_selected[] = intval($cat);
				}
			} else if ($params["type"] == "L" && is_array($_POST["cat_link"])) {
				foreach($_POST["cat_link"] as $cat) {
					$cat_selected[] = intval($cat);
				}
			} else if ($params["type"] == "I" && is_array($_POST["cat_interstitial"])) {
				foreach($_POST["cat_interstitial"] as $cat) {
					$cat_selected[] = intval($cat);
				}
			}
			foreach($cat_selected as $cat) {
				$add = 0;
				//fetch this section with number of subsections
				$rex = $db->q("select s.*, count(s2.id) p from advertise_section s left join advertise_section s2 on s.id = s2.parent where s.id = ?", array($cat));
				if (!$db->numrows($rex))
					continue;

				$rox = $db->r($rex);

				//bid determination
				$dcpc_ = $params["dcpc"];		//by default, bid has the value defined for campaign
				if (isset($_POST["_db_{$cat}"]) && (float)$_POST["_db_{$cat}"] == $_POST["_db_{$cat}"])
					$dcpc_ = (float)$_POST["_db_{$cat}"];	//if we defined custom default bid for this section, take this one
				if ($rox["defaultbid"] > $dcpc_)
					$dcpc_ = $rox["defaultbid"];	//if this section has defined minimal bid higher than we entered, take the minimal bid
				//round it
				if ($type == "P")
					$dcpc_ = round($dcpc_, 3);
				else
					$dcpc_ = round($dcpc_, 2);

				$name_string = $rox["external"]==1 ? "n" : "name";
				$p = $rox["p"] ? 1 : 0;
				$recursion = $rox["external"]&&isset($_POST["recursion".$cat])?1:0;
				$approved = !$rox['appneeded'] ? 1 : 0;

				//cpm determination
				$cpm = $params["cpm"];
				//for now we dont support special cpm per section, copy value from campaign
				if ($rox["cpm_minimum"] > $cpm)
					$cpm = $rox["cpm_minimum"];

				debug_log("c_id={$c_id}, cat={$cat}, dcpc={$dcpc_}");
				if (in_array($cat, $cat_original)) {
					//new placement 
					$db->q("INSERT INTO advertise_camp_section 
							(c_id, s_id, status, section, name, bid, haschild, recursion, cpm) 
							values 
							(?, ?, ?, ?, ?, ?, ?, ?, ?)
							on duplicate key
							update `show` = 1, name = ?, haschild = ?, recursion = ?, bid = ?, approved = if(approved<0,0,approved), cpm = ?",
							array($c_id, $cat, $status_budget, $rox["section"], $rox[$name_string], $dcpc_, $p, $recursion, $cpm,
								$rox[$name_string], $p, $recursion, $dcpc_, $cpm)
							);
				} else  {
					//update old placement
					$db->q("INSERT INTO advertise_camp_section 
							(c_id, s_id, status, section, name, bid, haschild, recursion, approved, cpm) 
							values
							(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
							on duplicate key 
							update `show` = 1, name = ?, haschild = ?, recursion = ?, bid = ?, cpm = ?",
							array($c_id, $cat, $status_budget, $rox["section"], $rox[$name_string], $dcpc_, $p, $recursion, $approved,  $cpm, 
								$rox[$name_string], $p, $recursion, $dcpc_, $cpm)
							);
				}
			}

			//store approved value for all old placements
			$rex = $db->q("select s_id, approved from advertise_camp_section where c_id = ?", array($c_id));
			$oldies = array();
			while($rox=$db->r($rex)) {
				$oldies[$rox["s_id"]] = $rox["approved"];
			}

			//store all selected subsections - go through all selected main section placements
			reset($_POST);
			foreach($_POST as $key=>$value) {
				if (strncmp($key, "selected", 8) || !is_array($value))
					continue;	//this is not placement selection http parameter

				$parent = intval(substr($key, 8));
				foreach($value as $val) {
					$val = intval($val);
					//fetch subsection
					if (strstr($key, 'selectedexternal'))
						$rex = $db->q("SELECT s2.defaultbid, s.section, s.external, s.appneeded, concat(s2.name,' - ',s.name) name, concat(s2.name,' - ',s.n) n 
										FROM advertise_section s 
										INNER JOIN advertise_section s2 on s.parent = s2.id 
										WHERE s.id = ?", array($val));
					else
						$rex = $db->q("SELECT s2.defaultbid, s.section, s.external, s.appneeded, concat(s2.name,' - ',s.name) name, concat(s2.name,' - ',s.n) n 
										FROM advertise_section s 
										INNER JOIN advertise_section s2 on s.parent = s2.id 
										WHERE s.id = ? and s.parent = ?", array($val, $parent));

					if (!$db->numrows($rex))
						continue;

					$rox = $db->r($rex);
					$name_string = $rox["external"] == 1 ? "n" : "name";

					$approved = $rox["appneeded"] == 1 ? 0 : 1;	//approved value for new campaign

					//approved value for old campaign
					if ($rox["appneeded"] == 0)
						$approved2 = 1;
					else
						$approved2 = isset($approved_dont_need) ? 1 : 0;

					//check if there is an active flat deal for this advertiser on this section - if yes, turn on flat_deal_status for this placement
					$flat_deal_status = 0;
					$res = $db->q("SELECT fd.id FROM advertise_flat_deal fd WHERE fd.account_id = ? AND fd.zone_id = ? AND fd.status = 1", [$account->getId(), $cat]);
					if ($db->numrows($res) > 0)
						$flat_deal_status = 1;
					$res = $db->q("SELECT fd.id FROM advertise_flat_deal fd INNER JOIN advertise_deal_zone adz on adz.deal_id = fd.id WHERE fd.account_id = ? AND adz.zone_id = ? AND fd.status = 1", [$account->getId(), $cat]);

					$defaultbid = $params["dcpc"];
					$bid = $params["dcpc"];
					$name = substr($rox[$name_string], 0, 50);
					$db->q("INSERT INTO advertise_camp_section 
							(c_id, s_id, status, section, name, bid, approved, flat_deal_status) 
							values 
							(?, ?, ?, ?, ?, ?, ?, ?) 
							on duplicate key UPDATE bid = ?, `show` = 1, name = ?, approved = ?", 
							array($c_id, $val, $status_budget, $rox["section"], $name, $defaultbid, $approved, $flat_deal_status, $bid, $name, $approved2));

					//if this section is external and 'approve needed'
					if ($rox["external"] == 1 && $rox["appneeded"]) {
						if (($new && $approved == 0)) {
							//if this campaign is new and not approved, add to notify list
							$notify_owner[] = $val;
						} elseif (!$new && 
							((isset($oldies[$val]) && $oldies[$val] == 1 && !isset($approved_dont_need)) 
							|| (isset($oldies[$val]) && $oldies[$val] == 0)
							|| (!isset($oldies[$val])) 
							) ) {
							//OR IF this is edit of campaign and:
							// 1. this placement was assigned before save, placement was approved, but url has changed
							// 2. this placement was assigned before save and placement was not approved
							// 3. this placement was not assigned before save
							$notify_owner[] = $val;
						}
					}
				}
			}

			$db->q("delete from advertise_camp_section_dont where c_id = '$c_id'");
			$x_dont = GetPostParam("sc_dont");
			if( !empty($x_dont) ) {
				$x_donts = explode("|", $x_dont);
				foreach($x_donts as $loc) {
					$loc = intval($loc);
					if( $loc ) $db->q("insert into advertise_camp_section_dont (id, c_id, section, loc) values ((select id from advertise_camp_section 
where c_id = '$c_id' and section = 'sc'), '$c_id', 'sc', '$loc')");
				}
			}
			$x_dont = GetPostParam("as_dont");
			if( !empty($x_dont) ) {
				$x_donts = explode("|", $x_dont);
				foreach($x_donts as $loc) {
					$loc = intval($loc);
					if( $loc ) $db->q("insert into advertise_camp_section_dont (id, c_id, section, loc) values ((select id from advertise_camp_section
where c_id = '$c_id' and section = 'as'), '$c_id', 'as', '$loc')");
				}
			}
			$x_dont = GetPostParam("emp_dont");
			if( !empty($x_dont) ) {
				$x_donts = explode("|", $x_dont);
				foreach($x_donts as $loc) {
					$loc = intval($loc);
					if( $loc ) $db->q("insert into advertise_camp_section_dont (id, c_id, section, loc) values ((select id from advertise_camp_section
where c_id = '$c_id' and section = 'emp'), '$c_id', 'emp', '$loc')");
				}
			}

			$x_dont = GetPostParam("cls_dont");
			if( !empty($x_dont) ) {
				$x_donts = explode("|", $x_dont);
				foreach($_POST['selected15'] as $s_idx) {
					$s_idx = intval($s_idx);
					foreach($x_donts as $loc) {
						$loc = intval($loc);
						if( $loc )
							$db->q("insert into advertise_camp_section_dont (id, c_id, section, loc) values ((select id from advertise_camp_section where c_id = '$c_id' and s_id = '$s_idx'), '$c_id', 'cls', '$loc')");
					}
				}
			}

			$db->q("delete from advertise_camp_section_dont_c where c_id = '$c_id'");
			$geo_dont = GetPostParam("_selectgeo");
			foreach($geo_dont as $key => $value) {
				$value = intval($value);
				if( $value ) $db->q("insert into advertise_camp_section_dont_c (c_id, loc) values ('$c_id', '$value')");
			}
		}

		if( isset($notify_owner) ) {
			foreach($notify_owner as $s_id) {
				$db->q("insert ignore into advertise_notification (account_id, s_id, what, time) values ((select account_id from advertise_section where id = '$s_id'), '$s_id', 'newadd', now())");
			}
		}

		$url = "/advertise/detail?id={$c_id}";
		if ($alert)
			$system->go($url, $alert);
		else
			$system->moved($url);
	}

	if (isset($error))
		$smarty->assign("error", $error);
}

if (empty($cat_selected) && count($cat_original))
	$cat_selected = $cat_original;


//text ads - get all sections with their subsections
$sql = "SELECT s.*, group_concat(s2.id ORDER BY s2.orden) ids, 
			group_concat(s2.name ORDER BY s2.orden) names, group_concat(s2.n ORDER BY s2.orden) ns, max(s2.appneeded) appneeded2 
		FROM advertise_section s
		LEFT JOIN advertise_section s2 on (s.id = s2.parent and (s2.external = 0 or s2.active = 1))
		WHERE s.parent = 0 AND s.type = 'T' AND s.status >= 0 AND (s2.status IS NULL OR s2.status >= 0)
		GROUP BY s.id
		ORDER BY s.orden, s.name";
$rex = $db->q($sql);
$cat_ad = NULL;
$subex = $subex2 = NULL;

while($rox=$db->r($rex)) {
	$ids = $rox["ids"];
	$names = $rox["external"]==0?$rox["names"]:$rox["ns"];
	$sub = $sub2 = NULL;
	
	if ($rox['id'] == 5 || $rox['id'] == 6)	//disable adult websites and tubesites
		continue;

	if (!empty($ids)) {
		$ids = explode(",", $ids);
		$names = explode(",", $names);
		for($i = 0; $i < count($ids); $i++) {
			if( $rox["external"] == 0 ) {
				//internal sections
				if (!in_array($ids[$i], $cat_original)) {
					$sub[] = array(
						"id" => $ids[$i],
						"name" => $names[$i]
					);
				} else {
					$sub2[] = array(
						"id" => $ids[$i],
						"name" => $names[$i]
					);
				}
			} else {
				//external websites
				if (!in_array($ids[$i], $cat_original)) {
					$subex[] = array(
						"id" => $ids[$i],
						"name" => $names[$i],
						"parent" => $rox["name"],
						"defaultbid" => number_format($rox["defaultbid"],2)
					);
				} else {
					$sub2ex[] = array(
						"id" => $ids[$i],
						"name" => $names[$i],
						"parent" => $rox["name"],
						"defaultbid" => number_format($rox["defaultbid"],2),
					);
				}
			}
		}
		if (count($sub) > 0 && count($sub2) == 0) {
			$sub2 = $sub;
			$sub = NULL;
		}
	}

	if ($rox["name"] == "Pop-Under Advertising") {
		$as_id = NULL;
		foreach ($sub2 as $key => $val) {
			if ($val["id"] == 79) {
				$as_id = $key;
				break;
			}
		}
		if ($as_id != NULL)
			unset($sub2[$as_id]);
	}

	if ($rox["external"] == 0) {
		$cat_ad[] = array(
			"id" => $rox["id"],
			"section" => $rox["section"],
			"name" => $rox["name"],
			"hasloc" => $rox["hasloc"],
			"checked" => in_array($rox["id"], $cat_selected),
			"dont" => isset($dont[$rox["section"]])?$dont[$rox["section"]]:"",
			"sub" => $sub,
			"sub2" => $sub2,
			"recursion" => 0,
			"bid" => isset($dbs[$rox["id"]])?$dbs[$rox["id"]]:($rox['defaultbid']?$rox['defaultbid']:0),
			"defaultbid" => $rox['defaultbid'] != 0.2 ? $rox['defaultbid'] : 0,
			"d" => !empty($rox["d"])?$rox["d"]:"",
			"appneeded" => $rox['appneeded'],
			"volume" => $rox["volume"],
		);
	} elseif( count($subex) || count($sub2ex)) {
		if ($rox["appneeded2"] == 1)
			$appneeded2 = true;
		continue;
	}

}

if( $subex || $sub2ex ) {
	if( count($subex) > 0 && count($sub2ex) == 0 ) { $sub2ex = $subex; $subex = NULL; }
	$cat_ad_ex = array("sub"=>$subex, "sub2"=>$sub2ex, "sub_total"=>$sub_total, "sub2_total"=>$sub2_total);
}

//banner ads - get all sections
$sql = "SELECT g.id as group_id, g.name as group_name, g.short_description, s.* 
		FROM advertise_section s
		LEFT JOIN advertise_zone_group g on g.id = s.group_id
		WHERE s.type = 'B' AND s.status > 0
		ORDER BY -g.orden DESC, s.orden, s.name";
$rex = $db->q($sql);
$cat_ban = array();
$last_group_id = NULL;
while ($rox = $db->r($rex)) {
	if ($rox["exclusive_access"] && !account::isImpersonatedAdmin()) {
		$acc_ids = explode(",", $rox["exclusive_access"]);
		if (!$account->isadmin() && (!in_array($account->getId(), $acc_ids)))
			continue;
	}
	$group_id = $rox["group_id"];
	$group_name = $rox["group_name"];
	$group_short_desc = $rox["short_description"];
	if ($group_id) {
		if ($group_id != $last_group_id) {
			$last_group_id = $group_id;
			$cat_ban[] = array(
				"type" => "group",
				"id" => $group_id,
				"name" => $group_name,
				"short_description" => $group_short_desc,
			);
		}
	}
	if (is_null($group_id) && $last_group_id) {
		$last_group_id = $group_id;
		$cat_ban[] = array(
			"type" => "group",
			"id" => 0,
			"name" => "Other websites",
			"short_description" => "",
		);
	}
	$cat_ban[] = array(
		"type" => "zone",
		"id" => $rox["id"],
		"name" => $rox["name"],
		"selected" => (in_array($rox["id"], $cat_selected)) ? true : false,
		"cpm_minimum" => $rox["cpm_minimum"],
		"in_group" => $group_id ? true : false,
		"example_screenshot" => $rox["example_screenshot"],
		"volume" => $rox["volume"],
		"test_link" => ($account->isadmin() || account::isImpersonatedAdmin()) ? advertise::get_test_link($rox["id"], $id) : "",
	);
}

//popunders - get all sections
$sql = "SELECT s.* FROM advertise_section s	WHERE s.type = 'P' AND s.status > 0 ORDER BY s.orden, s.name";
$rex = $db->q($sql);
$cat_pop = array();
while ($rox = $db->r($rex)) {
	if ($rox["exclusive_access"] && !account::isImpersonatedAdmin()) {
		$acc_ids = explode(",", $rox["exclusive_access"]);
		if (!$account->isadmin() && (!in_array($account->getId(), $acc_ids)))
			continue;
	}
	$cat_pop[] = array(
		"id" => $rox["id"],
		"name" => $rox["name"],
		"selected" => (in_array($rox["id"], $cat_selected)) ? true : false,
		"defaultbid" => number_format($rox["defaultbid"], 3),
		"volume" => $rox["volume"],
	);
}

//nav links - get all sections
$sql = "SELECT s.* FROM advertise_section s	WHERE s.type = 'L' AND s.status > 0 ORDER BY s.orden, s.name";
$rex = $db->q($sql);
$cat_link = array();
while ($rox = $db->r($rex)) {
	if ($rox["exclusive_access"] && !account::isImpersonatedAdmin()) {
		$acc_ids = explode(",", $rox["exclusive_access"]);
		if (!$account->isadmin() && (!in_array($account->getId(), $acc_ids)))
			continue;
	}
	$cat_link[] = array(
		"id" => $rox["id"],
		"name" => $rox["name"],
		"selected" => (in_array($rox["id"], $cat_selected)) ? true : false,
		"defaultbid" => number_format($rox["defaultbid"], 3),
		"volume" => $rox["volume"],
		"example_screenshot" => $rox["example_screenshot"],
	);
}

//interstitial links - get all sections
$sql = "SELECT s.* FROM advertise_section s	WHERE s.type = 'I' AND s.status > 0 ORDER BY s.orden, s.name";
$rex = $db->q($sql);
$cat_interstitial = array();
while ($rox = $db->r($rex)) {
	if ($rox["exclusive_access"] && !account::isImpersonatedAdmin()) {
		$acc_ids = explode(",", $rox["exclusive_access"]);
		if (!$account->isadmin() && (!in_array($account->getId(), $acc_ids)))
			continue;
	}
	$cat_interstitial[] = array(
		"id" => $rox["id"],
		"name" => $rox["name"],
		"selected" => (in_array($rox["id"], $cat_selected)) ? true : false,
		"defaultbid" => number_format($rox["defaultbid"], 3),
		"volume" => $rox["volume"],
		"example_screenshot" => $rox["example_screenshot"],
	);
}


//get all advertise images
$res = $db->q("SELECT * FROM advertise_image WHERE account_id = ? AND deleted IS NULL", array($account_id));
$advertise_images = array();
while ($row = $db->r($res)) {
	$advertise_images[] = array(
		"id" => $row["id"],
		"thumb_url" => $config_image_server."/promo/t/{$row["filename"]}",
		"filename" => $row["filename"],
		"width" => $row["width"],
		"height" => $row["height"],
		"uploaded" => date("Y-m-d H:i:s", $row["uploaded"]),
		);
}
$smarty->assign("advertise_images", $advertise_images);


$smarty->assign("cat_ad", $cat_ad);
$smarty->assign("cat_ad_ex", $cat_ad_ex);
$smarty->assign("cat_ban", $cat_ban);
$smarty->assign("cat_pop", $cat_pop);
$smarty->assign("cat_link", $cat_link);
$smarty->assign("cat_interstitial", $cat_interstitial);

if (isset($appneeded2))
	$smarty->assign("appneeded", true);

$smarty->assign("type", $params["type"]);
$smarty->assign("name", $params["name"]);
$smarty->assign("ad_title", $params["ad_title"]);
$smarty->assign("ad_line1", $params["ad_line1"]);
$smarty->assign("ad_line2", $params["ad_line2"]);
$smarty->assign("ad_displayurl", $params["ad_displayurl"]);
$smarty->assign("ad_prefix", $params["ad_prefix"]);
$smarty->assign("ad_url", $params["ad_url"]);
$smarty->assign("popup_url", $params["ad_url"]);
$smarty->assign("budget", $params["budget"]);
$smarty->assign("dcpc", $params["dcpc"]);
$smarty->assign("external", $params["external"]);
$smarty->assign("cpm", $params["cpm"]);
$smarty->assign("code", $params["code"]);
$smarty->assign("banner_iframe_src", $params["banner_iframe_src"]);
$smarty->assign("banner_a_href", $params["banner_a_href"]);
$smarty->assign("banner_img_src", $params["banner_img_src"]);

$smarty->assign("h1title", $new ? "New Campaign" : "Edit Campaign: {$row["name"]}");

include_html_head("js", "/js/tools/jquery.autotab.js?2");
include_html_head("js", "/js/tools/jquery.textareaCounter.plugin.js");

$smarty->assign("new", $new);

if (!$new)
	$smarty->assign("id", $id);

//TODO advertise exceptions
if (!$new) {
	$res = $db->q("select * from advertise_exception where c_id = '$id'");
	while($row=$db->r($res)) {
		$exception[] = array("x"=>$row["exception"]);
	} 
} else {
	if( is_array($_SESSION["advertise_exception"]) ) {
		foreach($_SESSION["advertise_exception"] as $ex)
			$exception[] = array("x"=>$ex);
	}
}
$smarty->assign("exception", $exception);
$smarty->assign("admin", $account->isadmin() || account::is_impersonated());

//ON/OFF switch for popunders
//TODO add into admin options
$smarty->assign("popunder_available", true);

$smarty->assign("nobanner", true);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/edit.tpl");

global $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

?>
