<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;
$system = new system;

if( !($account_id = $account->isloggedin())) {
        $account->asklogin();
        return;
}

$new = isset($_REQUEST["new"])?1:NULL;
$id = isset($_REQUEST["id"]) ? intval($_REQUEST["id"]) : NULL;
if( !$id ) $new = true;
if( !$id && !$new ) die;

if( !$new ) {
	$res = $db->q("select * from advertise_campaign where id = '$id' and account_id = '$account_id' limit 1");
	if( !$db->numrows($res) ) {
		reportAdmin("advertise exception");
		$system->moved("/");
	}
	$row = $db->r($res);

	if( isset($_POST["exception"]) ) {
		$ex = explode("\n", $_POST["exception"]);
		$db->q("delete from advertise_exception where c_id = '$id'");
		foreach($ex as $e) {
			if (!empty($e))
				$db->q("insert into advertise_exception (c_id, exception) values (?, ?)", [$id, $e]);
		}
		if( isset($_REQUEST["pop"]) ) die("Updated!");
		else $smarty->assign("updated", true);
	}

	$res = $db->q("select * from advertise_exception where c_id = '$id'");
	while($row=$db->r($res)) {
		$exception[] = array("x"=>$row["exception"]);
	}
	$smarty->assign("id", $id);
} else {
	if( isset($_POST["exception"]) ) {
		$ex = explode("\n", $_POST["exception"]);
		$_SESSION["advertise_exception"] = NULL;
		foreach($ex as $e) {
			if( !empty($e) ) $_SESSION["advertise_exception"][] = $e;
		}
		if( isset($_REQUEST["pop"]) ) die("Updated!");
		else $smarty->assign("updated", true);
	}

	if( is_array($_SESSION["advertise_exception"]) ) {
		foreach($_SESSION["advertise_exception"] as $ex) 
			$exception[] = array("x"=>$ex);
	}
	$smarty->assign("new", 1);
}

$smarty->assign("exception", $exception);
$smarty->display(_CMS_ABS_PATH."/templates/advertise/advertise_campaign_exception.tpl");

if( isset($_GET["pop"]) ) die;

?>
