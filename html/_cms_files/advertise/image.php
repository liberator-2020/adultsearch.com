<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate, $config_image_path;
$gIndexTemplate = "advertise_index.tpl";
$system = new system;

if (!$account->isloggedin()) {
	$account->asklogin();
	return false;
}

if (!$account->isadmin()) {
	die("You have no privileges to display this page.");
	return false;
}

if ($_REQUEST["action"] == "delete") {
	$id = intval($_REQUEST["id"]);
	if (!$id)
		return error_redirect("Image id# not specified !", "/advertise");

	$res = $db->q("SELECT * FROM advertise_image WHERE id = ?", array($id));
	if ($db->numrows($res) != 1)
		return error_redirect("Can't find image id#{$id} in database !", "/advertise");

	$row = $db->r($res);
	$filename = $row["filename"];

	if ($_REQUEST["live"] == "1") {
		$res = $db->q("DELETE FROM advertise_image WHERE id = ? LIMIT 1", array($id));
		$aff = $db->affected($res);
		if ($aff == 1) {
			//delete from disk
			@unlink($config_image_path."/promo/".$filename);
			@unlink($config_image_path."/promo/t/".$filename);
			return success_redirect("Advertise image #{$id} was succesfully deleted.", "/advertise");
		} else {
			return error_redirect("Error deleting advertise image #{$id}", "/advertise");
		}
	}

	echo "<h1>Do you really want to delete following image #{$id} ?</h1>";
	echo "<span style=\"color: red; font-weight: bold;\">This operation is irreversible.</span><br />\n";
	echo "<img src=\"//img.adultsearch.com/promo/{$filename}\" /><br />\n";
	echo "<form action=\"\">";
	echo "<input type=\"hidden\" name=\"action\" value=\"delete\" />\n";
	echo "<input type=\"hidden\" name=\"id\" value=\"{$id}\" />\n";
	echo "<input type=\"hidden\" name=\"live\" value=\"1\" />\n";
	echo "<input type=\"submit\" name=\"submit\" value=\"Yes, delete\" />\n";
	echo "</form>";
	return;
}

echo "Unknown action!<br />\n";
return;

?>
