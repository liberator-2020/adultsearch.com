<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle;
$system = new system;

if (!$account->isloggedin()) {
	$account->asklogin();
	return false;
}

if (!$account->isadmin()) {
	die("You have no privileges to display this page.");
	return false;
}

//today made
$today = date("m/d/Y");
$today_date = date("Y-m-d");
$today_start = strtotime("00:00:00");
$res = $db->q("
				SELECT s.id, s.name, SUM(bi.cost) as cost
				FROM advertise_banner_impression bi
				INNER JOIN advertise_camp_section cs on bi.cs_id = cs.id
				INNER JOIN advertise_campaign c on c.id = cs.c_id and c.type = 'B'
				INNER JOIN advertise_section s on s.id = cs.s_id
				WHERE bi.stamp >= ?
				GROUP BY s.id
				HAVING SUM(bi.cost) > 0
				ORDER BY 3 DESC
				", 
				array($today_start)
				);
$zones = array();
$total_made = 0;
while ($row = $db->r($res)) {
	$zones[] = array(
		"id" => $row["id"],
		"name" => $row["name"],
		"cost" => $row["cost"],
		);
	$total_made += $row["cost"];
}

$smarty->assign("today", $today);
$smarty->assign("today_total_made", $total_made);
$smarty->assign("today_zones", $zones);

//yesterday made
$yesterday = date("m/d/Y", strtotime("-1 day"));
$yesterday_date = date("Y-m-d", strtotime("-1 day"));
$yesterday_start = strtotime("-1 day", $today_start);
$res = $db->q("
				SELECT s.id, s.name, SUM(bi.cost) as cost
				FROM advertise_banner_impression bi
				INNER JOIN advertise_camp_section cs on bi.cs_id = cs.id
				INNER JOIN advertise_campaign c on c.id = cs.c_id and c.type = 'B'
				INNER JOIN advertise_section s on s.id = cs.s_id
				WHERE bi.stamp >= ? AND bi.stamp <= ?
				GROUP BY s.id
				HAVING SUM(bi.cost) > 0
				ORDER BY 3 DESC
				", 
				array($yesterday_start, $yesterday_start + 86400)
				);
$zones = array();
$total_made = 0;
while ($row = $db->r($res)) {
	$zones[] = array(
		"id" => $row["id"],
		"name" => $row["name"],
		"cost" => $row["cost"],
		);
	$total_made += $row["cost"];
}
$smarty->assign("yesterday", $yesterday);
$smarty->assign("yesterday_total_made", $total_made);
$smarty->assign("yesterday_zones", $zones);



include_html_head("js", "/js/advertise/jquery.flot.js");
include_html_head("js", "/js/advertise/jquery.flot.pie.js");
$smarty->display(_CMS_ABS_PATH."/templates/advertise/income.tpl");

global $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";
?>

