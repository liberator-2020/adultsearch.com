<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";
$gTitle = "Advertising with Adult Search";
$smarty->assign("nobanner", true);

function numeric_pad($request_param_name, $digits) {
	$val = intval($_REQUEST[$request_param_name]);
	return str_pad($val, $digits, "0", STR_PAD_LEFT);
}

if( !($account_id = $account->isloggedin())) {

	if( isset($_GET["section"]) && is_numeric($_GET["section"]) && $_GET["section"]>10000 ) {
		$s = intval($_GET["section"]);
		$res = $db->q("select thumb, name, n from advertise_section where id = '$s'");
		if( $db->numrows($res) ) {
			$row = $db->r($res); 
			$smarty->assign("thumb", $row["thumb"]);
			$smarty->assign("name", $row["name"]);
			$smarty->assign("n", $row["n"]);
			$smarty->assign("section", $s);
			$smarty->display(_CMS_ABS_PATH."/templates/advertise/advertise_home2.tpl");
			return;
		}
	}

	$smarty->display(_CMS_ABS_PATH."/templates/advertise/advertise_main.tpl");
	return;
}

if (!empty($_POST)) {
	$action = GetPostParam("action");
	$chk = $_POST["chk"];
	switch($action) {
		case 'Pause':
			foreach($chk as $id) {
				$id = intval($id);

				$cam = campaign::findOneById($id);
				if ($cam)
					$cam->notifyIfHasFlatDeal("pause");

				$res = $db->q("UPDATE advertise_campaign SET status = 0 WHERE id = ? AND account_id = ? LIMIT 1", [$id, $account_id]);
				if ($db->affected($res) > 0) {
					$res = $db->q("UPDATE advertise_camp_section SET status2 = 0 WHERE c_id = ?", [$id]);
					$aff = $db->affected($res);
					file_log("advertise", "advertise/index: Campaign #{$id} paused, {$aff} placements disabled, author=#{$account_id}");
				}
			}
			break;
		case 'Activate':
			foreach($chk as $id) {
				$id = intval($id);

				$cam = campaign::findOneById($id);
				if ($cam)
					$cam->notifyIfHasFlatDeal("activate");

				$res = $db->q("UPDATE advertise_campaign SET status = 1 WHERE id = ? AND account_id = ? LIMIT 1", [$id, $account_id]);
				if ($db->affected($res) > 0) {
					$res = $db->q("UPDATE advertise_camp_section SET status2 = 1 WHERE c_id = ?", [$id]);
					$aff = $db->affected($res);
					file_log("advertise", "advertise/index: Campaign #{$id} activated, {$aff} placements enabled, author=#{$account_id}");
				}
			}
			break;
		case 'Archive':
			foreach($chk as $id) {
				$id = intval($id);

				$cam = campaign::findOneById($id);
				if ($cam)
					$cam->notifyIfHasFlatDeal("archive");

				$res = $db->q("UPDATE advertise_campaign SET status = -1 WHERE id = ? AND account_id = ? LIMIT 1", [$id, $account_id]);
				if ($db->affected($res) > 0) {
					$res = $db->q("
						UPDATE advertise_camp_section 
						SET status = 0, `show` = 0, approved = 0, flat_deal_status = 0 
						WHERE c_id = ?",
						[$id]
						);
					$aff = $db->affected($res);
					file_log("advertise", "advertise/index: Campaign #{$id} archived, {$aff} placements disabled, author=#{$account_id}");
				}
			}
			break;
	}
	system::moved("/advertise/");
}

$res = $db->q("SELECT budget FROM advertise_budget WHERE account_id = ?", [$account_id]);
$row = $db->r($res); 
if ($row[0] < 1)
	$smarty->assign("fundneeded", true);
$budget = $row[0];
$smarty->assign("budget", number_format($row[0], 2));

if( isset($_POST["new"]) ) {
	$name = trim(GetPostParam("name"));
	if( !empty($name) ) $db->q("insert into advertise_campaign (account_id, name) values ('$account_id', '$name')");
	else { $smarty->assign("error", "Campaign name may not be blank"); $smarty->assign("showadd", true); }
}

$res = $db->q("
	SELECT day(date), month(date), year(date) 
	FROM advertise_campaign c 
	INNER JOIN (advertise_camp_section s inner join `advertise_data` d on s.id = d.s_id) on c.id = s.c_id 
	WHERE c.account_id = ?
	ORDER BY `date` 
	LIMIT 1
	",
	[$account_id]
	);

if ($db->numrows($res)) {
	$row = $db->r($res);
	$dayFrom = $row[0];
	$monthFrom = $row[1];
	$yearFrom = $row[2];
} else {
	$dayFrom = date("d");
	$monthFrom = date("m");
	$yearFrom = date("Y");
}

$range_or_period = GetGetParam("range_or_period");
if( $range_or_period != 2 ) {
	$dayTo = date("d");
	$monthTo = date("m");
	$yearTo = date("Y");
	$period = $_GET["period"];

	$today_start = strtotime("00:00:00");
	$stamp_end = time() + 5;

	switch($period) {
		case 1:
			//last 7 days
			$time_sql = "and (date >= date_sub(curdate(), interval 6 day) or d.s_id is null)";
			$time_sql = "and date >= date_sub(curdate(), interval 6 day)";
			$stamp_start = strtotime("-6 day", $today_start);
			break;
		case 2:
			//yesterday
			$time_sql = "and date = date_sub(curdate(), interval 1 day)";
			$stamp_start = strtotime("-1 day", $today_start);
			$stamp_end = $today_start - 1;
			break;
		case 3:
			//last 30 days
			$time_sql = "and date >= date_sub(curdate(), interval 29 day)";
			$stamp_start = strtotime("-29 day", $today_start);
			break;
		case 4:
			//this month
			getMonthRange($start, $end, 0);
			$time_sql = "and date >= '$start' and date <= '$end'";
			$stamp_start = mktime(0, 0, 0, date("n"), 1);
			$stamp_end = mktime(23, 59, 59, date("n"), date("t"));
			break;
		case 5:
			//last month
			getMonthRange($start, $end, 1);
			$time_sql = "and date >= '$start' and date <= '$end'";
			$stamp_start = strtotime("first day of previous month");
			$stamp_end = strtotime("last day of previous month") + 86400 - 1;
			break;
		default:
			//today
			$time_sql = "and date = curdate()";
			$stamp_start = $today_start;
			break;
	}

} else {
	$dayFrom = numeric_pad("dayFrom", 2);
	$monthFrom = numeric_pad("monthFrom", 2);
	$yearFrom = numeric_pad("yearFrom", 4);
	
	$dayTo = numeric_pad("dayTo", 2);
	$monthTo = numeric_pad("monthTo", 2);
	$yearTo = numeric_pad("yearTo", 4);

	$stamp_start = mktime(0, 0, 0, $monthFrom, $dayFrom, $yearFrom);
	$stamp_end = mktime(23, 59, 59, $monthTo, $dayTo, $yearTo);

	$time_sql = "and `date` >= '{$yearFrom}-{$monthFrom}-{$dayFrom}' and `date` <= '{$yearTo}-{$monthTo}-{$dayTo}'";

}

if ($stamp_start > $stamp_end)
	$smarty->assign("report_error", "Wrong date selection.");

$stamp_time_sql = "and stamp >= '{$stamp_start}' and stamp <= '{$stamp_end}'";

$res = $db->q("(SELECT c.*, 
				sum(d.impression) impression, 0 as click,
				(SELECT SUM(bi.cost * IF(bi.fixed=1,0,1)) FROM advertise_banner_impression bi WHERE bi.c_id = c.id {$stamp_time_sql}) as cost,
				(select count(*) from advertise_camp_section s where s.c_id = c.id and s.budget_available=0) as budgetx
			FROM advertise_campaign c 
			LEFT JOIN advertise_data d on (c.id = d.c_id $time_sql) 
			WHERE c.account_id = ? and c.status > -1 AND c.type = 'B'
			GROUP BY c.id)
			UNION
			(
			SELECT c.*,
				sum(impression) impression, sum(click) as click, sum(cost) as cost, 
				(select count(*) from advertise_camp_section s where s.c_id = c.id and s.budget_available=0) as budgetx 
			FROM advertise_campaign c 
			LEFT JOIN advertise_data d on (c.id = d.c_id $time_sql) 
			WHERE c.account_id = ? and c.status > -1 AND c.type <> 'B'
			GROUP BY c.id
			)", 
			array($account_id, $account_id)
		);

$total_i = $total_c = $total_cost = $total_a = 0;

while ($row = $db->r($res)) {

	$budget = ($row["budget"] >= 10000) ? "exclusive" : "<a href=\"/advertise/edit?id={$row["id"]}\">$".number_format($row["budget"], 2)."</a>";

	$campaign[] = array(
		"id" => $row["id"],
		"created_stamp" => $row["created_stamp"],
		"type" => $row["type"],
		"name" => ucfirst($row["name"]),
		"budget" => $budget,
		"status" => $row["status"],
		"category" => $row["category"],
		"impression" => number_format($row["impression"]),
		"click" => number_format($row["click"]),
		"cost" => number_format($row["cost"], 2),
		"acpc" => number_format($row["cost"]/$row["click"], 3),
		"budgetx" => $row['budgetx']?1:0
	);

	$total_i += $row["impression"];
	$total_c += $row["click"];
	$total_cost += $row["cost"];
}

if (!count($campaign) && $budget == 0 && !$account->isAdvertiser()) {
	$smarty->display(_CMS_ABS_PATH."/templates/advertise/advertise_main.tpl");
	return;
}

$smarty->assign("campaign", $campaign);
$smarty->assign("total_i", number_format($total_i));
$smarty->assign("total_c", number_format($total_c));
$smarty->assign("total_cost", number_format($total_cost, 2));
$smarty->assign("total_a", number_format($total_cost/$total_c,2));

$smarty->assign("dayFrom", $dayFrom);
$smarty->assign("monthFrom", $monthFrom);
$smarty->assign("yearFrom", $yearFrom);
$smarty->assign("dayTo", $dayTo);
$smarty->assign("monthTo", $monthTo);
$smarty->assign("yearTo", $yearTo);
$smarty->assign("id", $id);
$smarty->assign("period", $_GET["period"]);
$smarty->assign("range_or_period", $_GET["range_or_period"]);


$smarty->display(_CMS_ABS_PATH."/templates/advertise/main.tpl");

?>

