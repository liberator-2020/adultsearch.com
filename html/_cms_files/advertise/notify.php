<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;
$form = new form;
$system = new system;

if (!($account_id=$account->isLoggedIn())) {
	$account->asklogin();
	return;
}

if( isset($_POST["budget_notify"]) ) {
	$budget_notify = intval($_POST["budget_notify"]);
	$db->q("update advertise_budget set budget_notify = '$budget_notify' where account_id = '$account_id'");
	$system->moved('addfunds');
}

$res = $db->q("select * from advertise_budget where account_id = '$account_id'");
if( !$db->numrows($res) ) $system->go("/advertise/addfunds", "First you need to add funds to your account");
$row = $db->r($res); 

$amount[] = array("5"=>"\$5.00");
$amount[] = array("10"=>"\$10.00");
for($i=25;$i<1025;$i+=25) {
	$amount[] = array("$i"=>"\$".number_format($i, 2));
	if( $i >= 100 ) $i += 75;
}

$notify_amount = $form->select_add_array($amount, $row["budget_notify"]);

$smarty->assign("notify_amount", $notify_amount);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/advertise_notify.tpl");

global $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

?>
