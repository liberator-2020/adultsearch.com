<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage"))
	die("Invalid access!");

$red = new red();

$res = $db->q("
	SELECT cs.id, cs.impressions_since, cs.c_id, cs.s_id, c.type, c.name as c_name, s.name as s_name
	FROM advertise_camp_section cs
	INNER JOIN advertise_campaign c on cs.c_id = c.id
	INNER JOIN advertise_section s on cs.s_id = s.id
	WHERE cs.approved = 1 AND cs.show = 1 AND c.status = 1
		AND (
			(cs.status = 1 AND cs.budget_available = 1 AND (c.expire = 0 or c.expire > unix_timestamp()) AND c.flat_deal = 0)
			OR 
			cs.flat_deal_status = 1
			)
	");

$placements = [];
while ($row = $db->r($res)) {
	$cs_id = $row["id"];
	$c_type = $row["type"];
	$c_id = $row["c_id"];
	$c_name = $row["c_name"];
	$s_id = $row["s_id"];
	$s_name = $row["s_name"];

	$cs_is = $cs_is2 = null;
	if ($c_type == "B") {
		$cs_is = $row["impressions_since"];
		$cs_is2 = $red->get("as_ad_cs_{$cs_id}_is");
	}

	$placements[] = [
		"cs_id" => $cs_id,
		"cs_is" => $cs_is,
		"cs_is2" => $cs_is2,
		"c_id" => $c_id,
		"c_type" => $c_type,
		"c_name" => $c_name,
		"s_id" => $s_id,
		"s_name" => $s_name,
		];
}
//_darr($placements);
$smarty->assign("placements", $placements);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/placement_stats.tpl");

?>
