<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";
$form = new form;
$system = new system;

if (!($account_id=$account->isLoggedIn())) {
	$account->asklogin();
	return;
}

if( isset($_POST["recurring"]) ) {
	$recurring = intval($_POST["recurring"]);
	$recurring_amount = intval($_POST["recurring_amount"]);
	$recurring_max = intval($_POST["recurring_max"]);
	$cc = intval($_POST["cc"]);

	if( !$recurring ) {
		$db->q("update advertise_budget set recurring = 0 where account_id = ?", array($account_id));
		$smarty->assign("note", "Changes are made.");
	} else {
		if (!$recurring_amount)
			$smarty->assign("note", "You need to set a 'Recharge Amount'");
		$res = $db->q("select * from account_cc where cc_id = '$cc' and account_id = '$account_id' and deleted = 0");
		if( !$db->numrows($res) ) {
			reportAdmin("AS: Advertise recurring wrong CC", "Advertise recurring wrong CC", array("account_id" => $account_id, "cc_id" => $cc));
			$system->go("http://adultsearch.com/advertise/", "Error! Report has been generated and technical team has been notified.");
		}
		$db->q("UPDATE advertise_budget
				SET recurring = 1, recurring_amount = ?, recurring_max = ?, cc_id = ?
				WHERE account_id = ?",
				array($recurring_amount, $recurring_max, $cc_id, $account_id)
				);
		reportAdmin("AS: Advertise - recurring is enabled!", "advertise - recurring is enabled for $account_id", array("account_id" => $account_id));
		$smarty->assign("note", "Changes are made.");
	}
}

$res = $db->q("select * from advertise_budget where account_id = ?", array($account_id));
if (!$db->numrows($res))
	$system->go("/advertise/addfunds", "First you need to add funds to your account");

$row = $db->r($res);
$smarty->assign("budget", number_format($row[0], 2));
$smarty->assign("recurring", $row["recurring"]>=1?"On":"Off");

$rex = $db->q("select * from account_cc where account_id = ? and deleted = 0", array($account_id));
if (!$db->numrows($rex))
	$system->go("/advertise/addfunds", "You do not have a credit card in the system, please add funds first.");

while($rox = $db->r($rex)) {
	$card = substr($rox["cc"], -4);
	$ccs[] = array($rox["cc_id"]=>"XXXX$card - Exp {$rox["expmonth"]}/{$rox["expyear"]}");
}

$cc = $form->select_add_array($ccs, $row["cc_id"]);

$recurring = $form->select_add_yesno($row["recurring"]>=1?1:0);
for($i=25;$i<1025;$i+=25) 
	$amount[] = array("$i"=>"\$".number_format($i, 2));

for($i=100;$i<10001;$i+=100)
	$max[] = array("$i"=>"\$".number_format($i, 2));

$recurring_amount = $form->select_add_array($amount, $row["recurring_amount"]);
$recurring_max = $form->select_add_array($max, $row["recurring_max"]);

$smarty->assign("recurring", $recurring);
$smarty->assign("recurring_amount", $recurring_amount);
$smarty->assign("recurring_max", $recurring_max);
$smarty->assign("cc", $cc);
$smarty->assign("recurring_total", $row['recurring_total']);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/advertise_recurring.tpl");

?>
