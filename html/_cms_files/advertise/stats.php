<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage"))
    die("Invalid access!");

require_once(_CMS_ABS_PATH."/_cms_files/advertise/common.php");


//-------------------
//export or display ?
$export = false;
if ($_REQUEST["export"] == 1)
	$export = true;


//-------
//filters
$time_array = get_date_stamp_range();
if ($time_array === false) {
	echo "Wrong time selection!<br />\n";
	return;
}

$date_sql = "AND d.date >= '{$time_array["date_start"]}' AND d.date <= '{$time_array["date_end"]}'";
if ($time_array["date_start"] == $time_array["date_end"])
	$date_sql = "AND d.date = '{$time_array["date_start"]}'";

$type = $_REQUEST["type"];
$type_sql = "";
switch ($type) {
	case 'T':	$type_sql = "AND s.type = 'T'"; break;
	case 'B':	$type_sql = "AND s.type = 'B'"; break;
	case 'P':	$type_sql = "AND s.type = 'P'"; break;
	default:	$type = "";
}
$smarty->assign("type", $type);


if ($export) {
	$fp = fopen(_CMS_ABS_PATH.'/UserFiles/excel/advertise_stats.csv', 'w');
    $fields = "\"Zone Id\",\"Type\",\"Name\",\"Impression\",\"Click\",\"Cost\"\n";
    fputs($fp, $fields);
}


//--------------------
// get data
//we need to use subselect for banner costs
$query = "SELECT s.id as s_id, s.type, s.name,
            	SUM(d.impression) as impression, SUM(d.click) as click, SUM(d.cost) as cost,
				(SELECT SUM(cost) FROM advertise_banner_impression WHERE s_id = s.id AND stamp >= '{$time_array["stamp_start"]}' AND stamp <= '{$time_array["stamp_end"]}') as bi_cost
			FROM advertise_section s
            LEFT JOIN advertise_data d on d.realsid = s.id {$date_sql}
            WHERE s.status > 0 AND s.parent = 0 {$type_sql}
            GROUP BY s.id
			";
if ($type == "T" || $type == "P")	//if we dont need banner stats, the sql is simpler
	$query = "SELECT s.id as s_id, s.type, s.name,
    	        	SUM(d.impression) as impression, SUM(d.click) as click, SUM(d.cost) as cost
				FROM advertise_section s
	            LEFT JOIN advertise_data d on d.realsid = s.id {$date_sql}
    	        WHERE s.status > 0 AND s.parent = 0 {$type_sql}
        	    GROUP BY s.id
				";

//_d("query='{$query}'");
$res = $db->q($query);

$total_i = $total_c = $total_cost = $total_a = 0;
$zones = array();
while ($row = $db->r($res)) {
	$cost = ($row["type"] == "B") ? number_format($row["bi_cost"], 2) : number_format($row["cost"], 2);
	$zones[] = array(
		"id" => $row["s_id"],
		"type" => $row["type"],
		"name" => $row["name"],
		"impression" => number_format($row["impression"]),
		"click" => number_format($row["click"]),
		"cost" => $cost,
	);
	$total_i += $row["impression"];
	$total_c += $row["click"];
	$total_cost += $row["cost"] + $row["bi_cost"];
        
	if ($export) {
		$fields = "\"{$row["s_id"]}\",\"{$row["type"]}\",\"{$row["name"]}\",\"{$row["impression"]}\",\"{$row["click"]}\",\"{$cost}\"\n";
        fputs($fp, $fields);
	}
}

if ($export) {
	fclose($fp);
    system::moved("http://adultsearch.com/UserFiles/excel/advertise_stats.csv");
}

$smarty->assign("zones", $zones);

$smarty->assign("total_i", number_format($total_i));
$smarty->assign("total_c", number_format($total_c));
$smarty->assign("total_cost", number_format($total_cost, 2));

$smarty->display(_CMS_ABS_PATH."/templates/advertise/stats.tpl");

?>
