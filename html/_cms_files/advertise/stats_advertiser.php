<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage"))
	die("Invalid access!");

require_once(_CMS_ABS_PATH."/_cms_files/advertise/common.php");


$advertiser_id = intval($_REQUEST["id"]);
if (!$advertiser_id) {
	echo "No advertiser id!";
	return;
}

$res = $db->q("SELECT a.account_id, a.email, a.username, ab.nickname, ab.company 
				FROM account a 
				INNER JOIN advertise_budget ab on a.account_id = ab.account_id 
				WHERE a.account_id = ?", 
				array($advertiser_id));
if ($db->numrows($res) != 1) {
	echo "Cant find advertiser #{$advertiser_id}!";
	return;
}
$row = $db->r($res);
$smarty->assign("advertiser", $row);

$group_zone = true;

//-------------------
//export or display ?
$export = false;
if ($_REQUEST["export"] == 1)
	$export = true;


//-------
//filters
$time_array = get_date_stamp_range(3);	//default period is last 30 days (->3)
if ($time_array === false) {
	echo "Wrong time selection!<br />\n";
	return;
}
//_darr($time_array);

$date_sql = "AND d.date >= '{$time_array["date_start"]}' AND d.date <= '{$time_array["date_end"]}'";
if ($time_array["date_start"] == $time_array["date_end"])
	$date_sql = "AND d.date = '{$time_array["date_start"]}'";

$time_sql = "AND bi.stamp >= {$time_array["stamp_start"]} AND bi.stamp <= {$time_array["stamp_end"]}";


$group = $_REQUEST["group"];
if ($group != "daily" && $group != "monthly" && $group != "yearly" && $group != "alltime")
	$group = "daily";
$smarty->assign("group", $group);

if ($export) {
	$filename = "advertiser_stats_{$advertiser_id}_{$time_array["date_start"]}_{$time_array["date_end"]}.csv";
	$filepath = _CMS_ABS_PATH."/UserFiles/excel/{$filename}";
	$fp = fopen($filepath, 'w');
}



//----------
// get stats

//if ($type == "B" || $type == "P" || $zone_id < 10000 || $zone_id == 10058 || $zone_id == 10059) {
	$table = "advertise_data";
	$date_col = "date";
	$sid_col = "realsid";
//} else {
//	$table = "advertise_fraud";
//	$date_col = "time";
//	$sid_col = "s_id";
//}

if ($group == "daily")
	$sql = "SELECT YEAR(ad.date) as year, MONTH(ad.date) as month, DAY(ad.date) as day, ad.realsid zone_id, ads.type, ads.nickname, 
				SUM(ad.impression) as impression_count, SUM(ad.click) as click_count, SUM(ad.click)/SUM(ad.impression)*100 as ctr
			FROM advertise_data ad
			INNER JOIN advertise_campaign ac on ac.id = ad.c_id
			INNER JOIN advertise_section ads on ads.id = ad.realsid
			WHERE ac.account_id = '{$advertiser_id}' AND ad.date >= '{$time_array["date_start"]}' AND ad.date <= '{$time_array["date_end"]}' 
				AND (ads.type = 'B' OR ads.type = 'P' OR ads.type = 'L' OR ads.id < 10000 OR ads.id = 10058 OR ads.id = 10059)
			GROUP BY YEAR(ad.date), MONTH(ad.date), DAY(ad.date), ad.realsid
			ORDER BY 1 DESC, 2 DESC, 3 DESC, 4 ASC";
else if ($group == "monthly")
	$sql = "SELECT YEAR(ad.date) as year, MONTH(ad.date) as month, ad.realsid zone_id, ads.type, ads.nickname, 
				SUM(ad.impression) as impression_count, SUM(ad.click) as click_count, SUM(ad.click)/SUM(ad.impression)*100 as ctr
			FROM advertise_data ad
			INNER JOIN advertise_campaign ac on ac.id = ad.c_id
			INNER JOIN advertise_section ads on ads.id = ad.realsid
			WHERE ac.account_id = '{$advertiser_id}' AND ad.date >= '{$time_array["date_start"]}' AND ad.date <= '{$time_array["date_end"]}'
				AND (ads.type = 'B' OR ads.type = 'P' OR ads.type = 'L' OR ads.id < 10000 OR ads.id = 10058 OR ads.id = 10059)
			GROUP BY YEAR(ad.date), MONTH(ad.date), ad.realsid
			ORDER BY 1 DESC, 2 DESC, 3 ASC";
else if ($group == "yearly")
	$sql = "SELECT YEAR(ad.date) as year, ad.realsid zone_id, ads.type, ads.nickname, 
				SUM(ad.impression) as impression_count, SUM(ad.click) as click_count, SUM(ad.click)/SUM(ad.impression)*100 as ctr
			FROM advertise_data ad
			INNER JOIN advertise_campaign ac on ac.id = ad.c_id
			INNER JOIN advertise_section ads on ads.id = ad.realsid
			WHERE ac.account_id = '{$advertiser_id}' AND ad.date >= '{$time_array["date_start"]}' AND ad.date <= '{$time_array["date_end"]}'
				AND (ads.type = 'B' OR ads.type = 'P' OR ads.type = 'L' OR ads.id < 10000 OR ads.id = 10058 OR ads.id = 10059)
			GROUP BY YEAR(ad.date), ad.realsid zone_id 
			ORDER BY 1 DESC, 2 ASC";
else
	$sql = "SELECT ad.realsid zone_id, ads.type, ads.nickname,
				SUM(ad.impression) as impression_count, SUM(ad.click) as click_count, SUM(ad.click)/SUM(ad.impression)*100 as ctr
			FROM advertise_data ad
			INNER JOIN advertise_campaign ac on ac.id = ad.c_id
			INNER JOIN advertise_section ads on ads.id = ad.realsid
			WHERE ac.account_id = '{$advertiser_id}'
				AND (ads.type = 'B' OR ads.type = 'P' OR ads.type = 'L' OR ads.id < 10000 OR ads.id = 10058 OR ads.id = 10059)
			GROUP BY ad.realsid";
//_d("SQL='{$sql}'");
$res = $db->q($sql);
$nr = $db->numrows($res);

$stats = array();
$zones = array();
$total_impressions = $total_clicks = array();

while ($row = $db->r($res)) {
	$key = $row["year"]."|".$row["month"]."|".$row["day"];
	if (!array_key_exists($row["zone_id"], $zones))
		$zones[$row["zone_id"]] = array(
			"id" => $row["zone_id"],
			"nickname" => $row["nickname"],
			"type" => $row["type"],
			);
	$zone_stats = array(
		"zone_id" => $row["zone_id"],
		"impressions" => $row["impression_count"],
		"clicks" => $row["click_count"],
		);
	if (!array_key_exists($key, $stats)) {
		$stats[$key] = array(
			"year" => $row["year"],
			"month" => $row["month"],
			"day" => $row["day"],
			"zones" => array(
				$row["zone_id"] => $zone_stats
				)
			);
	} else {
		$arr = $stats[$key]["zones"];
		$arr[$row["zone_id"]] = $zone_stats;
		$stats[$key]["zones"] = $arr;
	}
	$total_impressions[$row["zone_id"]] = intval($total_impressions[$row["zone_id"]]) + $row["impression_count"];
	$total_clicks[$row["zone_id"]] = intval($total_clicks[$row["zone_id"]]) + $row["click_count"];
}

//_d("stats:");
//_darr($stats);
//_d("total_impressions:");
//_darr($total_impressions);
//_d("total_clicks:");
//_darr($total_clicks);

if ($nr && $export) {
	//header
	$fields = "";
	if ($group == "daily" || $group == "monthly" || $group == "yearly")
		$fields .= "Year,";
	if ($group == "daily" || $group == "monthly")
		$fields .= "Month,";
	if ($group == "daily")
		$fields .= "Day,";
	foreach ($zones as $zone_id => $zone) {
		$fields .= "{$zone["nickname"]},";
	}
	$fields = substr($fields, 0, -1);
	$fields .= "\n";
	fputs($fp, $fields);

	//rows
	foreach ($stats as $key => $val) {
		$arr = explode("|", $key);
		$fields = "";
		if ($group == "daily" || $group == "monthly" || $group == "yearly")
			$fields .= "{$arr[0]},";
		if ($group == "daily" || $group == "monthly")
			$fields .= "{$arr[1]},";
		if ($group == "daily")
			$fields .= "{$arr[2]},";
		foreach ($zones as $zone_id => $zone) {
			if (array_key_exists($zone_id, $val["zones"]))
				if ($zone["type"] == "P" || $zone["type"] == "T" || $zone["type"] == "L")
					$fields .= "{$val["zones"][$zone_id]["clicks"]}";
				else
					$fields .= "{$val["zones"][$zone_id]["impressions"]}";
			$fields .= ",";
		}
		$fields = substr($fields, 0, -1);
		$fields .= "\n";
		fputs($fp, $fields);
	}

	fclose($fp);
	system::moved("http://adultsearch.com/UserFiles/excel/{$filename}");
}


$smarty->assign("zones", $zones);
$smarty->assign("stats", $stats);
$smarty->assign("total_impressions", $total_impressions);
$smarty->assign("total_clicks", $total_clicks);
$smarty->display(_CMS_ABS_PATH."/templates/advertise/stats_advertiser.tpl");

?>
