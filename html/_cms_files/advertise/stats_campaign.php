<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage"))
    die("Invalid access!");

require_once(_CMS_ABS_PATH."/_cms_files/advertise/common.php");


$c_id = intval($_REQUEST["id"]);
if (!$c_id) {
	echo "No campaign id!";
	return;
}

$res = $db->q("SELECT * FROM advertise_campaign WHERE id = ?", array($c_id));
if ($db->numrows($res) != 1) {
	echo "Cant find campaign #{$c_id}!";
	return;
}
$row = $db->r($res);
$type = $row["type"];
$smarty->assign("campaign", $row);

//-------------------
//export or display ?
$export = false;
if ($_REQUEST["export"] == 1)
	$export = true;


//-------
//filters
$time_array = get_date_stamp_range(3);	//default period is last 30 days (->3)
if ($time_array === false) {
	echo "Wrong time selection!<br />\n";
	return;
}

//_darr($time_array);

$date_sql = "AND d.date >= '{$time_array["date_start"]}' AND d.date <= '{$time_array["date_end"]}'";
if ($time_array["date_start"] == $time_array["date_end"])
	$date_sql = "AND d.date = '{$time_array["date_start"]}'";

$time_sql = "AND bi.stamp >= {$time_array["stamp_start"]} AND bi.stamp <= {$time_array["stamp_end"]}";


$group = $_REQUEST["group"];
if ($group != "daily" && $group != "monthly" && $group != "yearly" && $group != "alltime")
	$group = "daily";
$smarty->assign("group", $group);

if ($export) {
	$fp = fopen(_CMS_ABS_PATH.'/UserFiles/excel/advertise_campaign_stats.csv', 'w');
}


//-----------
// get profit
if ($type != "B") {
	die("TODO");
	/*
	if ($group == "daily")
		$sql = "SELECT YEAR(ast.date) as year, MONTH(ast.date) as month, DAY(ast.date) as day, ROUND(SUM(ast.profit),2) as profit
				FROM advertise_stat ast
				WHERE ast.s_id = '{$zone_id}'
				GROUP BY YEAR(ast.date), MONTH(ast.date), DAY(ast.date)
				ORDER BY 1 DESC, 2 DESC, 3 DESC
				LIMIT 60";
	else if ($group == "monthly")
		$sql = "SELECT YEAR(ast.date) as year, MONTH(ast.date) as month, ROUND(SUM(ast.profit),2) as profit
				FROM advertise_stat ast
				WHERE ast.s_id = '{$zone_id}'
				GROUP BY YEAR(ast.date), MONTH(ast.date)
				ORDER BY 1 DESC, 2 DESC";
	else if ($group == "yearly")
		$sql = "SELECT YEAR(ast.date) as year, ROUND(SUM(ast.profit),2) as profit
				FROM advertise_stat ast
				WHERE ast.s_id = '{$zone_id}'
				GROUP BY YEAR(ast.date)
				ORDER BY 1 DESC";
	else
		$sql = "SELECT ROUND(SUM(ast.profit),2) as profit
				FROM advertise_stat ast
				WHERE ast.s_id = '{$zone_id}'";
	*/
} else {
	if ($group == "daily")
		$res = $db->q("
				SELECT YEAR(FROM_UNIXTIME(bi.stamp)) as year, MONTH(FROM_UNIXTIME(bi.stamp)) as month, DAY(FROM_UNIXTIME(bi.stamp)) as day, ROUND(SUM(bi.cost),2) as profit
				FROM advertise_banner_impression bi
				WHERE bi.c_id = ? {$time_sql}
				GROUP BY YEAR(FROM_UNIXTIME(bi.stamp)), MONTH(FROM_UNIXTIME(bi.stamp)), DAY(FROM_UNIXTIME(bi.stamp))
				ORDER BY 1 DESC, 2 DESC, 3 DESC
				LIMIT 60",
				array($zone_id));
	else if ($group == "monthly")
		$res = $db->q("
				SELECT YEAR(FROM_UNIXTIME(bi.stamp)) as year, MONTH(FROM_UNIXTIME(bi.stamp)) as month, ROUND(SUM(bi.cost),2) as profit
				FROM advertise_banner_impression bi
				WHERE bi.c_id = ? {$time_sql}
				GROUP BY YEAR(FROM_UNIXTIME(bi.stamp)), MONTH(FROM_UNIXTIME(bi.stamp))
				ORDER BY 1 DESC, 2 DESC",
				array($zone_id));
	else if ($group == "yearly")
		$res = $db->q("
				SELECT YEAR(FROM_UNIXTIME(bi.stamp)) as year, ROUND(SUM(bi.cost),2) as profit
				FROM advertise_banner_impression bi
				WHERE bi.c_id = ? {$time_sql}
				GROUP BY YEAR(FROM_UNIXTIME(bi.stamp))
				ORDER BY 1 DESC",
				array($zone_id));
	else
		$res = $db->q("
				SELECT ROUND(SUM(bi.cost),2) as profit
				FROM advertise_banner_impression bi
				WHERE bi.c_id = ?",
				array($zone_id));
}
$profits = array();
while ($row = $db->r($res)) {
	if ($group == "daily")
		$key = $row["year"].$row["month"].$row["day"];
	else if ($group == "monthly")
		$key = $row["year"].$row["month"];
	else if ($group == "yearly")
		$key = $row["year"];
	else
		$key = "alltime";
	$profits[$key] = $row["profit"];
}
//_d("profits:");
//_darr($profits);


//----------
// get stats

if ($type == "B" || $zone_id < 10000 || $zone_id == 10058 || $zone_id == 10059) {
	$table = "advertise_data";
	$date_col = "date";
	$cid_col = "c_id";
} else {
	die("TODO");
	/*
	$table = "advertise_fraud";
	$date_col = "time";
	$sid_col = "s_id";
	*/
}

if ($group == "daily")
	$sql = "SELECT YEAR(af.{$date_col}) as year, MONTH(af.{$date_col}) as month, DAY(af.{$date_col}) as day, SUM(af.impression) as impression_count, SUM(af.click) as click_count, SUM(af.click)/SUM(af.impression)*100 as ctr
			FROM {$table} af
			WHERE af.{$cid_col} = '{$c_id}' AND af.{$date_col} >= '{$time_array["date_start"]}' AND af.{$date_col} <= '{$time_array["date_end"]}'
			GROUP BY YEAR(af.{$date_col}), MONTH(af.{$date_col}), DAY(af.{$date_col})
			ORDER BY 1 DESC, 2 DESC, 3 DESC
			LIMIT 60";
else if ($group == "monthly")
	$sql = "SELECT YEAR(af.{$date_col}) as year, MONTH(af.{$date_col}) as month, SUM(af.impression) as impression_count, SUM(af.click) as click_count, SUM(af.click)/SUM(af.impression)*100 as ctr
			FROM {$table} af
			WHERE af.{$cid_col} = '{$c_id}' AND af.{$date_col} >= '{$time_array["date_start"]}' AND af.{$date_col} <= '{$time_array["date_end"]}'
			GROUP BY YEAR(af.{$date_col}), MONTH(af.{$date_col})
			ORDER BY 1 DESC, 2 DESC";
else if ($group == "yearly")
	$sql = "SELECT YEAR(af.{$date_col}) as year, SUM(af.impression) as impression_count, SUM(af.click) as click_count, SUM(af.click)/SUM(af.impression)*100 as ctr
			FROM {$table} af
			WHERE af.{$cid_col} = '{$c_id}' AND af.{$date_col} >= '{$time_array["date_start"]}' AND af.{$date_col} <= '{$time_array["date_end"]}'
			GROUP BY YEAR(af.{$date_col})
			ORDER BY 1 DESC";
else
	$sql = "SELECT SUM(af.impression) as impression_count, SUM(af.click) as click_count, SUM(af.click)/SUM(af.impression)*100 as ctr
			FROM {$table} af
			WHERE af.{$cid_col} = '{$c_id}'";
//_d("SQL='{$sql}'");
$res = $db->q($sql);
$nr = $db->numrows($res);

$stats = array();
if ($nr) {
	//header
	if ($export) {
		$fields = "";
		if ($group == "daily" || $group == "monthly" || $group == "yearly")
			$fields .= "Year,";
		if ($group == "daily" || $group == "monthly")
			$fields .= "Month,";
		if ($group == "daily")
			$fields .= "Day,";
		$fields .= "Impressions, Clicks, CTR (%), Profit (\$)\n";
		fputs($fp, $fields);
	}

	//rows
	while ($row = $db->r($res)) {
		$profit = 0;
		if ($group == "daily")
			$profit = $profits[$row["year"].$row["month"].$row["day"]];
		else if ($group == "monthly")
			$profit = $profits[$row["year"].$row["month"]];
		else if ($group == "yearly")
			$profit = $profits[$row["year"]];
		else
			$profit = $profits["alltime"];
		$profit = number_format(floatval($profit), 2);

		if ($export) {
			$fields = "";
			if ($group == "daily" || $group == "monthly" || $group == "yearly")
				$fields .= "{$row["year"]},";
			if ($group == "daily" || $group == "monthly")
				$fields .= "{$row["month"]},";
			if ($group == "daily")
				$fields .= "{$row["day"]},";
			$fields .= "{$row["impression_count"]},{$row["click_count"]},".number_format($row["ctr"],2).",".$profit."\n";
			fputs($fp, $fields);
		} else {
			$stats[] = array(
				"year" => $row["year"],
				"month" => $row["month"],
				"day" => $row["day"],
				"impressions" => number_format($row["impression_count"]),
				"clicks" => number_format($row["click_count"]),
				"ctr" => number_format($row["ctr"],2),
				"profit" => $profit,
				);
		}
	}

	if ($export) {
		fclose($fp);
		system::moved("http://adultsearch.com/UserFiles/excel/advertise_campaign_stats.csv");
	}
}

$smarty->assign("stats", $stats);

//_d("stats:");
//_darr($stats);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/stats_campaign.tpl");

?>
