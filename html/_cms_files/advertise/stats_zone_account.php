<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage"))
	die("Invalid access!");

require_once(_CMS_ABS_PATH."/_cms_files/advertise/common.php");

$zone_id = intval($_REQUEST["id"]);
if (!$zone_id) {
	echo "No zone id!";
	return;
}

$res = $db->q("SELECT * FROM advertise_section WHERE id = ?", array($zone_id));
if ($db->numrows($res) != 1) {
	echo "Cant find zone #{$zone_id}!";
	return;
}
$row = $db->r($res);
$type = $row["type"];
$smarty->assign("zone", $row);

//-------------------
//export or display ?
$export = false;
if ($_REQUEST["export"] == 1)
	$export = true;


//-------
//filters
$time_array = get_date_stamp_range(3);	//default period is last 30 days (->3)
if ($time_array === false) {
	echo "Wrong time selection!<br />\n";
	return;
}
//_darr($time_array);

$date_sql = "AND d.date >= '{$time_array["date_start"]}' AND d.date <= '{$time_array["date_end"]}'";
if ($time_array["date_start"] == $time_array["date_end"])
	$date_sql = "AND d.date = '{$time_array["date_start"]}'";

$time_sql = "AND bi.stamp >= {$time_array["stamp_start"]} AND bi.stamp <= {$time_array["stamp_end"]}";


$group = $_REQUEST["group"];
if ($group != "daily" && $group != "monthly" && $group != "yearly" && $group != "alltime")
	$group = "daily";
$smarty->assign("group", $group);

if ($export) {
	$fp = fopen(_CMS_ABS_PATH.'/UserFiles/excel/advertise_zone_account_stats.csv', 'w');
}


//----------
// get stats

if ($type == "B" || $type == "P" || $type == "L" || $zone_id < 10000 || $zone_id == 10058 || $zone_id == 10059) {
	//all zones but old text "external" zones
	$table = "advertise_data";
	$date_col = "date";
	$sid_col = "realsid";

	if ($group == "daily")
		$sql = "SELECT YEAR(af.{$date_col}) as year, MONTH(af.{$date_col}) as month, DAY(af.{$date_col}) as day, ac.account_id, a.email, 
					SUM(af.impression) as impression_count, SUM(af.click) as click_count, SUM(af.click)/SUM(af.impression)*100 as ctr
				FROM {$table} af
				INNER JOIN advertise_campaign ac on ac.id = af.c_id
				INNER JOIN account a on a.account_id = ac.account_id
				WHERE af.{$sid_col} = '{$zone_id}' AND af.{$date_col} >= '{$time_array["date_start"]}' AND af.{$date_col} <= '{$time_array["date_end"]}'
				GROUP BY YEAR(af.{$date_col}), MONTH(af.{$date_col}), DAY(af.{$date_col}), ac.account_id
				ORDER BY 1 DESC, 2 DESC, 3 DESC, 4 ASC";
	else if ($group == "monthly")
		$sql = "SELECT YEAR(af.{$date_col}) as year, MONTH(af.{$date_col}) as month, ac.account_id, a.email, 
					SUM(af.impression) as impression_count, SUM(af.click) as click_count, SUM(af.click)/SUM(af.impression)*100 as ctr
				FROM {$table} af
				INNER JOIN advertise_campaign ac on ac.id = af.c_id
				INNER JOIN account a on a.account_id = ac.account_id
				WHERE af.{$sid_col} = '{$zone_id}' AND af.{$date_col} >= '{$time_array["date_start"]}' AND af.{$date_col} <= '{$time_array["date_end"]}'
				GROUP BY YEAR(af.{$date_col}), MONTH(af.{$date_col}), ac.account_id
				ORDER BY 1 DESC, 2 DESC, 3 ASC";
	else if ($group == "yearly")
		$sql = "SELECT YEAR(af.{$date_col}) as year, ac.account_id, a.email, 
					SUM(af.impression) as impression_count, SUM(af.click) as click_count, SUM(af.click)/SUM(af.impression)*100 as ctr
				FROM {$table} af
				INNER JOIN advertise_campaign ac on ac.id = af.c_id
				INNER JOIN account a on a.account_id = ac.account_id
				WHERE af.{$sid_col} = '{$zone_id}' AND af.{$date_col} >= '{$time_array["date_start"]}' AND af.{$date_col} <= '{$time_array["date_end"]}'
				GROUP BY YEAR(af.{$date_col}), ac.account_id
				ORDER BY 1 DESC, 2 ASC";
	else
		$sql = "SELECT ac.account_id, a.email, 
					SUM(af.impression) as impression_count, SUM(af.click) as click_count, SUM(af.click)/SUM(af.impression)*100 as ctr
				FROM {$table} af
				INNER JOIN advertise_campaign ac on ac.id = af.c_id
				INNER JOIN account a on a.account_id = ac.account_id
				WHERE af.{$sid_col} = '{$zone_id}'
				GROUP BY ac.account_id";
} else {
	//old text "external" campaigns have real stats in different table :-(
	$table = "advertise_fraud";
	$date_col = "time";
	$sid_col = "s_id";

	if ($group == "daily")
		$sql = "SELECT YEAR(af.{$date_col}) as year, MONTH(af.{$date_col}) as month, DAY(af.{$date_col}) as day, a.account_id, a.email, 
					SUM(af.impression) as impression_count, SUM(af.click) as click_count, SUM(af.click)/SUM(af.impression)*100 as ctr
				FROM {$table} af
				INNER JOIN account a on a.account_id = af.account_id
				WHERE af.{$sid_col} = '{$zone_id}' AND af.{$date_col} >= '{$time_array["date_start"]}' AND af.{$date_col} <= '{$time_array["date_end"]}'
				GROUP BY YEAR(af.{$date_col}), MONTH(af.{$date_col}), DAY(af.{$date_col}), a.account_id
				ORDER BY 1 DESC, 2 DESC, 3 DESC, 4 ASC";
	else if ($group == "monthly")
		$sql = "SELECT YEAR(af.{$date_col}) as year, MONTH(af.{$date_col}) as month, a.account_id, a.email, 
					SUM(af.impression) as impression_count, SUM(af.click) as click_count, SUM(af.click)/SUM(af.impression)*100 as ctr
				FROM {$table} af
				INNER JOIN account a on a.account_id = af.account_id
				WHERE af.{$sid_col} = '{$zone_id}' AND af.{$date_col} >= '{$time_array["date_start"]}' AND af.{$date_col} <= '{$time_array["date_end"]}'
				GROUP BY YEAR(af.{$date_col}), MONTH(af.{$date_col}), a.account_id
				ORDER BY 1 DESC, 2 DESC, 3 ASC";
	else if ($group == "yearly")
		$sql = "SELECT YEAR(af.{$date_col}) as year, a.account_id, a.email, 
					SUM(af.impression) as impression_count, SUM(af.click) as click_count, SUM(af.click)/SUM(af.impression)*100 as ctr
				FROM {$table} af
				INNER JOIN account a on a.account_id = af.account_id
				WHERE af.{$sid_col} = '{$zone_id}' AND af.{$date_col} >= '{$time_array["date_start"]}' AND af.{$date_col} <= '{$time_array["date_end"]}'
				GROUP BY YEAR(af.{$date_col}), a.account_id
				ORDER BY 1 DESC, 2 ASC";
	else
		$sql = "SELECT a.account_id, a.email, 
					SUM(af.impression) as impression_count, SUM(af.click) as click_count, SUM(af.click)/SUM(af.impression)*100 as ctr
				FROM {$table} af
				INNER JOIN account a on a.account_id = af.account_id
				WHERE af.{$sid_col} = '{$zone_id}'
				GROUP BY a.account_id";
}

//_d("SQL='{$sql}'");
$res = $db->q($sql);
$nr = $db->numrows($res);

$stats = array();
$accounts = array();
$total_impressions = $total_clicks = array();

while ($row = $db->r($res)) {
	$key = $row["year"]."|".$row["month"]."|".$row["day"];
	if (!array_key_exists($row["account_id"], $accounts))
		$accounts[$row["account_id"]] = $row["email"];
	$acc_stats = array(
		"account_id" => $row["account_id"],
		"email" => $row["email"],
		"impressions" => $row["impression_count"],
		"clicks" => $row["click_count"],
		);
	if (!array_key_exists($key, $stats)) {
		$stats[$key] = array(
			"year" => $row["year"],
			"month" => $row["month"],
			"day" => $row["day"],
			"accounts" => array(
				$row["account_id"] => $acc_stats
				)
			);
	} else {
		$arr = $stats[$key]["accounts"];
		$arr[$row["account_id"]] = $acc_stats;
		$stats[$key]["accounts"] = $arr;
	}
	$total_impressions[$row["account_id"]] = intval($total_impressions[$row["account_id"]]) + $row["impression_count"];
	$total_clicks[$row["account_id"]] = intval($total_clicks[$row["account_id"]]) + $row["click_count"];
}

//_d("stats:");
//_darr($stats);
//_d("total_impressions:");
//_darr($total_impressions);
//_d("total_clicks:");
//_darr($total_clicks);

if ($nr && $export) {
	//header
	$fields = "";
	if ($group == "daily" || $group == "monthly" || $group == "yearly")
		$fields .= "Year,";
	if ($group == "daily" || $group == "monthly")
		$fields .= "Month,";
	if ($group == "daily")
		$fields .= "Day,";
	foreach ($accounts as $account_id => $email) {
		$fields .= "{$email},";
	}
	$fields = substr($fields, 0, -1);
	$fields .= "\n";
	fputs($fp, $fields);

	//rows
	foreach ($stats as $key => $val) {
		$arr = explode("|", $key);
		$fields = "";
		if ($group == "daily" || $group == "monthly" || $group == "yearly")
			$fields .= "{$arr[0]},";
		if ($group == "daily" || $group == "monthly")
			$fields .= "{$arr[1]},";
		if ($group == "daily")
			$fields .= "{$arr[2]},";
		foreach ($accounts as $account_id => $email) {
			if (array_key_exists($account_id, $val["accounts"]))
				$fields .= "{$val["accounts"][$account_id]["impressions"]}";
			$fields .= ",";
		}
		$fields = substr($fields, 0, -1);
		$fields .= "\n";
		fputs($fp, $fields);
	}

	fclose($fp);
	system::moved("http://adultsearch.com/UserFiles/excel/advertise_zone_account_stats.csv");
}


$smarty->assign("accounts", $accounts);
$smarty->assign("stats", $stats);
$smarty->assign("total_impressions", $total_impressions);
$smarty->assign("total_clicks", $total_clicks);
$smarty->display(_CMS_ABS_PATH."/templates/advertise/stats_zone_account.tpl");

?>
