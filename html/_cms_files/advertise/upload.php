<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $sphinx, $gIndexTemplate, $config_image_path;
$gIndexTemplate = "none.tpl";

$system = new system;

$account_id = $account->isloggedin();
if (!$account_id) {
	echo "Error: not logged in!";
	return false;
}

error_reporting(E_ALL | E_STRICT);
require(_CMS_ABS_PATH."/_cms_files/advertise/UploadHandler.php");
class CustomUploadHandler extends UploadHandler {
	protected function trim_file_name($file_path, $name, $size, $type, $error, $index, $content_range) {
		$name = uniqid();
		return $name;
	}
}
$options = array(
	"script_url" => "http://adultsearch.com/advertise/upload",
	"upload_dir" => $config_image_path."promo/",
	"upload_url" => "http://img.adultsearch.com/promo/",
	"image_versions" => array(
		// The empty image version key defines options for the original image:
		"" => array(
			// Automatically rotate images based on EXIF meta data:
			'auto_orient' => true
			),
		"thumbnail" => array(
			// Uncomment the following to use a defined directory for the thumbnails
			// instead of a subdirectory based on the version identifier.
			// Make sure that this directory doesn't allow execution of files if you
			// don't pose any restrictions on the type of uploaded files, e.g. by
			// copying the .htaccess file from the files directory for Apache:
			'upload_dir' => $config_image_path."promo/t/",
			'upload_url' => "http://img.adultsearch.com/promo/t/",
			// Uncomment the following to force the max
			// dimensions and e.g. create square thumbnails:
			'crop' => true,
			'max_width' => 75,
			'max_height' => 75,
			)
		),
	"print_response" => false,
	"correct_image_extensions" => true,
	);
$upload_handler = new CustomUploadHandler($options);

$response = $upload_handler->get_response();

if (!is_array($response) || !array_key_exists("files", $response)) {
	echo "Error 1 !";
	reportAdmin("AS: advertise/upload: Error 1", "Error 1", array("account_id" => $account->getId(), "response" => print_r($response, true)));
	die;
}
$obj = array_shift($response["files"]);
//echo "'".print_r($obj, true)."'";
$filename = $obj->name;
if (!$filename) {
	echo "Error 2 !";
	reportAdmin("AS: advertise/upload: Error 2", "Error 2", array("account_id" => $account->getId(), "response" => print_r($response, true)));
	die;
}
$filepath = $config_image_path."promo/{$filename}";
$t_filepath = $config_image_path."promo/t/{$filename}";
if (!file_exists($filepath)) {
	echo "Error 3 !";
	reportAdmin("AS: advertise/upload: Error 3", "Error 3", array("account_id" => $account->getId(), "response" => print_r($response, true), "filepath" => $filepath));
	die;
}
list($width, $height, $type, $attr) = getimagesize($filepath);


//------
//rename
$new_filename = "{$account_id}_{$filename}";
$new_filepath = $config_image_path."/promo/{$new_filename}";
$new_t_filepath = $config_image_path."/promo/t/{$new_filename}";
$ret1 = rename($filepath, $new_filepath);
$ret2 = true;
if (file_exists($t_filepath))
	$ret2 = rename($t_filepath, $new_t_filepath);
if (!$ret1 || !$ret2) {
	echo "Error 4 !";
	reportAdmin("AS: advertise/upload: Error 4", "Error 4", array("account_id" => $account->getId(), "response" => print_r($response, true), "filepath" => $filepath, "new_filepath" => $new_filepath, "t_filepath" => $t_filepath, "new_t_filepath" => $new_t_filepath, "ret1" => $ret1, "ret2" => $ret2));
	die;
}

//--------------
//insert into db
$uploaded = time();
$db->q("INSERT INTO advertise_image 
		(account_id, filename, width, height, uploaded)
		VALUES
		(?, ?, ?, ?, ?)",
		array($account_id, $new_filename, $width, $height, $uploaded)
		);
$aff = $db->affected($res);
if ($aff != 1) {
	echo "Error 5 !";
	reportAdmin("AS: advertise/upload: Error 5", "Error 5", array("account_id" => $account->getId(), "response" => print_r($response, true), "filepath" => $filepath, "db_error" => $db->error()));
	die;
}
$id = $db->insertid($res);

$resp = array(
	"status" => "ok",
	"id" => $id,
	"filename" => $new_filename,
	"width" => $width,
	"height" => $height,
	"uploaded" => date("Y-m-d H:i:s", $uploaded),
	"url" => "http://img.adultsearch.com/promo/{$new_filename}",
	"thumb_url" => "http://img.adultsearch.com/promo/t/{$new_filename}",
	"delete_url" => "http://adultsearch.com/advertise/image?id={$id}&action=delete",
	);

//reportAdmin("AS: advertise/upload: success", "Advertise image uploaded successfully, please check", $resp);
$sm = GetSmartyInstance();
$sm->assign("id", $id);
$sm->assign("account_id", $account->getId());
$sm->assign("account_email", $account->getEmail());
$sm->assign("width", $width);
$sm->assign("height", $height);
$email = ADMIN_EMAIL;
$html = $sm->fetch(_CMS_ABS_PATH."/templates/advertise/email/upload_admin_notify.html.tpl");
$subject = "AS: New banner image uploaded";
$embedded_images = array("image" => $config_image_path."promo/".$new_filename);
send_email(array(
	"from" => "support@adultsearch.com",
	"to" => $email,
	"subject" => $subject,
	"html" => $html,
	"embedded_images" => $embedded_images,
));

echo json_encode($resp);
return;
?>
