<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";
$smarty->assign("nobanner", true);
$smarty->assign("noshare", true);

if (!permission::access("advertise_manage"))
    die("Invalid access!");

$wireDir = _CMS_ABS_PATH."/../data/wire";

function stream($imageFilepath) {
	$h = fopen($imageFilepath, 'rb');
    header("Content-Type: ".mime_content_type($imageFilepath));
	header("Content-Length: ".filesize($imageFilepath));
	fpassthru($h);
	exit;
}

//fetch wire
$wireId = $_REQUEST["id"];
$res = $db->q("SELECT image, thumbnail, status FROM account_wire WHERE id = ? LIMIT 1", [$wireId]);
if ($db->numrows($res) != 1)
	die("Invalid wire #id!");
$row = $db->r($res);


if ($_REQUEST["action"] == "image") {
	$image = $row["image"];
	$imageFilepath = $wireDir."/".$image;
	if (!file_exists($imageFilepath))
		die("Error: Can't find wire image file for wire #{$wireId} !");
	stream($imageFilepath);
	echo "404 Not found";die;
}

if ($_REQUEST["action"] == "thumbnail") {
	//first try thumbnail
	$thumbnail = $row["thumbnail"];
	if ($thumbnail) {
		$thumbnailFilepath = $wireDir."/".$thumbnail;
		if (file_exists($thumbnailFilepath))
			stream($thumbnailFilepath);
			die("Error: Can't find wire image file for wire #{$wireId} !");
	}
	//then try fullsize image
	$image = $row["image"];
	if ($image) {
		$imageFilepath = $wireDir."/".$image;
		if (!file_exists($imageFilepath))
			die("Error: Can't find wire image file for wire #{$wireId} !");
		stream($imageFilepath);
	}
	echo "404 Not found";die;
}

if ($_REQUEST["action"] == "process") {
	$status = $row["status"];
	if ($status == 1)
		return error_redirect("Wire #{$wireId} is already marked processed.", "/advertise/wires");

	$res = $db->q(
		"UPDATE account_wire SET status = 1, processed_stamp = ?, processed_by = ? WHERE id = ? LIMIT 1", 
		[time(), $account->getId(), $wireId]
		);
	$aff = $db->affected($res);
	if ($aff != 1)
		return error_redirect("Failed to mark wire #{$wireId} as processed.", "/advertise/wires");
	else
		return success_redirect("Wire #{$wireId} marked as processed.", "/advertise/wires");
}

?>
