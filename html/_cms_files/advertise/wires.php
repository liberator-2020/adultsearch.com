<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage"))
	die("Invalid access!");

$wireDir = _CMS_ABS_PATH."/../data/wire";

//filters
$where = "";
$params = [];
$status = null;
if (!isset($_REQUEST["status"]))
	$status = 0;
else
	$status = $_REQUEST["status"];
if (is_numeric($status)) {
	$status = intval($status);
	$where .= " AND aw.status = ?";
	$params[] = $status;
	$smarty->assign("status", $status);
}

//select
$res = $db->q("SELECT aw.*, a.email
				FROM account_wire aw
				INNER JOIN account a on a.account_id = aw.account_id
				WHERE 1 = 1 {$where}
				ORDER BY aw.id ASC
				", $params);
$wires = array();
while ($row = $db->r($res)) {
	$account_id = $row["account_id"];
	$thumbnail = null;
	if ($row["thumbnail"] && file_exists($wireDir."/".$row["thumbnail"]))
		$thumbnail = $row["thumbnail"];
	else if ($row["image"] && file_exists($wireDir."/".$row["image"]))
		$thumbnail = $row["image"];
	$wire = [
		"id" => $row["id"],
		"account_id" => $row["account_id"],
		"email" => $row["email"],
		"name" => $row["name"],
		"amount" => $row["amount"],
		"bank_name" => $row["bank_name"],
		"image" => $row["image"],
		"thumbnail" => $thumbnail,
		"status" => $row["status"],
		"created_stamp" => $row["created_stamp"],
		];
	if ($status === 0) {
		//if showing unprocesed wires, group by account_id
		if (array_key_exists($account_id, $wires)) {
			$aw = $wires[$account_id];
			$aw[] = $wire;
		} else {
			$aw = [$wire];
		}
		$wires[$account_id] = $aw;
	} else {
		$wires[] = [$wire];
	}
}
$smarty->assign("wires", $wires);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/wires.tpl");

?>
