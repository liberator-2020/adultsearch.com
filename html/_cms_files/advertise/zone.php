<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";
$smarty->assign("nobanner", true);
$smarty->assign("noshare", true);

if (!account::ensure_admin())
    die("Invalid access");

//zone groups
$groups = array("" => "No group");
$res = $db->q("SELECT g.* FROM advertise_zone_group g", array());
while ($row = $db->r($res))
	$groups[$row["id"]] = $row["name"];
$smarty->assign("group_options", $groups);

if (isset($_REQUEST["new"])) {
	if (!isset($_REQUEST["submit"])) {
		$smarty->assign("new", true);
		$smarty->assign("zone", array());
		$smarty->display(_CMS_ABS_PATH."/templates/advertise/zone.tpl");
		return;
	}
} else {
	$zone_id = intval($_REQUEST["id"]);
	if (!$zone_id) {
		echo "Zone ID not specified!";
		return;
	}
}

function upload_image($zone_id) {
	require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
	$handle = new upload($_FILES["example_screenshot_file"]);
	if ($handle->uploaded) {
		$handle->file_new_name_body = $zone_id;
		$handle->file_new_name_ext = $handle->image_src_type;
		$handle->process(_CMS_ABS_PATH."/images/advertise/example");
		if ($handle->processed) {
			$handle->clean();
//			_darr($handle);die();
			return $handle->file_dst_name;
		} else {
			echo "Error uploading image : ".$handle->error."<br />\n";
			return false;
		}
	}
}

//submission
if (isset($_REQUEST["submit"])) {
	$type = $_REQUEST["type"];
	$status = intval($_REQUEST["status"]);
	$nickname = $_REQUEST["nickname"];
	$name = $_REQUEST["name"];
	$account_id = intval($_REQUEST["account_id"]);
	$defaultbid = NULL;
	$cpm_minimum = NULL;
	$backuphtml = NULL;
	$group_id = ($_REQUEST["group_id"]) ? $_REQUEST["group_id"] : NULL;
	$volume = $_REQUEST["volume"];
	$exclusive_access = ($_REQUEST["exclusive_access"]) ? $_REQUEST["exclusive_access"] : NULL;
	$notes = ($_REQUEST["notes"]) ? $_REQUEST["notes"] : null;

	if ($_REQUEST["new"]) {
		$currentbid = 0.2;

		if ($type == "T") {
			$defaultbid = number_format(floatval($_REQUEST["defaultbid"]), 3, ".", "");
			$currentbid = $defaultbid;
		} else if ($type == "B") {
			$cpm_minimum = number_format(floatval($_REQUEST["cpm_minimum"]), 2, ".", "");
			$backuphtml = $_REQUEST["backuphtml"];
			$defaultbid = 0.2;
		} else if ($type == "P") {
			$defaultbid = number_format(floatval($_REQUEST["defaultbid"]), 3, ".", "");
			$backuphtml = $_REQUEST["backuphtml"];
			$currentbid = $defaultbid;
		} else if ($type == "A") {
			$defaultbid = NULL;
			$backuphtml = NULL;
			$currentbid = NULL;
		} else if ($type == "L" || $type == "I") {
			$defaultbid = number_format(floatval($_REQUEST["defaultbid"]), 3, ".", "");
			$backuphtml = NULL;
			$currentbid = $defaultbid;
		} else {
			die("Error!");
		}

		$db->q("INSERT INTO advertise_section 
				(type, account_id, group_id, status, popup, nickname, name, defaultbid, currentbid, cpm_minimum, backuphtml, volume, exclusive_access, notes)
				VALUES
				(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
				array($type, $account->getId(), $group_id, $status, ($type == "P") ? 1 : 0, $nickname, $name, $defaultbid, $currentbid, $cpm_minimum, $backuphtml, $volume, $exclusive_access, $notes)
				);
		$zone_id = $db->insertid();
		$error = $db->error();

		if ($error) {
			return error_redirect("Error adding zone !", "/advertise/zones");
		} else {

			//example screenshot upload
			$example_image = upload_image($zone_id);
			if ($example_image) {
				$db->q("UPDATE advertise_section SET example_screenshot = ? WHERE id = ?", array($example_image, $zone_id));
			}
		
			audit::log("ADS", "New", $zone_id, "", $account->getId());
			return success_redirect("Zone #{$zone_id} inserted successfully.", "/advertise/zones");
		}
	} else {

		$update = "type = ?, status = ?, group_id = ?, nickname = ?, name = ?, volume = ?, exclusive_access = ?, notes = ?, popup = ?";
		$params = array($type, $status, $group_id, $nickname, $name, $volume, $exclusive_access, $notes);
		if ($type == "T") {
			$params[] = 0;
			$update .= ", defaultbid = ?";
			$params[] = number_format(floatval($_REQUEST["defaultbid"]), 3, ".", "");
		} else if ($type == "B") {
			$params[] = 0;
			$update .= ", cpm_minimum = ?, backuphtml = ?";
			$params[] = number_format(floatval($_REQUEST["cpm_minimum"]), 2, ".", "");
			$params[] = $_REQUEST["backuphtml"];
		} else if ($type == "P") {
			$params[] = 1;
			$update .= ", defaultbid = ?, backuphtml = ?";
			$params[] = number_format(floatval($_REQUEST["defaultbid"]), 3, ".", "");
			$params[] = $_REQUEST["backuphtml"];
		} else if ($type == "A") {
			$params[] = 0;
			$update .= ", defaultbid = NULL, backuphtml = NULL";
		} else if ($type == "L" || $type == "I") {
			$params[] = 0;
			$update .= ", defaultbid = ?";
			$params[] = number_format(floatval($_REQUEST["defaultbid"]), 3, ".", "");
		} else {
			die("Error!");
		}

		//example screenshot upload
		$example_image = upload_image($zone_id);
//		die("ei={$example_image}");
		if ($example_image) {
			$update .= ", example_screenshot = ?";
			$params[] = $example_image;
		}

		if ($account_id) {
			$update .= ", account_id = ?";
			$params[] = $account_id;
		}

		$params[] = $zone_id;
		$db->q("UPDATE advertise_section SET {$update} WHERE id = ?", $params);

		if ($db->error()) {
			return error_redirect("Error saving zone #{$zone_id} !", "/advertise/zones");
		} else {
			audit::log("ADS", "Edit", $zone_id, "", $account->getId());
			return success_redirect("Zone #{$zone_id} saved successfully.", "/advertise/zones");
		}
	}
}

$res = $db->q("SELECT s.* FROM advertise_section s WHERE s.id = ?", array($zone_id));
if (!$db->numrows($res)) {
	echo "Zone #{$zone_id} not found!";
	return;
}
$row = $db->r($res);
$smarty->assign("zone", $row);

$acc = account::findOneById($row["account_id"]);
if ($acc) {
	$smarty->assign("account", "#{$acc->getId()} - {$acc->getUsername()} - {$acc->getEmail()}");
	$smarty->assign("account_id", $acc->getId());
}

$smarty->display(_CMS_ABS_PATH."/templates/advertise/zone.tpl");

?>
