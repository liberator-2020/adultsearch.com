<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage") && !advertise::isPublisher())
    die("Invalid access!");

$zone_id = intval($_REQUEST["id"]);
if (!$zone_id) {
	echo "Zone ID not specified!";
	return;
}

//---------------------------
//get general info about zone
$zone_owner_sql = "";
if (!permission::has("advertise_manage"))
	$zone_owner_sql = " AND account_id = '".intval($account->getId())."'";
$res = $db->q("SELECT s.* FROM advertise_section s WHERE id = ? {$zone_owner_sql}", array($zone_id));
if ($db->numrows($res) != 1) {
	echo "Zone #{$zone_id} not found!";
	return;
}
$row = $db->r($res);
$smarty->assign("zone", $row);


//-----------------------------------------------
//get info about campaign placements in this zone
$res = $db->q("SELECT cs.id as cs_id, cs.approved, cs.bid, cs.cpm, 
					c.id as c_id, c.name, c.status as c_status, c.account_id,  
					a.email, 
					ab.budget
				FROM advertise_camp_section cs
				INNER JOIN advertise_campaign c on cs.c_id = c.id
				INNER JOIN account a on c.account_id = a.account_id
				LEFT JOIN advertise_budget ab on ab.account_id = c.account_id
				WHERE cs.s_id = ? AND cs.show = 1 AND c.status <> -1",
				array($zone_id)
				);

$paused = $not_approved = $out_of_budget = $live = $total = 0;
$placements = array();
$cs_ids_live = $cs_ids_paused = array();
while ($row = $db->r($res)) {
	$total++;

	$placement = array(
		"cs_id" => $row["cs_id"],
		"c_id" => $row["c_id"],
		"c_name" => $row["name"],
		"cpm" => $row["cpm"],
		"bid" => $row["bid"],
		"account_id" => $row["account_id"],
		"email" => $row["email"],
		"imp_date_first" => "-",
		"imp_date_last" => "-",
		"imp_total" => "-",
		"imp_today_so_far" => "-",
		"imp_yesterday" => "-",
		);

	if ($row["approved"] != 1) {
		$not_approved++;
		$placement["status"] = "not_approved";
		$placements[$row["cs_id"]] = $placement;
		continue;
	}

	$cpm = $row["cpm"];
	$budget = $row["budget"];
	$c_status = $row["c_status"];
	if (($cpm > $budget) && $c_status == 0) {
		$out_of_budget++;
		$placement["status"] = "out_of_budget";
		$placements[$row["cs_id"]] = $placement;
		continue;
	}
	if ($c_status == 0) {
		$paused++;
		$placement["status"] = "paused";
		$placements[$row["cs_id"]] = $placement;
		$cs_ids_paused[] = $row["cs_id"];
		continue;
	}

	$live++;	
	$placement["status"] = "live";
	$placements[$row["cs_id"]] = $placement;
	$cs_ids_live[] = $row["cs_id"];
}

$smarty->assign("total", $total);
$smarty->assign("not_approved", $not_approved);
$smarty->assign("out_of_budget", $out_of_budget);
$smarty->assign("paused", $paused);
$smarty->assign("live", $live);

//----------------
//get global stats
if (!empty($cs_ids_live) || !empty($cs_ids_paused)) {
	$res = $db->q("SELECT ad.s_id as cs_id, MIN(ad.`date`) as date_first, MAX(ad.`date`) as date_last, SUM(ad.impression) as impression
					FROM advertise_data ad
					WHERE ad.s_id IN (".implode(",", array_merge($cs_ids_live, $cs_ids_paused)).")
					GROUP BY ad.s_id",
					array($month_ago_date)
					);
	while ($row = $db->r($res)) {
		$cs_id = $row["cs_id"];
		$date_first = $row["date_first"];
		$date_last = $row["date_last"];
		$impression = $row["impression"];
		$placements[$cs_id]["imp_date_first"] = $date_first;
		$placements[$cs_id]["imp_date_last"] = $date_last;
		$placements[$cs_id]["imp_total"] = number_format($impression);
	}
}

//-------------------
//get last week stats
if (!empty($cs_ids_live)) {
	$yesterday_date = date("Y-m-d", time()-(60*60*24));
	$today_date = date("Y-m-d");
	$today_start = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
	$week_ago_start = $today_start - (6 * 86400);
	$month_ago_start = $today_start - (30 * 86400);
	$week_ago_date = date("Y-m-d", $week_ago_start);
	$month_ago_date = date("Y-m-d", $month_ago_start);
	$day_indexes = array();
	for($i = 0; $i < 31; $i++) {
		//$day_indexes[date("M-d", $week_ago_start+($i * 86400))] = $i;
		$day_indexes[date("M-d", $month_ago_start+($i * 86400))] = $i;
	}
	//_darr($day_indexes);
	$smarty->assign("day_indexes", $day_indexes);
	$smarty->assign("day_legend", array_flip($day_indexes));

	$res = $db->q("SELECT ad.s_id as cs_id, ad.`date`, DATE_FORMAT(ad.`date`,'%b-%d') as day, SUM(ad.impression) as impression, ad.c_id, ac.name
					FROM advertise_data ad
					INNER JOIN advertise_campaign ac on ac.id = ad.c_id
					WHERE ad.`date` >= ? AND ad.s_id IN (".implode(",", array_merge($cs_ids_live, $cs_ids_paused)).")
					GROUP BY ad.s_id, ad.`date`
					",
					array($month_ago_date)
					);
	$impressions = array();
	while ($row = $db->r($res)) {
		$cs_id = $row["cs_id"];
		$date = $row["date"];
		$day = $row["day"];
		$impression = $row["impression"];
		$c_name = "#{$row["c_id"]} - {$row["name"]}";
		if (array_key_exists($cs_id, $impressions)) {
			$a = $impressions[$cs_id]["data"];
			$a[$day] = $impression;
			$impressions[$cs_id]["data"] = $a;
		} else {
			$impressions[$cs_id] = array(
				"cs_id" => $cs_id, 
				"c_name" => $c_name, 
				"data" => array($day => $impression),
				);
		}
		if ($date == $today_date)
			$placements[$cs_id]["imp_today_so_far"] = number_format($impression);
		else if ($date == $yesterday_date)
			$placements[$cs_id]["imp_yesterday"] = number_format($impression);
	}
	//_darr($impressions);
	$smarty->assign("impressions", $impressions);

	$res = $db->q("SELECT bi.cs_id, DATE_FORMAT(FROM_UNIXTIME(bi.stamp),'%b-%d') as day, SUM(bi.cost) as cost, bi.c_id, ac.name
					FROM advertise_banner_impression bi
					INNER JOIN advertise_campaign ac on ac.id = bi.c_id
					WHERE bi.stamp >= ? AND bi.cs_id IN (".implode(",", $cs_ids_live).")
					GROUP BY bi.cs_id, DATE_FORMAT(FROM_UNIXTIME(bi.stamp),'%b-%d')
					",
					array($month_ago_start)
					);
	$costs = array();
	while ($row = $db->r($res)) {
		$cs_id = $row["cs_id"];
		$day = $row["day"];
		$cost = $row["cost"];
		$c_name = "#{$row["c_id"]} - {$row["name"]}";
		if (array_key_exists($cs_id, $costs)) {
			$a = $costs[$cs_id]["data"];
			$a[$day] = $cost;
			$costs[$cs_id]["data"] = $a;
		} else {
			$costs[$cs_id] = array(
				"cs_id" => $cs_id,
				"c_name" => $c_name,
				"data" => array($day => $cost)
				);;
		}
	}
	//_darr($costs);
	$smarty->assign("costs", $costs);
}

$smarty->assign("placements", $placements);

include_html_head("js", "/js/advertise/jquery.flot.js");
//include_html_head("js", "/js/advertise/jquery.flot.pie.js");

$smarty->display(_CMS_ABS_PATH."/templates/advertise/zone_detail.tpl");

?>
