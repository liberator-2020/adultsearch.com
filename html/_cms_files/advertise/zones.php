<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $sphinx, $gIndexTemplate;
$gIndexTemplate = "advertise_index.tpl";

if (!permission::access("advertise_manage") && !advertise::isPublisher())
	die("Invalid access!");


//get sphinx advertise data
$sphinx->reset();
$sphinx->SetLimits(0, 1000, 1000);
$sphinx->SetSortMode(SPH_SORT_ATTR_DESC, "cpm");
$results = $sphinx->Query("", "advertise");
$total = $results["total_found"];
$result = $results["matches"];
$zones_campaigns = array();
foreach ($results["matches"] as $match) {
	$zone_id = $match["attrs"]["s_id"];
	$type = $match["attrs"]["type"];
	$campaign_id = $match["attrs"]["c_id"];
	$bid = $match["attrs"]["bid"];
	$cpm =  $match["attrs"]["cpm"];

	if ($type == "B")
		$b = $cpm;
	else
		$b = $bid;

	if (!array_key_exists($zone_id, $zones_campaigns)) {
		$zones_campaigns[$zone_id] = array($campaign_id => $b);
	} else {
		$zone_arr = $zones_campaigns[$zone_id];
		$zone_arr[$campaign_id] = $b;
		$zones_campaigns[$zone_id] = $zone_arr;
	}
}


//---------------------
//get section

//filters
$where = "";
$params = [];
$zone_group_id = $zone_type = NULL;
if ($_REQUEST["zone_type"]) {
    $zone_type = $_REQUEST["zone_type"];
    $where .= " AND ads.type = ?";
    $params[] = $zone_type;
    $smarty->assign("zone_type", $zone_type);
}
if ($_REQUEST["zone_group_id"]) {
    $zone_group_id = intval($_REQUEST["zone_group_id"]);
    $where .= " AND ads.group_id = ?";
    $params[] = $zone_group_id;
    $smarty->assign("zone_group_id", $zone_group_id);
}
if ($_REQUEST["desktop_mobile"]) {
    $desktop_mobile = $_REQUEST["desktop_mobile"];
	if ($desktop_mobile == "D")
	    $where .= " AND ads.mobile = 0";
	else if ($desktop_mobile == "M")
        $where .= " AND ads.mobile = 1";
    $smarty->assign("desktop_mobile", $desktop_mobile);
}

if (!permission::access("advertise_manage"))
	$where .= " AND ads.account_id = '".intval($account->getId())."'";

$res = $db->q("SELECT ads.id, ads.group_id, ads.type, ads.name, ads.defaultbid, ads.cpm_minimum, ads.mobile, ads.volume
				FROM advertise_section ads
				LEFT JOIN advertise_zone_group g on g.id = ads.group_id
				WHERE ads.status >= 0 {$where}
				ORDER BY g.orden ASC, ads.type ASC, ads.id ASC
				", $params);
$zones = array();
while ($row = $db->r($res)) {
	$zone_id = $row["id"];
	$zones[] = array(
		"id" => $zone_id,
		"group_id" => $row["group_id"],
		"mobile" => $row["mobile"],
		"type" => $row["type"],
		"name" => $row["name"],
		"volume" => $row["volume"],
		"cpm_minimum" => ($row["type"] == "B") ? $row["cpm_minimum"] : "",
		"defaultbid" => ($row["type"] != "B") ? $row["defaultbid"] : "",
		"campaigns_advertising" => (array_key_exists($zone_id, $zones_campaigns)) ? count($zones_campaigns[$zone_id]) : 0,
		);
}
$smarty->assign("zones", $zones);

//zone groups
$zone_groups = array();
$res = $db->q("SELECT * FROM advertise_zone_group");
while ($row = $db->r($res)) {
        $zone_groups[$row["id"]] = $row["name"];
}
$smarty->assign("zone_groups", $zone_groups);

$smarty->display(_CMS_ABS_PATH."/templates/advertise/zones.tpl");
?>

