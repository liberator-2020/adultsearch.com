<?php
defined("_CMS_FRONTEND") or die("no access");

global $smarty;

$smarty->assign("main_groups", location::getHomeCities(true));
$smarty->assign("nobanner", true);
$smarty->assign("all_locations", true);
$smarty->display(_CMS_ABS_PATH."/templates/landing.tpl");
die;

?>
