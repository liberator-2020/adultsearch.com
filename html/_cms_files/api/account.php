<?php

defined("_CMS_FRONTEND") or die("no access");
require_once(_CMS_ABS_PATH.'/_cms_files/api/common.php');

/** @var \db $db */
global $db, $config_api_key;

header('Content-Type: application/json');

if ($config_api_key !== $_REQUEST['apikey']) {
	http_response_code(404);
	json_output('');
}

if ($phones = $_POST['phones']) {
	$params = implode(',', array_fill(0, count($phones), '?'));
	$rows   = $db->q("SELECT A.account_id, A.phone FROM account A WHERE A.phone IN ({$params}) GROUP BY A.phone", $phones);

	$result   = [];
	$protocol = $_SERVER['HTTPS'] ? 'https' : 'http';
	$viewUrl  = $protocol.'://'.$_SERVER['HTTP_HOST'].'/mng/accounts?account_id=';

	while ($row = $db->r($rows)) {
		$result[] = [
			'phone' => $row['phone'],
			'link'  => $viewUrl.$row['account_id'],
		];
	}

	json_output($result);
}

http_response_code(404);
json_output('');
