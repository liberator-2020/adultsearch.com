<?php
defined("_CMS_FRONTEND") or die("no access");

global $db, $test_mode, $allowed_clients;

$test_mode = false;

$allowed_clients = array(
	"eccie" => array(
		"password" => "KrotagJa",
		"ips" => array("ip1", "ip2"),
		),
	"bestgfe" => array(
		"password" => "pebrawiswa",
		"ips" => array("195.246.99.189"),
		),
	"tna" => array(
        "password" => "Lopov3to",
        "ips" => array("77.245.59.228", "173.239.54.100", "173.239.54.101", "173.239.54.102"),
        ),
	);

function request_log($str) {
	file_log("api_test", " [".account::getUserIp()."] ".replaceIpInStr($str));
}

function auth_check() {
	global $ctx, $test_mode, $allowed_clients;

	if ($test_mode) {
		request_log("Test mode");
		return true;
	}

	if ($_SERVER['REQUEST_METHOD'] != 'POST') {
		request_log("Error: not POST");
		json_output(array("status" => "error", "message" => "Bad Request; Use POST method"));
	}

	$client = preg_replace('/[^a-z0-9A-Z]/', '', $_REQUEST["client"]);
	if (empty($client)) {
		request_log("Error: no client specified");
		json_output(array("status" => "error", "message" => "Authentication fail"));
	}

	if (!array_key_exists($client, $allowed_clients)) {
		request_log("Error: client '{$client} not in the list");
		json_output(array("status" => "error", "message" => "Authentication fail"));
	}

	$client = $allowed_clients[$client];
	if (!$client) {
		request_log("Error: cant get client '{$client} from the list");
		json_output(array("status" => "error", "message" => "Authentication fail"));
	}

	$password = $_REQUEST["password"];
	if ($client["password"] != $password) {
		request_log("Error: Wrong password '{$password}' for client '{$client}");
		json_output(array("status" => "error", "message" => "Authentication fail"));
	}

	$ip = account::getUserIp();
	if (!in_array($ip, $client["ips"])) {
		request_log("Error: Wrong IP  '{$ip}' for client '{$client}");
		json_output(array("status" => "error", "message" => "Authentication fail"));
	}

	request_log("Auth ok");
	return true;
}

function json_output($out) {
	header('Content-Type: application/json');
	echo json_encode($out);
	die;
}

request_log("New request: method=".$_SERVER['REQUEST_METHOD'].", referer=".$_SERVER["HTTP_REFERER"].", params=".print_r($_REQUEST, true));

//auth check
auth_check();

//parameters check
$phone = "";
$email = "";

if ($_REQUEST["phone"] != "") {
	$phone = preg_replace('/[^0-9]/', '', $_REQUEST["phone"]);
	if ($phone == "") {
		request_log("Error: invalid phone '".$_REQUEST["phone"]."'");
		json_output(array("status" => "error", "message" => "Bad Request; invalid phone field: '".$_REQUEST["phone"]."'"));
	}
}

if ($_REQUEST["email"] != "") {
	$email = $_REQUEST["email"];
	if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
		request_log("Error: invalid email '".$_REQUEST["email"]."'");
		json_output(array("status" => "error", "message" => "Bad Request; invalid email parameter: '".$_REQUEST["email"]."'"));
	}
}

if (empty($phone) && empty($email)) {
	request_log("Error: missing parameter");
	json_output(array("status" => "error", "message" => "Bad Request; missing parameter"));
}


//search for classified
$where = "";
$params = array();
if (!empty($phone) && empty($email)) {
	//$where = "AND c.phone = ?";
	$where = "AND REPLACE(REPLACE(REPLACE(c.phone,'+',''),'-',''),' ','') = ?";
	$params[] = $phone;
} else if (empty($phone) && !empty($email)) {
	$where = "AND c.email = ?";
	$params[] = $email;
} else {
	//$where = "AND (c.phone = ? OR c.email = ?)";
	$where = "AND (REPLACE(REPLACE(REPLACE(c.phone,'+',''),'-',''),' ','') = ? OR c.email = ?)";
	$params[] = $phone;
	$params[] = $email;
}
$res = $db->q("SELECT c.* 
				FROM classifieds c 
				WHERE c.deleted IS NULL AND c.done > -1 {$where}
				ORDER BY c.type ASC, c.`date` DESC
				LIMIT 1",
				$params);
if ($db->numrows($res) == 0) {
	request_log("not found");
	json_output(array("status" => "notfound"));
}
$row = $db->r($res);

$clad = clad::withRow($row);
if (!$clad) {
	request_log("Error: while fetching clad, id='".$row["id"]."'");
	json_output(array("status" => "error", "message" => "Error while fetching classified ad"));
}

//getting URL
$url = $clad->getUrl();
if (!$url) {
	request_log("Error: while constructing URL for clad, id='".$clad->getId()."'");
	json_output(array("status" => "error", "message" => "Error constructing url for ad"));
}

request_log("Found: url={$url}");
json_output(array("status" => "found", "url" => $url));

?>
