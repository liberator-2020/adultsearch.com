<?php
global $debug_mode, $noauth_mode, $allowed_clients;

$debug_mode = false;
$noauth_mode = false;

$allowed_clients = array(
	"eccie" => array(
		"password" => "KrotagJa",
		"ips" => array("77.245.51.4", "77.245.52.4"),
		"ip_subnets" => array("74.206.244"),
		),
//	"bestgfe" => array(
//		"password" => "pebrawiswa",
//		"ips" => array("37.58.122.78", "37.58.79.54"),
//		),
//	"tna" => array(
//		"password" => "Lopov3to",
//		"ips" => array("5.153.51.82", "37.58.122.75", "37.58.79.53"),
//		),
	"ts" => array(
		"password" => "Jegestr0zk4",
		"ips" => array("159.8.36.179"),
		),
	"hotleads" => array(
		"password" => "Furt4Hovn4",
		"ips" => array("174.132.122.122", "174.132.122.123"),
		),
	);

function request_log($str) {
	file_log("api", "[".account::getUserIp()."] ".replaceIpInStr($str));
}

function auth_check() {
	global $ctx, $noauth_mode, $debug_mode, $allowed_clients;

	if ($noauth_mode) {
		request_log("NoAuth mode");
		return true;
	}

	if ($_SERVER['REQUEST_METHOD'] != 'POST') {
		request_log("Error: not POST");
		json_output(array("status" => "error", "message" => "Bad Request; Use POST method"));
	}

	$client = preg_replace('/[^a-z0-9A-Z]/', '', $_REQUEST["client"]);
	if (empty($client)) {
		request_log("Error: no client specified");
		json_output(array("status" => "error", "message" => "Authentication fail"));
	}

	if (!array_key_exists($client, $allowed_clients)) {
		request_log("Error: client '{$client} not in the list");
		json_output(array("status" => "error", "message" => "Authentication fail"));
	}

	$client_name = $client;
	$client = $allowed_clients[$client_name];
	if (!$client) {
		request_log("Error: cant get client '{$client_name}' from the list");
		json_output(array("status" => "error", "message" => "Authentication fail"));
	}

	$password = $_REQUEST["password"];
	if ($client["password"] != $password) {
		request_log("Error: Wrong password '{$password}' for client '{$client_name}'");
		json_output(array("status" => "error", "message" => "Authentication fail"));
	}

	$ip = account::getUserIp();
	if (!in_array($ip, $client["ips"])) {
		$ip_subnet_matches = false;
		if (array_key_exists("ip_subnets", $client)) {
			foreach ($client["ip_subnets"] as $ip_partial) {
				if (substr($ip, 0, strlen($ip_partial)) == $ip_partial) {
					$ip_subnet_matches = true;
					break;
				}
			}
		}
		if (!$ip_subnet_matches) {
			request_log("Error: Wrong IP  '{$ip}' for client '{$client_name}'");
			json_output(array("status" => "error", "message" => "Authentication fail"));
		}
	}

	if ($debug_mode)
		request_log("Auth ok");

	return true;
}

function json_output($out) {
	header('Content-Type: application/json');
	echo json_encode($out);
	die;
}

?>
