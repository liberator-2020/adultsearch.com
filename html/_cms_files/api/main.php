<?php
defined("_CMS_FRONTEND") or die("no access");

require_once(_CMS_ABS_PATH."/_cms_files/api/common.php");
ini_set('max_execution_time', 0);

global $debug_mode, $noauth_mode;

$noauth_mode = false;

if ($debug_mode)
	request_log("New request: method=".$_SERVER['REQUEST_METHOD'].", referer=".$_SERVER["HTTP_REFERER"].", params=".print_r($_REQUEST, true));

//auth check
auth_check();

//parameters check
$params = array();
if ($_REQUEST["params"] == "") {
	request_log("Error: empty params !");
	json_output(array("status" => "error", "message" => "Bad Request; empty params"));
	return;
}
$params = json_decode($_REQUEST["params"], true);
if (!$params) {
	request_log("Error: params can't be decoded as JSON");
	json_output(array("status" => "error", "message" => "Bad Request; params can't be decoded as JSON"));
	return;
}
if (!array_key_exists("action", $params)) {
	request_log("Error: No action param in params");
	json_output(array("status" => "error", "message" => "Bad Request; No action param in params"));
	return;
}
$action = preg_replace('/[^0-9a-zA-Z_\-]/', '', $params["action"]);
if ($action == "") {
	request_log("Error: empty action");
	json_output(array("status" => "error", "message" => "Bad Request; empty action"));
}



function decode_by_reverse_array($name, $val, $orig_array) {
	if (!is_array($orig_array)) {
		request_log("Error: error in original array for '{$name}' !");
		json_output(array("status" => "error", "message" => "Internal error; invalid original array for '{$name}' "));
		die;
	}
	$flipped_array = array_flip($orig_array);
	if (!$flipped_array) {
		request_log("Error: error in flip array for '{$name}' !");
		json_output(array("status" => "error", "message" => "Internal error; flip array for '{$name}' failed"));
		die;
	}
	if (!array_key_exists($val, $flipped_array)) {
		request_log("Error: Value '{$val}' not found in array for param '{$name}' !");
		json_output(array("status" => "error", "message" => "Bad request; Value '{$val}' not found in array for param '{$name}'"));
		die;
	}
	return $flipped_array[$val];
}

function decode_location($num, $loc_arr) {
	global $db;

	if (!array_key_exists("city", $loc_arr) || $loc_arr["city"] == "") {
		request_log("Error: City not specified in location #{$num} !");
		json_output(array("status" => "error", "message" => "Bad request; City not specified in location #{$num} !"));
		die;
	}
	$city = $loc_arr["city"];
	if (array_key_exists("state", $loc_arr)) {
		$state = $loc_arr["state"];
	}
	if (!array_key_exists("country", $loc_arr) || $loc_arr["country"] == "") {
		request_log("Error: Country not specified in location #{$num} !");
		json_output(array("status" => "error", "message" => "Bad request; Country not specified in location #{$num} !"));
		die;
	}
	$country = $loc_arr["country"];

	if ($state) {
		$res = $db->q("
			SELECT city.loc_id, state.loc_id as state_id, city.loc_name
			FROM location_location city
			INNER JOIN location_location state ON state.loc_type = 2 AND state.loc_id = city.loc_parent AND state.loc_name = ?
			INNER JOIN location_location country ON country.loc_type = 1 AND country.loc_id = state.loc_parent AND country.loc_name = ?
			WHERE city.loc_type = 3 AND city.loc_name = ?",
			array($state, $country, $city)
			);
	} else {
		$res = $db->q("
			SELECT city.loc_id, country.loc_id as state_id, city.loc_name
			FROM location_location city
			INNER JOIN location_location country ON country.loc_type = 1 AND country.loc_id = city.loc_parent AND country.loc_name = ?
			WHERE city.loc_type = 3 AND city.loc_name = ?",
			array($country, $city)
			);
	}
	if ($db->numrows($res) != 1) {
		request_log("Error: Could not find location #{$i} - city:'{$city}', state: '{$state}', country: '{$country}'");
		json_output(array("status" => "error", "message" => "Bad Request; Could not find location #{$i} - city:'{$city}', state: '{$state}', country: '{$country}'"));
		return;
	}
	$row = $db->r($res);
	return array("loc_id" => $row["loc_id"], "state_id" => $row["state_id"], "loc_name" => $row["loc_name"]);
}

function decode_boolean($name, $val) {
	if ($val === "") {
		request_log("Error: Param '{$name}' has empty value !");
		json_output(array("status" => "error", "message" => "Bad request; Param '{$name}' has empty value !"));
		die;
	} else if ($val == "1") {
		return 1;
	} else if ($val == "0") {
		return 0;
	} else {
		request_log("Error: Param '{$name}' has invalid value: '{$val}' !");
		json_output(array("status" => "error", "message" => "Bad request; Param '{$name}' has invalid value: '{$val}' !"));
		die;
	}
}

function decode_int($name, $val) {
	if ($val == "") {
		request_log("Error: Param '{$name}' has empty value !");
		json_output(array("status" => "error", "message" => "Bad request; Param '{$name}' has empty value !"));
		die;
	}
	return intval($val);
}

function decode_string($name, $val) {
	if ($val == "") {
		request_log("Error: Param '{$name}' has empty value !");
		json_output(array("status" => "error", "message" => "Bad request; Param '{$name}' has empty value !"));
		die;
	}
	return $val;
}

function decode_date($name, $val) {
	if ($val == "") {
		request_log("Error: Param '{$name}' has empty value !");
		json_output(array("status" => "error", "message" => "Bad request; Param '{$name}' has empty value !"));
		die;
	}
	if (!preg_match("/^\([0-9]{4}\)-\((0[1-9]|1[0-2])\)-\((0[1-9]|[1-2][0-9]|3[0-1])\)$/", $val)) {
		request_log("Error: Param '{$name}' has invalid value: '{$val}' !");
		json_output(array("status" => "error", "message" => "Bad request; Param '{$name}' has invalid value: '{$val}' !"));
		die;
	}
	return $val;
}

function optional_int($params, $name, &$attr_names, &$attr_values) {
	if (!array_key_exists($name, $params))
		return;
	$val = decode_int($name, $params[$name]);
	$attr_names[] = $name;
	$attr_values[] = $val;
}

function optional_boolean($params, $name, &$attr_names, &$attr_values, $db_name = NULL) {
	if ($db_name == NULL)
		$db_name = $name;
	if (!array_key_exists($name, $params))
		return;
	$val = decode_boolean($name, $params[$name]);
	$attr_names[] = $db_name;
	$attr_values[] = $val;
}

function optional_date($params, $name, &$attr_names, &$attr_values) {
	if (!array_key_exists($name, $params))
		return;
	$val = decode_date($name, $params[$name]);
	$attr_names[] = $name;
	$attr_values[] = $val;
}

function optional_string($params, $name, &$attr_names, &$attr_values) {
	if (!array_key_exists($name, $params))
		return;
	$val = decode_string($name, $params[$name]);
	$attr_names[] = $name;
	$attr_values[] = $val;
}

function optional_reverse($params, $name, &$attr_names, &$attr_values, $orig_arr, $db_name = NULL) {
	if ($db_name == NULL)
		$db_name = $name;
	if (!array_key_exists($name, $params))
		return;
	$val = decode_by_reverse_array($name, $params[$name], $orig_arr);
	$attr_names[] = $db_name;
	$attr_values[] = $val;
}

function optional_reverse_multiple($params, $name, &$attr_names, &$attr_values, $orig_arr) {
	if (!array_key_exists($name, $params))
		return;
	$vals = explode(",", $params[$name]);
	$decoded = "";
	foreach($vals as $val) {
		if (!empty($decoded))
			$decoded .= ",";
		$decoded .= decode_by_reverse_array($name, $val, $orig_arr);
	}
	$attr_names[] = $name;
	$attr_values[] = $decoded;
}

function create_thumbnail($source_path, $target_path) {
	list($w, $h, $t, $a) = getimagesize($source_path);

	$src_dim = $w;
	if ($w > $h)
		$src_dim = $h;

	$source = NULL;
	switch ($t) {
		case IMAGETYPE_GIF:
			$source = @imagecreatefromgif($source_path);
			break;
		case IMAGETYPE_JPEG:
			$source = @imagecreatefromjpeg($source_path);
			break;
		case IMAGETYPE_PNG:
			$source = @imagecreatefrompng($source_path);
			break;
		default:
			request_log("Error: Unsupported type of image: {$t} !");
			json_output(array("status" => "error", "message" => "Bad request; Unsupported type of image: {$t} !"));
			die;
	}

	if ($source === false) {
		reportAdmin("AS: api/main: create_thumbnail() Error", "GD - imagecreatefrom... failed.", 
					array("source_path" => $source_path, "target_path" => $target_path, "w" => $w, "h" => $h, "t" => $t));
		request_log("Error: Failed to resize image to thumbnail !");
		json_output(array("status" => "error", "message" => "Internal error; Failed to resize image to thumbnail !"));
		die;
	}

	$target = ImageCreateTrueColor(75, 75);
	$ret = imagecopyresampled($target, $source, 0, 0, 0, 0, 75, 75, $src_dim, $src_dim);
	if (!$ret) {
		request_log("Error: Failed to resize image to thumbnail !");
		json_output(array("status" => "error", "message" => "Internal error; Failed to resize image to thumbnail !"));
		die;
	}
	$ret = imagejpeg($target, $target_path);
	if (!$ret) {
		request_log("Error: Failed to save resizes image to thumbnail !");
		json_output(array("status" => "error", "message" => "Internal error; Failed to save resized image to thumbnail !"));
		die;
	}
	@imagedestroy($source);
	@imagedestroy($target);
	return true;
}

function api_clad_create($params) {
	global $db, $config_image_path;

	$mandatory_params = array("category", "locations", "available_to", "incall", "outcall", "age", "firstname", "title", "content");
	foreach ($mandatory_params as $mp) {
		if (!array_key_exists($mp, $params) || $params[$mp] === "") {
			request_log("Error: Mandatory param '{$mp}' not in passed params");
			json_output(array("status" => "error", "message" => "Bad Request; mandatory param '{$mp}' not in passed params"));
			return;
		}
	}

	$classifieds = new classifieds();

	$type = decode_by_reverse_array("category", $params["category"], $classifieds->adbuild_type_array);

	$locations = $params["locations"];
	if (!is_array($params["locations"])) {
		request_log("Error: Mandatory param locations is empty array");
		json_output(array("status" => "error", "message" => "Bad Request; mandatory param locations is empty array"));
		return;
	}
	$locations = array();
	$i = 1;
	foreach ($params["locations"] as $loc) {
		$locations[] = decode_location($i, $loc);
		$i++;
	}

	$avl_to = explode(",", $params["available_to"]);
	$avl_men = $avl_women = $avl_couple = 0;
	foreach ($avl_to as $val) {
		if ($val == "Men")
			$avl_men = 1;
		else if ($val == "Women")
			$avl_women = 1;
		else if ($val == "Couple")
			$avl_couple = 1;
		else {
			request_log("Error: Invalid value '{$val}' for param 'available_to'");
			json_output(array("status" => "error", "message" => "Bad Request; Invalid value '{$val}' for param 'available_to'"));
			return;
		}
	}

	$incall = decode_boolean("incall", $params["incall"]);
	$outcall = decode_boolean("outcall", $params["outcall"]);

	$age = intval($params["age"]);
	if (!$age)
		$age = null;

	$firstname = decode_string("firstname", $params["firstname"]);
	$title = decode_string("title", $params["title"]);
	$content = decode_string("content", $params["content"]);

	$email = null;
	if (array_key_exists("email", $params) && $params["email"]) {
		$email = decode_string("email", $params["email"]);
		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			request_log("Error: Invalid format of param 'email' : '{$email}'");
			json_output(array("status" => "error", "message" => "Bad Request; Invalid format of param 'email' : '{$email}'"));
			return;
		}
	}

	$now = time();
	$done = 1;
	$expire_stamp = $expires = null;
	if (array_key_exists("expire_stamp", $params) && intval($params["expire_stamp"])) {
		$expire_stamp = intval($params["expire_stamp"]);
	} else if(array_key_exists("expires", $params) && intval($params["expires"])) {
		$expire_stamp = intval($params["expires"]);
	} else {
		$expire_stamp = $now + 86400*30;
	}
	if ($expire_stamp < $now)
		$done = 0;
	$expires = date("Y-m-d H:i:s", $expire_stamp);

	$res = $db->q("INSERT INTO classifieds
					(state_id, type, loc_id, loc_name, account_id, done, 
					expires, expire_stamp, email, title, 
					date, content, incall, outcall, age, total_loc, firstname, 
					avl_men, avl_women, avl_couple, created, api)
					VALUES
					(?, ?, ?, ?, ?, ?, 
					?, ?, ?, ?, 
					?, ?, ?, ?, ?, ?, ?, 
					?, ?, ?, ?, ?)",
					[$locations[0]["state_id"], $type, $locations[0]["loc_id"], $locations[0]["loc_name"], 0, -1, 
					$expires, $expire_stamp, $email, $title, 
					date("Y-m-d H:i:s", $now), $content, $incall, $outcall, $age, count($locations), $firstname, 
					$avl_men, $avl_women, $avl_couple, $now, 1]
					);
	$clad_id = $db->insertid();
	if (!$clad_id) {
		request_log("Error: Error wile inserting clad'");
		json_output(array("status" => "error", "message" => "Internal error; Error while inserting clad"));
		return;
	}

	foreach($locations as $loc) {
		$res = $db->q("INSERT INTO classifieds_loc 
						(state_id, loc_id, type, post_id, updated, done)
						VALUES
						(?, ?, ?, ?, ?, ?)",
						array($loc["state_id"], $loc["loc_id"], $type, $clad_id, date("Y-m-d H:i:s", $now), -1)
						);
		$id = $db->insertid();
		if (!$id) {
			request_log("Error: Error wile inserting clad location");
			json_output(array("status" => "error", "message" => "Internal error; Error while inserting clad location: '".$loc["state_id"]."', '".$loc["loc_id"]."', '".$type."', '".$clad_id."', '".date("Y-m-d H:i:s", $now)."', '-1'"));
			return;
		}
	}

	//update with optional params
	$attr_names = array();
	$attr_values = array();
	optional_int($params, "incall_rate_hh", $attr_names, $attr_values);
	optional_int($params, "incall_rate_h", $attr_names, $attr_values);
	optional_int($params, "incall_rate_2h", $attr_names, $attr_values);
	optional_int($params, "incall_rate_day", $attr_names, $attr_values);
	optional_int($params, "outcall_rate_hh", $attr_names, $attr_values);
	optional_int($params, "outcall_rate_h", $attr_names, $attr_values);
	optional_int($params, "outcall_rate_2h", $attr_names, $attr_values);
	optional_int($params, "outcall_rate_day", $attr_names, $attr_values);
	optional_boolean($params, "visiting", $attr_names, $attr_values);
	optional_date($params, "visiting_from", $attr_names, $attr_values);
	optional_date($params, "visiting_to", $attr_names, $attr_values);
	optional_string($params, "location", $attr_names, $attr_values);
	optional_boolean($params, "gfe", $attr_names, $attr_values);
	optional_boolean($params, "fetish_dominant", $attr_names, $attr_values);
	optional_boolean($params, "fetish_submissive", $attr_names, $attr_values);
	optional_boolean($params, "fetich_switch", $attr_names, $attr_values);
	optional_reverse($params, "ethnicity", $attr_names, $attr_values, $classifieds->adbuild_ethnicity);
	optional_reverse_multiple($params, "language", $attr_names, $attr_values, $classifieds->adbuild_language);
	optional_int($params, "height_feet", $attr_names, $attr_values);
	optional_int($params, "height_inches", $attr_names, $attr_values);
	optional_int($params, "weight", $attr_names, $attr_values);
	optional_reverse($params, "eye_color", $attr_names, $attr_values, $classifieds->adbuild_eyecolor, "eyecolor");
	optional_reverse($params, "hair_color", $attr_names, $attr_values, $classifieds->adbuild_haircolor, "haircolor");
	optional_reverse($params, "build", $attr_names, $attr_values, $classifieds->adbuild_build, "build");
	optional_int($params, "measure_1", $attr_names, $attr_values);
	optional_int($params, "measure_2", $attr_names, $attr_values);
	optional_int($params, "measure_3", $attr_names, $attr_values);
	optional_reverse($params, "cupsize", $attr_names, $attr_values, $classifieds->adbuild_cupsize);
	optional_reverse($params, "penis_size", $attr_names, $attr_values, $classifieds->adbuild_penis_size);
	optional_reverse($params, "kitty", $attr_names, $attr_values, $classifieds->adbuild_kitty);
	optional_boolean($params, "pornstar", $attr_names, $attr_values);
	optional_boolean($params, "pregnant", $attr_names, $attr_values);
	optional_string($params, "website", $attr_names, $attr_values);
	optional_string($params, "twitter", $attr_names, $attr_values);
	optional_string($params, "facebook", $attr_names, $attr_values);
	optional_string($params, "instagram", $attr_names, $attr_values);
	optional_boolean($params, "payment_visa_mastercard", $attr_names, $attr_values, "payment_visa");
	optional_boolean($params, "payment_amex", $attr_names, $attr_values);
	optional_boolean($params, "payment_discover", $attr_names, $attr_values, "payment_dis");
	optional_int($params, "ter", $attr_names, $attr_values);

	//optional_string($params, "phone", $attr_names, $attr_values);
	$phone = null;
	if (array_key_exists("phone", $params)) {
		$phone = preg_replace('/[^0-9\+]/', '', $params["phone"]);
		if (substr($phone, 0, 2) == "+1" && strlen($phone) == 12)
			$phone = substr($phone, 2);
	}
	if ($phone) {
		$attr_names[] = "phone";
		$attr_values[] = $phone;
	}

	//thumbnail & photos
	$tmp_dir = _CMS_ABS_PATH."/tmp/api";
	$thumbnail = "";
	if (array_key_exists("thumbnail_href", $params)) {
		$thumb_url = $params["thumbnail_href"];
		if ($thumb_url == "") {
			request_log("Error: Thumbnail url is empty !");
			json_output(array("status" => "error", "message" => "Bad request; Thumbnail url is empty !"));
			return;
		}
		if (filter_var($thumb_url, FILTER_VALIDATE_URL) === false) {
			request_log("Error: Thumbnail url is invalid: '{$thumb_url}' !");
			json_output(array("status" => "error", "message" => "Bad request; Thumbnail url is invalid: '{$thumb_url}' !"));
			return;
		}
		$thumbnail = $tmp_dir."/clad_create.{$clad_id}.{$now}.t.jpg";
		$ret = file_put_contents($thumbnail, file_get_contents($thumb_url));
		if (!$ret) {
			request_log("Error: Can't download thumbnail from URL '{$thumb_url}' !");
			json_output(array("status" => "error", "message" => "Bad request; Can't download thumbnail from URL '{$thumb_url}' !"));
			return;
		}
	}
	$images = array();
	if (array_key_exists("photos", $params)) {
		$photos = $params["photos"];
		if (!is_array($photos)) {
			request_log("Error: Photos param is not array !");
			json_output(array("status" => "error", "message" => "Bad request; Photos param is not array !"));
			return;
		}
		$i = 1;
		foreach ($photos as $photo) {
			if (!array_key_exists("href", $photo)) {
				request_log("Error: Photo #{$i} does not have mandatory param 'href' !");
				json_output(array("status" => "error", "message" => "Bad request; Photo #{$i} does not have mandatory param 'href' !"));
				return;
			}
			$href = $photo["href"];
			if ($href == "") {
				request_log("Error: Photo #{$i} has empty 'href' param !");
				json_output(array("status" => "error", "message" => "Bad request; Photo #{$i} has empty 'href' param !"));
				return;
			}
			if (filter_var($href, FILTER_VALIDATE_URL) === false) {
				request_log("Error: Photo #{$i} has invalid 'href' param: '{$href}' !");
				json_output(array("status" => "error", "message" => "Bad request; Photo #{$i} has invalid 'href' param: '{$href}' !"));
				return;
			}
			$image_tmp_filepath = $tmp_dir."/clad_create.{$clad_id}.{$now}.i{$i}.jpg";
			$ret = file_put_contents($image_tmp_filepath, file_get_contents($href));
			if (!$ret) {
				request_log("Error: Can't download photo from URL '{$href}' !");
				json_output(array("status" => "error", "message" => "Bad request; Can't download photo from URL '{$href}' !"));
				return;
			}
			$images[] = $image_tmp_filepath;
			$i++;
		}
	}

	//check size of thumbnail and recreate it if wrong dimensions or if not specified and photo href provided
	if ($thumbnail) {
		list($w, $h, $t, $a) = getimagesize($thumbnail);
		if ($w > 75 || $h > 75) {
			$thumbnail_new = $tmp_dir."/clad_create.{$clad_id}.{$now}.t2.jpg";
			create_thumbnail($thumbnail, $thumbnail_new);
			@unlink($thumbnail);
			$thumbnail = $thumbnail_new;
		}
	} else if (!empty($images)) {
		$thumbnail = $tmp_dir."/clad_create.{$clad_id}.{$now}.t2.jpg";
		create_thumbnail($images[0], $thumbnail);
	}

	//move thumbnail to target folder
	if ($thumbnail) {
		$thumb_final_path = $config_image_path."/classifieds/{$clad_id}.jpg";
		$ret = rename($thumbnail, $thumb_final_path);
		if (!$ret) {
			request_log("Error: Thumbnail move to final location failed !");
			json_output(array("status" => "error", "message" => "Internal error; Thumbnail move to final location failed !"));
			return;
		}
		$attr_names[] = "thumb";
		$attr_values[] = "{$clad_id}.jpg";
	}

	//move images and add to classifieds_image
	$system = new system();
	foreach ($images as $image) {
		$text = $system->_makeText(20);
		list($w, $h, $t, $a) = getimagesize($image);
		$x = "w";
		if ($h > $w)
			$x = "h";

		$image_final_path = $config_image_path."/classifieds/{$clad_id}_{$text}.jpg";
		$ret = rename($image, $image_final_path);
		if (!$ret) {
			request_log("Error: Image move to final location failed !");
			json_output(array("status" => "error", "message" => "Internal error; Image move to final location failed !"));
			return;
		}

		$res = $db->q("INSERT INTO classifieds_image (id, filename, x, width, height) VALUES (?, ?, ?, ?, ?)", 
						array($clad_id, "{$clad_id}_{$text}.jpg", $x, $w, $h)
						);
		$id = $db->insertid();
		if (!$id) {
			request_log("Error: Failed inserting clad image into db !");
			json_output(array("status" => "error", "message" => "Internal error; Failed inserting clad image into db !"));
			return;
		}
	}

	//update optional params
	if (count($attr_names) > 0) {
		$update_str = "";
		foreach ($attr_names as $attr_name) {
			if (!empty($update_str))
				$update_str .= ", ";
			$update_str .= "{$attr_name} = ?";
		}
		$attr_values[] = $clad_id;
		$res = $db->q("UPDATE classifieds SET {$update_str} WHERE id = ?", $attr_values);
	}

	//everything succeeded, "publish" ad
	$db->q("UPDATE classifieds SET done = ? WHERE id = ?", array($done, $clad_id));
	$db->q("UPDATE classifieds_loc SET done = ? WHERE post_id = ?", array($done, $clad_id));

	request_log("Success: Clad #{$clad_id} inserted successfully");

	classifieds::rotateIndex();

	$clad = clad::findOneById($clad_id);
	if (!$clad) {
		request_log("Error: Failed inserting clad image into db !");
		json_output(array("status" => "error", "message" => "Internal error; Failed re-fetching clad after insert into db !"));
		return;
	}

	json_output([
		"status" => "success", 
		"message" => "Clad #{$clad_id} inserted successfully",
		"as_url" => $clad->getUrl(),
		]);
	return;
}

function api_clad_get($params) {
	global $db, $config_image_path;

	$mandatory_params = array("clad_id");
	foreach ($mandatory_params as $mp) {
		if (!array_key_exists($mp, $params) || $params[$mp] == "") {
			request_log("Error: Mandatory param '{$mp}' not in passed params");
			json_output(array("status" => "error", "message" => "Bad Request; mandatory param '{$mp}' not in passed params"));
			return;
		}
	}

	$clad_id = intval($params["clad_id"]);

	//get classified ad entry
	$res = $db->q("
		SELECT c.* 
			, a.email as user_email, a.password as user_password
		FROM classifieds c
		LEFT JOIN account a on a.account_id = c.account_id
		WHERE id = ? LIMIT 1", array($clad_id));
	if ($db->numrows($res) != 1) {
		request_log("Error: Clad #{$clad_id} does not exist !");
		json_output(array("status" => "error", "message" => "Clad #{$clad_id} does not exist !"));
		return;
	}
	$row = $db->r($res);

	$clad = clad::withRow($row);
	if (!$clad) {
		request_log("Error: Clad #{$clad_id} invalid !");
		json_output(array("status" => "error", "message" => "Clad #{$clad_id} is invalid !"));
		return;
	}
		
	//get classified ad location
	$res2 = $db->q("
		SELECT l.loc_name, lp.loc_name as parent_name, lc.loc_name as country_name
		FROM 
			(SELECT loc_id FROM classifieds_loc WHERE post_id = ? LIMIT 1) cl
		INNER JOIN location_location l on l.loc_id = cl.loc_id
		LEFT JOIN location_location lp on lp.loc_id = l.loc_parent
		LEFT JOIN location_location lc on lc.loc_id = l.country_id",
			array($clad_id));
	if ($db->numrows($res2) != 1) {
		request_log("Error: Clad #{$clad_id} doesnt have any location assigned !");
		json_output(array("status" => "error", "message" => "Clad #{$clad_id} doesnt have any location assigned !"));
		return;
	}
	$row2 = $db->r($res2);
	$location_city = $row2["loc_name"];
	$location_state = $row2["parent_name"];
	$location_country = $row2["country_name"];
	if ($location_state == $location_country)
		$location_state = "";

	//get classified images
	$res3 = $db->q("SELECT * FROM classifieds_image WHERE id = ? ORDER BY image_id ASC", array($clad_id));
	$images = [];
	while ($row3 = $db->r($res3)) {
		$images[] = [
			"url" => "{$config_image_server}/classifieds/{$row3["filename"]}",
			"orientation" => $row3["x"],
			"width" => $row3["width"],
			"height" => $row3["height"],
			];
	}		

	//get last sale date
	$last_sale_date = null;
	$res4 = $db->q("SELECT transaction_time FROM account_purchase WHERE `what` = 'classifieds' AND item_id = ? and total > 0", [$clad_id]);
	if ($db->numrows($res4) > 0) {
		$row4 = $db->r($res4);
		$stamp = $row["transaction_time"];
		if ($stamp) {
			$last_sale_date = date("Y-m-d", $stamp);
		}
	}

	//some normalisation
	$type = $incall = $outcall = "";
	switch ($row["type"]) {
		case 2: $type = "ts"; break;
		case 1: $type = "fe"; break;
		default: $type = ""; break;
	}
	if ($row["incall"])
		$incall = "yes";
	else
		$incall = "no";
	if ($row["outcall"])
		$outcall = "yes";
	else
		$outcall = "no";

	require_once(_CMS_ABS_PATH."/_cms_files/classifieds/array.php");

	$build = $adbuild_build[$row["build"]];
	$ethnicity = $adbuild_ethnicity[$row["ethnicity"]];
	$eye_color = $adbuild_eyecolor[$row["eyecolor"]];
	$hair_color = $adbuild_haircolor[$row["haircolor"]];
	$cupsize = $adbuild_cupsize[$row["cupsize"]];
	$penis_size = $adbuild_penis_size[$row["penis_size"]];
	$kitty = $adbuild_kitty[$row["kitty"]];

	$status = null;
	if ($clad->getDeleted()) {
		$status = "deleted";
	} else {
		switch($clad->getDone()) {
			case 0: $status = "expired"; break;
			case 1: $status = "live"; break;
			default: $status = "other"; break;	
		}
	}

	$resp = [
		"id" => $row["id"],
		"account_id" => $row["account_id"],
		"user_email" => $row["user_email"],
		"user_password" => $row["user_password"],
		"location_city" => $location_city,
		"location_state" => $location_state,
		"location_country" => $location_country,
		"location_detail" => $row["location_detail"],
		"type" => $type,
		"phone" => $row["phone"],
		"email" => $row["email"],
		"website" => $row["website"],
		"firstname" => $row["firstname"],
		"title" => $row["title"],
		"content" => $row["content"],
		"avl_men" => $row["avl_men"],
		"avl_women" => $row["avl_women"],
		"avl_couple" => $row["avl_couple"],
		"incall" => $incall,
		"outcall" => $outcall,
		"age" => $row["age"],
		"build" => $build,
		"height_feet" => $row["height_feet"],
		"height_inches" => $row["height_inches"],
		"weight" => $row["weight"],
		"measure_1" => $row["measure_1"],
		"measure_2" => $row["measure_2"],
		"measure_3" => $row["measure_3"],
		"ethnicity" => $ethnicity,
		"eye_color" => $eye_color,
		"hair_color" => $hair_color,
		"cupsize" => $cupsize,
		"penis_size" => $penis_size,
		"kitty" => $kitty,
		"gfe" => $row["gfe"],
		"pornstar" => $row["pornstar"],
		"images" => $images,
		"status" => $status,
		"as_url" => $clad->getUrl(),
		"recurring" => $row["recurring"],
		"last_sale_date" => $last_sale_date,
		];

	request_log("Success: Clad #{$clad_id} get successfull");
	json_output(array("status" => "success", "clad" => $resp));
	return;
}

function api_clad_search($params) {
	global $db;

	if ((!array_key_exists("phone", $params) || $params["phone"] == "") && (!array_key_exists("email", $params) || $params["email"] == "")) {
		request_log("Error: Mandatory param 'email' or 'phone' not in passed params");
		json_output(array("status" => "error", "message" => "Bad Request; mandatory param 'phone' or 'email' not in passed params"));
		return;
	}
	$phone = $params["phone"];
	$email = $params["email"];

	$phone = preg_replace('/[^0-9\+]/', '', $phone);
	if (substr($phone, 0, 1) == "+") {
		if (substr($phone, 0, 2) == "+1" && strlen($phone) == 12)
			$phone = substr($phone, 2);
	}

	//search for classified ad
	$where = "";
	$pars = array();
	if (!empty($phone) && empty($email)) {
		$where = "AND REPLACE(REPLACE(c.phone,'-',''),' ','') = ?";
		$pars[] = $phone;
	} else if (empty($phone) && !empty($email)) {
		$where = "AND c.email = ?";
		$pars[] = $email;
	} else {
		$where = "AND (REPLACE(REPLACE(c.phone,'-',''),' ','') = ? OR c.email = ?)";
		$pars[] = $phone;
		$pars[] = $email;
	}
	$res = $db->q("SELECT c.id
					FROM classifieds c 
					WHERE 1=1 {$where}
					ORDER BY c.type ASC, c.`date` DESC
					LIMIT 1",
					$pars);
	if ($db->numrows($res) == 0) {
		request_log("Client: ".$_REQUEST["client"].", Email: {$email}, Phone: {$phone}, Result: not found");
		//json_output(array("status" => "error", "message" => "Not found"));
		json_output(array("status" => "success", "message" => "Not found"));
	}
	$row = $db->r($res);

//	$resp = [
//		"clad_id" => $row["id"],
//		];

	request_log("Success: Clad #{$clad_id} get successfull");
	json_output(array("status" => "success", "clad_id" => $row["id"]));
	return;
}

/**
 * This api call is called from hotleads
 * It can create new ad or accept purchase for existing ad (such ad must not be live - so only expired ads for now)
 */
function api_clad_purchase($params) {
	global $db, $config_image_path;

	request_log("api_clad_purchase(): params=".print_r($params, true));
	$clad_id = intval($params["clad_id"]);

	$account_id = null;
	$now = time();
	$now_datetime = date("Y-m-d H:i:s", $now);

	if ($clad_id) {
		//use existing ad
		$res = $db->q("
			SELECT c.account_id, cl.loc_id, l.country_id, c.done
			FROM classifieds c
			INNER JOIN classifieds_loc cl on c.id = cl.post_id
			INNER JOIN location_location l on l.loc_id = cl.loc_id 
			WHERE c.id = ?", [$clad_id]
			);
		if ($db->numrows($res) > 1)
			return json_output(array("status" => "error", "message" => "Classified ad #{$clad_id} posted in multiple locations - not implemented yet"));
		if ($db->numrows($res) == 0)
			return json_output(array("status" => "error", "message" => "Classified ad #{$clad_id} does not have entry in classifieds_loc table"));
		$row = $db->r($res);
		$location_id = $row["loc_id"];
		$country_id = $row["country_id"];
		$account_id = $row["account_id"];
		$done = $row["done"];
		
		//check - currently only expired ads!
		if ($done > 0) {
			request_log("api_clad_purchase(): ad #{$clad_id} is live - can't use purchase api to prolong! done={$done}");
			return json_output(array("status" => "error", "message" => "Ad is currently live (done={$done}) - prolonging ads not implemented yet"));
		}
	}

	if (!$account_id) {
		//create new ad
		$account_email = $params["account_email"];
		$account_password = $params["account_password"];
		if ($account_email && $account_password) {
			//registering new account with specified email and password
			$res = $db->q("SELECT account_id FROM account WHERE email = ?", [$account_email]);
			if ($db->numrows($res) > 0) {
				request_log("api_clad_purchase(): Account with email '{$account_email}' already exists on AS");
				return json_output(array("status" => "error", "message" => "Account with email '{$account_email}' already exists on AS"));
			}
			$username = "acc_".time();
			$res = $db->q("
				INSERT INTO account
				(created_stamp, username, password, email, email_register, approved, email_confirmed) 
				VALUES 
				(?, ?, ?, ?, ?, ?, ?)
				",
				[time(), $username, $account_password, $account_email, $account_email, 1, 1]
				);
			$account_id = $db->insertid($res);
			if (!$account_id) {
				request_log("api_clad_purchase(): can't insert account into db, un='{$username}', pw='{$account_password}', em='{$account_email}'");
				return json_output(array("status" => "error", "message" => "Can't insert account into database"));
			}
		} else {
			//using system account system@adultsearch.com
			$res = $db->q("SELECT account_id FROM account WHERE email = 'system@adultsearch.com' LIMIT 1");
			if ($db->numrows($res) != 1) {
				return json_output(array("status" => "error", "message" => "Error: system ASaccount no present - please contact administrator"));
			}
			$row = $db->r($res);
			$account_id = $row["account_id"];
		}
	}

	if (!$clad_id) {
		//check and prepare all clad attributes
		$country_name = $params["location"]["country"];
		$state_name = $params["location"]["state"];
		$city_name = $params["location"]["city"];
		$res = $db->q("SELECT loc_id FROM location_location WHERE loc_type = 1 AND loc_name = ?", [$country_name]);
		if ($db->numrows($res) != 1)
			return json_output(array("status" => "error", "message" => "Country '{$country_name}' not found"));
		$row = $db->r($res);
		$parent_id = $country_id = $row["loc_id"];
		if ($state_name) {
			$res = $db->q("SELECT loc_id FROM location_location WHERE loc_type = 2 AND loc_name = ? AND loc_parent = ?", [$state_name, $country_id]);
			if ($db->numrows($res) != 1)
				return json_output(array("status" => "error", "message" => "State '{$state_name}' not found"));
			$row = $db->r($res);
			$parent_id = $state_id = $row["loc_id"];
		}
		$res = $db->q("SELECT loc_id FROM location_location WHERE loc_type = 3 AND loc_name = ? AND loc_parent = ?", [$city_name, $parent_id]);
		if ($db->numrows($res) != 1)
			return json_output(array("status" => "error", "message" => "City '{$city_name}' not found"));
		$row = $db->r($res);
		$city_id = $location_id = $row["loc_id"];

		//create ad in db
		$res = $db->q("
			INSERT INTO classifieds 
			(state_id, type, loc_id, loc_name, account_id, done, 
			ethnicity, email, website, reply, title, 
			content, incall, outcall, ter, bigdoggie,
			age, pornstar, haircolor, eyecolor, phone, 
			total_loc, firstname, avl_men, avl_women, avl_couple, 
			height_feet, height_inches, weight, build, measure_1, measure_2, measure_3, 
			cupsize, gfe, created)
			VALUES
			(?, ?, ?, ?, ?, ?, 
			?, ?, ?, ?, ?,
			?, ?, ?, ?, ?,
			?, ?, ?, ?, ?,
			?, ?, ?, ?, ?, 
			?, ?, ?, ?, ?, ?, ?,
			?, ?, ?)",
			[$parent_id, $params["type"], $location_id, $city_name, $account_id, -1, 
			$params["ethnicity"], $params["email"], $params["website"], 2, $params["title"], 
			$params["content"], $params["incall"], $params["outcall"], $params["ter"], $params["bigdoggie"], 
			$params["age"], $params["pornstar"], $params["haircolor"], $params["eyecolor"], $params["phone"],
			1, $params["firstname"], $params["avl_men"], $params["avl_women"], $params["avl_couple"],
			$params["height_feet"], $params["height_inches"], $params["weight"], $params["build"], $params["measure_1"], $params["measure_2"], $params["measure_3"],
			$params["cupsize"], $params["gfe"], $now]
			);
		$clad_id = $db->insertid();
		if (!$clad_id) {
			request_log("api_clad_purchase(): can't insert ad into db!");
			return json_output(array("status" => "error", "message" => "Can't insert ad into database"));
		}
		$res = $db->q("
			INSERT INTO classifieds_loc
			(state_id, loc_id, neighbor_id, type, post_id, updated, done)
			VALUES
			(?, ?, ?, ?, ?, ?, ?)",
			[$parent_id, $location_id, 0, $params["type"], $clad_id, $now_datetime, -1]
			);
		$clad_loc_id = $db->insertid();
		if (!$clad_loc_id) {
			request_log("api_clad_purchase(): can't insert ad loc into db!");
			return json_output(array("status" => "error", "message" => "Can't insert ad loc into database"));
		}
	}

	//parse payment option and create payment in db
	$international = false;
	$autorenew = $auto_renew_fr = $recurring = $sponsor_city = $sponsor_total = $auto_renew_time = $total = 0;
	if (!in_array($country_id, [16046, 16047, 41973]))
		$international = true;
	switch($params["payment_option"]) {
		case "single":
			if ($international)
				$total = 9.99;
			else
				$total = 9.99;
			break;
		case "recurring":
			$auto_renew_fr = 1; $recurring = 1; $total = 49.99;
			break;
		case "recurring_thumbnail_combo":
			$auto_renew_fr = 1; $recurring = 1; $sponsor_city = 1; $sponsor_total = 50; $total = 80.00;
			$res = $db->q("SELECT * FROM classifieds_sponsor WHERE post_id = ? AND loc_id = ?", [$clad_id, $location_id]);
			if ($db->numrows($res) != 1) {
				$res = $db->q("
					INSERT INTO classifieds_sponsor
					(loc_id, state_id, post_id, day, type, done)
					VALUES
					(?, ?, ?, ?, ?, ?)",
					[$location_id, $parent_id, $clad_id, 30, $params["type"], -1]
					);
				$clad_sponsor_id = $db->insertid();
				if (!$clad_sponsor_id) {
					request_log("api_clad_purchase(): can't insert ad sponsor into db!");
					return json_output(array("status" => "error", "message" => "Can't insert ad sponsor into database"));
				}
			}
			break;
		default:
			request_log("api_clad_purchase(): unsupported payment_option: '{$params["payment_option"]}'!");
			return json_output(array("status" => "error", "message" => "Unsupported payment option '{$params["payment_option"]}'"));
			break;
	}

	$res = $db->q("
		INSERT INTO classifieds_payment
		(post_id, account_id, loc_id, owner, autorenew, auto_renew_fr, recurring, sponsor_city, sponsor_total, auto_renew_time, total, `time`)
		VALUES
		(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
		[$clad_id, $account_id, $location_id, 0, $autorenew, $auto_renew_fr, $recurring, $sponsor_city, $sponsor_total, $auto_renew_time, $total, $now_datetime]
		);
	$clad_payment_id = $db->insertid();
	if (!$clad_payment_id) {
		request_log("api_clad_purchase(): can't insert ad payment into db!");
		return json_output(array("status" => "error", "message" => "Can't insert ad payment into database"));
	}

	request_log("api_clad_purchase(): successfully created classifieds_payment, clad_payment_id='{$clad_payment_id}'");

	//request_log("Success: Clad #{$clad_id} get successfull");
	return json_output(array("status" => "success", "clad_id" => $clad_id, "clad_payment_id" => $clad_payment_id));
}

//dispatcher
switch($action) {
	case "clad_create":
		api_clad_create($params);
		break;
	case "clad_get":
		api_clad_get($params);
		break;
	case "clad_search":
		api_clad_search($params);
		break;
	case "clad_purchase":
		api_clad_purchase($params);
		break;
	default:
		request_log("Error: unknown action '{$action}'");
		json_output(array("status" => "error", "message" => "Bad Request; unknown action: '{$action}'"));
		break;
}

die;

?>
