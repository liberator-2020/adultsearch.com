<?php
/** 
 * this script was used one-time to scrape all the escorts from BP, script actually ran on EMP server
 */

chdir("/www/virtual/adts/www.adultsearch.com/html");
define( '_CMS_FRONTEND', 1);

require_once('./inc/config.php');
require_once('./inc/common.php');

function mlog($msg) {
	$h = fopen("/www/virtual/adts/www.adultsearch.com/html/bp_scrape.log", "a");
	if (!$h)
		return;
	fputs($h, date("[Y-m-d H:i:s]")." CLlog: ".$msg."\n");
	fclose($h);
}

function add_location($loc_id) {
	global $db;

	$res = $db->q("SELECT loc_id FROM bp_scrape_loc WHERE loc_id = ?", array($loc_id));
	if ($db->numrows($res) > 0)
		return false;
	$db->q("INSERT INTO bp_scrape_loc (loc_id) VALUES (?)", array($loc_id));
	return true;
}

function add_phone($loc_id, $ad_url, $bp_id, $phone) {
	global $db;

	$res = $db->q("SELECT phone FROM bp_scrape_phone WHERE phone = ?", array($phone));
	if ($db->numrows($res) > 0) {
		$db->q("UPDATE bp_scrape_phone SET bp_id = ? WHERE phone = ?", array($bp_id, $phone));
		return false;
	}
	$db->q("INSERT INTO bp_scrape_phone (loc_id, link, bp_id, phone) VALUES (?, ?, ?, ?)", array($loc_id, $ad_url, $bp_id, $phone));
	return true;
}

global $db, $account, $smarty;

echo "Start...\n";
$curl = new curl("_bp.cookie");

$res = $db->q("SELECT l.loc_id, l.dir 
				FROM location_location l 
				LEFT JOIN bp_scrape_loc bl on bl.loc_id = l.loc_id
				WHERE l.bp = 1 AND bl.id IS NULL");
while($row = $db->r($res)) {
	$loc_id = $row["loc_id"];
	$dir = $row['dir'];
	$bp_loc_url_name = preg_replace('/-/', '', $dir);
	echo "Location '{$bp_loc_url_name}'\n";

	$url = "http://{$bp_loc_url_name}.backpage.com/FemaleEscorts/";

    $result = $curl->get($url);
	$h = fopen("./loc.html", "w");
	if ($h) {
		fputs($h, $result);
		fclose($h);
	}
	
	//$result = file_get_contents("./result.html");
	//var_dump($result);
	//echo "len(result)=".strlen($result)."\n";
	if (strlen($result) < 1000) {
		echo "ERROR - location not found! \n";
		continue;
	}


	$ret = preg_match_all('#<a href="'.$url.'([^"]*)"#', $result, $matches);
	if ($ret === false || $ret === 0) {
		echo "Error: no match!\n";
		var_dump($ret);
		break;
	}

	$cnt = count($matches[1]);
	echo "count(matches)=".$cnt."\n";
	$i = 0;
	$error = false;
	foreach ($matches[1] as $match) {
		//echo "Match {$i}: '{$match}' ";
		if (!preg_match('#^[a-zA-Z\-0-9]*/([0-9]*)$#', $match, $ms)) {
//			echo "NO\n";
			continue;
		}
		
		$bp_id = $ms[1];
		$res2 = $db->q("SELECT phone FROM bp_scrape_phone WHERE bp_id = ?", array($bp_id));
		if ($db->numrows($res2) > 0) {
			echo "Skipping '{$bp_id}'...\n";
			continue;
		}

		$ad_url = $url.$match;
		echo "Scraping '{$bp_id}' - '{$ad_url}' - ";

    	$result = $curl->get($ad_url);
		$h = fopen("./ad.html", "w");
		if ($h) {
			fputs($h, $result);
			fclose($h);
		}

		if (strpos($result, "Error 525") !== false) {
			echo "ERROR: Error 525 !\n";
			$error = true;
			break;
		}

//		$result = file_get_contents("./ad.html");

//		echo "len(ad_result)=".strlen($result)."\n";

		$ret = preg_match_all('#[^a-zA-Z0-9]([0-9]{3}[ \-*\(\)/]?[0-9]{3}[ \-*\)/]?[0-9]{4})[^a-zA-Z0-9]#', $result, $matches);
	    if ($ret === false || $ret === 0) {
			echo "ERROR: no phone ?\n";
			//break;
			continue;
		}
		$cnt = count($matches[1]);
//		echo "count(matches)=".$cnt."\n";
		$j = 0;
		foreach ($matches[1] as $match) {
			$phone = preg_replace('/[^0-9]/', '', $match);
//			echo "Phone {$j}: '{$phone}'\n";
			add_phone($loc_id, $ad_url, $bp_id, $phone);
			$j++;
		}

		echo "Added {$j} number(s)\n";

		$i++;
	}

	if ($error) {
		break;
	}

	add_location($loc_id);
	sleep(3);
	echo "Scraped {$i} ads.\n\n";
//	break;
}

echo "Done\n";

?>
