<?php
defined("_CMS_FRONTEND") or die("No Access");

deprecated_controller();

/*
global $ctx, $db, $smarty, $page, $account, $gTitle, $gDescription, $gModule, $gLocation, $advertise, $sphinx, $config_image_server;
$system = new system;

$location = location::findOneById($ctx->location_id);
if ($location) {
	$smarty->assign("link_back", $location->getUrl());
	$smarty->assign("location_video_html", $location->getVideoHtml());
}

if( $account->core_loc_id )
	$core_state_id = $account->core_loc_id;
else
	$core_state_id = $account->corestate(1);

$loc_around = array();
if ($account->core_loc_id && $account->core_loc_type > 2)
    $loc_around = $account->locAround();

$dancer = isset($_GET["dancer"]) ? intval($_GET["dancer"]) : 1;
if( !$dancer ) $dancer = 1;

$loc_name = $account->core_loc_array['loc_name'];
$gTitle = "$loc_name Brothels and Whorehouses Prices, Locations and Reviews";
$gDescription = "We provide the most complete guide to brothels and whorehouses in $loc_name along with rates, directions and user generated reviews.";

$filter_link = array();
$gTitlex = "";
foreach($_GET as $key => $value) {
	if( in_array($key, array("loc_id", "query", "zipcode")) && $value != "" ) {
		$ftype = $key;
		$valuex = $value;
		$skipfilter = 0;
		if( $key == "zipcode" ) { $key = "Zip Code: $valuex"; $skipfilter = 1;
			$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key, "link"=>"{$account->core_loc_array['loc_url']}$gModule/"); }
		else if( $key == "query" ) { $key = "'$valuex'"; }
		else if( $key == "loc_id" ) $key = $loc_name;

		if( !$skipfilter ) $filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key);
	}
}

$item_per_page = isset($_GET["ipp"]) ? intval($_GET["ipp"]) : 30;
if( $item_per_page > 100 ) $item_per_page = 30;
$start = (($page*$item_per_page)-$item_per_page);
if( $start < 0 ) $start = 0;

$sphinx->reset();
$sphinx->SetLimits( $start, $item_per_page, 1000 );

if( isset($_GET["zipcode"]) && !empty($_GET["zipcode"]) ) {
	if( ($pos=strpos($_GET["zipcode"], "-")) ) $_GET["zipcode"] = substr($_GET["zipcode"], 0, $pos);
	$zipcode = preg_replace("/[^0-9a-z]/i","", $_GET["zipcode"]);
	$mile_in = intval($_GET["miles"]); if( !$mile_in ) $mile_in = 10;
	if( $mile_in == -1 ) $mile_in = 1000;
	$res = $db->q("select z.lat, z.lon, radians(z.lat) latt, radians(z.lon) longg, l.loc_parent from core_zip z inner join location_location l on z.loc_id = l.loc_id where z.zip = '$zipcode' limit 1");
	if( $db->numrows($res) ) {
		$s = $db->r($res);
		$lat_me = $s["lat"]; $lon_me = $s["lon"];
		$core_state_id = $s["loc_parent"];
		$latt = $s["latt"]; $long = $s["longg"];
		$sphinx->SetGeoAnchor('loc_lat', 'loc_long', $latt, $long);
		$sphinx->SetFilterFloatRange('@geodist', 0, ($mile_in*1609.344));
		$zipfound = 1;
	} else {
		$error = "Zip code($zipcode) could not find. Please make sure that you typed the correct zip code.";
	}
	$smarty->assign("miles", $mile_in==1000?-1:$mile_in);
	$smarty->assign("zipcode", $_GET["zipcode"]);
} elseif( $account->core_loc_array['loc_lat'] ) {
	$sphinx->SetGeoAnchor('loc_lat', 'loc_long', deg2rad($account->core_loc_array['loc_lat']), deg2rad($account->core_loc_array['loc_long']));
	$sphinx->SetFilterFloatRange('@geodist', 0, (1000*1609.344));
	//$distance = 1;
}

if (!isset($zipfound) && !$account->core_loc_id)
	$sphinx->SetFilter('state_id', array($core_state_id));

if ($account->core_loc_id && $account->core_loc_type > 2)
	$sphinx->SetFilter('loc_id', $loc_around);
elseif ($account->core_loc_id)
	$sphinx->SetFilter('state_id', array($account->core_loc_id));

if ($account->core_state_id)
	$sphinx->SetFilter('state_id', array($account->core_state_id));

$advertise->showAd(16, $account->core_loc_id);

$query = "";
if( isset($_GET["phone"]) && !empty($_GET["phone"]) ) {
	$phone = preg_replace("/[^0-9\*]/", "", $_GET["phone"]);
	$query = $phone;
	//else $smarty->assign("err_phone", true);
}
if( isset($_GET["name"]) && !empty($_GET["name"]) ) {
	$name = GetGetParam("name");
	$query .= $query ? " $name" : "$name";
	$error = "No strip club found with the word: ".$_GET["name"];
	$smarty->assign("name", $_GET["name"]);
}

$orderway = isset($_GET["orderway"])&& intval($_GET["orderway"]) ? intval($_GET["orderway"]) : 1;
$way = $orderway == 1 ? "DESC" : "ASC";

if($_GET["order"] == 'overall') { $order = "overall $way"; }
elseif($_GET["order"] == 'review') $order = "review $way";
elseif($_GET["order"] == "name") $order = "name $way";
elseif($_GET["order"] == "last_review_id") $order = "last_review_id $way";
elseif($ordex == "score") $order = "overall $way";
elseif($_GET["order"] == 'distance' || ($zipfound||$distance) ) { $order = "sponsor DESC, @geodist $way"; }
else $order = "sponsor DESC, last_review_id DESC";
$sphinx->SetSortMode(SPH_SORT_EXTENDED, $order);

$sphinx_index = "brothel";
$results = $sphinx->Query($query, $sphinx_index );
$total = $results["total_found"];
$result = $results["matches"];


foreach($result as $res) {
	$ids .= $ids ? (",".$res["id"]) : $res["id"];
}

if ($ids)
	$res = $db->q("
		select b.*, l.loc_url, l.country_sub, r.comment rc, a2.avatar ra, a2.username as ru 
		from brothel b 
		inner join location_location l on b.loc_id = l.loc_id 
		left join (place_review r inner join account a2 using (account_id)) on b.last_review_id = r.review_id and r.module = 'brothel' 
		where b.id in ($ids) 
		order by field(b.id,$ids)
	");

while($row=$db->r($res)) {
	if( $zipcode ) {
		if( !isset($lat_me) ) continue;
		if( $row["loc_lat"] && $row["loc_long"] ) {
			$mile = number_format(distance($lat_me, $lon_me, $row["loc_lat"], $row["loc_long"], "M"),1);
			if( $mile_in != -1 && $mile_in < $mile && $zipcode != $row["zipcode"]) continue;
		} else $mile = "unknown";
	} elseif( $distance ) {
		$mile = number_format(distance($account->core_loc_array["loc_lat"], $account->core_loc_array["loc_long"], $row["loc_lat"], $row["loc_long"], "M"),1);
	}

	$link = dir::getPlaceLinkAux($row['country_sub'], $row["loc_id"], $gModule, $row["id"], $row['name'], trim($row['loc_url'], "/"));

	$locname = "{$row["loc_name"]}, {$row["state_name"]}";

	$thumbpath = !empty($row["thumb"]) ? "{$config_image_server}/brothel/t/{$row["thumb"]}" : "/images/placeholder.gif";
	$ra = "{$config_image_server}/avatar/" .  (!empty($row["ra"]) ? $row["ra"] : "no_avatar.jpg");

	$sc[] = array(
		"id"=>$row["id"],
		"name"=>$row["name"],"thumb"=>$row["thumb"],
		"address"=>$row["address1"],
		"phone"=>makeproperphonenumber($row["phone"]),
		"loc_name"=>$locname,
		"zipcode"=>$row["zipcode"],
		"distance"=>isset($zipcode)||$distance?(float)$mile:NULL,
		"review"=>$row["review"],
		"edit"=>$row["edit"],
		"sponsor"=>$row["sponsor"],
		"linkoverride" => $link,
		"longitude"=>$row['loc_long'],
		"latitude"=>$row['loc_lat'],
		"thumbpath"=>$thumbpath,
		"lr"=>$row['last_review_id'],
		"rc"=>$row['rc'],
		"ra"=>$ra,
		"ru"=>$row['ru'],
	);
}

$json = json_encode($sc);
$smarty->assign("mapjson", $json);

$smarty->assign("sc", $sc);
$smarty->assign("total", $total);

if( isset($error) ) $smarty->assign("error", $error);

$query = _pagingQuery();

$dir = new dir;
$ctx->page = $page;
$smarty->assign("pager_array", $dir->get_pager_array($total, $item_per_page, $account->core_loc_array['loc_url']."/{$gModule}/", true));
$smarty->assign("paging", $db->paging($total, 30, yes, yes, $query));

if( $zipfound && strstr($gLocation, "zipcode") ) $query = _pagingQuery(array("loc_id", "zipcode"));
else $query = _pagingQuery(array("loc_id"));

_pagingQueryFilter($query, $filter_link);
if( $zipfound && strstr($gLocation, "zipcode") ) $linksort = _pagingQuery(array("order", "orderway", "zipcode"));
else $linksort = _pagingQuery(array("order", "orderway"));

$smarty->assign("linksort", $linksort);
$smarty->assign("search_hidden", _pagingQuery(array("loc_id", "zipcode", "miles", "phone"), 1));
$smarty->assign("link", $query);
$smarty->assign("filter", $filter_link);

if ($zipfound)
	$loc_name = "$zipcode";

$order = isset($_GET["order"]) ? $_GET["order"] : ($distance?"distance":"last_review_id");
if( !isset($_GET["orderway"]) && $order == "last_review_id" ) $orderway = 1;
elseif( !isset($_GET["orderway"]) && $distance ) $orderway = 2;
$sort = array();
if( isset($zipfound) || isset($distance)) $sort[] = array("key"=> "Distance", "value"=>"distance", "way"=>$order == "distance" && $orderway == 2 ? 1 : 2,
"selected"=>$order == "distance" ?true : false);
$sort[] = array("key"=> "Rating", "value"=>"overall", "way"=>$order == "overall" && $orderway == 1 ? 2 : 1, "selected"=>$order == "overall" ? true : false);
$sort[] = array("key"=> "# of Reviews", "value"=>"review", "way"=>$order == "review" && $orderway == 1 ? 2 : 1, "selected"=> $order == "review"? true : false);
$sort[] = array("key"=> "Club Recently Reviewed", "value"=>"last_review_id", "way"=>$order == "last_review_id" && $orderway == 1 ? 2 : 1, "selected"=>$order
=="last_review_id" ?true : false);
$smarty->assign("sort", $sort);

$smarty->assign("category", "brothel");
$smarty->assign("what", "Brothel");
$smarty->assign("worker_link", "brothel");
$smarty->assign("image_path", "brothel");
$smarty->assign("place_link", "club");
$smarty->assign("imageserver", $config_image_server."/brothel/");
$smarty->assign("loc_name", $account->core_loc_array['loc_name']);
$smarty->assign("rss_available", true);

$file = $smarty->fetch(_CMS_ABS_PATH."/templates/place/place_list.tpl");
echo $file;
*/
?>
