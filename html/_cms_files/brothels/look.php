<?php
defined("_CMS_FRONTEND") or die("No Access");

deprecated_controller();

/*
global $db, $smarty, $account, $page, $gTitle, $gModule, $gLocation, $gItemID, $gDescription, $advertise, $config_mapquest_key, $config_image_server;
$system = new system;

$id = $gItemID;
$account_id = $account->isloggedin();

$review = new review("brothel", $id, "club", "", "brothel");

$res = $db->q("SELECT b.*, l.country_sub, l.loc_url 
				FROM brothel b 
				LEFT JOIN location_location l on b.loc_id = l.loc_id 
				WHERE b.id = '$id'");
if (!$db->numrows($res))
	$system->moved("/$gModule/");
$row = $db->r($res);

$name2url = name2url($row['name']);

if ($account->core_sub != $row["country_sub"] || !strstr($_SERVER['REQUEST_URI'], $row['loc_url']) || $gLocation != $name2url)
	system::moved(location::getUrlStatic($row["country_sub"], $row["loc_url"])."{$gModule}/{$name2url}/{$row['id']}");

$gTitle = "{$row["name"]} ". makeproperphonenumber($row['phone']) ." Brothels in {$row['loc_name']}, {$row['state_name']}";

//map
$map = prepareMap($id, 'brothel', $row, 'brothel', 'brothel');

$smarty->assign("map", $map);
$smarty->assign("link_map_update", "/map?id={$id}&section=brothel&update=1");

$smarty->assign('breadplace', $row['name']);
$quickread = $review->reviews();
$smarty->assign("quickread", $quickread);

$phone = makeproperphonenumber($row["phone"]);

if (!empty($quickread))
	$gDescription = substr(htmlspecialchars($quickread[0]['review'], ENT_QUOTES), 0, 100) .". ";
$gDescription .= "{$row['name']} $phone Brothel in {$account->core_loc_array['loc_name']} {$account->core_loc_array['s']}";

$loc = "<a href='{$account->core_loc_array['loc_url']}$gModule/' title='{$row["loc_name"]} brothels'>{$row["loc_name"]}</a>, <a href='{$account->core_loc_array['lp_loc_url']}$gModule/' title='{$row["state_name"]} brothels'>{$row["state_name"]}</a> {$row["zipcode"]}";

global $ctx;
$ctx->item = $row;
$smarty->assign("hours", dir::getHours(($row["byapponly"] == 1) ? true : false));
$smarty->assign("current_weekday", strtolower(date("l")));

$advertise->showAd(16, $account->core_loc_id);

if( $row["cc_visa_master"]||$row["cc_ae"]||$row["cc_dis"]||$row["cc_atm"]) {
	$p = array();
	if($row["cc_visa_master"])
		$p[] = "VISA & MC";
	if($row["cc_ae"])
		$p[] = "American Express";
	if($row["cc_dis"])
		$p[] = "Discover";
	if($row["cc_atm"])
		$p[] = "ATM Available";
	if (!empty($p))
		$c1[] = array("label" => "Payments", "val" => implode(",", $p));
}

if( $row["rate_15"] ) {
	$r = "$money_sign{$row["rate_15"]}.00";
	if( $exchange )
		$r .= " (\$".number_format($row["rate_15"]*$exchange, 2).")";
	$c1[] = array("label" => "15 Minutes", "val" => $r);
}

$c1[] = array("label" => "Price", "val" => "Negotiate With Prostitute");

$smarty->assign("c1", $c1);

//photos
$imagedir = "brothel";
$photos = array();
if ($row["image"] != "" && $row["thumb"] != "") {
	$photos["image"] = "{$config_image_server}/{$imagedir}/{$row["image"]}";
	$photos["thumb"] = "{$config_image_server}/{$imagedir}/t/{$row["thumb"]}";

	$pics = array();
	$rex = $db->q("select picture, filename, thumb from place_picture where id = '$id' and module = 'brothel'");
	while($rox=$db->r($rex)) {
		$pics[] = array(
			"module" => "brothel",
			"id" => $rox["picture"],
			"image" => "{$config_image_server}/{$imagedir}/".$rox["filename"],
			"thumb" => "{$config_image_server}/{$imagedir}/t/".$rox["thumb"],
			);
	} 

	if ($pics) {
		include_html_head("js", "/js/jquery.tools.scrollable.js");
		$pics_in_group = 2;
		$pics_groups = array_chunk($pics, $pics_in_group);
		$photos["pics"]  = $pics_groups;
	}
}
$smarty->assign("photos", $photos);

if( $row["website"] ) {
	if( strncmp($row["website"], "http://", 7) )  $row["website"] = "http://" . $row["website"];
	$web = parse_url($row["website"]);
	$website = $row["website"];
	$web_host = $web["host"];
}

$phone = makeproperphonenumber($row["phone"]);

$smarty->assign("category", "brothel");
$smarty->assign("name", $row["name"]);
$smarty->assign("loc_name", $row["loc_name"]);
$smarty->assign("id", $id);
$smarty->assign("totalreview", $row["review"]);
$smarty->assign("paging", $db->paging($row["review"], 10, false, true, "?id={$id}"));
$smarty->assign("editlink", "brothel");

$addressLx = "<b>{$phone}</b><br />{$row["address1"]}<br />{$loc}<br/>";
if ($row["email"])
	$addressLx .= "<a href=\"mailto:{$row["email"]}\" rel=\"nofollow\" >{$row["email"]}</a><br/>";
if ($website)
	$addressLx .= "<a href=\"$website\" target=\"_blank\" rel=\"nofollow\" class=\"website\">$web_host</a><br/>";
$smarty->assign("address", $addressLx);

$core_state_name = "<a href='{$account->core_loc_array["loc_url"]}$gModule/' title='{$row["loc_name"]} brothels'>{$row["loc_name"]} Brothels</a>";
$smarty->assign("core_state_name", $core_state_name);
$smarty->assign("link_back", "{$account->core_loc_array["loc_url"]}$gModule/");

//forum
$forum_id = 23;
$smarty->assign("forum_topic", forum::getLastTopicsForPlace($forum_id, $id));
$smarty->assign("new_topic_link", forum::getNewTopicLinkForCategory($forum_id, $id));

include_html_head("css", "/css/info.css");

//MapQuest and Leaflet include
include_html_head('js', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.js');
include_html_head('css', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.css');
include_html_head('js', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.js');
include_html_head('css', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.css');

$smarty->display(_CMS_ABS_PATH."/templates/place/place_place.tpl");
*/
?>
