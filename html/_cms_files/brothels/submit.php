<?php

defined('_CMS_FRONTEND') or die('Restricted access');

require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");

global $db, $smarty, $account;
$form = new data;
$system = new system;

$account_id = $account->IsLoggedIn();

$form->table = "brothel";

$c = new CColumnId("id", "ID");
$form->AppendColumn($c);

$c = new CColumnLocation("loc_id", "Location", 2);
if( !$account->isadmin() ) $c->setColUpdate(false);
$c->setColMandatory(true);
$c->setLocName("loc_name", "concat(loc_name, ' ', s)", "City");
$form->AppendColumn($c);

$c = new CColumnString("name", "Name");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("address1", "Address");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "zipcode");
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Phone");
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnString("email", "E-Mail");
$form->AppendColumn($c);

$c = new CColumnHour("hours", "Hours Of Operation");
$form->AppendColumn($c);

$c = new CColumnBoolean("cc_visa_master", "VISA & MC");
$c->setColTitle("Payments");
$c->AddOption("cc_ae", "American Express");
$c->AddOption("cc_dis", "Discover");
$c->AddOption("cc_atm", "ATM Available");
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setWatermark();
$c->setBackup("brothel");
$c->setPath("brothel");
$form->AppendColumn($c);

$c = new CColumnImage("coupon", "Coupon");
$c->setMaxDimensions(700, 500);
$c->setPath("brothel/c");
$form->AppendColumn($c);

$c = new CColumnPicture("brothel_picture", "Extra Pictures", "brothel");
$c->setPath("brothel");
$c->setPathT("brothel/t");
$c->setWatermark();
$form->AppendColumn($c);

$form->visitor_mode = true;
$form->visitor_url = "/brothels/";
$form->admin_url = "/worker/brothel";

$form->ShowPage();
?>
