<?php

global $db, $account, $smarty;

if (isset($_POST["phone1"]) ) {
	$phone1 = GetPostParam("phone1");
	$phone2 = GetPostParam("phone2");
	$phone3 = GetPostParam("phone3");
	$phone = preg_replace('/[^0-9]/', '', $phone1.$phone2.$phone3);
	$smarty->assign("phone1", $phone1);
	$smarty->assign("phone2", $phone2);
	$smarty->assign("phone3", $phone3);
	if (strlen($phone) == 10) {
		$res = $db->q("
			SELECT p.name, p.place_id, p.address1, p.zipcode, l.loc_name, l.s 
			FROM place p 
			INNER JOIN location_location l on l.loc_id = p.loc_id
			WHERE p.phone1 = ? AND edit >= 1 
			ORDER BY p.name
			", 
			[$phone]
			);
		if ($db->numrows($res)) {
			$row = $db->r($res);
			$smarty->assign("place", [
				"place_id" => $row["place_id"],
				"name" => $row["name"], 
				"address" => $row["address1"],
				"loc_name" => $row["loc_name"],
				"s" => $row["s"],
				"zipcode" => $row["zipcode"]
				]);
		} else 
			$smarty->assign("notfound", true);
	} else {
		$smarty->assign("error", "Phone number has to be 10 digit");
	}
} else if (isset($_GET["City"])) {
	$loc_id = intval($_GET["City"]);
	$res = $db->q("
		SELECT p.name, p.place_id, p.address1, p.zipcode, l.loc_name, l.s 
		FROM place p 
		INNER JOIN location_location l on l.loc_id = p.loc_id
		WHERE p.loc_id = ? AND edit >= 1 
		ORDER BY p.name
		", 
		[$loc_id]
		);
	while ($row = $db->r($res)) {
		$places[] = [
			"place_id" => $row["place_id"],
			"name" => $row["name"], 
			"address" => $row["address1"],
			"loc_name" => $row["loc_name"],
			"s" => $row["s"],
			"zipcode" => $row["zipcode"]
			];
	}
	$smarty->assign("places", $places);
	$smarty->assign("City", $loc_id);
}

include_html_head("js", "/js/tools/jquery.autotab.js");
$smarty->display(_CMS_ABS_PATH."/templates/businessowner/find.tpl");

?>
