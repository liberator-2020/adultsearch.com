<?php

global $smarty, $gTitle;

$gTitle = "Business Owners";

$id = intval($_REQUEST["id"]);

if (!$section || !$id)
	system::go("/businessowner/getstarted");

$smarty->assign("id", $id);
$smarty->display(_CMS_ABS_PATH."/templates/businessowner/home.tpl");

?>
