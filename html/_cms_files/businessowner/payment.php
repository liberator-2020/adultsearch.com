<?php

defined('_CMS_FRONTEND') or die('Restricted access');

require(_CMS_ABS_PATH."/_cms_files/businessowner/common.php");

global $db, $account, $smarty;

if (!account::ensure_loggedin())
	die("Invalid access");

$p = intval($_REQUEST["p"]);
$id = intval($_REQUEST["id"]);
if (!$id)
	system::moved("/");

//fetch place from db
$res = $db->q("SELECT sponsor, owner, place_type_id FROM place WHERE place_id = ? AND edit >= 1", [$id]);
if (!$db->numrows($res)) {
	reportAdmin("businessowner/payment error", "id={$id}, p={$p}");
	die();
}
$row = $db->r($res);

//checks
if ($row["sponsor"] == 1)
	system::go("/dir/place?id=".$id, "This place is already advertising...");
if( $row["owner"] == 1 )
	system::go("/dir/place?id=".$id, "This place is owned by another member.");
if (!in_array($p, array(1, 2, 3)))
	system::moved("/businessowner/find");

//determine price
$sponsor = null;
if ($p == 1) {
	//premium monthly
	$total = (in_array($row["place_type_id"], [1,15])) ? SECTION_SCEMP_PREMIUM : SECTION_DEFAULT_PREMIUM;
	$period = 30;
	$sponsor = 1;
} else if ($p == 3) {
	//premium yearly
	$total = (in_array($row["place_type_id"], [1,15])) ? SECTION_SCEMP_PREMIUM_YEAR : SECTION_DEFAULT_PREMIUM_YEAR;
	$period = 360;
	$sponsor = 1;
} else {
	//regular year
	$total = (in_array($row["place_type_id"], [1,15])) ? SECTION_SCEMP_NORMAL : SECTION_DEFAULT_NORMAL;
	$period = 360;
}

//create payment in database
$now = time();
$ip_address = account::getUserIp();
$res = $db->q("
	INSERT INTO payment 
	(created_stamp, account_id, author_id, amount, recurring_amount, recurring_period, email, result, ip_address)
	VALUES
	(?, ?, ?, ?, ?, ?, ?, ?, ?)",
	[$now, $account->getId(), $account->getId(), $total, $total, $period, $account->getEmail(), "I", $ip_address]
	);
$payment_id = $db->insertid($res);
if (!$payment_id) {
	reportAdmin("AS: businessowner/payment error B1", "Cant create payment in db!");
	system::moved("/businessowner/pick?section={$section}&id={$id}&error=".urlencode("Server error B1, please contact us at support@adultsearch.com"));
}

$res = $db->q("
	INSERT INTO payment_item
	(payment_id, type, place_id, place_sponsor, created_stamp)
	VALUES
	(?, ?, ?, ?, ?)",
	[$payment_id, "businessowner", $id, $sponsor, $now]
	);
$pi_id = $db->insertid($res);
if (!$pi_id) {
	reportAdmin("AS: businessowner/payment error B2", "Cant create payment item in db!");
	system::moved("/businessowner/pick?section={$section}&id={$id}&error=".urlencode("Server error B2, please contact us at support@adultsearch.com"));
}

return system::go("/payment/pay?id={$payment_id}");

?>
