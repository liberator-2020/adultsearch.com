<?php

require(_CMS_ABS_PATH."/_cms_files/businessowner/common.php");

global $db, $smarty, $config_site_url;

$premium = "You will be listed first in the hierarchy of listings.You will be able to edit your listing and add special promotions/events, pictures, videos, coupons & all your correct business information.";
$normal = "You will be able to edit your listing and add special promotions/events, pictures, videos, coupons & all your correct business information.";

$id = intval($_REQUEST["id"]);
if (!$id)
	system::moved("/");

//fetch place from db
$res = $db->q("select * from place where place_id = ? AND edit >= 1", [$id]);
if (!$db->numrows($res)) {
	debug_log("businessowner/pick error - cant find place, id='{$id}'");
	return system::go("/businesswoner/getstarted");
}
$row = $db->r($res);

//check if place does not already have owner or sponsor
if ($row["sponsor"] == 1)
	system::go("/dir/place?id=".$id, "This place is already advertising...");
if ($row["owner"] == 1)
	system::go("/dir/place?id=".$id, "This place is already owned by another user.");

//submission
if (isset($_POST["pay"])) {
	$p = intval($_POST["p"]);
	if (!$p || !in_array($p, [1,2,3]))
		$smarty->assign("pick", true);
	else
		system::moved($config_site_url."/businessowner/payment?id={$id}&p={$p}");
}

//price depends on the type of place
$premium_year = "";
if (in_array($row["place_type_id"], [1,15])) {
	//stripclub and EMP have higher prices
	$normal .= " <b>$".SECTION_SCEMP_NORMAL."/year</b>";
	$premium .= " <b>$".SECTION_SCEMP_PREMIUM."/month</b>";
	$premium_year = " <b>$".SECTION_SCEMP_PREMIUM_YEAR."/year</b>";
} else {
	//default prices
	$normal .= " <b>$".SECTION_DEFAULT_NORMAL."/year</b>";
	$premium .= " <b>$".SECTION_DEFAULT_PREMIUM."/month</b>";
	$premium_year = " <b>$".SECTION_DEFAULT_PREMIUM_YEAR."/year</b>";
}

$smarty->assign("place", $row);		
$smarty->assign("option_normal", $normal);
$smarty->assign("option_premium", $premium);
$smarty->assign("option_premium_year", $premium_year);

if ($_REQUEST["error"] == 1)
	$smarty->assign("error", true);

$smarty->display(_CMS_ABS_PATH."/templates/businessowner/pick.tpl");
?>
