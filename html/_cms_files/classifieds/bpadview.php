<?php
/**
 * Marks impression for BP promo ads and outputs 1px transparent gif
 */
defined("_CMS_FRONTEND") or die("No Access");

//mark impression
$asl_id = intval($_REQUEST["id"]);
if (!$asl_id) {
	//debug_log("BP: View Error: No id specified! ref=".$_SERVER["HTTP_REFERER"]);
} else {
	$ret = backpage::check_view($asl_id);
	if (!$ret)
		debug_log("BP: View Error: check_view() failed for id '{$asl_id}' !");
}

//display 1px transparent gif
ob_clean();
header('Content-Type: image/gif');
echo base64_decode('R0lGODlhAQABAJAAAP8AAAAAACH5BAUQAAAALAAAAAABAAEAAAICBAEAOw==');

die;
?>
