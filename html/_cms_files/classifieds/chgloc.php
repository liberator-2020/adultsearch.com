<?php

global $db, $account;

$_REQUEST["template"] = "blank.tpl";

//for classifieds/move page we display all cities with place or ad (not just bp cities like on adbuild wizard)
$move = false;
if ($_REQUEST["move"] == 1)
	$move = true;

if( isset($_GET["states"]) ) {
	$countries = classifieds::getStates(intval($_GET["state"]));
	$option = "";
	foreach ($countries as $country) {
		$option .= (!empty($option)) ? "<option disabled=\"disabled\"></option>" : "<option value=\"\">Choose country/state</option>";
		$option .= "<option disabled=\"disabled\">{$country["name"]}</option>";
		foreach ($country["list"] as $state) {
			$sel = ($state["selected"]) ? " selected=\"selected\"" : "";
			$option .= "<option value=\"{$state["loc_id"]}\"{$sel}>{$state["loc_name"]}</option>";
		}
	}
	echo $option;
	die;
}

$cities = classifieds::getCities(intval($_GET["parent_id"]), intval($_GET["city"]), true, $move);
$option = "";
foreach ($cities[intval($_GET["parent_id"])] as $city) {
	$sel = ($city["selected"]) ? " selected=\"selected\"" : "";
	$option .= "<option value=\"{$city["loc_id"]}\"{$sel}>{$city["loc_name"]}</option>";
}
if (empty($option))
	die;
$option = "<option value=\"\">-- cities --</option>" . $option;

echo $option;
die;

?>
