<?php
/**
 * This handles claiming of BP classified ad by some user
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $account, $smarty;
$system = new system;

if (permission::has("classified_manage") && isset($_GET['set'])) {
	//this part is used as ajax request from classified/manage admin page
	$clad = clad::findOneById(intval($_GET["set"]));
	if (!$clad)
		die;
	$ret = $clad->getClaimUrl();
	if ($ret === false) {
		die("This ad is owned by someone");
	}
	die("Here is the link to update/remove panel of the requested ad: \n\n{$ret}");
	
}

if (!isset($_GET['id']) || !isset($_GET['hash']))
	$system->go("/", "Wrong URL");

$clad = clad::findOneById(intval($_GET["id"]));
if (!$clad)
	die("Can't find classified ad!");
if ($clad->getAccountId() != 0)
	$system->go("/", "This classified ad is owned by an other account.");


$hash = GetGetParam("hash");
if ($clad->getHash() != $hash)
	$system->go("/", "Wrong security code.");

$account_id = $account->isloggedin();
if ($account_id) {
	//claim ad by currently logged in user
	$clad->claim();
	$system->moved("/classifieds/myposts");
} elseif (!empty($_POST['email'])) {
	$email = GetPostParam("email");
	$username = GetPostParam("username");
	$password = GetPostParam("password");
	$error = NULL;
	$account_id = $account->auto_register($email, $username, $password, 1, $error);
	if ($account_id > 0) {
		//claim ad by just registered user
		$acc = account::findOneById($account_id);
		if (!$acc) {
			reportAdmin("AS: classifieds/claim: Error", "Cant find account by id returned from auto_register", array("account_id" => $account_id));
			die("Error while auto registering, please contact administrator!");
		}
		$clad->claim($acc);

		$system->go("/classifieds/myposts", "You are now being redirected to control panel.");
	} else $smarty->assign("error", $error);
	$smarty->assign("email", $email);
	$smarty->assign("username", $username);
	$smarty->assign("password", $password);
}

$smarty->display(_CMS_ABS_PATH."/templates/classifieds/classifieds_claim.tpl");
?>
