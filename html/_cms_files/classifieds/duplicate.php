<?php
defined('_CMS_FRONTEND') or die('Restricted access');

require_once(_CMS_ABS_PATH."/_cms_files/classifieds/array.php");

global $db, $smarty, $account, $config_site_url;

if (!account::ensure_loggedin())
	die("Invalid access");
$account_id = $account->getId();

$form = new form;

$id = intval($_GET['id']);
if( !$id )
	die;

$type = isset($_GET['type'])?intval($_GET['type']):NULL;

if (!$type && !in_array($type_array_current, $type)) {
	$smarty->assign("id", $id);
	$smarty->assign("type", $form->select_add_select() . $form->select_add_array($type_array_current, ""));
	$smarty->display(_CMS_ABS_PATH."/templates/classifieds/classifieds_duplicate.tpl");
	return;
}

if ($account->isrealadmin()) {
	//real admin can make exact duplicate of other ads ...
	$res = $db->q("select * from classifieds where id = '$id'");
	$sql_admin = ", time_next, thumb, incall_rate_hh, incall_rate_h, incall_rate_2h, incall_rate_day, outcall_rate_hh, outcall_rate_h, outcall_rate_2h, outcall_rate_day, visiting_from, visiting_to, expires";
	$sql_admin .= ", avl_men, avl_women, avl_couple, avl_group, avl_black, fetish_dominant, fetish_submissive";
	$done = "done";
	$done_loc = "done";
	//TODO update home_cache !!!
} else {
	$res = $db->q("select * from classifieds where id = '$id' and account_id = '$account_id'");
	$done = "-1";
	$done_loc = "0";
}
if( !$db->numrows($res) )
	die;	//Ad not found (it is not our ad or ad does not exist)

$row = $db->r($res);

$res = $db->q("INSERT INTO classifieds 
(state_id, type, loc_id, loc_name, account_id, expires, ethnicity, email, website, reply, title, location, 
date, content, incall, outcall, available, 
incall_rate, outcall_rate, ter, age, pornstar, payment_visa, payment_amex, payment_dis, review, 
haircolor, eyecolor, pregnant, visiting, tantra, phone, cc_id, w, auto_renew_fr, auto_renew_time, 
done, local, total_loc, market, firstname, middlename, lastname
{$sql_admin}
) 

(select state_id, '$type', loc_id, loc_name, account_id, SUBDATE(NOW(),1), ethnicity, email, website, reply, title, location, 
now(), content, incall, outcall, available, 
incall_rate, outcall_rate, ter, age, pornstar, payment_visa, payment_amex, payment_dis, review, 
haircolor, eyecolor, pregnant, visiting, tantra, phone, cc_id, w, auto_renew_fr, auto_renew_time, 
{$done}, local, total_loc, market, firstname, middlename, lastname 
{$sql_admin}
FROM classifieds 
WHERE id = '$id' 
LIMIT 1)");

$post_id = $db->insertid;
if( !$post_id ) {
	reportAdmin("AS: classifieds/duplicate failed", "", array("type" => $type, "done" => $done, "sql_admin" => $sql_admin, "id" => $id));
	die('Error, our technical team has been notified about this problem.');
}

$db->q("INSERT INTO classifieds_loc 
		(state_id, loc_id, neighbor_id, type, post_id, updated, done) 
		(select state_id, loc_id, neighbor_id, '$type', '$post_id', now(), '{$done_loc}' from classifieds_loc where post_id = '$id')");

$res = $db->q("select * from classifieds_image where id = '$id' order by image_id");
while($row=$db->r($res)) {
	$text = $post_id .'_'.system::_makeText(20);
	@copy(classifieds::getUploadDir().$row['filename'], classifieds::getUploadDir().$text);
	@copy(classifieds::getUploadDir()."/t/".$row['filename'], classifieds::getUploadDir()."/t/".$text);
	$db->q("insert into classifieds_image (id, filename, x, width, height) values ('$post_id', '$text', '{$row['x']}', '{$row['width']}', '{$row['height']}')");
}

system::go($config_site_url."/adbuild/step2?ad_id={$post_id}", "You are now being redirected to location selection page for the new ad.");
die;
?>
