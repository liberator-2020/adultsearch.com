<?

defined('_CMS_FRONTEND') or die('Restricted access');

require_once(_CMS_ABS_PATH."/_cms_files/classifieds/array.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.classifieds.php");

global $db, $smarty, $account;
$system = new system;
$classifieds = new classifieds;

if (!($account_id=$account->isLoggedIn())) {
	$account->asklogin();
	return;
}

$res = $db->q("select c.id, c.type, c.title, c.thumb, l.loc_url, l.country_sub, l.loc_url loc_name from classifieds c inner join classifieds_loc cl on c.id = cl.post_id 
inner join location_location l on cl.loc_id = l.loc_id where account_id = '$account_id' and total_loc < 5");
if( !$db->numrows($res) ) $system->moved("/");
while($row=$db->r($res)) {

	$module = $classifieds->getModuleByType($row["type"]);

	$sub = !empty($row['country_sub']) ? "{$row['country_sub']}." : "";
	$cl[] = array("id"=>$row["id"], "title"=>$row["title"], "thumb"=>$row["thumb"], "module"=>$module, "loc_name"=>$row["loc_name"], 
"gLocation"=>$row["loc_url"], "csub"=>$sub);
} 

$smarty->assign("classifieds", $cl);
$smarty->display(_CMS_ABS_PATH."/templates/classifieds/classifieds_free_ad.tpl");

?>
