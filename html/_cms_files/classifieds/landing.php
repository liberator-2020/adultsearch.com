<?php
/**
 * Landing page for showing classifieds
 */
defined("_CMS_FRONTEND") or die("No Access");

global $config_site_url, $db, $mobile;

//get ip address
if (isset($_REQUEST["ip"]))
	$ip_address = preg_replace('/[^A-Fa-f0-9\.:]/', '', $_REQUEST["ip"]);
else
	$ip_address = preg_replace('/[^A-Fa-f0-9\.:]/', '', account::getRealIp());

$go_url = null;

$cat_url = preg_replace('/[^a-z\-]/', '', $_REQUEST["cat"]);	// e.g. female-escorts, body-rubs, ...
if (!$cat_url)
	$cat_url = "female-escorts";

//geolocate by IP address
$result = geolocation::getLocationByIp($ip_address);
if ($result["city_url"]) {
	$go_url = $result["city_url"]."{$cat_url}";
}
if (!$go_url && $result["country_url"]) {
	$go_url = $result["country_url"]."/{$cat_url}";
}

if (!$go_url)
	$go_url = $config_site_url;	//couldnt geolocate -> redirect to homepage

//log into analytics
$ga_campaign = (array_key_exists("utm_campaign", $_REQUEST)) ? $_REQUEST["utm_campaign"] : null;
$url = (array_key_exists("REQUEST_URI", $_SERVER)) ? $_SERVER["REQUEST_URI"] : null;
$referer = (array_key_exists("HTTP_REFERER", $_SERVER)) ? $_SERVER["HTTP_REFERER"] : null;
$db->q("INSERT INTO analytics 
        (stamp, day, ref, url, referer, ip, forward_url, mobile) 
        VALUES 
        (?, ?, ?, ?, ?, ?, ?, ?)",
        [time(), date("Y-m-d"), $ga_campaign, $url, $referer, $ip_address, $go_url, intval($mobile)]
        );

system::go($go_url);
die;
?>
