<?php
/**
 * Quick mass change of locations for posted ad for admin
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;
$system = new system;

if (!($account_id=$account->isLoggedIn())) {
	$account->asklogin();
	return;
}

//only for admin
if (!$account->isadmin()) {
	$system->moved("/");
	die;
}

$id = intval($_REQUEST["id"]);
if (!$id) {
	$system->moved("/");
	die;
}

$clad = clad::findOneById($id);
if (!$clad) {
	$system->moved("/", "Ad #{$id} not found/");
	die;
}

$ref = NULL;
if (isset($_REQUEST["ref"])) {
	$ref = htmlspecialchars($_REQUEST["ref"]);
	$smarty->assign("ref", $ref);
}

if (isset($_POST["submit"])) {

	if($_POST["loc_id"])
		$loc_ids = $_POST["loc_id"];

	if (!empty($loc_ids)) {
		_d("set");
		$db->q("DELETE FROM classifieds_loc WHERE post_id = ?", array($id));
		foreach($loc_ids as $loc_id) {
			$db->q("INSERT INTO classifieds_loc 
					(loc_id, type, state_id, post_id, updated, done) 
					values 
					(?, ?, (select loc_parent from location_location where loc_id = ?), ?, now(), ?)", array($loc_id, $clad->getType(), $loc_id, $id, $clad->getDone()));
		}
		$total_loc = count($loc_ids);
		$db->q("UPDATE classifieds SET total_loc = ? WHERE id = ?", array($total_loc, $id));

		flash::add(flash::MSG_SUCCESS, "Locations for ad #{$id} have been saved (total {$total_loc} locations).");
		if ($ref)
			$system->moved($ref);
		else
			$system->moved($clad->getUrl());
	} else {
		$smarty->assign("error", "Please select at least one location.");
	}
}

$posted_in = array();
$rex = $db->q("SELECT loc_id FROM classifieds_loc WHERE post_id = ?", array($id));
while ($rox = $db->r($rex)) {
	$posted_in[] = $rox["loc_id"];
}

//select states/international countries for top cities
$sql = "SELECT l.loc_name as real_name, l.loc_parent, t.loc_id, t.loc_name, t.s, t.live_ad, t.hasN, t.big 
		FROM location_topcities t 
		INNER JOIN location_location l on t.state_id = l.loc_id
		ORDER BY l.loc_parent, l.loc_name, t.loc_name";
$res = $db->q($sql);
$statex = "";
$totalcity = 50;
$locsx = NULL;
$skip = 0;
$us = $canada = $uk = NULL;
$addlast = false;
while($row=$db->r($res)) {
	$bold = $row['big'] ? true : false;

	//for new york city select boroughs
	if( $row['hasN'] ) {
		$rex = $db->q("select t.loc_id, t.loc_name, t.s, t.big FROM location_topcities t where t.state_id = '{$row['loc_id']}' order by loc_name");
		while($rox=$db->r($rex)) {
			$bolx = $rox['big'] ? true : false;
			$locsx[] = array(
				"loc_id" => $rox["loc_id"],
				"loc_name" => "{$rox["s"]} - {$rox["loc_name"]}", 
				"s" => $rox["s"], 
				"bold" => $bolx,
				"checked" => (in_array($rox["loc_id"], $posted_in)) ? true : false
				);
		}
		continue;
	}

	if ($statex != $row["real_name"]) {
		//state changed
		if( $locsx ) {
			//state is different than before, lets store all locs is current state to proper array
			if( $loc_parent == 16046)
				$us[] = array("real_name"=>$real_name, "city"=>$locsx);
			else if( $loc_parent == 16047 )
				$canada[] = array("real_name"=>$real_name, "city"=>$locsx);
			else if( $loc_parent == 41973 )
				$uk[] = array("real_name"=>$real_name, "city"=>$locsx);
			else {
				//TODO we dont add other countries than US,Canada and UK
			}
			$addlast = false;
		} else
			//very first entry
			$addlast = true;

		$statex = $real_name = $row["real_name"];
		$locsx = NULL;
		$locsx[] = array(
			"loc_id" => $row["loc_id"],
			"loc_name" => $row["loc_name"], 
			"s" => $row["s"], 
			"bold" => $bold,
			"checked" => (in_array($row["loc_id"], $posted_in)) ? true : false
			);
		if ($loc_parent == 16046)
			$totalcity++;
	} else {
		$skip = 0;
		$locsx[] = array(
			"loc_id" => $row["loc_id"],
			"loc_name" => $row["loc_name"], 
			"s" => $row["s"], 
			"bold" => $bold,
			"checked" => (in_array($row["loc_id"], $posted_in)) ? true : false
		);
		if ($loc_parent == 16046)
			$totalcity++;
		$loc_parent = $row['loc_parent'];
		$real_name = $row["real_name"];
		$addlast = true;
	}
}

//last 
if( $locsx ) {
	if( $loc_parent == 16046 )
		$us[] = array("real_name"=>$real_name, "city"=>$locsx);
	else if( $loc_parent == 16047 )
		$canada[] = array("real_name"=>$real_name, "city"=>$locsx);
	else if( $loc_parent == 41973 )
		$uk[] = array("real_name"=>$real_name, "city"=>$locsx);
}

//canada and uk are splitted by number of states
$canada = array_chunk($canada, ceil(count($canada)/4));
$uk = array_chunk($uk, ceil(count($uk)/4));

//us are splitted by number of cities
$loc_per_column = ceil($totalcity/4);
$x = $y = 0;
foreach($us as $l) {
	$x += count($l["city"]);
	$array = (int)($x / $loc_per_column);
	if( !count($locs2[$array]) && $y ) {
		$x += ($array*$loc_per_column>0?$array*$loc_per_column:$loc_per_column) - $y;
	}
	$y = $x;
	$locs2[$array][] = $l;
}
$smarty->assign("us", $locs2);

$list[] = array("active"=>1, "name"=>"US", "title"=>"United States", "list"=>$locs2);
$list[] = array("active"=>0, "name"=>"CA", "title"=>"Canada", "list"=>$canada);
$list[] = array("active"=>0, "name"=>"UK", "title"=>"United Kingdom", "list"=>$uk);

$smarty->assign("list", $list);
$smarty->assign("type", $type);
$smarty->display(_CMS_ABS_PATH."/templates/classifieds/classifieds_locations.tpl");

?>
