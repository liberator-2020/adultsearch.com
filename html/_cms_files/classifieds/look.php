<?php

defined("_CMS_FRONTEND") or die("No Access");

global $db, $config_site_url;
$system = new system;

$id = intval(GetGetParam("id"));

if (!$id)
	return "Error: no classified ad id specified!";

$res = $db->q("SELECT c.type, cl.loc_id
	FROM classifieds c 
	INNER JOIN classifieds_loc cl ON c.id = cl.post_id
	WHERE c.id = ?
	LIMIT 1", array($id));

if (!$db->numrows($res)) {
	//ad not found, check redirect table
	$res = $db->q("select new_id from classifieds_redirect where id = ?", array($id));
	if( $db->numrows($res) ) {
		$row = $db->r($res);
		$db->q("update classifieds_redirect set time = now() where id = '$id'");
		$db->q("delete from classifieds_redirect where time < date_sub(now(), interval 90 day)");
		header("Location: ".$config_site_url."/classifieds/look?id={$row[0]}", true, "301");
		die;
	}
	return "Error: Cant find this classified ad!";
}
$row = $db->r($res);

$url = classifieds::getUrl($id, $row["type"], $row["loc_id"]);
if ($url === false)
	return "Error: Cant find URL of this classified ad!";

$system->moved($url);

?>
