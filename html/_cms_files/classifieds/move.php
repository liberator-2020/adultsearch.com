<?php
/**
 * Move clasified ad to different location, or remove from one of the locations
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

if (!account::ensure_loggedin())
    die("Invalid access");
$account_id = $account->getId();


//get id of the classified ad andverify permissions
$id = isset($_REQUEST["id"]) ? intval($_REQUEST["id"]) : NULL;
if (!$id)
	system::go("/");

$clad = clad::findOneById($id);
if (!$clad || $clad->getDeleted())
	system::go("/classifieds/", "This classified ad has been removed.");

if ($clad->hasActiveSticky())
	system::go("/classifieds/myposts", "Classified ad #{$id} has active sticky upgrade and therefore you can't change location");

if (!permission::has("classified_manage")) {
	if ($clad->getAccountId() && $clad->getAccountId() != $account_id)
		system::moved("/classifieds/look?id=".$id);
	else if (!$clad->getAccountId() && !isset($_SESSION["cl_owner_".$id]))
		system::moved("/classifieds/look?id=".$id);
}

function move_ad($clad, $from, $city) {
	global $db, $smarty, $account;

	if (!$from) {
		$smarty->assign("error", "You did not pick which location do you want to move your ad from.");
		return false;
	}

	if (!$city) {
		$smarty->assign("error", "You did not pick the city that you want to move your ad to");
		return false;
	}

	//check if location is valid
	$res = $db->q("SELECT loc_name, loc_parent FROM location_location WHERE loc_id = ?", [$city]);
	if ($db->numrows($res) != 1) {
		$smarty->assign("error", "Invalid location");
		return false;
	}
	$row = $db->r($res);
	$loc_parent = $row["loc_parent"];
	$loc_name = $row["loc_name"];
	if (!$loc_parent || !$loc_name) {
		$smarty->assign("error", "Invalid location");
		return false;
	}

	if ($city == $from) {
		$smarty->assign("error", "You tried to move your ad to the location its already posted in.");
		return false;
	}

	//check if we already have this ad in desired location
	$res = $db->q("SELECT id FROM classifieds_loc WHERE post_id = ? AND loc_id = ?", [$clad->getId(), $city]);
	if ($db->numrows($res) > 0) {
		$smarty->assign("error", "You tried to move your ad to the location its already posted in.");
		return false;
	}

	//check if ad post price in new city is less or equal as former
	$from_price = classifieds::getPostPriceForLocation($from);
	$to_price = classifieds::getPostPriceForLocation($city);

	if (!$account->isadmin() && (($from_price + 0.01) <= $to_price)) {
		$smarty->assign("error", "Old location cost was \${$from_price} and new location cost is \${$to_price}. You can't move ad to more expensive location.");
		return false;
	}

	//everything ok, move location
	$db->q("UPDATE classifieds SET loc_id = ?, state_id = ?, loc_name = ? WHERE id = ?", [$city, $loc_parent, $loc_name, $clad->getId()]);
	$db->q("UPDATE classifieds_loc SET loc_id = ?, state_id = ?, neighbor_id = 0 WHERE post_id = ? AND loc_id = ? LIMIT 1",
		[$city, $loc_parent, $clad->getId(), $from]
		);

	if (classifieds::locationHasFreeSlotCityThumbnail($city))
		$db->q("UPDATE classifieds_sponsor SET loc_id = ?, state_id = ? WHERE post_id = ? AND loc_id = ?", [$city, $loc_parent, $clad->getId(), $from]);

	if (classifieds::locationHasFreeSlotSideSponsor($city, $clad->getType()))
		$db->q("UPDATE classifieds_side SET loc_id = ?, state_id = ? WHERE post_id = ? AND loc_id = ?", [$city, $loc_parent, $clad->getId(), $from]);

	if (classifieds::locationHasFreeSlotSticky($city, $clad->getType()))
		$db->q("UPDATE classified_sticky SET loc_id = ? WHERE classified_id = ? AND loc_id = ?", [$city, $clad->getId(), $from]);

	audit::log("CLA", "MoveLoc", $clad->getId(), "From:{$from},To:{$city}", $account->getId());
	system::go("/classifieds/myposts", "Your ad is moved. It might take up to 2 minutes to reflect change on our servers.");
}

if (isset($_POST['remove']) && $account->isadmin()) {
	$loc_id = intval($_POST['remove']);
	if( $loc_id ) {
		$db->q("DELETE FROM classifieds_loc WHERE loc_id = ? AND post_id = ? LIMIT 1", [$loc_id, $id]);
		$db->q("UPDATE classifieds SET total_loc = (select count(*) from classifieds_loc where post_id = ?) WHERE id = ?", [$id, $id]);
		audit::log("CLA", "RemoveLoc", $id, "Loc:{$loc_id}", $account_id);
		echo "<p>Removed.</p>";
	}

} else if( isset($_POST['add']) && $account->isadmin() ) {
	$loc_id = intval($_POST['City']);
	if( $loc_id ) {
		echo "<p>Added.</p>";
		$db->q("INSERT INTO classifieds_loc
				(state_id, loc_id, neighbor_id, type, post_id, updated, done) 
				values 
				((select loc_parent from location_location where loc_id = ?), ?, 0, ?, ?, ?, ?)"
				, [$loc_id, $loc_id, $clad->getType(), $id, $clad->getDate(), $clad->getDone()]
				);
		$db->q("UPDATE classifieds SET total_loc = (select count(*) from classifieds_loc where post_id = ?) WHERE id = ?", [$id, $id]);
		audit::log("CLA", "AddLoc", $id, "Loc:{$loc_id}", $account_id);
	}

} else if( isset($_POST['from']) ) {
	//move classified ad location
	$from = intval($_POST['from']);
	$city = intval($_POST['City']);
	move_ad($clad, $from, $city);
}

$res = $db->q("SELECT t.loc_id, l.state_id, concat(t.loc_name, ', ', t.s) as loc
				FROM classifieds_loc l
				INNER JOIN location_location t using (loc_id)
				WHERE l.post_id = ?
				ORDER BY s asc, t.loc_name asc",
				[$id]
				);
$default_loc_id = $default_state_id = null;
while($row=$db->r($res)) {
	$loc_id = $row["loc_id"];
	$state_id = $row["state_id"];
	$loc_label = $row["loc"];

	$current_loc[] = array(
		"loc_id"=>$row[0],
		"loc_name"=>$row[2]
		);

	$current_loc_options[$loc_id] = $loc_label;

//	if (!$default_loc_id)
//		$default_loc_id = $loc_id;
	if (!$default_state_id)
		$default_state_id = $state_id;
}

$smarty->assign("nobanner", true);

$smarty->assign("loc_id", $default_loc_id);
$smarty->assign("state_id", $default_state_id);
$smarty->assign("current_loc", $current_loc);
$smarty->assign("current_loc_options", $current_loc_options);
$smarty->assign("from", (intval($_REQUEST["from"])) ?: null);
$smarty->display(_CMS_ABS_PATH."/templates/classifieds/classifieds_move.tpl");

?>
