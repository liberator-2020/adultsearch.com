<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $config_site_url, $config_image_server;
$gTitle = "Manage Your Classified Ads";

if (!account::ensure_loggedin())
    die("Invalid access");

$account_id = $account->getId();

//user remove ad handler
if ($_GET["remove"]) {
	$id = intval($_GET["remove"]);
	$clad = clad::findOneById($id);
	if (!$clad || $clad->getDone() == -2) {
		system::go("classifieds/myposts");
		die;
	}
	$res = $db->q("select id from classifieds where id = ? AND account_id = ?", array($id, $account_id));
	if (!$db->numrows($res)) {
		$res = $db->q("select * from classifieds where id = ?", array($id));
		$row = $db->r($res);
		$text = "account $account_id tried to remove $id";
		foreach($row as $key=>$value) {
			if( !is_numeric($key) ) $text .= "$key: $value<br>";
		}
		reportAdmin("AS: myposts-remove hack", $text, array("account_id" => $account_id, "clad_id" => $id));
		system::go("classifieds/myposts");
		die;
	}

	//mark ad as requested to delete
	$db->q("UPDATE classifieds SET done = -2 WHERE id = ?", [$id]);
	$db->q("UPDATE classifieds_loc SET done = -2 WHERE post_id = ?", [$id]);
	$db->q("UPDATE classifieds_sponsor SET done = -2 WHERE post_id = ?", [$id]);
	$db->q("UPDATE classifieds_side SET done = -2 WHERE post_id = ?", [$id]);
	$db->q("UPDATE classified_sticky SET done = -2 WHERE classified_id = ?", [$id]);
	$ip = account::getUserIp();
	$db->q("insert into classifieds_removed 
			(id, time, account_id, data, ip_address) 
			values 
			(?, NOW(), ?, 'marked to be removed through my ads', ?)",
			array($id, $account_id, $ip)
			);
	audit::log("CLA", "RemoveRequest", $id, "", $account->getId());

	$clad->deleteAdCancelSubscriptions();

	system::go("classifieds/myposts");
}

//get some quick count stats
$res = $db->q("
	SELECT SUM(IF(c.done = 1, 1, 0)) as count_live, SUM(IF(c.done = 0, 1, 0)) as count_expired, COUNT(*) as count_total
	FROM classifieds c
	WHERE c.account_id = ? AND c.deleted IS NULL AND c.done >= 0
	",
	[$account_id]
	);
$row = $db->r($res);
$smarty->assign("count_live", $row["count_live"]);
$smarty->assign("count_expired", $row["count_expired"]);
$smarty->assign("count_total", $row["count_total"]);

$smarty->assign("repost", $account->getRepost());

//in Dallas and Houston display eccie link we arecommend to advertise on
$eccieLinkDisplay = false;

$res = $db->q(
		"SELECT e.*, 
			date_format(date, '%m/%d/%Y %h:%i %p') updated, 
			DATE_FORMAT(`expires`, '%b %D %Y %l:%i %p') as expire, 
			unix_timestamp(expires) as expires_unix,
			date_format(from_unixtime(time_next), '%m/%d/%Y %h:%i %p') timenext, 
			date_format(date_add(curdate(), interval auto_renew day), '%m/%d/%Y') nextbill, 
			(select filename from classifieds_image where id = e.id order by image_id limit 1) picture 
		FROM classifieds e 
		WHERE account_id = ? and done >= 0 AND deleted IS NULL
		ORDER BY date desc",
		[$account_id]
		);
$escorts = [];
$count = $db->numrows($res);
while ($row = $db->r($res)) {

	$clad = clad::withRow($row);
	if (!$clad)
		continue;

	$loc_labels = $clad->getLocLabels();
	$total_loc = count($loc_labels);
	if ($total_loc > 1)
		$location = "Multiple";
	else if ($total_loc == 1)
		$location = array_shift($loc_labels);
	else
		$location = "NO Location!";

	if ($location == "Houston, TX" || $location == "Dallas, TX")
		$eccieLinkDisplay = true;

	switch($row['market']) {
		case 1: $market = "Small Markets"; break;
		case 2: $market = "Mix Markets"; break;
		case 3: $market = "Big Markets"; break;
		case 4: $market = "Mixed Small Markets"; break;
		default: $market = "";
	}

	$thumb = $row['thumb'] ? $config_image_server."/classifieds/{$row['thumb']}" : $config_site_url."/images/placeholder.gif";

	//get sponsor info
	$sponsor_done = $sponsor_expire_stamp = $sponsor_expire_datetime = null;
	$rex = $db->q("SELECT done, expire_stamp
					FROM classifieds_sponsor 
					WHERE post_id = ?
					LIMIT 1", array($row['id']));
	if ($db->numrows($rex)) {
		$rox = $db->r($rex);
		$sponsor_done = $rox["done"];
		$sponsor_expire_stamp = $rox["expire_stamp"];
		$sponsor_expire_datetime = date("m/d/Y H:i T", $sponsor_expire_stamp);
	}

	//get side info
	$side_done = $side_expire_stamp = $side_expire_datetime = null;
	$rex = $db->q("SELECT done, expire_stamp
					FROM classifieds_side 
					WHERE post_id = ?
					LIMIT 1", array($row['id']));
	if ($db->numrows($rex)) {
		$rox = $db->r($rex);
		$side_done = $rox["done"];
		$side_expire_stamp = $rox["expire_stamp"];
		$side_expire_datetime = date("m/d/Y H:i T", $side_expire_stamp);
	}

	//get sticky info
	$sticky_done = $sticky_expire_stamp = $sticky_expire_datetime = null;
	$rex = $db->q("SELECT done, expire_stamp
					FROM classified_sticky
					WHERE classified_id = ?
					LIMIT 1", array($row['id']));
	if ($db->numrows($rex)) {
		$rox = $db->r($rex);
		$sticky_done = $rox["done"];
		$sticky_expire_stamp = $rox["expire_stamp"];
		$sticky_expire_datetime = date("m/d/Y H:i T", $sticky_expire_stamp);
	}

	$escorts[] = array(
		"id" => $row['id'],
		"filename" => $row['filename'] . ".htm",
		"name" => $row['title'],
		"location" => $location,
		"cat_id" => $row['cat_id'],
		"type" => $row["type"],
		"firstname"=> $row["firstname"],
		"category"=> classifieds::getcatnamebytype($row["type"]),
		"expire" => $row['expire'],
		"visit" => intval($row['visit']),
		"paid" => $row["paid"],
		"status" => $row["done"] == 1 ?  "Live" : ( $row["done"] == 0 ? "Expired" : ($row["done"]==-2?"Removed By You":"Payment Needed")),
		"local"=> $row["local"],
		"auto_renew"=> $row["auto_renew"],
		"time_next"=> $row["timenext"],
		"updated"=> $row["updated"],
		"done"=>$row["done"],

		"sponsor" => $sponsor_done,
		"sponsor_expires" => $sponsor_expire_datetime,
		"sponsor_expires_proper" => ($sponsor_expire_stamp && $sponsor_done == 1) ? $db->makeTime($sponsor_expire_stamp) : NULL,

		"side" => $side_done,
		"side_expires" => $side_expire_datetime,
		"side_expires_proper" => ($side_expire_stamp && $side_done == 1) ? $db->makeTime($side_expire_stamp) : NULL,

		"sticky" => $sticky_done,
		"sticky_expires" => $sticky_expire_datetime,
		"sticky_expires_proper" => ($sticky_expire_stamp && $sticky_done == 1) ? $db->makeTime($sticky_expire_stamp) : NULL,

		"recurring"=>$row["recurring"],
		"nextbill"=>$row["nextbill"],
		"total_loc" => $total_loc,
		"market"=>$market,
		"thumb"=>$thumb,
		"expires_proper" => $db->makeTime($row["expires_unix"]),
		"time_next_proper" => $db->makeTime($row['time_next']),
		"picture"=>$row['picture']
	);
}

$smarty->assign("account_id", $account_id);
$smarty->assign("agency", $account->getAgency());
$smarty->assign("escorts", $escorts);
$smarty->assign("count", $count);
$smarty->assign('current', time());
$smarty->assign("nobanner", true);
$smarty->assign("eccieLinkDisplay", $eccieLinkDisplay);

$smarty->assign("budget", $db->single("SELECT budget FROM advertise_budget WHERE account_id = ? LIMIT 1", [$account_id]));

escortsbiz::exportEbizLink();
//check if we already have website
if ($account->getEbizId() && $account->getEbizSecret()) {
	$smarty->assign('ebiz_login_url', escortsbiz::getMyLoginUrl());
}

include_html_head("js", "/js/tools/tooltip.js");

$smarty->display(_CMS_ABS_PATH."/templates/classifieds/myposts.tpl");

?>
