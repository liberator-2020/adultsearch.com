<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

$smarty->assign("nobanner", true);

$ip_address = account::getUserIp();
$now = time();

if (!permission::access("classified_pay"))
	return;

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");


$live = ($_REQUEST["live"] == 1) ? true : false;
$ids = getActionIds();
$cnt = count($ids);
$smarty->assign("action_ids", $ids);

if ($cnt == 0) {
	echo "No classifieds selected!<br />";
	return true;
}


//---------------------------------------------------
// get info about ads and their current subscriptions
//	SELECT c.id, c.account_id, c.thumb, c.type, c.firstname, c.title
$res = $db->q("
	SELECT c.*, a.email as account_email
	FROM classifieds c
	INNER JOIN account a on a.account_id = c.account_id
	WHERE c.id IN (".implode(",",$ids).")
	GROUP BY c.id",
	[]
	);
if ($db->numrows($res) == 0)
	return actionError("Cant find classifieds in db !");

$acc = $account_id = $account_email = null;
$six_months_ago = time() - 86400*30*6;
global $clads;
while ($row = $db->r($res)) {
	$ai = $row["account_id"];
	if (!$account_id)
		$account_id = $ai;
	else if ($account_id != $ai)
		return actionError("All ads need to have the same account owner!");

	$account_email = $row["account_email"];

	$clad = clad::withRow($row);

	if (count($clad->getLocations()) > 10)
		return actionError("Too many locations, please contact administrator!");

	if (!$acc)
		$acc = $clad->getAccount();

	$row["payments"] = $clad->getLastPaymentsAndActiveSubscriptions();
	$row["locations"] = $clad->getLocations();
	$clads[] = $row;
}

function check_upgrade_availability(&$error) {
	global $clads;

	$error = "";
	
	foreach ($clads as $clad) {
		foreach ($clad["locations"] as $loc) {
			$loc_id = $loc->getId();

			if ($_REQUEST["sticky_{$clad["id"]}_{$loc->getId()}"] == "1" && !classifieds::locationHasFreeSlotSticky($loc->getId(), $clad["type"])) {
				//sticky not available in that location
				$error += " Sticky in {$loc->getLabel()} is not available !";
			}

			if ($_REQUEST["city_thumbnail_{$clad["id"]}_{$loc->getId()}"] == "1" && !classifieds::locationHasFreeSlotCityThumbnail($loc->getId())) {
				//city thumbnail not available in that location
				$error += " City thumbnail in {$loc->getLabel()} is not available !";
			}

			if ($_REQUEST["side_sponsor_{$clad["id"]}_{$loc->getId()}"] == "1" && !classifieds::locationHasFreeSlotSideSponsor($loc->getId(), $clad["type"])) {
				//side sponsor not available in that location
				$error += " Side sponsor in {$loc->getLabel()} is not available !";
			}
		}
	}

	if ($error)
		return false;

	return true;
}


//-----------
// submission
if ($live) {

	$error = null;
	$msg = "";
	$amount = floatval($_REQUEST["amount"]);
	$amount_recurring = floatval($_REQUEST["amount_recurring"]);

	if ($amount < 1) {
		$error = "Amount not specified";
	} else if (!check_upgrade_availability($error)) {
//		
	} else {
		
		//cancel subscriptions
		foreach ($clads as $clad) {
			foreach ($clad["payments"] as $p) {
				if ($p["subscription_status"] == 1) {
					//cancel this subscription
					$ret = payment::cancel($p["payment_id"], "upgrade", $error);
					if ($ret === false) {
						debug_log("classifieds/pay: Error while canceling subscription #{$p["payment_id"]}, error={$error}");
						reportAdmin("AS: classifieds/pay error", "Error while canceling subscription #{$p["payment_id"]}, error={$error}");
					}
					debug_log("classifieds/pay: Subscription #{$p["payment_id"]} canceled.");
					$msg .= " Subscription #{$p["payment_id"]} canceled.";
				}
			}
		}

		if (!$error) {

			//create payment and payment items
			$amount = number_format($amount, 2, ".", "");
			$amount_recurring = number_format($amount_recurring, 2, ".", "");

			$res = $db->q("
				INSERT INTO payment 
				(created_stamp, account_id, author_id, cc_id, p1, p2, p3, amount, recurring_amount, recurring_period, 
					promocode_id, email, sent_stamp, result, ip_address)
				VALUES
				(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 
					?, ?, ?, ?, ?)",
				[$now, $account_id, $account->getId(), null, null, null, null, $amount, $amount_recurring, 30,
				null, $account_email, null, "I", $ip_address]
			);
			$payment_id = $db->insertid($res);
			if (!$payment_id)
				return actionError("Cant create payment in db!");

			foreach ($clads as $clad) {
				$clad_id = $clad["id"];

				$sponsor_desktop = ($_REQUEST["sponsor_desktop_{$clad_id}"] == "1") ? 1 : null;
				$sponsor_mobile = ($_REQUEST["sponsor_mobile_{$clad_id}"] == "1") ? 1 : null;
				$sponsor_position = intval($_REQUEST["sponsor_position_{$clad_id}"]);
				$daily_repost = ($_REQUEST["recurring_{$clad_id}"] == "1") ? 1 : null;
				$auto_renew = intval($_REQUEST["auto_renew_{$clad_id}"]);
				if (!$auto_renew)
					$auto_renew = null;

				if (($sponsor_desktop || $sponsor_mobile) && !$sponsor_position) {
					reportAdmin("AS: clasisfieds/pay error", "Sponsor position not defined ! do proper checking in the code so were not creating emapty payments in db !");
					die("Sponsor position not defined !");
				}

				foreach ($clad["locations"] as $loc) {

					$city_thumbnail = ($_REQUEST["city_thumbnail_{$clad_id}_{$loc->getId()}"] == "1") ? 1 : null;
					$side = ($_REQUEST["side_{$clad_id}_{$loc->getId()}"] == "1") ? 1 : null;
					$sticky = ($_REQUEST["sticky_{$clad_id}_{$loc->getId()}"] == "1") ? 1 : null;

					$res = $db->q("
						INSERT INTO payment_item
						(payment_id, type, classified_id, classified_status, loc_id
						, auto_renew, daily_repost, sponsor, side, sticky, sticky_position, sponsor_desktop, sponsor_mobile, created_stamp)
						VALUES
						(?, ?, ?, ?, ?
						, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
						[$payment_id, "classified", $clad_id, "U", $loc->getId()
						, $auto_renew, $daily_repost, $city_thumbnail, $side, $sticky, null, $sponsor_desktop, $sponsor_mobile, $now]
					);
					$pi_id = $db->insertid($res);
					if (!$pi_id) {
						reportAdmin("AS: clasisfieds/pay error", "Cant create payment classified xlink in db!");
						die("Cant create payment classified xlink in db!, contact admin");
					}

				}

				if ($sponsor_desktop || $sponsor_mobile) {
					$res = $db->q(
						"UPDATE classifieds SET sponsor = ?, sponsor_mobile = ?, sponsor_position = ? WHERE id = ?",
						[$sponsor_desktop, $sponsor_mobile, $sponsor_position, $clad_id]
						);
				} else {
					$res = $db->q("UPDATE classifieds SET sponsor = 0, sponsor_mobile = 0, sponsor_position = 0 WHERE id = ?", [$clad_id]);
				}
			}

			return system::go("/payment/pay?id={$payment_id}");
		}
	}

}

$smarty->assign("city_thumbnail_price", classifieds::get_city_thumbnail_upgrade_price($acc));
$smarty->assign("side_sponsor_price", classifieds::get_side_upgrade_price($acc));

$smarty->assign("clads", $clads);

$smarty->assign("error", $error);

$art_array = get_time_array();
$smarty->assign("art_values", array_keys($art_array));
$smarty->assign("art_names", array_values($art_array));

$smarty->assign("action_hidden_inputs", getConfirmActionHiddenInputs($action_name, $ids));

return $smarty->display(_CMS_ABS_PATH."/templates/classifieds/pay.tpl");

?>
