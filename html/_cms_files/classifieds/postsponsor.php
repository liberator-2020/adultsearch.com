<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $account;
$system = new system;

$account_id = $account->isloggedin();

$id = intval($_REQUEST["id"]);

if (!$account->isadmin()) {
	if ($id)
		$system->moved("/classifieds/look?id=".intval($id));
	else
		$system->moved("/");
	die;
}

reportAdmin("AS: ./_cms_files/classifieds/postsponsor called", "AS: ./_cms_files/classifieds/postsponsor called", array());
$system->moved("/");
die;

?>
