<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $account, $db;

if (!$account->isAdmin())
    die("access denied");

$classifieds = new classifieds();

$clad_id = intval($_REQUEST["id"]);
if (!$clad_id) {
	echo "No clad_id !";
	return;
}

$res = $db->q("
	SELECT c.*
		, a.email as account_email, a.username as account_username
		, l.loc_id as l_id, l.loc_name as l_name
		, lp.loc_type as lp_type, lp.loc_id as lp_id, lp.loc_name as lp_name
		, lpp.loc_type as lpp_type, lpp.loc_id as lpp_id, lpp.loc_name as lpp_name
	FROM classifieds c 
	INNER JOIN account a on a.account_id = c.account_id
	INNER JOIN classifieds_loc cl on cl.post_id = c.id
	INNER JOIN location_location l on l.loc_id = cl.loc_id
	INNER JOIN location_location lp on lp.loc_id = l.loc_parent
	LEFT JOIN location_location lpp on lpp.loc_id = lp.loc_parent
	WHERE c.id = ?
	GROUP BY c.id
	",
	[$clad_id]
	);
if ($db->numrows($res) != 1) {
	echo "Can't find Ad #{$clad_id} !";
	return;
}
$row = $db->r($res);

$location_state = $location_country = null;
$location_city = $row["l_name"];
if ($row["lp_type"] == 2) {
	$location_state = $row["lp_name"];
	$location_country = $row["lpp_name"];
} else {
	$location_country = $row["lp_name"];
}

$expire_stamp = null;
if ($row["expire_stamp"])
	$expire_stamp = $row["expire_stamp"];
else if ($row["expires"])
	$expire_stamp = strtotime($row["expires"]);

$thumbnail_url = null;
if ($row["thumb"])
	$thumbnail_url = "{$config_image_server}/classifieds/{$row["thumb"]}";

$params = [
	"account_email" => $row["account_email"],
	"account_username" => $row["account_username"],
	"firstname" => $row["firstname"],
	"title" => $row["title"],
	"firstname" => $row["firstname"],
	"location_city" => $location_city,
	"location_state" => $location_state,
	"location_country" => $location_country,
	"email" => $row["email"],
	"phone" => $row["phone"],
	"content" => $row["content"],
	"thumb" => $thumbnail_url,
	"avl_men" => $row["avl_men"],
	"avl_women" => $row["avl_women"],
	"avl_couple" => $row["avl_couple"],
	"incall" => $row["incall"],
	"incall_rate_h" => $row["incall_rate_h"],
	"incall_rate_hh" => $row["incall_rate_hh"],
	"outcall" => $row["outcall"],
	"outcall_rate_h" => $row["outcall_rate_h"],
	"outcall_rate_hh" => $row["outcall_rate_hh"],
	"age" => $row["age"],
	"ethnicity" => $classifieds->adbuild_ethnicity[$row["ethnicity"]],
	"body_type" => $classifieds->adbuild_build[$row["build"]],
	"hair_color" => $classifieds->adbuild_haircolor[$row["hair_color"]],
	"eye_color" => $classifieds->adbuild_eyecolor[$row["eye_color"]],
	"cupsize" => $classifieds->adbuild_cupsize[$row["cupsize"]],
	"penis_size" => $classifieds->adbuild_penis_size[$row["penis_size"]],
	"height_feet" => $row["height_feet"],
	"height_inches" => $row["height_inches"],
	"weight" => $row["weight"],
	"measure_1" => $row["measure_1"],
	"measure_2" => $row["measure_2"],
	"measure_3" => $row["measure_3"],
	"gfe" => $row["gfe"],
	"pornstar" => $row["pornstar"],
	"website" => $row["website"],
	"facebook" => $row["facebook"],
	"instagram" => $row["instagram"],
	"twitter" => $row["twitter"],
	"expire_stamp" => $expire_stamp,
	];

$images = [];
$res = $db->q("SELECT filename FROM classifieds_image WHERE id = ? ORDER BY image_id ASC", [$clad_id]);
while ($row = $db->r($res)) {
	$url = "{$config_image_server}/classifieds/{$row["filename"]}";
	$images[] = $url;
}
$params["images"] = $images;

echo "<pre>Params:\n".print_r($params, true)."\n</pre><br />\n";

$params = [
	"client" => "adultsearch",
	"password" => "Ga98dj38dk",
	"action" => "create_profile",
	"parameters" => json_encode($params),
	];

$curl = new curl();
//$response = $curl->post("http://dev.tsescorts.com/asapi/main", $params);
$response = $curl->post("https://www.tsescorts.com/asapi/main", $params);
if (!$response) {
	echo "Publish on TS failed !<br />\n";
} else {
	echo "Response: '{$response}<br />\n";
	$response = json_decode($response, true);
	//echo "<pre>Response:\n".print_r($response, true)."\n</pre><br />\n";
	if (!array_key_exists("status", $response) || $response["status"] != "success") {
		echo "Publish on TS failed: '{$response["message"]}' !<br />\n";
	} else {
		echo "Publish on TS succeeded: <a href=\"{$response["url"]}\" target=\"_blank\">view ad on TS</a>.<br />\n";
	}
}

return;

?>
