<?php
/**
 * Add or edit recurring for classifed ad. Called from myposts page
 */
defined('_CMS_FRONTEND') or die('Restricted access');

require_once(_CMS_ABS_PATH."/_cms_files/classifieds/array.php");

global $db, $smarty, $account, $mobile, $config_site_url;
$classifieds = new classifieds;

$smarty->assign("nobanner", true);

if (!account::ensure_loggedin())
	die("Invalid access");

$id = intval($_REQUEST["id"]);
if (!$id)
	system::go("/");

$res = $db->q("SELECT c.*, cc.cc, cc.expmonth, cc.expyear, cc.cvc2
				FROM classifieds c
				LEFT JOIN account_cc cc on cc.cc_id = c.cc_id and cc.deleted = 0
				WHERE c.id = ?", array($id));
if (!$db->numrows($res))
	system::go("/classifieds/", "This classifieds ad has been removed.");
$rowcl = $db->r($res);
if ($rowcl["done"] == 3)
	system::moved("/classifieds/myposts");

if( !$account->isadmin() ) {
	if( !empty($rowcl["account_id"]) && $account->getId() != $rowcl["account_id"] )
		system::moved("/classifieds/look?id=".$id);
	else if( empty($rowcl["account_id"]) && !isset($_SESSION["cl_owner_".$id]) )
		system::moved("/classifieds/look?id=".$id);
}

if( isset($_GET["cancel"]) ) {
	$db->q("update classifieds set recurring = -1 where id = '$id'");
	system::go("/classifieds/myposts", "Canceled");
}

if( $rowcl['recurring'] < 1 && $rowcl['total_loc'] > 10 ) {
	system::go("/classifieds/myposts", "Ads posted more than 10 locations are not eligible for this feature.");
} elseif( $classifieds->blockedContent($rowcl['content'], $rowcl['content'], $error)  ) {
	system::go("/classifieds/myposts", $error);
}

$recurring = isset($_GET["recurring"])&&in_array($_GET["recurring"], array(1)) ? $_GET["recurring"] : 0;
$auto_renew_time = intval($_GET["auto_renew_time2"]);

$res = $db->q("select count(*) from classifieds_loc where post_id = '$id'");
$row = $db->r($res);

$total = $realtotal = $classifieds->price_per_recurring * $row[0];
if( !$total ) {
	reportAdmin("AS: cl/recurring - total is zero for the id $id", "", array("id" => $id));
	system::moved("/classifieds/myposts");
}

if( $rowcl['recurring'] > 0 && $rowcl['recurring_total'] > 0 && $rowcl['recurring_total'] != $total )
	$total = $rowcl['recurring_total'];

$rowcl['recurring_total'] = $total;

$timezone_name = NULL;

if ($recurring) {

	$classifieds->timezone($id, $timezone_time, $timezone_name);

	if ($rowcl['auto_renew'] > 0 && $rowcl['cc_id']) {
		$today  = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));

		$db->q("UPDATE classifieds 
				SET recurring = 1, auto_renew_time = '$auto_renew_time', time_next = ($today+60*60*24+auto_renew_time*60+(60*$timezone_time)) 
				WHERE id = '$id'"
				);
		if ($rowcl['recurring'] == 1)
			system::go("/classifieds/myposts", "Changes are made.");
		system::go("/classifieds/myposts", "Recurring is enabled.");
	}

	/*
	$anet = new anet();
	$anet->total = $total;
	$anet->no_promotion = true;
	$anet->what = "classifieds";
	$anet->item_id = $id;
	$anet->page = "recurring";
	$anet->p2 = "recurring|{$realtotal}|{$recurring}|{$auto_renew_time}";
	$anet->p3 = $id;
	$anet->recurring = 49.99;
	$anet->recurringPeriod = 30;
	$anet->fullcharge($error);
	*/

	return;
}

$art_array = get_time_array();
$smarty->assign("art_values", array_keys($art_array));
$smarty->assign("art_names", array_values($art_array));

if( !empty($rowcl['cc']) ) {
	$cc = "Your credit card ending with <b>****" . substr($rowcl['cc'], -4) . " ({$rowcl['expmonth']}/{$rowcl['expyear']})</b> will be charged for this recurring payment. <a href='".$config_site_url."/account/updatecc?id={$rowcl['cc_id']}&ref=/classifieds/recurring?id={$id}'>Click here</a> to update your credit card information.";
	$smarty->assign("cc", $cc);
}

if (is_null($timezone_name))
	$classifieds->timezone($id, $timezone_time, $timezone_name);
$smarty->assign("timezone", $timezone_name);

$smarty->assign("auto_renew_time2", isset($_GET["auto_renew_time2"]) ? $_GET["auto_renew_time2"] : $rowcl["auto_renew_time"]);

$smarty->assign("auto_renew", $rowcl["auto_renew"]);
$smarty->assign("recurring", $rowcl["recurring"]);
$smarty->assign("id", $id);
$smarty->assign("total", $total);

if ($mobile != 1)
	include_html_head("css", "/css/adbuild.css");

$smarty->display(_CMS_ABS_PATH."/templates/classifieds/classifieds_recurring.tpl");

?>
