<?php
/**
 * Admin controller: deletes classified ad
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $account;
$system = new system;

if (!($account_id=$account->isLoggedIn())) {
	$account->asklogin();
	return;
}

if (!$account->isadmin()) {
	echo "You dont have permissions to perform this action!<br />";
	die;
}

$id = intval($_REQUEST["id"]);
if (!$id) {
	echo "Classified ad id not specified!<br />";
	die;
}

//get clad
$clad = clad::findOneById($id);
if ($clad == false) {
	flash::add(flash::MSG_ERROR, "Cant find classified ad id #{$id} !");
	$system->go("/");
}

//block phone if requested
if (isset($_REQUEST['blckphone'])) {
	$phone = $clad->getPhone();
	if (!empty($phone))
		$db->q("INSERT IGNORE INTO classifieds_dont (phone) values (?)", array($phone));
}

//remove clad
$ret = $clad->remove();
if ($ret)
	flash::add(flash::MSG_SUCCESS, "Ad #{$id} was successfully deleted.");
else
	flash::add(flash::MSG_ERROR, "Error while deleting ad id #{$id} !");

$system->go("/");

die;

?>
