<?php

require_once(_CMS_ABS_PATH."/_cms_files/classifieds/array.php");
global $db, $smarty, $account, $mobile, $config_site_url;
$system = new system; $form = new form;

$id = intval($_GET["id"]);
if (!$id)
	$system->go("/");

$res = $db->q("select * from classifieds where id = ?", array($id));
if (!$db->numrows($res))
	$system->go("/");
$row = $db->r($res);

if ($row["reply"] == 0)
	$system->go("/classifieds/look?id=".$id);

$title = "AdultSearch re: ".$row["title"];

if( isset($_POST["from"]) ) {

	$from = GetPostParam("from"); $from = trim($from);
	$fromVerify = GetPostParam("fromVerify");
	if ($mobile == 1)
		$fromVerify = $from;
	$m = GetPostParam("m");

	if (filter_var($from, FILTER_VALIDATE_EMAIL) === false)
		$error = "Wrong E-mail Address";
	else if( strcmp($from, $fromVerify) )
		$error = "Verify E-Mail Address is Wrong";
	else if( empty($m) )
		$error = "Message can not be empty.";
	else if( empty($_POST["captcha_str"]) )
		$error = "You need to type the security code";
	else if( strcasecmp($_POST["captcha_str"], $_SESSION["captcha_str"]) )
		$error = "Security code is wrong, please type it again";

	if( !isset($error) ) {
		$m .= "<br/><br/>Please reply to email address {$from}.<br/><br/>Post title: {$row["title"]}<br><br>To view your posting, please click the following link: <a href='".$config_site_url."/classifieds/look?id={$id}'>".$config_site_url."/classifieds/look?id={$id}</a><br><br>This person has contacted you anonymously through AdultSearch.com. If you believe their email is a scam, spam, or inappropriate, please forward this email to <a href='abuse@adultsearch.com'>abuse@adultsearch.com</a>.";
		$params = array(
			"from" => "support@adultsearch.com",
			"reply_to" => $from,
			"to" => $row["email"],
			"html" => $m,
			"subject" => "You have a message from a customer"
			);
		$ret = send_email($params, $error);
		if (!$ret)
			reportAdmin("AS: classifieds/reply: Error sending message", "", $params);
		$smarty->assign("id", $id);
		$smarty->display(_CMS_ABS_PATH."/templates/classifieds/classifieds_reply_done.tpl");
		return;
	} else {
		$smarty->assign("error", $error);
	}

	$smarty->assign("m", $_POST["m"]);
}

if( empty($_POST["from"]) && $account->isloggedin() )
	$_POST["from"] = $_SESSION["email"];
if( empty($_POST["fromVerify"]) && $account->isloggedin() )
	$_POST["fromVerify"] = $_SESSION["email"];

$smarty->assign("from", $_POST["from"]);
$smarty->assign("fromVerify", $_POST["fromVerify"]);

if( !empty($_POST["captcha_str"]) && !strcasecmp($_POST["captcha_str"], $_SESSION["captcha_str"]) ) {
	$smarty->assign("captcha_str", $_POST["captcha_str"]);
	$smarty->assign("captcha_ok", true);
}

$cat_name = $form->arrayValue($type_array, $row["type"]);
$smarty->assign("title", "You have a message from a customer.");
$smarty->assign("loc_name", $row["loc_name"]);
$smarty->assign("cat_name", $cat_name);
$smarty->assign("id", $id);
$smarty->assign("loc_id", $_GET["loc_id"]);

$smarty->display(_CMS_ABS_PATH."/templates/classifieds/classifieds_reply.tpl");

?>
