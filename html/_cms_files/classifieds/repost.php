<?php
/**
 * Manual reposting of the ad if there is at least one auto_repost left
 */

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $account;

if (!account::ensure_loggedin())
	die("Invalid access");

$id = intval($_REQUEST["id"]);
if (!$id)
	system::go("/classifieds/myposts");

$res = $db->q("
	SELECT unix_timestamp(c.date) unix, c.account_id, COUNT(cl.id) as cnt 
	FROM classifieds c
	INNER JOIN classifieds_loc cl on cl.post_id = c.id
	WHERE c.id = ?
	GROUP BY c.id", 
	[$id]
	);
if (!$db->numrows($res))
	system::go("/classifieds/myposts", "This classifieds ad has been removed.");
$row = $db->r($res);
$locationCount = intval($row["cnt"]);

//check if we have permission to repost this ad
if (!$account->isadmin()) {
	if (!empty($row["account_id"]) && $account->getId() != $row["account_id"])
		system::moved("/classifieds/myposts");
	else if (empty($row["account_id"]) && !isset($_SESSION["cl_owner_".$id]))
		system::moved("/classifieds/myposts");
}

//check if we didnt repost this ad too recently, we only allow to repost every 2 hours
if ($row["unix"] > (time() - 60*60*2) ) {
	if (isset($_GET['ajax']))
		die("0|You have to wait at least 2 hours to use this feature again.");
	system::go("/classifieds/myposts", "You have to wait at least 2 hours to use this feature again.");
}

//check if we have repost credits
$repost = intval($account->getRepost());
if (!$repost) {
	if (isset($_GET['ajax']))
		die("0|This posting doesn't have any remaining auto-posts");
	system::go("/classifieds/myposts", "This posting doesn't have any remaining auto-posts");
}
//we need to have 1 for each location this ad is posted in
if ($repost < $locationCount) {
	if (isset($_GET['ajax']))
		die("0|This ad is posted in {$locationCount} locations, but you only have {$repost} repost credits. You need 1 repost credit for each location ad is posted in.");
	system::go("/classifieds/myposts", "This ad is posted in {$locationCount} locations, but you only have {$repost} repost credits. You need 1 repost credit for each location ad is posted in.");
}

//do repost
$classifieds = new classifieds;
$classifieds->repost($id);

if (isset($_GET['ajax']))
	die("1|Your ad posted again. Please allow us upto 5 minutes to update our listings.");

system::go("/classifieds/myposts", "Your ad has been posted to the top of the list. Please allow us up to 5 minutes to update our listings.");

?>
