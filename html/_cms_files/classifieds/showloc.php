<?php
/**
 * Called from my ads page if ad is posted in multiple locations to display which are these locations
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $account;

if (!($account_id=$account->isLoggedIn())) {
	die;
}

$id = intval($_GET['id']);
if (!$id)
	die;

$res = $db->q("SELECT t.loc_id, t.loc_name, t.s, t.live_ad, t.big, c.done
				FROM classifieds c 
				INNER JOIN classifieds_loc l on c.id = l.post_id 
				INNER JOIN location_topcities t on l.loc_id = t.loc_id 
				WHERE c.id = ? and c.account_id = ?",
				array($id, $account_id));
$out = "";
while($row = $db->r($res)) {

	//if ad is expired, we can "change locations" (will basically init adbuild from step 2 with prefilled current info)
	if ($row['done'] == 0)
		$changeloc = true;

	$bold = $row['big'] ? true : false;
	$out .= !empty($out) ? ", " : "";
	if ($bold)
		$out .= "<b>{$row["loc_name"]} {$row["s"]}</b>";
	else
		$out .= "{$row["loc_name"]} {$row["s"]}";
}

if ($changeloc)
	$out = "<p><a href='/adbuild/step2?ad_id=$id'>Click here to change the location(s) where this ad was posted.</a></p>" . $out;

echo $out;
die;
?>
