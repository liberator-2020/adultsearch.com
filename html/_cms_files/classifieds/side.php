<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $mobile;
$classifieds = new classifieds();

$smarty->assign("nobanner", true);

if (!account::ensure_loggedin())
	die("Invalid access");

//get ad
$id = intval($_REQUEST["id"]);
if (!$id)
	system::go("/");
$clad = clad::findOneById($id);
if (!$clad)
	system::go("/", "This classified ad has been removed.");
if ($clad->getDone() == 3 || $clad->getDeleted())
	system::go("/classifieds/myposts");
if ($clad->getDone() < 1)
	system::go("/adbuild/step3?ad_id={$id}");    //ad is not live, go to renew ad page

//do we have permission - is this our ad ?
if (!$account->isadmin()) {
	if ($clad->getAccountId() != $account->getId())
		system::moved("/classifieds/myposts");
}

//get number of side sponsors in each city
//we need this so we can disallow to create another side sponsor in city which already has all side sponsor spots filled
$side_sponsor_counts = $classifieds->getSideSponsorCounts($clad->getType());
$side_sponsor_price = classifieds::get_side_upgrade_price();

$selected_loc_ids = array();
foreach ($_REQUEST as $key => $val) {
	if ($key != "loc_id")
		continue;
	if (is_array($val))
		$selected_loc_ids = $val;
	else
		$selected_loc_ids[] = $val;
}
//echo "selected_loc_ids=".print_r($selected_loc_ids, true)."<br />\n";


if ($_REQUEST["submit"] == "Submit") {

	$locs = [];
	foreach ($selected_loc_ids as $loc_id) {
		$loc_id = intval($loc_id);
		if (array_key_exists($loc_id, $side_sponsor_counts) && $side_sponsor_counts[$loc_id] >= classifieds::$side_sponsor_count_limit)
			continue;
		$locs[] = $loc_id;
	}

	if (empty($locs)) {
		$smarty->assign("error", "You need to select at least one location.");
	} else {

		$smarty->assign("ccform_hidden", "<input type=\"hidden\" name=\"loc_id[]\" value=\"".implode(",",$locs)."\" />");

		$total = count($locs) * $side_sponsor_price;

		//create payment in database
		$now = time();
		$period = 30;
		$clad_account = $clad->getAccount();
		$ip_address = account::getUserIp();
		$res = $db->q("
			INSERT INTO payment 
			(created_stamp, account_id, author_id, amount, recurring_amount, recurring_period, email, result, ip_address)
			VALUES
			(?, ?, ?, ?, ?, ?, ?, ?, ?)",
			[$now, $clad->getAccountId(), $account->getId(), $total, $total, $period, $clad_account->getEmail(), "I", $ip_address]
			);
		$payment_id = $db->insertid($res);
		if (!$payment_id)
			system::moved("/classifieds/myposts");

		foreach($locs as $loc_id) {
			$res = $db->q("
				INSERT INTO payment_item
				(payment_id, type, classified_id, classified_status, loc_id, side, created_stamp)
				VALUES
				(?, ?, ?, ?, ?, ?, ?)",
				[$payment_id, "classified", $clad->getId(), "U", $loc_id, 1, $now]
				);
			$pi_id = $db->insertid($res);
			if (!$pi_id)
				system::moved("/classifieds/myposts");
		}

		return system::go("/payment/pay?id={$payment_id}");

	}
}

$locations = [];
$available_locations = 0;
foreach ($clad->getLocations() as $loc_id => $loc) {
	$not_available = false;
	if (array_key_exists($loc_id, $side_sponsor_counts) && $side_sponsor_counts[$loc_id] >= classifieds::$side_sponsor_count_limit) {
		$not_available = true;
	} else {
		$available_locations++;
		$locations[$loc_id] = $loc;
	}
}

if ($available_locations == 0)
	$smarty->assign("error", "Unfortunately all side sponsors in your locations are already purchased.");

$smarty->assign("id", $id);
$smarty->assign("locations", $locations);
$smarty->assign("selected_loc_ids", $selected_loc_ids);
$smarty->assign("side_sponsor_price", $side_sponsor_price);

if ($mobile != 1)
	include_html_head("css", "/css/adbuild.css");

$smarty->display(_CMS_ABS_PATH."/templates/classifieds/classifieds_sidesponsor.tpl");

?>
