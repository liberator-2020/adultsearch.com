<?php
/*
This allows to renew sponsoring of cl ad by escorts directly from "expire sponsor" email via autologin
*/
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $mobile, $config_image_server;
$classifieds = new classifieds;

$smarty->assign("nobanner", true);

if (!account::ensure_loggedin())
	die("Invalid access");

//cl id is mandatory parameter
$id = intval($_REQUEST['id']);
if (!$id)
	system::go("/classifieds/myposts");

//fetch cl ad
$clad = clad::findOneById($id);
if (!$clad)
	system::go("/classifieds/myposts");
if ($clad->getDeleted() || $clad->getDone() == 3)
	system::go("/classifieds/myposts");
if ($clad->getDone() < 1)
	system::go("/adbuild/step3?ad_id={$id}");	//ad is not live, go to renew ad page
if ($clad->getAccountId() != $account->getId() && !$account->isadmin())
	system::go("/classifieds/myposts");

if (isset($_FILES['sponsorpic']))
	$classifieds->sponsorpicupdate($id);

if (isset($_REQUEST['removepic'])) {
	$classifieds->sponsorpicremove($id);
	system::reload(array("removepic"=>"true"));
}

//get number of thumbnails in each city
//we need this so we can disallow to create another thumbnail in city which already has all city thumbnail spots filled
$city_thumb_counts = $classifieds->getCityThumbCounts();
$city_thumbnail_sponsor_price = $classifieds::get_city_thumbnail_upgrade_price();
//_darr($city_thumb_counts);

if ($_REQUEST["submit"] == "Submit") {
	if (!count($_REQUEST["locs"])) {
		$smarty->assign("error", "You need to pick location!");
	} else {

		$locs = array();
		foreach ($_REQUEST['locs'] as $loc_id) {
			$loc_id = intval($loc_id);
			if (array_key_exists($loc_id, $city_thumb_counts) && $city_thumb_counts[$loc_id] >= classifieds::$city_thumbnail_count_limit)
				continue;
			$locs[] = $loc_id;
		}

		if (empty($locs)) {
			$smarty->assign("error", "You need to pick location!");
		} else {
			if ($account->isadmin()) {
				if( isset($_REQUEST['confirm']) ) {
					$what = implode(',', $_REQUEST['locs']);
					$what .= " added to homepage thumb ...";
					$db->q("insert into classifieds_manage (id, account_id, time, what) values ('$id', '{$account->isloggedin()}', now(), '$what')");
				} else {
					echo "<a href='{$_SERVER['REQUEST_URI']}&confirm'>make this live..</a>";
					return;
				}
			} else {

				$total = 0;
				$total_loc = count($locs);

				foreach($locs as $loc_id)
					$total += $city_thumbnail_sponsor_price;

				//create payment in database
				$now = time();
				$period = 30;
				$clad_account = $clad->getAccount();
				$ip_address = account::getUserIp();
				$res = $db->q("
					INSERT INTO payment 
					(created_stamp, account_id, author_id, amount, recurring_amount, recurring_period, email, result, ip_address)
					VALUES
					(?, ?, ?, ?, ?, ?, ?, ?, ?)",
					[$now, $clad->getAccountId(), $account->getId(), $total, $total, $period, $clad_account->getEmail(), "I", $ip_address]
					);
				$payment_id = $db->insertid($res);
				if (!$payment_id)
					system::moved("/classifieds/myposts");

				foreach($locs as $loc_id) {
					$res = $db->q("
						INSERT INTO payment_item
						(payment_id, type, classified_id, classified_status, loc_id, sponsor, created_stamp)
						VALUES
						(?, ?, ?, ?, ?, ?, ?)",
						[$payment_id, "classified", $clad->getId(), "U", $loc_id, 1, $now]
						);
					$pi_id = $db->insertid($res);
					if (!$pi_id)
						system::moved("/classifieds/myposts");
				}

				return system::go("/payment/pay?id={$payment_id}");

			}
		}
	}
}


$res = $db->q("SELECT l.loc_id, ll.loc_name, s.id
		FROM classifieds_loc l
		INNER JOIN location_location ll on l.loc_id = ll.loc_id
		LEFT JOIN classifieds_sponsor s on (l.loc_id = s.loc_id and l.post_id = s.post_id and s.done = 1) 
		WHERE l.post_id = ?", 
		[$id]
		);
$total_loc = $db->numrows($res);
$available_locations = 0;
$homesp = [];
while($row = $db->r($res)) {
	$loc_id = $row["loc_id"];
	$not_available = false;
	if (array_key_exists($loc_id, $city_thumb_counts) && $city_thumb_counts[$loc_id] >= classifieds::$city_thumbnail_count_limit) {
		$not_available = true;
	} else {
		$available_locations++;
	}
	$homesp[] = array(
		"loc_id" => $loc_id,
		"loc_name" => $row['loc_name'],
		"price" => $classifieds->gethomepricebyloc($row['loc_id']),
		"not_available" => $not_available,
	);
}

if ($available_locations == 0)
	$smarty->assign("error", "Unfortunately all city thumbnails in your locations are already purchased.");

if ($total_loc > 10)
	system::go("/classifieds/myposts", "Only ads that are posted in less than 10 locations are eligible for this upgrade.");

$smarty->assign("homesp", $homesp);

$res = $db->q("select images from classifieds_sponsor where post_id = '$id' limit 1");
if( $db->numrows($res) ) {
	$row = $db->r($res);
	if (!empty($row['images']))
		$smarty->assign("sponsorpic", $config_image_server."/classifieds/s/{$row['images']}");
} 

$smarty->assign("id", $id);

if ($mobile != 1)
	include_html_head("css", "/css/adbuild.css");

$smarty->assign("home_price_month", $city_thumbnail_sponsor_price);

$smarty->display(_CMS_ABS_PATH."/templates/classifieds/classifieds_homesponsor.tpl");

?>
