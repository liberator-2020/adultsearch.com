<?php

use App\Service\Classified\WaitingList;

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $mobile;
$smarty->assign("nobanner", true);
$classifieds = new classifieds();

if (!account::ensure_loggedin())
	die("Invalid access");

global $clad, $selected_loc_ids, $sticky_counts;

//get ad
$id = intval($_REQUEST["id"]);
if (!$id)
	system::go("/");
$clad = clad::findOneById($id);
if (!$clad)
	system::go("/", "This classified ad has been removed.");
if ($clad->getDone() == 3 || $clad->getDeleted())
	system::go("/classifieds/myposts");
if ($clad->getDone() < 1)
	system::go("/adbuild/step3?ad_id={$id}");    //ad is not live, go to renew ad page

//sticky upgrades are only for agencies, unless it is TS ad
if ($clad->getType() != 2  && !$account->getAgency())
	system::go("/classifieds/myposts");

//do we have permission - is this our ad ?
if (!$account->isadmin()) {
	if ($clad->getAccountId() != $account->getId())
		system::go("/classifieds/myposts");
}

//get number of stickies in each city
//we need this so we can disallow to create another sticky ad in city which already has all sticky slots filled
$sticky_counts = $classifieds->getStickyCounts($clad->getType());

$selected_loc_ids = [];
foreach ($_REQUEST as $key => $val) {
	if ($key != "loc_id")
		continue;
	if (is_array($val))
		$selected_loc_ids = $val;
	else
		$selected_loc_ids[] = $val;
}
//echo "selected_loc_ids=".print_r($selected_loc_ids, true)."<br />\n";

function process_submit() {
	global $db, $smarty, $account, $selected_loc_ids, $clad, $sticky_counts;

	$stickies = [];
	$amount = 0;
	$recurring = ($_REQUEST["recurring"] == 1);
	$some_30 = false;
	$some_not_30 = false;

	//go through all the locations we have checked in
	foreach ($selected_loc_ids as $loc_id) {
		$loc_id = intval($loc_id);
		$days = intval($_POST['days'][$loc_id]);

		if (array_key_exists($loc_id, $sticky_counts) && $sticky_counts[$loc_id] >= classifieds::$sticky_count_limit) {
			$smarty->assign("error", "You can'r post sticky ad in this location.");
			return;
		}

		//make sure we picked either 7 or 14 or 30 days for each selected location
		if (!in_array($days, [7,14,30])) {
			$smarty->assign("error", "Please pick the sticky duration option for each city you have selected.");
			return;
		}

		//this is to check whether we picked 30 days period on ALL the cities (and therefore whether we should check recurring option)
		if ($days == 30)
			$some_30 = true;
		else
			$some_not_30 = true;

		$stickies[] = [
			"loc_id" => $loc_id,
			"days" => $days
		];

		$amount += classifieds::get_sticky_upgrade_price($clad->getType(), $loc_id, $days);
	}

	if (!$stickies) {
		$smarty->assign("error", "You need to select at least one location.");
		return;
	}

	if (!$amount) {
		//amount is zero ? this should not happen
		$smarty->assign("error", "Invalid total price, please contact support@adultsearch.com");
		return;
	}

	//determine recurring option
	$recurring_amount = $recurring_period = null;
	$recurring = ($some_30 && !$some_not_30 && $recurring);
	if ($recurring) {
		$recurring_amount = $amount;
		$recurring_period = 30;
	}

	//create payment in database
	$now = time();
	$clad_account = $clad->getAccount();
	$ip_address = account::getUserIp();
	$res = $db->q("
		INSERT INTO payment 
		(created_stamp, account_id, author_id, amount, recurring_amount, recurring_period, email, result, ip_address)
		VALUES
		(?, ?, ?, ?, ?, ?, ?, ?, ?)",
		[$now, $clad->getAccountId(), $account->getId(), $amount, $recurring_amount, $recurring_period, $clad_account->getEmail(), "I", $ip_address]
	);
	$payment_id = $db->insertid($res);
	if (!$payment_id)
		return error_redirect("Server error, can't insert payment into database", "/classifieds/myposts");

	foreach($stickies as $sticky) {
		$res = $db->q("
			INSERT INTO payment_item
			(payment_id, type, classified_id, classified_status, loc_id, sticky, sticky_position, sticky_days, created_stamp)
			VALUES
			(?, ?, ?, ?, ?, ?, ?, ?, ?)",
			[$payment_id, "classified", $clad->getId(), "U", $sticky['loc_id'], 1, NULL, $sticky['days'], $now]
		);
		$pi_id = $db->insertid($res);
		if (!$pi_id)
			return error_redirect("Server error, can't insert payment item into database", "/classifieds/myposts");
	}

	system::go("/payment/pay?id={$payment_id}");
}

if ($_REQUEST["submit"] == "Submit") {
	process_submit();
}

$stickies = $clad->getStickyLocations();

$locations = [];
$sticky_min_monthly_price = null;
foreach ($clad->getLocations() as $loc_id => $loc) {
	$not_available = false;
	if (array_key_exists($loc_id, $sticky_counts) && $sticky_counts[$loc_id] >= classifieds::$sticky_count_limit) {
		$not_available = true;
	}
	$monthly_price = classifieds::get_sticky_upgrade_price($clad->getType(), $loc_id, 30);
	$weekly_price = classifieds::get_sticky_upgrade_price($clad->getType(), $loc_id, 7);
	if (!$sticky_min_monthly_price || $monthly_price < $sticky_min_monthly_price) {
		$sticky_min_monthly_price = $monthly_price;
	}

	$cell = [
		'loc' => $loc,
		"not_available" => $not_available,
		'has_sticky' => (in_array($loc_id, $stickies)),
		'price_monthly' => $monthly_price,
		'price_weekly' => $weekly_price,
	];
	$locations[] = $cell;
}
$smarty->assign("sticky_min_monthly_price", $sticky_min_monthly_price);

$stickyWaitingListService = new WaitingList();
$awaitingLocationsIds = $stickyWaitingListService->getActualAwaitingLocationsIdsByAccountId($account->getId());
$smarty->assign('awaiting_locations_ids', $awaitingLocationsIds);

$smarty->assign("id", $id);
$smarty->assign("clad", $clad);
$smarty->assign("locations", $locations);
$smarty->assign("selected_loc_ids", $selected_loc_ids);

if ($mobile != 1)
	include_html_head("css", "/css/adbuild.css");

$smarty->display(_CMS_ABS_PATH."/templates/classifieds/sticky.tpl");

?>
