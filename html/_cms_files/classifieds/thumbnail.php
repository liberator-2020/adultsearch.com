<?php
/**
 * Editing thumbnail for specific classified ad
 */

global $db, $smarty, $account, $config_image_path;

if (!account::ensure_loggedin())
	die("Invalid access");
$account_id = $account->getId();

$id = intval($_REQUEST['id']);
if (!$id) {
	echo "Classified ad id must be specified!<br />\n";
	die;
}

$clad = clad::findOneById($id);
if (!$clad) {
	echo "Can't find classified ad id #{$id} !<br />\n";
	die;
}

if (($clad->getAccountId() != $account_id) && !$account->isAdmin()) {
	echo "You don't have privileges for editing thumbnail for this classified ad !<br /><!-- {$clad->getAccountId()} {$account_id} -->\n";
	die;
}

function create_thumbnail($clad, $src_filepath, $upload_dir) {
	global $config_image_path;

	//find out new filename for thumbnail (the filename needs to be different than previous one, we want to be the change visible asap)
	$old_filename = $clad->getThumb();
	$pos_underscore = strpos($old_filename, "_");
	$pos_dot = strpos($old_filename, ".");
	if ($pos_underscore === false) {
		$new_filename_base = substr($old_filename, 0, $pos_dot);
		$curr_number = 1;
	} else {
		$new_filename_base = substr($old_filename, 0, $pos_underscore);
		$curr_number = intval(substr($old_filename, $pos_underscore + 1, ($pos_dot - $pos_underscore + 1))) + 1;
	}
	while (file_exists($config_image_path."/classifieds/{$new_filename_base}_{$curr_number}.jpg")) {
		$curr_number++;
	}

	//create thumbnail
	$uploader = new Upload($src_filepath);
	if (!$uploader->uploaded) {
		die("Error: Can't process image by Upload class!");
	}
	$uploader->image_convert = "jpg";
	$uploader->image_resize = true;
	if ($uploader->image_src_x > 75 || $uploader->image_src_y > 75)
		$uploader->image_precrop = "$y1 ".(($uploader->image_src_x)-$x2)." ".($uploader->image_src_y-$y2)." $x1";
	$uploader->image_x = 75;
	$uploader->image_y = 75;
	$uploader->image_ratio_crop = "T";
	$uploader->file_new_name_body = $new_filename_base."_".$curr_number;
	$uploader->file_overwrite = false;
	$uploader->process($upload_dir);
	if ($uploader->processed) {
		//saving as different filename (to avoid caching issues in browsers and to see instant change)
		$clad->setThumb($uploader->file_dst_name);
		$ret = $clad->update();
		@unlink($upload_dir."/".$old_filename);
		return $uploader->file_dst_name;
	}
	return false;
}

//-------------------
//AJAX update handler
//-------------------
if (isset($_REQUEST["image_id"])) {
	$image_id = intval($_REQUEST["image_id"]);
	$x1 = intval($_REQUEST["x1"]);
	$x2 = intval($_REQUEST["x2"]);
	$y1 = intval($_REQUEST["y1"]);
	$y2 = intval($_REQUEST["y2"]);
	$w = intval($_REQUEST["w"]);
	$h = intval($_REQUEST["h"]);
	if (!$image_id || !$y2 || !$x2 || !$w || !$h) {
		die("Error: some mandatory parameters not submitted!");
	}

	//get selected image filename
	$res = $db->q("SELECT filename FROM classifieds_image WHERE image_id = ? LIMIT 1", array($image_id));
	if (!$db->numrows($res)) {
		die("Error: classified image #{$image_id} not found!");
	}
	$row = $db->r($res);
	$src_filepath = classifieds::getUploadDir().$row["filename"];
	if (!file_exists($src_filepath)) {
		die("Error: file for classified image #{$image_id} does not exist!");
	}

	//find out new filename for thumbnail (the filename needs to be different than previous one, we want to be the change visible asap)
	$old_filename = $clad->getThumb();
	$pos_underscore = strpos($old_filename, "_");
	$pos_dot = strpos($old_filename, ".");
	if ($pos_underscore === false) {
		$new_filename_base = substr($old_filename, 0, $pos_dot);
		$curr_number = 1;
	} else {
		$new_filename_base = substr($old_filename, 0, $pos_underscore);
		$curr_number = intval(substr($old_filename, $pos_underscore + 1, ($pos_dot - $pos_underscore + 1))) + 1;
	}
	while (file_exists($config_image_path."/classifieds/{$new_filename_base}_{$curr_number}.jpg")) {
		$curr_number++;
	}

	//create thumbnail
	$uploader = new Upload($src_filepath);
	if (!$uploader->uploaded) {
		die("Error: Can't process image by Upload class!");
	}
	$uploader->image_convert = "jpg";
	$uploader->image_resize = true;
	$uploader->image_precrop = "$y1 ".(($uploader->image_src_x)-$x2)." ".($uploader->image_src_y-$y2)." $x1";
	$uploader->image_x = 75;
	$uploader->image_y = 75;
	$uploader->image_ratio_crop = "T";
	$uploader->file_new_name_body = $new_filename_base."_".$curr_number;
	$uploader->file_overwrite = false;
	$uploader->Process(classifieds::getUploadDir());
	if ($uploader->processed) {
		//saving as different filename (to avoid caching issues in browsers and to see instant change)
		$clad->setThumb($uploader->file_dst_name);
		$ret = $clad->update();
		@unlink(classifieds::getUploadDir()."/".$old_filename);
		die("http://img.adultsearch.com/classifieds/".$uploader->file_dst_name);
	}
	die("Error: while processing image by Upload class!");
}


//file upload handler
if ($_REQUEST["submit"] == "Upload") {
	if (!isset($_FILES['thumbnail_file']['error']) || is_array($_FILES['thumbnail_file']['error']) || $_FILES['thumbnail_file']['error'] != UPLOAD_ERR_OK) {
		flash::add(flash::MSG_ERROR, "Failed to upload file. Either no file sent or sent file is too large");
		system::go("/classifieds/thumbnail?id={$clad->getId()}");
	}
	$upl_file_path = $_FILES['thumbnail_file']['tmp_name'];
	$upl_file_name = $_FILES['thumbnail_file']['name'];
	$upl_file_size = $_FILES['thumbnail_file']['size'];
	debug_log("Upload new thumbnail: account_id={$account->getId()}, clad_id={$clad->getId()}, file_size={$upl_file_size}, file_path={$upl_file_path}, file_name={$upl_file_name}");
	$ret = create_thumbnail($clad, $upl_file_path, classifieds::getUploadDir());
	if (!$ret) {
		flash::add(flash::MSG_ERROR, "Failed to change thumbnail");
		system::go("/classifieds/thumbnail?id={$clad->getId()}");
	}
	flash::add(flash::MSG_SUCCESS, "Thumbnail successfully changed.");
	system::go("/classifieds/thumbnail?id={$clad->getId()}");
}

//---------
//main page
//---------
$images = array();
$res = $db->q("SELECT image_id, filename FROM classifieds_image WHERE id = ? ORDER BY image_id ASC", array($clad->getId()));
while ($row = $db->r($res)) {
	$images[] = array("id" => $row["image_id"], "filename" => $row["filename"]);
}
if (empty($images))
	$smarty->assign('nopic', true);

$smarty->assign("current", $clad->getThumb());
$smarty->assign("current_time", time());
$smarty->assign("images", $images);
$smarty->assign("id", $id);
$smarty->assign("nobanner", true);

include_html_head("css", "/css/adbuild.css");
$smarty->display(_CMS_ABS_PATH."/templates/classifieds/thumbnail.tpl");

?>
