<?php
//buying global top-up credits
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $mobile;
$classifieds = new classifieds;

$smarty->assign("nobanner", true);


//we must be logged in on this page
if (!account::ensure_loggedin())
	die("Invalid access");


//which account are we buying topups for ?
$acc = null;
if (($account->isadmin() || $account->isworker()) && intval($_REQUEST["account_id"])) {
	$account_id = intval($_REQUEST["account_id"]);
	$acc = account::findOneById($account_id);
	if (!$acc)
		die("Accountnot found for account_id={$account_id} !");
} else {
	$acc = $account;	//if we are not admin or worker, we're buying for ourselves
}

//establishing the price of topup
$price_topup = $classifieds->price_topup;
if ($acc->getAgency())
	$price_topup = $classifieds->price_topup_agency;
if (!$price_topup)
	die("Invalid top-up price!");

$number = intval($_REQUEST["number"]);

$error = null;
if ($_REQUEST["submit"] == "Submit") {
	if (!$number) {
		$error = "Please select the number of top-up credits you wish to purchase!";
	} else if ($number < 1 || $number > 100) {
		$error = "Invalid number of top-up credits!";
	}

	if (!$error) {
		//create payment in database
		$now = time();
		$ip_address = account::getUserIp();
		$total = $number * $price_topup;
		$res = $db->q("
			INSERT INTO payment 
			(created_stamp, account_id, author_id, amount, recurring_amount, recurring_period, email, result, ip_address)
			VALUES
			(?, ?, ?, ?, ?, ?, ?, ?, ?)",
			[$now, $acc->getId(), $account->getId(), $total, null, null, $acc->getEmail(), "I", $ip_address]
			);
		$payment_id = $db->insertid($res);
		if (!$payment_id)
			system::moved("/classifieds/myposts");

		$res = $db->q("
			INSERT INTO payment_item
			(payment_id, type, auto_renew, created_stamp)
			VALUES
			(?, ?, ?, ?)",
			[$payment_id, "repost", $number, $now]
			);
		$pi_id = $db->insertid($res);
		if (!$pi_id)
			system::moved("/classifieds/myposts");

		return system::go("/payment/pay?id={$payment_id}");
	}
}

$option_values = ["", 1, 2, 3, 5, 10, 20, 30, 50, 100];
$option_labels = [];
foreach ($option_values as $option) {
	if (!$option)
		$option_labels[] = "- Please choose -";
	else
		$option_labels[] = "{$option} top-up credits - \$".number_format($option * $price_topup, 2, ".", ",");
}
$smarty->assign("option_values", $option_values);
$smarty->assign("option_labels", $option_labels);
$smarty->assign("number", $number);

$smarty->assign("price_topup", $price_topup);
$smarty->assign("account_id", intval($_REQUEST["account_id"]));
$smarty->assign("error", $error);

if ($mobile != 1)
	include_html_head("css", "/css/adbuild.css");

$smarty->display(_CMS_ABS_PATH."/templates/classifieds/topup.tpl");

?>
