<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $config_site_url;
$gTitle = "Your latest top-ups";

if (!account::ensure_loggedin())
    die("Invalid access");

if ($_REQUEST["go_back"] == "1")
    system::go("/classifieds/myposts");

if ($account->isadmin() && intval($_REQUEST["account_id"]))
	$account_id = intval($_REQUEST["account_id"]);
else
	$account_id = $account->getId();

$id = intval($_REQUEST["id"]);

if ($id) {
	$res = $db->q("
    	SELECT rl.*
	    FROM repost_log rl
		INNER JOIN classifieds c on c.id = rl.classified_id
    	WHERE c.account_id = ? AND rl.classified_id = ?
		ORDER BY rl.stamp DESC
		LIMIT 100",
		[$account_id, $id]
		);
} else {
	$res = $db->q("
    	SELECT rl.*
	    FROM repost_log rl
		INNER JOIN classifieds c on c.id = rl.classified_id
    	WHERE c.account_id = ?
		ORDER BY rl.stamp DESC
		LIMIT 100",
		[$account_id]
		);
}

$top_ups = [];
while ($row = $db->r($res)) {
	$classified_id = $row["classified_id"];
	$stamp = $row["stamp"];
	$author_id = $row["author_id"];
	$top_ups[] = [
		"classified_id" => $classified_id,
		"stamp" => $stamp,
		"author_id" => $author_id,
		];
}

$smarty->assign("nobanner", true);
$smarty->assign("top_ups", $top_ups);
$smarty->assign("id", $id);
$smarty->assign("account_id", $account_id);

escortsbiz::exportEbizLink();
//check if we already have website
if ($account->getEbizId() && $account->getEbizSecret()) {
    $smarty->assign('ebiz_login_url', escortsbiz::getMyLoginUrl());
}

$smarty->display(_CMS_ABS_PATH."/templates/classifieds/topups.tpl");

?>
