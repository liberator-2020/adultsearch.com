<?php

defined('_CMS_FRONTEND') or die('Restricted access');

require_once(_CMS_ABS_PATH."/_cms_files/classifieds/array.php");

global $db, $smarty, $account;
$form = new form;
$system = new system;
$classifieds = new classifieds;

$smarty->assign("nobanner", true);

if (!account::ensure_loggedin())
    die("Invalid access");

$id = intval($_REQUEST["id"]);
if (!$id)
	$system->go("/");

$res = $db->q("select * from classifieds where id = ?", [$id]);
if (!$db->numrows($res))
	$system->go("/classifieds/", "This classified ad has been removed.");
$rowcl = $db->r($res);
if ($rowcl["done"] == 3)
	$system->moved("/classifieds/myposts");

//check if we are owner or admin
if (!$account->isadmin()) {
	if (!empty($rowcl["account_id"]) && $account->getId() != $rowcl["account_id"])
		$system->moved("/classifieds/look?id=".$id);
	else if (empty($rowcl["account_id"]) && !isset($_SESSION["cl_owner_".$id]))
		$system->moved("/classifieds/look?id=".$id);
}

$total_loc = intval($db->single("select count(*) from classifieds_loc where post_id = ?", [$id]));

if (!$account->isadmin()) {
	if ($total_loc > 10) {
		$system->go("/classifieds/myposts", "Ads posted in more than 10 locations can not be re-posted.");
	} elseif ($classifieds->blockedContent($rowcl['content'], '', $error)) {
		$system->go("/classifieds/myposts", $error);
	}
}

//by default take from classified entry
$auto_renew_time = $rowcl["auto_renew_time"];
$auto_renew_fr = $rowcl["auto_renew_fr"];

if ($_REQUEST["submit"] == "Submit") {

	$auto_renew = intval($_REQUEST["auto_renew"]);
	$auto_renew_time = intval($_REQUEST["auto_renew_time"]);
	$auto_renew_fr = intval($_REQUEST["auto_renew_fr"]);
	if ($auto_renew_fr > 30)
		$auto_renew_fr = 3;

	//form submitted
	$total = $classifieds->price_per_repost * $total_loc * $auto_renew;

	$ccform_hidden = "";
	$ccform_hidden .= "<input type=\"hidden\" name=\"auto_renew\" value=\"{$auto_renew}\" />";
	$ccform_hidden .= "<input type=\"hidden\" name=\"auto_renew_time\" value=\"{$auto_renew_time}\" />";
	$ccform_hidden .= "<input type=\"hidden\" name=\"auto_renew_fr\" value=\"{$auto_renew_fr}\" />";
	$smarty->assign("ccform_hidden", $ccform_hidden);

	/*
	$anet = new anet();
	$anet->total = $total;
	$anet->no_promotion = true;
	$anet->what = "classifieds";
	$anet->item_id = $id;
	$anet->page = "upgrade";
	$anet->p2 = "upgrade|{$auto_renew_fr}|{$auto_renew}|{$total_loc}";
	$anet->p3 = $id;
	$anet->fullcharge($error);
	*/

	return;
}

//art
$art_array = get_time_array();
$smarty->assign("art_values", array_keys($art_array));
$smarty->assign("art_names", array_values($art_array));
$smarty->assign("auto_renew_time", $auto_renew_time);

//arf
$arf_array = [];
for ($i = 1; $i <= 7; $i++) {
	if ($i == 1)
		$text = "Every Day";
	else
		$text = "{$i} days";
	$arf_array[$i] = $text;
}
$smarty->assign("arf_values", array_keys($arf_array));
$smarty->assign("arf_names", array_values($arf_array));
$smarty->assign("auto_renew_fr", $auto_renew_fr);

//auto_renew
$ar_array = [];
for ($i = 5; $i < 31; $i += 5) {
	$ar_array[$i] = "$i Times for $".number_format($classifieds->price_per_repost*$i*$total_loc);
}
$smarty->assign("ar_values", array_keys($ar_array));
$smarty->assign("ar_names", array_values($ar_array));
$smarty->assign("auto_renew", $auto_renew);


$classifieds->timezone($id, $timezone_time, $timezone_name);
$smarty->assign("timezone", $timezone_name);
$smarty->assign("total_loc", $total_loc*3);
$smarty->assign("price_per_repost", $classifieds->price_per_repost);
$smarty->assign("id", $id);

if ($mobile != 1)
	include_html_head("css", "/css/adbuild.css");

$smarty->display(_CMS_ABS_PATH."/templates/classifieds/classifieds_upgrade_new.tpl");

?>
