<?php

use App\Service\Classified\WaitingList;

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $account, $smarty;

if (!account::ensure_loggedin())
	die("Invalid access");

$classifiedId     = array_key_exists('classifiedId', $_POST) ? (int)$_POST['classifiedId'] : 0;
$classifiedTypeId = array_key_exists('typeId',       $_POST) ? (int)$_POST['typeId']       : 0;
$locationId       = array_key_exists('locationId',   $_POST) ? (int)$_POST['locationId']   : 0;

$classifieds = new classifieds();
if (!array_key_exists($classifiedTypeId, $classifieds->adbuild_type_array)) {
	echo json_encode(['status' => 'error', 'message' => 'Incorrect typeId']);
	exit;
}

if ($classifiedId < 1) {
	echo json_encode(['status' => 'error', 'message' => 'Incorrect classifiedId']);
	exit;
}

$clad = new clad();
$classified = $clad->findOneById($classifiedId);
if ($classified === null) {
	echo json_encode(['status' => 'error', 'message' => 'Incorrect classifiedId']);
	exit;
}

if ((int)$classified->getAccountId() !== (int)$account->getId()) {
	echo json_encode(['status' => 'error', 'message' => "You is not owner of classified #{$classifiedId}"]);
	exit;
}

if (!array_key_exists($locationId, $classified->getLocLabels())) {
	echo json_encode(['status' => 'error', 'message' => "Classified is not placed at location #{$locationId}"]);
	exit;
}

if (classifieds::locationHasFreeSlotSticky($locationId, $classifiedTypeId)) {
	echo json_encode(['status' => 'error', 'message' => 'Location has free sticky slot']);
	exit;
}

$stickyWaitingListService = new WaitingList();
$stickyWaitingListService->add($account->getId(), $locationId, $classifiedTypeId);

echo json_encode(['status' => 'success']);
exit;
