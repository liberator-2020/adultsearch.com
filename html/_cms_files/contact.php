<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle;
$gTitle = "Contact Us";

$where = isset($_GET["where"]) ? $_GET["where"] : NULL;
$account_id = intval($account->isloggedin());


// -----------------------------------------
// SPAM check
// page will not work for these IP addresses
$spam_ips = array(
	"188.143.234.155",
	"195.154.173.103",
	);
$ip = account::getUserIp();
if (in_array($ip, $spam_ips)) {
	echo "no entry";
	return;
}


// -----------------
// Processing

if (isset($_GET['mail'])) {
	$email = GetGetParam('from');
	$text = GetGetParam('message');
	$db->q("INSERT INTO contact 
			(account_id, email, ip_address, stamp, `text`, name, url, agent) 
			VALUES 
			(0, ?, ?, ?, ?, ?, 'E-Mail', 'E-Mail')",
			[$email, $ip, time(), $text, $email]
			);
	echo $db->insertid;
	die;
}

if (isset($_POST["name"])) {
	$name = substr(trim($_REQUEST["name"]), 0, 100);
	$email = substr(trim($_REQUEST["email"]), 0, 100);
	$text = classifieds::onlyPrintableCharacters($_REQUEST["text"]);
	$phone = substr($_REQUEST["phone"], 0, 20);
	if (is_null($phone))
		$phone = "";

	//check if we dont already have contact message with exactly same content in last month -> spam measure
	$double_post = false;
	$month_ago = time() - 86400*30;
	$res = $db->q("SELECT contact_id FROM contact WHERE `text` = ? AND stamp > ?", [$text, $month_ago]);
	if ($db->numrows($res) > 0)
		$double_post = true;

	if( empty($email) || filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
		$error = "Please check your email.";
	} else if (empty($_POST["captcha_str"])) {
		$error = "You need to type the security code";
	} else if (strcasecmp($_POST["captcha_str"], $_SESSION["captcha_str"])) {
		$error = "Security code is wrong, please type it again";
	} else if (empty($text)) {
		$error = $error ? $error : "You need to write something!";
	} else if ($_REQUEST["phone_or_email"]) {
		//this is hidden field and should be always submitted empty, if it is fille, it is spam robot, show fake successful message but dont save in db
		system::go($where, "Your message has been sent");
	} else if (strpos($text, "д") !== false 
		|| strpos($text, "и") !== false 
		|| strpos($text, "ь") !== false 
		|| strpos($text, "://vk.com") !== false
		|| strpos($text, "political news") !== false
		|| strpos($text, "CPA details") !== false
		|| strpos($text, "casino") !== false
		|| strpos($text, "gambling") !== false
		|| strpos($text, "copywriter") !== false
		|| strpos($text, "kathreadwrites.xyz") !== false
		|| strpos($text, "talkwithcustomer") !== false
		) {
		//filter out russian submissions - they are always spam - show fake sent message
		system::go($where, "Your message has been sent");
	} else if ($double_post) {
		//this is duplicate post, probably spam, show fake successful message but dont save in db
		system::go($where, "Your message has been sent");
	} else {
		//everything correctly filled, store in db and display sent message
		$ip = account::getUserIp();
		$url = substr($_SERVER["HTTP_HOST"].$where, 0, 180);
		$agent = substr($_SERVER['HTTP_USER_AGENT'], 0, 250);
		$db->q("INSERT INTO contact 
				(account_id, email, ip_address, stamp, phone, `text`, name, url, agent) 
				VALUES 
				(?, ?, ?, ?, ?, ?, ?, ?, ?)",
				[$account_id, $email, $ip, time(), $phone, $text, $name, $url, $agent]
				);

		system::go($where, "Your message has been sent");
	}

	//some error occured, display it to user	
	$smarty->assign("error", $error);
}

$smarty->assign("where", htmlspecialchars($where));
$smarty->assign("email", isset($_REQUEST["email"]) ? htmlspecialchars($_REQUEST["email"]) : (isset($_SESSION["email"]) ? $_SESSION["email"] : ""));
$smarty->assign("name", isset($_REQUEST["name"]) ? htmlspecialchars($_REQUEST["name"]) : (isset($_SESSION["username"]) ? $_SESSION["username"] : ""));
$smarty->assign("text", htmlspecialchars($text));
$smarty->assign("phone", htmlspecialchars($_REQUEST["phone"]));

if (!empty($_POST["captcha_str"]) && !strcasecmp($_POST["captcha_str"], $_SESSION["captcha_str"])) {
	$smarty->assign("captcha_str", $_POST["captcha_str"]);
	$smarty->assign("captcha_ok", true);
}

global $gCanonicalUrl;
$gCanonicalUrl = "https://adultsearch.com/contact";
$smarty->display(_CMS_ABS_PATH."/templates/contact/contact.tpl");

?>
