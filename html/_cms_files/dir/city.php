<?php

defined("_CMS_FRONTEND") or die("No Access");

require_once(_CMS_ABS_PATH."/inc/classes/class.dir.php");
$dir = new dir;

global $account, $ctx, $smarty, $gTitle, $gDescription, $config_image_server;

include_html_head("css", "/css/city.css?20180616");

location::setDefaultLocation($ctx->city_id);

$dir->ShowAd();

$gTitle = $dir->getTitle();
$gDescription = $dir->getMetaDescription();

$cats  = $dir->getLocCategories2();

//TODO move to dir class
$cat_names = array();
$categories = array();
$categories_gay = array();

foreach ($cats as $cat) {
	$name = $cat['name_plural'];
	$url = $cat['link'];
	$count = $cat['c'];
	$populars = $cat["populars"];

	//for long titles show only 4 popular places (so it fits into box)
	//TODO this is hardcoded and not nice
	if (((strlen($name) > 30) || (count($cat["subcats"]) > 1)) && (count($populars) > 4))
		array_pop($populars);

	$addl_classes = "";
	if (stripos($name, "gay") !== false)
		$addl_classes .= " gay";

	$subcats = array();
	foreach ($cat["subcats"] as $subcat) {
		$subcats[] = array(
			"name" => $subcat["name_plural"],
			"title" => $ctx->city_name." ".$subcat["name_plural"],
			"url" => $subcat["link"],
			"count" => $subcat["c"],
			);
	}

	$category = array(
		"name" => $name,
		"title" => $ctx->city_name." ".$name,
		"url" => $url,
		"count" => $count,
		"populars" => $populars,
		"addl_classes" => $addl_classes,
		"subcats" => $subcats,
	);

	$categories[] = $category;

	$cat_names[] = $cat['name_plural'];
}
//_darr($categories);
$categories = array_merge($categories, $categories_gay);
$smarty->assign("categories", $categories);

//-----------------------------------------
//gather all videos for places in this city
$city_videos = $dir->agetVideosForCity();
//_darr($city_videos);
if (!empty($city_videos)) {
	$video = $city_videos[0];
	if ($video->isExternal()) {
		$smarty->assign("youtube_video", $video->agetExternalVideoHtmlCode(325, 275));
	} else {

		include_html_head("css", "//releases.flowplayer.org/7.0.2/skin/skin.css?b");
		include_html_head("js", "//releases.flowplayer.org/7.0.2/flowplayer.min.js");

		//first video is main video
//		echo "<pre>";var_dump($video);echo "</pre>";
		$smarty->assign("video", $video->getTemplateArray());
		//_darr($video->getTemplateArray());
//		echo "<pre>";var_dump($video->getTemplateArray());echo "</pre>";

		$smarty->assign("video_count", count($city_videos));
		//if there are more videos, put all videos (including main video) into playlist
		if (count($city_videos) > 1) {

			include_html_head("js", "/js/jquery.tools.scrollable.js");

			$vid_in_group = 2;
			$city_videos = video::getAllTemplateArrays($city_videos);
			$video_pl = array_chunk($city_videos, $vid_in_group);
//			_darr($video_pl);
		//	echo "<pre>";var_dump($video_pl);echo "</pre>";
			$smarty->assign("video_pl", $video_pl);
		}
	}
}
$smarty->assign("flowplayer_jscode", video::getFlowplayerJsCode("vid_city"));	//identifier must be in sync with template
$smarty->assign("city_video_upload_link", $ctx->location_path."/uploadVideo");

$smarty->assign("loc_id", $ctx->location_id);
$smarty->assign("loc_name", $ctx->location_name);


//---------------------------------
//get erotic services for this city
$es = $dir->getEroticServicesForCity();
$smarty->assign("eroticservices", $es);


//------------------------
//get forums for this city

$cat_names[] = "Erotic Massage Parlors & Masseuses";
$cat_names[] = "Female Escorts";
$cat_names[] = "Strip Clubs & Strippers";
$cat_names[] = "Adult Stores";
$cat_names[] = "TS / TV Shemale Escorts";
$cat_names[] = "Coffee Shop Blowjobs";
$cat_names[] = "Male Escorts";
$cat_names[] = "Escorts";
$cat_names[] = "LifeStyle Clubs";
$forum = $dir->getForumsForCity($cat_names);
$smarty->assign("forum", $forum);


$dir->exportQuickSearch();

//get location info
global $db;
if ($ctx->location_id) {
	$res = $db->q("SELECT loc_name, s, loc_url, description, loc_lat, loc_long, image_filenames, video_html
					FROM location_location 
					WHERE loc_id = ?",
					array($ctx->location_id));
	if ($db->numrows($res)) {
		$row = $db->r($res);
		$smarty->assign("location_label", $row["loc_name"].", ".$row["s"]);
		if ($row["description"])
			$smarty->assign("location_description", $row["description"]);
		if ($row["loc_lat"] && $row["loc_long"]) {
			$smarty->assign("location_lat", $row["loc_lat"]);
			$smarty->assign("location_lng", $row["loc_long"]);
		}
		if ($row["image_filenames"]) {
			$location_images = explode("|", $row["image_filenames"]);
			$smarty->assign("location_images", $location_images);
		}
		if ($row["video_html"])
			$smarty->assign("location_video_html", $row["video_html"]);
	}
}


//todo do we need these?
$smarty->assign("imageserver", "{$config_image_server}/place/");
$smarty->assign("homelink", $ctx->domain_base_link);
$smarty->assign("country", $ctx->country);

$smarty->assign("page_type", "city");

$smarty->display(_CMS_ABS_PATH."/templates/city_intl.tpl");

?>
