<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $ctx, $twig, $smarty, $config_image_server;

if (!permission::access("edit_all_places"))
	return;

$smarty->assign("nobanner", true);

global $id, $error, $types, $cols, $questionValues;

//init
$error = null;
$id = $_REQUEST["id"] ?: null;
$types = dir::getTypes();
$questions = [];
$questionValues = [];

if ($id) {
	//we are editing, load place attribute values and store them in $questionValues
	$ctx->node_id = $id;
	$ret = dir::loadNode();
	$questionValues = $ctx->item;
}

//get columns
if (($cols = dir::getColumns()) === false) {
   	echo "Something terrible happened!"; die;
}

function gQV($qn) {
	global $questionValues;
	if (!array_key_exists($qn, $questionValues))
		return null;
	$qv = trim($questionValues[$qn]);
	if ($qv == "")
		return null;
	return $qv;
}

function processSubmit() {
	global $id, $db, $error, $types, $questionValues, $config_image_path, $cols, $config_image_backup, $ctx;

//_darr($questionValues);
//_darr($cols);
	$now = time();
	$message = null;
	$new = false;
	if (!$id)
		$new = true;	//new place submission flag

	//replacing question values with submitted ones from form
	if (is_array($_REQUEST["qv"])) {
		foreach ($cols as $qn => $col) {
			if (in_array($qn, ["place_id"]))
				continue;
			if (isset($_REQUEST["qv"][$qn])) {
				$questionValues[$qn] = trim($_REQUEST["qv"][$qn]);
				//_d("setting '$qn' to '{$_REQUEST["qv"][$qn]}'");
			} else {
				$questionValues[$qn] = null;
				//_d("unsetting '$qn'");
			}
		}
	}

	//check mandatory attributes
	$place_type_id = intval($questionValues["place_type_id"]);
	$loc_id = intval($questionValues["loc_id"]);
	if (!$place_type_id) {
		$error = "Please select place type";
		return false;
	} else if (!in_array($place_type_id, array_keys($types))) {
		$error = "Invalid place type";
		return false;
	}
	if (!$questionValues["name"]) {
		$error = "Please enter place name";
		return false;
	}
	if (!$loc_id) {
		$error = "Please select location";
		return false;
	}
	$location = location::findOneById($loc_id);
	//TODO remove this when unified
	if (!$location->isInternational()) {
		//before unificatoin, this only works with international locations
		$error = "This only works for international locations";
		return false;
	}
	if ($location->getType() != 3) {
		//make sure location is city
		$error = "Invalid location type: {$location->getType()} !";
		return false;
	}

	//filter some other attributes
	$questionValues["phone1"] = preg_replace('/[^0-9\+]/', '', $questionValues["phone1"]);
	$questionValues["phone2"] = preg_replace('/[^0-9\+]/', '', $questionValues["phone2"]);
	$questionValues["fax"] = preg_replace('/[^0-9\+]/', '', $questionValues["fax"]);
	if ($questionValues["email"]) {
		$email = filter_var($questionValues["email"], FILTER_VALIDATE_EMAIL);
		if (!$email) {
			$error = "Invalid email";
			return false;
		}
		$questionValues["email"] = $email;
	}
	if ($questionValues["website"]) {
		$website = filter_var($questionValues["website"], FILTER_VALIDATE_URL);
		if (!$website) {
			$error = "Invalid website URL";
			return false;
		}
		$questionValues["website"] = $website;
	}
	$questionValues["loc_lat"] = preg_replace('/[^0-9\.]/', '', $questionValues["loc_lat"]);
	$questionValues["loc_long"] = preg_replace('/[^0-9\.]/', '', $questionValues["loc_long"]);
	$questionValues["edit"] = intval($questionValues["edit"]);
	if ($questionValues["edit"] < -1 || $questionValues["edit"] > 2) {
		$error = "Invalid edit status";
		return false;
	}

	//phone unique check
	if ($_REQUEST["phone1_skip_unique"] != "1") {
		$phone1 = $questionValues["phone1"];
		if ($phone1) {
			$params = [$phone1];
			$addition = "";
			if (!$new) {
				$addition = " AND p.place_id <> ? ";
				$params[] = $id;
			}
			$res = $db->q("SELECT p.place_id FROM place p WHERE p.phone1 = ? {$addition}", $params);
			if ($db->numrows($res) > 0) {
				$row = $db->r($res);
				$error = "Phone '{$phone1}' is already used by place #{$row["place_id"]}, perhaps you want to edit this place instead ?";
				return false;
			}
		}
	}

	//openhours processing
	$days = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];
	foreach ($days as $day) {
		if ($questionValues[$day."_closed"] == 1) {
			$questionValues[$day."_from"] = -1;
			$questionValues[$day."_to"] = -1;
		}
	}
	if ($questionValues["always"] == 1) {
		foreach ($days as $day) {
            $questionValues[$day."_from"] = null;
            $questionValues[$day."_to"] = null;
        }
	}

	//image processing
	$image = $thumb = null;
	$fileNameBodyBase = date("Y-m-d-H-i")."_".getRandomString(5);
	if ($new) {
		//insert, tmp filename
		$fileNameBody = "0".getRandomString(3)."_".$fileNameBodyBase;
	} else {
		$fileNameBody = $id."_".$fileNameBodyBase;
		$image = gQV("image");
		$thumb = gQV("thumb");
	}
	$dirpath_image = $config_image_path."/place";
	$dirpath_thumb = $dirpath_image."/t";
    if (!is_dir($dirpath_image))
        mkdir($dirpath_image);
    if (!is_dir($dirpath_thumb))
        mkdir($dirpath_thumb);
	if (!is_dir($config_image_backup))
		mkdir($config_image_backup);
	//check deleting of previous image/thumb
	if (!$new) {
		if ($_REQUEST["image_delete"] == "1" && $image && file_exists($dirpath_image."/".$image)) {
			@unlink($dirpath_image."/".$image);
			$image = null;
		}
		if ($_REQUEST["thumb_delete"] == "1" && $thumb && file_exists($dirpath_thumb."/".$thumb)) {
			@unlink($dirpath_thumb."/".$thumb);
			$thumb = null;
		}
	}
	//process uploaded image/thumb	
	$fileNameExt = "jpg";
	$imageUploaded = $thumbUploaded = false;
    if (isset($_FILES["image"]) && $_FILES["image"]["error"] != UPLOAD_ERR_NO_FILE) {
		$handle = new upload($_FILES["image"]);
		if (!$handle->uploaded) {
			$error = "Error while processing uploaded image";
			return false;
		}
		$imageUploaded = true;
		//determine filename etc...
		$image = "{$fileNameBody}.{$fileNameExt}";
		//fullsize version
		$handle->allowed = ["image/x-png", "image/gif", "image/jpeg", "image/pjpeg", "image/png"];
		$handle->image_convert = 'jpg';
		$handle->file_new_name_body = $fileNameBody;
		$handle->file_new_name_ext = $fileNameExt;
		$handle->file_safe_name = false;
		$handle->image_resize = true;
		$handle->image_x = 700;
		$handle->image_y = 500;
		$handle->image_ratio = true;
		//process
		$handle->process($dirpath_image);
		if (!$handle->processed) {
			$error = "Failed to process image";
			return false;
		}
		file_log("dir", "edit: submission: fullsize image converted successfully: filename={$image}");
		//store original image into backup
		$handle->allowed = ["image/x-png", "image/gif", "image/jpeg", "image/pjpeg", "image/png"];
		$handle->image_convert = 'jpg';
		$handle->file_new_name_body = $fileNameBody;
		$handle->file_new_name_ext = $fileNameExt;
		$handle->file_safe_name = false;
		$handle->image_resize = false;
		$handle->image_ratio = false;
		$handle->image_ratio_crop = false;
		$handle->process($config_image_backup);
        if (!$handle->processed) {
			//storing in backup is not a critical error, just send email to admin
			file_log("dir", "edit: submission: error: failed o store image in backup directory: config_image_backup='{$config_image_backup}', filename='{$image}'");
			reportAdmin("AS: dir-edit error", "Failed to store image into backup", ["account_id" => $account->getId(), "id" => $id, "config_image_backup" => $config_image_backup, "filename" => $image]);
        } else {
	        file_log("dir", "edit: submission: original image copied to backup dir: dir='{$config_image_backup}', filename='{$image}'");
		}
	}
    if (isset($_FILES["thumb"]) && $_FILES["thumb"]["error"] != UPLOAD_ERR_NO_FILE) {
		$handle = new upload($_FILES["thumb"]);
		if (!$handle->uploaded) {
			$error = "Error while processing uploaded thumb";
			return false;
		}
		$thumbUploaded = true;
	}
	if ($thumbUploaded || ($imageUploaded && !$questionValues["thumb"])) {
		//thumbnail
		$thumb = "{$fileNameBody}.{$fileNameExt}";
		$handle->image_resize = true;
		$handle->image_x = 327;
		$handle->image_y = 245;
		$handle->image_ratio = true;
		$handle->image_ratio_crop = true;
		$handle->image_convert = 'jpg';
		$handle->file_new_name_body = $fileNameBody;
		$handle->file_new_name_ext = $fileNameExt;
		$handle->file_safe_name = false;
		$handle->process($dirpath_thumb);
		if (!$handle->processed) {
			$error = "Failed to process thumbnail of wire image: '{$handle->error}'";
			return false;
		}
		$handle->clean();
		file_log("dir", "edit: submission: thumb set successfully: filename={$thumb}");
	}

	//db store main attributes
	if ($new) {
		//new place, insert
		$res = $db->q("
			INSERT INTO place
			(place_type_id, name, loc_id, address1, address2, zipcode
			, phone1, phone2, fax, email, website, facebook, twitter, instagram
			, loc_lat, loc_long, image, thumb, edit, created)
			VALUES
			(?, ?, ?, ?, ?, ?
			, ?, ?, ?, ?, ?, ?, ?, ?
			, ?, ?, ?, ?, ?, NOW())",
			[$place_type_id, $questionValues["name"], $loc_id, gQV("address1"), gQV("address2"), gQV("zipcode")
			, gQV("phone1"), gQV("phone2"), gQV("fax"), gQV("email"), gQV("website"), gQV("facebook"), gQV("twitter"), gQV("instagram"), 
			gQV("loc_lat"), gQV("loc_long"), $image, $thumb, gQV("edit")]
			);
		if ($db->affected($res) != 1) {
			$error = "Error while storing place";
			return false;
		}
		$id = $db->insertid($res);
		if (!$id) {
			$error = "Error while storing place";
			return false;
		}
		$message = "Place #{$id} - {$questionValues["name"]} was successfully inserted.";
		//updating image & thumb name to contain place_id
		if ($image || $thumb) {
			$image_path_old = $image_path_new = $thumb_path_old = $thumb_path_new = null;
			$fileNameBody = $id."_".$fileNameBodyBase;
			$newFilename = "{$fileNameBody}.{$fileNameExt}";
			$update = ""; $params = [];
			if ($image) {
				$update = " image = ?";
				$params[] = $newFilename;
				$image_path_old = $dirpath_image."/".$image;
				$image_path_new = $dirpath_image."/{$newFilename}";
			}
			if ($thumb) {
				$update .= ($update) ? ", ": "";
				$update .= " thumb = ?";
				$params[] = $newFilename;
				$thumb_path_old = $dirpath_thumb."/".$thumb;
				$thumb_path_new = $dirpath_thumb."/{$newFilename}";
			}
			$params[] = $id;
			$res = $db->q("UPDATE place SET {$update} WHERE place_id = ? LIMIT 1", $params);
			if ($db->affected($res) != 1) {
				file_log("dir", "edit: submission: failed to update image & thumb names: id={$id}, update={$update}, params=".print_r($params, true));
				flash::add(flash::MSG_ERROR, "Error while updating image/thumb on place, please contact administrator");
			} else {
				if ($image_path_old && $image_path_new) {
					$ret = rename($image_path_old, $image_path_new);
					if (!$ret) {
						file_log("dir", "edit: submission: failed to rename image: image_path_old={$image_path_old}, image_path_new={$image_path_new}");
						flash::add(flash::MSG_ERROR, "Error while updating image/thumb on place, please contact administrator");
					} else {
						file_log("dir", "edit: submission: image renamed: image_path_old={$image_path_old}, image_path_new={$image_path_new}");
					}
				}
				if ($thumb_path_old && $thumb_path_new) {
					$ret = rename($thumb_path_old, $thumb_path_new);
					if (!$ret) {
						file_log("dir", "edit: submission: failed to rename thumb: thumb_path_old={$thumb_path_old}, thumb_path_new={$thumb_path_new}");
						flash::add(flash::MSG_ERROR, "Error while updating image/thumb on place, please contact administrator");
					} else {
						file_log("dir", "edit: submission: thumb renamed: thumb_path_old={$thumb_path_old}, thumb_path_new={$thumb_path_new}");
					}
				}
			}
		}
	} else {
		//update
		file_log("dir", "edit: submission: main update: place_type_id={$place_type_id}, name={$questionValues["name"]}, loc_id={$loc_id}, image={$image}, thumb={$thumb}, place_id={$id}");
		$res = $db->q("
			UPDATE place
			SET place_type_id = ?, name = ?, loc_id = ?, address1 = ?, address2 = ?, zipcode = ?
				, phone1 = ?, phone2 = ?, fax = ?, email = ?, website = ?, facebook = ?, twitter = ?, instagram = ?
				, loc_lat = ?, loc_long = ?, image = ?, thumb = ?, edit = ?, updated = ?
			WHERE place_id = ?
			LIMIT 1",
			[$place_type_id, $questionValues["name"], $loc_id, gQV("address1"), gQV("address2"), gQV("zipcode")
			, gQV("phone1"), gQV("phone2"), gQV("fax"), gQV("email"), gQV("website"), gQV("facebook"), gQV("twitter"), gQV("instagram")
			, gQV("loc_lat"), gQV("loc_long"), $image, $thumb, gQV("edit"), $now
			, $id]
			);
		if ($db->affected($res) != 1) {
			$error = "Error while updating place";
			return false;
		}
		file_log("dir", "edit: submission: main update: success");
		$message = "Place #{$id} - {$questionValues["name"]} was successfully updated.";
	}

	//store image backup info to db
	if ($imageUploaded) {
		if ($new) {
			//we also need to rename backup image
			$backup_path_old = $config_image_backup."/".$image;
			$backup_path_new = $config_image_backup."/{$newFilename}";
			$ret = rename($backup_path_old, $backup_path_new);
			if (!$ret) {
				file_log("dir", "edit: submission: failed to rename backup_image: backup_path_old={$backup_path_old}, backup_path_new={$backup_path_new}");
			} else {
				file_log("dir", "edit: submission: backup_image renamed: backup_path_old={$backup_path_old}, image_path_new={$backup_path_new}");
				$image = $newFilename;
			}
		}
		$res = $db->q("INSERT INTO image_backup (id, filename, module, `time`) VALUES (?, ?, ?, NOW());", [$id, $image, "place"]);
		if ($db->affected($res) != 1) {
	        file_log("dir", "edit: submission: error: failed to insert entry into image_backup table: filename='{$image}'");
		}
	}

	//db store extra attributes
	foreach ($cols as $col) {

		if (!array_key_exists("col_datatype", $col))
			continue;	//this is base attribute, not extra

		$attribute_id = $col["attribute_id"];
		$old_val = $ctx->item[$col["col_name"]];
		$new_val = trim($questionValues[$col["col_name"]]);
		//_d("attribute_id=$attribute_id old_val=$old_val new_val=$new_val");
		if (!$new && $old_val && !$new_val) {
			//if update, then if attribute value was deleted/emptied, then delete the entry from place_attribute table
			$res = $db->q("DELETE FROM place_attribute WHERE place_id = ? AND attribute_id = ? LIMIT 1", [$id, $attribute_id]);
			
		} else if ($new_val && $old_val != $new_val) {
			//else if we specified value and it is different than old value, upsert it
			$value_colname = "value";
			if ($col["col_datatype"] == "text")
				$value_colname = "value_text";
			//TODO performance optimize this - we dont need to do all these select counts -> add unique key and change to upsert (on duplicate key)
			if ($db->single("SELECT COUNT(*) FROM place_attribute WHERE place_id = ? AND attribute_id = ?", [$id, $attribute_id]) > 0) {
				$res = $db->q(
					"UPDATE place_attribute SET {$value_colname} = ? WHERE place_id = ? AND attribute_id = ? LIMIT 1",
					[$new_val, $id, $attribute_id]
					);
			} else {
				$res = $db->q("
					INSERT INTO place_attribute 
					(place_id, attribute_id, {$value_colname}) 
					VALUES 
					(?, ?, ?)",
					[$id, $attribute_id, $new_val]
					);
			}
		}
	}

	//rotate dir index
	rotate_index("dir");
	
	return $message;
}

if ($_REQUEST["submit"] == "Submit") {
	$ret = processSubmit();
	if ($ret) {
		//submission was successfull, redirect to edit place page again
		return success_redirect($ret, "/dir/edit?id={$id}");
	}
}


$questions[] = [
	"type" => "select",
	"name" => "place_type_id",
	"label" => "Type",
	"options" => dir::getTypes(),
	"mandatory" => true,
	];
$questions[] = [
	"type" => "string",
	"name" => "name",
	"label" => "Name",
	"mandatory" => true,
	];
$questions[] = [
	"type" => "location",
	"name" => "loc_id",
	"label" => "Location",
	"mandatory" => true,
	];

foreach ($cols as $col) {
	_darr($col);
   	//dir::displayEditCol($col, $form, true);
	$question = [
		"datatype" => $col["col_datatype"],
		"name" => $col["col_name"],
		"label" => $col["col_title"],
		];
	switch ($col["col_name"]) {
        case 'place_type_id':
		case 'name':
        case 'always':
            break;
        case 'monday_from':
			$question["input_type"] = "hours";
			$question["label"] = "Hours of Operation";
			$question["options_from"] = dir::getHourOptions();
			$question["options_to"] = dir::getHourOptions(true);
            break;
		case 'address1':
        case 'address2':
        case 'zipcode':
        case 'phone1':
        case 'phone2':
        case 'fax':
        case 'email':
        case 'website':
        case 'facebook':
        case 'twitter':
        case 'instagram':
        case 'loc_lat':
        case 'loc_long':
			$question["input_type"] = "string";
			break;
        case 'image':
			$question["input_type"] = "image";
			$question["image"] = $ctx->item["image"];
			$question["imageUrl"] = $config_image_server."/place/".$ctx->item["image"];
			$question["thumb"] = $ctx->item["thumb"];
			$question["thumbUrl"] = $config_image_server."/place/t/".$ctx->item["thumb"];
			break;
		default:
			switch ($col["col_datatype"]) {
				case 'boolean':
					$question["input_type"] = "checkbox";
					break;
				case 'boolean3':
					$question["input_type"] = "select";
					$question["options"] = ["1" => "No", "2" => "Yes"];
					break;
				case 'number':
				case 'string':
					$question["input_type"] = "string";
					break;
				case 'text':
					$question["input_type"] = "text";
					break;
				case 'enum':
					$options = [];
					$items = explode('|', $col["col_values"]);
					foreach ($items as $item) {
						$i = explode('@', $item);
						$options[$i[0]] = $i[1];
					}
					$question["input_type"] = "select";
					$question["options"] = $options;
					break;
				case 'fee':
					$options = dir::getFeeOptions($ctx->item["country_id"]);
					$question["input_type"] = "select";
					$question["options"] = $options;
    	            break;
				case 'multi':
//					die("Please contact Jay multi");
					break;
        	    case 'fee_range':
					//die("Please contact Jay fee range".print_r($col, true));
					$options = dir::getFeeOptions($ctx->item["country_id"]);
					$question["input_type"] = "select_from_to";
					$question["options"] = $options;
					break;
				default:
					break;
			}
			break;
	}
	if (in_array($col["col_name"], ["zipcode", "loc_lat", "loc_long"])) {
		$question["style"] = "width: 150px;";
	} elseif (in_array($col["col_name"], ["phone1", "phone2", "fax"])) {
		$question["style"] = "width: 200px;";
	}
	$questions[] = $question;
}
_darr($questions);

$questions[] = [
	"type" => "select",
	"name" => "edit",
	"label" => "Status",
	"options" => ["-2" => "Requested to be removed", "0" => "New Added", "1" => "Live"],
	];

$params = [
	"error" => $error,
	"id" => $id,
	"types" => $types,
	"type_name" => $ctx->item["place_type_id"] ? $types[$ctx->item["place_type_id"]] : null,
	"questions" => $questions,
	"question_values" => $questionValues,
	"loc_label" => $ctx->item["loc_label"] ?: null,
	"phone1_skip_unique" => !in_array($ctx->item["country_id"], [16046,16047,41973]),
	"place_view_url" => ($id) ? dir::getPlaceLink(null, true) : null,
	];

_darr($questionValues);

$template = $twig->load("place/edit.html.twig");
$html = $template->render($params);
echo $html;
return;


/*
$c = new CColumnenum("edit", "Status");
$c->AddEnumOption("-2", "Requested to be removed");
$c->AddEnumOption("0", "New Added");
$c->AddEnumOption("1", "Live");
$c->setDefaultValue(1);
$form->AppendColumn($c);

$form->show_remove = true;
*/
?>
