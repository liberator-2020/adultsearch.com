<?php
defined("_CMS_FRONTEND") or die("No Access");

global $db, $ctx, $smarty, $gTitle, $gDescription, $config_image_server, $gCanonicalUrl;
$dir = new dir;

if (isset($_REQUEST["ipp"]))
	dir::setItemsPerPage($_REQUEST["ipp"]);

location::setDefaultLocation($ctx->city_id);

$dir->ShowAd();

$gTitle = $dir->getTitle();
$gDescription = $dir->getMetaDescription();
$gCanonicalUrl = $ctx->domain_link.$ctx->location_path."/".$ctx->category;

//seo
$res = $db->q("SELECT description FROM seo WHERE location_id = ? AND type = 'LPT' AND place_type_id = ? LIMIT 1", [$ctx->location_id, $ctx->category_id]);
if ($db->numrows($res)) {
    $row = $db->r($res);
    $smarty->assign("category_description", $row["description"]);
}

$refine = array();

$locations = $dir->getRefineLocations();
//_darr($locations);
if (!empty($locations)) {
	$refine[] = [
		"name" => "Location", 
		"col" => 1,
		"alt" => $locations
		];
}

if (is_null($ctx->category_id)) {
	$refine[] = $dir->getRefineCategories();
}

$facets = $dir->getRefineFacets();
//_darr($facets);
if (!empty($facets))
	$refine = array_merge($refine, $facets);

$smarty->assign("refine", $refine);

$smarty->assign("filter", $dir->getFilters());

$smarty->assign("search_query_link", $dir->getSearchQueryLink());

$res = $dir->getListRows();
while($row=$db->r($res)) {

	$extra = "{$dir->getCategoryNameById($row["place_type_id"])}<br/>";
	$id = $row["place_id"];

	$extra .= $row["edit"] < 1 ? "<span class='nonerotic'>*Not Live*</span>" : NULL;

	$address = $row["address1"].($row["address2"]?" ({$row["address2"]})":"");

	$locname = "{$row["loc_name"]}, {$row["state"]}";
	$thumbpath = !empty($row["thumb"]) ? "{$config_image_server}/place/t/{$row["thumb"]}" : "/images/placeholder.gif";
	$ra = "{$config_image_server}/avatar/" .  (!empty($row["ra"]) ? $row["ra"] : "no_avatar.jpg");

	$company[] = array(
		"id" =>           $id,
		"extra" =>        $extra,
		"name" =>         $row["name"],
		"loc_name" =>     $row["loc_name"],
		"phone" =>        $dir->getPhone(),
		"thumb" =>        $row["thumb"],
		"address" =>      $address,
		"review" =>       $row["review"],
		"recommend" =>    $row["recommend"],
		"edit" =>         $row["edit"],
		"sponsor" =>      '',
		"url" =>          $dir->getPlaceLink($row),
		"longitude" =>    $row['loc_long'],
		"latitude" =>     $row['loc_lat'],
		"thumbpath" =>    $thumbpath,
		"lr" =>           $row['last_review_id'],
		"rc" =>           $row['rc'],
		"ra" =>           $ra,
		"video" =>        ($row["video_cnt"] > 0) ? true : false,
	);
}

$smarty->assign("mapjson", json_encode($company));

$smarty->assign("total", $dir->getTotalItems());
$smarty->assign("sc", $company);

$smarty->assign("pager_array", $dir->get_pager_array($dir->getTotalItems(), dir::getItemsPerPage(), $ctx->path_no_page));
$smarty->assign("paging", $dir->paging($dir->getTotalItems(), dir::getItemsPerPage(), true, true, $ctx->path_no_page));
$smarty->assign("ipp", dir::getItemsPerPage());
$smarty->assign("sort_array", $dir->getSortArray());

$smarty->assign("link", $ctx->category);
$smarty->assign("category", $ctx->category);

if (permission::has("edit_all_places"))
	$smarty->assign('display_add_link', true);

$smarty->assign("search_nozip", true);
$smarty->assign("what", $ctx->location_name." ".dir::getCategoryPlural($ctx->category_name));
$smarty->assign("place_type_id", $ctx->category_id);
$smarty->assign("worker_link", $ctx->country);
$smarty->assign("image_path", "place");
$smarty->assign("place_link", "place");
$smarty->assign("imageserver", "{$config_image_server}/place/");
$smarty->assign("homelink", $ctx->domain_base_link);

$dir->exportQuickSearch();

forum::exportPlaceListVars();

// Receiving sidebar sponsor ads
// 1 - is static type for places page
$classifieds = new classifieds();
$side = $classifieds->getSideSponsors(1, $ctx->location_id);

$smarty->assign("side", $side);

$smarty->display(_CMS_ABS_PATH."/templates/place/place_list.tpl");

?>
