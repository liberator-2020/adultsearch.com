<?php

defined("_CMS_FRONTEND") or die("No Access");

require_once(_CMS_ABS_PATH."/inc/classes/class.dir.php");
$dir = new dir();

global $smarty, $gTitle, $gDescription, $ctx, $db, $account, $config_mapquest_key, $mobile, $config_site_url; //, $config_image_server;

$map_limit  = 500;
$map_radius = 30; // mile
$map_zoom   = 10;

//location::setDefaultLocation($ctx->city_id);

$gTitle       = $dir->getTitle();
$gDescription = $dir->getMetaDescription();

$filter_types = array();

if (!empty($_REQUEST["type"])) {
	$filter_types = array_map('intval', explode(',', $_REQUEST["type"]));
}
if (!empty($_REQUEST["loc_lat"]) && !empty($_REQUEST["loc_long"])) {
	$loc_array = array('loc_lat' => floatval($_REQUEST["loc_lat"]), 'loc_long' => floatval($_REQUEST["loc_long"]));
} else {
	$loc_array = array('loc_lat' => $ctx->location_lat, 'loc_long' => $ctx->location_long);
}
if (!empty($_REQUEST["zoom"])) {
	$map_zoom = intval($_REQUEST["zoom"]);
}

$res = $dir->getListMarkers($filter_types, $map_limit, $map_radius, $loc_array);

$company = array();
while ($row = $db->r($res, MYSQL_ASSOC)) {
	if ($row['loc_lat'] > 0 || $row['loc_long'] > 0) { // @todo move to getListMarkers
		$id   = $row["place_id"];
		$link = $ctx->domain_link.$dir->getPlaceLink($row);

		$company[] = array(
			"id"           => $id,
			"name"         => $row["name"],
			"linkoverride" => $link,
			"longitude"    => $row['loc_long'],
			"latitude"     => $row['loc_lat'],
		);
	}
}

$smarty->assign("map_markers", json_encode($company));

//MapQuest and Leaflet include
include_html_head('js', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.js');
include_html_head('css', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.css');
include_html_head('js', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.js');
include_html_head('css', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.css');

include_html_head('js', 'https://unpkg.com/leaflet.markercluster@1.0.6/dist/leaflet.markercluster.js');
include_html_head('css', 'https://unpkg.com/leaflet.markercluster@1.0.6/dist/MarkerCluster.css');
include_html_head('css', 'https://unpkg.com/leaflet.markercluster@1.0.6/dist/MarkerCluster.Default.css');

include_html_head('css', "{$config_site_url}/css/prefixed-bootstrap.css");
include_html_head('css', 'https://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css');
include_html_head('js', 'https://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js');

$smarty->assign("categories", $dir->getTypesForCountry($ctx->country_id));
$smarty->assign("filter_types", $filter_types);
$smarty->assign("location", $ctx->loc_name);
$smarty->assign("location_lat", $loc_array['loc_lat']);
$smarty->assign("location_long", $loc_array['loc_long']);

$smarty->assign("map_public_key", $config_mapquest_key);
$smarty->assign("map_zoom", $map_zoom);

$smarty->display(_CMS_ABS_PATH."/templates/place/place_list_map.tpl");

?>
