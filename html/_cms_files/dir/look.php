<?php

defined("_CMS_FRONTEND") or die("No Access");

global $smarty, $gTitle, $gDescription, $gItemID, $ctx, $db, $account, $at_least_one_fee, $config_image_server, $config_dev_server;

$dir = new dir;

location::setDefaultLocation($ctx->city_id);

//global $account;
//if ($account->isrealadmin())
	$dir->exportQuickSearch();

$at_least_one_fee = false;

$row = $dir->loadNode();
if ($row == false) {
	echo "Error loading place!<br />";
	system::go("/");
}

if ($ctx->item["deleted"]) {
	system::go("/");
}

if (!empty($_REQUEST["back_url"])) {
	$smarty->assign("back_url", str_replace(['http:','https:','//'],'', urldecode($_REQUEST["back_url"])));
}

$dir->showAd();

//exception - KTV bars in angeles city
if ($ctx->city == "angeles-city" && $ctx->category == "ktv-bar" && $row["info"] == "")
	$row["info"] = "General Notice : The following is some general information about procedures that seems to pertain to all K Bars. First NO SEX in the K-Bar with K-Bar hostesses. You can get together with them after work if they want. They are their just to keep you company and pour you drinks. This is very typical asian K-Bar edict. Second you can pick who you want to hang out with.They will parade them all for you when you arrive.Then you pick the girls you want to hang with.";

$gTitle = $dir->getTitle();
$gDescription = $dir->getMetaDescription();

$smarty->assign("hours", $dir->getHours(($row["byapponly"] == 1) ? true : false));
$smarty->assign("current_weekday", strtolower(date("l")));

//TODO redol later to get column position and order from database
$col1_attrs = array(
	"orientation",
	"open_holidays",
	"happy_hours",
	"pickup_dropoff",
	"parking",
	"parking3", 
	"reservations",
	"food",
	"clubtype",
	"lapdance",
	"rate_hour",
	"rate_nite",
	"just_drinking_bar",
	"have_bar_fine",
	"lots_of_girls",
	"few_girls",
	"free_food",
	"live_entertainment",
	"coyote_dancers",
	"cover",
	"facilities",
	"services",
	"dancers",
	"info",
	"nude_topless_dancing",
	"hot_buffet",
	"cold_buffet",
	"alcoholic",
	"offers_extra_services",
	"offers_sex",
	"take_out",
	"private_room",
	"vip_room",
	"table_shower",
	"rate_table_shower",
	"sauna",
	"jacuzzi",
	"truckparking",
	"private_parking",
	"fourhand",
	"soapy_massage",
	"bar_fee",
	"girl_fee",
	"thai_massage_fee",
	"foot_massage_fee",
	"oil_massage_fee",
	"aroma_massage_fee",
	"aroma_milk_fee",
	"hot_oil_fee",
	"thai_herbal_hot_compress_fee",
	"herbal_steam_fee",
	"head_neck_shoulder_massage_fee",
	"foot_scrub_fee",
	"body_scrub_fee",
	"slimming_massage_fee",
	"aloe_vera_fee",
	"facial_treatment_fee",
	"manicure_pedicure_fee",
	"bar_fine",
	"girl_fine",
	"bar_fine_out",
	"girl_fine_out",
	"short_time_room",
	"short_time_room_fee",
	"ladies_drink",
	"double_ladies_drink",
	"fetish",
	"lingerie",
	"adult_toys",
	"adult_dvds",
	"peep_shows",
	"adult_books",
	"adult_video_arcade",
	"theaters",
	"rate_15",
	"rate_30",
	"rate_45",
	"rate_60",
	"rate_90",
	"rate_120",
	"cc",
	"admission",
	"model_prices",
	"duration_of_massage",
	"fishbowls",
	"number_girls_fishbowl",
	"number_girls_lobby",
	"quality_girls_fishbowl",
	"quality_girls_lobby",
	"quality_massage",
	"would_recommend",
	"soapy_massage_fee",
	"ethnicity",
	"overall_quality_girls",

	'coupon',
	'coupon_value',
	'location_detail',
	'discounts',
//	'cc_visa',
//	'cc_master',
//	'cc_ae',
//	'cc_dis',
	'masseuse',
	'byapponly',
//	'nonerotic',
	'phone_vip',
	'dancer',
	'bar_service',
	'vip',
	'vipfee_enum',
	'privatedance_fee',
	'privatedancevip',
	'age_18',
	'gloryhole',
	'theater_single_fee',
	'theater_couple_fee',
	'lgbt',
	'buddy_booth_available',
	'adultmovie',
	'toyshow',
	'daypass',
	'mem_1month',
	'mem_3month',
	'mem_6month',
	'mem_annual',
	'locker',
	'stdroom',
	'viproom',
	'slingroom',
	'gym',
	'pool',
	'showers',
	'internet',
//	'cc_visa_master',
//	'cc_atm',
	'cover_men',
	'cover_women',
	'cover_couple',
	'entry',
//	'private_address',
);

$columns = dir::getColumns();

function add_attributes_to_columns(&$col, $attr, $columns, $row) {
	global $smarty, $ctx;

	$col_datatype = $columns[$attr]["col_datatype"];
	$col_title	= dir::getAttributeTitle($attr, $columns[$attr]["col_title"]);
	if (is_null($col_datatype) || is_null($col_title)) {
		return;
	}

	//special descriptive columns
	if ($attr == "info" && !empty($row["info"])) {
		$smarty->assign("info", $row["info"]);
		return;
	}
	if ($attr == "facilities" && !empty($row["facilities"])) {
		$smarty->assign("facilities", $row["facilities"]);
		return;
	}

	switch ($col_datatype) {
		case 'boolean':
			dir::look_array_add_bool($col, $row, $attr, $col_title);
			break;
		case 'boolean3':
			dir::look_array_add_bool3($col, $row, $attr, $col_title);
			break;
		case 'fee':
			dir::look_array_add_fee($col, $row, $attr, $col_title);
			if ($ctx->country == "thailand" && $attr == "bar_fine") {
				$item = array_pop($col);
				if ($item["label"] == "Bar Fine") {
					$item["val"] = $item["val"]." and on up";
					$col[] = $item;
				}
			}
			break;
		case 'fee_range':
			dir::look_array_add_fee_range($col, $row, $attr, $col_title);
			break;
		case 'cover':
			dir::look_array_add_cover($col, $row, $attr, $col_title);
			break;
		case 'enum';
			$array = array();
			$items = explode('|', $columns[$attr]["col_values"]);
			foreach ($items as $item) {
				$i = explode('@', $item);
				if (count($i) == 3)
					$array[$i[0]] = $i[2];
				else
					$array[$i[0]] = $i[1];
			}
			dir::look_array_add_array($col, $row, $attr, $array, $col_title);
			break;
		case 'multi';
			$array = array();
			$items = explode('|', $columns[$attr]["col_values"]);
			foreach ($items as $item) {
				$i = explode('@', $item);
				if (count($i) == 3)
					$array[$i[0]] = $i[2];
				else
					$array[$i[0]] = $i[1];
			}
			dir::look_array_add_multi($col, $row, $attr, $array, $col_title);
			break;
		case 'number':
		case 'string':
		case 'text':
			if($attr == "coupon" && !empty($row[$attr])) {
				dir::look_array_add_coupon($col, $row, $attr, $col_title);
				break;
			}
			dir::look_array_add_text($col, $row, $attr, $col_title);
			if ($ctx->category == "erotic-massage-parlor" && $attr == "dancers" && !empty($row[$attr])) {
				$item = array_pop($col);
				$item["label"] = "Providers";
				$col[] = $item;
			}
			break;
		default:
			return;
			break;
	}
}
foreach ($col1_attrs as $attr)
	add_attributes_to_columns($c1, $attr, $columns, $row);

//-------
//special

//special UAE
if ($ctx->country == "uae" && $ctx->category=="erotic-massage-parlor")
		$c1[] = array("label" => "Masseuse", "val" => "Asian");

dir::look_array_add_creditcards($c1, $row);

$smarty->assign("c1", $c1);

if ($at_least_one_fee && currency_convert::isConvertible(dir::getCurrencyCodeByCountry())) {
	include_html_head("js", "/js/jquery.cookie.js");
	include_html_head("js", "/js/currency_convert.js");
	include_html_head("css", "/css/currency_convert.css");
	$cc_b_html = $smarty->fetch(_CMS_ABS_PATH."/templates/currency_convert/currency_convert_button.tpl");
	$smarty->assign("display_currency_button", true);
	$smarty->assign("currency_button", $cc_b_html);
}

$smarty->assign("name", $row["name"]);
$smarty->assign("loc_name", $row["loc_name"]);
$smarty->assign("overall", $row["overall"]);
$smarty->assign("phone", $dir->getPhone());
$smarty->assign("location", $dir->getLoc());
$smarty->assign("id", $ctx->node_id);
$smarty->assign("totalreview", $row["review"]);
$smarty->assign("review_pager", $dir->paging($row["review"], 10, false, true, $ctx->path_no_page));	//override variable what was filled in class.review->reviews()
include_html_head("css", "/css/info.css");
//include_html_head("js","//maps.googleapis.com/maps/api/js?key=AIzaSyCwh_-7FS9QabY_RjOzmC8Cv38GiVMCRS0&amp;sensor=false");

//MapQuest and Leaflet include
include_html_head('js', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.js');
include_html_head('css', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.css');
include_html_head('js', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.js');
include_html_head('css', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.css');

//TODO move video code to dir class ?
$videos = $dir->getVideosForPlace();
//_darr($videos);
if (!empty($videos)) {
	$video = $videos[0];
	if ($video->isExternal()) {
		$smarty->assign("youtube_video", $video->agetExternalVideoHtmlCode(350, 210));
	} else {
		include_html_head("css", "//releases.flowplayer.org/7.0.2/skin/skin.css?b");
        include_html_head("js", "//releases.flowplayer.org/7.0.2/flowplayer.min.js");
		//first video is main video
		$smarty->assign("video", $video->getTemplateArray());

		$smarty->assign("video_count", count($videos));

		//if there are more videos, put all videos (including main video) into playlist
		if (count($videos) > 1) {
			include_html_head("js", "//cdn.jquerytools.org/1.2.7/all/jquery.tools.min.js");
			$vid_in_group = 3;
			$videos = video::getAllTemplateArrays($videos);
			$video_pl = array_chunk($videos, $vid_in_group);
//		  _darr($video_pl);
			$smarty->assign("video_pl", $video_pl);
		}
	}
	//TODO add back not live warnings
}
$smarty->assign("flowplayer_jscode", video::getFlowplayerJsCode("vid_place"));   //identifier must be in sync with template


$addressLx = $dir->getAddressLx();
if ($row["nonerotic"]) {
	$addressLx = "<span class='error'>*Non-Erotic Massage Reported*</span><br/>".$addressLx;
	$smarty->assign("nonerotic","<span class='error'>*Non-Erotic Massage Reported*</span>");
}
$smarty->assign("address", $addressLx);

$map = prepareMap($ctx->node_id, 'place', $row, 'place', 'place');
$smarty->assign("map", $map);
//map update link
$smarty->assign("link_map_update", $ctx->path_original."/map?update=1");


//------
//photos
$photos = [];
if ($row["image"] != "" && $row["thumb"] != "") {
	$photos["image"] = "{$config_image_server}/place/{$row["image"]}";
	$photos["thumb"] = "{$config_image_server}/place/t/{$row["thumb"]}";
	$pics = $dir->getExtraPics("{$config_image_server}/place");
	if ($pics) {
		include_html_head("js", "/js/jquery.tools.scrollable.js");
    	$pics_in_group = 2;
        $pics_groups = array_chunk($pics, $pics_in_group);
		$photos["pics"]  = $pics_groups;
	}
}
$smarty->assign("photos", $photos);


$smarty->assign("category", $ctx->category);
$smarty->assign("quickread", $dir->getQuickreadReviews());
$smarty->assign("allowcomment", true);
$smarty->assign("review_link_add", $ctx->node_id."/add_review");
$smarty->assign("link_uploadVideo", $ctx->node_id."/uploadVideo");

$smarty->assign("forum_topic", forum::getLastTopicsForPlace($ctx->node_id));
$smarty->assign("new_topic_link", forum::getNewTopicLinkForCategory(NULL, NULL));

if (permission::has("edit_all_places"))
	$smarty->assign("can_edit_place", true);

if (!empty($row["owner"]) && ($row["owner"]==$account->getId()||$account->isrealadmin()))
	$smarty->assign("isowner", true);

if (empty($row["owner"])) {
	$smarty->assign("business_link", true);
}

$smarty->assign("editlink", $ctx->country);

$smarty->assign("is_vip", $account->isVip());
$smarty->assign("config_env", $config_dev_server);
$smarty->display(_CMS_ABS_PATH."/templates/place/place_place.tpl");

?>
