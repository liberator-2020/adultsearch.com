<?php

//TODO working for all locations

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $ctx, $account;

if( !($account_id = $account->isloggedin()) ) {
	$account->asklogin();
	return;
}

require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.dir.php");

$form = new data;
if ($ctx->country == NULL) {
	echo "Country not set!"; die;
}

if (isset($_REQUEST["id"])) {
	$form->itemID = GetRequestParam("id");

//load place to row array
	$form->loadRow();
}

$is_owner = !empty($form->row["owner"]) && ($form->row["owner"] == $account_id);
if (!permission::access("edit_all_places") && !$is_owner) {
	return;
}

$form->table = "place";
//get columns
if (($cols = dir::getColumns()) === false) {
   	echo "Something terrible happened!"; die;
}
foreach ($cols as $col) {
   	dir::displayEditCol($col, $form, false, $form->row["place_type_id"]);
}
//new interface files
$c = new CColumnPicture("place_picture", "Extra Picture", "place");
$c->setPath("{$form->table}");
$c->setPathT("{$form->table}/t");
$form->AppendColumn($c);

$form->cache_update = "dir";

//skip phone check on international places
//$_POST["phonesuphone1"] = 1;

$form->owner_mode = true;
$form->owner_link = dir::getPlaceLink($form->row);
$form->ShowPage();

return; 

?>
