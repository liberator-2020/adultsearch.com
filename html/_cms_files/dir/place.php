<?php
defined("_CMS_FRONTEND") or die("No Access");

global $db;

$id = intval($_REQUEST["id"]);
if (!$id) {
	system::go("http://adultsearch.com", "No id defined");
	die;
}

$res = $db->q("select loc_id, place_type_id, name FROM place WHERE place_id = ?", array($id));
if (!$db->numrows($res)) {
	system::go("http://adultsearch.com", "Place with id #{$id} not found.");
    die;
}
$row = $db->r($res);
$loc = location::findOneById($row["loc_id"]);
if ($loc == NULL) {
	system::go("http://adultsearch.com", "Location with id {$row["loc_id"]} not found for place #{$id}.");
	die;
}

system::moved($loc->getUrl()."/".dir::getCategoryUrlNameById($row["place_type_id"])."/".name2url($row["name"])."/{$id}");

?>
