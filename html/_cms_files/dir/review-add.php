<?php
defined('_CMS_FRONTEND') or die('Restricted access !');

global $ctx;

if (!$ctx->node_id || !$ctx->country)
	$id = intval($_REQUEST["id"]);
else
	$id = $ctx->node_id;

if ($id == 0)
	system::go("/");

$review = new review("place", $id, $ctx->country." place", "", "place");

$review->process();

