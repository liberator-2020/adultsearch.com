<?php
/**
 * Updates static map image for specified place and returns image data
 */
defined("_CMS_FRONTEND") or die("No Access");

global $db, $config_image_path, $config_mapquest_key, $config_dev_server;

function no_map_image() {
	$filename = _CMS_ABS_PATH."/images/no_map.png";
	return  dump_image($filename);
}
function create_map_image_filename($module, $id) {
	$filename = "{$id}.png";
	return $filename;
}
function create_map_image_filepath($module, $id) {
	global $config_image_path;
	$dirpath = $config_image_path."/{$module}/maps";
	if (!is_dir($dirpath)) {
		$ret = @mkdir($dirpath, 0777, true);
		if (!$ret)
			return false;
	}
	$filename = create_map_image_filename($module, $id);
	$filepath = "{$dirpath}/{$filename}";
	return $filepath;
}
function get_map_image_filepath($module, $map_image) {
	global $config_image_path;
	$dirpath = $config_image_path."/{$module}/maps";
	$filepath = "{$dirpath}/{$map_image}";
	return $filepath;
}
function dump_image($filepath) {
	$h = fopen($filepath, 'rb');
	header("Content-Type: image/png");
	header("Content-Length: " . filesize($filepath));
	fpassthru($h);
	exit;
	return true;
}

$lat = preg_replace('/[^0-9\.\-]/', '', $_REQUEST["lat"]);
$lng = preg_replace('/[^0-9\.\-]/', '', $_REQUEST["lng"]);
$module = $_REQUEST["module"];
$id = intval($_REQUEST["id"]);

if (!$lat || !$lng || !$module || !$id) {
	debug_log("dir:updatemap: Incorrect params (1) !");
	return no_map_image();
}

if (!in_array($module, ["place", "eroticmassage", "strip_club2", "adult_stores", "brothel", "gay", "gaybath", "lifestyle", "lingeriemodeling1on1"])) {
	debug_log("dir:updatemap: Incorrect params (2) !");
	return no_map_image();
}

$id_col_name = "id";
if ($module == "place")
	$id_col_name = "place_id";

//check if image is not already downloaded
$res = $db->q("SELECT map_image FROM {$module} WHERE {$id_col_name} = ? LIMIT 1", [$id]);
if ($db->numrows($res) != 1) {
	debug_log("dir:updatemap: Incorrect params (3) !");
	return no_map_image();
}
$row = $db->r($res);
$map_image = $row["map_image"];
if ($map_image) {
	$map_image_filepath = get_map_image_filepath($module, $map_image);
	if (file_exists($map_image_filepath)) {
		return dump_image($map_image_filepath);
	} else {
		//file does not exist! this should not normally happen - redownload the map
		debug_log("dir:updatemap: map image file '{$map_image_filepath}' does not exist but in DB present !");
	}
}

//Download image form MapQuest
$mapGenerator = new MapQuestMapGenerator($config_mapquest_key);
$url          = $mapGenerator->getStaticImageUrlForCoordinates($lat, $lng);
//file_log("mapquest", "dir/updatemap: url='{$_SERVER["REQUEST_URI"]}', static_image_url='{$url}'");
$curl         = new curl();

if($config_dev_server) {
	debug_log("dir:updatemap: Mapquest static Map request : ".$url);
}

try{
		$resp         = $curl->getImage($url, true);
}catch (GetMapImageException $exception){
	debug_log("dir:updatemap: error : {$exception->getCode()}#{$exception->getMessage()} ");
	return no_map_image();
}
if ($curl->errno || $curl->error) {
	debug_log("dir:updatemap: error : {$curl->errno}#{$curl->error} ");
	return no_map_image();
}
//save image to disk
$map_image_filename = create_map_image_filename($module, $id);
$map_image_filepath = create_map_image_filepath($module, $id);
$ret = file_put_contents($map_image_filepath, $resp);
if (!$ret) {
	debug_log("dir:updatemap: error create file of map : {$map_image_filepath} ");
	return no_map_image();
}

//update db
$res = $db->q("UPDATE {$module} SET map_image = ? WHERE {$id_col_name} = ? LIMIT 1", [$map_image_filename, $id]);
if ($config_dev_server) {
	debug_log("dir:updatemap: (CACHED) save file of map : {$map_image_filepath} ");
	return no_map_image();
}
//return image
return dump_image($map_image_filepath);

?>
