<?php
defined('_CMS_FRONTEND') or die('Restricted access !');

global $ctx, $account;
$system = new system;

if (!$ctx->node_id || !$ctx->country)
	$id = intval($_REQUEST["id"]);
else
	$id = $ctx->node_id;

//workers and admins can upload video without specifying place for which the video is
if ($ctx->location_id == NULL && $id == 0 && !$account->isadmin())
	$system->go("/");

//TODO put into template

if (isset($_REQUEST["upl_vid_details"])) {
	echo files::updateVideoFormSubmit();
	files::updateVideoFormGet($_REQUEST["upl_vid_file_id"]);	
	return;
}

if ($ctx->review_id != NULL)
	echo "<h1>Upload video for review #{$ctx->review_id}</h1>";
else if ($ctx->node_id != NULL)
	echo "<h1>Upload video for {$ctx->item["name"]}</h1>";
else if ($ctx->location_id != NULL)
	echo "<h1>Upload video for {$ctx->location_name}</h1>";
else
	echo "<h1>Upload video</h1>";

$uploader = GetGetParam("uploader");
switch($uploader) {
	case 'flash':		echo swfupload::getHtml();
				break;
	case 'jumploader':	echo jumploader::getJumploaderHtml(400,200);
				break;
	default:		echo resumable::getHtml();
				break;
}
?>
<div id="upload_ok" style="display: none;">Congratulations, you have successfully uploaded the video.</div>
<div id="upload_error" style="display: none;">We are sorry, the upload of the video has failed (Error: <span class="error_detail"></span>). Please try again and/or contact us at web [AT] adultsearch.com.</div>
