<?php

//TODO working only for international locations

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $ctx, $mobile;

require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.dir.php");

if (!permission::access("edit_all_places"))
    return;

if ($mobile == 1)
	system::reload(["mobile" => 2]);
	
$form = new data;
if ($ctx->country == NULL) {
	echo "Country not set!"; die;
}

$form->table = "place";
//get columns
if (($cols = dir::getColumns()) === false) {
   	echo "Something terrible happened!"; die;
}
foreach ($cols as $col) {
   	dir::displayEditCol($col, $form, true);
}
//new interface files
$c = new CColumnPicture("place_picture", "Extra Picture", "place");
$c->setPath("{$form->table}");
$c->setPathT("{$form->table}/t");
$form->AppendColumn($c);

$form->cache_update = "dir";


$c = new CColumnenum("edit", "Status");
$c->AddEnumOption("-2", "Requested to be removed");
$c->AddEnumOption("0", "New Added");
$c->AddEnumOption("1", "Live");
$c->setDefaultValue(1);
$form->AppendColumn($c);

$form->show_remove = true;

$types = dir::getTypes();
//_darr($types);
echo "<script type=\"text/javascript\">var allowedQuestionsPerType = [];\n";
foreach($types as $key => $val) {
//	echo "key=$key -> val=$val<br />";
	switch ($val) {
		case 'Strip Club':
		case 'Strip Clubs':
		case 'Male Strip Club':
		case 'Male Strip Clubs':
		case 'Strip Club & Brothel':
		case 'Strip Clubs - Brothels':
		case 'Strip Clubs & Brothels':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 'youtube', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'cover', 
				'pickup_dropoff', 'parking', 'dancer', 'clubtype', 'bar_service', 'food', 
				'reservations', 'facilities', 'services', 'dancers', 'nude_topless_dancing', 'alcoholic', 
				'offers_extra_services', 'offers_sex', 'take_out', 'age_18', 'admission', 'private_room', 'vip_room', 'lapdance', 'vip_fee', 'model_prices', 'info',
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Sex Shop':
		case 'Sex Shops':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'parking', 
				'lingerie', 'adult_toys', 'adult_dvds', 'peep_shows', 'adult_books', 'adult_video_arcade', 'gloryhole', 
				'theaters', 'theater_single_fee', 'theater_couple_fee', 'fetish', 'lgbt', 'buddy_booth_available', 'info', 
				'cruising_lounge', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Erotic Massage Parlor':
		case 'Erotic Massage Parlors':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'appointment_only', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'masseuse', 
				'rate_15', 'rate_30', 'rate_45', 'rate_60', 'rate_90', 'coupon', 
				'girl_fee', 'table_shower', 'rate_table_shower', 'short_time_room_fee', 
				'sauna', 'jacuzzi', 'parking', 'fourhand', 'info',
				'nice_rooms_showers', 'mixture_girls_ladyboys', 'overall_quality_girls', 'nonerotic',  
				'thai_massage_fee', 'foot_massage_fee', 'oil_massage_fee', 'aroma_massage_fee', 'hot_oil_fee', 'thai_herbal_hot_compress_fee', 
				'head_neck_shoulder_massage_fee', 'foot_scrub_fee', 'body_scrub_fee', 'aloe_vera_fee', 'soapy_massage', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Soapy Massage Parlor':
		case 'Soapy Massage Parlors':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'duration_of_massage', 'fishbowls', 'number_girls_fishbowl', 'number_girls_lobby', 
				'quality_girls_fishbowl', 'quality_girls_lobby', 'quality_massage', 'would_recommend', 'soapy_massage_fee_from', 
				'nonerotic',  
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Red Light District':
		case 'Red Light Districts':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'parking',  
				'reservations', 'facilities', 'services', 
				'private_room', 
				'rate_15', 'rate_30', 'rate_45', 'rate_60', 'offers_sex', 
				'alcoholic', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Table Dance':
		case 'Table Dances':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'parking',  
				'reservations', 'facilities', 'services', 'dancers', 
				'admission', 'alcoholic', 'take_out', 'ladies_drink', 'nude_topless_dancing', 'private_room', 
				'rate_30', 'rate_60', 'offers_sex', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Bordello':
		case 'Bordellos':
		case 'Eros Center':
		case 'Eros Centers':
		case 'Private':
		case 'Privates':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'parking',  
				'reservations', 'facilities', 'services',
				'admission', 'alcoholic', 'take_out', 'private_room', 
				'rate_30', 'rate_60', 'offers_sex', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Swinger Club':
		case 'Swinger Clubs':
		case 'Swingers Club':
		case 'Swingers Clubs':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'address', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'parking',  
				'reservations', 'facilities', 'services',
				'admission', 'alcoholic', 'private_room',  'coupon',
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Night Club':
		case 'Night Clubs':
		case 'Night Club - Freelance Escort':
		case 'Night Club - Freelance Escorts':
		case 'Cabaret':
		case 'Cabarets':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'address', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'parking',  
				'reservations', 'facilities', 'services',
				'admission', 'alcoholic', 'private_room', 'vip_room', 
				'ladies_drink', 'offers_sex', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Laufhaus':
		case 'LaufHaus':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'address', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'parking',  
				'reservations', 'facilities', 'services',
				'admission', 'alcoholic', 'take_out', 'private_room', 'vip_room', 
				'rate_30', 'rate_60', 'offers_sex', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'FKK':
		case 'FKKs':
		case 'Gay Sauna':
		case 'Gay Saunas':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'address', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'parking',  
				'reservations', 'facilities', 'services',
				'pickup_dropoff', 'admission', 'alcoholic', 'private_room', 'vip_room', 
				'rate_30', 'rate_60', 'offers_sex', 
				'hot_buffet', 'cold_buffet', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Sex Kino':
		case 'Sex Kinos':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'address', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'parking',  
				'reservations', 'facilities', 'services',
				'nude_or_topless_dancing', 
				'admission', 'private_room', 'vip_room', 
				'offers_sex', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Tranny Show':
		case 'Tranny Shows':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'address', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'parking',  
				'reservations', 'facilities', 'services',
				'nude_or_topless_dancing', 
				'admission', 'private_room', 'vip_room', 
				'alcoholic', 
				'offers_sex', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Gay Fetish Bar':
		case 'Gay Fetish Bars':
		case 'Gay Bar':
		case 'Gay Bars':
		case 'Fetish Bar':
		case 'Fetish Bars':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'address', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'parking',  
				'reservations', 'facilities', 'services', 'dancers', 
				'admission', 'private_room', 'vip_room', 
				'alcoholic', 
				'offers_sex', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'KTV Bar':
		case 'KTV Bars':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'address', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'parking',  
				'reservations', 'facilities', 'services', 'dancers', 
				'admission', 'private_room', 'vip_room', 
				'alcoholic', 
				'offers_sex',
				'info', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Disco':
		case 'Discos':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'address', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'cc', 'cc_visa_master', 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec', 
				'parking',  
				'reservations', 'facilities', 'services', 
				'admission', 'private_room', 'vip_room', 
				'alcoholic', 
				'offers_sex', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Beer Bar':
		case 'Beer Bars':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'address', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours',
				'bar_fine', 'girl_fee',
				'short_time_room', 'short_time_room_fee', 
				'just_drinking_bar', 'have_bar_fine', 'lots_of_girls', 'few_girls', 'free_food', 'live_entertainment', 'coyote_dancers', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Go Go Bar':
		case 'Go Go Bars':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'name', 'phone1', 'phone2', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 'day1o', 'day1c', 
				'ladies_drink', 'double_ladies_drink', 'double_ladies_drink_fee', 'bar_fine', 'girl_fee', 'info', 
				'sex_show', 'orientation', 'coyote_dancers', 'mixture_girls_ladyboys', 'overall_quality_girls', 'short_time_room', 
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Brothel Bar':
		case 'Brothel Bars':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'name', 'phone1', 'phone2', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 'day1o', 'day1c', 
				'girl_fee', 'orientation', 'mixture_girls_ladyboys', 'overall_quality_girls', 'short_time_room', 'short_time_room_fee', 'nice_rooms_showers', 'info',
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Gay Erotic Massage Parlor':
		case 'Gay Erotic Massage Parlors':
		case 'Lady Boy Massage Parlor':
		case 'Lady Boy Massage Parlors':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'orientation', 'name', 'phone1', 'phone2', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'always', 'day1o', 'day1c', 'open_holidays', 'happy_hours', 
				'nice_rooms_showers', 'mixture_girls_ladyboys', 'nonerotic',  
				'thai_massage_fee', 'foot_massage_fee', 'oil_massage_fee', 'aroma_massage_fee', 'hot_oil_fee', 'thai_herbal_hot_compress_fee', 
				'head_neck_shoulder_massage_fee', 'foot_scrub_fee', 'body_scrub_fee', 'aloe_vera_fee',
				'image', 'thumb', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		case 'Hotel':
		case 'Hotels':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'name', 'phone1', 'phone2', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'image', 'thumb', 'prices_from', 'location_bar_hopping', 'worker_comment', 'edit'];\n";
			break;
		case 'Short Time Hotel':
		case 'Short Time Hotels':
			echo "allowedQuestionsPerType['$key'] = [
				'type', 'name', 'phone1', 'phone2', 'neighborhood_area', 'address1', 'address2', 'building_name', 'City', 'zipcode', 'website', 'facebook', 'twitter', 'instagram', 'email', 'loc_lat', 'loc_long', 
				'image', 'thumb', 'short_time_room_fee', 'overall_quality_room', 'extra1', 'files_listing', 'worker_comment', 'edit'];\n";
			break;
		default:
			break;
	}
}
echo "</script>\n";

//skip phone check on intenational places
$_POST["phonesuphone1"] = 1;

$form->ShowPage();

return; 

?>
