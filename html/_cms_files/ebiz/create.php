<?php
/**
 * Autologin/create ebiz website from newsletter
 */
defined("_CMS_FRONTEND") or die("No Access");

global $account, $db;
$system = new system;

$email = $_REQUEST["email"];
$code = $_REQUEST["code"];

/*
//todelete
if ($_REQUEST["test"] == 1) {
	echo "<h1>Your free escorts.biz website was created successfully<h1>\n";
	echo "<a href=\"".htmlspecialchars($link)."\" target=\"_blank\" style=\"font-size: 0.8em;\">Visit my new website</a><br />\n";
	return;
}
*/

if(!$email || !$code) {
	echo "Missing parameters!";
	debug_log("EBIZ Newsletter: Error - missing parameters, req=".$_SERVER["REQUEST_URI"]);
	return;
}

//check if email and code matches
$res = $db->q("SELECT id, account_id FROM ebiz_newsletter WHERE email = ? AND code = ? LIMIT 1", array($email, $code));
if ($db->numrows($res) != 1) {
	echo "Incorrect parameters!";
	debug_log("EBIZ Newsletter: Error - incorrect parameters, req=".$_SERVER["REQUEST_URI"]);
	return;
}
$row = $db->r($res);
$account_id = $row["account_id"];
$id = $row["id"];

//check if user does not already has ebiz website
$res = $db->q("SELECT email, ebiz_id, ebiz_secret FROM account WHERE account_id = ?", array($account_id));
if ($db->numrows($res) != 1) {
    echo "This account does not exist!";
    debug_log("EBIZ Newsletter: Error - account does not exists, account_id={$account_id}, req=".$_SERVER["REQUEST_URI"]);
    return;
}
$row = $db->r($res);
if ($row["ebiz_id"] != NULL || $row["ebiz_secret"] != NULL) {
    echo "You already have escorts.biz website, please log in to your account and click on \"My Website\" link.<br />\n";
    debug_log("EBIZ Newsletter: Error - account already has ebiz website, account_id={$account_id}, req=".$_SERVER["REQUEST_URI"]);
    return;
}
$email_current = $row["email"];	//use current account email for autologin (what if user meanwhile changed his email...)

//get last live classified of this account
$res = $db->q("SELECT id FROM classifieds WHERE account_id = ? AND done = 1 ORDER BY id DESC LIMIT 1", array($account_id));
if ($db->numrows($res) != 1) {
    echo "Currently, you don't have any live classified ad in this account. Please create classified ad and then repeat this action.<br />\n";
    debug_log("EBIZ Newsletter: Error - no live ad, account_id={$account_id}, req=".$_SERVER["REQUEST_URI"]);
    return;
}
$row = $db->r($res);
$clad_id = intval($row["id"]);

if (!$clad_id) {
    echo "Wrong ad id !<br />\n";
    debug_log("EBIZ Newsletter: Error - wrong ad id, clad_id={$clad_id}, account_id={$account_id}, req=".$_SERVER["REQUEST_URI"]);
    return;
}

//autologin, if we are not logged in
// (we need to be logged in, so escortsbiz::create_website_request succeeds)
if ($account->getId() && ($account->getId() != $account_id)) {
	//we are logged in as different user, lets log out
    debug_log("EBIZ Newsletter: Logging out - currently logged in =".$account->getId()." , account_id={$account_id}, req=".$_SERVER["REQUEST_URI"]);
	$account->logout();
}
if (!$account->getId()) {
	$ret = $account->login($email_current, NULL, $error, 1);
	if (!$ret) {
    	debug_log("EBIZ Newsletter: Error - can't autologin !, error={$error}, clad_id={$clad_id}, account_id={$account_id}, req=".$_SERVER["REQUEST_URI"]);
		echo "Can't log you in. Please contact administrator.<br />\n";
		return;
	}
}

//create website
$arr = escortsbiz::create_website_request($clad_id);
if ($arr["status"] != "success") {
    debug_log("EBIZ Newsletter: Error - ebiz::create_website_request failed !, clad_id={$clad_id}, account_id={$account_id}, req=".$_SERVER["REQUEST_URI"]);
	echo "There has been problem creating website. Please contact administrator.<br />\n";
	return;
}

//----------
//success!!!
//----------

$db->q("UPDATE ebiz_newsletter SET used = ? WHERE id = ?", array(time(), $id));

//lets show some links
$link = $arr["link"];
debug_log("EBIZ Newsletter: Success: link={$link}, clad_id={$clad_id}, account_id={$account_id}, req=".$_SERVER["REQUEST_URI"]);

echo "<h1>Your free escorts.biz website was created successfully<h1>\n";
echo "<a href=\"".htmlspecialchars($link)."\" target=\"_blank\" style=\"font-size: 0.8em;\">Visit my new website</a><br />\n";
return;

?>
