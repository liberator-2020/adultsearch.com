<?php

defined("_CMS_FRONTEND") or die("No Access");

deprecated_controller();

require_once _CMS_ABS_PATH."/_cms_files/worker/array_sc.php";

global $db, $ctx, $smarty, $page, $account, $gTitle, $advertise, $mobile, $gModule, $gLocation, $gDescription, $sphinx, $config_image_server;
$form = new form; 
$system = new system;

session_write_close();

$location = location::findOneById($ctx->location_id);
if (!$location) {
	return $system->go("http://adultsearch.com/homepage");
} else {
	$smarty->assign("link_back", $location->getUrl());
	$smarty->assign("location_video_html", $location->getVideoHtml());
}

//seo
$res = $db->q("SELECT description FROM seo WHERE location_id = ? AND type = 'LPT' AND place_type_id = 15 LIMIT 1", array($location->getId()));
if ($db->numrows($res)) {
	$row = $db->r($res);
	$smarty->assign("category_description", $row["description"]);
}

$core_state_id = $account->corestate(1);
if( !$core_state_id ) return;

$loc_around = array();
if ($account->core_loc_id && $account->core_loc_type > 2)
	$loc_around = $account->locAround();

$advertise->popup();
$advertise->showAd(7, $core_state_id);

$gDescription = "Find {$account->core_loc_array['loc_name']} erotic massage parlors and enjoy a revitalizing erotic massage from a therapist straight out of a fairy tale happy ending.";

$loc_id = $account->core_loc_array['loc_id'];

$tags = isset($_GET["tags"]) ? GetGetParam("tags") : NULL;
$table_shower = isset($_GET["table_shower"]) ? 1 : NULL;
$sauna = isset($_GET["sauna"]) ? 1 : NULL;
$jacuzzi = isset($_GET["jacuzzi"]) ? 1 : NULL;
$cc_visa = isset($_GET["cc_visa"]) ? 1 : NULL;
$cc_ae = isset($_GET["cc_ae"]) ? 1 : NULL;
$cc_dis = isset($_GET["cc_dis"]) ? 1 : NULL;
$truckparking = isset($_GET["truckparking"]) ? 1 : NULL;
$private_parking = isset($_GET["private_parking"]) ? 1 : NULL;
$fourhand = isset($_GET["fourhand"]) ? 1 : NULL;
$always = isset($_GET["always"]) ? 1 : NULL;
$neighbor = isset($_GET['neighborhood']) ? intval($_GET['neighborhood']) : NULL;

if( $loc_id ) {
	$locx = $location->_coreLocationBreadArray($loc_id);
	$loc_name = $locx[0]["loc_name"];
	$smarty->assign("hidden_loc_id", $locx[0]["loc_id"]);
	$gTitle = "$loc_name, {$locx[0]["s"]}";
	$account->setcookie('loc_id', $loc_id, 30, $_SERVER['HTTP_HOST']);
} else {
	$loc_name = $gTitle = $account->core_loc_array['loc_name'];
}

$gTitle .= " %tags% Erotic Massage Parlors";

$res = $db->q("select * from eroticmassage_tag");
$ethx = NULL;
while($row=$db->r($res)) {
	$ethx[$row["tag_id"]] = $row["tag_name"]; 
}

$xx = _pagingQuery(array("x"));
$filter_link = array();
$gTitlex = "";
foreach($_GET as $key => $value) {
	if( in_array($key, array("tags", "table_shower", "sauna", "jacuzzi", "cc_visa", "cc_ae", "cc_dis", "truckparking", "private_parking", "loc_id", "query", 
"zipcode", "phone", "fourhand", "always", "neighborhood")) && $value != "" ) {
		$ftype = $key;
		$valuex = $value;
		$skipfilter = 0;
		if( $key == "tags" ) { 
			$key = "Masseuse: ".$ethx[$valuex];
			$gTitlex = " " . $ethx[$valuex];
		}
		else if( $key == "table_shower" ) { $key = "Amenities: Table Shower"; $gTitlex .= " Table Shower"; }
		else if( $key == "sauna" ) { $key = "Amenities: Sauna"; $gTitlex .= " Sauna"; }
		else if( $key == "jacuzzi" ) { $key = "Amenities: Jacuzzi"; $gTitlex .= " Jacuzzi"; }
		else if( $key == "truckparking" ) { $key = "Amenities: Truck Parking"; $gTitlex .= " Truck Parking"; }
		else if( $key == "private_parking" ) { $key = "Amenities: Private Parking"; $gTitlex .= " Private Parking"; }
		else if( $key == "cc_visa" ) { $key = "Payment: VISA/MC"; $gTitlex .= " VISA/MC"; }
		else if( $key == "cc_ae" ) { $key = "Payment: AMEX"; $gTitlex .= " AMEX"; }
		else if( $key == "cc_dis" ) { $key = "Payment: Discover"; $gTitlex .= " Discover Card";}
		else if( $key == "fourhand" ) { $key = "4 Hand Massage"; $gTitlex .= " 4 Hand Massage";}
		else if( $key == "always" ) { $key = "Open 24 Hours"; $gTitlex .= " Open 24 Hours";}
		else if( $key == "zipcode" ) { $key = "Zip Code: $valuex"; $skipfilter = 1; 
			$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key, "link"=>"{$account->core_loc_array['loc_url']}$gModule/"); }
		else if( $key == "query" ) { $key = "'$valuex'"; }
		else if( $key == "phone" ) { $key = "Phone #: '$valuex'"; }
		else if( $key == "loc_id" ) $key = $loc_name;
		else if( $key == "neighborhood" ) {
			$rex = $db->q("select name from location_neighbor where loc_id = '$value'");
			$rox = $db->r($rex);
			$key = "Neighborhood: <b>{$rox['name']}</b>";
		}

		if( !$skipfilter ) $filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key);
	}
}

if( $gTitlex ) {
	if( $zipcode ) $gTitle = "$zipcode$gTitlex - Erotic Massage Parlors";
	else $gTitle = "$loc_name$gTitlex - Erotic Massage Parlors";
} 

$gTitle = "$loc_name Erotic Massage Parlors in {$account->core_loc_array['lp_loc_name']}";

if ($page >1)
	$gTitle .= " - Page $page";

$item_per_page = isset($_GET["ipp"]) ? intval($_GET["ipp"]) : 30;
if( $item_per_page > 100 ) $item_per_page = 20;
$start = (($page*$item_per_page)-$item_per_page);
if( $start < 0 ) $start = 0;

$sphinx_index = "eroticmassage";
$sphinx->reset();
$sphinx->SetLimits( $start, $item_per_page, 2000 );

if( isset($_GET["zipcode"]) && !empty($_GET["zipcode"]) ) {
	$zipcode = preg_replace("/[^0-9a-z]/i","", $_GET["zipcode"]);
	$mile_in = intval($_GET["miles"]); if( !$mile_in ) $mile_in = 10;
	if( $mile_in == -1 )
		$mile_in = 1000;
	$res = $db->q("SELECT z.lat, z.lon, radians(z.lat) latt, radians(z.lon) longg, l.loc_parent
					FROM core_zip z
					INNER JOIN location_location l on z.loc_id = l.loc_id
					WHERE z.zip = '$zipcode' limit 1");
	if( $db->numrows($res) ) {
		$s = $db->r($res);
		$lat_me = $s["lat"]; $lon_me = $s["lon"];
		$latt = $s["latt"]; $long = $s["longg"];
		$sphinx->SetGeoAnchor('loc_lat', 'loc_long', $latt, $long);
		$sphinx->SetFilterFloatRange('@geodist', 0, ($mile_in*1609.344));
		$zipfound = 1;
	} else {
		$alert = array("ul"=>array("<a href='/eroticmassage/' title='$loc_name erotic massage parlors'>Click here</a> to remove the zip code($zipcode) from your search criteria"));
		$error = "Zip code($zipcode) could not find. Please make sure that you typed the correct zip code.";
		$res = $db->q("select id, loc_id, city, state from core_zip where zip = '$zipcode' limit 1");
		if( $db->numrows($res) ) {
			$rox = $db->r($res);
			$res2 = $db->q("select loc_id from location_location where loc_name = '{$rox["city"]}' and s = '{$rox["state"]}'");
			if( $db->numrows($res2) ) {
				$rox2 = $db->r($res2);
				$db->q("update core_zip set loc_id = '{$rox2["loc_id"]}' where id = '{$rox["id"]}'");
				$m = "{$rox["loc_id"]} becomes {$rox2["loc_id"]} for {$rox["id"]} - $zipcode";
				reportAdmin("core_zip updated...", $m);
				$system = new system;
				$system->moved($_SERVER["REQUEST_URI"]);
			} else {
				$m = "select loc_id from location_location where loc_name = '{$rox["city"]}' and s = '{$rox["state"]}'";
				reportAdmin("core_zip error", $m);
			}
		} else {
			$ip = account::getUserIp();
			$db->q("insert ignore into core_zip_update (zip, ip_address) values (?, ?)", [$zipcode, $ip]);
		}
	}
	$smarty->assign("miles", $mile_in==1000?-1:$mile_in);
	$smarty->assign("zipcode", $_GET["zipcode"]);
}

if( !is_null($tags) ) $sphinx->SetFilter('tags', array($tags));
if( !is_null($table_shower) ) $sphinx->SetFilter('table_shower', array(1));
if( !is_null($sauna) ) $sphinx->SetFilter('sauna', array(1));
if( !is_null($jacuzzi) ) $sphinx->SetFilter('jacuzzi', array(1));
if( !is_null($cc_visa) ) $sphinx->SetFilter('cc_visa', array(1));
if( !is_null($cc_ae) ) $sphinx->SetFilter('cc_ae', array(1));
if( !is_null($cc_dis) ) $sphinx->SetFilter('cc_dis', array(1));
if( !is_null($truckparking) ) $sphinx->SetFilter('truckparking', array(1));
if( !is_null($private_parking) ) $sphinx->SetFilter('private_parking', array(1));
if( !is_null($fourhand) ) $sphinx->SetFilter('fourhand', array(1));
if( !is_null($neighbor) ) $sphinx->SetFilter('neighbor', array($neighbor));
if( !is_null($always) ) $sphinx->SetFilter('always', array(1));

if( $account->core_loc_id && $account->core_loc_type > 2 && !$zipfound) {
	$nloc[] = $account->core_loc_id;
	$sphinx->SetFilter('loc_id', !$account->international ? $loc_around : array($account->core_loc_id));
} elseif ($account->core_loc_id && $account->core_loc_type < 3 ) {
	$sphinx->SetFilter('state_id', array($account->core_loc_id));
}

if (!$account->isWorker())
	$sphinx->SetFilter('edit', array(1, 2, 3));

$query = "";
if( isset($_GET["phone"]) && !empty($_GET["phone"]) ) {
	$phone = preg_replace("/[^0-9\*]/", "", $_GET["phone"]);
	$query = $phone;
}
if( isset($_GET["query"]) && !empty($_GET["query"]) ) {
	$name = preg_replace("/(\()?([0-9]{3})(\))?(.+)?([0-9]{3})(.)?([0-9]{4})/s", "$2$5$7", $_GET["query"]);
	$query .= $query ? " $name" : "$name";
	$error = "No massage parlor found with the word: ".$_GET["query"];
	$ul[]="<a href='{$account->core_loc_array['loc_url']}$gModule/' title='$loc_name erotic massage parlors'><b>Click here</b></a> to see results without the search 
word '{$_GET["query"]}'";
	$alert = array("ul"=>$ul);
}

$orderway = isset($_GET["orderway"])&& intval($_GET["orderway"]) ? intval($_GET["orderway"]) : (($zipfound||$distance)?2:1);
$way = $orderway == 1 ? "DESC" : "ASC";

if($_GET["order"] == 'overall') { $order = "overall $way"; }
elseif($_GET["order"] == 'review') $order = "review $way";
elseif($_GET["order"] == "name") $order = "name $way";
elseif($_GET["order"] == "last_review_id") $order = "last_review_id $way";
elseif($_GET["order"] == "score") $order = "overall $way";
elseif($_GET["order"] == 'distance' || ($zipfound||$distance) ) { $order = "sponsor DESC, @geodist $way"; }
elseif($_GET["order"] == 'coupon') $order = "coupon $way";
elseif($_GET["order"] == 'video') $order = "video_cnt $way";
else $order = "sponsor DESC, last_review_id DESC";
$sphinx->SetSortMode(SPH_SORT_EXTENDED, $order);

if( isset($_GET["query"]) && !empty($_GET["query"]) ) {
	$smarty->assign("sortquery", "query=".$_GET["query"]);
} 

$results = $sphinx->Query($query, $sphinx_index);
$total = $results["total_found"];
$result = $results["matches"];

//_darr($results);

$refine = NULL;

/* NY neighborhoods under the 5 borough */

if( in_array($account->core_loc_array['loc_id'], array(42838, 42840, 42841, 42842, 18310)) ) {
	$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$account->core_loc_array['loc_name'], 
"link"=>"/new-york/new-york-city/$gModule/$xxx");
}
if( $total && is_null($neighbor) && in_array($account->core_loc_id, array(18308,42838, 42840, 42841, 42842, 18310))) {
	if( $account->core_loc_id == 18308 ) {
			$typex = "neighbor2";
		$filter = array(42838, 42840, 42841, 42842, 18310);
		$table = "location_location";
	} else {
		$typex = "neighbor";
		$filter = array($account->core_loc_id);
		$table = "location_neighbor";
	}
	$type = "neighbor2";
	$neighbor = NULL;
	$sphinx->SetGroupBy($typex, SPH_GROUPBY_ATTR, '@count desc' );
	$sphinx->SetFilter($type, $filter);
	$results = $sphinx->Query($query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$neighbor[$match["attrs"][$typex]] = array("id"=>$match["attrs"][$typex], "c"=>$match["attrs"]["@count"]);
		$nid[] = $match["attrs"][$typex];
	}
	if( $neighbor ) {
		$rex = $db->q("select * from $table where loc_id in (".implode(',', $nid).")");
		while($row=$db->r($rex)) {
			$url = !empty($row['loc_url']) ? "{$row['loc_url']}{$gModule}/" : "";
			$name = !empty($row['loc_name'])?$row['loc_name']:$row['name'];
 			$nb[] = array("id"=>$row['loc_id'], "type"=>"neighborhood", "name"=>$name,
"c"=>$neighbor[$row['loc_id']]["c"], "link"=>$url, "title"=>"$name");
		} 
	}
	if( $nb )  $refine[] = array("name"=>"Neighborhood", "col"=>1, "alt"=>$nb);
}


$sphinx->SetLimits( 0, 10, 10 );

if( !$total && $zipfound && $mile_in < 100 ) {
	$sphinx->ResetAnchor();
	$sphinx->ResetFilter("@geodist");
	$sphinx->SetGeoAnchor('loc_lat', 'loc_long', $latt, $long);
	$sphinx->SetFilterFloatRange('@geodist', 0, (40*1609.344));
	$results2 = $sphinx->Query($query, $sphinx_index);
	$total2 = $results2["total_found"];
	if( $total2 > 0 ) {
		$ul[]="<a href='/eroticmassage/?zipcode=$zipcode&miles=40' title='$zipcode erotic massage parlors'><b>$total2</b></a> massage parlor with in <strong>40 miles</strong> of $zipcode";
	}

	$sphinx->ResetAnchor();
	$sphinx->ResetFilter("@geodist");
	$sphinx->SetGeoAnchor('loc_lat', 'loc_long', $latt, $long);
	$sphinx->SetFilterFloatRange('@geodist', 0, (100*1609.344));
	$results2 = $sphinx->Query($query, $sphinx_index);
	$total2 = $results2["total_found"];
	if( $total2 > 0 ) {
		$ul[]="<a href='/eroticmassage/?zipcode=$zipcode&miles=100' title='$zipcode erotic massage parlors'><b>$total2</b></a> massage parlor with in <strong>100 miles</strong> of $zipcode";
	}

	$sphinx->ResetAnchor();
	$sphinx->ResetFilter("@geodist");
	$results2 = $sphinx->Query($query, $sphinx_index);
	$total2 = $results2["total_found"];
	if( $total2 > 0 ) {
		$ul[]="<a href='/eroticmassage/?'
title='$loc_name erotic massage parlors'><b>$total2</b></a> massage parlor with in <strong>any distance</strong> of $zipcode";
	}

	if( $ul ) $alert = array("ul"=>$ul);
}

if( $total && is_null($table_shower) ) {
	$type = "table_shower";
	$amenities = NULL;
	$sphinx->SetFilter($type, array(1));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query($query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$namex = "Table Shower";
		$amenities[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
}

if( $total && is_null($sauna) ) {
	$type = "sauna";
	$sphinx->SetFilter($type, array(1));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query($query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$namex = "Sauna";
		$amenities[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
}

if( $total && is_null($jacuzzi) ) {
	$type = "jacuzzi";
	$sphinx->SetFilter($type, array(1));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query($query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$namex = "Jacuzzi";
		$amenities[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
}

if( $total && is_null($truckparking) ) {
	$type = "truckparking";
	$sphinx->SetFilter($type, array(1));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query($query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$namex = "Truck Parking";
		$amenities[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
}

if( $total && is_null($private_parking) ) {
	$type = "private_parking";
	$sphinx->SetFilter($type, array(1));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$namex = "Private Parking";
		$amenities[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
}
if( $amenities )
	$refine[] = array("name"=>"Amenities", "col"=>1, "alt"=>$amenities);

$extra = NULL;
if( $total && is_null($fourhand) ) {
	$type = "fourhand";
	$sphinx->SetFilter($type, array(1));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$namex = "4 Hand Massage";
		$extra[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}

}
if( $total && is_null($always) ) {
	$type = "always";
	$sphinx->SetFilter($type, array(1));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$namex = "Open 24 Hours";
		$extra[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
}
if( $extra )
	$refine[] = array("name"=>"Services", "col"=>1, "alt"=>$extra);

if( $total && is_null($tags) ) {
	$type = "tags";
	$sphinx->SetGroupBy('tags', SPH_GROUPBY_ATTR, '@count desc');
	$results = $sphinx->Query ( $query, $sphinx_index );
	$category = NULL;
	foreach($results["matches"] as $match) {
		$namex = $ethx[$match["attrs"]["@groupby"]];
		$category[] = array("id"=>$match["attrs"]["@groupby"], "type"=>$type, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
	if( $category )	$refine[] = array("name"=>"Masseuse Ethnicity", "col"=>2, "alt"=>$category);
}

if( $total && is_null($cc_visa) ) {
	$type = "cc_visa";
	$payment = NULL;
	$sphinx->SetFilter($type, array(1));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$namex = "VISA/MC";
		$payment[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
}

if( $total && is_null($cc_ae) ) {
	$type = "cc_ae";
	$sphinx->SetFilter($type, array(1));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$namex = "AMEX";
		$payment[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
}

if( $total && is_null($cc_dis) ) {
	$type = "cc_dis";
	$sphinx->SetFilter($type, array(1));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$namex = "Discover";
		$payment[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
}

if( $payment )
	$refine[] = array("name"=>"Payments", "col"=>1, "alt"=>$payment);

$smarty->assign("refine", $refine);

if( isset($_GET["name"]) && !empty($_GET["name"]) ) {
	$name_sql = "and name like '%".GetGetParam("name")."%'";
	$smarty->assign("name", $_GET["name"]);
	$error = "No store found with the word: ".$_GET["name"];
}

foreach($result as $res) {
	$ids .= $ids ? (",".$res["id"]) : $res["id"];
}

if( $ids )
	$res = $db->q("SELECT e.*, l.loc_url, l.country_sub, r.comment rc, a2.avatar ra, a2.username as ru, if(length(coupon)>1,1,0) coupon, 
						(select count(*) from place_picture pp where pp.place_file_type_id = 2 and pp.module = 'eroticmassage' and pp.id = e.id) as video
					FROM eroticmassage e
					INNER JOIN location_location l on e.loc_id = l.loc_id
					LEFT JOIN (place_review r inner join account a2 using (account_id)) on e.last_review_id = r.review_id
					WHERE e.id in ($ids)
					ORDER BY field(e.id,$ids)");

while($row=$db->r($res)) {

	if( $zipcode ) {
		if( !isset($lat_me) ) continue;
		if( $row["loc_lat"] && $row["loc_long"] ) {
			$mile = number_format(distance($lat_me, $lon_me, $row["loc_lat"], $row["loc_long"], "M"),1);
		} else $mile = "unknown";
	} elseif( $distance ) {
		$mile = number_format(distance($account->core_loc_array["loc_lat"], $account->core_loc_array["loc_long"], $row["loc_lat"], $row["loc_long"], "M"),1);
	}

	$link = dir::getPlaceLinkAux($row['country_sub'], $row["loc_id"], $gModule, $row["id"], $row['name'], trim($row['loc_url'], "/"));

	$locname = "{$row["loc_name"]}, {$row["state_name"]}";

	$phone = makeproperphonenumber($row["phone"]);
	$thumbpath = !empty($row["thumb"]) ? "{$config_image_server}/eroticmassage/t/{$row["thumb"]}" : "/images/placeholder.gif";
	$ra = "{$config_image_server}/avatar/" .  (!empty($row["ra"]) ? $row["ra"] : "no_avatar.jpg");

	$extra = $row["nonerotic"] ? "<span class='nonerotic'>*Non-Erotic Massage Reported*</span>" : NULL;
	$extra .= $row["edit"] < 1 ? "<span class='nonerotic'>*Not Live*</span>" : NULL;

	$sc[] = array(
		"id"=>$row["id"],
		"name"=>$row["name"],
		"thumb"=>$row["thumb"],
		"address"=>$row["address1"],
		"phone"=>$phone,
		"loc_name"=>$locname,
		"zipcode"=>$row["zipcode"],
		"distance"=>isset($zipcode)||isset($distance)?$mile:NULL,
		"review"=>$row["review"],
		"recommend"=>$row["recommend"],
		"edit"=>$row["edit"],
		"sponsor"=>$row["sponsor"],
		"linkoverride" => $link,
		"longitude"=>$row['loc_long'],
		"latitude"=>$row['loc_lat'],
		"thumbpath"=>$thumbpath,
		"lr"=>$row['last_review_id'],
		"rc"=>$row['rc'],
		"ra"=>$ra,
		"ru"=>$row['ru'],
		"extra"=>$extra,
		"coupon"=>$row['coupon'],
		"video"=>$row['video']
	);

}

$json = json_encode($sc);
$smarty->assign("mapjson", $json);


$smarty->assign("sc", $sc);
$smarty->assign("total", $total);
$smarty->assign("phone1", $_GET["phone1"]);
$smarty->assign("phone2", $_GET["phone2"]);
$smarty->assign("phone3", $_GET["phone3"]);

if( isset($error) ) $smarty->assign("error", $error);
if( isset($alert) ) $smarty->assign("alert", $alert);

$query = _pagingQuery(array("loc_id"));


_pagingQueryFilter($query, $filter_link);
if( $zipfound && strstr($gLocation, "zipcode") ) $linksort = _pagingQuery(array("order", "orderway", "zipcode"));
else $linksort = _pagingQuery(array("order", "orderway"));


$smarty->assign("linksort", $linksort);
$smarty->assign("search_hidden", _pagingQuery(array("loc_id", "zipcode", "miles", "phone"), 1));
$smarty->assign("link", $query);

$smarty->assign("filter", $filter_link);

$dir = new dir;
$ctx->page = $page;
$smarty->assign("pager_array", $dir->get_pager_array($total, $item_per_page, $account->core_loc_array['loc_url']."/{$gModule}/", true));
$smarty->assign("paging", $db->paging($total, $item_per_page, $mobile==1?false:true, $mobile==1?false:true, $query));

$order = isset($_GET["order"]) ? $_GET["order"] : ($distance?"distance":"last_review_id");
if( !isset($_GET["orderway"]) && $order == "last_review_id" ) $orderway = 1;
elseif( !isset($_GET["orderway"]) && $distance ) $orderway = 2;
$sort = array();
if( isset($zipfound) || isset($distance)) $sort[] = array("key"=> "Distance", "value"=>"distance", "way"=>$order == "distance" && $orderway == 2 ? 1 : 2, 
"selected"=>$order == "distance" ?true : false);
$sort[] = array("key"=> "Rating", "value"=>"overall", "way"=>$order == "overall" && $orderway == 1 ? 2 : 1, "selected"=>$order == "overall" ? true : false);
$sort[] = array("key"=> "# of Reviews", "value"=>"review", "way"=>$order == "review" && $orderway == 1 ? 2 : 1, "selected"=> $order == "review"? true : false);
$sort[] = array("key"=> "Review Date", "value"=>"last_review_id", "way"=>$order == "last_review_id" && $orderway == 1 ? 2 : 1, "selected"=>$order 
=="last_review_id" ?true : false);
$sort[] = array("key"=> "Video", "value"=>"video", "way"=>$order == "video" && $orderway == 1 ? 2 : 1, "selected"=> $order == "video"? true : false);
$sort[] = array("key"=> "Coupon", "value"=>"coupon", "way"=>$order == "coupon" && $orderway == 1 ? 2 : 1, "selected"=> $order == "coupon"? true : false);
$smarty->assign("sort", $sort);

$seo_loc_name = $loc_name;

if ($zipfound)
	$loc_name = "$zipcode";

$smarty->assign("core_state_id", $core_state_id);

$smarty->assign("category", "erotic-massage-parlor");
$smarty->assign("place_type_id", 15);
$smarty->assign("what", "Erotic Massage Parlor");
$smarty->assign("worker_link", "emp");
$smarty->assign("image_path", "eroticmassage");
$smarty->assign("place_link", "parlor");
$smarty->assign("loc_name", $loc_name);
$smarty->assign("gModule", $gModule);
$smarty->assign("rss_available", true);
$smarty->assign("imageserver", $config_image_server."/eroticmassage/");

$classifieds = new classifieds;
$classifieds->featuredads($account->core_loc_array['loc_id']);

$file = $smarty->fetch(_CMS_ABS_PATH."/templates/place/place_list.tpl");
echo $file;

?>
