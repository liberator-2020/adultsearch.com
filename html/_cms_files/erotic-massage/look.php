<?php
defined("_CMS_FRONTEND") or die("No Access");

deprecated_controller();

global $db, $smarty, $account, $page, $gTitle, $advertise, $gModule, $gLocation, $gItemID, $mobile, $gDescription, $config_mapquest_key, $config_image_server;
$form = new form;

session_write_close();

include_html_head("css", "/css/info.css");

$id = $gItemID;
$account_id = $account->isloggedin();

$review = new review("eroticmassage", $id, "eroticmassage", "", "eroticmassage");

$res = $db->q("SELECT e.*, l.loc_url, l.country_sub
				FROM eroticmassage e 
				LEFT JOIN location_location l on l.loc_id = e.loc_id
				WHERE e.id = ? AND e.deleted IS NULL", [$id]);
if( !$db->numrows($res) ) {
	if( empty($account->core_loc_array['loc_url']) ) $account->core_loc_array['loc_url'] = "/";
	system::moved("{$account->core_loc_array['loc_url']}{$gModule}/");
}
$row = $db->r($res);

$name2url = name2url($row['name']);

if ($account->core_sub != $row["country_sub"] || !strstr($_SERVER['REQUEST_URI'], $row['loc_url']) || $gLocation != $name2url)
	system::moved(location::getUrlStatic($row["country_sub"], $row["loc_url"])."{$gModule}/{$name2url}/{$row['id']}");

$smarty->assign('breadplace', $row['name']);

$advertise->showAd(7, $row["loc_id"]);
$phone = makeproperphonenumber($row["phone"]);

$gTitle = "{$row["name"]} ". makeproperphonenumber($row['phone']) ." {$row['loc_name']} Erotic Massage";

$mobile_class = $mobile == 1 ? " class='link'" : "";

$loc = "<a href='{$account->core_loc_array['loc_url']}$gModule/' title='{$row["loc_name"]} erotic massage parlors'$mobile_class>{$row["loc_name"]}</a>, {$row["state_name"]} {$row["zipcode"]}";
$smarty->assign("link_back", "{$account->core_loc_array["loc_url"]}$gModule/");

//map
$map = prepareMap($id, 'eroticmassage', $row, 'eroticmassage', 'eroticmassage');
$smarty->assign("map", $map);
$smarty->assign("link_map_update", "/map?id={$id}&section=eroticmassage&update=1");

$quickread = $review->reviews();
$smarty->assign("quickread", $quickread);
if( empty($quickread) && !empty($row['gdescription']) ) {
  $gDescription = $row['gdescription'];
  $smarty->assign("description", $gDescription);
} else {
	if( !empty($quickread) ) $gDescription = substr(htmlspecialchars($quickread[0]['review'], ENT_QUOTES), 0, 80) .". ";
	$gDescription .= "{$row['name']} $phone {$row['address']} Erotic Massage Parlor in {$row['loc_name']} {$row['state_name']}";
}

if( $row["owner"] )
	$c1[] = array("label" => "Updated By Business Owner", "val" => "Yes");

$rex = $db->q("select group_concat(tag_name separator ', ') tag_name from eroticmassage_tag_assign a inner join eroticmassage_tag t using (tag_id) where a.id = '$id'");
$rox = $db->r($rex);
if( !empty($rox["tag_name"]) )
	$c1[] = array("label" => "Masseuse", "val" => "{$rox["tag_name"]}");

global $ctx;
$ctx->item = $row;
$smarty->assign("hours", dir::getHours(($row["byapponly"] == 1) ? true : false));
$smarty->assign("current_weekday", strtolower(date("l")));

if( !empty($row["major_cross_streets"]) )
	$c1[] = array("label" => "Major streets", "val" => "{$row["major_cross_streets"]}");
if( !empty($row["other_services"]) )
	$smarty->assign("info", $row["other_services"]);

if( $row["table_shower"] ) {
	$c1[] = array("label" => "Table Shower", "val" => "Yes");
	$rate = !empty($row["rate_table_shower"]) ? $row["rate_table_shower"] : "Free";
	$c1[] = array("label" => "Table Shower Extra Rate", "val" => "{$rate}");
} else
	$c1[] = array("label" => "Table Shower", "val" => "No");
if( $row["sauna"] )
	$c1[] = array("label" => "Sauna", "val" => "Yes");
else
	$c1[] = array("label" => "Sauna", "val" => "No");
if( $row["jacuzzi"] )
	$c1[] = array("label" => "Jacuzzi", "val" => "Yes");
else
	$c1[] = array("label" => "Jacuzzi", "val" => "No");
if( $row["truckparking"] )
	$c1[] = array("label" => "Truck Parking", "val" => "Yes");
else
	$c1[] = array("label" => "Truck Parking", "val" => "No");
if( $row["private_parking"] )
	$c1[] = array("label" => "Private Parking", "val" => "Yes");
else
	$c1[] = array("label" => "Private Parking", "val" => "No");
if( $row["fourhand"] )
	$c1[] = array("label" => "4 Hand Massage", "val" => "Yes");

if( $row['country_id'] == 41973 ) {
	$money_sign = "GBP ";
	$rex = $db->q("select `from` from currency where code = 'GBP'");
	if( $db->numrows($rex) ) {
		$rox = $db->r($rex);
		$exchange = $rox[0];
	}
} else if ($row['country_id'] == 41972)
	$money_sign = "AED ";
else
	$money_sign = "$";

if( $row["rate_15"] ) {
	$r = "$money_sign{$row["rate_15"]}.00";
	if( $exchange )
		$r .= " (\$".number_format($row["rate_15"]*$exchange, 2).")";
	$c1[] = array("label" => "15 Minutes", "val" => $r);
}
if( $row["rate_30"] ) {
	$r = "$money_sign{$row["rate_30"]}.00";
	if( $exchange )
		$r .= " (\$".number_format($row["rate_30"]*$exchange, 2).")";
	$c1[] = array("label" => "30 Minutes", "val" => $r);
}
if( $row["rate_45"] ) {
	$r = "$money_sign{$row["rate_45"]}.00";
	if( $exchange )
		$r .= " (\$".number_format($row["rate_45"]*$exchange, 2).")";
	$c1[] = array("label" => "45 Minutes", "val" => $r);
}
if( $row["rate_60"] ) {
	$r = "$money_sign{$row["rate_60"]}.00";
	if( $exchange )
		$r .= " (\$".number_format($row["rate_60"]*$exchange, 2).")";
	$c1[] = array("label" => "60 Minutes", "val" => $r);
}
if( $row["rate_90"] ) {
	$r = "$money_sign{$row["rate_90"]}.00";
	if( $exchange )
		$r .= " (\$".number_format($row["rate_90"]*$exchange, 2).")";
	$c1[] = array("label" => "90 Minutes", "val" => $r);
}
if( $row["rate_120"] ) {
	$r = "$money_sign{$row["rate_120"]}.00";
	if( $exchange )
		$r .= " (\$".number_format($row["rate_120"]*$exchange, 2).")";
	$c1[] = array("label" => "120 Minutes", "val" => $r);
}

$payment = "";
if( $row["cc_visa"] ) $payment = "VISA/MC";
if( $row["cc_ae"] ) $payment .= ($payment ? ", " : "") . "American Express";
if( $row["cc_dis"] ) $payment .= ($payment ? ", " : "") . "Discover";
if( empty($payment) ) $payment = "Cash ONLY";
$c1[] = array("label" => "Accepted Credit Cards", "val" => "{$payment}");
if( !empty($row["coupon"]) )
	$c1[] = array("label" => "Coupon", "val" => "<a href='coupon?coupon={$row["coupon"]}' title='coupon' target='_blank' rel='nofollow'>Click to see</a>");

$smarty->assign("c1", $c1);

$smarty->assign("name", $row["name"]);
$smarty->assign("loc_name", $row["loc_name"]);
$smarty->assign("id", $id);
$smarty->assign("editlink", "emp");

$smarty->assign("totalreview", $row["review"]);

$overall = $row["overall"];
if ($overall > 5) {
	reportAdmin("Overall bigger than 5", "", array("table" => "eroticmassage", "id" => $id));
	$overall = 5;
}
$smarty->assign("overall", $row["overall"]);

if( strlen($row["website"])>1 ) {
        $url = trim($row["website"]);
        if( strncmp($url, "http://", 7) )  $url = "http://" . $url;
        $web = parse_url($url);
        $web_url = $url;
        $web_host = $web["host"];
}

$addressLx = "";
if( $row["nonerotic"] ) $addressLx = "<span class='error'>*Non-Erotic Massage Reported*</span><br/>";
if( !empty($phone) ) $addressLx .= "<b>{$phone}</b><br />";
$addressLx .= "{$row["address1"]}";
if( !empty($row["address2"]) ) $addressLx .= ", {$row["address2"]}";
$addressLx .= "<br />{$loc}<br/>";
if($web_url) $addressLx .= "<a href=\"$web_url\" target=\"_blank\" rel=\"nofollow\" class=\"website\">$web_host</a>";
if( $row['email'] ) $addressLx .= "<a href=\"mailto:{$row['email']}\" class=\"website\">{$row['email']}</a>";

$smarty->assign("address", $addressLx);

//photos
$imagedir = "eroticmassage";

$pics = array();
$rex = $db->q("select picture, filename, thumb from place_picture where id = '$id' and module = 'eroticmassage'");
while($rox=$db->r($rex)) {
	$pics[] = array(
		"module" => "eroticmassage",
		"id" => $rox["picture"],
		"image" => "{$config_image_server}/{$imagedir}/".$rox["filename"],
		"thumb" => "{$config_image_server}/{$imagedir}/t/".$rox["thumb"],
		);
}

$photos = array();
if ($row["image"] != "" && $row["thumb"] != "") {
    $photos["image"] = "{$config_image_server}/{$imagedir}/{$row["image"]}";
    $photos["thumb"] = "{$config_image_server}/{$imagedir}/t/{$row["thumb"]}";

    if ($pics) {
        include_html_head("js", "/js/jquery.tools.scrollable.js");
        $pics_in_group = 2;
        $pics_groups = array_chunk($pics, $pics_in_group);
        $photos["pics"]  = $pics_groups;
    }
}
$smarty->assign("photos", $photos);

/*
// 8.4.2018 max: disable forums sitewide
//forums
$forum_id = 3;
$smarty->assign("forum_topic", forum::getLastTopicsForPlace($forum_id, $id));
$smarty->assign("new_topic_link", forum::getNewTopicLinkForCategory($forum_id, $id));
*/

if( !empty($row["owner"])&&($row["owner"]==$account_id||$account->isrealadmin()) ) $smarty->assign("isowner", true);

if (empty($row["owner"])) {
	$smarty->assign("business_link", true);
	$smarty->assign("business_shortcut", "?section=emp&id=$gItemID");
}

$core_state_name = "<a href='{$account->core_loc_array['loc_url']}$gModule/' title='{$row["loc_name"]} Erotic Massage Parlors'>{$row["loc_name"]} Erotic Massage Parlors</a>";
$smarty->assign("core_state_name", $core_state_name);

$smarty->assign("category", "erotic-massage-parlor");
$smarty->assign("gModule", $gModule);
$smarty->assign("what", "Erotic Massage Parlors");

//if massage parlor, try to display "read dirty reviews link
/*
global $config_dev_server;
require_once(_CMS_ABS_PATH."/inc/classes/class.curl.php");
$data = "phone={$row["phone"]}&zipcode={$row["zipcode"]}";
$curl = new curl("emp_review.txt", 1);
if ($config_dev_server) {
   //$resp = $curl->post("http://173.239.54.100/admin/emp_reviews", $data, 1, array("Host: www.eroticmp.com"));
   $resp = $curl->post("http://www.eroticmp.com/admin/emp_reviews", $data);
} else {
   $resp = $curl->post("http://www.eroticmp.com/admin/emp_reviews", $data);
}
$resp = trim($resp);
if ($resp) {
    $smarty->assign("dirty_reviews_link", htmlspecialchars($resp));
}
*/

//MapQuest and Leaflet include
include_html_head('js', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.js');
include_html_head('css', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.css');
include_html_head('js', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.js');
include_html_head('css', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.css');

if (permission::has("edit_all_places"))
	$smarty->assign("can_edit_place", true);

$file = $smarty->display(_CMS_ABS_PATH."/templates/place/place_place.tpl");

echo $file;

?>
