<?php

deprecated_controller();

global $db, $smarty, $account, $page;
$system = new system;

if( !($account_id = $account->isloggedin()) ) {
	$account->asklogin();
	return;
}

require_once(_CMS_ABS_PATH."/_cms_files/worker/array_sc.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
$form = new data;
$system = new system;

$form->table = "eroticmassage";

$c = new CColumnId("id", "BUSINESS ID");
$form->AppendColumn($c);

$c = new CColumnString("name", "Parlor Name");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("address1", "Parlor Address");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("address2", "Floor/Unit");
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "Zip Code");
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Parlor Phone");
$c->setColMandatory(true);
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnString("email", "E-Mail");
$form->AppendColumn($c);

$c = new CColumnHour("hours", "Hours Of Operation");
$form->AppendColumn($c);

$c = new CColumnMultipleForeign('eroticmassage_tag_assign', 'tag_assign_id', 'id', 'tag_id', 'eroticmassage_tag', 'tag_id', 'tag_name', 'Available Masseuse');
$form->AppendColumn($c);

$c = new CColumnFee("rate_30", "30 Minutes Rate");
$form->AppendColumn($c);

$c = new CColumnFee("rate_45", "45 Minutes Rate");
$form->AppendColumn($c);

$c = new CColumnFee("rate_60", "60 Minutes Rate");
$form->AppendColumn($c);

$c = new CColumnFee("rate_90", "90 Minutes Rate");
$form->AppendColumn($c);

$c = new CColumnFee("rate_120", "120 Minutes Rate");
$form->AppendColumn($c);

$c = new CColumnEnum("table_shower", "Table Shower");
$c->AddEnumOption("1", "Yes");
$c->AddEnumOption("0", "No");
$form->AppendColumn($c);

$c = new CColumnString("rate_table_shower", "Table Shower Fee");
$form->AppendColumn($c);

$c = new CColumnEnum("sauna", "Sauna");
$c->AddEnumOption("1", "Yes");
$c->AddEnumOption("0", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("jacuzzi", "Jacuzzi");
$c->AddEnumOption("1", "Yes");
$c->AddEnumOption("0", "No");
$form->AppendColumn($c);

$c = new CColumnBoolean("truckparking", "Truck Parking");
$c->setColTitle("Parking");
$c->AddOption("private_parking", "Private Parking");
$form->AppendColumn($c);

$c = new CColumnBoolean("cc_visa", "VISA & MC");
$c->setColTitle("Payments");
$c->AddOption("cc_ae", "American Express");
$c->AddOption("cc_dis", "Discover");
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setPath("eroticmassage");
$c->setWatermark();
$form->AppendColumn($c);

$c = new CColumnImage("thumb", "Thumbnail");
$c->setDimensions(220, 165);
$c->setWatermark();
$c->setPath("eroticmassage/t");
$form->AppendColumn($c);

$c = new CColumnImage("coupon", "Coupon");
$c->setMaxDimensions(700, 500);
$c->setPath("eroticmassage/c");
$form->AppendColumn($c);

$c = new CColumnPicture("eroticmassage_picture", "Extra Pictures", "eroticmassage");
$c->setPath("eroticmassage");
$c->setPathT("eroticmassage/t");
$form->AppendColumn($c);

$form->owner_mode = true;
$form->owner_link = "parlor";
$form->ShowPage();

?>

