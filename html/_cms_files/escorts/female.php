<?php
/**
 * Landing page for traffic coming from popunder (on eccie.net or other websites)
 * Redirects directly to female escorts page
 */
defined("_CMS_FRONTEND") or die("No Access");

global $db, $config_site_url, $mobile, $gIndexTemplate, $config_image_server, $sphinx, $smarty;
$gIndexTemplate = "blank_bootstrap.tpl";

//get ip address
if (isset($_REQUEST["ip"]))
	$ip_address = preg_replace('/[^A-Fa-f0-9\.:]/', '', $_REQUEST["ip"]);
else
	$ip_address = preg_replace('/[^A-Fa-f0-9\.:]/', '', account::getRealIp());

$go_url = null;
//50% of the time pop AFF, 50% of time pop AS
//removed on 2018-11-06
/*
$rand = rand(0,1);
if ($rand)
	$go_url = "https://medleyads.com/spot/21180.html";
*/
if (!$go_url) {
	//get city url from ip address
	$location = geolocation::getClosestNonEmptyLocationByIp($ip_address);
	if (!$location)
		$go_url = $config_site_url."/all-locations";
//	else
//		$go_url = $location->getUrl()."/female-escorts/";
}

//log into analytics
$ga_campaign = (array_key_exists("utm_campaign", $_REQUEST)) ? $_REQUEST["utm_campaign"] : null;
$url = (array_key_exists("REQUEST_URI", $_SERVER)) ? $_SERVER["REQUEST_URI"] : null;
$referer = (array_key_exists("HTTP_REFERER", $_SERVER)) ? $_SERVER["HTTP_REFERER"] : null;
$db->q("INSERT INTO analytics 
		(stamp, day, ref, url, referer, ip, forward_url, mobile) 
		VALUES 
		(?, ?, ?, ?, ?, ?, ?, ?)",
		[time(), date("Y-m-d"), $ga_campaign, $url, $referer, $ip_address, $go_url, intval($mobile)]
		);

if ($go_url)
	return system::go($go_url);

//display female escorts landing page
$cat_link = $location->getUrl()."/female-escorts/";

$sphinx->reset();
$sphinx->SetFilter('type', [1]);
$sphinx->SetFilter('bp', [0]);
$sphinx->SetFilter('loc_id', [$location->getId()]);
$sphinx->SetFilter('sponsor', [0]);
$sphinx->SetFilter('sponsor_mobile', [0]);
if ($mobile == 1) {
    $sphinx->SetFilter('display_mobile', array(1));
} else {
    $sphinx->SetFilter('display_desktop', array(1));
}
$sphinx->SetSortMode(SPH_SORT_EXTENDED, "updated DESC");
$sphinx->setGroupBy('cid', SPH_GROUPBY_ATTR, "updated DESC");
$item_per_page = 24;
$sphinx->SetLimits(0, $item_per_page, 100);
$results = $sphinx->Query("", "escorts");

$total = $results["total_found"];
$ids = null;
foreach($results["matches"] as $result) {
	//echo "<pre>".print_r($result, true)."</pre><br />\n";
    $cid = $result["attrs"]["cid"];
    $ids .= $ids ? (",".$cid) : $cid;
}
if (!$ids)
	return system::go($config_site_url);
$res = $db->q("
    SELECT c.*
    FROM classifieds c 
    WHERE c.id in ($ids) 
    GROUP BY c.id 
    ORDER BY field(c.id,$ids)
    ");
$cl = [];
$i = 0;
while ($row = $db->r($res)) {
	$title = str_replace(">", "&gt;", $row["title"]);
    $title = str_replace("<", "&lt;", $title);
    $title = str_replace("\"", "&quot;", $title);

    //determine thumbnail
    $thumb = false;
    if ($row["multiple_thumbs"]) {
        $arr = explode(",", preg_replace('/[^a-zA-Z0-9\.\,_\-]/', '', $row["multiple_thumbs"]));
        $thumb = $arr[array_rand($arr)];
    }
    if (!$thumb && $row["thumb"])
        $thumb = $row["thumb"];

	//on mobile in photo view we are not displaying 75x75 thumbnail (classifieds.thumb) but thumbnail version of first image
    if ($mobile == 1 && (!isset($_GET['view']) || $_GET['view'] != 'list')) {
        if ($row["multiple_thumbs"])
            $tRes = $db->q("select filename from classifieds_image where id = ? order by rand() LIMIT 1", [$row["id"]]);
        else
            $tRes = $db->q("select filename from classifieds_image where id = ? order by image_id LIMIT 1", [$row["id"]]);
        if ($db->numrows($tRes)) {
            $roy=$db->r($tRes);
            $large_thumb = $roy[0];
        } else
            $large_thumb = "";
    } else
        $large_thumb = "";

    $ad_loc_id = $ad_loc[$row['id']];
    if (array_key_exists($ad_loc_id, $loc_links))
        $link = $loc_links[$ad_loc_id]."/".$gModule."/".$row['id'];
    else
        $link = $cat_link.$row['id'];

	$clads[] = [
        "id" => $row["id"],
        "name" => $title,
        "thumb" => $thumb,
        "location" => ucfirst($row["location"]),
        "large_thumb" => $large_thumb,
        "link" => ($row["direct_link"]) ?: $link,
        "firstname" => $row['firstname'],
    ];
	
	$i++;
	if ($total % 2 == 1 && $i == $total - 1)
		break;
}
//_darr($clads);
$smarty->assign("clads", $clads);
$smarty->assign("link_view_all", $location->getUrl()."/female-escorts/");

$smarty->display(_CMS_ABS_PATH."/templates/escorts/female.tpl");
?>
