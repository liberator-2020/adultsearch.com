<?php

reportAdmin("AS: escorts/landing", "landing page from eccie popunder called ?");

/**
 * Landing page for traffic coming from popunder (on eccie.net)
 * Tries to detect user location ip address
 * Half of visitors go to city page, half to escorts page
 */
defined("_CMS_FRONTEND") or die("No Access");

global $db, $mobile, $smarty, $config_image_path, $config_image_server, $config_site_url;

function get_img_files_from_dir($dir) {
	$h = opendir($dir);
	if (!$h)
		return false;

	$files = [];
	while (false !== ($entry = readdir($h))) {
		$ext = substr($entry, -4);
		if (!in_array($ext, [".jpg", "jpeg", ".png", ".gif"]))
			continue;
		$files[] = $entry;
	}
	closedir($h);

	return $files;
}

function get_img_files_cache() {
	$files = [
		"11.jpg",
		"12.jpg",
		"13.jpg",
		"14.jpg",
		"15.jpg",
		"16.jpg",
		"17.jpg",
		"ak.jpg",
		"am.jpg",
		"ba.jpg",
		"bay.jpg",
		"ber.jpg",
		"bri.jpg",
		"dan.jpg",
		"eiv.jpg",
		"ga_1.jpg",
		"ga_3.jpg",
		"ga_4.jpg",
		"gb-1.jpg",
		"gb_2.jpg",
		"gl_1.jpg",
		"gl_2.jpg",
		"gl_3.jpg",
		"gl_4.jpg",
		"kar.jpg",
		"kel.jpg",
		"kit.jpg",
		"lac.jpg",
		"lel.jpg",
		"lil.jpg",
		"lis.jpg",
		"lus.jpg",
		"lve.jpg",
		"meg.jpg",
		"mim.jpg",
		"mol.jpg",
		"mon.jpg",
		"myr.jpg",
		"nat.jpg",
		"nor.jpg",
		"sar.jpg",
		"sop.jpg",
		"sug.jpg",
	];
	return $files;
}

function get_img_files($dir, $count) {

	//$files = get_img_files_from_dir($dir);
	$files = get_img_files_cache();

	shuffle($files);
	$files = array_slice($files, 0, $count);

	return $files;
}

//$images = get_img_files($config_image_path."escorts", 15);
$images = get_img_files($config_image_path."escorts", 15);
$images = array_map(function($filename) { global $config_image_server; return $config_image_server."/escorts/".$filename;}, $images);
$_REQUEST["template"] = "blank_mobile.tpl";
$smarty->assign("images", $images);


//get city url from ip address
if (isset($_REQUEST["ip"]))
	$ip_address = preg_replace('/[^A-Fa-f0-9\.:]/', '', $_REQUEST["ip"]);
else
	$ip_address = preg_replace('/[^A-Fa-f0-9\.:]/', '', account::getRealIp());

$result = geolocation::getLocationByIp($ip_address);
//$smarty->assign("localisation_result", print_r($result, true));

if (!$result["city_url"]) {
	//we were not able to geolocate user, link to homepage
	$smarty->assign("link", $config_site_url);
	$smarty->display(_CMS_ABS_PATH."/templates/escorts/landing.tpl");
	return;
}
if ($result["city_name"])
	$smarty->assign("city", $result["city_name"]);

//$eccie_template = rand(0,1);
$eccie_template = 0;
if ($eccie_template) {
	$template_filename = "landing_eccie.tpl";
} else {
	$template_filename = "landing.tpl";
}

//$link_to_escorts = rand(0,1);
$link_to_escorts = 0;
if ($link_to_escorts) {
	//link to escorts page
	$link = $result["city_url"]."/female-escorts/";
	if ($eccie_template)
		$link .= "?utm_source=escortslanding&utm_medium=pop&utm_campaign=ecciepoplist";
	else
		$link .= "?utm_source=escortslanding&utm_medium=pop&utm_campaign=poplist";
} else {
	//link to city page
	$link = $result["city_url"];
	if ($eccie_template)
		$link .= "?utm_source=escortslanding&utm_medium=pop&utm_campaign=ecciepopcity";
	else
		$link .= "?utm_source=escortslanding&utm_medium=pop&utm_campaign=popcity";
}

$smarty->assign("link", $link);
$smarty->display(_CMS_ABS_PATH."/templates/escorts/{$template_filename}");
return;

?>
