<?php

defined("_CMS_FRONTEND") or die("No Access");

require(_CMS_ABS_PATH."/_cms_files/classifieds/array.php");

global $db, $smarty, $page, $account, $gTitle, $gDescription, $gKeywords, $advertise, $mobile, $gModule, $gLocation, $ctx, $config_dev_server, $gCanonicalUrl, $sphinx;

session_write_close();

$form = new form;
//$forum = new forum();
$classifieds = new classifieds;
$dir = new dir;

$type = $classifieds->getTypeByModule($gModule);
if (!$type)
	system::moved('/');

$page = intval($_REQUEST["page"]);
if ($page == 0)
	$page = 1;
$ctx->page = $page;

$location = location::findOneById($ctx->location_id);

if ($location)
	$gCanonicalUrl = $location->getUrl()."/{$ctx->category}";

$smarty->assign("category_path", $location->getUrl()."/{$ctx->category}");
$smarty->assign("link_back", $location->getUrl());
$smarty->assign("location_video_html", $location->getVideoHtml());

if ($ctx->location_type == 3) {
	$smarty->assign("city_url", $location->getUrl());
	$smarty->assign("loc_parent_name", $location->getParent()->getName());
	$smarty->assign("loc_parent_url", $location->getParent()->getUrl());
}

//seo
/*
if ($type != 2) {
	$res = $db->q("SELECT description FROM seo WHERE location_id = ? AND type = 'LE' LIMIT 1", array($location->getId()));
	if ($db->numrows($res)) {
		$row = $db->R($res);
		$smarty->assign("category_description", $row["description"]);
	}
}
*/

$account_id = $account->isloggedin();

$loc_around = array();
if ($account->core_loc_id)
	$loc_around = $account->locAround();
//_darr($loc_around);

$cat_name = $form->arrayValue($type_array, $type);
$gtitle = $form->arrayValue($gtitle_array, $type);
$gdesc = $form->arrayValue($gdescription_array, $type);
$gkey = $form->arrayValue($gkeywords_array, $type);

if ($ctx->location_type < 2)
	system::moved("/");

$core_state_id = $account->corestate(1);
if (!$core_state_id)
	return;

if ($account->core_state_id)
	$core_state_id = $account->core_state_id;

$advertise->popup();

//if ($ctx->category == "female-escorts")
//	$advertise->interstitial();

$gTitle = str_replace('{state}', $ctx->state_name, str_replace('{city}', $ctx->city_name, $gtitle));
$loc_name = $account->core_loc_array['loc_name'];
$core_state_name = "<a href='{$account->core_loc_array['loc_url']}{$gModule}/' title='$loc_name $cat_name'>$loc_name $cat_name</a>";
$smarty->assign("header_title", $core_state_name);
$gDescription = str_replace('{city}', $ctx->city_name, $gdesc);
$gKeywords = str_replace('{state}', $ctx->state_name, str_replace('{city}', $ctx->city_name, $gkey));

$h1title = str_replace('{state}', $ctx->state_name, str_replace('{city}', $ctx->city_name, $form->arrayValue($h1title_array, $type)));
$smarty->assign("h1title", $h1title);

if (!$account->core_loc_id || $account->core_loc_type < 3) {
	//--------------------------------------------------------------------------------
	//when we choose female-escorts in state location, display city select template...
	//--------------------------------------------------------------------------------
	$res = $db->q("
		SELECT cl.loc_id, l.loc_name, count(cl.loc_id) c, l.loc_url, GROUP_CONCAT(c.id)
		FROM classifieds_loc cl
		INNER JOIN classifieds c ON c.id = cl.post_id AND c.api = 0 AND c.deleted IS NULL AND c.sponsor = 0 AND c.sponsor_mobile = 0
		INNER JOIN location_location l on cl.loc_id = l.loc_id
		WHERE l.loc_parent = ? and cl.done = 1 and cl.type = ?
		GROUP BY l.loc_id
		ORDER BY l.loc_name",
		[$account->core_loc_id, $type]
		);

	while($row=$db->r($res)) {
		if (isset($locs[$row["loc_id"]]))
			$locs[$row["loc_id"]]['c'] += $row["c"];
		else
			$locs[$row["loc_id"]] = array(
				"loc_id" => $row["loc_id"],
				"loc_name" => $row["loc_name"],
				"c" => $row["c"],
				"loc_url" => $row["loc_url"], 
				"link" => "{$row['loc_url']}$gModule/"
				);
		$loc_url = $row['loc_url'];
	}

	asort($locs);
	$smarty->assign("locs", $locs);
	$smarty->assign("type", $type);

	if( count($locs) == 1 )
		system::moved("{$loc_url}$gModule/");

	$subcat_cols=5;
	if (!empty($locs)) {
		$loc_per_column = ceil(count($locs)/$subcat_cols);
		$loc_columns = array_chunk($locs, $loc_per_column);
		$smarty->assign("loc_columns", $loc_columns);
	}

	$cat_name = $form->arrayValue($type_array, $type);
	$gTitle = "{$_SESSION["core_state_name"]} $cat_name";
	$smarty->assign("cat_name", $cat_name);

	$core_state_name = "<a href='{$_SERVER['REQUEST_URI']}' title='$account->core_loc_array['loc_name'] $cat_name'>{$account->core_loc_array['loc_name']} $cat_name</a>";
	$smarty->assign("core_state_name", $core_state_name);

	$smarty->assign("gModule", $gModule);
	$gTitle = "{$account->core_loc_array['loc_name']} $cat_name";
	$smarty->display(_CMS_ABS_PATH."/templates/classifieds/classifieds_loc_select.tpl");
	return;
}

$loc_name = $account->core_loc_array["loc_name"];
$loc_id = $account->core_loc_array["loc_id"];

$core_state_name = "<a href='{$account->core_loc_array['loc_url']}{$gModule}/' title='$loc_name $cat_name'>$loc_name $cat_name</a>";
$smarty->assign("header_title", $core_state_name);

$ethnicity = isset($_GET["ethnicity"]) ? GetGetParam("ethnicity") : NULL;
$haircolor = isset($_GET["haircolor"]) ? GetGetParam("haircolor") : NULL;
$eyecolor = isset($_GET["eyecolor"]) ? GetGetParam("eyecolor") : NULL;
$pornstar = isset($_GET["pornstar"]) ? 1 : NULL;
$ter = isset($_GET["ter"]) ? 1 : NULL;
$tantra = isset($_GET["tantra"]) ? 1 : NULL;
$visa = isset($_GET["payment_visa"]) ? 1 : NULL;
$amex = isset($_GET["payment_amex"]) ? 1 : NULL;
$dis = isset($_GET["payment_dis"]) ? 1 : NULL;
$pregnant = isset($_GET["pregnant"]) ? 1 : NULL;
$visiting = isset($_GET["visiting"]) ? 1 : NULL;
$incall_rate = isset($_GET["incall_rate"]) ? intval($_GET["incall_rate"]) : NULL;
$outcall_rate = isset($_GET["outcall_rate"]) ? intval($_GET["outcall_rate"]) : NULL;
$available = isset($_GET["available"]) ? intval($_GET["available"]) : NULL;
$incall_min = isset($_GET["incall_min"]) && intval($_GET["incall_min"]) ? intval($_GET["incall_min"]) : NULL;
$incall_max = isset($_GET["incall_max"]) && intval($_GET["incall_max"]) ? intval($_GET["incall_max"]) : NULL;
$outcall_min = isset($_GET["outcall_min"]) && intval($_GET["outcall_min"]) ? intval($_GET["outcall_min"]) : NULL;
$outcall_max = isset($_GET["outcall_max"]) && intval($_GET["outcall_max"]) ? intval($_GET["outcall_max"]) : NULL;
$featured = isset($_GET['featured']) ? 1 : NULL;
$language = isset($_GET['language']) ? intval($_GET['language']) : NULL;

$outcall_max_max = 1000;
$incall_max_max = 1000;

$ad_type = NULL;
switch($type) {
	case 1: $ad_type = 16; break;
	case 2: $ad_type = 17; break;
	case 6: $ad_type = 18; break;
	case 12: $ad_type = 20; break;
	case 13: $ad_type = 19; break;
	case 3: $ad_type = 21; break;
	case 4: $ad_type = 22; break;
	case 7: $ad_type = 23; break;
	case 15: $ad_type = 24; break;
	case 14: $ad_type = 25; break;
	case 5: $ad_type = 16; break;
	default: $ad_type = NULL;
}
if( $ad_type ) $advertise->showAd($ad_type, $loc_id?$loc_id:$core_state_id);

if ($cat_name == "Female Escorts")
	$cat_name = "Escorts";
$smarty->assign("cat_name", $cat_name);

$currency = dir::getCurrencyCodeByCountry($ctx->country);
$currency_sign = currency_convert::getPriceTagOnly($currency);
$smarty->assign("currency_sign", $currency_sign);

$filter_link = array();
$gTitlex = ""; 
$incall_filter = false;
foreach($_GET as $key => $value) {
	if(in_array($key, [
			"query", "ethnicity", "haircolor", "eyecolor", "pornstar", "ter", "payment_visa", "payment_amex", "payment_dis", "pregnant", 
			"visiting", "incall_rate", "outcall_rate", "available", "incall_min", "incall_max", "outcall_min", "outcall_max", "featured", "language"
			]) 
		&& $value != "" ) {
		$skipfilter = 0;
		$ftype = $key;
		$valuex = $value == 2 ? "Yes" : ($value == 1 ? "No" : $value);
		if( $key == "lingerie" ) $key = "Lingerie: $valuex";
		else if( $key == "ethnicity" ) {
			$gTitlex .= $val = $form->arrayValue($classifieds->adbuild_ethnicity, $value)." ";
			$key = "Ethnicity: ".trim($val);
		}
		else if( $key == "language" ) {
			$gTitlex .= $val = $form->arrayValue($classifieds->adbuild_language, $value)." ";
			$key = "Language: ".trim($val);
		}
		else if( $key == "haircolor" ) {
			$val = $form->arrayValue($classifieds->adbuild_haircolor, $value);
			$key = "Hair Color: $val";
			$gTitlex .= " $val hair ";
		}
		else if( $key == "eyecolor" ) {
			$val = $form->arrayValue($classifieds->adbuild_eyecolor, $value);
			$key = "Eye Color: $val";
			$gTitlex .= " $val eyes ";
		}
		else if( $key == "pornstar" ) { $key = "Pornstar"; $gTitlex .= $key . " "; }
		else if( $key == "ter" ) { $key = "TER Reviewed"; $gTitlex .= "TER Reviewed "; }
		else if( $key == "query" ) { $key = (is_numeric($valuex) ? "# " : "") . $valuex; }
		else if( $key == "payment_visa" ) { $key = "Payment: Visa/Mastercard"; $gTitlex .= "Visa/Mastercard ";}
		else if( $key == "payment_amex" ) { $key = "Payment: AMEX"; $gTitlex .= "AMEX "; }
		else if( $key == "payment_dis" ) { $key = "Payment: Discover Card"; $gTitlex .= "Discover Card ";}
		else if( $key == "pregnant" ) { $key = "Pregnant"; $gTitlex .= "Pregnant ";}
		else if( $key == "visiting" ) { $key = "Visiting"; $gTitlex .= "Visiting ";}
		else if( $key == "incall_rate" ) { $key = "Incall Rate: {$currency_sign}$value"; $gTitlex .= "Incall Rate {$currency_sign}$value ";}
		else if( $key == "outcall_rate" ) { $key = "Outcall Rate: {$currency_sign}$value"; $gTitlex .= "Outcall Rate {$currency_sign}$value ";}
		else if( $key == "available" ) { $key = $value == 1 ? "Incall" : ($value == 2 ? "Outcall" : "In/Out Call"); $gTitlex .= $value == 1 ? "Incall " : ($value == 2 ? "Outcall " : "In/Out Call "); }
		else if( $key == "featured" ) { $key = "Featured"; $gTitlex .= "VIP ";}
		elseif($key == "incall_min" or $key == "incall_max") {
			if( $incall_filter ) continue;
			$key = "Incall: " . ($incall_min && $incall_max ? "{$currency_sign}$incall_min to {$currency_sign}$incall_max" : ($incall_min ? "more than {$currency_sign}$incall_min" : "less than {$currency_sign}$incall_max")); 
			$incall_filter = true;
			$skipfilter = 1;
			$filter_link[] = array("id"=>array($_GET['incall_min'], $_GET['incall_max']),"type"=>array('incall_min', 'incall_max'),"name"=>$key);
			$gTitlex .= $key . " ";			
		}
		elseif($key == "outcall_min" or $key == "outcall_max") {
			if( $outcall_filter ) continue;
			$key = "Outcall: " . ($outcall_min && $outcall_max ? "{$currency_sign}$outcall_min to {$currency_sign}$outcall_max" : ($outcall_min ? "more than {$currency_sign}$outcall_min" : "less than {$currency_sign}$outcall_max")); 
			$outcall_filter = true;
			$skipfilter = 1;
			$filter_link[] = array("id"=>array($_GET['outcall_min'], $_GET['outcall_max']),"type"=>array('outcall_min', 'outcall_max'),"name"=>$key);
		}
		if( !$skipfilter ) $filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key);
	}
}

if( $account->core_loc_type == 4 ) {
	$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$account->core_loc_array['loc_name'],"link"=>"{$account->core_loc_array['lp_loc_url']}$gModule/");
}


$sphinx_index = "escorts";
$sphinx->reset();

$item_per_page = isset($_GET["ipp"]) ? intval($_GET["ipp"]) : 30;
if( $mobile == 1 ) {
	$item_per_page = 20;
//	$sphinx->SetFilter('total_loc', array(1,2,3,4));
	$sphinx->SetFilter('display_mobile', array(1));
} else {
	//dena skype 35.feb.2014 16:21
	//is there an easy way you can make these ads that I set up as mobile sponsor so they don't show up on desktop or in the ad list? like, desktop sponsor ads only show up at the top of the desktop site list and they don't sit in below with the other ads and they don't show up on mobile at all.
	$sphinx->SetFilter('display_desktop', array(1));
}

if ($item_per_page > 200)
	$item_per_page = 100;
$start = (($page*$item_per_page)-$item_per_page);
if ($start < 0)
	$start = 0;

$sphinx->SetLimits( $start, $item_per_page, 2000 );

$savequery = false;
if( isset($_GET["query"]) && is_numeric($_GET["query"]) ) {
	$query_id = intval($_GET["query"]);
	$res = $db->q("SELECT id FROM classifieds WHERE api = 0 AND deleted IS NULL AND id = ?", array($query_id));
	if( $db->numrows($res) ) {
		$savequery = true;
		$savedquery = $_GET["query"];
		$sphinx->SetFilter('cid', array($query_id));
	}
}

//_d("account->core_loc_id={$account->core_loc_id}");
$locs = NULL;

$sphinx->SetFilter('type', array($type));

//dallas skype 22.8.2017 - dont display ads from bp anymore
$sphinx->SetFilter('bp', array(0));

//_d("type={$type}");

$sphinx->SetFilter('loc_id', $loc_around);

if( $ethnicity ) $sphinx->SetFilter('ethnicity', array($ethnicity));
if( $language ) $sphinx->SetFilter('language', array($language));
if( $haircolor ) $sphinx->SetFilter('haircolor', array($haircolor));
if( $eyecolor ) $sphinx->SetFilter('eyecolor', array($eyecolor));
if( $pornstar ) $sphinx->SetFilter('pornstar', array(1));
if( $ter ) $sphinx->SetFilter('ter', array(1));
if( $tantra && $type == 6) $sphinx->SetFilter('tantra', array(1));
if( $visa ) $sphinx->SetFilter('payment_visa', array(1));
if( $amex ) $sphinx->SetFilter('payment_amex', array(1));
if( $dis ) $sphinx->SetFilter('payment_dis', array(1));
if( $pregnant ) $sphinx->SetFilter('pregnant', array(1));
if( $visiting ) $sphinx->SetFilter('visiting', array(1));
if( $incall_rate ) $sphinx->SetFilter('incall_rate', array($incall_rate));
if( $outcall_rate ) $sphinx->SetFilter('outcall_rate', array($outcall_rate));
if( $available ) $sphinx->SetFilter('available', array($available));
if( $incall_min || $incall_max ) $sphinx->SetFilterRange('incall_rate', $incall_min, $incall_max?$incall_max:$incall_max_max);
if( $outcall_min || $outcall_max ) $sphinx->SetFilterRange('outcall_rate', $outcall_min, $outcall_max?$outcall_max:$outcall_max_max);
if( $featured ) {
	$sphinx->SetFilter('account_id', array(0), true);
	$sphinx->SetFilter('total_loc', array(1,2,3,4));
}

if ($mobile == 1) {
	$sphinx->SetSortMode(SPH_SORT_EXTENDED, "sponsor_mobile DESC, sponsor_position ASC, bp ASC, eccie_promo ASC, updated DESC");
	$sphinx->setGroupBy('cid', SPH_GROUPBY_ATTR, "sponsor_mobile DESC, sponsor_position ASC, bp ASC, eccie_promo ASC, updated DESC");
} else {
	$sphinx->SetSortMode(SPH_SORT_EXTENDED, "sponsor DESC, sponsor_position ASC, bp ASC, eccie_promo ASC, updated DESC");
	$sphinx->setGroupBy('cid', SPH_GROUPBY_ATTR, "sponsor DESC, sponsor_position ASC, bp ASC, eccie_promo ASC, updated DESC");
}

if ($savequery)
	$query = $_GET["query"] = "";
else if ($_GET["query"] != "")
	$query = $_GET["query"];

//querying for phone number fix
$query = preg_replace('/[^0-9]*([0-9]{3})[^0-9]*([0-9]{3})[^0-9]*([0-9]{4})[^0-9]*/', '\1\2\3', $query);

$results = $sphinx->Query ( $query, $sphinx_index );
//_d($sphinx->getLastError());
//_darr($results);
$total = $results["total_found"];

if ($total < 1)
	_d("sphinx last error = '{$sphinx->getLastError()}'");

$result = $results["matches"];
//_darr($result);

if( count($locs) ) {
	$loc_idx = "";
	foreach($locs as $loc)
		$loc_idx  .= $loc_idx ? (",".$loc["loc_id"]) : $loc["loc_id"];

	$res = $db->q("select loc_id, loc_name, loc_url from location_location where loc_id in ($loc_idx) order by loc_name");
	$loc_idx = array();
	while($rox=$db->r($res)) {
		$count = 0;
		foreach($locs as $l) {
			if( $l["loc_id"] == $rox["loc_id"] ) {
				$count = $l["count"]; break;
			}
		}
		$loc_idx[] = array($rox["loc_id"]=>$rox["loc_name"]." ($count)" );
		$category[] = array("id"=>"City", "type"=>"Location", "name"=>$rox["loc_name"], "c"=>$count,"link"=>"{$rox["loc_url"]}$gModule/","title"=>"{$rox["loc_name"]}");
	}
	
	$locs = $form->select_add_select();
	$locs .= $form->select_add_array($loc_idx, isset($_GET["loc_id"])?$_GET["loc_id"]:"");
}


//if( $category ) $refine[] = array("name"=>"Locations", "col"=>1, "alt"=>$category);

$sphinx->SetLimits( 0, 100, 100 );

$refine_further = in_array($type, array(14, 15)) ? 0 : 1;

if( $refine_further && is_null($ethnicity) ) {
	$typex = "ethnicity";
	$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$category = NULL;
	foreach($results["matches"] as $match) {
		if( !$match["attrs"]["ethnicity"] ) continue;
		$namex = $form->arrayValue($classifieds->adbuild_ethnicity, $match["attrs"]["ethnicity"]);
		$category[] = array("id"=>$match["attrs"][$typex], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
	if( $category ) {
		$refine[] = array("name"=>"Ethnicity", "col"=>2, "alt"=>$category);
	}
}
if( $refine_further && is_null($language) ) {
	$typex = "language";
	$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$category = NULL;
	foreach($results["matches"] as $match) {
		if( !$match["attrs"][$typex] ) continue;
		$namex = $form->arrayValue($classifieds->adbuild_language, $match["attrs"]["@groupby"]);
		$category[] = array("id"=>$match["attrs"]["@groupby"], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
	if( $category ) {
		$refine[] = array("name"=>"Language", "col"=>2, "alt"=>$category);
	}
}
if( $refine_further && is_null($available) ) {
	$typex = "available";
	$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$category = NULL;
	foreach($results["matches"] as $match) {
		if( !$match["attrs"][$typex] ) continue;
		$namex = $match["attrs"][$typex] == 1 ? "Incall" : ($match["attrs"][$typex] == 2 ? "Outcall" : "Incall/Outcall");
		$category[] = array("id"=>$match["attrs"][$typex], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
	if( $category ) $refine[] = array("name"=>"Incall/Outcall", "col"=>2, "alt"=>$category);
}

if( $refine_further && is_null($incall_rate) ) {
	$typex = "incall_rate";
	//if( $incall_min || $incall_max ) $sphinx->ResetFilter('incall_rate'); 
	$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, 'incall_rate ASC' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetGroupBy();
	//if( $incall_min || $incall_max ) $sphinx->SetFilterRange('incall_rate', $incall_min, $incall_max?$incall_max:$incall_max_max); 
	$category = NULL;
	$incall_select = NULL;
	foreach($results["matches"] as $match) {
		$namex = "{$currency_sign}{$match["attrs"]["incall_rate"]}.00";
		if( $match["attrs"]["incall_rate"] ) {
			$category[] = array("id"=>$match["attrs"][$typex], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
			$incall_select[] = array($match["attrs"][$typex]=>"$namex ({$match["attrs"]["@count"]})");
			$incall_array[] = $match["attrs"]["incall_rate"];
			$incall_array[] = "$namex ({$match["attrs"]["@count"]})";
		}
	}
	if( $category ) {
		$smarty->assign('incall_array', $incall_array);
		$incall_selected = !$incall_max ? "selected='selected'" : "";
		$incall_s = "<select name='incall_min' class='text short' id='incall_min' onChange=\"javascript:ls_oc('incall_min','incall_max',incall,false);\"><option 
value=''>No Min</option>" . $form->select_add_array($incall_select, $incall_min) . "</select> - " .
			"<select name='incall_max' class='text short' id='incall_max'>" . $form->select_add_array($incall_select, $incall_max) . "<option value='' 
$incall_selected>No Max</option></select>";
		$smarty->assign("incall_rate", $incall_s);
	}
}
if( $refine_further && is_null($outcall_rate) ) {
	$typex = "outcall_rate";
	$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, 'outcall_rate ASC' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetGroupBy();
	//if( $outcall_min || $outcall_max ) $sphinx->SetFilterRange('outcall_rate', $outcall_min, $outcall_max?$outcall_max:$outcall_max_max); 
	$category = $outcall_select = NULL;
	foreach($results["matches"] as $match) {
		$namex = "{$currency_sign}{$match["attrs"][$typex]}.00";
		if( $match["attrs"][$typex] ) {
			$category[] = array("id"=>$match["attrs"][$typex], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
			$outcall_select[] = array($match["attrs"][$typex]=>"$namex ({$match["attrs"]["@count"]})");
			$outcall_array[] = $match["attrs"]["outcall_rate"];
			$outcall_array[] = "$namex ({$match["attrs"]["@count"]})";
		}
	}
	if( $category ) {
		$smarty->assign('outcall_array', $outcall_array);
		$outcall_selected = !$outcall_max ? "selected='selected'" : "";
		$outcall_s = "<select name='outcall_min' class='text short' id='outcall_min' 
onChange=\"javascript:ls_oc('outcall_min','outcall_max',outcall,false);\"><option value=''>No Min</option>" . $form->select_add_array($outcall_select, $outcall_min) . "</select> - " .
			"<select name='outcall_max' class='text short' id='outcall_max'>" . $form->select_add_array($outcall_select, $outcall_max) . "<option value='' $outcall_selected>No Max</option></select>";
		$smarty->assign("outcall_rate", $outcall_s);
		//$refine[] = array("name"=>"Outcall Rate", "col"=>2, "alt"=>$category);
	}
}

$sphinx->SetLimits( 0, 10, 10 );
if( $refine_further && is_null($haircolor) ) {
	$typex = "haircolor";
	$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$category = NULL;
	foreach($results["matches"] as $match) {
		if( !$match["attrs"]["haircolor"] ) continue;
		$namex = $form->arrayValue($classifieds->adbuild_haircolor, $match["attrs"]["haircolor"]);
		$category[] = array("id"=>$match["attrs"][$typex], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
	if( $category ) $refine[] = array("name"=>"Hair Color", "col"=>2, "alt"=>$category);
}
if( $refine_further && is_null($eyecolor) ) {
	$typex = "eyecolor";
	$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$category = NULL;
	foreach($results["matches"] as $match) {
		if( !$match["attrs"]["eyecolor"] ) continue;
		$namex = $form->arrayValue($classifieds->adbuild_eyecolor, $match["attrs"]["eyecolor"]);
		$category[] = array("id"=>$match["attrs"][$typex], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
	if( $category ) $refine[] = array("name"=>"Eye Color", "col"=>2, "alt"=>$category);
}
$category = NULL;

if( $refine_further && is_null($ter) ) {
	$typex = "ter";
	$sphinx->SetFilter($typex, array(1));
	$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter($typex);
	foreach($results["matches"] as $match) {
		$namex = "TER Reviewed";
		$category[] = array("id"=>$match["attrs"][$typex], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
}
if( $refine_further && is_null($pornstar) ) {
		$typex = "pornstar";
	$sphinx->SetFilter('pornstar', array(1));
		$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, '@count desc' );
		$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter('pornstar');
		foreach($results["matches"] as $match) {
				$namex = "Pornstar";
				$category[] = array("id"=>$match["attrs"][$typex], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
		}
}
if( $refine_further && is_null($tantra) ) {
		$typex = "tantra";
	$sphinx->SetFilter($typex, array(1));
		$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, '@count desc' );
		$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter($typex);
		foreach($results["matches"] as $match) {
				$namex = "Tantra Massage";
				$category[] = array("id"=>$match["attrs"][$typex], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
		}
}
if( $refine_further && is_null($pregnant) ) {
		$typex = "pregnant";
	$sphinx->SetFilter($typex, array(1));
		$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, '@count desc' );
		$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter($typex);
		foreach($results["matches"] as $match) {
				$namex = "Pregnant";
				$category[] = array("id"=>$match["attrs"][$typex], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
		}
}
if( $refine_further && is_null($visiting) ) {
		$typex = "visiting";
	$sphinx->SetFilter($typex, array(1));
		$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, '@count desc' );
		$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter($typex);
		foreach($results["matches"] as $match) {
				$namex = "Visiting";
				$category[] = array("id"=>$match["attrs"][$typex], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
		}
}

if( $refine_further && is_null($featured) ) { //&& in_array($account->core_loc_id, array(4712, 15227)) ) {
		$typex = "account_id";
	$sphinx->SetFilter($typex, array(0), true);
	$sphinx->SetFilter('total_loc', array(1,2,3,4));
	$sphinx->ResetGroupBy();
		$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter($typex);
	$sphinx->ResetFilter('total_loc');
	if( $results["total_found"] ) {
				$namex = "Featured";
				$category[] = array("id"=>1, "type"=>"featured", "name"=>$namex, "c"=>$results["total_found"]);
		}
}

if ($refine_further && (is_null($ter) || is_null($pornstar) || is_null($tantra)) && $category)
	$refine[] = array("name"=>"Services", "col"=>1, "alt"=>$category);

$category = NULL;
if( $refine_further && is_null($visa) ) {
		$typex = "payment_visa";
	$sphinx->SetFilter($typex, array(1));
		$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, '@count desc' );
		$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter($typex);
		foreach($results["matches"] as $match) {
				$namex = "Visa - Mastercard";
				$category[] = array("id"=>$match["attrs"][$typex], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
		}
}
if( $refine_further && is_null($amex) ) {
		$typex = "payment_amex";
	$sphinx->SetFilter($typex, array(1));
		$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, '@count desc' );
		$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter($typex);
		foreach($results["matches"] as $match) {
				$namex = "AMEX";
				$category[] = array("id"=>$match["attrs"][$typex], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
		}
}
if( $refine_further && is_null($dis) ) {
	$typex = "payment_dis";
	$sphinx->SetFilter($typex, array(1));
	$sphinx->SetGroupBy( $typex, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index );
	$sphinx->ResetFilter($typex);
	foreach($results["matches"] as $match) {
		$namex = "Discover Card";
		$category[] = array("id"=>$match["attrs"][$typex], "type"=>$typex, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
}


if (!is_null($category))
	$refine[] = array("name"=>"Payment Options", "col"=>1, "alt"=>$category);

$smarty->assign("refine", $refine);

if ($savequery)
	$_GET["query"] = $savedquery;

//_darr($results);

$loc_name = $row["loc_name"];

$ad_loc = [];
$sponsor_clad_ids = [];
$stickies = [];
foreach($result as $res) {
	$cid = $res["attrs"]["cid"];
	$ids .= $ids ? (",".$cid) : $cid;
	$loc_id = $res["attrs"]["loc_id"];
	$ad_loc[$cid] = $loc_id;
	if ($res["attrs"]["sponsor"] || $res["attrs"]["sponsor_mobile"])
		$sponsor_clad_ids[] = $cid;
	if($res["attrs"]["sponsor"] == 1 || $res["attrs"]["sponsor_mobile"] == 1)
		$stickies[] = $cid;
}

//_d("ids=".$ids);
//_darr($ad_loc);
//_darr($sponsor_clad_ids);
//_darr($stickies);

$loc_links = array();
foreach ($ad_loc as $loc_id) {
	if (array_key_exists($loc_id, $loc_links))
		continue;
	$loc = location::findOneById($loc_id);
	if (!$loc)
		continue;
	$loc_links[$loc_id] = $loc->getUrl();
}
//_darr($loc_links);


//_d("ids={$ids}");
if ($ids)
	$res = $db->q("
	SELECT c.*, date_format(date,'%d') day, date_format(date,'%a. %b. %e') day_title 
	FROM classifieds c 
	WHERE c.id in ($ids) 
	GROUP BY c.id 
	ORDER BY field(c.id,$ids)
	");

//-------------------------------------------
//--- REORDERING ADS - SOME SPECIAL CASES ---
//-------------------------------------------
// 1. if ad has set exact position, force put it in that position
// 2. spread "fake" ads (those that have more than 1 location) across the resultset (every 4th ad)
$items = array();
$items_sponsor = array();
$items_fake = array();
$sponsored = 0;
while($row=$db->r($res)) {
	if ($row["sponsor"] || $row["sponsor_mobile"]) {
		// sponsor and mobile sponsor ads are always (even if they are posted in multiple locations) displayed correctly at the top of the list
//		_d("add 1 ".$row["id"]);
		$items_sponsor[] = $row;
		$sponsored++;
		continue;
	}	
	// lets pull out "fake" classifieds from other locations out of the resultset
//		_d("fake ".$row["id"]." loc=".$row["loc_id"]);
	$items_fake[] = $row;
}

//jay 2.1.2019 hardcoding logic:
//if sponsor ads of account #157187 and account #368371 are both present...
//... with 75% probability display the one of #368371, otherwise display the one of #157187
//jay 25.1.2019 - replace account #368371 with #47958
$acc_368371_index = $acc_47958_index = $acc_157187_index = $acc_400344_index = $acc_400344_1_index = $acc_400344_2_index = null;
foreach ($items_sponsor as $key => $ad) {
	if ($ad["account_id"] == 47958)
		$acc_47958_index = $key;
	if ($ad["account_id"] == 368371)
		$acc_368371_index = $key;
	if ($ad["account_id"] == 157187)
		$acc_157187_index = $key;
	if ($ad["account_id"] == 400344 && $ad["id"] == 1461176)
		$acc_400344_1_index = $key;
	if ($ad["account_id"] == 400344 && $ad["id"] == 1461188)
		$acc_400344_2_index = $key;
}
if (!is_null($acc_400344_1_index) && !is_null($acc_400344_2_index)) {
	$rand = rand(1, 2);
	if ($rand == 1) {
		unset($items_sponsor[$acc_400344_1_index]);
		$acc_400344_index = $acc_400344_2_index;
	} else {
		unset($items_sponsor[$acc_400344_2_index]);
		$acc_400344_index = $acc_400344_1_index;
	}
} else if (!is_null($acc_400344_1_index)) {
	$acc_400344_index = $acc_400344_1_index;
} else if (!is_null($acc_400344_2_index)) {
	$acc_400344_index = $acc_400344_2_index;
}
if (!is_null($acc_368371_index) && !is_null($acc_157187_index) && !is_null($acc_400344_index)) {
	$rand = rand(1, 4);
	if ($rand == 1) {
		unset($items_sponsor[$acc_368371_index]);
		unset($items_sponsor[$acc_400344_index]);
	} else if ($rand == 2) {
		unset($items_sponsor[$acc_368371_index]);
		unset($items_sponsor[$acc_157187_index]);
	} else {
		unset($items_sponsor[$acc_157187_index]);
		unset($items_sponsor[$acc_400344_index]);
	}
} else if (!is_null($acc_368371_index) && !is_null($acc_157187_index)) {
	$rand = rand(1, 4);
	if ($rand == 1) {
		unset($items_sponsor[$acc_368371_index]);
	} else {
		unset($items_sponsor[$acc_157187_index]);
	}
} else if (!is_null($acc_368371_index) && !is_null($acc_400344_index)) {
	$rand = rand(1, 2);
	if ($rand == 1) {
		unset($items_sponsor[$acc_368371_index]);
	} else {
		unset($items_sponsor[$acc_157187_index]);
	}
} else if (!is_null($acc_400344_index) && !is_null($acc_157187_index)) {
	$rand = rand(1, 4);
	if ($rand == 1) {
		unset($items_sponsor[$acc_400344_index]);
	} else {
		unset($items_sponsor[$acc_157187_index]);
	}
}

//_d("cnt_it=".count($items).", cnt fake=".count($items_fake));
if (count($items_fake) != 0) {
	//if ((count($items) - $sponsored) == 0) {
	if (count($items) == 0) {
		$items = array_merge($items, $items_fake);
	} else {
		//$interval = floor((count($items) - $sponsored) / count($items_fake));
		$interval = floor(count($items) / count($items_fake));
		if ($interval > 4)
			$interval = 4;
		$cnt_fake = count($items_fake);
		//$pos = $sponsored + $interval;
		$pos = $interval;
		while (!empty($items_fake)) {
			$it = array_shift($items_fake);
			array_splice($items, $pos, 0, array($it));
			$pos += ($interval + 1);
		}
	}
}
/*
//reinsert sponsored items at concrete positions
foreach ($items_sponsor as $is) {
	$position = intval($is["sponsor_position"]) - 1;
	if ($position > 20 || $position < 0)
		$position = 0;
	_d("reinserting ad #{$is["id"]} to index {$position}");
	array_splice($items, $position, 0, [$is]);
}
*/
function sponsor_position_cmp($a, $b) {
	$asp = $a["sponsor_position"];
	$bsp = $b["sponsor_position"];
	if ($asp < $bsp)
		return -1;
	else if ($asp > $bsp)
		return 1;
	return 0;
}
usort($items_sponsor, "sponsor_position_cmp");
$items = array_merge($items_sponsor, $items);

$cat_link = $ctx->domain_link.$ctx->location_path."/{$gModule}/";

while ($row = array_shift($items)) {
	$text = ($row["location"]?"{$row["location"]}, ":"") . 
			($row["ethnicity"]?$form->arrayValue($classifieds->adbuild_ethnicity, $row["ethnicity"]):"") . 
			($row["available"]==1?", Incall":($row["available"]==2?", Outcall":($row["available"]==3?", Incall and Outcall":"")));

	$age = $row["age"] ? (" - " . $row["age"]) : "";

	$incall_rate = $row["incall_rate"] ? "({$currency_sign}{$row["incall_rate"]}.00)" : "";
	$outcall_rate = $row["outcall_rate"] ? "({$currency_sign}{$row["outcall_rate"]}.00)" : "";
	$available = $row["available"] == 3 ? "Incall{$incall_rate} and Outcall{$outcall_rate}" : ($row["available"] == 2 ? "Outcall ONLY{$outcall_rate}" : ($row["available"] == 1?"Incall ONLY{$incall_rate}":""));

	$title = str_replace(">", "&gt;", $row["title"]);
	$title = str_replace("<", "&lt;", $title);
	$title = str_replace("\"", "&quot;", $title);

	//determine thumbnail
	$thumb = false;
	$thumbIndex = null;
	if ($row["multiple_thumbs"]) {
		$arr = explode(",", preg_replace('/[^a-zA-Z0-9\.\,_\-]/', '', $row["multiple_thumbs"]));
		if (count($arr) > 0) {
			$thumbIndex = rand(0, count($arr) - 1);
			$thumb = $arr[$thumbIndex];
		}
	}
	if (!$thumb && $row["thumb"])
		$thumb = $row["thumb"];
	
	//on mobile in photo view we are not displaying 75x75 thumbnail (classifieds.thumb) but thumbnail version of first image
	if ($mobile == 1 && (!isset($_GET['view']) || $_GET['view'] != 'list')) {
		if ($thumbIndex)
			$tRes = $db->q("SELECT filename FROM classifieds_image WHERE id = ? ORDER BY image_id ASC LIMIT {$thumbIndex}, 1", [$row["id"]]);
		else
			$tRes = $db->q("SELECT filename FROM classifieds_image WHERE id = ? ORDER BY image_id ASC LIMIT 1", [$row["id"]]);
		if ($db->numrows($tRes)) {
			$roy=$db->r($tRes);
			$large_thumb = $roy[0];
		} else
			$large_thumb = "";
	} else
		$large_thumb = "";

	//determine direct link
	$direct_link = null;
	$arr = explode(",", $row["direct_link"]);
	if ($thumbIndex && count($arr) > $thumbIndex)
		$direct_link = $arr[$thumbIndex];
	else if ($arr[0])
		$direct_link = $arr[0]; 

	$ad_loc_id = $ad_loc[$row['id']];
	if (array_key_exists($ad_loc_id, $loc_links))
		$link = $loc_links[$ad_loc_id]."/".$gModule."/".$row['id'];
	else
		$link = $cat_link.$row['id'];

/*
	//TODO incorporate this into database
	if (in_array($row["id"], array(1021908,1021910,1021911,1021912))) {
		$thumbnails = array(
			1021908 => array(
				 array(
					  "1021908_1.jpg",
					  "1021908_2.jpg",
					  "1021908_3.jpg",
					  "1021908_4.jpg",
					  "1021908_5.jpg",
					  "1021908_6.jpg",
					  ),
				 array(
					  "1021920_1.jpg",
					  "1021920_2.jpg",
					  "1021920_3.jpg",
					  "1021920_4.jpg",
					  ),
				 ),
			1021910 => array(
				 array(
					  "1021908_1.jpg",
					  "1021908_2.jpg",
					  "1021908_3.jpg",
					  "1021908_4.jpg",
					  "1021908_5.jpg",
					  "1021908_6.jpg",
					  ),
				 array(
					  "1021920_1.jpg",
					  "1021920_2.jpg",
					  "1021920_3.jpg",
					  "1021920_4.jpg",
					  ),
				 ),
			1021911 => array(
				array(
					  "1021908_1.jpg",
					  "1021908_2.jpg",
					  "1021908_3.jpg",
					  "1021908_4.jpg",
					  "1021908_5.jpg",
					  "1021908_6.jpg",
					  ),
				 array(
					  "1021920_1.jpg",
					  "1021920_2.jpg",
					  "1021920_3.jpg",
					  "1021920_4.jpg",
					  ),
				 ),
			1021912 => array(
				 array(
					  "1021908_1.jpg",
					  "1021908_2.jpg",
					  "1021908_3.jpg",
					  "1021908_4.jpg",
					  "1021908_5.jpg",
					  "1021908_6.jpg",
					  ),
				 array(
					  "1021920_1.jpg",
					  "1021920_2.jpg",
					  "1021920_3.jpg",
					  "1021920_4.jpg",
					  ),
				 ),
			);
		$titles = array(
			1021908 => array(
				array(
					"Pay me with your Penis! Don't Want Your Money Just Great Sex!",
					"Not A Working Girl... If You Can Host I am Free Tonight For Sex!",
					"Looking For A Mutually Beneficial Relationship! Spoil me and I will SPOIL YOU!",
					"Text me now... Be Inside Me Tonight!",
					),
				array(
					"Come Hookup With Me While My Husband Is At Work!",
					"Caught My Husband Cheating... Looking For Revenge Sex! Can you Host?",
					),
				),
			1021910 => array(
				array(
					"Pay me with your Penis! Don't Want Your Money Just Great Sex!",
					"Not A Working Girl... If You Can Host I am Free Tonight For Sex!",
					"Looking For A Mutually Beneficial Relationship! Spoil me and I will SPOIL YOU!",
					"Text me now... Be Inside Me Tonight!",
					),
				array(
					"Come Hookup With Me While My Husband Is At Work!",
					"Caught My Husband Cheating... Looking For Revenge Sex! Can you Host?",
					),
				),
			1021911 => array(
				array(
					"Pay me with your Penis! Don't Want Your Money Just Great Sex!",
					"Not A Working Girl... If You Can Host I am Free Tonight For Sex!",
					"Looking For A Mutually Beneficial Relationship! Spoil me and I will SPOIL YOU!",
					"Text me now... Be Inside Me Tonight!",
					),
				array(
					"Come Hookup With Me While My Husband Is At Work!",
					"Caught My Husband Cheating... Looking For Revenge Sex! Can you Host?",
					),
				),
			1021912 => array(
				array(
					"Pay me with your Penis! Don't Want Your Money Just Great Sex!",
					"Not A Working Girl... If You Can Host I am Free Tonight For Sex!",
					"Looking For A Mutually Beneficial Relationship! Spoil me and I will SPOIL YOU!",
					"Text me now... Be Inside Me Tonight!",
					),
				array(
					"Come Hookup With Me While My Husband Is At Work!",
					"Caught My Husband Cheating... Looking For Revenge Sex! Can you Host?",
					),
				),
			);
		$numgr = rand(0, count($thumbnails[$row["id"]])-1);
		//echo "numgr = {$numgr}<br />\n";
		$numi = rand(0, count($thumbnails[$row["id"]][$numgr])-1);
		//echo "numi = {$numi}<br />\n";
		$row["thumb"] = $thumbnails[$row["id"]][$numgr][$numi];
		$large_thumb = $thumbnails[$row["id"]][$numgr][$numi];
		$numt = rand(0, count($titles[$row["id"]][$numgr])-1);
		$title = $titles[$row["id"]][$numgr][$numt];
		$link = $link."?numgr={$numgr}&numt={$numt}";
		//echo "thumb = {$row["thumb"]}<br />\n";
		//echo "large thumb = {$large_thumb}<br />\n";
		//echo "numt = {$numt}<br />\n";
		//echo "title = {$title}<br />\n";
		//echo "link = {$link}<br />\n";
	}
*/

	$cl[] = array(
		"id"=>$row["id"],
		"account_id" => $row["account_id"],
		"name"=>$title,
		"thumb" => $thumb,
		"phone"=>makeproperphonenumber($row["phone"]),
		"ethnicity"=>$form->arrayValue($ethnicity_array, $row["ethnicity"]),
		"available"=>$available,
		"haircolor"=>$form->arrayValue($classifieds->adbuild_haircolor, $row["haircolor"]),
		"eyecolor"=>$form->arrayValue($classifieds->adbuild_eyecolor, $row["eyecolor"]),
		"pornstar"=>$row["pornstar"],
		"payment_visa"=>$row["payment_visa"],
		"payment_amex"=>$row["payment_amex"],
		"payment_dis"=>$row["payment_dis"],
		"ter"=>$row["ter"],
		"age"=>$row["age"],
		"pregnant"=>$row["pregnant"],
		"visiting"=>$row["visiting"],
		"location"=>ucfirst($row["location"]),
		"day"=>$row["day"],
		"day_title"=>$row["day_title"],
		"text"=>$text,
		"large_thumb"=>$large_thumb,
		"sponsor" => (in_array($row["id"],$sponsor_clad_ids)),
		"sticky" => (in_array($row["id"],$stickies)),
		"featured"=>$row['account_id']&&$row['total_loc']<5?true:false,
		"link"=>$link,
		"owner"=>$row['account_id']&&$row['account_id']==$account_id?true:false,
		"firstname"=>$row['firstname'],
		"incall_rate"=>$row['incall_rate_h'],
		"outcall_rate"=>$row['outcall_rate_h'],
		"direct_link" => $direct_link,
	);
}

// shuffle stickies
$count_stickies = 0;
$cl_sticky_keys = [];
$to_shuffle = [];
foreach ($cl as $k => $c){
	if($c['sticky'] == 1){
		$count_stickies++;
		$to_shuffle[] = $c;
		$cl_sticky_keys[] = $k;
	}
}

if($count_stickies > 1){
	shuffle($to_shuffle);
	array_splice($cl, $cl_sticky_keys[0], $count_stickies, $to_shuffle);
}
//end shuffle stickies

$smarty->assign("sc", $cl);

if (empty($cl)) {
	//if (!is_bot() && $location->getSignificant()) {
	//	reportAdmin("AS: empty CL!", "");
	//}
	$res = $db->q("select loc_name, s, state_id from location_topcities where loc_id = '$loc_id'");
	if( $db->numrows($res) ) {
		$row = $db->r($res);
		$smarty->assign("state_name", $row["s"]);
		$smarty->assign("loc_name", $row["loc_name"]);
		$smarty->assign("state_id", $row["state_id"]);
		$smarty->assign('whereas', 'in');
	} else {
		$smarty->assign('whereas', 'near');
	}
}

$smarty->assign("total", $total);
$smarty->assign("type", $type);
$smarty->assign("name", $_GET["name"]);
$smarty->assign("phone", $_GET["phone"]);
$query = _pagingQuery();

$smarty->assign("paging", $db->paging($total, $item_per_page, $mobile==1?false:true, $mobile==1?false:true, $query));
//for mobile new pager
$smarty->assign("pager_array", $dir->get_pager_array($total, $item_per_page, $cat_link, true));


$query = _pagingQuery();
_pagingQueryFilter($query, $filter_link);
$linksort = _pagingQuery(array("order", "orderway"));
$smarty->assign("linksort", $linksort);
$smarty->assign("search_hidden", _pagingQuery(array("zipcode", "miles", "phone", "loc_id", "type", "query", "incall_min", "incall_max", "outcall_min", "outcall_max", "view"), 1));
$smarty->assign("link", $query);
$smarty->assign("filter", $filter_link);

/*
//dallas 8.4.2018: disable forums sitewide
if ($type == 1)
	$forums = $forum->sectionTopic(7, $core_state_id);
elseif ($type == 2)
	$forums = $forum->sectionTopic(8, $core_state_id);
elseif ($type == 3 || $type == 4)
	$forums = $forum->sectionTopic(9, $core_state_id);
if (in_array($type, array(1, 2, 3, 4)))
	$smarty->assign("forum", $forums);

$smarty->assign("forumlink", $forum->forumcatlink());
*/

$smarty->assign("loc_id", $ctx->location_id);
$smarty->assign("image_path", "classifieds");
$smarty->assign("place_link", "look");
$smarty->assign("gModule", $gModule);
$smarty->assign("gLocation", $gLocation);
$smarty->assign('loc_name', $account->core_loc_array['loc_name']);

//_d("getSide: type={$type}, loc_id={$loc_id}, ctx-location_id={$ctx->location_id}");
$side = $classifieds->getSideSponsors($type, $ctx->location_id);
//_darr($side);
$smarty->assign("side", $side);
//$smarty->assign("side", $classifieds->getSideSponsors($type, $ctx->location_id));

if( isset($account->core_loc_array['loc_parent']) && $account->core_loc_type > 2)
	$smarty->assign('state_id', $account->core_loc_array['loc_parent']);

$smarty->assign("current", time());

$smarty->display(_CMS_ABS_PATH."/templates/classifieds/classifieds_home.tpl");

?>
