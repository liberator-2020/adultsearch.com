<?php

defined("_CMS_FRONTEND") or die("No Access");

require(_CMS_ABS_PATH."/_cms_files/classifieds/array.php");

global $db, $smarty, $page, $account, $gTitle, $advertise, $gItemID, $gLocation, $gModule, $mobile, $gDescription, $ctx, $gKeywords, $sphinx, $gCanonicalUrl, $config_site_url, $config_image_server, $config_dev_server;

$form = new form;
$classifieds = new classifieds;

session_write_close();

$core_state_id = $account->corestate();

if( !isset($gItemID) || !intval($gItemID) ) {
	if (empty($account->core_loc_array['loc_url']))
		$account->core_loc_array['loc_url'] = "/";
	system::moved("{$account->core_loc_array['loc_url']}{$gModule}/");
}
$id = intval($gItemID);


if ($ctx->international) {
	$loc_name = $ctx->location_name;
	$loc_id = $ctx->location_id;
	$state_name = $loc_name;
	$cat_link = $ctx->location_path."/".$ctx->category;
} else {
	$state_link = $account->core_loc_array["loc_url"];
	$loc_name = $account->core_loc_array["loc_name"];
	$loc_id = $account->core_loc_id;
	$state_name = $account->core_sub;
	$cat_link = $state_link.$gModule."/";
}

$location = location::findOneById($ctx->location_id);
if ($location)
	$gCanonicalUrl = $location->getUrl()."/{$ctx->category}/{$id}";

$smarty->assign("state_link", $state_link);

//find classified ad in database
if( $account->core_loc_id ) {
	$res = $db->q("SELECT c.*, date_format(`c`.`date`, '%W, %M %e, %Y, %l:%i %p') as date2, unix_timestamp(date) as timex, 
					date_format(visiting_from, '%m/%d/%Y') vf, date_format(visiting_to, '%m/%d/%Y') vt, a.whitelisted
					FROM classifieds c
					LEFT JOIN account a on a.account_id = c.account_id
					WHERE c.id = '$id' and c.deleted IS NULL");
// and s.loc_id = '{$account->core_loc_array['loc_parent']}'");
} else {
	$res = $db->q("SELECT c.*, date_format(`c`.`date`, '%M %e, %Y, %r') as date2, unix_timestamp(date) as timex, a.whitelisted
					FROM classifieds c
					LEFT JOIN account a on a.account_id = c.account_id
					WHERE c.id = '$id' and c.deleted IS NULL");
}

//if classified ad not found, check classifieds_redirect table
if( !$db->numrows($res) ) {
	$res = $db->q("select new_id from classifieds_redirect where id = '$id'");
	if( $db->numrows($res) ) {
		$row = $db->r($res);
		$db->q("update classifieds_redirect set time = now() where id = '$id'");
		$db->q("delete from classifieds_redirect where time < date_sub(now(), interval 90 day)");
		header("Location: ".$config_site_url."/classifieds/look?id={$row[0]}", true, "301");
		die;
	}
	system::moved($cat_link);
}
$row = $row_ad = $db->r($res);

//ECCASapi by Gio
//jay 1.4.2015 - disable for sponsored ads
//jay 3.1.2017 - disable for fake phoe numbers
/*
if ($row["sponsor"] != 1 && substr($row["phone"], 0, 3) != "000") {
	$ECCASapi = new ECCASapi();
	$ECCASapi_revsearch_data = $ECCASapi->revsearch_url($row);
	// Manually Update New
	$row['ECCASapi_revsearch_url'] = $ECCASapi_revsearch_data['ECCASapi_revsearch_url'];
	$row['ECCASapi_revsearch_url_lastrequest'] = $ECCASapi_revsearch_data['ECCASapi_revsearch_url_lastrequest'];
}
// END ECCASapi
*/

$ctx->item = $row;

if ($ctx->international) {
	$loc_name = $ctx->city_name;
	$parent_loc_name = $ctx->country_name;
} else {
	$loc_name = $account->core_loc_array['loc_name'];
	$parent_loc_name = $account->core_loc_array['lp_loc_name'];
}
$gtitle = !empty($row['firstname']) ? preg_replace("/<.*>/", "", $row['firstname']) : preg_replace("/<.*>/", "", $row["title"]);
if (strpos($gtitle, $loc_name) == (strlen($gtitle) - strlen($loc_name))) {
	//if classified name already includes city name at the end, add only country/state
	$loc_name = $parent_loc_name;
} else {
	//add city + country/state
	$loc_name = $loc_name.", ".$parent_loc_name;
}
$cat_name = $form->arrayValue($gtitle_array2, $row['type']);
$gTitle = makeproperphonenumber($row["phone"]) . " $gtitle {$loc_name} $cat_name";

$phone = makeproperphonenumber($row["phone"]);
$gDescription = limit_text(preg_replace('/\n/', '', strip_tags(html_entity_decode($row['content']))), 22);
$gDescription .= ". $phone {$row['loc_name']}, {$account->core_loc_array['lp_loc_name']}";
$gDescription = str_replace('"', '', $gDescription);

$smarty->assign('breadplace', $phone?$phone:$row['title']);

if( $row_ad['bp'] == 1 && !empty($_SERVER["HTTP_REFERER"]) && strstr($_SERVER["HTTP_REFERER"], 'theeroticreview.com') && !strstr($_SERVER["HTTP_REFERER"], 'viewmsg.asp') ) {
	$ter_id = intval(substr($_SERVER["HTTP_REFERER"], strrpos($_SERVER["HTTP_REFERER"], '=')+1));
	if( $ter_id > 1 && $row_ad['ter'] != $ter_id )
		$db->q("update classifieds set ter = '$ter_id' where id = '$id'");
}

if( $row_ad['bp'] == 1 && !empty($_SERVER["HTTP_REFERER"]) && strstr($_SERVER["HTTP_REFERER"], 'eccie.net') ) {
	$eccie = substr($_SERVER["HTTP_REFERER"], 0, 250);
	$db->q("UPDATE classifieds SET eccie = ? WHERE id = ?", array($eccie, $id));
}

if( empty($row_ad['bigdoggie']) && !empty($_SERVER["HTTP_REFERER"]) && strstr($_SERVER["HTTP_REFERER"], 'bigdoggie.net') ) {
	$bigdoggie = preg_replace('/^http:\/\/www\.bigdoggie\.net\/escorts\/by\-id\/(\d+)\.html$/i', '$1', $_SERVER["HTTP_REFERER"]);
	if( $bigdoggie && is_numeric($bigdoggie) ) {
		$db->q("update classifieds set bigdoggie = '$bigdoggie' where id = '$id'");
	}
}

$title = str_replace(">", "&gt;", $row["title"]);
$title = str_replace("<", "&lt;", $title);
$title = str_replace("\"", "&quot;", $title);

$row["loc_name"] = $loc_name;

$module = $classifieds->getModuleByType($row["type"]);
if( !$module ) system::moved('/');

//expired ads
if( $row["done"] == 0 && $row["total_loc"] < 5 ) {

	if( isset($_GET["adminrepost"]) && $account->isrealadmin() ) {
		$classifieds->adminPost($id);
		system::reload(array("adminrepost"=>"1"));
	}

	//auto prolong expiration if visitor came from backpage
	if (strstr($_SERVER["HTTP_REFERER"],"backpage.com") && $row['bp'] == 0) {
		if (classifieds::prolong($id, 7))
			audit::log("CLA", "Prolong", $id, "BP ref - 7 days", "S-_cms_files/female-escorts/look");
		system::reload();
	} elseif(!empty($_SERVER["HTTP_REFERER"]) && !strstr($_SERVER["HTTP_REFERER"],"adultsearch.com") && $row["total_loc"] < 5 && $row["bp"]==1 && $account->core_loc_id != 15575) {
		classifieds::prolong($id, 3, true);
		//we dont audit prolongation of this ad - bp stolen ads ...
	}

	$isowner = $classifieds->isowner($row);
	if ($isowner)
		$smarty->assign("expired", true);

//this is probably meant for ads not posted yet ??? (done=-1) ???
} elseif(strstr($_SERVER["HTTP_REFERER"],"backpage.com") && $row["total_loc"] < 5 && $row['bp'] == 0) {
	if( $row["done"] != 2 && $row["timex"] < (time()-60*60*12) ) {
		//reportAdmin("AS: class/look: referer from BP ?", "", array("id" => $id));
		$db->q("update classifieds set done = 1, `date` = now() where id = '$id'");
		$db->q("update classifieds_loc set done = 1, updated = now() where post_id = '$id'");
		$db->q("update classifieds_sponsor set done = 1 where post_id = '$id'");
		$db->q("update classifieds_side set done = 1 where post_id = '$id'");

		if ($row["timex"] < (time()-60*60*24))
			$db->q("insert into classifieds_stat (time, top) values (curdate(), 1) on duplicate key update top = top + 1");
	}
}

if ($row["done"] == 0) {
	$smarty->assign("adexpired", true);
}

//get location of this ad
$loc_url = array();
$rex = $db->q("SELECT l.loc_id, ll.loc_url, ll.country_sub, ll.country_id 
				FROM classifieds_loc l 
				INNER JOIN location_location ll using (loc_id) 
				WHERE l.post_id = '$id'");
$currency = "USD";
$metric = -1;
while($rox=$db->r($rex)) {
	$loc_url[] = $rox["loc_url"];
	$loc_ids[] = $rox["loc_id"];
	$sub = $rox["loc_url"];
	$currency = dir::getCurrencyCodeByCountry($rox['country_sub']);
	if( in_array($rox['country_id'], array(16046, 16047)) )
		$metric = false;
	else if($metric == -1)
		$metric = true;
}
$smarty->assign("currency_sign", currency_convert::getPriceTagOnly($currency));

if (currency_convert::isConvertible($currency)) {
	include_html_head("js", "/js/jquery.cookie.js");
	include_html_head("js", "/js/currency_convert.js");
	include_html_head("css", "/css/currency_convert.css");
	$cc_b_html = $smarty->fetch(_CMS_ABS_PATH."/templates/currency_convert/currency_convert_button.tpl");
	$smarty->assign("display_currency_button", true);
	$smarty->assign("currency_button", $cc_b_html);
}

if( !$account->core_loc_id ) {
	if( !in_array($gLocation, $loc_url) ) {
		system::moved("/$module/{$loc_url[0]}/$id");
	} elseif( $gModule != $module ) system::moved("/$module/{$loc_url[0]}/$id");
} else {
	if (!in_array($account->core_loc_id, $loc_ids)) {
		system::moved("{$sub}{$module}/$id");
	} elseif( $gModule != $module ) {
		system::moved("{$sub}{$module}/$id");
	}
}

//show ad depending of the type of the ad
$ad_type = NULL;
switch($row["type"]) {
	case 1: $ad_type = 16; break;
	case 2: $ad_type = 17; break;
	case 6: $ad_type = 18; break;
	case 12: $ad_type = 20; break;
	case 13: $ad_type = 19; break;
	case 3: $ad_type = 21; break;
	case 4: $ad_type = 22; break;
	case 7: $ad_type = 23; break;
	case 15: $ad_type = 24; break;
	case 14: $ad_type = 25; break;
	default: $ad_type = NULL;
}
if( $ad_type )
	$advertise->showAd($ad_type, $loc_id?$loc_id:$core_state_id);

$row["ethnicity"] = $form->arrayValue($classifieds->adbuild_ethnicity, $row["ethnicity"]);
$row["cupsize"] = $form->arrayValue($adbuild_cupsize, $row["cupsize"]);
$row["penis_size"] = $form->arrayValue($adbuild_penis_size, $row["penis_size"]);
$row["kitty"] = $form->arrayValue($adbuild_kitty, $row["kitty"]);
$row["eyecolor"] = $form->arrayValue($adbuild_eyecolor, $row["eyecolor"]);
$row["haircolor"] = $form->arrayValue($adbuild_haircolor, $row["haircolor"]);
$row["build"] = $form->arrayValue($adbuild_build, $row["build"]);
$row["phone"] = makeproperphonenumber($row["phone"]);
if (in_array($ctx->country_id,[16046,16047]) && (substr($row["phone"], 0, 1) != "+"))
	$row["phoneInternational"] = "+1".preg_replace('/[^0-9]/', '', makeproperphonenumber($row["phone"]));

$weight_sign = $metric ? "kg" : "lbs";
if( $row['weight'] ) $row['weight'] = "{$row['weight']}{$weight_sign}";
if( $row["height_feet"] ) {
	if( !$metric ) $row['height'] = $form->arrayValue($adbuild_height_feet, $row["height_feet"]) . '\' ' . $form->arrayValue($adbuild_height_inches, $row["height_inches"]) . '\'\'';
	else $row['height'] = "{$row["height_feet"]}{$row["height_inches"]}cm";
}

if( $row['fetish'] ) {
	$bdsm = explode(',', $row['fetish']);
	if( is_array($bdsm) ) {
		foreach($bdsm as $b) {
			$row['bdsm'] .= (!empty($row['bdsm']) ? ", " : "") . $form->arrayvalue($adbuild_fetish, $b);
		}
	}
	$row['fetish'] = NULL;
}
if( $row['language'] ) {
	$language = explode(',', $row['language']);
	$row['language'] = NULL;
	if( is_array($language) ) {
		foreach($language as $b) {
			$row['language'] .= (!empty($row['language']) ? ", " : "") . $form->arrayvalue($classifieds->adbuild_language, $b);
		}
	}
}


if( $row['fetish_dominant'] ) $row['fetish'] = "Dominant";
if( $row['fetish_submissive'] ) $row['fetish'] = !empty($row['fetish']) ? "{$row['fetish']}, Submissive" : "Submissive";
if( $row['fetish_swith'] ) $row['fetish'] = !empty($row['fetish']) ? "{$row['fetish']}, Switch" : "Switch";


if( $row['measure_1'] && $row['measure_2'] && $row['measure_3'] ) $row['measurements'] = "{$row['measure_1']}-{$row['measure_2']}-{$row['measure_3']}";

$row["content"] = classifieds::sanitizeContentForOutput($row["content"], $row["allowlinks"], $row["account_id"]);

$row["incall_rate"] = $row["incall_rate_h"] ? currency_convert::display_amount($currency, $row["incall_rate_h"]) : "";
$row["incall_rate_hh"] = $row["incall_rate_hh"] ? currency_convert::display_amount($currency, $row["incall_rate_hh"]) : "";
$row["incall_rate_2h"] = $row["incall_rate_2h"] ? currency_convert::display_amount($currency, $row["incall_rate_2h"]) : "";
$row["incall_rate_day"] = $row["incall_rate_day"] ? currency_convert::display_amount($currency, $row["incall_rate_day"]) : "";
$row["outcall_rate"] = $row["outcall_rate_h"] ? currency_convert::display_amount($currency, $row["outcall_rate_h"]) : "";
$row["outcall_rate_hh"] = $row["outcall_rate_hh"] ? currency_convert::display_amount($currency, $row["outcall_rate_hh"]) : "";
$row["outcall_rate_2h"] = $row["outcall_rate_2h"] ? currency_convert::display_amount($currency, $row["outcall_rate_2h"]) : "";
$row["outcall_rate_day"] = $row["outcall_rate_day"] ? currency_convert::display_amount($currency, $row["outcall_rate_day"]) : "";

$row['available'] = $row['incall'] ? ($row['outcall']?3:1) : ($row['outcall'] ? 2:0);

//_darr($row);

if( $row['age'] > 0 )
	$age = "{$row['age']} years old";

$gKeywords = "{$row_ad['phone']}, {$row_ad['firstname']}, {$row_ad['loc_name']}, $age, {$row['ethnicity']}, {$row['height']}, {$row['weight']}, {$row['eyecolor']}, {$row['haircolor']}, {$row['build']}, {$row['measurements']}, {$row['cupsize']}, {$row['penis_size']}, {$row['kitty']}";
$gKeywords = str_replace(', ,', '', $gKeywords);

if (!$row["whitelisted"]) {
	//display website only for whitelisted accounts
	$row['website'] = "";
} else {
	if (!empty($row['website']) && strncmp($row['website'], 'http', 4)) {
		$row['website'] = "http://" . $row['website'];
	}
	$row['website'] = classifieds::sanitizeWebsite($row['website']);
}

//jay
//

$smarty->assign("type", $row['type']);

//mark skype 7.7.2015 - remove reply for non-whitelisted accounts
if ($row["reply"] == 1 && $row["whitelisted"] != 1) {
	$row["reply"] = 0;
}


//TODO incorporate this into database
if (in_array($row["id"], array(1021908,1021910,1021911,1021912))) {
	$titles = array(
		1021908 => array(
			array(
				"Pay me with your Penis! Don't Want Your Money Just Great Sex!",
				"Not A Working Girl... If You Can Host I am Free Tonight For Sex!",
				"Looking For A Mutually Beneficial Relationship! Spoil me and I will SPOIL YOU!",
				"Text me now... Be Inside Me Tonight!",
				),
			array(
				"Come Hookup With Me While My Husband Is At Work!",
				"Caught My Husband Cheating... Looking For Revenge Sex! Can you Host?",
				),
			),
		1021910 => array(
			array(
				"Pay me with your Penis! Don't Want Your Money Just Great Sex!",
				"Not A Working Girl... If You Can Host I am Free Tonight For Sex!",
				"Looking For A Mutually Beneficial Relationship! Spoil me and I will SPOIL YOU!",
				"Text me now... Be Inside Me Tonight!",
				),
			array(
				"Come Hookup With Me While My Husband Is At Work!",
				"Caught My Husband Cheating... Looking For Revenge Sex! Can you Host?",
				),
			),
		1021911 => array(
			array(
				"Pay me with your Penis! Don't Want Your Money Just Great Sex!",
				"Not A Working Girl... If You Can Host I am Free Tonight For Sex!",
				"Looking For A Mutually Beneficial Relationship! Spoil me and I will SPOIL YOU!",
				"Text me now... Be Inside Me Tonight!",
				),
			array(
				"Come Hookup With Me While My Husband Is At Work!",
				"Caught My Husband Cheating... Looking For Revenge Sex! Can you Host?",
				),
			),
		1021912 => array(
		array(
				"Pay me with your Penis! Don't Want Your Money Just Great Sex!",
				"Not A Working Girl... If You Can Host I am Free Tonight For Sex!",
				"Looking For A Mutually Beneficial Relationship! Spoil me and I will SPOIL YOU!",
				"Text me now... Be Inside Me Tonight!",
				),
			array(
				"Come Hookup With Me While My Husband Is At Work!",
				"Caught My Husband Cheating... Looking For Revenge Sex! Can you Host?",
				),
			),
		);

	$contents = array(
		1021908 => array(
			array(
				'<iframe src="http://textad.adulthookup.com/if/1/6607/0/ref=753040;keyword=Paywithpenis-AH-Allaboutsex5" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6609/0/ref=753040;keyword=Notworkinggirl-AH-Contactme" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6610/0/ref=753040;keyword=Mutuallybenefical-ES-discreetmeetings" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6610/0/ref=753040;keyword=Textmenow-ES-discreetmeetings" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				),
			array(
				'<iframe src="http://textad.adulthookup.com/if/1/6613/0/ref=753040;keyword=husbandatwork-Milftastic-Lonleywivesclub" width=“520” height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6614/0/ref=753040;keyword=Caughthusband-Milftastic-Milfclub" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				),
			),
		1021910 => array(
			array(
				'<iframe src="http://textad.adulthookup.com/if/1/6607/0/ref=753040;keyword=Paywithpenis-AH-Allaboutsex5" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6609/0/ref=753040;keyword=Notworkinggirl-AH-Contactme" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6610/0/ref=753040;keyword=Mutuallybenefical-ES-discreetmeetings" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6610/0/ref=753040;keyword=Textmenow-ES-discreetmeetings" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				),
			array(
				'<iframe src="http://textad.adulthookup.com/if/1/6613/0/ref=753040;keyword=husbandatwork-Milftastic-Lonleywivesclub" width=“520” height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6614/0/ref=753040;keyword=Caughthusband-Milftastic-Milfclub" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				),
			),
		1021911 => array(
			array(
				'<iframe src="http://textad.adulthookup.com/if/1/6607/0/ref=753040;keyword=Paywithpenis-AH-Allaboutsex5" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6609/0/ref=753040;keyword=Notworkinggirl-AH-Contactme" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6610/0/ref=753040;keyword=Mutuallybenefical-ES-discreetmeetings" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6610/0/ref=753040;keyword=Textmenow-ES-discreetmeetings" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				),
			array(
				'<iframe src="http://textad.adulthookup.com/if/1/6613/0/ref=753040;keyword=husbandatwork-Milftastic-Lonleywivesclub" width=“520” height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6614/0/ref=753040;keyword=Caughthusband-Milftastic-Milfclub" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				),
			),
		1021912 => array(
			array(
				'<iframe src="http://textad.adulthookup.com/if/1/6607/0/ref=753040;keyword=Paywithpenis-AH-Allaboutsex5" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6609/0/ref=753040;keyword=Notworkinggirl-AH-Contactme" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6610/0/ref=753040;keyword=Mutuallybenefical-ES-discreetmeetings" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6610/0/ref=753040;keyword=Textmenow-ES-discreetmeetings" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				),
			array(
				'<iframe src="http://textad.adulthookup.com/if/1/6613/0/ref=753040;keyword=husbandatwork-Milftastic-Lonleywivesclub" width=“520” height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				'<iframe src="http://textad.adulthookup.com/if/1/6614/0/ref=753040;keyword=Caughthusband-Milftastic-Milfclub" width="700" height="946" marginheight="0" marginwidth="0" scrolling="no" frameborder="0"></iframe>',
				),
			),
		);

	$numgr = intval($_REQUEST["numgr"]);
	$numt = intval($_REQUEST["numt"]);
	if ($numgr > (count($titles[$row["id"]])-1))
		$numgr = 0;
	if ($numt > (count($titles[$row["id"]][$numgr])-1))
		$numt = 0;
	//echo "numgr={$numgr}, numt={$numt}<br />\n";
	$row["title"] = $titles[$row["id"]][$numgr][$numt];
	$row["content"] = $contents[$row["id"]][$numgr][$numt];
	//echo "title={$row["title"]}<br />\n";
	//echo "content={$row["content"]}<br />\n";
}

//check and enhance social links
$classifieds->social_links_valid($row);

$smarty->assign("post", $row);

$rex = $db->q("SELECt * FROM classifieds_image WHERE id = ? ORDER BY image_id", [$id]);
$image = array();
while($rox=$db->r($rex)) {
	$resize = $rox["width"]>200||!$rox["width"]?true:false;
	$image[] = array("filename"=>$rox["filename"], "resize"=>$resize);
}
$smarty->assign("images", $image);

// videos
$rex = $db->q("SELECT id, thumbnail, filename, width, height FROM classified_video WHERE classified_id = ? AND converted=2 LIMIT 1", [$id]);
$videos = array();
while($rox=$db->r($rex)) {
	$resize = $rox["width"]>200||!$rox["width"]?true:false;
	$videos[] = array(
		"file_url"=> $config_image_server . vid::videoUri( $rox["filename"] ),
		"thumbnail_url"=> $config_image_server . vid::thumbnailUri($rox['thumbnail']),
		"width"=> $rox['width'],
		"height"=> $rox['height'],
		"resize"=>$resize
	);
}
$smarty->assign("videos", $videos);


if ((!empty($row["account_id"]) && $account->isloggedin() == $row["account_id"]) 
		|| $account->isadmin() 
		|| isset($_SESSION["cl_owner_".$id])
		|| permission::has("edit_all_ads")
		|| permission::has("classified_edit")
	)
	$smarty->assign("canedit", true);

$cat_name = substr($form->arrayValue($gtitle_array2, $row['type']), 0, -1);
$smarty->assign("loc_name", $row["loc_name"]);
$smarty->assign("cat", substr($cat_name, -1));
$smarty->assign("cat_name", "{$row["loc_name"]} $cat_name");
$smarty->assign("loc_id", isset($_GET["loc_id"])?$_GET["loc_id"]:$row["loc_id"]);
$smarty->assign("dontchangeloc", true);

$core_state_name = "Go back to <a href='{$account->core_loc_array['loc_url']}$gModule/' title='{$row["loc_name"]} $cat_name'>{$row["loc_name"]} $cat_name</a> page.";
$smarty->assign("header_title", $core_state_name);

$smarty->assign("cat_link", $cat_link);
$smarty->assign("link_back", $cat_link);

//if visitor came from other website, log referer
if (!empty($_SERVER["HTTP_REFERER"]) 
		&& !strstr($_SERVER["HTTP_REFERER"],"adultsearch.com") 
		&& in_array(substr($_SERVER["HTTP_REFERER"], 0, 6), array("http:/", "https:"))
	) {
	$referer = substr(classifieds::onlyPrintableCharacters($_SERVER["HTTP_REFERER"]), 0, 255);
	$ip = account::getUserIp();
	$db->q("INSERT INTO classifieds_refer (id, referer, stamp, ip_address) VALUES (?, ?, ?, ?)", [$id, $referer, time(), $ip]);
	if (strstr($_SERVER["HTTP_REFERER"], "backpage."))
		$db->q("INSERT IGNORE INTO classifieds_bp (id, date) VALUES ('$id', curdate())");
}

$ip = account::getUserIp();
if ($ip) {
	$db->q("insert ignore into classifieds_ip (id, ip_address, time) values (?, ?, ?)", [$id, $ip, time()]);
	if( $db->affected ) {
		//if ($row_ad["total_loc"] <= 4)
		//	$db->q("update classifieds set hit = hit + 1 where id = '$id'");
		$db->q("insert into classifieds_click (post_id, loc_id, date) values ('$id', '$loc_id', curdate()) on duplicate key update c = c + 1");
	}
}

//for admins, list referers
if (permission::has("classified_manage")) {
	$res = $db->q("SELECT refer_id, referer, stamp, ip_address FROM classifieds_refer WHERE id = ? ORDER BY refer_id desc limit 20", [$id]);
	$refers = array();
	while($rox=$db->r($res)) {
		$refers[] = array(
			"refer_id" => $rox["refer_id"],
			"referer" => $rox["referer"],
			"time" => date("m/d/Y H:i:s T", $rox["stamp"])." ".$rox["ip_address"],
			);
	}
	$smarty->assign("refers", $refers);
	if ($account->isrealadmin())
		$smarty->assign("isrealadmin", true);
}


$account_id = $account->isloggedin();

$smarty->assign("gModule", $gModule);
$smarty->assign("gLocation", $gLocation);


//for desktop page, load classified ads for marquee
if( $mobile != 1 ) {

	$sphinx_index = "escorts";
	$sphinx->reset();
	$sphinx->SetLimits( 0, 50, 50 );
	$sphinx->SetFilter('type', array($row_ad['type']));
	$sphinx->SetFilter('cid', array($gItemID), true);
	$sphinx->SetFilterRange('total_loc', 1, 5);
	$sphinx->SetFilter('loc_id', array($loc_id));
	$sphinx->SetSortMode(SPH_SORT_EXTENDED, "@random DESC");

	$results = $sphinx->Query ( $query, $sphinx_index );
	$total = $results["total_found"];
	$result = $results["matches"];
	$ids = ""; foreach($result as $res) {$ids .= $ids ? (",".$res["id"]) : $res["id"];}

	if ($ids)
		$res = $db->q("SELECT c.title, c.id, c.thumb
						FROM classifieds_loc l 
						INNER JOIN classifieds c on l.post_id = c.id AND c.api = 0 AND c.deleted IS NULL
						WHERE l.id in ($ids) 
						ORDER BY field(l.id,$ids)");
	$marquee = NULL;
	while($row=$db->r($res)) {
		// download static placeholder from production imgserver
		$thumb = !empty($row['thumb'])? $config_image_server . "/classifieds/{$row["thumb"]}":"{$config_site_url}/images/placeholder.gif";

		$marquee[] = [
			"title" => htmlspecialchars($row["title"]),
			"link" => $ctx->location_path."/{$gModule}/{$row['id']}",
			"loc_name" => "",
			"thumb" => $thumb,
			"review" => 0
			];
	}
	
	$smarty->assign("marquee", $marquee);
}

if ($row_ad['bp'] == 1)
	$smarty->assign("bp", true);
if ($row_ad['paid'] > 0)
	$smarty->assign("paid", true);

if (permission::has("classified_ter_check"))
	$smarty->assign("clad_ter_check", true);

$smarty->assign("jquery", "jquery.js");
$smarty->assign("image_path", "classifieds");
$smarty->assign("thumb", $row["thumb"]);
$smarty->assign("config_image_server", $config_image_server);
$file = $smarty->fetch(_CMS_ABS_PATH."/templates/classifieds/classifieds_ad.tpl");
echo $file;

$smarty->assign("pluslink", false);

?>
