<?php

global $db, $smarty, $account, $mcache, $sphinx, $config_site_url;

$core_state_id = $account->corestate();
$type = intval($_GET["type"]);
$loc_id = intval($_GET["loc_id"]);

if (!$core_state_id || !$loc_id || !$type)
	return "Error: Missing parameters!";

//check if we have recent cached HTML file
if( file_exists(_CMS_ABS_PATH."/templates/classifieds/rss/cl_rss_".$loc_id.".tpl") ) {
	$time = filemtime(_CMS_ABS_PATH."/templates/classifieds/rss/cl_rss_".$loc_id.".tpl");
	if( $time > time() - 600 ) {
		$handle = fopen(_CMS_ABS_PATH."/templates/classifieds/rss/cl_rss_".$loc_id.".tpl", "r");
		$content = fread($handle, filesize (_CMS_ABS_PATH."/templates/classifieds/rss/cl_rss_".$loc_id.".tpl"));
		fclose($handle);
		header('Content-Type: application/rss+xml');
		die($content);
	}
}

//get location
$loc = $mcache->get("SQL:LOC-ID",array($loc_id));
if ($loc === false)
	return "Error: Wrong loc_id {$loc_id} !";
$loc_name = $loc["loc_name"];
$s = $loc["s"];

$module = classifieds::getModuleByType($type);

if (in_array($loc["country_id"], array(16046, 16047, 41973))) {
	//domestic
	if ($loc["country_id"] == 16046)
		$link_base = $config_site_url."{$loc["loc_url"]}{$module}";
	else if ($loc["country_id"] == 16047)
        $link_base = "http://ca.adultsearch.com{$loc["loc_url"]}{$module}";
	else if ($loc["country_id"] == 41973)
        $link_base = "http://uk.adultsearch.com{$loc["loc_url"]}{$module}";
} else {
	//international
	$link_base = "http://{$loc["country_sub"]}.adultsearch.com/{$loc["dir"]}/{$module}";
}
$link = $link_base."/rss?type={$type}&loc_id={$loc_id}";


$sphinx_index = "escorts";
$sphinx->reset();
$sphinx->SetLimits( 0, 100, 1048 );
$sphinx->SetFilter('state_id', array($core_state_id));
$sphinx->SetFilter('type', array($type));
$sphinx->SetFilter('loc_id', array($loc_id));
$sphinx->SetSortMode(SPH_SORT_ATTR_DESC, "updated");
$results = $sphinx->Query("", $sphinx_index);
$result = $results["matches"];

foreach($result as $res) {
	$ids .= $ids ? (",".$res["attrs"]["cid"]) : $res["attrs"]["cid"];
}

if (!$ids)
	return "Error: No ads for this location!";

$res = $db->q("SELECT c.id, c.title, c.age, c.content, date_format(c.date, '%a, %e %b %Y %T') as date 
				FROM classifieds c 
				WHERE c.id in ($ids) 
				GROUP BY c.id 
				ORDER BY field(c.id,$ids)");
$cl = NULL;
while($row=$db->r($res)) {
	$title = $row["title"] . ($row["age"] ? (" - " . $row["age"]) : "");
	$l = $link_base."/{$row["id"]}";
	$cl[] = array(
		"id"=>$row["id"],
		"title"=>$title,
		"content"=>$row["content"],
		"link"=>$l,
		"date"=>$row["date"]
	);
}

$smarty->assign("cl", $cl);
$smarty->assign("link", $link);
$smarty->assign("loc_name", $loc_name);
$smarty->assign("s", $s);
$smarty->assign("built", date("r"));
$fetch = $smarty->fetch(_CMS_ABS_PATH."/templates/classifieds/classifieds_rss.tpl");
$handle = fopen(_CMS_ABS_PATH."/templates/classifieds/rss/cl_rss_".$loc_id.".tpl", "w");
fputs($handle, $fetch);
fclose($handle);
header('Content-Type: application/rss+xml');
die($fetch);

?>
