<?php

defined("_CMS_FRONTEND") or die("No Access");

deprecated_controller();

require_once _CMS_ABS_PATH."/_cms_files/worker/array_sc.php";

global $ctx, $db, $smarty, $page, $account, $gTitle, $advertise, $gModule, $gLocation, $gFilename, $mobile, $sphinx;
$form = new form;
$system = new system;

$location = location::findOneById($ctx->location_id);
if ($location) {
	$smarty->assign("link_back", $location->getUrl());
	$smarty->assign("location_video_html", $location->getVideoHtml());
}

$core_state_id = $account->corestate(1);
if (!$core_state_id)
	return;

$loc_around = array();
if ($account->core_loc_id && $account->core_loc_type > 2)
    $loc_around = $account->locAround();

$lingerie = isset($_GET["lingerie"]) ? GetGetParam("lingerie") : NULL;

//$advertise->showAd(8, $core_state_id);

$loc_name = $account->core_loc_array['loc_name'];

$gTitlex = NULL;
$filter_link = array();
foreach($_GET as $key => $value) {
		if( $key == "ethx" ) {
				$filter["eth"][] = $value; $filter_link[] = array("id"=>$value,"type"=>"eth","name"=>"Ethnicity");
		} else if( in_array($key, array("query", "lingerie", "gloryhole", "theater", "fetish", "lgbt",
"buddybooth", "zipcode", "phone", "loc_id")) && $value != "" ) {
		$skipfilter = 0;
				$ftype = $key;
		$valuex = $value == 2 ? "Yes" : ($value == 1 ? "No" : $value);
				if( $key == "lingerie" ) { $key = "Lingerie: $valuex"; $gTitlex .= $gTitlex ? ", Lingerie":" Lingerie";}
				else if( $key == "gloryhole" ) { $key = "Glory Holes"; $gTitlex .= $gTitlex ? ", Glory Holes":" Glory Holes";}
				else if( $key == "theater" ) { $key = "Theater"; $gTitlex .= $gTitlex ? ", Theater":" Theater";}
				else if( $key == "fetish" ) { $key = "Fetish"; $gTitlex .= $gTitlex ? ", Fetish":" Fetish";}
				else if( $key == "lgbt" ) { $key = "LGBT"; $gTitlex .= $gTitlex ? ", LGBT":" LGBT";}
				else if( $key == "buddybooth" ) { $key = "Buddy Booths"; $gTitlex .= $gTitlex ? ", Buddy Booths":" Buddy Booths";}
				else if( $key == "zipcode" ) { 
			$key = "Zip Code: $valuex"; $skipfilter = 1; 
			$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key, "link"=>"{$account->core_loc_array['loc_url']}$gModule/"); }
				else if( $key == "query" ) { $key = "'$valuex'"; }
				else if( $key == "phone" ) { $key = "Phone #: '$valuex'"; }
				else if( $key == "loc_id" ) { $key = $loc_name; }

		if( !$skipfilter ) {
			if( $key == "loc_id" ) $filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key, "link"=>"{$account->core_loc_array['loc_url']}$gModule/");
			else $filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key);
		}
	}
}

if( !empty($gLocation) && !strstr($gLocation, "zipcode") ) {
	$system->goModuleBase();
		$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$loc_name, "link"=>"http://$state_link.{$account->core_host}/$gModule/");
}

$item_per_page = isset($_GET["ipp"]) ? intval($_GET["ipp"]) : 30;
if( $item_per_page > 60 ) $item_per_page = 20;
$start = (($page*$item_per_page)-$item_per_page);
if( $start < 0 ) $start = 0;

$sphinx_index = "gaybath";
$sphinx->reset();
$sphinx->SetLimits( $start, $item_per_page, 1000 );

if( isset($_GET["zipcode"]) && !empty($_GET["zipcode"]) ) {
	if( ($pos=strpos($_GET["zipcode"], "-")) ) $_GET["zipcode"] = substr($_GET["zipcode"], 0, $pos);
		$zipcode = preg_replace("/[^0-9a-z]/i","", $_GET["zipcode"]);
		$mile_in = intval($_GET["miles"]); if( !$mile_in ) $mile_in = 10;
		if( $mile_in == -1 ) $mile_in = 1000;
		$res = $db->q("select z.lat, z.lon, radians(z.lat) latt, radians(z.lon) longg, l.loc_parent from core_zip z inner join location_location l on z.loc_id = l.loc_id
where z.zip = '$zipcode' limit 1");
		if( $db->numrows($res) ) {
				$s = $db->r($res); 
				$lat_me = $s["lat"]; $lon_me = $s["lon"];
				$core_state_id = $s["loc_parent"];
				$latt = $s["latt"]; $long = $s["longg"];
				$sphinx->SetGeoAnchor('loc_lat', 'loc_long', $latt, $long);
				$sphinx->SetFilterFloatRange('@geodist', 0, ($mile_in*1609.344));
				$zipfound = 1;
		} else {
				$error = "Zip code($zipcode) could not find. Please make sure that you typed the correct zip code.";
		}
		$smarty->assign("miles", $mile_in==1000?-1:$mile_in);
		$smarty->assign("zipcode", $_GET["zipcode"]);
} elseif( $account->core_loc_array['loc_lat'] ) {
		$sphinx->SetGeoAnchor('loc_lat', 'loc_long', deg2rad($account->core_loc_array['loc_lat']), deg2rad($account->core_loc_array['loc_long']));
		$sphinx->SetFilterFloatRange('@geodist', 0, (700*1609.344));
		//$distance = 1;
}

if (!is_null($lingerie))
	$sphinx->SetFilter('lingerie', array($lingerie));

if ($account->core_loc_id && $account->core_loc_type > 2 && !$zipfound) {
	$nloc[] = $account->core_loc_id;
	$sphinx->SetFilter('loc_id', $loc_around);
} elseif ($account->core_loc_id && $account->core_loc_type < 3)
	$sphinx->SetFilter('state_id', array($account->core_loc_id));

if (!$account->isWorker())
	$sphinx->SetFilter('edit', array(1, 2));

$query = "";
if( isset($_GET["phone"]) && !empty($_GET["phone"]) ) {
		$phone = preg_replace("/[^0-9\*]/", "", $_GET["phone"]);
		$query = $phone;
		//else $smarty->assign("err_phone", true);
}
if( isset($_GET["query"]) && !empty($_GET["query"]) ) {
	$name = preg_replace("/(\()?([0-9]{3})(\))?(.+)?([0-9]{3})(.)?([0-9]{4})/s", "$2$5$7", $_GET["query"]);
		$query .= $query ? " $name" : "$name";
		$error = "No strip club found with the word: ".$_GET["query"];
		$smarty->assign("name", $_GET["query"]);
}

$orderway = isset($_GET["orderway"])&& intval($_GET["orderway"]) ? intval($_GET["orderway"]) : (($zipfound||$distance)?2:1);
$way = $orderway == 1 ? "DESC" : "ASC";

if($_GET["order"] == 'overall') { $order = "overall $way"; }
elseif($_GET["order"] == 'review') $order = "review $way";
elseif($_GET["order"] == "name") $order = "name $way";
elseif($_GET["order"] == "last_review_id") $order = "last_review_id $way";
elseif($_GET["order"] == "score") $order = "overall $way";
elseif($_GET["order"] == 'distance' || ($zipfound||$distance) ) { $order = "sponsor DESC, @geodist $way"; }
elseif($_GET["order"] == 'coupon') $order = "coupon $way";
elseif($_GET["order"] == 'video') $order = "video_cnt $way";
else
	$order = "sponsor DESC, last_review_id DESC";

$sphinx->SetSortMode(SPH_SORT_EXTENDED, $order);

$results = $sphinx->Query($query, $sphinx_index);
$total = $results["total_found"];
$result = $results["matches"];

if( $total && !isset($zipfound) ) {

	$sphinx->ResetFilter('loc_id');
	$sphinx->SetGroupBy('loc_id', SPH_GROUPBY_ATTR, '@group desc' );
	$namex = "loc_id";

	$sphinx->SetLimits( 0, 1000, 1000 );
	$results = $sphinx->Query($query, $sphinx_index);
	$rex = $results["matches"];
	$locs = array();
	foreach($rex as $res) {
		$locs[] = array(
			"loc_id"=>$res["attrs"][$namex],
			"count"=>$res["attrs"]["@count"]
		);
	}

	if (!$account->core_loc_id)
		$sphinx->SetFilter('loc_id', array($loc_id));
}

if( count($locs) ) {
		$loc_ids = "";
		foreach($locs as $loc)
				$loc_ids  .= $loc_ids ? (",".$loc["loc_id"]) : $loc["loc_id"];

		$res = $db->q("select loc_id, loc_name, loc_url from location_location where loc_id in ($loc_ids) order by loc_name");
		$loc_ids = array();
	$category = NULL;
		while($row=$db->r($res)) {
				$count = 0;
				foreach($locs as $l) {
						if( $l["loc_id"] == $row["loc_id"] ) $count = $l["count"];
				}
				$loc_ids[] = array($row["loc_id"]=>$row["loc_name"]." ($count)" );
		$category[] = array("id"=>"City", "type"=>"Location", "name"=>$row["loc_name"], 
"c"=>$count,"link"=>"{$row['loc_url']}$gModule/","title"=>"{$row["loc_name"]} Sex Shops");
		}
		$locs = $form->select_add_select();
		$locs .= $form->select_add_array($loc_ids, $loc_id?$loc_id:0);
}

$refine = NULL;
if( $category ) $refine[] = array("name"=>"Location", "col"=>1, "alt"=>$category);

$sphinx->SetLimits( 0, 10, 10 );

$category = $gTitlex = NULL;
if( $total>1 && is_null($lingerie) ) {
	$type = "lingerie";
	$sphinx->SetFilter($type, array(2));
	$sphinx->SetGroupBy( 'lingerie', SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query($query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"Lingerie", "c"=>$match["attrs"]["@count"]);
	}
	if( $category ) $gTitlex .= " Lingerie";
}

$gTitle = $account->core_loc_array['loc_name'] . " Gay Bath Houses {$account->core_loc_array['lp_loc_name']}";

if( $category ) $refine[] = array("name"=>"Amenities", "col"=>1, "alt"=>$category);
$smarty->assign("refine", $refine);

foreach($result as $res) {
		$ids .= $ids ? (",".$res["id"]) : $res["id"];
}

if ($ids)
	$res = $db->q("SELECT s.*, l.loc_url, l.country_sub, r.comment rc, a2.avatar ra, a2.username as ru, if(length(coupon)>1,1,0) coupon
						, (select count(*) FROM place_picture pp where pp.place_file_type_id = 2 and pp.module = 'gaybath' and pp.id = s.id) as video 
					FROM gaybath s 
					INNER JOIN location_location l on l.loc_id = s.loc_id 
					LEFT JOIN (place_review r inner join account a2 using (account_id)) on (s.last_review_id = r.review_id and r.module = 'gaybath') 
					WHERE s.id IN ($ids) 
					ORDER BY field(s.id,$ids)");

while($row=$db->r($res)) {

	if( $zipcode ) {
		if( !isset($lat_me) ) continue;
		if( $row["loc_lat"] && $row["loc_long"] ) {
			$mile = number_format(distance($lat_me, $lon_me, $row["loc_lat"], $row["loc_long"], "M"),1);
		} else
			$mile = "unknown";
	} elseif( $distance ) {
		$mile = number_format(distance($account->core_loc_array["loc_lat"], $account->core_loc_array["loc_long"], $row["loc_lat"], $row["loc_long"], "M"),1);
	}

	$link = dir::getPlaceLinkAux($row['country_sub'], $row["loc_id"], $gModule, $row["id"], $row['name'], trim($row['loc_url'], "/"));

	$lox_name = "{$row["loc_name"]}, {$row["state"]}";
	$extra = $row["edit"] < 1 ? "<span class='nonerotic'>*Not Live*</span>" : NULL;
	$thumbpath = !empty($row["thumb"]) ? "{$config_image_server}/gaybath/t/{$row["thumb"]}" : "/images/placeholder.gif";
	$ra = "{$config_image_server}/avatar/" .  (!empty($row["ra"]) ? $row["ra"] : "no_avatar.jpg");

	$sc[] = array(
		"id"=>$row["id"],
		"name"=>$row["name"],"thumb"=>$row["thumb"],
		"address"=>$row["address1"],
		"phone"=>makeproperphonenumber($row["phone"]),
		"loc_name"=>$lox_name,
		"zipcode"=>$row["zipcode"],
		"distance"=>isset($zipcode)||isset($distance)?$mile:NULL,
		"review"=>$row["review"],
		"edit"=>$row["edit"],
		"sponsor"=>$row["sponsor"],
		"recommend"=>$row["recommend"],
		"linkoverride" => $link,
		"title"=>"- {$row["loc_name"]}, {$row["state"]} {$row["zipcode"]}",
		"longitude"=>$row['loc_long'],
		"latitude"=>$row['loc_lat'],
		"thumbpath"=>$thumbpath,
		"lr"=>$row['last_review_id'],
		"rc"=>$row['rc'],
		"ra"=>$ra,
		"ru"=>$row['ru'],
		"extra"=>$extra,
	   "coupon"=>$row['coupon'],
	   "video"=>$row['video']
	);
} 

$json = json_encode($sc);
$smarty->assign("mapjson", $json);

$smarty->assign("sc", $sc);
$smarty->assign("total", $total);
$smarty->assign("phone1", $_GET["phone1"]);
$smarty->assign("phone2", $_GET["phone2"]);
$smarty->assign("phone3", $_GET["phone3"]);

if( isset($error) ) $smarty->assign("error", $error);
if( isset($alert) ) $smarty->assign("alert", $alert);

if( $zipfound && !strstr($gLocation, "zipcode") ) $query = _pagingQuery(array("loc_id"));
else $query = _pagingQuery(array("loc_id", "zipcode"));

_pagingQueryFilter($query, $filter_link);
$linksort = _pagingQuery(array("order", "orderway"));
$smarty->assign("linksort", $linksort);
$smarty->assign("search_hidden", _pagingQuery(array("loc_id", "zipcode", "miles", "phone"), 1));
$smarty->assign("link", $query);
$smarty->assign("filter", $filter_link);

$dir = new dir;
$ctx->page = $page;
$smarty->assign("pager_array", $dir->get_pager_array($total, $item_per_page, $account->core_loc_array['loc_url']."/{$gModule}/", true));
$smarty->assign("paging", $db->paging($total, $item_per_page, yes, yes, $query));
include_html_head("js", "/js/tools/jquery.autotab.js");

$order = isset($_GET["order"]) ? $_GET["order"] : ($distance?"distance":"last_review_id");
if( !isset($_GET["orderway"]) && $order == "last_review_id" ) $orderway = 1;
elseif( !isset($_GET["orderway"]) && $distance ) $orderway = 2;
$sort = array();
if( isset($zipfound) || isset($distance)) $sort[] = array("key"=> "Distance", "value"=>"distance", "way"=>$order == "distance" && $orderway == 2 ? 1 : 2,
"selected"=>$order == "distance" ?true : false);
$sort[] = array("key"=> "Rating", "value"=>"overall", "way"=>$order == "overall" && $orderway == 1 ? 2 : 1, "selected"=>$order == "overall" ? true : false);
$sort[] = array("key"=> "# of Reviews", "value"=>"review", "way"=>$order == "review" && $orderway == 1 ? 2 : 1, "selected"=> $order == "review"? true : false);
$sort[] = array("key"=> "Review Date", "value"=>"last_review_id", "way"=>$order == "last_review_id" && $orderway == 1 ? 2 : 1, "selected"=>$order=="last_review_id" ?true : 
false);
$sort[] = array("key"=> "Video", "value"=>"video", "way"=>$order == "video" && $orderway == 1 ? 2 : 1, "selected"=> $order == "video"? true : false);
$sort[] = array("key"=> "Coupon", "value"=>"coupon", "way"=>$order == "coupon" && $orderway == 1 ? 2 : 1, "selected"=> $order == "coupon"? true : false);
$smarty->assign("sort", $sort);

if ($zipfound)
	$loc_name = "$zipcode";

$smarty->assign("category", "gay-bath-house");
$smarty->assign("what", "Gay Bath House");
$smarty->assign("worker_link", "gaybath");
$smarty->assign("image_path", "gaybath");
$smarty->assign("place_link", "place");
$smarty->assign("gModule", $gModule);
$smarty->assign("loc_name", $loc_name);
$smarty->assign("rss_available", true);

$advertise->popup();

$file = $smarty->fetch(_CMS_ABS_PATH."/templates/place/place_list.tpl");
echo $file;


?>
