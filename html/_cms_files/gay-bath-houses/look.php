<?php
defined("_CMS_FRONTEND") or die("No Access");

deprecated_controller();

global $db, $smarty, $account, $page, $gTitle, $advertise, $gModule, $gLocation, $gItemID, $mobile, $gDescription, $config_mapquest_key, $config_image_server;

$id = $gItemID;
$account_id = $account->isloggedin();

$review = new review("gaybath", $id, "gay bath house", "", "gaybath");

$res = $db->q("SELECT a.*, l.country_sub, l.loc_url
				FROM gaybath a 
				LEFT JOIN location_location l on l.loc_id = a.loc_id
				WHERE a.id = '$id'");
if( !$db->numrows($res) ) {
	if (empty($account->core_loc_array['loc_url']))
		$account->core_loc_array['loc_url'] = "/";
	system::moved("{$account->core_loc_array['loc_url']}{$gModule}/");
}
$row = $db->r($res);

$name2url = name2url($row['name']);

if ($account->core_sub != $row["country_sub"] || !strstr($_SERVER['REQUEST_URI'], $row['loc_url']) || $gLocation != $name2url)
	system::moved(location::getUrlStatic($row["country_sub"], $row["loc_url"])."{$gModule}/{$name2url}/{$row['id']}");

$gTitle = "{$row["name"]} ". makeproperphonenumber($row['phone']) ." {$row['loc_name']} Gay Bath House";

$mobile_class = $mobile == 1 ? " class='link'" : "";

$loc = "<a href='{$account->core_loc_array['loc_url']}$gModule/' title='{$row["loc_name"]} gay bath house'$mobile_class>{$row["loc_name"]}</a>, {$row["state"]} {$row["zipcode"]}";
$smarty->assign("link_back", "{$account->core_loc_array["loc_url"]}$gModule/");

// map
$map = prepareMap($id, 'gaybath', $row, 'gaybath', 'gaybath');
$smarty->assign("map", $map);
$smarty->assign("link_map_update", "/map?id={$id}&section=gaybath&update=1");

$quickread = $review->reviews();
$smarty->assign("quickread", $quickread);
$smarty->assign("description", $row['gdescription']);
if (empty($quickread) && !empty($row['gdescription'])) {
	$gDescription = $row['gdescription'];
} else {
	if (!empty($quickread))
		$gDescription = substr(htmlspecialchars($quickread[0]['review'], ENT_QUOTES), 0, 100) .". ";
	$gDescription .= "{$row['name']} $phone {$row['address1']} Gay Bath Houses in {$row['loc_name']} {$row['state_name']}";
}

global $ctx;
$ctx->item = $row;
$smarty->assign("hours", dir::getHours(($row["byapponly"] == 1) ? true : false));
$smarty->assign("current_weekday", strtolower(date("l")));

if( !empty($row["other_services"]) )
	$smarty->assign("info", $row["other_services"]);

if( $row['daypass'] )
	$c1[] = array("label" => "Day Pass", "val" => "\${$row['daypass']}.00");
if( $row['mem_1month'] )
	$c1[] = array("label" => "Monthly Membership", "val" => "\${$row['mem_1month']}.00");
if( $row['mem_3month'] )
	$c1[] = array("label" => "3 Months Membership", "val" => "\${$row['mem_3month']}.00");
if( $row['mem_6month'] )
	$c1[] = array("label" => "6 Months Membership", "val" => "\${$row['mem_6month']}.00");
if( $row['mem_annual'] )
	$c1[] = array("label" => "Annual Membership", "val" => "\${$row['mem_annual']}.00");
if( $row['parking'] ) {
	$parking = $row['parking'] == -1 ? "Free" : "\${$row['parking']}.00";
	$c1[] = array("label" => "Parking", "val" => "{$parking}");
}
if( $row['stdroom'] )
	$c1[] = array("label" => "Standard Room", "val" => "\${$row['stdroom']}");
if( $row['viproom'] )
	$c1[] = array("label" => "VIP Room", "val" => "\${$row['viproom']}");
if( $row['slingroom'] )
	$c1[] = array("label" => "Sling Room", "val" => "\${$row['slingroom']}");
if( $row['locker'] )
	$c1[] = array("label" => "Locker", "val" => "\${$row['locker']}");

if( $row['gym'] )
	$c1[] = array("label" => "Gym", "val" => "Yes");
if( $row['pool'] )
	$c1[] = array("label" => "Pool", "val" => "Yes");
if( $row['sauna'] )
	$c1[] = array("label" => "Sauna", "val" => "Yes");
if( $row['jacuzzi'] )
	$c1[] = array("label" => "Jacuzzi", "val" => "Yes");
if( $row['showers'] )
	$c1[] = array("label" => "Showers", "val" => "Yes");
if( $row['internet'] )
	$c1[] = array("label" => "Internet", "val" => "Yes");
if( $row['gloryhole'] )
	$c1[] = array("label" => "Gloryhole", "val" => "Yes");

if( !empty($row["coupon"]) )
	$c1[] = array("label" => "Coupon", "val" => "<a href='coupon?coupon={$row["coupon"]}' title='coupon' target='_blank' rel='nofollow'>Click to see</a>");

$smarty->assign("c1", $c1);

$smarty->assign("category", "gay-bath-house");
$smarty->assign("gModule",$gModule);
$smarty->assign("what", "Gay Bath House");

$smarty->assign("name", $row["name"]);
$smarty->assign("loc_name", $row["loc_name"]);
$smarty->assign("id", $id);
$smarty->assign("editlink", "gaybath");
$smarty->assign("totalreview", $row["review"]);
$smarty->assign("overall", $row["overall"]);

if( strlen($row["website"])>1 ) {
	$url = trim($row["website"]);
	if( strncmp($url, "http://", 7) )  $url = "http://" . $url;
	$web = parse_url($url);
	$web_url = $url;
	$web_host = $web["host"];
}

$phone = makeproperphonenumber($row["phone"]);
if ($row['fax'])
	$fax = makeproperphonenumber($row["fax"]);

$addressLx = "<b>{$phone}</b><br/>";
if( $fax ) $addressLx .= " Fax: $fax<br/>";
$addressLx .= "{$row["address1"]}<br />{$loc}<br/>";
if($web_url) $addressLx .= "<a href=\"$web_url\" target=\"_blank\" rel=\"nofollow\" class=\"website\">$web_host</a>";
if( $row['email'] ) $addressLx .= "<a href=\"mailto:{$row['email']}\" class=\"website\">{$row['email']}</a>";
$smarty->assign("address", $addressLx);

if( !empty($row["owner"])&&($row["owner"]==$account_id||$account->isrealadmin()) ) $smarty->assign("isowner", true);

include_html_head("css", "/css/info.css");

//photos
$imagedir = "gaybath";
$photos = array();
if ($row["image"] != "" && $row["thumb"] != "") {
	$photos["image"] = "{$config_image_server}/{$imagedir}/{$row["image"]}";
	$photos["thumb"] = "{$config_image_server}/{$imagedir}/t/{$row["thumb"]}";

	$pics = array();
	$rex = $db->q("select picture, filename, thumb from place_picture where id = '$id' and module = 'gaybath'");
	while($rox=$db->r($rex)) {
		$pics[] = array(
			"module" => "gaybath",
			"id" => $rox["picture"],
			"image" => "{$config_image_server}/{$imagedir}/".$rox["filename"],
			"thumb" => "{$config_image_server}/{$imagedir}/t/".$rox["thumb"],
			);
	} 

	if ($pics) {
		include_html_head("js", "/js/jquery.tools.scrollable.js");
		$pics_in_group = 2;
		$pics_groups = array_chunk($pics, $pics_in_group);
		$photos["pics"]  = $pics_groups;
	}
}
$smarty->assign("photos", $photos);

/*
//forum
$forum_id = 151;
$smarty->assign("forum_topic", forum::getLastTopicsForPlace($forum_id, $id));
$smarty->assign("new_topic_link", forum::getNewTopicLinkForCategory($forum_id, $id));
*/

//MapQuest and Leaflet include
include_html_head('js', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.js');
include_html_head('css', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.css');
include_html_head('js', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.js');
include_html_head('css', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.css');

if (permission::has("edit_all_places"))
	$smarty->assign("can_edit_place", true);

$smarty->display(_CMS_ABS_PATH."/templates/place/place_place.tpl");

?>
