<?php

defined("_CMS_FRONTEND") or die("No Access");

deprecated_controller();

require_once _CMS_ABS_PATH."/_cms_files/worker/array_sc.php";

global $db, $smarty, $page, $account;
$form = new form; $system = new system;

if( !$account->isadmin() ) $system->moved("/");

$res = $db->q("select * from gaybath where edit < 1 order by name");
while($row=$db->r($res)) {

	$loc_name = $row["loc_name"] . " <a href=\"http://www.google.com/search?q={$row["name"]}+{$row["zipcode"]}\" target='_blank'>google it</a>";

	$sc[] = array(
		"id"=>$row["id"],
		"name"=>$row["name"],
		"thumb"=>$row["thumb"],
		"address"=>$row["address1"],
		"phone"=>makeproperphonenumber($row["phone"]),
		"loc_name"=>$loc_name,
		"zipcode"=>$row["zipcode"],
		"distance"=>isset($zipcode)?(float)$mile:NULL,
		"review"=>$row["review"],
		"edit"=>$row["edit"]
	);
} 
$total = count($sc);
$sc = $db->arrayClear($sc, 40, $total);

$smarty->assign("sc", $sc);
$smarty->assign("total", $total);

if( isset($error) ) $smarty->assign("error", $error);

$query = _pagingQuery();
$smarty->assign("paging", $db->paging($total, 40, yes, yes, $query));

if ($account->isadmin()) {
	$smarty->assign("ref", urlencode($_SERVER["REQUEST_URI"]));
}

$smarty->assign("worker_link", "gaybath");
$smarty->assign("edit", true);
$smarty->display(_CMS_ABS_PATH."/templates/place/place_list.tpl");

?>
