<?php

defined('_CMS_FRONTEND') or die('Restricted access !');

deprecated_controller();

require_once (_CMS_ABS_PATH."/inc/classes/class.review.php");

global $db, $account, $smarty;
$system = new system;

$id = isset($_REQUEST["id"]) ? intval($_REQUEST["id"]) : NULL;
if( !$id ) $system->go("/");

$review = new review("gaybath", $id, "gay bath house", "", "gaybath");
$review->spam_protection = true;
$review->process();

?>
