<?php

defined("_CMS_FRONTEND") or die("No Access");

deprecated_controller();

require_once _CMS_ABS_PATH."/_cms_files/worker/array_sc.php";

global $db, $smarty, $page, $account, $gTitle, $advertise, $gModule, $gLocation, $gFilename, $mobile, $ctx, $sphinx;
$system = new system;

$location = location::findOneById($ctx->location_id);
if ($location) {
	$smarty->assign("link_back", $location->getUrl());
	$smarty->assign("location_video_html", $location->getVideoHtml());
}	

$core_state_id = $account->corestate(1);
if (!$core_state_id)
	return;

$loc_around = array();
if ($account->core_loc_id && $account->core_loc_type > 2)
    $loc_around = $account->locAround();

$lingerie = isset($_GET["lingerie"]) ? GetGetParam("lingerie") : NULL;
$adulttoys = isset($_GET["adulttoys"]) ? GetgetParam("adulttoys") : NULL;
$adultdvd = isset($_GET["adultdvd"]) ? GetGetParam("adultdvd") : NULL;
$peepshows = isset($_GET["peepshows"]) ? intval($_GET["peepshows"]) : NULL;
$adultbooks = isset($_GET["adultbooks"]) ? intval($_GET["adultbooks"]) : NULL;
$arcade = isset($_GET["arcade"]) ? intval($_GET["arcade"]) : NULL;
$gloryholes = isset($_GET["gloryhole"]) ? intval($_GET["gloryhole"]) : NULL;
$theater = isset($_GET["theater"]) ? intval($_GET["theater"]) : NULL;
$fetish	= isset($_GET["fetish"]) ? intval($_GET["fetish"]) : NULL;
$lgbt = isset($_GET["lgbt"]) ? intval($_GET["lgbt"]) : NULL;
$buddybooth = isset($_GET["buddybooth"]) ? intval($_GET["buddybooth"]) : NULL;

$advertise->showAd(8, $core_state_id);

$loc_name = $account->core_loc_array['loc_name'];

$gTitlex = NULL;
$filter_link = array();
foreach($_GET as $key => $value) {
		if( $key == "ethx" ) {
				$filter["eth"][] = $value; $filter_link[] = array("id"=>$value,"type"=>"eth","name"=>"Ethnicity");
		} else if( in_array($key, array("query", "lingerie", "adulttoys", "adultdvd", "peepshows", "adultbooks", "arcade", "gloryhole", "theater", "fetish", "lgbt",
"buddybooth", "zipcode", "phone", "loc_id")) && $value != "" ) {
		$skipfilter = 0;
				$ftype = $key;
		$valuex = $value == 2 ? "Yes" : ($value == 1 ? "No" : $value);
				if( $key == "lingerie" ) { $key = "Lingerie: $valuex"; $gTitlex .= $gTitlex ? ", Lingerie":" Lingerie";}
				else if( $key == "adulttoys" ) { $key = "Adult Toys: $valuex"; $gTitlex .= $gTitlex ? ", Adult Toys":" Adult Toys";}
				else if( $key == "adultdvd" ) { $key = "Adult DVDs: $valuex"; $gTitlex .= $gTitlex ? ", Adult DVDs":" Adult DVDs"; }
				else if( $key == "peepshows" ) { $key = "Peep Shows: $valuex"; $gTitlex .= $gTitlex ? ", Peep Shows":" Peep Shows"; }
				else if( $key == "adultbooks" ) { $key = "Adult Books: $valuex"; $gTitlex .= $gTitlex ? ", Adult Books":" Adult Books";}
				else if( $key == "arcade" ) { $key = "Arcade: $valuex"; $gTitlex .= $gTitlex ? ", Adult Video Arcades":" Adult Video Arcades"; }
				else if( $key == "gloryhole" ) { $key = "Glory Holes"; $gTitlex .= $gTitlex ? ", Glory Holes":" Glory Holes";}
				else if( $key == "theater" ) { $key = "Theater"; $gTitlex .= $gTitlex ? ", Theater":" Theater";}
				else if( $key == "fetish" ) { $key = "Fetish"; $gTitlex .= $gTitlex ? ", Fetish":" Fetish";}
				else if( $key == "lgbt" ) { $key = "LGBT"; $gTitlex .= $gTitlex ? ", LGBT":" LGBT";}
				else if( $key == "buddybooth" ) { $key = "Buddy Booths"; $gTitlex .= $gTitlex ? ", Buddy Booths":" Buddy Booths";}
				else if( $key == "zipcode" ) { 
			$key = "Zip Code: $valuex"; $skipfilter = 1; 
			$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key, "link"=>"{$account->core_loc_array['loc_url']}$gModule/"); }
				else if( $key == "query" ) { $key = "'$valuex'"; }
				else if( $key == "phone" ) { $key = "Phone #: '$valuex'"; }
				else if( $key == "loc_id" ) { $key = $loc_name; }

		if( !$skipfilter ) {
			if( $key == "loc_id" ) $filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key, "link"=>"{$account->core_loc_array['loc_url']}$gModule/");
			else $filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key);
		}
	}
}

if( !empty($gLocation) && !strstr($gLocation, "zipcode") ) {
		$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$loc_name, "link"=>"http://$state_link.{$account->core_host}/$gModule/");
}

$item_per_page = isset($_GET["ipp"]) ? intval($_GET["ipp"]) : 20;
if( $item_per_page > 60 ) $item_per_page = 20;
$start = (($page*$item_per_page)-$item_per_page);
if( $start < 0 ) $start = 0;

$sphinx_index = "gay";
$sphinx->reset();
$sphinx->SetLimits( $start, $item_per_page, 1000 );

if( isset($_GET["zipcode"]) && !empty($_GET["zipcode"]) ) {
	if( ($pos=strpos($_GET["zipcode"], "-")) )
		$_GET["zipcode"] = substr($_GET["zipcode"], 0, $pos);
	$zipcode = preg_replace("/[^0-9a-z]/i","", $_GET["zipcode"]);
	$mile_in = intval($_GET["miles"]); if( !$mile_in ) $mile_in = 10;
	if( $mile_in == -1 ) $mile_in = 1000;
	$res = $db->q("select z.lat, z.lon, radians(z.lat) latt, radians(z.lon) longg, l.loc_parent from core_zip z inner join location_location l on z.loc_id = l.loc_id where z.zip = '$zipcode' limit 1");
	if( $db->numrows($res) ) {
		$s = $db->r($res); 
		$lat_me = $s["lat"]; $lon_me = $s["lon"];
		$latt = $s["latt"]; $long = $s["longg"];
		$sphinx->SetGeoAnchor('loc_lat', 'loc_long', $latt, $long);
		$sphinx->SetFilterFloatRange('@geodist', 0, ($mile_in*1609.344));
		$zipfound = 1;
	} else {
		$error = "Zip code($zipcode) could not find. Please make sure that you typed the correct zip code.";
	}
	$smarty->assign("miles", $mile_in==1000?-1:$mile_in);
	$smarty->assign("zipcode", $_GET["zipcode"]);
}

if( !is_null($lingerie) ) $sphinx->SetFilter('lingerie', array($lingerie));
if( !is_null($adulttoys) ) $sphinx->SetFilter('adulttoys', array($adulttoys));
if( !is_null($adultdvd) ) $sphinx->SetFilter('adultdvd', array($adultdvd));
if( !is_null($peepshows) ) $sphinx->SetFilter('peepshows', array($peepshows));
if( !is_null($adultbooks) ) $sphinx->SetFilter('adultbooks', array($adultbooks));
if( !is_null($arcade) ) $sphinx->SetFilter('arcade', array($arcade));
if( !is_null($gloryholes) ) $sphinx->SetFilter('gloryhole', array($gloryholes));
if( !is_null($theater) ) $sphinx->SetFilter('theater', array($theater));
if( !is_null($fetish) ) $sphinx->SetFilter('fetish', array($fetish));
if( !is_null($lgbt) ) $sphinx->SetFilter('lgbt', array($lgbt));
if( !is_null($buddybooth) ) $sphinx->SetFilter('buddybooth', array(1));
if( $account->core_loc_id && $account->core_loc_type > 2 && !$zipfound) {
	$nloc[] = $account->core_loc_id;
	$sphinx->SetFilter('loc_id', $loc_around);
}
elseif( $account->core_loc_id && $account->core_loc_type < 3 )
	$sphinx->SetFilter('state_id', array($account->core_loc_id));

if (!$account->isWorker())
	$sphinx->SetFilter('edit', array(1, 2));

$query = "";
if( isset($_GET["phone"]) && !empty($_GET["phone"]) ) {
		$phone = preg_replace("/[^0-9\*]/", "", $_GET["phone"]);
		$query = $phone;
		//else $smarty->assign("err_phone", true);
}
if( isset($_GET["name"]) && !empty($_GET["name"]) ) {
		$name = GetGetParam("name");
		$query .= $query ? " $name" : "$name";
		$error = "No strip club found with the word: ".$_GET["name"];
		$smarty->assign("name", $_GET["name"]);
}

$orderway = isset($_GET["orderway"])&& intval($_GET["orderway"]) ? intval($_GET["orderway"]) : (($zipfound||$distance)?2:1);
$way = $orderway == 1 ? "DESC" : "ASC";

if($_GET["order"] == 'overall') { $order = "overall $way"; }
elseif($_GET["order"] == 'review') $order = "review $way";
elseif($_GET["order"] == 'distance' || ($zipfound||$distance) ) { $order = "@geodist $way, review DESC"; }
elseif($_GET["order"] == "name") $order = "name $way";
elseif($_GET["order"] == "last_review_id") $order = "last_review_id $way";
else $order = "sponsor DESC, last_review_id DESC";
$sphinx->SetSortMode(SPH_SORT_EXTENDED, $order);

$results = $sphinx->Query($query, $sphinx_index);
$total = $results["total_found"];
$result = $results["matches"];

$refine = NULL;

$sphinx->SetLimits( 0, 10, 10 );

$category = $gTitlex = NULL;
if( $total>1 && is_null($lingerie) ) {
	$type = "lingerie";
	$sphinx->SetFilter($type, array(2));
	$sphinx->SetGroupBy( 'lingerie', SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"Lingerie", "c"=>$match["attrs"]["@count"]);
	}
	if( $category ) $gTitlex .= " Lingerie";
}
if( $total>1 && is_null($adulttoys) ) {
	$type = "adulttoys";
	$sphinx->SetFilter($type, array(2));
	$sphinx->SetGroupBy( 'adulttoys', SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"Adult Toys", "c"=>$match["attrs"]["@count"]);
	}
	if( !empty($results["matches"]) ) $gTitlex .= ", Adult Toys";
}
if( $total>1 && is_null($adultdvd) ) {
	$type = "adultdvd";
	$sphinx->SetFilter($type, array(2));
	$sphinx->SetGroupBy( 'adultdvd', SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"Adult DVDs", "c"=>$match["attrs"]["@count"]);
	}
	if( !empty($results["matches"]) ) $gTitlex .= ", Adult DVDs";
}
if( $total>1 && is_null($peepshows) ) {
	$type = "peepshows";
	$sphinx->SetFilter($type, array(2));
	$sphinx->SetGroupBy( 'peepshows', SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"Peep Shows", "c"=>$match["attrs"]["@count"]);
	}
	if( !empty($results["matches"]) ) $gTitlex .= ", Peep Shows";
}
if( $total>1 && is_null($adultbooks) ) {
	$type = "adultbooks";
	$sphinx->SetFilter($type, array(2));
	$sphinx->SetGroupBy( 'adultbooks', SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"Adult Books", "c"=>$match["attrs"]["@count"]);
	}
	if( !empty($results["matches"]) ) $gTitlex .= ", Adult Books";
}
if( $total>1 && is_null($arcade) ) {
	$type = "arcade";
	$sphinx->SetFilter($type, array(2));
	$sphinx->SetGroupBy( 'arcade', SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"Adult Video Arcade", "c"=>$match["attrs"]["@count"]);
	}
	if( !empty($results["matches"]) ) $gTitlex .= ", Adult Video Arcades";
}
if( $total>1 && is_null($gloryholes) ) {
	$type = "gloryhole";
	$sphinx->SetFilter($type, array(2));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index);
	$sphinx->ResetFilter($type);
	if( is_array($results["matches"]) ) {
		foreach($results["matches"] as $match) {
			$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"Glory Hole", "c"=>$match["attrs"]["@count"]);
		}
	} else {
		//$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"Glory Hole", "c"=>0, "nonlink"=>true);
	}
	if( !empty($results["matches"]) ) $gTitlex .= ", Glory Holes";
}
if( $total>1 && is_null($theater) ) {
	$type = "theater";
	$sphinx->SetFilter($type, array(2));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"Theater", "c"=>$match["attrs"]["@count"]);
	}
	if( !empty($results["matches"]) ) $gTitlex .= ", Theater";
}
if( $total>1 && is_null($fetish) ) {
	$type = "fetish";
	$sphinx->SetFilter($type, array(2));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"Fetish", "c"=>$match["attrs"]["@count"]);
	}
	if( !empty($results["matches"]) ) $gTitlex .= ", Fetish";
}
if( $total>1 && is_null($lgbt) ) {
	$type = "lgbt";
	$sphinx->SetFilter($type, array(2));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"LGBT", "c"=>$match["attrs"]["@count"]);
	}
	if( !empty($results["matches"]) ) $gTitlex .= ", LGBT";
}
if( $total>1 && is_null($buddybooth) ) {
	$type = "buddybooth";
	$sphinx->SetFilter($type, array(1));
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, $sphinx_index);
	$sphinx->ResetFilter($type);
	foreach($results["matches"] as $match) {
		$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"Buddy Booths", "c"=>$match["attrs"]["@count"]);
	}
	if( !empty($results["matches"]) ) $gTitlex .= ", Buddy Booths";
}

if( $category ) $refine[] = array("name"=>"Amenities", "col"=>1, "alt"=>$category);
$smarty->assign("refine", $refine);

if( isset($_GET["name"]) && !empty($_GET["name"]) ) {
	$name_sql = "and name like '%".GetGetParam("name")."%'";
	$smarty->assign("name", $_GET["name"]);
	$error = "No store found with the word: ".$_GET["name"];
}

foreach($result as $res) {
		$ids .= $ids ? (",".$res["id"]) : $res["id"];
}

$gTitle = "Gay Places in {$loc_name}";

if( $ids )
	$res = $db->q("SELECT s.*, l.loc_url, l.country_sub, r.comment rc, a2.avatar ra, a2.username as ru 
					FROM gay s 
					INNER JOIN location_location l on l.loc_id = s.loc_id
					LEFT JOIN (place_review r inner join account a2 using (account_id)) on (s.last_review_id = r.review_id and r.module = 'gay') 
					WHERE s.id in ($ids) 
					ORDER BY field(s.id,$ids)");

while($row=$db->r($res)) {

	if( $zipcode ) {
		if( !isset($lat_me) ) continue;
		if( $row["loc_lat"] && $row["loc_long"] ) {
			$mile = number_format(distance($lat_me, $lon_me, $row["loc_lat"], $row["loc_long"], "M"),1);
		} else $mile = "unknown";
	} elseif( $distance ) {
		$mile = number_format(distance($_SESSION["loc_lat"], $_SESSION["loc_long"], $row["loc_lat"], $row["loc_long"], "M"),1);
	}

	$link = dir::getPlaceLinkAux($row['country_sub'], $row["loc_id"], $gModule, $row["id"], $row['name'], trim($row['loc_url'], "/"));

	$lox_name = "{$row["loc_name"]}, {$row["state"]}";
	$extra = $row["edit"] < 1 ? "<span class='nonerotic'>*Not Live*</span>" : NULL;
	$thumbpath = !empty($row["thumb"]) ? "{$config_image_server}/gay/t/{$row["thumb"]}" : "/images/placeholder.gif";
	$ra = "{$config_image_server}/avatar/" .  (!empty($row["ra"]) ? $row["ra"] : "no_avatar.jpg");

	$sc[] = array(
		"id"=>$row["id"],
		"name"=>$row["name"],"thumb"=>$row["thumb"],
		"address"=>$row["address1"],
		"phone"=>makeproperphonenumber($row["phone"]),
		"loc_name"=>$lox_name,
		"zipcode"=>$row["zipcode"],
		"distance"=>isset($zipcode)||isset($distance)?$mile:NULL,
		"review"=>$row["review"],
		"edit"=>$row["edit"],
		"sponsor"=>$row["sponsor"],
		"recommend"=>$row["recommend"],
		"linkoverride" => $link,
		"title"=>"- {$row["loc_name"]}, {$row["state"]} {$row["zipcode"]}",
		"longitude"=>$row['loc_long'],
		"latitude"=>$row['loc_lat'],
		"thumbpath"=>$thumbpath,
		"lr"=>$row['last_review_id'],
		"rc"=>$row['rc'],
		"ra"=>$ra,
		"ru"=>$row['ru'],
		"extra"=>$extra
	);
} 

$json = json_encode($sc);
$smarty->assign("mapjson", $json);

$smarty->assign("sc", $sc);
$smarty->assign("total", $total);
$smarty->assign("phone1", $_GET["phone1"]);
$smarty->assign("phone2", $_GET["phone2"]);
$smarty->assign("phone3", $_GET["phone3"]);

if( isset($error) ) $smarty->assign("error", $error);
if( isset($alert) ) $smarty->assign("alert", $alert);

if( $zipfound && !strstr($gLocation, "zipcode") ) $query = _pagingQuery(array("loc_id"));
else $query = _pagingQuery(array("loc_id", "zipcode"));

_pagingQueryFilter($query, $filter_link);
$linksort = _pagingQuery(array("order", "orderway"));
$smarty->assign("linksort", $linksort);
$smarty->assign("search_hidden", _pagingQuery(array("loc_id", "zipcode", "miles", "phone"), 1));
$smarty->assign("link", $query);
$smarty->assign("filter", $filter_link);

$dir = new dir;
$ctx->page = $page;
$smarty->assign("pager_array", $dir->get_pager_array($total, $item_per_page, $account->core_loc_array['loc_url']."/{$gModule}/", true));
$smarty->assign("paging", $db->paging($total, 20, yes, yes, $query));
include_html_head("js", "/js/tools/jquery.autotab.js");

$order = isset($_GET["order"]) ? $_GET["order"] : ($distance?"distance":"last_review_id");
if( !isset($_GET["orderway"]) && $order == "last_review_id" ) $orderway = 1;
elseif( !isset($_GET["orderway"]) && $distance ) $orderway = 2;
$sort = array();
if( isset($zipfound) || isset($distance)) $sort[] = array("key"=> "Distance", "value"=>"distance", "way"=>$order == "distance" && $orderway == 2 ? 1 : 2,
"selected"=>$order == "distance" ?true : false);
$sort[] = array("key"=> "Rating", "value"=>"overall", "way"=>$order == "overall" && $orderway == 1 ? 2 : 1, "selected"=>$order == "overall" ? true : false);
$sort[] = array("key"=> "Number of Reviews", "value"=>"review", "way"=>$order == "review" && $orderway == 1 ? 2 : 1, "selected"=> $order == "review"? true : false);
$sort[] = array("key"=> "Store Recently Reviewed", "value"=>"last_review_id", "way"=>$order == "last_review_id" && $orderway == 1 ? 2 : 1, "selected"=>$order=="last_review_id" ?true : false);
$smarty->assign("sort", $sort);

$smarty->assign("category", "gay");
$smarty->assign("what", 'Gay Place');
$smarty->assign("worker_link", "gay");
$smarty->assign("image_path", "gay");
$smarty->assign("place_link", "place");
$smarty->assign("gModule", $gModule);
$smarty->assign("loc_name", $loc_name);
$smarty->assign("rss_available", true);

$advertise->popup();

$file = $smarty->fetch(_CMS_ABS_PATH."/templates/place/place_list.tpl");
echo $file;


?>
