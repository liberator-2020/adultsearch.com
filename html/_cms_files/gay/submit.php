<?php

defined('_CMS_FRONTEND') or die('Restricted access');

deprecated_controller();

global $db, $smarty, $account, $page;
$system = new system;

require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
$form = new data;
$system = new system;

$form->table = "gay";

$c = new CColumnId("id", "ID");
$form->AppendColumn($c);

$c = new CColumnString("name", "Name");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnLocation("City", "Location", 3);
$c->setColMandatory(true);
$c->setLocName("loc_name", "loc_name", "City");
$c->setLocName("state", "s", "City");
$form->AppendColumn($c);

$c = new CColumnString("address1", "Address");
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "Zipcode");
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Club Phone");
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setPath("gay");
$c->setWatermark();
$form->AppendColumn($c);

$form->visitor_mode = true;
$form->visitor_url = "/gay/";
$form->admin_url = "/worker/gay";
$form->ShowPage();

?>
