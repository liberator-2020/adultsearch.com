<?php
/**
 * This is called from http://adultsearch.com/m/location to redirect to closest city
 */
  
global $db, $config_site_url;

$my_lat = preg_replace('/[^0-9\.\-]/', '', $_REQUEST["lat"]);
$my_long = preg_replace('/[^0-9\.\-]/', '', $_REQUEST["long"]);

if (!$my_lat || !$my_long)
	system::moved("/");

$res = $db->q("SELECT *, SQRT(
					POW(69.1 * (loc_lat - {$my_lat}), 2) +
					POW(69.1 * ({$my_long} - loc_long) * COS(loc_lat / 57.3), 2)) as distance
				FROM location_location 
				WHERE loc_type = 3 AND ( significant = 1 OR has_place_or_ad = 1 )
				HAVING distance < 150 
				ORDER BY distance 
				LIMIT 1", []);

if ($db->numrows($res)) {
	$row = $db->r($res);
	$loc = location::withRow($row);
	if ($loc) {
		header("Location: {$loc->getUrl()}");
		die;
	}
}

//echo "Could not find a location within 100 miles of you, please browse our locations instead.  <a href='http://www.adultsearch.com/'>Browse Now</a>"; 
header("Location: {$config_site_url}/m/location?nlr=1");
?>
