<?php
defined("_CMS_FRONTEND") or die("no access");

global $db, $config_image_server;

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set("memory_limit","512M");

file_log("feed", "Request, HTTP_IF_MODIFIED_SINCE='{$_SERVER['HTTP_IF_MODIFIED_SINCE']}'");

if (!isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
	header('HTTP/1.1 400 Bad Request', true, 400);
	header('Content-Type: application/json; charset=UTF-8');
	die(json_encode(array("status" => 400, "message" => "Bad Request; missing 'If-Modified-Since' header")));
}

//$ims = "Thu, 19 Sep 2013 00:00:00 GMT";
//$ims = "Mon, 28 Jan 2013 00:00:00 GMT";
$ims = $_SERVER['HTTP_IF_MODIFIED_SINCE'];


$dt = date_create_from_format("D, j M Y H:i:s e", $ims);
if ($dt === false) {
	header('HTTP/1.1 400 Bad Request', true, 400);
	header('Content-Type: application/json; charset=UTF-8');
	die(json_encode(array("status" => 400, "message" => "Bad Request; invalid value of 'If-Modified-Since' header, please use HTTP format of datetime")));
}

$from = $dt->getTimestamp();
if ($from === false || $from == -1) {
	header('HTTP/1.1 400 Bad Request', true, 400);
	header('Content-Type: application/json; charset=UTF-8');
	die(json_encode(array("status" => 400, "message" => "Bad Request; invalid value of 'If-Modified-Since' header, please use HTTP format of datetime")));
}

function attrs_to_sql($a) {
	$sql = "";
	foreach($a as $b) {
	    $sql .= ", {$b} ";
	}
	return $sql;
}

function get_reviews($module, $id) {
	global $db;

	$result = array();
	$res = $db->q("SELECT r.reviewdate, r.comment, r.star
					FROM place_review r 
					WHERE r.id = ? AND r.module = ?
					ORDER BY r.reviewdate DESC",
			array($id, $module));
	while ($row = $db->r($res)) {
		$review = array(
			"date" => $row["reviewdate"],
			"star" => $row["star"],
			"comment" => $row["comment"],
			);
		$result[] = $review;
	}
	return $result;
}

function add_hours($row, &$result_array) {
	global $ctx;
	$ctx->item = $row;
	$result_array = array_merge($result_array, dir::getHours());
}

function yn($val) {
	return ($val) ? "Yes" : "No";
}

function yn3($val) {
	if ($val == 2)
		return "Yes";
	else if ($val == 1)
        return "No";
	else
		return "";
}

global $attr_types;
$attr_types = array();
$res = $db->q("SELECT name, datatype, options FROM attribute");
while ($row = $db->r($res)) {
	$entry = array("datatype" => $row["datatype"]);
	if ($row["datatype"] == "enum") {
		$arr1 = explode("|", $row["options"]);
		$options = array();
		foreach ($arr1 as $item) {
			$arr2 = explode("@", $item);
			if (count($arr2) == 2)
				$options[$arr2[0]] = $arr2[1];
		}
		$entry["options"] = $options;
	}
	$attr_types[$row["name"]] = $entry;
}

function get_attr_val($name, $val) {
	global $attr_types;
	if (!array_key_exists($name, $attr_types)) {
		return $val;
	}
	$attr = $attr_types[$name];
	if ($attr["datatype"] == "boolean") {
		return yn($val);
	} else if ($attr["datatype"] == "boolean3") {
        return yn3($val);
    } else if ($attr["datatype"] == "enum") {
		if (!array_key_exists($val, $attr["options"]))
			return $val;
        return $attr["options"][$val];
    }
}


$data = array();

//echo "from=$from";die;
if ($from > 0) {
	//normal date boundary
	$where = "(p.created >= FROM_UNIXTIME(?) OR p.updated >= ? OR p.deleted >= ?) 
				AND (p.created < FROM_UNIXTIME(?) OR p.created IS NULL OR p.deleted < ? OR p.deleted IS NULL)";
	$params = array($from, $from, $from, $from, $from);
} else {
	//all time select
	$where = "p.deleted IS NULL";
	$params = array();
}


//place table
$sql = "select attribute_id, name from attribute";
$rex = $db->q($sql);
$sql_denorm = "";
$attrs = array();
$attr_count = count($rex);
while($rox = $db->r($rex)) {
    $attr_id = $rox["attribute_id"];
    $attr_name = $rox["name"];
    $attrs[$attr_id] = $attr_name;
    $sql_denorm .= ", MAX(CASE WHEN pa.attribute_id = '$attr_id' THEN pa.value ELSE NULL END) AS $attr_name";
    $attrs[] = $attr_name;
}

$res = $db->q("
SELECT p.place_id, p.place_type_id, pt.name as type, p.loc_id, p.name, p.address1, p.address2, p.zipcode, p.phone1, p.email, p.website
	, p.loc_lat, p.loc_long, p.thumb
	, l.country_sub, l.loc_url, l.dir, l.loc_name as city_name
	, (CASE WHEN ls.loc_type = 2 THEN ls.loc_name ELSE '' END) AS state_name
	, lc.loc_name AS country_name
	, (CASE WHEN p.deleted IS NOT NULL THEN 'delete' WHEN p.updated IS NOT NULL THEN 'update' ELSE 'new' END) AS delta_type
	, (CASE WHEN p.deleted IS NOT NULL THEN p.deleted WHEN p.updated IS NOT NULL THEN p.updated ELSE UNIX_TIMESTAMP(p.created) END) AS last_modified
	{$sql_denorm}
FROM place p
INNER JOIN place_type pt on pt.place_type_id = p.place_type_id
INNER JOIN location_location l on p.loc_id = l.loc_id
INNER JOIN location_location ls on ls.loc_id = l.loc_parent
INNER JOIN location_location lc on lc.loc_id = l.country_id
LEFT JOIN place_attribute pa on pa.place_id = p.place_id
WHERE {$where} 
GROUP BY p.place_id
ORDER BY p.created ASC, p.updated ASC, p.deleted ASC",
$params
);
//TODO fix order and add custom attributes

while ($row = $db->r($res)) {
	
	//images
	$res2 = $db->q("
		SELECT thumb 
		FROM place_picture 
		WHERE module = 'place' AND (place_file_type_id IS NULL OR place_file_type_id = 0) AND loc_id IS NULL AND visible = 1 AND id = ?
		", 
		[$row["place_id"]]
		);
	$images = array();
	while ($row2 = $db->r($res2)) {
		$images[] = "{$config_image_server}/place/t/{$row2["thumb"]}";
	}

	$place_url = dir::getPlaceLink($row, true);

	$thumbnail_url = ($row["thumb"]) ? "{$config_image_server}/place/t/{$row["thumb"]}" : "";
	$entry = array(
		"id" => "P".$row["place_id"],
		"delta_type" => $row["delta_type"],
		"source_type" => $row["type"],
		"last_modified" => date("c", $row["last_modified"]),
		"last_modified_unix" => $row["last_modified"],
		"href" => $place_url,
		//2019-07-20 zach: write review link should go to business listing page
		//"write_review_href" => dir::getPlaceLink($row, true)."/add_review",
		"write_review_href" => $place_url,
		"thumbnail_href" => $thumbnail_url,
		"images" => $images,
		"name" => $row["name"],
		"city_name" => $row["city_name"],
		"country_name" => $row["country_name"],
		"loc_lat" => $row["loc_lat"],
		"loc_long" => $row["loc_long"],
		);
	if ($row["address1"] != null && $row["address1"] != "")
		$entry["address1"] = $row["address1"];
	if ($row["address2"] != null && $row["address2"] != "")
		$entry["address2"] = $row["address2"];
	if ($row["state_name"] != null && $row["state_name"] != "")
		$entry["state_name"] = $row["state_name"];
	if ($row["zipcode"] != null && $row["zipcode"] != "")
		$entry["zipcode"] = $row["zipcode"];
	if ($row["phone1"] != null && $row["phone1"] != "")
		$entry["phone"] = $row["phone1"];
	if ($row["email"] != null && $row["email"] != "")
		$entry["email"] = $row["email"];
	if ($row["website"] != null && $row["website"] != "")
		$entry["website"] = $row["website"];
	foreach($attrs as $attr_name) {
		$val = $row[$attr_name];
		if (!$row[$attr_name])
			continue;

		if (in_array($attr_name, array("monday_from", "monday_to", "tuesday_from", "tuesday_to", "wednesday_from", "wednesday_to", "thursday_from", "thursday_to", "friday_from", "friday_to", "saturday_from", "saturday_to", "sunday_from", "sunday_to")))
//			$entry[$attr_name] = dir::convertHours($val);
			continue;
		else if ((substr($attr_name, -4) == "_fee") && $val == "|")
			continue;
		else {
			$val_text = get_attr_val($attr_name, $val);
			if ($val_text != NULL)
				$entry[$attr_name] = $val_text;
		}
	}
	$reviews = get_reviews("place", $row["place_id"]);
	if (!empty($reviews))
		$entry["reviews"] = $reviews;
	add_hours($row, $entry);
	$data[] = $entry;
}


//classifieds table
$classifieds = new classifieds();
$from_datetime = date("Y-m-d H:i:s", $from);
$res = $db->q("
SELECT c.id, c.type, c.ethnicity, c.email, c.website, c.title, c.date as created, c.content, c.thumb
	, c.incall, c.outcall, c.age, c.pornstar, c.haircolor, c.eyecolor, c.phone, c.firstname
	, c.avl_men, c.avl_women, c.avl_couple, c.measure_1, c.measure_2, c.measure_3, c.cupsize, c.penis_size, c.kitty, c.fetish, c.gfe 
	, c.build, c.height_feet, c.height_inches, c.weight, c.ter
	, (CASE WHEN c.deleted IS NOT NULL THEN 'delete' WHEN c.done = 0 THEN 'delete' ELSE 'new' END) AS delta_type
	, (CASE WHEN c.deleted IS NOT NULL THEN c.deleted WHEN c.done = 0 THEN UNIX_TIMESTAMP(c.expires) ELSE UNIX_TIMESTAMP(c.`date`) END) AS last_modified
	, cl.loc_id
	, l.loc_name as city_name, l.country_sub, l.loc_url, l.dir
	, lp.loc_type as parent_type, lp.loc_name as state_name
	, lc.loc_name as country_name
FROM classifieds c
INNER JOIN classifieds_loc cl on cl.post_id = c.id
INNER JOIN location_location l on l.loc_id = cl.loc_id
INNER JOIN location_location lp on lp.loc_id = l.loc_parent
INNER JOIN location_location lc on lc.loc_id = l.country_id
WHERE c.bp = 0
	AND (c.done = 1 OR c.done = 0) 
	AND (	(c.`date` >= FROM_UNIXTIME(?) AND c.deleted IS NULL) 
			OR (c.expires >= FROM_UNIXTIME(?) AND c.expires < NOW() AND c.done = 0 AND c.deleted IS NULL) 
			OR c.deleted >= ?)
	AND NOT (c.done = 0 AND c.`date` >= FROM_UNIXTIME(?) AND c.expires < NOW())
	AND c.thumb <> ''
GROUP BY c.id
ORDER by c.`date` ASC, c.expires ASC, c.deleted ASC
",
[$from, $from, $from, $from]
);
$adbuild_cupsize = array('1'=>'A', '2'=>'B', '3'=>'C', '4'=>'D', '5'=>'DD', '6'=>'DDD', '7'=>'Just Plain Huge');
$adbuild_penis_size = array('3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'Monster');
$adbuild_kitty = array('1'=>'Bald', '2'=>'Partially Shaved', '3'=>'Trimmed', '4'=>'Natural');
$adbuild_fetish = array('1'=>'Anything', '2'=>'24/7 (Total Power Exchange)', '3'=>'Age Play', '6'=>'Arse (Ass) Play', '7'=>'Asphyxiaphilia (Breath Play)', '84'=>'Biting',
'9'=>'Blindfolds', '10'=>'Blood', '11'=>'Body Hair', '12'=>'Bondage', '13'=>'Branding', '14'=>'Breast/Nipple Torture, Clamps, etc.', '16'=>'Candle Wax', '18'=>'Chains',
'17'=>'Chastity Devices', '19'=>'Chinese Balls/Ben Wa Balls/Anal Beads', '20'=>'Cling Film', '21'=>'Cock and Ball Torture, Clamps, etc.', '23'=>'Collar and Lead/Leash',
'15'=>'Confinement/Caging', '22'=>'Coprophilia (Shit Play)/Scat', '70'=>'Cross Dressing', '24'=>'Cupping (Suction of the Skin)', '25'=>'Dacryphilia (Arousal from Tears)',
'85'=>'Defilement, Seeing a Partner Dirty or Wet', '26'=>'Denim', '27'=>'Depilation/Shaving', '30'=>'Dildos (Handheld &amp; Strap-ons)', '29'=>'Discipline',
'47'=>'Doctor/Nurse Fetish', '31'=>'Domination', '32'=>'Ears', '33'=>'Exhibitionism/Sex in Public', '35'=>'Feathers', '72'=>'Fire Play', '36'=>'Fisting', '73'=>'Food 
Play', '37'=>'Gangbangs', '39'=>'Hair Pulling', '38'=>'Handcuffs/Shackles', '4'=>'High Heels', '74'=>'Humiliation', '28'=>'Infantilism/Diapers', '40'=>'Klismaphilia 
(Douching/Enema)', '5'=>'Knife Play', '41'=>'Lace/Lingerie', '75'=>'Lactation', '42'=>'Latex', '43'=>'Leather', '76'=>'Making Home "Movies"', '44'=>'Masks',
'45'=>'Masochism', '86'=>'Massage', '62'=>'Master/Slave', '8'=>'Masturbation', '87'=>'Mutual Masturbation', '77'=>'Needle Play', '46'=>'Nipples', '48'=>'Oral Fixation',
'34'=>'Pain', '78'=>'Pantyhose/Stockings', '79'=>'Participating in Erotic Photography', '52'=>'Piercings', '80'=>'Pinching', '50'=>'Play Piercing', '71'=>'Podophilia 
(Foot Fetish)', '53'=>'Power Exchange', '81'=>'Pussy and/or Cock Worship', '55'=>'Religious (Nunplay, Priestplay)', '56'=>'Retifism (Shoes or Boots)', '57'=>'Rimming',
'58'=>'Role Playing', '59'=>'Rubber', '60'=>'Sadism', '61'=>'Scent', '82'=>'Sensory Deprivation', '49'=>'Spanking/Paddling', '54'=>'The Rack/Medieval Devices',
'63'=>'Tongue Fetish', '64'=>'Toys', '65'=>'Transvestism', '66'=>'Urolagnia (Water Sports/Urine)', '67'=>'Vibrators', '68'=>'Voyeurism', '69'=>'Whips');
$adbuild_build = array('1'=>'Tiny', '2'=>'Slim/Slender', '3'=>'Athletic', '4'=>'Average', '5'=>'Curvy', '6'=>'BBW');
while ($row = $db->r($res)) {
	//var_dump($row);die;

	$city_name = $row["city_name"];
	$state_name = "";
	if ($row["parent_type"] == 2)
		$state_name = $row["state_name"];
	$country_name = $row["country_name"];

	$url = location::getUrlStatic($row["country_sub"], $row["loc_url"], $row["dir"]);
	$url .= "/".$classifieds->getModuleByType($row["type"])."/".$row["id"];

	$thumbnail_url = ($row["thumb"] != "") ? "{$config_image_server}/classifieds/".$row["thumb"] : "";

	//images
	$res2 = $db->q("SELECT filename FROM classifieds_image WHERE id = ? AND width > 0", array($row["id"]));
	$images = array();
	while ($row2 = $db->r($res2)) {
		$images[] = "{$config_image_server}/classifieds/t/{$row2["filename"]}";
	}

	$created = strtotime($row["created"]);
	$entry = array(
		"id" => "C".$row["id"],
		"delta_type" => $row["delta_type"],
		"source_type" => "classifieds",
		"last_modified" => date("c", $row["last_modified"]),
		"last_modified_unix" => $row["last_modified"],
		"href" => $url,
		"thumbnail_href" => $thumbnail_url,
		"images" => $images,
		"type" => $classifieds->getcatnamebytype($row["type"]),
		"city_name" => $city_name,
		"country_name" => $country_name,
		"email" => $row["email"],
		"title" => $row["title"],
		"content_free" => strip_tags($row["content"]),
		"age" => $row["age"],
		"pornstar" => yn($row["pornstar"]),
		"firstname" => $row["firstname"],
		"avl_men" => yn($row["avl_men"]),
		"avl_women" => yn($row["avl_women"]),
		"avl_couple" => yn($row["avl_couple"]),
		"gfe" => yn($row["gfe"]),
		);
	if ($state_name != null && $state_name != "")
		$entry["state_name"] = $state_name;
	if ($row["website"] != null && $row["website"] != "")
		$entry["website"] = $row["website"];
	if ($row["phone"] != null && $row["phone"] != "")
		$entry["phone"] = $row["phone"];
	if ($row['incall'] && $row['outcall']) {
		$entry["available"] = "Incall and Outcall";
	} else if ($row['incall']) {
		$entry["available"] = "Incall ONLY";
	} else if ($row['outcall']) {
		$entry["available"] = "Outcall ONLY";
	}
	$et = $classifieds->adbuild_ethnicity[$row["ethnicity"]];
	if ($et != null)
		$entry["ethnicity"] = $et;
	$hc = $classifieds->adbuild_haircolor[$row["haircolor"]];
	if ($hc != null)
		$entry["haircolor"] = $hc;
	$ec = $classifieds->adbuild_eyecolor[$row["eyecolor"]];
	if ($ec != null)
		$entry["eyecolor"] = $ec;
	$cu = $adbuild_cupsize[$row["cupsize"]];
	if ($cu != null)
		$entry["cupsize"] = $cu;
	$ps = $adbuild_penis_size[$row["penis_size"]];
	if ($ps != null)
		$entry["penis_size"] = $ps;
	$ki = $adbuild_kitty[$row["kitty"]];
	if ($ki != null)
		$entry["kitty"] = $ki;
	$fe = $adbuild_fetish[$row["fetish"]];
	if ($fe != null)
		$entry["fetish"] = $fe;
	if ($row["measure_1"] != 0)
		$entry["measure_1"] = $row["measure_1"];
	if ($row["measure_2"] != 0)
		$entry["measure_2"] = $row["measure_2"];
	if ($row["measure_3"] != 0)
		$entry["measure_3"] = $row["measure_3"];
	$bu = $adbuild_build[$row["build"]];
	if ($bu != null)
		$entry["build"] = $bu;
	if ($row["height_feet"] && $row["height_inches"])
		$entry["height"] = "{$row["height_feet"]}'{$row["height_inches"]}\"";
	if ($row["weight"])
		$entry["weight"] = $row["weight"];
	if ($row["ter"])
		$entry["ter"] = $row["ter"];
	$data[] = $entry;
}


//sort array by modified datetime
function cmpu($a, $b) {
	$a_u = $a["last_modified_unix"];
	$b_u = $b["last_modified_unix"];
    if ($a_u == $b_u) {
        return 0;
    }
    return ($a_u < $b_u) ? -1 : 1;
}
usort($data, "cmpu");

//limit output to predefined number of entries
$total = count($data);
$max = 1000;
if ($total > $max) {
	$last_modified_max = $data[$max-1]["last_modified_unix"];
	$ind = $max;
	while ($data[$ind]["last_modified_unix"] == $last_modified_max) {
		$ind++;
	}
	$data = array_slice($data, 0, $ind);
}
$count = count($data);

//final array
$output = array(
	"status" => "200",
	"total" => $total,
	"count" => $count,
	"data" => $data,
	);

file_log("feed", "output: status=200, total={$total} count={$count}");

//HTTP/JSON output
header('HTTP/1.1 200 OK', true, 200);
header('Content-Type: application/json; charset=UTF-8');
die(json_encode($output));
?>
