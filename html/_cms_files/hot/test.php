<?php
defined("_CMS_FRONTEND") or die("no access");

global $config_site_url;

if (!account::ensure_admin())
	die("Invalid access");

$url = $config_site_url."/hot/feed";

?>
<form action="" method="get">
<select id="timestamp" name="if_modified_since">
<option value="beginning">Beginning of time</option>
<option value="minus_3_year">-3 Years</option>
<option value="last_year">Last year</option>
<option value="last_month">Last month</option>
<option value="last_week">Last week</option>
<option value="custom">Custom</option>
</select>
<div id="custom" style="display: none;">
	<input type="text" name="custom_timestamp" value="2019-01-01" />
</div>
<input type="submit" name="submit" value="Submit" />
</form>
<script>
$(document).ready(function() {
	$('#timestamp').change(function() {
		console.log($('#timestamp').val());
		if ($('#timestamp').val() == 'custom') {
			$('#custom').show();
		} else {
			$('#custom').hide();
		}
	});
});
</script>
<?php

$if_modified_since = $_REQUEST["if_modified_since"];
if (!in_array($if_modified_since, array("beginning", "minus_3_year", "last_year", "last_month", "last_week", "custom"))) {
	return;
}

// If-Modified-Since: Mon, 29 Nov 2010 09:47:40 GMT
if ($if_modified_since == "beginning") {
	//$from_header = "Mon, 19 Jul 2010 07:00:00 GMT";//2010-07-19T09:00:00+02:00
	//$from_header = "Thu, 2 Jul 2015 20:01:17 GMT";//2015-07-02T22:01:17+02:00
	$from_header = "Thu, 1 Jan 1970 00:00:00 GMT";
} else {
	if ($if_modified_since == "minus_3_year")
		$date = strtotime("-3 year");
	else if ($if_modified_since == "last_year")
		$date = strtotime("-1 year");
	else if ($if_modified_since == "last_month") 
		$date = strtotime("-1 month");
	else if ($if_modified_since == "last_week") 
		$date = strtotime("-1 week");
	else if ($if_modified_since == "custom") {
		$date = $_REQUEST["custom_timestamp"];
		if (!preg_match('/^[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]$/', trim($date)))
			die("Invalid format of date, must be YYYY-MM-DD");
		$date = strtotime(trim($date));
	} else {
		return;
	}
	$from_header = date("D, j M Y", $date)." 00:00:00 GMT";
}

//$from_heder = "Sun, 1 Sep 2013 00:00:00 GMT";
$headers = "If-Modified-Since: {$from_header}";

echo "<h2>Request</h2>";
echo "URL: {$url}<br />";
echo "Headers: {$headers}<br />";

$start = microtime(true);

$ch=curl_init();
curl_setopt($ch,CURLOPT_URL,$url);
curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch,CURLOPT_HTTPHEADER, array($headers));
$result=curl_exec($ch);
echo "'".curl_error($ch)."'<br />\n";
if(curl_errno($ch)) {
	echo "CURL Error: ".curl_error($ch);
}
curl_close($ch);

$end = microtime(true);

$len_mb = number_format(strlen($result) / 1048576, 2);
echo "<h2>Response</h2>";
echo "Time = ".number_format($end - $start, 3)." s<br />\n";
echo "Length = {$len_mb} MB<br />\n";
//echo "Raw=<br /><pre>{$result}</pre><br />\n";
echo "<pre>".prettyPrint($result)."</pre><br />\n";

function prettyPrint($json) {

	$result = '';
	$level = 0;
	$prev_char = '';
	$in_quotes = false;
	$ends_line_level = NULL;
	$json_length = strlen( $json );

	for( $i = 0; $i < $json_length; $i++ ) {
		$char = $json[$i];
		$new_line_level = NULL;
		$post = "";
		if( $ends_line_level !== NULL ) {
			$new_line_level = $ends_line_level;
			$ends_line_level = NULL;
		}
		if( $char === '"' && $prev_char != '\\' ) {
			$in_quotes = !$in_quotes;
		} else if( ! $in_quotes ) {
			switch( $char ) {
				case '}': case ']':
					$level--;
					$ends_line_level = NULL;
					$new_line_level = $level;
					break;

				case '{': case '[':
					$level++;
				case ',':
					$ends_line_level = $level;
					break;

				case ':':
					$post = " ";
					break;

				case " ": case "\t": case "\n": case "\r":
					$char = "";
					$ends_line_level = $new_line_level;
					$new_line_level = NULL;
					break;
			}
		}
		if( $new_line_level !== NULL ) {
			$result .= "\n".str_repeat( "\t", $new_line_level );
		}

//		if ($char == "<")
//			$char = "&lt;";
//		else if ($char == ">")
//			$char = "&gt;";

		$result .= $char.$post;
		$prev_char = $char;
	}

	return $result;
}

?>
