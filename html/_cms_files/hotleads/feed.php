<?php
defined("_CMS_FRONTEND") or die("no access");

global $db;

require_once(_CMS_ABS_PATH."/_cms_files/classifieds/array.php");

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set("memory_limit","256M");


if (!isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
    header('HTTP/1.1 400 Bad Request', true, 400);
    header('Content-Type: application/json; charset=UTF-8');
    die(json_encode(array("status" => 400, "message" => "Bad Request; missing 'If-Modified-Since' header")));
}

//$ims = "Thu, 19 Sep 2013 00:00:00 GMT";
//$ims = "Mon, 28 Jan 2013 00:00:00 GMT";
$ims = $_SERVER['HTTP_IF_MODIFIED_SINCE'];


$dt = date_create_from_format("D, j M Y H:i:s e", $ims);
if ($dt === false) {
    header('HTTP/1.1 400 Bad Request', true, 400);
    header('Content-Type: application/json; charset=UTF-8');
    die(json_encode(array("status" => 400, "message" => "Bad Request; invalid value of 'If-Modified-Since' header, please use HTTP format of datetime")));
}

$from = $dt->getTimestamp();
if ($from === false || $from == -1) {
    header('HTTP/1.1 400 Bad Request', true, 400);
    header('Content-Type: application/json; charset=UTF-8');
    die(json_encode(array("status" => 400, "message" => "Bad Request; invalid value of 'If-Modified-Since' header, please use HTTP format of datetime")));
}


$classifieds = new classifieds();

$data = array();

$res = $db->q("SELECT c.*,
                    , (CASE WHEN c.deleted IS NOT NULL THEN 'delete' WHEN c.done = 0 THEN 'delete' ELSE 'new' END) AS delta_type
                    , (CASE WHEN c.deleted IS NOT NULL THEN c.deleted WHEN c.done = 0 THEN UNIX_TIMESTAMP(c.expires) ELSE UNIX_TIMESTAMP(c.`date`) END) AS last_modified
					, a.email as user_email, a.password as user_password
		        FROM classifieds c
        		LEFT JOIN account a on a.account_id = c.account_id
                WHERE (c.done = 1 OR c.done = 0) 
                        AND (   (c.`date` >= FROM_UNIXTIME(?) AND c.deleted IS NULL) 
                                OR (c.expires >= FROM_UNIXTIME(?) AND c.expires < NOW() AND c.done = 0 AND c.deleted IS NULL) 
                                OR c.deleted >= ?)
                        AND NOT (c.done = 0 AND c.`date` >= FROM_UNIXTIME(?) AND c.expires < NOW())
                ORDER by c.`date` ASC, c.expires ASC, c.deleted ASC",
    array($from, $from, $from, $from)
    );

while ($row = $db->r($res)) {

	$clad_id = $row["id"];
    $clad = clad::withRow($row);
    if (!$clad) {
		//TODO handle error ?
		continue;
    }

    //get classified ad location
    $res2 = $db->q("
        SELECT l.loc_name, lp.loc_name as parent_name, lc.loc_name as country_name
        FROM 
            (SELECT loc_id FROM classifieds_loc WHERE post_id = ? LIMIT 1) cl
        INNER JOIN location_location l on l.loc_id = cl.loc_id
        LEFT JOIN location_location lp on lp.loc_id = l.loc_parent
        LEFT JOIN location_location lc on lc.loc_id = l.country_id",
            array($clad_id));
    if ($db->numrows($res2) != 1) {
		//TODO handle error ?
		continue;
    }
    $row2 = $db->r($res2);
    $location_city = $row2["loc_name"];
    $location_state = $row2["parent_name"];
    $location_country = $row2["country_name"];
    if ($location_state == $location_country)
        $location_state = "";

	//get classified images
    $res3 = $db->q("SELECT * FROM classifieds_image WHERE id = ? ORDER BY image_id ASC", array($clad_id));
    $images = [];
    while ($row3 = $db->r($res3)) {
        $images[] = [
            "url" => "{$config_image_server}/classifieds/{$row3["filename"]}",
            "orientation" => $row3["x"],
            "width" => $row3["width"],
            "height" => $row3["height"],
            ];
    }

    //get last sale date
    $last_sale_date = null;
    $res4 = $db->q("SELECT transaction_time FROM account_purchase WHERE `what` = 'classifieds' AND item_id = ? and total > 0", [$clad_id]);
    if ($db->numrows($res4) > 0) {
        $row4 = $db->r($res4);
        $stamp = $row["transaction_time"];
        if ($stamp) {
            $last_sale_date = date("Y-m-d", $stamp);
        }
    }

    //some normalisation
    $type = $incall = $outcall = "";
    switch ($row["type"]) {
        case 2: $type = "ts"; break;
        case 1: $type = "fe"; break;
        default: $type = ""; break;
    }
	if ($row["incall"])
        $incall = "yes";
    else
        $incall = "no";
    if ($row["outcall"])
        $outcall = "yes";
    else
        $outcall = "no";

    $build = $adbuild_build[$row["build"]];
    $ethnicity = $adbuild_ethnicity[$row["ethnicity"]];
    $eye_color = $adbuild_eyecolor[$row["eyecolor"]];
    $hair_color = $adbuild_haircolor[$row["haircolor"]];
    $cupsize = $adbuild_cupsize[$row["cupsize"]];
    $penis_size = $adbuild_penis_size[$row["penis_size"]];
    $kitty = $adbuild_kitty[$row["kitty"]];

    $status = null;
    if ($clad->getDeleted()) {
        $status = "deleted";
    } else {
        switch($clad->getDone()) {
            case 0: $status = "expired"; break;
            case 1: $status = "live"; break;
            default: $status = "other"; break;
        }
    }

	$entry = [
        "delta_type" => $row["delta_type"],
        "last_modified" => date("c", $row["last_modified"]),
        "last_modified_unix" => $row["last_modified"],
        "id" => $row["id"],
        "account_id" => $row["account_id"],
        "user_email" => $row["user_email"],
        "user_password" => $row["user_password"],
        "location_city" => $location_city,
        "location_state" => $location_state,
        "location_country" => $location_country,
        "location_detail" => $row["location_detail"],
        "type" => $type,
        "phone" => $row["phone"],
        "email" => $row["email"],
        "website" => $row["website"],
        "firstname" => $row["firstname"],
        "title" => $row["title"],
        "content" => $row["content"],
        "avl_men" => $row["avl_men"],
        "avl_women" => $row["avl_women"],
        "avl_couple" => $row["avl_couple"],
        "incall" => $incall,
        "outcall" => $outcall,
        "age" => $row["age"],
        "build" => $build,
        "height_feet" => $row["height_feet"],
        "height_inches" => $row["height_inches"],
        "weight" => $row["weight"],
        "measure_1" => $row["measure_1"],
        "measure_2" => $row["measure_2"],
        "measure_3" => $row["measure_3"],
        "ethnicity" => $ethnicity,
        "eye_color" => $eye_color,
        "hair_color" => $hair_color,
        "cupsize" => $cupsize,
        "penis_size" => $penis_size,
        "kitty" => $kitty,
		"gfe" => $row["gfe"],
        "pornstar" => $row["pornstar"],
        "images" => $images,
        "status" => $status,
        "as_url" => $clad->getUrl(),
        "recurring" => $row["recurring"],
        "last_sale_date" => $last_sale_date,
        ];
    $data[] = $entry;
}

//sort array by modified datetime
function cmpu($a, $b) {
    $a_u = $a["last_modified_unix"];
    $b_u = $b["last_modified_unix"];
    if ($a_u == $b_u) {
        return 0;
    }
    return ($a_u < $b_u) ? -1 : 1;
}
usort($data, "cmpu");

//limit output to predefined number of entries
$total = count($data);
$max = 1000;
if ($total > $max) {
    $last_modified_max = $data[$max-1]["last_modified_unix"];
    $ind = $max;
    while ($data[$ind]["last_modified_unix"] == $last_modified_max) {
        $ind++;
    }
    $data = array_slice($data, 0, $ind);
}
$count = count($data);

//final array
$output = array(
    "status" => "200",
    "total" => $total,
    "count" => $count,
    "data" => $data,
    );

//HTTP/JSON output
header('HTTP/1.1 200 OK', true, 200);
header('Content-Type: application/json; charset=UTF-8');
die(json_encode($output));
?>
