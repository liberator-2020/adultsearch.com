<?php
//TODO obsolete controller -> delete
/**
 * Displays landing page and state/city page for domestic locations
 */
defined("_CMS_FRONTEND") or die("no access");

global $smarty, $db, $account, $gDescription, $gKeywords, $gTitle, $advertise, $mobile, $gModule, $ctx, $sphinx, $config_site_url;

session_write_close();

reportAdmin("AS: ./_cms_files/index.php called", "find out from where was this called");
system::go($config_site_url);
/*
require_once(_CMS_ABS_PATH."/_cms_files/classifieds/array.php");
$form = new form;

$core_state_id = $account->corestate($mobile == 1 ? 1 : 0);
if( $core_state_id ) { 
	if( $account->core_loc_id ) {
		$core_state_sql = "n.state_id = '$account->core_loc_id'";
		$core_news_sql = $account->core_loc_type == 3 ? ("n.loc_id in (".implode(',', $account->locAround()).")") : $core_state_sql;
		$core_state_name = $account->core_loc_array["loc_name"];
	}
	else {
		$core_state_sql = "n.state_id = '$core_state_id'"; 
		$core_state_name = $account->getcookie("core_state_name") . " ";
	}
} elseif( $account->core_loc_id && $account->core_loc_type < 3 ) {
	$core_state_sql = $core_news_sql = "n.state_id = '$account->core_loc_id'";
} else
	$core_state_sql = "1";

$loc_id = $account->findDefLoc();
if ($mobile == 1 && (!$loc_id || in_array($loc_id, array(16046, 16047, 41973))))  {
	$system = new system;
	$system->moved("/m/location");
	return;
} 

$loc_around = array();
if ($account->core_loc_id)
	$loc_around = $account->locAround();


//--------------------
if (!$loc_id || in_array($loc_id, [16046, 16047, 41973])) {
	//this is original place of display landing page code, but it has been moved to _cms_files/homepage.php
	//we should never get here anymore
	error_log("Error we got to original landing page code, HTTP_HOST='{$_SERVER["HTTP_HOST"]}', REQUEST_URI='{$_SERVER["REQUEST_URI"]}'");
	die;
}


$smarty->assign("page_type", "city");

//get location info
if( $loc_id && is_numeric($loc_id) ) {
	$res = $db->q("SELECT loc_name, s, loc_url, description, loc_lat, loc_long, image_filenames, video_html
					FROM location_location 
					WHERE loc_id = ?",
					array($loc_id));
	if ($db->numrows($res)) {
		$row = $db->r($res);
		$smarty->assign("core_state_real_name", "{$row[1]}");
		$smarty->assign("core_state_name", "{$row[0]}, {$row[1]}");
		if ($row["description"]) {
			$smarty->assign("location_description", $row["description"]);
			$smarty->assign("location_label", $row["loc_name"].", ".$row["s"]);
			if ($row["loc_lat"] && $row["loc_long"]) {
				$smarty->assign("location_lat", $row["loc_lat"]);
				$smarty->assign("location_lng", $row["loc_long"]);
			}
			$smarty->assign("location_video_html", $row["video_html"]);
		}
		if ($row["image_filenames"]) {
			$location_images = explode("|", $row["image_filenames"]);
			$smarty->assign("location_images", $location_images);
		}
	}
}

$curloc = $account->core_loc_type == 3 ? $account->core_loc_array['loc_parent'] : $account->core_loc_array['loc_id'];
$smarty->assign("curloc", $curloc);

$advertise->popup();

if ($account->core_loc_id && $account->core_loc_type == 3) 
	$smarty->assign("core_state_name", "{$account->core_loc_array['loc_name']}. See everything in <a href='{$account->core_loc_array['lp_loc_url']}' title='{$account->core_loc_array['lp_loc_name']} - Adult Search'>{$account->core_loc_array['lp_loc_name']}</a>");
elseif( $account->core_loc_id )
	$smarty->assign("core_state_name", $account->core_loc_array['loc_name']);

if ($account->core_loc_type == 2) {
	//$gTitle = "{$account->core_loc_array['loc_name']} Escorts, Strip Clubs, Massage Parlors and Sex Shops in {$account->core_loc_array['loc_name']}";
	$gTitle = "{$account->core_loc_array['loc_name']} Escorts, Strip Clubs, Massage Parlors and Sex Shops";
} else {
	//$gTitle = "{$account->core_loc_array['loc_name']} Escorts, Strip Clubs, Erotic Massage and Sex Shops in {$account->core_loc_array['lp_loc_name']}";
	$gTitle = "{$account->core_loc_array['loc_name']} Escorts, Strip Clubs, Erotic Massage and Sex Shops";
}

if( isset($account->core_loc_id) )
	$smarty->assign('loc_name', $account->core_loc_array['loc_name']);

include_html_head("css", "/css/city.css?20171119");

if ($mobile == 1) {
	$smarty->assign("link_back", "/m/location");
} else {
	$smarty->assign("link_back", "/homepage");
}

if ($account->country_id == 16046)
	$smarty->assign("us", true);
elseif ($account->country_id == 16047)
	$smarty->assign("ca", true);
elseif ($account->country_id == 41973)
	$smarty->assign("uk", true);


if ($account->core_loc_id) {
	if (!$account->core_state_id)
		$account->core_state_id = 16046;

	$sql = "SELECT * FROM home_cache WHERE state_id = '$account->core_loc_id'";
} else {
	$sql = "select * from home_cache where $core_state_sql";
}
$res = $db->q($sql);

$sclimit = 4;
if( $account->core_loc_id ) {
	$cities = array();
	$dont = false;
	while($row=$db->r($res)) {
		$bold = $row['cl1'] > 0 ? true : false;
		if( $row['sc5'] > 0 ) $sclimit = 3;
		if( !in_array($row['state_id'], $loc_around) ) {
			$cities[] = array("loc_name"=>$row['loc_name'], 'loc_url'=>$row['loc_url'], 'bold'=>$bold); 
			foreach($row as $key=>$value)
				if( !is_numeric($key) ) $first[$key] += $value;
			continue;
		}
		$cities[] = array("loc_name"=>$row['loc_name'], 'loc_url'=>$row['loc_url'], 'bold'=>$bold); 

		foreach($row as $key=>$value) {
			if (!is_numeric($key)) {
				$dont = true;
				$frm[$key] = $value;
			}
		}
	}

	if( isset($first) && !$dont ) {
		$frm = $first;
	}

	foreach($frm as $key=>$value) {
		$smarty->assign($key, $value);
	}

	$subcat_cols=2;
	if (!empty($cities)) {
		$loc_per_column = ceil(count($cities)/$subcat_cols);
		$loc_columns = array_chunk($cities, $loc_per_column);
		$smarty->assign("loc_columns", $loc_columns);
		$smarty->assign("state_name", "");
	}
}

$advertise->showAd(1);

if( $account->isloggedin() && $core_state_id ) {
	$forum = new forum();

	$tracked_topics = $forum->get_tracked_topics();
	$res = $db->q("
		SELECT f.forum_id forum_parent, f2.forum_id, t.topic_id, t.topic_last_post_time 
		FROM forum_topic t 
		INNER JOIN (
			forum_forum f 
			LEFT JOIN  forum_forum f2 on f.forum_parent = f2.forum_id
			) on t.forum_id = f.forum_id and t.loc_id = f.loc_id 
		WHERE t.topic_last_post_time > '{$_SESSION["last_visit"]}' and t.loc_id = '$core_state_id'");

	while($row=$db->r($res)) {

		if( !$row["forum_id"] ) $row["forum_id"] = $row["forum_parent"];

		$new = false;
		if ($row['topic_last_post_time'] > $_SESSION['last_visit'] 
			&& (empty($tracked_topics['forums'][$row['forum_id']])||$row['topic_last_post_time'] >$tracked_topics['forums'][$row['forum_id']])) {
			if ((empty($tracked_topics['topics'][$row["topic_id"]]) || $tracked_topics['topics'][$row["topic_id"]] < $row["topic_last_post_time"])
				&&(empty($tracked_topics['forums'][$row['forum_id']]) || $tracked_topics['forums'][$row['forum_id']] < $row["topic_last_post_time"])){
				$smarty->assign("forum_new".$row["forum_id"], true);
			}
		}
	}
}

$classifieds = new classifieds;

if (!isset($core_news_sql))
	$core_news_sql = $core_state_sql;

$sql_news = $core_news_sql;

if (empty($sql_news))
	$sql_news = "1";

$news = NULL;

$now = time();
$sql = "
(
select 'sponsor' as grp, m.images, l.country_sub, l.loc_url, c.type, c.id, c.thumb, c.available, c.incall, c.outcall, c.ethnicity 
from (
	select n.post_id, n.loc_id, n.images 
	from classifieds_sponsor n
	where n.expire_stamp > {$now} and n.done > 0 and n.type <> 16 and {$sql_news}
	group by n.post_id, n.loc_id, n.images
	order by rand() limit 8
	) m
inner join classifieds c on m.post_id = c.id and c.deleted IS NULL and c.done = 1 
inner join location_location l on m.loc_id = l.loc_id 
)
union
(
select 'normal' as grp, '', l.country_sub, l.loc_url, c.type, c.id, c.thumb, c.available, c.incall, c.outcall, c.ethnicity 
from classifieds c 
inner join (classifieds_loc n inner join location_location l on n.loc_id = l.loc_id) on c.id = n.post_id
where c.deleted IS NULL and c.done = 1 and c.bp = 0 and c.total_loc < 5 and c.type <> 16 and {$sql_news} 
order by c.`date` desc 
limit 8
)
limit 8";
//_d("'{$sql}'");

$res = $db->q($sql);

$sponsor_cnt = 0;
while($row=$db->r($res)) {
	$realavatar = true;
	switch($row["what"]) {
		case 'new': $realavatar = false; if( isset($types[$row["type"]]) ) $what = "New ".$types[$row["type"]]; else $what = "New Classified Ad"; break;
		case 'update': $realavatar = false; if( isset($types[$row["type"]]) ) $what = "Updated ".$types[$row["type"]]; else $what = "Updated Classified Ad"; break;
		case 'review': if( isset($types_r[$row["module"]]) ) $what = "New ".$types_r[$row["module"]]; else $what = "New Review"; break;
		case 'comment': if( isset($types_r[$row["module"]]) ) $what = "Comment on ".$types_r[$row["module"]]; else $what = "New Comment"; break;
		case 'ftopic': $what = "New Discussion Forum"; break;
		case 'fpost': $what = "New Forum Message"; break;
		default: $what = $row["what"];
	}

	if (!$realavatar && $row["haspic"])
		$row["avatar"] = "{$config_image_server}/classifieds/{$row["item_id"]}.jpg";
	elseif (!empty($row["avatar"]))
		$row["avatar"] = "{$config_image_server}/avatar/".$row["avatar"];
	else
		$row["avatar"] = $config_site_url."/images/icons/User_1.jpg";

	if (!empty($row['images']))
		$row["avatar"] = "{$config_image_server}/classifieds/s/{$row["images"]}";
	else if (!empty($row['thumb']))
		$row["avatar"] = "{$config_image_server}/classifieds/{$row["thumb"]}";

	switch($row['type']) {
		case 1: $module = "Female Escorts"; break;
		case 2: $module = "TS/TV Escorts"; break;
		case 6: $module = "Body Rubs"; break;
		case 5: $module = "Escort Agency"; break;
		case 13: $module = "BDSM / Fetish"; break;
		case 14: $module = "Adult Help Wanted"; break;
		case 3: $module = "Male For Female"; break;
		case 4: $module = "Male For Male Escorts"; break;
		case 7: $module = "Male For Male Body Rubs"; break;
		case 16: $module = "Phone And Web"; break;
	}

	//$info1 = $row['available']==1?"Incall":($row['available']==2?"Outcall":"In/Out Call");
	$info1 = "";
	if ($row["incall"] > 0)
		$info1 = "Incall";
	if ($row["outcall"] > 0)
		$info1 .= (empty($info1)) ? "Outcall" : "/Outcall";

	$info2 = $form->arrayValue($ethnicity_array, $row['ethnicity']);

	$info = $info1 . (!empty($info1)&&!empty($info2)?"<br />":"") . $info2;

	$cat = $classifieds->getModuleByType($row['type']);
	$link = isset($row['link']) && !empty($row['link']) ? $row['link'] : location::getUrlStatic($row["country_sub"], $row["loc_url"])."{$cat}/{$row['id']}";

	$news[] = array(
		"grp" => $row["grp"],
		"state_name" => $row["state_name"],
		"category" => $module,
		"avatar" => $row["avatar"],
		"link" => htmlspecialchars($link),
		"what" => $what,
		"info" => $info
	);

	if ($row["grp"] == "sponsor")
		$sponsor_cnt++;
}

//mark skype 3.7.2015 - put available image when 5 or 6 thumbnails are real sponsored city thumbnails
if ($sponsor_cnt == 5 || $sponsor_cnt == 6) {
	$news2 = array();
	foreach ($news as $item) {
		if ($item["grp"] == "sponsor")
			$news2[] = $item;
		else
			$news2[] = array(
				"grp" => "available",
				"state_name" => "",
				"category" => "",
				"avatar" => "/images/icons/city_thumbnail_available.jpg",
				"link" => $config_site_url."/adbuild/",
				"what" => "",
				"info" => ""
			);
	}
	$news = $news2;
}

//dallas skype 2.6.2015 - increase city thumbnails from 5 to 7
//dallas 16.11.2017 - increase from 7 to 8
$news2 = array();
if (count($news) > 5) {
	$chunks = array_chunk($news, 5);
	$news = $chunks[0];
	$news2 = $chunks[1];
}
$smarty->assign("news", $news);
$smarty->assign("news2", $news2);

$smarty->assign("bottHeader", true);

if( $mobile!=1 ) {

$sphinx->reset();
$sphinx->SetLimits( 0, 5, 5 );
$sphinx->SetFilter('edit', array(1,2,5));	//TODO why do strip clubs have edit value of 5 ?
if( $account->core_loc_id && $account->core_loc_type == 3) {
	$sphinx->SetFilter('loc_id', $loc_around);
} elseif( $account->core_loc_id ) {
	$sphinx->SetFilter('state_id', array($account->core_loc_id));
} else {
	$sphinx->SetFilter('state_id', array($core_state_id));
}
$sphinx->SetSortMode(SPH_SORT_EXTENDED, "last_review_id DESC");
$results = $sphinx->Query ( "", "eroticmassage" );
$result = $results["matches"];
$ids = NULL; 
foreach($result as $res) {$ids .= $ids ? (",".$res["id"]) : $res["id"];}
if( !is_null($ids) ) $res = $db->q("select e.id, e.name, e.thumb, e.review, e.recommend, l.country_sub, l.loc_url, date_format(r.reviewdate, '%m/%d/%Y') as reviewdate 
from eroticmassage e inner join 
location_location l using (loc_id) left join place_review r on (e.last_review_id = r.review_id and r.module = 'eroticmassage') where e.id in ($ids) order by 
field(e.id,$ids)");
$top = NULL;
while($row=$db->r($res)) {
	$av = $row["thumb"] ? "{$config_image_server}/eroticmassage/t/{$row["thumb"]}" : "{$config_site_url}/images/placeholder.gif";
	$rank = !$row['review']?"Not reviewed yet":($row['recommend'] . " positive reviews");
	$name2url = name2url($row['name']);
	$top[] = array(
		"id"=>$row['id'],
		"name"=>$row['name'],
		"thumb"=>$av,
		"link" => location::getUrlStatic($row["country_sub"], $row["loc_url"])."erotic-massage/{$name2url}/{$row['id']}",
		"rank"=>$rank,
		"time"=>$row['reviewdate'] ? ("on {$row['reviewdate']}") : ""
	);
}
$smarty->assign("popular_emp", $top);

$sphinx->SetLimits( 0, $sclimit, $sclimit );
$results = $sphinx->Query ( '', "stripclub" );
$result = $results["matches"];
$ids = NULL;
foreach($result as $res) {$ids .= $ids ? (",".$res["id"]) : $res["id"]; }
if( !is_null($ids) ) $res = $db->q("select s.id, s.name, s.thumb, s.review, s.recommend, l.country_sub, l.loc_url, date_format(r.reviewdate, '%m/%d/%Y') as reviewdate 
from strip_club2 s inner join location_location l using 
(loc_id) left join place_review r on (s.last_review_id = r.review_id and r.module = 'stripclub') where s.id 
in ($ids) order by field(s.id,$ids)");
$top = NULL;
while($row=$db->r($res)) {
	$av = $row["thumb"]?"{$config_image_server}/stripclub/t/{$row["thumb"]}":"{$config_site_url}/images/placeholder.gif";
	$rank = !$row['review']?"Not reviewed yet":($row['recommend'] . " positive reviews");
	$name2url = name2url($row['name']);
	$top[] = array(
		"id"=>$row['id'],
		"name"=>$row['name'],
		"thumb"=>$av,
		"link" => location::getUrlStatic($row["country_sub"], $row["loc_url"])."strip-clubs/$name2url/{$row['id']}",
		"rank"=>$rank,
		"time"=>$row['reviewdate'] ? ("on {$row['reviewdate']}") : ""
	);
}
$smarty->assign("popular_sc", $top);

$sphinx->SetLimits( 0, 5, 5);
$results = $sphinx->Query ( '', 'adultstore' );
$result = $results["matches"];
$ids = NULL; 
foreach($result as $res) {$ids .= $ids ? (",".$res["id"]) : $res["id"];}
if( !is_null($ids) ) $res = $db->q("select a.id, a.name, a.thumb, a.review, a.recommend, l.country_sub, l.loc_url, date_format(r.reviewdate, '%m/%d/%Y') as reviewdate 
from adult_stores a inner join location_location l 
using (loc_id) left join place_review r on (a.last_review_id = r.review_id and r.module = 'adultstore') where 
a.id in ($ids) order by field(a.id,$ids)");
$top = NULL;

while($row=$db->r($res)) {
	$av = $row["thumb"]?"{$config_image_server}adultstore/t/{$row["thumb"]}":"{$config_site_url}/images/placeholder.gif";
	$rank = !$row['review']?"Not reviewed yet":($row['recommend'] . " positive reviews");
	$name2url = name2url($row['name']);
	$top[] = array(
		"id"=>$row['id'],
		"name"=>$row['name'],
		"thumb"=>$av,
		"link" => location::getUrlStatic($row["country_sub"], $row["loc_url"])."sex-shops/$name2url/{$row['id']}",
		"rank"=>$rank,
		"time"=>$row['reviewdate'] ? ("on {$row['reviewdate']}") : ""
	);
}
$smarty->assign("popular_as", $top);

$results = $sphinx->Query ( '', 'lifestyle' );
$result = $results["matches"];
$ids = NULL; 
foreach($result as $res) {$ids .= $ids ? (",".$res["id"]) : $res["id"];}
if( !is_null($ids) )
	$res = $db->q("select a.id, a.type, a.name, a.thumb, a.review, a.recommend, l.country_sub, l.loc_url, date_format(r.reviewdate, '%m/%d/%Y') as reviewdate
					from lifestyle a
					inner join location_location l using (loc_id)
					left join place_review r on (a.last_review_id = r.review_id and r.module = 'lifestyle')
					where a.id in ($ids)
					order by field(a.id,$ids)
					LIMIT 3");
$top = NULL;
while($row=$db->r($res)) {
	$av = $row["thumb"]?"{$config_image_server}lifestyle/t/{$row["thumb"]}":"{$config_site_url}/images/placeholder.gif";
	$rank = !$row['review']?"Not reviewed yet":($row['recommend'] . " positive reviews");
	$name2url = name2url($row['name']);
	$cat = $row['type'] == 1 ? "swinger-clubs" : ($row['type'] == 2 ? "bdsm" : "topless-pools");
	$top[] = array(
		"id"=>$row['id'],
		"name"=>$row['name'],
		"thumb"=>$av,
		"link" => location::getUrlStatic($row["country_sub"], $row["loc_url"])."{$cat}/$name2url/{$row['id']}",
		"rank"=>$rank,
		"time"=>$row['reviewdate'] ? ("on {$row['reviewdate']}") : ""
	);
}
$smarty->assign("popular_ls", $top);

$results = $sphinx->Query ( '', 'gaybath' );
$result = $results["matches"];
$ids = NULL; 
foreach($result as $res) {$ids .= $ids ? (",".$res["id"]) : $res["id"];}
if( !is_null($ids) ) $res = $db->q("select a.id, a.name, a.thumb, a.review, a.recommend, l.country_sub, l.loc_url, date_format(r.reviewdate, '%m/%d/%Y') as 
reviewdate from gaybath a inner join location_location l 
using (loc_id) left join place_review r on (a.last_review_id = r.review_id and r.module = 'gaybath') where 
a.id in ($ids) order by field(a.id,$ids)");
$top = NULL;
while($row=$db->r($res)) {
	$av = $row["thumb"]?"{$config_image_server}gaybath/t/{$row["thumb"]}":"{$config_site_url}/images/placeholder.gif";
	$rank = !$row['review']?"Not reviewed yet":($row['recommend'] . " positive reviews");
	$name2url = name2url($row['name']);
	$top[] = array(
		"id"=>$row['id'],
		"name"=>$row['name'],
		"thumb"=>$av,
		"link" => location::getUrlStatic($row["country_sub"], $row["loc_url"])."gay-bath-houses/$name2url/{$row['id']}",
		"rank"=>$rank,
		"time"=>$row['reviewdate'] ? ("on {$row['reviewdate']}") : ""
	);
}
$smarty->assign("popular_gb", $top);

$results = $sphinx->Query ( '', 'gay' );
$result = $results["matches"];
$ids = NULL; 
foreach($result as $res) {$ids .= $ids ? (",".$res["id"]) : $res["id"];}
if( !is_null($ids) ) $res = $db->q("select a.id, a.name, a.thumb, a.review, a.recommend, l.country_sub, l.loc_url, date_format(r.reviewdate, '%m/%d/%Y') as 
reviewdate from gay a inner join location_location l 
using (loc_id) left join place_review r on (a.last_review_id = r.review_id and r.module = 'gay') where 
a.id in ($ids) order by field(a.id,$ids)");
$top = NULL;
while($row=$db->r($res)) {
	$av = $row["thumb"]?"{$config_image_server}gay/t/{$row["thumb"]}":"{$config_site_url}/images/placeholder.gif";
	$rank = !$row['review']?"Not reviewed yet":($row['recommend'] . " positive reviews");
	$name2url = name2url($row['name']);
	$top[] = array(
		"id"=>$row['id'],
		"name"=>$row['name'],
		"thumb"=>$av,
		"link" => location::getUrlStatic($row["country_sub"], $row["loc_url"])."gay/$name2url/{$row['id']}",
		"rank"=>$rank,
		"time"=>$row['reviewdate'] ? ("on {$row['reviewdate']}") : ""
	);
}
$smarty->assign("popular_gay", $top);

$results = $sphinx->Query ( '', 'brothel' );
$result = $results["matches"];
$ids = NULL; 
foreach($result as $res) {$ids .= $ids ? (",".$res["id"]) : $res["id"];}
if( !is_null($ids) ) $res = $db->q("select a.id, a.name, a.thumb, a.review, a.recommend, l.country_sub, l.loc_url, date_format(r.reviewdate, '%m/%d/%Y') as 
reviewdate from brothel a inner join location_location l 
using (loc_id) left join place_review r on (a.last_review_id = r.review_id and r.module = 'brothel') where 
a.id in ($ids) order by field(a.id,$ids)");
$top = NULL;
while($row=$db->r($res)) {
	$av = $row["thumb"]?"{$config_image_server}brothel/t/{$row["thumb"]}":"{$config_site_url}/images/placeholder.gif";
	$rank = !$row['review']?"Not reviewed yet":($row['recommend'] . " positive reviews");
	$name2url = name2url($row['name']);
	$top[] = array(
		"id"=>$row['id'],
		"name"=>$row['name'],
		"thumb"=>$av,
		"link" => location::getUrlStatic($row["country_sub"], $row["loc_url"])."brothels/$name2url/{$row['id']}",
		"rank"=>$rank,
		"time"=>$row['reviewdate'] ? ("on {$row['reviewdate']}") : ""
	);
}
$smarty->assign("popular_brothel", $top);

$results = $sphinx->Query ( '', 'lingeriemodeling1on1' );
$result = $results["matches"];
$ids = NULL; 
foreach($result as $res) {$ids .= $ids ? (",".$res["id"]) : $res["id"];}
if( !is_null($ids) ) $res = $db->q("select a.id, a.name, a.thumb, a.review, a.recommend, l.country_sub, l.loc_url, date_format(r.reviewdate, '%m/%d/%Y') as 
reviewdate from lingeriemodeling1on1 a inner join location_location l 
using (loc_id) left join place_review r on (a.last_review_id = r.review_id and r.module = 'lingeriemodeling1on1') where 
a.id in ($ids) order by field(a.id,$ids)");
$top = NULL;
while($row=$db->r($res)) {
	$av = $row["thumb"]?"{$config_image_server}lingeriemodeling1on1/t/{$row["thumb"]}":"{$config_site_url}/images/placeholder.gif";
	$rank = !$row['review']?"Not reviewed yet":($row['recommend'] . " positive reviews");
	$name2url = name2url($row['name']);
	$top[] = array(
		"id"=>$row['id'],
		"name"=>$row['name'],
		"thumb"=>$av,
		"link" => location::getUrlStatic($row["country_sub"], $row["loc_url"])."lingerie-modeling-1on1//$name2url/{$row['id']}",
		"rank"=>$rank,
		"time"=>$row['reviewdate'] ? ("on {$row['reviewdate']}") : ""
	);
}
$smarty->assign("popular_lm1on11", $top);
}

if (isset($account->core_loc_id))
	$smarty->assign('loc_name', $account->core_loc_array['loc_name']);

// city forum numbers tweak until a better solution for counties .. 
if( $ctx->county_id ) {
	//county
	$res = $db->q("SELECT forum_topics, forum_id  FROM `forum_forum` WHERE `loc_id` = '{$ctx->county_id}' group by forum_id");
} elseif( $ctx->location_type == 2 ) {
	//state so we need to sum the total topics in the parent locs
	$res = $db->q("SELECT sum(forum_topics), forum_id  FROM `forum_forum` WHERE `loc_parent` = '{$ctx->location_id}' group by forum_id");
} else { 
	//city
	$res = $db->q("SELECT sum(forum_topics), forum_id  FROM `forum_forum` WHERE `loc_id` = '{$ctx->location_id}' group by forum_id");
}

$forum = NULL;
while($row=$db->r($res)) {
	$forum_id = $row['forum_id'] < 6 ? $row['forum_id'] : 7;
	$forum[$forum_id] += $row[0];
}

foreach($forum as $key=>$val) {
	$smarty->assign("frm{$key}", $val);
}

$file = $smarty->fetch(_CMS_ABS_PATH."/templates/cityhome.tpl");
echo $file;
*/
?>
