<?php
/**
 * Landing page for showing erotic massage parlors
 */
defined("_CMS_FRONTEND") or die("No Access");

global $config_site_url, $db, $mobile;


/**
 * @return string User ip address
 */
function getIp()
{
    $ip = isset($_REQUEST['ip']) ? $_REQUEST['ip'] : account::getRealIp();

    return preg_replace('/[^A-Fa-f0-9\.:]/', '', $ip);
}

/**
 * @param $latitude float
 * @param $longitude float
 * @param $placeTypeId integer
 * @return array
 */
function getClosestCityWithPlaces($latitude, $longitude, $placeTypeId)
{
    global $sphinx;

    if (empty($latitude) || empty($longitude))
		return null;

    $sphinxIndex = 'dir';
    $sphinx->reset();
    $sphinx->SetLimits(0, 1, 1048);

    // geo
    $sphinx->SetGeoAnchor('loc_lat', 'loc_long', deg2rad($latitude), deg2rad($longitude));
    $sphinx->SetFilterFloatRange('@geodist', 0, (1000000 * 1609.344));
    $sphinx->SetSortMode(SPH_SORT_EXTENDED, '@geodist ASC');

    // filters
    $sphinx->SetFilter('place_type_id', [$placeTypeId]);
    $sphinx->SetFilter('edit', [1]);

    $results = $sphinx->Query('', $sphinxIndex);

	if (!is_array($results) || !array_key_exists("matches", $results))
		return null;
	$match = array_shift($results["matches"]);
	if (!is_array($match) || !array_key_exists("attrs", $match))
		return null;
	$attrs = $match["attrs"];
	if (!is_array($attrs) || !array_key_exists("loc_id", $attrs))
		return null;
	$locId = intval($attrs["loc_id"]);
	if (!$locId)
		return null;
	$location = location::findOneById($locId);
	if (!$location)
		return null;
	$url = $location->getUrl()."/erotic-massage-parlor";

	return $url;
}


// Getting user geo location by ip
$ipAddress   = getIp();
$geoLocation = geolocation::getLocationByIp($ipAddress);
$latitude    = isset($geoLocation['latitude']) ? $geoLocation['latitude'] : null;
$longitude   = isset($geoLocation['longitude']) ? $geoLocation['longitude'] : null;

// Collecting escort profiles data
$goUrl = getClosestCityWithPlaces($latitude, $longitude, 15);

//log into analytics
$ga_campaign = (array_key_exists("utm_campaign", $_REQUEST)) ? $_REQUEST["utm_campaign"] : null;
$url = (array_key_exists("REQUEST_URI", $_SERVER)) ? $_SERVER["REQUEST_URI"] : null;
$referer = (array_key_exists("HTTP_REFERER", $_SERVER)) ? $_SERVER["HTTP_REFERER"] : null;
$db->q("INSERT INTO analytics 
        (stamp, day, ref, url, referer, ip, forward_url, mobile) 
        VALUES 
        (?, ?, ?, ?, ?, ?, ?, ?)",
        [time(), date("Y-m-d"), $ga_campaign, $url, $referer, $ipAddress, $goUrl, intval($mobile)]
        );

if (!$goUrl)
    system::go($config_site_url);

system::go($goUrl);
die;
?>
