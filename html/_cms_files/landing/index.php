<?php

defined('_CMS_FRONTEND') or die('No Access');

global $db, $smarty, $mobile, $config_site_url, $sphinx, $gTitle;

const AMOUNT_FOR_V_1 = 6;
const AMOUNT_FOR_V_2 = 1;

const TEMPLATE_NAME_V_1 = '/templates/landing/landing.tpl';
const TEMPLATE_NAME_V_2 = '/templates/landing/landingV2.tpl';

// Available Landing page ids
$availablePageIds = [1, 2, 3, 4, 5];

$landingV2Ids = [4, 5];

// Redirect to first Landing page if id is missing or not available
if (!isset($_REQUEST['id']) ||
    !in_array(intval($_REQUEST['id']), $availablePageIds)) {
    header('Location: ' . $config_site_url . '/landing/?id=1', true, '301');
}

$pageId = intval($_REQUEST['id']);

// Getting user geo location by ip
$ipAddress   = getIp();
$geoLocation = geolocation::getLocationByIp($ipAddress);
$latitude    = isset($geoLocation['latitude']) ? $geoLocation['latitude'] : null;
$longitude   = isset($geoLocation['longitude']) ? $geoLocation['longitude'] : null;

switch ($pageId) {
    case in_array($pageId, $landingV2Ids) :
        $classifiedsAmount = AMOUNT_FOR_V_2;
        $templateName      = TEMPLATE_NAME_V_2;
        $willBeDisplayed   = false;
        break;
    default :
        $classifiedsAmount = AMOUNT_FOR_V_1;
        $templateName      = TEMPLATE_NAME_V_1;
        $willBeDisplayed   = true;
        break;
}
// Collecting escort profiles data
$classifieds = getClassifieds($latitude, $longitude, $classifiedsAmount, $willBeDisplayed);
if (!$classifieds["escorts"])
	system::go("/");

$gTitle = 'Find A Local Escort In Your City!';
$smarty->assign('pageId', $pageId);
$smarty->assign('classifieds', $classifieds['escorts']);
$smarty->assign('urlCity', $classifieds['urlCity']);
$smarty->assign('urlFemaleEscorts', $classifieds['urlFemaleEscorts']);
$smarty->assign('nobanner', true);

$smarty->display(_CMS_ABS_PATH . $templateName);


/**
 * @return string User ip address
 */
function getIp()
{
    $ip = isset($_REQUEST['ip']) ? $_REQUEST['ip'] : account::getRealIp();

    return preg_replace('/[^A-Fa-f0-9\.:]/', '', $ip);
}


/**
 * @param $latitude              float
 * @param $longitude             float
 * @param $amount                integer
 * @param $willBeDisplayed       boolean
 *
 * @return array
 */
function getClassifieds($latitude, $longitude, $amount, $willBeDisplayed)
{
    global $sphinx;

    if (empty($latitude) || empty($longitude)) {
        return filterData([]);
    }

    $sphinxIndex = 'escorts';
    $sphinx->reset();
    $sphinx->SetLimits(0, $amount, 1048);

    // geo
    $sphinx->SetGeoAnchor('loc_lat', 'loc_long', deg2rad($latitude),
        deg2rad($longitude));
    $sphinx->SetFilterFloatRange('@geodist', 0, (1000000 * 1609.344));
    $sphinx->SetSortMode(SPH_SORT_EXTENDED, '@geodist ASC');

    if($willBeDisplayed) {
        // filters
        $sphinx->SetFilter('fullname', [''], true);
        $sphinx->SetFilter('photo', [''], true);
    }

    $results = $sphinx->Query('', $sphinxIndex);

    return filterData($results);
}

/**
 * @param $data array
 * @return array
 */
function filterData($data)
{
    global $config_image_server, $config_site_url;

    $classifiedsClass = new classifieds;
    $classifieds      = [
        'urlCity'          => '',
        'urlFemaleEscorts' => '',
        'escorts'          => [],
    ];

    if (!isset($data['matches']) || empty($data['matches'])) {
        $classifieds['urlCity'] = $config_site_url;
        $classifieds['urlFemaleEscorts'] = $config_site_url;
        return $classifieds;
    }

    foreach ($data['matches'] as $result) {
        $result   = $result['attrs'];
        $category = $classifiedsClass->getModuleByType($result['type']);

        // Base url (needs for resolving sub-domains)
        $baseUrl = location::getUrlStatic($result['country_sub'], $result['loc_url']);

        // Getting url to nearest escort location
        if (!$classifieds['urlCity'] || !$classifieds['urlFemaleEscort']) {
			$urlCity = rtrim(location::getUrlStatic($result['country_sub'], $result['loc_url']), "/");
            $classifieds['urlCity'] = $urlCity;
            $classifieds['urlFemaleEscorts'] = $urlCity."/female-escorts";
        }

        array_push($classifieds['escorts'], [
            'id'          => $result['cid'],
            'location'    => $result['location_name'],
            'name'        => $result['fullname'],
            'photo'       => $config_image_server . '/classifieds/' . $result['photo'],
            'profile_url' => $baseUrl . $category . '/' . $result['cid'],
        ]);
    }

    return $classifieds;
}
