<?php

defined("_CMS_FRONTEND") or die("No Access");

require_once _CMS_ABS_PATH."/_cms_files/lingerie-modeling-1on1/array.php";

global $ctx, $db, $smarty, $page, $account, $gTitle, $advertise, $gModule, $gLocation, $sphinx, $config_image_server;
$form = new form;

$location = location::findOneById($ctx->location_id);
if (!$location)
	system::go("/");
$smarty->assign("link_back", $location->getUrl());

$core_state_id = $account->corestate(1);
if( !$core_state_id ) return;

$loc_around = array();
if ($account->core_loc_id && $account->core_loc_type > 2)
    $loc_around = $account->locAround();

$adultmovie = isset($_GET["adultmovie"]) ? intval($_GET["adultmovie"]) : NULL;
$toyshow = isset($_GET["toyshow"]) ? intval($_GET["toyshow"]) : NULL;
$alcohol = isset($_GET["alcohol"]) ? intval($_GET["alcohol"]) : NULL;
$cover = isset($_GET["cover"]) ? intval($_GET["cover"]) : NULL;

$advertise->showAd(78, $core_state_id);

$loc_name = $account->core_loc_array['loc_name'];
$gTitle = "{$loc_name} Nude Modeling 1on1 - Lingerie Modeling";

$filter_link = array();
foreach($_GET as $key => $value) {
		if( $key == "ethx" ) {
				$filter["eth"][] = $value; $filter_link[] = array("id"=>$value,"type"=>"eth","name"=>"Ethnicity");
		} else if( in_array($key, array("query", "cover", "adulttoys", "alcohol", "adultmovie", "toyshow", "arcade", "gloryhole", "zipcode", "phone",
"loc_id")) && $value != "" ) {
				$skipfilter = 0;
				$ftype = $key;
				$valuex = $value == 2 ? "Yes" : ($value == 1 ? "No" : $value);
				if( $key == "cover" ) { $valuex = $valuex == -1 ? "Free" : "\$$valuex.00"; $key = "Entry Fee: $valuex"; $gTitlex .= " $valuex";}
				else if( $key == "adulttoys" ) { $key = "Adult Toys: $valuex"; $gTitlex .= " Adult Toys";}
				else if( $key == "alcohol" ) { $valuex = $form->arrayValue($bar_array, $value); $key = "Alcohol: $valuex"; $gTitlex .= " Alcohol: $valuex"; }
				else if( $key == "adultmovie" ) { $key = "Adult Movie"; $gTitlex .= " Adult Movie"; }
				else if( $key == "adultbooks" ) { $key = "Adult Books: $valuex"; $gTitlex .= " Adult Books";}
				else if( $key == "toyshow" ) { $key = "Toy Show"; $gTitlex .= " Toy Show"; }
				else if( $key == "gloryhole" ) { $key = "Glory Holes"; $gTitlex .= " Glory Holes";}
				else if( $key == "zipcode" ) {
						$key = "Zip Code: $valuex"; $skipfilter = 1;
						$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key, "link"=>"{$account->core_loc_array['loc_url']}$gModule/"); }
				else if( $key == "query" ) { $key = "'$valuex'"; }
				else if( $key == "phone" ) { $key = "Phone #: '$valuex'"; }
				else if( $key == "loc_id" ) { $key = $loc_name; }

				if( !$skipfilter ) {
						if( $key == "loc_id" ) $filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key, "link"=>"{$account->core_loc_array['loc_url']}$gModule/");
						else $filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key);
				}
		}
}

if( !empty($gLocation) && !strstr($gLocation, "zipcode") ) {
		$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$loc_name, "link"=>"http://$state_link.adultsearch.com/$gModule/");
}

if( $gTitlex ) {
		if( $zipcode ) $gTitle = "$zipcode$gTitlex - Nude Modeling 1on1";
		else $gTitle = "$loc_name$gTitlex - Nude Modeling 1on1";
}

$item_per_page = isset($_GET["ipp"]) ? intval($_GET["ipp"]) : 30;
if( $item_per_page > 60 ) $item_per_page = 20;
$start = (($page*$item_per_page)-$item_per_page);
if( $start < 0 ) $start = 0;

$sphinx_index = "lingeriemodeling1on1";
$sphinx->reset();
$sphinx->SetLimits( $start, $item_per_page, 2000 );

if( isset($_GET["zipcode"]) && !empty($_GET["zipcode"]) ) {
		$zipcode = preg_replace("/[^0-9a-z]/i","", $_GET["zipcode"]);
		$mile_in = intval($_GET["miles"]); if( !$mile_in ) $mile_in = 10;
		if( $mile_in == -1 ) $mile_in = 1000;
		$res = $db->q("select z.lat, z.lon, radians(z.lat) latt, radians(z.lon) longg, l.loc_parent from core_zip z inner join location_location l on z.loc_id = l.loc_id
where z.zip = '$zipcode' limit 1");
		if( $db->numrows($res) ) {
				$s = $db->r($res); 
				$lat_me = $s["lat"]; $lon_me = $s["lon"];
				$core_state_id = $s["loc_parent"];
				$latt = $s["latt"]; $long = $s["longg"];
				$sphinx->SetGeoAnchor('loc_lat', 'loc_long', $latt, $long);
				$sphinx->SetFilterFloatRange('@geodist', 0, ($mile_in*1609.344));
				$zipfound = 1;
		} else {
				$error = "Zip code($zipcode) could not find. Please make sure that you typed the correct zip code.";
		}
		$smarty->assign("miles", $mile_in);
		$smarty->assign("zipcode", $_GET["zipcode"]);
} elseif( $account->core_loc_array['loc_lat'] ) {
		$sphinx->SetGeoAnchor('loc_lat', 'loc_long', deg2rad($account->core_loc_array['loc_lat']), deg2rad($account->core_loc_array['loc_long']));
		$sphinx->SetFilterFloatRange('@geodist', 0, (500*1609.344));
		//$distance = 1;
}


$sphinx->SetFilter('state_id', array($core_state_id));
if( $account->core_loc_id && $account->core_loc_type > 2 ) $sphinx->SetFilter('loc_id', $loc_around);
elseif( $loc_id ) $sphinx->SetFilter('loc_id', array($loc_id));
if( $adultmovie ) $sphinx->SetFilter('adultmovie', array(1));
if( $toyshow ) $sphinx->SetFilter('toyshow', array(1));
if( $alcohol ) $sphinx->SetFilter('alcohol', array(1));
if( $cover ) $sphinx->SetFilter('cover', array($cover));

$query = "";
if( isset($_GET["phone"]) && !empty($_GET["phone"]) ) {
		$phone = preg_replace("/[^0-9\*]/", "", $_GET["phone"]);
		$query = $phone;
		//else $smarty->assign("err_phone", true);
}
if( isset($_GET["name"]) && !empty($_GET["name"]) ) {
		$name = GetGetParam("name");
		$query .= $query ? " $name" : "$name";
		$error = "No strip club found with the word: ".$_GET["name"];
		$smarty->assign("name", $_GET["name"]);
}

$orderway = isset($_GET["orderway"])&& intval($_GET["orderway"]) ? intval($_GET["orderway"]) : (($zipfound||$distance)?2:1);
$way = $orderway == 1 ? "DESC" : "ASC";

if($_GET["order"] == 'overall') { $order = "overall $way"; }
elseif($_GET["order"] == 'review') $order = "review $way";
elseif($_GET["order"] == "name") $order = "name $way";
elseif($_GET["order"] == "last_review_id") $order = "last_review_id $way";
elseif($_GET["order"] == "score") $order = "overall $way";
elseif($_GET["order"] == 'distance' || ($zipfound||$distance) ) { $order = "sponsor DESC, @geodist $way"; }
else $order = "last_review_id DESC";
$sphinx->SetSortMode(SPH_SORT_EXTENDED, $order);

if (!$account->isWorker())
	$sphinx->SetFilter('edit', array(1,2));

$results = $sphinx->Query ( $query, $sphinx_index );
$total = $results["total_found"];
$result = $results["matches"];

$sphinx->SetLimits( 0, 10, 10 );

if( $amenities ) $refine[] = array("name"=>"Amenities", "col"=>1, "alt"=>$amenities);
if( $payment ) $refine[] = array("name"=>"Payments", "col"=>1, "alt"=>$payment);

$category = NULL;

if( $total && $account->core_loc_type < 3 && !isset($zipfound)) {

		$sphinx->SetLimits( 0, 1000, 1000 );
		$sphinx->SetGroupBy( 'loc_id', SPH_GROUPBY_ATTR, '@group desc' );
		$results = $sphinx->Query ( $query, $sphinx_index );
		$rex = $results["matches"];
		$locs = array();
		foreach($rex as $res) {
				$locs[] = array(
						"loc_id"=>$res["attrs"]["loc_id"],
						"count"=>$res["attrs"]["@count"]
				);
		}
}

if( count($locs) ) {
		$loc_idx = "";
		foreach($locs as $loc)
				$loc_idx  .= $loc_idx ? (",".$loc["loc_id"]) : $loc["loc_id"];

		$res = $db->q("select loc_id, loc_name, loc_url from location_location where loc_id in ($loc_idx) order by loc_name");
		$loc_idx = array();
		while($row=$db->r($res)) {
				$count = 0;
				foreach($locs as $l) {
						if( $l["loc_id"] == $row["loc_id"] ) $count = $l["count"];
				}
				$loc_idx[] = array($row["loc_id"]=>$row["loc_name"]." ($count)" );
		$category[] = array("id"=>"City", "type"=>"Location", "name"=>$row["loc_name"], "c"=>$count,"link"=>"{$row['loc_url']}$gModule/", 
"title"=>"{$row["loc_name"]} Nude Modeling 1on1");
		}
		$locs = $form->select_add_select();
		$locs .= $form->select_add_array($loc_idx, isset($_GET["loc_id"])?$_GET["loc_id"]:"");
}

$refine = NULL;
if( $category ) $refine[] = array("name"=>"Location", "col"=>1, "alt"=>$category);

$category = NULL;
if( $total>1 && is_null($adultmovie) ) {
		$type = "adultmovie";
		$sphinx->SetFilter($type, array(1));
		$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
		$results = $sphinx->Query ( $query, $sphinx_index );
		$sphinx->ResetFilter($type);
		foreach($results["matches"] as $match) {
				$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"Adult Movie", "c"=>$match["attrs"]["@count"]);
		}
		if( !empty($results["matches"]) ) $gTitlex .= ", Adult Movie";
}
if( $total>1 && is_null($toyshow) ) {
		$type = "toyshow";
		$sphinx->SetFilter($type, array(1));
		$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
		$results = $sphinx->Query ( $query, $sphinx_index );
		$sphinx->ResetFilter($type);
		foreach($results["matches"] as $match) {
				$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>"Toy Show", "c"=>$match["attrs"]["@count"]);
		}
		if( !empty($results["matches"]) ) $gTitlex .= ", Toy Show";
}
if( $category ) $refine[] = array("name"=>"Amenities", "col"=>1, "alt"=>$category);

if( $total>1 && is_null($alcohol) ) {
		$type = "alcohol";
		$sphinx->SetFilter($type, array(1, 2));
		$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
		$results = $sphinx->Query ( $query, $sphinx_index );
		$sphinx->ResetFilter($type);
	$category = NULL;
		foreach($results["matches"] as $match) {
		$namex = $form->arrayValue($bar_array, $match["attrs"][$type]);
				$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
		}
		if( !empty($results["matches"]) ) $gTitlex .= ", Alcohol";
	if( $category ) $refine[] = array("name"=>"Alcohol", "col"=>2, "alt"=>$category);
}

if( $total>1 && is_null($cover) ) {
		$type = "cover";
		$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
		$results = $sphinx->Query ( $query, $sphinx_index );
	$category = NULL;
		foreach($results["matches"] as $match) {
		if( $match["attrs"][$type] == 0 ) continue;
		$namex = $match["attrs"][$type] == -1  ? "Free" : "\${$match["attrs"][$type]}.00";
				$category[] = array("id"=>$match["attrs"][$type], "type"=>$type, "name"=>$namex, "c"=>$match["attrs"]["@count"]);
		}
		if( !empty($results["matches"]) ) $gTitlex .= ", Entry Fee: \${$match["attrs"][$type]}.00";
	if( $category ) $refine[] = array("name"=>"Entry Fee", "col"=>2, "alt"=>$category);
}


$smarty->assign("refine", $refine);

if( isset($_GET["name"]) && !empty($_GET["name"]) ) {
	$name_sql = "and name like '%".GetGetParam("name")."%'";
	$smarty->assign("name", $_GET["name"]);
	$error = "No store found with the word: ".$_GET["name"];
}

foreach($result as $res) {
	$ids .= $ids ? (",".$res["id"]) : $res["id"];
}

if ($ids)
	$res = $db->q("SELECT s.*, l.loc_url, l.country_sub, r.comment rc, a2.avatar ra, a2.username as ru 
					FROM lingeriemodeling1on1 s 
					INNER JOIN location_location l on l.loc_id = s.loc_id
					LEFT JOIN (place_review r inner join account a2 using (account_id)) on s.last_review_id = r.review_id and r.module = 'lingeriemodeling1on1' 
					WHERE s.id in ($ids) 
					ORDER BY field(s.id,$ids)");
while($row=$db->r($res)) {

	if( $zipcode ) {
		if( !isset($lat_me) ) continue;
		if( $row["loc_lat"] && $row["loc_long"] ) {
			$mile = number_format(distance($lat_me, $lon_me, $row["loc_lat"], $row["loc_long"], "M"),1);
		} else $mile = "unknown";
	} elseif( $distance ) {
		$mile = number_format(distance($account->core_loc_array["loc_lat"], $account->core_loc_array["loc_long"], $row["loc_lat"], $row["loc_long"], "M"),1);
	}

	$link = dir::getPlaceLinkAux($row['country_sub'], $row["loc_id"], $gModule, $row["id"], $row['name'], trim($row['loc_url'], "/"));

	$lox_name = "{$row["loc_name"]}, {$row["state_name"]}";

	$thumbpath = !empty($row["thumb"]) ? "{$config_image_server}/lingeriemodeling1on1/t/{$row["thumb"]}" : "/images/placeholder.gif";
	$ra = "{$config_image_server}/avatar/" .  (!empty($row["ra"]) ? $row["ra"] : "no_avatar.jpg");
	$extra = $row["edit"] < 1 ? "<span class='nonerotic'>*Not Live*</span>" : NULL;

	$sc[] = array(
		"id"=>$row["id"],
		"name"=>$row["name"],"thumb"=>$row["thumb"],
		"address"=>$row["address1"],
		"phone"=>makeproperphonenumber($row["phone"]),
		"loc_name"=>$lox_name,
		"zipcode"=>$row["zipcode"],
		"distance"=>isset($zipcode)||$distance?(float)$mile:NULL,
		"review"=>$row["review"],
		"edit"=>$row["edit"],
		"linkoverride" => $link,
		"longitude"=>$row['loc_long'],
		"latitude"=>$row['loc_lat'],
		"thumbpath"=>$thumbpath,
		"lr"=>$row['last_review_id'],
		"rc"=>$row['rc'],
		"ra"=>$ra,
		"ru"=>$row['ru'],
		"extra"=>$extra
	);
} 

$json = json_encode($sc);
$smarty->assign("mapjson", $json);

$smarty->assign("sc", $sc);
$smarty->assign("total", $total);
$smarty->assign("phone1", $_GET["phone1"]);
$smarty->assign("phone2", $_GET["phone2"]);
$smarty->assign("phone3", $_GET["phone3"]);

if( isset($error) ) $smarty->assign("error", $error);

$query = _pagingQuery();
_pagingQueryFilter($query, $filter_link);
$linksort = _pagingQuery(array("order", "orderway"));
$smarty->assign("linksort", $linksort);
$smarty->assign("search_hidden", _pagingQuery(array("loc_id", "zipcode", "miles", "phone"), 1));
$smarty->assign("link", $query);
$smarty->assign("filter", $filter_link);

$dir = new dir;
$ctx->page = $page;
$smarty->assign("pager_array", $dir->get_pager_array($total, $item_per_page, $account->core_loc_array['loc_url']."/{$gModule}/", true));
$smarty->assign("paging", $db->paging($total, $item_per_page, yes, yes, $query));
$smarty->assign("loc_name", $locname);
$smarty->assign("gModule", $gModule);

$order = isset($_GET["order"]) ? $_GET["order"] : ($distance?"distance":"last_review_id");
if( !isset($_GET["orderway"]) && $order == "last_review_id" ) $orderway = 1;
$sort = array();
if( isset($zipfound) || isset($distance)) $sort[] = array("key"=> "Distance", "value"=>"distance", "way"=>$order == "distance" && $orderway == 2 ? 1 : 2,
"selected"=>$order == "distance" ?true : false);
$sort[] = array("key"=> "Rating", "value"=>"overall", "way"=>$order == "overall" && $orderway == 1 ? 2 : 1, "selected"=>$order == "overall" ? true : false);
$sort[] = array("key"=> "Number of Reviews", "value"=>"review", "way"=>$order == "review" && $orderway == 1 ? 2 : 1, "selected"=> $order == "review"? true : false);
$sort[] = array("key"=> "Store Recently Reviewed", "value"=>"last_review_id", "way"=>$order == "last_review_id" && $orderway == 1 ? 2 : 1, "selected"=>$order =="last_review_id" ?true : false);
$smarty->assign("sort", $sort);

$smarty->assign("category", "lingerie-modeling-1on1");
$smarty->assign("what", "Lingerie Modeling");
$smarty->assign("worker_link", "lingeriemodeling1on1");
$smarty->assign("image_path", "lingeriemodeling1on1");
$smarty->assign("place_link", "place");
$smarty->assign("imageserver", $config_image_server."/lingeriemodeling1on1/");
$smarty->assign("rss_available", true);

$file = $smarty->fetch(_CMS_ABS_PATH."/templates/place/place_list.tpl");
echo $file;

?>
