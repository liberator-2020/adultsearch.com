<?php
defined("_CMS_FRONTEND") or die("No Access");

require_once (_CMS_ABS_PATH."/_cms_files/lingerie-modeling-1on1/array.php");

global $db, $smarty, $account, $page, $advertise, $gTitle, $gModule, $gLocation, $gItemID, $config_mapquest_key, $config_image_server;
$form = new form;

$id = $gItemID;
$account_id = $account->isloggedin();

$review = new review("lingeriemodeling1on1", $id, "lingeriemodeling1on1", "", "lingeriemodeling1on1");

$res = $db->q("SELECT l.*, l2.loc_url, l2.country_sub 
				FROM lingeriemodeling1on1 l 
				LEFT JOIN location_location l2 on l2.loc_id = l.loc_id 
				WHERE l.id = '$id'");
if( !$db->numrows($res) ) {
	if( empty($account->core_loc_array['loc_url']) ) $account->core_loc_array['loc_url'] = "/";
	system::moved("{$account->core_loc_array['loc_url']}{$gModule}/");
}
$row = $db->r($res); 
$name2url = name2url($row['name']);

if ($account->core_sub != $row["country_sub"] || !strstr($_SERVER['REQUEST_URI'], $row['loc_url']) || $gLocation != $name2url)
	system::moved(location::getUrlStatic($row["country_sub"], $row["loc_url"])."{$gModule}/{$name2url}/{$row['id']}");

$smarty->assign('breadplace', $row['name']);
$advertise->showad(78, $row["loc_id"]);

$gTitle = "{$row["name"]} - {$row["loc_name"]}, {$row["state_name"]} {$row["zipcode"]}";
if (!empty($row['phone']))
	$gTitle .= " | ".makeproperphonenumber($row['phone']);
if ($row["review"])
	$gTitle .= " | Total Review: {$row["review"]}";

//map
$map = prepareMap($id, 'lingeriemodeling1on1', $row, 'lingeriemodeling1on1', 'lingeriemodeling1on1');
$smarty->assign("map", $map);
$smarty->assign("link_map_update", "/map?id={$id}&section=lingeriemodeling1on1&update=1");

$quickread = $review->reviews();
$smarty->assign("quickread", $quickread);

$loc = "<a href='{$account->core_loc_array['loc_url']}$gModule/' title='{$row["loc_name"]} {$row["state_name"]} Nude Modeling 1on1'>{$row["loc_name"]}</a>, {$row["state_name"]} {$row["zipcode"]}";
$smarty->assign("link_back", "{$account->core_loc_array["loc_url"]}$gModule/");

$imagedir = "lingeriemodeling1on1";
$photos = array();
if ($row["image"] != "" && $row["thumb"] != "") {
	$photos["image"] = "{$config_image_server}/{$imagedir}/{$row["image"]}";
	$photos["thumb"] = "{$config_image_server}/{$imagedir}/t/{$row["thumb"]}";

	$pics = array();
	$rex = $db->q("select picture, filename, thumb from place_picture where id = '$id' and module = 'lingeriemodeling1on1'");
	while($rox=$db->r($rex)) {
		$pics[] = array(
			"module" => "lingeriemodeling1on1",
			"id" => $rox["picture"],
			"image" => "{$config_image_server}/{$imagedir}/".$rox["filename"],
			"thumb" => "{$config_image_server}/{$imagedir}/t/".$rox["thumb"],
			);
	} 

	if ($pics) {
		include_html_head("js", "/js/jquery.tools.scrollable.js");
		$pics_in_group = 2;
		$pics_groups = array_chunk($pics, $pics_in_group);
		$photos["pics"]  = $pics_groups;
	}
}
$smarty->assign("photos", $photos);

$smarty->assign("bar_fee", $row["bar_fee"]);
$smarty->assign("girl_fee", $row["girl_fee"]);

if( $row["web_url"] ) {
	$web = parse_url($row["web_url"]);
	$smarty->assign("web_url", $row["web_url"]);
	$smarty->assign("web_host", $web["host"]);
}

$phone = !empty($row["phone"]) ? (makeproperphonenumber($row["phone"]) . (!empty($row["phone2"]) ? (" or {$row["phone2"]}" . (!empty($row["phone3"]) ? (" or {$row["phone3"]}") : "")): "")) : "";

$smarty->assign("category", "lingerie-modeling-1on1");
$smarty->assign("name", $row["name"]);
$smarty->assign("loc_name", $row["loc_name"]);
$smarty->assign("id", $id);
$smarty->assign("totalreview", $row["review"]);
$smarty->assign("paging", $db->paging($row["review"], 10, false, true, "?id={$id}"));
$smarty->assign("gModule", $gModule);
$smarty->assign("what", "Nude Modeling 1 on 1");
$smarty->assign("overall", $row["overall"]);

global $ctx;
$ctx->item = $row;
$smarty->assign("hours", dir::getHours(($row["byapponly"] == 1) ? true : false));
$smarty->assign("current_weekday", strtolower(date("l")));

if( $row["cover"]>0 )
	$c1[] = array("label" => "Entry Fee", "val" => "\${$row["cover"]}.00");
if( $row["bar_service"] )
	$c1[] = array("label" => "Alcohol", "val" => $form->arrayValue($bar_array, $row["bar_service"]));
$adultmovie = $row["adultmovie"] == 2 ? "Yes" : "No";
$c1[] = array("label" => "Adult movies in the room", "val" => "{$adultmovie}");
$toyshow = $row["toyshow"] == 2 ? "Yes" : "No";
$c1[] = array("label" => "Toy Shows", "val" => "{$toyshow}");

if( !empty($row["other_services"]) )
	$smarty->assign("info", $row["other_services"]);

if( !empty($row["coupon"]) )
	$c1[] = array("label" => "Coupon", "val" => "<a href='coupon?coupon={$row["coupon"]}' title='coupon' target='_blank' rel='nofollow'>Click to use</a>");

$smarty->assign("c1", $c1);

if( $row["website"] ) {
	$row["website"] = trim($row["website"]);
	if( strncmp($row["website"], "http://", 7) )  $row["website"] = "http://" . $row["website"];
	$web = parse_url($row["website"]);
	$web_url = $row["website"];
	$web_host = $web["host"];
}

$addressLx = "<b>{$phone}</b><br />{$row["address1"]} {$row["address2"]}<br/>{$loc}<br/>";
if ($web_url)
	$addressLx .= "<a href=\"$web_url\" target=\"_blank\" rel=\"nofollow\" class=\"website\">$web_host</a><br/>";
$smarty->assign("address", $addressLx);

$core_state_name = "<a href='{$account->core_loc_array['loc_url']}$gModule/' title='{$row["loc_name"]}'>{$row["loc_name"]} Nude Modeling 1on1</a>";
$smarty->assign("core_state_name", $core_state_name);

//MapQuest and Leaflet include
include_html_head('js', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.js');
include_html_head('css', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.css');
include_html_head('js', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.js');
include_html_head('css', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.css');

include_html_head("css", "/css/info.css");

if (empty($row["owner"])) {
	$smarty->assign("business_link", true);
	$smarty->assign("business_shortcut", "?section=lingerie&id=$gItemID");
}

/*
//forum
$forum_id = 153;
$smarty->assign("forum_topic", forum::getLastTopicsForPlace($forum_id, $id));
$smarty->assign("new_topic_link", forum::getNewTopicLinkForCategory($forum_id, $id));
*/

$smarty->assign("editlink", "lingeriemodeling1on1");

$smarty->display(_CMS_ABS_PATH."/templates/place/place_place.tpl");

?>
