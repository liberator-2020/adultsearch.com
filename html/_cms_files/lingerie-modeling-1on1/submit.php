<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $page;
$system = new system;

require_once(_CMS_ABS_PATH."/_cms_files/lingerie-modeling-1on1/array.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");

$form = new data;

$form->table = "lingeriemodeling1on1";

$c = new CColumnId("id", "ID");
$form->AppendColumn($c);

$c = new CColumnString("name", "Name");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnLocation("City", "Location", 3);
$c->setLocName("loc_name", "loc_name", "City");
$c->setLocName("state_name", "s", "City");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("address1", "Address");
$form->AppendColumn($c);

$c = new CColumnString("address2", "Floor/Unit");
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "Zipcode");
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Club Phone");
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnString("email", "E-Mail");
$form->AppendColumn($c);

$c = new CColumnHour("hours", "Hours of Operation:");
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setBackup("lingeriemodeling1on1");
$c->setPath("lingeriemodeling1on1");
$c->setWatermark();
$form->AppendColumn($c);

$c = new CColumnEnum("bar_service", "Serve alcohol ?");
foreach($bar_array as $key=>$value) $c->AddEnumOption($key, $value);
$form->AppendColumn($c);

$c = new CColumnEnum("adultmovie", "Adult movies in the room");
$c->AddEnumOption("1", "Yes");
$c->AddEnumOption("0", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("toyshow", "Toy Shows");
$c->AddEnumOption("1", "Yes");
$c->AddEnumOption("0", "No");
$form->AppendColumn($c);

$c = new CColumnText("other_services", "Additional Information");
$form->AppendColumn($c);

$form->visitor_mode = true;
$form->visitor_url = "/lingerie-modeling-1on1/";
$form->admin_url = "/worker/lingeriemodeling1on1";

$form->ShowPage();

?>
