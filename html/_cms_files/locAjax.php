<?php

global $db;

$_REQUEST["template"] = "blank.tpl";

$action		= GetGetParam('action');
$select_name	= GetGetParam('select_name');
$parent_name	= GetGetParam('parent_name');
$parent_id	= GetGetParam('parent_id');
$class = GetGetPAram('class');
$city_id = GetGetParam('city_id');

$mpOnly = intval($_GET["mponly"]);
$mponlyjs = $mpOnly ? "&mponly=1" : "";

$jscript1 = "''";
$jscript2 = "''";

$city_select = isset($_GET["city_select"])  ? $_GET["city_select"] : "city_select";

$classselect = !empty($class) ? "class=\"$class\"" : "";
$classjs = !empty($class) ?  "&class=$class" : "";

$city_idselect = !empty($city_id) ? "id=\"$city_id\"" : "";
$city_idjs = !empty($city_id) ? "&city_id=$city_id" : "";

if (!empty($parent_name)) {
	$parent_name = strtoupper($parent_name);
	$res = $db->q("SELECT loc_id FROM location_location WHERE loc_id = '{$parent_name}'");
	if (!$db->numrows($res)) {
		echo "<select name=\"{$select_name}\"><option value=\"\">-- Select --</option></select>";
		die;
	}
	$SQLresult = $db->r($res);
	$parent_id = $SQLresult["loc_id"];
}

if (empty($parent_id)) {
	//echo "<select name=\"{$select_name}\" $classselect><option value=\"\">-- Select --</option></select>";
	die;
}

if( $mpOnly )
	$res = $db->q("SELECT DISTINCT ml.`loc_id`, l.loc_name FROM `mp_loc` ml inner join location_location l using (loc_id) WHERE ml.`loc_parent` = '{$parent_id}' order by loc_name");
else
	$res = $db->q("SELECT loc_id, loc_name FROM location_location WHERE loc_parent = '{$parent_id}' ORDER BY loc_name ASC");
if ($db->numrows($res)) {
	//$jscript1 .= "var loc_ids = [";
	//$jscript2 .= "var loc_names = [";
	$jscript1 = "[";
	$jscript2 = "[";
	$i = 0;
	$alt_location = strtoupper($select_name) == "STATE" ? "-- states --" : "-- cities --";
	$option = "<option value=\"\">$alt_location</option>";
	while($SQLrow=$db->r($res)) {
		list($loc_id, $loc_name) = $SQLrow;
		if ($i != 0) {
			$jscript1 .= ",";
			$jscript2 .= ",";
		}
		$jscript1 .= "'".addslashes($loc_id)."'";
		$jscript2 .= "'".addslashes($loc_name)."'";
		$i++;
		$selected = (strtoupper($select_name) == "STATE" && $loc_id == $_GET["State"]) ? "selected=\"selected\"" : 
			((strtoupper($select_name) == "CITY" && $loc_id == $_GET["City"]) ? "selected=\"selected\"" : "");
		$option .= "<option value=\"{$loc_id}\" $selected>{$loc_name}</option>";
	}
	$jscript1 .= "]";
	$jscript2 .= "]";
} else
	die;

$onChange = isset($_GET["onchange"]) ? "{$_GET["onchange"]}(this.options[this.selectedIndex].value);" : "";
$onChangeJs = $onChange ? "&onchange={$_GET["onchange"]}" : "";

if (strtoupper($select_name) == "STATE") {
	if( isset($_GET["onChangeCity"]) )
		$onChangeCityJs = "&onChangeCity=".$_GET["onChangeCity"];
	else
		$onChangeCityJs = "";
	$state_select = "onchange=\"$('{$city_select}').html(''); _ajax('get','/locAjax.htm', 'parent_name='+this.options[this.selectedIndex].value+'&select_name=City$mponlyjs$classjs$city_idjs$onChangeCityJs$onChangeJs', '{$city_select}');$onChange\"";
	$city_idselect = "";
} else {
	if( isset($_GET["onChangeCity"]) )
		$state_select = "onchange=\"".$_GET["onChangeCity"] ."(this.options[this.selectedIndex].value); $onChange;return false;\"";
	else
		$state_select = "onchange=\"$onChange return false;\"";
}

$onChangeCity = ""; 
echo "<select name=\"{$select_name}\" id=\"{$select_name}\" $state_select $classselect $city_idselect $onChangeCity>{$option}</select>";
die;

?>
