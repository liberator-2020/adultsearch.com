<?php

global $config_site_url;

$ip_address = null;
if (isset($_REQUEST["ip"]))
    $ip_address = preg_replace('/[^A-Fa-f0-9\.:]/', '', $_REQUEST["ip"]);
else
    $ip_address = preg_replace('/[^A-Fa-f0-9\.:]/', '', account:getRealIp());

if (!$ip_address)
	system::go($config_site_url);

$loc = geolocation::getClosestNonEmptyLocationByIp($ip_address);

if (!$loc)
	system::go($config_site_url);

system::go($loc->getUrl());

?>
