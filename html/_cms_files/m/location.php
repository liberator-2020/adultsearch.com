<?php
defined("_CMS_FRONTEND") or die("No Access");

global $smarty, $gTitle, $mobile;

if ($mobile != 1)
	system::go("/");

$gTitle = "Choose Your Location";

$smarty->assign("main_groups", location::getHomeCities(true, false));
$smarty->assign("no_change", true);
$smarty->display(_CMS_ABS_PATH."/templates/mobile/location/location_home_mobile.tpl");
?>
