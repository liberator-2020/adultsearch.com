<?php

defined("_CMS_FRONTEND") or die("No Access");

require_once _CMS_ABS_PATH."/_cms_files/worker/array_sc.php";

global $db, $smarty, $page, $account, $gTitle, $mobile, $gModule, $gDescription;
$form = new form;

$core_state_id = $account->corestate(1);
if (!$core_state_id)
	return;

if( stristr($gModule, 'male-revues') ) {
    $type = 1;
} elseif( stristr($gModule, 'male-gay-revues') ) {
    $type = 2;
} else
	$type = 1;

switch ($type) {
    case 1:
        $cat_name = "Male Revue";
        $gTitle = $account->core_loc_array['loc_name'] . " Male Revues and Reviews {$account->core_loc_array['lp_loc_name']}";
		$gDescription = "Male Revues in {$account->core_loc_array['loc_name']} for Chippendales bachelorette parties male strippers and a wild girls night out on the town. Complete list of male revues in {$account->core_loc_array['loc_name']}.";
        break;
    case 2:
        $cat_name = "Male Gay Revue";
        $gTitle = $account->core_loc_array['loc_name'] . " Male Gay Revues and Reviews {$account->core_loc_array['lp_loc_name']}";
		$gDescription = "Male Gay Revues in {$account->core_loc_array['loc_name']}. Complete list of male gay revues in {$account->core_loc_array['loc_name']}.";
        break;
}

$loc_id = isset($_GET["loc_id"])&&intval($_GET["loc_id"])?intval($_GET["loc_id"]):NULL;
$loc_id_sql = $loc_id ? "and loc_id = '$loc_id'" : "";

if( isset($_GET["name"]) && !empty($_GET["name"]) ) {
	$name_sql = "and name like '%".GetGetParam("name")."%'";
	$smarty->assign("name", $_GET["name"]);
	$error = "No strip revue found with the word: ".$_GET["name"];
}

$phone1 = isset($_GET["phone1"]) && intval($_GET["phone1"]) ? intval($_GET["phone1"]) : "";
$phone2 = !empty($_GET["phone2"]) ? GetGetParam(phone2) : "";
$phone3 = !empty($_GET["phone3"]) ? GetGetParam(phone3) : "";
$phone_sql = NULL;
if( $phone1 || $phone2 || $phone3 ) {
        $phone1 = $phone1 ? (strlen($phone1) == 3 ? $phone1 : "$phone1%") : "%";
        $phone2 = $phone2 ? (strlen($phone2) == 3 ? $phone2 : "%$phone2%") : "%";
        $phone3 = $phone3 ? (strlen($phone3) == 3 ? $phone3 : "%$phone3") : "%";
        $phone_sql = "and (phone like '$phone1$phone2$phone3')";
	$error = "There is no strip clubs found with this phone number";
} else if( isset($_GET["phone"]) && !empty($_GET["phone"]) ) {
	$phone = preg_replace("/[^0-9]/", "", $_GET["phone"]);
	$phone_sql = "and phone like '$phone%'";
	$smarty->assign("phone", $_GET["phone"]);
} else {
        unset($_GET["phone1"]); unset($_GET["phone2"]); unset($_GET["phone3"]);
}

if( isset($_GET["zipcode"]) && !empty($_GET["zipcode"]) ) {
        $zipcode = preg_replace("/[^0-9a-z]/i","", $_GET["zipcode"]);
	$mile_in = intval($_GET["miles"]); if( !$mile_in ) $mile_in = 10;
	$res = $db->q("select z.lat, z.lon, l.loc_parent from core_zip z inner join location_location l on z.loc_id = l.loc_id where z.zip = '$zipcode' limit 1");
	if( $db->numrows($res) ) {
		$s = $db->r($res); 
		$lat_me = $s["lat"]; $lon_me = $s["lon"];
		$core_state_id = $s["loc_parent"];
	} else {
		$error = "Zip code($zipcode) could not find. Please make sure that you typed the correct zip code.";	
	}
	$smarty->assign("miles", $mile_in);
        $smarty->assign("zipcode", $_GET["zipcode"]);
}

$res = $db->q("select s.*, l.loc_url from strip_revues s inner join location_location l using (loc_id) where state_id = '$core_state_id' and type = '{$type}' 
$loc_id_sql $name_sql $phone_sql order by name");
while($row=$db->r($res)) {

	if( $zipcode ) {
		if( !isset($lat_me) ) continue;
		if( $row["loc_lat"] && $row["loc_long"] ) {
			$mile = number_format(distance($lat_me, $lon_me, $row["loc_lat"], $row["loc_long"], "M"),1);
			if( $mile_in != -1 && $mile_in < $mile && $zipcode != $row["zipcode"]) continue;
		} else $mile = "unknown";
	}

	$link = "{$row['loc_url']}$gModule/" . name2url($row['name']) . "/";

	$sc[] = array(
		"id"=>$row["id"],
		"name"=>$row["name"],"thumb"=>$row["thumb"],
		"address"=>$row["address1"],
		"phone"=>makeproperphonenumber($row["phone"]),
		"loc_name"=>$row["loc_name"],
		"zipcode"=>$row["zipcode"],
		"distance"=>isset($zipcode)?(float)$mile:NULL,
		"review"=>$row["review"],
		"edit"=>$row["edit"],
		"linkoverride"=>$link.$row["id"],
	);
} 
$total = count($sc);
if( $zipcode ) $sc = $db->arraysort($sc, "distance", 0);
$sc = $db->arrayClear($sc, 20, $total);

$smarty->assign("sc", $sc);
$smarty->assign("loc_name", $loc_name);
$smarty->assign("gModule", $gModule);
$smarty->assign("total", $total);
$smarty->assign("phone1", $_GET["phone1"]);
$smarty->assign("phone2", $_GET["phone2"]);
$smarty->assign("phone3", $_GET["phone3"]);

if( isset($error) ) $smarty->assign("error", $error);

$query = _pagingQuery();

$dir = new dir;
$ctx->page = $page;
$smarty->assign("pager_array", $dir->get_pager_array($total, $item_per_page, $account->core_loc_array['loc_url']."/{$gModule}/", true));
$smarty->assign("paging", $db->paging($total, 20, yes, yes, $query));
include_html_head("js", "/js/tools/jquery.autotab.js");
$smarty->assign("type", $_GET["type"]);

$smarty->assign("what", $cat_name);
$smarty->assign("worker_link", "strip_revues");
$smarty->assign("image_path", "stripclub/revues");
$smarty->assign("place_link", "revue");
$smarty->assign("loc_name", $loc_name);
$smarty->assign("gModule", $gModule);

$smarty->display(_CMS_ABS_PATH."/templates/place/place_list.tpl");

?>

