<?php

global $db, $smarty, $account, $gModule, $gItemID, $gLocation, $gTitle, $gDescription, $ctx, $config_image_server;

$id = $gItemID;
if (!$id)
	system::goModuleBase();

$res = $db->q("SELECT c.*, l.loc_name, upper(l.s) s, l.loc_parent, l.country_sub, l.loc_url 
				FROM strip_revues c 
				LEFT JOIN location_location l on l.loc_id = c.loc_id 
				WHERE id = '$id'");
if (!$db->numrows($res))
	system::moved($ctx->location_path."/male-revues/");
$row = $db->r($res);

$name2url = name2url($row['name']);

if ($account->core_sub != $row["country_sub"] || !strstr($_SERVER['REQUEST_URI'], $row['loc_url']) || $gLocation != $name2url)
	system::moved(location::getUrlStatic($row["country_sub"], $row["loc_url"])."{$gModule}/{$name2url}/{$row['id']}");

$review = new review("strip_revues", $id, "sc_revue", "", "sc_revue");

$gTitle = "{$row['name']} {$row['loc_url']} Strip Club Revue";
$type = $row["type"] == 1 ? "male" : "female";

$phone = makeproperphonenumber($row["phone"]);

$gTitle = "{$row["name"]} {$row["loc_name"]}, {$row["s"]} {$row["zipcode"]}"; if( $row["review"] ) $gTitle .= " | Total Review: {$row["review"]}";

$loc = "<a href='{$account->core_loc_array['loc_url']}$gModule/' title='{$row["loc_name"]} {$dancer_name} $type revues' 
class='link'>{$row["loc_name"]}</a>, {$row["s"]} {$row["zipcode"]}";

if( strlen($row["website"])>1 ) {
	if (strncmp($row["website"], "http://", 7))
	$row["website"] = "http://" . $row["website"];
	$web = parse_url($row["website"]);
	$smarty->assign("web_url", $row["website"]);
	$smarty->assign("web_host", $web["host"]);
}

$quickread = $review->reviews();
$smarty->assign("quickread", $quickread);
$phone = makeproperphonenumber($row["phone"]);

if( empty($quickread) && !empty($row['gdescription']) ) {
	$gDescription = $row['gdescription'];
	$smarty->assign("description", $gDescription);
} else {
	$gDescription .= "$phone {$row['name']} {$row['address1']} Male Revue Clubs";
}

$smarty->assign("name", $row["name"]);
$smarty->assign("loc_name", $row["loc_name"]);
$smarty->assign("phone", $phone);
$smarty->assign("address", $row["address1"]);
$smarty->assign("location", $loc);
$smarty->assign("id", $id);
$smarty->assign("totalreview", $row["review"]);
$smarty->assign("paging", $db->paging($row["review"], 10, false, true, "?id={$id}"));

//image
$imagedir = "stripclub/revues";
$photos = array();
if ($row["image"] != "" && $row["thumb"] != "") {
	$photos["image"] = "{$config_image_server}/{$imagedir}/{$row["image"]}";
	$photos["thumb"] = "{$config_image_server}/{$imagedir}/t/{$row["thumb"]}";
}
$smarty->assign("photos", $photos);


if( strlen($row["url"])>1 ) {
	$url = trim($row["url"]);
	if (strncmp($url, "http://", 7))
		$url = "http://" . $url;
	$web = parse_url($url);
	$web_url = $url;
	$web_host = $web["host"];
}

if( $row["ticket"] ) $c0 = "<b>Ticket Price</b>: \${$row["ticket"]}<br/>";
if( !empty($row["schedule"]) ) $c0 .= "<b>Show Schedule</b>:<br/>".nl2br($row["schedule"]);
$smarty->assign("c0", $c0);

$addressLx = "<b>{$phone}</b><br />{$row["address1"]}<br />{$loc}<br/>";
if ($web_url)
	$addressLx .= "<a href=\"$web_url\" target=\"_blank\" rel=\"nofollow\" class=\"website\">$web_host</a><br/>";
$smarty->assign("address", $addressLx);

$core_state_name = "<a href='{$account->core_loc_array['loc_url']}$gModule/' title='{$row["loc_name"]} Strip Revues'>{$row["loc_name"]} Strip 
Revues</a>";
$smarty->assign("core_state_name", $core_state_name);

include_html_head("css", "/css/info.css");
$smarty->assign("editlink", "strip_revues");

$smarty->display(_CMS_ABS_PATH."/templates/place/place_place.tpl");


?>
