<?php

defined('_CMS_FRONTEND') or die('Restricted access !');
require_once (_CMS_ABS_PATH."/_cms_files/strip-clubs/array.php");

global $db, $account, $smarty;
$system = new system; 

$id = isset($_REQUEST["id"]) ? intval($_REQUEST["id"]) : NULL;
if( !$id ) $system->go("/");

$review = new review("strip_revues", $id, "sc_revue", "", "sc_revue");
$review->spam_protection = true;
$review->process();

return;

?>

