<?php

defined("_CMS_FRONTEND") or die("No Access");

global $db, $gModule, $ctx;
$system = new system;

$id = GetGetParam("id");

$res = $db->q("select loc_id, type, name FROM strip_revues WHERE id = '{$id}'");
if (!$db->numrows($res))
    $system->moved($ctx->location_path);
$row = $db->r($res);
$loc = location::findOneById($row["loc_id"]);
if ($loc == NULL)
    $system->moved("/");

switch ($row["type"]) {
    case 1: $gModule = "male-revues"; break;
    case 2: $gModule = "male-gay-revues"; break;
    default: $gModule = "male-revues"; break;
}

$system->moved($loc->getPath()."/{$gModule}/".name2url($row["name"])."/{$id}");

?>
