<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty;
$system = new system;

require_once(_CMS_ABS_PATH."/_cms_files/worker/array_sc.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
$form = new data;
$system = new system;

$form->table = "strip_revues";

$c = new CColumnId("id", "ID");
$form->AppendColumn($c);

$c = new CColumnEnum("type", "Type");
$c->AddEnumOption("1", "Male Revue");
$c->AddEnumOption("2", "Male Gay Revue");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("name", "Name");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnLocation("City", "Location", 3);
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("address1", "Address");
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "Zipcode");
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Club Phone");
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setPath("stripclub");
$c->setWatermark();
$form->AppendColumn($c);

$form->visitor_mode = true;
$form->visitor_url = "/male-revues/";
$form->admin_url = "/worker/strip_revues";
$form->ShowPage();

?>
