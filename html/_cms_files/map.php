<?php

defined("_CMS_FRONTEND") or die("No Access");

global $db, $smarty, $account, $gTitle, $ctx, $config_mapquest_key;

if (!isset($_GET["update"]))
	system::moved("/");

$id = intval($_GET["id"]);
$account_id = $account->isloggedin();

$section = $_GET["section"];

//defaults		
$address = "address";
$phone = "phone";
$row = NULL;

switch($section) {
	case 'adultstore':
		$res = $db->q("select * from adult_stores where id = ?", array($id));
		$moved = "/adultstores/";
		$link = "/adultstores/store";
		$linkw = "/worker/adultstore";
		$table = "adult_stores";
		break;
	case 'brothel':
		$res = $db->q("select * from brothel where id = ?", array($id));
		$moved = "/brothels/";
		$link = "/brothels/club";
		$linkw = "/worker/brothel";
		$table = "brothel";
		break;
	case 'eroticmassage':
		$res = $db->q("select * from eroticmassage where id = ?", array($id));
		$module = "eroticmassage";
		$moved = "/eroticmassage/";
		$address = "address1";
		$link = "/erotic-massage/parlor";
		$linkw = "/worker/emp";
		$table = "eroticmassage";
		break;
	case 'lifestyle':
		$res = $db->q("select * from lifestyle where id = ?", array($id));
		$moved = "/lifestyle/";
		$link = "/lifestyle/club";
		$linkw = "/worker/lifestyle";
		$table = "lifestyle";
		break;
	case 'lingeriemodeling1on1':
		$res = $db->q("select * from lingeriemodeling1on1 where id = ?", array($id));
		$moved = "/lingeriemodeling1on1/";
		$address = "address1";
		$link = "/lingeriemodeling1on1/place";
		$table = "lingeriemodeling1on1";
		$linkw = "/worker/lingeriemodeling1on1";
		break;
	case 'sc':
	case 'stripclub':
		$res = $db->q("select * from strip_club2 where id = ?", array($id));
		$moved = "/strip-clubs/";
		$address = "street";
		$link = "/strip-clubs/club";
		$linkw = "/worker/sc";
		$table = "strip_club2";
		break;
	case 'gaybath':
		if( $account->isadmin() && !isset($_GET["id"]) ) 
			$res = $db->q("select * from gaybath where loc_lat = 0 limit 1");
		else
			$res = $db->q("select * from gaybath where id = ?", array($id));
		$moved = "/gay-bath-houses/";
		$link = "/gay-bath-houses/place";
		$table = "gaybath";
		$location = "loc_name";
		$linkw = "/worker/gaybath";
		break;
	case 'gay':
		if( $account->isadmin() && !isset($_GET["id"]) ) 
			$res = $db->q("select * from gay where loc_lat = 0 limit 1");
		else
			$res = $db->q("select * from gay where id = ?", array($id));
		$moved = "/gay/";
		$link = "/gay/place";
		$table = "gay";
		$location = "loc_name";
		$linkw = "/worker/gay";
		break;
	default :
		if ($ctx->page_type != "map" || $ctx->country == NULL) {
			system::moved("/");
		}

		$module = $ctx->country;
		$moved = "/".$ctx->country."/";
		$address = "address1";
		$phone = "phone1";
		$link = "/".$ctx->country."/place";
		$table = "place";
		$location = "loc_name";
		$linkw = "/worker/".$ctx->country;

		if (!$ctx->node_id && ($account->isadmin()||$account->hasPermission('sexclub')||$account->hasPermission($ctx->country))) {
			$res = $db->q("select * from place where loc_lat = 0 limit 1");
		} elseif ($ctx->node_id) {
			$res = $db->q("SELECT p.*, l.loc_name FROM place p LEFT JOIN location_location l on p.loc_id = l.loc_id WHERE place_id = ?", array($ctx->node_id));
		} else {
			system::moved("/");
		}
		if (!$db->numrows($res))
			system::moved($moved);
		$row = $db->r($res);

		$section = $ctx->country;
		break;
}

if ($row == NULL) {
	if( !$db->numrows($res) ) system::moved($moved);
	$row = $db->r($res);
}

$gTitle = "Directions to {$row["name"]}, ".ucfirst($table);

$smarty->assign("loc_name", $row["loc_name"]);
$smarty->assign("phone", makeproperphonenumber($row[$phone]));
$smarty->assign("name", $row["name"]);
$smarty->assign("street", $row[$address]);

if ($ctx->node_id) {
	$id = $ctx->node_id;
	$link = dir::getPlaceLink($row);
} else {
	$id = $row["id"];
	$link = $link."?id=".$id;
}
$smarty->assign("id", $id);
$smarty->assign("link", $link);

if( isset($location) )
	$smarty->assign("location", $row[$location]);


$lat = preg_replace('/[^0-9\-\.]/', '', $_POST["lat"]);
$long = preg_replace('/[^0-9\-\.]/', '', $_POST["long"]);
if ($lat && $long) {
	// ------------------------
	// submitting new lac & lot

	if ($account->isadmin() || $account->hasPermission($module)) {
		//if we are admin, directly update database

		$db->q("UPDATE place SET loc_lat = ?, loc_long = ? WHERE place_id = ?", array($lat, $long, $id));

		if (!isset($_GET["id"]) && !$ctx->node_id)
			system::moved("/map?section=".$section."&update=1");
		system::moved($link);
	}

	$wlink = "https://{$_SERVER["HTTP_HOST"]}/{$linkw}?id={$id}&lat=$lat&long=$long";
	$x = "Check out the edit page and look at the bottom 2 maps underneath the form. first one is the current map and the second one is the requested. if the addresses matches with the new one, click to update button to save it..<br/></br><a href='{$wlink}'>{$wlink}</a>";
	send_email([
		"from" => "noreply@adultseach.com", 
		"to" => SUPPORT_EMAIL, 
		"subject" => "Map Update",
		"html" => $x,
		]);
	system::go($link, "Thank you for your help, we will update the map as soon as possible.");
}


// --------------------------
// displaying map update page
$rex = $db->q("SELECT l.loc_name name1, l2.loc_name name2
				FROM location_location l 
				LEFT JOIN location_location l2  on l.loc_parent = l2.loc_id 
				WHERE l.loc_id = ?", 
				array($row["loc_id"]));
if( $db->numrows($rex) ) {
	$rox = $db->r($rex);
	$loc_name = "{$rox["name1"]}, {$rox["name2"]}";
} else {
	$loc_name = $row["loc_name"];
}

//MapQuest and Leaflet include
include_html_head('js', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.js');
include_html_head('css', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.css');
include_html_head('js', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.js');
include_html_head('css', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.css');

$smarty->assign("location", $loc_name);
$smarty->assign("map_public_key", $config_mapquest_key);

$smarty->display(_CMS_ABS_PATH."/templates/place/place_mapupdate.tpl");
return;

?>


