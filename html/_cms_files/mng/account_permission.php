<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!account::ensure_admin())
	die("Invalid access");

if (!$account->isrealadmin())
	die("Invalid access");

global $url_last_piece, $filters, $where_cols;

//configuration
$url_last_piece = "account_permission";
$where_cols = array(
	"account_id" => array("title" => "Account Id", "where" => "a.account_id", "match" => "exact", "size" => 5),
	"email"   => array("title" => "Email", "where" => "a.email"),
);

function add() {
	global $db;

	if (!isset($_REQUEST["u_submit"])) {
		echo "<h2>permission</h2>";
		echo "<form method=\"post\" action=\"\">";
		echo "<table>";
		echo "<tr><th>Name:</th><td><input type=\"text\" name=\"u_name\" value=\"\" /></td></tr>\n";
		echo "<tr><th></th><td><input type=\"submit\" name=\"u_submit\" value=\"Submit\" /></td></tr>\n";
		echo "</table>";
		echo "</form>";
		return true;
	}

	//were going live
	$name = $_REQUEST["u_name"];
	$res = $db->q("INSERT INTO permission (name) VALUES (?)", array($name));
	$id = $db->insertid();
	if ($db->affected($res) == 1) {
		return actionSuccess("New permission has been added.");
	} else {
		return actionError("Error while adding new permission - please contact administrator!");
	}
}

function edit() {
	global $account, $db;

	$account_id = intval($_REQUEST["action_id"]);
	if ($account_id == 0)
		return actionError("Invalid account id !");

	$acc = account::findOnebyId($account_id);
	if ($acc == false)
		return actionError("Cant find account of id={$account_id} !");

	//get all permissions
	$res = $db->q("SELECT * FROM permission ORDER BY name ASC");
	$permissions = array();
	while ($row = $db->r($res)) {
		$permissions[$row["id"]] = array("name" => $row["name"], "selected" => false);
	}

	//get worker permissions
	$res = $db->q("SELECT * FROM account_permission WHERE account_id = ?", array($account_id));
	while ($row = $db->r($res)) {
		$permission_id = $row["permission_id"];
		if (!array_key_exists($permission_id, $permissions)) {
			echo "<span style=\"color: red; font-weight: bold;\">Error: Can't find permission by id #{$permission_id} !</span><br />"; 
			continue;
		}
		$p = $permissions[$permission_id];
		$p["selected"] = true;
		$permissions[$permission_id] = $p;
	}

	//submission processing	
	if (isset($_REQUEST["submit"])) {
		$selected_permissions = array();
		foreach ($_REQUEST as $key => $val) {
			if (substr($key, 0, 4) != "u_p_")
				continue;
			if ($val != 1)
				continue;
			$permission_id = intval(substr($key, 4));
			$selected_permissions[] = $permission_id;
		}
		$msg = "";
		foreach ($permissions as $permission_id => $p) {
			if ($p["selected"] == true && !in_array($permission_id, $selected_permissions)) {
				$db->q("DELETE FROM account_permission WHERE account_id = ? AND permission_id = ?", array($account_id, $permission_id));
				$msg .= "Removed permission '{$p["name"]}'<br />";
			} else if ($p["selected"] == false && in_array($permission_id, $selected_permissions)) {
				$db->q("INSERT INTO account_permission (account_id, permission_id) VALUES (?, ?)", array($account_id, $permission_id));
				$msg .= "Added permission '{$p["name"]}'<br />";
			}
		}
		if ($msg != "") {
			return actionSuccess("Changed worker permissions of account #{$account_id} ({$acc->getEmail()}):<br />".$msg);
		} else {
			return actionSuccess("No changes made.");
		}
	}

	echo "<h2>Edit worker permissions of account #{$acc->getId()} - {$acc->getEmail()}</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Permissions:</th><td>";
	foreach($permissions as $key => $p) {
		echo "<input type=\"checkbox\" name=\"u_p_{$key}\" value=\"1\" ";
		if ($p["selected"] == true)
			echo "checked=\"checked\"";
		echo "/>{$p["name"]}<br />\n";
	}
	echo "</td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
	echo "</table>";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$account_id}\" />";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'edit':
		$ret = edit();
		if (!$ret)
			return;
		break;
}

$params = [];
$where = getWhere($params, true);
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
	$order = "ORDER BY 3 DESC, 1 ASC";

//query db
$sql = "SELECT count(distinct a.account_id) as total
		FROM account a
		LEFT JOIN account_permission ap on ap.account_id = a.account_id
		WHERE a.worker = 1 AND a.deleted IS NULL {$where}";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT a.account_id, a.email, COUNT(ap.id) as cnt_permissions
		FROM account a
		LEFT JOIN account_permission ap on ap.account_id = a.account_id
		WHERE a.worker = 1 AND a.deleted IS NULL {$where}
		GROUP BY a.account_id
		$order 
		$limit";
$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No permissions.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Worker permissions</h2>\n";
displayFilterForm();
echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr>
<th>".getOrderLink("Account Id", "a.account_id")."</th>
<th>".getOrderLink("Email", "a.email")."</th>
<th># permissions</th>
<th/></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<td>{$rox['account_id']}</td>";
	echo "<td>{$rox['email']}</td>";
	echo "<td>{$rox['cnt_permissions']}</td>";
	echo "<td><a href=\"".getActionLink("edit", $rox['account_id'])."\">edit</a></td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;


?>
