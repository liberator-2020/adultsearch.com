<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("access_admin_accounts"))
	die("Invalid access");

$account_level = $account->getAccountLevel();

global $url_last_piece, $filters, $where_cols;

//configuration
$url_last_piece = "accounts";
$where_cols = array(
	"account_id" => array("title" => "Account ID", "where" => "a.account_id", "match" => "exact", "size" => "7"),
	"status" => array("title" => "Status", "where" => "a.banned", "type" => "select", "options" => array("" => "-", "0" => "Live", "1" => "Banned")),
	"username" => array("title" => "Username", "where" => "a.username", "size" => "10"),
	"email" => array("title" => "Email", "where" => "a.email", "size" => "15"),
	"password" => array("title" => "Password", "where" => "a.password", "size" => "10"),
	"ip" => array("title" => "IP", "where_condition" => " a.ip_address = ?", "where_params" => ["{val}"], "size" => "10"),
	"phone"   => array("title" => "Phone", "where" => "a.phone", "match" => "preg_replace", "pattern" => "/[^0-9]/", "replacement" => "", "size" => "8"),	
	"phone_verified" => array("title" => "Phone verified", "where" => "a.phone_verified", "type" => "select", "options" => array("" => "-", "0" => "No", "1" => "Yes")),
	"whitelisted" => array("title" => "Whitelisted", "where" => "a.whitelisted", "type" => "select", "options" => array("" => "-", "0" => "No", "1" => "Yes")),
	"worker" => array("title" => "Worker", "where" => "a.worker", "type" => "select", "options" => array("" => "-", "0" => "No", "1" => "Yes")),
	"agency" => array("title" => "Escort Agency", "where" => "a.agency", "type" => "select", "options" => array("" => "-", "0" => "No", "1" => "Yes")),
	"deleted" => [
		"title" => "Deleted", 
		"where" => [
			"" => "",
			"yes" => " a.deleted IS NOT NULL ",
			"no" => " a.deleted IS NULL ",
			],
		"type" => "select", 
		"options" => [
			"" => "-", 
			"no" => "No", 
			"yes" => "Yes"
			],
		],
/*
	"with_live_ad" => [
		"title" => "With live ad(s)", 
		"having" => [
			"" => "",
			"yes" => " COUNT(distinct c2.id) > 0 ",
			"no" => " COUNT(distinct c2.id) = 0 ",
			],
		"type" => "select", 
		"options" => [
			"" => "-", 
			"yes" => "Yes",
			"no" => "No", 
			],
		],
*/
);

function add() {

	if (isset($_REQUEST["submit"])) {

		//set params	
		if (isset($_REQUEST["username1"]))
			$username = $_REQUEST["username1"];
		if (isset($_REQUEST["password1"]))
			$password = $_REQUEST["password1"];
		if (isset($_REQUEST["email1"]))
			$email = $_REQUEST["email1"];
		if (isset($_REQUEST["approved1"]))
			$approved = 1;
		else
			$approved = 0;
		if (isset($_REQUEST["email_confirmed1"]))
			$email_confirmed = 1;
		else
			$email_confirmed = 0;
		if (isset($_REQUEST["worker1"]))
			$worker = 1;
		else
			$worker = 0;
		$advertiser = (isset($_REQUEST["advertiser1"])) ? 1 : 0;
		$agency = (isset($_REQUEST["agency1"])) ? 1 : 0;
		if (isset($_REQUEST["account_level1"]))
			$account_level = intval($_REQUEST["account_level1"]);

		//check params
		if ($username == "")
			echo "Error: Please enter username!<br />\n";
		else if ($password == "")
			echo "Error: Please enter password !<br />\n";
		else if ($email == "")
			echo "Error: Please enter email !<br />\n";
		else if (!in_array($account_level, array(0,1,2,3)))
			echo "Error: Please enter correct account_level !<br />\n";
		else {

			$acc = new account();
			$acc->setUsername($username);
			$acc->setPlainPassword($password);
			$acc->setEmail($email);
			$acc->setApproved($approved);
			$acc->setEmailConfirmed($email_confirmed);
			$acc->setAccountLevel($account_level);
			$acc->setWorker($worker);
			$acc->setAdvertiser($advertiser);
			$acc->setAgency($agency);
			$ret = $acc->add();
			if ($ret) {
				return actionSuccess("You have successfully added account #{$acc->getId()}.");
			} else {
				return actionError("Error while adding new account ! Please contact administrator.");
			}
		}
	} else {
		$approved = 1;
		$email_confirmed = 1;
		$account_level = 0;
		$worker = $advertiser = $agency = 0;
	}

	echo "<h2>Add new accont</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Username: </th><td><input type=\"text\" name=\"username1\" size=\"30\" value=\"{$username}\"></td></tr>\n";
	echo "<tr><th>Password: </th><td><input type=\"text\" name=\"password1\" size=\"30\" value=\"{$password}\"></td></tr>\n";
	echo "<tr><th>Email: </th><td><input type=\"text\" name=\"email1\" size=\"30\" value=\"{$email}\"></td></tr>\n";
	$approved_checked = "";
	if ($approved)
		$approved_checked = " checked=\"checked\" ";
	echo "<tr><th colspan=\"2\"><input type=\"checkbox\" name=\"approved1\" {$approved_checked} /> Approved</th></tr>\n";
	$email_confirmed_checked = "";
	if ($email_confirmed)
		$email_confirmed_checked = " checked=\"checked\" ";
	echo "<tr><th colspan=\"2\"><input type=\"checkbox\" name=\"email_confirmed1\" {$email_confirmed_checked} /> Email confirmed</th></tr>\n";
	$worker_checked = "";
	if ($worker)
		$worker_checked = " checked=\"checked\" ";
	echo "<tr><th colspan=\"2\"><input type=\"checkbox\" name=\"worker1\" {$worker_checked} /> Worker</th></tr>\n";
	$agency_checked = "";
	if ($agency)
		$agency_checked = " checked=\"checked\" ";
	echo "<tr><th colspan=\"2\"><input type=\"checkbox\" name=\"agency1\" {$agency_checked} /> Escort Agency</th></tr>\n";
	$advertiser_checked = "";
	if ($advertiser)
		$advertiser_checked = " checked=\"checked\" ";
	echo "<tr><th colspan=\"2\"><input type=\"checkbox\" name=\"advertiser1\" {$advertiser_checked} /> Advertiser (CPC/CPM advertising)</th></tr>\n";
	echo "<tr><th>Account level: </th><td><input type=\"text\" name=\"account_level1\" size=\"10\" value=\"{$account_level}\"><br />1 = worker, 2 = admin, 3 = superadmin</td></tr>\n";

	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Add\" /></td></tr>\n";
	echo "</table>";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}



function edit() {
	global $account, $db;

	$account_id = intval($_REQUEST["action_id"]);
	if ($account_id == 0)
		return actionError("Invalid account id !");

	$acc = account::findOnebyId($account_id);
	if ($acc == false)
		return actionError("Cant find account of id={$account_id} !");

	if ($acc->getAccountLevel() >= $account->getAccountLevel())
		return actionError("You dont have privileges to edit this user (account_id={$account_id}) !");

	if ($acc->getBanned()) {
		if (substr($acc->getBanReason(), 0, 11) == "integrate #") {
			$new_account_id = substr($acc->getBanReason(), 11);
			return actionError("Account #{$acc->getId()} - {$acc->getEmail()} was merged into another account #{$new_account_id}, <a href=\"/mng/accounts?action=edit&action_id={$new_account_id}\">edit account #{$new_account_id}</a>");
		}
		return actionError("Account #{$acc->getId()} is banned, you can't edit it. Ban reason: {$acc->getBanReason()}");
	}

	if (isset($_REQUEST["submit"])) {
		$updated = "";

		//double check phone for verification is not used for any other account
		$phone = phone_to_international(preg_replace('/[^0-9\+]/', '', $_REQUEST["u_phone"]));
		$duplicate_phone = false;
		if (!$phone)
			$phone = null;
		if ($phone) {
			$cnt = $db->single("SELECT count(*) FROM account WHERE phone = ? AND account_id <> ?", [$phone, $account_id]);
			if ($cnt)
				$duplicate_phone = true;
		}
		if ($duplicate_phone)
			return error_redirect("Phone '{$phone}' is duplicate", "/mng/".getActionLink("edit", $account_id));

		if (isset($_REQUEST["u_notes"])) {
			$acc->setNotes($_REQUEST["u_notes"]);
			$updated .= " notes";
		}
		if (isset($_REQUEST["u_password"])) {
			$password = trim($_REQUEST["u_password"]);
			if ($password) {
				$acc->setPlainPassword($password);
				$updated .= " password";
			}
		}
		$acc->setWorker(intval($_REQUEST["u_worker"]));
		$acc->setAdvertiser(intval($_REQUEST["u_advertiser"]));
		$acc->setAgency(intval($_REQUEST["u_agency"]));
		$acc->setAdsLimit(($_REQUEST["u_ads_limit"] != "") ? intval($_REQUEST["u_ads_limit"]) : null);
		$acc->setPhoneVerified(intval($_REQUEST["u_phone_verified"]));
		if ($acc->getPhone() != $phone) {
			$acc->setPhone($phone);
			$updated .= " phone";
		}
		$repost = intval($_REQUEST["u_repost"]);
		if (intval($acc->getRepost()) != $repost) {
			$acc->setRepost($repost);
			$updated .= " repost";
		}
		$ret = $acc->update();
		if ($ret) {
			if (intval($_REQUEST["u_agency"])) {
				//if we are changing account to agency, delete all pending payments
				$db->q("DELETE FROM payment where account_id = ? AND result = 'P'", [$account_id]);
			}
			return actionSuccess("You have successfully updated ({$updated}) account id #{$account_id}.");
		} else {
			return actionError("Error while updating account id #{$account_id}.");
		}
	}

	//TODO put edit functionality into account class ??
	echo "<h2>Edit account #{$acc->getId()} - {$acc->getEmail()}</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Account ID</th><td>{$acc->getId()}</td></tr>\n";
	echo "<tr><th>Email</th><td>{$acc->getEmail()}</td></tr>\n";
	echo "<tr><th>Password</th><td><input type=\"text\" name=\"u_password\" size=\"20\" value=\"\" /><small>Note: if left blank, password is not going to be changed.</small></td></tr>\n";
	echo "<tr><th>Notes</th><td><textarea name=\"u_notes\" cols=\"80\" rows=\"6\">".htmlspecialchars($acc->getNotes())."</textarea></td></tr>\n";
	$worker_checked = "";
	if ($acc->isWorker())
		$worker_checked = " checked=\"checked\" ";
	echo "<tr><th/><td><label><input type=\"checkbox\" name=\"u_worker\" {$worker_checked} value=\"1\" /> Worker</label></td></tr>\n";
	$agency_checked = "";
	if ($acc->isAgency())
		$agency_checked = " checked=\"checked\" ";
	echo "<tr><th/><td><label><input type=\"checkbox\" name=\"u_agency\" {$agency_checked} value=\"1\" /> Escort Agency</label></td></tr>\n";
	echo "<tr><th>Max. number of classified ads</th><td><input type=\"text\" name=\"u_ads_limit\" size=\"10\" value=\"{$acc->getAdsLimit()}\" /> Note: if left blank, following default is in place: <strong>standard</strong> accounts: <strong>2</strong> classified ads, <strong>escort agencies</strong>: <strong>10</strong> classified ads.</td></tr>\n";
	$advertiser_checked = "";
	if ($acc->isAdvertiser())
		$advertiser_checked = " checked=\"checked\" ";
	echo "<tr><th/><td><label><input type=\"checkbox\" name=\"u_advertiser\" {$advertiser_checked} value=\"1\" /> Advertiser (CPC/CPM Advertising)</label></td></tr>\n";
	$phone_verified_checked = "";
	if ($acc->isPhoneVerified())
		$phone_verified_checked = " checked=\"checked\" ";
	echo "<tr><th/><td><label><input type=\"checkbox\" name=\"u_phone_verified\" {$phone_verified_checked} value=\"1\" /> Phone Verified</label></td></tr>\n";
	echo "<tr><th>Phone</th><td><input type=\"text\" name=\"u_phone\" size=\"20\" value=\"{$acc->getPhone()}\" /></td></tr>\n";
	echo "<tr><th>Global repost credits</th><td><input type=\"text\" name=\"u_repost\" size=\"10\" value=\"{$acc->getRepost()}\" /></td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
	echo "</table>";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$account_id}\" />";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}	

function delete() {
	global $account, $smarty, $db;

	$account_id = intval($_REQUEST["action_id"]);
	if ($account_id == 0)
		return actionError("Invalid account id !");

	$acc = account::findOnebyId($account_id);
	if ($acc == false)
		return actionError("Cant find account of id={$account_id} !");

	if ($acc->getAccountLevel() >= $account->getAccountLevel())
		return actionError("You dont have privileges to delete this user (account_id={$account_id}) !");

	//form field values		
	$reason = $send_email = $email_text = $contact_id = null;
	if (isset($_REQUEST["u_reason"]))
		$reason = $_REQUEST["u_reason"];
	$send_email = (boolean)$_REQUEST["u_send_email"];
	$email_text = $_REQUEST["u_email_text"];
	$email_html = nl2br($email_text);

	//this is used when this action is called from internal mail system (/mnt/index) - so we can mark contact as read and redirect back to /mng/index
	if (intval($_REQUEST["contact_id"]))
		$contact_id = intval($_REQUEST["contact_id"]);

	//check if account does not have active subscriptions
	$res = $db->q("SELECT COUNT(*) as cnt FROM payment p WHERE p.account_id = ? AND subscription_status = 1", [$acc->getId()]);
	$row = $db->r($res);
	$cnt_active_subscriptions = $row["cnt"];
	$smarty->assign("cnt_active_subscriptions", $cnt_active_subscriptions);
	//check if account does not have active ads
	$res = $db->q("SELECT COUNT(*) as cnt FROM classifieds c WHERE c.account_id = ? AND c.done > 0 AND c.deleted IS NULL", [$acc->getId()]);
	$row = $db->r($res);
	$cnt_active_ads = $row["cnt"];
	$smarty->assign("cnt_active_ads", $cnt_active_ads);

	if (isset($_REQUEST["submit"])) {
		$ret = $acc->remove($reason);
		if ($ret) {

			//optional sending of email to user that his account has been deleted	
			if ($send_email) {
				$ret2 = send_email([
					"from" => SUPPORT_EMAIL,
		   			"to" => $acc->getEmail(),
					"subject" => "Your account is deleted",
					"html" => $email_html,
					"text" => $email_text,
				]);
				if ($ret2)
					flash::add(flash::MSG_SUCCESS, "Email to '{$acc->getEmail()}' was sent successfully");
				else	
					flash::add(flash::MSG_ERROR, "Failed to send email to '{$acc->getEmail()}'.");
			}

			//optional marking contact as read and redirecting back to internal mail system
			if ($contact_id) {
				$db->q("UPDATE contact SET `read` = 1, respond = 'User account #{$acc->getId()} deleted', read_by = ? WHERE contact_id = ?",
						[$account->getId(), $contact_id]
						);
				return success_redirect("You have successfully deleted account id #{$acc->getId()}", "/mng/");
			}

			return actionSuccess("You have successfully deleted account id #{$account_id}.");

		} else {
			return actionError("Error while deleting account id #{$account_id}.");
		}
	} else {
		$send_email = true;
		$email_text = "Hello,\nyour account on adultsearch.com has been deleted.\nHave a nice day.\nJay\n";
	}

/*
	echo "<h2>Delete account #{$acc->getId()} - {$acc->getEmail()}</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Account ID</th><td>{$acc->getId()}</td></tr>\n";
	echo "<tr><th>Email</th><td>{$acc->getEmail()}</td></tr>\n";
	echo "<tr><th>Reason</th><td><input type=\"text\" name=\"u_reason\" value=\"\" size=\"60\" /></td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Delete\" /></td></tr>\n";
	echo "</table>";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$account_id}\" />";
	echo getFilterFormFields();
	echo "</form>";
	return false;
*/
	$smarty->assign("id", $acc->getId());
	$smarty->assign("email", $acc->getEmail());
	$smarty->assign("reason", $reason);
	$smarty->assign("send_email", $send_email);
	$smarty->assign("email_text", $email_text);
	$smarty->assign("contact_id", $contact_id);
	$smarty->assign("html_hidden_fields", getFilterFormFields());
	echo $smarty->fetch(_CMS_ABS_PATH."/templates/mng/account_delete.tpl");
	return false;
}

function undelete() {
	global $account;

	$account_id = intval($_REQUEST["action_id"]);
	if ($account_id == 0)
		return actionError("Invalid account id !");

	$acc = account::findOnebyId($account_id);
	if (!$acc)
		return actionError("Cant find account of id={$account_id} !");

	if (!$account->isadmin())
		return actionError("You dont have privileges to undelete this user (account_id={$account_id}) !");

	if (isset($_REQUEST["submit"])) {
		$ret = $acc->undelete();
		if ($ret)
			return actionSuccess("You have successfully undeleted account id #{$account_id}.");
		else
			return actionError("Error while undeleting account id #{$account_id}.");			
	}

	echo "<h2>Undelete account #{$acc->getId()} - {$acc->getEmail()}</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Account ID</th><td>{$acc->getId()}</td></tr>\n";
	echo "<tr><th>Email</th><td>{$acc->getEmail()}</td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Undelete\" /></td></tr>\n";
	echo "</table>";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$account_id}\" />";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}

function ban() {
	global $account, $db;

	$account_id = intval($_REQUEST["action_id"]);
	if ($account_id == 0)
		return actionError("Error: Account id not specified !");

	$acc = account::findOnebyId($account_id);
	if ($acc == false)
		return actionError("Error: Cant find account of id={$account_id}) !");

	if ($acc->isBanned())
		return actionWarning("Account is already banned.");

	if ($acc->getAccountLevel() == 3)
		return actionError("Error: Account #{$account_id} ({$acc->getEmail()}) is superadmin account (jay). You can't ban it.");

	if ($acc->getAccountLevel() == 2)
		return actionError("Error: Account #{$account_id} ({$acc->getEmail()}) is admin account. You can't ban it.");

	if (isset($_REQUEST["submit"])) {

		$reason = $_REQUEST["u_reason"];
		$ret = $acc->ban($reason);

		if ($ret) {

			//21.12.2015 Dallas email: All his comments & posts should auto delete when you ban him.
			$res = $db->q("DELETE FROM place_review_comment WHERE account_id = ?", array($account_id));
			$aff1 = $db->affected($res);
			$res = $db->q("DELETE FROM place_review WHERE account_id = ?", array($account_id));
			$aff2 = $db->affected($res);
			$res = $db->q("DELETE FROM forum_post WHERE account_id = ?", array($account_id));
			$aff3 = $db->affected($res);
			$res = $db->q("DELETE FROM forum_topic WHERE account_id = ?", array($account_id));
			$aff4 = $db->affected($res);

			$msg = "";
			if ($aff1)
				$msg .= "{$aff1} his reviews have been removed. ";
			if ($aff2)
				$msg .= "{$aff2} his review comments have been removed. ";
			if ($aff3)
				$msg .= "{$aff3} his forum posts have been removed. ";
			if ($aff4)
				$msg .= "{$aff4} his forum topics have been removed. ";

			return actionSuccess("Account {$acc->getEmail()} (account_id #{$acc->getId()}) successfully banned.");
		} else {
			return actionError("Error: Account {$owner->getEmail()} (account_id #{$owner->getId()}) was not banned.");
		}
	}

	$res = $db->q("SELECT COUNT(comment_id) as cnt FROM place_review_comment WHERE account_id = ?", array($account_id));
	$row = $db->r($res);
	$cnt1 = $row["cnt"];
	$res = $db->q("SELECT COUNT(review_id) as cnt FROM place_review WHERE account_id = ?", array($account_id));
	$row = $db->r($res);
	$cnt2 = $row["cnt"];
	$res = $db->q("SELECT COUNT(post_id) as cnt FROM forum_post WHERE account_id = ?", array($account_id));
	$row = $db->r($res);
	$cnt3 = $row["cnt"];
	$res = $db->q("SELECT COUNT(topic_id) as cnt FROM forum_topic WHERE account_id = ?", array($account_id));
	$row = $db->r($res);
	$cnt4 = $row["cnt"];

	echo "<h2>Ban account #{$acc->getId()} - {$acc->getEmail()}</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "Are you sure you want to ban this account ?<br />\n";
	if ($cnt2)
		echo "His {$cnt2} reviews will be deleted. (irreversible operation)<br />\n";
	if ($cnt1)
		echo "His {$cnt1} review comments will be deleted. (irreversible operation)<br />\n";
	if ($cnt3)
		echo "His {$cnt3} forum posts will be deleted. (irreversible operation)<br />\n";
	if ($cnt4)
		echo "His {$cnt4} forum topics will be deleted. (irreversible operation)<br />\n";

	echo "<table>";
	echo "<tr><th>Reason:</th><td><select name=\"u_reason\"><option value=\"\"></option><option value=\"spammer\">Spammer</option><option value=\"chargeback\">Chargeback</option></select></td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Ban\" /></td></tr>\n";
	echo "</table>";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$account_id}\" />";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}

function unban() {
	$account_id = intval($_REQUEST["action_id"]);
	if ($account_id == 0)
		return actionError("Error: Account id not specified !");

	$acc = account::findOneById($account_id);
	if (!$acc)
		return actionError("Error: Can't find account of id={$account_id}) !");

	if (!$acc->isbanned())
		return actionWarning("Member is not banned.");

	$ret = $acc->unban();
	if ($ret) {
		audit::log("ACC", "Unban", $acc->getId(), "");
		return actionSuccess("Account {$acc->getEmail()} (account_id #{$acc->getId()}) successfully unbanned.");
	} else {
		return actionError("Error while unbanning account {$acc->getEmail()} (account_id #{$acc->getId()}) !");
	}
}

function mark_verified() {
	global $db;

	$account_id = intval($_REQUEST["action_id"]);
	if ($account_id == 0)
		return actionError("Error: No account id specified !");

	$acc = account::findonebyid($account_id);
	if ($acc == false)
		return actionError("Error: Can't find account of id#{$account_id} !");

	//TODO add confirmation page ?

	//TODO change to $acc->setVerified(1); $acc->update();
	$res = $db->q("UPDATE account SET phone_verified = 1 WHERE account_id = ?", array($account_id));
	$aff = $db->affected($res);

	if ($aff != 1)
		return actionError("Error: Can't verify account #{$account_id} ! (maybe account is already verified?)");

	//putting all waiting ads live
	$res = $db->q("SELECT * FROM classifieds WHERE account_id = ? AND done = 3", array($account_id));
	$i = 0; $err = 0;
	while ($row = $db->r($res)) {
		$clad = clad::withRow($row);
		if ($clad->makeLive())
			$i++;
		else
			$err++;
	}

	if ($err) {
		reportAdmin("Failed to make {$err} classifieds of account id $account_id live !", "", array());
		return actionWarning("Account {$acc->getEmail()} (#{$acc->getId()}) was successfully verified, BUT Failed to make {$err} classifieds live !");
	}

	$ads_msg = "";
	if ($i)
		$ads_msg = " ({$i} cl. ads changed from 'waiting' to 'live')";

	return actionSuccess("Account {$acc->getEmail()} (#{$acc->getId()}) was successfully verified.{$ads_msg}");
}

function whitelist() {
	global $db;

	$account_id = intval($_REQUEST["action_id"]);
	if ($account_id == 0)
		return actionError("Error: No account id specified !");

	$acc = account::findonebyid($account_id);
	if ($acc == false)
		return actionError("Error: Can't find account of id#{$account_id} !");

	//TODO add confirmation page ?

	//TODO change to $acc->setWhitelisted(1); $acc->update();
	$res = $db->q("UPDATE account SET whitelisted = 1 WHERE account_id = ?", array($account_id));
	$aff = $db->affected($res);

	if ($aff != 1)
		return actionError("Error: Can't whitelist account #{$account_id} ! (maybe account is already whitelisted?)");

	audit::log("ACC", "Whitelist", $acc->getId(), "");

	//putting all waiting ads live
	$res = $db->q("SELECT * FROM classifieds WHERE account_id = ? AND done = 3", array($account_id));
	$i = 0; $err = 0;
	while ($row = $db->r($res)) {
		$clad = clad::withRow($row);
		if ($clad->makeLive())
			$i++;
		else
			$err++;
	}

	if ($err) {
		reportAdmin("Failed to make {$err} classifieds of account id $account_id live !", "", array());
		return actionWarning("Account {$acc->getEmail()} (#{$acc->getId()}) was successfully whitelisted, BUT Failed to make {$err} classifieds live !");
	}

	$ads_msg = "";
	if ($i)
		$ads_msg = " ({$i} cl. ads changed from 'waiting' to 'live')";

	return actionSuccess("Account {$acc->getEmail()} (#{$acc->getId()}) was successfully whitelisted.{$ads_msg}");
}

function unwhitelist() {
	global $db;

	$account_id = intval($_REQUEST["action_id"]);
	if ($account_id == 0)
		return actionError("Error: No account id specified !");

	$acc = account::findonebyid($account_id);
	if ($acc == false)
		return actionError("Error: Can't find account of id#{$account_id} !");

	//TODO add confirmation page ?

	//TODO change to $acc->setWhitelisted(1); $acc->update();
	$res = $db->q("UPDATE account SET whitelisted = 0 WHERE account_id = ?", array($account_id));
	$aff = $db->affected($res);

	if ($aff != 1)
		return actionError("Error: Can't unwhitelist account #{$account_id} ! (maybe it is not whitelisted?)");

	audit::log("ACC", "Unwhitelist", $acc->getId(), "");

	return actionSuccess("Account {$acc->getEmail()} (#{$acc->getId()}) was successfully unwhitelisted.{$ads_msg}");
}

function confirm_registration() {
	global $db, $account;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid classified id !");

	$res = $db->q("SELECT * FROM account_confirms WHERE account_id = ? LIMIT 1", array($action_id));
	if (!$db->numrows($res))
		return actionError("Error while 'email confirming' account #{$action_id} - no entry found in table account_confirms !");

	$res = $db->q("UPDATE account SET email_confirmed = 1 WHERE account_id = ? LIMIT 1", array($action_id));
	$aff = $db->affected($res);
	if ($aff != 1) 
		return actionError("Error while 'email confirming' account #{$action_id} - affected {$aff} rows during update of account table !");

	$res = $db->q("DELETE FROM account_confirms WHERE account_id = ?", array($action_id));
	$aff = $db->affected($res);
	if ($aff == 0) 
		return actionError("Error while 'email confirming' account #{$action_id} - affected {$aff} rows during delete from account_confirms table !");

	return actionSuccess("Account ${$action_id} was successfully 'email confirmed'.");
}

//move contents of the account (classifieds, credit cards, purchases) under different account and disable this account
function integrate() {
	global $account, $db;

	$account_id = intval($_REQUEST["action_id"]);
	if ($account_id == 0)
		return actionError("Invalid account id !");

	$acc = account::findOnebyId($account_id);
	if ($acc == false)
		return actionError("Cant find account of id={$account_id} !");

	if (isset($_REQUEST["submit"])) {
		$new_account_id = intval($_REQUEST["new_account_id"]);
		$notify = intval($_REQUEST["notify"]);
		if (!$new_account_id) {
			echo "<span style=\"color: red; font-weight: bold;\">Error: you need to enter new account id!</span><br />";
		} else {
			$new_acc = account::findOneById($new_account_id);
			if (!$new_acc) {
				echo "<span style=\"color: red; font-weight: bold;\">Can't find account for account_id #{$new_account_id} !</span><br />";
			} else {
				echo "Moving items from account #{$account_id} ($acc->getEmail()) to account #{$new_account_id} ({$new_acc->getEmail()}) ...<br />";
				file_log("account", "integrate: Integrating account #{$account_id} to #{$new_account_id}");
				$msg = "";

				//update items	
				$cl_aff = 0;
				$msg = "";
				$res = $db->q("SELECT c.id FROM classifieds c WHERE c.account_id = ?", array($account_id));
				$cl_ids = [];
				while ($row = $db->r($res)) {
					$id = $row["id"];
					$res2 = $db->q("UPDATE classifieds SET account_id = ? WHERE id = ?", array($new_account_id, $id));
					if ($db->affected($res)) {
						audit::log("CLA", "Owner change", $id, "Owner account changed from #{$account_id} to #{$new_account_id}");
						$cl_aff++;
						$db->q("INSERT INTO classifieds_manage (id, account_id, time, what) VALUEs (?, ?, NOW(), ?)", array($id, $account->getId(), $msg));
						$cl_ids[] = $id;
					}
				}
				if (count($cl_ids)) {
					$msg .= "Moved classified ads: #".implode(", #", $cl_ids)."\n";
				}
			
				$res = $db->q("UPDATE account_cc SET account_id = ? WHERE account_id = ?", array($new_account_id, $account_id));
				$cc_aff = $db->affected($res);
			
				$res = $db->q("UPDATE account_purchase SET account_id = ? WHERE account_id = ?", array($new_account_id, $account_id));
				$pu_aff = $db->affected($res);

				$res = $db->q("UPDATE payment SET account_id = ? WHERE account_id = ?", array($new_account_id, $account_id));
				$pa_aff = $db->affected($res);

				//move global repost credits
				$repost = intval($db->single("SELECT repost FROM account WHERE account_id = ?", [$account_id]));
				if ($repost) {
					file_log("account", "integrate: moving {$repost} repost credits from #{$account_id} to #{$new_account_id}");
					$res = $db->q("UPDATE account SET repost = IFNULL(repost, 0) + ? WHERE account_id = ?", [$repost, $new_account_id]);
					$res = $db->q("UPDATE account SET repost = NULL WHERE account_id = ?", [$account_id]);
					$acc->setRepost(null);
					$msg .= "Moved {$repost} top-up credits.\n";
				}

				//move advertiser balance
				$bu_aff = 0;
				$budget = $db->single("SELECT budget FROM advertise_budget WHERE account_id = ?", array($account_id));
				if ($budget && $budget > 0.01) {
					file_log("account", "integrate: moving budget '{$budget}' from #{$account_id} to #{$new_account_id}");
					$res = $db->q("
						INSERT INTO advertise_budget (account_id, budget) VALUES (?, ?) ON DUPLICATE KEY UPDATE budget = budget + ?", 
						[$new_account_id, $budget, $budget]
						);
					$res = $db->q("UPDATE advertise_budget SET budget = NULL WHERE account_id = ?", [$account_id]);
					$msg .= "Moved advertising balance of \${$budget}.\n";
				}
				
				//ban account and update notes
				$ret = $acc->ban("integrate #{$new_account_id}");
				if ($ret) {
					$acc->setNotes($acc->getNotes()." Banned due to integration to new account #{$new_account_id}\n");
					$acc->setPhone(null);
					$acc->setPhoneVerified(null);
					$acc->update();
					
					$res = $db->q("DELETE FROM phone_verification WHERE account_id = ?", [$account_id]);
			
					//notification
					if ($notify) {
						$email = $db->single("SELECT email FROM account WHERE account_id = ?", [$account_id]);
						$new_email = $db->single("SELECT email FROM account WHERE account_id = ?", [$new_account_id]);
						$msg = "Hello,\n\nWe have merged your Adultsearch account #{$account_id} ({$email}) into another account of yours #{$new_account_id} ({$new_email}).\n\nWe allow only 1 account per person/agency on our website. Creating multiple accounts is forbidden.\n\nWe have moved all your assets (classified ads, purchases, top-up credits, advertising balance, etc...), all your active ads remained active. If you want to manage your ads and campaigns of your \"old\" account #{$account_id} ({$email}), you need to log in to our website under \"new\" account #{$new_account_id} ({$new_email}).\n\nPlease contact us at support@adultsearch.com if you have any questions.\n\nDetails or your moved assets:\n{$msg}\n\nHave a nice day,\nteam adultsearch.com\nsupport@adultsearch.com\n";
						$ret2 = send_email([
							"to" => $email,
							"subject" => "Your AS account #{$account_id} was merged into #{$new_account_id}",
							"html" => nl2br($msg),
							"text" => $msg,
							]);
						if ($ret2)
							flash::add(flash::MSG_SUCCESS, "Notification sent to {$email}");
						else
							flash::add(flash::MSG_ERROR, "Failed to send notification email to {$email}");
					}
				}
	
				if ($ret) {
					$msg = "Account #{$account_id} ({$acc->getEmail()}) successfully banned, following items: {$cl_aff} ads, {$cc_aff} cards, {$pu_aff} purchases, {$pa_aff} payments, {$repost} global repost credits";
					if ($budget && $bu_aff)
						$msg .= ", {$budget} budget moved";
					$msg .= " were moved into account #{$new_account_id} ({$new_acc->getEmail()}).";
					return actionSuccess($msg);
				} else {
					return actionWarning("Error while banning account #{$account_id}, moved {$cl_aff} ads, {$cc_aff} cards, {$pu_aff} purchases.");
				}
			}		
		}
	}

	//number of classified ads
	$cl_cnt = 0;
	$res = $db->q("SELECT count(c.id) as cnt FROM classifieds c WHERE c.account_id = ?", array($account_id));
	if ($db->numrows($res)) {
		$row = $db->r($res);
		$cl_cnt = $row["cnt"];
	}
	//number of credit cards
	$cc_cnt = 0;
	$res = $db->q("SELECT count(cc.cc_id) as cnt FROM account_cc cc WHERE cc.account_id = ? AND deleted = 0", array($account_id));
	if ($db->numrows($res)) {
		$row = $db->r($res);
		$cc_cnt = $row["cnt"];
	}
	//number of purchases
	$pu_cnt = 0;
	$res = $db->q("SELECT count(ap.id) as cnt FROM account_purchase ap WHERE ap.account_id = ?", array($account_id));
	if ($db->numrows($res)) {
		$row = $db->r($res);
		$pu_cnt = $row["cnt"];
	}
	//number of payments
	$pa_cnt = 0;
	$res = $db->q("SELECT count(p.id) as cnt FROM payment p WHERE p.account_id = ?", array($account_id));
	if ($db->numrows($res)) {
		$row = $db->r($res);
		$pa_cnt = $row["cnt"];
	}
	//global repost credits
	$repost = intval($db->single("SELECT repost FROM account WHERE account_id = ?", [$account_id]));
	//advertiser budget
	$budget = $db->single("SELECT budget FROM advertise_budget WHERE account_id = ?", array($account_id));

	echo "<h2>Integrate account #{$acc->getId()} - {$acc->getEmail()}</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>New account ID</th><td><input type=\"text\" name=\"new_account_id\" value=\"\" /></td></tr>\n";
	echo "<tr><th># classified ads:</th><td>{$cl_cnt}</td></tr>\n";
	echo "<tr><th># credit cards:</th><td>{$cc_cnt}</td></tr>\n";
	echo "<tr><th># purchases:</th><td>{$pu_cnt}</td></tr>\n";
	echo "<tr><th># payments:</th><td>{$pa_cnt}</td></tr>\n";
	echo "<tr><th># top-up credits:</th><td>{$repost}</td></tr>\n";
	echo "<tr><th>advertiser budget:</th><td>\$".number_format(floatval($budget), 2, ".", "")."</td></tr>\n";
	echo "<tr><th></th><td><input type=\"checkbox\" name=\"notify\" value=\"1\" />Notify by email</td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
	echo "</table>";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$account_id}\" />";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}


$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'add':
		$ret = add();
		if (!$ret)
			return;
		break;
	case 'edit':
		if (!permission::has("account_edit"))
			die;
		$ret = edit();
		if (!$ret)
			return;
		break;
	case 'delete':
		if (!permission::has("account_manage"))
			die;
		$ret = delete();
		if (!$ret)
			return;
		break;
	case 'undelete':
		if (!permission::has("account_manage"))
			die;
		$ret = undelete();
		if (!$ret)
			return;
		break;
	case 'ban':
		if (!permission::has("account_ban"))
			die;
		$ret = ban();
		if (!$ret)
			return;
		break;
	case 'unban':
		if (!permission::has("account_unban"))
			die;
		$ret = unban();
		if (!$ret)
			return;
		break;
	case 'mark_verified':
		if (!permission::has("account_manage"))
			die;
		$ret = mark_verified();
		if (!$ret)
			return;
		break;
	case 'whitelist':
		if (!permission::has("account_whitelist"))
			die;
		$ret = whitelist();
		if (!$ret)
			return;
		break;
	case 'unwhitelist':
		if (!permission::has("account_whitelist"))
			die;
		$ret = unwhitelist();
		if (!$ret)
			return;
		break;
	case 'confirm_registration':
		if (!permission::has("account_confirm"))
			die;
		$ret = confirm_registration();
		if (!$ret)
			return;
		break;
	case 'integrate':
		if (!permission::has("account_manage"))
			die;
		$ret = integrate();
		if (!$ret)
			return;
		break;
	default:
		break;
}

$params = [];
$where = getWhere($params);
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
	$order = "ORDER BY a.account_id DESC";

//query db
$sql = "SELECT count(*) as total
		FROM account a
		$where";
$res = $db->q($sql, $params);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT a.account_id, a.created_stamp, a.banned, a.ban_reason, a.username, a.email, a.account_level, a.password
				, a.last_login_stamp, a.ip_address, a.notes, a.email_confirmed
				, a.phone, a.phone_verified, a.whitelisted, a.deleted, a.advertiser, a.agency, a.ads_limit, a.repost
				, ab.budget
				, COUNT(distinct c.id) as cl_count
				, COUNT(distinct c2.id) as cl_count_not_deleted
				, COUNT(distinct ac.id) as ac_count
		FROM account a
		LEFT JOIN advertise_budget ab on a.account_id = ab.account_id 
		LEFT JOIN classifieds c on c.account_id = a.account_id 
		LEFT JOIN classifieds c2 on c2.account_id = a.account_id and c2.deleted IS NULL and c2.done <> -1
		LEFT JOIN advertise_campaign ac on ac.account_id = a.account_id 
		$where
		GROUP BY a.account_id
		$order
		$limit";
$res = $db->q($sql, $params);
if ($db->numrows($res) == 0) {
	echo "No accounts.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Accounts</h2>\n";
displayFilterForm();

if ($account->isrealadmin()) {
	echo "<a href=\"?action=add\">add</a><br />";
}

echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr>
<th>".getOrderLink("Id", "account_id")."</th>
<th>".getOrderLink("Registered", "created_stamp")."</th>
<th>Status</th>
<th>".getOrderLink("Username", "username")."</th>
<th>".getOrderLink("Email", "email")."</th>
<th>".getOrderLink("Last login", "last_login_stamp")."</th>
<th>IP Address</th>
<th>Notes</th>
<th>Classifieds</th>
<th>Repost Credits</th>
<th>Advertising</th>
<th>".getOrderLink("Phone", "phone")."</th>
<th>Verified</th>
<th>Whitelisted</th>
<th/>
</tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	$deleted = ($rox["deleted"]) ? true : false;

	if ($deleted)
		echo "<tr style=\"background-color: #FFAAAA;\">";
	else
		echo "<tr>";

	echo "<td>{$rox['account_id']}</td>";
	echo "<td>".get_datetime_admin_timezone($rox["created_stamp"])."</td>";

	if ($rox["banned"]) {
		echo "<td><span style=\"color: red; font-weight: bold;\">Banned</span>";
		if ($rox["ban_reason"] == "spammer")
			echo "<br /><span style=\"color: red;\">(spammer)</span>";
		elseif ($rox["ban_reason"] == "chargeback")
			echo "<br /><span style=\"color: red;\">(chargeback)</span>";
		elseif (substr($rox["ban_reason"], 0, 11) == "integrate #")
			echo "<br /><span style=\"color: red;\">(merged into account #".substr($rox["ban_reason"], 11).")</span>";
		echo "</td>";
	} else if ($deleted) {
		echo "<td><span style=\"color: darkred;\">Deleted</span></td>";
	} else {
		echo "<td>Live</td>";
	}

	echo "<td>{$rox["username"]}</td>";
	echo "<td>{$rox["email"]}</td>";

	echo "<td>".get_datetime_admin_timezone($rox["last_login_stamp"])."</td>";

	echo "<td>{$rox["ip_address"]}&nbsp;<a href=\"#\" class=\"geolocate\" title=\"Geolocate this IP address\"><img src=\"/images/control/crosshair_16.png\"/></a></td>";

	$notes = (strlen($rox["notes"] > 20)) ? substr($rox["notes"], 0, 20)."..." : $rox["notes"];
	$notes = htmlspecialchars($notes);
	echo "<td>{$notes}</td>";

	$cl_count = $rox["cl_count"];
	$cl_count_not_deleted = $rox["cl_count_not_deleted"];
	//if ($rox["cl_count"] > 0) {
	if ($rox["cl_count_not_deleted"] > 0) {
		echo "<td><a href=\"/mng/classifieds?account_id={$rox['account_id']}\">{$rox["cl_count_not_deleted"]} cl ad(s)</a></td>";
	} else if ($rox["cl_count"] > 0 && $account->isrealadmin()) {
		echo "<td>Only <a href=\"/mng/classifieds?account_id={$rox['account_id']}&deleted=yes\">{$rox["cl_count"]} deleted cl ad(s)</a></td>";
	} else {
		echo "<td>-</td>";
	}
	
	echo "<td>{$rox["repost"]}<br /><a href=\"/classifieds/topups?account_id={$rox["account_id"]}\">view&nbsp;latest top-ups</a></td>";

	echo "<td>";
	$budget = round($rox["budget"]);
	if ($rox["advertiser"] == 1) {
		echo "<span style=\"color: orange; font-weight: bold;\">CPC/CPM&nbsp;Advertiser</span><br />balance&nbsp;\${$budget}<br />{$rox["ac_count"]}&nbsp;campaigns";
	} else if ($rox["agency"]) {
		echo "<span style=\"color: green; font-weight: bold;\">Escort&nbsp;agency</span><br />balance&nbsp;\${$budget}<span>";
		if (!is_null($rox["ads_limit"]))
			echo "<br />Ads limit: {$rox["ads_limit"]}";
	} else {
		echo "-";
	}
	echo "</td>";
	
	echo "<td>{$rox['phone']}</td>";
	if ($rox["phone_verified"])
		echo "<td>Yes</td>";
	else
		echo "<td>No</td>";
	
	if ($rox["whitelisted"])
		echo "<td>Yes</td>";
	else
		echo "<td>No</td>";

	echo "<td>";
//	if (intval($rox["account_level"]) < $account_level || $account->isrealadmin()) {
	if (intval($rox["account_level"]) < $account_level && permission::has("account_edit")) {
		echo "<a href=\"".getActionLink("edit", $rox['account_id'])."\">edit</a>";
	}

	if (permission::has("account_impersonate"))
		echo "&nbsp;&middot;&nbsp;<a href=\"/account/impersonate?id={$rox['account_id']}\" target=\"_blank\">impersonate</a>";

	if (permission::has("access_admin_sales"))
		echo "&nbsp;&middot;&nbsp;<a href=\"/mng/sales?account_id={$rox["account_id"]}\">sales</a>";

	if (permission::has("account_unban") && $rox["banned"]) {
		echo "&nbsp;&middot&nbsp;<a href=\"".getActionLink("unban", $rox['account_id'])."\">unban</a>";
	}
	if (permission::has("account_ban") && !$rox["banned"]) {
		echo "&nbsp;&middot&nbsp;<a href=\"".getActionLink("ban", $rox['account_id'])."\">ban</a>";
	}

	//if (!$rox["phone_verified"] && $rox['phone'])
	//if (!$rox["phone_verified"])
	//	echo "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("mark_verified", $rox['account_id'])."\">verify</a>";
	
	if (permission::has("account_whitelist")) {
		if (!$rox["whitelisted"]) {
			echo "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("whitelist", $rox['account_id'])."\">whitelist</a>";
		} else {
			echo "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("unwhitelist", $rox['account_id'])."\">unwhitelist</a>";
		}
	}

	if (permission::has("account_manage")) {
		if (!$deleted)
			echo "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("delete", $rox['account_id'])."\">delete...</a>";
		else if ($deleted && $account->isrealadmin())
			echo "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("undelete", $rox['account_id'])."\">undelete...</a>";

		echo "&nbsp;&middot;&nbsp;<a href=\"/mng/audit?type=ACC&p1={$rox['account_id']}\">history</a>";
	}

	if (permission::has("account_confirm") && !$rox["email_confirmed"]) {
		echo "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("confirm_registration", $rox['account_id'])."\">confirm_registration</a>";
	}
	
	if ($account->isrealadmin()) {
		echo "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("integrate", $rox['account_id'])."\">integrate</a>";
	}

	if ($rox["advertiser"] || $rox["agency"])
		echo " &middot; <a href=\"/advertise/advertiser?id={$rox['account_id']}\">advertiser</a>";

	echo "</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;

?>
<script type="text/javascript">
function ip_geolocate(elem) {
	var text = $(elem).parent().text().replace(/[^0-9\.]/g,'');
	$.get('/_ajax/ip_geolocate', { ip: text }, function(resp) {
		if (resp == '')
			resp = '<span style="color: red;">Error</span>';
		$(elem).parent().append('<br />'+resp);
		$(elem).remove();
	});
}
$(document).ready(function() {
	$('.geolocate').each(function(ind,obj) {
		$(obj).click(function(evt) {
			ip_geolocate(evt.currentTarget);
			evt.stopPropagation();
			return false;
		});
	});
});
</script>
