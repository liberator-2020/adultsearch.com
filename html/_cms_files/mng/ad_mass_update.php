<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
require_once(_CMS_ABS_PATH."/inc/classes/parsecsv.lib.php");

define('UPL_DIR', _CMS_ABS_PATH."/tmp");

global $account, $db, $num_errors, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";
    
//we have to be logged in 
if (!$account->isMember()) {
    $account->asklogin(); 
    return false; 
}           
        
if (!$account->isrealadmin())
    die("Invalid access");

$num_errors = 0;

function echo_column_assign($num, $guess) {
	$col_types = array("id", "location");
	echo "<select name=\"col_{$num}\">";
	echo "<option value=\"\">-- Select --</option>";
	foreach ($col_types as $col_type) {
		$selected = "";
		if ($col_type == $guess)
			$selected = " selected=\"selected\" ";
		echo "<option value=\"{$col_type}\"{$selected}>{$col_type}</option>";
	}
	echo "</select>";
}

function er($msg, $fix = "") {
	global $num_errors;
	echo "<span style=\"color: red;\"><strong>Error</strong>: {$msg}</span> {$fix}<br />\n";
	$num_errors++;
}

function get_loc($country, $state, $city) {
	global $mcache;

	$loc_id = "";
	$loc_name = "";
	$country_id = "";
	$state_id = "";
	$state_abbr = "";
	if ($country) {
		$row = $mcache->get("SQL:LOC-CTR-NAM",array($country));
		if ($row === false) {
			er("Cant find country '{$country}'");
			return array();
		}
		$country_id = $row["loc_id"];
	}
	if ($country_id) {
		if ($state) {
			$row = $mcache->get("SQL:LOC2-S-CTR",array($state, $country_id));
			if ($row === false) {
				$row = $mcache->get("SQL:LOC2-NAM-CTR",array($state, $country_id));
				if ($row === false) {
					er("Cant find state '{$state}' for country '{$country}', country_id '{$country_id}'");
					return array();
				}
			}
			$state_id = $row["loc_id"];
			$state_abbr = $row["s"];
		}
		if ($state_id) {
			$row = $mcache->get("SQL:LOC-NAM-PAR",array($city, $state_id));
			if ($row === false) {
				er("Cant find city '{$city}', state={$state}, state_id='{$state_id}'", "<a href=\"/mng/locations?action=add&name={$city}&state={$state}&country_id={$country_id}\">Add {$city} to {$country}</a>.");
				return array();
			}
		} else {
			$row = $mcache->get("SQL:LOC-NAM-CTR",array($city, $country_id));
			if ($row === false) {
				er("Cant find city '{$city}', country_id='{$country_id}'", "<a href=\"/mng/locations?action=add&name={$city}&state={$state}&country_id={$country_id}\">Add {$city} to {$country}</a>.");
				return array();
			}
		}
		$loc_id = $row["loc_id"];
		$loc_name = $row["loc_name"];
	} else {
		$row = $mcache->get("SQL:LOC-NAM",array($city));
		if ($row === false) {
			er("Cant find city '{$city}'");
			return array();
		}
		$loc_id = $row["loc_id"];
		$loc_name = $row["loc_name"];
	}

	if (!$loc_id || !$country_id) {
		er("Loc_id not acquired");
		return array();
	}

	if (in_array($country_id, array(16046, 16047, 41973))) {
		if ($country_id == 16046)
			$link = "http://adultsearch.com{$row["loc_url"]}";
		else if ($country_id = 16047)
			$link = "http://ca.adultsearch.com{$row["loc_url"]}";
		else if ($country_id = 41973)
			$link = "http://uk.adultsearch.com{$row["loc_url"]}";
	} else {
		$link = "http://{$row["country_sub"]}.adultsearch.com/{$row["dir"]}/";
	}

	return array("loc_id" => $loc_id, "loc_name" => $loc_name, "state_id" => $state_id, "state_abbr" => $state_abbr, "country_id" => $country_id, "country_sub" => $row["country_sub"], "link" => $link);
}

/**
 * Looks up location in format "San Diego, CA"
 */
function get_location($location) {
	global $mcache, $db;

	if (($pos = strpos($location, ",")) === false)
		return false;

	$city_name = strtolower(trim(substr($location, 0, $pos)));
	$state_code = trim(substr($location, $pos + 1));
	//echo "city_name={$city_name}, state_code={$state_code}<br />\n";

	$res = $db->q("
		SELECT l.loc_id, lp.loc_id as state_id 
		FROM location_location l 
		INNER JOIN location_location lp on lp.loc_id = l.loc_parent 
		WHERE l.loc_name like ? AND lp.s LIKE ? AND l.loc_lat IS NOT NULL",
		[$city_name, $state_code]
		);

	if ($db->numrows($res) != 1) {
		return false;
	}

	$row = $db->r($res);

	return [
		"loc_id" => $row["loc_id"],
		"state_id" => $row["state_id"],
		];
}

function process() {
	global $db, $config_image_path, $mcache, $num_errors;

	$system = new system;
	set_time_limit(0);
	$report_file_name = "newly_added_".date("Ymd_His").".html";
	$report_file_path = _CMS_ABS_PATH."/tmp/".$report_file_name;
	$report_file_url = "http://adultsearch.com/tmp/{$report_file_name}";

	//checks
	if (!is_dir(UPL_DIR)) {
		echo "<span style=\"color: red;\">Tmp upload dir '".UPL_DIR."' does not exists !</span><br />";
		return false;
	}

	$live = intval($_REQUEST["live"]);

	$filepath = UPL_DIR."/"."ad_mass_update.csv";
	if ($live == 0) {
		//store csv file into temp directory
		$file = new Upload($_FILES['file']);
		if ($file->uploaded) {
			@unlink($filepath);
			$file->file_new_name_body = "ad_mass_update";
			$file->file_new_name_ext = "csv";
			$file->Process(UPL_DIR."/");
			if (!$file->processed) {
				echo "<span style=\"color: red;\">Error while copying uploaded csv file to directory '".UPL_DIR."' !</span><br />";
				return false;
			}
		}
	}
	
	//parse csv file
	$csv = new parseCSV();
	$csv->auto($filepath);
	$num_entries = count($csv->data);
	if ($num_entries > 0)
		echo "Parsed CSV file: <strong>{$num_entries}</strong> entries.<br />\n";
	else {
		echo "<span style=\"color: red;\">Error: CSV file does not have any entry.</span><br />\n";
		return;
	}

	if ($live == 0) {
		//form for column assign
		echo "<form>";
        echo "<input type=\"hidden\" name=\"action\" value=\"process\" />";
        echo "<input type=\"hidden\" name=\"live\" value=\"1\" />";
		echo "<h3>Column assign</h3>\n";
		echo "<table>\n";
		$row1 = array_shift($csv->data);
		$num_fields = count($row1);
		for ($i = 0; $i < $num_fields; $i++) {
			$guess = "";
			$title = array_shift($csv->titles);
			$val = array_shift($row1);
			if (in_array(strtolower($title), array("id")))
				$guess = "id";
			else if (in_array(strtolower($title), array("location")))
                $guess = "location";
			echo "<tr><td>{$i}</td><td>".htmlspecialchars($val)."</td><td>";
			echo_column_assign($i, $guess);
			echo "</td></tr>\n";
		}
		echo "</table>";
        echo "<input type=\"submit\" name=\"submit\" value=\"Yes\" />";
		echo "</form>\n";
		return;	
	}

	$col_types = array();
	$i = 0;
	while (1) {
		if (isset($_REQUEST["col_{$i}"])) {
			$col_types[$i] = preg_replace("/[^a-zA-Z]/", "", $_REQUEST["col_{$i}"]);
		} else {
			break;
		}
		$i++;
	}

	if ($live == 1 ) {
		echo "<h3>Import check</h3>\n";
?>
<style type="text/css">
table.comp {
	border-collapse: collapse;
}
table.comp td {
	border: 1px solid #ccc;
	padding: 3px;
}
</style>
<?php

		foreach ($csv->data as $key => $row) {
			$id = "";
			$location = "";
			$i = 0;
			foreach ($row as $value) {
				if ($col_types[$i] == "id")
					$id = $value;
				else if ($col_types[$i] == "location")
					$location = $value;
				$i++;
			}

			//mandatory fields
			if (!$id) {
				er("Id not filled");
				continue;
			}
		
			$res = $db->q("SELECT * FROM classifieds c INNER JOIN classifieds_loc cl on cl.post_id = c.id WHERE c.id = ?", [$id]);
			if (!$db->numrows($res)) {
				echo "Classified Ad #{$id} not found in database !<br />\n";
				$num_errors++;
			}

			//$loc = get_loc($country, $state, $city);
			$loc = get_location($location);

			if (!$loc) {
				echo "New location {$location} not found for ad #id {$id} !<br />\n";
				$num_errors++;
			}
		}
	
		if ($num_errors == 0) {
			echo "0 errors, do you want to update these ads ?<br />\n";
			echo "<form>";
			foreach ($col_types as $key => $val) {
				echo "<input type=\"hidden\" name=\"col_{$key}\" value=\"{$val}\" />";
			}
			echo "<input type=\"hidden\" name=\"action\" value=\"process\" />";
			echo "<input type=\"hidden\" name=\"live\" value=\"2\" />";
			echo "<input type=\"submit\" name=\"submit\" value=\"Yes\" />";
			echo "</form>\n";
		} else {
			echo "{$num_errors} errors.<br />Fix the errors first.<br />\n";
		}

		return;
	}

	if ($live == 2) {

		$cnt_success = 0;	
		$cnt_failed = 0;	

		$handle = fopen($report_file_path, "w");
		if (!$handle)
			die("Cant open report file!");

		foreach ($csv->data as $key => $row) {
			$id = "";
			$location = "";
			$i = 0;
			foreach ($row as $value) {
				if ($col_types[$i] == "id")
					$id = $value;
				else if ($col_types[$i] == "location")
					$location = $value;
				$i++;
			}

			//$loc = get_loc($country, $state, $city);
			$loc = get_location($location);

			if (!$loc)
				continue;
		
			$loc_id = $loc["loc_id"];
			$state_id = intval($loc["state_id"]);
			//echo "loc_id = {$loc_id}, state_id={$state_id}, country_id={$country_id}<br />\n";

			//echo "UPDATE classifieds_loc SET loc_id = $loc_id, state_id = $state_id WHERE post_id = $id LIMIT 1<br />\n";$aff = 1;
			$res = $db->q("UPDATE classifieds_loc SET loc_id = ?, state_id = ? WHERE post_id = ? LIMIT 1", [$loc_id, $state_id, $id]);
			$aff = $db->affected($res);
			if ($aff != 1) {
				$cnt_failed++;
				echo "<span style=\"color: red;\">UPDATE classifieds_loc FAILED, loc_id = {$loc_id}, state_id = {$state_id}, post_id = {$id}</span><br />\n";
			} else {
				$cnt_success++;
				echo "Ad #{$id} successfully updated<br />\n";
			}
		}

		echo "Successfully updated: {$cnt_success} ads.<br />\n";
		echo "Failed to update: {$cnt_failed} ads.<br />\n";
		fclose($handle);
		echo "Report link: <a href=\"{$report_file_url}\">{$report_file_url}</a><br />\n";

	}

	return true;
}

?>
<h1>Ad mass update</h1>
<p>Currently supported only locatoin update</p>
<?php

switch ($_REQUEST["action"]) {
	case 'process':
		$ret = process();
		if (!$ret)
			return;
		break;
}

?>
<form action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="action" value="process" />
<input type="hidden" name="live" value="0" />
Please select CSV file :<input type="file" name="file" />
<input type="submit" name="submit" value="Upload" />
</form>
