<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!account::ensure_admin())
    die("Invalid access");

echo "<h2>IP Geolocation</h2>\n";

if ($_REQUEST["submit"] == "Check IP") {
	$ip = $_REQUEST["ip"];
	echo "IP: {$ip}<br />\n";

	$result = geolocation::getLocationByIp($ip);
	echo "Result of geolocation::getLocationByIp():<br /><pre>\n".print_r($result, true)."\n</pre><br />";

	$location = geolocation::getClosestNonEmptyLocationByIp($ip);
	if (!$location)
		echo "No closest significant location for ip '{$ip}' ?!<br />\n";
	else
		echo "Closest significant location for ip {$ip} is <a href=\"{$location->getUrl()}\"><strong>{$location->getLabel()}</strong></a><br /><br />\n";
}


?>
<strong>Check IP:</strong><br />
<form action="">
<input type="text" name="ip" value="<?php echo $_REQUEST["ip"]; ?>" style="width: 300px;"/>
<input type="submit" name="submit" value="Check IP" />
</form>

