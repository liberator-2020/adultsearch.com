<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) {
    $account->asklogin();
    return false;
}

if (!$account->isadmin())
    die("Invalid access");

global $url_last_piece, $filters, $where_cols;

global $important_types;
$important_types = array(
	"CLA" => "Classified ad",
	"ACA" => "Advertising campaign",
	);

//configuration
$url_last_piece = "admin_important";
$where_cols = array(
	"id" => array("title" => "Id", "where" => "id", "match" => "exact", "size" => 5),
	"type"     => array("title" => "Type", "where" => "type", "match" => "exact", "type" => "select", "options" => array("" => "-") + $important_types),
	"item_id" => array("title" => "Item id", "where" => "item_id", "size" => 5),
);

function add() {
	global $db;

	if (!isset($_REQUEST["u_submit"])) {
		echo "<h2>Add new important item</h2>";
		echo "<form method=\"post\" action=\"\">";
		echo "<table>";
		echo "<tr><th>Type:</th><td><select name=\"u_type\"><option value=\"CLA\">Classified ad</option><option value=\"ACA\">Advertising campaign</option></select></td></tr>\n";
		echo "<tr><th>Item id:</th><td><input type=\"text\" name=\"u_item_id\" value=\"\" /></td></tr>\n";
		echo "<tr><th></th><td><input type=\"submit\" name=\"u_submit\" value=\"Submit\" /></td></tr>\n";
		echo "</table>";
		echo "</form>";
		return true;
	}

	//were going live
	$type = $_REQUEST["u_type"];
	$item_id = $_REQUEST["u_item_id"];
	$res = $db->q("INSERT INTO admin_important (type, item_id) VALUES (?, ?)", array($type, $item_id));
	$id = $db->insertid();
	if ($db->affected($res) == 1) {
		audit::log("AIM", "Add", $id, "Type={$type},Item_id={$item_id}");
		return actionSuccess("New important item has been added.");
	} else {
		return actionError("Error while adding new important item - please contact administrator!");
	}
}

function delete() {
	global $db, $important_types;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid admin watch id !");

	$res = $db->q("SELECT type, item_id FROM admin_important WHERE id = ?", array($action_id));
	if ($db->numrows($res) != 1) {
		return actionError("Cant find admin important item with id #{$action_id} !");
	}
	$row = $db->r($res);
	$type = $row["type"];
	$item_id = $row["item_id"];

	if (!isset($_REQUEST["u_submit"])) {
		echo "<h2>Delete admin important item - {$important_types[$type]} #{$item_id} ?</h2>";
		echo "<form method=\"post\" action=\"\">";
		echo "<table>";
		echo "<tr><th></th><td><input type=\"submit\" name=\"u_submit\" value=\"Submit\" /></td></tr>\n";
		echo "</table>";
		echo "<input type=\"hidden\" name=\"action_id\" value=\"{$action_id}\" />";
		echo getFilterFormFields();
		echo "</form>";
		return false;
	}

	//were going live
	$res = $db->q("DELETE FROM admin_important WHERE id = ? LIMIT 1", array($action_id));
    if ($db->affected($res) == 1) {
		audit::log("AIM", "Delete", $action_id, "Type={$type},Item_id={$item_id}");
		return actionSuccess("Admin important item - {$important_types[$type]} #{$action_id} was successfully deleted.");
	} else {
		return actionError("Error while deleting admin importnt item - {$important_types[$type]} #{$action_id} - please contact administrator!");
	}

	return true;
}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'add':
		$ret = add();
		if (!$ret)
			return;
		break;
	case 'delete':
		$ret = delete();
		if (!$ret)
			return;
		break;
}

$where = getWhere();
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
	$order = "ORDER BY id ASC";

//query db
$sql = "SELECT count(*) as total
		FROM admin_important
		$where";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT id, type, item_id
		FROM admin_important
		$where
		$order 
		$limit";
$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No admin important items.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Important classified ads and advertising campaigns</h2>\n";
echo "<a href=\"?action=add\">Add new important item</a><br />\n";
displayFilterForm();
echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr><th>".getOrderLink("Id", "id")."</th><th>".getOrderLink("Type", "type")."</th><th>".getOrderLink("Item id", "item_id")."</th><th/></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<td>{$rox['id']}</td>";
	echo "<td>{$important_types[$rox['type']]}</td>";
	echo "<td>{$rox['item_id']}</td>";
	echo "<td><a href=\"".getActionLink("delete", $rox['id'])."\">delete</a></td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;


?>
