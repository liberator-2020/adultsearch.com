<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) {
    $account->asklogin();
    return false;
}

if (!permission::access("settings_manage"))
    die("Invalid access");

global $url_last_piece, $filters, $where_cols;

//configuration
$url_last_piece = "admin_settings";
$where_cols = array(
	"id" => array("title" => "ID", "where" => "id", "match" => "exact", "size" => "7"),
	"name" => array("title" => "Name", "where" => "name", "size" => "15"),
);


function edit() {
	global $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid id !");

	$res = $db->q("SELECT * FROM admin_settings WHERE id = ?", array($action_id));
	if ($db->numrows($res) != 1)
		return actionError("Invalid id !");
	$row = $db->r($res);
	$name = $row["name"];
	$comment = $row["comment"];
	$value = str_replace(",", "\n", $row["value"]);

	if (isset($_REQUEST["submit"])) {
		$updated = "";
		if (isset($_REQUEST["u_value"])) {
			$value = str_replace(array("\r\n", "\r"), ",", trim($_REQUEST["u_value"]));
			$value = preg_replace('/[^0-9A-Za-z, \.\n]/', '', $value);
			$db->q("UPDATE admin_settings SET value = ? WHERE id = ?", array($value, $action_id));
			return actionSuccess("You have successfully updated settings.");
		} else {
			die("value not set!");
		}
	}

	echo "<h2>Edit setting #{$action_id}</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Setting ID</th><td>{$action_id}</td></tr>\n";
	echo "<tr><th>Name</th><td>".htmlspecialchars($name)."</td></tr>\n";
	echo "<tr><th>Comment</th><td>".htmlspecialchars($comment)."</td></tr>\n";
	echo "<tr><th>Value</th><td><textarea name=\"u_value\" cols=\"80\" rows=\"6\">".htmlspecialchars($value)."</textarea></td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
	echo "</table>";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$action_id}\" />";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}	


$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'edit':
		if (!permission::has("settings_manage"))
			die("no privileges!");
		$ret = edit();
		if (!$ret)
			return;
		break;
	default:
		break;
}

$where = getWhere();
$order = getOrder();
$limit = getLimit();

//query db
$sql = "SELECT count(*) as total 
		FROM admin_settings
		$where";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT *
		FROM admin_settings
		$where
		$order
		$limit";
$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No settings.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Settings</h2>\n";
displayFilterForm();

echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr>
<th>".getOrderLink("Id", "id")."</th>
<th>".getOrderLink("Name", "name")."</th>
<th>".getOrderLink("Comment", "comment")."</th>
<th>".getOrderLink("Value", "value")."</th>
<th/>
</tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
    echo "<tr>";

	echo "<td>{$rox['id']}</td>";
	echo "<td>{$rox['name']}</td>";
	echo "<td>{$rox['comment']}</td>";
	echo "<td>".str_replace(",", "<br />", $rox['value'])."</td>";

	echo "<td>";
	echo "<a href=\"".getActionLink("edit", $rox['id'])."\">edit</a>";
	echo "</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;


?>
