<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("account_manage"))
    die("Invalid access");

global $url_last_piece, $filters, $where_cols;

//configuration
$url_last_piece = "admin_spammers";
$where_cols = array(
	"id" => array("title" => "Id", "where" => "id", "match" => "exact", "size" => 5),
	"email" => array("title" => "Email", "where" => "email", "size" => 15),
	"ip_address" => array("title" => "IP Address", "where" => "ip_address", "size" => 15),
	"password" => array("title" => "Password", "where" => "password", "size" => 15),
);

function add() {
	global $db;

	$email = $_REQUEST["email"];
	$ip_address = $_REQUEST["ip_address"];
	$password = $_REQUEST["password"];

	$spammer = new spammer();

	if ($email) {
		if (spammer::isEmailOnSpamList($email))
			return actionError("Error: Email pattern '{$email}' already is in spam list !");
		$spammer->setEmail($email);
		$ret = $spammer->add();
		if ($ret)
			return actionSuccess("Email pattern '{$email}' has been added to spam list.");
		else
			return actionError("Error while adding email pattern '{$email}' to spam list - please contact administrator!");
	} else if ($ip_address) {
		if (spammer::isIpOnSpamList($ip_address))
			return actionError("Error: IP Address '{$ip_address}' already is in spam list !");
		$spammer->setIpAddress($ip_address);
		$ret = $spammer->add();
		if ($ret)
			return actionSuccess("IP Address '{$ip_address}' has been added to spam list.");
		else
			return actionError("Error while adding ip address '{$ip_address}' to spam list - please contact administrator!");
	} else if ($password) {
		if (spammer::isPasswordOnSpamList($password))
			return actionError("Error: Password '{$password}' already is in spam list !");
		$spammer->setPassword($password);
		$ret = $spammer->add();
		if ($ret)
			return actionSuccess("Password '{$password}' has been added to spam list.");
		else
			return actionError("Error while adding password '{$password}' to spam list - please contact administrator!");
	} else {
		return actionError("Error while adding new item to spam list - no email/ip/password specified");
	}
}

function remove() {
	global $db;

	$email = $_REQUEST["email"];
	$ip_address = $_REQUEST["ip_address"];
	$password = $_REQUEST["password"];

	$spammer = new spammer();

	if ($email) {
		$spammer = spammer::findOneByEmail($email);
        if (!$spammer)
            $spammer = spammer::findOneByEmail(spammer::getDomainPart($email));
		if (!$spammer)
			return actionWarning("Error: Email pattern '{$email}' is not in spam list !");
		if ($spammer->remove())
			return actionSuccess("Email pattern '{$email}' has been removed from spam list.");
		else
			return actionError("Error while removing email pattern '{$email}' from spam list - please contact administrator!");
	} else if ($ip_address) {
		$spammer = spammer::findOneByIp($ip_address);
		if (!$spammer)
			return actionWarning("Error: IP Address '{$ip_address}' is not in spam list !");
		if ($spammer->remove())
			return actionSuccess("IP address '{$ip_address}' has been removed from spam list.");
		else
			return actionError("Error while removing IP address '{$ip_address}' from spam list - please contact administrator!");
	} else if ($password) {
		$spammer = spammer::findOneByPassword($password);
		if (!$spammer)
			return actionWarning("Error: Password '{$password}' is not in spam list !");
		if ($spammer->remove())
			return actionSuccess("Password '{$password}' has been removed from spam list.");
		else
			return actionError("Error while removing password '{$password}' from spam list - please contact administrator!");
	} else {
		return actionError("Error while adding new item to spam list - no email/ip/password specified");
	}
}

function delete() {
	global $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid admin spammer id !");

	$spammer = spammer::findOneById($action_id);
	if (!$spammer) {
		return actionError("Cant find admin spammer with id #{$action_id} !");
	}

	if (!isset($_REQUEST["u_submit"])) {
		echo "<h2>Delete spammer #{$action_id} ?</h2>";
		echo "<form method=\"post\" action=\"\">";
		echo "<table>";
		echo "<tr><th></th><td><input type=\"submit\" name=\"u_submit\" value=\"Submit\" /></td></tr>\n";
		echo "</table>";
		echo "<input type=\"hidden\" name=\"action_id\" value=\"{$action_id}\" />";
		echo getFilterFormFields();
		echo "</form>";
		return false;
	}

	//were going live
	$ret = $spammer->remove();
	if ($ret) {
		return actionSuccess("Spammer #{$action_id} was successfully deleted.");
	} else {
		return actionError("Error while deleting spammer #{$action_id} - please contact administrator!");
	}

	return true;
}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'add':
		$ret = add();
		if (!$ret)
			return;
		break;
	case 'remove':
		$ret = remove();
		if (!$ret)
			return;
		break;
	case 'delete':
		$ret = delete();
		if (!$ret)
			return;
		break;
}

$params = [];
$where = getWhere($params);
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
	$order = "ORDER BY id ASC";

//query db
$sql = "SELECT count(*) as total
		FROM admin_spammer
		$where";
$res = $db->q($sql, $params);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT id, email, ip_address, password
		FROM admin_spammer
		$where
		$order 
		$limit
		";
$res = $db->q($sql, $params);
if ($db->numrows($res) == 0) {
	echo "No spammers.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Spam list</h2>\n";
?>
<strong>Add new items to spam list</strong><br />
<form action="">
Email: 
<input type="text" name="email" value="" />
<input type="submit" name="submit" value="Add Email to spam list" />
<input type="hidden" name="action" value="add" />
</form>
<form action="">
IP Address: 
<input type="text" name="ip_address" value="" />
<input type="submit" name="submit" value="Add IP Address to spam list" />
<input type="hidden" name="action" value="add" />
</form>
<form action="">
Password: 
<input type="text" name="password" value="" />
<input type="submit" name="submit" value="Add Password to spam list" />
<input type="hidden" name="action" value="add" />
</form>
<br />

<strong>Remove items from spam list</strong><br />
<form action="">
Email: 
<input type="text" name="email" value="" />
<input type="submit" name="submit" value="Remove Email from spam list" />
<input type="hidden" name="action" value="remove" />
</form>
<form action="">
IP Address: 
<input type="text" name="ip_address" value="" />
<input type="submit" name="submit" value="Remove IP Address from spam list" />
<input type="hidden" name="action" value="remove" />
</form>
<form action="">
Password: 
<input type="text" name="password" value="" />
<input type="submit" name="submit" value="Remove Password from spam list" />
<input type="hidden" name="action" value="remove" />
</form>
<br />

<strong>List</strong><br />
<?php
displayFilterForm();
echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr>
<th>".getOrderLink("Id", "id")."</th>
<th>".getOrderLink("Email", "email")."</th>
<th>".getOrderLink("IP Address", "ip_address")."</th>
<th>".getOrderLink("Password", "password")."</th>
<th/></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<td>{$rox['id']}</td>";
	echo "<td>{$rox['email']}</td>";
	echo "<td>{$rox['ip_address']}</td>";
	echo "<td>{$rox['password']}</td>";
	echo "<td><a href=\"".getActionLink("delete", $rox['id'])."\">delete</a></td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;


?>
