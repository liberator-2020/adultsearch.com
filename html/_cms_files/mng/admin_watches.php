<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) {
    $account->asklogin();
    return false;
}

if (!$account->isadmin())
    die("Invalid access");

global $url_last_piece, $filters, $where_cols;

//configuration
$url_last_piece = "admin_watches";
$where_cols = array(
	"id" => array("title" => "Id", "where" => "id", "match" => "exact", "size" => 5),
	"password" => array("title" => "Password", "where" => "password", "size" => 12),
);

function add() {
	global $db;

	if (!isset($_REQUEST["submit"])) {
		echo "<h2>Add new password to watch</h2>";
		echo "<form method=\"post\" action=\"\">";
		echo "<table>";
		echo "<tr><th>Password:</th><td><input type=\"text\" name=\"u_password\" value=\"\" size=\"20\" /></td></tr>\n";
		echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
		echo "</table>";
		echo "</form>";
		return true;
	}

	//were going live
	$password = $_REQUEST["u_password"];
	$res = $db->q("INSERT INTO admin_watch (password) VALUES (?)", array($password));
	if ($db->affected($res) == 1) {
		return actionSuccess("New password to watch has been added.");
	} else {
		return actionError("Error while adding new password to watch - please contact administrator!");
	}
}

function delete() {
	global $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid admin watch id !");

	$res = $db->q("SELECT password FROM admin_watch WHERE id = ?", array($action_id));
	if ($db->numrows($res) != 1) {
		return actionError("Cant find admin watch with id #{$action_id} !");
	}
	$row = $db->r($res);
	$password = $row["password"];

	if (!isset($_REQUEST["submit"])) {
		echo "<h2>Delete admin watch - password {$password} ?</h2>";
		echo "<form method=\"post\" action=\"\">";
		echo "<table>";
		echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
		echo "</table>";
		echo "<input type=\"hidden\" name=\"action_id\" value=\"{$action_id}\" />";
		echo getFilterFormFields();
		echo "</form>";
		return true;
	}

	//were going live
	$res = $db->q("DELETE FROM admin_watch WHERE id = ? LIMIT 1", array($action_id));
    if ($db->affected($res) == 1) {
		return actionSuccess("Admin watch id #{$action_id} was successfully deleted.");
	} else {
		return actionError("Error while deleting admin watch id #{$action_id} - please contact administrator!");
	}

	return true;
}

function accounts() {
	global $db;
	
	$res = $db->q("SELECT a.password, a.account_id, a.created_stamp, a.email
					FROM account a
					INNER JOIN admin_watch aw ON aw.password = a.password
					WHERE a.banned = 0
					ORDER BY a.account_id DESC
					LIMIT 200");
	echo "<table>";
	while ($row = $db->r($res)) {
		echo "<tr><td>{$row["password"]}</td><td>".get_datetime_admin_timezone($row["created_stamp"])."</td><td><a href=\"/mng/accounts?account_id={$row["account_id"]}\">{$row["account_id"]} - {$row["email"]}</a></td></tr>\n";
	}
	echo "</table>";
	return true;
}


$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'add':
		$ret = add();
		if (!$ret)
			return;
		break;
	case 'delete':
		$ret = delete();
		if (!$ret)
			return;
	case 'accounts':
		$ret = accounts();
		if (!$ret)
			return;
		break;
}

$where = getWhere();
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
	$order = "ORDER BY id ASC";

//query db
$sql = "SELECT count(*) as total
		FROM admin_watch
		$where";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT id, password
		FROM admin_watch
		$where
		$order 
		$limit";
$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No admin watches.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Password watches</h2>\n";
echo "<a href=\"?action=accounts\">Not banned accounts</a><br />\n";
echo "<a href=\"?action=add\">Add new password watch</a><br />\n";
displayFilterForm();
echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr><th>".getOrderLink("Id", "id")."</th><th>".getOrderLink("Password", "password")."</th><th/></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<td>{$rox['id']}</td>";
	echo "<td>{$rox['password']}</td>";
	echo "<td><a target=\"_blank\" href=\"/mng/sales?password={$rox['password']}\">sales</a>&nbsp;&middot;&nbsp;<a href=\"".getActionLink("delete", $rox['id'])."\">delete</a></td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;


?>
