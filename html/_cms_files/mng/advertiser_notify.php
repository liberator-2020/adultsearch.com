<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!account::ensure_admin())
	die("Invalid access");

global $config;

echo "<h1>Notify Advertisers</h1>";

$error = null;
$who = $_REQUEST["who"];
$what = $_REQUEST["what"];
$subject = $_REQUEST["subject"];
$header = $_REQUEST["header"];
$submit_label = "Submit";
$live = intval($_REQUEST["live"]);

function createBatchMessage($batch_id, $account_id, $email, $subject, $header) {
	global $db;

	//determine text
	$smarty = GetSmartyInstance();
	$smarty->assign("account_id", $account_id);
	$html = $smarty->fetch("wire_info.tpl");
	$html = "{$header}{$html}<br /><br />Thank you.<br /><a href=\"agency@adultsearch.com\">agency@adultsearch.com</a>";
	$text = preg_replace('/\t/', '', strip_tags(br2nl($html)));

	$res2 = $db->q("
		INSERT INTO blast_message 
		(batch_id, `from`, `to`, subject, content_html, content_text)
		VALUES
		(?, ?, ?, ?, ?, ?)
		",
		[$batch_id, "agency@adultsearch.com", $email, $subject, $html, $text]
		);
	$message_id = $db->insertid($res2);
	if (!$message_id) {
		error_redirect("Failed to creat blast message !", "/mng/advertiser_notify");
		return false;
	}
	
	return true;
}

if (isset($_REQUEST["live"]) && !$error) {

	if (!in_array($who, ["agency", "myself"])) {
		$error = "Please select recipient";

	} else if ($live == 1) {

		//create email blast batch
		$batch_name = "Advertiser new wire info notification";
		$res = $db->q("INSERT INTO blast_batch (stamp, author_id, type, name) VALUES (?, ?, ?, ?)", [time(), $account->getId(), "E", $batch_name]);
		$batch_id = $db->insertid($res);
		if (!$batch_id)
			return error_redirect("Failed to creat blast batch !", "/mng/advertiser_notify");

		//get all recipients and create batch messages
		$i = 0;
echo "who='{$who}'";
		if ($who == "agency") {
			$res = $db->q("SELECT a.account_id, a.email FROM account a WHERE a.agency = 1 AND a.banned <> 1");
			while ($row = $db->r($res)) {
				$account_id = $row["account_id"];
				$email = $row["email"];

				$ret = createBatchMessage($batch_id, $account_id, $email, $subject, $header);
				if (!$ret)
					return;

				$i++;
			}
		} elseif ($who == "myself") {
			$ret = createBatchMessage($batch_id, $account->getId(), $account->getEmail(), $subject, $header);
			if (!$ret)
				return;
			$i = 1;
		}
		
		$db->q("UPDATE blast_batch SET cnt_total = ? WHERE id = ? LIMIT 1", [$i, $batch_id]);

		//redirect to blast send page
		system::go("/mng/blast_view?batch_id={$batch_id}");

	} else {
		$recipientCount = 0;
		if ($who == "agency") {
			$recipientCount = $db->single("SELECT COUNT(*) FROM account a WHERE a.agency = 1 AND a.banned <> 1");
		} elseif ($who == "myself") {
			$recipientCount = 1;
		}

		echo "Going to send email to {$recipientCount} recipients.<br /><br />\n";
		$live = 1;
		$submit_label = "Yes, send now";

	}
} else {
	$subject = "Our new bank wire details";
	$header = "Hello,<br /><br />We have changed our bank wire details. Please make sure when you send money to us you use the new wire details below:<br /><br />";
}

if ($error)
	echo "<span class=\"error\" style=\"color: red; font-weight: bold;\">{$error}</span><br />\n";

?>
<form method="post">
<table>
<tr>
	<td/>
	<td>
		<input type="radio" name="who" value="agency" <?php if ($who == "agency") echo "checked=\"checked\""; ?> />Escort Agencies
		<input type="radio" name="who" value="myself" <?php if ($who == "myself") echo "checked=\"checked\""; ?> />Myself
	</td>
</tr>
<tr>
	<td/>
	<td>
		<input type="radio" name="what" value="wire_info" checked="checked">New Wire Info
	</td>
</tr>
<tr>
	<td>Subject of email:</td>
	<td>
		<input type="text" name="subject" value="<?php echo htmlspecialchars($subject);?>" style="width: 500px;"/>
	</td>
</tr>
<tr>
	<td>Header of HTML email:</td>
	<td>
		<textarea name="header" style="width: 500px; height: 100px;"><?php echo htmlspecialchars($header);?></textarea>
	</td>
</tr>
<tr>
	<td>
	</td><td><button type="submit"><?php echo $submit_label;?></button></td>
</tr>
</table>
<input type="hidden" name="live" value="<?php echo $live;?>" />
</form>
