<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) { 
    $account->asklogin(); 
    return false; 
}

if (!$account->isWorker()) {
	echo "You dont have privileges to access this page!";
	die;
}

global $url_last_piece, $filters, $where_cols;

//configuration
$url_last_piece = "attributes";

$order = getOrder();
$limit = getLimit();

//get country_id -> country_url_name
$sql = "SELECT loc_id, dir FROM location_location where loc_type = 1";
$countries = array();
$res = $db->q($sql);
while ($row = $db->r($res)) {
	$countries[$row["loc_id"]] = $row["dir"];
}
//_darr($countries);

//query db
$sql = "select count(*) as total from place_picture where place_file_type_id = 2";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT a.* 
		FROM attribute a 
		LEFT JOIN location_attribute la on a.attribute_id = la.attribute_id 
		$order 
		$limit";
$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No attributes.";
	return;
}

//pager
$pager = getPager($total);

//output
echo "<h2>Attributes</h2>\n";
echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr><th>".getOrderLink("Id", "attribute_id")."</th><th>".getOrderLink("Name", "name")."</th><th>".getOrderLink("Type", "type")."</th><th></th></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<tr><td>{$rox['attribute_id']}</td><td>{$rox['name']}</td><td>{$rox['type']}</td><td>{$edit}</td></tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;


?>
