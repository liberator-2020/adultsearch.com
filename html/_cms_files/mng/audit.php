<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("access_admin_audit"))
    return;

global $url_last_piece, $filters, $where_cols, $classifieds;

//configuration
$url_last_piece = "audit";
$where_cols = array(
	"id"         => array("title" => "ID", "size" => "7"),
	"type"       => array(
		"title" => "Type",
		"where" => "type",
		"match" => "exact",
		"type" => "select",
		"options" => array(
			""    => "-",
			"ABU" => "Advertise budget",
			"ACA" => "Advertise campaign",
			"AFD" => "Advertise flat deal",
			"AZO" => "Advertise zone",
			"ACC" => "Account",
			"AIM" => "Admin important",
			"CLA" => "Classified ad",
			"CC"  => "Credit Card",
			)
		),
	"p1"         => array("title" => "Item", "where" => "p1"),
	"subtype"    => array("title" => "Subtype", "size" => "10"),
	"email"      => array("title" => "Email", "where" => "a.email"),
);

$filters = getFilters();

switch ($_REQUEST["action"]) {
	/*
	case 'aaa':
		$ret = aaa();
		if (!$ret)
			return;
		break;
	*/
}

$params = [];
$where = getWhere($params);
$having = getHaving();
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
    $order = "ORDER BY au.id DESC";

//query db
$sql = "SELECT count(*) as total 
		FROM audit au
		LEFT JOIN account a on a.account_id = au.who
		$where
		";
$res = $db->q($sql, $params);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT au.*, a.email, a.password
		FROM audit au
		LEFT JOIN account a on a.account_id = au.who
		$where
		$order 
		$limit";

$res = $db->q($sql, $params);
if ($db->numrows($res) == 0) {
	echo "No audit logs.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>History logs</h2>\n";

displayFilterForm();

echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr>
<th>".getOrderLink("Id", "id")."</th>
<th>".getOrderLink("Created", "created")."</th>
<th>".getOrderLink("Type", "type")."</th>
<th>".getOrderLink("Item", "p1")."</th>
<th>".getOrderLink("Subtype", "subtype")."</th>
<th>".getOrderLink("Who", "who")."</th>
<th>".getOrderLink("Message", "message")."</th>
<th>".getOrderLink("Email", "email")."</th>
<th/></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<tr>";
	echo "<td>{$rox['id']}</td>";
	echo "<td>{$rox["created"]}</td>";
	echo "<td>{$rox["type"]}</td>";
	echo "<td>{$rox["p1"]}</td>";
	echo "<td>{$rox["subtype"]}</td>";
	
	if ($rox["email"]) {
		echo "<td><a href=\"/mng/accounts?account_id={$rox["who"]}\">{$rox["who"]}</a></td>";
	} else {
		echo "<td>{$rox["who"]}</td>";
	}

	echo "<td>{$rox["message"]}</td>";

	if ($rox["email"]) {
		echo "<td>{$rox["email"]}</td>";
	} else {
		echo "<td></td>";
	}

	$links = "";	
	echo "<td>{$links[0]}</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;

?>
