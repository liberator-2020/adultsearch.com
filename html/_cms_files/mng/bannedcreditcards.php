<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("access_admin_sales"))
	return;

global $url_last_piece, $filters, $where_cols;

//configuration
$url_last_piece = "bannedcreditcards";
$where_cols = array(
	"id" => array("title" => "CC.Id", "where" => "c.id", "size" => 5),
	"cc_first4" => array("title" => "CC#: First 4 digits", "where" => "c.cc", "position" => "first", "size" => 5),
    "cc_last4" => array("title" => "CC#: Last 4 digits", "where" => "c.cc", "position" => "last", "size" => 5),
	"firstname" => array("title" => "Firstname", "where" => "c.firstname", "size" => 12),
	"lastname" => array("title" => "Lastname", "where" => "c.lastname", "size" => 14),
	"address" => array("title" => "Address", "where" => "c.address", "size" => 12),
	"city" => array("title" => "City", "where" => "c.city", "size" => 12),
	"state" => array("title" => "State", "where" => "c.state", "size" => 12),
	"shared"   => array(
		"title" => "Shared",
		"where" => "c.shared",
		"type" => "select",
		"options" => array(
			"" => "-",
			"0" => "No",
			"1" => "Yes",
			),
		),
);

function edit() {
	global $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid banned card id !");

	$res = $db->q("SELECT * FROM account_cc_ban WHERE id = ? LIMIT 1", array($action_id));
	if (!$db->numrows($res))
		return actionError("Cant find banned credit card for id={$action_id} !");
	$row = $db->r($res);

	if (isset($_REQUEST["submit"])) {
		$reason = $_REQUEST["u_reason"];
		$db->q("UPDATE account_cc_ban SET reason = ? WHERE id = ? LIMIT 1", array($reason, $action_id));
		$aff = $db->affected($res);
		if ($aff == 1)
			return actionSuccess("You have successfully updated banned credit card id #{$action_id}.");
		else
			return actionError("Error while updating banned credit card id #{$action_id}.");
	}

	echo "<h2>Edit banned credit card #{$action_id}</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>ID</th><td>{$action_id}</td></tr>\n";
	echo "<tr><th>CC no#</th><td>".htmlspecialchars($row["cc"])."</td></tr>\n";
	echo "<tr><th>Reason</th><td><input type=\"text\" name=\"u_reason\" size=\"50\" value=\"".htmlspecialchars($row["reason"])."\" /></td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
	echo "</table>";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$action_id}\" />";
	echo getFilterFormFields();
	echo "</form>";
	return true;
}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'edit':
		$ret = edit();
		if (!$ret)
			return;
		break;
}

$params = [];
$where = getWhere($params);
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
	$order = "ORDER BY c.id DESC";

//query db
$sql = "SELECT count(*) as total
		FROM account_cc_ban c
		$where";
$res = $db->q($sql, $params);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT c.*
		FROM account_cc_ban c
		$where
		$order 
		$limit";
$res = $db->q($sql, $params);
if ($db->numrows($res) == 0) {
	echo "No banned credit cards.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Banned credit cards</h2>\n";
displayFilterForm();
echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr>";
echo "<th>".getOrderLink("Id", "id")."</th>";
echo "<th>".getOrderLink("CC Number", "cc")."</th>";
echo "<th>Exp</th>";
echo "<th>".getOrderLink("Firstname", "c.firstname")."</th>";
echo "<th>".getOrderLink("Lastname", "lastname")."</th>";
echo "<th>Time</th>";
echo "<th>Reason</th>";
echo "<th>".getOrderLink("Address", "c.address")."</th>";
echo "<th>".getOrderLink("City", "c.city")."</th>";
echo "<th>".getOrderLink("State", "c.state")."</th>";
echo "<th>".getOrderLink("Zipcode", "c.zipcode")."</th>";
echo "<th>".getOrderLink("Shared", "c.shared")."</th>";
echo "<th/></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<tr>";
	echo "<td>{$rox['id']}</td>";
	echo "<td>{$rox["cc"]}</td>";
	echo "<td>{$rox['month']}/{$rox['year']}</td>";
	echo "<td>{$rox["firstname"]}</td>";
	echo "<td>{$rox["lastname"]}</td>";
	echo "<td>{$rox["time"]}</td>";
	echo "<td>{$rox["reason"]}</td>";
	echo "<td>{$rox["address"]}</td>";
	echo "<td>{$rox["city"]}</td>";
	echo "<td>{$rox["state"]}</td>";
	echo "<td>{$rox["zipcode"]}</td>";
	
	if ($rox["shared"] == 1) {
		echo "<td>YES</td>";
	} else {
		echo "<td>-</td>";
	}

	$links = "<a href=\"".getActionLink("edit", $rox['id'])."\">edit</a>";
	
	echo "<td>{$links}</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;


?>
