<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

function batch_run($batch_id, $limit = 10) {
	global $db;

	$now = time();

	$res = $db->q("
		SELECT bm.*, bb.type
		FROM blast_message bm 
		INNER JOIN blast_batch bb on bb.id = bm.batch_id
		WHERE bm.batch_id = ? AND bm.status IS NULL 
		ORDER BY bm.id ASC 
		LIMIT {$limit}", 
		[$batch_id]
		);
	while ($row = $db->r($res)) {
		$id = $row["id"];
		$type = $row["type"];
		$from = $row["from"];
		$to = $row["to"];
		$subject = $row["subject"];
		$html = $row["content_html"];
		$text = $row["content_text"];

		$error = null;

		if ($type == "E") {
			if (!$from || !$to || !$subject || !$html || !$text) {
				$error = "mandatory param not defined: id={$id}, type='{$type}', from='{$from}', to='{$to}', subject='{$subject}', html='{$html}', text='{$text}'";
			} else {
				$ret = send_email([
					"from" => $from,
					"to" => $to,
					"subject" => $subject,
					"html" => $html,
					"text" => $text,
					], $error);
			}
		} else {
			$error = "undefined type: id='{$id}', type='{$type}', from='{$from}', to='{$to}', subject='{$subject}', html='{$html}', text='{$text}'";
		}

		if ($error) {
			reportAdmin("AS: batch_run - error", $error);
			$db->q("UPDATE blast_message SET status = 2 WHERE id = ? LIMIT 1", [$id]);
			$db->q("UPDATE blast_batch SET cnt_error = IFNULL(cnt_error,0) + 1 WHERE id = ? LIMIT 1", [$batch_id]);
			return false;
		}

		//success
		$db->q("UPDATE blast_message SET status = 1, sent_stamp = ? WHERE id = ? LIMIT 1", [$now, $id]);
		$db->q("UPDATE blast_batch SET cnt_success = IFNULL(cnt_success,0) + 1 WHERE id = ? LIMIT 1", [$batch_id]);
	}

	return true;
}

if ($_REQUEST["refresh"] == "1")

if (!account::ensure_real_admin())
	die("Invalid access");

$batch_id = intval($_REQUEST["batch_id"]);
if (!$batch_id)
	die("batch_id not specified!");

if ($_REQUEST["activate"] == "1") {
	$db->q("UPDATE blast_batch SET status = 1 WHERE id = ? LIMIT 1", [$batch_id]);
}
if ($_REQUEST["activate"] == "0") {
	$db->q("UPDATE blast_batch SET status = NULL WHERE id = ? LIMIT 1", [$batch_id]);
}

$res = $db->q("
	SELECT b.*, a.email 
	FROM blast_batch b
	INNER JOIN account a on a.account_id = b.author_id
	WHERE id = ? 
	LIMIT 1", 
	[$batch_id]
	);
if ($db->numrows($res) != 1)
	die("batch not found in db!");
$row = $db->r($res);

if ($row["status"] == 1 && (intval($row["cnt_success"]) + intval($row["cnt_error"]) < intval($row["cnt_total"]))) {
	//if batch active, run a batch
	batch_run($batch_id);
}

//re-read
$res = $db->q("
	SELECT b.*, a.email 
	FROM blast_batch b
	INNER JOIN account a on a.account_id = b.author_id
	WHERE id = ? 
	LIMIT 1", 
	[$batch_id]
	);
if ($db->numrows($res) != 1)
	die("batch not found in db!");
$row = $db->r($res);

echo "<h1>Blast #{$batch_id}</h1>";
echo "<table>";
echo "<tr><th>Created</th><td>".date("Y-m-d H:i", $row["stamp"])."</td></tr>";
echo "<tr><th>Author</th><td>{$row["email"]}</td></tr>";
echo "<tr><th>Type</th><td>{$row["type"]}</td></tr>";
echo "<tr><th>Name</th><td>{$row["name"]}</td></tr>";
echo "<tr><th>Status</th><td>".(($row["status"] == 1) ? "active" : "not active")."</td></tr>\n";
echo "<tr><th>Total messages</th><td>{$row["cnt_total"]}</td></tr>\n";
echo "<tr><th>Sent successfully</th><td>{$row["cnt_success"]}</td></tr>\n";
echo "<tr><th>Failed</th><td>{$row["cnt_error"]}</td></tr>\n";
echo "</table>";

if ($row["status"] == 1)
	echo "<button type=\"button\" id=\"deactivate_btn\">Deactivate</button>\n";
else
	echo "<button type=\"button\" id=\"activate_btn\">Activate</button>\n";
?>
<button type="button" id="refresh_btn">Automatic refresh disabled</button>

<script type="text/javascript">
<?php
if ($_REQUEST["refresh"] == "1")
	echo "var refresh_enabled = true;\n";
else
	echo "var refresh_enabled = false;\n";
?>
var refresh_timeout = null;
function refresh() {
	var url = new URL(document.location.href);
	url.searchParams.set('refresh', 1);
	url = removeParam(url.toString(), 'activate');
	window.location.href = url;
}
function activate(act) {
	var url = new URL(document.location.href);
	url.searchParams.set('activate', act);
	window.location.href = url;
}
function refresh_on() {
	refresh_enabled = true;
	refresh_timeout = setTimeout(refresh, 3000);
	$('#refresh_btn').html('Automatic refresh enabled');
}
function refresh_off() {
	refresh_enabled = false;
	clearTimeout(refresh_timeout);
	$('#refresh_btn').html('Automatic refresh disabled');
}
function refresh_init() {
	if (refresh_enabled)
		refresh_on();
	else
		refresh_off();
}
$(document).ready(function() {
	$('#refresh_btn').click(function() {
		if (refresh_enabled)
			refresh_off();
		else
			refresh_on();
	});
	$('#activate_btn').click(function() {
		activate(1);
	});
	$('#deactivate_btn').click(function() {
		activate(0);
	});
	refresh_init();
});
</script>
