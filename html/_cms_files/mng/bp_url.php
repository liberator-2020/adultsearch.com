<?php
require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in
if (!$account->isMember()) {
	$account->asklogin();
	return false;
}

//only for burak and jay for now
if (!$account->isrealadmin())
	system::go('/');

echo "<h2>Backpage url lookup</h2>";

function getLocation($url_name) {
    global $mcache, $db;

    $key = "SQL:LOC-DIR-HAS";
    $params = array($url_name);
    $res = $mcache->get($key, $params);

    if ($res === false) {
        $mul = $mcache->multiple_results;
        //check redir table
        $key = "SQL:BPL-URL";
        $res2 = $mcache->get($key, $params, true);
        if ($res2 !== false) {
            $loc_id = intval($res2["loc_id"]);

            $key = "SQL:LOC-ID";
            $params = array($loc_id);
            $res3 = $mcache->get($key, $params, true);
            if ($res3 !== false)
                return $res3;
            else {
				echo "GEOLOC failed - Not found loc_id '{$loc_id}' in location_location table !<br />\n";
                return false;
            }
        }
        if ($mul) {
            echo "<span style=\"color: red;\">Multiple</span> results for '{$url_name}':<br />\n";
            $sres = $db->q("SELECT l.loc_id, c.loc_name as country_name, l.loc_name, l.s 
                            FROM location_location l 
                            INNER JOIN location_location c on c.loc_id = l.country_id
                            WHERE l.dir = ? and l.has_place_or_ad = 1", array($url_name));
            echo "<style type=\"text/css\">table {border-collapse: collapse;} td { border: 1px solid #aaa; padding: 5px;} td.cli {cursor: pointer;}</style>\n";
            echo "<table>";
            while ($row = $db->r($sres)) {
                echo "<tr><td class=\"cli\">{$row["loc_id"]}</td><td>{$row["country_name"]}</td><td>{$row["loc_name"]}, {$row["s"]}</td></tr>";
            }
            echo "</table>";
            return false;
        } else {
            echo "<span style=\"color: red;\">No</span> result for '{$url_name}' !<br />\n";
            return false;
        }
        return false;
	} else {
        echo "Unique result:<br />";
        return $res;
    }
}

if (isset($_REQUEST["add"]) && $_REQUEST["add"] == 1) {
    //adding loc_id to lookup table
    $url_name = preg_replace('/[^a-z\-]/','', $_REQUEST["url_name"]);
    $loc_id = intval($_REQUEST["loc_id"]);
    if ($loc_id == 0) {
        echo "Error: loc_id cant be 0!<br />\n";
    } else {
        if (isset($_REQUEST["substitute"]))
            $substitute = 1;
        else
            $substitute = 0;
        echo "Adding ".htmlspecialchars($url_name)." -&gt; {$loc_id}, substitute={$substitute}<br />";
        $res = $db->q("INSERT INTO bp_promo_loc_lookup (url_name, loc_id) VALUES (?, ?)", array($url_name, $loc_id));
        $id = $db->insertid();
        if ($id)
            echo "<span style=\"color: green;\">Added succesfully</span>, id={$id}<br /><br />";
        else
            echo "<span style=\"color: red;\">ERROR!</span>, id={$id}<br /><br />";
    }

} else if (isset($_REQUEST["submit"])) {
	//checking url_name
    $url_name = preg_replace('/[^a-z\-]/','', $_REQUEST["url_name"]);
    echo "URL name: <strong>".htmlspecialchars($url_name)."</strong><br /><br />";
    $loc_row = getLocation($url_name);
    if ($loc_row !== false) {
        echo "<br />{$loc_row["loc_id"]} -&gt; <strong>{$loc_row["loc_name"]}</strong>, <strong>{$loc_row["s"]}</strong> ({$loc_row["loc_url"]}), {$loc_row["country_id"]}<br /><br />";
        echo "(".print_r($loc_row,true).")<br />\n";
    }

    echo "<br /><form>Add <strong>".htmlspecialchars($url_name)."</strong>:<br />";
    echo "<input type=\"hidden\" name=\"url_name\" value=\"".htmlspecialchars($url_name)."\" />";
    echo "<input type=\"hidden\" name=\"add\" value=\"1\" />";
    echo "Loc_id: <input id=\"add_loc_id\" type=\"text\" name=\"loc_id\" value=\"\" /><br />";
    echo "Substitute: <input type=\"checkbox\" name=\"substitute\" value=\"1\" /><br />";
    echo "<input type=\"submit\" name=\"submit2\" value=\"Add\" />";
    echo "</form>";
    echo "<br /><br /><br />";
}
?>
<form action="" method="get">
URL name:<input type="text" name="url_name" /><br />
<input type="submit" name="submit" value="Submit" />
</form>
<script type="text/javascript">
$('.cli').each(function() {
    $(this).click(function() {
        $('#add_loc_id').val($(this).text());
    });
});
</script>

