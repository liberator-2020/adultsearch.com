<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("bppromo_manage"))
    return;

global $url_last_piece, $filters, $where_cols;

?>
<style type="text/css">
.list_td {
	border: 1px solid #aaa;
}
.list_td ul {
	padding-left: 10px;
}
.list_td li {
	margin-bottom: 2px;
}
#img_ava_list {
	max-height: 350px;
	overflow: scroll;
}
#bpp_img_selector li {
	cursor: pointer;
}
.used {
	margin-left: 5px;
}
</style>
<script type="text/javascript">
function reload_available_images() {
	var type = $('#img_ava_type').val();
	$.ajax({
		url: '/backpage/bppromo_list_images',
       	data: 'type=' + type,
       	dataType: 'json',
       	success: function(data) {
			if (data.status != 'success') {
				alert('Error!');
			} else {
				var images = data.images;
				$('#img_ava_list').html('');
				for (var i = 0; i < images.length; i++) {
					var img = images[i];
					$('#img_ava_list').append('<li data-type="'+img.type+'" data-filename="'+img.filename+'" data-url="'+img.url+'" style="min-height: 75px;"><img src="'+img.url+'" width="50" />'+img.filename+'<span class="rem_link"></span><span class="used">'+img.used+'</span></li>');
				}
				$('#img_ava_list li').click(img_selector_select);
			}
		}
	});
}

function img_selector_select() {
	//TODO add check if image is not already selected
	//TODO remove selected images from avialable list
	var item = $(this).clone();
	$(item).find('.rem_link').html('<a href="#" onclick="img_selector_remove(this, event); return false;">remove</a>');
	$('#img_sel_list').append(item);
	img_selector_recompute();
}

function img_selector_remove(link, event) {
	event.stopPropagation();
	$(link).parent().parent().remove();
	img_selector_recompute();
	return false;
}

function img_selector_recompute() {
	var str = '';
	$('#img_sel_list li').each(function() {
		var item_str = $(this).attr('data-type')+'@'+$(this).attr('data-filename');
		if (str != '')
			str += '|';
		str += item_str;
	});
	$('#img_selector_input').val(str);
}

function img_selector_init() {
	var imgs = $('#img_selector_input').val().split('|');
	for(var i = 0; i < imgs.length; i++) {
		var img = imgs[i].split('@');
		var type = img[0];
		var filename = img[1];
		if (type == '' || filename == '')
			continue;
		var url = '/backpage/bppromo_image?type='+type+'&filename='+filename;
		$('#img_sel_list').append('<li data-type="'+type+'" data-filename="'+filename+'" data-url="'+url+'"><img src="'+url+'" width="50" />'+filename+'<span class="rem_link"><a href="#" onclick="img_selector_remove(this, event); return false;">remove</a></span></li>');
	}
}

$(document).ready(function() {
	if (document.getElementById('bpp_img_selector')) {
		reload_available_images();
		img_selector_init();	
	}	
	if (document.getElementById('img_ava_type')) {
		$('#img_ava_type').change(reload_available_images);
	}
	$('.deletead').click(function(){
        return confirm("Are you sure you want to delete?");
	});
});
</script>
<?

//configuration
$url_last_piece = "bppromo_ad";
$where_cols = array(
	"id"	=> array("title" => "ID", "where" => "ad.id", "match" => "exact", "size" => "7"),
	"type"	=> array("title" => "Type", "where" => "ad.type", "type" => "select", "options" => array("" => "-", "1" => "Escort", "2" => "Bodyrubs", "3" => "TS/TV", "4" => "Stripclubs")),
	"title" => array("title" => "Title", "where" => "ad.title"),
	"content" => array("title" => "Content", "where" => "ad.content"),
	"paused" => array("title" => "Paused", "where" => "ad.paused", "type" => "select", "options" => array("" => "-", "0" => "No", "1" => "Yes")),
);

function add() {

	if (isset($_REQUEST["submit"])) {

		//set params	
		if (isset($_REQUEST["type1"]))
			$type = intval($_REQUEST["type1"]);
		if (isset($_REQUEST["title1"]))
			$title = $_REQUEST["title1"];
		if (isset($_REQUEST["content1"]))
			$content = $_REQUEST["content1"];
		if (isset($_REQUEST["link_text1"]))
			$link_text = $_REQUEST["link_text1"];
		if (isset($_REQUEST["paused1"]))
			$paused = 1;
		else
			$paused = 0;
		if (isset($_REQUEST["images1"]))
			$images = $_REQUEST["images1"];

		//check params
		if ($type == 0)
			echo "Error: Please choose type for new ad!<br />\n";
		else if ($title == "")
			echo "Error: Please enter title for new ad !<br />\n";
		else if ($content == "")
			echo "Error: Please enter content for new ad !<br />\n";
		else if ($link_text == "")
			echo "Error: Please enter link_text for new ad !<br />\n";
		else {

			$ad = new bpad();
			$ad->setType($type);
			$ad->setTitle($title);
			$ad->setContent($content);
			$ad->setLinkText($link_text);
			$ad->setPaused($paused);
			$ad->setImageString($images);
			$ret = $ad->add();
			if ($ret)
				return actionSuccess("You have successfully added new ad.");
			else
				return actionError("Error while adding new ad ! Please contact administrator.");
		}
	} else {
		$paused = 0;
		$link_text = "more pics and contact info";
	}
	
	echo "<h2>Add new ad</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Type: </th><td><select name=\"type1\">";
	$ad_types = backpage::get_ad_types();
	foreach ($ad_types as $key => $val) {
		$selected = "";
		if ($key == $type)
			$selected = " selected=\"selected\" ";
		echo "<option value=\"{$key}\"{$selected}>{$val}</option>";
	}
	echo "</select></td></tr>\n";	
	echo "<tr><th>Title: </th><td><input type=\"text\" name=\"title1\" size=\"60\" value=\"{$title}\"></td></tr>\n";
	echo "<tr><th>Content: </th><td><textarea name=\"content1\" cols=\"60\" rows=\"10\">{$content}</textarea></td></tr>\n";
	echo "<tr><th>Link text: </th><td><input type=\"text\" name=\"link_text1\" size=\"60\" value=\"{$link_text}\"></td></tr>\n";
	$pau_checked = "";
	if ($paused)
		$pau_checked = " checked=\"checked\" ";
	echo "<tr><th colspan=\"2\"><input type=\"checkbox\" name=\"paused1\" {$pau_checked} /> Paused</th></tr>\n";

	echo "<tr><th>Ad images: </th><td>";
	echo "<input id=\"img_selector_input\" type=\"hidden\" name=\"images1\" value=\"{$images}\" />";
	echo get_image_selector_html();
	echo "</td></tr>";

	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Add\" /></td></tr>\n";
	echo "</table>";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}

function get_image_selector_html() {
	echo "<table id=\"bpp_img_selector\"><tr><td>Available images: <select id=\"img_ava_type\"><option value=\"female\">Female</option><option value=\"male\">Male</option><option value=\"tstv\">TS/TV</option></select></td><td/><td>Selected images:</td></tr>";
	echo "<tr><td class=\"list_td\"><ul id=\"img_ava_list\"></ul></td><td width=\"50\"></td><td class=\"list_td\"><ul id=\"img_sel_list\"></ul></td></tr>";
	echo "</table>";
}

function pause() {
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid ad id !");

	$ad = bpad::findOnebyId($action_id);
	if ($ad == false)
		return actionError("Can't find ad for id={$action_id} !");
	$ad->setPaused(1);
	$ret = $ad->update();
	if ($ret)
		return actionSuccess("You have successfully paused ad id #{$action_id}.");
	else
		return actionError("Error while pausing ad id #{$action_id}.");
}

function unpause() {
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid ad id !");

	$ad = bpad::findOnebyId($action_id);
	if ($ad == false)
		return actionError("Can't find ad for id={$action_id} !");
	$ad->setPaused(0);
	$ret = $ad->update();
	if ($ret)
		return actionSuccess("You have successfully unpaused ad id #{$action_id}.");
	else
		return actionError("Error while unpausing ad id #{$action_id}.");
}

function edit() {
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid ad id !");

	$ad = bpad::findOnebyId($action_id);
	if ($ad == false)
		return actionError("Can't find ad for id={$action_id} !");
	if (isset($_REQUEST["submit"])) {

		//set params	
		if (isset($_REQUEST["type1"]))
			$type = intval($_REQUEST["type1"]);
		if (isset($_REQUEST["title1"]))
			$title = $_REQUEST["title1"];
		if (isset($_REQUEST["content1"]))
			$content = $_REQUEST["content1"];
		if (isset($_REQUEST["link_text1"]))
			$link_text = $_REQUEST["link_text1"];
		if (isset($_REQUEST["paused1"]))
			$paused = 1;
		else
			$paused = 0;
		if (isset($_REQUEST["images1"]))
			$images = $_REQUEST["images1"];

		//check params
		if ($type == 0)
			echo "Error: Please choose type for the ad!<br />\n";
		else if ($title == "")
			echo "Error: Please enter title for the ad !<br />\n";
		else if ($content == "")
			echo "Error: Please enter content for the ad !<br />\n";
		else if ($link_text == "")
			echo "Error: Please enter link_text for the ad !<br />\n";
		else {
			$ad->setType($type);
			$ad->setTitle($title);
			$ad->setContent($content);
			$ad->setLinkText($link_text);
			$ad->setPaused($paused);
			$ad->setImageString($images);
			$ret = $ad->update();
			if ($ret)
				return actionSuccess("You have successfully updated ad id #{$action_id}.");
			else
				return actionError("Error while updating ad id #{$action_id}.");
		}
	} else {
		$type = $ad->getType();
		$title = $ad->getTitle();
		$content = $ad->getContent();
		$link_text = $ad->getLinkText();
		$paused = $ad->getPaused();
		$images1 = $ad->getImageString();
	}
	
	echo "<h2>Edit ad #{$ad->getId()}</h2>";
	echo "<form method=\"post\" action=\"\" id=\"editadform\">";
	echo "<table>";
	echo "<tr><th>Type: </th><td><select name=\"type1\">";
	$ad_types = backpage::get_ad_types();
	foreach ($ad_types as $key => $val) {
		$selected = "";
		if ($key == $type)
			$selected = " selected=\"selected\" ";
		echo "<option value=\"{$key}\"{$selected}>{$val}</option>";
	}
	echo "</select></td></tr>\n";	
	echo "<tr><th>Title: </th><td><input type=\"text\" name=\"title1\" size=\"60\" value=\"{$title}\"></td></tr>\n";
	echo "<tr><th>Content: </th><td><textarea name=\"content1\" cols=\"60\" rows=\"10\">{$content}</textarea></td></tr>\n";
	echo "<tr><th>Link text: </th><td><input type=\"text\" name=\"link_text1\" size=\"60\" value=\"{$link_text}\"></td></tr>\n";
	$pau_checked = "";
	if ($paused)
		$pau_checked = " checked=\"checked\" ";
	echo "<tr><th colspan=\"2\"><input type=\"checkbox\" name=\"paused1\" {$pau_checked} /> Paused</th></tr>\n";

	echo "<tr><th>Ad images: </th><td>";
	echo "<input id=\"img_selector_input\" type=\"hidden\" name=\"images1\" value=\"{$images1}\" />";
	echo get_image_selector_html();
	echo "</td></tr>";

	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Update\" /></td></tr>\n";
	echo "</table>";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}

function delete() {
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid ad id !");

	$ad = bpad::findOnebyId($action_id);
	if ($ad == false)
		return actionError("Can't find ad for id={$action_id} !");

	if ($ad->remove()){
		return actionSuccess("You have successfully deleted ad id #{$action_id}.");
	}
	return actionError("Error while deleting ad id #{$action_id}.");
}


$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'add':
		$ret = add();
		if (!$ret)
			return;
		break;
	case 'edit':
		$ret = edit();
		if (!$ret)
			return;
		break;
	case 'pause':
		$ret = pause();
		if (!$ret)
			return;
		break;
	case 'unpause':
		$ret = unpause();
		if (!$ret)
			return;
		break;
	case 'delete':
		$ret = delete();
		if (!$ret)
			return;
		break;
}

$where = getWhere();
$order = getOrder();
$limit = getLimit();

//query db
$sql = "SELECT count(*) as total 
		FROM bp_promo_ad ad
		$where
		";

$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT ad.*, asl.id as slot_id
		FROM bp_promo_ad ad
		LEFT JOIN bp_promo_ad_slot asl on asl.curr_ad_id = ad.id
		$where
		$order 
		$limit";

$res = $db->q($sql);

//pager
$pager = getPager($total);

//output
echo "<h2>BP Promo ads</h2>\n";
echo "<a href=\"/mng/bppromo_ad?action=add\">Add new ad</a><br />\n";
displayFilterForm();
echo $pager."<br />";
echo getFilterFormFields();

if ($db->numrows($res) == 0) {
	echo "No BP promo ads.";
	echo "</form>";
	return;
}

echo "<table class=\"control\">";
echo "<thead><tr><th class=\"check\"><input type=\"checkbox\" name=\"master\" value=\"\" /></th><th>".getOrderLink("Id", "id")."</th><th>".getOrderLink("Type", "type")."</th><th>".getOrderLink("Title", "title")."</th><th>".getOrderLink("Content", "content")."</th><th>".getOrderLink("Paused", "paused")."</th><th>Currently live</th><th>".getOrderLink("Last put live", "since")."</th><th>".getOrderLink("Hits Since", "hits_since")."</th><th>".getOrderLink("Impressions Since", "impressions_since")."</th><th>CTR (%)</th><th/></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<tr>";
	echo "<td class=\"check\"><input type=\"checkbox\" name=\"id[]\" value=\"{$rox['id']}\" /></td>";
	echo "<td>{$rox["id"]}</td>";

	$type_name = backpage::get_ad_type_name($rox["type"]);
	if (!$type_name)
		echo "<td><span style=\"color: red; font-weight: bold;\">ERR - unknown type '{$rox["type"]}' !</span></td>";
	else
		echo "<td>{$type_name}</td>";
	
	echo "<td>{$rox["title"]}</td>";
	
	if (strlen($rox["content"]) > 30)
		$content = substr($rox["content"], 0 , 30)."...";
	else
		$content = $rox["content"];
	echo "<td>{$content}</td>";

	if ($rox["paused"])
		echo "<td>YES</td>";
	else
		echo "<td>NO</td>";

	if ($rox["slot_id"]) {
		echo "<td>YES (<a href=\"/mng/bppromo_adslot?id={$rox["slot_id"]}\">ad slot #{$rox["slot_id"]}</a>)</td>";
	} else {
		echo "<td>NO</td>";
	}

	if ($rox["since"] == NULL) {
		echo "<td>-</td><td/><td/><td/>";
	} else {
		echo "<td>".date("m/d/Y H:i:s", $rox["since"])." (".time_elapsed_string($rox["since"]).")</td>";
		$impressions = intval($rox["impressions_since"]);
	    $hits = intval($rox["hits_since"]);
	    if ($impressions > 0)
    	    $ctr = number_format($hits / $impressions * 100, 1);
	    else
    	    $ctr = "";
	    echo "<td>{$impressions}</td>";
	    echo "<td>{$hits}</td>";
    	echo "<td>{$ctr}</td>";
	}

	echo "<td>";
	echo "<a href=\"".getActionLink("edit", $rox['id'])."\">Edit</a>&nbsp;&middot&nbsp;";
	if ($rox["paused"])
		echo "<a href=\"".getActionLink("unpause", $rox['id'])."\">Unpause</a>&nbsp;&middot&nbsp;";
	else
		echo "<a href=\"".getActionLink("pause", $rox['id'])."\">Pause</a>&nbsp;&middot&nbsp;";

	echo "<a href=\"/mng/bppromo_stats?ad_id={$rox['id']}\">Stats</a>&nbsp;&middot&nbsp;";
	echo "<a class=\"deletead\" href=\"".getActionLink("delete", $rox['id'])."\">Delete</a>";
	if ($account->isrealadmin())
		echo "&nbsp;&middot&nbsp;<a href=\"/mng/audit?type=BPA&p1={$rox['id']}\">history</a>";
	echo "</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo "</form>";
echo $pager;

?>
