<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) {
    $account->asklogin();
    return false;
}

if (!$account->isadmin()) {
    echo "You dont have privileges to access this page!";
    die;
}

global $url_last_piece, $filters, $where_cols;


//configuration
$url_last_piece = "bppromo_adslot";
$where_cols = array(
	"id"	=> array("title" => "ID", "where" => "asl.id", "match" => "exact", "size" => "7"),
	"type"	=> array("title" => "Type", "where" => "asl.type", "type" => "select", "options" => array("" => "-", "1" => "Escort", "2" => "Bodyrubs", "3" => "TS/TV", "4" => "Stripclubs")),
	"ad_id"	=> array("title" => "ID", "where" => "asl.ad_id", "match" => "exact", "size" => "7"),
);

function add() {
	if (isset($_REQUEST["submit"])) {
		//check params
		$type = intval($_REQUEST["type1"]);
		$bp_code = $_REQUEST["bp_code1"];
		if ($type == 0)
			echo "Error: Please choose type for new ad slot!<br />\n";
		else if ($bp_code == "")
			echo "Error: Please enter bp code for new ad slot!<br />\n";
		else {
			//check if ad slot with that bp_code already exists
			$asl = bpadslot::findOneByBpCode($bp_code);
			//die("asl=".print_r($asl, true));
			if ($asl) {
				die("di");
				return actionError("BP Ad Slot with bp code '{$bp_code}' already exists !");
			} else {

				$asl = new bpadslot();
				$asl->setType($type);
				$asl->setBpCode($bp_code);
				$ret = $asl->add();
				if ($ret)
					return actionSuccess("You have successfully added new ad slot.");
				else
					return actionError("Error while adding new ad slot ! Please contact administrator.");
			}
		}
	}
	
	echo "<h2>Add new ad slot</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Type: </th><td><select name=\"type1\"><option value=\"1\">Escort</option><option value=\"2\">Bodurybs</option><option value=\"3\">TS/TV</option><option value=\"4\">Stripclub</option></select></td></tr>\n";	
	echo "<tr><th>BP code: </th><td><input type=\"text\" name=\"bp_code1\" size=\"60\" value=\"\"><br /><small>e.g. oid=304117590&amp;id=4e9ab89e10d6b554ff8ca46f4b8ad3fe-1389737733-central</small></td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Add\" /></td></tr>\n";
	echo "</table>";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}

function edit() {
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid ad slot id !");

	$asl = bpadslot::findOnebyId($action_id);
	if ($asl == false)
		return actionError("Can't find ad slot for id={$action_id} !");

	if (isset($_REQUEST["submit"])) {

		$type = intval($_REQUEST["type1"]);
		$bp_code = $_REQUEST["bp_code1"];
		if (isset($_REQUEST["active1"]))
       	    $active = 1;
        else
	        $active = 0;
		if ($type == 0)
			echo "Error: Please choose type for new ad slot!<br />\n";
		else if ($bp_code == "")
			echo "Error: Please enter bp code for new ad slot!<br />\n";
		else {
			$asl->setType($type);
			$asl->setBpCode($bp_code);
			$asl->setActive($active);
			$ret = $asl->update();
			if ($ret)
				return actionSuccess("You have successfully updated ad slot id #{$action_id}.");
			else
				return actionError("Error while updating ad slot id #{$action_id}.");
		}
	}

	echo "<h2>Edit ad slot #{$asl->getId()}</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Type: </th><td><select name=\"type1\">";
	$ad_types = backpage::get_ad_types();
	foreach ($ad_types as $key => $val) {
		$selected = "";
		if ($asl->getType() == $key)
			$selected = " selected=\"selected\"";
		echo "<option value=\"{$key}\"{$selected}>{$val}</option>";
	}
	echo "</select></td></tr>\n";
	echo "<tr><th>BP code: </th><td><input type=\"text\" name=\"bp_code1\" size=\"60\" value=\"{$asl->getBpCode()}\"></td></tr>\n";
	$act_checked = "";
    if ($asl->isActive())
        $act_checked = " checked=\"checked\" ";
    echo "<tr><th colspan=\"2\"><input type=\"checkbox\" name=\"active1\" {$act_checked} /> Active</th></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Update\" /></td></tr>\n";
	echo "</table>";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}

function delete() {
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid ad slot id !");

	$asl = bpadslot::findOnebyId($action_id);
	if ($asl == false)
		return actionError("Can't find ad slot for id={$action_id} !");

	if ($asl->remove()){
		return actionSuccess("You have successfully deleted ad slot id #{$action_id}.");
	}
	return actionError("Error while deleting ad slot id #{$action_id}.");
}

function rotate() {
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid ad slot id !");

	$asl = bpadslot::findOnebyId($action_id);
	if ($asl == false)
		return actionError("Can't find ad slot for id={$action_id} !");

	//TODO add some confirmation before actual rotating ???
	$ret = backpage::rotate_ad_slot($asl);
	$ret = true;

	if ($ret)
		return actionSuccess("You have successfully rotated ad slot id #{$action_id}.");
	else
		return actionError("Error while rotating ad slot id #{$action_id}.");

}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'add':
		$ret = add();
		if (!$ret)
			return;
		break;
	case 'edit':
		$ret = edit();
		if (!$ret)
			return;
		break;
	case 'delete':
		$ret = delete();
		if (!$ret)
			return;
	case 'rotate':
		$ret = rotate();
		if (!$ret)
			return;
		break;
}

$where = getWhere();
$order = getOrder();
$limit = getLimit();

//query db
$sql = "SELECT count(*) as total 
		FROM bp_promo_ad_slot asl
		$where
		";

$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT *
		FROM bp_promo_ad_slot asl
		$where
		$order 
		$limit";

$res = $db->q($sql);

//pager
$pager = getPager($total);

//output
echo "<h2>BP Promo ad slots</h2>\n";
echo "<a href=\"/mng/bppromo_adslot?action=add\">Add new ad slot</a><br />\n";
displayFilterForm();
echo $pager."<br />";
echo getFilterFormFields();

if ($db->numrows($res) == 0) {
	echo "No BP promo ad slots.";
	echo "</form>";
	return;
}

echo "<table class=\"control\">";
echo "<thead><tr><th class=\"check\"><input type=\"checkbox\" name=\"master\" value=\"\" /></th><th>".getOrderLink("Id", "id")."</th><th>".getOrderLink("Type", "type")."</th><th>".getOrderLink("BP code", "bp_code")."</th><th>".getOrderLink("Active", "active")."</th><th>".getOrderLink("Current Ad", "curr_ad_id")."</th><th>".getOrderLink("Current Ad Since", "curr_ad_since")."</th><th>".getOrderLink("Impressions Since", "impressions_since")."</th><th>".getOrderLink("Hits Since", "hits_since")."</th><th>".getOrderLink("Impressions Total", "impressions_total")."</th><th>".getOrderLink("Hits Total", "hits_total")."</th><th/></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<tr>";
	echo "<td class=\"check\"><input type=\"checkbox\" name=\"id[]\" value=\"{$rox['id']}\" /></td>";
	echo "<td>{$rox["id"]}</td>";

	$type_name = backpage::get_ad_type_name($rox["type"]);
    if (!$type_name)
        echo "<td><span style=\"color: red; font-weight: bold;\">ERR - unknown type '{$rox["type"]}' !</span></td>";
    else
        echo "<td>{$type_name}</td>";

	$code = $rox["bp_code"];
	if (strlen($code) > 30)
		$code = substr($code,0,8)."...".substr($code,-18);
	echo "<td>{$code}</td>";	
	
	if ($rox["active"])
		echo "<td>YES</td>";
	else
		echo "<td>NO</td>";

	if ($rox["curr_ad_id"] == NULL)
		echo "<td>-</td>";
	else
		echo "<td><a href=\"/mng/bppromo_ad?id={$rox["curr_ad_id"]}\">{$rox["curr_ad_id"]}</a></td>";

	if ($rox["curr_ad_since"] == NULL)
		echo "<td>-</td>";
	else
		echo "<td>".date("m/d/Y H:i:s", $rox["curr_ad_since"])."</td>";

	echo "<td>{$rox["impressions_since"]}</td>";
	echo "<td>{$rox["hits_since"]}</td>";
	echo "<td>{$rox["impressions_total"]}</td>";
	echo "<td>{$rox["hits_total"]}</td>";
	echo "<td>";
		echo "<a href=\"".getActionLink("rotate", $rox['id'])."\">Rotate</a>&nbsp;&middot&nbsp;";
		echo "<a href=\"".getActionLink("edit", $rox['id'])."\">Edit</a>&nbsp;&middot&nbsp;";
		echo "<a href=\"".getActionLink("delete", $rox['id'])."\">Delete</a>";
		if ($account->isrealadmin())
			echo "&nbsp;&middot&nbsp;<a href=\"/mng/audit?type=BPS&p1={$rox['id']}\">history</a>";
	echo "</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo "</form>";
echo $pager;

?>
