<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");

global $account, $db, $ctx, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("bppromo_manage"))
    return;

global $url_last_piece, $filters, $where_cols, $classifieds;

//configuration
define('BPPROMO_IMG_DIR', _CMS_ABS_PATH."/data/bppromo_images");
$url_last_piece = "bppromo_images";
$where_cols = array(
	"type"	=> array("title" => "Type", "where" => "type", "type" => "select", "options" => array("female" => "Female", "male" => "Male", "tstv" => "TSTV")),
);

function getImgDir($type) {
	$dir = BPPROMO_IMG_DIR;
	if ($type)
		$dir .= "/".$type;
	return $dir;
}

function process_image($i) {
	$type = $_REQUEST["type1_{$i}"];
	$handle = new Upload($_FILES["image_file_{$i}"]);

	//TODO add some global arr
	if (!in_array($type, array("female", "male", "tstv"))) {
		return "Error: Image #{$i} - Please enter valid type for new image !<br />\n";
	} else if (!$handle->uploaded) {
		return "Error: Image #{$i} - Please choose valid image file !<br />\n";
	} else {
		list($width, $height, $imgtype, $attr) = getimagesize($handle->file_src_pathname);
		if ($width > 500 || $height > 700) {
			return "Error: Image #{$i} - Maximum dimensions are 500x700 !";
		} else if ($imgtype != IMG_JPG) {
			return "Error: Image #{$i} - Image is not JPG !";
		//TODO add check if the image with the same filename is not already uploaded !
		} else {
			$handle->file_auto_rename = false;
			$handle->file_overwrite = false;
			$dir = getImgDir($type);
			$handle->Process($dir);
			if ($handle->processed) {
				return true;
			} else {
				return "Error while processing uploaded image file #{$i}! Please contact administrator.";
			}
		}
	}
	
}

function add() {

	if (isset($_REQUEST["submit"])) {
		//echo "<pre>".print_r($_FILES, true)."</pre><br /><br />\n";
		//die;
		$succ = 0;
		for ($i = 1; $i <= 10; $i++) {
			if ($_FILES["image_file_{$i}"]["size"] > 0) {
				$ret = process_image($i);
				if ($ret === true) {
					$succ++;
					continue;
				}
				return actionError($ret);
			}
		}
		return actionSuccess("You have successfully added {$succ} new images.");
	}
	
	echo "<h2>Add new image</h2>";
	echo "<small>Please upload JPG image type with max dimensions 500x700</small><br /><br />\n";
	echo "<form method=\"post\" action=\"\" enctype=\"multipart/form-data\">";
	echo "<table>";
	for ($i = 1; $i <= 10; $i++) {
		echo "<tr style=\"vertical-align: middle;\"><th>Image #{$i}: </th><td style=\"vertical-align: middle;\">Type: <select name=\"type1_{$i}\"><option value=\"female\">Female</option><option value=\"male\">Male</option><option value=\"tstv\">TS/TV</option></select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Image file: <input type=\"file\" name=\"image_file_{$i}\" ></td></tr>\n";
	}
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Add\" /></td></tr>\n";
	echo "</table>";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}

function delete() {
	global $ctx;

	$filename = $_REQUEST["action_id"];
	if (!$filename)
		return actionError("Invalid filename !");

	$type = $_REQUEST["type"];
		if (!in_array($type, array("female", "male", "tstv")))
		return actionError("Invalid type !");
	
	$dir = getImgDir($type);
	if (!is_dir($dir))
		return actionError("Directory for type '{$type}' does not exist !");

	$filepath = $dir."/".$filename;
	if (!is_file($filepath))
		return actionError("Image file for filename '{$filename}' and type '{$type}' does not exist !");
	
	if (isset($_REQUEST["submit"])) {
		if (unlink($filepath)) {
			return actionSuccess("You have successfully deleted image '{$filename}'.");
		} else {
			return actionError("Error while deleting image file '{$filename}' ! Please contact administrator.");
		}
	}

	echo "<h2>Are you sure you want to delete following image ?</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Type: </th><td>{$type}</td></tr>\n";
	echo "<tr><th>Filename: </th><td>{$filename}</td></tr>\n";
	$img_url = "http://{$ctx->domain_base}/backpage/bppromo_image?type={$type}&filename={$filename}";
	echo "<tr><th>Thumbnail: </th><td><img src=\"{$img_url}\" width=\"100\" /></td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Delete\" /></td></tr>\n";
	echo "</table>";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}


$filters = getFilters();
if (!array_key_exists("type", $filters) || (!in_array($filters["type"], array("female", "male", "tstv"))))
	$filters["type"] = "female";

switch ($_REQUEST["action"]) {
	case 'add':
		$ret = add();
		if (!$ret)
			return;
		break;
	case 'delete':
		$ret = delete();
		if (!$ret)
			return;
		break;
}

//custom implementation of limit
function getOffsetLength() {
	global $page, $ipp;

	if (!is_numeric($page) && $page == "one") {
		return false;
	} else {
		$start = ($page-1)*$ipp;
	}
	return array($start, $ipp);
}


//query filesystem
$type = $filters["type"];
$dir = getImgDir($type);
//echo "Dir = '{$dir}'<br />\n";
if ($handle = opendir($dir)) {
	while (false !== ($entry = readdir($handle))) {
		if ($entry != "." && $entry != "..") {
			//echo "$entry\n";
			$files[] = $entry;
		}
	}
	closedir($handle);
}

$total = count($files);
//pager
$pager = getPager($total);

//output
echo "<h2>BP Promo images</h2>\n";
echo "<a href=\"/mng/bppromo_images?action=add\">Add new images</a><br />\n";
displayFilterForm();
echo $pager."<br />";
echo getFilterFormFields();

if (count($files) == 0) {
	echo "No BP promo images.";
	echo "</form>";
	return;
}

$ret = sort($files);
if (!$ret)
	die("Can't sort image files!");

$arr = getOffsetLength();
if ($arr !== false) {
	list($off,$len) = $arr;
	$files = array_slice($files, $off, $len);
}


echo "<table class=\"control\">";
echo "<thead><tr><th>Filename</th><th>Thumbnail</th><th/></tr></thead>\n";
echo "<tbody>";
foreach ($files as $file) {
	echo "<tr>";
	echo "<td>{$file}</td>";
	$img_url = "http://{$ctx->domain_base}/backpage/bppromo_image?type={$type}&filename={$file}";
	echo "<td><img src=\"{$img_url}\" width=\"100\" /></td>";
	echo "<td>";
		echo "<a href=\"".getActionLink("delete", $file)."\">Delete</a>";
	echo "</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo "</form>";
echo $pager;

?>
