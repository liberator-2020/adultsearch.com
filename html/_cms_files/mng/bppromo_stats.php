<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("bppromo_manage"))
    return;

global $ordercol, $orderway, $url_last_piece, $filters, $where_cols, $classifieds;

?>
<script type="text/javascript">

$(document).ready(function() {
	$('#dmstats input[type=radio]').click(function() {
		$('#dmstats').submit();
	});
});
</script>

<?php

//configuration
$ipp = 30;
$ordercol = 'stats.day'; 
$orderway = 'DESC';
$url_last_piece = "bppromo_stats";
$where_cols = array(
	"id"	=> array("title" => "Ad Slot ID", "where" => "stats.ad_slot_id", "match" => "exact", "size" => "7"),
	"ad_id"	=> array("title" => "AD ID", "where" => "stats.ad_id", "match" => "exact", "size" => "7"),
);

$daily_checked = 'checked="checked"';
$monthly_checked = '';
$monthly = false;

$filters = array();
if($_REQUEST['dm'] == 'monthly'){
	$daily_checked = '';
	$monthly_checked = 'checked="checked"';
	$monthly = true;
}

if($monthly){
	$group_by = ' GROUP BY YEAR(day), MONTH(day) ';
}
else{
	$filters = getFilters();
}

$where = getWhere();
$order = getOrder();
$limit = getLimit();

//query db

$sql = "SELECT count(*) as total 
		FROM bp_promo_stats stats
		$where
		";
if($monthly){
	$sql .= $group_by;
}

$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

//query db
if($monthly){
	$sql = "SELECT YEAR(day) AS year, MONTH(day) AS month, sum(hits) AS hits, sum(impressions) AS impressions
		FROM bp_promo_stats stats
		$where
		$group_by
		$limit";
}
else{
	$sql = "SELECT *
		FROM bp_promo_stats stats
		$where
		$order 
		$limit";
}

$res = $db->q($sql);

//pager
$pager = getPager($total);

//output
echo "<h2>BP Promo stats</h2>\n";

echo '<form method="get" action="bppromo_stats" id="dmstats">
	<span style="display: inline-block;"><input type="radio" name="dm" value="daily" '.$daily_checked.'>Daily Stats<input type="radio" name="dm" value="monthly" '.$monthly_checked.'>Monthly Stats</span>
	</form>';

if(!$monthly){
	displayFilterForm();
}

echo $pager."<br />";
echo getFilterFormFields();

if ($db->numrows($res) == 0) {
	echo "No stats.";
	echo "</form>";
	return;
}

echo "<table class=\"control\">";
if($monthly){
	echo "<thead><tr><th>Year</th><th>Month</th><th>Impressions</th><th>Hits</th><th>CTR (%)</th></tr></thead>\n";
}
else{
	echo "<thead><tr><th>".getOrderLink("Ad slot #", "ad_slot_id")."</th><th>".getOrderLink("Ad #", "ad_id")."</th><th>".getOrderLink("Day", "day")."</th><th>Impressions</th><th>Hits</th><th>CTR (%)</th></tr></thead>\n";
}
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<tr>";
	if($monthly){
		echo "<td>{$rox["year"]}</td>";
		echo "<td>{$rox["month"]}</td>";
	}
	else{
		echo "<td>{$rox["ad_slot_id"]}</td>";
		echo "<td>{$rox["ad_id"]}</td>";
		echo "<td>{$rox["day"]}</td>";
	}
	$impressions = intval($rox["impressions"]);
	$hits = intval($rox["hits"]);
	if ($impressions > 0)
		$ctr = number_format($rox["hits"] / $rox["impressions"] * 100, 1);
	else
		$ctr = "";

	echo "<td>{$rox["impressions"]}</td>";
	echo "<td>{$rox["hits"]}</td>";
	echo "<td>{$ctr}</td>";

	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo "</form>";
echo $pager;

?>
