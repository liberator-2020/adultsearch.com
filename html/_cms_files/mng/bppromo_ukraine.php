<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("bppromo_manage"))
    return;

global $url_last_piece, $filters, $where_cols;

echo "<h1>BPPromo old images</h1>\n";

$url_last_piece = "bppromo_ukraine";
$where_cols = array(
	"ts" => array("title" => "TS/TV", "where" => "g.ts", "type" => "select", "options" => array("" => "-", "0" => "No", "1" => "Yes")),
);

function move() {
	global $db;

	$id = intval($_REQUEST["action_id"]);
	if ($id == 0)
		return false;

	$pics = $_REQUEST["images"];
//	echo "Moving girl id={$id}, count(pics)=".count($pics)."<br />\n";
//	echo "<pre>".print_r($pics, true)."</pre><br />\n";

	$folder = "female";
	if (isset($_REQUEST["ts"]) && $_REQUEST["ts"] == 1)
		$folder = "tstv";

	$err = false;
	$err2 = false;
	$i = 1;
	$cnt = 0;
	foreach ($pics as $pic) {
		$from = "/www/virtual/adts/www.adultsearch.com/cron/promo/down/{$pic}";
		$to = _CMS_ABS_PATH."/data/bppromo_images/{$folder}/{$id}_{$i}.jpg";
		$ret = rename($from, $to);
		if ($ret === false) {
//			echo "ERROR moving file '{$from}' to '{$to}' !<br />\n";
			return actionError("Error moving file '{$from}' to '{$to}' !");
			$err = true;
		} else {
			$cnt++;
		}
		$i++;
		
	}
	if ($err === false) {
		$res = $db->q("SELECT pic FROM _ukranian_pic WHERE id = ?", array($id));
		while ($row = $db->r($res)) {
			$pic = $row["pic"];
			if (!in_array($pic, $pics)) {
				$path = "/www/virtual/adts/www.adultsearch.com/cron/promo/down/{$pic}";
				$ret = unlink($path);
				if ($ret === false) {
//					echo "ERROR deleting file '{$path}' !<br />\n";
					return actionError("Error while deleting file '{$path}' !");
					$err2 = true;
				}
			}
		}
	}
	if ($err === false && $err2 === false) {
		$db->q("DELETE FROM _ukranian_girls WHERE id = ?", array($id));
		$aff1 = $db->affected();
//		echo "DEL 1 Affected: {$aff1}<br />\n";
		$db->q("DELETE FROM _ukranian_pic WHERE id = ?", array($id));
		$aff2 = $db->affected();
//		echo "DEL 2 Affected: {$aff2}<br />\n";
		return actionSuccess("You have successfully moved {$cnt} pictures of girl id #{$id} to folder '{$folder}'. (aff1={$aff1},aff2={$aff2})");
	}
}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'move':
		$ret = move();
		if (!$ret)
			return;
		break;
}

$where = getWhere();
$order = getOrder();
$limit = getLimit();

if (empty($where))
	$where = " WHERE g.valid = 1 ";
else
	$where = $where." AND g.valid = 1 ";

if (empty($order))
	$order = "ORDER BY g.id ASC";

$sql = "SELECT count(*) as total 
		FROM _ukranian_girls g
		$where";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT * FROM _ukranian_girls g {$where} {$order} {$limit}";
$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No girls.";
	die;
}

//pager
$pager = getPager($total);

displayFilterForm();

echo $pager;

//$res = $db->q("SELECT * FROM _ukranian_girls g WHERE g.valid = 1 AND g.ts = 0 ORDER BY g.id LIMIT 50");
while ($row = $db->r($res)) {
	$id = $row["id"];
	$res2 = $db->q("SELECT * FROM _ukranian_pic p WHERE p.id = ? ORDER BY pic ASC", array($id));
	echo "<div class=\"old\">";
	echo "<span style=\"display: inline-block; float: left;\">{$id}:</span>";
	while ($row2 = $db->r($res2)) {
		$pic = $row2["pic"];
		echo "<img class=\"up\" src=\"/__downb/{$pic}\" data-pic=\"{$pic}\" style=\"max-width: 200px; max-height: 200px; float: left;\"/>\n";
	}
	echo "<button id=\"but_{$id}\" class=\"but\" data-girl=\"{$id}\">move selected</button>";
	echo "<br style=\"clear: both;\" />\n";
	echo "</div>";
	echo "<hr style=\"width: 100%;\'/>\n";
	echo "<br style=\"clear: both;\" />\n";
}
?>
<script type="text/javascript">
$(document).ready(function() {
	$('img.up').click(function() {
		if ($(this).hasClass('sel')) {
			$(this).removeClass('sel');
		} else {
			$(this).addClass('sel');
		}
	});
	$('button.but').click(function() {
		var girl = $(this).attr('data-girl');
		var pics = [];
		$(this).parent().children('img.up').each(function() {
			if ($(this).hasClass('sel')) {
				var pic = $(this).attr('data-pic');
				pics[pics.length] = pic;
			}
		});
		var cnt_selected = pics.length;
		if (cnt_selected == 0) {
			alert('You have not selected images to move!');
			return false;
		}
		var ret = confirm('Are you sure you want to keep only selected '+cnt_selected+' pictures ?');
		if (ret == false)
			return false;
		var ts_val = $('select[name="ts"]').val();
		if (ts_val === '0' || ts_val === '1') {
			$.form('http://adultsearch.com/mng/bppromo_ukraine', { action: 'move', images: pics, action_id: girl, ts: ts_val }, 'POST').submit();
		} else {
			$.form('http://adultsearch.com/mng/bppromo_ukraine', { action: 'move', images: pics, action_id: girl }, 'POST').submit();
		}
	});

});
jQuery(function($) { $.extend({
	form: function(url, data, method) {
		if (method == null) method = 'POST';
		if (data == null) data = {};

		var form = $('<form>').attr({
			method: method,
			action: url
		 }).css({
			display: 'none'
		 });

		var addData = function(name, data) {
			if ($.isArray(data)) {
				for (var i = 0; i < data.length; i++) {
					var value = data[i];
					addData(name + '[]', value);
				}
			} else if (typeof data === 'object') {
				for (var key in data) {
					if (data.hasOwnProperty(key)) {
						addData(name + '[' + key + ']', data[key]);
					}
				}
			} else if (data != null) {
				form.append($('<input>').attr({
					type: 'hidden',
				  name: String(name),
				  value: String(data)
				}));
			}
		};

		for (var key in data) {
			if (data.hasOwnProperty(key)) {
				addData(key, data[key]);
			}
		}

		return form.appendTo('body');
	}
}); });
</script>
<style type="text/css">
.old img {
	border: 2px solid white;
}
.old img.sel {
	border: 2px solid red;
}
</style>
