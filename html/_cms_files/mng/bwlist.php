<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

$is_whitelist = $_REQUEST['type'];

//we have to be logged in 
if (!$account->isMember()) { 
	$account->asklogin(); 
	return false; 
}

if (!$account->isadmin()) {
	echo "You dont have privileges to access this page!";
	die;
}

global $url_last_piece, $filters, $where_cols, $classifieds;

$classifieds = new classifieds();

//TODO - this should be got from classifieds_bw_list bwlass
//configuration
$url_last_piece = "bwlist?type=0";
if($is_whitelist) $url_last_piece = "bwlist?type=1";
$where_cols = array(
	"cid"		 => array("title" => "ID", "where" => "bw.id", "match" => "exact", "size" => "7"),
	"field_type" => array("title" => "Field Type", "where" => "bw.field_type", "type" => "select", "options" => array("" => "-", "0" => "Email", "1" => "Phone", "2" => "Website")),
	"value"	  => array("title" => "Value", "where" => "bw.value"),
);


function delete_bw($type) {
	$phrase = 'blacklist';
	if($type) {
		$phrase = 'whitelist';
	}

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid ".$phrase." id !");

	$clbw = clbw::findOnebyId($action_id);
	if ($clbw == false)
		return actionError("Can't find ".$phrase."ed item for id={$action_id} !");

	if($clbw->remove()){
		return actionSuccess("You have successfully deleted ".$phrase."ed item id #{$action_id}.");
	}
	return actionError("Error while deleting ".$phrase."ed item id #{$action_id}.");
}

function edit($type) {
	$phrase = 'blacklist';
	if($type) {
		$phrase = 'whitelist';
	}

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid ".$phrase." id !");

	$clbw = clbw::findOnebyId($action_id);
	if ($clbw == false)
		return actionError("Can't find ".$phrase."ed item for id={$action_id} !");

	if (isset($_REQUEST["editsubmit"])) {
		if ((isset($_REQUEST["phone1"]) && $_REQUEST["phone1"] != '') || (isset($_REQUEST["email1"]) && $_REQUEST["email1"] != '') || (isset($_REQUEST["website1"]) && $_REQUEST["website1"] != '')){
			$phone = NULL;
			$email = NULL;
			$website = NULL;

			if(isset($_REQUEST["email1"])) {
				$email = $_REQUEST["email1"];
				if($email != ''){
					$exist = clbw::findOneByField($email, clbw::EMAIL, $action_id);
					if($exist){
						return actionError("Email address(".$email.") already exists in the blacklist/whitelist");
					}
					$clbw->setData($email, clbw::EMAIL, $type);
				}
				else{
					$email = NULL;
				}
			}
			
			if(isset($_REQUEST["phone1"])) {
				$phone = $_REQUEST["phone1"];
				if($phone != ''){
					$exist = clbw::findOneByField($phone, clbw::PHONE, $action_id);
					if($exist){
						return actionError("Phone number(".$phone.") already exists in the blacklist/whitelist");
					}
					$clbw->setData($phone, clbw::PHONE, $type);
				}
				else{
					$phone = NULL;
				}
			}

			if(isset($_REQUEST["website1"])) {
				$website = $_REQUEST["website1"];
				if($website != ''){
					$exist = clbw::findOneByField($phone, clbw::WEBSITE, $action_id);
					if($exist){
						return actionError("Website(".$website.") already exists in the blacklist/whitelist");
					}
					$clbw->setData($website, clbw::WEBSITE, $type);
				}
				else{
					$website = NULL;
				}
			}

			$ret = $clbw->updateBWList();
		}
		if ($ret)
			return actionSuccess("You have successfully updated ".$phrase."ed item id #{$action_id}.");
		else
			return actionError("Error while updating ".$phrase."ed item id #{$action_id}.");
	}

	
	echo "<h2>Edit ".$phrase."ed item #{$clbw->getId()}</h2>";

	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	if($clbw->getFieldType() == clbw::EMAIL){
		echo "<tr><th>Email: </th><td><input type=\"text\" name=\"email1\" value=\"".$clbw->getValue()."\"></td></tr>\n";	
	}
	if($clbw->getFieldType() == clbw::PHONE){
		echo "<tr><th>Phone Number: </th><td><input type=\"text\" name=\"phone1\" value=\"".$clbw->getValue()."\" size=\"8\"></td></tr>\n";
	}
	if($clbw->getFieldType() == clbw::WEBSITE){
		echo "<tr><th>Website: </th><td><input type=\"text\" name=\"website1\" value=\"".$clbw->getValue()."\"></td></tr>\n";
	}
	echo "<tr><th></th><td><input type=\"submit\" name=\"editsubmit\" value=\"Update\" /></td></tr>\n";
	echo "</table>";
	echo getFilterFormFields();
	echo "</form>";
	return true;
}

function add($type) {
	if($type) {
		$phrase = 'whitelist';
	}
	else{
		$phrase = 'blacklist';	
	}

	if (isset($_REQUEST["addsubmit"])) {
		if ((isset($_REQUEST["phone1"]) && $_REQUEST["phone1"] != '') || (isset($_REQUEST["email1"]) && $_REQUEST["email1"] != '') || (isset($_REQUEST["website1"]) && $_REQUEST["website1"] != '')){
			$phone = NULL;
			$email = NULL;
			$website = NULL;
			
			if(isset($_REQUEST["email1"])) {
				$email = $_REQUEST["email1"];
				if($email != ''){
					$exist = clbw::findOneByField($email, clbw::EMAIL);
					if($exist){
						actionError("Email address(".$email.") already exists in the blacklist/whitelist");
					}
					else{
						$clbw = new clbw();
						$clbw->setData($email, clbw::EMAIL, $type);
						$ret = $clbw->addBWList();
					}
				}
				else{
					$email = NULL;
				}
			}

			if(isset($_REQUEST["phone1"])) {
				$phone = $_REQUEST["phone1"];
				if($phone != ''){
					$exist = clbw::findOneByField($phone, clbw::PHONE);
					if($exist){
						actionError("Phone number(".$phone.") already exists in the blacklist/whitelist");
					}
					else{
						$clbw = new clbw();
						$clbw->setData($phone, clbw::PHONE, $type);
						$ret = $clbw->addBWList();
					}
				}
				else{
					$phone = NULL;
				}
			}
			
			if(isset($_REQUEST["website1"])) {
				$website = $_REQUEST["website1"];
				if($website != ''){
					$exist = clbw::findOneByField($website, clbw::WEBSITE, $type);
					if($exist){
						actionError("Website(".$website.") already exists in the blacklist/whitelist");
					}
					else{
						$clbw = new clbw();
						$clbw->setData($website, clbw::WEBSITE, $type);
						$ret = $clbw->addBWList();
					}
				}
				else{
					$website = NULL;
				}
			}
		}
		
		if ($ret)
			return actionSuccess("You have successfully added data to the " . $phrase . ".");
		else
			return actionError("Error while adding data to the " . $phrase . ".");
	}

	return true;
}


$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'edit_bw':
		$ret = edit($is_whitelist);
		if (!$ret)
			return;
		break;
	case 'add_bw':
		$ret = add($is_whitelist);
		if (!$ret)
			return;
		break;
	case 'delete_bw':
		$ret = delete_bw($is_whitelist);
		if (!$ret)
			return;
		break;
}

$where = getWhere();
if($where == '') {
	$where = ' WHERE bw.type = ' . $is_whitelist;
}
else{
	$where .= ' AND bw.type = ' . $is_whitelist;
}

$order = getOrder();
$limit = getLimit();

//query db
$sql = "SELECT count(*) as total 
		FROM classifieds_bw_list bw
		$where
	";

$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT *
		FROM classifieds_bw_list bw
		$where
		$order 
		$limit";

$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	if($is_whitelist){
		echo "No whitelisted.";
	}
	else{
		echo "No blacklisted.";	
	}
	
}

//pager
$pager = getPager($total);

//output
$phrase = 'Blacklist';
if($is_whitelist){
	$phrase = 'Whitelist';
}

$field_types = array(clbw::EMAIL => 'Email', clbw::PHONE => 'Phone', clbw::WEBSITE => 'Website');

echo "<h2>" . $phrase . "</h2>\n";

echo '<div id="addtobwlist" style="display: none;border: 1px solid #ddd;padding: 20px;margin-bottom: 30px;">';
echo "<h2>Add to " . $phrase . "</h2>";
	
	
echo "<form method=\"post\" action=\"\">";
echo "<table>";

echo "<tr><th>Email: </th><td><input type=\"text\" name=\"email1\" value=\"\"></td></tr>\n";
echo "<tr><th>Phone Number: </th><td><input type=\"text\" name=\"phone1\" value=\"\" size=\"8\"></td></tr>\n";
echo "<tr><th>Website: </th><td><input type=\"text\" name=\"website1\" value=\"\"></td></tr>\n";
echo "<tr><th></th><td><input type=hidden name=\"action\" value=\"add_bw\"/><input type=\"submit\" name=\"addsubmit\" value=\"Add\" />&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"button\" name=\"canceladd\" value=\"Cancel\" id=\"canceladdtobwlist\" /></td></tr>\n";
echo "</table>";
echo "</form>";
echo "</div>";
echo '<script type="text/javascript">';
echo '$(document).ready(function() {
		$("#showaddtobwlist").click(function(){
	        $("#addtobwlist").slideDown();
	        $("#showaddtobwlist").hide();
		});
		$("#canceladdtobwlist").click(function(){
	        $("#addtobwlist").slideUp();
	        $("#showaddtobwlist").show();
		});
		$(".deletefrombwlist").click(function(){
	        return confirm("Are you sure you want to delete?");
		});
	});';
echo '</script>';

displayFilterForm();

echo '<input type="button" name="addtobw" id="showaddtobwlist" value="Add to ' . $phrase . '" />';

echo $pager."<br />";

echo getFilterFormFields();

echo "<table class=\"control\">";
echo "<thead><tr><th class=\"check\"><input type=\"checkbox\" name=\"master\" value=\"\" /></th><th>".getOrderLink("Id", "id")."</th><th>".getOrderLink("Field Type", "field_type")."</th><th>".getOrderLink("Value", "value")."</th><th>".getOrderLink("Count Matches", "count_matches")."</th><th>".getOrderLink("Last Matched At", "last_matched_at")."</th><th>".getOrderLink("Last Matched Id", "last_matched_id")."</th><th>Added Date</th><th/></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
    echo "<tr>";
	echo "<td class=\"check\"><input type=\"checkbox\" name=\"id[]\" value=\"{$rox['id']}\" /></td>";
	echo "<td>{$rox['id']}</td>";
	echo "<td>".$field_types[$rox["field_type"]]."</td>";
	if($rox["field_type"] == 1){
		echo "<td>".makeproperphonenumber($rox["value"])."</td>";	
	}
	else{
		echo "<td>{$rox["value"]}</td>";	
	}
	echo "<td>{$rox["count_matches"]}</td>";
	echo "<td>{$rox["last_matched_at"]}</td>";
	echo "<td><a href=\"classifieds?cid={$rox["last_matched_id"]}\" target=\"_blank\">{$rox["last_matched_id"]}</a></td>";
	echo "<td>{$rox["add_date"]}</td>";
	echo "<td>";
		echo "<a href=\"".getActionLink("edit_bw", $rox['id'])."\">Edit</a>&nbsp;&middot&nbsp;";
		echo "<a class=\"deletefrombwlist\" href=\"".getActionLink("delete_bw", $rox['id'])."\">Delete</a>";
	echo "</td>";

	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;

?>
