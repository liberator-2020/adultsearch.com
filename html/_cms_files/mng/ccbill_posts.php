<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!$account->isrealadmin())
	return;

global $url_last_piece, $filters, $where_cols, $classifieds;

//configuration
$url_last_piece = "ccbill_posts";
$where_cols = array(
//	"id"		 => array("title" => "ID", "where" => "ap.id", "match" => "exact", "size" => "7"),
);


function approved() {
	global $account, $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid ccbill post id !");

	$res = $db->q("SELECT * FROM ccbill_post WHERE id = ?", array($action_id));
	if ($db->numrows($res) != 1)
		return actionError("Cant find ccbill post with id '$action_id' !");
	$row = $db->r($res);

	$arr = explode("&", $row["post"]);
	$params = array();
	foreach ($arr as $item) {
		$arr2 = explode("=", $item);
		$params[$arr2[0]] = $arr2[1];
	}

	$now = time();
	$ccbill = new ccbill();
	$checksum = md5($params["account_id"].$params["what"].$params["item_id"].$action_id."0".$params["page"].$ccbill->pepper);
	$response = array(
		"customer_fname" => "Gomez",
		"customer_lname" => "Ramirez",
		"email" => "jle@email.cz",
		"subscription_id" => "999999000{$now}",
		"clientAccnum" => 948343,
		"clientSubacc" => 0000,
		"address1" => "123 Kardashian Road",
		"city" => "Las Vegas",
		"state" => "NV",
		"country" => "US",
		"zipcode" => "89140",
		"start_date" => date("Y-m-d H:i:s", $now-10),
		"referringUrl" => "https://adultsearch.com/mng/ccbill_posts",
		"formName" => "211cc",
		"cardType" =>"VISA",
		"responseDigest" => "4fbe78e97472df5bab614b1876c1f648",
		"promo_code" => 0,
		"checksum" => $checksum,
		"what" => $params["what"],
		"page" => $params["page"],
		"ccbill_post_id" => $action_id,
		"account_id" => $params["account_id"],
		"item_id" => $params["item_id"],
		"typeId" => 0,
		"initialPrice" => $params["formPrice"],
		"initialPeriod" => $params["formPeriod"],
		"recurringPrice" => $params["formRecurringPrice"],
		"recurringPeriod" => $params["formRecurringPeriod"],
		"rebills" => $params["formRebills"],
		"ip_address" => account::getUserIp(),
		);

	$url = "http://adultsearch.com/payment/ccbill_approved?".http_build_query($response);
	system::go($url);
	die;
}

function cont() {
	global $account, $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid ccbill post id !");

	$url = "http://adultsearch.com/payment/continue";
	system::go($url);
	die;
}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'approved':
		$ret = approved();
		if (!$ret)
			return;
		break;
	case 'denied':
		$ret = denied();
		if (!$ret)
			return;
		break;
	case 'cont':
		$ret = cont();
		if (!$ret)
			return;
		break;
	default:
		break;
}

$where = getWhere();
//$having = getHaving();
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
	$order = "ORDER BY cp.id DESC";

if (empty($limit))
	$order = "LIMIT 20";

//query db
$sql = "SELECT count(*) as total
		FROM ccbill_post cp
		$where
		";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT *
		FROM ccbill_post cp
		$where
		$order 
		$limit";

$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No ccbill posts.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>CCBill posts</h2>\n";

displayFilterForm();

echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr>
<th>".getOrderLink("CCBill Post Id", "cp.id")."</th>
<th>".getOrderLink("Timestamp", "cp.stamp")."</th>
<th>".getOrderLink("Result", "cp.result")."</th>
<th>Account Id</th>
<th>What</th>
<th>Item Id</th>
<th>Page</th>
<th></tr></thead>\n";
echo "<tbody>";
while ($row = $db->r($res)) {

	echo "<tr>";
	echo "<td>{$row["id"]}</td>";
	echo "<td>".date("Y-m-d H:i:s T", $row["stamp"])."</td>";
	echo "<td>{$row["result"]}</td>";

	$arr = explode("&", $row["post"]);
	$params = array();
	foreach ($arr as $item) {
		$arr2 = explode("=", $item);
		$params[$arr2[0]] = $arr2[1];
	}

	echo "<td>{$params["account_id"]}</td>\n";
	echo "<td>{$params["what"]}</td>\n";
	echo "<td>{$params["item_id"]}</td>\n";
	echo "<td>{$params["page"]}</td>\n";

	$links = "";

	if ($row["result"] == NULL) {
		$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("approved", $row["id"])."\" target=\"ccbill_simulate\" >simulate approval</a>";
		$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("denied", $row["id"])."\" target=\"ccbill_simulate\" >simulate denial</a>";
	} else {
		$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("cont", $row["id"])."\" target=\"ccbill_simulate\" >simulate continue</a>";
	}

	echo "<td>{$links}</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;
echo "</form>";

?>
