<style type="text/css">
table {
	border-collapse: collapse;
}
table td, table th {
	padding: 4px;
	border: 1px solid #ccc;
}
</style>
<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("account_ban"))
	die("Invalid access");

require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
require_once(_CMS_ABS_PATH."/inc/classes/parsecsv.lib.php");

function upload_chargeback_report() {
	global $db;

	//store csv file into temp directory
	$upl_dir = _CMS_ABS_PATH."/tmp/";
	$file_base = "chr-".date("YmdHis");
	$file = $file_base.".csv";
	$filepath = $upl_dir."/".$file;
	$upload = new Upload($_FILES['file']);
	if ($upload->uploaded) {
		@unlink($filepath);
		$upload->file_new_name_body = $file_base;
		$upload->file_new_name_ext = "csv";
		$upload->mime_check = false;
		$upload->Process($upl_dir);
		if (!$upload->processed) {
			echo "<span style=\"color: red;\">Error while copying uploaded csv file to directory '{$upl_dir}' - error: ".$upload->error." !</span><br />";
			return false;
		}
	}

	system::go("/mng//chargeback_report?file={$file}");
}

function process_chargeback_report() {
	global $db;

	$upl_dir = _CMS_ABS_PATH."/tmp/";
	$file = $_REQUEST["file"];
	$filepath = $upl_dir."/".$file;
	$process_max = 49;

	//parse csv file
	$csv = new parseCSV();
	$csv->auto($filepath);
	$num_entries = count($csv->data);
	if ($num_entries > 0)
		echo "Parsed CSV file: <strong>{$num_entries}</strong> entries.<br />\n";
	else {
		echo "<span style=\"color: red;\">Error: CSV file does not have any entry.</span><br />\n";
		return;
	}

	$pos = intval($_REQUEST["pos"]);
	if ($pos) {
		echo "Starting from position {$pos}<br />\n";
	}

	echo "<table>";
	echo "<thead><tr>";
	echo "<th>Invoice num.</th>";
	echo "<th>Transaction #id</th>";
	echo "<th>Refunded/Chargeback</th>";
	echo "<th>Account</th>";
	echo "<th>Class. ads</th>";
	echo "<th></th>";
	echo "</tr></thead>";
	echo "<tbody>";

	//echo print_r($csv->titles, true);
	$linenum = 0; $skipped = 0; $i = 0;
	foreach($csv->data as $line) {
		$line_num++;
		if ($pos && $line_num < $pos)
			continue;
		$invoince_num = NULL;
		$amount = NULL;
		$line_ok = true;
		foreach ($line as $key => $val) {
			if ($key == "Merchant Order #") {
				if ($val == "") {
					$line_ok = false;
				}
				$invoice_num = preg_replace('/[^0-9A-Za-z]/', '', $val);
			} else if ($key == "Amount") {
				$amount = floatval($val);
			}
			continue;
		}
		if (!$line_ok)
			continue;

		$site = substr($invoice_num, 0, 2);
		if ($site == "TS") {
			//echo "Invoice_num='{$invoice_num}', amount='{$amount}' Site=TS -> skipping...<br />";
			$skipped++;
			continue;
		} else if ($site != "AS") {
			echo "Invoice_num='{$invoice_num}', amount='{$amount}' <strong>Error: UNKNOWN SITE '{$site}' ! -> skipping...<br />";
			continue;
		}
		$payment_id = substr($invoice_num, 2);

		$rebill = false;
		if (substr($invoice_num, -1) == "R") {
			$rebill = true;
			$payment_id = substr($payment_id, 0, -1);
		}

		$payment_id = intval($payment_id);
		//echo "Invoice_num='{$invoice_num}', amount='{$amount}', site='{$site}', payment_id={$payment_id}, rebill=".intval($rebill)."<br />";

		//find payment
		$res = $db->q("SELECT p.trans_id FROM payment p WHERE p.id = ?", [$payment_id]);
		if ($db->numrows($res) != 1) {
			echo "<strong>ERROR: Payment not found for payment_id={$payment_id}' !</strong><br />";
			continue;
		}
		$row = $db->r($res);
		$payment_trans_id = $row["trans_id"];
		//echo "payment_id={$payment_id}, payment_trans_id={$payment_trans_id}<br />\n";

		//find transactions
		if (!$rebill) {
			$andwhere = " and t.trans_id = ?";
		} else {
			$andwhere = " and t.trans_id <> ?";
		}
		$res = $db->q("
			SELECT t.id, t.amount, t.trans_id
				, ap.id as purchase_id
				, ar.type as refund_type, ar.id as refund_id, ar.amount as refund_amount, ar.transaction_time as refund_stamp, aar.username as refund_who
			FROM transaction t 
			INNER JOIN account_purchase ap on ap.transaction_id = t.trans_id
			LEFT JOIN account_refund ar on ar.purchase_id = ap.id
			LEFT JOIN account aar on aar.account_id = ar.author_id
			WHERE t.payment_id = ? {$andwhere}", 
			[$payment_id, $payment_trans_id]
			);
		$cnt = $db->numrows($res);
		if ($cnt == 0) {
			//echo "<strong>ERROR: Transaction not found !</strong><br />";
			echo "<tr>";
			echo "<td><a href=\"/mng/sales?payment_id=".preg_replace('/[^0-9]/', '', $invoice_num)."\" target=\"_blank\">{$invoice_num}</a></td>";
			echo "<td colspan=\"5\"><span style=\"color: red;\">Transaction not found !</span></td>";
			echo "</tr>";
		} else if ($cnt > 1) {
			echo "<tr>";
			echo "<td><a href=\"/mng/sales?payment_id=".preg_replace('/[^0-9]/', '', $invoice_num)."\" target=\"_blank\">{$invoice_num}</a></td>";
			//echo "<td colspan=\"5\"><span style=\"color: red;\">Multiple transactions found !</span></td>";
			echo "<td colspan=\"5\"><table>";
			while ($row = $db->r($res)) {
				echo "<tr>";
				echo "<td><a href=\"/mng/sales?transaction_id={$row["trans_id"]}\" target=\"_blank\">#{$row["trans_id"]}</a></td>";
				echo "<td>";
				if ($row["refund_type"] == "R") {
					echo "Refunded";
				} else if ($row["refund_type"] == "CH") {
					echo "Marked as chargeback";
				} else if (is_null($row["refund_type"])) {
					echo "<span style=\"color: red;font-weight: bold;\">No refund or chargeback!</span>";
				} else {
					echo "<span style=\"color: red; font-weight: bold;\">ERROR: refund_type = '{$row["refund_type"]}' - contatc admin</span>";
				}
				echo "</td>";
				echo "</tr>";				
			}
			echo "</table></td>";
			echo "</tr>";
		} else {
			//good, only one transaction matches
			$row = $db->r($res);
			//echo "OK, transaction found, #{$row["id"]}<br />";
			echo "<tr>";
			echo "<td><a href=\"/mng/sales?payment_id=".preg_replace('/[^0-9]/', '', $invoice_num)."\" target=\"_blank\">{$invoice_num}</a></td>";
			echo "<td><a href=\"/mng/sales?transaction_id={$row["trans_id"]}\" target=\"_blank\">#{$row["trans_id"]}</a></td>";

			echo "<td>";
			if ($row["refund_type"] == "R") {
				echo "Refunded";
			} else if ($row["refund_type"] == "CH") {
				echo "Marked as chargeback";
			} else if (is_null($row["refund_type"])) {
				echo "<span style=\"color: red;font-weight: bold;\">No refund or chargeback!</span>";
			} else {
				echo "<span style=\"color: red; font-weight: bold;\">ERROR: refund_type = '{$row["refund_type"]}' - contatc admin</span>";
			}
			echo "</td>";

			echo "<td>";
			echo "</td>";

			echo "<td>";
			echo "</td>";

			echo "<td>";
			echo "</td>";
		}
		echo "</tr>";
		$i++;

		if ($i > $process_max)
			break;
	}

	echo "</tbody></table><br />";
	echo "<br />Processed {$i} entries, skipped {$skipped} TS chargebacks, ".($num_entries - ($pos + $i))." chargebacks to go.<br />";
	if ($i > $process_max)
		echo "<a href=\"/mng/chargeback_report?file={$file}&pos=".($line_num+1)."\" class=\"btn btn-sm btn-default\">Next</a><br />\n";
	else
		echo "<strong>Done</strong><br />\n";
	return;
}


if ($_REQUEST["submit"] != "") {
	upload_chargeback_report();
} elseif ($_REQUEST["file"]) {
	process_chargeback_report();
} else {
	?>
	<h1>Upload chargeback CSV file:</h1>
	<form method="post" enctype="multipart/form-data">
	Chargback report csv file:<input type="file" name="file" style="width: 250px;"/><br />
	<input type="submit" name="submit" value="Submit" />
	</form>
	<?php
}

return;
?>
