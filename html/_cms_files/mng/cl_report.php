<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $db, $account, $smarty, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("access_admin_cl_stats"))
    return;

$timezone = "EST";

$range_or_period = GetGetParam("range_or_period");
$yearFrom = 2018;
$dayFrom = 1;
if( $range_or_period != 2 ) {
	$dayTo = date("d");
	$monthTo = date("m"); 
	$yearTo = date("Y");  
	$period = $_GET["period"];
	switch($period) {
		case 1:
			//last 7 days
			$from = time() - 86400*7;
			$transaction_time_sql = " AND t.stamp >= {$from} ";
			break;   
		case 2:
			//yesterday
			$dt = new \DateTime("yesterday", new \DateTimeZone($timezone));
    	    $from = $dt->getTimestamp();
    	    $to = $from + 86400;
			$transaction_time_sql = " AND t.stamp >= {$from} AND t.stamp < {$to}";
			break;
		case 3:
			//today
			$dt = new \DateTime("today", new \DateTimeZone($timezone));
    	    $from = $dt->getTimestamp();
    	    $to = $from + 86400;
			$transaction_time_sql = " AND t.stamp >= {$from} AND t.stamp < {$to}";
			break;
		case 4:
			//this month
			$dt1 = new \DateTime("first day of this month", new \DateTimeZone($timezone));
			$dt1->setTime(0, 0);
			$dt2 = new \DateTime("first day of next month", new \DateTimeZone($timezone));
			$dt2->setTime(0, 0);
    	    $from = $dt1->getTimestamp();
    	    $to = $dt2->getTimestamp();
			$transaction_time_sql = " AND t.stamp >= {$from} AND t.stamp < {$to}";
			break;
		case 5:
			$dt1 = new \DateTime("first day of this month", new \DateTimeZone($timezone));
			$dt1->setTime(0, 0);
			$dt2 = new \DateTime("first day of next month", new \DateTimeZone($timezone));
			$dt2->setTime(0, 0);
    	    $from = $dt1->getTimestamp();
    	    $to = $dt2->getTimestamp();
			$transaction_time_sql = " AND t.stamp >= {$from} AND t.stamp < {$to}";
			break;  
		default:
			//last 30 days
			$from = time() - 86400*30;
			$transaction_time_sql = " AND t.stamp >= {$from} ";
			break;
	}
} else {
	$dayFrom = GetGetParam("dayFrom");
	$monthFrom = GetGetParam("monthFrom");
	$yearFrom = GetGetParam("yearFrom");

	$dayTo = GetGetParam("dayTo");
	$monthTo = GetGetParam("monthTo");
	$yearTo = GetGetParam("yearTo");
		
	$start_timer = mktime(0,0,1,$monthFrom,$dayFrom,$yearFrom);
	$end_timer = mktime(0,0,1, $monthTo, $dayTo, $yearTo);
			
	if( $start_timer > $end_timer )
		$smarty->assign("report_error", "Wrong date selection.");

	$transaction_time_sql = " AND t.stamp >= {$start_timer} AND t.stamp <= {$end_timer} ";  
}

/*
$promocode_sql = "";
if ($_REQUEST["promocode"] != "") {
	$promocode_sql = " and c.promo_code = '{$_REQUEST["promocode"]}' ";
	$smarty->assign("promocode", $_REQUEST["promocode"]);
}

$paid_sql = "";
if ($_REQUEST["paid"] != "") {
	if ($_REQUEST["paid"] === "0") {
		$paid_sql = " and c.paid = 0 ";
		$smarty->assign("paid", "0");
	} else if ($_REQUEST["paid"] === "1") {
		$paid_sql = " and c.paid > 0 ";
		$smarty->assign("paid", "1");
	}
}
*/

$agency_sql = "";
if ($_REQUEST["agency"] != "") {
	if ($_REQUEST["agency"] == "na") {
		$agency_sql = " AND a.agency = 0 ";
		$smarty->assign("agency", "na");
	} else if ($_REQUEST["agency"] == "a") {
		$agency_sql = " AND a.agency = 1 ";
		$smarty->assign("agency", "a");
	}
}

/*
$location_sql = "";
if ($_REQUEST["location"] != "") {
	if ($_REQUEST["location"] == "us") {
		$location_sql = " and l.country_id = 16046 ";
	} else if ($_REQUEST["location"] === "ca") {
		$location_sql = " and l.country_id = 16047 ";
	} else if ($_REQUEST["location"] === "uk") {
		$location_sql = " and l.country_id = 41973 ";
	} else if ($_REQUEST["location"] === "do") {
		$location_sql = " and l.country_id IN (16046, 16047, 41973) ";
	} else if ($_REQUEST["location"] === "in") {
		$location_sql = " and l.country_id NOT IN (16046, 16047, 41973) ";
	}
	$smarty->assign("location", $_REQUEST["location"]);
}
*/

$smarty->assign("dayFrom", $dayFrom);   
$smarty->assign("monthFrom", $monthFrom);
$smarty->assign("yearFrom", $yearFrom);
$smarty->assign("dayTo", $dayTo);
$smarty->assign("monthTo", $monthTo);
$smarty->assign("yearTo", $yearTo);
$smarty->assign("period", $_GET["period"]);
$smarty->assign("range_or_period", $_GET["range_or_period"]);

switch($_GET['sort']){
	case 'name': $sort = "l.loc_name"; break;
	default: $sort = "made desc";
}


$where = "WHERE {$transaction_time_sql} ";

$sql = "
	SELECT t.id, t.amount
		, a.agency
		, COUNT(distinct pi.id) payment_items_cnt
		, GROUP_CONCAT(pi.classified_id) classified_ids
		, MAX(pi.type) type
		, MAX(pi.classified_status) classified_status
		, GROUP_CONCAT(pi.loc_id) as loc_ids
		, MAX(l.loc_name) loc_name, MAX(l.s) s
	FROM transaction t
	INNER JOIN payment p on t.payment_id = p.id
	INNER JOIN account a on a.account_id = p.account_id
	INNER JOIN payment_item pi on pi.payment_id = p.id
	LEFT JOIN location_location l on l.loc_id = pi.loc_id
	WHERE (pi.type = 'classified' OR pi.type = 'repost') {$transaction_time_sql} {$agency_sql}
	GROUP BY t.id
	ORDER BY 1
	";
//_d("sql={$sql}");
$res = $db->q($sql);

$total_new_count = $total_new_amount = $total_renewal_count = $total_renewal_amount = $total_upgrade_count = $total_upgrade_amount = $total_count = $total_amount = 0;
$location_stats = [];
while($row = $db->r($res)) {

	if ($row["type"] == "repost") {
		if (!array_key_exists("R", $location_stats)) {
			$location_stat = [
				"loc_label" => "Top-Up Credits",
				"new_count" => "-",
				"new_amount" => "-",
				"renewal_count" => "-",
				"renewal_amount" => "-",
				"upgrade_count" => "-",
				"upgrade_amount" => "-",
				"location_total_count" => 0,
				"location_total_amount" => 0,
				];
		} else {
			$location_stat = $location_stats["R"];
		}
		$amount = $row["amount"];
		$location_stat["location_total_count"] = $location_stat["location_total_count"] + 1;
        $location_stat["location_total_amount"] = $location_stat["location_total_amount"] + $amount;
        $total_count += 1;
        $total_amount += $amount;
        $location_stats["R"] = $location_stat;
		continue;
	}

	$loc_ids = explode(",", $row["loc_ids"]);
	foreach ($loc_ids as $loc_id) {
		$location_stat = $location_stats[$loc_id];
		if (!$location_stat) {
			if (count($loc_ids) == 1) {
				$loc_label = $row["loc_name"].", ".$row["s"];
			} else {
				$loc = location::findOneById($loc_id);
				$loc_label = $loc->getLabel();
			}
			$location_stat = [
				"loc_label" => $loc_label,
				"new_count" => 0,
				"new_amount" => 0,
				"renewal_count" => 0,
				"renewal_amount" => 0,
				"upgrade_count" => 0,
				"upgrade_amount" => 0,
				"location_total_count" => 0,
				"location_total_amount" => 0,
				];
		}
		$amount = $row["amount"] / count($loc_ids);
		$type = $row["classified_status"];
		if ($type == "N") {
			$location_stat["new_count"] = $location_stat["new_count"] + 1;
			$location_stat["new_amount"] = $location_stat["new_amount"] + $amount;
			$total_new_count += 1;
			$total_new_amount += $amount;
		} else if ($type == "R") {
			$location_stat["renewal_count"] = $location_stat["renewal_count"] + 1;
			$location_stat["renewal_amount"] = $location_stat["renewal_amount"] + $amount;
			$total_renewal_count += 1;
			$total_renewal_amount += $amount;
		} else if ($type == "U") {
			$location_stat["upgrade_count"] = $location_stat["upgrade_count"] + 1;
			$location_stat["upgrade_amount"] = $location_stat["upgrade_amount"] + $amount;
			$total_upgrade_count += 1;
			$total_upgrade_amount += $amount;
		}
		$location_stat["location_total_count"] = $location_stat["location_total_count"] + 1;
		$location_stat["location_total_amount"] = $location_stat["location_total_amount"] + $amount;
		$total_count += 1;
		$total_amount += $amount;
		$location_stats[$loc_id] = $location_stat;
	}
}
//_darr($location_stats);

function cmp($a, $b) {
	$av = $a["location_total_amount"];
	$bv = $b["location_total_amount"];
	if ($ab == $bv)
		return 0;
	else if ($av < $bv)
		return 1;
	else
		return -1;
}
uasort($location_stats, cmp);

$location_stats["total"] = [
	"loc_label" => "Total",
	"new_count" => $total_new_count,
	"new_amount" => $total_new_amount,
	"renewal_count" => $total_renewal_count,
	"renewal_amount" => $total_renewal_amount,
	"upgrade_count" => $total_upgrade_count,
	"upgrade_amount" => $total_upgrade_amount,
	"location_total_count" => $total_count,
	"location_total_amount" => $total_amount,
	];

$smarty->assign("location_stats", $location_stats);

/*
//add totals
//$report[] = array(
array_unshift($report, array(
	"loc_name" => "Total",
	"total" => $total,
	"ar" => $ar,
	"sc" => $sc,
	"recurring" => $recurring,
	"made" => number_format($made, 2),
	"link" => "",
));
*/

/*
//export to CSV
if ($_REQUEST["export"] == "export") {
	$fp = fopen(_CMS_ABS_PATH.'/UserFiles/excel/cl_report.csv', 'w');
    $fields = "\"City\",\"Link\",\"New ads\",\"Total auto reposts\",\"Total city thumbnails\",\"Recurring\",\"Total made\"\n";
    fputs($fp, $fields);
    foreach ($report as $item) {
        $fields = "\"{$item["loc_name"]}\",\"{$item["link"]}\",\"{$item["total"]}\",\"{$item["ar"]}\",\"{$item["sc"]}\",\"{$item["recurring"]}\",\"{$item["made"]}\"\n";
        fputs($fp, $fields);
    }
    fclose($fp);
	$system = new system();
    $system->moved("http://adultsearch.com/UserFiles/excel/cl_report.csv");
	die;
}
*/

//$smarty->assign('report', $report);
$smarty->display(_CMS_ABS_PATH."/templates/mng/mng_cl_report.tpl");

?>
