<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("access_admin_classifieds"))
	return;

$sql = "
select l.*, count(*) as cnt, group_concat(cs.post_id) as ad_ids
from classifieds_sponsor cs
inner join classifieds c on c.id = cs.post_id and c.done = 1 and c.deleted IS NULL
inner join location_location l on cs.loc_id = l.loc_id
where cs.done = 1
group by loc_id
order by count(*) desc
";

$res = $db->q($sql);

echo "<h1>City thumbnails location stats</h1>\n<table class=\"control\">";
echo "<thead><tr>
<th>Location</th>
<th>Count</th>
<th>Ads</th>
</tr></thead>\n";
echo "<tbody>";
$total = 0;
while ($rox = $db->r($res)) {

	$loc = location::withRow($rox);
	if (!$loc) {
		echo "Can't find loc by id {$rox["loc_id"]}!<br />\n";
		continue;
	}

	echo "<tr>";
	echo "<td><a href=\"".$loc->getUrl()."\" >{$rox["loc_name"]}, {$rox["s"]}</a></td>";
	echo "<td>{$rox["cnt"]}</td>";

	echo "<td>";
	$ad_ids = explode(",", $rox["ad_ids"]);
	foreach ($ad_ids as $id) {
		echo "<a href=\"/mng/classifieds?cid={$id}\">{$id}</a>, ";
	}
	echo "</td>";

	$total += $rox["cnt"];

	echo "</tr>\n";
}
echo "<tr><td><strong>Total</strong></td><td><strong>{$total}</strong></td><td/></tr>";
echo "</tbody>";
echo "</table><br />";

?>
