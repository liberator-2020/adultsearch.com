<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) { 
	$account->asklogin(); 
	return false; 
}

if (!$account->isadmin()) {
	echo "You dont have privileges to access this page!";
	die;
}

$clad_id = intval($_REQUEST["clad_id"]);
$from = $_REQUEST["from"];
$to = $_REQUEST["to"];
$group = $_REQUEST["group"];
$export = $_REQUEST["export"];

if ($_REQUEST["submit"] == "Submit" && empty($to))
	$to = date("Y-m-d");


if (!$export) {
echo "
<h1>Classified ad click stats</h1>
<form action=\"\" type=\"post\">
Classified ad #id: <input type=\"text\" name=\"clad_id\" value=\"{$clad_id}\" size=\"7\" />";

echo " Date from: <input type=\"date\" name=\"from\" value=\"{$from}\" />\n";

echo " Date to: <input type=\"date\" name=\"to\" value=\"{$to}\" />\n";

if ($group == "day")
	$selected_day = " selected=\"selected\" ";
else if ($group == "month")
	$selected_month = " selected=\"selected\" ";
echo " Group by: <select name=\"group\"><option value=\"day\"{$selected_day}>Day</option><option value=\"month\"{$selected_month}>Month</option></select>\n";

echo '<input type="submit" name="submit" value="Submit" /></form><br />';
}

if ($_REQUEST["submit"] != "Submit")
	return;


//-------------------------------------
// PROCESSING

//echo "clad_id={$clad_id}, from={$from}, to={$to}, group={$group}<br />\n";

if ($export) {
	if ($export == "csv") {
		$filename = "clad_click_stats_{$clad_id}.".time().".csv";
		$h = fopen(_CMS_ABS_PATH."/tmp/{$filename}", "w");
	} else {
		die("Uknknown type of export!");
	}
}



if ($group == "day")
	$groupby = "GROUP BY date";
else if ($group == "month")
	$groupby = "GROUP BY YEAR(date), MONTH(date)";

$query = "SELECT YEAR(date) as year, MONTH(date) as month, DAY(date) as day, SUM(c) as click_count
			FROM classifieds_click
			WHERE post_id = ? AND date >= ? AND date <= ?
			{$groupby}
			ORDER BY date ASC";

if (!$export) {
	echo "<style type=\"text/css\">table.stats {border-collapse: collapse; border-spacing: 0px;} table.stats td, table.stats th {padding: 2px; text-align: right; border: 1px solid #999;}</style>\n";
	echo "<strong>#{$clad_id}</strong><br />";
	$download_link = "/mng/clad_clicks?clad_id={$clad_id}&from={$from}&to={$to}&group={$group}&export=csv&submit=Submit";
	echo "<a href=\"{$download_link}\">Download CSV</a><br />\n";
}

if (!$export) {
	echo "<table class=\"stats\">";
	echo "<tr><th>Year</th>";
	echo "<th>Month</th>";
	if ($group == "day")
		echo "<th>Day</th>";
	echo "<th>Clicks</th></tr></thead><tbody>\n";
} else {
	$line = "Year,Month";
	if ($group == "day")
		$line .= ",Day";
	$line .= ",Clicks\n";
	fputs($h, $line);
}

$res = $db->q($query, array($clad_id, $from, $to));
$pops = array();
$total = 0;
while ($row = $db->r($res)) {
	$year = $row["year"];
	$month = $row["month"];
	$day = $row["day"];
	$count = $row["click_count"];
	$total += $count;

	if (!$export) {
		echo "<tr><td>{$year}</td><td>{$month}</td>";
		if ($group == "day")
			echo "<td>{$day}</td>";
		echo "<td>{$count}</td></tr>\n";
	} else {
		$line = "{$year},{$month}";
		if ($group == "day")
			$line .= ",{$day}";
		$line .= ",{$count}\n";
		fputs($h, $line);
	}
}

if (!$export) {
	$colspan = 2;
	if ($group == "day")
		$colspan = 3;
	echo "<tr><td colspan=\"{$colspan}\"><strong>Total</strong></td><td>{$total}</td></tr>\n";
	echo "</tbody></table>\n";
	return;
} else {
	$comma = "";	
	if ($group == "day")
		$comma = ",";
	fputs($h, "Total,,{$comma}{$total}\n");
	fclose($h);
	system::moved("http://adultsearch.com/tmp/{$filename}");
}


//END
