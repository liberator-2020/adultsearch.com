<?php

use App\Service\Classified\WaitingList;

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("access_admin_classifieds"))
	return;

$params = [
	[
		WaitingList::STATUS_PENDING,
		WaitingList::STATUS_RESERVED,
	],
];

$res = $db->q("
select l.*, cs.type, count(distinct classified_id) as cnt, group_concat(distinct(classified_id)) as ad_ids,
(SELECT COUNT(*) 
 FROM classified_waiting_list 
 WHERE status_id IN ?  
 AND location_id = cs.loc_id 
 AND classified_type_id = cs.type) as waiting_count
from classified_sticky cs
inner join classifieds c on c.id = cs.classified_id and c.done = 1 and c.deleted IS NULL
inner join location_location l on cs.loc_id = l.loc_id
where cs.done = 1
group by cs.loc_id, cs.type
order by count(*) desc;
", $params);

$total = 0;
$loc_rows = [];
while ($row = $db->r($res)) {

	$location = location::withRow($row);
	if (!$location) {
		echo "Can't find loc by id {$row["loc_id"]}!<br />\n";
		continue;
	}

	$type         = $row["type"];
	$cnt          = $row["cnt"];
	$ad_ids       = $row["ad_ids"];
	$waitingCount = $row['waiting_count'];

	if (array_key_exists($location->getId(), $loc_rows)) {
		$loc = $loc_rows[$location->getId()];
	} else {
		$loc = [
			"location"          => $location,
			"total_cnt"         => 0,
			"fe_awaiting_count" => 0,
			"ts_awaiting_count" => 0,
			"br_awaiting_count" => 0,
			"fe_weekly_price"   => classifieds::get_sticky_upgrade_price(1, $location->getId(), 7),
			"fe_monthly_price"  => classifieds::get_sticky_upgrade_price(1, $location->getId(), 30),
			"br_weekly_price"   => classifieds::get_sticky_upgrade_price(6, $location->getId(), 7),
			"br_monthly_price"  => classifieds::get_sticky_upgrade_price(6, $location->getId(), 30),
			"ts_weekly_price"   => classifieds::get_sticky_upgrade_price(2, $location->getId(), 7),
			"ts_monthly_price"  => classifieds::get_sticky_upgrade_price(2, $location->getId(), 30),
		];
	}

	$loc["total_cnt"] = $loc["total_cnt"] + $cnt;
	$total += $cnt;

	switch ($type) {
		case 1:
			$loc["fe_cnt"] = $cnt;
			$loc["fe_ad_ids"] = $ad_ids;
			$loc["fe_awaiting_count"] = $waitingCount;
			break;
		case 2:
			$loc["ts_cnt"] = $cnt;
			$loc["ts_ad_ids"] = $ad_ids;
			$loc["ts_awaiting_count"] = $waitingCount;
			break;
		case 6:
			$loc["br_cnt"] = $cnt;
			$loc["br_ad_ids"] = $ad_ids;
			$loc["br_awaiting_count"] = $waitingCount;
			break;
		default:
			break;
	}
	$loc_rows[$location->getId()] = $loc;
}

echo "<div class='row'>";
echo "<div class='col-xs-10'>";
echo "<h1>Sticky sponsor location stats</h1>\n<table class=\"control\">";
echo "<thead><tr>
<th>Location</th>
<th>Count</th>
<th>FE</th>
<th>TS</th>
<th>BR</th>
<th>FE (Awaiting)</th>
<th>TS (Awaiting)</th>
<th>BR (Awaiting)</th>
<th>Prices</th>
<th>Ads</th>
</tr></thead>\n";
echo "<tbody>";
foreach ($loc_rows as $loc) {

	$location = $loc["location"];

	echo "<tr>";

	echo "<td><a href=\"".$location->getUrl()."\" >{$location->getLabel()}</a></td>";

	echo "<td>{$loc["total_cnt"]}</td>";

	$class = ($loc["fe_cnt"] >= classifieds::$sticky_count_limit) ? "class=\"sold-out\"" : "";
	echo "<td {$class}>{$loc["fe_cnt"]}</td>";

	$class = ($loc["ts_cnt"] >= classifieds::$sticky_count_limit) ? "class=\"sold-out\"" : "";
	echo "<td {$class}>{$loc["ts_cnt"]}</td>";

	$class = ($loc["br_cnt"] >= classifieds::$sticky_count_limit) ? "class=\"sold-out\"" : "";
	echo "<td {$class}>{$loc["br_cnt"]}</td>";

	echo "<td>{$loc["fe_awaiting_count"]}</td>";
	echo "<td>{$loc["ts_awaiting_count"]}</td>";
	echo "<td>{$loc["br_awaiting_count"]}</td>";

    echo "<td>";
	echo "<b>FE</b> - \${$loc["fe_weekly_price"]} / \${$loc["fe_monthly_price"]}; ";
	echo "<b>BR</b> - \${$loc["br_weekly_price"]} / \${$loc["br_monthly_price"]}; ";
	echo "<b>TS</b> - \${$loc["ts_weekly_price"]} / \${$loc["ts_monthly_price"]}; ";
	echo "</td>";

	echo "<td>";
	if (!empty($loc['fe_ad_ids'])){
        $fe_ids = explode(",", $loc["fe_ad_ids"]);
        echo '<b>FE</b> - ';
        foreach ($fe_ids as $fe_id) {
            echo "<a href=\"/mng/classifieds?cid={$fe_id}\">{$fe_id}</a>, ";
        }
	}

	if(! empty($loc["br_ad_ids"])){
        $br_ids = explode(",", $loc["br_ad_ids"]);
        echo '<b>BR</b> - ';
        foreach ($br_ids as $br_id) {
            echo "<a href=\"/mng/classifieds?cid={$br_id}\">{$br_id}</a>, ";
        }
	}

	if(! empty($loc["ts_ad_ids"])){
        $ts_ids = explode(",", $loc["ts_ad_ids"]);
        echo '<b>TS</b> - ';
        foreach ($ts_ids as $ts_id) {
            echo "<a href=\"/mng/classifieds?cid={$ts_id}\">{$ts_id}</a>, ";
        }
	}
	echo "</td>";
	echo "</tr>\n";
}
echo "<tr><td><strong>Total</strong></td><td><strong>{$total}</strong></td><td/></tr>";
echo "</tbody>";
echo "</table><br />";
echo "</div><div class='col-xs-2'>";

echo '<div id="legend">';
echo "<h4>Legend</h4>";
echo "<ul>";

echo '<li><span class="label label-danger">Sold Out</span></li>';

echo '</ul>';
echo '</div>';
echo "</div></div>";

?>
