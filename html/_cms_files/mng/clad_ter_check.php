<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("access_admin_classifieds"))
    return;

global $ipp, $url_last_piece, $filters, $where_cols;

//configuration
$ipp = 100;
$url_last_piece = "clad_ter_check";
$where_cols = array(
	"cid"		 => array("title" => "ID", "where" => "c.id", "match" => "exact", "size" => "7"),
	"firstname"	 => array("title" => "Firstname", "where" => "c.firstname", "size" => "15"),
);

function ter() {
	global $db, $account;
	$system = new system();

	$clad_id = intval($_REQUEST["action_id"]);
	if ($clad_id == 0)
		return actionError("Invalid classified id !");
	$clad = clad::findOnebyId($clad_id);
	if ($clad == false)
		return actionError("Cant find classified for id={$clad_id} !");

	if ($_REQUEST["submit"] == "Linked") {
		$clad->setTerCheckStamp(time());
		$ret = $clad->update();
		if ($ret) {
			audit::log("CLA", "TER Check", $clad->getId(), "Already Linked", $account->getId());
			success_redirect("Ad #{$clad_id} successfully marked as already linked.", "/mng/clad_ter_check");
		} else {
			error_redirect("Error marking ad as TER already linked for classified ad #{$clad_id} !", "/mng/clad_ter_check");
		}
	}

	if ($_REQUEST["submit"] == "Unapproved") {
		$clad->setTerCheckStamp(time());
		$curr_ter = $clad->getTer();
		$clad->setTerCheckUnapproved($curr_ter);
		$clad->setTer(NULL);
		$ret = $clad->update();
		if ($ret) {
			audit::log("CLA", "TER Check", $clad->getId(), "Unapproved: {$curr_ter}", $account->getId());
			success_redirect("Ad #{$clad_id} successfully marked as TER unapproved.", "/mng/clad_ter_check");
		} else {
			error_redirect("Error marking ad as TER unapproved for classified ad #{$clad_id} !", "/mng/clad_ter_check");
		}
	}

	if ($_REQUEST["submit"] == "No TER Id") {
		$clad->setTer(NULL);
		$clad->setTerCheckStamp(time());
		$ret = $clad->update();
		if ($ret) {
			audit::log("CLA", "TER Check", $clad->getId(), "No TER Id", $account->getId());
			success_redirect("Ad #{$clad_id} successfully marked as without TER Id.", "/mng/clad_ter_check");
		} else {
			error_redirect("Error resetting TER Id for classified ad #{$clad_id} !", "/mng/clad_ter_check");
		}
	}

	$ter = intval($_REQUEST["ter_id"]);
	if (!$ter)
		error_redirect("No TER Id specified for classified ad #{$clad_id} !", "/mng/clad_ter_check");

//	echo "<pre>".print_r($_REQUEST, true)."</pre><br />\n";
//	return true;

	$clad->setTer($ter);
	$clad->setTerCheckStamp(time());
	$ret = $clad->update();
	if ($ret) {
		audit::log("CLA", "TER Check", $clad->getId(), "TER Id = {$ter}", $account->getId());
		success_redirect("TER Id successfully set to {$ter} for ad #{$clad_id}", "/mng/clad_ter_check");
	} else {
		error_redirect("Failed to set TER Id for classified ad #{$clad_id} !", "/mng/clad_ter_check");
	}
}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'ter':
		$ret = ter();
		if (!$ret)
			return;
		break;
}

$condition = "l.loc_type <> 4 AND c.deleted IS NULL AND (c.done = 1 OR c.done = 0) AND c.total_loc < 100 AND c.ter_check_stamp IS NULL AND c.ter IS NOT NULL AND c.ter <> ''";
$where = getWhere();
if($where == "")
	$where = " WHERE {$condition} ";
else 
	$where .= " AND {$condition} ";

$order = " ORDER BY ter_check_priority DESC, ter DESC ";
//$order = getOrder();
$limit = getLimit();

//query db
$sql = "SELECT count(*) as total 
		FROM classifieds c
		INNER JOIN location_location l on c.loc_id = l.loc_id
		$where
	";

$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];


$sql = "SELECT c.*, ah.id as highlight_id
		FROM classifieds c 
		INNER JOIN location_location l on c.loc_id = l.loc_id
		LEFT JOIN admin_highlight ah on ah.module = 'clad_ter_check' AND ah.item_id = c.id
		$where
		$order 
		$limit";

$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No ads to check TER.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>TER Check</h2>\n";
displayFilterForm();
echo $pager."<br />";
echo getFilterFormFields();

echo "<table class=\"control\">";
echo "<thead><tr><th/><th>".getOrderLink("Id", "id")."</th><th>Name</th><th>Phone</th><th>TER</th><th/></tr></thead>\n";
echo "<tbody>";
while ($row = $db->r($res)) {
	$clad = clad::withRow($row);
	if (!$clad)
		die("Cant create clad object!");

	echo "<tr class=\"highlightable";
	if ($row["highlight_id"])
		echo " highlighted";
	echo "\" data-jay=\"{$row["highlight_id"]}\" data-highlighted=\"".(($row["highlight_id"]) ? 1 : 0)."\" data-item-id=\"{$clad->getId()}\" >";
	echo "<td/>";
	echo "<td>{$clad->getId()}</td>";
	echo "<td>{$clad->getFirstname()}</td>";
	echo "<td>".makeProperPhoneNumber($clad->getPhone())."</td>";
	echo "<td>".$clad->getTer()."</td>";
	echo "<td>";

	echo "<a href=\"https://www.theeroticreview.com/reviews/newreviewsList.asp?searchreview=1&SortBy=3&cs_config_country_field=countrySelect&cs_config_city_field=citySelect&cs_config_city2_field=city2Select&cs_config_country_default=countryDefault&cs_config_city_default=cityDefault&cs_config_city2_default=city2Default&cs_select_city_text=select+city&cs_select_country_text=selectCountry&cs_all_countries_text=All+Countries&cs_all_cities_text=All+Cities&searchFlDD=1&Phone=".$clad->getPhone()."\" target=\"ter_search\" />Search on TER</a>";

	echo " &middot; <a href=\"".$clad->getUrl()."\" target=\"as_view\">view ad on AS</a>";

	echo " &middot; <a href=\"/adbuild/step3?ad_id=".$clad->getId()."\" target=\"edit_view\">edit ad</a>";

	echo " <input type=\"text\" id=\"url_{$clad->getId()}\" data-clipboard-action=\"copy\" data-clipboard-target=\"#url_{$clad->getId()}\" class=\"copycat\" value=\"{$clad->getUrl()}\" size=\"70\" /> ";

	echo "<form style=\"display:inline-block;\" action=\"\" method=\"post\"><input type=\"hidden\" name=\"action\" value=\"ter\" /><input type=\"hidden\" name=\"action_id\" value=\"{$clad->getId()}\" />Set TER Id:<input type=\"text\" name=\"ter_id\" value=\"\" /><input type=\"submit\" name=\"submit\" value=\"Submit\" /> <input type=\"submit\" name=\"submit\" value=\"No TER Id\" /> <input type=\"submit\" name=\"submit\" value=\"Linked\" /> <input type=\"submit\" name=\"submit\" value=\"Unapproved\" /></form>";

	echo "</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;

?>
<script src="/js/clipboard.min.js"></script>
<script type="text/javascript">
function highlight_toggle(elem) {
	var item_id = $(elem).closest('tr').data('item-id');
	var highlighted = $(elem).closest('tr').data('highlighted');
	$.post(
		'/_ajax/highlight', 
		{'module': 'clad_ter_check', 'item_id': item_id, 'current': highlighted}, 
		function(data) {
			if (data.status != 'ok')
				return;
			if (highlighted == 0) {
				$(elem).closest('tr').data('highlighted', 1);
				$(elem).closest('tr').addClass('highlighted');
				$(elem).text('U');
			} else {
				$(elem).closest('tr').data('highlighted', 0);
				$(elem).closest('tr').removeClass('highlighted');
				$(elem).text('H');
			}
		},
		'json'
		);
}

$(document).ready(function() {
	var clipboard = new Clipboard('.copycat');
	$('.control tr.highlightable').each(function(ind,obj) {
		var first_td = $(this).find('td:first-child');
		var highlighted = $(this).data('highlighted');
		if (highlighted)
			first_td.append('<button type="button" class=\"highlight_btn\">U</button>');
		else
			first_td.append('<button type="button" class=\"highlight_btn\">H</button>');
	});
	$('.highlight_btn').click(function() {
		highlight_toggle(this);
	});
});

</script>
