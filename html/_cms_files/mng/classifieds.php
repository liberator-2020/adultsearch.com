<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate, $config_image_server;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("access_admin_classifieds"))
	die("Invalid access");

global $url_last_piece, $filters, $where_cols, $classifieds;

$classifieds = new classifieds();


//TODO - this should be got from classifieds class
$classified_types = array(
	"1" => "Escort",
	"2" => "TSTV",
	"6" => "Body Rubs",
	);

//configuration
$url_last_piece = "classifieds";
$where_cols = array(
	"cid"		 => array("title" => "ID", "where" => "c.id", "match" => "exact", "size" => "7"),
	"status"	 => array(
		"title" => "Status",
		"where" => "c.done",
		"match" => "exact",
		"type" => "select",
		"options" => array(
			"" => "-",
			"-2" => "To be removed",
			"-1" => "Not Posted Yet",
			"1" => "Live",
			"0" => "Expired",
			"2" => "Invisible",
			"3" => "Waiting")
		),
	"deleted"	 => array(
		"title" => "Deleted",
		"where" => array(
			"" => "",
			"yes" => " NOT c.deleted IS NULL ",
			"no" => " c.deleted IS NULL ",
			"all" => " 1=1 ",
			),
		"type" => "select",
		"options" => array(
			"" => "-",
			"yes" => "Yes",
			"no" => "No",
			"all" => "All",
			),
		),
	"account_id" => array("title" => "Acc.ID", "where" => "c.account_id", "match" => "exact", "size" => "7"),
	"type"	   => array("title" => "Type", "where" => "c.type", "match" => "exact", "type" => "select", "options" => array("" => "-") + $classified_types),
	"phone"	  => array("title" => "Phone", "where" => "c.phone", "match" => "preg_replace", "pattern" => "/[^0-9]/", "replacement" => "", "size" => "8"),
	"firstname"  => array("title" => "Firstname", "size" => "10"),
	"title"	  => array("title" => "Title", "where" => "c.title"),
	"content"	=> array("title" => "Content", "where" => "c.content"),
	"email"	  => array("title" => "Email", "where_condition" => " ( c.email like ? OR a.email like ? ) ", "where_params" => ["%{val}%", "%{val}%"]),
	"video_count"	=> array(
		"title" => "Video",
		"having" => array(
			"" => "",
			"NONE" => " video_count = 0 ",
			"1" => " video_count = 1 ",
			"2plus" => " video_count > 1 ",
			),
		"type" => "select",
		"options" => array(
			"" => "-",
			"NONE" => "None",
			"1" => "1 video",
			"2plus" => "2+ videos"
			),
		),

/*
	"num_loc"	=> array(
		"title" => "# locs",
		"having" => array(
			"" => "",
			"1" => " COUNT(cl.id) = 1 ",
			"2plus" => " COUNT(cl.id) > 1 ",
			"3plus" => " COUNT(cl.id) > 2 ",
			),
		"type" => "select",
		"options" => array(
			"" => "-",
			"1" => "1 loc",
			"2plus" => "2+ locs",
			"3plus" => "3+ locs"
			),
		),
	"content_link"	 => array(
		"title" => "Contains link",
		"where" => array(
			"" => "",
			"yes" => " c.content like '%http%' ",
			"no" => " c.content not like '%http%' ",
			),
		"type" => "select",
		"options" => array(
			"" => "-",
			"yes" => "Yes",
			"no" => "No",
			),
		),
	"have_phone"	 => array(
		"title" => "Have phone",
		"where" => array(
			"" => "",
			"yes" => " c.phone IS NOT NULL AND c.phone != '' ",
			"no" => " (c.phone = '' OR c.phone IS NULL) ",
			),
		"type" => "select",
		"options" => array(
			"" => "-",
			"yes" => "Yes",
			"no" => "No",
			),
		),
	"bp"	 => array(
		"title" => "BP",
		"where" => "c.bp",
		"type" => "select",
		"options" => array(
			"" => "-",
			"0" => "No",
			"1" => "Yes",
			),
		),
*/
	"sponsor"	 => array(
		"title" => "Sponsor",
		"where" => "c.sponsor",
		"type" => "select",
		"options" => array(
			"" => "-",
			"0" => "No",
			"1" => "Yes",
			),
		),
	"sponsor_mobile"	 => array(
		"title" => "Sponsor_mobile",
		"where" => "c.sponsor_mobile",
		"type" => "select",
		"options" => array(
			"" => "-",
			"0" => "No",
			"1" => "Yes",
			),
		),
/*
	"paid"	 => array(
		"title" => "Paid",
		"where" => "c.paid",
		"where" => array(
			"" => "",
			"0" => " c.paid = 0 ",
			"1" => " c.paid > 0 ",
			),
		"type" => "select",
		"options" => array(
			"" => "-",
			"0" => "Free Ad",
			"1" => "Paid Ad",
			),
		),
	"visa"	 => array(
		"title" => "Visa",
		"where" => "c.payment_visa",
		"type" => "select",
		"options" => array(
			"" => "-",
			"0" => "No",
			"1" => "Yes",
			),
		),
	"wpromo"	 => array(
		"title" => "Promocode",
		"where" => "c.promo_code",
		"where" => array(
			"" => "",
			"0" => " c.promo_code = '' ",
			"1" => " c.promo_code <> '' ",
			),
		"type" => "select",
		"options" => array(
			"" => "-",
			"0" => "No promocode",
			"1" => "Promocode",
			),
		),
	"avlto"	 => array(
		"title" => "Available to",
		"where" => "c.avl_men",
		"where" => array(
			"" => "",
			"men" => "avl_men = 1 and avl_women = 0 and avl_couple = 0",
			"women" => "avl_men = 0 and avl_women = 1 and avl_couple = 0",
			"couples" => "avl_men = 0 and avl_women = 0 and avl_couple = 1",
			"men_women" => "avl_men = 1 and avl_women = 1 and avl_couple = 0",
			"men_couples" => "avl_men = 1 and avl_women = 0 and avl_couple = 1",
			"women_couples" => "avl_men = 0 and avl_women = 1 and avl_couple = 1",
			"all" => "avl_men = 1 and avl_women = 1 and avl_couple = 1",
			),
		"type" => "select",
		"options" => array(
			"" => "-",
			"men" => "Men",
			"women" => "Women",
			"couples" => "Couples",
			"men_women" => "Men & Women",
			"men_couples" => "Men & Couples",
			"women_couples" => "Women & Couples",
			"all" => "All",
			),
		),
	"fetish"	 => array(
		"title" => "Fetish",
		"where" => "c.fetish_dominant",
		"where" => array(
			"" => "",
			"dom" => "fetish_dominant = 1 and fetish_submissive = 0 and fetish_swith = 0",
			"sub" => "fetish_dominant = 0 and fetish_submissive = 1 and fetish_swith = 0",
			"swi" => "fetish_dominant = 0 and fetish_submissive = 0 and fetish_swith = 1",
			"dom_sub" => "fetish_dominant = 1 and fetish_submissive = 1 and fetish_swith = 0",
			"dom_swi" => "fetish_dominant = 1 and fetish_submissive = 0 and fetish_swith = 1",
			"sub_swi" => "fetish_dominant = 0 and fetish_submissive = 1 and fetish_swith = 1",
			"all" => "fetish_dominant = 1 and fetish_submissive = 1 and fetish_swith = 1",
			),
		"type" => "select",
		"options" => array(
			"" => "-",
			"dom" => "Dominant",
			"sub" => "Submissive",
			"swi" => "Switch",
			"dom_sub" => "Dominant & Submissive",
			"dom_swi" => "Dominant & Switch",
			"sub_swi" => "Submissive & Switch",
			"all" => "All",
			),
		),
	"wter"	 => array(
		"title" => "TER",
		"where" => "c.ter",
		"where" => array(
			"" => "",
			"0" => " c.ter = '' ",
			"1" => " c.ter <> '' ",
			),
		"type" => "select",
		"options" => array(
			"" => "-",
			"0" => "No TER Id",
			"1" => "With TER Id",
			),
		),
	"usa"	=> array(
		"title" => "USA",
		"where" => "l.country_id",
		"where" => array(
			"" => "",
			"1" => " l.country_id = 16046 ",
			),
		"type" => "select",
		"options" => array(
			"" => "-",
			"1" => "USA",
			),
		),
*/
);

function manage() {
	global $db, $classifieds;
	$system = new system();

	$smarty = GetSmartyInstance();

	$clad_id = $action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid classified id !");

	$clad = clad::findOnebyId($action_id);
	if ($clad == false)
		return actionError("Cant find classified for id={$action_id} !");

	$classifieds->timezone($action_id, $timezone_time, $timezone_name);

	$acc = $clad->getAccount();

	if ($_REQUEST["submit"] == "clad_manage") {

		//handle skip button
		if (isset($_REQUEST["next"])) {
			//fetch next waiting ad
			//TODO unify with code below
			$res = $db->q("SELECT c.id FROM classifieds c WHERE c.id < ? AND c.done = 3 AND c.deleted IS NULL ORDER BY c.id DESC LIMIT 1", array($action_id));
			if ($db->numrows($res) == 1) {
				$row = $db->r($res);
				$system->go("/mng/classifieds?action=manage&action_id={$row["id"]}");	
			} else {
				return actionWarning("There is no other waiting ad.");
			}
		}

		//handle edit button
		if (isset($_REQUEST["edit"])) {
			$system->go("/adbuild/step3?ad_id={$action_id}&ref=".urlencode("/mng/classifieds?action=manage&action_id={$action_id}"));	
		}

		//first handle delete&ban actions
		if (isset($_REQUEST["delete"])) {
			$ret = $clad->remove();
			if ($ret)
				return actionSuccess("Classified ad #{$id} has been deleted successfully.", "/mng/classifieds?status=3");
			else
				return actionError("Error while deleting classified ad #{$id} !", "/mng/classifieds?cid={$id}");
		} else if (isset($_REQUEST["delete_ban"])) {
			$ret = $clad->remove();
			if (!$ret)
				return actionError("Error while deleting classified ad #{$id}. Account {$acc->getEmail()} has not been banned!", "/mng/classifieds?cid={$id}");
			if ($acc->isBanned()) {
				return actionSuccess("Classified ad #{$id} has been deleted successfully. Ad's owner acount {$acc->getEmail()} has already been banned before.", "/mng/classifieds?cid={$id}");
			} else {
				if (($acc->getAccountLevel() == 3) || (($acc->getAccountLevel() == 2) && !$account->isrealadmin()))
					return actionWarning("Classified ad #{$id} has been deleted successfully, but you can't ban administrator account (#{$account_id} - {$acc->getEmail()}). Please contact administrator.");
				$ret = $acc->ban();
				if ($ret) {
					return actionSuccess("Classified ad #{$id} has been deleted successfully. Account {$acc->getEmail()} has been successfully banned.", "/mng/classifieds?cid={$id}");
				} else {
					return actionWarning("Classified ad #{$id} has been deleted successfully, but there has been error banning account {$acc->getEmail()} ! This should not happen, please contact administrator.", "/mng/classifieds?cid={$id}");
				}
			}
		} else if (isset($_REQUEST["delete_all_ban"])) {
			if ($acc) {
				$res = $db->q("SELECT c.* FROM classifieds c WHERE c.account_id = ?", array($acc->getId()));
				$succ = 0;
				$err = 0;
				while ($row = $db->r($res)) {
					//fetch clad
					$clad = clad::withRow($row);
					if (!$clad)
						continue;
					if ($clad->getDeleted())
						continue;
					//remove ad
					if ($clad->remove())
						$succ++;
					else
						$err++;
				}
				if (($acc->getAccountLevel() == 3) || (($acc->getAccountLevel() == 2) && !$account->isrealadmin())) {
					return actionWarning("{$succ} classified ads have been deleted successfully. You can't ban administrator account though (#{$account_id} - {$acc->getEmail()}). Please contact administrator.");
				}
				$ret = $acc->ban();
				if ($ret && ($err == 0)) {
					return actionSuccess("{$succ} classified ads have been deleted successfully. Account {$acc->getEmail()} has been successfully banned.", "/mng/classifieds?account_id={$acc->getId()}");
				} else {
					if ($ret)
						$acc_msg = "Account {$acc->getEmail()} has been successfully banned.";
					else
						$acc_msg = "Error banning account {$acc->getEmail()}!";
					return actionError("Error: {$succ} classified ads have been deleted, {$err} classifieds failed to be deleted. {$acc_msg}. This should not happen, please contact administrator.", "/mng/classifieds?cid={$id}");
				}
			} else {
				flash::add(flash::MSG_ERROR, "Ad does not have owner, it was probably created via API");
			}
		}

		//submission of changes
		$error = "";

		$expires_date = $_REQUEST["expires_date"];
		$expires_hour = intval($_REQUEST["expires_hour"]);
		$expires_minute = intval($_REQUEST["expires_minute"]);
		if ($expires_hour > 23 || $expires_hour < 0) {
			$error .= "Expires hour is not valid!<br />";
		}
		if ($expires_minute > 59 || $expires_minute < 0) {
			$error .= "Expires minute is not valid!<br />";
		}
		if ($expires_hour < 10)
			$expires_hour = "0{$expires_hour}";
		if ($expires_minute < 10)
			$expires_minute = "0{$expires_minute}";
		$expires = NULL; $time_next = NULL;
		$auto_renew_fr = intval($_REQUEST["auto_renew_fr"]);
		$auto_renew_time = intval($_REQUEST["auto_renew_time"]);
		if ($expires_date) {
			$expires = "{$expires_date} {$expires_hour}:{$expires_minute}:00";

			$expires_stamp = strtotime($expires);
			$time_next = $expires_stamp;
			if (intval($_REQUEST["auto_renew"]) > 0) {
				//lets not rely on php.ini correctly set
				date_default_timezone_set('America/Los_Angeles');
				$tomorrow_stamp = mktime(0, 0, 0, date("m"), date("d")+1, date("Y"));	   //timestamp of tomorrow start of day in PST
				//if auto_renew_fr is 1 (= renew every day), first renewal will be tomorrow at specified time
				//debug_log("tomorrow_stamp={$tomorrow_stamp}, timezone_time={$timezone_time}, auto_renew_time={$auto_renew_time}, auto_renew_fr={$auto_renew_fr}");
				$time_next = $tomorrow_stamp + (60*$timezone_time) + (60*$auto_renew_time) + (86400 * ($auto_renew_fr - 1));
			}
			//debug_log("time_next={$time_next}");
		}

		$account_id = intval($_REQUEST["account_id"]);
		$account_move_cc = false;
		if ($account_id != $clad->getAccountId() && intval($_REQUEST["account_move_cc"]))
			$account_move_cc = true;


		if ($error) {
			echo "<span style=\"color: red; font-weight: bold;\">Error:<br />{$error}</span>";
		} else {
			//everything ok, lets update the ad
			$clad->setType(intval($_REQUEST["type"]));
			if ($clad->getDone() < 1 && intval($_REQUEST["done"]) == 1) {
				//when puting ad up, update posted date (move to top)
				$clad->setDate(date("Y-m-d H:i:s"));
			}
			$clad->setDone(intval($_REQUEST["done"]));
			$clad->setExpires($expires);
			$clad->setAccountId($account_id);
			$clad->setSponsor(intval($_REQUEST["sponsor"]));
			$clad->setSponsorMobile(intval($_REQUEST["sponsor_mobile"]));
			$clad->setSponsorPosition(intval($_REQUEST["sponsor_position"]));
			$clad->setAutoRepost(intval($_REQUEST["auto_repost"]));
			$clad->setAutoRenewFr($auto_renew_fr);
			$clad->setAutoRenewTime($auto_renew_time);
			$clad->setTimeNext($time_next);
			$clad->setAllowlinks(intval($_REQUEST["allowlinks"]));
			$clad->setEccieReviewsLink($_REQUEST["eccie_revies_link"]);
			$clad->setDirectLink($_REQUEST["direct_link"]);
			$ret = $clad->update(true);
			if ($ret) {
				if ($account_move_cc) {
					$db->q("UPDATE account_purchase p
							INNER JOIN account_cc c using on account_cc.cc_id = p.cc_id
							SET p.account_id = ?, c.account_id = ? 
							WHERE p.item_id = ?",
							array($account_id, $account_id, $action_id)
							);
				}

				debug_log(print_r($_REQUEST, true));	
				if (isset($_REQUEST["save_next"])) {
					//fetch next waiting ad
					$res = $db->q("SELECT c.id FROM classifieds c WHERE c.id < ? AND c.done = 3 AND c.deleted IS NULL ORDER BY c.id DESC LIMIT 1", array($action_id));
					if ($db->numrows($res) == 1) {
						$row = $db->r($res);
						flash::add(flash::MSG_SUCCESS, "You have successfully updated classified ad id #{$action_id}.");
						$system->go("/mng/classifieds?action=manage&action_id={$row["id"]}");	
					}
				}
				return actionSuccess("You have successfully updated classified ad id #{$action_id}.");
			} else {
				return actionError("Error while updating classified ad id #{$action_id}.");
			}
		}
	}

	//going to display manage page
	$smarty->assign("id", $action_id);
	$smarty->assign("title", htmlspecialchars($clad->getTitle()));

	$thumb_url = $multiple_thumbs_urls = null;
	if ($clad->getMultipleThumbsUrls()) {
		$multiple_thumbs_urls = $clad->getMultipleThumbsUrls();
		array_walk($multiple_thumbs_urls, htmlspecialchars);
	} else {
		$thumb_url = htmlspecialchars($clad->getThumbUrl());
	}
	$smarty->assign("thumb_url", $thumb_url);
	$smarty->assign("multiple_thumbs_urls", $multiple_thumbs_urls);

	$smarty->assign("images", $clad->getImages());
	$smarty->assign("videos", $clad->getVideos());
	$type_values = array(); $type_names = array();
	foreach($classifieds->adbuild_type_array as $key => $val) {
		$type_values[] = $key;
		$type_names[] = $val;
	}
	$smarty->assign("type_values", $type_values);
	$smarty->assign("type_names", $type_names);
	$smarty->assign("type_selected", $clad->getType());
	$smarty->assign("done_values", array("-2", "-1", "0", "1", "2", "3"));
	$smarty->assign("done_names", array("To be removed", "Not posted yet", "Expired", "Live", "Invisible", "Waiting"));
	$smarty->assign("done_selected", $clad->getDone());
	$smarty->assign("deleted", $clad->getDeleted());

	$datetime = new DateTime($clad->getExpires());
	$expires_date = $datetime->format('Y-m-d');
	$expires_hour = $datetime->format('H');
	$expires_minute = $datetime->format('i');
	$smarty->assign("expires_date", $expires_date);
	$smarty->assign("expires_hour", $expires_hour);
	$smarty->assign("expires_minute", $expires_minute);

	$expires_in_one_week = new DateTime();
	$expires_in_one_week->setTimestamp(time() + 7 * 86400);

	$smarty->assign("expires_in_one_week", $expires_in_one_week->format('Y-m-d'));

	$smarty->assign("account_id", $clad->getAccountId());

	$cc_ids = array("0"); $cc_labels = array("-");
	$res = $db->q("SELECT cc_id, cc FROM account_cc WHERE account_id = ? AND deleted = 0", array($clad->getAccountId()));
	while($row = $db->r($res)) {
		$cc_ids[] = $row["cc_id"];
		$cc_labels[] = "x".substr($row["cc"], -4);
	}
	$smarty->assign("cc_ids", $cc_ids);	
	$smarty->assign("cc_labels", $cc_labels);	
	$smarty->assign("cc_id", $clad->getCcId());

	$smarty->assign("content", classifieds::sanitizeContentForOutput($clad->getContent(), $clad->getAllowlinks(), $clad->getAccountId()));

	$city_thumbnails = array();
	$res = $db->q("SELECT cs.id, cs.done, cs.expire_stamp, l.* 
					FROM classifieds_sponsor cs
					INNER JOIN location_location l on l.loc_id = cs.loc_id 
					WHERE cs.post_id = ?", 
					array($action_id));
	while ($row = $db->r($res)) {
		$loc = location::withRow($row);
		$city_thumbnails[] = array(
			"id" => $row["id"],
			"loc_id" => $loc->getId(),
			"location" => $loc->getLabel(),
			"done" => $row["done"],
			"expire_stamp" => $row["expire_stamp"],
			);
	}
	$smarty->assign("city_thumbnails", $city_thumbnails);

	$side_sponsors = array();
	$res = $db->q("SELECT cs.id, cs.done, cs.expire_stamp, l.* 
					FROM classifieds_side cs
					INNER JOIN location_location l on l.loc_id = cs.loc_id 
					WHERE cs.post_id = ?", 
					array($action_id));
	while ($row = $db->r($res)) {
		$loc = location::withRow($row);
		$side_sponsors[] = array(
			"id" => $row["id"],
			"loc_id" => $loc->getId(),
			"location" => $loc->getLabel(),
			"done" => $row["done"],
			"expire_stamp" => $row["expire_stamp"],
			);
	}
	$smarty->assign("side_sponsors", $side_sponsors);

	$stickies = array();
	$res = $db->q("SELECT cs.id, cs.done, cs.expire_stamp, l.* 
					FROM classified_sticky cs
					INNER JOIN location_location l on l.loc_id = cs.loc_id 
					WHERE cs.classified_id = ?",
		array($action_id));
	while ($row = $db->r($res)) {
		$loc = location::withRow($row);
		$stickies[] = array(
			"id" => $row["id"],
			"loc_id" => $loc->getId(),
			"location" => $loc->getLabel(),
			"done" => $row["done"],
			"expire_stamp" => $row["expire_stamp"],
		);
	}
	$smarty->assign("stickies", $stickies);

	$smarty->assign("sponsor", $clad->getSponsor());
	$smarty->assign("sponsor_mobile", $clad->getSponsorMobile());
	$smarty->assign("sponsor_position", $clad->getSponsorPosition());
	$smarty->assign("side_sponsors", $side_sponsors);
	$smarty->assign("auto_repost", $clad->getAutoRepost());
	$smarty->assign("repost", ($acc) ? $acc->getRepost() : 0);
	$smarty->assign("arf_values", array("1", "2", "3", "4", "5", "6", "7"));
	$smarty->assign("arf_names", array("Everyday", "2 days", "3 days", "4 days", "5 days", "6 days", "7 days"));
	$smarty->assign("auto_renew_fr", $clad->getAutoRenewFr());
	$time_array = get_time_array();
	$smarty->assign("art_values", array_keys($time_array));
	$smarty->assign("art_names", array_values($time_array));
	$smarty->assign("auto_renew_time", $clad->getAutoRenewTime());
	$smarty->assign("timezone", $timezone_name);

	//next repost
	$next_repost = "-";
	if ($clad->getAutoRepost() > 0) {
		$curr = new \DateTime();
		$nr = new \DateTime();
		$nr->setTimestamp($clad->getTimeNext());	//in db we have UTC timestamp
		$nr->setTimezone(new \DateTimeZone($timezone_name));			//but show in local timezone of the ad
		$next_repost = $nr->format("m/d/Y H:i A, T")." - in ".formatDateDiff($curr, $nr);
	}
	$smarty->assign("next_repost", $next_repost);

	//last repost
	$last_repost = "-";
	$res = $db->q("SELECT day, stamp FROM repost_log WHERE classified_id = ? ORDER BY stamp DESC LIMIT 1", [$clad->getId()]);
	if ($db->numrows($res) == 1) {
		$row = $db->r($res);
		if ($row["stamp"]) {
			$lr = new DateTime();
			$lr->setTimestamp($row["stamp"]);
			$lr->setTimezone(new \DateTimeZone($timezone_name));
			$last_repost = $lr->format("m/d/Y H:i A, T")." - ".formatDateDiff($lr, $curr)." ago";
		} else if ($row["day"]) {
			$last_repost = $row["day"];
		}
	}
	$smarty->assign("last_repost", $last_repost);

	$smarty->assign("direct_link", $clad->getDirectLink());
	$smarty->assign("allowlinks", $clad->getAllowlinks());
	$smarty->assign("eccie_reviews_link", $clad->getEccieReviewsLink());
	$smarty->assign("hit", $clad->getHit());

	$locations = $clad->getLocLabels();
	$location_1_id = NULL;
	$locations_text = "";
	$i = 0;
	foreach ($locations as $loc_id => $location) {
		$locations_text .= (empty($locations_text)) ? "" : ", ";
		$locations_text .= $location;
		$i++;
		if ($i > 10) {
			$locations_text .= ", ...";
			break;
		}
		$location_1_id = $loc_id;
	}
	$smarty->assign("location_count", count($locations));
	$smarty->assign("locations_text", $locations_text);
	$smarty->assign("location_1_id", $location_1_id);

	$other_ads_important = false;
	if ($clad->getAccountId()) {
		$other_ads = array();
		$res = $db->q("SELECT c.*, ai.id as important, count(cl.id) as loc_count
						FROM classifieds c
						LEFT JOIN admin_important ai ON ai.item_id = c.id and ai.type = 'CLA'
						LEFT JOIN classifieds_loc cl ON c.id = cl.post_id and cl.done = c.done
						WHERE c.account_id = ? AND c.id <> ?
						GROUP BY c.id, ai.id, c.done
						ORDER BY c.done DESC",
					array($clad->getAccountId(), $action_id));
		while($row = $db->r($res)) {
			$ad = clad::withRow($row);
			$other_ads[] = get_ad_info($ad, $row["loc_count"]);
			if ($row["important"])
				$other_ads_important = true;
		}
	}
	$smarty->assign("other_ads", $other_ads);
	$smarty->assign("other_ads_important", $other_ads_important);

	//
	$res = $db->q("SELECT id FROM admin_important ai WHERE ai.item_id = ? and ai.type = 'CLA' LIMIT 1", array($action_id));
	$smarty->assign("important", ($db->numrows($res) == 1));

	$smarty->assign("whitelisted", ($acc && $acc->isWhitelisted()));
	$smarty->assign("email", ($acc) ? $acc->getEmail() : "");

	$smarty->assign("url_refresh", "/mng/classifieds?action=manage&action_id={$action_id}");
	$smarty->assign("url_view", $clad->getUrl());
	$smarty->assign("url_list", "/mng/classifieds?cid={$action_id}");

	//get payment and last event
	$res = $db->q("
		SELECT DISTINCT p.* 
		FROM payment_item pi 
		INNER JOIN payment p on p.id = pi.payment_id
		WHERE pi.type = 'classified' and pi.classified_id = ?
		ORDER BY p.id DESC",
		[$clad_id]
		);
	if ($db->numrows($res) == 0) {
		$res = $db->q("SELECT p.*
			FROM payment p
			WHERE p.p3 = ?
			ORDER BY p.id DESC",
			[$clad_id]
			);
	}
	if ($db->numrows($res) == 0) {
		$smarty->assign("payment", null);
	} else {
		$row = $db->r($res);	
		$smarty->assign("payment", $row);
		$payment_id = $row["id"];
		$res = $db->q("SELECT pe.* FROM payment_event pe WHERE pe.payment_id = ? ORDER BY id DESC LIMIT 1", [$payment_id]);
		if ($db->numrows($res) == 1) {
			$row = $db->r($res);
			$smarty->assign("last_payment_event", $row);
		}
	}

	echo $smarty->fetch(_CMS_ABS_PATH."/templates/mng/classifieds_manage.tpl");
	return false;
}

function undelete() {
	global $config_image_server;
/*
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid classified id !");

	$clad = clad::findOnebyId($action_id);
	if ($clad == false)
		return actionError("Cant find classified for id={$action_id} !");

	if (!$clad->getDeleted())
		return actionError("Classified ad #{$action_id} is not deleted !");

	if (isset($_REQUEST["submit"])) {
		$ret = $clad->undelete();
		if ($ret)
			return actionSuccess("You have successfully undeleted classified ad id #{$action_id}.");
		else
			return actionError("Error while undeleting classified ad id #{$action_id}.");
	}

	echo "<h2>Undelete classified ad #{$clad->getId()}</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Classified ad ID</th><td>{$clad->getId()}</td></tr>\n";
	echo "<tr><th>Done</th><td>{$clad->getDone()}</td></tr>\n";
	echo "<tr><th>Expires</th><td>{$clad->getExpires()}</td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
	echo "</table>";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$action_id}\" />";
	echo getFilterFormFields();
	echo "</form>";
	return true;
*/
	$live = ($_REQUEST["live"] == 1) ? true : false;

	$ids = getActionIds();
	$cnt = count($ids);
	if ($cnt == 0)
		return actionWarning("No classifieds selected !");

	$clads = clad::findAllByIds($ids);
	if (empty($clads))
		return actionError("No classifieds found for selected ids !");
	$total = count($clads);

	if (!$live) {
		echo "<h2>Are you sure you want to undelete following {$total} ad(s) ?</h2>";
		echo "<table><tr><th>Id</th><th>Thumb</th><th>Type</th><th>Title</th><th>Account Id</th><th>Account Email</th></tr>";
		foreach ($clads as $clad) {
			echo "<tr><td>{$clad->getId()}</td>";
			printf( '<td><img src="%s/classifieds/%s">"/></td>', $config_image_server, $clad->getThumb() );
			echo "<td>{$clad->getType()}</td>";
			echo "<td>".htmlspecialchars($clad->getTitle())."</td>";
			echo "<td>{$clad->getAccountId()}</td>";
			$acc = $clad->getAccount();
			if ($acc)
				$email = $acc->getEmail();
			else
				$email = "<span style=\"color: red;\">No account found for account_id {$clad->getAccountId()} !</span>";
			echo "<td>{$email}</td></tr>";
		}
		echo "</table>";

		echo getConfirmActionForm("undelete", $ids);
		return false;
	}

	//undelete
	$und = 0;
	foreach($clads as $clad) {
		if (!$clad->getDeleted())
			continue;
		if ($clad->undelete())
			$und++;
	}

	if ($total == 1) {
		if ($und == 1)
			return actionSuccess("Classified ad #{$clad->getId()} was successfully undeleted.");
		else
			return actionError("Error while undeleting classified ad #{$clad->getId()}.");
	} else {
		if ($und == $total)
			return actionSuccess("All <strong>{$und}</strong> classified ad(s) successfully undeleted.");
		else if ($mov == 0)
			return actionError("Error: no classified ad(s) (out of selected {$total}) was undeleted.");
		else
			return actionError("Warning: Only <strong>{$und}</strong> from {$total} classified ad(s) have been successfully undeleted.");
	}
}

function recreate_thumb() {
	global $db, $classifieds, $config_image_server;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid classified id !");

	$ret = $classifieds->createThumbnail($action_id);

	if ($ret) {
		echo "Creating thumbnail successful.<br />";
		$res = $db->q("SELECT thumb FROM classifieds WHERE id = ?", array($action_id));
		$row = $db->r($res);
		$thumb_url = $config_image_server."/classifieds/{$row["thumb"]}";
		echo "<img src=\"{$thumb_url}\" /><br />";
	} else {
		echo "<span style=\"color: red;\">Error: could not create thumbnail !</span><br />\n";
	}
	return true;
}

function recreate_multiple_thumbs() {
	global $db, $classifieds, $config_image_server;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid classified id !");

	$ret = $classifieds->createMultipleThumbnails($action_id);

	if ($ret) {
		echo "Creating multiple thumbnails successful.<br />";
		$res = $db->q("SELECT multiple_thumbs FROM classifieds WHERE id = ?", array($action_id));
		$row = $db->r($res);
		$arr = explode(",", $row["multiple_thumbs"]);
		foreach ($arr as $thumb) {
			$thumb_url = $config_image_server."/classifieds/{$thumb}";
			echo "<img src=\"{$thumb_url}\" /><br />";
		}
	} else {
		echo "<span style=\"color: red;\">Error: could not create multiple thumbnails !</span><br />\n";
	}
	return true;
}

function ebiz_create() {
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid classified id !");

	echo "<a href=\"#\" onclick=\"ebizCreateWebsite({$action_id}); return false;\">Create my web presentation at Escorts.biz</a><br /><div id=\"ebiz_response\"></div>";
	echo "<script type=\"text/javascript\">
			function ebizCreateWebsite(ad_id) {
				_ajax('GET', '/ajax/escortsbiz/create', 'ad_id='+ad_id, 'ebiz_response');	
			}
			</script>";

}

function prevent_delete() {
	global $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid classified id !");

	$res = $db->q("INSERT INTO classifieds_refer (id, referer, stamp) VALUES (?, ?, ?)", [$action_id, "manual", time()]);
	$aff = $db->affected();

	if ($aff == 1)
		return actionSuccess("Successfully added reference for ad #{$action_id} into db table classifieds_refer.");
	else
		return actionError("Error while adding manual reference for ad #{$action_id} into table classifieds_refer !");
}

function resend_receipt() {
	global $db, $account;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid classified id !");

	$clad = clad::findOneById($action_id);
	if (!$clad)
		return actionError("Cant find classified ad id#{$action_id} id !");

	if ($_REQUEST["submit"] == "Send") {
		$email = $_REQUEST["u_email"];
		if ($email == "other")
			$email = $_REQUEST["u_other_email"];

		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			return actionError("Email '{$email}' does not have correct format!");
		}

		$ret = $clad->emailReceipt($email);

		if ($ret)
			return actionSuccess("Successfully sent email receipt for ad #id {$action_id} to email '{$email}'.");
		else
			return actionError("Error sending email receipt for ad #id {$action_id} to email '{$email}' !");
	}

	echo "<h2>Re-send ad receipt for ad#{$action_id} to:</h2>";
	echo "<form action=\"\" method=\"post\">\n";
	echo "<input type=\"radio\" name=\"u_email\" value=\"{$account->getEmail()}\" selected=\"selected\" />Me ({$account->getEmail()})<br />\n";
	echo "<input type=\"radio\" name=\"u_email\" value=\"{$clad->getAccount()->getEmail()}\" />Classified ad owner email ({$clad->getAccount()->getEmail()})<br />\n";
	echo "<input type=\"radio\" name=\"u_email\" value=\"other\" />Other email:<br />\n";
	echo "<input type=\"text\" name=\"u_other_email\" value=\"\" /><br /><br />\n";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$action_id}\" />\n";
	echo "<input type=\"submit\" name=\"submit\" value=\"Send\" />\n";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}

function send_admin_notify_to_me() {
	global $db, $account;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid classified id !");

	$clad = clad::findOneById($action_id);
	if (!$clad)
		return actionError("Cant find classified ad id#{$action_id} id !");

	$email = ADMIN_EMAIL;
	$email = ADMIN_EMAIL;
	//$email = NULL;

	$error = "";
	$ret = $clad->emailAdminNotify($email, false, $error);

	if ($ret)
		return actionSuccess("Successfully sent email admin notify for ad #id {$action_id} to email '{$email}'.");
	else
		return actionError("Error sending email admin notify for ad #id {$action_id} to email '{$email}': '{$error}' !");
}


function move_to_phone_and_web() {
	global $config_image_server;

	$live = ($_REQUEST["live"] == 1) ? true : false;

	$ids = getActionIds();
	$cnt = count($ids);
	if ($cnt == 0)
		return actionWarning("No classifieds selected !");

	$clads = clad::findAllByIds($ids);
	if (empty($clads))
		return actionError("No classifieds found for selected ids !");
	$total = count($clads);

	if (!$live) {
		echo "<h2>Are you sure you want to move these {$total} ads to category Phone &amp; Web ?</h2>";
		echo "<table><tr><th>Id</th><th>Thumb</th><th>Type</th><th>Title</th><th>Account Id</th><th>Account Email</th></tr>";
		foreach ($clads as $clad) {
			echo "<tr><td>{$clad->getId()}</td>";
			echo "<td><img src=\"{$config_image_server}/classifieds/{$clad->getThumb()}\" /></td>";
			echo "<td>{$clad->getType()}</td>";
			echo "<td>".htmlspecialchars($clad->getTitle())."</td>";
			echo "<td>{$clad->getAccountId()}</td>";
			$acc = $clad->getAccount();
			if ($acc)
				$email = $acc->getEmail();
			else
				$email = "<span style=\"color: red;\">No account found for account_id {$clad->getAccountId()} !</span>";
			echo "<td>{$email}</td></tr>";
		}
		echo "</table>";

		echo getConfirmActionForm("move_to_phone_and_web", $ids);
		return false;
	}

	//move to phone & web
	$mov = 0;
	foreach($clads as $clad) {
		if ($clad->changeType(16))
			$mov++;
	}

	if ($mov == $total)
		return actionSuccess("All <strong>{$mov}</strong> classified ad(s) successfully moved to category Phone &amp; Web.");
	else if ($mov == 0)
		return actionError("No classified ad(s) (out of selected {$total}) have been moved to category Phone &amp; Web.");
	else
		return actionError("Warning: Only <strong>{$mov}</strong> from {$total} classified ad(s) have been successfully moved to category Phone &amp; Web.");
}

function makeLive() {
	global $db, $classifieds, $account, $config_image_server;

	$live = ($_REQUEST["live"] == 1) ? true : false;

	$ids = getActionIds();
	$cnt = count($ids);

	if ($cnt == 0) {
		echo "No classifieds selected!<br />";
		return true;
	}

	//echo "Ids: <pre>".print_r($ids, true)."<br />";

	if (!$live) {
		echo "Are you sure you want to make live these {$cnt} ads ?<br />";
	
		$sql = "SELECT c.id, c.thumb, c.type, c.title, a.account_id, a.username, a.email FROM classifieds c
				LEFT JOIN account a on c.account_id = a.account_id
				WHERE c.id IN (".implode(",",$ids).") AND c.done > -1";
		$res = $db->q($sql);
		if (!$db->numrows($res)) {
			echo "ERROR!"; die;
		}
		$cnt = $db->numrows($res);

		echo "<table><tr><th>Id</th><th>Thumb</th><th>Type</th><th>Title</th><th>Account Id</th><th>Account Email</th></tr>";
		while ($row = $db->r($res)) {
			echo "<tr><td>{$row["id"]}</td><td><img src=\"{$config_image_server}/classifieds/{$row["thumb"]}\" /></td><td>{$row["type"]}</td><td>".htmlspecialchars($row["title"])."</td><td>{$row["account_id"]}</td><td>{$row["email"]}</td></tr>";
		}
		echo "</table>";

		$custom_form_html = "";
		if ($account->isAdmin()) {
			$custom_form_html .= "<hr />";
			$custom_form_html .= "<input type=\"checkbox\" name=\"u_expires_in_month\" value=\"1\" />Make sure ad expires in at least 1 month<br />\n";
			$custom_form_html .= "<input type=\"checkbox\" name=\"u_daily_repost\" value=\"1\" />Add daily repost<br />\n";
			$custom_form_html .= "<hr />";
		}

		echo getConfirmActionForm("makeLive", $ids, $custom_form_html);
		return false;
	}

	$expires_in_month = (boolean)($_REQUEST["u_expires_in_month"]);
	$daily_repost = (boolean)($_REQUEST["u_daily_repost"]);
	$month_after_stamp = time() + 86400*30;
	$month_after_datetime = date("Y-m-d H:i:s", $month_after_stamp);
	$tomorrow_stamp = time() + 86400;

	$sql = "SELECT * 
			FROM classifieds c
			WHERE c.id IN (".implode(",",$ids).")";
	$res = $db->q($sql);
	if (!$db->numrows($res)) {
		echo "ERROR!"; die;
	}
	$total = $db->numrows($res);
	$ver = 0;
	while ($row = $db->r($res)) {
		//fetch clad
		$clad = clad::withRow($row);
		if (!$clad)
			continue;
		if (!$account->isAdmin() && $clad->getDone() != 3) {
			echo "Cl.ad #{$clad->getId()} is not in waiting state !<br />\n";
			continue;
		}
		//makeLive
		if ($clad->makeLive())
			$ver++;

		if ($account->isAdmin()) {
			$set = "";
			$params = [];
			if ($expires_in_month) {
				$expire_stamp = strtotime($clad->getExpires());
				if ($expire_stamp < $month_after_stamp) {
					$set .= (empty($set)) ? "" : ", ";
					$set .= " expires = ? ";
					$params[] = $month_after_datetime;
				}
			}
			if ($daily_repost) {
				$set .= (empty($set)) ? "" : ", ";
				$set .= " auto_renew_fr = 1, auto_renew = 30, time_next = ? ";
				$params[] = $tomorrow_stamp;
			}
			if ($set && !empty($params)) {
				$params[] = $clad->getId();
				$db->q("UPDATE classifieds SET {$set} WHERE id = ? LIMIT 1", $params);
			}
		}
	}

	if ($ver == $total)
		return actionSuccess("All <strong>{$ver}</strong> classified ad(s) successfully made live.");
	else
		return actionError("Warning: Only <strong>{$ver}</strong> from {$total} classified ad(s) have been successfully made live.");
}

//called from classified look page
function delete_print_ad($clad, $loc_count) {
	echo "<img src=\"".htmlspecialchars($clad->getThumbUrl())."\" style=\"float: left; margin-right: 5px; margin-bottom: 28px;\" />";
	echo "Cl.id#: <a href=\"/mng/classifieds?cid={$clad->getId()}\">{$clad->getId()}</a><br />";
	echo "Title: <strong>".htmlspecialchars($clad->getTitle())."</strong><br />";
	echo "Type: ".classifieds::getcatnamebytype($clad->getType())."<br />";
	$status_arr = array(
		"-2" => "To be removed",
		"-1" => "Not posted yet",
		"0" => "<span style=\"color: red;\">Expired</span>",
		"1" => "<span style=\"color: green; font-weight: bold;\">Live</span>",
		"2" => "Invisible",
		"3" => "Waiting"
		);
	$status = $status_arr[$clad->getDone()];
	if ($clad->getDeleted())
		$status = "<span style=\"color: red;\">DELETED</span>";
	echo "Posted in: <strong>{$loc_count}</strong> location(s)<br />";
	echo "Status: {$status}<br />";
	if ($clad->getDone() > -1)
		echo "Posted: ".$clad->getDate()."<br />Expires: ".$clad->getExpires()."<br />\n";
}

function get_ad_info($clad, $loc_count) {
	$html = "";
	$html .= "<img src=\"".htmlspecialchars($clad->getThumbUrl())."\" style=\"float: left; margin-right: 5px; margin-bottom: 28px;\" />";
	$html .= "Cl.id#: <a href=\"/mng/classifieds?cid={$clad->getId()}\">{$clad->getId()}</a><br />";
	$html .= "Title: <strong>".htmlspecialchars($clad->getTitle())."</strong><br />";
	$html .= "Type: ".classifieds::getcatnamebytype($clad->getType())."<br />";
	$status_arr = array(
		"-2" => "To be removed",
		"-1" => "Not posted yet",
		"0" => "<span style=\"color: red;\">Expired</span>",
		"1" => "<span style=\"color: green; font-weight: bold;\">Live</span>",
		"2" => "Invisible",
		"3" => "Waiting"
		);
	$status = $status_arr[$clad->getDone()];
	if ($clad->getDeleted())
		$status = "<span style=\"color: red;\">DELETED</span>";
	$html .= "Posted in: <strong>{$loc_count}</strong> location(s)<br />";
	$html .= "Status: {$status}";
	if ($clad->getDone() > -1)
		$html .= "<br />Posted: ".$clad->getDate()."<br />Expires: ".$clad->getExpires();
	else
		$html .= "<br />";
	return $html;
}

function delete() {
	global $account, $db;

	$live = ($_REQUEST["live"] == 1) ? true : false;
	$id = intval($_REQUEST["action_id"]);
	if (!$id)
		return actionError("Error: No classified id# specified!");

	$clad = clad::findOneById($id);
	if (!$clad)
		return actionError("Error: Classified id# {$id} not found!");

	$bp = $clad->getBp();
	if (!$bp) {
		$acc = $clad->getAccount();
		if (!$acc)
			echo "Error: can't get account of classified ad {$clad->getId()} !<br />\n";
	}

	if (!$live) {
		echo "<h1>Deleting classified ad #{$id}</h1>\n";

		if ($bp) {
			echo "<strong>This is BACKPAGE ad, without an owner.</strong><br /><br />\n";
		}

		echo "<div style=\"width: 420px; float: left;\">";
		echo "<fieldset style=\"border-color: #FF4500;\">";
		echo "<legend style=\"color: #FF4500;\">Selected classified ad</legend>";
		delete_print_ad($clad, $clad->getLocCount());
		$ad_important = false;
		$res = $db->q("SELECT id FROM admin_important WHERE type = 'CLA' AND item_id = ?", array($id));
		if ($db->numrows($res) > 0) {
			$ad_important = true;
		}
		if ($ad_important)
			echo "<span class=\"control_tag\"><img src=\"/images/control/money-bag-dollar.png\" /> Important ad</span><br />";
		echo "</fieldset>";
		if ($account->getId() == 3974) {
			echo "<fieldset style=\"border-color: #00D6DF;\">";
			echo "<legend style=\"color: #009399;\"><img src=\"/images/control/refund.png\" /> Refund ?</legend>";
			$res = $db->q("SELECT ap.id, cc.cc_id, ap.transaction_time, ap.total, ap.what, ap.item_id, cc.cc
								, ar.id as refund_id, ar.amount as refund_amount, ar.stamp as refund_stamp, aar.username as refund_who
							FROM account_purchase ap
							LEFT JOIN account_cc cc on cc.cc_id = ap.cc_id
							LEFT JOIN account_refund ar on ar.purchase_id = ap.id
							LEFT JOIN account aar on aar.account_id = ar.account_id
							WHERE ap.what = 'classifieds' AND ap.item_id = ?", array($id));
			if ($db->numrows($res) == 0) {
				echo "No purchases.";
			} else {
				echo "<img src=\"/images/control/refund.png\" />&nbsp;<input type=\"checkbox\" name=\"refund\" value=\"1\" /> Also refund following purchases:<br />";
				echo "<table class=\"control\">";
				echo "<thead><tr><th/><th>Date/time</th><th>Amount</th><th>Card</th><th/></tr></thead>\n";
				while ($row = $db->r($res)) {
					echo "<tr><td><input type=\"checkbox\" name=\"refund_ap_id[]\" value=\"{$row["id"]}\" /></td><td>".date("Y-m-d H:i:s T", $row["transaction_time"])."</td><td>{$row["total"]}</td><td>{$row["cc"]}</td><td>";
					if ($row["refund_id"])
						echo "Already refunded on ".date("m/d/Y", $row["refund_stamp"]);
					echo "</td></tr>";
				}
				echo "</table>";
			}
			echo "</fieldset>";
		}
		echo "</div>";

		if (!$bp && $acc) {
			echo "<fieldset style=\"width: 200px; float: left;\">";
			echo "<legend>Ad owner account</legend>";
			if ($acc->getAccountLevel() >= 2)
				echo "<span style=\"color: red; font-weight: bold;\"><img src=\"/images/control/exclamation--frame.png\" /> Administrator</span><br />";
			echo "Account id#: <a href=\"/mng/accounts?account_id={$acc->getId()}\">{$acc->getId()}</a><br />";
			echo "Email: {$acc->getEmail()}<br />";
			$status = "<span style=\"color: green; font-weight: bold;\">Live</span>";
			if ($acc->getDeleted())
				$status = "<span style=\"color: red;\">DELETED</span>";
			else if ($acc->isBanned())
				$status = "<span style=\"color: red;\">Banned</span>";
			echo "Status: {$status}<br />";
			if ($acc->isWhitelisted())
				echo "<span class=\"control_tag\"><img src=\"/images/control/star.png\" /> Whitelisted</span><br />";
			if ($acc->getNotes())
				echo "Notes: ".htmlspecialchars($acc->getNotes())."<br />";
			echo "</fieldset>";

			echo "<fieldset style=\"width: 400px; float: left;\">";
			echo "<legend>Other ads of this account</legend>";
			$res = $db->q("SELECT c.*, ai.id as important, count(cl.id) as loc_count
							FROM classifieds c
							LEFT JOIN admin_important ai ON ai.item_id = c.id and ai.type = 'CLA'
							LEFT JOIN classifieds_loc cl ON c.id = cl.post_id and cl.done = c.done
							WHERE c.account_id = ? AND c.id <> ?
							GROUP BY c.id, c.done
							ORDER BY c.done DESC", 
						array($acc->getId(), $id));
			$other_count = 0;
			$all_other_deleted = true;
			$some_ad_important = false;
			if ($db->numrows($res) == 0) {
				echo "<em>This account has no other ads.</em><br />";
			} else {
				while($row = $db->r($res)) {
					if ($other_count > 0)
						echo "<hr />";
					$oclad = clad::withRow($row);
					delete_print_ad($oclad, $row["loc_count"]);
					if ($row["important"]) {
						$some_ad_important = true;
						echo "<span class=\"control_tag\"><img src=\"/images/control/money-bag-dollar.png\" /> Important ad</span><br />";
					}
					if (!$oclad->getDeleted())
						$all_other_deleted = false;
					$other_count++;
				}
			}
			echo "</fieldset>";
		}

		$disabled_del = ""; $disabled_del_ban = ""; $disabled_del_all_ban = "";
		if ($clad->getDeleted())
			$disabled_del = " disabled";
		if ($bp || ($acc && $acc->isBanned()))
			$disabled_del_ban = " disabled";
		if ($other_count == 0 || $all_other_deleted)
			$disabled_del_all_ban = " disabled";

		$msg = "";
		if ($acc && $acc->isWhitelisted() && !$account->isrealadmin()) {
			//nobody but jay can delete/ban whitelisted users - dallas delete protection
			$disabled_del = " disabled";
			$disabled_del_ban = " disabled";
			$disabled_del_all_ban = " disabled";
			$msg = "<span style=\"color: blue; font-weight: bold;\">You can't delete ads / ban user, because account is whitelisted. Contact administrator if you want to do it.</span><br />";
		}

		$confirm_del = ""; $confirm_del_ban = ""; $confirm_del_all_ban = "";
		if ($ad_important) {
			$confirm_del = "onclick=\"return confirm('The ad is marked as important. It is probably sponsored ad. Are you sure you want to delete it ?');\" ";
			$confirm_del_ban = "onclick=\"return confirm('The ad is marked as important. It is probably sponsored ad. Are you sure you want to delete it ?');\" ";
			$confirm_del_all_ban = "onclick=\"return confirm('The ad is marked as important. It is probably sponsored ad. Are you sure you want to delete it ?');\" ";
		} else if ($acc && $acc->isWhitelisted()) {
			$confirm_del_ban = "onclick=\"return confirm('Account is whitelisted. Are you sure you want to ban it ?');\" ";
			$confirm_del_all_ban = "onclick=\"return confirm('Account is whitelisted. Are you sure you want to ban it ?');\" ";
		} else if ($some_ad_important) {
			$confirm_del_all_ban = "onclick=\"return confirm('Some of the ads are marked as important. They are probably sponsored ads. Are you sure you want to delete them ?');\" ";
		}
		

		echo "<br style=\"clear: both;\"/><br />";
		echo "<strong>What do you want to do ?</strong><br /><br />";
		echo $msg;
		if ($acc && $acc->getAccountLevel() >= 2)
			echo "<span class=\"control_warning\"><img src=\"/images/control/exclamation--frame.png\" /> This account is administrator ! Be careful with deleting ads and banning this account !</span><br />";
		echo "<a href=\"javascript:window.history.go(-1);\" class=\"control_btn\"><img src=\"/images/control/arrow-curve-180-left.png\" /> Go back</a>";
		echo "<a href=\"?action=delete&action_id={$id}&live=1&submit=del\" class=\"control_btn{$disabled_del}\" {$confirm_del}style=\"margin-left: 30px;\"><img src=\"/images/control/cross.png\" /> Only delete this ad</a>";
		echo "<a href=\"?action=delete&action_id={$id}&live=1&submit=del_ban\" id=\"btn_del_ban\" class=\"control_btn{$disabled_del_ban}\" {$confirm_del_ban}style=\"margin-left: 30px;\"><img src=\"/images/control/cross.png\" /> Delete this ad &amp; <img src=\"/images/control/user-red.png\" /> Ban account</a>";
		echo "<a href=\"?action=delete&action_id={$id}&live=1&submit=del_all_ban\" class=\"control_btn{$disabled_del_all_ban}\" {$confirm_del_all_ban}style=\"margin-left: 30px;\"><img src=\"/images/control/cross-two.png\" /> Delete *ALL* ads of this account &amp; <img src=\"/images/control/user-red.png\" /> Ban this account</a>";
		echo "<br /><br />";

		return false;
	}

	$submit = $_REQUEST["submit"];
	if ($submit != "del" && $submit != "del_ban" && $submit != "del_all_ban") {
		//this should not happen
		system::go("/mng/classifieds?action=delete&action_id={$id}", "Unknown action submitted ! ($submit)");
		die;
	}

	if ($submit == "del") {
		$ret = $clad->remove();
		if ($ret)
			return actionSuccess("Classified ad #{$id} has been deleted successfully.", "/mng/classifieds?cid={$id}");
		else
			return actionError("Error while deleting classified ad #{$id} !", "/mng/classifieds?cid={$id}");
	} else if ($submit == "del_ban") {
		$ret = $clad->remove();
		if (!$ret)
			return actionError("Error while deleting classified ad #{$id}. Account {$acc->getEmail()} has not been banned!", "/mng/classifieds?cid={$id}");
		if ($acc->isBanned()) {
			return actionSuccess("Classified ad #{$id} has been deleted successfully. Ad's owner acount {$acc->getEmail()} has already been banned before.", "/mng/classifieds?cid={$id}");
		} else {
			if (($acc->getAccountLevel() == 3) || (($acc->getAccountLevel() == 2) && !$account->isrealadmin()))
				return actionWarning("Classified ad #{$id} has been deleted successfully, but you can't ban administrator account (#{$account_id} - {$acc->getEmail()}). Please contact administrator.");
			$ret = $acc->ban();
			if ($ret) {
				return actionSuccess("Classified ad #{$id} has been deleted successfully. Account {$acc->getEmail()} has been successfully banned.", "/mng/classifieds?cid={$id}");
			} else {
				return actionWarning("Classified ad #{$id} has been deleted successfully, but there has been error banning account {$acc->getEmail()} ! This should not happen, please contact administrator.", "/mng/classifieds?cid={$id}");
			}
		}
	} else if ($submit == "del_all_ban") {
		$res = $db->q("SELECT c.* FROM classifieds c WHERE c.account_id = ?", array($acc->getId()));
		$succ = 0;
		$err = 0;
		while ($row = $db->r($res)) {
			//fetch clad
			$clad = clad::withRow($row);
			if (!$clad)
				continue;
			if ($clad->getDeleted())
				continue;
			//remove ad
			if ($clad->remove())
				$succ++;
			else
				$err++;
		}
		if (($acc->getAccountLevel() == 3) || (($acc->getAccountLevel() == 2) && !$account->isrealadmin())) {
			return actionWarning("{$succ} classified ads have been deleted successfully. You can't ban administrator account though (#{$account_id} - {$acc->getEmail()}). Please contact administrator.");
		}
		$ret = $acc->ban();
		if ($ret && ($err == 0)) {
			return actionSuccess("{$succ} classified ads have been deleted successfully. Account {$acc->getEmail()} has been successfully banned.", "/mng/classifieds?account_id={$acc->getId()}");
		} else {
			if ($ret)
				$acc_msg = "Account {$acc->getEmail()} has been successfully banned.";
			else
				$acc_msg = "Error banning account {$acc->getEmail()}!";
			return actionError("Error: {$succ} classified ads have been deleted, {$err} classifieds failed to be deleted. {$acc_msg}. This should not happen, please contact administrator.", "/mng/classifieds?cid={$id}");
		}
	}
	die;
}

function expire_ad() {
	global $db, $classifieds, $config_image_server;

	$live = ($_REQUEST["live"] == 1) ? true : false;
	$ids = getActionIds();
	$cnt = count($ids);

	if ($cnt == 0) {
		echo "No classifieds selected!<br />";
		return true;
	}

	if (!$live) {
		echo "Are you sure you want to expire these {$cnt} ads ?<br />";
	
		$sql = "SELECT c.id, c.thumb, c.type, c.title, a.account_id, a.username, a.email FROM classifieds c
				LEFT JOIN account a on c.account_id = a.account_id
				WHERE c.id IN (".implode(",",$ids).")";
		$res = $db->q($sql);
		if (!$db->numrows($res)) {
			echo "ERROR!"; die;
		}
		$cnt = $db->numrows($res);

		echo "<table><tr><th>Id</th><th>Thumb</th><th>Type</th><th>Title</th><th>Account Id</th><th>Account Email</th></tr>";
		while ($row = $db->r($res)) {
			echo "<tr><td>{$row["id"]}</td><td><img src=\"{$config_image_server}/classifieds/{$row["thumb"]}\" /></td><td>{$row["type"]}</td><td>".htmlspecialchars($row["title"])."</td><td>{$row["account_id"]}</td><td>{$row["email"]}</td></tr>";
		}
		echo "</table>";
	
		echo getConfirmActionForm("expire_ad", $ids);
		return false;
	}

	$sql = "SELECT c.*
			FROM classifieds c
			WHERE c.id IN (".implode(",",$ids).")";
	//echo "SQL='{$sql}'";
	$res = $db->q($sql);
	if (!$db->numrows($res)) {
		echo "ERROR!"; die;
	}
	$cl = 0;
	while ($row = $db->r($res)) {
		//fetch clad
		$clad = clad::withRow($row);
		if (!$clad)
			continue;
		//epxire ad
		if ($clad->expire())
			$cl++;
	}

	echo "<strong>{$cl}</strong> classified ad(s) successfully expired.<br />";

	return true;
}

function pay_ads() {
	global $account, $db, $classified_types, $config_image_server;

	$live = ($_REQUEST["live"] == 1) ? true : false;
	$ids = getActionIds();
	$cnt = count($ids);

	if ($cnt == 0) {
		echo "No classifieds selected!<br />";
		return true;
	}

	return system::go("/classifieds/pay?".getActionIdsQueryString());
}

function delete_ad() {
	global $db, $classifieds, $config_image_server;

	$live = ($_REQUEST["live"] == 1) ? true : false;
	$ids = getActionIds();
	$cnt = count($ids);

	if ($cnt == 0) {
		echo "No classifieds selected!<br />";
		return true;
	}

	if (!$live) {
		echo "Are you sure you want to delete these {$cnt} ads ?<br />";
		echo "<span style=\"color: red; font-weight: bold\">(Deleting classified ad is currently irreversible operation !)</span><br />";
	
		$sql = "SELECT c.id, c.thumb, c.type, c.title, a.account_id, a.username, a.email FROM classifieds c
				LEFT JOIN account a on c.account_id = a.account_id
				WHERE c.id IN (".implode(",",$ids).")";
		$res = $db->q($sql);
		if (!$db->numrows($res)) {
			echo "ERROR!"; die;
		}
		$cnt = $db->numrows($res);

		echo "<table><tr><th>Id</th><th>Thumb</th><th>Type</th><th>Title</th><th>Account Id</th><th>Account Email</th></tr>";
		while ($row = $db->r($res)) {
			echo "<tr><td>{$row["id"]}</td><td><img src=\"{$config_image_server}/classifieds/{$row["thumb"]}\" /></td><td>{$row["type"]}</td><td>".htmlspecialchars($row["title"])."</td><td>{$row["account_id"]}</td><td>{$row["email"]}</td></tr>";
		}
		echo "</table>";
	
		echo getConfirmActionForm("delete_ad", $ids);
		return false;
	}

	$sql = "SELECT c.*
			FROM classifieds c
			WHERE c.id IN (".implode(",",$ids).")";
	//echo "SQL='{$sql}'";
	$res = $db->q($sql);
	if (!$db->numrows($res)) {
		echo "ERROR!"; die;
	}
	$cl = 0;
	while ($row = $db->r($res)) {
		//fetch clad
		$clad = clad::withRow($row);
		if (!$clad)
			continue;
		//remove ad
		if ($clad->remove())
			$cl++;
	}

	echo "<strong>{$cl}</strong> classified ad(s) successfully removed.<br />";

	return true;
}

function delete_ad_ban_owner() {
	global $db, $classifieds, $config_image_server;

	$live = ($_REQUEST["live"] == 1) ? true : false;
	$ids = getActionIds();
	$cnt = count($ids);

	if ($cnt == 0) {
		echo "No classifieds selected!<br />";
		return true;
	}

	if (!$live) {
		echo "Are you sure you want to delete these {$cnt} ads and ban their owners ?<br />";
		echo "<span style=\"color: red; font-weight: bold\">(Deleting classified ad is currently irreversible operation !)</span><br />";
	
		$sql = "SELECT c.id, c.thumb, c.type, c.title, a.account_id, a.username, a.email FROM classifieds c
				LEFT JOIN account a on c.account_id = a.account_id
				WHERE c.id IN (".implode(",",$ids).")";
		$res = $db->q($sql);
		if (!$db->numrows($res)) {
			echo "ERROR!"; die;
		}
		$cnt = $db->numrows($res);

		echo "<table><tr><th>Id</th><th>Thumb</th><th>Type</th><th>Title</th><th>Account Id</th><th>Account Email</th></tr>";
		while ($row = $db->r($res)) {
			echo "<tr><td>{$row["id"]}</td><td><img src=\"{$config_image_server}/classifieds/{$row["thumb"]}\" /></td><td>{$row["type"]}</td><td>".htmlspecialchars($row["title"])."</td><td>{$row["account_id"]}</td><td>{$row["email"]}</td></tr>";
		}
		echo "</table>";
	
		echo getConfirmActionForm("delete_ad_ban_owner", $ids);
		return false;
	}

	$sql = "SELECT c.*
			FROM classifieds c
			WHERE c.id IN (".implode(",",$ids).")";
	//echo "SQL='{$sql}'";
	$res = $db->q($sql);
	if (!$db->numrows($res)) {
		echo "ERROR!"; die;
	}
	$cl = 0; $ac = 0;
	$banned_accounts = array();
	while ($row = $db->r($res)) {
		//fetch clad
		$clad = clad::withRow($row);
		if (!$clad)
			continue;
		//get account
		$acc = $clad->getAccount();
		//remove ad
		if ($clad->remove())
			$cl++;
		if (in_array($acc->getId(), $banned_accounts))
			continue;
		//ban account
		if ($acc && $acc->ban()) {
			$banned_accounts[] = $acc->getId();
			$ac++;
		}
	}

	echo "<strong>{$cl}</strong> classified ad(s) successfully removed. <strong>{$ac}</strong> account(s) successfully banned.<br />";

	return true;
}

function ban_owner_delete_all_his_ads() {
	global $db, $classifieds, $config_image_server;

	$live = ($_REQUEST["live"] == 1) ? true : false;
	$ids = getActionIds();
	$cnt = count($ids);

	if ($cnt == 0) {
		echo "No classifieds selected!<br />";
		return true;
	}

	//get account ids
	$sql = "SELECT distinct a.account_id
			FROM classifieds c
			INNER JOIN account a on a.account_id = c.account_id
			WHERE c.id IN (".implode(",",$ids).")";
	$res = $db->q($sql);
	if (!$db->numrows($res)) {
		echo "No owners of classified ads found !<br />";
		return true;
	}
	$cnt = $db->numrows($res);
	$aids = array();
	while ($row = $db->r($res)) {
		$aids[] = $row["account_id"];
	}

	if (!$live) {
		echo "Are you sure you want to ban these {$cnt} accounts, and delete <strong>ALL</strong> their classified ads (listed below) ?<br />";
		echo "<span style=\"color: red; font-weight: bold\">(Deleting classified ad is currently irreversible operation !)</span><br />";
	
		$sql = "SELECT a.account_id, a.username, a.email, c.id, c.thumb, c.type, c.title
				FROM account a
				LEFT JOIN classifieds c on c.account_id = a.account_id
				WHERE a.account_id IN (".implode(",",$aids).")
				ORDER BY a.account_id";
		$res = $db->q($sql);
		if (!$db->numrows($res)) {
			echo "ERROR!"; die;
		}
		$cnt = $db->numrows($res);

		echo "<table><tr><th>Account Id</th><th>Account Email</th><th>Classified Id</th><th>Thumb</th><th>Type</th><th>Title</th></tr>";
		while ($row = $db->r($res)) {
			echo "<tr><td>{$row["account_id"]}</td><td>{$row["email"]}</td><td>{$row["id"]}</td><td><img src=\"{$config_image_server}/classifieds/{$row["thumb"]}\" /></td><td>{$row["type"]}</td><td>".htmlspecialchars($row["title"])."</td></tr>";
		}
		echo "</table>";
		echo "Total number of classified ads = <strong>{$cnt}</strong>.<br />";
	
		echo getConfirmActionForm("ban_owner_delete_all_his_ads", $ids);
		return false;
	}

	//remove ads
	$sql = "SELECT c.*
			FROM classifieds c
			INNER JOIN account a on c.account_id = a.account_id
			WHERE a.account_id IN (".implode(",",$aids).")";
	$res = $db->q($sql);
	if (!$db->numrows($res)) {
		echo "ERROR!"; die;
	}
	$cl = 0; $ac = 0;
	while ($row = $db->r($res)) {
		//fetch clad
		$clad = clad::withRow($row);
		if (!$clad)
			continue;
		//remove ad
		if ($clad->remove())
			$cl++;
	}

	//ban accounts
	foreach ($aids as $id) {
		//get account
		$acc = account::findOneById($id);
		//ban account
		if ($acc && $acc->ban()) {
			$ac++;
		}
	}

	echo "<strong>{$cl}</strong> classified ad(s) successfully removed. <strong>{$ac}</strong> account(s) successfully banned.<br />";

	return true;
}

function blacklist() {
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid classified id !");

	$clad = clad::findOnebyId($action_id);
	if ($clad == false)
		return actionError("Cant find classified for id={$action_id} !");

	if (isset($_REQUEST["addtoblacklist"])) {
		$type = 0;
		$phrase = 'blacklist';
		if ((isset($_REQUEST["phone1"]) && $_REQUEST["phone1"] != '') || (isset($_REQUEST["email1"]) && $_REQUEST["email1"] != '') || (isset($_REQUEST["website1"]) && $_REQUEST["website1"] != '')){
			$phone = NULL;
			$email = NULL;
			$website = NULL;
			
			if(isset($_REQUEST["email1"])) {
				$email = $_REQUEST["email1"];
				if($email != ''){
					$exist = clbw::findOneByField($email, clbw::EMAIL);
					if($exist){
						actionError("Email address(".$email.") already exists in the ".$phrase);
					}
					else{
						$clbw = new clbw();
						$clbw->setData($email, clbw::EMAIL, $type);
						$ret = $clbw->addBWList();
					}
				}
				else{
					$email = NULL;
				}
			}

			if(isset($_REQUEST["phone1"])) {
				$phone = $_REQUEST["phone1"];
				if($phone != ''){
					$exist = clbw::findOneByField($phone, clbw::PHONE);
					if($exist){
						actionError("Phone number(".$phone.") already exists in the ".$phrase);
					}
					else{
						$clbw = new clbw();
						$clbw->setData($phone, clbw::PHONE, $type);
						$ret = $clbw->addBWList();
					}
				}
				else{
					$phone = NULL;
				}
			}
			
			if(isset($_REQUEST["website1"])) {
				$website = $_REQUEST["website1"];
				if($website != ''){
					$exist = clbw::findOneByField($phone, clbw::WEBSITE);
					if($exist){
						actionError("Website(".$website.") already exists in the ".$phrase);
					}
					else{
						$clbw = new clbw();
						$clbw->setData($website, clbw::WEBSITE, $type);
						$ret = $clbw->addBWList();
					}
				}
				else{
					$website = NULL;
				}
			}
		}
		if ($ret){
			$acc = account::findOneById($clad->getAccountId());
			if (!$acc) {
				flash::add(flash::MSG_WARNING, "Coudln't find account for this ad - not added account notes. Please contact administrator.");
			} else {
				$notes = $acc->getNotes();
				$notes .= "\nBlacklisted account";
				if($phone != NULL){
					$notes .= ", phone: {$phone}";
				}
				if($email != NULL){
					$notes .= ", email: {$email}";
				}
				if($website != NULL){
					$notes .= ", website: {$website}";
				}
				$acc->setNotes($notes);
				$ret = $acc->update();
				if (!$ret)
					flash::add(flash::MSG_WARNING, "Can't update account notes. Please contact administrator.");
			}

			return actionSuccess("You have successfully added data to the " . $phrase . ".");	
		}
		else
			return actionError("Error while adding data to the " . $phrase . ".");
	}

	echo "<h2>Add To Blacklist</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";

	$info_exist = false;
	if($clad->getEmail() != null && $clad->getEmail() != ''){
		echo "<tr><th>Email: </th><td><input type=\"checkbox\" name=\"email1\" value=\"".$clad->getEmail()."\">".$clad->getEmail()."</td></tr>\n";	
		$info_exist = true;
	}
	if($clad->getPhone() != null && $clad->getPhone() != ''){
		echo "<tr><th>Phone Number: </th><td><input type=\"checkbox\" name=\"phone1\" value=\"".$clad->getPhone()."\" size=\"8\">".$clad->getPhone()."</td></tr>\n";	
		$info_exist = true;
	}
	if($clad->getWebsite() != null && $clad->getWebsite() != ''){
		echo "<tr><th>Website: </th><td><input type=\"checkbox\" name=\"website1\" value=\"".$clad->getWebsite()."\">".$clad->getWebsite()."</td></tr>\n";
		$info_exist = true;
	}
	if(!$info_exist){
		echo "<tr><th></th><td>There is no information to be added to the blacklist.</td></tr>\n";
	}

	echo "<tr><th></th><td><input type=\"submit\" name=\"addtoblacklist\" value=\"Add\" />&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"button\" name=\"canceladd\" value=\"Cancel\" id=\"canceladdtobwlist\" /></td></tr>\n";
	echo "</table>";
	echo getFilterFormFields();
	echo "</form>";
	echo '<script type="text/javascript">';
	echo '$(document).ready(function() {
		$("#canceladdtobwlist").click(function(){
			window.history.back();
		});
	});';
	echo '</script>';
	return true;
}

function free_renew($months) {
	echo "<strong>TO be implemented...</strong><br />";
	return true;
}

function free_prolong($num_weeks, $interval = "WEEK") {
	global $db, $config_image_server;

	if (!in_array($interval, array("DAY", "WEEK")))
		return actionError("Invalid interval '{$interval}' !");

	$live = ($_REQUEST["live"] == 1) ? true : false;

	$ids = getActionIds();
	$cnt = count($ids);
	if ($cnt == 0)
		return actionWarning("No classifieds selected !");

	$clads = clad::findAllByIds($ids);
	if (empty($clads))
		return actionError("No classifieds found for selected ids !");
	$total = count($clads);

	if (!$live) {
		echo "<h2>Are you sure you want to prolong {$total} ads for {$num_weeks} {$interval}(s) ?</h2>";
		echo "<table><tr><th>Id</th><th>Thumb</th><th>Type</th><th>Title</th><th>Account Id</th><th>Account Email</th></tr>";
		foreach ($clads as $clad) {
			echo "<tr><td>{$clad->getId()}</td>";
			echo "<td><img src=\"{$config_image_server}/classifieds/{$clad->getThumb()}\" /></td>";
			echo "<td>{$clad->getType()}</td>";
			echo "<td>".htmlspecialchars($clad->getTitle())."</td>";
			echo "<td>{$clad->getAccountId()}</td>";
			$acc = $clad->getAccount();
			if ($acc)
				$email = $acc->getEmail();
			else
				$email = "<span style=\"color: red;\">No account found for account_id {$clad->getAccountId()} !</span>";
			echo "<td>{$email}</td></tr>";
		}
		echo "</table>";

		echo getConfirmActionForm("free_prolong_week", $ids);
		return false;
	}

	//prolong
	$upd = 0;
	foreach($clads as $clad) {
		$id = $clad->getId();
		$db->q("UPDATE classifieds SET expires = DATE_ADD(expires, INTERVAL {$num_weeks} {$interval}) WHERE id = ?", array($id));
		$aff = $db->affected();
		if ($aff == 1) {
			$clad->makeLive();
			$upd++;
		}
	}

	if ($upd == $total)
		return actionSuccess("All <strong>{$upd}</strong> classified ad(s) successfully prolonged.");
	else if ($upd == 0)
		return actionError("No classified ad(s) (out of selected {$total}) have been prolonged.");
	else
		return actionError("Warning: Only <strong>{$upd}</strong> from {$total} classified ad(s) have been successfully prolonged.");
}

//updates 'posted' timestamp in classified_loc and classifieds table to current timestamp (and therefore moving the ad to the top of the list)
function bump() {
	global $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid classified id !");

	$clad = clad::findOneById($action_id);
	if (!$clad)
		return actionError("Error: Can't find clad by id#{$action_id} !");

	$ret = $clad->bump();
	if ($ret)
		return actionSuccess("Classified ad #{$action_id} successfully reposted / bumped to the top of the list.");
	else
		return actionError("Error: Failed to repost / bump to the top classified ad #{$action_id} !");
}

//reposts ad (and decrements auto_renew counter)
function repost() {
	global $db, $config_image_server;

	$classifieds = new classifieds();

	$live = ($_REQUEST["live"] == 1) ? true : false;

	$ids = getActionIds();
	$cnt = count($ids);
	if ($cnt == 0)
		return actionWarning("No classifieds selected !");

	$clads = clad::findAllByIds($ids);
	if (empty($clads))
		return actionError("No classifieds found for selected ids !");
	$total = count($clads);

	if (!$live) {
		echo "<h2>Do you want to repost all these {$total} ads ?</h2>";
		echo "<table><tr><th>Id</th><th>Thumb</th><th>Title</th><th>Account</th><th></th></tr>";
		foreach ($clads as $clad) {
			echo "<tr><td>{$clad->getId()}</td>";
			echo "<td><img src=\"{$config_image_server}/classifieds/{$clad->getThumb()}\" /></td>";
			echo "<td>".htmlspecialchars($clad->getTitle())."</td>";

			$acc = $clad->getAccount();
			if ($acc)
				$email = $acc->getEmail();
			else
				$email = "<span style=\"color: red;\">No account found for account_id {$clad->getAccountId()} !</span>";
			$topup_credits = intval($clad->getAccount()->getRepost());

			echo "<td>";
			echo "#{$clad->getAccountId()}<br />";
			echo "{$email}<br />";
			echo "Top-Up credits: {$topup_credits}<br />";
			echo "</td>";

			echo "<td>";
			if ($topup_credits < 1)
				echo "<span style=\"color: red;\">Account has no topup credits, repost will be for free</span>";
			echo "</td>";

			echo "</tr>";
		}
		echo "</table>";

		echo getConfirmActionForm("repost", $ids, "<input type=\"checkbox\" name=\"auto_repost\" value=\"1\">Enable Auto-repost ?<br />\n");
		return false;
	}

	$auto_repost = intval($_REQUEST["auto_repost"]);

	//do reposts
	$reposted = 0;
	foreach($clads as $clad) {

		if ($auto_repost) {
			$clad->setAutoRepost(1);
			$clad->update();
		}

		$ret = $classifieds->repost($clad->getId(), true);
		if ($ret)
			$reposted++;
	}

	if ($reposted == $total)
		return actionSuccess("All <strong>{$reposted}</strong> classified ad(s) successfully reposted.");
	else
		return actionWarning("Warning: Only <strong>{$reposted}</strong> from {$total} classified ad(s) have been successfully reposted.");
}


//fills session variable so we can take over and finish this not posted ad
function take_over() {
	global $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid classified id !");

	$SESSION["cl_owner_{$action_id}"] = 1;

	return actionSuccess("Ad #{$action_id} taken over. Logout and go here: 'https://adultsearch.com/adbuild/step4?ad_id={$action_id}'.");
}

//prints out all personal data regarding this ad for law enforcement
function law_enforcement() {
	global $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid classified id !");

	$clad = clad::findOneById($action_id);
	if (!$clad)
		return actionError("Error: cant find this ad in database #{$action_id} !");

	$acc = $clad->getAccount();

	echo "<h1>Law enforcement info for classified ad #{$action_id}</h1>";
	
	echo "<div id=\"selectable\" onclick=\"selectText('selectable')\">\n";

	echo "<strong>Account:</strong><br />\n";
	echo "Registered under email: {$acc->getEmail()}<br />\n";
	echo "Registered on: ".date("m/d/Y H:i:s T", $acc->getCreatedStamp())."<br />\n";
	if ($acc->getPhoneVerified())
		echo "Verified with phone: ".makeproperphonenumber($acc->getPhone())."<br />\n";
	echo "Last Login: ".date("m/d/Y H:i:s T", $acc->getLastLoginStamp())."<br />\n";
	echo "IP Address: {$acc->getIpAddress()}<br />\n";
	echo "<br />\n";

	echo "<strong>Classified ad:</strong><br />\n";
	echo "Name on ad: {$clad->getFirstname()}<br />\n";
	if ($clad->getPhone())
		echo "Phone number on ad: ".makeproperphonenumber($clad->getPhone())."<br />\n";
	if ($clad->getEmail())
		echo "Email on ad: {$clad->getEmail()}<br />\n";

	$res = $db->q("
		select t.stamp, t.amount, cc.cc, cc.expmonth, cc.expyear, cc.firstname, cc.lastname, cc.address, cc.city, cc.state, cc.zipcode, cc.country
		from transaction t
		inner join payment p on p.id = t.payment_id
		inner join account_cc cc on cc.cc_id = p.cc_id
		where p.account_id = ? and p.p1 = 'classifieds' and p.p3 = ?
		union
		select t.stamp, t.amount, cc.cc, cc.expmonth, cc.expyear, cc.firstname, cc.lastname, cc.address, cc.city, cc.state, cc.zipcode, cc.country
		from transaction t
		inner join payment p on p.id = t.payment_id
		inner join payment_item pi on pi.payment_id = p.id
		inner join account_cc cc on cc.cc_id = p.cc_id
		where p.account_id = ? and pi.type = 'classified' and pi.classified_id = ?
		",
		[$clad->getAccountId(), $clad->getId(), $clad->getAccountId(), $clad->getId()]
		);
	if ($db->numrows($res) > 0) {
		echo "<br />\n";
		echo "<strong>Transactions</strong>:<br />\n";
		while ($row = $db->r($res)) {
			echo date("m/d/Y g:i:s A T", $row["stamp"]).", \${$row["amount"]}, Credit card: ".substr($row["cc"], -5)." {$row["expmonth"]}/{$row["expyear"]}, {$row["firstname"]} {$row["lastname"]}, {$row["address"]}, {$row["city"]}, {$row["state"]}, {$row["zipcode"]}, {$row["country"]}<br />\n"; 
		}
	}

	echo "</div>\n";	

	echo "<br /><form>";
	echo getFilterFormFields();
	echo "<input type=\"submit\" name=\"submit\" value=\"Go Back\" />";
	echo "</form>";
	return false;
}

function capture_whitelist_and_live() {
	global $db;

	$ids = getActionIds();
	$cnt = count($ids);
	if ($cnt == 0) {
		echo "No classifieds selected!<br />";
		return true;
	}
	//echo "Ids: <pre>".print_r($ids, true)."<br />";die;

	$sql = "SELECT * 
			FROM classifieds c
			WHERE c.id IN (".implode(",",$ids).")";
	$res = $db->q($sql);
	if (!$db->numrows($res)) {
		echo "ERROR!"; die;
	}
	$total = $db->numrows($res);
	$ver = 0;
	while ($row = $db->r($res)) {
		//fetch clad
		$clad = clad::withRow($row);
		if (!$clad)
			continue;
		if ($clad->getDone() != 3) {
			echo "Cl.ad #{$clad->getId()} is not in waiting state !<br />\n";
			continue;
		}

		//get payment and capture it
		$res2 = $db->q("
			SELECT p.id, p.trans_id
			FROM payment p 
			INNER JOIN payment_item pi on pi.payment_id = p.id
			WHERE pi.classified_id = ? AND p.result = 'U'
			",
			[$clad->getId()]
			);
		if ($db->numrows($res2) > 1) {
			flash::add(flash::MSG_ERROR, "Error: Found multiple uncaptured payments for classified ad #{$clad->getId()} -> please contact administrator");
			continue;
		} else if ($db->numrows($res2) == 0) {
			flash::add(flash::MSG_WARNING, "Error: Not found uncaptured payment for classified ad #{$clad->getId()} -> please contact administrator");
		} else {
			$row2 = $db->r($res2);
			$payment_id = $row2["id"];
			$trans_id = $row2["trans_id"];
			$ret = payment::capture($payment_id, $error);
			if (!$ret) {
				flash::add(flash::MSG_ERROR, "Error: Failed to capture payment #{$payment_id} (charge_id '{$trans_id}') -> please contact administrator");
				continue;
			} else {
				flash::add(flash::MSG_SUCCESS, "Payment #{$payment_id} (charge_id '{$trans_id}') captured successfully");
			}
		}

		//put up ad (done 3 -> 1) clad::makeLive
		if ($clad->makeLive()) {
			$ver++;
			flash::add(flash::MSG_SUCCESS, "Classified ad #{$clad->getId()} - was put up live: <a href=\"/mng/classifieds?cid={$clad->getId()}\" target=\"blank\">[ view ad ]</a>.");
		} else {
			flash::add(flash::MSG_ERROR, "Error: Failed to put up classified ad #{$clad->getId()} -> please contact administrator");
		}
		$clad->emailAdLive();

		//whitelist account
		$acc = $clad->getAccount();
		if ($acc->isWhitelisted()) {
			flash::add(flash::MSG_SUCCESS, "Account #{$acc->getId()} - {$acc->getEmail()} was alredy whitelisted");
		} else {
			$acc->setWhitelisted(1);
			$ret = $acc->update();
			if ($ret) {
				audit::log("ACC", "Whitelist", $acc->getId(), "");
				flash::add(flash::MSG_SUCCESS, "Account #{$acc->getId()} - {$acc->getEmail()} was whitelisted <a href=\"/mng/accounts?account_id={$acc->getId()}\" target=\"blank\">[ view account ]</a>.");
			} else {
				flash::add(flash::MSG_ERROR, "Error: Failed to whitelist account #{$acc->getId()} - {$acc->getEmail()} -> please contact administrator");
			}
		}
	}

	return actionBack();
}

function delete_and_refund() {
	global $db;

	$ids = getActionIds();
	$cnt = count($ids);
	if ($cnt == 0) {
		echo "No classifieds selected!<br />";
		return true;
	}
	if ($cnt > 1) {
        return actionError("You can't call this with multiple action ids ! - please contact administrator");
	}
	//echo "Ids: <pre>".print_r($ids, true)."<br />";die;

	$sql = "SELECT * 
			FROM classifieds c
			WHERE c.id IN (".implode(",",$ids).")";
	$res = $db->q($sql);
	if (!$db->numrows($res)) {
		echo "ERROR!"; die;
	}
	$total = $db->numrows($res);
	$ver = 0;
	while ($row = $db->r($res)) {
		//fetch clad
		$clad = clad::withRow($row);
		if (!$clad)
			continue;
		if ($clad->getDone() != 3) {
			echo "Cl.ad #{$clad->getId()} is not in waiting state !<br />\n";
			continue;
		}

		//first check if account is still not whitelisted -> so we dont have conflicting resolutions if there are 2 waiting clads for one account
		$acc = $clad->getAccount();
		if ($acc->isWhitelisted()) {
			flash::add(flash::MSG_ERROR, "Account #{$acc->getId()} - {$acc->getEmail()} is whitelisted ?? Error - please contact administrator");
			return false;
		}

		//then refund the authorised payment
		$res2 = $db->q("
			SELECT p.id, p.amount
			FROM payment p
			WHERE p.account_id = ? AND p.result = 'U'
			",
			[$clad->getAccountId()]
			);
		while ($row2 = $db->r($res2)) {
			$payment_id = $row2["id"];
			$amount = $row2["amount"];
		    $error = null;
			$ret = payment::refund($payment_id, null, true, null, "Not approved advertiser", $error);
			if ($ret === false) {
				return actionError("Refund of payment #{$payment_id} failed: '{$error}'. Please contact administrator.");
			}
			$msg = $ret;
			//audit refund
			audit::log("REF", "Refund", "P".$payment_id, $msg, $clad->getAccountId());
			flash::add(flash::MSG_SUCCESS, "Payment #{$payment_id} in amount \${$amount} was refunded.");
		}

		//then delete the ad
		if ($clad->remove()) {
			flash::add(flash::MSG_SUCCESS, "Classified ad #{$clad->getId()} was deleted <a href=\"/mng/classifieds?cid={$clad->getId()}&deleted=all\" target=\"blank\">[ view ad ]</a>.");
		} else {
			flash::add(flash::MSG_ERROR, "Error: Failed to delete classified ad #{$clad->getId()} -> please contact administrator");
		}
	}

	return actionBack();
}



$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'manage':
		if (!permission::has("classified_manage"))
			die;
		$ret = manage();
		if (!$ret)
			return;
		break;
	case 'undelete':
		if (!permission::has("classified_undelete"))
			die;
		$ret = undelete();
		if (!$ret)
			return;
		break;
	case 'recreate_thumb':
		if (!permission::has("manage_all_ads"))
			die;
		$ret = recreate_thumb();
		if (!$ret)
			return;
		break;
	case 'recreate_multiple_thumbs':
		if (!permission::has("manage_all_ads"))
			die;
		$ret = recreate_multiple_thumbs();
		if (!$ret)
			return;
		break;
	case 'ebiz_create':
		if (!permission::has("manage_all_ads"))
			die;
		$ret = ebiz_create();
		if (!$ret)
			return;
		break;
	case 'prevent_delete':
		if (!permission::has("manage_all_ads"))
			die;
		$ret = prevent_delete();
		if (!$ret)
			return;
		break;
	case 'resend_receipt':
//		if (!permission::has("manage_all_ads"))
//			die;
		$ret = resend_receipt();
		if (!$ret)
			return;
		break;
	case 'send_admin_notify_to_me':
		if (!permission::has("manage_all_ads"))
			die;
		$ret = send_admin_notify_to_me();
		if (!$ret)
			return;
		break;
	case 'makeLive':
		if (!permission::has("classified_manage"))
			die;
		$ret = makeLive();
		if (!$ret)
			return;
		break;
	case 'delete':
		if (!permission::has("classified_delete"))
			die;
		$ret = delete();
		if (!$ret)
			return;
		break;
	case 'expire_ad':
		if (!permission::has("classified_manage"))
			die;
		$ret = expire_ad();
		if (!$ret)
			return;
		break;
	case 'pay_ads':
		if (!permission::has("classified_manage"))
			die;
		$ret = pay_ads();
		if (!$ret)
			return;
		break;
	case 'delete_ad':
		if (!permission::has("classified_delete"))
			die;
		$ret = delete_ad();
		if (!$ret)
			return;
		break;
	case 'delete_ad_ban_owner':
		if (!permission::has("classified_delete") || !permission::has("account_ban"))
			die;
		$ret = delete_ad_ban_owner();
		if (!$ret)
			return;
		break;
	case 'ban_owner_delete_all_his_ads':
		if (!permission::has("classified_delete") || !permission::has("account_ban"))
			die;
		$ret = ban_owner_delete_all_his_ads();
		if (!$ret)
			return;
		break;
	case 'move_to_phone_and_web':
		if (!permission::has("manage_all_ads"))
			die;
		$ret = move_to_phone_and_web();
		if (!$ret)
			return;
		break;
	case 'free_renew_3_months':
		if (!permission::has("classified_manage"))
			die;
		$ret = free_renew(3);
		if (!$ret)
			return;
		break;
	case 'free_prolong_week':
		if (!permission::has("classified_manage"))
			die;
		$ret = free_prolong(1, "WEEK");
		if (!$ret)
			return;
		break;
	case 'free_prolong_month':
		if (!permission::has("classified_manage"))
			die;
		$ret = free_prolong(30, "DAY");
		if (!$ret)
			return;
		break;
	case 'blacklist':
		if (!permission::has("classified_blacklist"))
			die;
		$ret = blacklist();
		if (!$ret)
			return;
		break;
	case 'bump':
		if (!permission::has("classified_manage"))
			die;
		$ret = bump();
		if (!$ret)
			return;
		break;
	case 'repost':
		if (!permission::has("classified_manage"))
			die;
		$ret = repost();
		if (!$ret)
			return;
		break;
	case 'take_over':
		if (!$account->isadmin())
			die;
		$ret = take_over();
		if (!$ret)
			return;
		break;
	case 'law_enforcement':
		if (!$account->isadmin())
			die;
		$ret = law_enforcement();
		if (!$ret)
			return;
		break;
	case 'capture_whitelist_and_live':
		if (!permission::has("classified_manage"))
            die;
		$ret = capture_whitelist_and_live();
		if (!$ret)
			return;
		break;
	case 'delete_and_refund':
		if (!permission::has("classified_manage"))
            die;
		$ret = delete_and_refund();
		if (!$ret)
			return;
		break;
}

$params = [];
$where = getWhere($params);
//dallas 25.9.2017 - don't show not posted yet ads by default
if (strpos($where, "c.done") === false) {
	if ($where)
		$where .= " AND c.done <> -1 ";
	else
		$where = " WHERE c.done <> -1 ";
}
//dallas 15.1.2018 - don't show deleted ads by default
if (!array_key_exists("deleted", $filters)) {
	if ($where)
		$where .= " AND c.deleted IS NULL ";
	else
		$where = " WHERE c.deleted IS NULL ";
}

$having = getHaving();
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order)) {
	//if we are filtering ads by account_id, list live ads first and also last reposted ads first
	if (array_key_exists("account_id", $filters))
		$order = "ORDER BY c.done DESC, cl.updated DESC";
	else
		$order = "ORDER BY c.id DESC";
}

//query db
$sql = "SELECT count(*) as total 
		FROM (
			SELECT c.id
			FROM classifieds c
			LEFT JOIN account a on a.account_id = c.account_id
			LEFT JOIN classifieds_loc cl on cl.post_id = c.id
			$where
			GROUP BY c.id
			$having
			) a
		";
if (isFilterSet("usa") == 1) {
	$sql = "SELECT count(*) as total 
			FROM (
				SELECT c.id
				FROM classifieds c
				LEFT JOIN account a on a.account_id = c.account_id
				INNER JOIN classifieds_loc cl on cl.post_id = c.id
				INNER JOIN location_location l on l.loc_id = cl.loc_id
				$where
				GROUP BY c.id
				$having
				) a
			";
}

$res = $db->q($sql, $params);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT c.id, c.thumb, c.account_id, c.bp, c.deleted, c.done, UNIX_TIMESTAMP(c.date) as `date`, c.expires
			, c.type, c.phone, c.firstname, c.title, c.content
			, c.sponsor, c.sponsor_mobile, c.promo_code
			, a.email, a.whitelisted, a.agency, a.repost
			, COUNT(DISTINCT cl.id) as loc_count, GROUP_CONCAT(DISTINCT cl.loc_id) as loc_ids
			, COUNT(DISTINCT cs.id) as sponsor_locs, SUM(cs.day) as sponsor_days, MAX(cs.expire_stamp) as sponsor_expire_stamp
			, COUNT(DISTINCT cs2.id) as side_locs, SUM(cs2.day) as side_days, MAX(cs2.expire_stamp) as side_expire_stamp
			, COUNT(DISTINCT cs3.id) as sticky_locs, SUM(cs3.days) as sticky_days, MAX(cs3.expire_stamp) as sticky_expire_stamp
			, (SELECT CONCAT('$',ROUND(ap.total, 2),' on ',ap.time) FROM account_purchase ap WHERE ap.item_id = c.id ORDER BY id DESC LIMIT 1) as last_purchase
			, (SELECT COUNT(cv.id) FROM classified_video AS cv WHERE cv.classified_id = c.id AND cv.converted = 2) AS video_count
		FROM classifieds c
		LEFT JOIN account a on a.account_id = c.account_id
		LEFT JOIN classifieds_loc cl on cl.post_id = c.id and cl.done <> -1 
		LEFT JOIN classifieds_sponsor cs on cs.post_id = c.id and cs.done = 1
		LEFT JOIN classifieds_side cs2 on cs2.post_id = c.id and cs2.done = 1
		LEFT JOIN classified_sticky cs3 on cs3.classified_id = c.id and cs3.done = 1
		$where
		GROUP BY c.id
		$having
		$order 
		$limit
";

if (isFilterSet("usa") == 1) {
	$sql = "SELECT c.id, c.thumb, c.account_id, c.bp, c.deleted, c.done, UNIX_TIMESTAMP(c.date) as `date`, c.expires
				, c.type, c.phone, c.firstname, c.title, c.content
				, c.sponsor, c.sponsor_mobile, c.promo_code
				, a.email, a.whitelisted, a.agency, a.repost
				, COUNT(DISTINCT cl.id) as loc_count, GROUP_CONCAT(DISTINCT cl.loc_id) as loc_ids
				, COUNT(DISTINCT cs.id) as sponsor_locs, SUM(cs.day) as sponsor_days, MAX(cs.expire_stamp) as sponsor_expire_stamp
				, COUNT(DISTINCT cs2.id) as side_locs, SUM(cs2.day) as side_days, MAX(cs2.expire_stamp) as side_expire_stamp
				, (SELECT COUNT(cv.id) FROM classified_video AS cv WHERE cv.classified_id = c.id AND cv.converted = 2) AS video_count
			FROM classifieds c
			LEFT JOIN account a on a.account_id = c.account_id
			INNER JOIN classifieds_loc cl on cl.post_id = c.id and cl.done <> -1
			INNER JOIN location_location l on l.loc_id = cl.loc_id
			LEFT JOIN classifieds_sponsor cs on cs.post_id = c.id
			$where
			GROUP BY c.id
			$having
			$order 
			$limit
	";
}

$res = $db->q($sql, $params);
if ($db->numrows($res) == 0) {
	echo "No classifieds.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Classifieds</h2>\n";

displayFilterForm();

echo $pager."<br />";

echo "<form method=\"post\">";
	echo "<select name=\"action\"><option value=\"\">-</option>";
if (permission::has("classified_manage")) {
//	echo "<option value=\"makeLive\">Make live ad(s)</option>";
	echo "<option value=\"expire_ad\">Expire ad(s)</option>";
	echo "<option value=\"pay_ads\">Pay for these ads</option>";
}
if (permission::has("classified_delete")) {
	echo "<option value=\"delete_ad\">Delete ad(s)</option>";
	if (permission::has("account_ban")) {
		echo "<option value=\"delete_ad_ban_owner\">Delete ad(s) &amp; ban owner(s)</option>";
		echo "<option value=\"ban_owner_delete_all_his_ads\">Ban owner(s) of selected ads &amp; delete ALL their ads</option>";
	}
}
if (permission::has("manage_all_ads"))
	echo "<option value=\"undelete\">Undelete ad(s)</option>";

//	echo "<option value=\"move_to_phone_and_web\">Move to Phone &amp; Web</option>";
	//echo "<option value=\"free_renew_3_months\">Free renew for 3 months</option>";
if (permission::has("classified_manage")) {
	echo "<option value=\"repost\">Repost</option>";
	echo "<option value=\"free_prolong_week\">Free prolong week</option>";
	echo "<option value=\"free_prolong_month\">Free prolong month</option>";
}
	echo "</select>";
	echo "<input type=\"submit\" name=\"submit\" value=\"Execute...\" />";

echo getFilterFormFields();

echo "<table class=\"control\">";
echo "<thead><tr>
<th class=\"check\"><input type=\"checkbox\" name=\"master\" value=\"\" /></th>
<th>".getOrderLink("Id", "id")."</th>
<th>Posted</th>
<th>".getOrderLink("Status", "done")."</th>
<th>Thumb</th>
<th>Video</th>
<th>Acc.ID</th>
<th>".getOrderLink("Type", "type")."</th>
<th>Upgrades</th>
<th>Phone</th>
<th>".getOrderLink("Firstname", "firstname")."</th>
<th>".getOrderLink("Title", "title")."</th>
<th>Location(s)</th>
<th>Last Purchase</th>
<th/>
</tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	$deleted = ($rox["deleted"]) ? true : false;

	if ($deleted)
		echo "<tr style=\"background-color: #FFAAAA;\">";
	else
		echo "<tr>";

	echo "<td class=\"check\"><input type=\"checkbox\" name=\"action_id[]\" value=\"{$rox['id']}\" /></td>";
	echo "<td>{$rox['id']}</td>";
	echo "<td>".get_datetime_admin_timezone($rox["date"])."</td>";

	if ($deleted) {
		echo "<td><span title=\"Done: {$rox["done"]}, Expires: {$rox["expires"]}\">DELETED</span></td>";
	} else {
		$status = "<span style=\"color: red;\">???</span>";
		switch($rox["done"]) {
			case -2: $status="<span title=\"Expires on {$rox["expires"]}\">To be removed</span>";break;
			case -1: $status="<span title=\"Expires on {$rox["expires"]}\">Not posted yet</span>";break;
			case 0: $status="<span title=\"Expired on {$rox["expires"]}\">Expired</span>";break;
			case 1: $status="<span title=\"Expires on {$rox["expires"]}\" style=\"font-weight: bold; color: green;\">Live</span>";break;
			case 2: $status="<span title=\"Expires on {$rox["expires"]}\">Invisible</span>";break;
			case 3: $status="<span title=\"Expires on {$rox["expires"]}\">Waiting</span>";break;
		}
		echo "<td>{$status}</td>";
	}

	if ($rox["thumb"])
		echo "<td><img src=\"{$config_image_server}/classifieds/{$rox["thumb"]}\" style=\"max-width: 75px;\"/></td>";
	else
		echo "<td></td>";


	echo '<td>',$rox["video_count"]>0?$rox["video_count"]:'','</td>';

	if ($rox["account_id"] == 0) {
		echo "<td></td>";
	} else {
		echo "<td>";
		echo "<a href=\"/mng/accounts?account_id={$rox["account_id"]}\">{$rox["account_id"]}</a><br />({$rox["email"]})";
		if ($rox["agency"])
	        echo "<br /><span style=\"color: green; font-weight: bold;\">Escort agency</span>";
		else if ($rox["whitelisted"])
			echo "<br /><span style=\"font-weight: bold; color: green;\">whitelisted</span>";
		echo "</td>";
	}

	/*
	if ($rox["bp"] == 1)
		echo "<td>BP</td>";
	else
		echo "<td></td>";
	*/

	echo "<td>{$classified_types[$rox["type"]]}</td>";

	echo "<td>";
	if ($rox["sponsor_expire_stamp"]) {
		$sponsor_expire_stamp = $rox["sponsor_expire_stamp"];
		$sponsor_expire_datetime = date("m/d/Y H:i", $sponsor_expire_stamp);
		$now = time();
		if ($sponsor_expire_stamp > $now) {
			$sp_title = "City cover expires in ".time_elapsed_string($sponsor_expire_stamp)." ({$$sponsor_expire_datetime}) - paid for {$rox["sponsor_locs"]} location(s), in total for {$rox["sponsor_days"]} days";
			echo "<span title=\"{$sp_title}\">City thumb <span style=\"color: green;\">Live</span><br />({$rox["sponsor_locs"]} loc(s) / {$rox["sponsor_days"]} days)</span>";
		} else {
			$sp_title = "City cover expired on {$sponsor_expire_datetime} (".time_elapsed_string($sponsor_expire_stamp).") - paid for {$rox["sponsor_locs"]} location(s), in total for {$rox["sponsor_days"]} days";
			echo "<span title=\"{$sp_title}\">City thumb expired<br />({$rox["sponsor_locs"]} loc(s) / {$rox["sponsor_days"]} days)</span>";
		}
		echo "<br />";
	}
	if ($rox["side_expire_stamp"]) {
		$side_expire_stamp = $rox["side_expire_stamp"];
		$side_expire_datetime = date("m/d/Y H:i", $side_expire_stamp);
		$now = time();
		if ($side_expire_stamp > $now) {
			$sp_title = "Side sponsor expires in ".time_elapsed_string($side_expire_stamp)." ({$$side_expire_datetime}) - paid for {$rox["side_locs"]} location(s), in total for {$rox["side_days"]} days";
			echo "<span title=\"{$sp_title}\">Side sponsor <span style=\"color: green;\">Live</span><br />({$rox["side_locs"]} loc(s) / {$rox["side_days"]} days)</span>";
		} else {
			$sp_title = "Side sponsor expired on {$side_expire_datetime} (".time_elapsed_string($side_expire_stamp).") - paid for {$rox["side_locs"]} location(s), in total for {$rox["side_days"]} days";
			echo "<span title=\"{$sp_title}\">Side sponsor expired<br />({$rox["side_locs"]} loc(s) / {$rox["side_days"]} days)</span>";
		}
		echo "<br />";
	}
	if ($rox["sticky_expire_stamp"]) {
		$sticky_expire_stamp = $rox["sticky_expire_stamp"];
		$sticky_expire_datetime = date("m/d/Y H:i", $sticky_expire_stamp);
		$now = time();
		if ($sticky_expire_stamp > $now) {
			$sp_title = "Sticky sponsor expires in ".time_elapsed_string($sticky_expire_stamp)." ({$$sticky_expire_datetime}) - paid for {$rox["sticky_locs"]} location(s), in total for {$rox["sticky_days"]} days";
			echo "<span title=\"{$sp_title}\">Sticky sponsor <span style=\"color: green;\">Live</span><br />({$rox["sticky_locs"]} loc(s) / {$rox["sticky_days"]} days)</span>";
		} else {
			$sp_title = "Sticky sponsor expired on {$sticky_expire_datetime} (".time_elapsed_string($sticky_expire_stamp).") - paid for {$rox["sticky_locs"]} location(s), in total for {$rox["sticky_days"]} days";
			echo "<span title=\"{$sp_title}\">Sticky sponsor expired<br />({$rox["sticky_locs"]} loc(s) / {$rox["sticky_days"]} days)</span>";
		}
		echo "<br />";
	}

	if ($rox["sponsor"] > 0 && $rox["sponsor_mobile"] > 0) {
		echo "<span style=\"font-weight: bold; color: green;\">Desktop &amp; mobile sponsor</span><br />";
	} else if ($rox["sponsor"] > 0) {
		echo "<span style=\"font-weight: bold; color: green;\">Desktop Sponsor</span><br />";
	} else if ($rox["sponsor_mobile"] > 0) {
		echo "<span style=\"font-weight: bold; color: green;\">Mobile Sponsor</span><br />";
	}
	echo "</td>";

	echo "<td>".makeproperphonenumber($rox["phone"])."</td>";
	echo "<td>{$rox["firstname"]}</td>";
	echo "<td>{$rox["title"]}</td>";

	$links = "";

	$at_least_one_loc_valid = false;
	if ($rox["loc_count"] > 0) {
		$arr = explode(',', $rox['loc_ids']);
		if (strlen($rox['loc_ids']) >= 1024) {
			array_pop($arr);
//			echo "<span style=\"color: red;\">These are not all the results!</span><br/>";
		}
		$type_url = $classifieds->getModuleByType($rox["type"]);
		$locs = "";
		$i = 0;
		foreach ($arr as $loc_id) {
			if ($i >= 3) {
				$locs .= ", ...";
				break;
			}
			$locs .= (empty($locs)) ? "" : "; ";
			$loc = location::findOneById($loc_id);
			if (!$loc) {
				$locs .= "Non-exist loc id=#{$loc_id}";
			} else {
				$at_least_one_loc_valid = true;
				$locs .= $loc->getName();
				if ($loc->getS())
					$locs .= ",".$loc->getS();
			}
			$i++;
		}
		if (permission::has("manage_all_ads")) {
			if ($rox["loc_count"] == 1)
				echo "<td><a href=\"/classifieds/move?id={$rox['id']}\" title=\"Change locations for classified ad #{$rox['id']}\">{$rox["loc_count"]} loc: {$locs}</a></td>";
			else
				echo "<td><a href=\"/classifieds/move?id={$rox['id']}\" title=\"Change locations for classified ad #{$rox['id']}\">{$rox["loc_count"]} locs: {$locs}</a></td>";
		} else {
			if ($rox["loc_count"] == 1)
				echo "<td>{$rox["loc_count"]} loc: {$locs}</td>";
			else
				echo "<td>{$rox["loc_count"]} locs: {$locs}</td>";
		}
	} else
		echo "<td><span style=\"color: red; font-weight: bold;\">no locations ?!</span></td>";

	echo "<td>{$rox["last_purchase"]}</td>";

	if ($rox["done"] == 3 && permission::has("classified_manage") && !$deleted) {
		$links .= "<a href=\"".getActionLink("capture_whitelist_and_live", $rox['id'])."\" class=\"btn btn-sm btn-success\" style=\"margin-bottom: 5px;\">whitelist owner and put ad live</a><br />";
		$links .= "<a href=\"".getActionLink("delete_and_refund", $rox['id'])."\" class=\"btn btn-sm btn-danger\" style=\"margin-bottom: 5px;\">delete ad and refund</a><br />";
	}

	if (permission::has("classified_manage")) {
		$links .= "<a href=\"".getActionLink("manage", $rox['id'])."\">manage</a>&nbsp;&middot;&nbsp;";
	} else if (permission::has("classified_edit")) {
		$links .= "<a href=\"https://adultsearch.com/adbuild/step3?ad_id={$rox['id']}&ref={$_SERVER["REQUEST_URI"]}\">edit</a>&nbsp;&middot;&nbsp;";
	}

	if ($at_least_one_loc_valid) {
		$links .= "<a href=\"".$loc->getUrl()."/{$type_url}/{$rox['id']}\" target=\"_blank\">view</a>";
	}

	if (permission::has("access_admin_sales"))
		$links .= "&nbsp;&middot; <a href=\"/mng/sales?account_id={$rox["account_id"]}&item_id={$rox['id']}\">sales</a>";

	if ($account->isrealadmin()) {
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("recreate_thumb", $rox['id'])."\">(re)create thumb</a>";
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("recreate_multiple_thumbs", $rox['id'])."\">(re)create multiple thumbs</a>";
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("ebiz_create", $rox['id'])."\">ebiz_create</a>";
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("prevent_delete", $rox['id'])."\">prevent_delete</a>";
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("send_admin_notify_to_me", $rox['id'])."\">send_admin_notify_to_me</a>";
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("bump", $rox['id'])."\">bump</a>";
	}

	if ($rox["done"] != 3) {

		if (permission::has("classified_pay"))
			$links .= "&nbsp;&middot; <a href=\"/classifieds/pay?action_id={$rox['id']}\">pay</a>";

		if (permission::has("access_admin_audit"))
			$links .= "&nbsp;&middot; <a href=\"/mng/audit?type=CLA&p1={$rox['id']}\">history</a>";

		if ($rox["done"] == 0 && permission::has("classified_manage"))
			$links .= "&nbsp;&middot; <a href=\"".getActionLink("pay_ads", $rox['id'])."\">pay for ad</a>";

		if (permission::has("account_whitelist") && !$rox["whitelisted"])
			$links .= "&nbsp;&middot; <a href=\"/mng/accounts?action=whitelist&action_id={$rox["account_id"]}\">whitelist_ad_owner</a>";

		if (permission::has("classified_manage") && $rox["repost"] > 0) {
			$links .= "&nbsp;&middot; <a href=\"".getActionLink("repost", $rox['id'])."\">repost</a>";
		}

		$links .= "&nbsp;&middot; <a href=\"".getActionLink("resend_receipt", $rox['id'])."\">resend_receipt...</a>";

		if ($account->isadmin() && $rox["done"] == -1)
			$links .= "&nbsp;&middot; <a href=\"".getActionLink("take_over", $rox['id'])."\">take_over</a>";

		$links .= "&nbsp;&middot; <a href=\"".getActionLink("law_enforcement", $rox['id'])."\">law enforcement</a>";
	}
	
	if (permission::has("classified_undelete") && $deleted)
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("undelete", $rox['id'])."\">undelete</a>";


	if (!$deleted || $account->isrealadmin()) {
		echo "<td>{$links}</td>";
	} else {
		echo "<td>{$links}</td>";
	}


	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo "</form>";
echo $pager;

?>
