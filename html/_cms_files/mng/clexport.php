<?php
require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("classified_manage"))
    return;

if ($_REQUEST["submit"] == "Download") {
	download();
}

if ($_REQUEST["submit"] == "Escorts with TER") {
	escorts_with_ter();
}
function downloadCities($cat) {
	global $db;

	switch($cat) {
		case 1: $cat = "female-escorts/"; $anchor = "escorts"; break;
		case 2: $cat = "tstv-shemale-escorts/"; $anchor = "TV/TS escorts"; break;
		case 3: $cat = "male-escorts-for-female/"; $anchor = "m4f escorts"; break;
		case 4: $cat = "male-for-male-escorts/"; $anchor = "m4m escorts"; break;
		default: $cat = ""; $anchor = "";
	}

	$fp = fopen(_CMS_ABS_PATH.'/UserFiles/excel/city.csv', 'w');
	$fields = "'City','Parent','URL','Anchor'\n";
	fputs($fp, $fields);
	$res = $db->q("SELECT l.loc_name city, l2.loc_name parent_location, 
						concat('http://', ((CASE WHEN length(l.country_sub) > 0 THEN concat(l.country_sub,'.') ELSE '' END)), 'adultsearch.com', l.loc_url,'$cat') url, 
						concat(l.loc_name, ' $anchor') anchor
					FROM location_location l
					INNER JOIN location_location l2 on l.loc_parent = l2.loc_id
					WHERE l.loc_type = 3 AND l.has_place_or_ad = 1");
	while($row=$db->r($res)) {
		$fields = "'{$row['city']}','{$row['parent_location']}','{$row['url']}','{$row['anchor']}'\n";
		fputs($fp, $fields);
	}
	fclose($fp);

	system::moved("http://adultsearch.com/UserFiles/excel/city.csv");
}

function download() {
	global $db;
	
	require(_CMS_ABS_PATH.'/_cms_files/classifieds/array.php');

	$params = array();

//	$type = "AND (c.type = 1 OR c.type = 2)";
	$type_sql = "";

	$account_sql = "";
	if (intval($_REQUEST["account_id"])) {
		$account_sql = " AND c.account_id = '".intval($_REQUEST["account_id"])."' ";
	}

	$done_sql = "";
	if ($_REQUEST["done"]) {
		if ($_REQUEST["done"] == "-5")
			$done_sql = " AND c.deleted IS NOT NULL ";
		else
			$done_sql = " AND c.deleted IS NULL AND c.done = '".intval($_REQUEST["done"])."' ";
	}

	$paid_sql = "";
	if ($_REQUEST["paid"] == "paid") {
		_d("only paid");
		$paid_sql = " AND c.paid > 0 ";
	} else if ($_REQUEST["paid"] == "free") {
		$paid_sql = " AND c.paid = 0 ";
	}

	$currently_sponsor_sql = "";
	if ($_REQUEST["currently_sponsor"] == "no") {
		$currently_sponsor_sql = " AND (cs.id IS NULL OR cs.expire_stamp < '".time()."') ";
	}

	$currently_recurring_sql = "";
	if ($_REQUEST["currently_recurring"] == "no") {
		$currently_recurring_sql = " AND c.recurring = 0 ";
	}

	$promocode_sql = "";
	if ($_REQUEST["promocode"] != "") {
		$promocode_sql = " AND c.promo_code = ?";
		$params[] = $_REQUEST["promocode"];
	}

	$order = "";
	$order_way = "asc";
	if ($_REQUEST["order_way"] == "desc")
		$order_way = "desc";
	switch ($_REQUEST["order_by"]) {
		case "created":
			$order = "ORDER BY created {$order_way}";
			break;
		case "country_state_city":
			$order = "ORDER BY country_id, state_name, city_name";
			break;
		case "id":
		default:
			$order = "ORDER BY id {$order_way}";
			break;
	}

	$sql = "SELECT country.loc_id as country_id, country.dir as country_dir, country.loc_name as country_name, 
                    city.loc_url, city.dir as city_dir, state.loc_name as state_name, city.loc_name as city_name, cla.*
			FROM
			(
			SELECT a.phone as phone2, cc.firstname as cc_firstname, cc.lastname as cc_lastname, c.firstname, c.phone, c.email, c.website, c.type, c.title, c.id, c.ter, MIN(cl.loc_id) as loc_id, cpd.`time`, cpd.total,
					c.created, c.expires, a.password
			FROM classifieds c
			INNER JOIN account a on a.account_id = c.account_id
			LEFT JOIN account_cc cc on cc.cc_id = c.cc_id
			INNER JOIN classifieds_loc cl on cl.post_id = c.id
			LEFT JOIN classifieds_sponsor cs on cs.post_id = c.id
			LEFT JOIN 
				(
				select c2.post_id, c2.`time`, c2.total from classifieds_payment_done c2
				inner join (select post_id, max(id) as max_id from classifieds_payment_done group by post_id) c1 on c1.max_id = c2.id
				) cpd ON cpd.post_id = c.id
			WHERE c.deleted IS NULL and c.done = 1 and c.bp = 0 {$account_sql} {$done_sql} {$type_sql} {$paid_sql} {$currently_sponsor_sql} {$currently_recurring_sql} {$promocode_sql}
			GROUP BY c.phone, c.type, c.title, c.id
			) cla
			INNER JOIN location_location city on city.loc_id = cla.loc_id
            INNER JOIN location_location state on state.loc_id = city.loc_parent
            INNER JOIN location_location country on country.loc_id = city.country_id
            {$order}";

	$fp = fopen(_CMS_ABS_PATH.'/UserFiles/excel/erotic_services.csv', 'w');
	$fields = '"Id","Created","Expires/Expired","CC First name","CC Last name","Firstname","Title","Phone","Site","Link","Email","Personal website","Paid on","Paid","Country","State","City","Type","TER","Password"'."\n";
	fputs($fp, $fields);
	$res = $db->q($sql, $params);
	
	while($row=$db->r($res)) {
		$type = classifieds::getcatnamebytype($row["type"]);
		$url  = $url_array[$row["type"]];
		$phone = $row["phone"];
		if (empty($phone) && $row["phone2"])
			$phone = $row["phone2"];
		$phone = (strlen($phone)==10) ? preg_replace("/([0-9]{3})+([0-9]{3})+([0-9]{4})/", "$1-$2-$3", $phone) : $phone;
		if ($row["country_id"] == 16046) {
			$url = "http://adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else if ($row["country_id"] == 16047) {
			$url = "http://ca.adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else if ($row["country_id"] == 41973) {
			$url = "http://uk.adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else {
			$url = "http://{$row["country_dir"]}.adultsearch.com/{$row["city_dir"]}/{$url}/{$row["id"]}";
		}
		$fields = "\"{$row["id"]}\",\"".date("Y-m-d H:i:s T", $row["created"])."\",\"{$row["expires"]}\",\"{$row["cc_firstname"]}\",\"{$row["cc_lastname"]}\",\"{$row["firstname"]}\",\"{$row["title"]}\",\"{$phone}\",\"AS\",\"{$url}\",\"{$row["email"]}\",\"{$row["website"]}\",\"{$row["time"]}\",\"{$row["total"]}\",\"{$row["country_name"]}\",\"{$row["state_name"]}\",\"{$row["city_name"]}\",\"{$type}\",\"{$row["ter"]}\",\"{$row["password"]}\"\n";
		fputs($fp, $fields);
	}
	fclose($fp);

	system::moved("http://adultsearch.com/UserFiles/excel/erotic_services.csv");
	die;
}

function escorts_with_ter() {
	global $db;
	
	require(_CMS_ABS_PATH.'/_cms_files/classifieds/array.php');

//	$type = "AND (c.type = 1 OR c.type = 2)";
	$type = "";

	$sql = "SELECT country.loc_id as country_id, country.dir as country_dir, country.loc_name as country_name, 
                    city.loc_url, city.dir as city_dir, state.loc_name as state_name, city.loc_name as city_name, cla.*
			FROM
			(
			SELECT a.phone as phone2, cc.firstname, cc.lastname, c.phone, c.email, c.website, c.type, c.title, c.id, c.ter, MIN(cl.loc_id) as loc_id, cpd.`time`, cpd.total
			FROM classifieds c
			INNER JOIN account a on a.account_id = c.account_id
			LEFT JOIN account_cc cc on cc.cc_id = c.cc_id
			INNER JOIN classifieds_loc cl on cl.post_id = c.id
			LEFT JOIN 
				(
				select c2.post_id, c2.`time`, c2.total from classifieds_payment_done c2
				inner join (select post_id, max(id) as max_id from classifieds_payment_done group by post_id) c1 on c1.max_id = c2.id
				) cpd ON cpd.post_id = c.id
			WHERE c.deleted IS NULL and c.done = 1 and c.bp = 0 and c.ter > 0 {$type}
			GROUP BY c.phone, c.type, c.title, c.id
			) cla
			INNER JOIN location_location city on city.loc_id = cla.loc_id
            INNER JOIN location_location state on state.loc_id = city.loc_parent
            INNER JOIN location_location country on country.loc_id = city.country_id
            ORDER BY country_id, state_name, city_name";

	$fp = fopen(_CMS_ABS_PATH.'/UserFiles/excel/escorts_with_ter.csv', 'w');
	$fields = '"AS Link","TER Link"'."\n";
	fputs($fp, $fields);
	$res = $db->q($sql);
	
	while($row=$db->r($res)) {
		/*
		//$type = ($row["type"] == 5) ? "Agency" : "Independent";
		$type = classifieds::getcatnamebytype($row["type"]);
		$phone = $row["phone"];
		if (empty($phone) && $row["phone2"])
			$phone = $row["phone2"];
		$phone = (strlen($phone)==10) ? preg_replace("/([0-9]{3})+([0-9]{3})+([0-9]{4})/", "$1-$2-$3", $phone) : $phone;
		*/
		$url  = $url_array[$row["type"]];
		if ($row["country_id"] == 16046) {
			$url = "http://adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else if ($row["country_id"] == 16047) {
			$url = "http://ca.adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else if ($row["country_id"] == 41973) {
			$url = "http://uk.adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else {
			$url = "http://{$row["country_dir"]}.adultsearch.com/{$row["city_dir"]}/{$url}/{$row["id"]}";
		}
		$ter_url = "http://www.theeroticreview.com/reviews/show.asp?id={$row["ter"]}";
		$fields = "\"{$url}\",\"{$ter_url}\"\n";
		fputs($fp, $fields);
	}
	fclose($fp);

	system::moved("http://adultsearch.com/UserFiles/excel/escorts_with_ter.csv");
	die;
}

function downloadTSEroticServices() {
	global $db;
	
	require(_CMS_ABS_PATH.'/_cms_files/classifieds/array.php');

	$sql = "SELECT country.loc_id as country_id, country.dir as country_dir, country.loc_name as country_name, 
                    city.loc_url, city.dir as city_dir, state.loc_name as state_name, city.loc_name as city_name, cla.*
			FROM
			(
			SELECT a.phone as phone2, cc.firstname, cc.lastname, c.phone, c.email, c.website, c.type, c.title, c.id, MIN(cl.loc_id) as loc_id, cpd.`time`, cpd.total
			FROM classifieds c
			INNER JOIN account a on a.account_id = c.account_id
			LEFT JOIN account_cc cc on cc.cc_id = c.cc_id
			INNER JOIN classifieds_loc cl on cl.post_id = c.id
			LEFT JOIN 
				(
				select c2.post_id, c2.`time`, c2.total from classifieds_payment_done c2
				inner join (select post_id, max(id) as max_id from classifieds_payment_done group by post_id) c1 on c1.max_id = c2.id
				) cpd ON cpd.post_id = c.id
			WHERE c.deleted IS NULL and c.done = 1 and c.bp = 0 AND c.type = 2
			GROUP BY c.phone, c.type, c.title, c.id
			) cla
			INNER JOIN location_location city on city.loc_id = cla.loc_id
            INNER JOIN location_location state on state.loc_id = city.loc_parent
            INNER JOIN location_location country on country.loc_id = city.country_id
            ORDER BY country_id, state_name, city_name";

	$fp = fopen(_CMS_ABS_PATH.'/UserFiles/excel/ts_erotic_services.csv', 'w');
	$fields = '"First name","Last name","Phone","Site","Link","Email","Personal website","Paid on","Paid","Country","State","City"'."\n";
	fputs($fp, $fields);
	$res = $db->q($sql);
	while($row=$db->r($res)) {
		$url  = $url_array[$row["type"]];
		$phone = $row["phone"];
		if (empty($phone) && $row["phone2"])
			$phone = $row["phone2"];
		$phone = (strlen($phone)==10) ? preg_replace("/([0-9]{3})+([0-9]{3})+([0-9]{4})/", "$1-$2-$3", $phone) : $phone;
		if ($row["country_id"] == 16046) {
			$url = "http://adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else if ($row["country_id"] == 16047) {
			$url = "http://ca.adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else if ($row["country_id"] == 41973) {
			$url = "http://uk.adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else {
			$url = "http://{$row["country_dir"]}.adultsearch.com/{$row["city_dir"]}/{$url}/{$row["id"]}";
		}
		$fields = "\"{$row["firstname"]}\",\"{$row["lastname"]}\",\"{$phone}\",\"AS\",\"{$url}\",\"{$row["email"]}\",\"{$row["website"]}\",\"{$row["time"]}\",\"{$row["total"]}\",\"{$row["country_name"]}\",\"{$row["state_name"]}\",\"{$row["city_name"]}\"\n";
		fputs($fp, $fields);
	}
	fclose($fp);

	system::moved("http://adultsearch.com/UserFiles/excel/ts_erotic_services.csv");
	die;
}

function showTSEroticServices() {
	global $db;
	
	require(_CMS_ABS_PATH.'/_cms_files/classifieds/array.php');

	$sql = "SELECT country.loc_id as country_id, country.dir as country_dir, country.loc_name as country_name, 
                    city.loc_url, city.dir as city_dir, state.loc_name as state_name, city.loc_name as city_name, cla.*
			FROM
			(
			SELECT c.account_id, a.phone as phone2, cc.firstname, cc.lastname, c.phone, c.email, c.website, c.type, c.title, c.id, MIN(cl.loc_id) as loc_id, cpd.`time`, cpd.total
			FROM classifieds c
			INNER JOIN account a on a.account_id = c.account_id
			LEFT JOIN account_cc cc on cc.cc_id = c.cc_id
			INNER JOIN classifieds_loc cl on cl.post_id = c.id
			LEFT JOIN 
				(
				select c2.post_id, c2.`time`, c2.total from classifieds_payment_done c2
				inner join (select post_id, max(id) as max_id from classifieds_payment_done group by post_id) c1 on c1.max_id = c2.id
				) cpd ON cpd.post_id = c.id
			WHERE c.deleted IS NULL and c.done = 1 and c.bp = 0 AND c.type = 2
			GROUP BY c.phone, c.type, c.title, c.id
			) cla
			INNER JOIN location_location city on city.loc_id = cla.loc_id
            INNER JOIN location_location state on state.loc_id = city.loc_parent
            INNER JOIN location_location country on country.loc_id = city.country_id
            ORDER BY country_id, state_name, city_name";

	echo '<link href="/css/control.css" rel="stylesheet" type="text/css" />';
	echo "<h1>TS/TV escorts on AS</h1>\n";
	echo "<table class=\"control\">\n";
	echo "<thead><tr><th>First name</th><th>Last name</th><th>Phone</th><th>Link</th><th>Email</th><th>Personal website</th><th>Paid on</th><th>Paid</th><th>Country</th><th>State</th><th>City</th><th /></tr></thead>\n<tbody>";
	$res = $db->q($sql);
	while($row=$db->r($res)) {
		$url  = $url_array[$row["type"]];
		$phone = $row["phone"];
		if (empty($phone) && $row["phone2"])
			$phone = $row["phone2"];
		$phone = (strlen($phone)==10) ? preg_replace("/([0-9]{3})+([0-9]{3})+([0-9]{4})/", "$1-$2-$3", $phone) : $phone;
		if ($row["country_id"] == 16046) {
			$url = "http://adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else if ($row["country_id"] == 16047) {
			$url = "http://ca.adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else if ($row["country_id"] == 41973) {
			$url = "http://uk.adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else {
			$url = "http://{$row["country_dir"]}.adultsearch.com/{$row["city_dir"]}/{$url}/{$row["id"]}";
		}

		$acc = account::findOneById($row["account_id"]);
		$red = "";
		if ($acc && (strpos($acc->getNotes(), "Called") !== false))
			$red = " style=\"background-color: #FF6666;\"";
		echo "<tr id=\"row_{$row["id"]}\" {$red}><td>".htmlspecialchars($row["firstname"])."</td><td>".htmlspecialchars($row["lastname"])."</td><td>".htmlspecialchars($phone)."</td><td><a href=\"".htmlspecialchars($url)."\" target=\"_blank\">".htmlspecialchars($url)."</a></td><td>".htmlspecialchars($row["email"])."</td><td style=\"max-width: 200px; overflow: hidden;\">".htmlspecialchars($row["website"])."</td><td>".htmlspecialchars($row["time"])."</td><td>".htmlspecialchars($row["total"])."</td><td>".htmlspecialchars($row["country_name"])."</td><td>".htmlspecialchars($row["state_name"])."</td><td>".htmlspecialchars($row["city_name"])."</td><td>";
		echo "<button data-cid=\"{$row["id"]}\" class=\"called\" >Called</button>";
		if ($_SESSION["account"] == 3974) {
			if ($acc) {
				echo "Notes:<br />".htmlspecialchars($acc->getNotes());
			}
		}
		echo "</td></tr>\n";
	}
	echo "</tbody>\n</table>\n";

echo "
<script type=\"text/javascript\">
$(document).ready(function(){
	$('.called').each(function() {
		$(this).click(function() {
			var cid = $(this).attr('data-cid');
			alert('cid='+cid);
			$.get('', { action: 'called', cid: cid }, function(data) {
				if (data == 'ok') {
					$('#row_'+cid).css('background-color','#FF6666');
					$('#row_'+cid+' button').prop('disabled','disabled');
				}
				});
		});
	});
});
</script>
";

	return false;
}


if ($_REQUEST["action"] == "called") {
	$cid = intval($_REQUEST["cid"]);
	$clad = clad::findOneById($cid);
	if (!$clad)
		die("error");
	$acc= $clad->getAccount();
	if (!$acc)
		die("error");
	global $account;
	$acc->setNotes($acc->getNotes()."\nCalled at ".date("Y-m-d H:i")." by #".$account->getId()."\n");
	$acc->update();
	echo "ok";
	die;
}

switch(GetGetParam("action")) {
	case 'downloadCities':
		downloadCities($_GET['cat']);
		break;
	case 'downloadEroticServices':
		downloadEroticServices();
		break;
	case 'showTSEroticServices':
		showTSEroticServices();
		return;
		break;
	case 'downloadTSEroticServices':
		downloadTSEroticServices();
		break;
	case 'downloadBpEmails':
		downloadBpEmails();
		break;
	default:
		break;
}

?>

<h1>Classifieds export</h2>
<form>
<strong>All ads:</strong><br />
Filters:<br />
Account Id#: <input type="text" name="account_id" value="" />
<br />
Status:
<select name="done">
	<option value="" >-</option>
	<option value="-5" >Deleted</option>
	<option value="0" >Expired</option>
	<option value="1" >Live</option>
</select>
<br />
Paid ?
<select name="paid">
	<option value="" >-</option>
	<option value="paid" >Only paid ads</option>
	<option value="free" >Only free ads</option>
</select>
Currently sponsor ?
<select name="currently_sponsor">
	<option value="" >-</option>
	<option value="no" >No</option>
	<option value="yes" >Yes</option>
</select>
Currently recurring ?
<select name="currently_recurring">
	<option value="" >-</option>
	<option value="no" >No</option>
	<option value="yes" >Yes</option>
</select>
<br />
Promocode: <input type="text" name="promocode" value="" />
<br />
Order by: 
<select name="order_by">
<option value="id">Id</option>
<option value="created">Date/Time created</option>
<option value="country_state_city">Country, State, City</option>
</select>
Order way:
<select name="order_way">
<option value="asc">Ascending</option>
<option value="desc">Descending</option>
</select>
<br />
<input type="submit" name="submit" value="Download" />
</form>
<br /><br />
<form>
Escorts with TER:<br />
<input type="submit" name="submit" value="Escorts with TER" />
</form>
