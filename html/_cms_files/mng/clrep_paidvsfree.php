<?php
/**
 * This report gives number of ads in major cities which are free and which are paid
 */

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $db, $account, $smarty, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("access_admin_cl_stats"))
    return;

/*
CHECK SQL
SELECT a.post_id, a.payment_id
    FROM (
        SELECT cpd.post_id, MAX(cpd.id) as payment_id, COUNT(*) as cnt, MAX(promo_id) as max_promo_id, MIN(promo_id) as min_promo_id
        FROM classifieds_payment_done cpd
        WHERE cpd.upgradeonly = 0
        GROUP BY cpd.post_id
    ) a
    WHERE a.cnt > 1 and a.max_promo_id > 0 and min_promo_id = 0
    ORDER BY a.post_id DESC
*/

//get the last entry from classified_payment_done and if it has promocode, the ad is treated as free
$res = $db->q("
	SELECT CONCAT(l.loc_name, ',', l.s) as location, SUM(IF(cpd.promo_id > 0, 1, 0)) free, SUM(IF(cpd.promo_id = 0, 1, 0)) paid, COUNT(*) total
	FROM (
		SELECT post_id, loc_id, MAX(id) as id
		FROM classifieds_payment_done
		WHERE upgradeonly = 0
		GROUP BY post_id, loc_id
	) a
	INNER JOIN classifieds_payment_done cpd on cpd.id = a.id
	INNER JOIN classifieds c on c.id = a.post_id
	INNER JOIN location_location l on l.loc_id = a.loc_id
	WHERE c.done = 1 AND l.significant = 1
	GROUP BY a.loc_id
	ORDER BY COUNT(*) DESC
	");
$data = array();
$sum_free = $sum_paid = $sum_total = 0;
while($row = $db->r($res)) {
	$data[] = array(
		"location" => $row["location"],
		"total" => $row["total"],
		"free" => $row["free"],
		"paid" => $row["paid"],
		"paid_percent" => round($row["paid"] / ($row["total"] / 100)),
		);
	$sum_free += $row["free"];
	$sum_paid += $row["paid"];
	$sum_total += $row["total"];
}
$data[] = array(
	"location" => "Total",
	"total" => $sum_total,
	"free" => $sum_free,
	"paid" => $sum_paid,
	"paid_percent" => round($sum_paid / ($sum_total / 100)),
	);
$smarty->assign("data", $data);
$smarty->display(_CMS_ABS_PATH."/templates/mng/clrep_paidvsfree.tpl");

return;

/*
//export to CSV
if ($_REQUEST["export"] == "export") {
	$fp = fopen(_CMS_ABS_PATH.'/UserFiles/excel/cl_report.csv', 'w');
    $fields = "\"City\",\"Link\",\"New ads\",\"Total auto reposts\",\"Total city thumbnails\",\"Recurring\",\"Total made\"\n";
    fputs($fp, $fields);
    foreach ($report as $item) {
        $fields = "\"{$item["loc_name"]}\",\"{$item["link"]}\",\"{$item["total"]}\",\"{$item["ar"]}\",\"{$item["sc"]}\",\"{$item["recurring"]}\",\"{$item["made"]}\"\n";
        fputs($fp, $fields);
    }
    fclose($fp);
	$system = new system();
    $system->moved("http://adultsearch.com/UserFiles/excel/cl_report.csv");
	die;
}

$smarty->assign('report', $report);
$smarty->display(_CMS_ABS_PATH."/templates/mng/mng_cl_report.tpl");
*/

?>
