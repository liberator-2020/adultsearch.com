<?php
/**
 * This report gives number of ads in major cities which are free and which are paid
 */

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $db, $account, $smarty, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("access_admin_cl_stats"))
	return;


//-------
//filters
$filters_str = "";
$since_sql = "";
$since = $_REQUEST["since"];
switch ($since) {
	case "beginning_year": $since_sql = " AND cpd.`time` > '".date("Y")."-01-01 00:00:00' "; $filters_str .= ", Since beginning of this year"; break;
	default: $since = ""; break;
}
$smarty->assign("since", $since);

$promocode_created_by_sql = "";
$promocode_created_by = preg_replace('/[^0-9a-zA-Z_]/', '', $_REQUEST["promocode_created_by"]);
if ($promocode_created_by) {
	$promocode_created_by_sql = " AND cpa.username = '{$promocode_created_by}' ";
	$filters_str .= ", promocode created by {$promocode_created_by}";
}
$smarty->assign("promocode_created_by", $promocode_created_by);

$promocode_sql = "";
$promocode = preg_replace('/[^a-zA-Z0-9_]/', '', $_REQUEST["promocode"]);
if ($promocode) {
	$promocode_sql = " AND cp.code = '{$promocode}' ";
	$filters_str .= ", promocode {$promocode}";
}
$smarty->assign("promocode", $promocode);

$select = "SELECT * ";
$order = "";

$group_by = $_REQUEST["group_by"];
switch ($group_by) {
	case "account": break;
	case "promocode": break;
	default: $group_by = ""; break;
}
$smarty->assign("group_by", $group_by);
if ($group_by == "account") {
	$group = "GROUP BY sub.posted_by";
	$select = "SELECT stamp, clad_id, promocode, promocode_created_by, posted_by, SUM(paid) as paid, SUM(should_have_paid) as should_have_paid, SUM(discount) as discount, COUNT(*) as cnt, COUNT(DISTINCT clad_id) as cnt_ads, COUNT(DISTINCT id) as post_cnt ";
	$order = " ORDER BY 8 DESC";
	$filters_str .= ", group by account";
} else if ($group_by == "promocode") {
	$group = "GROUP BY sub.promocode";
	$select = "SELECT stamp, clad_id, promocode, promocode_created_by, posted_by, SUM(paid) as paid, SUM(should_have_paid) as should_have_paid, SUM(discount) as discount, COUNT(*) as cnt, COUNT(DISTINCT clad_id) as cnt_ads, COUNT(DISTINCT id) as post_cnt, COUNT(DISTINCT posted_by) as account_cnt";
	$order = " ORDER BY 8 DESC";
	$filters_str .= ", group by promocode";
}

//construct SQL
$sql = "
{$select}
FROM (
	SELECT *
		, ROUND(IF(country_id IN (16046, 16047, 41973), 9.99, 9.99) + side_total + sponsor_total + IF(autorenew,49.99,0), 2) as should_have_paid
		, ROUND(IF(country_id IN (16046, 16047, 41973), 9.99, 9.99) + side_total + sponsor_total + IF(autorenew,49.99,0) - paid, 2) as discount
	FROM (
		SELECT cpd.id, cpd.`time` as stamp, cpd.post_id as clad_id, cp.code as promocode, cpa.username as promocode_created_by, a.email as posted_by
			, cpd.total as paid, cpd.side_total, cpd.sponsor_total, cpd.autorenew
			, (SELECT l.country_id FROM classifieds_loc cl INNER JOIN location_location l on l.loc_id = cl.loc_id WHERE cl.post_id = cpd.post_id LIMIT 1) as country_id
		FROM classifieds_payment_done cpd
		LEFT JOIN account a ON a.account_id = cpd.account_id
		LEFT JOIN classifieds_promocodes cp ON cp.id = cpd.promo_id
		LEFT JOIN account cpa ON cpa.account_id = cp.created_by
		WHERE cpd.promo_id > 0 and cpd.total = 0 {$since_sql} {$promocode_sql} {$promocode_created_by_sql}
		ORDER BY cpd.id DESC
	) sub2
) sub
{$group}
{$order}
	";
//_d("sql = '{$sql}'");

$res = $db->q($sql);

$total = $total_discount = 0;
while($row = $db->r($res)) {
	$line = [
		"datetime" => $row["stamp"],
		"promocode" => $row["promocode"],
		"promocode_created_by" => $row["promocode_created_by"],
		"paid" => $row["paid"],
		"should_have_paid" => $row["should_have_paid"],
		"discount" => $row["discount"],
	];

	if (!$group_by || $row["cnt_ads"] == 1)	
		$line["clad_id"] = "#".$row["clad_id"];
	else
		$line["clad_id"] = "{$row["cnt_ads"]} different ads, e.g. #{$row["clad_id"]}";

	if ($group_by != "promocode" || $row["account_cnt"] == 1)	
		$line["posted_by"] = $row["posted_by"];
	else
		$line["posted_by"] = "{$row["account_cnt"]} different accounts, e.g. {$row["posted_by"]}";

	if ($group_by == "account" || $group_by == "promocode")
		$line["post_cnt"] = $row["post_cnt"];

	$report[] = $line;

	if ($group_by == "account" || $group_by == "promocode")
		$total += $row["cnt"];
	else
		$total += 1;

	$total_discount += $row["discount"];
}

$total_discount = number_format($total_discount, 2, ".", ",");
$average_discount = number_format($total_discount / $total, 2, ".", ",");

$smarty->assign("total", $total);
$smarty->assign("total_discount", $total_discount);
$smarty->assign("average_discount", $average_discount);

//export to XLS
if ($_REQUEST["export"] == "export") {
	require_once(_CMS_ABS_PATH."/inc/classes/excel/Classes/PHPExcel.php");
	require_once(_CMS_ABS_PATH."/inc/classes/excel/Classes/PHPExcel/Writer/Excel5.php");
	$o = new PHPExcel();
	$o->setActiveSheetIndex(0);
	$a = $o->getActiveSheet();
	$a->setTitle('Promocodes Usage List');
	$a->getColumnDimension('A')->setWidth(18);
	if ($group_by == "account")
		$a->getColumnDimension('B')->setWidth(24);
	else
		$a->getColumnDimension('B')->setWidth(10);
	$a->getColumnDimension('C')->setWidth(10);
	$a->getColumnDimension('E')->setWidth(35);
	$title = "Classifieds promocodes report{$filters_str}";
	$a->SetCellValue("A1", $title);
	$a->SetCellValue("A2", "Date/Time");
	$a->SetCellValue("B2", "Ad #Id");
	$a->SetCellValue("C2", "Promocode");
	$a->SetCellValue("D2", "Promocode created by");
	$a->SetCellValue("E2", "Posted by");
	$a->SetCellValue("F2", "Price paid");
	$a->SetCellValue("G2", "Price without promo");
	$a->SetCellValue("H2", "Discount");
	if ($group_by == "account" || $group_by == "promocode")
		$a->SetCellValue("I2", "Purchase count");
	$i = 3;
	foreach($report as $line) {
		$a->SetCellValue("A{$i}", $line["datetime"]);
		$a->SetCellValue("B{$i}", $line["clad_id"]);
		$a->SetCellValue("C{$i}", $line["promocode"]);
		$a->SetCellValue("D{$i}", $line["promocode_created_by"]);
		$a->SetCellValue("E{$i}", $line["posted_by"]);
		$a->SetCellValue("F{$i}", $line["paid"]);
		$a->SetCellValue("G{$i}", $line["should_have_paid"]);
		$a->SetCellValue("H{$i}", $line["discount"]);
		if ($group_by == "account" || $group_by == "promocode")
			$a->SetCellValue("I{$i}", $line["post_cnt"]);
		$i++;
	}
	$a->SetCellValue("A{$i}", "Total purchases");
	$a->SetCellValue("B{$i}", $total);
	$i++;
	$a->SetCellValue("A{$i}", "Total discount");
	$a->SetCellValue("B{$i}", $total_discount);
	$i++;
	$a->SetCellValue("A{$i}", "Average discount");
	$a->SetCellValue("B{$i}", $average_discount);
	$i++;
	$w = new PHPExcel_Writer_Excel5($o);
	$file = "/UserFiles/excel/cl_rep_promocodes_".time().".xls";
	$w->save(_CMS_ABS_PATH.$file);
	system::moved($file);
}

//get promocode users
$res = $db->q("
	SELECT DISTINCT a.username 
	FROM account a
	INNER JOIN classifieds_promocodes cp on cp.created_by = a.account_id
	");
$promocode_created_by_options = [];
while($row = $db->r($res)) {
	$promocode_created_by_options[$row["username"]] = $row["username"];
}
$smarty->assign('promocode_created_by_options', $promocode_created_by_options);

$smarty->assign('report', $report);
$smarty->display(_CMS_ABS_PATH."/templates/mng/mng_cl_report_promocodes.tpl");

?>
