<?php

global $ipp, $ordercol, $orderway, $page;

$ipp = 30;
$ordercol = $_REQUEST["order"];
$orderway = $_REQUEST["orderway"];
$page = ($_REQUEST["page"] == "one") ? "one" : intval($_REQUEST["page"]);
if (is_numeric($page) && $page == 0)
	$page = 1;

function getFilters() {
	global $where_cols;
	$filters = array();
	foreach ($_REQUEST as $key => $val) {
		if (array_key_exists($key, $where_cols) && ($val !== ""))
			$filters[$key] = GetRequestParamStripped($key);
	}
	return $filters;
}

function getActionIds() {
	$ids = array();
	if (is_array($_REQUEST["action_id"])) {
		//mass action
		foreach ($_REQUEST["action_id"] as $id) {
			$id = intval($id);
			if ($id == 0)
				continue;
			$ids[] = $id;
		}
	} else {
		//single action
		$id = intval($_REQUEST["action_id"]);
		if ($id)
			$ids[] = $id;
	}
	return $ids;
}

function getActionIdsQueryString() {
	$ids = getActionIds();
	$query_string = "";	
	foreach ($ids as $id) {
		$query_string .= (empty($query_string)) ? "" : "&";
		$query_string .= "action_id[]={$id}";
	}
	return $query_string;
}

function isFilterSet($filter_name) {
	global $filters;

	$found = false;
	foreach ($filters as $key => $value) {
		if ($key == $filter_name) {
			$found = $value;
			break;
		}
	}

	return $found;
}

function getFilterFormFields($with_page = true) {
	global $filters, $page, $ordercol, $orderway;

	$html = "";

	foreach ($filters as $key => $value) {
		$html .= "<input type=\"hidden\" name=\"{$key}\" value=\"{$value}\" />";
	}

	if ($with_page && isset($page) && $page != "") {
		$html .= "<input type=\"hidden\" name=\"page\" value=\"{$page}\" />";
	}

	if (isset($ordercol) && $ordercol != "") {
		$html .= "<input type=\"hidden\" name=\"order\" value=\"{$ordercol}\" />";
		if (isset($orderway))
			$html .= "<input type=\"hidden\" name=\"orderway\" value=\"{$orderway}\" />";
	}

	return $html;
}

function getConfirmActionHiddenInputs($action_name, $ids) {
	$html = "";
	$html .= "<input type=\"hidden\" name=\"action\" value=\"{$action_name}\" />";
	$html .= "<input type=\"hidden\" name=\"live\" value=\"1\" />";
	foreach ($ids as $id) {
		$html .= "<input type=\"hidden\" name=\"action_id[]\" value=\"{$id}\" />";
	}
	return $html;
}

function getConfirmActionForm($action_name, $ids, $custom_form_html = "") {
	$html = "";
	$html .= "<form>";
	$html .= getConfirmActionHiddenInputs($action_name, $ids);
	$html .= $custom_form_html;
	$html .= "<input type=\"submit\" name=\"submit\" value=\"Yes\" />";
	$html .= getFilterFormFields();
	$html .= "</form>";
	return $html;
}

function getFilterLink($with_page = true, $with_order = true) {
	global $url_last_piece, $filters, $page, $ordercol, $orderway;

	$pars = "";
	$empty = false;

	if (strpos($url_last_piece, "?") === false){
		$empty = true;
	}

	foreach ($filters as $key => $value) {
		if ($empty && empty($pars))
			$pars = "?{$key}={$value}";
		else
			$pars .= "&{$key}={$value}";
	}

	if ($with_page && isset($page) && $page != "") {
		$pars .= ($empty && empty($pars)) ? "?" : "&";
		$pars .= "page={$page}";
	}

	if ($with_order && isset($ordercol) && $ordercol != "") {
		$pars .= ($empty && empty($pars)) ? "?" : "&";
		$pars .= "order=$ordercol";
		if (isset($orderway))
		 	$pars .= "&orderway=$orderway";
	}

	return $url_last_piece.$pars;
}

function actionBack($path = NULL) {
	echo "<br /><strong>Please wait...</strong><br />";
	if ($path == NULL)
		$path = "/mng/".getFilterLink();
	system::go($path);
	return false;
}

function actionError($error_msg, $path = NULL) {
	flash::add(flash::MSG_ERROR, $error_msg);
	echo "<br /><strong>Please wait...</strong><br />";
	if ($path == NULL)
		$path = "/mng/".getFilterLink();
	system::go($path);
	return false;
}

function actionWarning($error_msg, $path = NULL) {
	flash::add(flash::MSG_WARNING, $error_msg);
	echo "<br /><strong>Please wait...</strong><br />";
	if ($path == NULL)
		$path = "/mng/".getFilterLink();
	system::go($path);
	return false;
}

function actionSuccess($msg, $path = NULL) {
	flash::add(flash::MSG_SUCCESS, $msg);
	echo "<br /><strong>Please wait...</strong><br />";
	if ($path == NULL)
		$path = "/mng/".getFilterLink();
	system::go($path);
	return false;
}

function getOrderLink($title, $colname) {
	global $ordercol, $orderway;

	$link = getFilterLink(true, false);

	if (strpos($link, "?") === false)
		$link_par = "{$link}?";
	else
		$link_par = "{$link}&";

	if ($colname != $ordercol) {
		return "<a href=\"{$link_par}order=$colname&orderway=DESC\">$title</a>";
	}
	switch ($orderway) {
		case 'DESC':
			return "<a href=\"{$link_par}order=$colname&orderway=ASC\" class=\"selected\">$title</a>";
			break;
		case 'ASC':
			return "<a href=\"{$link}\" class=\"selected\">$title</a>";
			break;
	}
	return "<a href=\"{$link}\" class=\"selected\">$title</a>";
}

function getPageLink($page) {
	global $ordercol, $orderway, $url_last_piece, $filters;

	$link = getFilterLink(false, true);

	if (strpos($link, "?") === false)
		$link .= "?";
	else
		$link .= "&";

	$link .= "page={$page}";

	return $link;
}

function getActionLink($action_name, $action_id = NULL) {
	$link = getFilterLink();

	if (strpos($link, "?") === false)
		$link .= "?";
	else
		$link .= "&";

	$link .= "action={$action_name}";
	if ($action_id)
		$link .= "&action_id={$action_id}";

	return $link;
}

function getFilterField($colname, $title_arr) {
	global $filters;

	//type of filter field
	$type = "";
	if (is_array($title_arr) && array_key_exists("type", $title_arr))
		$type = $title_arr["type"];
	if ($type != "text" && $type != "radio" && $type != "select")
		$type = "text";

	//title
	if (!is_array($title_arr))
		$title = $title_arr;
	else
		$title = $title_arr["title"];

	$value = "";
	if (isset($filters[$colname]))
		$value = trim($filters[$colname]);

	$html = "";

	$classes = "";
	if ($value != "")
		$classes = " class=\"filter_set\"";

	switch ($type) {
		case "radio":
			if (!array_key_exists("options", $title_arr))
				return "";
			$html .= "<span style=\"border: 1px solid #ccc; display: inline-block;\">{$title}:";
			foreach ($title_arr["options"] as $val => $opt_title) {
				$html .= "<input type=\"radio\" name=\"{$colname}\" value=\"{$val}\" ";
				if ((string)$val === (string)$value)
					$html .= "checked=\"checked\" ";
				$html .= ">{$opt_title}";
			}
			$html .= "</span>";
			return $html;
			break;
		case "select":
			if (!array_key_exists("options", $title_arr))
				return "";
			$html .= "{$title}:<select name=\"{$colname}\"{$classes}>";
			foreach ($title_arr["options"] as $val => $opt_title) {
				$html .= "<option value=\"{$val}\" ";
				if ((string)$val === (string)$value)
					$html .= "selected=\"selected\" ";
				$html .= ">{$opt_title}</option>";
			}
			$html .= "</select>";
			return $html;
			break;
		case "text":
			$size = (array_key_exists("size", $title_arr) && intval($title_arr["size"])) ? " size=\"".intval($title_arr["size"])."\"" : "";
			return " {$title}:<input type=\"text\" name=\"{$colname}\" value=\"".htmlspecialchars($value)."\"{$size}{$classes} />";
			break;
	}
	return "";
}

function getWhere(&$params = [], $only_additional = false) {
	global $filters, $where_cols;

	$where = "";
	foreach ($filters as $key => $val) {

		if (array_key_exists("having", $where_cols[$key]))
			continue;

		if (!$only_additional && empty($where))
			$where .= " WHERE ";
		else
			$where .= " AND ";

		if (is_array($where_cols[$key]) && array_key_exists("where", $where_cols[$key]) && is_array($where_cols[$key]["where"])) {
			if (array_key_exists($val, $where_cols[$key]["where"]))
				$where .= $where_cols[$key]["where"][$val];
			else
				$where .= " 1=1 ";
			continue;
		}

		if (is_array($where_cols[$key]) && array_key_exists("where", $where_cols[$key]))
			$filter_col = $where_cols[$key]["where"];
		else
			$filter_col = $key;

		$val = trim($val);

		if (is_array($where_cols[$key]) && array_key_exists("match", $where_cols[$key]) && $where_cols[$key]["match"] == "preg_replace") {
			$pattern = $where_cols[$key]["pattern"];
			$replacement = $where_cols[$key]["replacement"];
			$val = preg_replace($pattern, $replacement, $val);
		}

		if (is_array($where_cols[$key]) && array_key_exists("where_condition", $where_cols[$key]) && array_key_exists("where_params", $where_cols[$key])) {
			$where .= " {$where_cols[$key]["where_condition"]} ";
			foreach ($where_cols[$key]["where_params"] as $p) {
				$params[] = str_replace("{val}", $val, $p);
			}
			continue;
		}

		if (is_array($where_cols[$key]) && array_key_exists("match", $where_cols[$key]) && $where_cols[$key]["match"] == "exact") {
			$where .= " $filter_col = ? ";
			$params[] = $val;
			continue;
		}

		if (is_array($where_cols[$key]) && array_key_exists("position", $where_cols[$key])) {
			$position = $where_cols[$key]["position"];
			if ($position == "first")
				$where_val = "{$val}%";
			else if ($position == "last")
				$where_val = "%{$val}";
		} else {
			if (strpos($val, "%") === false)
				$where_val = "%{$val}%";
			else
				$where_val = $val;
		}

		$where .= " $filter_col like ? ";
		$params[] = $where_val;
	}

	return $where;
}

function getHaving($only_additional = false) {
	global $filters, $where_cols;

	$having = "";
	foreach ($filters as $key => $val) {

		if (!array_key_exists("having", $where_cols[$key]))
			continue;

		if (!$only_additional && empty($having))
			$having .= " HAVING ";
		else
			$having .= " AND ";

		if (is_array($where_cols[$key]) && array_key_exists("having", $where_cols[$key]) && is_array($where_cols[$key]["having"])) {
			if (array_key_exists($val, $where_cols[$key]["having"]))
				$having .= $where_cols[$key]["having"][$val];
			else
				$having .= " 1=1 ";
			continue;
		}
	}

	return $having;
}

function getOrder() {
	global $ordercol, $orderway;

	$order = "";
	if (!empty($ordercol)) {
		$order = " ORDER BY {$ordercol}";

		if (!empty($order)) {
			$order2 = " DESC";
			if (!empty($orderway)) {
				switch ($orderway) {
				case 'ASC':
					$order2 = " ASC";
					break;
				}
			}
			$order = $order.$order2;
		}
	}

	return $order;
}

function getLimit() {
	global $page, $ipp;

	if (!is_numeric($page) && $page == "one") {
		$limit = "";
	} else {
			
		$start = ($page-1)*$ipp;
		$limit = "LIMIT $start, $ipp";
	}
	return $limit;
}

function displayFilterForm() {
	global $url_last_piece, $filters, $where_cols;

	if (empty($where_cols))
		return;

	echo "<form method=\"get\" action=\"{$url_last_piece}\" style=\"display: inline;\"/>";
	foreach ($where_cols as $col => $title) {
		echo getFilterField($col, $title);
	}
	echo "<input type=\"submit\" name=\"submit\" value=\"Submit\" />";
	if (strpos($url_last_piece, "?") > -1){
		$strpars = substr($url_last_piece, strpos($url_last_piece, "?") + 1);
		$pars = split('=', $strpars);
		if(count($pars) == 2){
			$hidden_field = "<input type=\"hidden\" name=\"{$pars[0]}\" value=\"{$pars[1]}\" />";
		}
	}
	echo $hidden_field;
	echo "</form>";
	echo "<form method=\"get\" action=\"{$url_last_piece}\" style=\"display: inline;\"/>".$hidden_field."<input type=\"submit\" name=\"submit\" value=\"Reset\" /></form><br />";
}

function getPager($total) {
	global $page, $ipp;

	if ($page == "one") {
		return "Total: {$total} items. <a href=\"".getPageLink(1)."\">Paged results</a>";
	}

	if ($page == 1 && ceil($total/$ipp) == 1)
		return "Total: {$total} items.";

	$pager = "";
	if ($page > 1) {
		$pager .= "<a href=\"".getPageLink(1)."\">First</a> ";
		$pager .= "<a href=\"".getPageLink($page-1)."\">Prev</a> ";
	}
	$pager .= " Page <form method=\"get\" style=\"display: inline;\"><input type=\"text\" size=\"2\" name=\"page\" value=\"{$page}\" />";
	$pager .= getFilterFormFields(false);
	$pager .= "</form> of ".ceil($total/$ipp)." ";
	if ($total > ($page*$ipp)) {
		$pager .= "<a href=\"".getPageLink($page+1)."\">Next</a> ";
		$pager .= "<a href=\"".getPageLink(ceil($total/$ipp))."\">Last</a> ";
	}

	if ($total > $ipp && ceil($total/$ipp) < 10)
		$pager .= "&middot; <a href=\"".getPageLink("one")."\">One page</a> ";

	$pager .= " (total={$total})";

	return $pager;
}

global $smarty;
$smarty->assign("noshare", true);
$smarty->assign("nobanner", true);

?>
