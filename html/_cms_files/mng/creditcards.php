<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("access_admin_sales"))
	return;

global $url_last_piece, $where_cols, $filters;

//configuration
$ipp = 30;
$url_last_piece = "creditcards";
$where_cols = array(
	"id" => array("title" => "CC.Id", "where" => "c.cc_id", "match" => "exact", "size" => 5),
	"account_id" => array("title" => "Acc.ID", "where" => "c.account_id", "match" => "exact", "size" => 5),
	"email" => array("title" => "Email", "where" => "a.email", "size" => 14),
	"password" => array("title" => "Password", "where" => "a.password", "size" => 12),
	"cc_first4" => array("title" => "CC#: First 4 digits", "where" => "c.cc", "position" => "first", "size" => 5),
	"cc_last4" => array("title" => "CC#: Last 4 digits", "where" => "c.cc", "position" => "last", "size" => 5),
	"firstname" => array("title" => "Firstname", "where" => "c.firstname", "size" => 12),
	"lastname" => array("title" => "Lastname", "where" => "c.lastname", "size" => 14),
	"address" => array("title" => "Address", "where" => "c.address", "size" => 12),
	"city" => array("title" => "City", "where" => "c.city", "size" => 12),
	"state" => array("title" => "State", "where" => "c.state", "size" => 12),
	"shared"   => array(
		"title" => "Shared",
		"where" => "c.shared",
		"type" => "select",
		"options" => array(
			"" => "-",
			"0" => "No",
			"1" => "Yes",
			),
		),
);

function ban() {
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid credit card id !");

	$cc = creditcard::findOnebyId($action_id);
	if ($cc == false) {
		return actionError("Error: Cant find credit card of id={$action_id}) !");
	}
	
	if (creditcard::isBannedAux($cc->getCc(), $cc->getCvc2(), $cc->getZipcode()))
		return actionWarning("Credit card ".$cc->getCc()." was already banned before!");


	if (!isset($_REQUEST["submit"])) {
		echo "<h2>Ban credit card {$cc->getCc()}</h2>";
		echo "<form method=\"post\" action=\"\">";
		echo "<table>";
		echo "<tr><th>Reason:</th><td><input type=\"text\" name=\"u_reason\" value=\"\" size=\"20\" /></td></tr>\n";
		echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
		echo "</table>";
		echo "<input type=\"hidden\" name=\"action_id\" value=\"{$action_id}\" />";
		echo getFilterFormFields();
		echo "</form>";
		return true;
	}

	//were going live
	$reason = $_REQUEST["u_reason"];

	if ($cc->ban($reason)) {
		
		//also add reason of ban to account notes
		$acc = $cc->getAccount();
		if (!$acc) {
			flash::add(flash::MSG_WARNING, "Coudln't find account for banned credit card - not added reason to account notes. Please contact administrator.");
		} else {
			$notes = $acc->getNotes();
			$notes .= "\nBanned credit card #{$cc->getCc()}, reason: {$reason}";
			$acc->setNotes($notes);
			$ret = $acc->update();
			if (!$ret)
				flash::add(flash::MSG_WARNING, "Can't update account notes with reason of blocking the cc. Please contact administrator.");
		}

		return actionSuccess("Credit card ".$cc->getCc()." is banned.");
	} else {
		return actionError("Error while banning credit card ".$cc->getCc()." - please contact administrator!");
	}

	return true;
}

function unban() {
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid credit card id !");

	$cc = creditcard::findOnebyId($action_id);
	if ($cc == false) {
		return actionError("Error: Cant find credit card of id={$action_id}) !");
	}
	
	if (!creditcard::isBannedAux($cc->getCc(), $cc->getCvc2(), $cc->getZipcode()))
		return actionWarning("Credit card ".$cc->getCc()." is not banned!");


	if (!isset($_REQUEST["submit"])) {
		echo "<h2>Do you want to unban credit card {$cc->getCc()} ?</h2>";
		echo "<form method=\"post\" action=\"\">";
		echo "<table>";
		echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Yes\" /></td></tr>\n";
		echo "</table>";
		echo "<input type=\"hidden\" name=\"action_id\" value=\"{$action_id}\" />";
		echo getFilterFormFields();
		echo "</form>";
		return true;
	}

	//were going live
	if ($cc->unban()) {
		return actionSuccess("Credit card ".$cc->getCc()." is unbanned.");
	} else {
		return actionError("Error while unbanning credit card ".$cc->getCc()." - please contact administrator!");
	}

	return true;
}

function edit() {
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid creditcard id !");

	$cc = creditcard::findOnebyId($action_id);
	if ($cc == false)
		return actionError("Cant find creditcard for id={$action_id} !");

	if (isset($_REQUEST["submit"])) {
		if (isset($_REQUEST["u_shared"]) && (intval($_REQUEST["u_shared"]) == 0 || intval($_REQUEST["u_shared"]) == 1)) {
			$cc->setShared(intval($_REQUEST["u_shared"]));
			$ret = $cc->update();
			if ($ret)
				return actionSuccess("You have successfully updated creditcard id #{$action_id}.");
			else
				return actionError("Error while updating creditcard id #{$action_id}.");
		}
	}

	$select_html = "<select name=\"u_shared\">";
	if ($cc->getShared())
		$select_html .= "<option value=\"0\">No</option><option value=\"1\" selected=\"selected\">Yes</option>";
	else
		$select_html .= "<option value=\"0\" selected=\"selected\">No</option><option value=\"1\">Yes</option>";
	$select_html .= "</select>\n";

	echo "<h2>Edit creditcard #{$cc->getId()}: {$cc->getCc()}</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Creditcard ID</th><td>{$cc->getId()}</td></tr>\n";
	echo "<tr><th>Creditcard #</th><td>{$cc->getCc()}</td></tr>\n";
	echo "<tr><th>Shared</th><td>{$select_html}</td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
	echo "</table>";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$action_id}\" />";
	echo getFilterFormFields();
	echo "</form>";
	return true;
}

function delete() {
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid credit card id !");

	$cc = creditcard::findOnebyId($action_id);
	if ($cc == false) {
		return actionError("Error: Cant find credit card of id={$action_id}) !");
	}
	
	if (!isset($_REQUEST["submit"])) {
		echo "<h2>Delete credit card {$cc->getCc()} ?</h2>";
		echo "<form method=\"post\" action=\"\">";
		echo "<table>";
		echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
		echo "</table>";
		echo "<input type=\"hidden\" name=\"action_id\" value=\"{$action_id}\" />";
		echo getFilterFormFields();
		echo "</form>";
		return true;
	}

	//were going live
	if ($cc->remove()) {
		return actionSuccess("Credit card ".$cc->getCc()." was deleted.");
	} else {
		return actionError("Error while deleting credit card ".$cc->getCc()." - please contact administrator!");
	}

	return true;
}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'ban':
		$ret = ban();
		if (!$ret)
			return;
		break;
	case 'unban':
		$ret = unban();
		if (!$ret)
			return;
		break;
	case 'edit':
		$ret = edit();
		if (!$ret)
			return;
		break;
	case 'delete':
		$ret = delete();
		if (!$ret)
			return;
		break;
}

$params = [];
$where = getWhere($params);
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
	$order = "ORDER BY c.cc_id DESC";

//query db
$sql = "SELECT count(*) as total
		FROM account_cc c
		LEFT JOIN account a on c.account_id = a.account_id
		$where";
$res = $db->q($sql, $params);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT c.cc_id, c.account_id, c.cc, c.cvc2, c.expmonth, c.expyear, c.firstname, c.lastname, c.address, c.city, c.state, c.zipcode, c.shared, c.deleted
			, a.email, a.password, a.banned, a.ban_reason
		FROM account_cc c
		LEFT JOIN account a on c.account_id = a.account_id 
		$where
		$order 
		$limit
		";
$res = $db->q($sql, $params);
if ($db->numrows($res) == 0) {
	echo "No credit cards.";
	return;
}

//pager
$pager = getPager($total);

//output
echo "<h2>Credit cards</h2>\n";
displayFilterForm();
echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr>";
echo "<th>".getOrderLink("Id", "cc_id")."</th>";
echo "<th>".getOrderLink("Acc.ID", "c.account_id")."</th>";
echo "<th>".getOrderLink("Email", "a.email")."</th>";
echo "<th>".getOrderLink("Password", "a.password")."</th>";
echo "<th>".getOrderLink("CC Number", "cc")."</th>";
echo "<th>Exp</th>";
echo "<th>".getOrderLink("Firstname", "c.firstname")."</th>";
echo "<th>".getOrderLink("Lastname", "lastname")."</th>";
echo "<th>".getOrderLink("Address", "c.address")."</th>";
echo "<th>".getOrderLink("City", "c.city")."</th>";
echo "<th>".getOrderLink("State", "c.state")."</th>";
echo "<th>".getOrderLink("Zip", "c.zipcode")."</th>";
echo "<th>Banned</th>";
echo "<th/></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	$deleted = ($rox["deleted"]) ? true : false;

	if ($deleted)
		echo "<tr style=\"background-color: #FFAAAA;\">";
	else
		echo "<tr>";

	echo "<td>{$rox['cc_id']}</td>";

	if ($rox["account_id"]) {
		echo "<td><a href=\"/mng/accounts?account_id={$rox["account_id"]}\">{$rox["account_id"]}</a>";
		if ($rox["banned"]) {
	        echo "<br /><span style=\"color: red; font-weight: bold;\">Banned</span>";
			if ($rox["ban_reason"] == "spammer")
				echo "<br /><span style=\"color: red;\">(spammer)</span>";
			elseif ($rox["ban_reason"] == "chargeback")
				echo "<br /><span style=\"color: red;\">(chargeback)</span>";
		} else if ($deleted) {
    	    echo "<br /><span style=\"color: darkred;\">Deleted</span>";
		}
		echo "</td>";
	} else {
		echo "<td>-</td>";
	}

	echo "<td>{$rox["email"]}</td>";
	echo "<td>{$rox["password"]}</td>";

	$ccno = creditcard::ccnoFirstLast($rox["cc"]);
	echo "<td>{$ccno}&nbsp;<a href=\"#\" class=\"reveal\" title=\"Reveal this CC number\" data-cc-id=\"{$rox['cc_id']}\"><img src=\"/images/control/crosshair_16.png\"/></a></td>";

	echo "<td>{$rox['expmonth']}/{$rox['expyear']}</td>";
	echo "<td>{$rox["firstname"]}</td>";
	echo "<td>{$rox["lastname"]}</td>";
	echo "<td>{$rox["address"]}</td>";
	echo "<td>{$rox["city"]}</td>";
	echo "<td>{$rox["state"]}</td>";
	echo "<td>{$rox["zipcode"]}</td>";

	$banned = creditcard::isBannedAux($rox["cc"], $rox["cvc2"], $rox["zipcode"]);
	if ($banned) {
		echo "<td>YES</td>";
	} else {
		echo "<td>-</td>";
	}

	$links = $deleted_links = "<a href=\"/mng/sales?cc_id={$rox["cc_id"]}\">sales</a>";
	if (!$banned) {
		$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("ban", $rox['cc_id'])."\">ban</a>";
		$deleted_links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("ban", $rox['cc_id'])."\">ban</a>";
	} else
		$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("unban", $rox['cc_id'])."\">unban</a>";
	
	$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("edit", $rox['cc_id'])."\">edit</a>";
	
	if ($_SESSION["account"] == 3974)
		$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("delete", $rox['cc_id'])."\">delete</a>";

	if (!$deleted) {
		echo "<td>{$links}</td>";
	} else {
		//if CC is deleted, show up only ban link
		echo "<td>{$deleted_links}</td>";
	}

	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;

?>
SS Private key:<br /><textarea id="ss_private_key" name="ss_private_key"></textarea>
<script type="text/javascript">
function cardno_reveal(elem) {
	var ss_private_key = $('#ss_private_key').val();
	var cc_id = $(elem).attr('data-cc-id');
	$.post('/_ajax/cardno_reveal', { cc_id: cc_id, ss_private_key: ss_private_key }, function(resp) {
		if (resp == '')
			resp = '<span style="color: red;">Error</span>';
		$(elem).parent().append('<br />'+resp);
		$(elem).remove();
	});
}
$(document).ready(function() {
	$('.reveal').each(function(ind,obj) {
		$(obj).click(function(evt) {
			cardno_reveal(evt.currentTarget);
			evt.stopPropagation();
			return false;
		});
	});
});
</script>
