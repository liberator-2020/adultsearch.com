<?php
require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

// @todo need to rewoked after unification

global $account, $db, $gIndexTemplate, $config_image_path, $config_env;
$gIndexTemplate = "admin_index.tpl";

if (!account::ensure_admin() || $config_env !== 'dev') {
	die("Invalid access");
}

echo "<h2>DEV Download images from PROD</h2>\n";

$tables = [
	'place',
//	'eroticmassage',
//	'strip_club2',
//	'adult_stores',
//	'lingeriemodeling1on1',
//	'gay',
//	'gaybath',
//	'brothel',
//	'lifestyle',
];

if (isset($_REQUEST["return_url"]) && $tableFromUrl = getTableFromUrl($_REQUEST["return_url"])) {
	$tables = [
		$tableFromUrl,
//		'place',
	];
}

if (isset($_REQUEST["id"])) {
	$ids = explode(',', $_REQUEST["id"]);
	$ids = array_map(function ($item) {
		return intval($item);
	}, $ids);
	if (($countIds = count($ids)) == 0) {
		die('id not exist');
	}
	$inQuery = implode(',', array_fill(0, count($ids), '?'));

	foreach ($tables as $table) {

		$map_field = $table!=='strip_revues'?', e.map_image':'';

		if ($table == 'place') {
			checkPathForTypes( $table );
			$res        = $db->q("SELECT p.place_id, p.image, p.thumb, p.map_image, lc.dir as country 
				FROM `place` p
				INNER JOIN location_location l ON p.loc_id = l.loc_id
				INNER JOIN location_location lc ON l.country_id = lc.loc_id
				WHERE p.place_id IN( {$inQuery} )", $ids);
			$res_coupon = $db->q("SELECT pa.place_id, pa.value
				FROM place_attribute pa 
				WHERE pa.attribute_id=148 AND pa.place_id IN( {$inQuery} )", $ids);
			$result     = $res_coupon->fetchAll(PDO::FETCH_ASSOC);
			$coupons    = array_column($result, 'value', 'place_id');
		} else {
			checkPathForTypesOld( $table );
			$res = $db->q("SELECT e.id, e.image, e.coupon, e.thumb{$map_field}
				FROM {$table} e 
				INNER JOIN location_location l on l.loc_id = e.loc_id
				INNER JOIN location_location lc on lc.loc_id = l.country_id
				WHERE e.id IN( {$inQuery} )", $ids);
		}

		//TODO this code supports only one place
		// (country is set to last place's country so if places are from different country, it wouldnt lookup the images)
		$country = null;

		while ($row = $db->r($res)) {

			if ($table == 'place') {
				if ($row['image']) {
//					$image = copyImgItem($table, $row['image'], 'image', $row['country'], true);
					$image = copyImgItem($table, $row['image'], 'image');
				}
				if ($row['thumb']) {
//					$thumb = copyImgItem($table, $row['thumb'], 'thumb', $row['country'], true);
					$thumb = copyImgItem($table, $row['thumb'], 'thumb');
				}
				if (isset($coupons[$row['place_id']])) {
//					$coupon = copyImgItem($table, $coupons[$row['place_id']], 'coupon', $row['country'], true);
					$coupon = copyImgItem($table, $coupons[$row['place_id']], 'coupon');
				}
			} else {
//				if ($row['image']) {
//					$image = copyImgItem($table, $row['image'], 'image');
//				}
//				if ($row['thumb']) {
//					$thumb = copyImgItem($table, $row['thumb'], 'thumb');
//				}
//				if ($row['coupon']) {
//					$coupon = copyImgItem($table, $row['coupon'], 'coupon');
//				}
			}
			if (isset($row['map_image']) && $row['map_image']) {
				$map_image = copyImgItem($table, $row['map_image'], 'map_image');
			}
		}
		$module   = getModulFromTable($table);
		$res_pics = $db->q($r="SELECT pp.picture, pp.filename, pp.thumb 
			FROM place_picture pp 
			WHERE 
				pp.module = '{$module}' 
				AND (pp.id IN( {$inQuery} ))",
			$ids);
		while ($row_pics = $db->r($res_pics)) {
			if ($row_pics['filename']) {
				$image = copyImgItem($table, $row_pics['filename'], 'image');
			}
			if ($row_pics['thumb']) {
				$thumb = copyImgItem($table, $row_pics['thumb'], 'thumb');
			}
		}

		//copy review provider images
		$res2 = $db->q("
			SELECT prpp.filename 
			FROM place_review_provider prp 
			INNER JOIN place_review_provider_pic prpp on prpp.provider_id = prp.provider_id
			WHERE prp.module = '{$module}' AND (prp.place_id IN( {$inQuery} ))",
			$ids);
		while ($row2 = $db->r($res2)) {
			$filename = $row2["filename"];
			$ret = copyImgItem($table, $filename, 'rprovider');
			$ret = copyImgItem($table, $filename, 'rprovider_t');
		}
	}
}

if (isset($_REQUEST["return_url"])) {
	echo '<br><a href="'.$_REQUEST["return_url"].'"> Return to place</a><br>';
}

function copyImgItem($table, $image, $typeImage, $country = '', $countryPath = false) {
	global $config_image_path, $config_image_server;
	if ($image) {
		$pathType = getPathType($typeImage, $countryPath ? $country : $table);
		$image_src = "http://img.adultsearch.com/{$pathType}/{$image}";
		$image_tgt = $config_image_path."{$pathType}/{$image}";
		$url_tgt   = $config_image_server."/{$pathType}/{$image}";
		if (!is_dir($config_image_path."/{$pathType}"))
			mkdir($config_image_path."/{$pathType}", 0777, true);
		$ret       = copy($image_src, $image_tgt);
//		$ret = file_put_contents($image_tgt, fopen($image_src, "r"));
		if (!$ret) {
			if (!file_exists($image_src)) {
				echo "{$typeImage} file '{$image_src}' not found !<br>";
			} else {
				echo "failed to copy {$typeImage} file '{$image_src}' -> '{$image_tgt}' !<br>";
			}
		} else {
			echo "COPY {$typeImage} file <a href='{$image_src}'>'{$image_src}'</a>  -> <a href='{$url_tgt}'>'{$url_tgt}'</a> !<br>";
		}
	}

	return $image;
}

function getPathTypeOld($typeImage, $table) {
	$pathImage = getPathFromTable($table, null);
	switch ($typeImage) {
		case 'thumb':
			$pathType = "{$pathImage}/t";
			break;
		case 'coupon':
			$pathType = "{$pathImage}/c";
			break;
		case 'map_image':
			$pathType = "maps/{$table}";
			break;
		case 'image':
		default:
			$pathType = "{$pathImage}";
	}

	return $pathType;
}

function getPathType($typeImage, $table) {
	$pathImage = getPathFromTable($table, null);
	switch ($typeImage) {
		case 'thumb':
			$pathType = "{$pathImage}/t";
			break;
		case 'coupon':
			$pathType = "{$pathImage}/c";
			break;
		case 'map_image':
			$pathType = "{$table}/maps";
			break;
		case "rprovider":
			$pathType = "rprovider";
			break;
		case "rprovider_t":
			$pathType = "rprovider/t";
			break;
		case 'image':
		default:
			$pathType = "{$pathImage}";
	}

	return $pathType;
}

function getTableFromUrl($url) {
	$table          = '';
	$path_query_arr = explode("?", $url);
	$path_arr       = array_filter(explode("/", trim($path_query_arr[0], '/')), 'strlen');

	if (isset($path_arr[2])) {
		switch ($path_arr[2]) {

			case "erotic-massage":
			case "erotic-massage-parlor":
				$table = "eroticmassage";
				break;
			case "brothels":
				$table = "brothel";
				break;
			case "gay":
				$table = "gay";
				break;
			case "gay-bath-houses":
				$table = "gaybath";
				break;
			case "lingerie-modeling-1on1":
				$table = "lingeriemodeling1on1";
				break;
			case "swinger-clubs":
				$table = "lifestyle";
				break;
			case "sex-shops":
				$table = "adult_stores";
				break;
			case "strip-clubs":
				$table = "strip_club2";
				break;
			case "male-revues":
			case "strip-revues":
				$table = "strip_revues";
				break;
			default:
				break;
		}
	}

	return $table;
}

function getPathFromTable($table_name, $country) {
	if ($country && !in_array($country, ["united-states", "ca", "uk"]))
		return $country;
	switch ($table_name) {
		case "place":
			$pathImage = "place";
			break;
		case "eroticmassage":
			$pathImage = "eroticmassage";
			break;
		case "brothel":
			$pathImage = "brothel";
			break;
		case "gay":
			$pathImage = "gay";
			break;
		case "gaybath":
			$pathImage = "gaybath";
			break;
		case "lingeriemodeling1on1":
			$pathImage = "lingeriemodeling1on1";
			break;
		case "lifestyle":
			$pathImage = "lifestyle";
			break;
		case "adult_stores":
			$pathImage = "adultstore";
			break;
		case "strip_club2":
			$pathImage = "stripclub";
			break;
		case "strip_revues":
			$pathImage = "stripclub/revues";
			break;
		default:
//			die("getPathFromTable : path for images not defined for table $table_name!");
			$pathImage = $table_name;
			break;
	}

	return $pathImage;
}

function checkPathForTypesOld( $table ) {
	global $config_image_path;
	$pathImage = getPathFromTable($table, null);
	foreach (['image', 'thumb','map_image'] as $typeImage) {
		switch ($typeImage) {
			case 'thumb':
				$pathType = "{$pathImage}/t";
				break;
			case 'coupon':
				$pathType = "{$pathImage}/c";
				break;
			case 'map_image':
				$pathType = "maps/{$table}";
				break;
			case 'image':
			default:
				$pathType = "{$pathImage}";
		}
		$dirname = $config_image_path."{$pathType}/";
		if (!file_exists($dirname)) {
			mkdir( $dirname, 0777, true);
			echo "The directory $dirname was successfully created.<br>";
		}
	}

	return $pathType;
}

function checkPathForTypes( $table ) {
	global $config_image_path;
	$pathImage = getPathFromTable($table, null);
	foreach (['image', 'thumb','map_image'] as $typeImage) {
		switch ($typeImage) {
			case 'thumb':
				$pathType = "{$pathImage}/t";
				break;
			case 'coupon':
				$pathType = "{$pathImage}/c";
				break;
			case 'map_image':
				$pathType = "{$pathImage}/maps";
				break;
			case 'image':
			default:
				$pathType = "{$pathImage}";
		}
		$dirname = $config_image_path."{$pathType}/";
		if (!file_exists($dirname)) {
			mkdir( $dirname, 0777, true);
			echo "The directory $dirname was successfully created.<br>";
		}
	}

	return $pathType;
}

function getModulFromTable($table_name) {
	switch ($table_name) {
		case "place":
			$pathImage = "place";
			break;
		case "eroticmassage":
			$pathImage = "eroticmassage";
			break;
		case "brothel":
			$pathImage = "brothel";
			break;
		case "gay":
			$pathImage = "gay";
			break;
		case "gaybath":
			$pathImage = "gaybath";
			break;
		case "lingeriemodeling1on1":
			$pathImage = "lingeriemodeling1on1";
			break;
		case "lifestyle":
			$pathImage = "lifestyle";
			break;
		case "adult_stores":
			$pathImage = "adultstore";
			break;
		case "strip_club2":
			$pathImage = "stripclub";
			break;
		case "strip_revues":
			$pathImage = "stripclubrevue";
			break;
		default:
			die("getModulFromTable : path for images not defined for table $table_name!");
			break;
	}

	return $pathImage;
}
