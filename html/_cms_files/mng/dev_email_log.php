<?php
require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!account::ensure_admin())
    die("Invalid access");

echo "<h2>DEV Email Log</h2>\n";

$h = fopen(_CMS_ABS_PATH."/../data/log/email.log", "r");
if (!$h)
	die("Cant open email.log file");

?>
<style type="text/css">
table.simple {
	border-collapse: collapse;
}
table.simple td, table.simple th {
	padding: 4px;
	vertical-align: top;
	border: 1px solid #ccc;
}
</style>
<table class="simple">
<tr>
	<th>Date/Time</th>
	<th>From</th>
	<th>To</th>
	<th>Subject</th>
	<th>Text</th>
	<th>Html</th>
</tr>
<?php

function convert($val) {
	$val = preg_replace('/<EOL>/', "\n", $val);
	return $val;
}

while ($arr = fgetcsv($h)) {

	$datetime = date("m/d/Y H:i:s T", $arr[0]);
	$from = convert($arr[1]);
	$to = convert($arr[2]);
	$subject = convert($arr[3]);
	$text = nl2br(convert($arr[4]));
	$html = convert($arr[5]);

	echo "<tr><td>{$datetime}</td><td>{$from}</td><td>{$to}</td><td>{$subject}</td><td>{$text}</td><td>{$html}</td></tr>\n";
}

fclose($h);
?>
</table>

