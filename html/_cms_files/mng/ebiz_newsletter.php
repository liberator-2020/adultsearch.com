<?php
require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) { 
	$account->asklogin(); 
	return false; 
}

if (!$account->isadmin()) {
	echo "You dont have privileges to access this page!";
	die;
}

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols, $classifieds;

//configuration
$ipp = 30;
$url_last_piece = "ebiz_newsletter";

function generate() {
	global $url_last_piece;

	if (isset($_REQUEST["submit"])) {
		$how_many = intval($_REQUEST["how_many"]);
		if (isset($_REQUEST["websites"]))
            $websites = true;
        else
            $websites = false;

		if ($how_many) {
			$results = escortsbiz::generate_codes($how_many, $websites);
			if ($results === false) {
				echo "Error: while generating newsletter codes !<br />\n";
			} else if (empty($results)) {
				echo "Error: no accounts left for generating newsletter codes !<br />\n";
			} else {
				echo "OK, we generated ".count($results)." newsletter codes.<br />\n";

				//lets build csv file for download
				$fp = fopen(_CMS_ABS_PATH.'/tmp/ebiz_newsletter.csv', 'w');
			    fputs($fp, "\"Email\",\"Name\",\"Code\"\n");
				foreach ($results as $entry) {
					fputs($fp, "\"{$entry["email"]}\",\"{$entry["name"]}\",\"{$entry["code"]}\"\n");
				}
    			fclose($fp);
    
				echo "<a href=\"/tmp/ebiz_newsletter.csv\" target=\"_blank\">CSV</a><br /><br />\n";
				
				echo "<a href=\"/mng/{$url_last_piece}\">Back</a><br />\n";
			}
		} else {
			echo "Error: you need to specify the number how many entries you want to generate !<br />\n";
		}
	}

	echo "<h2>Generate new ebiz_newsletter batch:</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>How many ?</th><td><input type=\"text\" name=\"how_many\" value=\"\" size=\"5\" /> <small>e.g. 10</small></td></tr>\n";
	echo "<tr><th>With websites ?</th><td><input type=\"checkbox\" name=\"websites\" value=\"1\" /> <small>By default, no</small></td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
	echo "</table>";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}


$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'generate':
		$ret = generate();
		if (!$ret)
			return;
		break;
}

$where = getWhere();
$having = getHaving();
$order = getOrder();
$limit = getLimit();

//query db
$sql = "SELECT count(*) as total 
		FROM (
			SELECT e.stamp
			FROM ebiz_newsletter e
			GROUP BY e.stamp
			) a
		";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];


$sql = "SELECT e.stamp, COUNT(e.id) as e_count
		FROM ebiz_newsletter e
		GROUP BY e.stamp
		$order 
		$limit";

$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No ebiz_newsletter batches.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Escorts.biz newsletter batches</h2>\n";

displayFilterForm();

echo "<a href=\"?action=generate\">Generate new batch</a><br />\n";
echo $pager."<br />";
echo "<form>";
echo getFilterFormFields();
echo "<table class=\"control\">";
echo "<thead><tr><th>".getOrderLink("Generated at", "stamp")."</th><th>Count</th><th/></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<td>".date("Y-m-d H:i:s T", $rox["stamp"])." - ".time_elapsed_string($rox["stamp"])."</td>";
	echo "<td>{$rox["e_count"]}</td>";

	if ($_SESSION["account"] == 3974)
		$links = "<a href=\"#\"	>CSV</a>&nbsp;&middot;&nbsp;<a href=\"#\" >stats</a>";

	echo "<td>{$links}</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo "</form>";
echo $pager;

?>
