<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("access_admin_classifieds"))
    return;

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols;

//configuration
$ipp = 100;
$url_last_piece = "ebiz_ter_check";
$where_cols = array(
	"id"		 => array("title" => "ID", "where" => "e.id", "match" => "exact", "size" => "7"),
	"website"	 => array("title" => "Website", "where" => "e.website", "size" => "15"),
);

function ter() {
	global $db, $account;
	$system = new system();

	$id = intval($_REQUEST["action_id"]);
	if ($id == 0)
		return actionError("Invalid ebiz_ter_check id !");
	$res = $db->q("SELECT * FROM ebiz_ter_check WHERE id = ?", array($id));
	if ($db->numrows($res) != 1)
		return actionError("Cant find classified for id={$clad_id} !");
	$row = $db->r($res);

	if ($_REQUEST["submit"] == "No TER") {
		$res2 = $db->q("UPDATE ebiz_ter_check SET no_ter_stamp = ? WHERE id = ? LIMIT 1", array(time(), $id));
		$aff = $db->affected($res2);
		if ($aff) {
			success_redirect("Website #{$id} {$row["website"]} successfully marked as no TER.", "/mng/ebiz_ter_check");
		} else {
			error_redirect("Error marking website #{$id} as no TER !", "/mng/ebiz_ter_check");
		}
	}

	if ($_REQUEST["submit"] == "Already Linked") {
		$res2 = $db->q("UPDATE ebiz_ter_check SET already_linked_stamp = ? WHERE id = ? LIMIT 1", array(time(), $id));
		$aff = $db->affected($res2);
		if ($aff) {
			success_redirect("Website #{$id} {$row["website"]} successfully marked as already linked.", "/mng/ebiz_ter_check");
		} else {
			error_redirect("Error marking website #{$id} as already linked !", "/mng/ebiz_ter_check");
		}
	}

	if ($_REQUEST["submit"] == "Linked") {
		$res2 = $db->q("UPDATE ebiz_ter_check SET linked_stamp = ? WHERE id = ? LIMIT 1", array(time(), $id));
		$aff = $db->affected($res2);
		if ($aff) {
			success_redirect("Website #{$id} {$row["website"]} successfully marked as linked.", "/mng/ebiz_ter_check");
		} else {
			error_redirect("Error marking website #{$id} as linked !", "/mng/ebiz_ter_check");
		}
	}

	error_redirect("Unknown action !", "/mng/ebiz_ter_check");
}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'ter':
		$ret = ter();
		if (!$ret)
			return;
		break;
}

$condition = "e.already_linked_stamp IS NULL AND e.no_ter_stamp IS NULL AND e.linked_stamp IS NULL";
$where = getWhere();
if($where == "")
	$where = " WHERE {$condition} ";
else 
	$where .= " AND {$condition} ";

//$order = " ORDER BY ter_check_priority DESC, ter DESC ";
$order = getOrder();
$limit = getLimit();

//query db
$sql = "SELECT count(*) as total 
		FROM ebiz_ter_check e
		$where
	";

$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];


$sql = "SELECT e.*, ah.id as highlight_id
		FROM ebiz_ter_check e
		LEFT JOIN admin_highlight ah on ah.module = 'ebiz_ter_check' AND ah.item_id = e.id
		$where
		$order 
		$limit";

$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No ebiz websites to check TER.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>EBIZ TER Check</h2>\n";
displayFilterForm();
echo $pager."<br />";
echo getFilterFormFields();

echo "<table class=\"control\">";
echo "<thead><tr><th/><th>".getOrderLink("Id", "id")."</th><th>Website</th><th>Email</th><th>Phone</th><th/></tr></thead>\n";
echo "<tbody>";
while ($row = $db->r($res)) {

	echo "<tr class=\"highlightable";
	if ($row["highlight_id"])
		echo " highlighted";
	echo "\" data-jay=\"{$row["highlight_id"]}\" data-highlighted=\"".(($row["highlight_id"]) ? 1 : 0)."\" data-item-id=\"{$row["id"]}\" >";
	echo "<td/>";
	echo "<td>{$row["id"]}</td>";
	echo "<td>{$row["website"]}</td>";
	echo "<td>{$row["email"]}</td>";
	echo "<td>".makeProperPhoneNumber($row["phone"])."</td>";
	echo "<td>";

	echo "<a href=\"https://www.theeroticreview.com/reviews/newreviewsList.asp?searchreview=1&SortBy=3&cs_config_country_field=countrySelect&cs_config_city_field=citySelect&cs_config_city2_field=city2Select&cs_config_country_default=countryDefault&cs_config_city_default=cityDefault&cs_config_city2_default=city2Default&cs_select_city_text=select+city&cs_select_country_text=selectCountry&cs_all_countries_text=All+Countries&cs_all_cities_text=All+Cities&searchFlDD=1&Phone={$row["phone"]}\" target=\"ter_search\" />Search on TER</a>";

	echo " &middot; <a href=\"http://{$row["website"]}\" target=\"as_view\">view website</a> ";

	echo "<form style=\"display:inline-block;\" action=\"\" method=\"post\">";
	echo "<input type=\"hidden\" name=\"action\" value=\"ter\" />";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$row["id"]}\" />";
	echo " <input type=\"submit\" name=\"submit\" value=\"No TER\" />";
	echo " <input type=\"submit\" name=\"submit\" value=\"Already Linked\" />";
	echo " <input type=\"submit\" name=\"submit\" value=\"Linked\" />";
	echo "</form>";

	echo "</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;

?>
<script src="/js/clipboard.min.js"></script>
<script type="text/javascript">
function highlight_toggle(elem) {
	var item_id = $(elem).closest('tr').data('item-id');
	var highlighted = $(elem).closest('tr').data('highlighted');
	$.post(
		'/_ajax/highlight', 
		{'module': 'ebiz_ter_check', 'item_id': item_id, 'current': highlighted}, 
		function(data) {
			if (data.status != 'ok')
				return;
			if (highlighted == 0) {
				$(elem).closest('tr').data('highlighted', 1);
				$(elem).closest('tr').addClass('highlighted');
				$(elem).text('U');
			} else {
				$(elem).closest('tr').data('highlighted', 0);
				$(elem).closest('tr').removeClass('highlighted');
				$(elem).text('H');
			}
		},
		'json'
		);
}

$(document).ready(function() {
	var clipboard = new Clipboard('.copycat');
	$('.control tr.highlightable').each(function(ind,obj) {
		var first_td = $(this).find('td:first-child');
		var highlighted = $(this).data('highlighted');
		if (highlighted)
			first_td.append('<button type="button" class=\"highlight_btn\">U</button>');
		else
			first_td.append('<button type="button" class=\"highlight_btn\">H</button>');
	});
	$('.highlight_btn').click(function() {
		highlight_toggle(this);
	});
});

</script>
