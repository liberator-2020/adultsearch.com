<?php
require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");
global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) {
    $account->asklogin();
    return false;
}

if (!$account->isadmin()) {
    echo "You dont have privileges to access this page!";
    die;
}

?>
<h1>How to set up your email client for Atmail mailboxes</h1>
<table>
<tr><th colspan="2">Receiving emails (IMAP)</th></tr>
<tr><td>Server Name:</td><td>mail-geterdone.atmailcloud.com</td></tr>
<tr><td>Port:</td><td>993</td></tr>
<tr><td>User Name:</td><td>yourname@adultsearch.com</td></tr>
<tr><td>Connection Security:</td><td>SSL/TLS</td></tr>
<tr><td>Authentication Method:</td><td>Normal password</td></tr>
<tr><td colspan="2"><img src="/images/control/email_settings_imap.png" /></td></tr>
<tr><th colspan="2">Outgoing emails (SMTP)</th></tr>
<tr><td>Server Name:</td><td>mail-geterdone.atmailcloud.com</td></tr>
<tr><td>Port:</td><td>465</td></tr>
<tr><td>User Name:</td><td>yourname@adultsearch.com</td></tr>
<tr><td>Connection Scurity:</td><td>SSL/TLS</td></tr>
<tr><td>Authentication Method:</td><td>Normal password</td></tr>
<tr><td colspan="2"><img src="/images/control/email_settings_smtp.png" /></td></tr>
<tr><td colspan="2"><img src="/images/control/email_settings_smtp_2.png" /></td></tr>
</table>
