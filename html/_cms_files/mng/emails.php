<?php
require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $system, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in
if (!$account->isMember()) {
	$account->asklogin();
	return false;
}

if (!$account->isrealadmin()) {
	$system->go("/");
	die;
}

//basic php email
function emailPhp() {
	$email = $_REQUEST["email"];
	$subject = "Test email subject";
	$message = "Test email message";
	$headers = "From: support@adultsearch.com";
	//echo "email=$email, subject=$subject, message=$message headers=$headers<br />";
	$result = mail($email, $subject , $message, $headers);
	$result_text = ($result === false) ? "ERROR!" : "OK";
	echo "Result: {$result_text}<br />";
}

function advertise_lowbudget() {
	$email = $_REQUEST["email"];
	$a = new advertise();
	$a->emailLowBudget($email);
	echo "Done.";
}

if (isset($_REQUEST["email_php"])) {
	emailPhp();
} else if (isset($_REQUEST["advertise_lowbudget"])) {
	advertise_lowbudget();
}

?>

<br /><h3>Send Test Email</h3>
<form action="/mng/emails" method="post">
Email:<input type="text" size="25" name="email" value="" /><br />
<input type="submit" name="email_php" value="Basic PHP mail function" />
<input type="submit" name="advertise_lowbudget" value="Advertise - low budget" />
</form>
