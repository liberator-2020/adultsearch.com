<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) { 
    $account->asklogin(); 
    return false; 
}

if (!$account->isRealAdmin()) {
	echo "You dont have privileges to access this page!";
	die;
}

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols;

//configuration
$ipp = 30;
$url_last_piece = "empty_latlong";

$order = getOrder();
$limit = getLimit();

//query db
$sql = "SELECT count(*) as total FROM place WHERE loc_lat IS NULL OR loc_long IS NULL";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT p.place_id, p.name, p.address1, p.loc_lat, p.loc_long, p.loc_id, l.loc_name 
		FROM place p
		LEFT JOIN location_location l on l.loc_id = p.loc_id
		WHERE p.loc_lat IS NULL OR p.loc_long IS NULL
		$order 
		$limit";

$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No places with empty loclong.";
	return;
}

//pager
$pager = getPager($total);

//output
echo "<h2>Empty loclongs</h2>\n";
echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr>
<th>".getOrderLink("Id", "place_id")."</th>
<th>Name</th>
<th>Address1</th>
<th>Loc_lat</th>
<th>Loc_long</th>
<th>Loc_id</th>
<th>Loc_name</th>
<th/>
</tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<tr>";

	echo "<td>{$rox["place_id"]}</td>";
	echo "<td>{$rox["name"]}</td>";
	echo "<td>{$rox["address1"]}</td>";
	echo "<td>{$rox["loc_lat"]}</td>";
	echo "<td>{$rox["loc_long"]}</td>";
	echo "<td>{$rox["loc_id"]}</td>";
	echo "<td>{$rox["loc_name"]}</td>";

//		$place_link = dir::getPlaceLink($row, true);
	$edit = "<a href=\"javascript:void(0)\" onclick=\"return win_popup('/ajax/files/{$rox['module']}/{$id}/edit/{$rox['picture']}', 'Edit extra file', 800, 650);\">Edit</a>";
	echo "<td></td>";

	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;


?>
