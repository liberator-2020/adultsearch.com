<?php

defined("_CMS_FRONTEND") or die("No Access");

require_once(_CMS_ABS_PATH."/inc/classes/excel/Classes/PHPExcel.php");
require_once(_CMS_ABS_PATH."/inc/classes/excel/Classes/PHPExcel/Writer/Excel2007.php");

global $db, $smarty, $page, $account;
$system = new system;

$acc_id = $account->isloggedin();

$excel = isset($_GET['excel']) ? 1 : 0;

$item_per_page = isset($_GET["ipp"]) ? intval($_GET["ipp"]) : 40;
if( $item_per_page > 60 ) $item_per_page = 50;
$start = (($page*$item_per_page)-$item_per_page);
if( $start < 0 ) $start = 0;

$id = isset($_REQUEST['id']) && !empty($_REQUEST['id']) ? GetRequestParam('id') : NULL;
$account_id = isset($_REQUEST['account_id']) && !empty($_REQUEST['account_id']) ? GetRequestParam('account_id') : NULL;
$phone = isset($_REQUEST['phone']) && !empty($_REQUEST['phone']) ? GetRequestParam('phone') : NULL;
$cl_email = isset($_REQUEST['cl_email']) && !empty($_REQUEST['cl_email']) ? GetRequestParam('cl_email') : NULL;
$loc_name = isset($_REQUEST['loc_name']) && !empty($_REQUEST['loc_name']) ? GetRequestParam('loc_name') : NULL;
$state = isset($_REQUEST['state']) && !empty($_REQUEST['state']) ? GetRequestParam('state') : NULL;
$paid = isset($_REQUEST['paid']) && !empty($_REQUEST['paid']) ? GetRequestParam('paid') : NULL;
$done = isset($_REQUEST['done']) && !empty($_REQUEST['done']) ? GetRequestParam('done') : NULL;
$promo = isset($_REQUEST['promo']) && !empty($_REQUEST['promo']) ? GetRequestParam('promo') : NULL;

$param = NULL;
if( $id ) {
	$smarty->assign('id', $id);
	$smarty->assign("id", $id);
	$param[] = array("key"=>"id", "value"=>$id);
}
if( $account_id ) {
	$smarty->assign('account_id', $account_id);
	$smarty->assign("account_id", $account_id);
	$param[] = array("key"=>"account_id", "value"=>$account_id);
}
if( $phone ) {
	$phone = preg_replace("/[^0-9]/", "", $phone);
	$query = $phone;
	$smarty->assign("phone", $phone);
	$param[] = array("key"=>"phone", "value"=>$phone);
}
if( $loc_name ) {
	$loc_name = trim($loc_name);
	$query = $query ? ($query . " " .$loc_name) : $loc_name;
	$smarty->assign("loc_name", $loc_name);
	$param[] = array("key"=>"loc_name", "value"=>$loc_name);
}
if( $state ) {
	$state = trim($state);
	$res = $db->q("select loc_id from location_location where loc_type = 2 and s = '$state' limit 1");
	if( $db->numrows($res) ) {
		$row = $db->r($res);
		$state_id = $row[0];
	} else $state_id = -1;
	$smarty->assign("state", $state);
	$param[] = array("key"=>"state", "value"=>$state);
}
if( $cl_email ) {
	$cl_email = trim($cl_email);
	$res = $db->q("select account_id from account where email = '$cl_email'");
	if( $db->numrows($res) ) {
		$row = $db->r($res);
	}
	$param[] = array("key"=>"cl_email", "value"=>$cl_email);
	$smarty->assign("cl_email", $cl_email);
}
if( $paid ) {
	$smarty->assign("paid", $paid);
	$param[] = array("key"=>"paid", "value"=>$paid);
}
if( $done ) {
	$smarty->assign('done', $done);
	$param[] = array("key"=>"done", "value"=>$done);
}
if( $promo ) {
	$promo = trim($promo);
	$res = $db->q("select id from classifieds_promocodes where code = '$promo'");
	if( $db->numrows($res) ) {
		$row = $db->r($res);
	}
	$smarty->assign('promo', $promo);
	$param[] = array("key"=>"promo", "value"=>$promo);
}


$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Websolutions Netherlands");
$objPHPExcel->getProperties()->setLastModifiedBy("Websolutions Netherlands");
$objPHPExcel->getProperties()->setTitle("Office Doc");
$objPHPExcel->getProperties()->setSubject("Office Doc");
$objPHPExcel->getProperties()->setDescription("generated through web.");

$objPHPExcel->setActiveSheetIndex(0);

$res = $db->q("SELECT C.`id`, A.`email`, L.loc_name, L.s, C.`phone`, `paid`, `date`, `auto_renew`, C.`promo`, P.`code`, cc.firstname, cc.lastname, right(cc.cc, 4) ccn, cc.zipcode
				FROM account_purchase PU
				INNER JOIN classifieds C on PU.account_id = C.account_id
				INNER JOIN account A on C.account_id = A.account_id
				INNER JOIN account_cc cc on PU.cc_id = cc.cc_id and cc.deleted = 0
				LEFT JOIN location_location L using(loc_id)
				LEFT JOIN classifieds_promocodes P on C.promo = P.id
				WHERE PU.time > date_sub(now(), interval 30 day) and PU.what = 'classifieds' and C.phone != '' and total_loc < 5
				GROUP BY phone");
$listing = NULL;
$c = 0; $number = 1; $ex = NULL;
while($row=$db->r($res)) {
	$objPHPExcel->getActiveSheet()->SetCellValue("A$number", $row['id']);
	$objPHPExcel->getActiveSheet()->SetCellValue("B$number", $row['email']);
	$objPHPExcel->getActiveSheet()->SetCellValue("C$number", $row['firstname'].($row['lastname']?", {$row['lastname']}":''));
	$objPHPExcel->getActiveSheet()->SetCellValue("D$number", $row['loc_name']);
	$objPHPExcel->getActiveSheet()->SetCellValue("E$number", $row['state']);
	$objPHPExcel->getActiveSheet()->SetCellValue("F$number", $row['phone']);
	$objPHPExcel->getActiveSheet()->SetCellValue("G$number", $row['paid']);
	$objPHPExcel->getActiveSheet()->SetCellValue("H$number", $row['date']);
	$objPHPExcel->getActiveSheet()->SetCellValue("I$number", "http://adultsearch.com/classifieds/look?id={$e['id']}");
	$objPHPExcel->getActiveSheet()->SetCellValue("J$number", $row['ccn']);
	$objPHPExcel->getActiveSheet()->SetCellValue("K$number", $row['auto_renew']);
	$objPHPExcel->getActiveSheet()->SetCellValue("L$number", $row['promo']);
	$objPHPExcel->getActiveSheet()->SetCellValue("M$number", $row['code']);
	$number++;
}

if( $number < 2 )
	die('no record.');

$objPHPExcel->getActiveSheet()->setTitle('Simple');
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$file = "/UserFiles/excel/" . $acc_id . "_". time() . ".xlsx";
$objWriter->save(_CMS_ABS_PATH.$file);
$system->moved($file);

$q = "?";
foreach($param as $p) {
	$q .= "{$p["key"]}={$p["value"]}&";
}

$smarty->assign("total", $total);
$smarty->assign("q", $q);
$smarty->assign("paging", $db->paging($total, $item_per_page, true, true, $q));

?>
