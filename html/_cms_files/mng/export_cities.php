<?php

defined("_CMS_FRONTEND") or die("No Access");

require_once(_CMS_ABS_PATH."/inc/classes/excel/Classes/PHPExcel.php");
require_once(_CMS_ABS_PATH."/inc/classes/excel/Classes/PHPExcel/Writer/Excel2007.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!account::ensure_admin())
	die("Invalid access");

if ($_REQUEST["submit"] != "Export") {
	echo "<h1>Export city list ?</h1>";
	echo "<form><input type=\"submit\" name=\"submit\" value=\"Export\" /></form>";
	return;
}


$o = new PHPExcel();

$o->getProperties()->setCreator("AS web")->setTitle("AS City List");
$o->setActiveSheetIndex(0);
$a = $o->getActiveSheet()->setTitle('City List');

//put in header
$a->setCellValue("A1", "Country");
$a->setCellValue("B1", "State/Province");
$a->setCellValue("C1", "City");
$a->setCellValue("D1", "Text Link");
$a->setCellValue("E1", "Text Link 2 if text Link 1 is to long");
$a->setCellValue("F1", "Description for hover over text");
$a->setCellValue("G1", "URL");

//column widths
$a->getColumnDimension('A')->setWidth(18);
$a->getColumnDimension('B')->setWidth(18);
$a->getColumnDimension('C')->setWidth(18);
$a->getColumnDimension('D')->setWidth(30);
$a->getColumnDimension('E')->setWidth(30);
$a->getColumnDimension('F')->setWidth(35);
$a->getColumnDimension('G')->setWidth(70);

$a->setCellValue("D3", "Female escorts");
$a->setCellValue("E3", "Female escorts in your area");
$a->setCellValue("F3", "Female escorts in your area");
$a->setCellValue("G3", "https://adultsearch.com");

//write locations
$res = $db->q("
	SELECT DISTINCT
	(CASE WHEN lp.loc_type = 1 THEN lp.loc_name WHEN lpp.loc_type = 1 THEN lpp.loc_name ELSE NULL END) as country_name, 
	(CASE WHEN lp.loc_type = 2 THEN lp.loc_name ELSE NULL END) as state_name,
	l.loc_name, l.dir, l.loc_url
	, (CASE WHEN lp.loc_type = 1 THEN lp.dir WHEN lpp.loc_type = 1 THEN lpp.dir ELSE NULL END) as country_sub
	FROM classifieds_loc cl
	INNER JOIN classifieds c on c.id = cl.post_id
	INNER JOIN location_location l on cl.loc_id = l.loc_id
	INNER JOIN location_location lp on l.loc_parent = lp.loc_id
	LEFT JOIN location_location lpp on lp.loc_parent = lpp.loc_id
	WHERE cl.done = 1 AND c.deleted IS NULL
	ORDER BY 1,2,3
	");
$i = 4;
while ($row = $db->r($res)) {
	$url = location::getUrlStatic($row["country_sub"], $row["loc_url"], $row["dir"]);
	$hover = "{$row["loc_name"]} Female Escorts";
	$description = "Female escorts in {$row["loc_name"]}";
	$short_hover = "{$row["loc_name"]} Escorts";
	$a->setCellValue("A{$i}", $row["country_name"]);
	$a->setCellValue("B{$i}", $row["state_name"]);
	$a->setCellValue("C{$i}", $row["loc_name"]);
	$a->setCellValue("D{$i}", $hover);
	$a->setCellValue("E{$i}", $short_hover);
	$a->setCellValue("F{$i}", $description);
	$a->setCellValue("G{$i}", '=HYPERLINK("'.$url.'","'.$url.'")');
	$i++;
}

$w = new PHPExcel_Writer_Excel5($o);
$file = "/UserFiles/excel/as_city_list.xls";
$w->save(_CMS_ABS_PATH.$file);
system::moved($file);

?>
