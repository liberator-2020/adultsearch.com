<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("edit_all_places"))
	return;

$account_level = $account->getAccountLevel();

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols;

//configuration
$ipp = 30;
$url_last_piece = "forum_posts";
$where_cols = array(
	"account_id" => array("title" => "Account ID", "where" => "fp.account_id", "match" => "exact", "size" => "7"),
	"topic_id" => array("title" => "Topic ID", "where" => "fp.topic_id", "match" => "exact", "size" => "7"),
	"post_text"   => array("title" => "Content", "where" => "fp.post_text"),
);

function delete() {
	global $account, $db;

	$forum = new forum();

	$post_id = intval($_REQUEST["action_id"]);
	if ($post_id == 0)
		return actionError("Invalid post id !");

	$ret = $forum->delPost($post_id, $error);
	if ($ret) {
		return actionSuccess("Forum post #{$post_id} successfully deleted.");
	} else {
		return actionError("Error deelting forum post #{$post_id}: '{$error}' !");
	}
}	

function mass_delete() {
	global $account, $db;

	$live = ($_REQUEST["live"] == 1) ? true : false;
	$ids = getActionIds();
	$cnt = count($ids);

	if ($cnt == 0) {
		echo "No forum posts selected!<br />";
		return true;
	}

	if (!$live) {
		echo "Are you sure you want to delete these {$cnt} forum posts ?<br />";
		echo "<span style=\"color: red; font-weight: bold\">(Deleting forum post is currently irreversible operation !)</span><br />";

		$sql = "SELECT p.post_id, p.posted, a.account_id, a.email, p.post_text
				FROM forum_post p
				LEFT JOIN account a on p.account_id = a.account_id
				WHERE p.post_id IN (".implode(",",$ids).")";
		$res = $db->q($sql);
		if (!$db->numrows($res)) {
			echo "ERROR!"; die;
		}
		$cnt = $db->numrows($res);

		echo "<table><tr><th>Id</th><th>Posted on</th><th>Account</th><th>Post text</th></tr>";
		while ($row = $db->r($res)) {
			echo "<tr>";
			echo "<td>{$row["post_id"]}</td>";
			echo "<td>".date("Y-m-d H:i:s T", $row["posted"])."</td>";
			echo "<td>#{$row["account_id"]} - {$row["email"]}</td>";
			$text = $row["post_text"];
			if (strlen($text) > 200)
				$text = substr($text, 0, 200)." ...";
			echo "<td>{$text}</td>";
			echo "</tr>";
		}
		echo "</table>";

		echo getConfirmActionForm("mass_delete", $ids);
		return false;
	}

	$sql = "SELECT *
			FROM  forum_post p
			WHERE p.post_id IN (".implode(",",$ids).")";
	//echo "SQL='{$sql}'";
	$res = $db->q($sql);
	if (!$db->numrows($res)) {
		echo "ERROR!"; die;
	}
	$cl = 0;
	while ($row = $db->r($res)) {
		//fetch post
		$post = post::withRow($row);
		if (!$post)
			continue;
		//remove post
		if ($post->remove())
			$cl++;
	}

	echo "<strong>{$cl}</strong> forum post(s) successfully removed.<br />";

	return true;
}	

function send_admin_notify_to_me() {
	global $db, $account;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid classified id !");

	$post = post::findOneById($action_id);
	if (!$post)
		return actionError("Can't find post by id#{$action_id} !");
	
	$email = ADMIN_EMAIL;
	$email = ADMIN_EMAIL;

	$ret = $post->emailAdminNotify($email);
	
	if ($ret)
		return actionSuccess("Successfully sent admin notify email for post #id {$action_id} to email '{$email}'.");
	else
		return actionError("Error sending admin notify email receipt for post #id {$action_id} to email '{$email}' !");
}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'delete':
		$ret = delete();
		if (!$ret)
			return;
		break;
	case 'mass_delete':
		$ret = mass_delete();
		if (!$ret)
			return;
		break;
	case 'send_admin_notify_to_me':
		$ret = send_admin_notify_to_me();
		if (!$ret)
			return;
		break;
	default:
		break;
}

$where = getWhere();
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
    $order = "ORDER BY fp.post_id DESC";

//query db
$sql = "SELECT count(*) as total 
		FROM forum_post fp
		$where";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT fp.post_id, fp.topic_id, fp.account_id, fp.posted, fp.post_text
		FROM forum_post fp
		LEFT JOIN account a on a.account_id = fp.account_id
		LEFT JOIN forum_topic ft on ft.topic_id = fp.topic_id
		$where
		$order
		$limit";
$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No accounts.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Forum posts</h2>\n";
displayFilterForm();

echo $pager;

echo "<form method=\"post\" action=\"{$url_last_piece}\">";
echo "<select name=\"action\"><option value=\"\">-</option>";
echo "<option value=\"mass_delete\">Delete</option>";
echo "</select>";
echo "<input type=\"submit\" name=\"submit\" value=\"Execute...\" />";

echo "<table class=\"control\">";
echo "<thead><tr>
<th class=\"check\"><input type=\"checkbox\" name=\"master\" value=\"\" /></th>
<th>".getOrderLink("Id", "post_id")."</th>
<th>".getOrderLink("Topic ID", "topic_id")."</th>
<th>".getOrderLink("Account ID", "account_id")."</th>
<th>Post</th>
<th/>
</tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<tr>";

	echo "<td class=\"check\"><input type=\"checkbox\" name=\"action_id[]\" value=\"{$rox['post_id']}\" /></td>";
	echo "<td>{$rox["post_id"]}</td>";
	echo "<td>{$rox["topic_id"]}</td>";
	echo "<td><a href=\"/mng/accounts?account_id={$rox["account_id"]}\">{$rox["account_id"]}</a></td>";
	echo "<td>{$rox["post_text"]}</td>";

	echo "<td>";
	echo "<a href=\"".getActionLink("delete", $rox['post_id'])."\">delete</a>";
	echo "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("send_admin_notify_to_me", $rox['post_id'])."\">send_admin_notify_to_me</a>";
	echo "</td>";

	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo "</form>";
echo $pager;


?>
