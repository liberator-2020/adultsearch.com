<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("forum_topic_manage"))
    return;

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols, $classifieds;

//configuration
$ipp = 30;
$url_last_piece = "forum_topics";
$where_cols = array(
	"topic_id" => "Topic ID",
	"account_id" => "Account ID",
	"title" => array("title" => "Title", "where" => "t.topic_title"),
);


function delete_ad_ban_owner() {
	global $db, $classifieds;

	$live = ($_REQUEST["live"] == 1) ? true : false;

	$ids = array();
	foreach ($_REQUEST["id"] as $id) {
		$id = intval($id);
		if ($id == 0)
			continue;
		$ids[] = $id;
	}
	$cnt = count($ids);

	if ($cnt == 0) {
		echo "No classifieds selected!<br />";
		return true;
	}

	//echo "Ids: <pre>".print_r($ids, true)."<br />";

	if (!$live) {
		echo "Are you sure you want to delete these {$cnt} ads and ban their owners ?<br />";
		echo "<span style=\"color: red; font-weight: bold\">(Deleting classified ad is currently irreversible operation !)</span><br />";
	
		$sql = "SELECT c.id, c.type, c.title, a.account_id, a.username, a.email FROM classifieds c
				LEFT JOIN account a on c.account_id = a.account_id
				WHERE c.id IN (".implode(",",$ids).")";
		$res = $db->q($sql);
		if (!$db->numrows($res)) {
			echo "ERROR!"; die;
		}
		$cnt = $db->numrows($res);

		echo "<table><tr><th>Id</th><th>Type</th><th>Title</th><th>Account Id</th><th>Account Email</th></tr>";
		while ($row = $db->r($res)) {
			echo "<tr><td>{$row["id"]}</td><td>{$row["type"]}</td><td>".htmlspecialchars($row["title"])."</td><td>{$row["account_id"]}</td><td>{$row["email"]}</td></tr>";
		}
		echo "</table>";
	
		echo "<form>";
		echo "<input type=\"hidden\" name=\"action\" value=\"delete_ad_ban_owner\" />";
		echo "<input type=\"hidden\" name=\"live\" value=\"1\" />";
		foreach ($ids as $id) {
			echo "<input type=\"hidden\" name=\"id[]\" value=\"{$id}\" />";
		}
		echo "<input type=\"submit\" name=\"submit\" value=\"Yes\" />";
		echo "</form>";
		return false;
	}

	$sql = "SELECT c.id, c.account_id as acc_id, a.account_id as account_id
			FROM classifieds c
			LEFT JOIN account a on c.account_id = a.account_id
			WHERE c.id IN (".implode(",",$ids).")";
	//echo "SQL='{$sql}'";
	$res = $db->q($sql);
	if (!$db->numrows($res)) {
		echo "ERROR!"; die;
	}
	$cl = 0; $ac = 0;
	while ($row = $db->r($res)) {
		//remove ad
		$classifieds->remove($row["id"]);
		$cl++;

		//ban user
		if ($row["acc_id"] == 0 || $row["account_id"] == NULL)
			continue;

        if (($acc = account::findOneById($row["account_id"])) === false)
			continue;

        $ret = $acc->ban();
		if ($ret)
			$ac++;
    }

	echo "<strong>{$cl}</strong> classified ad(s) successfully removed. <strong>{$ac}</strong> account(s) successfully banned.<br />";

	return true;
}

function send_admin_notify_to_me() {
    global $db, $account;

    $action_id = intval($_REQUEST["action_id"]);
    if ($action_id == 0)
        return actionError("Invalid forum topic id !");

    $topic = topic::findOneById($action_id);
    if (!$topic)
        return actionError("Cant find topic id#{$action_id} !");

    $email = ADMIN_EMAIL;
    $email = ADMIN_EMAIL;
//    $email = NULL;

    $ret = $topic->emailAdminNotify($email);

    if ($ret)
        return actionSuccess("Successfully sent email admin notify for topic #id {$action_id} to email '{$email}'.");
    else
        return actionError("Error sending email admin notify for topic #id {$action_id} to email '{$email}' !");
}

switch ($_REQUEST["action"]) {
	case 'delete_ad_ban_owner':
		$ret = delete_ad_ban_owner();
		if (!$ret)
			return;
		break;
	case 'send_admin_notify_to_me':
		$ret = send_admin_notify_to_me();
		if (!$ret)
			return;
		break;
}

$filters = getFilters();
$where = getWhere();
$order = getOrder();
$limit = getLimit();

//query db
$sql = "SELECT count(*) as total 
		FROM forum_topic t
		$where";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT t.topic_id, t.account_id, t.topic_time, t.loc_id, t.topic_title, t.forum_id, l.loc_type, l.loc_name, c.county
		FROM forum_topic t
		LEFT JOIN location_location l on l.loc_id = t.loc_id
		LEFT JOIN location_county c on c.county_id = t.loc_id
		$where
		$order 
		$limit";

$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No forum topics.";
	return;
}

//pager
$pager = getPager($total);

//output
echo "<h2>Forum topics</h2>\n";

echo "<br />\n";
displayFilterForm();

echo "<form>";
echo "<select name=\"action\"><option value=\"\">-</option>";
//echo "<option value=\"delete_ad_ban_owner\">Delete ad &amp; ban owner</option>";
//echo "<option value=\"free_renew_3_months\">Free renew for 3 months</option>";
echo "</select>";
echo "<input type=\"submit\" name=\"submit\" value=\"Execute\" />";

echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr><th class=\"check\"><input type=\"checkbox\" name=\"master\" value=\"\" /></th><th>".getOrderLink("Id", "topic_id")."</th><th>".getOrderLink("Account ID", "account_id")."</th><th>".getOrderLink("Created", "topic_time")."</th><th>Location</th><th>".getOrderLink("Title", "topic_title")."</th><th/></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<tr>";
	echo "<td class=\"check\"><input type=\"checkbox\" name=\"id[]\" value=\"{$rox['id']}\" /></td>";
	echo "<td>{$rox['topic_id']}</td>";
	echo "<td>{$rox["account_id"]}</td>";
	echo "<td>{$rox["topic_time"]}</td>";

	$location = "?";
	if ($rox["loc_type"] == 3) {
		$location = "City: {$rox["loc_name"]}";
	} else if ($rox["loc_type"] == 2) {
		$location = "State: {$rox["loc_name"]}";
	} else if ($rox["loc_type"] == NULL) {
		if ($rox["county"] != NULL)
			$location = "County: {$rox["county"]}";
		else
			$location = "N/A in loc table, loc_id={$rox["loc_id"]}";
	}
	echo "<td>{$location}</td>";

	echo "<td>{$rox["topic_title"]}</td>";

	$link = forum::getTopicLink($rox["topic_id"], $rox["forum_id"], true);
	$deltopic_link = forum::getTopicActionLink("deltopic", $rox["topic_id"], $rox["forum_id"], true);
	echo "<td><a href=\"{$link}\">view</a>";
	echo "&nbsp;&middot;&nbsp;<a href=\"{$deltopic_link}\">delete</a>";
	echo "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("send_admin_notify_to_me", $rox['topic_id'])."\">resend_admin_notify_to_me</a>";
	echo "</td>";

	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;
echo "</form>";


?>
