<?php
require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in
if (!$account->isMember()) {
	$account->asklogin();
	return false;
}

//only for burak and jay for now
if (!$account->isrealadmin())
	system::go('/');


function catNamestoPlural(&$item , $key) {
	$item = dir::getCategoryPlural($item);
}

function getCities($loc_id) {
	global $db;
	//$res = $db->q("select loc_id, loc_name from location_location where loc_type = 3 and loc_parent = '{$loc_id}'");
	$res = $db->q("select loc_id, loc_name from location_location where loc_type = 3 and country_id = ?", array($loc_id));
    $cities = array();
    while ($row = $db->r($res)) {
        $cities[$row['loc_id']] = $row['loc_name'];
    }
	return $cities;
}

function forumsForLocation() {
	global $db, $ctx;

	//get location 
	$loc_id = GetRequestParam("loc_id");
	if ($loc_id == "") {
		 system::go("/control/forums");
	}

	//get live flag
	$live = (GetRequestParam("live") == "1") ? true : false;

	echo "<h2>Forums for location {$loc_id}</h2>\n";

	//TODO this piece working just for internationakl
	//get categories for country
	$res = $db->q("select dir from location_location where loc_type = 1 and loc_id = '{$loc_id}'");
	if ($db->numrows($res) != 1) {
		echo "Can;t find country in location_location table!<br />\n";
		return;
	}
	$row = $db->r($res);
	$ctx->country = $row['dir'];
	$dir = new dir();
	$types = $dir->getTypesForCountry($loc_id);
	foreach ($types as $type)
		$cats[] = $type["name_pl"];
//	_darr($cats);
	$cats[] = "General Talk";

	//get forum names
	$res = $db->q("select * from forum_name");
	$forum_names = array();
	while ($row = $db->r($res)) {
		$forum_names[$row['forum_name']] = $row['forum_id'];
	}
	//_darr($forum_names);
	
	//found categories in forum_name table
	$forums_should = array();
	foreach ($cats as $cat) {
		if (array_key_exists($cat, $forum_names)) {
			$forums_should[$forum_names[$cat]] = $cat;
		} else {
			echo "<span style=\"color: red; font-weight: bold;\">Category $cat not found in forum_name table !</span><br />\n";
		}
	}
	//_darr($forums_should);

	//get cities
	$cities = getCities($loc_id);
	if (empty($cities)) {
		echo "<span style=\"color: red; font-weight: bold;\">No cities for this location !</span><br />\n";
		return;
	}
	//_darr($cities);
	$ids = "";
	foreach ($cities as $key => $val) {
		$ids .= (empty($ids)) ? "{$key}" : ",{$key}";
	}

	//get forums
	//_d("ids = $ids");
	$res = $db->q("select * from forum_forum where loc_id IN ({$ids})");
	$forums = array();
	while ($row = $db->r($res)) {
		$forums[$row['loc_id']][$row['forum_id']] = $row['forum_name'];
	}
	//_darr($forums);

	//check if there is such a forum
	$change = false;
	foreach ($cities as $city_id => $city_name) {
		$missing = "";
		$missing_cnt = 0;
		$insert_cnt = 0;
		foreach ($forums_should as $forum_id => $forum_name) {
			if ($forums[$city_id][$forum_id] != $forum_name) {
				//echo "Forum $forum_name does not exists for location $city_name<br />\n";
				if (!$live) {
					$missing .= (empty($missing)) ? "{$forum_name}" : ",{$forum_name}";
					$missing_cnt++;
				} else {
					$forum_desc = "{$forum_name} Talks";
					$sql = "insert into forum_forum (forum_id, loc_id, forum_name, forum_desc, forum_last_post_time) values ('{$forum_id}', '{$city_id}', '{$forum_name}', '{$forum_desc}', '0')";
					//echo "SQL: {$sql}<br />";
					$db->q($sql);
					if ($db->affected() != 1) {
						echo "<span style=\"color: red; font-weight: bold;\">Error while running sql '{$sql}' !</span><br />\n";
						return;
					}
					$insert_cnt++;
				}
			}
		}
		if (!$live) {
			if ($missing_cnt == 0)
				continue;
			echo "For location <strong>$city_name</strong> (loc_id = $city_id) are missing <strong>$missing_cnt</strong> forums ($missing).<br />\n";
			$change = true;
		} else {
			echo "Successfully inserted {$insert_cnt} forums for location {$city_name}.<br />\n";
		}
	}

	if ($change) {
		echo "<br />Do you want to create all these forums in DB ?<br />";
		echo "<form><input type=\"hidden\" name=\"action\" value=\"forums_for_location\" />";
		echo "<input type=\"hidden\" name=\"loc_id\" value=\"{$loc_id}\" />";
		echo "<input type=\"hidden\" name=\"live\" value=\"1\" />";
		echo "<input type=\"submit\" name=\"submit\" value=\"Yes\" /></form>";
	} else {
		echo "There are no forums missing for location $loc_id.<br />\n";
	}
}

switch(GetGetParam("action")) {
	case 'forums_for_location':
		forumsForLocation();
		break;
	default:
		break;
}

global $db;

?>
<h2>Forums</h2>
<form action="" method="get">
<input type="hidden" name="action" value="forums_for_location" />
List forums for country: <?php echo location::getLocationSelect("location","loc_id", true); ?><br />
<input type="submit" name="submit" value="submit" />
</form>
