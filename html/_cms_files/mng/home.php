<?php
require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $system, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("access_admin"))
	return;

function downloadCities($cat) {
	global $db;
	$system = new system;

	switch($cat) {
		case 1: $cat = "female-escorts/"; $anchor = "escorts"; break;
		case 2: $cat = "tstv-shemale-escorts/"; $anchor = "TV/TS escorts"; break;
		case 3: $cat = "male-escorts-for-female/"; $anchor = "m4f escorts"; break;
		case 4: $cat = "male-for-male-escorts/"; $anchor = "m4m escorts"; break;
		default: $cat = ""; $anchor = "";
	}

	$fp = fopen(_CMS_ABS_PATH.'/UserFiles/excel/city.csv', 'w');
	$fields = "'City','Parent','URL','Anchor'\n";
	fputs($fp, $fields);
	$res = $db->q("SELECT l.loc_name city, l2.loc_name parent_location, 
						concat('http://', ((CASE WHEN length(l.country_sub) > 0 THEN concat(l.country_sub,'.') ELSE '' END)), 'adultsearch.com', l.loc_url,'$cat') url, 
						concat(l.loc_name, ' $anchor') anchor
					FROM location_location l
					INNER JOIN location_location l2 on l.loc_parent = l2.loc_id
					WHERE l.loc_type = 3 AND l.has_place_or_ad = 1");
	while($row=$db->r($res)) {
		$fields = "'{$row['city']}','{$row['parent_location']}','{$row['url']}','{$row['anchor']}'\n";
		fputs($fp, $fields);
	}
	fclose($fp);

	$system->moved("http://adultsearch.com/UserFiles/excel/city.csv");
}

function downloadEroticServices() {
	global $db;
	$system = new system;
	
	require(_CMS_ABS_PATH.'/_cms_files/classifieds/array.php');

	$sql = "SELECT country.loc_id as country_id, country.dir as country_dir, country.loc_name as country_name, 
					city.loc_url, city.dir as city_dir, state.loc_name as state_name, city.loc_name as city_name, cla.* 
			FROM
			(
			SELECT c.phone, c.type, c.title, c.id, MIN(cl.loc_id) as loc_id, 
			FROM classifieds c
			INNER JOIN classifieds_loc cl on cl.post_id = c.id
			LEFT JOIN classifieds_payment_done cpd on cpd.post_id = c.id
			WHERE c.done = 1 AND (c.type = 1 OR c.type = 2)
			GROUP BY c.phone, c.type, c.title, c.id
			) cla
			INNER JOIN location_location city on city.loc_id = cla.loc_id
			INNER JOIN location_location state on state.loc_id = city.loc_parent
			INNER JOIN location_location country on country.loc_id = city.country_id
			ORDER BY country_id, state_name, city_name";

	$sql = "SELECT country.loc_id as country_id, country.dir as country_dir, country.loc_name as country_name, 
					city.loc_url, city.dir as city_dir, state.loc_name as state_name, city.loc_name as city_name, cla.*
			FROM
			(
			SELECT a.phone as phone2, cc.firstname, cc.lastname, c.phone, c.email, c.website, c.type, c.title, c.id, MIN(cl.loc_id) as loc_id, cpd.`time`, cpd.total
			FROM classifieds c
			INNER JOIN account a on a.account_id = c.account_id
			LEFT JOIN account_cc cc on cc.cc_id = c.cc_id
			INNER JOIN classifieds_loc cl on cl.post_id = c.id
			LEFT JOIN 
				(
				select c2.post_id, c2.`time`, c2.total from classifieds_payment_done c2
				inner join (select post_id, max(id) as max_id from classifieds_payment_done group by post_id) c1 on c1.max_id = c2.id
				) cpd ON cpd.post_id = c.id
			WHERE c.deleted IS NULL and c.done = 1 and c.bp = 0 AND (c.type = 1 OR c.type = 5)
			GROUP BY c.phone, c.type, c.title, c.id
			) cla
			INNER JOIN location_location city on city.loc_id = cla.loc_id
			INNER JOIN location_location state on state.loc_id = city.loc_parent
			INNER JOIN location_location country on country.loc_id = city.country_id
			ORDER BY country_id, state_name, city_name";

	$fp = fopen(_CMS_ABS_PATH.'/UserFiles/excel/erotic_services.csv', 'w');
	$fields = '"First name","Last name","Phone","Site","Link","Email","Personal website","Paid on","Paid","Country","State","City","Type"'."\n";
	//$fields = '"URL","Phone","Type","Title"'."\n";
	//$fields = '"URL","Phone"'."\n";
	fputs($fp, $fields);
	$res = $db->q($sql);
	while($row=$db->r($res)) {
		//$type = $type_array[$row["type"]];
		$type = ($row["type"] == 5) ? "Agency" : "Independent";
		$url  = $url_array[$row["type"]];
		$phone = $row["phone"];
		if (empty($phone) && $row["phone2"])
			$phone = $row["phone2"];
		$phone = (strlen($phone)==10) ? preg_replace("/([0-9]{3})+([0-9]{3})+([0-9]{4})/", "$1-$2-$3", $phone) : $phone;
		if ($row["country_id"] == 16046) {
			$url = "http://adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else if ($row["country_id"] == 16047) {
			$url = "http://ca.adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else if ($row["country_id"] == 41973) {
			$url = "http://uk.adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else {
			$url = "http://{$row["country_dir"]}.adultsearch.com/{$row["city_dir"]}/{$url}/{$row["id"]}";
		}
		$fields = "\"{$row["firstname"]}\",\"{$row["lastname"]}\",\"{$phone}\",\"AS\",\"{$url}\",\"{$row["email"]}\",\"{$row["website"]}\",\"{$row["time"]}\",\"{$row["total"]}\",\"{$row["country_name"]}\",\"{$row["state_name"]}\",\"{$row["city_name"]}\",\"{$type}\"\n";
		//$fields = "\"{$url}\",\"{$phone}\",\"{$type}\",\"{$row["title"]}\"\n";
		//$fields = "\"{$url}\",\"{$phone}\"\n";
		fputs($fp, $fields);
	}
	fclose($fp);

	$system->moved("http://adultsearch.com/UserFiles/excel/erotic_services.csv");
	die;
}

function downloadTSEroticServices() {
	global $db;
	$system = new system;
	
	require(_CMS_ABS_PATH.'/_cms_files/classifieds/array.php');

	$sql = "SELECT country.loc_id as country_id, country.dir as country_dir, country.loc_name as country_name, 
					city.loc_url, city.dir as city_dir, state.loc_name as state_name, city.loc_name as city_name, cla.*
			FROM
			(
			SELECT a.phone as phone2, cc.firstname, cc.lastname, c.phone, c.email, c.website, c.type, c.title, c.id, MIN(cl.loc_id) as loc_id, cpd.`time`, cpd.total
			FROM classifieds c
			INNER JOIN account a on a.account_id = c.account_id
			LEFT JOIN account_cc cc on cc.cc_id = c.cc_id
			INNER JOIN classifieds_loc cl on cl.post_id = c.id
			LEFT JOIN 
				(
				select c2.post_id, c2.`time`, c2.total from classifieds_payment_done c2
				inner join (select post_id, max(id) as max_id from classifieds_payment_done group by post_id) c1 on c1.max_id = c2.id
				) cpd ON cpd.post_id = c.id
			WHERE c.deleted IS NULL and c.done = 1 and c.bp = 0 AND c.type = 2
			GROUP BY c.phone, c.type, c.title, c.id
			) cla
			INNER JOIN location_location city on city.loc_id = cla.loc_id
			INNER JOIN location_location state on state.loc_id = city.loc_parent
			INNER JOIN location_location country on country.loc_id = city.country_id
			ORDER BY country_id, state_name, city_name";

	$fp = fopen(_CMS_ABS_PATH.'/UserFiles/excel/ts_erotic_services.csv', 'w');
	$fields = '"First name","Last name","Phone","Site","Link","Email","Personal website","Paid on","Paid","Country","State","City"'."\n";
	fputs($fp, $fields);
	$res = $db->q($sql);
	while($row=$db->r($res)) {
		$url  = $url_array[$row["type"]];
		$phone = $row["phone"];
		if (empty($phone) && $row["phone2"])
			$phone = $row["phone2"];
		$phone = (strlen($phone)==10) ? preg_replace("/([0-9]{3})+([0-9]{3})+([0-9]{4})/", "$1-$2-$3", $phone) : $phone;
		if ($row["country_id"] == 16046) {
			$url = "http://adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else if ($row["country_id"] == 16047) {
			$url = "http://ca.adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else if ($row["country_id"] == 41973) {
			$url = "http://uk.adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else {
			$url = "http://{$row["country_dir"]}.adultsearch.com/{$row["city_dir"]}/{$url}/{$row["id"]}";
		}
		$fields = "\"{$row["firstname"]}\",\"{$row["lastname"]}\",\"{$phone}\",\"AS\",\"{$url}\",\"{$row["email"]}\",\"{$row["website"]}\",\"{$row["time"]}\",\"{$row["total"]}\",\"{$row["country_name"]}\",\"{$row["state_name"]}\",\"{$row["city_name"]}\"\n";
		fputs($fp, $fields);
	}
	fclose($fp);

	$system->moved("http://adultsearch.com/UserFiles/excel/ts_erotic_services.csv");
	die;
}

function showTSEroticServices() {
	global $db;
	$system = new system;
	
	require(_CMS_ABS_PATH.'/_cms_files/classifieds/array.php');

	$sql = "SELECT country.loc_id as country_id, country.dir as country_dir, country.loc_name as country_name, 
					city.loc_url, city.dir as city_dir, state.loc_name as state_name, city.loc_name as city_name, cla.*
			FROM
			(
			SELECT c.account_id, a.phone as phone2, cc.firstname, cc.lastname, c.phone, c.email, c.website, c.type, c.title, c.id, MIN(cl.loc_id) as loc_id, cpd.`time`, cpd.total
			FROM classifieds c
			INNER JOIN account a on a.account_id = c.account_id
			LEFT JOIN account_cc cc on cc.cc_id = c.cc_id
			INNER JOIN classifieds_loc cl on cl.post_id = c.id
			LEFT JOIN 
				(
				select c2.post_id, c2.`time`, c2.total from classifieds_payment_done c2
				inner join (select post_id, max(id) as max_id from classifieds_payment_done group by post_id) c1 on c1.max_id = c2.id
				) cpd ON cpd.post_id = c.id
			WHERE c.deleted IS NULL and c.done = 1 and c.bp = 0 AND c.type = 2
			GROUP BY c.phone, c.type, c.title, c.id
			) cla
			INNER JOIN location_location city on city.loc_id = cla.loc_id
			INNER JOIN location_location state on state.loc_id = city.loc_parent
			INNER JOIN location_location country on country.loc_id = city.country_id
			ORDER BY country_id, state_name, city_name";

	echo '<link href="/css/control.css" rel="stylesheet" type="text/css" />';
	echo "<h1>TS/TV escorts on AS</h1>\n";
	echo "<table class=\"control\">\n";
	echo "<thead><tr><th>First name</th><th>Last name</th><th>Phone</th><th>Link</th><th>Email</th><th>Personal website</th><th>Paid on</th><th>Paid</th><th>Country</th><th>State</th><th>City</th><th /></tr></thead>\n<tbody>";
	$res = $db->q($sql);
	while($row=$db->r($res)) {
		$url  = $url_array[$row["type"]];
		$phone = $row["phone"];
		if (empty($phone) && $row["phone2"])
			$phone = $row["phone2"];
		$phone = (strlen($phone)==10) ? preg_replace("/([0-9]{3})+([0-9]{3})+([0-9]{4})/", "$1-$2-$3", $phone) : $phone;
		if ($row["country_id"] == 16046) {
			$url = "http://adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else if ($row["country_id"] == 16047) {
			$url = "http://ca.adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else if ($row["country_id"] == 41973) {
			$url = "http://uk.adultsearch.com{$row["loc_url"]}{$url}/{$row["id"]}";
		} else {
			$url = "http://{$row["country_dir"]}.adultsearch.com/{$row["city_dir"]}/{$url}/{$row["id"]}";
		}

		$acc = account::findOneById($row["account_id"]);
		$red = "";
		if ($acc && (strpos($acc->getNotes(), "Called") !== false))
			$red = " style=\"background-color: #FF6666;\"";
		echo "<tr id=\"row_{$row["id"]}\" {$red}><td>".htmlspecialchars($row["firstname"])."</td><td>".htmlspecialchars($row["lastname"])."</td><td>".htmlspecialchars($phone)."</td><td><a href=\"".htmlspecialchars($url)."\" target=\"_blank\">".htmlspecialchars($url)."</a></td><td>".htmlspecialchars($row["email"])."</td><td style=\"max-width: 200px; overflow: hidden;\">".htmlspecialchars($row["website"])."</td><td>".htmlspecialchars($row["time"])."</td><td>".htmlspecialchars($row["total"])."</td><td>".htmlspecialchars($row["country_name"])."</td><td>".htmlspecialchars($row["state_name"])."</td><td>".htmlspecialchars($row["city_name"])."</td><td>";
		echo "<button data-cid=\"{$row["id"]}\" class=\"called\" >Called</button>";
		if ($_SESSION["account"] == 3974) {
			if ($acc) {
				echo "Notes:<br />".htmlspecialchars($acc->getNotes());
			}
		}
		echo "</td></tr>\n";
	}
	echo "</tbody>\n</table>\n";

echo "
<script type=\"text/javascript\">
$(document).ready(function(){
	$('.called').each(function() {
		$(this).click(function() {
			var cid = $(this).attr('data-cid');
			alert('cid='+cid);
			$.get('', { action: 'called', cid: cid }, function(data) {
				if (data == 'ok') {
					$('#row_'+cid).css('background-color','#FF6666');
					$('#row_'+cid+' button').prop('disabled','disabled');
				}
				});
		});
	});
});
</script>
";

	return false;
}

function downloadBpEmails() {
	global $db;
	$system = new system;

	$fp = fopen(_CMS_ABS_PATH.'/UserFiles/excel/bp_emails.csv', 'w');
	$fields = "\"Email\"\n";
	fputs($fp, $fields);
	$res = $db->q("SELECT email FROM bp_emails");
	while($row=$db->r($res)) {
		$fields = "\"{$row['email']}\"\n";
		fputs($fp, $fields);
	}
	fclose($fp);

	$system->moved("http://adultsearch.com/UserFiles/excel/bp_emails.csv");
}

if ($_REQUEST["action"] == "called") {
	$cid = intval($_REQUEST["cid"]);
	$clad = clad::findOneById($cid);
	if (!$clad)
		die("error");
	$acc= $clad->getAccount();
	if (!$acc)
		die("error");
	global $account;
	$acc->setNotes($acc->getNotes()."\nCalled at ".date("Y-m-d H:i")." by #".$account->getId()."\n");
	$acc->update();
	echo "ok";
	die;
}

switch(GetGetParam("action")) {
	case 'downloadCities':
		downloadCities($_GET['cat']);
		break;
	case 'downloadEroticServices':
		downloadEroticServices();
		break;
	case 'showTSEroticServices':
		showTSEroticServices();
		return;
		break;
	case 'downloadTSEroticServices':
		downloadTSEroticServices();
		break;
	case 'downloadBpEmails':
		downloadBpEmails();
		break;
	default:
		break;
}

//dashboard
$lcu_status = "red";
$lcu_time = NULL;
$lcu_index = NULL;
$res = $db->q("SELECT * FROM admin_operation_log WHERE code = 'last_cache_update'");
if ($db->numrows($res)) {
	$row = $db->r($res);
	$lcu_time = $row["stamp"];
	$lcu_index = $row["msg"];
}
if ($lcu_time) {
	$diff = time() - $lcu_time;
	if ($diff < 3600)
		$lcu_status = "green";
	else if ($diff < 6*3600)
		$lcu_status = "orange";
}

$lab_status = "red";
$lab_time = NULL;
$lab_msg = NULL;

$res = $db->q("SELECT * FROM admin_operation_log WHERE code = 'last_advertise_budget'");
if ($db->numrows($res)) {
	$row = $db->r($res);
	$lab_time = $row["stamp"];
	$lab_msg = $row["msg"];
}
if ($lab_time) {
	$diff = time() - $lab_time;
	if ($diff < 7*86400)	//1 week
		$lab_status = "green";
	else if ($diff < 30*86400)	//1 month
		$lab_status = "orange";
}

/*
$lbo_status = "red";
$lbo_time = NULL;
$lbo_msg = NULL;
$res = $db->q("SELECT * FROM admin_operation_log WHERE code = 'last_businessowner'");
if ($db->numrows($res)) {
	$row = $db->r($res);
	$lbo_time = $row["stamp"];
	$lbo_msg = $row["msg"];
}
if ($lbo_time) {
	$diff = time() - $lbo_time;
	if ($diff < 2592000)	//1month
		$lbo_status = "green";
	else
		$lbo_status = "orange";
}
*/

$acc_status = "red";
$acc_cnt = 0;
$res = $db->q("SELECT count(*) as cnt FROM account WHERE created_stamp > ?", [time() - 86400]);
if ($db->numrows($res)) {
	$row = $db->r($res);
	$acc_cnt = $row["cnt"];
}
if ($acc_cnt) {
	if ($acc_cnt > 6)
		$acc_status = "green";
	else if ($acc_cnt > 0)
		$acc_status = "orange";
}

$cla_status = "red";
$cla_cnt = 0;
$res = $db->q("SELECT count(*) as cnt FROM classifieds WHERE ( done = 1 OR done = 3 ) AND created > ?", array(time() - 86400));
if ($db->numrows($res)) {
	$row = $db->r($res);
	$cla_cnt = $row["cnt"];
}
if ($cla_cnt) {
	if ($cla_cnt > 0)	//TODO
		$cla_status = "green";
	else if ($cla_cnt > 0)
		$cla_status = "orange";
}

$spo_status = "red";
$res = $db->q("SELECT * FROM classifieds_sponsor_log ORDER BY `date` DESC LIMIT 1");
if ($db->numrows($res)) {
	$row = $db->r($res);
	$spo_date = $row["date"];
	$spo_time = strtotime($spo_date);
	$spo_id = $row["post_id"];	
	$spo_account_id = $row["account_id"];	
}
if ($spo_time) {
	$diff = time() - $spo_time;
	if ($diff < 3*86400)  //3 day
		$spo_status = "green";
	else if ($diff < 7*86400)   //7 week
		$spo_status = "orange";
}

/*
//forwarding emails stopped working on BP in june 2015
$bpe_status = "red";
$bpe_msg = "Error";
$res = $db->q("SELECT COUNT(*) as cnt FROM bp_emails WHERE inserted > ?", array(time() - 86400));
if ($db->numrows($res)) {
	$row = $db->r($res);
	$cnt = intval($row["cnt"]);
	if ($cnt > 5) {
		$bpe_status = "green";
	} else if ($cnt > 0) {
		$bpe_status = "orange";
	}
	$bpe_msg = intval($row["cnt"])." emails grabbed in last 24 hours";
}
*/

$lsg_status = "red";
$lsg_time = NULL;
$lsg_msg = NULL;
$res = $db->q("SELECT * FROM admin_operation_log WHERE code = 'last_sitemap_generated'");
if ($db->numrows($res)) {
	$row = $db->r($res);
	$lsg_time = $row["stamp"];
	$lsg_msg = $row["msg"];
}
if ($lsg_time) {
	$diff = time() - $lsg_time;
	if ($diff < 86400)	//1 day
		$lsg_status = "green";
	else if ($diff < 7*86400)	//1 week
		$lsg_status = "orange";
}

//last clad claim
/*
$lcc_status = "red";
$lcc_time = NULL;
$lcc_msg = NULL;
$res = $db->q("SELECT * FROM admin_operation_log WHERE code = 'last_clad_claim'");
if ($db->numrows($res)) {
	$row = $db->r($res);
	$lcc_time = $row["stamp"];
	$lcc_msg = $row["msg"];
}
if ($lcc_time) {
	$diff = time() - $lcc_time;
	if ($diff < 2592000)	//1month
		$lcc_status = "green";
	else if ($diff < 3*2592000)	//3months
		$lcc_status = "orange";
}
*/

//last purchase
$lp_status = "red";
$lp_time = NULL;
$lp_msg = NULL;
$res = $db->q("SELECT * FROM account_purchase ORDER BY id DESC LIMIT 1");
if ($db->numrows($res)) {
	$row = $db->r($res);
	$lp_time = $row["transaction_time"];
	$lp_msg = "Account #{$row["account_id"]} - \${$row["total"]} for {$row["what"]} #{$row["item_id"]}";
}
if ($lp_time) {
	$diff = time() - $lp_time;
	if ($diff < 86400)			//1day
		$lp_status = "green";
	else if ($diff < 3*86400)	//3days
		$lp_status = "orange";
}


if ($account->isrealadmin()) {
	$ser_status = "red";
	$ser_time = NULL;
	$ser_msg = NULL;
	$err_log_file = _CMS_ABS_PATH."/../data/log/php_error_log";
	if (!is_readable($err_log_file)) {
		$ser_msg = "Error log file '{$err_log_file}' does not exist or is not readable!";
	} else {
		$handle = fopen($err_log_file, "r");
		$cursor = -1;
		fseek($handle, $cursor, SEEK_END);
		$char = fgetc($handle);
		// Trim trailing newline chars of the file
		while ($char === "\n" || $char === "\r") {
			fseek($handle, $cursor--, SEEK_END);
			$char = fgetc($handle);
		}
		$last_char = "";
		// Read until the start of file or first newline char
		while ($char !== false && ($char !== "\n" || $last_char !== "[")) {
			// Prepend the new char
			$line = $char.$line;
			fseek($handle, $cursor--, SEEK_END);
			$last_char = $char;
			$char = fgetc($handle);
		}
		$pos = strpos($line, "]");
		if ($pos === false) {
			$ser_msg = "Unknown format of error log entry: '{$line}'";
		} else {
			$time_str = substr($line, 1, $pos-1);
			$ser_time = strtotime(trim($time_str));
			$now = time();
			if ($ser_time < $now-(7*86400))
				$ser_status = "green";
			else if ($ser_time < $now-(3*86400))
				$ser_status = "orange";

			$line = substr($line, $pos+2);
			$pos = strpos($line, "]");
			if ($pos !== false) {
				$ser_msg = substr($line, $pos+2);
			} else {
				$ser_msg = $line;
			}
		}
	}

	$spl_status = "green";
	$spl_cnt = 0;
	$spl_cnt = $db->single("SELECT count(*) FROM spool_gmail WHERE succeeded IS NULL", []);
	if ($spl_cnt) {
		if ($spl_cnt > 20)
			$spl_status = "red";
		else if ($spl_cnt > 10)
			$spl_status = "orange";
	}

	$sg_status = "green";
	$sg_usage = 0;
	global $config_sendgrid_api_key;
	$sg = new \SendGrid($config_sendgrid_api_key);
	$query_params = json_decode('{"aggregated_by": "month", "limit": 1, "start_date": "'.date('Y-m').'-01", "end_date": "'.date('Y-m-d').'", "offset": 1}');
	$response = $sg->client->stats()->get(null, $query_params);
	if ($response->statusCode() == 200) {
		$b = json_decode($response->body(), true);
		if (is_array($b)
			&& array_key_exists("stats", $b[0])
			&& is_array($b[0]["stats"])
			&& is_array($b[0]["stats"][0])
			&& array_key_exists("metrics", $b[0]["stats"][0])
			&& is_array($b[0]["stats"][0]["metrics"])
			&& array_key_exists("requests", $b[0]["stats"][0]["metrics"])
			) {
			$requests = intval($b[0]["stats"][0]["metrics"]["requests"]);
			$sg_usage = round($requests / (100000 / 100), 2);	//percentage
			if ($sg_usage > 95)
				$sg_status = "red";
			else if ($sg_usage > 85)
				$sg_status = "orange";
		}
	} else {
		$sg_status = "red";
	}
}


//-----------------------------
//submitted places not put live
/*
$sub_no_live = "";
$res = $db->q("SELECT id FROM eroticmassage WHERE edit = 0 ORDER BY created DESC LIMIT 10");
while($row = $db->r($res)) {
	$sub_no_live .= "<a href=\"/worker/emp?id={$row["id"]}\">#{$row["id"]}</a>, ";
}
*/


//-------------------
//waiting ads from last week
$week_ago = time() - (7*86400);
$res = $db->q("SELECT c.id, c.`date` 
				FROM classifieds c 
				WHERE c.done = 3 AND c.deleted IS NULL AND c.account_id <> 3974 AND (created > ? OR `date` > NOW() - INTERVAL 1 WEEK)
				ORDER BY created DESC LIMIT 200",
	array($week_ago));
$waiting_ads = "";
$waiting_cnt = 0;
while($row = $db->r($res)) {
	$id = $row["id"];
	if (!empty($waiting_ads))
		$waiting_ads .= ", ";
	$waiting_ads .= "<a href=\"/mng/classifieds?cid={$id}\">{$id}</a>";
	$waiting_cnt++;
}
$waiting_status = "green";
if ($waiting_cnt > 3)
	$waiting_status = "red";
else if ($waiting_cnt > 0)
	$waiting_status = "orange";

//----------------------
//VPN IP addresses usage
$month_ago = time() - (30*86400);
$hour_ago = time() - 3600;
$res = $db->q("
	SELECT s.id, s.ip_address, s.last_reserved_stamp, c.id ccbill_post_id
	FROM vpn_server s
	LEFT JOIN ccbill_post c on c.ip_address = s.ip_address and c.stamp > ?",
	array($month_ago)
	);
$vpn_total = $vpn_used = $vpn_reserved = $vpn_free = 0;
while ($row = $db->r($res)) {
	$vpn_total++;
	if ($row["ccbill_post_id"])
		$vpn_used++;
	else if ($row["last_reserved_stamp"] && intval($row["last_reserved_stamp"]) > $hour_ago)
		$vpn_reserved++;
	else
		$vpn_free++;
}
$vpn_status = "green";
if ($vpn_free == 0)
	$vpn_status = "red";
else if ($vpn_free < 10)
	$vpn_status = "orange";
$vpn_message = "Out of total {$vpn_total} IPs: {$vpn_used} are used, {$vpn_reserved} are reserved, {$vpn_free} are free";

//----------------------------
//deleted and not refunded ads
$res = $db->q("SELECT c.id, c.`date` FROM classifieds c WHERE c.done IN (1,3) AND c.deleted IS NOT NULL AND c.account_id <> 3974 ORDER BY c.deleted DESC LIMIT 200");
$last_deleted_ads = array();
while($row = $db->r($res)) {
	$id = $row["id"];
	$deleted = $row["deleted"];
	$last_deleted_ads[$id] = $deleted;
}
$res = $db->q("SELECT ap.item_id, ROUND(SUM(ap.total),2) as total FROM account_purchase ap WHERE ap.what = 'classifieds' and ap.total > 0 GROUP BY ap.item_id ORDER BY id DESC LIMIT 500");
$last_ad_purchases = array();
while($row = $db->r($res)) {
	$item_id = $row["item_id"];
	$total = $row["total"];
	$last_ad_purchases[$item_id] = $total;
}
$del_no_ref = "";
foreach ($last_deleted_ads as $key => $val) {
	if (!array_key_exists($key, $last_ad_purchases))
		continue;
	$del_no_ref .= (!empty($del_no_ref)) ? ", " : "";
	$del_no_ref .= "<a href=\"/mng/classifieds?cid={$key}\">{$key}</a>";
}

//----------------------------
//incostistence between classifieds.done and classifieds_loc.done
$res = $db->q("SELECT c.id, c.done, l.done
				FROM classifieds c
				LEFT JOIN classifieds_loc l on l.post_id = c.id
				WHERE c.done <> l.done and c.done = 1
				ORDER BY c.id desc");
$done_inconsistence = "";
while ($row = $db->r($res)) {
	$id = $row["id"];
	if (!empty($done_inconsistence))
		$done_inconsistence .= ", ";
	$done_inconsistence .= "<a href=\"/mng/classifieds?cid={$id}\">{$id}</a>";
}

//-------
//duples in classifed_loc
$res = $db->q("select loc_id, post_id, count(*), max(done) as max_done
from classifieds_loc
group by loc_id, neighbor_id, post_id
having count(*) > 1
order by 3 desc");
$classifieds_loc_duples = "";
while ($row = $db->r($res)) {
	$id = $row["post_id"];
	$max_done = $row["max_done"];
	if ($max_done <= 0)
		continue;
	if (!empty($classifieds_loc_duples))
		$classifieds_loc_duples .= ", ";
	$classifieds_loc_duples .= "<a href=\"/mng/classifieds?cid={$id}\">{$id}</a>";
}
//-------------
//important ads
$imp_ads = array();
//clads
$clad_exp_threshold = 86400 * 7;	//1 week - warning threshold for classified ads in seconds
$res = $db->q("SELECT c.id, c.title, a.account_id, a.email, c.done, UNIX_TIMESTAMP(c.expires) as expires, c.deleted
				FROM admin_important ai
				INNER JOIN classifieds c on c.id = ai.item_id
				INNER JOIN account a on a.account_id = c.account_id
				WHERE ai.type = 'CLA'");
while ($row = $db->r($res)) {
	$status = "red";
	$text = "";
	if ($row["done"] != 1 || $row["deleted"]) {
		$text = "<strong>Not live!</strong> (done status = {$row["done"]}, deleted='{$row["deleted"]}')";
	} else {
		$expires = $row["expires"];
		if ($expires < time()) {
			//this should not happen
			$text = "<strong>Classified ad already expired!</strong> (expired on ".date("m/d/Y H:i:s T", $expires)." - ".time_elapsed_string($expires).")";
		} else if ($expires < (time() + $clad_exp_threshold)) {
			$status = "orange";
			$text = "<strong>Classified ad will expire on ".date("m/d/Y H:i:s T", $expires)." - in ".time_elapsed_string($expires)." !</strong>";
		} else {
			$status = "green";
			$text = "Live, expires on ".date("m/d/Y", $expires)." (in ".time_elapsed_string($expires).")";
			//TODO additional checks ?
		}
	}
	$title = $row["title"];
	if (strlen($title) > 30)
		$title = substr($title, 0, 27)."...";
	$imp_ads[] = array("type" => "CLA", "id" => $row["id"], "title" => $title, "acc_id" => $row["account_id"], "acc_email" => $row["email"], "status" => $status, "text" => $text);
}
//adv.campaigns
$budget_threshold = 5000;	//warning threshold for advertise campaign budget
$res = $db->q("SELECT ac.id, ac.name, a.account_id, a.email, ab.budget, COUNT(acs.s_id) as cnt_sections
				FROM admin_important ai
				INNER JOIN advertise_campaign ac on ac.id = ai.item_id
				INNER JOIN account a on ac.account_id = a.account_id
				LEFT JOIN advertise_budget ab on ab.account_id = a.account_id
				LEFT JOIN advertise_camp_section acs on acs.c_id = ac.id and acs.status = 1 and acs.approved = 1 and acs.show = 1
				WHERE ai.type = 'ACA'
				GROUP BY ac.id, ac.name, a.account_id, a.email, ab.budget");
while ($row = $db->r($res)) {
	$status = "red";
	$text = "";
	$budget = $row["budget"];
	if ($budget < 0.1) {
		$text = "<strong>Out of budget!</strong>";
	} else if ($budget < $budget_threshold) {
		$status = "orange";
		$text = "<strong>Will run out of budget soon!</strong> current budget \$".$budget;
	} else {
		$status = "green";
		$text = "Live, budget is \${$budget}";
		//TODO additional checks ?
	}
	$imp_ads[] = array("type" => "ACA", "id" => $row["id"], "name" => $row["name"], "acc_id" => $row["account_id"], "acc_email" => $row["email"], "status" => $status, "text" => $text);
}
?>

<style type="text/css">
div.mnghome {
	float: left;
}
div.mnghome:not(:first-child) {
	margin-left: 50px;
}
.status_table {
	max-width: 900px;
	border-collapse: collapse;
}
.status_table td, .status_table th {
	vertical-align: middle;
	border: 1px solid #ccc;
	padding: 2px;
}
.status_table .green, .status_table .orange, .status_table .red {
	width: 20px;
}
.status_table .green {
	background-color: green;
}
.status_table .orange {
	background-color: orange;
}
.status_table .red {
	background-color: red;
}
</style>

<div>

<div class="mnghome">
	<h2>Quicksearch</h2>
	Email:<br />
	<form action="/mng/accounts" method="get"><input type="text" size="20" name="email" value="" /><input type="submit" name="lookup" value="Go" /></form>
	<br />
	Account id#: <br />
	<form action="/mng/accounts" method="get"><input type="text" size="10" name="account_id" value="" /><input type="submit" name="lookup" value="Go" /></form>
	<br />
	Cl.ad id#:<br />
	<form action="/mng/classifieds" method="get"><input type="text" size="10" name="cid" value="" /><input type="submit" name="lookup" value="Go" /></form>
	<br />
	Transaction id#:<br />
	<form action="/mng/sales" method="get" id="txn_lkp_form" ><input type="text" size="20" name="transaction_id" id="txn_lkp_input" value="" /><input type="submit" name="lookup" id="txn_lkp_submit" value="Go" /></form>
	<br />
</div>

<div class="mnghome">
	<h2>Dashboard</h2>
	<table class="status_table">
<?php
if ($account->isadmin()) {
	echo "<tr><th>Last index rotated: </th><td class=\"{$lcu_status}\"></td><td>index {$lcu_index} - <strong>".time_elapsed_string($lcu_time)."</strong> (".date("H:i:s m/d/Y T", $lcu_time).")</td></tr>";
	echo "<tr><th>Account signups: </th><td class=\"{$acc_status}\"></td><td><strong>{$acc_cnt}</strong> signups in last 24 hours</td></tr>";
}
if (permission::has("classified_manage")) {
	echo "<tr><th>New classified ads: </th><td class=\"{$cla_status}\"></td><td><strong>{$cla_cnt}</strong> new classified ads in last 24 hours</td></tr>";
}
if ($account->isadmin()) {
	echo "<tr><th>Last sponsorship bought: </th><td class=\"{$spo_status}\"></td><td>On <strong>{$spo_date}</strong> cl.ad#{$spo_id} - acc#id{$spo_account_id} - ".time_elapsed_string($spo_time)." (".date("H:i:s m/d/Y T", $spo_time).")</td></tr>";
	echo "<tr><th>Last adv.budget upping: </th><td class=\"{$lab_status}\"></td><td>{$lab_msg} - <strong>".time_elapsed_string($lab_time)."</strong> (".date("H:i:s m/d/Y T", $lab_time).")</td></tr>";
	/*
	echo "<tr><th>Last businessowner: </th><td class=\"{$lbo_status}\"></td><td>{$lbo_msg} - <strong>".time_elapsed_string($lbo_time)."</strong> (".date("H:i:s m/d/Y T", $lbo_time).")</td></tr>";
	echo "<tr><th>BP emails: </th><td class=\"{$bpe_status}\"></td><td>{$bpe_msg}</td></tr>";
	*/
	echo "<tr><th>Last sitemap generated: </th><td class=\"{$lsg_status}\"></td><td>{$lsg_msg} - <strong>".time_elapsed_string($lsg_time)."</strong> (".date("H:i:s m/d/Y T", $lsg_time).")</td></tr>";
}
/*
if (permission::has("classified_manage")) {
	echo "<tr><th>Last cl.ad claim: </th><td class=\"{$lcc_status}\"></td><td>{$lcc_msg} - <strong>".time_elapsed_string($lcc_time)."</strong> (".date("H:i:s m/d/Y T", $lcc_time).")</td></tr>";
}
*/
if ($account->isadmin()) {
	echo "<tr><th>Last purchase: </th><td class=\"{$lp_status}\"></td><td>{$lp_msg} - <strong>".time_elapsed_string($lp_time)."</strong> (".date("H:i:s m/d/Y T", $lp_time).")</td></tr>";
}
if ($account->isrealadmin()) {
	echo "<tr><th>Last SQL error: </th><td class=\"{$ser_status}\"></td><td style=\"max-width: 300px;\">{$ser_msg} - <strong>".time_elapsed_string($ser_time)."</strong> (".date("H:i:s m/d/Y T", $ser_time).")</td></tr>";
	echo "<tr><th>Emails spooled: </th><td class=\"{$spl_status}\"></td><td><strong>{$spl_cnt}</strong> emails waiting to be sent out</td></tr>";
	echo "<tr><th>Sendgrid viphost usage: </th><td class=\"{$sg_status}\"></td><td><strong>{$sg_usage}%</strong> quota used</td></tr>";
}
if ($del_no_ref) {
	echo "<tr><th>Deleted and not refunded ads:</th><td class=\"red\"></td><td>{$del_no_ref}</td></tr>\n";
}
if ($sub_no_live) {
	echo "<tr><th>Places not live:</th><td class=\"red\"></td><td>{$sub_no_live}</td></tr>\n";
}
if ($account->isrealadmin()) {
	if ($done_inconsistence)
		echo "<tr><th>Incorrect done in classifieds_loc:</th><td class=\"red\"></td><td>{$done_inconsistence}</td></tr>\n";
	if ($classifieds_loc_duples)
		echo "<tr><th>Classifieds_loc duples:</th><td class=\"red\"></td><td>{$classifieds_loc_duples}</td></tr>\n";
}
if (permission::has("classified_manage") && $waiting_ads) {
	echo "<tr><th><a href=\"/mng/classifieds?status=3&deleted=no\">Waiting ads</a>:</th><td class=\"{$waiting_status}\"></td><td>{$waiting_ads}</td></tr>\n";
}
echo "<tr><th>VPN IP usage: </th><td class=\"{$vpn_status}\"></td><td>{$vpn_message}</td></tr>";
?>
	</table>

<?php
if (permission::has("classified_important") && count($imp_ads)) {
	echo "<h2>Important ads</h2>";
	echo "<table class=\"status_table\">";
	foreach ($imp_ads as $imp) {
		echo "<tr>";
		if ($imp["type"] == "CLA")
			echo "<td>Cl.ad <a href=\"/mng/classifieds?cid={$imp["id"]}\">#{$imp["id"]}</a> - ".htmlspecialchars($imp["title"])." (<a href=\"/mng/accounts?account_id={$imp["acc_id"]}\">{$imp["acc_email"]}</a>)</td>";
		else if ($imp["type"] == "ACA")
			echo "<td>Adv.camp. <a href=\"#\">#{$imp["id"]}</a> - {$imp["name"]} (<a href=\"/mng/accounts?account_id={$imp["acc_id"]}\">{$imp["acc_email"]}</a>)</td>";
		else
			echo "<td />";
		echo "<td class=\"{$imp["status"]}\"></td><td>{$imp["text"]}</td></tr>";
	}
	echo "</table>";
}
?>
</div>


<?php
	if ($account->isadmin()) {
	?>
<div class="mnghome">
	<h2>Download CSV files</h2>
<!--<br /><br /><h3>Download CSV files:</h3>-->
	<a href="?action=downloadCities&cat=0"><img src="/images/csv_icon_16.gif" />Cities for Landing Page</a><br />
	<a href="?action=downloadCities&cat=1"><img src="/images/csv_icon_16.gif" />Cities for Female Escorts</a><br />
	<a href="?action=downloadCities&cat=2"><img src="/images/csv_icon_16.gif" />Cities for TV TS</a><br />
	<a href="?action=downloadCities&cat=3"><img src="/images/csv_icon_16.gif" />Cities for Male for Female</a><br />
	<a href="?action=downloadCities&cat=4"><img src="/images/csv_icon_16.gif" />Cities for Male for Male</a><br />
	<br />
	<a href="?action=downloadEroticServices"><img src="/images/csv_icon_16.gif" />Telephone numbers of every ad in erotic services</a><br />
	<br />
	<a href="?action=showTSEroticServices">TS/TV erotic services</a><br />
	<a href="?action=downloadTSEroticServices"><img src="/images/csv_icon_16.gif" />TS/TV erotic services</a><br />
	<br />
	<a href="?action=downloadBpEmails"><img src="/images/csv_icon_16.gif" />BP emails</a><br />
</div>
	<?php
	}
?>

<br style="clear: both;"/>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$('#txn_lkp_form').submit(function() {
		var txn_val = $('#txn_lkp_input').val();
		if (txn_val.substring(0,1) != '0' && txn_val.length == 18) {
			$('#txn_lkp_input').val('0' + txn_val);
		}
		return true;
	});
});
</script>
