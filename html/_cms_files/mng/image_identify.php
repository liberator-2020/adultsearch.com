<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) {
	$account->asklogin();
	return false;
}

if (!$account->isrealadmin()) {
	echo "You dont have privileges to access this page!";
	die;
}

//trying to identify to what places images belong
$img_dir = _CMS_ABS_PATH."/tmp/image_mass_upload/upload";
echo "img_dir = '{$img_dir}'<br />";
if (($dir = opendir($img_dir)) === false) {
    echo "<span style=\"color: red;\">Error while opendir !</span><br />";
    return false;
}
echo "<table>";
while (false !== ($file = readdir($dir))) {
    if($file == '.' || $file == '..')
        continue;
    $files[] = $file;
}
closedir($dir);

if (count($files) == 0) {
	echo "No files.<br />";
}

sort($files);
foreach ($files as $file) {
    $pos = strpos($file,"_");
    if ($pos === false)
        continue;
    $id = intval(substr($file, 0, $pos));
    $emp_name = "";
    $sc_name = "";
    $as_name = "";
    $br_name = "";
    $lif_name = "";
    $lin_name = "";
    $ga_name = "";
    $gb_name = "";
    $res = $db->q("SELECT name FROM eroticmassage WHERE id = ? LIMIT 1", array($id));
    if ($db->numrows($res) == 1) {
        $row = $db->r($res);
        $emp_name = $row["name"];
    }
    $res = $db->q("SELECT name FROM strip_club2 WHERE id = ? LIMIT 1", array($id));
    if ($db->numrows($res) == 1) {
        $row = $db->r($res);
        $sc_name = $row["name"];
    }
    $res = $db->q("SELECT name FROM adult_stores WHERE id = ? LIMIT 1", array($id));
    if ($db->numrows($res) == 1) {
        $row = $db->r($res);
        $as_name = $row["name"];
    }
	$res = $db->q("SELECT name FROM brothel WHERE id = ? LIMIT 1", array($id));
    if ($db->numrows($res) == 1) {
        $row = $db->r($res);
        $br_name = $row["name"];
    }
    $res = $db->q("SELECT name FROM lifestyle WHERE id = ? LIMIT 1", array($id));
    if ($db->numrows($res) == 1) {
        $row = $db->r($res);
        $lif_name = $row["name"];
    }
    $res = $db->q("SELECT name FROM lingeriemodeling1on1 WHERE id = ? LIMIT 1", array($id));
    if ($db->numrows($res) == 1) {
        $row = $db->r($res);
        $lin_name = $row["name"];
    }
    $res = $db->q("SELECT name FROM gay WHERE id = ? LIMIT 1", array($id));
    if ($db->numrows($res) == 1) {
        $row = $db->r($res);
        $ga_name = $row["name"];
    }
    $res = $db->q("SELECT name FROM gaybath WHERE id = ? LIMIT 1", array($id));
    if ($db->numrows($res) == 1) {
        $row = $db->r($res);
        $gb_name = $row["name"];
    }
    echo "<tr><td>{$file}</td><td>{$id}</td><td>{$emp_name}</td><td>{$sc_name}</td><td>{$as_name}</td><td>{$br_name}</td><td>{$lif_name}</td><td>{$lin_name}</td><td>{$ga_name}</td><td>{$gb_name}</td></tr>\n";
}
echo "</table>";
echo "Done.";
return;

?>
