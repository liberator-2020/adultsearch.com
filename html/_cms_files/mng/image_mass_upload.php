<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");

define('MAIN_DIR', _CMS_ABS_PATH."/tmp/image_mass_upload");
define('UPL_DIR', MAIN_DIR."/upload");
define('FULL_MIN_WIDTH', 500);
define('FULL_STANDARD_WIDTH', 700);
define('FULL_MAX_HEIGHT', 500);
//define('THUMB_WIDTH', 220);
define('THUMB_WIDTH',329);
define('WATERMARK_PATH', _CMS_ABS_PATH.'/images/watermark.png');

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) {
	$account->asklogin();
	return false;
}

if (!$account->isadmin()) {
	echo "You dont have privileges to access this page!";
	die;
}

//main check	
if (!is_dir(MAIN_DIR)) {
	echo "<span style=\"color: red;\">Image upload main dir '".MAIN_DIR."' does not exists !</span><br />";
	return false;
}

set_time_limit(0);
$start = microtime();

function prepend_dir(&$item, $key, $dir) {
	$item = "{$dir}/{$item}";
}

function search_dir($path) {
	if (($dir = opendir($path)) === false) {
		echo "<span style=\"color: red;\">Error while opendir !</span><br />";
		return false;
	}
	$imgs_matched = $imgs_not_matched = $imgs_skipped = [];
	while (false !== ($file = readdir($dir))) {
		if($file == '.' || $file == '..')
			continue;
		if (is_dir($path."/".$file)) {
			$arr = search_dir($path."/".$file);
			list($im, $inm, $is) = $arr;
			array_walk($im, 'prepend_dir', $file);
			array_walk($inm, 'prepend_dir', $file);
			$imgs_matched = array_merge($imgs_matched, $im);
			$imgs_not_matched = array_merge($imgs_not_matched, $inm);
			$imgs_skipped = array_merge($imgs_skipped, $is);
			continue;
		}
		if (preg_match('/^[0-9]+_.*\.(jpg|png)$/i', $file) == 1) {
			$imgs_matched[] = $file;
		} else if (preg_match('/^.*.(jpg|png)$/i', $file) == 1) {
			$imgs_not_matched[] = $file;
		} else {
			$imgs_skipped[] = $file;
		}
	}
	closedir($dir);
	return array($imgs_matched, $imgs_not_matched, $imgs_skipped);
}

function recursive_remove_directory($directory, $empty = FALSE) {
	if(substr($directory,-1) == '/') {
		$directory = substr($directory,0,-1);
	}
	if(!file_exists($directory) || !is_dir($directory))	{
		return FALSE;
	} elseif(is_readable($directory)) {
		$handle = opendir($directory);
		while (FALSE !== ($item = readdir($handle))) {
			if($item != '.' && $item != '..') {
				$path = $directory.'/'.$item;
				if(is_dir($path)) {
					recursive_remove_directory($path);
				} else {
					unlink($path);
				}
			}
		}
		closedir($handle);
		if($empty == FALSE) {
			if(!rmdir($directory)) {
				return FALSE;
			}
		}
	}
	return TRUE;
}

function save_to_db($table, $img_dir, $id, $image, $thumb, $extra) {
	global $db, $account;

	if ($extra) {
		$res = $db->q("INSERT INTO place_picture (place_file_type_id, id, module, filename, thumb, visible, account_id) VALUES (?, ?, ?, ?, ?, ?, ?)", 
						array(0, $id, $img_dir, $image, $thumb, 1, $account->getId()));
		$insertid = $db->insertid();
		if ($insertid)
			return true;
		return false;
	}

	$update = "";
	$update_params = array();
	if ($image) {
		$update .= " image = ? ";
		$update_params[] = $image;
	}
	if ($thumb) {
		if (!empty($update))
			$update .= " , ";
		$update .= " thumb = ? ";
		$update_params[] = $thumb;
	}
	$update_params[] = $id;

	$id_col_name = "id";
	if ($table == "place")
		$id_col_name = "place_id";

	$res = $db->q("UPDATE {$table} SET {$update} WHERE {$id_col_name} = ?", $update_params);
	$aff = $db->affected($res);
	if ($aff == 1)
		return true;
	return false;
}

function process() {
	global $db, $config_image_path;

	$system = new system;

	//checks
	if (!is_file(WATERMARK_PATH)) {
		echo "<span style=\"color: red;\">Could not find watermark file at path '".WATERMARK_PATH."' !</span><br />";
		return false;
	}
	if (!is_dir(UPL_DIR)) {
		echo "<span style=\"color: red;\">Tmp upload dir '".UPL_DIR."' does not exists !</span><br />";
		return false;
	}

	$live = false;
	if ($_REQUEST["live"] == 1)
		$live = true;

	$table = $_REQUEST["table"];
	if (!in_array($table,array("place","adult_stores","eroticmassage","strip_club2"))) {
		echo "<span style=\"color: red;\">Unsupported table: '{$table}' !</span><br />";
		return false;
	}

	if (!$live && $_REQUEST["submit"] != "Current") {
		//empty upload tmp directory
		echo "Emptying directory '".UPL_DIR."'...<br />\n";
		recursive_remove_directory(UPL_DIR, true);

		$zip_filepath = "";

		if ($_REQUEST["submit"] == "Select") {
			//we chose zip file from disk
			$filename = $_REQUEST["filename"];
			if ($filename == "") {
				echo "Error: No file selected !<br />\n";
				return false;
			}
			$path = MAIN_DIR."/".$filename;
			if (!file_exists($path)) {
				echo "Error: selected file '{$path}' does not exist !<br />\n";
				return false;
			}
			$ret = copy($path, UPL_DIR."/".$filename);
			if (!$ret) {
				echo "Error: copying file '{$path}' to directory '".UPL_DIR."' failed !<br />\n";
				return false;
			}
			$zip_filepath = UPL_DIR."/".$filename;
		} else if ($_REQUEST["submit"] == "Upload") {
			//we chose file upload, lets move zip file to upl folder
			$zip = new Upload($_FILES['file']);
			if (!$zip->uploaded) {
				echo "<span style=\"color: red;\">Error: upload failed !</span><br />";
				return false;
			}
			$zip->Process(UPL_DIR."/");
			if (!$zip->processed) {
				echo "<span style=\"color: red;\">Error while copying uplaoded zip file to directory '".UPL_DIR."' !</span><br />";
				return false;
			}
			$zip_filepath = "./".$zip->file_dst_name;
		} else {
			echo "Error: invalid submit value submitted: '".$_REQUEST["submit"]."' !<br />\n";
			return false;
		}

		//unzip
		$olddir = getcwd();
		chdir(UPL_DIR);
		$command = "/usr/bin/unzip \"{$zip_filepath}\" > ".UPL_DIR."/unzip_error 2>&1";
		$line = system($command, $ret);
		chdir($olddir);
		if ($ret != 0) {
			echo "<span style=\"color: red;\">Error while unzipping file !</span><br />";
			return false;
		}
	}

	//go recursively through all files, gather <number>_whatever.jpg ones
	$arr = search_dir(UPL_DIR);
	if ($arr === false) {
		echo "<span style=\"color: red;\">Error while opendir !</span><br />";
		return false;
	}
	list($im, $inm, $is) = $arr;
	
	echo "Matched images: ".count($im)."<br />\n";
	echo "Not matched images: ".count($inm)."<br />\n";
	echo "Skipped images: ".count($is)."<br />\n";

	$cnt_big = 0;
	$cnt_full = 0;
	$cnt_thumb = 0;
	$place_ids = array();

	//check places for gathered images
	foreach ($im as $rel_path) {
		$abs_path = UPL_DIR."/".$rel_path;
		$filename = basename($rel_path);
		$ret = preg_match('/^([0-9]+).*\.(jpg|png)/i', $filename, $matches);
		if ($ret != 1) {
			//this should not happen
			continue;
		}
		$id = intval($matches[1]);
		if ($id == 0) {
			//this should not happen
			continue;
		}
		$handle = new upload($abs_path);
		if (!$handle->uploaded) {
			echo "<span style=\"color: red;\">Class upload can't access file '".UPL_DIR."/".$rel_path."' !</span><br />";
			continue;
		}
		$w = $handle->image_src_x;
		$f_big = $f_full = $f_thumb = false;
		if ($w > FULL_STANDARD_WIDTH) {
			$f_big = true;
		} else if ($w > FULL_MIN_WIDTH) {
			$f_full = true;
		} else if ($w == THUMB_WIDTH) {
			$f_thumb = true;
		} else {
			echo "<span style=\"color: red;\">Image {$rel_path} has width {$w} - not full image, not thumbnail !</span><br />";
			continue;
		}

		if ($table == "place")
			$identifier = "place_id";
		else
			$identifier = "id";

		$res = $db->q("SELECT p.image, p.thumb, l.country_id, c.dir as country 
						FROM {$table} p
						INNER JOIN location_location l on l.loc_id = p.loc_id
						INNER JOIN location_location c on c.loc_id = l.country_id
						WHERE {$identifier} = '{$id}' LIMIT 1");
		if ($db->numrows($res) != 1) {
			echo "<span style=\"color: red;\">Could not find place in table '{$table}' with {$identifier} = '{$id}' !</span><br />";
			continue;
		}
		$row = $db->r($res);
		$image = $row["image"];
		$thumb = $row["thumb"];
		$country_id = $row["country_id"];
		$country = $row["country"];

		if ($table == "adult_stores")
			$img_dir = "adultstore";
		else if ($table == "eroticmassage")
			$img_dir = "eroticmassage";
		else if ($table == "strip_club2")
			$img_dir = "stripclub";
		else if ($table == "place") {
			if (in_array($country_id, array(16046, 16047, 41973))) {
				echo "<span style=\"color: red;\">Countries US,CA and UK are not supported for table place (yet) !</span><br />";
				continue;
			}
			$img_dir = $country;
		}
		//echo "id={$id}, image='{$image}', thumb='{$thumb}', country_id={$country_id}, img_dir='{$img_dir}'<br />";

		//create directories if not exist
		$full_dir  = "{$config_image_path}/{$img_dir}/";
		if (!is_dir($full_dir)) {
			echo "Creating target dir '{$full_dir}'...<br />\n";
			$ret = mkdir($full_dir);
			if (!$ret) {
				echo "<span style=\"color: red;\">Could not create target dir '{$full_dir}' !</span><br />";
				return false;
			}
		}
		$thumb_dir = "{$config_image_path}/{$img_dir}/t/";
		if (!is_dir($thumb_dir)) {
			echo "Creating target dir '{$thumb_dir}'...<br />\n";
			$ret = mkdir($thumb_dir);
			if (!$ret) {
				echo "<span style=\"color: red;\">Could not create target dir '{$thumb_dir}' !</span><br />";
				return false;
			}
		}

		if ($live) {
			$handle->image_watermark = WATERMARK_PATH;
			$handle->image_watermark_x = 0;
			$handle->image_watermark_y = 0;
			$handle->file_auto_rename = false;
			$handle->file_overwrite = true;
			$handle->image_resize = false;

			$new_filename_body = $system->_makeText()."_{$id}";
			$new_filename_ext = "jpeg";
			$new_filename = $new_filename_body.".".$new_filename_ext;
			$handle->file_new_name_body = $new_filename_body;
			$handle->file_new_name_ext = $new_filename_ext;
			echo "new_filename = '{$new_filename}'<br />\n";

			if (file_exists($full_dir."/{$new_filename}") || file_exists($thumb_dir."/{$new_filename}")) {
				echo "<span style=\"color: red;\">Error: Thumb or full file already exists ???<br />Full: '".$full_dir."/{$new_filename}'<br />Thumb: '".$thumb_dir."/{$new_filename}'<br /></span>\n";
				return false;
			}
		}

		$extra = false;
		if (empty($image) && empty($thumb)) {
			echo "Id: {$id} - updating main picture (table {$table})<br />\n";
		} else if (!empty($image) && !empty($thumb)) {
			$extra = true;
			echo "Id: {$id} - adding EXTRA picture<br />\n";
		} else {
			echo "<span style=\"color: red;\">Error: In table '{$table}', id {$id} is one of image/thumb empty !<br />\n";
			return false;
		}

		if ($f_big) {
			//big image
			echo "Big<br />\n";
			if ($live) {
				echo "Saving big-&gt;full {$full_dir}/{$new_filename} <br />\n";
				if ($handle->image_src_x > FULL_STANDARD_WIDTH) {
					$handle->image_resize = true;
					$handle->image_ratio_y = true;
					$handle->image_x = FULL_STANDARD_WIDTH;
				}
				$ret = $handle->Process($full_dir);
				if ($handle->error) {
					echo "<span style=\"color: red;\">Error saving fullsize: '{$handle->error}', '{$full_dir}/{$new_filename}'</span><br />\n";
					return false;
				} else {
					echo "Success: '{$full_dir}/{$new_filename}'<br />\n";
				}

				echo "Saving thumb-&gt;thumb {$thumb_dir}/{$new_filename} <br />\n";
				$handle->image_watermark = WATERMARK_PATH;
				$handle->image_watermark_x = 0;
				$handle->image_watermark_y = 0;
				$handle->file_auto_rename = false;
				$handle->file_overwrite = true;
				$handle->image_resize = false;
				if ($handle->image_src_x > THUMB_WIDTH) {
					$handle->image_resize = true;
					$handle->image_ratio_y = true;
					$handle->image_x = THUMB_WIDTH;
				}
				$handle->file_new_name_body = $new_filename_body;
				$handle->file_new_name_ext = $new_filename_ext;
                $ret = $handle->Process($thumb_dir);
				if ($handle->error) {
					echo "<span style=\"color: red;\">Error saving thumbnail: '{$handle->error}', '{$thumb_dir}/{$new_filename}'</span><br />\n";
					return false;
				} else {
					echo "Success: '{$thumb_dir}/{$new_filename}'<br />\n";
				}

				$ret = save_to_db($table, $img_dir, $id, $new_filename, $new_filename, $extra);
				if (!$ret) {
					echo "<span style=\"color: red;\">Error saving to db: table={$table}, img_dir={$img_dir}, id={$id}, image={$new_filename}, thumb={$new_filename}, extra={$extra} !</span><br />\n";
					return false;
				} else {
					echo "Saved to DB successfully (extra={$extra}).<br/>\n";
				}
			}
			$cnt_big++;
		} else if ($f_full) {
			//full image
			if (!empty($image)) {
				echo "<span style=\"color: red;\">Image {$rel_path} is full image, but place {$id} already has assigned full image: '{$image}' !</span><br />";
				continue;
			}
			if ($live) {
				//add watermark and save into target folder
  				$handle->Process($full_dir);	
				//update db
				$ret = save_to_db($table, $img_dir, $id, $new_filename, "", $extra);
				if (!$ret) {
					echo "<span style=\"color: red;\">Error saving to db: table={$table}, img_dir={$img_dir}, id={$id}, image={$new_filename}, thumb={$new_filename}, extra={$extra} !</span><br />\n";
					return false;
				} else {
					echo "Saved to DB successfully (extra={$extra}).<br/>\n";
				}
			}
			$cnt_full++;
		} else {
			//thumbnail
			if (!empty($thumb)) {
				echo "<span style=\"color: red;\">Image {$rel_path} is thumbnail, but place {$id} already has assigned thumbnail: '{$thumb}' !</span><br />";
				continue;
			}
			if ($live) {
				//add watermark and save into target folder
  				$handle->Process($thumb_dir);	
				//update db
				$ret = save_to_db($table, $img_dir, $id, "", $new_filename, $extra);
				if (!$ret) {
					echo "<span style=\"color: red;\">Error saving to db: table={$table}, img_dir={$img_dir}, id={$id}, image={$new_filename}, thumb={$new_filename}, extra={$extra} !</span><br />\n";
					return false;
				} else {
					echo "Saved to DB successfully (extra={$extra}).<br/>\n";
				}
			}
			$cnt_thumb++;
		}

		//delete file
		if ($live)
			unlink($abs_path);

		if ($live && (($cnt_big + $cnt_full + $cnt_thumb) > 15)) {
			echo "<b>Breaking...</b><br />";
			return true;
		}

		if (!in_array($id, $place_ids))
			$place_ids[] = $id;
	}

	if (!$live) {
		echo "#bigs={$cnt_big} #fulls={$cnt_full}, #thumbs={$cnt_thumb}, #places=".count($place_ids)."<br />\n";
		if ($cnt_big || $cnt_full || $cnt_thumb) {
			echo "<br />Do you want to continue and assign these images to places ?<br /><br />\n";
			echo "<form>";
			echo "<input type=\"hidden\" name=\"action\" value=\"process\" />";
			echo "<input type=\"hidden\" name=\"table\" value=\"{$table}\" />";
			echo "<input type=\"hidden\" name=\"live\" value=\"1\" />";
			echo "<input type=\"submit\" name=\"submit\" value=\"Yes\" />";
			echo "</form>";
			return false;
		}
	} else {
		echo "Successfully assigned #images={$cnt_full}, #thumbs={$cnt_thumb}, #places=".count($place_ids)."<br />\n";
	}

	return true;
}

function formatSizeUnits($bytes) {
	if ($bytes >= 1073741824) {
		$bytes = number_format($bytes / 1073741824, 2) . ' GB';
	} elseif ($bytes >= 1048576) {
		$bytes = number_format($bytes / 1048576, 2) . ' MB';
	} elseif ($bytes >= 1024) {
		$bytes = number_format($bytes / 1024, 2) . ' KB';
	} elseif ($bytes > 1) {
		$bytes = $bytes . ' bytes';
	} elseif ($bytes == 1) {
		$bytes = $bytes . ' byte';
	} else {
		$bytes = '0 bytes';
	}
	return $bytes;
}

switch ($_REQUEST["action"]) {
	case 'process':
		$start = microtime(true);
		$ret = process();
		$end = microtime(true);
		echo "Took ".number_format($end-$start, 2)." sec.<br />\n";
		if (!$ret)
			return;
		break;
}

//read zip files in ./tmp/image_mass_upload folder
if (($dir = opendir(MAIN_DIR)) === false) {
	echo "<span style=\"color: red;\">Error while opendir of '".MAIN_DIR."' !</span><br />";
} else {
	$zip_files = array();
	while (false !== ($file = readdir($dir))) {
		if($file == '.' || $file == '..')
			continue;
		$path = MAIN_DIR."/".$file;
		if (!is_file($path))
			continue;
		if (preg_match('/^.*\.zip$/i', $file) == 1) {
			$zip_files[] = array("filename" => $file, "size" => formatSizeUnits(filesize($path)), "mtime" => date("Y-m-d H:i:s", filemtime($path)));
		}
	}
	closedir($dir);
}
?>
<h1>Image mass upload</h1>
<form action="image_mass_upload" method="post" enctype="multipart/form-data">
<input type="hidden" name="action" value="process" />
<input type="hidden" name="live" value="0" />
Table: <select name="table">
	<option value=""> - </option>
	<option value="place">place</option>
	<option value="adult_stores">adult_stores</option>
	<option value="eroticmassage">eroticmassage</option>
	<option value="strip_club2">strip_club2</option>
</select><br /><br />
<?php
echo "Please select ZIP file from directory ".MAIN_DIR.":<br />\n";
if (count($zip_files) > 0) {
	foreach ($zip_files as $file) {
		echo "<input type=\"radio\" name=\"filename\" value=\"{$file["filename"]}\" />{$file["filename"]} - {$file["size"]} - {$file["mtime"]}<br />";
	}
	echo "<input type=\"submit\" name=\"submit\" value=\"Select\" />";
} else {
	echo "<i>No zip files.</i>";
}
?>
<br /><br />
Or upload ZIP file with images:<br />
<input type="file" name="file" /><br />
<input type="submit" name="submit" value="Upload" /><br />
<br />
Or use images already copied to directory ./tmp/image_mass_upload/upload:<br />
<input type="submit" name="submit" value="Current" />
</form>
