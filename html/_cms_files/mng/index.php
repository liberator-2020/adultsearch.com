<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $db, $account, $smarty, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("mail_manage"))
	return;

function find_ads($res, &$ads_html, &$ads_found_count, &$ads_found, &$i, &$html) {
	global $db;

	$i = 0;
	$html = "";
	while ($row = $db->r($res)) {
		$clad_id = $row["id"];
		if (in_array($clad_id, $ads_found))
			continue;
		$res2 = $db->q("
			SELECT DISTINCT c.*
				,l.loc_name, l.s
			FROM classifieds c
			INNER JOIN classifieds_loc cl on cl.post_id = c.id
			INNER JOIN location_location l on l.loc_id = cl.loc_id
			WHERE c.id = ?",
			[$clad_id]
			);
		if ($db->numrows($res2) != 1)
			continue;
		$item_html = "";
		$i++;
		$ads_found_count++;
		$ads_found[] = $clad_id;
		$row2 = $db->r($res2);
		$clad = clad::withRow($row2);
		$category = classifieds::getcatnamebytype($row2["type"]);
		if ($row2["deleted"])
			$status = "<span style=\"color: darkred;\">DELETED</span>";
		else if ($row2["done"] == 0)
			$status = "Expired";
		else if ($row2["done"] == 1)
			$status = "Live";
		else
			$status = "Other";
		$item_html .= "<img src=\"//img.adultsearch.com/classifieds/{$row2["thumb"]}\" style=\"width: 75px; height: 75px; float: left;\"/>";
		$item_html .= "#{$clad_id}<br />";
		$item_html .= "Category: {$category}, Name: {$row2["firstname"]}, title: {$row2["title"]}, Location: {$row2["loc_name"]},{$row2["s"]}, Status: {$status}<br />";
		$item_html .= "<a href=\"{$clad->getUrl()}\" class=\"round_btn\" target=\"_blank\" style=\"margin-right: 5px;\">View ad</a>";
		$item_html .= "<a href=\"/mng/classifieds?cid={$clad_id}\" class=\"round_btn\" target=\"_blank\" style=\"margin-right: 5px;\">Manage</a>";
		$item_html .= "<a href=\"/classifieds/pay?action_id={$clad_id}\" class=\"round_btn green_btn\" target=\"_blank\" style=\"margin-right: 5px;\">$ Pay</a>";
		$item_html = "<div style=\"clear: both; border-bottom: 1px solid #aaa; padding-bottom: 5px; margin-bottom: 5px;\">{$item_html}<br style=\"clear: both;\" /></div>";
		$html .= $item_html;
	}
	return true;
}

function export_contact_details($contact_id, &$contact_email = null) {
	global $account, $db, $smarty;

	if (!$contact_id)
		return false;
	$res = $db->q("
		SELECT c.account_id, c.email, c.name, c.`text`, c.phone, c.url, c.agent, c.stamp, c.ip_address, c.assigned, a.username 
		FROM contact c
		LEFT JOIN account a on c.account_id = a.account_id
		WHERE c.contact_id = ?", 
		[$contact_id]
		);
	if (!$db->numrows($res))
		return false;
	$row = $db->r($res);

	$smarty->assign("contact_id", $contact_id);
	$smarty->assign("url", $row['url']);
	$smarty->assign("submit", date("m/d/Y g:i a T", $row['stamp']));

	$contact_account_id = intval($row["account_id"]);
	$smarty->assign("account_id", $contact_account_id);

	if ($contact_account_id) {
		$u_reason = "Contact form ".date("m/d/Y g:i a T", $row["stamp"]).": {$row["text"]}";
		$smarty->assign("user_delete_link", "/mng/accounts?action=delete&action_id={$contact_account_id}&u_reason=".urlencode($u_reason)."&u_send_email=1&contact_id={$contact_id}");
	}

	$smarty->assign("username", htmlspecialchars($row["username"]));
	$smarty->assign("name", htmlspecialchars($row["name"]));
	$email = $row["email"];
	$contact_email = $row["email"];
	$smarty->assign("email", htmlspecialchars($email));
	$phone = $row["phone"];
	$smarty->assign("phone", makeProperPhoneNumber($phone));

	$ip_address = $row["ip_address"];
	$ip_address_label = $ip_address." - ".geolocation::getLocationLabel($ip_address);
	$smarty->assign("ip_address_label", $ip_address_label);

	$device = null;
	if ($row["agent"])
		$device = get_device_type($row["agent"])." - ".get_os($row["agent"])." - ".get_browser_name($row["agent"]);//." <span style=\"color: #999;\">({$row["agent"]})</span>";
	$smarty->assign("device", $device);

	$smarty->assign("text", nl2br(htmlspecialchars($row["text"])));

	if ($row['assigned'] == $account_id)
		$smarty->assign('assigned', true);

	$attachments = [];
	$res = $db->q("SELECT * FROM contact_att WHERE contact_id = ?", [$contact_id]);
	while($row = $db->r($res)) {
		$attachments[] = [
			"id" => $row["id"],
			"filename" => $row['filename'],
			];
	}
	$smarty->assign("attachments", $attachments);

	$previous = [];
	$res = $db->q("SELECT `text`, stamp FROM contact WHERE email = ? AND contact_id != ? LIMIT 2", [$email, $contact_id]);
	while($row = $db->r($res)) {
		$previous[] = [
			0 => $row["text"],
			1 => date("m/d/Y g:i a T", $row["stamp"]),
			];
	}
	$smarty->assign("previous", $previous);

	//if contact form was submitted by logged in user, search his ads
	if ($contact_account_id) {
		$ads_html = "";
		$ads_found_count = 0;
		$ads_found = [];
		//find ads directly in this account
		$res = $db->q("SELECT c.id FROM classifieds c WHERE c.account_id = ? AND c.done <> -1", [$contact_account_id]);
		if ($db->numrows($res) > 0) {
			find_ads($res, $ads_html, $ads_found_count, $ads_found, $i, $html);
			if ($i)
				$ads_html .= "Found <strong>{$i}</strong> ads for user #{$contact_account_id}:<br />{$html}";
		} else {
			$ads_html = "<strong>User #{$contact_account_id} has no classifieds ads in the system.</strong><br />";
		}

		//find ads by phone
		$phone = preg_replace('/[^0-9]/', '', $phone);
		if ($phone) {
			$res = $db->q("SELECT c.id FROM classifieds c WHERE c.phone = ? AND c.done <> -1", [$phone]);
			if ($db->numrows($res) > 0) {
				find_ads($res, $ads_html, $ads_found_count, $ads_found, $i, $html);
				if ($i)
					$ads_html .= "Found <strong>{$i}</strong> ads for phone ".makeProperPhoneNumber($phone).":<br />{$html}";
			} else {
				$ads_html .= "No other classified ads found by search for phone ".makeProperPhoneNumber($phone).".<br />";
			}
		}

		//find ads by email
		if ($email) {
			$res = $db->q("SELECT c.id FROM classifieds c WHERE c.email = ? AND c.done <> -1", [$email]);
			if ($db->numrows($res) > 0) {
				find_ads($res, $ads_html, $ads_found_count, $ads_found, $i, $html);
				if ($i)
					$ads_html .= "Found <strong>{$i}</strong> ads for email {$email}:<br />{$html}";
			} else {
				$ads_html .= "No other classified ads found by search for email {$email}.<br />";
			}
		}

		$smarty->assign("ads", $ads_html);
	}

	if (strstr($row['url'], 'female-escorts') 
		|| strstr($row['url'], 'body-rubs') 
		|| strstr($row['url'], 'tstv-shemale-escorts')
		) {
		$id = intval(substr($row['url'], strrpos($row['url'], '/')+1));
		$res = $db->q("select * from classifieds where id = ? and bp = 1", [$id]);
		if ($db->numrows($res) ) {
			$smarty->assign('bp_ad_id', $id);
		}
	}

	return true;
}

$account_id = $account->isloggedin();

$respond_from_email = "support@adultsearch.com";

if( isset($_GET['assign']) ) {
	$contact_id = intval($_GET['assign']);
	$db->q("update contact set assigned = '$account_id' where contact_id = '$contact_id'");
	echo "This mail is only visible to you now.";
	die;
}

if( isset($_GET['count']) )  {
	ob_clean();
	if( !isset($_SESSION["mng_contact"]) || ($_SESSION["mng_contact"] < (time()-15)) ) {
		$res = $db->q("select count(*) from contact where `read` = 0 and (assigned = 0 or assigned = $account_id)");
		$row = $db->r($res);
		$_SESSION["mng_contact"] = time();
		$_SESSION["mng_contact_c"] = $row[0];
		die($row[0]);
	} 
	die($_SESSION["mng_contact_c"]);
}

if( isset($_POST['contact_id']) ) {
	$contact_id = intval($_POST['contact_id']);

	$res = $db->q("SELECT respond FROM contact WHERE contact_id = ?", [$contact_id]);
	if ($db->numrows($res) != 1) {
		die("Invalid contact_id!");
	}
	$row = $db->r($row);
	$respond = $row["respond"];

	if (isset($_POST['read'])) {
		$db->q("update contact set `read` = 1, read_by = '$account_id' where contact_id = '$contact_id'");
	} else if (isset($_POST['toadmin']) || isset($_POST['toagency'])) {
		//forwarding issue to email

		$contact_email = null;
		$ret = export_contact_details($contact_id, $contact_email);
		if (!$ret)
			die;

		$html = $smarty->fetch(_CMS_ABS_PATH."/templates/contact/forward_email.html.tpl");
		$text = $smarty->fetch(_CMS_ABS_PATH."/templates/contact/forward_email.txt.tpl");
		$subject = "Adult Search Customer Service #{$contact_id}";
		if (isset($_POST['toagency']))
			$to = "agency@adultsearch.com";
		else
			$to = ADMIN_EMAIL; //to admin
		//$embedded_images = array("logo_email" => _CMS_ABS_PATH."/images/logo_email.png");

		$ret = send_email([
			"from" => $respond_from_email,
			"reply_to" => $contact_email,
			"to" => $to,
			"subject" => $subject,
			"html" => $html,
			"text" => $text,
//			"embedded_images" => $embedded_images
			]);
		if ($ret)
			flash::add(flash::MSG_SUCCESS, "Customer service issue #{$contact_id} successfully forwarded to {$to}");
		else
			flash::add(flash::MSG_ERROR, "Failed to forward customer service issue to '{$to}'");

		unset($_POST['respond']);
		unset($_POST['toagency']);
		unset($_POST['toadmin']);

		$respond .= "sent to {$to}";
		$db->q("UPDATE contact 
				SET `read` = 1, respond = ?, read_by = ? 
				WHERE contact_id = ?", 
				[$respond, $account_id, $contact_id]
				);

	} else {
		$text = $_REQUEST["respond"];
		$html = nl2br($text);
		if (empty($text) || !$contact_id ) {
			$smarty->assign("open", $contact_id);
			$smarty->assign("error", "You did not type any response ?");
		} else {
			$res = $db->q("select email, url from contact where contact_id = '$contact_id'");
			if ($db->numrows($res)) {
				$row = $db->r($res);
				$url = "https://{$row['url']}";
				$text .= "\n\nURL: {$url}";
				$html .= "<br /><br />URL: <a href=\"{$url}\">{$url}</a><br />";
				$respond .= $text;
				send_email([
					"from" => $respond_from_email,
					"to" => $row['email'],
//					"to" => "admin@adultsearch.com",
					"subject" => "Adult Search Customer Service #{$contact_id}",
					"text" => $text,
					"html" => $html,
					]);
				$db->q("
					UPDATE contact 
					SET `read` = 1, respond = ?, read_by = ?
					WHERE contact_id = ?",
					[$respond, $account_id, $contact_id]
					);
				return success_redirect("Your response was sent to {$row['email']}.", "/mng/");
			}
		}
	}
}


if (isset($_GET['read'])) {
	//displaying popup details about contact message

	$contact_id = intval($_REQUEST["read"]);

	$ret = export_contact_details($contact_id);
	if (!$ret)
		die;

	if (isset($_GET['error']))
		$smarty->assign("error", "You did not type any respond ?");

	$smarty->display(_CMS_ABS_PATH.'/templates/contact/contact_table_reply.tpl');
	die;
}

$res = $db->q("SELECT c.contact_id, c.email, c.stamp, c.ip_address, c.text, a.username
				FROM contact c
				LEFT JOIN account a using (account_id)
				WHERE c.`read` = 0 and (c.assigned = 0 or c.assigned = '$account_id')
				ORDER by c.contact_id desc");
$_SESSION["mng_contact_c"] = $db->numrows($res);
while($row=$db->r($res)) {
	$text = trim($row['text']);
	if ($text == "")
		$text = "empty text";
	$new_contacts[] = array(
		"contact_id" => $row['contact_id'],
		"username" => $row['username'],
		"email" => $row['email'],
		"ip" => $row['ip_address'],
		"time" => date("m/d/Y g:i a T", $row["stamp"]),
		"text" => htmlspecialchars($text)
	);
}

$smarty->assign("new_contacts", $new_contacts);
$smarty->display(_CMS_ABS_PATH.'/templates/contact/contact_manager.tpl');


?>
