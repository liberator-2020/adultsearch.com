<?php
require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.location.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in
if (!$account->isMember()) {
	$account->asklogin();
	return false;
}

//only for burak and jay for now
if (!$account->isrealadmin())
	system::go('/');


function check_loc_attrs() {
	global $db;

	$live = ($_REQUEST["live"]) ? true : false;
	$loc_id = intval($_REQUEST["loc_id"]);
	if (!$loc_id) {
		echo "Error: Please select country!<br />\n";
		return;
	}

	$attrs = array();
	$res = $db->q("SELECT attribute_id FROM attribute");
	while ($row = $db->r($res)) {
		$attrs[] = $row["attribute_id"];
	}

	$loc_attrs = array();
	$res = $db->q("SELECT attribute_id FROM location_attribute WHERE location_id = ?", array($loc_id));
	while ($row = $db->r($res)) {
		$loc_attrs[] = $row["attribute_id"];
	}
	
	$diff = array_diff($attrs, $loc_attrs);
	$cnt = count($diff);
	echo "In location {$loc_id} are missing {$cnt} attributes:<br />";
	echo "<pre>".print_r($diff, true)."</pre><br />\n";

	if ($cnt && !$live) {
        echo "<br />Do you want to continue and add these {$cnt} attributes ?<br />\n";
	    echo "<form><input type=\"hidden\" name=\"action\" value=\"check_loc_attrs\" />";
    	echo "<input type=\"hidden\" name=\"loc_id\" value=\"{$loc_id}\" />";
	    echo "<input type=\"hidden\" name=\"live\" value=\"1\" />";
    	echo "<input type=\"submit\" name=\"submit\" value=\"Yes\" /></form>";
		return;
	}

	if ($cnt) {
		//adding missing attributes
		$succ = 0;
		foreach ($diff as $attribute_id) {
			$res = $db->q("INSERT INTO location_attribute (location_id, attribute_id) VALUES (?, ?)", array($loc_id, $attribute_id));
			$aff = $db->affected($res);
			if ($aff != 1) {
				echo "Error: Affected = {$aff} from insert: loc_id = {$loc_id}, attribute_id = {$attribute_id}<br />\n";
				echo "Interrupting inserts.<br />\n";
				break;
			} else {
				$succ++;
			}
		}
		echo "Successfully added {$succ} attributes for location {$loc_id}.<br />\n";
	}
}

//refresh cities
function refreshCities() {
	global $db;

	$location = new location($db);
	$location->createcitiesindb();
	echo "Done.<br />";
	return;
}

function moveToMajor() {
	global $db;

	$location_exact_id = intval(GetGetParam("location_exact_id"));
	$loc_id = intval(GetGetParam("loc_id"));
	if (!$location_exact_id) {
		echo "ERROR: location_exact_id not specified !<br />\n";
		return;
	}
	if (!$loc_id) {
		echo "ERROR: loc_id not specified !<br />\n";
		return;
	}
	if ($location_exact_id == $loc_id) {
		echo "ERROR: location_exact_id == loc_id == {$location_exact_id} !<br />\n";
		return;
	}
	$location_exact = location::findOneById($location_exact_id);
	if (!$location_exact) {
		echo "ERROR: Cant fetch location_exact: location_exact_id = {$location_exact_id} !<br />\n";
		return;
	}
	echo "From: ".$location_exact->getName()." (location_exact_id={$location_exact_id}) To: loc_id={$loc_id}<br />\n";

	//NOTE: setting of location_exact_id is a bit more complicated, because we want to support also reversing the change, so if we are setting back the original city (so value of location_exact_id is becoming again loc_id), location_exact_id should be come NULL
	$res = $db->q("UPDATE strip_club2 SET loc_id = ?, location_exact_id = IF(location_exact_id = ?, NULL, IF(ISNULL(location_exact_id), ?, location_exact_id)) WHERE loc_id = ?", array($loc_id, $loc_id, $location_exact_id, $location_exact_id));
	$aff = $db->affected($res);
	if ($aff)
		echo "Moved <strong>{$aff}</strong> strip clubs.<br />\n";

	$res = $db->q("UPDATE eroticmassage SET loc_id = ?, location_exact_id = IF(location_exact_id = ?, NULL, IF(ISNULL(location_exact_id), ?, location_exact_id)) WHERE loc_id = ?", array($loc_id, $loc_id, $location_exact_id, $location_exact_id));
	$aff = $db->affected($res);
	if ($aff)
		echo "Moved <strong>{$aff}</strong> EMPs.<br />\n";

	$res = $db->q("UPDATE adult_stores SET loc_id = ?, location_exact_id = IF(location_exact_id = ?, NULL, IF(ISNULL(location_exact_id), ?, location_exact_id)) WHERE loc_id = ?", array($loc_id, $loc_id, $location_exact_id, $location_exact_id));
	$aff = $db->affected($res);
	if ($aff)
		echo "Moved <strong>{$aff}</strong> sex shops.<br />\n";

	$res = $db->q("UPDATE brothel SET loc_id = ?, location_exact_id = IF(location_exact_id = ?, NULL, IF(ISNULL(location_exact_id), ?, location_exact_id)) WHERE loc_id = ?", array($loc_id, $loc_id, $location_exact_id, $location_exact_id));
	$aff = $db->affected($res);
	if ($aff)
		echo "Moved <strong>{$aff}</strong> brothels.<br />\n";

	$res = $db->q("UPDATE lifestyle SET loc_id = ?, location_exact_id = IF(location_exact_id = ?, NULL, IF(ISNULL(location_exact_id), ?, location_exact_id)) WHERE loc_id = ?", array($loc_id, $loc_id, $location_exact_id, $location_exact_id));
	$aff = $db->affected($res);
	if ($aff)
		echo "Moved <strong>{$aff}</strong> lifestyle clubs.<br />\n";

	$res = $db->q("UPDATE lingeriemodeling1on1 SET loc_id = ?, location_exact_id = IF(location_exact_id = ?, NULL, IF(ISNULL(location_exact_id), ?, location_exact_id)) WHERE loc_id = ?", array($loc_id, $loc_id, $location_exact_id, $location_exact_id));
	$aff = $db->affected($res);
	if ($aff)
		echo "Moved <strong>{$aff}</strong> lingeriemodeling1on1 clubs.<br />\n";

	$res = $db->q("UPDATE gay SET loc_id = ?, location_exact_id = IF(location_exact_id = ?, NULL, IF(ISNULL(location_exact_id), ?, location_exact_id)) WHERE loc_id = ?", array($loc_id, $loc_id, $location_exact_id, $location_exact_id));
	$aff = $db->affected($res);
	if ($aff)
		echo "Moved <strong>{$aff}</strong> gay places.<br />\n";

	$res = $db->q("UPDATE gaybath SET loc_id = ?, location_exact_id = IF(location_exact_id = ?, NULL, IF(ISNULL(location_exact_id), ?, location_exact_id)) WHERE loc_id = ?", array($loc_id, $loc_id, $location_exact_id, $location_exact_id));
	$aff = $db->affected($res);
	if ($aff)
		echo "Moved <strong>{$aff}</strong> gay bath houses.<br />\n";

	$res = $db->q("SELECT DISTINCT post_id FROM classifieds_loc WHERE loc_id = ?", array($location_exact_id));
	$ads = 0;
	while ($row = $db->r($res)) {
		$clad_id = $row["post_id"];

		//update location of the ad
		$res3 = $db->q("UPDATE classifieds_loc SET loc_id = ? WHERE post_id = ? AND loc_id = ?", array($loc_id, $clad_id, $location_exact_id));

		$res2 = $db->q("SELECT COUNT(*) as cnt FROM classifieds_loc WHERE post_id = ?", array($clad_id));
		$row2 = $db->r($res2);
		$cnt = $row2["cnt"];
		if ($cnt == 1) {
			//this ad is posted only in this location, lets check if ad has already filled location detail
			$res4 = $db->q("SELECT location FROM classifieds WHERE id = ?", array($clad_id));
			$row4 = $db->r($res4);
			$location_detail = $row4["location"];
			if (!$location_detail) {
				//location detail is empty, fill it with name of exact location
				$res5 = $db->q("UPDATE classifieds SET location = ? WHERE id = ? LIMIT 1", array($location_exact->getName(), $clad_id));
			}
		}
		$ads++;		
	}
	if ($ads)
		echo "Moved <strong>{$ads}</strong> classified ads.<br />\n";

	//TODO
	return;
}

function addLocationVideo() {
	global $db;

	$loc_id = intval(GetGetParam("loc_id"));
	echo "loc_id=$loc_id<br />";

	//TODO
	$country = "netherlands";
	$city_dir = $city;
	$url = "http://$country.adultsearch.com/$city_dir/uploadVideo";	//TODO not working for domestic
	echo "url=$url<br />";	
	system::go($url);
}

function city_to_neighborhood() {
	global $db;

	$real_do = GetGetParam("real_do");

	//ew neighborhood name and other location_location attributes
	$nb = GetGetParam("nb");
	$dir = strtolower(str_replace(" ", "-", $nb));
	$loc_url = "/{$dir}/";

	//get transformed location
	$loc_id = GetGetParam("loc_id");
	$sql = "select * from location_location where loc_id = '{$loc_id}' and loc_type = 3";
	$res = $db->q($sql);
	if ($db->numrows($res) != 1) {
		echo "Error: ".$db->numrows($res)." locations found for loc_id '{$loc_id}' !<br />";
		return;
	}
	$row = $db->r($res);
	$loc_name = $row['loc_name'];
	$country_id = $row['country_id'];
	$country_sub = $row['country_sub'];
	$loc_lat = $row['loc_lat'];
	$loc_long = $row['loc_long'];
	$loc_dir = $row["dir"];
	echo "loc_id={$loc_id}, loc_name=$loc_name, country_id={$country_id}, country_sub={$country_sub}, loc_lat={$loc_lat}, loc_long={$loc_long}<br />";
	
	//get parent city location
	$parent_loc_id = GetGetParam("parent_loc_id");
	$sql = "select * from location_location where loc_id = '{$parent_loc_id}' and loc_type = 3";
	$res = $db->q($sql);
	if ($db->numrows($res) != 1) {
		echo "Error: ".$db->numrows($res)." locations found for loc_id '{$parent_loc_id}' !<br />";
		return;
	}
	$row = $db->r($res);
	$parent_loc_name = $row['loc_name'];
	$parent_country_id = $row['country_id'];
	$parent_loc_dir = $row["dir"];
	echo "parent_loc_id={$parent_loc_id}, parent_loc_name=$parent_loc_name, parent_country_id={$parent_country_id}<br />";

	echo "New neighborhood name = '{$nb}', loc_url='{$loc_url}', dir='{$dir}'<br />";
	echo "<br />";

	if (in_array($country_id, array(16046, 16047, 41973))) {
		echo "The country s domestic, this action is programmed only for international cities (not checking domestic category tables like eroticmassage, gay, ...)!<br />";
		return;	
	}
	if ($country_id != $parent_country_id) {
		echo "The country for cities is different!";
		return;	
	}

	if (!$real_do) {
		//lets do checks

		$check_failed = false;

		//check if loc_id is not in not managed tables
		$tables = array("account_permission_loc", "advertise_income", "affiliate", 
						"category", "category_place", "classifieds", "classifieds_bp_link", "classifieds_click", 
						"classifieds_loc", "classifieds_payment", "classifieds_payment_done", "classifieds_sponsor", "classifieds_sponsor_log", 
						"core_loc", "core_zip", "core_zip_update", "currency", 
						"forum_new", "forum_post", "forum_topic", "home", "location", "location_neighbor", 
						"location_population", "location_topcities", "place_picture", "tweet_log", "twitter_data");
		foreach ($tables as $table) {
			$sql = "select count(*) as cnt from {$table} where loc_id = '{$loc_id}'";
			//echo "sql='{$sql}'<br />";
			$res = $db->q($sql);
			$row = $db->r($res);
			$cnt = $row['cnt'];
			if ($cnt > 0) {
				echo "{$cnt} rows for this location in table '{$table}' !<br />";
				echo "sql='{$sql}'<br /><br />";
				$check_failed = true;
			}
		}

		if ($check_failed) {
			echo "<strong>Please fix the problems or enhance this code.</strong><br />";
			return;
		}

        echo "<br />Do you want to continue and change city {$loc_name} (loc_id={$loc_id}) to neighborhood {$nb} (loc_url='{$loc_url}', dir='{$dir}') under city {$parent_loc_name} (loc_id={$parent_loc_id}) ?<br />";
	    echo "<form><input type=\"hidden\" name=\"action\" value=\"city_to_neighborhood\" />";
    	echo "<input type=\"hidden\" name=\"loc_id\" value=\"{$loc_id}\" />";
	    echo "<input type=\"hidden\" name=\"nb\" value=\"{$nb}\" />";
    	echo "<input type=\"hidden\" name=\"parent_loc_id\" value=\"{$parent_loc_id}\" />";
	    echo "<input type=\"hidden\" name=\"real_do\" value=\"1\" />";
    	echo "<input type=\"submit\" name=\"submit\" value=\"Yes\" /></form>";
        return;
    }

	//real do
	//tables changed: location_location, forum_forum, place

	//delete empty forums
	$sql = "delete from forum_forum where loc_id = '{$loc_id}' and forum_posts = 0 and forum_topics = 0";
	echo "SQL='{$sql}'<br />";
	$db->q($sql);
	$aff = $db->affected();
	echo "affected rows = {$aff}<br />";

	//check forums if we are not leaving any forum behind
	$sql = "select count(*) as cnt from forum_forum where loc_id = '{$loc_id}'";
	echo "Forum check sql='{$sql}'<br /><br />";
	$res = $db->q($sql);
	$row = $db->r($res);
	$cnt = $row['cnt'];
	if ($cnt > 0) {
		echo "{$cnt} rows for this location still in table forum_forum !<br />";
		return;
	}

	//insert new neighborhood into location_location table
	$sql = "insert into location_location 
			(loc_type, loc_parent, country_id, county_id, country_sub, loc_name, loc_url, s, dir, loc_lat, loc_long)
			values
			('4', '{$parent_loc_id}', '{$country_id}', '0', '{$country_sub}', '{$nb}', '{$loc_url}', '', '{$dir}', '{$loc_lat}', '{$loc_long}')";
	echo "SQL='{$sql}'<br />";
	$db->q($sql);
	$nb_id = $db->insertid();
	echo "neighborhood loc_id = '{$nb_id}'<br /><br />";
	if ($nb_id == 0) {
		echo "Error: newly inserted neighborhood has insetid 0 !<br />";
		return;
	}

	//update place table
	$sql = "update place set loc_id = '{$parent_loc_id}', neighbor_id = '{$nb_id}' where loc_id = '{$loc_id}'";
	echo "SQL='{$sql}'<br />";
	$db->q($sql);
	$aff = $db->affected();
	echo "affected rows = {$aff}<br /><br />";

	//delete old location_location city entry
	$sql = "delete from location_location where loc_id = '{$loc_id}'";
	echo "SQL='{$sql}'<br />";
	$db->q($sql);
	$aff = $db->affected();
	echo "affected rows = {$aff}<br /><br />";

	//done	
	echo "<strong>Done.</strong><br />Please:<br /><ul>";
	echo "<li>refresh cities</li>";
	echo "<li>refresh dir sphinx index</li>";
	echo "<li>flush memcached instances</li>";
	echo "<li>add \"{$loc_dir}\" => \"{$parent_loc_dir}\", to \$loc_redirect array in class.url</li>";
	echo "<li>update landing page</li>";
	echo "<li>?? update home_cache set state_id = {$parent_loc_id} where state_id = {$loc_id}; ??</li>";
	echo "</ul>";
}

function city_name_change() {
	global $db;

	$real_do = GetGetParam("real_do");

	//new neighborhood name and other location_location attributes
	$nb = GetGetParam("nb");
	$nb_dir = strtolower(str_replace(" ", "-", $nb));
	$nb_loc_url = "/{$nb_dir}/";

	//new location name
	$new_loc_name= GetGetParam("new_loc_name");
	$new_dir = strtolower(str_replace(" ", "-", $new_loc_name));
	$new_loc_url = "/{$new_dir}/";

	//get transformed location
	$loc_id = GetGetParam("loc_id");
	$sql = "select * from location_location where loc_id = '{$loc_id}' and loc_type = 3";
	$res = $db->q($sql);
	if ($db->numrows($res) != 1) {
		echo "Error: ".$db->numrows($res)." locations found for loc_id '{$loc_id}' !<br />";
		return;
	}
	$row = $db->r($res);
	$loc_name = $row['loc_name'];
	$country_id = $row['country_id'];
	$country_sub = $row['country_sub'];
	$loc_lat = $row['loc_lat'];
	$loc_long = $row['loc_long'];
	$loc_dir = $row["dir"];
	echo "loc_id={$loc_id}, loc_name=$loc_name, country_id={$country_id}, country_sub={$country_sub}, loc_lat={$loc_lat}, loc_long={$loc_long}<br />";

	if (!$real_do) {
        echo "<br />Do you want to continue and change city name from '{$loc_name}' to '{$new_loc_name}' (loc_id={$loc_id}, new dir='{$new_dir}') and assign places in this city without neighborhood to new neighborhood '{$nb}' (loc_url='{$nb_loc_url}', dir='{$nb_dir}') ?<br />";
	    echo "<form><input type=\"hidden\" name=\"action\" value=\"city_name_change\" />";
    	echo "<input type=\"hidden\" name=\"loc_id\" value=\"{$loc_id}\" />";
	    echo "<input type=\"hidden\" name=\"nb\" value=\"{$nb}\" />";
    	echo "<input type=\"hidden\" name=\"new_loc_name\" value=\"{$new_loc_name}\" />";
	    echo "<input type=\"hidden\" name=\"real_do\" value=\"1\" />";
    	echo "<input type=\"submit\" name=\"submit\" value=\"Yes\" /></form>";
        return;
    }

	//insert new neighborhood into location_location table
	$sql = "insert into location_location 
			(loc_type, loc_parent, country_id, county_id, country_sub, loc_name, loc_url, s, dir, loc_lat, loc_long)
			values
			('4', '{$loc_id}', '{$country_id}', '0', '{$country_sub}', '{$nb}', '{$nb_loc_url}', '', '{$nb_dir}', '{$loc_lat}', '{$loc_long}')";
	echo "SQL='{$sql}'<br />";
	$db->q($sql);
	$nb_id = $db->insertid();
	echo "neighborhood loc_id = '{$nb_id}'<br /><br />";
	if ($nb_id == 0) {
		echo "Error: newly inserted neighborhood has insetid 0 !<br />";
		return;
	}

	//update place table
	$sql = "update place set neighbor_id = '{$nb_id}' where loc_id = '{$loc_id}' and (neighbor_id = 0 OR neighbor_id is NULL)";
	echo "SQL='{$sql}'<br />";
	$db->q($sql);
	$aff = $db->affected();
	echo "affected rows = {$aff}<br /><br />";

	//update name for location
	$sql = "update location_location set loc_name = '{$new_loc_name}', loc_url = '{$new_loc_url}', s = '', dir = '{$new_dir}' where loc_id = '{$loc_id}'";
	echo "SQL='{$sql}'<br />";
	$db->q($sql);
	$aff = $db->affected();
	echo "affected rows = {$aff}<br /><br />";

	//done	
	echo "<strong>Done.</strong><br />Please:<br /><ul>";
	echo "<li>refresh cities</li>";
	echo "<li>refresh dir sphinx index</li>";
	echo "<li>flush memcached instances</li>";
	echo "<li>add \"{$loc_dir}\" => \"{$new_dir}\", to \$loc_redirect array in class.url and maybe modity others redirect target filenames to new one: '{$new_dir}'</li>";
	echo "<li>update landing page</li>";
	echo "</ul>";
}

?>
<?php

switch(GetGetParam("action")) {
	case 'moveToMajor':
		moveToMajor();
		break;
	case 'check_loc_attrs':
		return check_loc_attrs();
		break;
	case 'refreshCities':
		refreshCities();
		break;
	case 'addLocationVideo':
		addLocationVideo();
		break;
	case 'city_to_neighborhood':
		city_to_neighborhood();
		break;
	case 'city_name_change':
		city_name_change();
		break;
	default:
		break;
}

?>
<h2>Location utils</h2>

<a href="?action=refreshCities">Refresh cities</a> (Refreshes db tables for quick search. Use after adding new cities.)<br /><br />

<form action="" method="get">
<input type="hidden" name="action" value="moveToMajor" />
Move places from city <?php echo location::getLocationSelect("loc_old", "location_exact_id"); ?> to major city <?php echo location::getLocationSelect("loc_new", "loc_id"); ?>
<input type="submit" name="submit" value="submit" />
</form>
<br />

Check location attributes:
<form action="" method="get">
<select name="loc_id">
<option value="">-- Select --</option>
<?php
$res = $db->q("SELECT loc_id, loc_name, dir FROM location_location WHERE loc_type = 1 ORDER BY loc_name ASC");
while ($row = $db->r($res)) {
	echo "<option value=\"{$row["loc_id"]}\">{$row["loc_name"]}</option>\n";
}
?>
<input type="hidden" name="action" value="check_loc_attrs" />
<input type="submit" name="submit" value="submit" />
</form>
<br /><br />

<form action="" method="get">
<input type="hidden" name="action" value="addLocationVideo" />
Add video for location: <?php echo location::getLocationSelect("location", "loc_id"); ?>
<input type="submit" name="submit" value="submit" />
</form>
<br />
Change international city to neighborhood:
<form action="" method="get">
<input type="hidden" name="action" value="city_to_neighborhood" />
loc_id of city:<input type="text" name="loc_id" value="" /><br />
neighborhood name:<input type="text" name="nb" value="" /><br />
loc_id of parent city:<input type="text" name="parent_loc_id" value="" /><br />
<input type="submit" name="submit" value="submit" />
</form>
<br />
Change name of international city and create neighborhood for it:
<form action="" method="get">
<input type="hidden" name="action" value="city_name_change" />
loc_id of city:<input type="text" name="loc_id" value="" /><br />
new name of city:<input type="text" name="new_loc_name" value="" /><br />
neighborhood name for unassigned places:<input type="text" name="nb" value="" /><br />
<input type="submit" name="submit" value="submit" />
</form>
