<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access(""))
	die("Invalid access");

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols, $classifieds;

//configuration
$ipp = 30;
$url_last_piece = "location_attributes";
/*
$where_cols = array(
	"cid"		 => array("title" => "ID", "where" => "c.id", "match" => "exact", "size" => "7"),
);
*/

function blabla() {
}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'blabla':
		if (!permission::has("classified_undelete"))
			die;
		$ret = blabla();
		if (!$ret)
			return;
		break;
}

$where = getWhere();

$having = getHaving();
$order = getOrder();
$limit = getLimit();

//TODO add default order
/*
if (empty($order)) {
	//if we are filtering ads by account_id, list live ads first and also last reposted ads first
	if (array_key_exists("account_id", $filters))
		$order = "ORDER BY c.done DESC, cl.updated DESC";
	else
		$order = "ORDER BY c.id DESC";
}
*/

//query db
$sql = "SELECT count(*) as total 
		FROM (
			SELECT c.id, COUNT(cl.id) as loc_count
			FROM classifieds c
			LEFT JOIN account a on a.account_id = c.account_id
			LEFT JOIN classifieds_loc cl on cl.post_id = c.id
			$where
			GROUP BY c.id
			$having
			) a
		";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];


$sql = "SELECT c.id, c.thumb, c.account_id, c.bp, c.deleted, c.done, c.date, c.expires, c.type, c.phone, c.firstname, c.title, c.content, 
			c.sponsor, c.sponsor_mobile, c.promo_code, c.auto_renew, 
			a.email, a.whitelisted, 
			COUNT(cl.id) as loc_count, GROUP_CONCAT(cl.loc_id) as loc_ids,
			COUNT(cs.id) as sponsor_locs, SUM(cs.day) as sponsor_days, MAX(cs.expire_stamp) as sponsor_expire_stamp,
			(SELECT CONCAT('$',ROUND(ap.total, 2),' on ',ap.time) FROM account_purchase ap WHERE ap.item_id = c.id ORDER BY id DESC LIMIT 1) as last_purchase
		FROM classifieds c
		LEFT JOIN account a on a.account_id = c.account_id
		LEFT JOIN classifieds_loc cl on cl.post_id = c.id 
		LEFT JOIN classifieds_sponsor cs on cs.post_id = c.id
		$where
		GROUP BY c.id
		$having
		$order 
		$limit";

$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No location attribute assigns.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Attributes for locations</h2>\n";

displayFilterForm();

echo $pager."<br />";

echo "<form method=\"post\">";
	echo "<select name=\"action\"><option value=\"\">-</option>";
if (permission::has("classified_manage")) {
	echo "<option value=\"verify\">Verify ad(s)</option>";
	echo "<option value=\"makeLive\">Make live ad(s)</option>";
	echo "<option value=\"expire_ad\">Expire ad(s)</option>";
	echo "<option value=\"pay_ads\">Pay for these ads</option>";
}
if (permission::has("classified_delete")) {
	echo "<option value=\"delete_ad\">Delete ad(s)</option>";
	if (permission::has("account_ban")) {
		echo "<option value=\"delete_ad_ban_owner\">Delete ad(s) &amp; ban owner(s)</option>";
		echo "<option value=\"ban_owner_delete_all_his_ads\">Ban owner(s) of selected ads &amp; delete ALL their ads</option>";
	}
}
if (permission::has("manage_all_ads"))
	echo "<option value=\"undelete\">Undelete ad(s)</option>";

//	echo "<option value=\"move_to_phone_and_web\">Move to Phone &amp; Web</option>";
	//echo "<option value=\"free_renew_3_months\">Free renew for 3 months</option>";
if (permission::has("classified_manage")) {
	echo "<option value=\"free_prolong_week\">Free prolong week</option>";
	echo "<option value=\"free_prolong_month\">Free prolong month</option>";
}
	echo "</select>";
	echo "<input type=\"submit\" name=\"submit\" value=\"Execute...\" />";

echo getFilterFormFields();

echo "<table class=\"control\">";
echo "<thead><tr>
<th class=\"check\"><input type=\"checkbox\" name=\"master\" value=\"\" /></th>
<th>".getOrderLink("Id", "id")."</th>
<th>Posted</th>
<th>".getOrderLink("Status", "done")."</th>
<th>Thumb</th>
<th>Acc.ID</th>
<th>BP?</th>
<th>".getOrderLink("Type", "type")."</th>
<th>City cover</th>
<th>Promo</th>
<th>Sponsor</th>
<th>Phone</th>
<th>".getOrderLink("Firstname", "firstname")."</th>
<th>".getOrderLink("Title", "title")."</th>
<th>Location(s)</th>
<th>Last Purchase</th>
<th/>
</tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	$deleted = ($rox["deleted"]) ? true : false;

	if ($deleted)
		echo "<tr style=\"background-color: #FFAAAA;\">";
	else
		echo "<tr>";

	echo "<td class=\"check\"><input type=\"checkbox\" name=\"action_id[]\" value=\"{$rox['id']}\" /></td>";
	echo "<td>{$rox['id']}</td>";
	echo "<td>{$rox["date"]}</td>";

	if ($deleted) {
		echo "<td><span title=\"Done: {$rox["done"]}, Expires: {$rox["expires"]}\">DELETED</span></td>";
	} else {
		$status = "<span style=\"color: red;\">???</span>";
		switch($rox["done"]) {
			case -2: $status="<span title=\"Expires on {$rox["expires"]}\">To be removed</span>";break;
			case -1: $status="<span title=\"Expires on {$rox["expires"]}\">Not posted yet</span>";break;
			case 0: $status="<span title=\"Expired on {$rox["expires"]}\">Expired</span>";break;
			case 1: $status="<span title=\"Expires on {$rox["expires"]}\" style=\"font-weight: bold; color: green;\">Live</span>";break;
			case 2: $status="<span title=\"Expires on {$rox["expires"]}\">Invisible</span>";break;
			case 3: $status="<span title=\"Expires on {$rox["expires"]}\">Waiting</span>";break;
		}
		echo "<td>{$status}</td>";
	}

	if ($rox["thumb"])
		echo "<td><img src=\"//img.adultsearch.com/classifieds/{$rox["thumb"]}\" style=\"max-width: 75px;\"/></td>";
	else
		echo "<td></td>";

	if ($rox["account_id"] == 0) {
		echo "<td></td>";
	} else {
		echo "<td><a href=\"/mng/accounts?account_id={$rox["account_id"]}\">{$rox["account_id"]}</a><br />({$rox["email"]})";
		if ($rox["whitelisted"])
			echo "<br /><span style=\"font-weight: bold; color: green;\">whitelisted</span>";
		echo "</td>";
	}
	if ($rox["bp"] == 1)
		echo "<td>BP</td>";
	else
		echo "<td></td>";
	echo "<td>{$classified_types[$rox["type"]]}</td>";

	echo "<td>";
	if ($rox["sponsor_expire_stamp"]) {
		$sponsor_expire_stamp = $rox["sponsor_expire_stamp"];
		$sponsor_expire_datetime = date("m/d/Y H:i", $sponsor_expire_stamp);
		$now = time();
		if ($sponsor_expire_stamp > $now) {
			$sp_title = "City cover expires in ".time_elapsed_string($sponsor_expire_stamp)." ({$$sponsor_expire_datetime}) - paid for {$rox["sponsor_locs"]} location(s), in total for {$rox["sponsor_days"]} days";
			echo "<span title=\"{$sp_title}\"><span style=\"color: green;\">Live</span><br />({$rox["sponsor_locs"]} loc(s) / {$rox["sponsor_days"]} days)</span>";
		} else {
			$sp_title = "City cover expired on {$sponsor_expire_datetime} (".time_elapsed_string($sponsor_expire_stamp).") - paid for {$rox["sponsor_locs"]} location(s), in total for {$rox["sponsor_days"]} days";
			echo "<span title=\"{$sp_title}\">Expired<br />({$rox["sponsor_locs"]} loc(s) / {$rox["sponsor_days"]} days)</span>";
		}
	}
	echo "</td>";

	echo "<td>{$rox["promo_code"]}</td>";

	if ($rox["sponsor"] > 0 && $rox["sponsor_mobile"] > 0) {
		echo "<td style=\"font-weight: bold; color: green;\">Desktop &amp; mobile sponsor</td>";
	} else if ($rox["sponsor"] > 0) {
		echo "<td style=\"font-weight: bold; color: green;\">YES</td>";
	} else if ($rox["sponsor_mobile"] > 0) {
		echo "<td style=\"font-weight: bold; color: green;\">Mobile</td>";
	} else {
		echo "<td/>";
	}

	echo "<td>".makeproperphonenumber($rox["phone"])."</td>";
	echo "<td>{$rox["firstname"]}</td>";
	echo "<td>{$rox["title"]}</td>";

	$links = "";
	if (permission::has("classified_manage")) {
		$links .= "<a href=\"".getActionLink("manage", $rox['id'])."\">manage</a>&nbsp;&middot;&nbsp;";
	} else if (permission::has("classified_edit")) {
		$links .= "<a href=\"https://adultsearch.com/adbuild/step3?ad_id={$rox['id']}&ref={$_SERVER["REQUEST_URI"]}\">edit</a>&nbsp;&middot;&nbsp;";
	}

	//TODELETE
	//if ($_SESSION["account"] == 3974) {
	//	$links .= "<a href=\"/classifieds/manage?id={$rox['id']}\">old mng</a>&nbsp;&middot;&nbsp;";
	//}

	$at_least_one_loc_valid = false;
	if ($rox["loc_count"] > 0) {
		$arr = explode(',', $rox['loc_ids']);
		if (strlen($rox['loc_ids']) >= 1024) {
			array_pop($arr);
//			echo "<span style=\"color: red;\">These are not all the results!</span><br/>";
		}
		$type_url = $classifieds->getModuleByType($rox["type"]);
		$locs = "";
		$i = 0;
		foreach ($arr as $loc_id) {
			if ($i >= 3) {
				$locs .= ", ...";
				break;
			}
			$locs .= (empty($locs)) ? "" : "; ";
			$loc = location::findOneById($loc_id);
			if (!$loc) {
//				$links .= "<span style=\"color: red;\">Non-existent location id: $loc_id !</span>";
				$locs .= "Non-exist loc id=#{$loc_id}";
			} else {
				$at_least_one_loc_valid = true;
				$locs .= $loc->getName();
				if ($loc->getS())
					$locs .= ",".$loc->getS();
			}
			$i++;
		}
		if (permission::has("manage_all_ads")) {
			if ($rox["loc_count"] == 1)
				echo "<td><a href=\"/classifieds/move?id={$rox['id']}\" title=\"Change locations for classified ad #{$rox['id']}\">{$rox["loc_count"]} loc: {$locs}</a></td>";
			else
				echo "<td><a href=\"/classifieds/move?id={$rox['id']}\" title=\"Change locations for classified ad #{$rox['id']}\">{$rox["loc_count"]} locs: {$locs}</a></td>";
		} else {
			if ($rox["loc_count"] == 1)
				echo "<td>{$rox["loc_count"]} loc: {$locs}</td>";
			else
				echo "<td>{$rox["loc_count"]} locs: {$locs}</td>";
		}
	} else
		echo "<td><span style=\"color: red; font-weight: bold;\">no locations ?!</span></td>";

	echo "<td>{$rox["last_purchase"]}</td>";

	if ($at_least_one_loc_valid) {
		$links .= "<a href=\"".$loc->getUrl()."/{$type_url}/{$rox['id']}\" target=\"_blank\">view</a>";
	}

	if (permission::has("access_admin_sales"))
		$links .= "&nbsp;&middot; <a href=\"/mng/sales?account_id={$rox["account_id"]}&item_id={$rox['id']}\">sales</a>";

	if (permission::has("access_admin_audit"))
		$links .= "&nbsp;&middot; <a href=\"/mng/audit?type=CLA&p1={$rox['id']}\">history</a>";

	if ($rox["done"] == 0 && permission::has("classified_manage")) {
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("pay_ads", $rox['id'])."\">pay for ad</a>";
	}

	if (permission::has("account_whitelist") && !$rox["whitelisted"])
		$links .= "&nbsp;&middot; <a href=\"/mng/accounts?action=whitelist&action_id={$rox["account_id"]}\">whitelist_ad_owner</a>";

	if (permission::has("classified_blacklist"))
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("blacklist", $rox['id'])."\">blacklist</a>";

	if ($account->isrealadmin()) {
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("recreate_thumb", $rox['id'])."\">(re)create thumb</a>";
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("ebiz_create", $rox['id'])."\">ebiz_create</a>";
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("prevent_delete", $rox['id'])."\">prevent_delete</a>";
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("send_admin_notify_to_me", $rox['id'])."\">send_admin_notify_to_me</a>";
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("bump", $rox['id'])."\">bump</a>";
	}
	if (permission::has("classified_manage") && $rox["auto_renew"] > 0) {
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("repost", $rox['id'])."\">repost</a>";
	}

	$links .= "&nbsp;&middot; <a href=\"".getActionLink("resend_receipt", $rox['id'])."\">resend_receipt...</a>";

	if (permission::has("classified_undelete") && $deleted)
		$links .= "&nbsp;&middot; <a href=\"".getActionLink("undelete", $rox['id'])."\">undelete</a>";

	if (!$deleted || $account->isrealadmin()) {
		echo "<td>{$links}</td>";
	} else {
		echo "<td>{$links}</td>";
	}


	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo "</form>";
echo $pager;

?>
