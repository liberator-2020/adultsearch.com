<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("location_manage"))
	die("Invalid access");

//jay 2019-05-08 disabling google maps, needs to be redon into mapquest
//include_html_head("js","http://maps.googleapis.com/maps/api/js?key=AIzaSyCwh_-7FS9QabY_RjOzmC8Cv38GiVMCRS0&amp;sensor=false");
include_html_head("js","/js/ckeditor/ckeditor.js");

?>
<script type="text/javascript">

function country_changed() {
	$('#state_select').hide();
	var country_option = $('#country_id option:selected');
	$.ajax({
		url: '/_ajax/states',
		data: { country_id: country_option.val() },
		dataType: 'html',
		success: function(data, textStatus) {
			country_change_success(data, textStatus);
		}
	});
}

function country_change_success(data, textStatus) {
	if (data == '')
		return;
	$('#state_id').html(data);
	$('#state_select').show();
}

function get_state_short() {
	var state_shortname = '';
	if ($('#state_select').is(':visible') && $('#state_id option:selected').val() != '') {
		var state_text = $('#state_id option:selected').text();
		var n = state_text.indexOf('(');
		if (n != -1) {
			state_shortname = state_text.substring(n+1, state_text.length - 1);
		}
	}
	return state_shortname;
}

function search_location() {
	var location = $('#location').val();
	var country = $('#country_id option:selected').text();
	var state_shortname = get_state_short();
	if (state_shortname)
		state_shortname = '|administrative_area:'+state_shortname;
	var url = 'http://maps.googleapis.com/maps/api/geocode/json';
//  console.log('AJAX: location='+location+' country='+country+' url='+url);
	$.ajax({
		url: url,
		data: 'address='+encodeURIComponent(location)+'&components=country:'+encodeURIComponent(country)+state_shortname+'&sensor=false',
		dataType: 'json',
		success: function(data, textStatus) {
			search_location_success(data, textStatus);
		}
	});
}

function search_location_success(data, textStatus) {
	$('#search_results_header').show();
	$('div#add_location').hide();
	$('div#map-canvas').hide();
	$('#loc_search_results').empty();
	for (var i = 0, len = data.results.length; i < len; i++) {
		var entry = data.results[i];
		var acs = entry.address_components;
		var locality = '';
		var adm_area_1 = '';
		var country = '';
		for (var j = 0, len2 = acs.length; j < len2; j++) {
			var ac = acs[j];
			if ($.inArray("locality", ac.types) != -1)
				locality = '<span id="name_'+i+'">' + ac.long_name + "</span> ("+ac.short_name+")";
			else if ($.inArray("administrative_area_level_1", ac.types) != -1)
				adm_area_1 = ac.long_name+" ("+ac.short_name+")";
			else if ($.inArray("country", ac.types) != -1)
				country = ac.long_name+" ("+ac.short_name+")";
		}
		var lat = entry.geometry.location.lat;
		var lon = entry.geometry.location.lng;
		var location = locality + " - " + adm_area_1 + " - " + country + ' (<span id="lat_'+i+'">' + lat + '</span>, <span id="lon_'+i+'">' + lon + '</span>)';
		var newLi = document.createElement('li');
		newLi.setAttribute("data-ind", i);
		newLi.innerHTML = location;
		$('#loc_search_results').append(newLi);
	}
	$('#loc_search_results li').click(function() {
		search_loc_select($(this).attr("data-ind"));
	});
}

var map = '';
function search_loc_select(ind) {
	$('#add_location_result').text('');
	$('div#add_location').show();
	$('div#map-canvas').show();
	//fill new location inputs
	var country_option = $('#country_id option:selected');
	var state_option = $('#state_id option:selected');
	var name = $('#name_'+ind).text();
	var dir = name.toLowerCase().replace(/ /g, "-").replace(/[^a-zA-Z.-]/g, "");
	var lat = $('#lat_'+ind).text();
	var lon = $('#lon_'+ind).text();
	$('#loc_type').val(3);
	if ($('#state_select').is(':visible') && state_option.val()) {
		$('#loc_parent').val(state_option.val());
		$('#s').val(get_state_short());
	} else {
		$('#loc_parent').val(country_option.val());
		$('#s').val('');
	}
	$('#loc_country_id').val(country_option.val());
	$('#country_sub').val(country_option.attr('data-dir'));
	$('#loc_name').val(name);
	$('#dir').val(dir);
	$('#loc_lat').val(lat); 
	$('#loc_long').val(lon);
	//show on map
	var latlng = new google.maps.LatLng( lat, lon);
	var mapOptions = {
		zoom: 8,
		center: latlng
	};
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	var marker = new google.maps.Marker({
		position: latlng,
		map: map,
		title:"A"
	});
}

function add_location() {
	$.ajax({
		url: '/_ajax/addloc',
		type: 'POST',
		data: { 
			loc_type: $('#loc_type').val(),
			loc_parent: $('#loc_parent').val(),
			country_id: $('#loc_country_id').val(),
			country_sub: $('#country_sub').val(),
			loc_name: $('#loc_name').val(),
			dir: $('#dir').val(),
			s: $('#s').val(),
			loc_lat: $('#loc_lat').val(),
			loc_long: $('#loc_long').val()
			},
		dataType: 'html',
		success: function(data, textStatus) {
			add_location_success(data, textStatus);
		}
	}); 
}

function add_location_success(data, textStatus) {
	var result = '';
	var loc_id = parseInt(data);
	if (loc_id > 0)
		result = 'Location successfully added. Loc_id: '+loc_id;
	else if (data == '')
		result = 'Error while adding location. Data: '+data;
	else if (loc_id == -1)
		result = 'Location already exists';
	$('#add_location_result').text(result);
}

$(document).ready(function() {
	var country_option = $('#country_id option:selected');
	var state_option = $('#state_id option:selected');
	if (country_option.val() != '' && state_option.val() == '')
		country_changed();

	var toolbar1 = [[ 'Bold', 'Italic', 'Strikethrough', '-', 'RemoveFormat' ], ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'], ['Undo', 'Redo'], ['Link', 'Unlink'], [ 'Source' ], [ 'Maximize' ]];

//	CKEDITOR.replace('location_description', { 
//		toolbar: toolbar1,
//		allowedContent: 'p; br; strong; b; em; i; a[!href,!target];'
//	});

});

</script>
<?php



global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols;

//configuration
$ipp = 30;
$url_last_piece = "locations";
$where_cols = array(
	"id"	=> array("title" => "ID", "where" => "loc_id", "match" => "exact", "size" => "7"),
	"type"	=> array("title" => "Type", "where" => "loc_type", "type" => "select", "options" => array("" => "-", "1" => "Country", "2" => "State", "3" => "City")),
	"name" => array("title" => "Name", "where" => "loc_name"),
);


//add new location
function add() {
	global $db;

	$name = "";
	$state = "";
	$country_id = "";

	if (isset($_REQUEST["name"]))
		$name = preg_replace("/[^a-zA-Z. -]/", "", $_REQUEST["name"]);
	if (isset($_REQUEST["state"]))
		$state = preg_replace("/[^a-zA-Z. -]/", "", $_REQUEST["state"]);
	if (isset($_REQUEST["country_id"]))
		$country_id = intval($_REQUEST["country_id"]);

	$countries = array();
	$countries_dir = array();
	$res = $db->q("SELECT loc_id, loc_name, dir FROM location_location WHERE loc_type = 1 ORDER BY loc_name ASC");
	while ($row = $db->r($res)) {
		$countries[$row["loc_id"]] = $row["loc_name"];
		$countries_dir[$row["loc_id"]] = $row["dir"];
	}

	$states_options = "";
	$state_found = false;
	if ($country_id) {
		$res = $db->q("SELECT loc_id, loc_name, s FROM location_location WHERE loc_type = 2 AND loc_parent = ?", array($country_id));
		if ($db->numrows($res) > 0) {
			$states_options = "<option value=\"\">-- Select --</option>\n";
			while($row = $db->r($res)) {
				$selected = "";
				if ($state && ($state == $row["loc_name"] || $state == $row["s"])) {
					$state_found = true;
					$selected = " selected=\"selected\" ";
				}
				$states_options .= "<option value=\"{$row["loc_id"]}\"{$selected}>{$row["loc_name"]} ({$row["s"]})</option>\n";
			}
		}
	}
	$state_not_found = "";
	if ($state && !$state_found)
		$state_not_found = "<span style=\"color: red;\">State <strong>{$state}</strong> was not found !</span>\n";
	
	?>
	<style type="text/css">
	ul#loc_search_results li { cursor: pointer; }
	ul#loc_search_results li:hover { background-color: #FFE87C; }
	</style>

	<h1>Add new location</h1>
	<table>
	<tr>
	<td>
	<h3>Params</h3>
	<form action="#" method="get" onsubmit="search_location(); return false;">
	Country: <select id="country_id" name="country_id" onchange="country_changed();">
	<option value="">-- Select --</option>
	<?php
	foreach ($countries as $key => $val) {
		$selected = "";
		if ($key == $country_id)
			$selected = " selected=\"selected\" ";
		echo "<option value=\"{$key}\"{$selected} data-dir=\"".htmlspecialchars($countries_dir[$key])."\">".htmlspecialchars($val)."</option>\n";
	}
	echo "</select><br />\n";

	$css = "";
	if (!$states_options)
		$css = " style=\"display: none;\"";
	echo "<div id=\"state_select\" {$css} >State: <select id=\"state_id\" name=\"state_id\" >{$states_options}\n</select>{$state_not_found}<br /></div>\n";

	echo "Location name: <input type=\"text\" id=\"location\" name=\"location\" value=\"{$name}\" /><br />\n";
	?>
	<button onclick="search_location();">Search</button><br />
	</form>
	<br />
	<h3 id="search_results_header" style="display: none;">Search results</h3>
	<ul id="loc_search_results" style="list-style-type: disc;"></ul>
<?php
	//<div id="add_location" style="display: none;">
?>
	<div id="add_location">
	<br />
	<h3>Selected location</h3>
	<table>
		<tr><th>Type</th><th>Parent</th><th>Country</th><th>Country sub</th><th>Name</th><th>S</th><th>Dir</th><th>Latitude</th><th>Longitude</th></tr>
		<tr>
			<td><input type="text" id="loc_type" name="loc_type" value="" size="3" /></td>
			<td><input type="text" id="loc_parent" name="loc_parent" value="" size="5" /></td>
			<td><input type="text" id="loc_country_id" name="loc_country_id" value="" size="5" /></td>
			<td><input type="text" id="country_sub" name="country_sub" value="" size="12" /></td>
			<td><input type="text" id="loc_name" name="loc_name" value="" size="18" /></td>
			<td><input type="text" id="s" name="s" value="" size="3" /></td>
			<td><input type="text" id="dir" name="dir" value="" size="18" /></td>
			<td><input type="text" id="loc_lat" name="loc_lat" value="" size="10" /></td>
			<td><input type="text" id="loc_long" name="loc_long" value="" size="10" /></td>
		</tr>
	</table>
	<button onclick="add_location();">Add location</button><br />
	<div id="add_location_result"></div>
	</div>
	</td>
	<td>
		<div id="map-canvas" style="width: 600px; height: 500px;"></div>
	</td>
	</tr>
	</table>
	<?php

	return false;
}


function upload_location_image($action_id, $input_file_name, &$filenames) {
	global $config_image_path;
	$system = new system();

	$upl = new upload($_FILES[$input_file_name]);
	if ($upl->uploaded) {
		$upl->image_max_width = 500;
		$upl->image_max_height = 500;
		$upload_dir = $config_image_path."location/";
		$filename = $action_id."_".$system->_makeText(5);
		$i = 0;
		while (file_exists($upload_dir.$filename.".jpg") && $i < 10) {
			$filename = $action_id."_".$system->_makeText(5);
			$i++;
		}
		if ($i >= 10)
			die("hell!");
		$upl->file_new_name_body = $filename;
		$upl->file_new_name_ext = "jpg";
		$upl->process($upload_dir);
		if (!$upl->processed)
			die("mayhem: '".$upl->error."' !");
		$filename = $filename.".jpg";
		$filenames[] = $filename;
	}
	return;
}

function edit() {
	global $config_image_path, $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid location id !");

	$location = location::findOnebyId($action_id);
	if ($location == false)
		return actionError("Can't find location for id={$action_id} !");

	if (isset($_REQUEST["submit"])) {

		//set params	
		if (isset($_REQUEST["name1"]))
			$name = $_REQUEST["name1"];
		if (isset($_REQUEST["description1"]))
			$description = $_REQUEST["description1"];
		if (isset($_REQUEST["video_html1"]))
			$video_html = $_REQUEST["video_html1"];
		//if (isset($_REQUEST["paused1"]))
		//	$paused = 1;
		//else
		//	$paused = 0;

		$filenames = "";
		upload_location_image($action_id, "image1", $filenames);
		upload_location_image($action_id, "image2", $filenames);

		//check params
		if ($name == "")
			echo "Error: Please enter name for the location !<br />\n";
		else {
			$location->setName($name);
			$location->setDescription($description);
			$location->setVideoHtml($video_html);
			foreach ($filenames as $filename) {
				$ret = $location->addImageFilename($filename);
				if (!$ret)
					die("devil!");
			}
			$ret = $location->update();
			if ($ret) {
				return actionSuccess("You have successfully updated location id #{$action_id}.");
			}
			else
				return actionError("Error while updating location id #{$action_id}.");
		}
	}
	
	echo "<h2>Edit location #{$action_id} - {$location->getLabel()}</h2>";
	echo "<form method=\"post\" action=\"\" id=\"editlocationform\" enctype=\"multipart/form-data\">";
	echo "<table>";
	echo "<tr><th>Id: </th><td>{$location->getId()}</td></tr>\n";
	echo "<tr><th>Name: </th><td><input type=\"text\" name=\"name1\" size=\"60\" value=\"{$location->getName()}\"></td></tr>\n";
	echo "<tr><th>Description: </th><td><textarea id=\"location_description\" class=\"ckeditor\" name=\"description1\" cols=\"60\" rows=\"10\">{$location->getDescription()}</textarea></td></tr>\n";
	echo "<tr><th>Video HTML: </th><td><input type=\"text\" name=\"video_html1\" size=\"160\" value=\"".htmlspecialchars($location->getVideoHtml())."\"></td></tr>\n";
	$imgs = explode("|", $location->getImageFilenames());
	if ($imgs) {
		echo "<tr><th>Image(s): </th><td>";
		foreach ($imgs as $img) {
			if ($img)
				echo "<img src=\"http:{$config_image_server}/location/{$img}\" width=\"100\" height=\"100\" /><br />";
		}
		echo "</td></tr>\n";
	}
	echo "<tr><th>Image upload: </th><td><input type=\"file\" name=\"image1\" /><br /><input type=\"file\" name=\"image2\" /></td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Update\" /></td></tr>\n";
	echo "</table>";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}

function delete() {
	global $account;
	if (!$account->isAdmin())
		return actionError("You need to be admin to delete locations !");
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid ad id !");
	return actionError("Error while deleting ad id #{$action_id}.");
}


$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'add':
		$ret = add();
		if (!$ret)
			return;
		break;
	case 'edit':
		$ret = edit();
		if (!$ret)
			return;
		break;
	case 'delete':
		$ret = delete();
		if (!$ret)
			return;
		break;
}

$params = [];
$where = getWhere($params);
$order = getOrder();
$limit = getLimit();

//query db
$sql = "SELECT count(*) as total 
		FROM location_location
		$where
		";

$res = $db->q($sql, $params);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT l.*
		FROM location_location l
		$where
		$order 
		$limit";

$res = $db->q($sql, $params);

//pager
$pager = getPager($total);

//output
echo "<h2>Locations</h2>\n";
echo "<a href=\"/mng/locations?action=add\">Add new location</a><br />\n";
displayFilterForm();
echo $pager."<br />";
echo getFilterFormFields();

if ($db->numrows($res) == 0) {
	echo "No BP promo ads.";
	echo "</form>";
	return;
}

echo "<table class=\"control\">";
echo "<thead><tr>
<th class=\"check\"><input type=\"checkbox\" name=\"master\" value=\"\" /></th>
<th>".getOrderLink("Id", "loc_id")."</th>
<th>".getOrderLink("Type", "loc_type")."</th>
<th>".getOrderLink("Name", "loc_name")."</th>
<th>".getOrderLink("S", "s")."</th>
<th>".getOrderLink("Lat", "loc_lat")."</th>
<th>".getOrderLink("Lng", "loc_long")."</th>
<th/>
</tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<tr>";
	echo "<td class=\"check\"><input type=\"checkbox\" name=\"id[]\" value=\"{$rox['loc_id']}\" /></td>";
	echo "<td>{$rox["loc_id"]}</td>";

	if ($rox["loc_type"] == 1)
		$loc_type_name = "Country";
	else if ($rox["loc_type"] == 2)
		$loc_type_name = "State";
	else if ($rox["loc_type"] == 3)
		$loc_type_name = "City";
	else
		$loc_type_name = $rox["loc_type"];
	echo "<td>{$loc_type_name}</td>";
	
	echo "<td>{$rox["loc_name"]}</td>";	
	echo "<td>{$rox["s"]}</td>";	
	
	echo "<td>{$rox["loc_lat"]}</td>";
	echo "<td>{$rox["loc_long"]}</td>";

	echo "<td>";
	echo "<a href=\"".getActionLink("edit", $rox['loc_id'])."\">Edit</a>&nbsp;&middot&nbsp;";
	//	echo "<a href=\"".getActionLink("pause", $rox['id'])."\">Pause</a>&nbsp;&middot&nbsp;";

	echo "<a class=\"delete\" href=\"".getActionLink("delete", $rox['loc_id'])."\">Delete</a>";
	echo "</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo "</form>";
echo $pager;

?>
