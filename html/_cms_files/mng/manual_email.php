<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!account::ensure_admin())
	die("Invalid access");

echo "<h1>Manual email send form</h1>";

if ($_REQUEST["submit"] == "submit") {
	$from = $_REQUEST["from"];
	$reply_to = $_REQUEST["reply_to"];
	$to = $_REQUEST["to"];
	$subject = $_REQUEST["subject"];
	$html = $_REQUEST["html"];
	$txt = $_REQUEST["txt"];
	$params = array(
			"from" => $from,
			"to" => $to,
			"subject" => $subject,
			"html" => $html,
			"text" => $txt,
			);
	if ($reply_to)
		$params["reply_to"] = $reply_to;
	$error = "";
	$ret = send_email($params, $error);
	if ($ret)
		return actionSuccess("Email has been successfully sent to {$to}.", "/mng/manual_email");
	echo "<span style=\"color: red; font-weight: bold;\">Error sending email: '{$error}' !</span></td></tr>";
}

?>
<form method="post">
<table>
	<tr><td>From:</td><td><input type="text" name="from" size="30" value="<?php echo $from;?>" /></td></tr>
	<tr><td>Reply to:</td><td><input type="text" name="reply_to" size="30" value="<?php echo $reply_to;?>" /></td></tr>
	<tr><td>To:</td><td><input type="text" name="to" size="30" value="<?php echo $to;?>" /></td></tr>
	<tr><td>Subject:</td><td><input type="text" name="subject" size="50" value="<?php echo $subject;?>" /></td></tr>
	<tr><td>Txt:</td><td><textarea name="txt" cols="50" rows="5"><?php echo $txt;?></textarea></td></tr>
	<tr><td>Html:</td><td><textarea name="html" cols="50" rows="5"><?php echo $html;?></textarea></td></tr>
	<tr><td></td><td><input type="submit" name="submit" value="submit" /></td></tr>
</table>
</form>
