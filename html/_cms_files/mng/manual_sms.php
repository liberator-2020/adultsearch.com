<?php

use App\Service\Sms\Sms;
use App\Entity\Message;

error_reporting(E_ALL);
ini_set('log_errors', true);
ini_set('display_errors', true);
ini_set('max_execution_time', 0);

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!account::ensure_admin())
	die("Invalid access");

echo "<h1>Manual sms send form</h1>";

if ($_REQUEST['submit'] === 'submit') {
	$to       = $_REQUEST['to'];
	$sendType = $_REQUEST['sendType'];
	$txt      = $_REQUEST['txt'];

	$smsService = new Sms();
	$message    = new Message();

	$message->setToNumber($to);
	$message->setText($txt);

	switch ($sendType) {
		case 'plivo' :
			$result = $smsService->sendViaPlivo($message);
			break;
		case 'swift' :
			$result = $smsService->sendViaSwift($message);
			break;
		case 'important' :
			$message->setImportant(true);
			$result = $smsService->send($message);
			break;
		default:
			$result = $smsService->send($message);
	}

	if ($result) {
		return actionSuccess("SMS has been successfully sent to {$to}.", '/mng/manual_sms');
	}
	echo "<span style=\"color: red; font-weight: bold;\">Error sending SMS: '".$sms->error."' !</span><br />";
}

?>
<form method="post">
	<table>
		<tr>
			<td>To:</td>
			<td><input type="text" name="to" value="<?php echo $to; ?>"/><br/>
				<small>e.g. <strong>+27 76 113 9912</strong> or <strong>(702) 123 5566</strong></small>
			</td>
		</tr>
		<tr>
			<td>Provider:</td>
			<td>
				<select name="sendType">
					<option value="plivo">Plivo</option>
					<option value="swift">Swift</option>
					<option value="important" selected>Important(Both Swift and Plivo)</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Txt:</td>
			<td><textarea name="txt" cols="50" rows="5"><?php echo $txt; ?></textarea></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" name="submit" value="submit"/></td>
		</tr>
	</table>
</form>
