<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) { 
	$account->asklogin(); 
	return false; 
}

if (!$account->isrealadmin()) {
	echo "You dont have privileges to access this page!";
	die;
}

global $url_last_piece, $where_cols;

//configuration
$url_last_piece = "permission";
$where_cols = array(
	"id" => array("title" => "Id", "where" => "id", "match" => "exact", "size" => 5),
	"name"   => array("title" => "Name", "where" => "name"),
);

function add() {
	global $db;

	if (!isset($_REQUEST["u_submit"])) {
		echo "<h2>Add new permission</h2>";
		echo "<form method=\"post\" action=\"\">";
		echo "<table>";
		echo "<tr><th>Name:</th><td><input type=\"text\" name=\"u_name\" value=\"\" /></td></tr>\n";
		echo "<tr><th></th><td><input type=\"submit\" name=\"u_submit\" value=\"Submit\" /></td></tr>\n";
		echo "</table>";
		echo "</form>";
		return true;
	}

	//were going live
	$name = $_REQUEST["u_name"];
	$res = $db->q("INSERT INTO permission (name) VALUES (?)", array($name));
	$id = $db->insertid();
	if ($db->affected($res) == 1) {
		return actionSuccess("New permission has been added.");
	} else {
		return actionError("Error while adding new permission - please contact administrator!");
	}
}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'add':
		$ret = add();
		if (!$ret)
			return;
		break;
}

$where = getWhere();
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
	$order = "ORDER BY id ASC";

//query db
$sql = "SELECT count(*) as total
		FROM permission
		$where";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT id, name
		FROM permission
		$where
		$order 
		$limit";
$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No permissions.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Permissions</h2>\n";
echo "<a href=\"?action=add\">Add new permission</a><br />\n";
displayFilterForm();
echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr><th>".getOrderLink("Id", "id")."</th><th>".getOrderLink("Name", "name")."</th><th/></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<td>{$rox['id']}</td>";
	echo "<td>{$rox['name']}</td>";
	echo "<td></td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;


?>
