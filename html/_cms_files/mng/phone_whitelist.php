<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
require_once(_CMS_ABS_PATH."/inc/classes/parsecsv.lib.php");

set_time_limit(0);

define('UPL_DIR', _CMS_ABS_PATH."/tmp");

global $account, $db, $num_errors, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

$num_errors = 0;

if (!account::ensure_admin())
	die("Invalid access");

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols;

//configuration
$ipp = 30;
$url_last_piece = "phone_whitelist";
$where_cols = array(
	"id" => array("title" => "Id", "where" => "id", "match" => "exact", "size" => 5),
	"phone" => array("title" => "Phone", "where" => "phone", "size" => 12),
);

function upload() {
	global $db;

	//checks
	if (!is_dir(UPL_DIR)) {
		echo "<span style=\"color: red;\">Tmp upload dir '".UPL_DIR."' does not exists !</span><br />";
		return false;
	}
	$filepath = UPL_DIR."/"."phone-whitelist-upload.csv";

	$live = intval($_REQUEST["live"]);
	$source = $_REQUEST["source"];

	//echo "live={$live}";
	if ($live == 0) {
		echo '<h1>Upload CSV file with phones</h1>';
		echo '<form action="" method="post" enctype="multipart/form-data">';
		echo '<input type="hidden" name="action" value="upload" />';
		echo '<input type="hidden" name="live" value="1" />';
		echo 'Please select CSV file :<input type="file" name="file" /><br />';
		echo 'Source: <input type="text" name="source" value="" /><br />';
		echo '<input type="submit" name="submit" value="Upload" />';
		echo '</form>';
		return false;
	}

	if ($live == 1) {	
		//store csv file into temp directory
		$file = new Upload($_FILES['file']);
		if (!$file->uploaded) {
			echo "<span style=\"color: red;\">Error while uploading the file !</span><br />";
			return false;
		}
		@unlink($filepath);
		$file->file_new_name_body = "phone-whitelist-upload";
		$file->file_new_name_ext = "csv";
		$file->process(UPL_DIR."/");
		if (!$file->processed) {
			echo "<span style=\"color: red;\">Error while copying uploaded csv file to directory '".UPL_DIR."' !</span><br />";
			return false;
		}
	}
	
	//parse csv file
	$csv = new parseCSV();
	$csv->heading = false;
	$csv->delimiter = ",";
	$csv->parse($filepath);
	$num_entries = count($csv->data);
	if ($num_entries > 0)
		echo "Parsed CSV file: <strong>{$num_entries}</strong> entries.<br />\n";
	else {
		echo "<span style=\"color: red;\">Error: CSV file does not have any entry.</span><br />\n";
		return;
	}

	$now = time();
	$total = $errors = $dupes = $imported = 0;
	foreach ($csv->data as $row) {
		$total++;

		$phone = $row[0];

		$error = "";
		//echo "phone={$phone}<br />\n";
		$phone = phone_to_international($phone, $error);
		//echo "phone={$phone}<br />\n";

		if ($phone === false) {
			$errors++;
			echo "<span style=\"color: red;\">Error:</span> '{$phone}': {$error}<br />\n";
			continue;
		}

		if (strlen($phone) > 14) {
			$errors++;
			echo "<span style=\"color: red;\">Error:</span> phone number '{$phone}' is too long<br />\n";
			continue;
		}

		//check dupe
		$res = $db->q("SELECT phone FROM phone_whitelist WHERE phone = ?", [$phone]);
		if ($db->numrows($res) > 0) {
			$dupes++;
			echo "Note: '{$phone}' is already in db<br />\n";
			continue;
		}

		//insert into db
		$res = $db->q("INSERT INTO phone_whitelist (phone, source, added_stamp) VALUES (?, ?, ?)", [$phone, $source, $now]);
		$id = $db->insertid($res);
		if (!$id) {
			$errors++;
			echo "<span style=\"color: red;\">Error:</span> failed to insert phone number '{$phone}' into db<br />\n";
			continue;
		}

		$imported++;
	}

	echo "<br /><br />Successfully finished: total=<strong>{$total}</strong>, errors=<strong>{$errors}</strong>, dupes=<strong>{$dupes}</strong>, imported=<strong>{$imported}</strong><br />\n";
	
	return false;
}

switch ($_REQUEST["action"]) {
	case 'upload':
		$ret = upload();
		if (!$ret)
			return;
		break;
}

$where = getWhere();
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
	$order = "ORDER BY id ASC";

//query db
$sql = "SELECT count(*) as total
		FROM phone_whitelist
		$where";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT id, phone, source, added_stamp
		FROM phone_whitelist
		$where
		$order 
		$limit";
$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No phones in whitelist.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Phone whitelist</h2>\n";
echo "<a href=\"?action=upload\">Upload CSV with whitelisted phone numbers</a><br />\n";
displayFilterForm();
echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr><th>".getOrderLink("Id", "id")."</th><th>".getOrderLink("Phone", "phone")."</th><th>Source</th><th/></tr></thead>\n";
echo "<tbody>";
while ($row = $db->r($res)) {
	echo "<td>{$row['id']}</td>";
	echo "<td>{$row['phone']}</td>";
	echo "<td>{$row['source']}</td>";
	echo "<td></td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;

//END
