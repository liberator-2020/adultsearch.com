<?php
require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("edit_all_places"))
	return;

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols, $classifieds;

//configuration
$ipp = 30;
$url_last_piece = "place_duplicities";
$where_cols = [];

$order = getOrder();
//TODO add default order
if (empty($order)) {
	$order = " ORDER BY l.loc_name ASC ";
}

$limit = getLimit();

//query db
$sql = "
select count(*) as total from (
select l.loc_name, l.s, p.name, p.loc_id, p.address1, p.phone1, count(*)
from place p
inner join location_location l on l.loc_id = p.loc_id
where p.deleted IS NULL and p.address1 IS NOT NULL and p.address1 <> ''
group by p.name, p.loc_id, p.address1, p.phone1
having count(*) > 1
) a
";
$res = $db->q($sql, $params);
$row = $db->r($res);
$total = $row['total'];

$sql = "
select l.loc_name, l.s, p.name, p.loc_id, p.address1, p.phone1, GROUP_CONCAT(p.place_id) as place_ids, count(*)
from place p
inner join location_location l on l.loc_id = p.loc_id
where p.deleted IS NULL and p.address1 IS NOT NULL and p.address1 <> ''
group by p.name, p.loc_id, p.address1, p.phone1
having count(*) > 1
		$order 
		$limit";

$res = $db->q($sql, $params);
if ($db->numrows($res) == 0) {
	echo "No place duplicities.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Place duplicities</h2>\n";

displayFilterForm();

echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr>
<th>".getOrderLink("Location", "l.loc_name")."</th>
<th>".getOrderLink("Place name", "p.name")."</th>
<th>Address</th>
<th>Phone</th>
<th></th>
</tr></thead>\n";
echo "<tbody>";
while ($row = $db->r($res)) {

	echo "<tr>";
	echo "<td>{$row['loc_name']}, {$row["s"]}</td>";
	echo "<td>{$row["name"]}</td>";
	echo "<td>{$row["address1"]}</td>";
	echo "<td>{$row["phone1"]}</td>";

	echo "<td>";
	$place_ids = explode(",", $row["place_ids"]);
	foreach ($place_ids as $place_id) {
		echo "<a href=\"/dir/place?id={$place_id}\">#{$place_id}</a>&nbsp;&nbsp;\n";
	}
	echo "</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;
echo "</form>";

?>
