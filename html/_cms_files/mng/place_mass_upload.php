<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
require_once(_CMS_ABS_PATH."/inc/classes/parsecsv.lib.php");

define('UPL_DIR', _CMS_ABS_PATH."/../data/tmp");

global $account, $db, $num_errors, $gIndexTemplate, $dupes, $to_import, $masseuse_values, $us_states;
$gIndexTemplate = "admin_index.tpl";
$num_errors = 0;
$dupes = 0;
$to_import = 0;

if (!account::ensure_admin())
	die("Invalid access");


//----------------
//--- PRELOADS ---

//preload masseuses attr values
$masseuse_values = [];
$res = $db->q("SELECT options FROM attribute WHERE attribute_id = 179 LIMIT 1", []);
if (!$db->numrows($res))
	die("ERR - cant read masseuse attr value options");
$row = $db->r($res);
$arr1 = explode("|", $row["options"]);
foreach($arr1 as $a) {
	$arr2 = explode("@", $a);
	$masseuse_values[strtolower($arr2[1])] = $arr2[0];
}
//echo print_r($masseuse_values, true)."<br />\n";die;

//preload US states
$us_states = [];
$res = $db->q("SELECT loc_id, loc_name FROM location_location WHERE loc_parent = 16046", []);
if (!$db->numrows($res))
	die("ERR - cant preload US states");
while ($row = $db->r($res)) {
	$us_states[$row["loc_name"]] = $row["loc_id"];
}
//echo print_r($us_states, true)."<br />\n";die;


function guess_col_name($title) {
	$title = strtolower($title);
	if (in_array($title, ["name", "store name"]))
		return "name";
	else if (in_array($title, ["address"]))
		return "address";
	else if (in_array($title, ["city"]))
		return "city";
	else if (in_array($title, ["state"]))
		return "state";
	else if (in_array($title, ["zip", "zipcode"]))
		return "zipcode";
	else if (in_array($title, ["country"]))
		return "country";
	else if (in_array($title, ["phone"]))
		return "phone";
	else if (in_array($title, ["site", "website"]))
		return "website";
	else if (in_array($title, ["masseuse", "masseuse ethnicity"]))
		return "masseuse";
	else if (in_array($title, ["cards", "cards accepted"]))
		return "cards";
	else if (in_array($title, ["table shower"]))
		return "table_shower";
	else if (in_array($title, ["sauna"]))
		return "sauna";
	else if (in_array($title, ["jacuzzi"]))
		return "jacuzzi";
	else if (in_array($title, ["truck parking"]))
		return "truck_parking";
	else if (in_array($title, ["rates"]))
		return "rates";
	else if (in_array($title, ["hours"]))
		return "hours";
	return null;
}

function echo_column_assign($num, $guess) {
	$col_types = [
		"name", "address", "city", "state", "zipcode", "country", "phone", "website"
		, "masseuse", "cards", "table_shower", "sauna", "jacuzzi", "truck_parking", "rates", "hours"
		];
	echo "<select name=\"col_{$num}\">";
	echo "<option value=\"\">-- Select --</option>";
	foreach ($col_types as $col_type) {
		$selected = "";
		if ($col_type == $guess)
			$selected = " selected=\"selected\" ";
		echo "<option value=\"{$col_type}\"{$selected}>{$col_type}</option>";
	}
	echo "</select>";
}

function er($msg, $fix = "") {
	global $num_errors;
	echo "<span style=\"color: red;\"><strong>Error</strong>: {$msg}</span> {$fix}<br />\n";
	$num_errors++;
}

function get_loc($country, $state, $city) {
	global $mcache, $db, $us_states;

	if ($state == "District of Columbia")
		$state = "Washington DC";

	if ($country == "United States") {
		if (!array_key_exists($state, $us_states))
			die("State '{$state}' not in preloaded states !");
		$state_id = $us_states[$state];
		$res = $db->q("
			SELECT l.loc_id, l.loc_name, l.loc_url, l.dir
			FROM location_location l
			WHERE l.loc_parent = ? AND l.loc_name = ?
			",
			[$state_id, $city]
			);
		if ($db->numrows($res) == 1) {
			$row = $db->r($res);
			return [
				"loc_id" => $row["loc_id"],
				"loc_name" => $row["loc_name"],
				"state_id" => $state_id,
				"country_id" => 16046, 
				"country_sub" => "", 
				"link" => location::getUrlStatic("", $row["loc_url"], $row["dir"]),
				"loc_url" => $row["loc_url"],
				"dir" => $row["dir"],
				];
		}
	}

	$loc_id = "";
	$loc_name = "";
	$country_id = "";
	$state_id = "";
	if ($country) {
		$row = $mcache->get("SQL:LOC-CTR-NAM",array($country));
		if ($row === false) {
			er("Cant find country '{$country}'");
			return array();
		}
		$country_id = $row["loc_id"];
	}
	if ($country_id) {
		if ($state) {
			$row = $mcache->get("SQL:LOC2-S-CTR",array($state, $country_id));
			if ($row === false) {
				$row = $mcache->get("SQL:LOC2-NAM-CTR",array($state, $country_id));
				if ($row === false) {
					er("Cant find state '{$state}' for country '{$country}', country_id '{$country_id}'");
					return array();
				}
			}
			$state_id = $row["loc_id"];
		}
		if ($state_id) {
			$row = $mcache->get("SQL:LOC-NAM-PAR",array($city, $state_id));
			if ($row === false) {
				er("Cant find city '{$city}', state={$state}, state_id='{$state_id}'", "<a href=\"/mng/locations?action=add&name={$city}&state={$state}&country_id={$country_id}\">Add {$city} to {$country}</a>.");
				return array();
			}
		} else {
			$row = $mcache->get("SQL:LOC-NAM-CTR",array($city, $country_id));
			if ($row === false) {
				er("Cant find city '{$city}', country_id='{$country_id}'", "<a href=\"/mng/locations?action=add&name={$city}&state={$state}&country_id={$country_id}\">Add {$city} to {$country}</a>.");
				return array();
			}
		}
		$loc_id = $row["loc_id"];
		$loc_name = $row["loc_name"];
	} else {
		$row = $mcache->get("SQL:LOC-NAM",array($city));
		if ($row === false) {
			er("Cant find city '{$city}'");
			return array();
		}
		$loc_id = $row["loc_id"];
		$loc_name = $row["loc_name"];
	}

	if (!$loc_id || !$country_id) {
		er("Loc_id not acquired loc_id={$loc_id} country_id={$country_id}");
		return array();
	}

	return [
		"loc_id" => $loc_id, 
		"loc_name" => $loc_name, 
		"state_id" => $state_id, 
		"country_id" => $country_id, 
		"country_sub" => $row["country_sub"], 
		"link" => location::getUrlStatic($row["country_sub"], $row["loc_url"], $row["dir"]),
		"loc_url" => $row["loc_url"],
		"dir" => $row["dir"],
		];
}

function get_data($csv, $col_types) {
	$data = [];
	foreach ($csv->data as $key => $row) {
		$item = [];
		$i = 0;
		foreach ($row as $value) {
			$value = trim($value);
			if ($col_types[$i])
				$item[$col_types[$i]] = $value;
			$i++;
		}
		$data[] = $item;
	}
	return $data;
}

function do_extra_insert($place_id, $attr_id, $value) {
	global $db;
	$res = $db->q("INSERT INTO place_attribute (place_id, attribute_id, value) VALUES (?, ?, ?)", [$place_id, $attr_id, $value]);
	$pa_id = $db->insertid($res);
	if (!$pa_id)
		die("Error: failed to insert extra attribute: place_id=$place_id, attribute_id=$attr_id, value=$value");
	return true;
}

function hm2int($hours, $minutes, $ampm, $from_or_to) {
	$num = (60*intval($hours)) + intval($minutes);
	if ($ampm == "p")
		$num += 720;
	if ($ampm == "a" && $from_or_to == 1)
		$num += 1440;
	return $num;
}
function extra_insert($item, $attr_name, $attr_type, $attr_id, $place_id) {
	global $db, $masseuse_values;

	if (!isset($item[$attr_name]))
		return;
	
	if ($attr_type == "boolean3") {
		$a = trim(strtolower($item[$attr_name]));
		//echo "ei: '{$attr_name}' -> '{$a}'<br />\n";
		$value = ($a == "yes") ? 2 : (($a == "no" ? 1 : null));
		//echo "ei: value='{$value}<br />\n";
		if ($value) {
			//echo "{$attr_name} -> {$value}<br />\n";
			return do_extra_insert($place_id, $attr_id, $value);
		}
		return true;

	} else if ($attr_type == "boolean") {
		$a = trim(strtolower($item[$attr_name]));
		//echo "ei: '{$attr_name}' -> '{$a}'<br />\n";
		$value = ($a == "yes") ? 1 : null;
		//echo "ei: value='{$value}<br />\n";
		if ($value) {
			//echo "{$attr_name} -> {$value}<br />\n";
			return do_extra_insert($place_id, $attr_id, $value);
		}
		return true;

	} else if ($attr_type == "cards") {
		$a = trim(strtolower($item[$attr_name]));
		if (stripos($a, "visa") !== false || stripos($a, "mastercard") !== false) {
			//echo "visa/mc<br />\n";
			do_extra_insert($place_id, 29, 1);
		}
		if (stripos($a, "discover") !== false) {
			//echo "discover<br />\n";
			do_extra_insert($place_id, 31, 1);
		}
		if (stripos($a, "american express") !== false) {
			//echo "<br />\n";
			do_extra_insert($place_id, 30, 1);
		}

	} else if ($attr_type == "rates") {
		$a = trim(strtolower($item[$attr_name]));
		if (preg_match('/30 min: \$([0-9]+)/i', $a, $matches)) {
			$val = $matches[1];
			//echo "30 min rate: {$val}<br />\n";
			do_extra_insert($place_id, 22, $val);
		}
		if (preg_match('/45 min: \$([0-9]+)/i', $a, $matches)) {
			$val = $matches[1];
			//echo "45 min rate: {$val}<br />\n";
			do_extra_insert($place_id, 23, $val);
		}
		if (preg_match('/60 min: \$([0-9]+)/i', $a, $matches)) {
			$val = $matches[1];
			//echo "60 min rate: {$val}<br />\n";
			do_extra_insert($place_id, 24, $val);
		}
		if (preg_match('/90 min: \$([0-9]+)/i', $a, $matches)) {
			$val = $matches[1];
			//echo "90 min rate: {$val}<br />\n";
			do_extra_insert($place_id, 56, $val);
		}
			
	} else if ($attr_type == "hours") {
		$a = trim(strtolower($item[$attr_name]));
		if (preg_match('#24/7#i', $a)) {
			//echo "always open<br />\n";
			do_extra_insert($place_id, 1, 1);
		}
		if (preg_match('/Daily ([0-9]+):([0-9]+)([ap])m - ([0-9]+):([0-9]+)([ap])m/i', $a, $matches)) {
			//echo "ei: hours: daily, matches=".print_r($matches, true)."<br />\n";
			array_shift($matches);
			list($from_hours, $from_minutes, $from_ampm, $to_hours, $to_minutes, $to_ampm) = $matches;
			$from = hm2int($from_hours, $from_minutes, $from_ampm, 0);
			$to = hm2int($to_hours, $to_minutes, $to_ampm, 1);
			//echo "daily, from:{$from_hours}:{$from_minutes}:{$from_ampm} ($from) to:{$to_hours}:{$to_minutes}:{$to_ampm} ({$to})<br />\n";
			do_extra_insert($place_id, 2, $from);do_extra_insert($place_id, 3, $to);
			do_extra_insert($place_id, 4, $from);do_extra_insert($place_id, 5, $to);
			do_extra_insert($place_id, 6, $from);do_extra_insert($place_id, 7, $to);
			do_extra_insert($place_id, 8, $from);do_extra_insert($place_id, 9, $to);
			do_extra_insert($place_id, 10, $from);do_extra_insert($place_id, 11, $to);
			do_extra_insert($place_id, 12, $from);do_extra_insert($place_id, 13, $to);
			do_extra_insert($place_id, 14, $from);do_extra_insert($place_id, 15, $to);
		}
		if (preg_match('/Mon ([0-9]+):([0-9]+)([ap])m - ([0-9]+):([0-9]+)([ap])m/i', $a, $matches)) {
			array_shift($matches);
			list($from_hours, $from_minutes, $from_ampm, $to_hours, $to_minutes, $to_ampm) = $matches;
			$from = hm2int($from_hours, $from_minutes, $from_ampm, 0);
			$to = hm2int($to_hours, $to_minutes, $to_ampm, 1);
			//echo "mon, from:{$from_hours}:{$from_minutes}:{$from_ampm} ($from) to:{$to_hours}:{$to_minutes}:{$to_ampm} ({$to})<br />\n";
			do_extra_insert($place_id, 2, $from);do_extra_insert($place_id, 3, $to);
		}
		if (preg_match('/Mon closed/i', $a)) {
			//echo "mon closed<br />\n";
			do_extra_insert($place_id, 2, -1);do_extra_insert($place_id, 3, -1);
		}
		if (preg_match('/Tue ([0-9]+):([0-9]+)([ap])m - ([0-9]+):([0-9]+)([ap])m/i', $a, $matches)) {
			array_shift($matches);
			list($from_hours, $from_minutes, $from_ampm, $to_hours, $to_minutes, $to_ampm) = $matches;
			$from = hm2int($from_hours, $from_minutes, $from_ampm, 0);
			$to = hm2int($to_hours, $to_minutes, $to_ampm, 1);
			//echo "tue, from:{$from_hours}:{$from_minutes}:{$from_ampm} ($from) to:{$to_hours}:{$to_minutes}:{$to_ampm} ({$to})<br />\n";
			do_extra_insert($place_id, 4, $from);do_extra_insert($place_id, 5, $to);
		}
		if (preg_match('/Tue closed/i', $a)) {
			//echo "tue closed<br />\n";
			do_extra_insert($place_id, 4, -1);do_extra_insert($place_id, 5, -1);
		}
		if (preg_match('/Wed ([0-9]+):([0-9]+)([ap])m - ([0-9]+):([0-9]+)([ap])m/i', $a, $matches)) {
			array_shift($matches);
			list($from_hours, $from_minutes, $from_ampm, $to_hours, $to_minutes, $to_ampm) = $matches;
			$from = hm2int($from_hours, $from_minutes, $from_ampm, 0);
			$to = hm2int($to_hours, $to_minutes, $to_ampm, 1);
			//echo "wed, from:{$from_hours}:{$from_minutes}:{$from_ampm} ($from) to:{$to_hours}:{$to_minutes}:{$to_ampm} ({$to})<br />\n";
			do_extra_insert($place_id, 6, $from);do_extra_insert($place_id, 7, $to);
		}
		if (preg_match('/Wed closed/i', $a)) {
			//echo "wed closed<br />\n";
			do_extra_insert($place_id, 6, -1);do_extra_insert($place_id, 7, -1);
		}
		if (preg_match('/Thu ([0-9]+):([0-9]+)([ap])m - ([0-9]+):([0-9]+)([ap])m/i', $a, $matches)) {
			array_shift($matches);
			list($from_hours, $from_minutes, $from_ampm, $to_hours, $to_minutes, $to_ampm) = $matches;
			$from = hm2int($from_hours, $from_minutes, $from_ampm, 0);
			$to = hm2int($to_hours, $to_minutes, $to_ampm, 1);
			//echo "thu, from:{$from_hours}:{$from_minutes}:{$from_ampm} ($from) to:{$to_hours}:{$to_minutes}:{$to_ampm} ({$to})<br />\n";
			do_extra_insert($place_id, 8, $from);do_extra_insert($place_id, 9, $to);
		}
		if (preg_match('/Thu closed/i', $a)) {
			//echo "thu closed<br />\n";
			do_extra_insert($place_id, 8, -1);do_extra_insert($place_id, 9, -1);
		}
		if (preg_match('/Fri ([0-9]+):([0-9]+)([ap])m - ([0-9]+):([0-9]+)([ap])m/i', $a, $matches)) {
			array_shift($matches);
			list($from_hours, $from_minutes, $from_ampm, $to_hours, $to_minutes, $to_ampm) = $matches;
			$from = hm2int($from_hours, $from_minutes, $from_ampm, 0);
			$to = hm2int($to_hours, $to_minutes, $to_ampm, 1);
			//echo "fri, from:{$from_hours}:{$from_minutes}:{$from_ampm} ($from) to:{$to_hours}:{$to_minutes}:{$to_ampm} ({$to})<br />\n";
			do_extra_insert($place_id, 10, $from);do_extra_insert($place_id, 11, $to);
		}
		if (preg_match('/Fri closed/i', $a)) {
			//echo "fri closed<br />\n";
			do_extra_insert($place_id, 10, -1);do_extra_insert($place_id, 11, -1);
		}
		if (preg_match('/Sat ([0-9]+):([0-9]+)([ap])m - ([0-9]+):([0-9]+)([ap])m/i', $a, $matches)) {
			array_shift($matches);
			list($from_hours, $from_minutes, $from_ampm, $to_hours, $to_minutes, $to_ampm) = $matches;
			$from = hm2int($from_hours, $from_minutes, $from_ampm, 0);
			$to = hm2int($to_hours, $to_minutes, $to_ampm, 1);
			//echo "sat, from:{$from_hours}:{$from_minutes}:{$from_ampm} ($from) to:{$to_hours}:{$to_minutes}:{$to_ampm} ({$to})<br />\n";
			do_extra_insert($place_id, 12, $from);do_extra_insert($place_id, 13, $to);
		}
		if (preg_match('/Sat closed/i', $a)) {
			//echo "sun closed<br />\n";
			do_extra_insert($place_id, 12, -1);do_extra_insert($place_id, 13, -1);
		}
		if (preg_match('/Sun ([0-9]+):([0-9]+)([ap])m - ([0-9]+):([0-9]+)([ap])m/i', $a, $matches)) {
			array_shift($matches);
			list($from_hours, $from_minutes, $from_ampm, $to_hours, $to_minutes, $to_ampm) = $matches;
			$from = hm2int($from_hours, $from_minutes, $from_ampm, 0);
			$to = hm2int($to_hours, $to_minutes, $to_ampm, 1);
			//echo "sun, from:{$from_hours}:{$from_minutes}:{$from_ampm} ($from) to:{$to_hours}:{$to_minutes}:{$to_ampm} ({$to})<br />\n";
			do_extra_insert($place_id, 14, $from);do_extra_insert($place_id, 15, $to);
		}
		if (preg_match('/Sun closed/i', $a)) {
			//echo "sun closed<br />\n";
			do_extra_insert($place_id, 14, -1);do_extra_insert($place_id, 15, -1);
		}

	} else if ($attr_type == "masseuse") {
		$arr = explode(",", strtolower($item[$attr_name]));
		$values = [];
		foreach ($arr as $m) {
			$m = trim($m);
			//echo "masseuse: '{$m}'<br />\n";
			if (array_key_exists($m, $masseuse_values)) {
				//echo "masseuse: -> ".$masseuse_values[$m]."<br />\n";
				$values[] = $masseuse_values[$m];
			} else {
				//echo "masseuse: not in our list<br />\n";
			}
		}
		//echo "masseuse: array = ".print_r($values, true)."<br />\n";
		if (!empty($values)) {
			do_extra_insert($place_id, 179, implode(",", $values));
		}
	}

	return true;
}

function process() {
	global $db, $config_image_path, $mcache, $num_errors, $dupes, $to_import;

	set_time_limit(0);
	$report_file_name = "newly_added_".date("Ymd_His").".html";
	$report_file_path = _CMS_ABS_PATH."/../data/tmp/".$report_file_name;
	//$report_file_url = "http://adultsearch.com/tmp/{$report_file_name}";

	//checks
	if (!is_dir(UPL_DIR)) {
		echo "<span style=\"color: red;\">Tmp upload dir '".UPL_DIR."' does not exists !</span><br />";
		return false;
	}

	$live = intval($_REQUEST["live"]);

	$filepath = UPL_DIR."/"."places.csv";
	if ($live == 0) {
		//store csv file into temp directory
		$file = new Upload($_FILES['file']);
		if ($file->uploaded) {
			@unlink($filepath);
			$file->file_new_name_body = "places";
			$file->file_new_name_ext = "csv";
			$file->Process(UPL_DIR."/");
			if (!$file->processed) {
				echo "<span style=\"color: red;\">Error while copying uploaded csv file to directory '".UPL_DIR."' !</span><br />";
				return false;
			}
		}
	}
	
	//parse csv file
	$csv = new parseCSV();
	$csv->auto($filepath);
	$num_entries = count($csv->data);
	if ($num_entries > 0)
		echo "Parsed CSV file: <strong>{$num_entries}</strong> entries.<br />\n";
	else {
		echo "<span style=\"color: red;\">Error: CSV file does not have any entry.</span><br />\n";
		return;
	}

	if ($live == 0) {
		$place_types = array();
		$res = $db->q("SELECT place_type_id, name FROM place_type ORDER BY name ASC");
		while ($row = $db->r($res)) {
			$place_types[$row["place_type_id"]] = $row["name"];
		}
	
		//form for column assign
		echo "<h3>Place type assign</h3>\n";
		echo "<form>";
        echo "<input type=\"hidden\" name=\"action\" value=\"process\" />";
        echo "<input type=\"hidden\" name=\"live\" value=\"1\" />";
		echo "Place type: <select name=\"place_type_id\">";
		echo "<option value=\"\">-- Select --</option>\n";
		foreach ($place_types as $key => $val) {
			echo "<option value=\"".intval($key)."\">".htmlspecialchars($val)."</option>\n";
		}
		echo "</select>\n";
		echo "<h3>Column assign</h3>\n";
		echo "<table>\n";
		$row1 = array_shift($csv->data);
		$num_fields = count($row1);
		for ($i = 0; $i < $num_fields; $i++) {
			$guess = "";
			$title = array_shift($csv->titles);
			$val = array_shift($row1);
			$guess = guess_col_name($title);
			echo "<tr><td>{$i}</td><td>".htmlspecialchars($val)."</td><td>";
			echo_column_assign($i, $guess);
			echo "</td></tr>\n";
		}
		echo "</table>";
        echo "<input type=\"submit\" name=\"submit\" value=\"Yes\" />";
		echo "</form>\n";
		return;	
	}

	$place_type_id = intval($_REQUEST["place_type_id"]);
	if ($place_type_id == 0) {
		echo "ERROR: place file type not selected !<br />\n";
		return;
	}

	$col_types = array();
	$i = 0;
	while (1) {
		if (isset($_REQUEST["col_{$i}"])) {
			$col_types[$i] = preg_replace('/[^a-zA-Z\_]/', '', $_REQUEST["col_{$i}"]);
		} else {
			break;
		}
		$i++;
	}

	if ($live ==1 ) {
		echo "<h3>Import check</h3>\n";

		$data = get_data($csv, $col_types);

		foreach ($data as $item) {
			//mandatory fields
			if (!isset($item["city"])) {
				er("City not filled");
				continue;
			}
			if (!isset($item["name"])) {
				er("Name not filled");
				continue;
			}
			if (!isset($item["address"])) {
				er("Address not filled");
				continue;
			}
	
			$loc = get_loc($item["country"], $item["state"], $item["city"]);
			if (empty($loc))
				continue;

			$phone = preg_replace('/[^0-9\+]/', '', $item["phone"]);

			//check duplicate
			$loc_id = $loc["loc_id"];
			$loc_name = $loc["loc_name"];
			$state_id = intval($loc["state_id"]);
			$country_id = $loc["country_id"];

			$res = $db->q("
				SELECT p.place_id, p.name, p.loc_id, p.address1, p.zipcode, p.phone1, l.loc_name
				FROM place p
				INNER JOIN location_location l on l.loc_id = p.loc_id
				WHERE (p.name like ? AND p.loc_id = ?) OR REPLACE(REPLACE(REPLACE(REPLACE(p.phone1,'(',''),')',''),'-',''),' ','') like ?
				", 
				["%".$item["name"]."%", $loc_id, $phone]
				);
			if ($db->numrows($res)) {
				//we consider all these as dupes
				//echo "<span style=\"color: orange;\"><strong>Dupe</strong>: {$item["name"]},{$loc_name},{$phone}</span> -> not importing.<br />\n";
				$dupes++;
				continue;
				/*
				$row = $db->r($res);
				if ($row["phone1"] == $phone) {
					//this is dupe for sure
					echo "<span style=\"color: orange;\"><strong>Dupe</strong>: {$item["name"]},{$loc_name},{$phone}</span> -> not importing.<br />\n";
					$dupes++;
					continue;
				}
				echo "<table class=\"comp\"><tr><td>&nbsp;</td><td>name</td><td>address</td><td>location</td><td>loc_id</td><td>phone</td></tr>\n";
				echo "<tr style=\"color: red;\"><td>Warning! Possible duplicity</td><td>{$item["name"]}</td><td>{$item["address"]}</td><td>{$loc_name}</td><td>{$loc_id}</td><td>{$phone}</td></tr>\n";
				do {
					echo "<tr><td>{$row["place_id"]}</td><td>{$row["name"]}</td><td>{$row["address1"]}</td><td>{$row["loc_name"]}</td><td>{$row["loc_id"]}</td><td>{$row["phone1"]}</td></tr>\n";
				} while ($row = $db->r($res));
				echo "<br />\n";
				*/
			} else {
				//echo "<span style=\"color: green;\"><strong>Check ok</strong>: {$item["name"]},{$loc_name},{$phone}</span><br />\n";
			}

			$to_import++;
		}
	
		if ($num_errors == 0) {
			echo "{$to_import} places to import, {$dupes} dupes, 0 errors, do you want to import these places ?<br />\n";
			echo "<form>";
			echo "<input type=\"hidden\" name=\"place_type_id\" value=\"{$place_type_id}\" />";
			foreach ($col_types as $key => $val) {
				echo "<input type=\"hidden\" name=\"col_{$key}\" value=\"{$val}\" />";
			}
			echo "<input type=\"hidden\" name=\"action\" value=\"process\" />";
			echo "<input type=\"hidden\" name=\"live\" value=\"2\" />";
			echo "<input type=\"submit\" name=\"submit\" value=\"Yes\" />";
			echo "</form>\n";
		} else {
			echo "{$to_import} places to import, {$dupes} dupes, {$num_errors} errors.<br />Fix the errors first.<br />\n";
		}

		return;
	}

	if ($live == 2) {

		$cnt_success = 0;	
		$cnt_failed = 0;	

		$handle = fopen($report_file_path, "w");
		if (!$handle)
			die("Cant open report file!");

		$data = get_data($csv, $col_types);

		foreach ($data as $item) {
			//mandatory fields
			if (!isset($item["city"])) {
				er("City not filled");
				continue;
			}
			if (!isset($item["name"])) {
				er("Name not filled");
				continue;
			}
			if (!isset($item["address"])) {
				er("Address not filled");
				continue;
			}
		
			$loc = get_loc($item["country"], $item["state"], $item["city"]);
			if (empty($loc))
				continue;

			$phone = preg_replace('/[^0-9\+]/', '', $item["phone"]);

			//check duplicate
			$loc_id = $loc["loc_id"];
			$loc_name = $loc["loc_name"];
			$state_id = intval($loc["state_id"]);
			$country_id = $loc["country_id"];

			$res = $db->q("
				SELECT p.place_id, p.name, p.loc_id, p.address1, p.zipcode, p.phone1, l.loc_name
				FROM place p
				INNER JOIN location_location l on l.loc_id = p.loc_id
				WHERE (p.name like ? AND p.loc_id = ?) OR REPLACE(REPLACE(REPLACE(REPLACE(p.phone1,'(',''),')',''),'-',''),' ','') like ?
				", 
				["%".$item["name"]."%", $loc_id, $phone]
				);
			if ($db->numrows($res)) {
				//we consider all these as dupes
				$dupes++;
				continue;
			}
			
			// all checks passed, going to import!!!

			$country_sub = $loc["country_sub"];
			//echo "loc_id = {$loc_id}, state_id={$state_id}, country_id={$country_id}<br />\n";

			//main place table insert
			$params = array($place_type_id, $item["name"], $loc_id, $item["address"], $item["zipcode"], $phone, $item["website"]);
			$res = $db->q("
				INSERT INTO place 
				(place_type_id, name, loc_id, address1, zipcode, phone1, website, edit) 
				VALUES 
				(?, ?, ?, ?, ?, ?, ?, 1)
				", 
				$params
				);
			$id = $db->insertid($res);
			if (!$id) {
				$cnt_failed++;
				echo "<span style=\"color: red;\">INSERT into place failed, params=".print_r($params, true)."</span><br />\n";
			} else {
				$cnt_success++;
				echo "Place <b>{$name}</b> (type={$place_type_id}) added, id={$id}<br />\n";
			}
			$place_link = dir::getPlaceLinkStatic($country_sub, $loc_url, $dir, $place_type_id, $id, $name);
			$edit_link = "https://adultsearch.com/worker/{$country_sub}?id={$id}&savelive=1";
			fputs($handle, "{$country} - {$loc_name} - {$name} - <a href=\"{$place_link}\" target=\"_blank\">[view]</a> <a href=\"{$edit_link}\" target=\"_blank\">[edit]</a><br />\n");

			//extra attributes insert
			//echo "item=".print_r($item, true)."<br />\n";
			extra_insert($item, "table_shower", "boolean3", 58, $id);
			extra_insert($item, "sauna", "boolean3", 60, $id);
			extra_insert($item, "jacuzzi", "boolean3", 61, $id);
			extra_insert($item, "truck_parking", "boolean", 170, $id);
			extra_insert($item, "cards", "cards", null, $id);
			extra_insert($item, "rates", "rates", null, $id);
			extra_insert($item, "hours", "hours", null, $id);
			extra_insert($item, "masseuse", "masseuse", null, $id);

			//test
			//break;
		}

		echo "<hr />Successfully inserted: {$cnt_success} places.<br />\n";
		echo "Failed to insert: {$cnt_failed} places.<br />\n";
		fclose($handle);
		echo "Report path: '{$report_file_path}'<br />\n";

	}

	return true;
}

?>
<h1>Place mass upload</h1>
<style type="text/css">
table.comp {
	border-collapse: collapse;
}
table.comp td {
	border: 1px solid #ccc;
	padding: 3px;
}
</style>
<?php

switch ($_REQUEST["action"]) {
	case 'process':
		$ret = process();
		if (!$ret)
			return;
		break;
}

?>
<form action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="action" value="process" />
<input type="hidden" name="live" value="0" />
Please select CSV file :<input type="file" name="file" />
<input type="submit" name="submit" value="Upload" />
</form>
