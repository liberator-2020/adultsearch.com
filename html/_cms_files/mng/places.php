<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) {
    $account->asklogin();
    return false;
}

if (!$account->isWorker()) {
	echo "You dont have privileges to access this page!";
	die;
}

function showPlaces($country_id, $country) {
global $page, $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols, $account, $db;

//configuration
$ipp = 30;
//$country = "thailand";
//list($country_id, $country) = explode("|", GetGetParam("country"));
$url_last_piece = "places";
$where_cols = array(
    "loc_name" => "City",
    "address1" => "Address1",
	"country_id" => "Country"
);

$filters = getFilters();
$params = [];
$where = getWhere($params, true);
$order = getOrder();
$limit = getLimit();

//query db
$sql = "select count(*) as total from place p inner join location_location l on p.loc_id = l.loc_id where l.country_id = '{$country_id}' $where";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "select p.* from place p inner join location_location l on p.loc_id = l.loc_id where l.country_id = '{$country_id}' $where $order $limit";
$res = $db->q($sql);

$pager = getPager($total);

//output
echo "<h2>Places</h2>\n";
echo $filter_form;
echo $pager;

if ($db->numrows($res) == 0) {
	echo "No results.";
	return;
}

echo "<table class=\"control\">";
echo "<thead><tr>
<th>".getOrderLink("Id", "id")."</th>
<th>".getOrderLink("Name", "name")."</th>
<th>".getOrderLink("Address1", "address1")."</th>
<th>".getOrderLink("Address2", "address2")."</th>
<th>".getOrderLink("City", "loc_name")."</th>
<th>".getOrderLink("Live", "edit")."</th>
</tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	$place_link = "<a href=\"".dir::getPlaceLinkAux($rox['loc_id'], $rox['place_type_id'], $rox['id'], $rox['name'])."\" target=\"_new\">{$rox['name']}</a>";
	$place_link .= " <a href=\"http://$country.adultsearch.com/worker/$country?id={$rox['id']}\" target=\"_new\">[edit]</a>";
	$edit = "<img src=\"/images/control/".(($rox['edit'] == 1) ? "tick.png" : "cross.png")."\" />";
	echo "<tr>
<td>{$rox['id']}</td>
<td>{$place_link}</td>
<td>{$rox['address1']}</td>
<td>{$rox['address2']}</td>
<td>{$rox['loc_name']}</td>
<td>{$edit}</td>
</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;

}


switch(GetGetParam("action")) {
    case 'places':
        list($country_id, $country) = explode("|", GetGetParam("country"));
		showPlaces($country_id, $country);
        break;
    default:
        break;
}

//get countries
$countries = array();
$location  = new location();
$location->createModules();
foreach($location->modules as $module) {
    if ($module['loclevel'] == 1)
    $countries[$module['country_id']."|".$module['table']] = $module['table'];
}

//filters form
$filter_form = "<form action=\"\" method=\"get\">";
$filter_form .= "<input type=\"hidden\" name=\"action\" value=\"places\" />";
$filter_form .= "Country: ";
$filter_form .= "<select name=\"country\" />";
foreach ($countries as $key => $val) $filter_form .= "<option value=\"{$key}\">{$val}</option>\n";
$filter_form .= "</select>";
foreach($where_cols as $colname => $title) {
	$filter_form .= getFilterField($colname, $title);
}
$filter_form .= "<input type=\"submit\" name=\"submit\" value=\"Filter\" />";
$filter_form .= "</form>\n";

echo $filter_form;
?>
