<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
define('UPL_DIR', '/www/virtual/adts/www.adultsearch.com/cron/promo/down');

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) { 
	$account->asklogin(); 
	return false; 
}

if (!$account->isadmin()) {
	echo "You dont have privileges to access this page!";
	die;
}

function promo_girls_upload() {
	global $db;

	$ts = intval($_REQUEST["ts"]);
	if ($ts != 0 && $ts != 1) {
		echo "Error: invalid value of ts: {$ts}!<br />";
		return false;
	}

	$res = $db->q("select max(id) as maxid from _ukranian_girls");
	$row = $db->r($res);
	$max_id = intval($row["maxid"]);
	if ($max_id == 0) {
		echo "Error: cant determine max id from _ukranina_girls!<br />";
		return false;
	}

	$id = $max_id + 1;
	echo "Id = {$id}, TS={$ts}<br />";

	$pic_names = array();
	for ($i = 1; $i <= 6; $i++) {
		$pic = new Upload($_FILES['pic'.$i]);
		if (!$pic->uploaded)
			continue;
		//echo "UPL: file_src_name={$pic->file_src_name}, file_src_pathname={$pic->file_src_pathname}, file_src_mime={$pic->file_src_mime}, file_src_size={$pic->file_src_size}, file_src_error={$pic->file_src_error}, file_is_image={$pic->file_is_image}<br />";
		$fname = "{$id}_{$i}";
		$pic->file_new_name_body = $fname;
		$pic_name = $fname.".".$pic->file_src_name_ext;
		$pic->process(UPL_DIR."/");
		if (!$pic->processed) {
			echo "Error: cant copy file '{$pic_name}' to dir ".UPL_DIR." !<br />";
			continue;
		}
		echo "Successfully processed filename {$pic_name}.<br />";
		$pic_names[] = $pic_name;
	}

	$cnt = count($pic_names);

	if ($cnt == 0) {
		echo "Error: no pictures successfully uploaded !<br />";
		return false;
	}
	if ($cnt < 4) {
		echo "Error: minimum number of pics for one girl is 4!<br />Uploaded {$cnt} pics:<br />";
		foreach ($pic_names as $pic_name) {
			echo UPL_DIR."/".$pic_name."<br />";
		}
		return false;
	}

	$url = "upl_".date("Ymd_His");
	$db->q("INSERT INTO _ukranian_girls (id, url, last, pic, ts, valid) VALUES ('{$id}', '{$url}', 0, '{$cnt}', '{$ts}', 1);");
	$aff = $db->affected();
	if ($aff != 1) {
		echo "Error: insert 1 row into _ukranian_girls (id={$id}, url={$url}, cnt={$cnt}, ts={$ts}, aff=$aff) !<br />";
		return false;
	}

	foreach ($pic_names as $pic_name) {
		$db->q("INSERT INTO _ukranian_pic (id, pic) VALUES ('{$id}', '{$pic_name}');");
		$aff = $db->affected();
		if ($aff != 1) {
			echo "Error: insert 1 row into _ukranian_girls (id={$id}, pic={$pic}, aff=$aff) !<br />";
			return false;
		}
	}

	echo "Done.<br />";
	
	foreach ($pic_names as $pic_name) {
		echo "<img src=\"/__downb/{$pic_name}\" /><br />";
	}

	return true;
}

if ($_REQUEST["action"] == "promo_girls_upload")
	promo_girls_upload();

?>

<h4>Upload new promo girl</h4>
<form action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="action" value="promo_girls_upload" />
TS?&nbsp;&nbsp;<input type="radio" name="ts" value="1" />Yes&nbsp;&nbsp;&nbsp;<input type="radio" name="ts" value="0" />No<br />
<input type="file" name="pic1" /><br />
<input type="file" name="pic2" /><br />
<input type="file" name="pic3" /><br />
<input type="file" name="pic4" /><br />
<input type="file" name="pic5" /><br />
<input type="file" name="pic6" /><br />
<input type="submit" name="submit" value="Submit" />
</form>
