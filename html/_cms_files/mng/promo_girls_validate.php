<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) { 
	$account->asklogin(); 
	return false; 
}

if (!$account->isadmin()) {
	echo "You dont have privileges to access this page!";
	die;
}

function form_submitted() {
	global $db;

	$id = intval($_REQUEST["id"]);
	if ($id == 0) {
		echo "Error: submittted ID is not valid.";
		return false;
	}
	if (isset($_REQUEST["girl_valid"]) && !isset($_REQUEST["girl_not_valid"])) {
		$valid = true;
	} else if (!isset($_REQUEST["girl_valid"]) && isset($_REQUEST["girl_not_valid"])) {
        $valid = false;
    } else {
		echo "Error: not set valid or not valid !";
		return false;
	}

	$res = $db->q("select * from `_ukranian_girls` WHERE id = '{$id}'");
	if ($db->numrows($res) != 1) {
		echo "Error: could not find girl with id '{$id}' in database !";
		return false;
	}
	$row = $db->r($res);
	if ($row["valid"] != NULL) {
		echo "Error: girl with id '{$id}' does not have valid = NULL !";
		return false;
	}
	
	$v = ($valid) ? "1": "0";
	$sql = "UPDATE `_ukranian_girls` set valid = '{$v}' WHERE id = '{$id}'";
	$db->q($sql);
	$aff = $db->affected();
	if ($aff != 1) {
		echo "Error: setting girl with id '{$id}' valid = '{$v}' (# affected rows = {$aff}) !";
		return false;
	}
	
	echo "Girl {$id} successfully set valid: {$v}.<br />";
	return true;
}

if (isset($_REQUEST["id"]))
	//form was submitted
	form_submitted();

//pick random not validated yet girl
$sql = "SELECT g.*, (SELECT GROUP_CONCAT(pic ORDER BY RAND()) 
			FROM _ukranian_pic p 
			WHERE g.id = p.id) pic 
		FROM `_ukranian_girls` g 
		WHERE g.pic > 0 AND g.valid IS NULL 
		ORDER BY g.last  DESC
		LIMIT 1";
$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "Error: no girl selected!<br />";
	return;
}
$row = $db->r($res);

$id = $row["id"];

echo "<br /><form action=\"\">";
echo "<input type=\"hidden\" name=\"id\" value=\"{$id}\" />";
echo "<input type=\"submit\" name=\"girl_valid\" value=\"This girl is valid\" />";
echo "<input type=\"submit\" name=\"girl_not_valid\" value=\"This girl is not valid\" />";
echo "</form>";

echo "Girl ID: {$id}<br /><br />";

//display pictures
$pictures = explode(',', $row["pic"]);
foreach ($pictures as $picture) {
	$file_path = $_SERVER["DOCUMENT_ROOT"]."/../cron/promo/down/{$picture}";
	if (!file_exists($file_path)) {
		echo "Error: file path '{$file_path}' does not exist !<br />";
		continue;
	}
	$image_data = base64_encode(file_get_contents($file_path));
	echo "<div style=\"float:left; padding: 3px;\"><img src=\"data: ".mime_content_type($file_path).";base64,{$image_data}\" /><br />{$picture}</div>";
}

?>
