<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("promocode_manage"))
	return;

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols, $classifieds;

//configuration
$ipp = 30;
$url_last_piece = "promocodes";
$where_cols = array(
	"id"		=> array("title" => "ID", "where" => "cp.id", "match" => "exact", "size" => "7"),
	"code"	  	=> array("title" => "Code", "where" => "cp.code"),
	"section"	=> array(
		"title" => "Section",
		"where" => "cp.section",
		"match" => "exact",
		"type" => "select",
		"options" => array(
			"" => "-",
			"classifieds" => "Classifieds",
			"businessowner" => "Business Owner")
		),
	"left"		=> array("title" => "Left", "where" => "cp.left", "match" => "exact", "size" => "7"),
);

function usage() {
	global $db, $account;
	$params = array();

	$id = intval($_REQUEST["action_id"]);
    if (!$id)
        return actionError("Error: No promocode id# specified!");

	$res = $db->q("SELECT COUNT(cl.loc_id) as loc_cnt, COUNT(distinct c.id) as cnt, c.done
					FROM classifieds c 
					INNER JOIN classifieds_loc cl on cl.post_id = c.id
					WHERE c.promo = ?
					GROUP BY c.done", 
					array($id)
				);

	echo "<h1>Promocode #{$id} usage</h1>";
	echo "<table class=\"control\">";
	echo "<tr><th>Done</th><th>Ad count</th><th>Location Count</th></tr>";
	while ($row = $db->r($res)) {
		echo "<tr><td>{$row["done"]}</td><td>{$row["cnt"]}</td><td>{$row["loc_cnt"]}</td></tr>";
	}
	echo "</table>";
	echo "<br /><a href=\"javascript:history.go(-1);\">Go back</a>";

	return false;
}

function add() {
	global $db, $account;

	$params = array();

	if (isset($_REQUEST["submit"])) {
		//set params	
		$params = get_params();

		//check params
		if (empty($params["code"]))
			echo "Error: Please enter code!<br />\n";
		else if (empty($params["type"]))
			echo "Error: Please select type of discount !<br />\n";
		else if ($params["type"] == "discount" && intval($params["discount"]) < 1)
			echo "Error: Please enter amount of discount !<br />\n";
		else if ($params["type"] == "percentage" && intval($params["percentage"]) < 1)
			echo "Error: Please enter amount of percentage discount !<br />\n";
		else if ($params["type"] == "addup" && intval($params["addup"]) < 1)
			echo "Error: Please enter amount of addup !<br />\n";
		else if ($params["type"] == "fixedprice" && intval($params["fixedprice"]) < 1)
			echo "Error: Please enter fixed price !<br />\n";
		else if (intval($params["left"]) < 1)
			echo "Error: Please enter amount of promocode uses !<br />\n";
		else {

			//check if code does not exist yet
			$res = $db->q("SELECT * FROM classifieds_promocodes WHERE code = ?", array($params["code"]));
			if ($db->numrows($res) > 0) {
				echo "Error: Promocode '{$params["code"]}' already exist! Please change code.<br />\n";
			} else {
				$now = time();
				$created_by = $account->getId();
				$db->q("INSERT INTO classifieds_promocodes
						(code, section, `left`, max, 
						free, discount, percentage, addup, fixedprice, eccie_promo, tracking, 
						can_auto_renew, day, limit_per_account, loclimit, setassponsor, recurring, 
						citythumbnail, sidesponsor, reposts, 
						notify, notify_to, added, created, created_by)
						VALUES
						(?, ?, ?, ?, 
						?, ?, ?, ?, ?, ?, ?,
						?, ?, ?, ?, ?, ?, 
						?, ?, ?, 
						?, ?, NOW(), ?, ?)", array(
						$params["code"], $params["section"], $params["left"], $params["max"], 
						$params["free"], $params["discount"], $params["percentage"], $params["addup"], $params["fixedprice"], $params["eccie_promo"], $params["tracking"], 
						$params["can_auto_renew"], $params["day"], $params["limit_per_account"], $params["loclimit"], $params["setassponsor"], $params["recurring"],
						$params["citythumbnail"], $params["sidesponsor"], $params["reposts"],
						$params["notify"], $params["notify_to"], $now, $created_by
						));
				$aff = $db->affected();
				if ($aff == 1) {
					$id = $db->insertid;
					audit::log("PRO", "Add", $id, "Code: '{$params["code"]}', left: {$params["left"]}", $account->getId());
					return actionSuccess("You have successfully added promocode '{$params["code"]}.");
				} else {
					return actionError("Error while adding new promocode ! Please contact administrator.");
				}
			}
		}
	} else if (isset($_REQUEST["clone_id"])) {
		$id = intval($_REQUEST["clone_id"]);
		if (!$id)
			return actionError("Error: Wrong promocode id# specified!");

		$res = $db->q("SELECT * FROM classifieds_promocodes WHERE id = ?", array($id));
		if (!$db->numrows($res))
			return actionError("Error: Promocode id#{$id} not found!");
		$row = $db->r($res);
		$params = get_params_from_row($row);
	}

	pc_form("Add new promocode", $params, "Add");
	return false;
}

function edit() {
	global $db, $account;
	$params = array();

	$id = intval($_REQUEST["action_id"]);
    if (!$id)
        return actionError("Error: No promocode id# specified!");

	$res = $db->q("SELECT * FROM classifieds_promocodes WHERE id = ?", array($id));
	if (!$db->numrows($res))
        return actionError("Error: Promocode id#{$id} not found!");
	$row = $db->r($res);

	if (isset($_REQUEST["submit"])) {
		//set params	
		$params = get_params();

		//check params
		if (empty($params["code"]))
			echo "Error: Please enter code!<br />\n";
		else if (empty($params["type"]))
			echo "Error: Please select type of discount !<br />\n";
		else if ($params["type"] == "discount" && intval($params["discount"]) < 1)
			echo "Error: Please enter amount of discount !<br />\n";
		else if ($params["type"] == "percentage" && intval($params["percentage"]) < 1)
			echo "Error: Please enter amount of percentage discount !<br />\n";
		else if ($params["type"] == "addup" && intval($params["addup"]) < 1)
			echo "Error: Please enter amount of addup !<br />\n";
		else if ($params["type"] == "fixedprice" && intval($params["fixedprice"]) < 1)
			echo "Error: Please enter fixed price !<br />\n";
		else if (intval($params["left"]) < 1)
			echo "Error: Please enter amount of promocode uses !<br />\n";
		else {

			$db->q("UPDATE classifieds_promocodes
					SET code = ?, section = ?, `left` = ?, max = ?, 
						free = ?, discount = ?, percentage = ?, addup = ?, fixedprice = ?, eccie_promo = ?, tracking = ?, 
						can_auto_renew = ?, day = ?, limit_per_account = ?, loclimit = ?, setassponsor = ?, recurring = ?, 
						citythumbnail = ?, sidesponsor = ?, reposts = ?, 
						notify = ?, notify_to = ?
					WHERE id = ?
					LIMIT 1", array(
					$params["code"], $params["section"], $params["left"], $params["max"], 
					$params["free"], $params["discount"], $params["percentage"], $params["addup"], $params["fixedprice"], $params["eccie_promo"], $params["tracking"], 
					$params["can_auto_renew"], $params["day"], $params["limit_per_account"], $params["loclimit"], $params["setassponsor"], $params["recurring"],
					$params["citythumbnail"], $params["sidesponsor"], $params["reposts"],
					$params["notify"], $params["notify_to"], 
					$id
					));
			$aff = $db->affected();
			if ($aff == 1) {
				audit::log("PRO", "Edit", $id, "Left: {$params["left"]}", $account->getId());
				return actionSuccess("You have successfully updated promocode '{$params["code"]}.");
			} else {
				return actionError("Error while updating promocode ! Please contact administrator.");
			}
		}
	} else {
		$params = get_params_from_row($row);
	}

	pc_form("Edit promocode {$params["code"]}", $params, "Update");
	return false;
}

function get_params_from_row($row) {

	$type = $free_checked = $eccie_promo_checked = $discount_checked = $percentage_checked = $addup_checked = $fixedprice_checked = $tracking_checked = "";
	if ($row["free"]) {
		$type = "free";
		$free_checked = "checked";
	} else if ($row["eccie_promo"]) {
		$type = "eccie_promo";
		$eccie_promo_checked = "checked";
	} else if ($row["discount"]) {
		$type = "discount";
		$discount_checked = "checked";
	} else if ($row["percentage"]) {
		$type = "percentage";
		$percentage_checked = "checked";
	} else if ($row["addup"]) {
		$type = "addup";
		$addup_checked = "checked";
	} else if ($row["fixedprice"]) {
		$type = "fixedprice";
		$fixedprice_checked = "checked";
	} else if ($row["tracking"]) {
		$type = "tracking";
		$tracking_checked = "checked";
	}
	$params = array(
		"code" => $row["code"],
		"section" => $row["section"],
		"max" => $row["max"],
		"left" => $row["left"],
		"type" => $type,
		"free_checked" => $free_checked,
		"eccie_promo_checked" => $eccie_promo_checked,
		"discount_checked" => $discount_checked,
		"percentage_checked" => $percentage_checked,
		"addup_checked" => $addup_checked,
		"fixedprice_checked" => $fixedprice_checked,
		"tracking_checked" => $tracking_checked,
		"free" => $row["free"],
		"discount" => $row["discount"],
		"percentage" => $row["percentage"],
		"addup" => $row["addup"],
		"fixedprice" => $row["fixedprice"],
		"eccie_promo" => $row["eccie_promo"],
		"tracking" => $row["tracking"],
		"can_auto_renew" => $row["can_auto_renew"],
		"loclimit" => $row["loclimit"],
		"limit_per_account" => $row["limit_per_account"],
		"day" => $row["day"],
		"setassponsor" => $row["setassponsor"],
		"citythumbnail" => $row["citythumbnail"],
		"sidesponsor" => $row["sidesponsor"],
		"reposts" => $row["reposts"],
		"recurring" => $row["recurring"],
		"notify" => $row["notify"],
		"notify_to" => $row["notify_to"],
		);
	return $params;
}

function get_params() {
	$params = array();

	if (isset($_REQUEST["action_id"]))
		$params["action_id"] = intval($_REQUEST["action_id"]);

	if (isset($_REQUEST["u_code"]))
		$params["code"] = $_REQUEST["u_code"];
	if (isset($_REQUEST["u_section"]))
		$params["section"] = $_REQUEST["u_section"];
	if (isset($_REQUEST["u_max"]))
		$params["max"] = $_REQUEST["u_max"];
	if (isset($_REQUEST["u_left"]))
		$params["left"] = intval($_REQUEST["u_left"]);
	$params["free"] = $params["discount"] = $params["percentage"] = $params["addup"] = $params["fixedprice"] = $params["eccie_promo"] = $params["tracking"] = 0;
	$params["type"] = "";
	$params["free_checked"] = $params["discount_checked"] = $params["percentage_checked"] = $params["addup_checked"] = $params["fixedprice_checked"] = $params["eccie_promo_checked"] = $params["tracking_checked"] = "";
	$params["notify"] = 0;
	$params["notify_to"] = "";
	switch ($_REQUEST["u_type"]) {
		case 'free':
			$params["type"] = "free";
			$params["free_checked"] = "checked";
			$params["free"] = 1;
			break;
		case 'discount':
			$params["type"] = "discount";
			$params["discount_checked"] = "checked";
			$params["discount"] = intval($_REQUEST["u_discount"]);
			break;
		case 'percentage':
			$params["type"] = "percentage";
			$params["percentage_checked"] = "checked";
			$params["percentage"] = intval($_REQUEST["u_percentage"]);
			break;
		case 'addup':
			$params["type"] = "addup";
			$params["addup_checked"] = "checked";
			$params["addup"] = intval($_REQUEST["u_addup"]);
			break;
		case 'fixedprice':
			$params["type"] = "fixedprice";
			$params["fixedprice_checked"] = "checked";
			$params["fixedprice"] = intval($_REQUEST["u_fixedprice"]);
			break;
		case 'eccie_promo':
			$params["type"] = "eccie_promo";
			$params["eccie_promo_checked"] = "checked";
			$params["eccie_promo"] = 1;
			break;
		case 'tracking':
			$params["type"] = "tracking";
			$params["tracking_checked"] = "checked";
			$params["tracking"] = 1;
			break;
	}
	if (isset($_REQUEST["u_can_auto_renew"]))
		$params["can_auto_renew"] = 1;
	else
		$params["can_auto_renew"] = 0;
	if (isset($_REQUEST["u_loclimit"]))
		$params["loclimit"] = intval($_REQUEST["u_loclimit"]);
	if (isset($_REQUEST["u_limit_per_account"]))
		$params["limit_per_account"] = 1;
	else
		$params["limit_per_account"] = 0;
	if (isset($_REQUEST["u_day"]))
		$params["day"] = intval($_REQUEST["u_day"]);
	if (isset($_REQUEST["u_setassponsor"]))
		$params["setassponsor"] = 1;
	else
		$params["setassponsor"] = 0;
	if (isset($_REQUEST["u_citythumbnail"]))
		$params["citythumbnail"] = 1;
	else
		$params["citythumbnail"] = 0;
	if (isset($_REQUEST["u_sidesponsor"]))
		$params["sidesponsor"] = 1;
	else
		$params["sidesponsor"] = 0;
	if (isset($_REQUEST["u_reposts"]))
		$params["reposts"] = intval($_REQUEST["u_reposts"]);
	if (isset($_REQUEST["u_recurring"]))
		$params["recurring"] = 1;
	else
		$params["recurring"] = 0;
	if (isset($_REQUEST["u_notify"])) {
		$params["notify"] = 1;
		$params["notify_to"] = $_REQUEST["u_notify_to"];
	} else
		$params["notify"] = 0;

	return $params;
}

function pc_form($header, $params, $submit_label) {
	echo "<h2>{$header}</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Code (max 15.characters): </th><td><input type=\"text\" name=\"u_code\" size=\"15\" maxlength=\"15\" value=\"{$params["code"]}\"></td></tr>\n";
	echo "<tr><th>Section: </th><td><select name=\"u_section\">";
	$sections = array(
		"" => "Any purchase",
		"classifieds" => "Only Classifieds",
		"businessowner" => "Only Business Owner",
		);
	foreach ($sections as $val => $label) {
		$selected = "";
		if ($val == $params["section"])
			$selected = " selected=\"selected\"";
		echo "<option value=\"{$val}\"{$selected}>{$label}</option>\n";
	}
	echo "</select></td></tr>\n";
	echo "<tr><th>Max posting worth in \$: </th><td><input type=\"text\" name=\"u_max\" size=\"10\" value=\"{$params["max"]}\"><small>If left empty, user can use this promocode with any amount worth of posting.</small></td></tr>\n";
	echo "<tr><th>How many times can this promocode be used: </th><td><input type=\"text\" name=\"u_left\" size=\"10\" value=\"{$params["left"]}\"></td></tr>\n";
	echo "<tr><th>Type of discount:</th><td>

<input type=\"radio\" name=\"u_type\" value=\"free\" {$params["free_checked"]} />Free<br />

<input type=\"radio\" name=\"u_type\" value=\"discount\" {$params["discount_checked"]} />Fixed discount: 
<div id=\"t_discount\">&nbsp;\$&nbsp;<input type=\"text\" name=\"u_discount\" value=\"{$params["discount"]}\" size=\"5\"/></div><br />

<input type=\"radio\" name=\"u_type\" value=\"percentage\" {$params["percentage_checked"]} />Percentage discount: 
<div id=\"t_percentage\"><input type=\"text\" name=\"u_percentage\" value=\"{$params["percentage"]}\" size=\"5\"/>%</div><br />

<input type=\"radio\" name=\"u_type\" value=\"addup\" {$params["addup_checked"]} />Addup: 
<div id=\"t_addup\">&nbsp;\$&nbsp;<input type=\"text\" name=\"u_addup\" value=\"{$params["addup"]}\" size=\"5\"/></div><br />

<input type=\"radio\" name=\"u_type\" value=\"fixedprice\" {$params["fixedprice_checked"]} />Fixed price: 
<div id=\"t_fixedprice\">&nbsp;\$&nbsp;<input type=\"text\" name=\"u_fixedprice\" value=\"{$params["fixedprice"]}\" size=\"5\"/></div><br />

<input type=\"radio\" name=\"u_type\" value=\"eccie_promo\" {$params["eccie_promo_checked"]} />Eccie Promo Ad<br />

<input type=\"radio\" name=\"u_type\" value=\"tracking\" {$params["tracking_checked"]} />Only Tracking<br />
			</td></tr>\n";
	echo "<td colspan=\"2\"><hr /></td>\n";
	$can_auto_renew_checked = "";
	if ($params["can_auto_renew"])
		$can_auto_renew_checked = " checked=\"checked\" ";
	echo "<tr><th>Can use auto-repost feature: </th><td><input type=\"checkbox\" name=\"u_can_auto_renew\" {$can_auto_renew_checked} /></td></tr>\n";
	echo "<tr><th>Location limit: </th><td><input type=\"text\" name=\"u_loclimit\" size=\"10\" value=\"{$params["loclimit"]}\"><small>Promo code can be used only if number of ad locations is less or equal to this. 0 = no limit</small></td></tr>\n";
	$limit_per_account_checked = "";
	if ($params["limit_per_account"])
		$limit_per_account_checked = " checked=\"checked\" ";
	echo "<tr><th>Limit per account: </th><td><input type=\"checkbox\" name=\"u_limit_per_account\" {$limit_per_account_checked} /><small>If checked, this promo code can be used only once per account</small></td></tr>\n";
	echo "<tr><th>Max Day to be live: </th><td><input type=\"text\" name=\"u_day\" size=\"10\" value=\"{$params["day"]}\" /></td></tr>\n";
	$setassponsor_checked = "";
	if ($params["setassponsor"])
		$setassponsor_checked = " checked=\"checked\" ";
	echo "<tr><th>Set as sponsor ad: </th><td><input type=\"checkbox\" name=\"u_setassponsor\" {$setassponsor_checked} /></td></tr>\n";
	$citythumbnail_checked = "";
	if ($params["citythumbnail"])
		$citythumbnail_checked = " checked=\"checked\" ";
	echo "<tr><th>Add city thumbnail: </th><td><input type=\"checkbox\" name=\"u_citythumbnail\" {$citythumbnail_checked} /></td></tr>\n";
	$sidesponsor_checked = "";
	if ($params["sidesponsor"])
		$sidesponsor_checked = " checked=\"checked\" ";
	echo "<tr><th>Add side sponsor: </th><td><input type=\"checkbox\" name=\"u_sidesponsor\" {$sidesponsor_checked} /></td></tr>\n";
	echo "<tr><th>Reposts: </th><td><input type=\"text\" size=\"10\" name=\"u_reposts\" value=\"{$params["reposts"]}\" /><small>Number of free reposts added to the ad</small></td></tr>\n";
	$recurring_checked = "";
	if ($params["recurring"])
		$recurring_checked = " checked=\"checked\" ";
	echo "<tr><th>Set as recurring ? </th><td><input type=\"checkbox\" name=\"u_recurring\" {$recurring_checked} /></td></tr>\n";
	echo "<td colspan=\"2\"><hr /></td>\n";
	$notify_checked = "";
	if ($params["notify"])
		$notify_checked = " checked=\"checked\" ";
	echo "<tr><th>Email notification: </th><td><input type=\"checkbox\" name=\"u_notify\" {$notify_checked} /><div id=\"t_notify\" style=\"display: none;\">&nbsp;Email address:&nbsp;<input type=\"text\" name=\"u_notify_to\" value=\"{$params["notify_to"]}\" size=\"30\"/></div></td></tr>\n";

	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"{$submit_label}\" /></td></tr>\n";
	echo "</table>";

echo "<script type=\"text/javascript\">
function type_changed(obj) {
	var choice = $(obj).val();
	$('#t_discount').hide();
	$('#t_percentage').hide();
	$('#t_addup').hide();
	$('#t_fixedprice').hide();
	if (choice == 'discount')
		$('#t_discount').show();
	else if (choice == 'percentage')
		$('#t_percentage').show();
	else if (choice == 'addup')
		$('#t_addup').show();
	else if (choice == 'fixedprice')
		$('#t_fixedprice').show();
}
function notify_changed() {
	if ($('input[name=u_notify]').attr('checked') == 'checked')
		$('#t_notify').show();
	else
		$('#t_notify').hide();
}
$(document).ready(function() {
	$('input[name=u_type]').change(function() {
		type_changed(this);
	});
	$('input[name=u_notify]').change(function() {
		notify_changed();
	});
	$('#t_discount').hide();
	$('#t_percentage').hide();
	$('#t_addup').hide();
	$('#t_fixedprice').hide();
	$('input[name=u_type]:checked').each(function(i,obj) {type_changed(obj);});
	notify_changed();
});
</script>";

	echo getFilterFormFields();
	echo "</form>";
}

function delete() {
	global $account, $db;

	$live = ($_REQUEST["live"] == 1) ? true : false;
	$id = intval($_REQUEST["action_id"]);
	if (!$id)
		return actionError("Error: No promocode id# specified!");

	$res = $db->q("SELECT code, deleted FROM classifieds_promocodes WHERE id = ?", array($id));
	if (!$db->numrows($res))
		return actionError("Error: Can't find promocode id#{$id} !");

	$row = $db->r($res);
	$code = $row["code"];
	if ($row["deleted"])
		return actionWarning("Warning: Can't delete promocode '{$code}' (id#{$id}) - it is already deleted.");

	if ($live) {
		//delete
		$db->q("UPDATE classifieds_promocodes SET deleted = ?, deleted_by = ? WHERE id = ? LIMIT 1", array(time(), $account->getId(), $id));
		$aff = $db->affected();
		if ($aff == 1) {
			audit::log("PRO", "Delete", $id, "", $account->getId());
			return actionSuccess("You have successfully deleted promocode '{$code}' (id#{$id}).");
		} else {
			return actionError("Error while deleting promocode '{$code}' (id#{$id}) ! Please contact administrator.");
		}
	}

	//show confirmation form
	echo "<h2>Delete promocode #{$id} - {$code}</h2>";
    echo "<form method=\"post\" action=\"\">";
	echo "Are you sure you want to delete promocode '{$code}' (id#{$id}) ?<br />\n";
    echo "<input type=\"submit\" name=\"submit\" value=\"Delete\" />\n";
    echo "</table>";
    echo "<input type=\"hidden\" name=\"action_id\" value=\"{$id}\" />";
    echo "<input type=\"hidden\" name=\"live\" value=\"1\" />";
    echo getFilterFormFields();
    echo "</form>";
    return false;
}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'usage':
		if (!permission::has("promocode_manage"))
			die;
		$ret = usage();
		if (!$ret)
			return;
		break;
	case 'add':
		if (!permission::has("promocode_manage"))
			die;
		$ret = add();
		if (!$ret)
			return;
		break;
	case 'edit':
		if (!permission::has("promocode_manage"))
			die;
		$ret = edit();
		if (!$ret)
			return;
		break;
	case 'delete':
		if (!permission::has("promocode_manage"))
			die;
		$ret = delete();
		if (!$ret)
			return;
		break;
}

$where = getWhere();
$having = getHaving();
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
	$order = "ORDER BY cp.id DESC";

//query db
$sql = "SELECT count(*) as total 
		FROM (
			SELECT cp.id
			FROM classifieds_promocodes cp
			$where
			GROUP BY cp.id
			$having
			) a
		";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];


$sql = "SELECT cp.id, cp.code, cp.section, cp.subcat
				, cp.left, cp.can_auto_renew, cp.max, cp.free, cp.discount, cp.percentage, cp.addup, cp.fixedprice, cp.tracking, cp.day, cp.limit_per_account, cp.loclimit
				, cp.notify, cp.notify_to, cp.setassponsor, cp.citythumbnail, cp.sidesponsor, cp.reposts, cp.recurring, cp.added, cp.eccie_promo 
				, cp.deleted, cp.deleted_by
		FROM classifieds_promocodes cp
		$where
		GROUP BY cp.id
		$having
		$order 
		$limit";

$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No promocodes.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Promocodes</h2>\n";

displayFilterForm();
echo "<a href=\"?action=add\">add</a><br />";
echo $pager."<br />";

echo "<form method=\"post\">";
echo getFilterFormFields();

echo "<table class=\"control\">";
echo "<thead><tr>
<th>".getOrderLink("Id", "cp.id")."</th>
<th>".getOrderLink("Code", "cp.code")."</th>
<th>".getOrderLink("Section", "cp.section")."</th>
<th>".getOrderLink("Left", "cp.left")."</th>
<th>".getOrderLink("Max", "cp.max")."</th>
<th>Type</th>
<th>".getOrderLink("Notify", "cp.notify_to")."</th>
<th>".getOrderLink("Added", "cp.added")."</th>
<th/>
</tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	$deleted = ($rox["deleted"]) ? true : false;

    if ($deleted)
        echo "<tr style=\"background-color: #FFAAAA;\">";
    else
        echo "<tr>";

	echo "<td>{$rox["id"]}</td>";
	echo "<td>{$rox["code"]}</td>";
	echo "<td>{$rox["section"]}</td>";
	echo "<td>{$rox["left"]}</td>";
	$max = "";
	if ($rox["max"])
		$max = "\${$rox["max"]}";
	echo "<td>{$max}</td>";

	$type = "";
	if ($rox["free"]) {
		$type = "Free";
	} else if ($rox["eccie_promo"]) {
		$type = "Eccie promo";
	} else if ($rox["discount"]) {
		$type = "Discount: \${$rox["discount"]}";
	} else if ($rox["percentage"]) {
		$type = "Percentage: {$rox["percentage"]}%";
	} else if ($rox["addup"]) {
		$type = "Addup: \${$rox["addup"]}";
	} else if ($rox["fixedprice"]) {
		$type = "Fixed price: \${$rox["fixedprice"]}";
	} else if ($rox["tracking"]) {
		$type = "Only tracking";
	}
	echo "<td>{$type}</td>";

	if ($rox["notify"] == 1)
		echo "<td>{$rox["notify_to"]}</td>";
	else
		echo "<td>-</td>";

	echo "<td>{$rox["added"]}</td>";

	$links = "";
	$links .= "<a href=\"?action=usage&action_id={$rox["id"]}\">usage</a>";
	if (!$deleted) {
		$links .= " &middot; <a href=\"".getActionLink("edit", $rox["id"])."\">edit</a>";
		$links .= " &middot; <a href=\"?action=add&clone_id={$rox["id"]}\">clone</a>";
		$links .= " &middot; <a href=\"".getActionLink("delete", $rox["id"])."\">delete</a>";
	} else {
		$links .= "<span style=\"color: red;\">DELETED</span>";
	}
	$links .= " &middot; <a href=\"/mng/audit?type=PRO&p1={$rox["id"]}\">history</a>";
	echo "<td>{$links}</td>";

	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo "</form>";
echo $pager;

?>
