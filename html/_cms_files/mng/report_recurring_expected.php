<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!account::ensure_admin())
	return;

$sql = "
select p.next_renewal_date, p.processor, count(*) as cnt, SUM(recurring_amount) as amount
from payment p
where p.subscription_status = 1 and p.next_renewal_date < DATE_ADD(NOW(), INTERVAL 30 DAY)
group by p.next_renewal_date, p.processor
order by 1 asc, 2 asc
";
$res = $db->q($sql);

echo "<h1>Expected recurring sales stats</h1>\n";
echo "Note: Normally only about 60% of rebills succeed.<br />\n";
echo "Planned rebills in next 30 days :<br />\n";
echo "<table class=\"control\">";
echo "<thead><tr>
<th>Date</th>
<th>Processor</th>
<th>#rebills</th>
<th>Amount</th>
</tr></thead>\n";
echo "<tbody>";
$total_cnt = $total_amount = 0;
while ($row = $db->r($res)) {

	$date = $row["next_renewal_date"];
	$processor = $row["processor"];
	$cnt = $row["cnt"];
	$amount = $row["amount"];

	echo "<tr>";
	echo "<td>{$date}</td>";
	echo "<td>{$processor}</td>";
	echo "<td style=\"text-align: right;\">".number_format($cnt, 0, "", ",")."</td>";
	echo "<td style=\"text-align: right;\">\$".number_format($amount, 2, ".", ",")."</td>";
	echo "</tr>\n";

	$total_cnt += $cnt;
	$total_amount += $amount;
}
echo "<tr>";
echo "<td colspan=\"2\"><strong>Total</strong></td>";
echo "<td style=\"text-align: right;\"><strong>".number_format($total_cnt, 0, "", ",")."</strong></td>";
echo "<td style=\"text-align: right;\"><strong>\$".number_format($total_amount, 2, ".", ",")."</strong></td>";
echo "</tr>";
echo "</tbody>";
echo "</table><br />";

?>
