<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

global $classifieds;
$classifieds = new classifieds();

//we have to be logged in 
if (!$account->isMember()) { 
	$account->asklogin(); 
	return false; 
}

if (!$account->isadmin()) {
	echo "You dont have privileges to access this page!";
	die;
}

global $url_last_piece;

//configuration
$url_last_piece = "revenue";

function getRevenueTitle($what) {
	switch ($what) {
		case "classifieds":
			return "Classified Ads";
		case "advertise":
		case "advertise2":
		case "728x90 banner l":
		case "CPC funding":
			return "Advertising";
		case "businessowner":
			return "Business Owner";
	}
	return "Other";
}

function revenue() {
	global $db, $url_last_piece;

	$system = new system;

	echo "<br />";

	$export = $_REQUEST["export"];
	if ($export != "" && $export != "csv")
		$export = "";

	if ($export == "csv") {
		$filepath = _CMS_ABS_PATH.'/UserFiles/excel/revenue_stats.csv';
		$fp = fopen($filepath, 'w');
	}

	$sql = "select year(ap.time) as year, month(ap.time) as month, ap.what, round(sum(ap.total),2) as total, count(*) 
			from account_purchase ap
			where time >= '2012-01-01'
			group by year(ap.time), month(ap.time), ap.what
			order by 1, 2, 3";
	
	$res = $db->q($sql);
	$stats = array();
	$ly = $lm = NULL;
	while ($row = $db->r($res)) {
		$y = $row["year"];
		$m = $row["month"];
		$w = $row["what"];
		$t = $row["total"];
		if ($ly != $y || $lm != $m) {
			if ($ly != NULL && $lm != NULL)
				$stats[]  =array("y" => $ly, "m" => $lm, "s" => $stat);
			$stat = array("Classified Ads" => 0, "Advertising" => 0, "Business Owner" => 0, "Other" => 0);
			$ly = $y;
			$lm = $m;
		}
		$stat[getRevenueTitle($w)] = $t;
	}
	$stats[]  =array("y" => $ly, "m" => $lm, "s" => $stat);
	//_darr($stats);

	if ($export == "csv") {
		$fields = "";
        $fields .= "Year,";
        $fields .= "Month,";
        $fields .= "Classified Ads, Advertising, Business Owner, Other\n";
    	fputs($fp, $fields);
	} else {
		echo "<style type=\"text/css\">table.stats {border-collapse: collapse; border-spacing: 0px;} table.stats td, table.stats th {padding: 2px; text-align: right; border: 1px solid #999;}</style>\n";
		echo "<table class=\"stats\"><tr>";
		echo "<th>Year</th>";
		echo "<th>Month</th>";
		echo "<th>Classified Ads</th><th>Advertising</th><th>Business Owner</th><th>Other</th></tr></thead><tbody>\n";
	}
		
	foreach ($stats as $s) {
		if ($export == "csv") {
			$fields = "";
           	$fields .= "{$s["y"]},";
           	$fields .= "{$s["m"]},";
			$fields .= number_format($s["s"]["Classified Ads"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["Advertising"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["Business Owner"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["Other"], 2, '.', '')."\n";
	       	fputs($fp, $fields);
		} else {
			echo "<tr>";
			echo "<td>{$s["y"]}</td>";
			echo "<td>{$s["m"]}</td>";
			echo "<td>".number_format($s["s"]["Classified Ads"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["Advertising"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["Business Owner"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["Other"], 2, '.', '')."</td></tr>\n";
		}
	}
		
	if ($export == "csv") {
		fclose($fp);
        $system->moved("http://adultsearch.com/UserFiles/excel/revenue_stats.csv");
	}
		
	echo "</tbody></table>\n";

	echo "<br /><a href=\"javascript: window.history.go(-1);\">Back</a>";
	echo "&nbsp;&middot;&nbsp;<a href=\"/mng/revenue?action=revenue&export=csv\">Save as CSV file</a><br /><br />\n";
	return false;
}

function getClassifiedsTitle($type) {
	global $classifieds;
	$title = $classifieds->getcatnamebytype($type);
	if ($title == "")
		$title = "Other";
	return $title;
}

function classifieds() {
	global $db, $url_last_piece;

	$system = new system;

	echo "<br />";

	$export = $_REQUEST["export"];
	if ($export != "" && $export != "csv")
		$export = "";

	if ($export == "csv") {
		$filepath = _CMS_ABS_PATH.'/UserFiles/excel/revenue_classifieds_stats.csv';
		$fp = fopen($filepath, 'w');
	}

	$sql = "select year(ap.time) as year, month(ap.time) as month, c.type, round(sum(ap.total), 2) as total, count(*) 
			from account_purchase ap
			left join classifieds c on ap.item_id = c.id
			where what = 'classifieds'
			and time >= '2012-01-01'
			group by year(ap.time), month(ap.time), c.type
			order by 1, 2, 3";

	$res = $db->q($sql);
	$stats = array();
	$ly = $lm = NULL;
	while ($row = $db->r($res)) {
		$y = $row["year"];
		$m = $row["month"];
		$w = $row["type"];
		$t = $row["total"];
		if ($ly != $y || $lm != $m) {
			if ($ly != NULL && $lm != NULL)
				$stats[]  =array("y" => $ly, "m" => $lm, "s" => $stat);
			$stat = array(
				"Female Escorts" => 0,
				"TS/TV Escorts" => 0,
				"Male For Female" => 0,
				"Male For Male Escorts" => 0,
				"Escort Agency" => 0,
				"Body Rubs" => 0,
				"Male For Male Body Rubs" => 0,
				"Strippers for Hire" => 0,
				"BDSM / Fetish" => 0,
				"Adult Help Wanted" => 0,
				"Other" => 0,
				);
			$ly = $y;
			$lm = $m;
		}
		$stat[getClassifiedsTitle($w)] = $t;
	}
	$stats[]  =array("y" => $ly, "m" => $lm, "s" => $stat);
	//_darr($stats);

	if ($export == "csv") {
		$fields = "";
        $fields .= "Year,";
        $fields .= "Month,";
        $fields .= "Female Escorts, TS/TV Escorts, Male For Female, Male For Male Escorts, Escort Agency, Body Rubs, Male For Male Body Rubs, Strippers for Hire, BDSM / Fetish, Adult Help Wanted, Other\n";
    	fputs($fp, $fields);
	} else {
		echo "<style type=\"text/css\">table.stats {border-collapse: collapse; border-spacing: 0px;} table.stats td, table.stats th {padding: 2px; text-align: right; border: 1px solid #999;}</style>\n";
		echo "<table class=\"stats\"><tr>";
		echo "<th>Year</th>";
		echo "<th>Month</th>";
		echo "<th>Female Escorts</th>";
		echo "<th>TS/TV Escorts</th>";
		echo "<th>Male For Female</th>";
		echo "<th>Male For Male Escorts</th>";
		echo "<th>Escort Agency</th>";
		echo "<th>Body Rubs</th>";
		echo "<th>Male For Male Body Rubs</th>";
		echo "<th>Strippers for Hire</th>";
		echo "<th>BDSM / Fetish</th>";
		echo "<th>Adult Help Wanted</th>";
		echo "<th>Other</th>";
		echo "</tr></thead><tbody>\n";
	}
		
	foreach ($stats as $s) {
		if ($export == "csv") {
			$fields = "";
           	$fields .= "{$s["y"]},";
           	$fields .= "{$s["m"]},";
			$fields .= number_format($s["s"]["Female Escorts"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["TS/TV Escorts"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["Male For Female"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["Male For Male Escorts"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["Escort Agency"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["Body Rubs"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["Male For Male Body Rubs"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["Strippers for Hire"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["BDSM / Fetish"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["Adult Help Wanted"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["Other"], 2, '.', '')."\n";
	       	fputs($fp, $fields);
		} else {
			echo "<tr>";
			echo "<td>{$s["y"]}</td>";
			echo "<td>{$s["m"]}</td>";
			echo "<td>".number_format($s["s"]["Female Escorts"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["TS/TV Escorts"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["Male For Female"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["Male For Male Escorts"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["Escort Agency"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["Body Rubs"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["Male For Male Body Rubs"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["Strippers for Hire"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["BDSM / Fetish"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["Adult Help Wanted"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["Other"], 2, '.', '')."</td></tr>\n";
		}
	}
		
	if ($export == "csv") {
		fclose($fp);
        $system->moved("http://adultsearch.com/UserFiles/excel/revenue_classifieds_stats.csv");
	}
		
	echo "</tbody></table>\n";

	echo "<br /><a href=\"javascript: window.history.go(-1);\">Back</a>";
	echo "&nbsp;&middot;&nbsp;<a href=\"/mng/revenue?action=classifieds&export=csv\">Save as CSV file</a><br /><br />\n";
	return false;
}

function getClassifiedsCountry($country) {
	if ($country == "United States" || $country == "Canada" || $country == "United Kingdom")
		return $country;
	if (!empty($country))
		return "International";
	return "Other";
}

function cl_country() {
	global $db, $url_last_piece;

	$system = new system;

	echo "<br />";

	$export = $_REQUEST["export"];
	if ($export != "" && $export != "csv")
		$export = "";

	if ($export == "csv") {
		$filepath = _CMS_ABS_PATH.'/UserFiles/excel/revenue_cl_country_stats.csv';
		$fp = fopen($filepath, 'w');
	}

	$sql = "select year(ap.time) as year, month(ap.time) as month, lc.loc_name, round(sum(ap.total), 2) as total, count(*) 
			from account_purchase ap
			left join classifieds c on ap.item_id = c.id
			left join location_location l on c.loc_id = l.loc_id
			left join location_location lc on lc.loc_id = l.country_id
			where what = 'classifieds'
			and time >= '2012-01-01'
			group by year(ap.time), month(ap.time), lc.loc_name
			order by 1, 2, 3";

	$res = $db->q($sql);
	$stats = array();
	$ly = $lm = NULL;
	while ($row = $db->r($res)) {
		$y = $row["year"];
		$m = $row["month"];
		$w = $row["loc_name"];
		$t = $row["total"];
		if ($ly != $y || $lm != $m) {
			if ($ly != NULL && $lm != NULL)
				$stats[]  =array("y" => $ly, "m" => $lm, "s" => $stat);
			$stat = array(
				"United States" => 0,
				"Canada" => 0,
				"United Kingdom" => 0,
				"International" => 0,
				"Other" => 0,
				);
			$ly = $y;
			$lm = $m;
		}
		$stat[getClassifiedsCountry($w)] = $t;
	}
	$stats[]  =array("y" => $ly, "m" => $lm, "s" => $stat);
	//_darr($stats);

	if ($export == "csv") {
		$fields = "";
        $fields .= "Year,";
        $fields .= "Month,";
        $fields .= "United States, Canada, United Kingdom, International, Other\n";
    	fputs($fp, $fields);
	} else {
		echo "<style type=\"text/css\">table.stats {border-collapse: collapse; border-spacing: 0px;} table.stats td, table.stats th {padding: 2px; text-align: right; border: 1px solid #999;}</style>\n";
		echo "<table class=\"stats\"><tr>";
		echo "<th>Year</th>";
		echo "<th>Month</th>";
		echo "<th>United States</th>";
		echo "<th>Canada</th>";
		echo "<th>United Kingdom</th>";
		echo "<th>International</th>";
		echo "<th>Other</th>";
		echo "</tr></thead><tbody>\n";
	}
		
	foreach ($stats as $s) {
		if ($export == "csv") {
			$fields = "";
           	$fields .= "{$s["y"]},";
           	$fields .= "{$s["m"]},";
			$fields .= number_format($s["s"]["United States"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["Canada"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["United Kingdom"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["International"], 2, '.', '');
			$fields .= ",".number_format($s["s"]["Other"], 2, '.', '')."\n";
	       	fputs($fp, $fields);
		} else {
			echo "<tr>";
			echo "<td>{$s["y"]}</td>";
			echo "<td>{$s["m"]}</td>";
			echo "<td>".number_format($s["s"]["United States"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["Canada"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["United Kingdom"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["International"], 2, '.', '')."</td>";
			echo "<td>".number_format($s["s"]["Other"], 2, '.', '')."</td></tr>\n";
		}
	}
		
	if ($export == "csv") {
		fclose($fp);
        $system->moved("http://adultsearch.com/UserFiles/excel/revenue_cl_country_stats.csv");
	}
		
	echo "</tbody></table>\n";

	echo "<br /><a href=\"javascript: window.history.go(-1);\">Back</a>";
	echo "&nbsp;&middot;&nbsp;<a href=\"/mng/revenue?action=cl_country&export=csv\">Save as CSV file</a><br /><br />\n";
	return false;
}


switch ($_REQUEST["action"]) {
	case 'revenue':
		$ret = revenue();
		if (!$ret)
			return;
		break;
	case 'classifieds':
		$ret = classifieds();
		if (!$ret)
			return;
		break;
	case 'cl_country':
		$ret = cl_country();
		if (!$ret)
			return;
		break;
}

echo "<a href=\"{$url_last_piece}?action=revenue\">Monthly revenue</a><br /><br />";
echo "<a href=\"{$url_last_piece}?action=classifieds\">Monthly revenue - classifieds by type</a><br /><br />";
echo "<a href=\"{$url_last_piece}?action=cl_country\">Monthly revenue - classifieds by country</a><br /><br />";
?>
