<?php

global $db, $smarty, $account, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!$account->isadmin()) {
	$account->asklogin();
    return false;
}

if( isset($_GET['out']) ) {
	$out = intval($_GET['out']);
	$db->q("update place_review set done = 1 where review_id = ?", array($out));
	die('1');
}

if (isset($_REQUEST["resendEmail"])) {
	$id = intval($_REQUEST["resendEmail"]);
	$review = review::findOneById($id);
	$review->notifyNewReview("X", "", $review->getAccountId(), "XusernameX", "XcommentX", array(ADMIN_EMAIL));
	die('1');
}

$res = $db->q("SELECT p.*
				FROM place_review p
				WHERE p.done = 0 AND p.provider_id = 0 and p.id != 0 and p.module in ('adultstore', 'eroticmassage', 'stripclub', 'place')");
while($row=$db->r($res)) {
	$edit = "";
	switch($row['module']) {
		case 'eroticmassage':
			$url = "/eroticmassage/review-add?id={$row['id']}&edit={$row['review_id']}";
			$edit = "/worker/emp?id={$row['id']}";
			break;
		case 'stripclub':
			$url = "/stripclubs/review-add?id={$row['id']}&edit={$row['review_id']}";
			$edit = "/worker/sc?id={$row['id']}";
			break;
		case 'adultstore':
			$url = "/adultstores/review-add?id={$row['id']}&edit={$row['review_id']}";
			$edit = "/worker/adultstore?id={$row['id']}";
			break;
		case 'place':
			//get place link
			$res2 = $db->q("SELECT place_id, loc_id, place_type_id, name FROM place WHERE place_id = ?", array($row["id"]));
			if ($db->numrows($res2) != 1) {
				//die("Cant find place id #{$row["id"]} in place table !");
				continue;
			}
			$row2 = $db->r($res2);
			$place_link = dir::getPlaceLink($row2, true);

			$url = dirname($place_link)."/review-add?id={$row['id']}&edit={$row['review_id']}";
//			$edit = "/worker/adultstore?id={$row['id']}";
			break;
		default:
			die($row['module']);
	}

	$review[] = array(
		"review_id"=>$row['review_id'],
		"id"=>$row['id'],
		"module"=>$row['module'],
		"comment"=>$row['comment'],
		"url"=>$url,
		"edit"=>$edit
	);
} 

$smarty->assign('review', $review);
$smarty->assign("admin", $account->isRealAdmin());
$smarty->display(_CMS_ABS_PATH."/templates/mng/mng_review.tpl");

?>
