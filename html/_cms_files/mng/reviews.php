<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("edit_all_places"))
	return;

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols, $classifieds;

//configuration
$ipp = 30;
$url_last_piece = "reviews";
$where_cols = array(
	"review_id" => "Review ID",
	"account_id" => "Account ID",
);

function delete() {
    global $db, $account;

    $action_id = intval($_REQUEST["action_id"]);
    if ($action_id == 0)
        return actionError("Invalid review id !");

    $review = review::findOneById($action_id);
    if (!$review)
        return actionError("Cant find review id#{$action_id} !");

	$ret = $review->remove(true);
    if ($ret)
        return actionSuccess("Successfully deleted review #id {$action_id}.");
    else
        return actionError("Error deleting review #id {$action_id}, please contact administrator !");
}

function send_admin_notify_to_me() {
    global $db, $account;

    $action_id = intval($_REQUEST["action_id"]);
    if ($action_id == 0)
        return actionError("Invalid review id !");

    $review = review::findOneById($action_id);
    if (!$review)
        return actionError("Cant find review id#{$action_id} !");

    $email = ADMIN_EMAIL;
//    $email = NULL;
	
	$review->notifyNewReview("N/A", "", $review->account_id, "N/A", "N/A", array($email));
    return actionSuccess("Done.");
}

switch ($_REQUEST["action"]) {
	case 'delete':
		$ret = delete();
		if (!$ret)
			return;
		break;
	case 'send_admin_notify_to_me':
		$ret = send_admin_notify_to_me();
		if (!$ret)
			return;
		break;
}

$filters = getFilters();
$where = getWhere();
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
    $order = "ORDER BY r.review_id DESC";

//query db
$sql = "SELECT count(*) as total 
		FROM place_review r
		$where";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT r.review_id, r.module, r.id, r.account_id, r.comment, r.reviewdate
		FROM place_review r
		$where
		$order 
		$limit";

$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No reviews.";
	return;
}

//pager
$pager = getPager($total);

//output
echo "<h2>Place reviews</h2>\n";

echo "<br />\n";
displayFilterForm();
/*
echo "<form>";
echo "<select name=\"action\"><option value=\"\">-</option>";
echo "</select>";
echo "<input type=\"submit\" name=\"submit\" value=\"Execute\" />";
*/
echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr>
<th class=\"check\"><input type=\"checkbox\" name=\"master\" value=\"\" /></th>
<th>".getOrderLink("Review Id", "review_id")."</th>
<th>Date</th>
<th>".getOrderLink("Module", "module")."</th>
<th>".getOrderLink("Item Id", "id")."</th>
<th>".getOrderLink("Account ID", "account_id")."</th>
<th>Comment</th>
<th/></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<tr>";
	echo "<td class=\"check\"><input type=\"checkbox\" name=\"id[]\" value=\"{$rox['id']}\" /></td>";
	echo "<td>{$rox['review_id']}</td>";
	echo "<td>{$rox["reviewdate"]}</td>";
	echo "<td>{$rox["module"]}</td>";
	echo "<td>{$rox["id"]}</td>";
	echo "<td><a href=\"/mng/accounts?account_id={$rox["account_id"]}\">{$rox["account_id"]}</a></td>";

	$comment = substr($rox["comment"], 0, 50)." ...";
	echo "<td>".htmlspecialchars($comment)."</td>";

	echo "<td>";
	echo "<a href=\"".getActionLink("delete", $rox['review_id'])."\">delete</a>";
	echo "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("send_admin_notify_to_me", $rox['review_id'])."\">resend_admin_notify_to_me</a>";
	echo "</td>";

	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;
echo "</form>";


?>
