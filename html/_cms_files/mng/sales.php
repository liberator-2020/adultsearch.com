<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("access_admin_sales"))
	return;

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols, $classifieds;

//configuration
$ipp = 30;
$url_last_piece = "sales";
$where_cols = array(
	"id"		 => array("title" => "ID", "where" => "t.id", "match" => "exact", "size" => "5"),
	"account_id" => array("title" => "Acc.ID", "where" => "a.account_id", "match" => "exact", "size" => "5"),
	"agency"	 => array(
		"title" => "Agency",
		"where" => array(
			"" => "",
			"a" => " a.agency = 1 ",
			"na" => " a.agency = 0 ",
			),
		"type" => "select",
		"options" => array(
			"" => "-",
			"a" => "Agency",
			"na" => "Not Agency",
			),
		),
	"email"	  => array("title" => "Email", "where" => "a.email"),
//	"password"   => array("title" => "Password", "where" => "a.password"),
	"cc_id"	  => array("title" => "CC.ID", "where" => "p.cc_id", "match" => "exact", "size" => "5"),
	"date"	   => array("title" => "Date", "where" => "t.stamp", "match" => "exact", "size" => "10"),
	"amount"	  => array("title" => "Amount", "where" => "t.amount", "size" => "5"),
	"item_id"	=> [
		"title" => "AD.Id", 
		"where" => "pi.classified_id", 
		"match" => "preg_replace", 
		"pattern" => "/[^0-9]/", 
		"replacement" => "", 
		"size" => "5"
		],
	"payment_id"	=> array("title" => "Invoice #Id", "where" => "p.id", "match" => "preg_replace", "pattern" => "/[^0-9]/", "size" => "5"),
	"transaction_id"	=> array("title" => "Txn.Id", "where" => "t.trans_id", "size" => "35"),
	"subscription_id"	=> array("title" => "Subscr.Id", "where" => "t.subscription_id", "match" => "exact", "size" => "10"),
	"invoice_num"	=> array("title" => "Invoice no.", "where" => "p.id", "match" => "preg_replace", "pattern" => "/[^0-9]/", "size" => "10"),
	"transaction_type"	 => array(
		"title" => "Transaction type",
		"where" => array(
			"" => "",
			"rebill" => " t.trans_id <> p.trans_id ",
			),
		"type" => "select",
		"options" => array(
			"" => "-",
			"rebill" => "Only rebills",
			),
		),
);


function cancel() {
	global $account, $db;

	$live = ($_REQUEST["live"] == 1) ? true : false;
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid transaction id !");
	$transaction_id = $action_id;

	$res = $db->q("
		SELECT t.trans_id, t.payment_id
			, p.trans_id as payment_trans_id, p.recurring_amount, p.subscription_id, p.subscription_status, p.account_id, p.processor, p.next_renewal_date
		FROM transaction t
		INNER JOIN payment p on p.id = t.payment_id
		WHERE t.id = ?", 
		[$transaction_id]
		);
	if ($db->numrows($res) != 1)
		return actionError("Cant find transaction #{$transaction_id} !");
	$row = $db->r($res);

	$payment_id = $row["payment_id"];
	$subscription_id = $row["subscription_id"];
	$subscription_status = $row["subscription_status"];
	if (!is_null($subscription_status) && !$subscription_id)
		$subscription_id = $payment_id;
	$amount = number_format($row["recurring_amount"], 2, ".", "");
	$next_renewal_date = $row["next_renewal_date"];
	
	if ($subscription_status != 1) {
		if ($subscription_status == 2)
			return actionWarning("Subscription #{$subscription_id} is already canceled.");
		else
			return actionError("Unknown status of subscription #{$subscription_id} ($subscription_status), please contact administrator.");
	}

	if ($live) {
		$reason = $_REQUEST["u_reason"];
		$error = null;
		$ret = payment::cancel($payment_id, $reason, $error, true);
		if ($ret === false)
			return actionError($error);
		audit::log("SUB", "Cancel", $subscription_id, "", $account->getId());
		return actionSuccess("Subscription #{$subscription_id} successfully canceled.");
	}
	
	echo "<h2>Cancel subscription #{$subscription_id} ?</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "Are you sure you want to cancel subscription #{$subscription_id} (\${$amount}, next renewal date: {$next_renewal_date}) ?<br />\n";
	echo "<table>";
	echo "<tr><th>Internal Reason:</th><td><select name=\"u_reason\"><option value=\"\">Please select</option><option value=\"spammer\">Spammer</option><option value=\"client\">Client cancel</option><option value=\"\">Other/No reason</option></select></td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Cancel\" /></td></tr>\n";
	echo "</table>";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$transaction_id}\" />";
	echo "<input type=\"hidden\" name=\"live\" value=\"1\" />";
	echo getFilterFormFields();
	echo "</form>";
	return true;
}

function refund() {
	global $account, $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid transaction id !");
	$transaction_id = $action_id;

	$res = $db->q("
		SELECT t.trans_id, t.amount, t.payment_id
			, p.trans_id as payment_trans_id, p.subscription_id, p.subscription_status, p.account_id, p.cc_id, p.processor
			, cc.cc, cc.type as cc_type
		FROM transaction t
		INNER JOIN payment p on p.id = t.payment_id
		LEFT JOIN account_cc cc on cc.cc_id = p.cc_id
		WHERE t.id = ?", 
		[$transaction_id]
		);
	if ($db->numrows($res) != 1)
		return actionError("Cant find transaction #{$transaction_id} !");
	$row = $db->r($res);

	$trans_id = $row["trans_id"];
	$amount = $row["amount"];
	$payment_id = $row["payment_id"];
	$payment_trans_id = $row["payment_trans_id"];
	$subscription_id = $row["subscription_id"];
	if (!is_null($subscription_status) && !$subscription_id)
		$subscription_id = $payment_id;
	$subscription_status = $row["subscription_status"];
	$account_id = $row["account_id"];
	$cc_id = $row["cc_id"];
	$cc = $row["cc_type"]." X".substr($row["cc"], -4);

	if (!isset($_REQUEST["submit"])) {
		echo "<h2>Refund transaction {$trans_id} of amount \${$amount} ?</h2>";
		echo "<form method=\"post\" action=\"\">";
		echo "<table>";
		if ($trans_id == $payment_trans_id && !is_null($subscription_status))  {
			echo "<tr><td colspan=\"2\">This is initial transaction of recurring payment #{$payment_id}.</td></tr>";
			if ($subscription_status == 1)
				echo "<tr><td/><td><input type=\"checkbox\" name=\"u_cancel_subscription\" value=\"1\" checked=\"checked\" onclick=\"return false\"> Cancel subscription too</td></tr>";
			else
				echo "<tr><td/><td>Subscription is already cancelled</td></tr>";
		} else if ($trans_id == $payment_trans_id) {
			echo "<tr><td colspan=\"2\">This is one time (non-recurring) purchase.</td></tr>";
		} else if ($trans_id != $payment_trans_id && !is_null($subscription_status)) {
			echo "<tr><td colspan=\"2\">This is one of the rebills.</td></tr>";
			if ($subscription_status == 1)
				echo "<tr><td/><td><input type=\"checkbox\" name=\"u_cancel_subscription\" value=\"1\" checked=\"checked\" onclick=\"return false\"> Cancel subscription too</td></tr>";
			else
				echo "<tr><td/><td>Subscription is already cancelled</td></tr>";
		} else {
			die("Unknown state");
		}
		echo "<tr><th>Internal Reason:</th><td><select name=\"u_refund_reason\"><option value=\"\">Please select</option><option value=\"spammer\">Spammer</option><option value=\"client changed mind\">Client changed her mind</option><option value=\"\">Other/No reason</option></select></td></tr>\n";
		echo "<tr><th>Refund amount:</th><td><input type=\"text\" name=\"u_amount\" value=\"{$amount}\" size=\"8\" /></td></tr>\n";
		echo "<tr><th>Description:</th><td><input type=\"text\" name=\"u_description\" size=\"60\" value=\"Refunding cardholder.\" /></td></tr>\n";
		echo "<tr><th>Ban:</th><td><input type=\"checkbox\" name=\"u_ban\" value=\"1\" /> ";
		if ($cc_id)
			echo "Ban credit card {$cc}, ";
		echo "Ban account #{$account_id} and delete all his ads ?</td></tr>\n";
		echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
		echo "</table>";
		echo "<input type=\"hidden\" name=\"action_id\" value=\"{$transaction_id}\" />";
		echo getFilterFormFields();
		echo "</form>";
		return true;
	}

	//were going live
	$amount = number_format(floatval($_REQUEST["u_amount"]), 2);
	$refund_reason = $_REQUEST["u_refund_reason"];
	$description = $_REQUEST["u_description"];
	$cancel_subscription = (boolean)($_REQUEST["u_cancel_subscription"]);
	$ban = (boolean)($_REQUEST["u_ban"]);

	//mark purchase as refunded
	$error = null;
	$ret = payment::refund($payment_id, $transaction_id, $cancel_subscription, $amount, $refund_reason, $error);

	if ($ret === false)
		return actionError("Refund failed: '{$error}'. Please contact administrator.");
	$msg = $ret;

	//audit refund
	audit::log("REF", "Refund", $transaction_id, $msg, $account->getId());

	if ($ban) {
		if ($cc_id) {
			//ban CC
			$cre = creditcard::findOneById($cc_id);
			if (!$cre)
				return actionWarning("Refund succeeded: {$msg}. But banning CC #{$cc_id} failed!");
			$cc_reason = "";
			if ($refund_reason == "spammer")
				$cc_reason = "spammer";
			$ret = $cre->ban($cc_reason);
			if ($ret)
				$msg .= " Credit card #{$cc_id} banned.";
		}
		//ban account
		$acc = account::findOneById($account_id);
		if (!$acc)
			return actionWarning("Refund succeeded: {$msg}. CC banned. But banning account #{$account_id} failed!");
		$ret = $acc->ban(($refund_reason == "spammer") ? $refund_reason : null);
		if ($ret)
			$msg .= " Account #{$account_id} banned.";

		//delete all ads by account
		$res = $db->q("SELECT c.* FROM classifieds c WHERE c.account_id = ? AND c.deleted IS NULL", [$account_id]);
		$cl = 0;
		while ($row = $db->r($res)) {
			//fetch clad
			$clad = clad::withRow($row);
			if (!$clad)
				continue;
			//remove ad
			if ($clad->remove())
				$cl++;
		}
		$msg .= " {$cl} ads of account #{$account_id} deleted.";
	}

	return actionSuccess($msg);
}

function anet_chargeback() {
	global $account, $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid transaction id !");
	$transaction_id = $action_id;

	$res = $db->q("
		SELECT t.trans_id as purchase_trans_id
			, ap.id as purchase_id, ap.total as purchase_total
			, p.*
			, cc.cc, cc.type as cc_type
		FROM transaction t 
		INNER JOIN account_purchase ap on ap.transaction_id = t.trans_id
		INNER JOIN payment p on p.id = ap.payment_id
		INNER JOIN account_cc cc on cc.cc_id = p.cc_id
		WHERE t.id = ?", 
		[$transaction_id]
		);
	if ($db->numrows($res) != 1)
		return actionError("Cant find transaction with id '$transaction_id' !");
	$row = $db->r($res);

	$purchase_id = $row["purchase_id"];
	$purchase_trans_id = $row["purchase_trans_id"];
	$purchase_total = $row["purchase_total"];
	
	$payment_trans_id = $row["trans_id"];
	$payment_subscription_id = $row["subscription_id"];
	$payment_subscription_status = $row["subscription_status"];
	$account_id = $row["account_id"];
	$cc_id = $row["cc_id"];
	$cc = $row["cc_type"]." X".substr($row["cc"], -4);

	if (!isset($_REQUEST["submit"])) {
		echo "<h2>Mark transaction {$purchase_trans_id} of amount \${$purchase_total} as chargebacked ?</h2>";
		echo "<form method=\"post\" action=\"\">";
		echo "<table>";
		if ($payment_subscription_id && $payment_subscription_status == 1) {
			echo "<tr><td/><td><input type=\"checkbox\" name=\"u_cancel_subscription\" value=\"1\" checked=\"checked\"> Cancel subscription too</td></tr>";
		}
		echo "<tr><th>Amount:</th><td><input type=\"text\" name=\"u_amount\" value=\"{$purchase_total}\" size=\"8\" /></td></tr>\n";
		echo "<tr><th>Ban CC:</th><td><input type=\"checkbox\" name=\"u_ban\" value=\"1\" /> Ban credit card {$cc}, ban account #{$account_id} and delete all his ads ?</td></tr>\n";
		echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
		echo "</table>";
		echo "<input type=\"hidden\" name=\"action_id\" value=\"{$transaction_id}\" />";
		echo getFilterFormFields();
		echo "</form>";
		return true;
	}

	//were going live
	$amount = number_format(floatval($_REQUEST["u_amount"]), 2);
	$cancel_subscription = (boolean)($_REQUEST["u_cancel_subscription"]);
	$ban = (boolean)($_REQUEST["u_ban"]);

	//mark purchase as chargebacked
	$anet = new anet();
	$error = null;
	$ret = $anet->chargebackByPurchaseId($purchase_id, $amount, $cancel_subscription, $error);

	if ($ret === false)
		return actionError("Chargeback failed: '{$error}'. Please contact administrator.");
	$msg = $ret;

	//audit chargeback
	audit::log("PUC", "Chargeback", $purchase_id, $msg, $account->getId());

	if ($ban) {
		//ban CC
		$cre = creditcard::findOneById($cc_id);
		if (!$cre)
			return actionWarning("Chargeback succeeded: {$msg}. But banning CC #{$cc_id} failed!");
		$ret = $cre->ban("chargeback");
		if ($ret)
			$msg .= " Credit card #{$cc_id} banned.";

		//ban account
		$acc = account::findOneById($account_id);
		if (!$acc)
			return actionWarning("Chargeback succeeded: {$msg}. CC banned. But banning account #{$account_id} failed!");
		$ret = $acc->ban("chargeback");
		if ($ret)
			$msg .= " Account #{$account_id} banned.";

		//delete all ads by account
		$res = $db->q("SELECT c.* FROM classifieds c WHERE c.account_id = ? AND c.deleted IS NULL", [$account_id]);
		$cl = 0;
		while ($row = $db->r($res)) {
			//fetch clad
			$clad = clad::withRow($row);
			if (!$clad)
				continue;
			//remove ad
			if ($clad->remove())
				$cl++;
		}
		$msg .= " {$cl} ads of account #{$account_id} deleted.";
	}

	//TODO send out email

	return actionSuccess($msg);
}

function mark_chargeback() {
	global $account, $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid transaction id !");
	$transaction_id = $action_id;

	$res = $db->q("
		SELECT t.trans_id, t.amount
			, p.trans_id as payment_trans_id, p.subscription_id, p.subscription_status, p.account_id, p.cc_id 
			, cc.cc, cc.type as cc_type
		FROM transaction t 
		INNER JOIN payment p on p.id = t.payment_id
		INNER JOIN account_cc cc on cc.cc_id = p.cc_id
		WHERE t.id = ?", 
		[$transaction_id]
		);
	if ($db->numrows($res) != 1)
		return actionError("Cant find transaction with id '$transaction_id' !");
	$row = $db->r($res);

	$trans_id = $row["trans_id"];
	$amount = $row["amount"];
	$payment_trans_id = $row["payment_trans_id"];
	$payment_subscription_id = $row["subscription_id"];
	$payment_subscription_status = $row["subscription_status"];
	$account_id = $row["account_id"];
	$cc_id = $row["cc_id"];
	$cc = $row["cc_type"]." X".substr($row["cc"], -4);

	if (!isset($_REQUEST["submit"])) {
		echo "<h2>Mark transaction {$trans_id} of amount \${$amount} as chargebacked ?</h2>";
		echo "<form method=\"post\" action=\"\">";
		echo "<table>";
		if ($payment_subscription_status == 1) {
			echo "<tr><td/><td><input type=\"checkbox\" name=\"u_cancel_subscription\" value=\"1\" checked=\"checked\"> Cancel subscription too</td></tr>";
		}
		echo "<tr><th>Amount:</th><td><input type=\"text\" name=\"u_amount\" value=\"{$amount}\" size=\"8\" /></td></tr>\n";
		echo "<tr><th>Ban CC:</th><td><input type=\"checkbox\" name=\"u_ban\" value=\"1\" /> Ban credit card {$cc}, ban account #{$account_id} and delete all his ads ?</td></tr>\n";
		echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
		echo "</table>";
		echo "<input type=\"hidden\" name=\"action_id\" value=\"{$transaction_id}\" />";
		echo getFilterFormFields();
		echo "</form>";
		return true;
	}

	//were going live
	$amount = number_format(floatval($_REQUEST["u_amount"]), 2);
	$cancel_subscription = (boolean)($_REQUEST["u_cancel_subscription"]);
	$ban = (boolean)($_REQUEST["u_ban"]);

	//mark transaction as chargebacked
	$error = null;
	$ret = payment::chargeback($transaction_id, $cancel_subscription, $amount, $error);
	if ($ret === false)
		return actionError("Chargeback failed: '{$error}'. Please contact administrator.");
	$msg = $ret;

	//audit chargeback
	audit::log("CHB", "Chargeback", $transaction_id, $msg, $account->getId());

	if ($ban) {
		//ban CC
		$cre = creditcard::findOneById($cc_id);
		if (!$cre)
			return actionWarning("Chargeback succeeded: {$msg}. But banning CC #{$cc_id} failed!");
		$ret = $cre->ban("chargeback");
		if ($ret)
			$msg .= " Credit card #{$cc_id} banned.";

		//ban account
		$acc = account::findOneById($account_id);
		if (!$acc)
			return actionWarning("Chargeback succeeded: {$msg}. CC banned. But banning account #{$account_id} failed!");
		$ret = $acc->ban("chargeback");
		if ($ret)
			$msg .= " Account #{$account_id} banned.";

		//delete all ads by account
		$res = $db->q("SELECT c.* FROM classifieds c WHERE c.account_id = ? AND c.deleted IS NULL", [$account_id]);
		$cl = 0;
		while ($row = $db->r($res)) {
			//fetch clad
			$clad = clad::withRow($row);
			if (!$clad)
				continue;
			//remove ad
			if ($clad->remove())
				$cl++;
		}
		$msg .= " {$cl} ads of account #{$account_id} deleted.";
	}

	//TODO send out email

	return actionSuccess($msg);
}


/*
function revert() {
	global $account, $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid purchase id !");
	$purchase_id = $action_id;

	$res = $db->q("
		SELECT ap.transaction_id as purchase_trans_id, ap.total as purchase_total
			, p.*
			, a.email
		FROM account_purchase ap
		INNER JOIN payment p on p.id = ap.payment_id
		INNER JOIN account a on a.account_id = p.account_id
		WHERE ap.id = ?", 
		[$purchase_id]
		);
	if ($db->numrows($res) != 1)
		return actionError("Cant find purchase with id '$purchase_id' !");
	$row = $db->r($res);

	$trans_id = $row["purchase_trans_id"];
	$payment_id = $row["id"];
	$subscription_id = $row["subscription_id"];
	$clad_id = intval($row["p3"]);
	$account_id = $row["account_id"];
	$email = $row["email"];

	if (!$clad_id)
		return actionError("Cant determine clad_id !");

	if (!isset($_REQUEST["submit"])) {
		echo "<h2>Revert transaction #{$trans_id} (payment_id={$payment_id}) to declined ?</h2>";
		echo "<form method=\"post\" action=\"\">";
		echo "<table>";
		echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
		echo "</table>";
		echo "<input type=\"hidden\" name=\"action_id\" value=\"{$purchase_id}\" />";
		echo getFilterFormFields();
		echo "</form>";
		return true;
	}

	//were going live
	$anet = new anet();
	file_log("anet", "revert(): purchase_id={$purchase_id}, payment_id={$payment_id}, trans_id={$trans_id}, subscription_id={$subscription_id}, clad_id={$clad_id}, account_id={$account_id}, email={$email}");

	//remove transaction
	$res = $db->q("DELETE FROM transaction WHERE payment_id = ?", [$payment_id]);
	$aff = $db->affected($res);
	if ($aff != 1) {
		file_log("anet", "revert(): Error while deleting transaction, aff={$aff}");
		die("Error while deleting transaction, aff={$aff} !");
	}
	file_log("anet", "revert(): Transaction deleted");

	//cancel subscription
	if ($subscription_id) {
		$error = null;
		$ret = $anet->cancelSubscriptionById($subscription_id, "revert", $error);
		if ($ret === false) {
			file_log("anet", "revert(): Error while canceling subscription, error={$error}");
			die("Error while canceling subscription, error={$error} !");
		}
		file_log("anet", "revert(): Subscription canceled");
	}

	//update payment
	$res = $db->q("
		UPDATE payment 
		SET result = 'D', last_rebill_result = NULL, trans_id = NULL, subscription_id = NULL, subscription_status = NULL, next_renewal_date = NULL
		WHERE id = ?
		LIMIT 1",
		[$payment_id]
		);
	$aff = $db->affected($res);
	if ($aff != 1) {
		file_log("anet", "revert(): Error while updating payment, aff={$aff}");
		die("Error while updating payment, aff={$aff} !");
	}
	file_log("anet", "revert(): Payment updated");

	//expiring ad
	$db->q("UPDATE classifieds_loc SET done = 0 WHERE post_id = ?", [$clad_id]);
	$aff = $db->affected($res);
	if ($aff != 1) {
		file_log("anet", "revert(): Error while updating clad_loc, aff={$aff}");
		die("Error while updating clad_loc, aff={$aff} !");
	}
	$db->q("UPDATE classifieds_sponsor SET done = 0 WHERE post_id = ?", [$clad_id]);
	$db->q("UPDATE classifieds_side SET done = 0 WHERE post_id = ?", [$clad_id]);
	$db->q("
		UPDATE classifieds 
		SET done = 0, time_next = NULL, expires = '2018-04-06 00:00:00', expire_stamp = NULL, recurring = 0, auto_renew = 0
		WHERE id = ?",
		[$clad_id]
		);
	$aff = $db->affected($res);
	if ($aff != 1) {
		file_log("anet", "revert(): Error while updating clad, aff={$aff}");
		die("Error while updating clad, aff={$aff} !");
	}
	file_log("anet", "revert(): Classified expired");

	//send out email about wrong approval
	$text = "Hello,\n\nWe apologize for the confusion, but few hours ago there happened to be an error in our system and your credit card transaction #{$trans_id} for your ad #{$clad_id}, was not approved but it was fact declined. We mistakenly treated this decline as approval and put your ad live on our website. Now we have corrected this error and put your ad down.\n\n";
	$text .= "Again, your transaction was declined and therefore your money never left your account and this transaction should never appear on your credit card statement.\n\n";
	$text .= "If you still want to put your ad up, you need to login into your account, go to 'My Classified Ads' page and select 'Renew Ad' option from 'Manage My Ad' menu and do the purchase again.\n\n";
	$text .= "Thanks for understanding and we apologize again for our mistake.\n\nIf you decide to post your ad, drop us a line at admin@adultsearch.com and we will gave you some small upgrade for free.\n\n";
	$text .= "Have a nice day and thank you for advertising with us.\n\n";
	$text .= "Adultsearch.com team\nadmin@adultsearch.com";

	$html = nl2br($text);

	$subject = "Your transaction #{$trans_id} was declined and your ad expired";

	$ret = send_email(array(
			"from" => "admin@adultsearch.com",
			"to" => $email,
//		  "bcc" => "admin@tsescorts.com",
			"subject" => $subject,
			"html" => $html,
			"text" => $text,
			));
	if (!$ret) {
		file_log("anet", "revert(): Error while sending out email to {$email}.");
		die("Error while sending out email to {$email}.");
	}
	file_log("anet", "revert(): Email sent.");

	//remove purchase
	$res = $db->q("DELETE FROM account_purchase WHERE id = ?", [$purchase_id]);
	$aff = $db->affected($res);
	if ($aff != 1) {
		file_log("anet", "revert(): Error while deleting purchase, aff={$aff}");
		die("Error while deleting purchase, aff={$aff} !");
	}
	file_log("anet", "revert(): Purchase deleted");

	return actionSuccess("All good, trans #{$trans_id} reverted to decline and user '{$email}' notified about his expired clad #{$clad_id}");
}

function resend_renewal_email() {
	global $account, $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid purchase id !");
	$purchase_id = $action_id;

	$res = $db->q("
		SELECT ap.transaction_id as trans_id, p.email, t.id
		FROM account_purchase ap 
		INNER JOIN payment p on p.id = ap.payment_id
		INNER JOIN transaction t on t.trans_id = ap.transaction_id
		WHERE ap.id = ?", 
		[$purchase_id]
		);
		
	if ($db->numrows($res) != 1)
		die("ERR1");
	$row = $db->r($res);
	$trans_id = $row["trans_id"];
	$payment_email = $row["email"];
	$transaction_id = $row["id"];
	
	if ($_REQUEST["submit"] == "Send") {
		$email = $_REQUEST["u_email"];
		if ($email == "other")
			$email = $_REQUEST["u_other_email"];

		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			return actionError("Email '{$email}' does not have correct format!");
		}

		$anet = new anet();
		$ret = $anet->emailRenewalSuccess($transaction_id, $email);

		if ($ret)
			return actionSuccess("Successfully sent renewal email to email '{$email}'.");
		else
			return actionError("Error sending renewal email receipt for to email '{$email}' !");
	}

	echo "<h2>Re-send renewal email receipt for purchase #{$purchase_id} - transaction #{$trans_id} to:</h2>";
	echo "<form action=\"\" method=\"post\">\n";
	echo "<input type=\"radio\" name=\"u_email\" value=\"{$account->getEmail()}\" selected=\"selected\" />Me ({$account->getEmail()})<br />\n";
	echo "<input type=\"radio\" name=\"u_email\" value=\"{$payment_email}\" />Payment email ({$payment_email})<br />\n";
	echo "<input type=\"radio\" name=\"u_email\" value=\"other\" />Other email:<br />\n";
	echo "<input type=\"text\" name=\"u_other_email\" value=\"\" /><br /><br />\n";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$action_id}\" />\n";
	echo "<input type=\"submit\" name=\"submit\" value=\"Send\" />\n";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}

*/

function send_upcoming_charge_email() {
	global $account, $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid payment id !");
	$payment_id = $action_id;
	
	if ($_REQUEST["submit"] == "Send") {
		$email = $_REQUEST["u_email"];
		if ($email == "other")
			$email = $_REQUEST["u_other_email"];

		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			return actionError("Email '{$email}' does not have correct format!");
		}

		$charge_date = new \DateTime();
		$charge_date->modify("+2 days");
		$ret = payment::sendUpcomingChargeNotification($payment_id, $charge_date, $email);

		if ($ret)
			return actionSuccess("Successfully sent upcoming charge email for payment #{$payment_id} to email '{$email}'.");
		else
			return actionError("Error sending upcoming charge email for payment #{$payment_id} to email '{$email}' !");
	}

	echo "<h2>Send upcoming charge email for payment #{$payment_id} to:</h2>";
	echo "<form action=\"\" method=\"post\">\n";
	echo "<input type=\"radio\" name=\"u_email\" value=\"{$account->getEmail()}\" selected=\"selected\" />Me ({$account->getEmail()})<br />\n";
//	echo "<input type=\"radio\" name=\"u_email\" value=\"{$payment_email}\" />Payment email ({$payment_email})<br />\n";
	echo "<input type=\"radio\" name=\"u_email\" value=\"other\" />Other email:<br />\n";
	echo "<input type=\"text\" name=\"u_other_email\" value=\"\" /><br /><br />\n";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$action_id}\" />\n";
	echo "<input type=\"submit\" name=\"submit\" value=\"Send\" />\n";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}

function resend_receipt_email() {
	global $account, $db;

	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid transaction id !");
	$transaction_id = $action_id;

	$res = $db->q("
		SELECT t.payment_id, t.trans_id, p.email
		FROM transaction t 
		INNER JOIN payment p on p.id = t.payment_id
		WHERE t.id = ?", 
		[$transaction_id]
		);
		
	if ($db->numrows($res) != 1)
		die("ERR1");
	$row = $db->r($res);
	$payment_id = $row["payment_id"];
	$trans_id = $row["trans_id"];
	$payment_email = $row["email"];
	
	if ($_REQUEST["submit"] == "Send") {
		$email = $_REQUEST["u_email"];
		if ($email == "other")
			$email = $_REQUEST["u_other_email"];

		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			return actionError("Email '{$email}' does not have correct format!");
		}

		$res = $db->q("SELECT c.* FROM payment_item pi INNER JOIN classifieds c on c.id = pi.classified_id WHERE pi.payment_id = ?", [$payment_id]);
		if ($db->numrows($res) == 0)
			return actionError("Payment #{$payment_id} is not for classified ad !");
		else if ($db->numrows($res) > 10)
			return actionError("Payment #{$payment_id} is for too many clads: ".$db->numrows($res)." !");
		while ($row = $db->r($res)) {
			$clad = clad::withRow($row);
			if (!$clad)
				return actionError("Error can't init clad from row, clad_id=".$row["id"]);
			$ret = $clad->emailReceipt($email, $trans_id);
			if (!$ret)
				break;
		}
		if ($ret)
			return actionSuccess("Successfully sent receipt email to email '{$email}'.");
		else
			return actionError("Error sending receipt email to email '{$email}' !");
	}

	echo "<h2>Re-send receipt email receipt for transaction #{$transaction_id} - {$trans_id} (payment #{$payment_id}) to:</h2>";
	echo "<form action=\"\" method=\"post\">\n";
	echo "<input type=\"radio\" name=\"u_email\" value=\"{$account->getEmail()}\" selected=\"selected\" />Me ({$account->getEmail()})<br />\n";
	echo "<input type=\"radio\" name=\"u_email\" value=\"{$payment_email}\" />Payment email ({$payment_email})<br />\n";
	echo "<input type=\"radio\" name=\"u_email\" value=\"other\" />Other email:<br />\n";
	echo "<input type=\"text\" name=\"u_other_email\" value=\"\" /><br /><br />\n";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$action_id}\" />\n";
	echo "<input type=\"submit\" name=\"submit\" value=\"Send\" />\n";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}


function detail() {
	global $account, $db;

	$payment_id = $action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid purchase id !");

	$res = $db->q("SELECT * FROM payment WHERE id = ?", array($action_id));
	if ($db->numrows($res) != 1)
		return actionError("Cant find payment with id '$action_id' !");
	$row = $db->r($res);
	$account_id = $row["account_id"];
	$processor = $row["processor"];
	$responded_stamp = $row["responded_stamp"];
	$amount = $row["amount"];
	$recurring_amount = $row["recurring_amount"];
	$result = $row["result"];
	$decline_code = $row["decline_code"];
	$decline_reason = $row["decline_reason"];
	$subscription_status = $row["subscription_status"];
	$last_rebill_stamp = $row["last_rebill_stamp"];
	$last_rebill_result = $row["last_rebill_result"];

	echo "<h1>Payment #{$payment_id}</h1>";
	echo "<table class=\"control\"><tbody>";
	echo "<tr><th>Account</th><td><a href=\"/mng/accounts?account_id={$account_id}\">#{$account_id}</a></td></tr>\n";
	echo "<tr><th>Processor</th><td>{$processor}</td></tr>\n";
	echo "<tr><th>Amount</th><td>\${$amount}</td></tr>\n";
	echo "<tr><th>Recurring</th><td>";
	if (!$recurring_amount)
		echo "No";
	else
		echo "Yes";
	echo "</td></tr>\n";
	if ($recurring_amount) {
		echo "<tr><th>Recurring amount</th><td>\${$recurring_amount}</td></tr>\n";
		echo "<tr><th>Recurring status</th><td>";
		if ($subscription_status == 1)
			echo "Active";
		else if ($subscription_status == 2)
			echo "Canceled";
		else
			echo "UNKNOWN";
		echo "</td></tr>\n";
	}
	echo "</tbody></table><br />\n";

	echo "<h2>Payment items</h2>";
	echo "<table class=\"control\">";
	echo "<tr><th>Type</th><th>Description</th><th>Location</th></tr>\n";

	$res = $db->q("SELECT * FROM payment_item pi WHERE pi.payment_id = ?", [$payment_id]);
	$clad_ids = [];
	while ($row = $db->r($res)) {
		echo "<tr>";
		if ($row["type"] == "classified") {
			$clad_ids[] = $row["classified_id"];
			echo "<td>Classified ad <a href=\"/mng/classifieds?cid={$row["classified_id"]}\" target=\"_blank\">#{$row["classified_id"]}</a>";
			if ($row["classified_status"] == "N")
				echo " - New ad";
			else if ($row["classified_status"] == "U")
				echo " - Upgrade";
			echo "</td>";
			echo "<td>";
			if (!$row["sponsor"] && !$row["side"] && !$row["sticky"] && !$row["sponsor_desktop"] && !$row["sponsor_mobile"] && !$row["daily_repost"])
				echo "Standard ad<br />";
			if ($row["sponsor"])
				echo "City thumbnail<br />";
			if ($row["side"])
				echo "Side sponsor<br />";
			if ($row["sticky"])
				echo "Sticky ad on {$row["sticky_position"]} position<br />";
			if ($row["sponsor_desktop"])
				echo "Sticky ad on Desktop<br />";
			if ($row["sponsor_mobile"])
				echo "Sticky ad on Mobile<br />";
			if ($row["daily_repost"])
				echo "Daily repost<br />";
			$res2 = $db->q("SELECT l.loc_name, l.s FROM location_location l WHERE l.loc_id = ?", [$row["loc_id"]]);
			if ($db->numrows($res2)) {
				$row2 = $db->r($res2);
				echo "<td>{$row2["loc_name"]}, {$row2["s"]}<td>";
			} else {
				echo "<td><span style=\"color: red; font-weight: bold;\">Unknown location</span></td>";
			}
		} else if ($row["type"] == "businessowner") {
			$html_description = businessowner::get_html_description($row);
			echo "<td>Business owner</td>";
			echo "<td>{$html_description}</td>";
			echo "<td></td>";
		} else if ($row["type"] == "advertise") {
			echo "<td>Advertising credit</td>";
			echo "<td></td>";
			echo "<td></td>";
		} else if ($row["type"] == "repost") {
			echo "<td>Global top-up credits</td>";
			echo "<td>{$row["auto_renew"]} top-up credits</td>";
			echo "<td></td>";
		} else {
			echo "<td><span style=\"color: red; font-weight: bold;\">UNKNOWN TYPE - Contact admin</span></td>";
			echo "<td></td>";
			echo "<td></td>";
		}
	}

	echo "</table><br />\n";

	
	echo "<h2>Transactions</h2>";
	echo "<table class=\"control\"><tbody>";
	echo "<tr><th>Type</th><th>Date/Time</th><th>Amount</th><th>Result</th></tr>\n";

	$res = $db->q("
		SELECT p.result, p.trans_id, t.stamp, t.amount, t.type
		FROM payment p
		LEFT JOIN transaction t on t.payment_id = p.id
		WHERE p.id = ?
		ORDER BY t.stamp ASC
		", [$payment_id]
		);
	$i = 0;
	while ($row = $db->r($res)) {
		$i++;
		$stamp = $row["stamp"];
		$t_amount = $row["amount"];
		$type = $row["type"];

		echo "<tr>";

		echo "<td>";
		if ($i == 1)
			echo "Initial payment";
		else
			echo "Rebill";
		echo "</td>";

		echo "<td>";
		if ($stamp)
			echo date("Y-m-d H:i:s T", $stamp);
		else
			echo date("Y-m-d H:i:s T", $responded_stamp);
		echo "</td>";

		echo "<td>";
		if ($i == 1) {
			//initial
			if ($amount == $t_amount)
				echo "\${$amount}";
			else
				echo "\${$t_amount} - originally \${$amount}";
		} else {
			//rebill
			if ($recurring_amount == $t_amount)
				echo "\${$recurring_amount}";
			else
				echo "\${$t_amount} - originally \${$recurring_amount}";
		}
		echo "</td>";

		echo "<td>";
		if ($i == 1) {
			if ($result == "A")
				echo "Successfully charged";
			else if ($result == "D")
				echo "Declined: '{$decline_code} - {$decline_reason}'";
			else
				echo "<em>Other?</em>";
		} else {
			if ($last_rebill_result == "A")
				echo "Successfully charged";
			else if ($last_rebill_result == "D")
				echo "Declined: '{$decline_code} - {$decline_reason}'";
			else
				echo "<em>Other?</em>";
		}
		echo "</td>";
		echo "</tr>";
	}
	if ($last_rebill_stamp && $last_rebill_result == "D") {
		echo "<tr>";
		echo "<td>Rebill</td>";
		echo "<td>".date("Y-m-d H:i:s T", $last_rebill_stamp)."</td>";
		echo "<td>\${$recurring_amount}</td>";
		echo "<td>Declined: '{$decline_code} - {$decline_reason}'</td>";
		echo "</tr>\n";
	}
	echo "</tbody></table>\n";

	if ($recurring_amount && $subscription_status == 2 && count($clad_ids) > 0 && $account->isrealadmin()) {
		//check if there is a different active payment for this payment items
		$cnt = $db->single("
			SELECT COUNT(p.id) as cnt 
			FROM payment p 
			INNER JOIN payment_item pi on pi.payment_id = p.id
			WHERE p.subscription_status = 1 AND pi.classified_id IN (?)",
			[implode(",", $clad_ids)]
			);
		if ($cnt > 0)
			echo "There is another active payment for at least one of these classified ids.<br />\n";
		else
			echo "<a href=\"/mng/sales/?action=force_rebill&action_id={$payment_id}\" class=\"btn btn-warning btn-xs\">force rebill</a><br />\n";
	}

	echo "<br />";
	echo "<button type=\"button\" onclick=\"javascript:history.go(-1);\">Go back</button>\n";

	return false;
}

function force_rebill() {
	global $account, $db;

	$payment_id = $action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid payment id !");

	//echo "Rebilling payment #{$payment_id}...<br />\n";
	$error = null;
	$ret = payment::rebill($payment_id, $error);
	if ($ret)
		return actionSuccess("Payment #{$payment_id} rebilled successfully.");
	else
		return actionError("Error while rebilling payment #{$payment_id}: '{$error}'");
}


$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'resend_renewal_email':
		if (!permission::has("refund"))
			die;
		$ret = resend_renewal_email();
		if (!$ret)
			return;
		break;
	case 'resend_receipt_email':
		if (!permission::has("refund"))
			die;
		$ret = resend_receipt_email();
		if (!$ret)
			return;
		break;
	case 'cancel':
		if (!permission::has("refund"))
			die;
		$ret = cancel();
		if (!$ret)
			return;
		break;
	case 'refund':
		if (!permission::has("refund"))
			die;
		$ret = refund();
		if (!$ret)
			return;
		break;
	case 'anet_chargeback':
		if (!permission::has("refund"))
			die;
		$ret = anet_chargeback();
		if (!$ret)
			return;
		break;
	case 'mark_chargeback':
		if (!permission::has("refund"))
			die;
		$ret = mark_chargeback();
		if (!$ret)
			return;
		break;
	case 'detail':
		if (!permission::has("access_admin_sales"))
			die;
		$ret = detail();
		if (!$ret)
			return;
		break;
	case 'force_rebill':
		if (!permission::has("refund"))
			die;
		$ret = force_rebill();
		if (!$ret)
			return;
		break;
	case 'revert':
		if (!permission::has("refund"))
			die;
		$ret = revert();
		if (!$ret)
			return;
		break;
	case 'send_upcoming_charge_email':
		$ret = send_upcoming_charge_email();
		if (!$ret)
			return;
		break;
	default:
		break;
}

$params = [];
$where = getWhere($params);

$order = getOrder();
//TODO add default order
if (empty($order)) {
	$order = "ORDER BY t.stamp DESC";
}

$limit = getLimit();

//TODO add default order
if (empty($order))
	$order = "ORDER BY t.id DESC";

//query db
$sql = "SELECT count(*) as total
		FROM transaction t
		INNER JOIN payment p on p.id = t.payment_id
		INNER JOIN account a on a.account_id = p.account_id
		LEFT JOIN (
			SELECT pai.payment_id, pai.classified_id, c.deleted as c_deleted
			FROM payment_item pai
			LEFT JOIN classifieds c on c.id = pai.classified_id
			GROUP BY pai.payment_id) pi on pi.payment_id = p.id
		$where
		";
$res = $db->q($sql, $params);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT t.*
			, a.account_id, a.email, a.password, a.banned, a.ban_reason, a.agency
			, p.processor, p.cc_id, p.subscription_id, p.subscription_status, p.trans_id as original_trans_id
			, pi.classified_id
			, pi.c_deleted
			, ar.id as refund_id, ar.type as refund_type, ar.amount as refund_amount, ar.stamp as refund_stamp, aar.username as refund_who
		FROM transaction t
		INNER JOIN payment p on p.id = t.payment_id
		INNER JOIN account a on a.account_id = p.account_id
		LEFT JOIN account_refund ar on ar.transaction_id = t.id
		LEFT JOIN account aar on aar.account_id = ar.author_id
		LEFT JOIN (
			SELECT pai.payment_id, pai.classified_id, c.deleted as c_deleted
			FROM payment_item pai
			LEFT JOIN classifieds c on c.id = pai.classified_id
			GROUP BY pai.payment_id) pi on pi.payment_id = p.id
		$where
		$order 
		$limit";

$res = $db->q($sql, $params);
if ($db->numrows($res) == 0) {
	echo "No transactions.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Sales transactions</h2>\n";

displayFilterForm();

echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr><th class=\"check\"><input type=\"checkbox\" name=\"master\" value=\"\" /></th>
<th>".getOrderLink("Id", "t.id")."</th>
<th>".getOrderLink("Acc.Id", "p.account_id")."</th>
<th>".getOrderLink("Email", "a.email")."</th>
<th>".getOrderLink("CC.Id", "p.cc_id")."</th>
<th>".getOrderLink("Date", "t.stamp")."</th>
<th>".getOrderLink("AD.Id", "pi.classified_id")."</th>
<th>".getOrderLink("Total", "t.amount")."</th>
<th>".getOrderLink("Invoice #ID", "p.id")."</th>
<th>".getOrderLink("Transaction ID", "t.trans_id")."</th>
<th>Subscription</th>
<th>Refunded?</th>
<th></tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {

	$processor = $rox["processor"];

	echo "<tr>";
	echo "<td class=\"check\"><input type=\"checkbox\" name=\"id[]\" value=\"{$rox['id']}\" /></td>";
	echo "<td>{$rox['id']}</td>";

	if ($rox["account_id"]) {
		echo "<td><a href=\"/mng/accounts?account_id={$rox["account_id"]}\">{$rox["account_id"]}</a>";
		if ($rox["banned"]) {
			echo "<br /><span style=\"color: red; font-weight: bold;\">Banned</span>";
			if ($rox["ban_reason"] == "spammer")
				echo "<br /><span style=\"color: red;\">(spammer)</span>";
			elseif ($rox["ban_reason"] == "chargeback")
				echo "<br /><span style=\"color: red;\">(chargeback)</span>";
		} else if ($deleted) {
			echo "<br /><span style=\"color: darkred;\">Deleted</span>";
		}
		if ($rox["agency"]) 
			echo "<br /><span style=\"color: green; font-weight: bold;\">Escort agency</span>";
		echo "</td>";
	} else {
		echo "<td>-</td>";
	}

//	echo "<td><a href=\"?password={$rox["password"]}&submit=Submit\">{$rox["password"]}</a></td>";
	echo "<td>{$rox["email"]}</td>";

	if ($rox["cc_id"] == 0)
		echo "<td></td>";
	else {
		if ($rox["cc_deleted"])
			echo "<td><span style=\"color:red;\">Deleted:<a href=\"/mng/creditcards?id={$rox["cc_id"]}\">{$rox["cc_id"]}</a></span></td>";
		else
			echo "<td><a href=\"/mng/creditcards?id={$rox["cc_id"]}\">{$rox["cc_id"]}</a></td>";
	}

	echo "<td>".get_datetime_admin_timezone($rox["stamp"])."</td>";

	$links = "";

	if ($rox["classified_id"]) {
		$clad_id = $rox["classified_id"];
		$clad_deleted = $rox["c_deleted"];
		$clad = clad::findOneById($clad_id);
		if (!$clad)
			echo "<td><span style=\"color: red; font-weight: bold;\">ERROR - cant find clad # {$clad_id}!</span></td>";
		else {
			echo "<td><a href=\"/mng/classifieds?cid=".$clad->getId()."\">".$clad->getId()."</a>";
			if ($clad_deleted)
				echo "<br /><span style=\"color: darkred;\">Deleted</span>";
			echo "</td>";
			$links = "<a href=\"".$clad->getUrl()."\">view ad</a>";
		}
	} else {
		echo "<td>-</td>";
	}

	$amount = $rox["amount"];
//	if ($rox["refund_id"])
//		$amount += $rox["refund_amount"];
	echo "<td>".number_format($amount, 2)."</td>";

	echo "<td>AS{$rox["payment_id"]}</td>";

	echo "<td>";
	if ($processor == "anet")
		echo "<a href=\"https://account.authorize.net/ui/themes/anet/transaction/transactiondetail.aspx?transID={$rox["trans_id"]}\" target=\"anet\">{$rox["transaction_id"]}</a>";
	else if ($processor == "securionpay")
		echo "<a href=\"https://securionpay.com/charges/{$rox["trans_id"]}\" target=\"securionpay\">{$rox["trans_id"]}</a>";
	else
		echo $rox["trans_id"];
	echo "</td>";

	echo "<td>";
	if (!is_null($rox["subscription_status"])) {
		if ($processor == "anet")
			echo "<a href=\"https://account.authorize.net/ui/themes/anet/ARB/SubscriptionDetail.aspx?SubscrID={$rox["subscription_id"]}\" target=\"anet\">{$rox["subscription_id"]}</a>";
		else
			echo $rox["payment_id"];
		if ($rox["subscription_status"] == 1)
			echo "<br /><span style=\"color: green;\">Active</span>";
		else if ($rox["subscription_status"] == 2)
			echo "<br /><span style=\"color: darkred;\">Canceled</span>";
		else
			echo "<br /><span style=\"color: red; font-weight: bold;\">Unknown status</span>";
	}
	echo "</td>";

	echo "<td>";
	if ($rox["refund_id"]) {
		$str = "";
		if ($rox["refund_type"] == "R")
			echo "\${$rox["refund_amount"]} on ".date("Y-m-d H:i:s", $rox["refund_stamp"])." by {$rox["refund_who"]}";
		else if ($rox["refund_type"] == "V")
			echo "Voided (\${$rox["refund_amount"]}) on ".date("Y-m-d H:i:s", $rox["refund_stamp"])." by {$rox["refund_who"]}";
		else if ($rox["refund_type"] == "CH")
			echo "<strong>Chargeback!</strong> in value of {$rox["refund_amount"]} on ".date("Y-m-d H:i:s", $rox["refund_stamp"]);
		else
			echo "<span style=\"color: red; font-weight: bold;\">Unknown refund type: '{$rox["refund_type"]}' !";
	}
	echo "</td>";

//	if ($_SESSION["account"] == 3974)
	$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("detail", $rox['payment_id'])."\">payment detail</a>";

	if (!float_equal($rox["refund_amount"], $amount) && permission::has("refund")) {
		if ($processor == "anet") {
			//$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("anet_refund", $rox['id'])."\">refund</a>";
			$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("anet_chargeback", $rox['id'])."\">chargeback</a>";
		} else if ($processor == "securionpay" || $processor == "budget") {
			$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("refund", $rox['id'])."\">refund</a>";
		}
		if ($processor == "securionpay") {
			$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("mark_chargeback", $rox['id'])."\">chargeback</a>";
		}
	}

	if ($rox["subscription_status"] == 1 && permission::has("refund")) {
		$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("cancel", $rox['id'])."\">cancel&nbsp;subscription</a>";
	}

	if ($rox["trans_id"] != $rox["original_trans_id"]) {
		$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("resend_renewal_email", $rox['id'])."\">resend&nbsp;renewal&nbsp;email</a>";
	} else {
		$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("resend_receipt_email", $rox['id'])."\">resend&nbsp;receipt&nbsp;email</a>";
	}

	if ($account->isrealadmin()) {
		$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("revert", $rox['id'])."\">revert&nbsp;to&nbsp;declined</a>";
		$links .= "&nbsp;&middot;&nbsp;<a href=\"".getActionLink("send_upcoming_charge_email", $rox['payment_id'])."\">send&nbsp;upcoming&nbsp;charge&nbsp;email</a>";
	}

	echo "<td>{$links}</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;
echo "</form>";

?>
