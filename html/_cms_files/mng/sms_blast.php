<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) { 
	$account->asklogin(); 
	return false; 
}

if (!$account->isadmin()) {
	echo "You dont have privileges to access this page!";
	die;
}

ini_set('max_execution_time', 0);

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols, $classifieds;

//configuration
$ipp = 30;
$url_last_piece = "sms_blast";

?>
<h2>Sms blast</h2>

<style type="text/css">
.ui-progressbar {
	position: relative;
}
.progress-label {
	position: absolute;
	left: 50%;
	top: 4px;
	font-weight: bold;
	text-shadow: 1px 1px 0 #fff;
}
</style>
<script>
function update() {
	$('#sent').html(sent);
	if (to_send > 0)
		var val = parseInt((sent / to_send) * 100);
	else
		var val = 0;
	progressbar.progressbar("value", val);
}

function progress() {
	//send ajax request to send next batch of messages
	var jqxhr = $.get('/_ajax/sms_blast', {}, function(data) {
		var data2 = data.trim();
		if (data2.substr(0,1) == 'S') {
			sent += parseInt(data2.substr(2));
			update();
			if (sent < to_send) {
				setTimeout(progress, 3000);
			} else {
				$('#log').append('All messages successfully sent.<br />');
			}
		} else if (data2.substr(0,1) == 'E') {
			var data3 = data2.substr(2);
			var pos = data3.indexOf('-');
			if (!pos) {
				alert('Not valid format of error response from AJAX sms_blast script: ' + data2);
			} else {
				var ss = parseInt(data3.substr(0,pos));
				if (ss > 0)
				$('#log').append('Successfully sent '+ss+' messages from chunk, but then failed:<br />');
				var error = data3.substr(pos+1);
				$('#log').append('Error: '+error);
			}
		} else {
			alert('Unknown response from AJAX sms_blast script: ' + data2);
		}
	})
	.fail(function() {
		$('#log').append('Error - AJAX sms_blast hook failed ! Please contact administrator.');
	});
}
function countChar() {
	var len = $('#sb_msg').val().length;
	$('#sb_chrcount').text(len);
};
</script>

<?php
function add() {
	global $db;

	$invalid_area_codes = array("800", "844", "855", "866", "877", "888");	//toll free numbers - its not possible to send sms messages to these with swiftsms gateway

	if (isset($_REQUEST["submit"])) {
		//upload file
		if (!array_key_exists("number_file", $_FILES) || !array_key_exists("error", $_FILES['number_file']) || $_FILES['number_file']['error'] != UPLOAD_ERR_OK) {
			echo "Error: no number file attached!<br />\n";
			return false;
		}
		$tmp_file_path = _CMS_ABS_PATH."/../data/tmp/sms_blast/".uniqid()."_".$_FILES['number_file']['name'];
		if (!move_uploaded_file($_FILES['number_file']['tmp_name'], $tmp_file_path)) {
			echo "Error: cant move uploaded file into '{$tmp_file_path}' !<br />\n";
			return false;
		}
		//get message
		$message = $_REQUEST["message"];
		if (empty($message)) {
			echo "Error: empty message !<br />\n";
			return false;
		}
		//if only not paying, then build first list of phone numbers of paying escorts
		$only_not_paying = false;
		$paying_phones = [];
		if ($_REQUEST["only_not_paying"] == "1") {
			$only_not_paying = true;
			$res = $db->q("SELECT phone FROM classifieds WHERE done = 1 AND deleted IS NULL AND phone IS NOT NULL AND phone <> ''");
			while ($row = $db->r($res)) {
				$phone = preg_replace('/[^0-9\+]/', '', $row["phone"]);
				if (!$phone || strlen($phone) < 10)
					continue;
				$paying_phones[] = $phone;
			}
		}
		echo "Paying phones : ".count($paying_phones)." items.<br />\n";
		//read numbers from file
		$handle = fopen($tmp_file_path, "r");
		if (!$handle) {
			echo "Error: cant open tmp file '{$tmp_file_path}' for reading !<br />\n";
			return false;
		}
		$numbers = array();
		$dupes = 0;
		$invalid = 0;
		$paying = 0;
		while (($line = fgets($handle, 4096)) !== false) {
			if (empty($line))
				continue;
			$arr = explode(",", $line);
			$number = $arr[0];
			$number = preg_replace('/[^0-9\+]/', "", $number);
			if (empty($number))
				continue;
			if ($only_not_paying && in_array($number, $paying_phones)) {
				echo "<span style=\"color: orange;\">Phone number '{$number}' is already paying for an ad on AS, skipping</span><br />\n";
                $paying++;
				continue;
			}
			if (substr($number, 0, 1) != "+") {
				if (strlen($number) != 10) {
					echo "<span style=\"color: red;\">Error:</span> Invalid US/Canada phone number: '{$number}' (wrong number of digits)<br />\n";
					$invalid++;
					continue;
				}
				$area_code = substr($number, 0, 3);
				if (in_array($area_code, $invalid_area_codes)) {
					echo "<span style=\"color: red;\">Error:</span> Invalid US/Canada phone number: '{$number}' (invalid area number)<br />\n";
					$invalid++;
					continue;
				}
			}
			if (in_array($number, $numbers)) {
				$dupes++;
			} else {
				$numbers[] = $number;
			}
		}
		fclose($handle);
		echo "Read <strong>".count($numbers)."</strong> valid phone numbers from file.<br /><strong>{$dupes}</strong> dupes.<br /><span style=\"color: red;\">{$invalid}</span> invalid numbers, {$paying} phone numbers had already paid ad on AS (skipped).<br /><br />\n";
		if (count($numbers) > 15000) {
			echo "<span style=\"color: red;\">Error:</span> Safety stop, too much numbers!<br />\n";
			return false;
		}
		//insert into database
		$i = 0;
		$now = time();
		foreach ($numbers as $number) {
			$res = $db->q("INSERT INTO sms_blast (insert_stamp, phone, message) VALUES (?, ?, ?)", array($now, $number, $message));
			$id = $db->insertid($res);
			if (!$id) {
				echo "<span style=\"color: red;\">Error:</span> Insert to db failed !<br />\n";
				return false;
			} else {
				$i++;
			}
		}
		echo "Successfully inserted {$i} rows - messages to send.<br />\n";
		return true;
	}

	echo "<h2>Add new SMS blast:</h2>";
	echo "<form method=\"post\" action=\"\" enctype=\"multipart/form-data\">";
	echo "<table>";
	echo "<tr><th>CSV file with valid phone numbers<br />(only first column is relevant):</th><td><input type=\"file\" name=\"number_file\" style=\"width: 600px;\"/><br /></td></tr>\n";
	echo "<tr><th>Only phone numbers that are not paying for ad on AS ?</th><td><input type=\"checkbox\" name=\"only_not_paying\" value=\"1\" /></td></tr>\n";
	echo "<tr><th>Message:</th><td><textarea name=\"message\" id=\"sb_msg\" cols=\"80\" rows=\"5\" onkeyup=\"countChar();\"></textarea></td></tr>\n";
	echo "<tr><th>Character count:</th><td><span id=\"sb_chrcount\">0</span></td></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
	echo "</table>";
	echo "</form>";
	return false;
}

function run() {
	global $db;

	$res = $db->q("SELECT COUNT(*) as cnt FROM sms_blast WHERE sent_stamp IS NULL");
	if ($db->numrows($res) != 1)
		return false;
	$row = $db->r($res);
	$cnt = $row["cnt"];
	echo "<h3>Sending SMS messages...</h3>\n";
	echo "Sent <span id=\"sent\" style=\"font-weight: bold;\">0</span> from {$cnt} messages.<br />\n";
	echo "<div id=\"progressbar\"><div class=\"progress-label\">Loading...</div></div>\n";
	echo "<div id=\"log\">Log:<br /></div><br /><br /><br /><br />\n";
	echo "<script type=\"text/javascript\">\n
var to_send = {$cnt};\n
var sent = 0;\n
\n
var progressbar = $(\"#progressbar\"), progressLabel = $(\".progress-label\");\n
\n
progressbar.progressbar({\n
		value: false,\n
		change: function() {\n
			progressLabel.text( progressbar.progressbar(\"value\") + \"%\");\n
		},\n
		complete: function() {\n
			progressLabel.text(\"Complete!\");\n
		}\n
	});\n
\n
setTimeout(progress, 100);\n
</script>\n";

	return false;
}

function clear() {
	global $db;
	$res = $db->q("DELETE FROM sms_blast WHERE sent_stamp IS NULL");
	$aff = $db->affected($res);
	echo "Successfully deleted {$aff} unsent messages.<br />\n";
	return true;
}

$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'add':
		$ret = add();
		if (!$ret)
			return;
		break;
	case 'run':
		$ret = run();
		if (!$ret)
			return;
		break;
	case 'clear':
		$ret = clear();
		if (!$ret)
			return;
		break;
}


//output
echo "<a href=\"?action=add\">Add new blast</a><br />\n";
echo "<br />\n";

$res = $db->q("SELECT count(*) as cnt FROM sms_blast WHERE sent_stamp IS NULL");
if ($db->numrows($res) != 1) {
	echo "Error = can't get number of unsent sms messages!<br />\n";
	exit(1);
}
$row = $db->r($res);
$cnt = $row["cnt"];
if ($cnt == 0) {
	echo "There are no unsent SMS messages.<br />\n";
} else {
	echo "There are {$cnt} unsent SMS messages.<br />\n";
	echo "<a href=\"?action=run\">Send all of these</a><br /><br />\n";
	if ($account->isrealadmin()) {
		echo "<a href=\"?action=clear\" onclick=\"return confirm('Are you sure you want to clear all unsent messages from the list ?');\">Clear list</a><br />\n";
	}
}

?>
