<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
require_once(_CMS_ABS_PATH."/inc/classes/parsecsv.lib.php");

define('UPL_DIR', _CMS_ABS_PATH."/tmp");

global $account, $db, $num_errors, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

$num_errors = 0;

//we have to be logged in 
if (!$account->isMember()) {
	$account->asklogin();
	return false;
}

if (!$account->isadmin()) {
	echo "You dont have privileges to access this page!";
	die;
}

function er($msg, $fix = "") {
	global $num_errors;
	echo "<span style=\"color: red;\"><strong>Error</strong>: {$msg}</span> {$fix}<br />\n";
	$num_errors++;
}

function process() {
	global $db, $config_image_path, $mcache, $num_errors;

	set_time_limit(0);

	//checks
	if (!is_dir(UPL_DIR)) {
		echo "<span style=\"color: red;\">Tmp upload dir '".UPL_DIR."' does not exists !</span><br />";
		return false;
	}

	$live = intval($_REQUEST["live"]);

	$filepath = UPL_DIR."/"."terid.csv";
	if ($live == 0) {
		//store csv file into temp directory
		$file = new Upload($_FILES['file']);
		if ($file->uploaded) {
			@unlink($filepath);
			$file->file_new_name_body = "terid";
			$file->file_new_name_ext = "csv";
			$file->Process(UPL_DIR."/");
			if (!$file->processed) {
				echo "<span style=\"color: red;\">Error while copying uploaded csv file to directory '".UPL_DIR."' !</span><br />";
				return false;
			}
		}
	}
	
	//parse csv file
	$csv = new parseCSV();
	$csv->auto($filepath);
	$num_entries = count($csv->data);
	if ($num_entries > 0)
		echo "Parsed CSV file: <strong>{$num_entries}</strong> entries.<br />\n";
	else {
		echo "<span style=\"color: red;\">Error: CSV file does not have any entry.</span><br />\n";
		return;
	}

/*
	foreach ($csv->data as $key => $entry) {
		//echo "key={$key}, entry=".print_r($entry, true)."<br />\n";
		$as_url = $entry["as_url"];
		$phone = $entry["phone"];
		$terid = $entry["terid"];
		$ter_url = $entry["ter_url"];
		$clad_id = basename($as_url);
		//echo "as_url={$as_url}, clad_id={$clad_id}<br />\n";
		$res = $db->q("SELECT phone, ter FROM classifieds WHERE id = ?", array($clad_id));
		if ($db->numrows($res) != 1) {
			echo "<span style=\"color: red;\">Error: Could not find classified with id#{$clad_id} !</span><br />\n";
			continue;
		}
		$row = $db->r($res);
		$ter = intval($row["ter"]);
		if ($ter == 0) {
			echo "Adding terid '{$terid}' for clad id#{$clad_id}... <a href=\"{$as_url}\">{$as_url}</a> - <a href=\"{$ter_url}\">{$ter_url}</a><br />\n";
			//TODO
			$res2 = $db->q("UPDATE classifieds SET ter = ? WHERE id = ? LIMIT 1", array($terid, $clad_id));
			if ($db->affected($res2) != 1) {
				echo "<span style=\"color: red;\">Error: updating ter for clad id#{$clad_id} !</span><br />\n";
				break;
			}
		} else if ($ter <> $terid) {
			echo "<span style=\"color: red;\">Error: Different TERid for clad id#{$clad_id}: in db: {$ter}, from excel: {$terid} !</span> <a href=\"{$as_url}\">{$as_url}</a> - <a href=\"{$ter_url}\">{$ter_url}</a><br />\n";
			$res2 = $db->q("UPDATE classifieds SET ter = ? WHERE id = ? LIMIT 1", array($terid, $clad_id));
			if ($db->affected($res2) != 1) {
				echo "<span style=\"color: red;\">Error: updating ter for clad id#{$clad_id} !</span><br />\n";
				break;
			}
		} else {
			//the same terid is already in db, do nothing
		}
	}
*/

/*
	$clads = array();
	foreach ($csv->data as $key => $entry) {
		$as_url = $entry["as_url"];
        $phone = $entry["phone"];
        $terid = $entry["terid"];
        $ter_url = $entry["ter_url"];
        $clad_id = basename($as_url);
		if (array_key_exists($clad_id, $clads)) {
			$arr = $clads[$clad_id];
			$arr[] = $entry;
			$clads[$clad_id] = $arr;
		} else {
			$clads[$clad_id] = array($entry);
		}
	}

	$singles = array();
	$multiples = array();
	foreach ($clads as $key => $val) {
		if (count($val) == 1)
			$singles[$key] = $val;
		else
			$multiples[$key] = $val;
	}
	
	echo "singles cnt=".count($singles)."<br />\n";
	echo "multiples cnt=".count($multiples)."<br />\n";

	$h1 = fopen(_CMS_ABS_PATH."/tmp/singles.csv", "w");
	fputs($h1, "as_url,phone,terid,ter_url\n");
	foreach ($singles as $val) {
		foreach ($val as $entry) {
			fputs($h1, $entry["as_url"].",".$entry["phone"].",".$entry["terid"].",".$entry["ter_url"]."\n");
		}
	}
	fclose($h1);
	
	$h2 = fopen(_CMS_ABS_PATH."/tmp/multiples.csv", "w");
	fputs($h2, "as_url,phone,terid,ter_url\n");
	foreach ($multiples as $val) {
		foreach ($val as $entry) {
			fputs($h2, $entry["as_url"].",".$entry["phone"].",".$entry["terid"].",".$entry["ter_url"]."\n");
		}
	}
	fclose($h2);
*/

	return true;
}

?>
<h1>TER id upload</h1>
<?php

switch ($_REQUEST["action"]) {
	case 'process':
		$ret = process();
		if (!$ret)
			return;
		break;
}

?>
<form action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="action" value="process" />
<input type="hidden" name="live" value="0" />
Please select CSV file :<input type="file" name="file" />
<input type="submit" name="submit" value="Upload" />
</form>
