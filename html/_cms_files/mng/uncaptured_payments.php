<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!account::ensure_admin())
	die("no privileges!");

if (!$account->isrealadmin()) {
	die("no privileges!");
}

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols, $classifieds;

//configuration
$ipp = 30;
$url_last_piece = "uncaptured_payments";
$where_cols = [
	"id"			 => array("title" => "ID", "where" => "p.id", "match" => "exact", "size" => "5"),
	"account_id"	 => array("title" => "Acc.ID", "where" => "a.account_id", "match" => "exact", "size" => "5"),
	"transaction_id" => array("title" => "Trans Id", "where" => "p.trans_id", "size" => "35"),
];

function capture() {
	global $account, $db;

	$live = ($_REQUEST["live"] == 1) ? true : false;
	$action_id = intval($_REQUEST["action_id"]);
	if ($action_id == 0)
		return actionError("Invalid transaction id !");
	$payment_id = $action_id;

	$res = $db->q("
		SELECT p.*
		FROM payment p
		WHERE p.id = ?", 
		[$payment_id]
		);
	if ($db->numrows($res) != 1)
		return actionError("Cant find payment #{$payment_id} !");
	$row = $db->r($res);

	$amount = $row["amount"];
	$trans_id = $row["trans_id"];
	$subscription_status = $row["subscription_status"];
	
	if ($live) {
		$error = null;
		$ret = payment::capture($payment_id, $error);
		if (!$ret) {
			return actionError("Error: Failed to capture payment #{$payment_id} (charge_id '{$trans_id}'): '{$error}' !");
		} else {
			return actionSuccess("Payment #{$payment_id} (charge_id '{$trans_id}') captured successfully");
		}
	}
	
	echo "<h2>Capture payment #{$payment_id} ?</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "Are you sure you want to capture payment #{$payment_id} (\${$amount}) ?<br />\n";
	echo "<input type=\"submit\" name=\"submit\" value=\"Capture\" />\n";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$payment_id}\" />";
	echo "<input type=\"hidden\" name=\"live\" value=\"1\" />";
	echo getFilterFormFields();
	echo "</form>";
	return true;
}


$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'capture':
		if (!permission::has("refund"))
			die;
		$ret = capture();
		if (!$ret)
			return;
		break;
	default:
		break;
}

$params = [];
$where = " WHERE p.result = 'U' ".getWhere($params, true);

$order = getOrder();
//TODO add default order
if (empty($order)) {
	$order = "ORDER BY p.id ASC";
}

$limit = getLimit();

//query db
$sql = "SELECT count(*) as total
		FROM payment p
		INNER JOIN account a on a.account_id = p.account_id
		$where
		";
$res = $db->q($sql, $params);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT p.*
		FROM payment p
		INNER JOIN account a on a.account_id = p.account_id
		$where
		$order 
		$limit";

$res = $db->q($sql, $params);
if ($db->numrows($res) == 0) {
	echo "No uncaptured payments.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Uncaptured payments</h2>\n";

displayFilterForm();

echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr><th class=\"check\"><input type=\"checkbox\" name=\"master\" value=\"\" /></th>
<th>".getOrderLink("Id", "p.id")."</th>
<th>".getOrderLink("Account Id", "p.account_id")."</th>
<th>".getOrderLink("Total", "p.amount")."</th>
<th>".getOrderLink("Trans Id", "p.trans_id")."</th>
<th></th>
</tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {

	echo "<tr>";
	echo "<td class=\"check\"><input type=\"checkbox\" name=\"id[]\" value=\"{$rox['id']}\" /></td>";
	echo "<td>{$rox['id']}</td>";
	echo "<td>{$rox['account_id']}</td>";
	echo "<td>{$rox['amount']}</td>";
	echo "<td>{$rox['trans_id']}</td>";

	$links = "<a href=\"".getActionLink("capture", $rox['id'])."\">capture</a>";

	echo "<td>{$links}</td>";
	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;
echo "</form>";

?>
