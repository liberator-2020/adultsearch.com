<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $db, $mcache, $gIndexTemplate, $config_db_name, $account, $grid;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("edit_all_places"))
    return;

class col {
	private $name;
	private $id;
	private $title;
	public function col($name, $title = null, $id = false) {
		$this->name = $name;
		$this->title = $title;
		$this->id = $id;
	}
	public function getName() {
		return $this->name;
	}
	public function getTitle() {
		return (!empty($this->title)) ? $this->title : $this->name;
	}
}

class filter {
	private $grid;
	private $name;
	private $title;
	private $value;
	private $options;
	private $facet;
	public function filter($grid, $name, $title, $facet = true) {
		$this->grid = $grid;
		$this->name = $name;
		$this->title = $title;
		$this->value = NULL;
		$this->facet = $facet;
		$this->options = array();
	}
	public function getName() {
		return $this->name;
	}
	public function getTitle() {
		return $this->title;
	}
	public function set() {
		return ($this->value == NULL) ? false : true;
	}
	public function setValue($value) {
		$this->value = $value;
	}
	public function getValue() {
		return $this->value;
	}
	public function addOption($value, $title, $count) {
		$this->options[] = array('value' => $value, 'title' => $title, 'count' => $count);
	}
	public function getFilterCondition() {
		$a = $this->getName();
		if ($a == "place_type_id")
			$a = "n.place_type_id";
		else if ($a == "loc_id")
			$a = "n.loc_id";
		else if ($a == "name")
			$a = "n.name";
		else if ($a == "state_id")
			$a = "l.loc_parent";
		else if ($a == "checked") {
			$b = $this->getValue();
			if ($b == 0)
				return " n.last_checked_stamp IS NULL ";
			else
				return " n.last_checked_stamp IS NOT NULL ";
		}
		$b = $this->getValue();
		if ($this->facet)
			return "$a = '$b'";
		else
			return "$a LIKE '%$b%'";
	}

	public static function cmp_filter_options_title($a, $b) {
		return strcmp($a['title'], $b['title']);
	}

	public function draw() {
//		echo "filter {$this->getName()}, #options=".count($this->options)."<br />";
		if ($this->set()) {
			if ($this->facet && !empty($this->options)) {
				$val = "<span style=\"color: red;\">Error?</span>";
				foreach($this->options as $option) {
					if ($option['value'] == $this->getValue()) {
						$val = $option['title'];
						break;
					}
				}
			} else {
				$val = $this->getValue();
			}
			echo "<span class=\"filter_set\">";
			if ($this->facet)
				echo $this->getTitle()."={$val}";
			else
				echo $this->getTitle()."~={$val}";
			echo " <a href=\"".$this->grid->getLink(array($this->getName()))."\">remove</a></span>\n";
		} else if (!$this->facet) {
			echo $this->getTitle()."<input type=\"text\" name=\"{$this->getName()}\" value=\"\" />\n";
		} else if (!empty($this->options) && (count($this->options) > 1 || ($this->options[0]['count'] != $this->grid->getTotal()))) {
			//alphabetically sort options by title
			usort($this->options, array('filter', 'cmp_filter_options_title'));
			echo $this->getTitle()."<select class=\"filter\" name=\"".$this->getName()."\">";
			echo "<option value=\"\">-- Select --</option>";
			foreach ($this->options as $option) {
				echo "<option value=\"{$option['value']}\">{$option['title']} ({$option['count']})</option>";
			}
			echo "</select>\n";
		}
	}
}

class grid {
	private $cols;		//ordered array of columns
	private $rows;		//ordered array of rows, row is ordered array of column values
	private $filters;	//filters we have set
	private $query;
	private $total;		//total number of rows
	private $ids;
	private $page;
	private $ipp = 30;
	private $ordercol;
	private $orderway;

	public function grid() {
		$this->cols = array();
		$this->rows = array();
		$this->query = "";
		$this->ids = NULL;
		$this->add_filters();
		$this->set_filter_values();
		$this->set_filter_options();
		$this->set_order();

		//set page
		$this->page = intval($_GET["page"]);
		if ($this->page == 0)
			$this->page = 1;
	}

	public function getTotal() {
		return $this->total;
	}

	public function getLimit() {
		return ($this->page - 1)*$this->ipp.", ".$this->ipp;
	}

	private function filter_exists($filter_name) {
		foreach ($this->filters as $filter) {
			if ($filter->getName() == $filter_name)
				return $filter;
		}
		return false;
	}

	/**
	 * function add all possible filters
	 */
	private function add_filters() {
		global $db;

		//add filters on base columns
		$this->filters[] = new filter($this, "place_type_id", "Place type");
		$this->filters[] = new filter($this, "country_id", "Country");
		$this->filters[] = new filter($this, "state_id", "State");
		$this->filters[] = new filter($this, "loc_id", "City");
//		$this->filters[] = new filter($this, "neighbor_id", "Neighborhood");
//		$this->filters[] = new filter($this, "name", "Name", false);
//		$this->filters[] = new filter($this, "address1", "Address", false);
		$this->filters[] = new filter($this, "checked", "Checked");
		$this->filters[] = new filter($this, "edit", "Status");

		//get facet filters
		//disabled now
		/*
		$sql = "select attribute_id, datatype, name from attribute where facet = 1";
		$res = $db->q($sql);
		while ($row = $db->r($res)) {
			$this->filters[] = new filter($this, $row['name'], $row['name']);
		}
		*/
	}

	/**
	 * sets values for filters that are set
	 */
	private function set_filter_values() {
		//process url and save filters
		$query_parts = explode('&', $_SERVER['QUERY_STRING']);
		$flts = array();
		foreach ($query_parts as $part) {
			if (strpos($part, "=") === FALSE)
				continue;
			$a = explode("=", $part);
			if (($flt = $this->filter_exists($a[0])) !== false)
				$flt->setValue($a[1]);
			else if ($a[0] == "query")
				$this->query = $a[1];
		}
	}

	private function set_filter_options() {
		global $mcache, $sphinx;

		$sphinx->reset();
		$sphinx->SetLimits(0, 1000, 1000);

		//first add where conditions (already selected filters)
		foreach ($this->filters as $filter) {
			if (!$filter->set())
				continue;
			$res = $sphinx->SetFilter($filter->getName(), array($filter->getValue()));
//			echo "setfilter: name={$filter->getName()} value={$filter->getValue()} res=$res<br />\n";
		}

		//set total row number
		$results = $sphinx->Query($this->query, "dir");
		$this->total = $results["total_found"];

		if ($this->query) {
			if ($this->total < 200) {
				$result = $results["matches"];
    	        $this->ids = NULL;
        	    foreach($results["matches"] as $res)
            		$this->ids .= $this->ids ? (",".$res["id"]) : $res["id"];
			} else {
				$this->ids = "-1";
			}
		}

		foreach ($this->filters as $filter) {
//			echo "adding options for filter name={$filter->getName()}<br />";
        
			$ret = $sphinx->SetGroupBy($filter->getName(), SPH_GROUPBY_ATTR, '@group desc');
//			echo "setgroupby res=$res<br />\n";
			$results = $sphinx->Query($this->query, "dir");
//			_darr($results);
			$rex = $results["matches"];
			foreach($rex as $res) {
//				echo "fv: {$filter->getName()} val={$res["attrs"][$filter->getName()]} dump=".var_dump($res["attrs"][$filter->getName()])."<br />";

				$value = $res["attrs"][$filter->getName()];
				if ($value == 0 && in_array($filter->getName(), array("neighbor_id", "state_id")))
					$value = NULL;

				if (is_null($value))
					continue;

				$count = $res["attrs"]["@count"];
				$title = $value;
				if (in_array($filter->getName(), array("country_id", "state_id", "loc_id", "neighbor_id"))) {
					$loc = $mcache->get("SQL:LOC-ID", array($value));
					if ($loc !== false)
						$title = $loc['loc_name'];
				} else if ($filter->getName() == "place_type_id") {
					$pty = $mcache->get("SQL:PTY-ID", array($value));
					if ($pty !== false)
						$title = $pty['name'];
				} else if ($filter->getName() == "edit") {
					if ($value == 1)
						$title = "Live";
					else if ($value == 0)
						$title = "Not Live";
				} else if ($filter->getName() == "checked") {
					if ($value == 1)
						$title = "Checked";
					else if ($value == 0)
						$title = "Not Checked";
				}
				//echo "adding option filter name={$filter->getName()}, title={$title} val={$value}, count={$count}<br />";
				$filter->addOption($value, $title, $count);
			}
        	$sphinx->ResetGroupBy();
		}
	}
	
	/**
	 * sets order variables
	 */
	private function set_order() {
		//process url and save filters
		$query_parts = explode('&', $_SERVER['QUERY_STRING']);
		foreach ($query_parts as $part) {
			if (strpos($part, "=") === FALSE)
				continue;
			$a = explode("=", $part);
			if ($a[0] == "order") {
				$this->ordercol = urldecode($a[1]);
			}
			if ($a[0] == "orderway") {
				$this->orderway = urldecode($a[1]);
			}
		}
		if (!empty($this->ordercol) && empty($this->orderway)) {
			$this->orderway = "DESC";
		}
	}

	public function tooManyResults() {
		if ($this->ids === "-1")
			return true;
		return false;
	}
	
	public function noResults() {
		if ($this->total == 0)
			return true;
		return false;
	}

	public function getPager() {
		$pager = "Total results: {$this->total}.";
		if ($this->total > $this->ipp) {
			$total_pages = ceil($this->total / $this->ipp);
			$start = (($this->page - 1) * $this->ipp) + 1;
			$end = $this->page * $this->ipp;
			if ($end > $this->total)
				$end = $this->total;
			$pager .= " Page {$this->page} of {$total_pages} (results $start-$end).";
		}
	    if ($this->page > 1)
	        $pager .= "<a href=\"".$this->getPageLink($this->page-1)."\">Prev page</a> ";
    	if ($this->total > ($this->page*$this->ipp))
        	$pager .= "<a href=\"".$this->getPageLink($this->page+1)."\">Next page</a> ";
	    return $pager;
	}

	private function getPageLink($page) {
		return $this->getLink([], ["page" => $page]);
	}

	public function addCol($col) {
		$this->cols[] = $col;
	}

	/*
	 * $row is array of column values
	 */
	public function addRow($row) {
		$this->rows[] = $row;
		return true;
	}

	public function getNumcols() {
		return count($this->cols);
	}

	public function getNumRows() {
		return count($this->rows);
	}
	
	public function getNumFilters() {
		return count($this->filters);
	}

	public function getWhere() {

		if ($this->ids)
			return " AND n.place_id IN ($this->ids) ";

		$where = "";
		foreach ($this->filters as $filter) {
		//_d("filter = <pre>".print_r($filter,true)."</pre><br />");
			if (!$filter->set())
				continue;
			$where .= " AND ".$filter->getFilterCondition();
		}
		return $where;
	}
	
	public function getOrder() {

		if (!empty($this->ordercol))
			return " ORDER BY {$this->ordercol} {$this->orderway} ";

		return "";
	}

	public function getLink($except_filter = [], $new_parameters = []) {
		$curr_url = $_SERVER['REQUEST_URI'];
		if (strpos($curr_url, "?") !== FALSE)
			$base_url = substr($curr_url, 0, strpos($curr_url, "?"));
		else
			$base_url = $curr_url;

		$params = [];

		//add page
		if (intval($_REQUEST["page"]))
			$params["page"] = intval($_REQUEST["page"]);

		//add sort links
		if ($_REQUEST["order"] && $_REQUEST["orderway"]) {
			$params["order"] = $_REQUEST["order"];
			$params["orderway"] = $_REQUEST["orderway"];
		}

		//add filters
		foreach ($this->filters as $filter) {
			if (!$filter->set())
				continue;
			if (!empty($except_filter) && in_array($filter->getName(), $except_filter))
				continue;
			$params[$filter->getName()] = $filter->getValue();
		}

		//add new params
		$params = array_merge($params, $new_parameters);

		$url_filters = "";
		foreach ($params as $key => $val) {
			$url_filters .= (empty($url_filters)) ? "?" : "&";
			$url_filters .= $key."=".$val;
		}

		return $base_url.$url_filters;
	}

	private function draw_set_filters() {
		$first = true;
		foreach ($this->filters as $filter) {
			if (!$filter->set())
				continue;
			if ($first) {
				echo "Set filters:&nbsp;";
				$first = false;
			}
			$filter->draw();
			echo "<input type=\"hidden\" name=\"{$filter->getName()}\" value=\"{$filter->getValue()}\" />";
		}
		if (!$first)
			echo "<br />";
	}
	
	private function draw_filters() {
		$first = true;
		foreach ($this->filters as $filter) {
			if ($filter->set())
				continue;
			if ($first) {
				echo "<strong>Filters:</strong>&nbsp;";
				$first = false;
			}
			$filter->draw();
		}
	}
	
	private function getOrderLink($col_name) {
		$link = $this->getLink(array("order", "orderway"));

		if ($this->ordercol == $col_name) {
			$order_way = ($this->orderway == "DESC") ? "ASC" : "";
		} else {
			$order_way = "DESC";
		}

		if (empty($order_way))
			return $link;

		if (strpos($link, "?") !== false)
			return $link."&order={$col_name}&orderway={$order_way}";
		else
			return $link."?order={$col_name}&orderway={$order_way}";
	}
	
	private function getOrderClass($col_name) {
		if ($this->ordercol == $col_name) {
			return ($this->orderway == "DESC") ? "sort-desc" : "sort-asc";
		} else {
			return "sort";
		}
	}

	/*
	 * Draws HTML grid
	 */
	public function draw() {
		global $config_site_url, $config_img_server;

		echo "<link href=\"/css/unidir_explorer.css\" rel=\"stylesheet\" type=\"text/css\" />\n";
//		echo "<script type=\"text/javascript\" src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js\"></script>\n";
//		echo "<script type=\"text/javascript\" src=\"unidir_explorer.js\"></script>\n";
//		If we're unified, there's nothing we cannot do. (Ray Nagin)

		//draw filters
		echo "<div id=\"unidir_filters\" style=\"margin-top: 10px;\">\n";
		echo "<form method=\"get\">";
		echo $this->draw_set_filters();
		echo $this->draw_filters();
		echo "Query:<input type=\"text\" name=\"query\" value=\"{$this->query}\" />";
		echo "<input type=\"submit\" name=\"submit\" value=\"Refresh\" /></form>";
		echo "</div>\n";

		if ($this->tooManyResults()) {
			echo "Too many results($this->total) for displaying. Please use filters to limit search results with this query.<br />\n";
			return;
		}
		
		if ($this->noResults()) {
			echo "No results.<br />\n";
			return;
		}

		echo $this->getPager();
		echo "<table class=\"unidir_explorer\">\n";
		echo "<thead><tr>";
		foreach ($this->cols as $col) {
			$order_link = $this->getOrderLink($col->getName());
			$class = $this->getOrderClass($col->getName());
			$class = (!empty($class)) ? " class=\"{$class}\" " : "";
			if (!empty($order_link)) {
				echo "<th><a href=\"{$order_link}\" {$class}>{$col->getTitle()}</a></th>";
			} else {
				echo "<th>".$col->getName()."</th>";
			}
		}
		echo "<th></th>";
		echo "</tr></thead>\n<tbody>\n";
		foreach ($this->rows as $row) {
			echo "<tr>";
			$view_link = $edit_link = null;
			foreach ($this->cols as $col) {
				$val = $row[$col->getName()];
				switch ($col->getName()) {
					case 'thumb':
						if (!empty($val))
							$val = " <img src=\"{$config_img_server}/place/t/{$val}\" />";
						break;
					case 'name':
						//get link to place
						$country = $row["country"];
						$view_link = dir::getPlaceLinkAux($row['loc_id'], $row['place_type_id'], $row['place_id'], $val);
						$edit_link = $config_site_url."/worker/{$country}?id={$row['place_id']}";
//						$val = "<a href=\"{$link}\" target=\"_new\">{$val}</a> {$edit_link}";
						break;
					case 'email':
						if (!empty($val))
							$val = "<a href=\"mailto:{$val}\">{$val}</a>";
						break;
					case 'website':
						if (!empty($val))
							$val = "<a href=\"{$val}\" target=\"_new\">{$val}</a>";
						break;
					case 'loc_lat':
						if (!empty($val) && !empty($row['loc_long'])) {
							$val_long = $row['loc_long'];
							$val .= " <a href=\"http://maps.google.com/maps?ll={$val},{$val_long}&spn=0.1,0.1&t=m&q={$val},{$val_long}\" target=\"_new\">map</a>";
						}
						break;
					case 'phone1':
//						$val = phoneNiceFormat($val);
						$val = "<a href=\"tel:{$val}\">".phoneNiceFormat($val)."</a>";
						break;
					case 'edit':
						if ($val == 1)
							$val = "Live";
						else if ($val == 0)
							$val = "Not Live";
						else
							$val = "Unknown status!";
						if ($row["source"])
							$val .= " ({$row["source"]})";
						break;
					default:
						break;
				}
				//echo "<td title=\"col-getName={$col->getName()} row[col-getName()]={$row[$col->getName()]}\">{$val}</td>";
				echo "<td>{$val}</td>";
			}
			//actions
			echo "<td>";
			echo "<a href=\"{$view_link}\" target=\"_new\" class=\"btn btn-sm btn-default\">view</a>";
			echo "<a href=\"{$edit_link}\" target=\"_new\" class=\"btn btn-sm btn-default\">edit</a>";
			if (!$row["last_checked_stamp"])
				echo "<a href=\"".$this->getLink(["action", "id"], ["action" => "checked", "id" => $row["place_id"]])."\" class=\"btn btn-sm btn-default\">open</button>";
			echo "<a href=\"".$this->getLink(["action", "id"], ["action" => "delete", "id" => $row["place_id"]])."\" class=\"btn btn-sm btn-default\">delete</a>";
			echo "</td>";
			echo "</tr>\n";
		}
		echo "</tbody>\n</table>";
		echo $this->getPager();
	}

	public function errorReload($error_msg) {
		flash::add(flash::MSG_ERROR, $error_msg);
		echo "<br /><strong>Please wait...</strong><br />";
		$go_url = $this->getLink(["action", "id"]);
		system::go($go_url);
		die;
	}

	public function successReload($msg) {
		flash::add(flash::MSG_SUCCESS, $msg);
		echo "<br /><strong>Please wait...</strong><br />";
		$go_url = $this->getLink(["action", "id"]);
		system::go($go_url);
		die;
	}

}

$grid = new grid();

if ($_REQUEST["action"] == "delete" && intval($_REQUEST["id"])) {
	$id = intval($_REQUEST["id"]);
	$res = $db->q("UPDATE place SET deleted = ? WHERE place_id = ? LIMIT 1", [time(), $id]);
	if ($db->affected($res) == 1) {
		$grid->successReload("Place #{$id} was deleted.");
	} else {
		$grid->errorReload("Error deleting place #{$id} !");
	}
} else if ($_REQUEST["action"] == "checked" && intval($_REQUEST["id"])) {
	$id = intval($_REQUEST["id"]);
	$res = $db->q("UPDATE place SET last_checked_stamp = ?, last_checked_by = ? WHERE place_id = ? LIMIT 1", [time(), $account->getId(), $id]);
	if ($db->affected($res) == 1) {
		$grid->successReload("Place #{$id} was marked as checked.");
	} else {
		$grid->errorReload("Error marking place #{$id} as checked !");
	}
}

if ($grid->tooManyResults() || $grid->noResults())
	return $grid->draw();

$grid->addCol(new col("place_id", "Id", true));
$grid->addCol(new col("thumb", "Thumbnail"));
$grid->addCol(new col("place_type_name", "Category"));
$grid->addCol(new col("loc_name", "Location"));
$grid->addCol(new col("name", "Name"));
$grid->addCol(new col("address1", "Address"));
$grid->addCol(new col("phone1", "Phone"));
$grid->addCol(new col("edit", "Status"));

/*
$sql = "select attribute_id, name from attribute";
$rex = $db->q($sql);
$sql_denorm = "";
$attr_count = count($rex);
while($rox = $db->r($rex)) {
	$attr_id = $rox["attribute_id"];
	$attr_name = $rox["name"];
	$attrs[$attr_id] = $attr_name;
	$sql_denorm .= ", MAX(CASE WHEN na.attribute_id = '$attr_id' THEN na.value ELSE NULL END) AS $attr_name";
	$grid->addCol(new col($attr_name), false);
}
*/

$where = $grid->getWhere();
$order = $grid->getOrder();

//TODO in case loc filter is selected we dont need to join huge localtion_location table !!!
//TODO maybe we dont need to join place_type table (pull place type name from memcached), 
//TODO	 when place_type_id filter is selected for sure ....
$sql = "SELECT n.*, nt.name as place_type_name, l.loc_name, l.country_id as country_id${sql_denorm} 
				FROM place n 
				LEFT JOIN place_type nt ON n.place_type_id = nt.place_type_id
				LEFT JOIN location_location l ON n.loc_id = l.loc_id
				LEFT JOIN place_attribute na ON n.place_id = na.place_id
				WHERE n.deleted IS NULL {$where}
				GROUP BY n.place_id
				{$order}
				LIMIT ".$grid->getLimit();

//echo "SQL:\n$sql";

$rex = $db->q($sql);
while($rox = $db->r($rex)) {
	$rox2 = array();	//XXX TODO add option to db.class to fetch only named array
	foreach ($rox as $key => $val) {
		if (is_numeric($key))
			continue;
		$rox2[$key] = $val;
		if ($key == "country_id") {
			$loc = $mcache->get("SQL:LOC-ID", array($val));
            if ($loc !== false)
				$rox2['country'] = $loc["dir"];
		}
	}
	//print_r($rox2);
	$grid->addRow($rox2);
}

//echo "#cols = ".$grid->getNumCols()."\n";
//echo "#rows = ".$grid->getNumRows()."\n";
//echo "#filters = ".$grid->getNumFilters()."\n";

echo "<style type=\"text/css\">td .btn { margin: 2px 10px 5px 2px; } </style>\n";
echo "<h1>Places</h1>\n";
$grid->draw();

?>
