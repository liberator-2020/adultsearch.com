<?php

global $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!account::ensure_admin())
    die("Invalid access");

function sphinxRefreshConf() {
	global $db;

	echo "<strong>Refreshing config</strong><br />";

	//create sphinx SQL
	$sql = "select attribute_id, datatype, name from attribute where facet = 1";
	$res = $db->q($sql);
	$facets = "";
	$facet_uints = "";
	while ($row = $db->r($res)) {
		$text = ($row['datatype'] == "text") ? "_text" : "";
		$facets .= "\t\t, MAX(CASE WHEN pa.attribute_id = '{$row['attribute_id']}' THEN pa.value{$text} ELSE NULL END) AS {$row['name']} \\\n";
		$facet_uints .= "\tsql_attr_uint\t\t= {$row['name']}\n";
	}
	$sql = "";
	$sql .= "\t\tSELECT p.place_id, p.place_type_id, p.loc_id, p.neighbor_id, p.name, p.address1, p.phone1, p.review, p.overall, p.last_review_id, p.edit \\\n";
	$sql .= "\t\t, (CASE WHEN lp1.loc_type = 1 THEN lp1.loc_id ELSE lp2.loc_id END) AS country_id \\\n";
	$sql .= "\t\t, (CASE WHEN lp1.loc_type = 2 THEN lp1.loc_id ELSE NULL END) AS state_id \\\n";
	$sql .= "\t\t, (CASE WHEN l.county_id <> 0 THEN l.county_id ELSE NULL END) AS county_id \\\n";
	$sql .= $facets;
	$sql .= "\t\t, COUNT(distinct pp.id) as video_cnt \\\n";
	$sql .= "\t\tFROM place p \\\n";
	$sql .= "\t\tINNER JOIN location_location l ON p.loc_id = l.loc_id \\\n";
	$sql .= "\t\tLEFT JOIN location_location lp1 ON l.loc_parent = lp1.loc_id \\\n";
	$sql .= "\t\tLEFT JOIN location_location lp2 ON lp1.loc_parent = lp2.loc_id \\\n";
	$sql .= (empty($facets)) ? "" : "\t\tLEFT JOIN place_attribute pa ON p.place_id = pa.place_id \\\n";
	$sql .= "\t\tLEFT JOIN place_picture pp ON pp.place_file_type_id = 2 and pp.module = 'place' and pp.id = p.place_id \\\n";
	$sql .= "\t\tWHERE p.edit >= 0 AND deleted IS NULL";
	$sql .= "\\\n\t\tGROUP BY p.place_id";

	$conf = "";
	$conf .= <<< END
source dir {
	type		= mysql
	sql_host	= 10.105.13.100
	sql_user	= adultsearch009
	sql_pass	= elanFero123
	sql_db		= asdb
	sql_port	= 3306
	sql_query	= \

END;
	$conf .= $sql;
	$conf .= <<< END

	sql_attr_uint		= place_type_id
	sql_attr_uint		= loc_id
	sql_attr_uint		= neighbor_id
	sql_attr_uint		= review
	sql_attr_uint		= overall
	sql_attr_uint		= last_review_id
	sql_attr_uint		= edit
	sql_attr_uint		= country_id
	sql_attr_uint		= state_id
	sql_attr_uint		= county_id
	sql_attr_uint		= video_cnt

END;
	$conf .= (!empty($facet_uints)) ? "\n{$facet_uints}" : "";
	$conf .= <<< END
	sql_range_step		= 1000
	sql_ranged_throttle	= 0
}

index dir {
	source			= dir
	path			= /var/lib/sphinx/dir
	docinfo			= extern
	mlock			= 0
	morphology		= none
	min_word_len	= 1
	charset_type	= sbcs
	enable_star		= 0
	html_strip		= 0
	ignore_chars	= '
}
END;
//'

/*
	//write conf to temp file
	$tmp_filepath = $_SERVER['DOCUMENT_ROOT']."/tmp/sphinx.dir.conf";
	if (($handle = fopen($tmp_filepath, "w")) === false) {
		echo "Can't open temp file!<br />";
		return false;
	}
	if (fwrite($handle, $conf) === false) {
		echo "Can't write to temp file!<br />";
		return false;
	}
	fclose($handle);
	echo "New conf file created ok.<br />";
	
	//backup conf file on db machine and copy the new one
	$command = "ssh adts@10.105.13.100 \"cp ~/sphinx.dir.conf ~/sphinx.dir.conf.bak\"";
	//echo "COMMAND= $command<br />";
	$line = system($command, $ret);
	if ($ret != 0) {
		echo "Command '{$command}' failed!<br >";
		return false;
	}
	echo "Backup of old conf file created ok.<br />";

	$command = "scp \"{$tmp_filepath}\" adts@10.105.13.110:~/sphinx.dir.conf";
	//echo "COMMAND= $command<br />";
	$line = system($command, $ret);
	if ($ret != 0) {
		echo "Command '{$command}' failed!<br >";
		return false;
	}
	echo "Copying new conf file ok.<br />";
*/

	echo "<h1>copy &amp paste this into root@db.adultsearch.com:/etc/sphinx/sphinx.dir.conf:</h1>\n";
	echo "<textarea style=\"width: 100%; height: 300px;\">".htmlspecialchars($conf)."</textarea>\n";
	echo "Done.\n";
}

function sphinxReindexDir() {
	/*
	echo "<strong>Reindexing dir</strong><br />";
	$command = "ssh adts@10.105.13.110 \"indexer dir --rotate\"";
	//echo "COMMAND= $command<br />";
	echo "<pre>";
	$line = system($command, $ret);
	echo "</pre><br />";
	if ($ret != 0) {
		echo "Command '{$command}' failed!<br >";
		return false;
	}
	echo "Index dir successfully re-indexed.<br />";
	*/
	echo "<h1>copy &amp paste this into root@db.adultsearch.com:</h1>\n";
	echo "<input type=\"text\" value=\"".htmlspecialchars("indexer -c /etc/sphinx/sphinx.conf --rotate dir")."\" style=\"width: 100%;\" /><br />\n";
	echo "Done.\n";
}

function memcachedFlush() {
	global $config_memcached_server, $config_memcached_port;

	echo "<strong>Flushing memcached instances</strong><br />";

	$mcs = array($config_memcached_server);

	foreach ($mcs as $mc) {
		$command = "echo 'flush_all' | ncat -i 1 {$mc} {$config_memcached_port}";
		echo "COMMAND= $command<br />";
		echo "<pre>";
		$line = system($command, $ret);
		echo "</pre><br />";
		if ($ret != 0) {
			echo "Command '{$command}' failed!<br >";
			return false;
		}
	}

	echo "All memcached instances succesfully flushed.<br />";
}


//echo mysql_client_encoding($db->link)."<br />";

switch(GetGetParam("action")) {
	case 'sphinx_refresh_conf':
		sphinxRefreshConf();
		break;
	case 'sphinx_reindex_dir':
		sphinxReindexDir();
		break;
	case 'memcached_flush':
		memcachedFlush();
		break;
	default:
		break;
}

?>
<h2>Unify DB</h2>
<a href="/mng/unify_db?action=sphinx_refresh_conf">refresh sphinx conf</a>
<br />
<a href="/mng/unify_db?action=sphinx_reindex_dir">reindex dir</a>
<br />
<a href="/mng/unify_db?action=memcached_flush">flush memcached instances</a>
<br />

