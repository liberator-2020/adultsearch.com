<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account,$db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) { 
    $account->asklogin(); 
    return false; 
}

if (!$account->isWorker() && !$account->isRealAdmin()) {
	echo "You dont have privileges to access this page!";
	die;
}

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols;

//configuration
$ipp = 30;
$url_last_piece = "videos";

function delete() {
    global $db, $classifieds;

    $live = ($_REQUEST["live"] == 1) ? true : false;
    $ids = getActionIds();
    $cnt = count($ids);

    if ($cnt == 0) {
        echo "No videos selected!<br />";
        return true;
    }

    if (!$live) {
        echo "<h1>Are you sure you want to delete these {$cnt} videos ?</h1>";

        echo "<table><tr><th>Id</th><th>Filename</th><th>Thumb</th><th>File</th></tr>";
		foreach ($ids as $id) {
			$video = new video($id);
			$video->fetch();
			$path = $video->getVideoAbsPath();
			$size = -1;
			if (file_exists($path))
				$size = filesize($path);
            echo "<tr><td>{$video->getFileId()}</td><td>{$video->getFilename()}</td><td>{$video->getThumb()}</td>";
			if ($size == -1)
				echo "<td>{$path} - <span style=\"color: red;\">File does not exist!</span></td>";
			else
				echo "<td>{$path} - {$size} B</td>";
			echo "</tr>";
        }
        echo "</table>";

		echo "<br /><span style=\"color: red; font-weight: bold;\">Videos will be deleted from disk ! (irreversible operation)</span><br /><br />\n";

        echo getConfirmActionForm("delete", $ids);
        return false;
    }

	//first delete files
	$files_deleted = 0;
	foreach ($ids as $id) {
		$video = new video($id);
		$video->fetch();
		$path = $video->getVideoAbsPath();
		if (file_exists($path)) {
			if (@unlink($path))
				$files_deleted++;
		}
	}
	$message = "<strong>{$files_deleted}</strong> files successfully deleted from disk.<br />";

	//then db entries
	$entries_deleted = 0;
	foreach ($ids as $id) {
		$db->q("DELETE FROM place_picture WHERE picture = ? LIMIT 1", array($id));
		$aff = $db->affected();
		if ($aff == 1)
			$entries_deleted++;
	}
	$message .= "<strong>{$entries_deleted}</strong> files successfully deleted from database.<br />";

	return actionSuccess($message);
}


$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'delete':
        if (!permission::has("video_delete"))
            die;
        $ret = delete();
        if (!$ret)
            return;
        break;
}

$order = getOrder();
$limit = getLimit();

//query db
$sql = "select count(*) as total from place_picture where place_file_type_id = 2";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT pp.*, ll.loc_name, ll.dir, ll.country_id 
		, (CASE WHEN pp.module = 'place' THEN p.place_id WHEN pp.module = 'review' THEN p2.place_id ELSE NULL END) as place_id
		, (CASE WHEN pp.module = 'place' THEN p.place_type_id WHEN pp.module = 'review' THEN p2.place_type_id ELSE NULL END) as place_type_id
		, (CASE WHEN pp.module = 'place' THEN p.loc_id WHEN pp.module = 'review' THEN p2.loc_id ELSE NULL END) as p_loc_id
		, (CASE WHEN pp.module = 'place' THEN p.name WHEN pp.module = 'review' THEN p2.name ELSE NULL END) as p_name
		, (CASE WHEN pp.module = 'place' THEN l1.country_id WHEN pp.module = 'review' THEN l2.country_id ELSE NULL END) as p_country_id
		, (CASE WHEN pp.module = 'place' AND (pp.loc_id is NULL OR pp.loc_id = 0) THEN l1c.dir WHEN pp.module = 'review' THEN l2c.dir ELSE llc.dir END) as country
		, a.username
		FROM place_picture pp 
		LEFT JOIN location_location ll on pp.loc_id = ll.loc_id 
		LEFT JOIN location_location llc on ll.country_id = llc.loc_id

		LEFT JOIN place p on pp.id = p.place_id
		LEFT JOIN location_location l1 on p.loc_id = l1.loc_id
		LEFT JOIN location_location l1c on l1.country_id = l1c.loc_id

		LEFT JOIN place_review pr on pp.id = pr.review_id
		LEFT JOIN place p2 on pr.id = p2.place_id
		LEFT JOIN location_location l2 on p2.loc_id = l2.loc_id
		LEFT JOIN location_location l2c on l2.country_id = l2c.loc_id

		LEFT JOIN account a ON a.account_id = pp.account_id

		WHERE place_file_type_id = 2 
		$order 
		$limit";

$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No videos.";
	return;
}

//pager
$pager = getPager($total);

//output
echo "<h2>Videos</h2>\n";
echo $pager;

echo "<form method=\"post\">";
if (permission::has("video_delete")) {
    echo "<select name=\"action\"><option value=\"\">-</option>";
    echo "<option value=\"delete\">Delete video(s)</option>";
    echo "</select>";
    echo "<input type=\"submit\" name=\"submit\" value=\"Execute...\" />";
}

echo getFilterFormFields();

echo "<table class=\"control\">";
echo "<thead><tr>
<th class=\"check\"><input type=\"checkbox\" name=\"master\" value=\"\" /></th>
<th>".getOrderLink("Id", "picture")."</th>
<th>Info</th>
<th>Type</th>
<th>Object</th>
<th>".getOrderLink("Name", "name")."</th>
<th>Description</th>
<th>".getOrderLink("Visible", "visible")."</th>
<th>".getOrderLink("#&nbsp;played", "count_play")."</th>
<th>Uploaded by</th>
<th></th>
</tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {

	if (intval($rox['loc_id']) > 0)
		$type = "location";
	else if ($rox['module'] == "review")
		$type = "review";
	else
		$type = "place";

	//create video object
	$video = video::withAll($rox["picture"], $rox["module"], $rox["loc_id"], $rox["country"], $rox["filename"], $rox["thumb"], $rox["name"], $rox["description"], $rox["visible"]);

	//object
	$object = "<span style=\"color: red; font-weight: bold;\">Error?</span>\n";
	if ($rox["place_id"] != NULL && $rox["place_type_id"] != NULL && $rox["p_loc_id"] != NULL && $rox["p_name"] != NULL && $rox["p_country_id"] != NULL) {
		//get place link
		$row = array("place_id" => $rox["place_id"], "place_type_id" => $rox["place_type_id"], "loc_id" => $rox["p_loc_id"], "name" => $rox["p_name"], "country" => $rox["country"]);
		$place_link = dir::getPlaceLink($row, true);
		$id = $rox['place_id'];
		$object = "<a href=\"{$place_link}\" target=\"_new\">".htmlspecialchars($rox["p_name"])."</a>";
	}

	if ($type == "location") {
		$id = $rox['loc_id'];
		$object = "<a href=\"http://{$row["country"]}.adultsearch.com/{$rox['dir']}\" target=\"_new\">".htmlspecialchars($rox['loc_name'])."</a>";
	}

	if ($video->isExternal()) {
		$info = "External";
	} else {
		$info = $video->getInfo();
	}

	$visible = "<img src=\"/images/control/".(($rox['visible'] == 1) ? "tick.png" : "cross.png")."\" />";

	$edit = "<a href=\"javascript:void(0)\" onclick=\"return win_popup('/ajax/files/{$rox['module']}/{$id}/edit/{$rox['picture']}', 'Edit extra file', 800, 650);\">Edit</a>";

	echo "<tr>";
	echo "<td class=\"check\"><input type=\"checkbox\" name=\"action_id[]\" value=\"{$rox['picture']}\" /></td>";

	echo "<td>{$rox['picture']}</td>";
	echo "<td>{$info}</td>";
    echo "<td>{$type}</td>";
    echo "<td>{$object}</td>";
    echo "<td>{$rox['name']}</td>";
    echo "<td style=\"max-width: 300px;\">{$rox['description']}</td>";
    echo "<td>{$visible}</td>";
    echo "<td>{$rox['count_play']}</td>";
    echo "<td>{$rox['username']}</td>";
    echo "<td>{$edit}</td>";
    echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;


?>
