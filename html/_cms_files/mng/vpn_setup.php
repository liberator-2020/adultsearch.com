<?php
require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");
global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

//we have to be logged in 
if (!$account->isMember()) {
    $account->asklogin();
    return false;
}

if (!$account->isadmin()) {
    echo "You dont have privileges to access this page!";
    die;
}

?>

<style type="text/css">
ol li {
	font-size: 1.1em;
	margin-bottom: 10px;
}
a {
	font-size: 1.1em;
}
</style>

<h1>How to post ads using VPN</h1>
<ol>
<li>
	Install free program OpenVPN on your computer following steps <strong>1-8</strong> (yes, only steps 1 to 8) in this guide: <a href="https://www.privateinternetaccess.com/pages/client-support/windows-openvpn" target="_blank">https://www.privateinternetaccess.com/pages/client-support/windows-openvpn</a>.</li>
<li>
	Download program VpnSwitch from <a href="/tmp/VpnSwitch/VpnSwitch.zip">here</a> and unpack the zip in any folder on your computer (it can be your desktop, Documents folder, or C:\ folder). I'm using C:\ folder:<br />
	<img src="/images/manual/vpn_switch_1.png" />
</li>
<li>
	Run program VpnSwitch double-clicking on file VpnSwitch.exe (you can also create shortcut on your desktop, if you want...)
</li>
<li>
	Whenever you want to post new ad with VPN, just click on first button "Switch VPN IP":<br />
	<img src="/images/manual/vpn_switch_2.png" />
</li>
<li>
	For a short while, while program is connecting to VPN server, you should see turning gears:<br />
	<img src="/images/manual/vpn_switch_3.png" />
</li>
<li>
	After a short while (15-20 seconds), you should hear a sound and there should be green checkmark and text "Connected to ..." in status bar:<br />
	<img src="/images/manual/vpn_switch_4.png" />
</li>
<li>
	After successful connection to VPN server, you can post an ad on Adultsearch (including paying for ad on CCBILL website)
</li>
<li>
	If you by any chance get following page:<br />
	<img src="/images/manual/vpn_check.png" /><br />
	probably you forgot to connect to VPN, or the VPN IP address has already been used recently to post an ad. Just try another VPN IP address simply by clicking again on button "Switch VPN IP" in application VpnSwitch.
</li>
<li>
	When you are finished with posting ads and/or you want to disconnect from VPN server (and again use your south-african IP address), simply close the VpnSwitch program by clicking on second button "Exit", or clicking on [X] button in title bar of VpnSwitch window:<br />
	<img src="/images/manual/vpn_switch_5.png" />
</li>
<li>
	If you get any error or have any questions, please contact jay
</li>
</ol>
