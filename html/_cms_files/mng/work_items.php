<?php

require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";

if (!permission::access("work_assign"))
    die("Invalid access");

global $ipp, $ordercol, $orderway, $url_last_piece, $filters, $where_cols;

//configuration

//TODO refaxctor this
if (!isset($_REQUEST["worker"]))
	$_REQUEST["worker"] = $account->getId();
if (!isset($_REQUEST["completed"]))
	$_REQUEST["completed"] = 0;

$filter_workers = ["" => "-"];
$res = $db->q("
	SELECT DISTINCT a.account_id, a.username, a.email 
	FROM account a 
	INNER JOIN work_item i on i.account_id = a.account_id
	ORDER BY a.email ASC
	");
while($row = $db->r($res)) {
	if ($account->getId() == $row["account_id"])
		$worker_label = "Me ({$row["email"]})";
	else
		$worker_label = "{$row["email"]}";
	$filter_workers[$row["account_id"]] = $worker_label;
}

$ipp = 30;
$url_last_piece = "work_items";
$where_cols = array(
	"id" => array("title" => "ID", "where" => "i.id", "match" => "exact", "size" => "7"),
	"worker" => array("title" => "Worker", "where" => "i.account_id", "type" => "select", "options" => $filter_workers),
	"module" => array("title" => "Module", "where" => "i.module", "type" => "select", "options" => array("" => "-", "eroticmassage" => "Erotic Massage Parlors")),
	"completed" => array("title" => "Completed", "where" => "i.completed", "type" => "select", "options" => array("" => "-", "0" => "No", "1" => "Yes")),
);


function add() {
	global $db, $account;

	if (!$account->isrealadmin()) {
		echo "This feature is not enabled for you.<br />\n";
		return true;
	}

	if (isset($_REQUEST["submit"])) {

		$module = $_REQUEST["module1"];
		$worker = intval($_REQUEST["worker1"]);
		$with_image = intval($_REQUEST["with_image"]);
		
		if (!in_array($module, ["eroticmassage"]))
			echo "Error: Invalid module '{$module}'!<br />\n";
		else if (!$worker)
			echo "Error: worker not specified!<br />\n";
		else {

			$with_image_sql = "";
			if ($with_image) {
				echo "With Images<br />";
				$with_image_sql = " AND t.image IS NOT NULL AND t.thumb IS NOT NULL ";
			} else {
				echo "Without Images<br />";
				$with_image_sql = " AND ( t.image IS NULL OR t.thumb IS NULL ) ";
			}
			$res = $db->q("
				SELECT t.id 
				FROM {$module} t
				INNER JOIN location_location l on l.loc_id = t.loc_id
				WHERE t.deleted IS NULL AND l.country_id IN (16046, 16047, 41973) {$with_image_sql} 
				ORDER BY l.country_id ASC, l.loc_parent ASC, l.loc_name ASC");
			$cnt = 0;
			$now = time();
			while ($row = $db->r($res)) {
				$cnt++;
				$res2 = $db->q("
					INSERT INTO work_item 
					(module, module_id, created_stamp, account_id, completed, completed_stamp, orden)
					VALUES
					(?, ?, ?, ?, 0, NULL, ?)
					",
					[$module, $row["id"], $now, $worker, $cnt]
					);
			}
			echo "{$cnt} places inserted.<br />\n";
		}
	} else {
//		$with_image = 0;
	}

	$modules = [
		"eroticmassage" => "Erotic Massage Parlors",
		];

	$workers = [];
	$res = $db->q("
		SELECT a.account_id, a.username, a.email 
		FROM account a 
		WHERE ( a.worker = 1 OR a.account_level > 2 ) AND a.deleted IS NULL 
		ORDER BY a.email ASC
		");
	while($row = $db->r($res)) {
		$workers[$row["account_id"]] = $row["email"];
	}

	echo "<h2>Add new work batch</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Module: </th><td><select name=\"module1\"><option value=\"\">-- Select --</option>";
	foreach ($modules as $val => $label) {
		echo "<option value=\"{$val}\">{$label}</option>";
	}
	echo "</select></td></tr>\n";
	echo "<tr><th>Worker: </th><td><select name=\"worker1\"><option value=\"\">-- Select --</option>";
	foreach ($workers as $val => $label) {
		echo "<option value=\"{$val}\">{$label}</option>";
	}
	echo "</select></td></tr>\n";
	$with_image_checked = "";
	if ($with_image)
		$with_image_checked = " checked=\"checked\" ";
	echo "<tr><th colspan=\"2\"><input type=\"checkbox\" name=\"with_image\" value=\"1\" {$with_image_checked} /> with_images</th></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Add\" /></td></tr>\n";
	echo "</table>";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}



function edit() {
	global $account;

	$account_id = intval($_REQUEST["action_id"]);
	if ($account_id == 0)
		return actionError("Invalid account id !");

	$acc = account::findOnebyId($account_id);
	if ($acc == false)
		return actionError("Cant find account of id={$account_id} !");

	if ($acc->getAccountLevel() >= $account->getAccountLevel())
		return actionError("You dont have privileges to edit this user (account_id={$account_id}) !");

	if (isset($_REQUEST["submit"])) {
		$updated = "";
		if (isset($_REQUEST["u_notes"])) {
			$acc->setNotes($_REQUEST["u_notes"]);
			$updated .= " notes";
		}
		if (isset($_REQUEST["u_password"])) {
			$password = trim($_REQUEST["u_password"]);
			if ($password) {
				$acc->setPassword($password);
				$updated .= " password";
			}
		}
		$acc->setWorker(intval($_REQUEST["u_worker"]));
		$acc->setAdvertiser(intval($_REQUEST["u_advertiser"]));
		$ret = $acc->update();
		if ($ret)
			return actionSuccess("You have successfully updated ({$updated}) account id #{$account_id}.");
		else
			return actionError("Error while updating account id #{$account_id}.");			
	}

	//TODO put edit functionality into account class ??
	echo "<h2>Edit account #{$acc->getId()} - {$acc->getEmail()}</h2>";
	echo "<form method=\"post\" action=\"\">";
	echo "<table>";
	echo "<tr><th>Account ID</th><td>{$acc->getId()}</td></tr>\n";
	echo "<tr><th>Email</th><td>{$acc->getEmail()}</td></tr>\n";
	echo "<tr><th>Password</th><td><input type=\"text\" name=\"u_password\" size=\"20\" value=\"\" /><small>Note: if left blank, password is not going to be changed.</small></td></tr>\n";
	echo "<tr><th>Notes</th><td><textarea name=\"u_notes\" cols=\"80\" rows=\"6\">".htmlspecialchars($acc->getNotes())."</textarea></td></tr>\n";
	$worker_checked = "";
	if ($acc->isWorker())
		$worker_checked = " checked=\"checked\" ";
	echo "<tr><th colspan=\"2\"><input type=\"checkbox\" name=\"u_worker\" {$worker_checked} value=\"1\" /> Worker</th></tr>\n";
	$advertiser_checked = "";
	if ($acc->isAdvertiser())
		$advertiser_checked = " checked=\"checked\" ";
	echo "<tr><th colspan=\"2\"><input type=\"checkbox\" name=\"u_advertiser\" {$advertiser_checked} value=\"1\" /> Advertiser</th></tr>\n";
	echo "<tr><th></th><td><input type=\"submit\" name=\"submit\" value=\"Submit\" /></td></tr>\n";
	echo "</table>";
	echo "<input type=\"hidden\" name=\"action_id\" value=\"{$account_id}\" />";
	echo getFilterFormFields();
	echo "</form>";
	return false;
}	


$filters = getFilters();

switch ($_REQUEST["action"]) {
	case 'add':
		$ret = add();
		if (!$ret)
			return;
		break;
	case 'edit':
		if (!permission::has("account_edit"))
			die;
		$ret = edit();
		if (!$ret)
			return;
		break;
	default:
		break;
}

$where = getWhere();
$order = getOrder();
$limit = getLimit();

//TODO add default order
if (empty($order))
	$order = "ORDER BY i.id ASC";

//query db
$sql = "SELECT count(*) as total 
		FROM work_item i
		$where";
$res = $db->q($sql);
$row = $db->r($res);
$total = $row['total'];

$sql = "SELECT i.*
			, a.username, a.email
			, e.name as ename, CONCAT(el.loc_name, ', ',el.s) as elocation
		FROM work_item i
		LEFT JOIN account a on a.account_id = i.account_id
		LEFT JOIN eroticmassage e on e.id = i.module_id
		LEFT JOIN location_location el on el.loc_id = e.loc_id
		$where
		GROUP BY i.id
		$order
		$limit";
$res = $db->q($sql);
if ($db->numrows($res) == 0) {
	echo "No work items.";
}

//pager
$pager = getPager($total);

//output
echo "<h2>Work Items</h2>\n";
displayFilterForm();

if ($account->isrealadmin()) {
	echo "<a href=\"?action=add\">add</a><br />";
}

echo $pager;
echo "<table class=\"control\">";
echo "<thead><tr>
<th>".getOrderLink("Id", "id")."</th>
<th>".getOrderLink("Module", "module")."</th>
<th>".getOrderLink("Place Id", "module_id")."</th>
<th>Name</th>
<th>Location</th>
<th>".getOrderLink("Created", "created_stamp")."</th>
<th>".getOrderLink("Worker", "account_id")."</th>
<th>".getOrderLink("Completed", "completed_stamp")."</th>
<th/>
</tr></thead>\n";
echo "<tbody>";
while ($rox = $db->r($res)) {
	echo "<tr>";

	echo "<td>{$rox['id']}</td>";
	
	$module_label = "";
	switch ($rox['module']) {
		case "eroticmassage": $module_label = "Erotic Massage Parlor"; break;
		default: break;
	}
	echo "<td>{$module_label}</td>";
	echo "<td>{$rox['module_id']}</td>";

	$name = $location = "";
	switch ($rox['module']) { 
		case "eroticmassage": $name = $rox["ename"]; $location = $rox["elocation"]; break;
		default: break;
	}
	echo "<td>{$name}</td>";
	echo "<td>{$location}</td>";

	echo "<td>".date("m/d/Y H:i:s", $rox['created_stamp'])."</td>";
	echo "<td>{$rox['username']}</td>";
	echo "<td>";
	if ($rox['completed']) {
		echo "YES (".date("m/d/Y H:i:s", $rox['completed_stamp']).")";
	} else {
		echo "<span style=\"color: darkgrey;\">No</span>";
	}
	echo "</td>";

	$edit_link = "";
	switch($rox['module']) {
		case "eroticmassage": $edit_link = "//adultsearch.com/worker/emp?id={$rox['module_id']}"; break;
		default: break;
	}
	echo "<td>";
	echo "<a href=\"{$edit_link}\" target=\"_blank\">edit</a>";
	echo "</td>";

	echo "</tr>\n";
}
echo "</tbody>";
echo "</table>";
echo $pager;

