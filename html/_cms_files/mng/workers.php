<?php
require_once(_CMS_ABS_PATH."/_cms_files/mng/common.php");

global $account, $db, $gIndexTemplate;
$gIndexTemplate = "admin_index.tpl";
    
//we have to be logged in
if (!$account->isMember()) {
	$account->asklogin();
	return false;
}

if (!$account->isadmin())
    die("Invalid access");

function worker_stats() {
	global $db;

	list($account_id, $username) = explode("|", GetGetParam("worker"));
	$account_id = intval($account_id);
	$username = htmlspecialchars($username);
	$month = intval(GetGetParam("month"));
	$year = intval(GetGetParam("year"));

	//get files stats
	$sql = "select count(*) as cnt from place_picture where account_id = '{$account_id}'";
    $res = $db->q($sql);
    if ($db->numrows($res) != 1) {
        echo "Error: ".$db->numrows($res)." while executing sql '{$sql}' !<br />";
        return;
    }
	$row = $db->r($res);
	$files_cnt = $row["cnt"];

	//get places stats
	$place_stats = array();
	//TODO

	//get forum stats
	$sql = "select count(*) as cnt from forum_post where account_id = '{$account_id}'";
    $res = $db->q($sql);
    if ($db->numrows($res) != 1) {
        echo "Error: ".$db->numrows($res)." while executing sql '{$sql}' !<br />";
        return;
    }
    $row = $db->r($res);
    $forum_post_cnt = $row["cnt"];
	$sql = "select count(*) as cnt from forum_topic where account_id = '{$account_id}'";
    $res = $db->q($sql);
    if ($db->numrows($res) != 1) {
        echo "Error: ".$db->numrows($res)." while executing sql '{$sql}' !<br />";
        return;
    }
    $row = $db->r($res);
    $forum_topic_cnt = $row["cnt"];


	echo "<br /><h2>Statistics for worker '{$username}' (account_id={$account_id})</h2>";
	
	echo "Places statistics:<br />";
	foreach ($place_stats as $key => $val) {
		echo "&nbsp;&nbsp;{$key}: <strong>{$val}</strong><br />";
	}
	echo "<br />";

	echo "Files (additional images, videos) upload count: <strong>{$files_cnt}</strong><br /><br />";
	
	echo "Forum statistics:<br />";
	echo "&nbsp;&nbsp;Topics opened: <strong>{$forum_topic_cnt}</strong><br />";
	echo "&nbsp;&nbsp;Forum posts written: <strong>{$forum_post_cnt}</strong><br />";


}


//get workers
$res=$db->q("select account_id, username from account where worker = 1 order by username asc");
$workers = array();
while ($row = $db->r($res)) {
    $workers[$row['account_id']."|".$row["username"]] = $row["username"];
}

switch(GetGetParam("action")) {
	case 'worker_stats':
		worker_stats();
		break;
	default:
		break;
}

?>
<h2>Workers</h2>
<form action="" method="get">
<input type="hidden" name="action" value="worker_stats" />
Show stats for worker
<select name="worker" />
<?php foreach ($workers as $key => $val) echo "<option value=\"{$key}\">{$val}</option>\n"; ?>
</select>
for period
<select name="month">
	<option value="1">January</option>
	<option value="2">February</option>
	<option value="3">March</option>
	<option value="4">April</option>
	<option value="5">May</option>
	<option value="6">June</option>
	<option value="7">July</option>
	<option value="8">August</option>
	<option value="9">September</option>
	<option value="10">October</option>
	<option value="11">November</option>
	<option value="12">December</option>
</select>
<select name="year">
<?php for ($i = intval(date("Y")); $i >= 2007; $i--) echo "<option value=\"{$i}\">{$i}</option>"; ?>
</select><br />
<input type="submit" name="submit" value="submit" />
</form>
