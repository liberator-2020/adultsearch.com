<?php
defined("_CMS_FRONTEND") or die("No Access");

global $account, $smarty, $db;
$smarty->assign("nobanner", true);
$ccbill = new ccbill();

$ipaddress = account::getUserIp();
file_log("ccbill", "Checkip, ip='{$ipaddress}");


if ($_REQUEST["submit"] == "Check again") {
	//check if ip address is one of vpn addressess or office addresses
	if ($ccbill->checkip())
		return true;	//ip is ok, we should have been redirected to ccbill by now
	//ip already used in past, continue and display "change ip" page
} else if ($_REQUEST["submit"] == "Go back") {
	$ccbill->afterPurchase(false);
	return true;
}



//------------------------
//display "change ip" page
$res = $db->q("SELECT s.id, h.name FROM vpn_server s INNER JOIN vpn_host h on s.host_id = h.id WHERE s.ip_address = ?", array($ipaddress));
if ($row = $db->r($res)) {
	$arr = explode(".", $row["name"]);
	$smarty->assign("vpn_host", $row["id"]."-".$arr[0]);
}
//$smarty->assign("vpn_host", "12-us-california");
$smarty->assign("ipaddress", $ipaddress);

$smarty->display(_CMS_ABS_PATH."/templates/payment/checkip.tpl");
return;
?>
