<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

$smarty->assign("nobanner", true);

//we need to be logged in
if (!($account_id=$account->isLoggedIn())) {
	$account->asklogin();
	return;
}

global $phone, $email;
$phone = $email = null;
function get_phone_email() {
	global $phone, $email, $account, $db;

	$payment_id = intval($_REQUEST["id"]);
	if (!$payment_id)
		return;

	$account_id = $account->getId();
	//$account_id = 199929;
	$res = $db->q("SELECT p.p1, p.p3 FROM payment p WHERE p.account_id = ? AND p.id = ?", [$account_id, $payment_id]);
	if (!$db->numrows($res))
		return;

	$row =  $db->r($res);
	if ($row["p1"] == "classifieds") {
		$clad_id = $row["p3"];
	} else {
		$res = $db->q("SELECT pi.classified_id FROM payment_item pi WHERE pi.payment_id = ? and pi.classified_id IS NOT NULL", [$payment_id]);
		if (!$db->numrows($res))
			return;
		$row = $db->r($res);
		$clad_id = $row["classified_id"];
	}

	$res = $db->q("SELECT c.phone, c.email FROM classifieds c WHERE c.id = ?", [$clad_id]);
	if (!$db->numrows($res))
		return;
	$row = $db->r($res);
	$phone = makeProperPhoneNumber($row["phone"]);
	$email = $row["email"];
}
get_phone_email();

//_d("phone='{$phone}', email='{$email}'");
$smarty->assign("phone", $phone);
$smarty->assign("email", $email);

return $smarty->display(_CMS_ABS_PATH."/templates/payment/done.tpl");
?>
