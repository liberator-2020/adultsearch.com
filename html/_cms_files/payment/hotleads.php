<?php
/**
 * This script is called from hotleads.com for creating ad purchase and redirecting to ccbill to pay for the ad
 * It might create payment for existing ad or create completely new ad
 */
die("OK");
/*
defined('_CMS_FRONTEND') or die('Restricted access');

global $db;

//get params from request
$clad_payment_id = $_REQUEST["clad_payment_id"];
$firstname = $_REQUEST["firstname"];
$lastname = $_REQUEST["lastname"];
$address = $_REQUEST["address"];
$city = $_REQUEST["city"];
$state = $_REQUEST["state"];
$zip = $_REQUEST["zip"];
$country = $_REQUEST["country"];
$ref = $_REQUEST["ref"];

if (!$ref) {
	debug_log("payment/hotleads: error: ref not specified - this should not happen!");
	//this needs to be neutral so if somebody calls directly this url nothing bad happens...
	system::go("http://adultsearch.com");
}

//basic validate
if (!$clad_payment_id || !$firstname || !$lastname || !$address || !$city || !$zip || !$country) {
	debug_log("payment/hotleads: error: some mandatory param not specified - this should not happen! REQ=".print_r($_REQUEST, true));
	$return_url = $ref."?error=".urlencode("some mandatory param not specified");
	return system::go($return_url);
}

//fetch classified payment
$res = $db->q("
	SELECT cp.*, a.email
	FROM classifieds_payment cp
	INNER JOIN account a on a.account_id = cp.account_id
	WHERE cp.id = ? LIMIT 1
	", 
	[$clad_payment_id]
	);
if ($db->numrows($res) != 1) {
	debug_log("payment/hotleads: error: can't find classifieds_payment in db: clad_payment_id='{$clad_payment_id}'");
	return system::go($ref);
}
$row = $db->r($res);

$clad_id = $row["post_id"];
$email = $row["email"];
$total = $row["total"];
$account_id = $row["account_id"];

//testing
$total=2.99;

//prepare params
$params = [
	"firstname" => $firstname,
	"lastname" => $lastname,
	"address" => $address,
	"city" => $city,
	"state" => $state,
	"zip" => $zip,
	"country" => $country,
	"email" => $email,
	"total" => $total,
	"account_id" => $account_id,
	];
if ($row["recurring"] == 1) {
	$params["recurring_period"] = 30;
	$params["recurring_amount"] = 49.99;
	//testing
	//$params["recurring_amount"] = 1.99;
}

//prepare ccbil object
$ccbill = new ccbill();
$ccbill->total = $total;
$ccbill->no_promotion = true;
$ccbill->item_id = $clad_id;
$ccbill->what = "classifieds";
$ccbill->p1 = "classifieds";
$ccbill->p2 = "hotleads";
$ccbill->p3 = $clad_id;

//charge
file_log("ccbill", "payment:hotleads Going to charge via ccbill: clad_id={$clad_id}, total={$total}, params=".print_r($params, true));
$error = "";
$ret = $ccbill->charge($params, $error);

//processing error - redirect back to hotleads with error in URL param
$return_url = $ref."?error=".urlencode($error);
system::go($return_url);
return;
*/
?>
