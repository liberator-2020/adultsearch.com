<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

$smarty->assign("nobanner", true);

//we need to be logged in
if (!($account_id=$account->isLoggedIn())) {
	$account->asklogin();
	return;
}

$payment_id = intval($_REQUEST["id"]);
if (!$payment_id)
	system::go("/");

$payment = new payment();
$ret = $payment->pay($payment_id);

if ($ret)
	system::go("/payment/successful?id={$payment_id}");	//payment successful

return;
?>
