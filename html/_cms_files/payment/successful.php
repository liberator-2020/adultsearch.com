<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

$smarty->assign("nobanner", true);

//we need to be logged in
if (!($account_id=$account->isLoggedIn())) {
	$account->asklogin();
	return;
}

$payment_id = intval($_REQUEST["id"]);
if (!$payment_id)
	system::go("/");

//look up payment
$res = $db->q("SELECT * FROM payment WHERE id = ?", [$payment_id]);
if ($db->numrows($res) != 1)
	system::go("/");

$row = $db->r($res);
$ai = $row["account_id"];
$result = $row["result"];
$amount = $row["amount"];
$recurring_amount = $row["recurring_amount"];

//check if we are allowed to see this payment page
if (!$account->isAdmin() && $account_id != $ai)
	system::go("/");

//check if payment was successful
if ($result != "A") {
	reportAdmin("AS: payment/successful error", "Payment #{$payment_id} was not successful, what are we doing on this page?");
	$smarty->assign("error", "Payment was not successful");
	return $smarty->display(_CMS_ABS_PATH."/templates/payment/successful.tpl");
}

//export some payment data
if ($amount)
	$amount = number_format($amount, 2, ".", "");
if ($recurring_amount)
	$recurring_amount = number_format($recurring_amount, 2, ".", "");
$smarty->assign("amount", $amount);
$smarty->assign("recurring_amount", $recurring_amount);


//-------------------
//check payment items
$ads = [];
$res = $db->q("SELECT DISTINCT pi.classified_id FROM payment_item pi WHERE pi.payment_id = ? AND pi.type = 'classified'", [$payment_id]);
while ($row = $db->r($res)) {
	$clad_id = $row["classified_id"];
	//get additional info about ad
	$res2 = $db->q("SELECT c.type, c.thumb FROM classifieds c WHERE c.id = ?", [$clad_id]);
	if ($db->numrows($res2) == 0)
		continue;
	$row2 = $db->r($res2);
	$ads[] = [
		"id" => $clad_id,
		"thumb" => $row2["thumb"],
		];
}
$smarty->assign("ads", $ads);

$items = [];
$res = $db->q("SELECT pi.* FROM payment_item pi WHERE pi.payment_id = ? AND pi.type <> 'classified'", [$payment_id]);
while ($row = $db->r($res)) {
	$type = $row["type"];
	$items[] = [
		"type" => $type,
		];
}
$smarty->assign("items", $items);

return $smarty->display(_CMS_ABS_PATH."/templates/payment/successful.tpl");
?>
