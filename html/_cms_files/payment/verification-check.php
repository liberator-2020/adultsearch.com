<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $config_image_path;

if (!account::ensure_loggedin())
	die("Invalid access");

$now = time();


$verification_id = intval($_REQUEST["verification_id"]);
if (!$verification_id)
	die("verification_id not specified !");
$res = $db->q("SELECT * FROM verification WHERE id = ? LIMIT 1", [$verification_id]);
if (!$db->numrows($res))
	die("Can't find verification #{$verification_id} !");
$row = $db->r($res);
$account_id = $row["account_id"];
$filename = $row["filename"];
$status = $row["status"];

//check if we are authorized to do this
if ($account_id != $account->getId() && !$account->is_admin())
	die("Unable to check this video");

if (!$filename)
	return json_response(["status" => 1]);

$path = null;
if ($status == 0) {
	//TODO do this as a runner from crontab ???
	//$ret = process_videos();
	$ret = process_videos($verification_id);
	if (!$ret)
		error_log("payment/verification-check: process_videos() failed");
}

//re-read from db
$res = $db->q("SELECT * FROM verification WHERE id = ? LIMIT 1", [$verification_id]);
if (!$db->numrows($res))
	die("Can't find verification #{$verification_id} !");
$row = $db->r($res);
$account_id = $row["account_id"];
$filename = $row["filename"];
$thumbnail = $row["thumbnail"];
$status = $row["status"];
$path = ($status == 2 && $filename) ? "//img.adultsearch.com/verification/{$filename}" : null;
$thumb_path = ($status == 2 && $thumbnail) ? "//img.adultsearch.com/verification/{$thumbnail}": null;

return json_response(["status" => $status, "path" => $path, "thumb_path" => $thumb_path]);

?>
