<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $config_image_path;

if (!account::ensure_loggedin())
	die("Invalid access");


$verification_id = intval($_REQUEST["verification_id"]);
if (!$verification_id)
	die("verification_id not specified !");
$res = $db->q("SELECT * FROM verification WHERE id = ? LIMIT 1", [$verification_id]);
if (!$db->numrows($res))
	die("Can't find verification #{$verification_id} !");
$row = $db->r($res);
$account_id = $row["account_id"];
$filename = $row["filename"];
$thumbnail = $row["thumbnail"];
$status = $row["status"];

//check if we are authorized to do this
if ($account_id != $account->getId() && !$account->is_admin())
	die("Unable to check this video");


file_log("pay", "payment/verification-delete: verification_id={$verification_id}, account_id={$account_id}");

$video_filepath = $config_image_path."/verification/{$filename}";
$thumbnail_filepath = $config_image_path."/verification/{$thumbnail}";

if (!file_exists($video_filepath)) {
	if ($verification->getStatus() == 3) {
		file_log("pay", "payment/verification-delete: video is already deleted");
		die("OK");
	} else {
		file_log("pay", "payment/verification-delete: video is already deleted, but status is not yet 3 ? - updating verification status");
		reportAdmin("AS: payment/verification-delete: error", "File not on disk, but status in db is not deleted ??", [
			"verification_id" => $verification_id,
			"filename" => $filename,
			"thumbnail" => $thumbnail,
			"video_filepath" => $video_filepath,
			"thumbnail_filepath" => $thumbnail_filepath,
			"status" => $status,
			]);
		$db->q("UPDATE verification SET status = 3 WHERE id = ? LIMIT 1", [$verification_id]);
		die("OK");
	}
}

//delete from disk
$last_line = system("rm '{$video_filepath}'", $ret);
if ($ret) {
	file_log("pay", "payment/verification-delete: error deleting video file from disk, video_filepath='{$video_filepath}', last_line='{$last_line}', retval='{$ret}'");
	$this->get("system")->reportAdmin("TSE: payment/verification-delete: error deleting file from disk", "", [
		"verification_id" => $verification_id,
		"filename" => $filename,
		"thumbnail" => $thumbnail,
		"video_filepath" => $video_filepath,
		"thumbnail_filepath" => $thumbnail_filepath,
		"status" => $status,
		"last_line" => $last_line,
		"retval" => $ret,
		]);
	die("Error, please contact support@adultsearch.com");
}

$db->q("UPDATE verification SET status = 3 WHERE id = ? LIMIT 1", [$verification_id]);

//delete thumbnail
if (file_exists($thumbnail_filepath))
	@system("rm '{$thumbnail_filepath}'", $ret);

file_log("pay", "payment/verification-delete: video deleted, verification_id={$verification_id}");
die("OK");
?>
