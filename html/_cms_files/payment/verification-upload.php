<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $config_image_path;
$gTitle = "Video Verification";

if (!account::ensure_loggedin())
	die("Invalid access");

$now = time();

$payment = $clad_id = null;
//get payment and then clad
$payment_id = intval($_REQUEST["payment_id"]);
if ($payment_id) {
	$payment = $db->single("SELECT p.id FROM payment p WHERE p.id = ?", [$payment_id]);
	if ($payment) {
		$clad_id = anet::get_payment_classified_id($payment_id);
	}
}

//target video folder
$tgt_path = $config_image_path."/verification/uploaded";
if (!is_dir($tgt_path))
	mkdir($tgt_path, 0755, true);

//upload handler uploads video file(s) into tmp folder: _CMS_ABS_PATH."/tmp/vid"
$handler = new uploadhandler();
$handler->initialize("video");
$response = $handler->get_response();
if (is_array($response) && array_key_exists("video", $response) && count($response["video"]) > 0) {
	$items = $response["video"];
}

foreach ($items as $file) {
	file_log("pay", "jay: file");
	if (property_exists($file, "error") && $file->error) {
		//this is error, for example image does not meet minimum width/height requirements
		file_log("pay", "jay error: '{$file->error}'");
		$files[] = $file;
		continue;
	}

	$filename = $file->name;
	$file->path = "/tmp/vid/{$file->name}"; //url path
	$filepath = _CMS_ABS_PATH."/tmp/vid/".$filename;	//absolute path on disk

	//assure we have correct mime type
	$mime_type = mime_content_type($filepath);
	if (substr($mime_type, 0, 6) != "video/") {
		file_log("pay", "jay: Invalid file mime type: mime_type='{$mime_type}', type='{$type}' !");
		unlink($filepath);
		$file->error = "Invalid file type";
		$files[] = $file;
		continue;
	}

	file_log("pay", "jay: filename='{$filename}', filepath='{$filepath}', mime_type='{$mime_type}'");


	//succesfully uploaded video file
	file_log("pay", "jay: ProfileController:upload: video uploaded, filename='{$filename}'");
	unset($file->deleteUrl);
	unset($file->deleteType);
	unset($file->url);
	unset($file->path);

	//make new filename character safe
	$new_filename = "{$account->getId()}_{$filename}";
	$new_filename = preg_replace('/[^a-zA-Z0-9_\-\.]/', '', preg_replace('/ /', '_', $new_filename));
	$tgt_filepath = "{$tgt_path}/{$new_filename}";
	$ret = rename($filepath, $tgt_filepath);
	if (!$ret) {
		file_log("pay", "can't move uploaded video '{$filepath}' to temp folder '{$tgt_filepath}' !");
	} else {
		//create image entry in DB
		$width = (property_exists($file, "width") && intval($file->width)) ? intval($file->width) : null;
		$height = (property_exists($file, "height") && intval($file->height)) ? intval($file->height) : null;
		$public_verification = ($_REQUEST["public_verification"] == "1") ? 1 : null;

		$res = $db->q("INSERT INTO verification
			(account_id, filename, thumbnail, width, height, status, error, created_stamp, created_by)
			VALUES
			(?, ?, ?, ?, ?, ?, ?, ?, ?)",
			[$account->getId(), $new_filename, $thumbnail, $width, $height, 0, null, $now, $account->getId()]
			);
		$verification_id = $db->insertid($res);
		if (!$verification_id) {
			file_log("pay", "verification-upload: Error can't insert video into db: account_id={$account->getId()}, filename={$filename}, payment_id={$payment_id}, width={$width}, height={$height} !");
			reportAdmin("AS: verification-upload: Error", "can't insert video into db: account_id={$account->getId()}, filename={$filename}, payment_id={$payment_id}, width={$width}, height={$height} !");
			//TODO
			die;
		}
		$file->verification_id = $verification_id;
		file_log("pay", "verification-upload: Video #{$verification_id} uploaded successfully.");

		if ($clad_id) {
			$db->q("UPDATE classifieds SET verification_id = ? WHERE id = ? LIMIT 1", [$verification_id, $clad_id]);
		}
	}

	$files[] = $file;
}

$response = array("files" => $files);
header('Content-Type: application/json');
die(json_encode($response));

?>
