<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle;
$gTitle = "Video Verification";

if (!account::ensure_loggedin())
	die("Invalid access");

$payment = $clad_id = null;

//get payment and then clad
$payment_id = intval($_REQUEST["id"]);
if ($payment_id) {
	$payment = $db->single("SELECT p.id FROM payment p WHERE p.id = ?", [$payment_id]);
	if ($payment) {
		$clad_id = anet::get_payment_classified_id($payment_id);
	}
}

if (isset($_REQUEST["submit"])) {
	file_log("pay", "payment/verification: submitted");
	system::go("/payment/done?id={$payment_id}");
}

$smarty->assign("nobanner", true);
$smarty->assign("jquery_file_upload", true);
$smarty->assign("flowplayer", true);
$smarty->assign("payment_id", $payment_id);
$smarty->assign("clad_id", $clad_id);
$smarty->display(_CMS_ABS_PATH."/templates/payment/verification.tpl");

?>
