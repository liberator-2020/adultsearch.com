<?php
/*
 * Private messages - Block user
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

if (!$account->isloggedin()) {
	$account->asklogin();
	return;
}

$id = intval($_REQUEST["id"]);
if (!$id)
	return error_redirect("Invalid id.", "/pm/");

if ($id == $account->getId())
	return error_redirect("Invalid id.", "/pm/");

$block_account = account::findOneById($id);
if (!$block_account)
	return error_redirect("Invalid id.", "/pm/");

//check if block account is not already in our block list
$res = $db->q("SELECT * FROM message_block WHERE account_id = ? AND blocked_id = ?", array($account->getId(), $block_account->getId()));
if ($db->numrows($res) > 0) {
	//user already blocked, show success message
	return success_redirect("User ".$block_account->getUsername()." blocked.", "/pm/");
}

//insert block into db
$res = $db->q("INSERT INTO message_block
				(account_id, blocked_id, blocked_stamp, reason)
				VALUES
				(?, ?, ?, ?)",
				array($account->getId(), $block_account->getId(), time(), NULL)
				);
$block_id = $db->insertid($res);
if ($block_id)
	return success_redirect("User ".$block_account->getUsername()." blocked.", "/pm/");
else
	return error_redirect("Error while blocking user ".$block_account->getUsername().".", "/pm/");

die;

?>
