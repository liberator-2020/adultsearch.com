<?php
/*
 * Private messages - Blocked users list
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

if (!$account->isloggedin()) {
	$account->asklogin();
	return;
}

//select blocked users from db
$res = $db->q("
	SELECT b.blocked_id, b.blocked_stamp, a.username
	FROM message_block b
	INNER JOIN account a on a.account_id = b.blocked_id
	WHERE b.account_id = ?
	ORDER BY b.id ASC",
	array($account->getId()));
$blocked = [];
while ($row = $db->r($res)) {
	$blocked[] = array(
		"id" => $row["blocked_id"],
		"stamp" => $row["blocked_stamp"],
		"username" => $row["username"],
		);
}
$smarty->assign("blocked", $blocked);

escortsbiz::exportEbizLink();

$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/pm/blocked.tpl");

?>
