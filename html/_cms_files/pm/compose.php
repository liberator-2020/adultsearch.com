<?php
/*
 * Private messages - Compose
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

if (!account::ensure_loggedin())
	die("Invalid access!");

if (!$account->isPhoneVerified()) {
	// anti spam measure, we need to have verified phone to be able to send PM
	// (fuckers were creating hundreds accounts and spamming all review posters with PMs (that send out emails)
	escortsbiz::exportEbizLink();
	$smarty->assign("nobanner", true);
	$smarty->assign("phone_not_verified", true);
	$smarty->display(_CMS_ABS_PATH."/templates/pm/compose.tpl");
	return;
}

//search for recipient
$recipient_id = intval($_REQUEST["recipient"]);
$recipient = account::findOneById($recipient_id);
if (!$recipient) {
	$smarty->assign("error", "Can't find recipient #".htmlspecialchars($recipient_id)." !");
	$smarty->display(_CMS_ABS_PATH."/templates/pm/compose.tpl");
	return;
}
if ($recipient->getDeleted()) {
	$smarty->assign("error", "You can't send message to this recipient !");
	$smarty->display(_CMS_ABS_PATH."/templates/pm/compose.tpl");
	return;
}
//check if recipient did not block author - TODO - put this to message class
$res = $db->q("SELECT id FROM message_block WHERE account_id = ? AND blocked_id = ?", array($account->getId(), $recipient->getId()));
if ($db->numrows($res) > 0) {
	$smarty->assign("error", "User ".$recipient->getUsername()." doesn't want to receive any messages from you.");
	$smarty->display(_CMS_ABS_PATH."/templates/pm/compose.tpl");
	return;
}

$smarty->assign("recipient_id", $recipient_id);
$smarty->assign("recipient", htmlspecialchars($recipient->getUsername()));

//replying to check
$conversation_id = NULL;
$reply = intval($_REQUEST["reply"]);
if ($reply) {
	$reply_message = message::findOneById($reply);
	if ($reply_message && $reply_message->getAuthorId() == $recipient_id) {
		$conversation_id = ($reply_message->getConversationId()) ? $reply_message->getConversationId() : $reply_message->getId();
	}
}
$smarty->assign("reply", $reply);


function upload_image($num, $message_id) {
	global $db;

	$system = new system();

	$h = new Upload($_FILES["file{$num}"]); 

	if (!$h->uploaded)
		return;

	$ext = "";
	switch($h->file_src_mime) {
		case "image/jpeg":
			$ext = "jpg";
			break;
		case "image/gif":
			$ext = "gif";
			break;
		case "image/png":
			$ext = "png";
			break;
		default:
			return;
			break;
	}

	$original_body = preg_replace('/[^0-9a-zA-Z_\-\.]/', '', $h->file_src_name_body);
	$rand = $system->_makeText(6);
	$filename_body = "{$message_id}_{$rand}_{$original_body}";
	
	$h->file_new_name_body = $filename_body;
	$h->file_new_name_ext = $ext;

	$h->Process(_CMS_ABS_PATH."../data/pm_files/");
	if (!$h->processed) {
		$h->Clean();
		reportAdmin("AS: pm/compose error", "cant process file", array("message_id" => $message_id));
		return false;
	}

	$h->Clean();

	$filename = $h->file_dst_name;
	//_d("upl filename: '{$filename}'");

	//insert into db
	$db->q("INSERT INTO message_file (message_id, filename) VALUES (?, ?)", array($message_id, $filename));
	$id = $db->insertid($res);
	if ($id) {
		return true;
	} else {
		reportAdmin("AS: pm/compose error", "cant insert entry into message_file table", array("message_id" => $message_id, "filename" => $filename));
		return false;
	}
}

//----------------------
// processing submission
if ($_POST["submit"] == "submit") {


	$content = $_REQUEST["content"];
	if (!$content) {
		$smarty->assign("error", "You need to enter message !");
		$smarty->display(_CMS_ABS_PATH."/templates/pm/compose.tpl");
		return;
	}

	//TODO add spam drop !!!
	$ipAddress = account::getUserIp();
	if ($ipAddress == "2.95.133.30")
		return success_redirect("Your message has been successfully sent", "/pm/index");

	$ret = message::send($account, $recipient, NULL, $content, $reply);
	if ($ret === false) {
		//this should not happen
		reportAdmin("AS: pm/compose error", "Error sending message", array("recipient_id" => $recipient->getId(), "content" => $content)); 
		$smarty->assign("error", "Error sending message !");
		$smarty->display(_CMS_ABS_PATH."/templates/pm/compose.tpl");
		return;
	}

	//message sent succesfully, handle images
	for ($i = 1; $i <= 6; $i++) {
		upload_image($i, $ret);
	}

	return success_redirect("Your message has been successfully sent", "/pm/index");
}

escortsbiz::exportEbizLink();

$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/pm/compose.tpl");

?>
