<?php
/*
 * Private messages - Delete message
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

if (!$account->isloggedin()) {
	$account->asklogin();
	return;
}

$id = intval($_REQUEST["id"]);
if (!$id)
	return error_redirect("Invalid message id.", "/pm/");

$message = message::findOneById($id);
if (!$message)
	return error_redirect("Invalid message id.", "/pm/");

if ($message->getAuthorId() != $account->getId() 
	&& $message->getRecipientId() != $account->getId()
	&& !$account->isadmin()
	)
	return error_redirect("Invalid message id.", "/pm/");

$txt = "your inbox";
if ($message->getAuthorId() == $account->getId())
	$txt = "your sent list";

//delete message
$ret = $message->delete();
if ($ret)
	return success_redirect("The message was deleted from {$txt}.", "/pm/");
else
	return error_redirect("Error while deleting the message from {$txt}.", "/pm/");

die;

?>
