<?php
/*
 * Private messages - Read image file
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $account;

if (!account::ensure_loggedin())
    die("Invalid access");
$account_id = $account->getId();

$id = intval($_REQUEST["id"]);
$p_filename = $_REQUEST["filename"];
if (!$id || !$p_filename)
	die("Invalid params");

$res = $db->q("
	SELECT mf.filename, m.author_id, m.recipient_id 
	FROM message_file mf 
	INNER JOIN message m on m.id = mf.message_id 
	WHERE mf.id = ? LIMIT 1", 
	[$id]
	);
if ($db->numrows($res) != 1)
	die("Invalid params");
$row = $db->r($res);
$filename = $row["filename"];
$author_id = $row["author_id"];
$recipient_id = $row["recipient_id"];

if (!$account->isadmin() && $account_id != $author_id && $account_id != $recipient_id)
	die("Invalid access");
if ($filename != $p_filename)
	die("Invalid params");

$filepath = _CMS_ABS_PATH."/../data/pm_files/{$filename}";
if (!file_exists($filepath))
	die("Cant access file");

$mime_type = mime_content_type($filepath);
if (substr($mime_type, 0, 5) != "image")
	die("Invalid file type");

//stream file to browser
$h = fopen($filepath, 'rb');
if (!$h)
	die("Server error");

ob_end_clean();
header("Content-Type: ".$mime_type);
header("Content-Length: ".filesize($filepath));
fpassthru($h);
exit;
