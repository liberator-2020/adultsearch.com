<?php
/*
 * Private messages - Inbox
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

if (!$account->isloggedin()) {
	$account->asklogin();
	return;
}

$messages = message::getReceivedMessages();

$messages_2 = [];
$unread = 0;
foreach ($messages as $message) {
	$subject = $message->getsubject();
	if (!$subject) {
		$subject = substr($message->getContent(), 0, 20);
		if (strlen($message->getContent()) > 20)
			$subject .= "...";
	}
	$messages_2[] = array(
		"id" => $message->getId(),
		"sent_stamp" => $message->getSentStamp(),
		"author" => $message->getAuthor()->getUsername(),
		"subject" => $subject,
		"read_stamp" => $message->getReadStamp(),
		"read_link" => "/pm/read?id=".$message->getId(),
		);
	if (!$message->getReadStamp() && !$message->getRecipientDeletedStamp())
		$unread++;
}

$smarty->assign("unread", $unread);
$smarty->assign("messages", $messages_2);

escortsbiz::exportEbizLink();

$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/pm/index.tpl");

?>
