<?php
/*
 * Private messages - Read message
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

if (!$account->isloggedin()) {
	$account->asklogin();
	return;
}

$id = intval($_REQUEST["id"]);
if (!$id) {
	$smarty->assign("error", "Invalid message id.");
	$smarty->display(_CMS_ABS_PATH."/templates/pm/read.tpl");
	return;
}

$message = message::findOneById($id);
if (!$message) {
	$smarty->assign("error", "Invalid message id.");
	$smarty->display(_CMS_ABS_PATH."/templates/pm/read.tpl");
	return;
}

if ($message->getAuthorId() != $account->getId() 
	&& $message->getRecipientId() != $account->getId()
	&& !$account->isadmin()
	) {
	$smarty->assign("error", "Invalid message id.");
	$smarty->display(_CMS_ABS_PATH."/templates/pm/read.tpl");
	return;
}

if ($message->getRecipientId() == $account->getId()) {
	//check if this is first time reading message and mark as read accordingly
	$message->checkReadStamp();
}

$smarty->assign("message", array(
	"id" => $message->getId(),
	"author_id" => $message->getAuthorId(),
	"author" => ($account->getId() == $message->getAuthorId()) ? "me" : $message->getAuthor()->getUsername(),
	"recipient_id" => $message->getRecipientId(),
	"recipient" => ($account->getId() == $message->getRecipientId()) ? "me" : $message->getRecipient()->getUsername(),
	"subject" => htmlspecialchars($message->getSubject()),
	"content" => nl2br(htmlspecialchars($message->getContent())),
	"sent_stamp" => $message->getSentStamp(),
	"images" => $message->getImages(),
	));

escortsbiz::exportEbizLink();

message::exportUnread();

$smarty->assign("nobanner", true);
$smarty->display(_CMS_ABS_PATH."/templates/pm/read.tpl");

?>
