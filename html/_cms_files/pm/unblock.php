<?php
/*
 * Private messages - Unblock user
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

if (!$account->isloggedin()) {
	$account->asklogin();
	return;
}

$id = intval($_REQUEST["id"]);
if (!$id)
	return error_redirect("Invalid id.", "/pm/");

if ($id == $account->getId())
	return error_redirect("Invalid id.", "/pm/");

$block_account = account::findOneById($id);
if (!$block_account)
	return error_redirect("Invalid id.", "/pm/");

//find block entry in db
$res = $db->q("SELECT id FROM message_block WHERE account_id = ? AND blocked_id = ?", array($account->getId(), $block_account->getId()));
if ($db->numrows($res) != 1)
	return error_redirect("Error while unblocking user ".$block_account->getUsername().".", "/pm/");

//delete block from db
$res = $db->q("DELETE FROM message_block WHERE account_id = ? AND blocked_id = ? LIMIT 1", array($account->getId(), $block_account->getId()));
$aff = $db->affected($res);
if ($aff == 1)
	return success_redirect("User ".$block_account->getUsername()." unblocked.", "/pm/");
else
	return error_redirect("Error while unblocking user ".$block_account->getUsername().".", "/pm/");

die;

?>
