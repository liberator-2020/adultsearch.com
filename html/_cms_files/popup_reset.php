<?php
global $db;

$my_ip = account::getRealIp();

echo "<h1>Ad popup IP reset</h1>";

if ($_POST["ip"] != "") {
	$ip = preg_replace('/[^0-9\.]/', '', $_POST["ip"]);
	echo "Trying to delete IP '{$ip}'...<br />";
	$res = $db->q("DELETE FROM popup_ip WHERE ip_address = ? LIMIT 10", [$ip]);
	$aff = $db->affected($res);
	echo "IP {$ip} ({$ia}) successfully resetted for popup (deleted {$aff} sections), dont forget to delete your browser cache &amp; cookies for testing (or use incognito mode).<br />";
} else {
	$ip = $my_ip;
}

echo "<br />";

echo "My IP: <input type=\"text\" name=\"q\" value=\"{$my_ip}\" disabled=\"disabled\"><br /><br />";
echo "<form method=\"post\" data-ajax=\"false\">IP: <input type=\"text\" name=\"ip\" value=\"{$ip}\" /><input type=\"submit\" name=\"submit\" value=\"Submit\" /></form>\n";

?>
