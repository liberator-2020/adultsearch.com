<?php
/**
 * Should be used with nginx bypass of index.php (so we skip session,db init etc...)
 * location /poster/show {
 *	   try_files $uri /_cms_files/poster/show.php$is_args$args;
 * }
 * 
 * <iframe src="https://adultsearch.com/poster/show?id=" width="" height="" frameborder="0" scrolling="no"></iframe>
 */

if (basename(__FILE__) == "show.php") {
	//this executes if we run PHP file directly
	require_once(__DIR__."/../../inc/config.php");
	require_once(__DIR__."/../../inc/common.php");
} else {
	//this executes if we run this via index.php
	defined("_CMS_FRONTEND") or die("No Access");
}

global $gIndexTemplate, $config_site_url, $config_env, $smarty;

$gIndexTemplate = "none.tpl";

$id = $_REQUEST["id"];

$googleAnalytics = "";
//2019-05-28 zach wanted real numbers in GA, not traffic tha represents banner views
//if ($config_env == "prod")
//	$googleAnalytics = $smarty->fetch(_CMS_ABS_PATH."/templates/google_analytics.tpl");

$banners = [
	[
	"id" => "5escorts-300-250",
	"image_name" => "5escorts_300_250.png",
	"source_name" => "5escorts",
	"width" => "300",
	"height" => "250",
	],
	[
	"id" => "adultlook-728-90",
	"image_name" => "adultlook_728_90.png",
	"source_name" => "adultlook",
	"width" => "728",
	"height" => "90",
	],
	[
	"id" => "avaescorts-180-150",
	"image_name" => "avaescorts_180_150.jpg",
	"source_name" => "avaescorts",
	"width" => "180",
	"height" => "150",
	],
	[
	"id" => "avaescorts-468-60",
	"image_name" => "avaescorts_468_60.jpg",
	"source_name" => "avaescorts",
	"width" => "468",
	"height" => "60",
	],
	[
	"id" => "cityxguide-300-250",
	"image_name" => "cityxguide_300_250.png",
	"source_name" => "cityxguide",
	"width" => "300",
	"height" => "250",
	],
	[
	"id" => "eccie-160-600-left",
	"image_name" => ["ecc_ver1.png", "eccie_160_600_stat.jpg"],
	"source_name" => "eccie",
	"campaign_name" => ["eccie-160-600-left", "eccie-160-600-stat"],
	"width" => "160",
	"height" => "600",
	],
	[
	"id" => "eccie-300-100-stat",
	"image_name" => "eccie_300_100_stat.jpg",
	"source_name" => "eccie",
	"width" => "300",
	"height" => "100",
	],
	[
	"id" => "escortfish-300-100",
	"image_name" => "escortfish_300_100.png",
	"source_name" => "escortfish",
	"width" => "300",
	"height" => "100",
	],
	[
	"id" => "escortfish-300-250",
	"image_name" => "escortfish_300_250.png",
	"source_name" => "escortfish",
	"width" => "300",
	"height" => "250",
	],
	[
	"id" => "escortfish-728-90",
	"image_name" => "escortfish_728_90.png",
	"source_name" => "escortfish",
	"width" => "728",
	"height" => "90",
	],
	[
	"id" => "eurogirlsescort-210-290",
	"image_name" => "eurogirlsescort_210_290.png",
	"source_name" => "eurogirlsescort",
	"width" => "210",
	"height" => "290",
	],
	[
	"id" => "eurogirlsescort-250-73",
	"image_name" => ["eurogirlsescort_250_73_1.png", "eurogirlsescort_250_73_2.png"],
	"source_name" => "eurogirlsescort",
	"width" => "250",
	"height" => "73",
	],
	[
	"id" => "friday-728-90",
	"image_name" => ["friday_728_90.jpg", "friday_728_90_ts.jpg"],
	"target_url" => ["{$config_site_url}/classifieds/landing", "https://www.tsescorts.com"],
	"source_name" => "friday",
	"campaign_name" => ["friday-728-90", "friday-728-90-ts"],
	"width" => "728",
	"height" => "90",
	],
	[
	"id" => "friday-300-250",
	"image_name" => ["friday_300_250.jpg", "friday_300_250_ts.jpg"],
	"target_url" => ["{$config_site_url}/classifieds/landing", "https://www.tsescorts.com"],
	"source_name" => "friday",
	"campaign_name" => ["friday-300-250", "friday-300-250-ts"],
	"width" => "300",
	"height" => "250",
	],
	[
	"id" => "mrporngeek-900-250",
	"image_name" => "mrporngeek_900_250.png",
	"source_name" => "mrporngeek",
	"width" => "900",
	"height" => "250",
	],
	[
	"id" => "onebackpage-160-600",
	"image_name" => "onebackpage_160_600.png",
	"source_name" => "onebackpage",
	"width" => "160",
	"height" => "600",
	],
	[
	"id" => "onebackpage-300-250",
	"image_name" => "onebackpage_300_250.png",
	"source_name" => "onebackpage",
	"width" => "300",
	"height" => "250",
	],
	[
	"id" => "onebackpage-728-90",
	"image_name" => "onebackpage_728_90.png",
	"source_name" => "onebackpage",
	"width" => "728",
	"height" => "90",
	],
	[
	"id" => "pornhub-300-150",
	"image_name" => "pornhub_300_150.png",
	"source_name" => "pornhub",
	"width" => "300",
	"height" => "150",
	],
	[
	"id" => "pornhub-300-250",
	"image_name" => "pornhub_300_250.png",
	"source_name" => "pornhub",
	"width" => "300",
	"height" => "250",
	],
	[
	"id" => "pornhub-305-99",
	"image_name" => "pornhub_305_99.png",
	"source_name" => "pornhub",
	"width" => "305",
	"height" => "99",
	],
	[
	"id" => "pornhub-315-300",
	"image_name" => "pornhub_315_300.png",
	"source_name" => "pornhub",
	"width" => "315",
	"height" => "300",
	],
	[
	"id" => "terb-195-60",
	"image_name" => "terb_195_60.gif",
	"source_name" => "terb",
	"width" => "195",
	"height" => "60",
	],
	[
	"id" => "terb-468-60",
	"image_name" => "terb_468_60.png",
	"source_name" => "terb",
	"width" => "468",
	"height" => "60",
	],
	[
	"id" => "terb-728-90",
	"image_name" => "terb_728_90.png",
	"source_name" => "terb",
	"width" => "728",
	"height" => "90",
	],
	[
	"id" => "tnaboard-300-250",
	"image_name" => "tnaboard_300_250.png",
	"source_name" => "tnaboard",
	"width" => "300",
	"height" => "250",
	],
];

if ($config_env != "prod" && $_REQUEST["action"] == "showbannercodes") {
	foreach ($banners as $banner) {
		$iframeCode = "<iframe src=\"https://adultsearch.com/poster/show?id={$banner["id"]}\" width=\"{$banner["width"]}\" height=\"{$banner["height"]}\" frameborder=\"0\" scrolling=\"no\"></iframe>";
		echo "AS Banner for {$banner["source_name"]} {$banner["width"]}x{$banner["height"]}:<br />";
		$testUrl = $config_site_url."/poster/show?id={$banner["id"]}";
		echo "Test link: <a href=\"{$testUrl}\">{$testUrl}</a><br />";
		echo "<code>".htmlspecialchars($iframeCode)."</code><br /><br />\n";
	}
	return;
}
	
$html = $link = $link_style = $img_src = $img_style = "";

$found = false;
foreach ($banners as $banner) {
	if ($banner["id"] == $id) {
		$found = true;
		break;
	}
}
if ($found) {
	$imageFilename = "";
	$campaignName = $banner["id"];
	$link = "{$config_site_url}/classifieds/landing";
	if (array_key_exists("target_url", $banner) && !is_array($banner["target_url"]))
		$link = $banner["target_url"];
	if (is_array($banner["image_name"])) {
		$ind = rand(0,count($banner["image_name"]) - 1);
		$imageFilename = $banner["image_name"][$ind];
		if (array_key_exists("campaign_name", $banner) && is_array($banner["campaign_name"])) {
			$campaignName = $banner["campaign_name"][$ind];
		}
		if (array_key_exists("target_url", $banner) && is_array($banner["target_url"])) {
			$link = $banner["target_url"][$ind];
		}
	} else {
		$imageFilename = $banner["image_name"];
	}
	$link .= "?utm_source={$banner["source_name"]}&utm_medium=banner&utm_campaign={$campaignName}";
	$img_src = "{$config_site_url}/images/poster/{$imageFilename}";
	$img_style = "width: {$banner["width"]}px; height: {$banner["height"]}px;";
} else {
	switch ($id) {
		case "usg_728_top":
			$html ='<a href="https://adultfriendfinder.com/go/g1445619-pmob?lang=english" title="Click here for more." target="_blank"><img src="https://secureimage.securedataimages.com/banners/aff/39497/38004_728x90.jpg" width="728" height="90" border="0" alt="Click here for more!" /></a>';
			break;
		case "usg_900_main":
			$html = '<a href="https://adultfriendfinder.com/go/g1445619-pmob?lang=english" title="Click here for more!" target="_blank"><img src="https://secureimage.securedataimages.com/banners/aff/mdly204/39689_950x250.jpg" width=“950" height=“250" border="0" alt="Click here for more!" /></a>';
			break;
		case "usg_sky_left":
			$link = "https://adultsearch.com?utm_source=usg&utm_medium=banner&utm_campaign=poster_usg_sky_left";
			$img_src = "https://adultsearch.com/images/poster/usg_akelkd.png";
			$img_style = "width: 160px; height: 600px;";
			break;
		case "usg_sky_right":
			$html = '<a href="https://adultfriendfinder.com/p/register.cgi?lang=english&pid=g1445619-pmob" target="_blank"><img src="https://secureimage.securedataimages.com/banners/aff/crp/41126_160x600_crp188-1.gif" width="160" height="600" border="0" /></a>';
			break;

		case "ecc_468_top":
			$html = '<a href="https://adultfriendfinder.com/p/register.cgi?pid=g1445619-pmob.subecctopnavbar" target="_blank"><img src="https://secureimage.securedataimages.com/banners/aff/36860/AFF01/AFF01_english.gif" width="468" height="90" border="0"/></a>';
			break;
		case "ecc_728_main":
			$html ='<a href="https://adultfriendfinder.com/go/g1445619-pmob.subeccmaincenter?lang=english" title="Click here for more." target="_blank"><img src="https://secureimage.securedataimages.com/banners/aff/39497/38004_728x90.jpg" width="728" height="90" border="0" alt="Click here for more!" /></a>';
			break;
		case "ecc_728_esc":
			$link = "https://adultsearch.com/escorts/female?utm_source=eccie&utm_medium=banner&utm_campaign=eccie-728x90-escorts";
			$img_src = "https://adultsearch.com/images/poster/ecc_esc_hor2.jpg";
			$img_style = "width: 728px; height: 90px;";
			break;
		case "ecc_300_100_esc":
			$link = "https://adultsearch.com/escorts/female?utm_source=eccie&utm_medium=banner&utm_campaign=eccie-300x100-escorts";
			$link_style = "display: block; margin: auto; width: 90%;";
			$img_src = "https://adultsearch.com/images/poster/ecc_esc_hor4.jpg";
			//$img_style = "width: 300px; height: 100px;";
			$img_style = "width: 90%; display: block; margin: auto;";
			break;
		case "ecc_sky_right":
			$html = '<a href="https://adultfriendfinder.com/go/page/landing_page_93?pid=g1445619-pmob.subeccskyright" target="_top"><img src="https://secureimage.securedataimages.com/banners/aff/SOC-2702/milf/40236_aff_milf_160x600_nn.gif" width="160" height="600" border=0></a>';
			break;
		case "ecc_900_bottom":
			$link = "https://adultsearch.com?utm_source=usg&utm_medium=banner&utm_campaign=poster_ecc_900_bottom";
			$img_src = "https://adultsearch.com/images/poster/ecc_hor3.png";
			$img_style = "width: 900px; height: 250px;";
			break;
		case "lc-300-250":
			$link = "{$config_site_url}/classifieds/landing?utm_source=listcrawler&utm_medium=banner&utm_campaign=lc-300-250";
			$width = "300px";
			$height = "250px";
			$font_size = "14px";
			$background_color = "#e8f7fe";
			$color = "#0d0f40";
			$ul_margin = "10px";
			$li_width = "140px";
			$li_line_height = "26px";
			$list = ["Dallas", "Chicago", "Las Vegas", "L.A.", "Atlanta", "Wash. DC", "Houston", "Orlando", "Miami", "San Diego", "Phoenix", "F Lauderdale", "Brooklyn", "Austin", "Sacramento", "Baltimore", "San Antonio", "Denver"];
			break;
		case "lc-600-195":
			$link = "{$config_site_url}/classifieds/landing?utm_source=listcrawler&utm_medium=banner&utm_campaign=lc-600-195";
			$width = "600px";
			$height = "195px";
			$font_size = "16px";
			$background_image = "linear-gradient(#fef9d3, white)";
			$color = "#0d4a9a";
			$ul_margin = "15px";
			$li_width = "180px";
			$li_line_height = "28px";
			$list = ["Dallas", "Chicago", "Las Vegas", "L.A.", "Atlanta", "Wash. DC", "Houston", "Orlando", "Miami", "San Diego", "Phoenix", "F Lauderdale", "Brooklyn", "Austin", "Sacramento", "Baltimore", "San Antonio", "Denver"];
			break;
		case "locanto-300-250":
			$link = "{$config_site_url}/classifieds/landing?utm_source=friday&utm_medium=banner&utm_campaign=locanto-300-250";
			$img_src = "{$config_site_url}/images/poster/locanto_300_250.jpg";
			$img_style = "width: 300px; height: 250px;";
			break;
		case "locanto-300-50":
			$link = "{$config_site_url}/classifieds/landing?utm_source=friday&utm_medium=banner&utm_campaign=locanto-300-50";
			$img_src = "{$config_site_url}/images/poster/locanto_300_50.jpg";
			$img_style = "width: 300px; height: 50px;";
			break;
		case "locanto-300-100":
			$link = "{$config_site_url}/classifieds/landing?utm_source=friday&utm_medium=banner&utm_campaign=locanto-300-100";
			$img_src = "{$config_site_url}/images/poster/locanto_300_100.jpg";
			$img_style = "width: 300px; height: 100px;";
			break;
		default:
			$html = ".";
			break;
	}
}
?>
<html>
<head>
</head>
<body padding="0" margin="0" style="padding: 0px; margin: 0px;">
<?php 
if ($list) {
	echo "
<style type=\"text/css\">
body {
	font-family: arial, sans-serif;
	font-size: {$font_size};
}
div {
	width: {$width};
	height: {$height};
	";
	if ($background_color)
		echo "	background-color: {$background_color};";
	else if ($background_image)
		echo "	background-image: {$background_image};";
	echo "
	overflow: hidden;
}
a, a:hover, a:visited {
	color: {$color};
	text-decoration: none;
}
ul {
	list-style-type: none;
	width: 100%;
	padding: 0px;
	margin: {$ul_margin};
}
li {
	display: inline-block;
	width: {$li_width};
	line-height: {$li_line_height};
}
</style>
<div>
<a href=\"{$link}\">
<ul>";
	foreach ($list as $city) {
		echo "<li><strong>{$city}</strong> Escorts</li>";
	}
	echo "
</ul>
</a>
</div>
	";
} else if ($html) {
	echo $html;
} else {
	echo "<a href=\"{$link}\" style=\"{$link_style}\" target=\"_top\" ><img src=\"{$img_src}\" style=\"{$img_style}\" /></a>";
}
echo $googleAnalytics;
?>
</body>
</html>
