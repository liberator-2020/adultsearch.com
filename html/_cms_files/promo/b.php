<?php

ob_end_clean();
header("Cache-Control: no-cache, must-revalidate"); 
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

global $db, $mobile, $advertise, $sphinx, $debug, $config_site_url;

session_write_close();

if (is_bot())
	die("");

$mob = ($mobile == 1) ? 1 : 0;

$red = new red();

// if advertise_section.debug == 1, debug some information into logfile
function banner_debug($str) {
	global $debug;
	if (!$debug)
		return;

	$str = replaceIpInStr($str);

	$h = fopen(_CMS_ABS_PATH."/banner.log", "a");
	fputs($h, "[".date("Y-m-d H:i:s")."] [".account::getUserIp()."] {$str}\n");
	fclose($h);
}

//create HTML code for banner
function get_banner_html($code, $img_src, $a_href, $iframe_src) {
	$html = "";
	$now = time();

	$iframe_src = str_replace('{$timestamp}', $now, $iframe_src);
	$a_href = str_replace('{$timestamp}', $now, $a_href);
	$img_src = str_replace('{$timestamp}', $now, $img_src);

	if ($code) {
		//this is ultimate HTML override
		$html = $code;
	} else {
		//standard banners
		if ($img_src) {
			$img_src = str_replace("img.adultsearch.com/advertise/", "img.adultsearch.com/promo/", $img_src);
			$img_src = str_replace("http://img.adultsearch.com/", "//img.adultsearch.com/", $img_src);
			$html = "<img src=\"{$img_src}\" />";
		}
		if ($a_href) {
			$html = "<a href=\"{$a_href}\" target=\"_blank\">{$html}</a>";
		}
		if ($iframe_src) {
			$html = "<style type=\"text/css\">body { margin: 0px; padding: 0px;} iframe { margin:0px;}</style><iframe src=\"{$iframe_src}\" width=\"100%\" height=\"100%\" style=\"border: 0px;\" marginwidth=\"0\" marginheight=\"0\" hspace=\"0\" vspace=\"0\" frameborder=\"0\" scrolling=\"no\">{$html}</iframe>";
		}
	}
	return $html;
}


$s = intval($_GET["s"]);
if (!$s)
	die;

//echo "{$advertise->get_test_zone_id()},{$advertise->get_test_campaign_id()}";
if ($advertise->get_test_zone_id() && $advertise->get_test_campaign_id()) {
	if ($advertise->get_test_zone_id() != $s) {
		echo "<html><body class=\"srafo\"><style type=\"text/css\">.srafo { background: repeating-linear-gradient(45deg, #606dbc, #606dbc 10px, #465298 10px, #465298 20px);}</style></body></html>";
		die;
	}
	$res = $db->q("SELECT c.* FROM advertise_campaign c WHERE c.id = ?", array($advertise->get_test_campaign_id()));
	if (!$db->numrows($res)) {
		echo "<html><body style=\"background-color: red;\"><h1>Error</h1></body></html>";
		die;
	}
	$row = $db->r($res);
	$html = get_banner_html($row["code"], $row["banner_img_src"], $row["banner_a_href"], $row["banner_iframe_src"]); 
	echo $html;
	die;
}

//fetch section info
$res = $db->q("SELECT active, account_id, cpm_minimum, debug, backuphtml, exclusive_access FROM advertise_section WHERE id = ?", array($s));
if (!$db->numrows($res))
	die;
$row = $db->r($res);
$cpm_minimum = $row["cpm_minimum"];
$debug = intval($row["debug"]);
$backup_html = $row["backuphtml"];

$exclusive_access = array();	//array of account_ids of advertisers, who are only permitted to advertise in this zone
$exclusive_access_str = $row["exclusive_access"];
if ($exclusive_access_str)
	$exclusive_access = explode(",", $exclusive_access_str);

banner_debug("s={$s} active={$row["active"]} account_id={$row["account_id"]}, exclusive_access_str={$exclusive_access_str}");

//if section active flag not set, set it
if ($row["active"] == 0)
	$db->q("UPDATE LOW_PRIORITY advertise_section SET active = 1 WHERE id = ?", array($s));

//specify which template to use
//$display = _CMS_ABS_PATH."/templates/advertise/banner_300x250.tpl";
$display = _CMS_ABS_PATH."/templates/advertise/banner.tpl";


//-------------
//search banner
$sphinx->reset();
$sphinx->SetFilter('s_id', array($s));
$sphinx->SetLimits(0, 50, 50);
$sphinx->SetSortMode(SPH_SORT_ATTR_DESC, "cpm");
$results = $sphinx->Query("", "advertise");
$total = $results["total_found"];
$result = $results["matches"];

$cs_id = null;
$html = "";

banner_debug("total={$total}");

if ($total) {

	$cpm_to_charge = NULL;

	if ($total == 1) {
		//we have only one advertiser in this section - display that campaign
		$res = array_shift($result);
		if ($res)
			$cs_id = $res["id"];

	} else if (empty($exclusive_access)) {
		//traditional bidding system - banner of highest bidder is displayed
		$id = NULL;
		$max = 0;
		$acc_max_bids = [];		//array $acc_max_bids stores max bids of each advertiser
		$bids = [];
		foreach ($result as $match) {
			$account_id = $match["attrs"]["account_id"];
			$cpm = round($match["attrs"]["cpm"], 2);

			if (!array_key_exists($account_id, $acc_max_bids) || ($cpm > $acc_max_bids[$account_id]))
				$acc_max_bids[$account_id] = $cpm;
				
			$bids[] = [
				"cs_id" => $match["id"],
				"account_id" => $account_id,
				"cpm" => $cpm,
				];
		}

		//determine cpm to charge
		arsort($acc_max_bids);
		$highest_cpm = current($acc_max_bids);
		$second_highest_cpm = next($acc_max_bids);
		if ($highest_cpm == $second_highest_cpm)
			$cpm_to_charge = $highest_cpm;
		else
			$cpm_to_charge = round(($second_highest_cpm + 0.01), 2);

		//filter advertisers that offered cpm higher than cpm to charge (so if 2 or more advertisers offered the same highet cpm, they all will advertise)
		$acc_to_advertise = [];
		foreach ($acc_max_bids as $account_id => $max_bid) {
			if ($max_bid < $cpm_to_charge)
				break;
			$acc_to_advertise[] = $account_id;
		}
		if (empty($acc_to_advertise)) {
			banner_debug("no acc_to_advertise");
			//banner_debug("cpm_to_charge={$cpm_to_charge}, highest_cpm={$highest_cpm}, second_highest_cpm={$second_highest_cpm}, bids".print_r($bids, true).", acc_max_bids=".print_r($acc_max_bids, true).";");
			die;	//sanity check
		}

		//determine advertiser - if only one advertiser offered highest cpm, it will be him. If there are more advertisers with the same highest cpm, then pick random one.
		if (count($acc_to_advertise) == 1)
			$account_id = $acc_to_advertise[0];
		else
			$account_id = $acc_to_advertise[array_rand($acc_to_advertise)];
		$maccount_id = $account_id;
		
		//go through all placements of selected advertiser, and gather all of them which has cpm >= cpm_to_charge
		$valid_bids = [];
		foreach ($bids as $bid) {
			if ($bid["account_id"] != $account_id)
				continue;
			if ($bid["cpm"] < $cpm_to_charge)
				continue;
			$valid_bids[] = $bid;
		}
		if (empty($valid_bids)) {
			banner_debug("no valid bids");
			die;	//sanity check
		}

		//determine placement - if advertiser has only one placement, it will be that one. if he has more, than pick random one
		if (count($valid_bids) == 1)
			$bid = $valid_bids[0];
		else
			$bid = $valid_bids[array_rand($valid_bids)];

		//get data we need
		$cs_id = $bid["cs_id"];
		$cpm = $bid["cpm"];

		banner_debug("System: BID, chose cs_id={$cs_id}, cpm={$cpm}, cpm_to_charge={$cpm_to_charge}");

	} else {
		//proportional system 
		//we have more campaigns for this section, choose proportionally to their cpms 
		//(if there are 2 campaigns, and one has cpm $10 and other one $5, than choose the first one with 66% probability)
		$sum = 0;
		$boundaries = array();
		foreach ($result as $match) {
			$cs_id = $match["id"];
			$cpm = $match["attrs"]["cpm"];
			$account_id = $match["attrs"]["account_id"];
			$cpm100 = intval($cpm * 100);
			$c_id = $match["attrs"]["c_id"];

			//if exclusive access to this zone is set up, skip advertisers that are not in exclusive access array
			if (!empty($exclusive_access) && !in_array($account_id, $exclusive_access))
				continue;

			$sum += $cpm100;
			$boundaries[$sum] = $cs_id;
			banner_debug("boundary: cpm=".number_format($cpm, 2).", sum={$sum}, cs_id={$cs_id}");
		}
		//$ran = rand(0, $sum);
		$ran = mt_rand(0, $sum);
		foreach ($boundaries as $cpm100 => $id) {
			if ($ran <= $cpm100) {
				$cs_id = $id;
				break;
			}
		}
		banner_debug("chose campaign: ran={$ran}, cs_id={$cs_id}");
	}

	//-----------------------------------------------------------------------------------------------------------------------
	//Ok, we have chosen which placement are we going to display, now fetch some campaign details that we need for accounting
	/*
	$res = $db->q("
		SELECT s.cpm as cs_cpm, s.flat_deal_status
			, fd1.id as fd1_id, fd1.type as fd1_type, fd1.impressions_left as fd1_impressions_left, fd1.next_deal_id as fd1_next_deal_id
			, d2_id, d2_type, d2_impressions_left, d2_next_deal_id
			, c.* 
		FROM advertise_camp_section s 
		INNER JOIN advertise_campaign c on s.c_id = c.id 
		LEFT JOIN advertise_flat_deal fd1 on fd1.account_id = c.account_id and fd1.zone_id = s.s_id and fd1.status = 1 and ( fd1.type = 'ZI' OR fd1.type = 'ZT' )

		LEFT JOIN 
		  (SELECT afd.id as d2_id, afd.type as d2_type, afd.account_id as d2_account_id, afd.impressions_left as d2_impressions_left, afd.next_deal_id as d2_next_deal_id
			, adz.id as adz_id, adz.zone_id as adz_zone_id
		   FROM advertise_flat_deal afd
		   INNER JOIN advertise_deal_zone adz ON adz.deal_id = afd.id
		   WHERE afd.status = 1 AND (afd.type = 'ZI' OR afd.type = 'ZT')
		  ) d2
		  ON d2.d2_account_id = c.account_id AND d2.adz_zone_id = s.s_id

		WHERE s.id = ?",
		[$cs_id]
		);
	*/
	$res = $db->q("
		SELECT s.cpm as cs_cpm, s.flat_deal_status, c.* 
		FROM advertise_camp_section s 
		INNER JOIN advertise_campaign c on s.c_id = c.id 
		WHERE s.id = ?",
		[$cs_id]
		);
	if ($db->numrows($res)) {
		$row = $db->r($res);

		$c_id = $row["id"];
		$flat_deal = ($row["flat_deal_status"] == 1);
		$dbudget = $row["budget"];
		$account_id = $row["account_id"];	//campaign owner
		$iframe_src = $row["banner_iframe_src"];
		$a_href = $row["banner_a_href"];
		$img_src = $row["banner_img_src"];
		$code = $row["code"];
		/*
		$fd_id = $fd_type = $fd_left = $next_deal_id = null;
		if ($row["fd1_id"] && $row["fd1_type"]) {
			$fd_id = $row["fd1_id"];
			$fd_type = $row["fd1_type"];
			$fd_left = $row["fd1_impressions_left"];
			$next_deal_id = $row["fd1_next_deal_id"];
		} else if ($row["d2_id"] && $row["d2_type"]) {
  			$fd_id = $row["d2_id"];
			$fd_type = $row["d2_type"];
			$fd_left = $row["d2_impressions_left"];
			$next_deal_id = $row["d2_next_deal_id"];
		}
		if ($flat_deal && (!$fd_id || !$fd_type))
			$flat_deal = false;
		*/

		//compute bid
		if ($total == 1)
			$cpm = $cpm_minimum;		//if there is only 1 advertiser, use minimum bid of the section (he does not need to overbid anybody)
		else
			$cpm = $row["cs_cpm"];

		//increment and read number of impressions since last accounting from redis atomically
		$impressions_since = $red->incr("as_ad_cs_{$cs_id}_is");

		banner_debug("ACS found, id={$id}, account_id={$account_id}, c_id={$c_id}, impressions_since={$impressions_since}");

		$thousand_reached = false;

		if ($impressions_since % 1000 == 0) {
			//another thousand impressions
			$thousand_reached = true;

			//zero impression counter
			$red->decrBy("as_ad_cs_{$cs_id}_is", $impressions_since);

			if ($impressions_since > 1000) {
				//this should theoretically never happen, but notify admin if impressions since was not zeroed when reaching 1000, but it incremented until 200 ?
				reportAdmin("AS: promo/b impressions since skipped 1000 ?!", "s_id={$s} cs_id={$id}, account_id={$account_id}, c_id={$c_id}, impressions_since={$impressions_since}");
			}
		}

		if (is_null($c_id)) {
			//if we fucked up and we are getting lock timeouts from previous select, dont continue in running this script!
			debug_log("Error: advertise/banner : c_id is null!");
			return;
		}

		if ($thousand_reached) {

			//fetch flat deal information		
			$fd_id = $fd_type = $fd_left = $next_deal_id = null;
			$res = $db->q("
				SELECT afd.id, afd.type, afd.impressions_left, afd.next_deal_id
				FROM advertise_flat_deal afd
				INNER JOIN advertise_deal_zone adz ON adz.deal_id = afd.id
				WHERE afd.status = 1 AND (afd.type = 'ZI' OR afd.type = 'ZT') AND afd.account_id = ? AND adz.zone_id = ?
				",
				[$account_id, $s]
				);
			if ($db->numrows($res) > 0) {
				$row = $db->r($res);
	  			$fd_id = $row["id"];
				$fd_type = $row["type"];
				$fd_left = $row["impressions_left"];
				$next_deal_id = $row["next_deal_id"];
			}
			if ($flat_deal && (!$fd_id || !$fd_type))
				$flat_deal = false;


			if ($s != 10077 || is_null($cpm_to_charge)) {
				$cpm_to_charge = $cpm;
			}
	
			//and insert impression checkpoint
			$db->q("INSERT INTO advertise_banner_impression
					(c_id, cs_id, s_id, stamp, impressions, cost, fixed)
					VALUES
					(?, ?, ?, ?, ?, ?, ?)",
					array($c_id, $cs_id, $s, time(), 1000, $cpm_to_charge, intval($flat_deal))
					);

			//adding stat for a zone
			$profit = $cpm_to_charge;	//TODO this is only true for direct advertising, not through publisher
			$db->q("INSERT INTO advertise_stat 
					(s_id, date, c, backup, profit, mobile, e) 
					VALUES 
					(?, CURDATE(), 1000, 0, ?, ?, 0)
					ON DUPLICATE KEY UPDATE c = c + 1000, profit = profit + ?, mobile = mobile + ?",
					[$s, $profit, $mob, $profit, $mob]
					);

			if (!$flat_deal) {
				//if this is classically paid campaign, lets substract from budget
				$res = $db->q("SELECT budget FROM advertise_budget WHERE account_id = ?", array($account_id));
				if (!$db->numrows($res)) {
					//no entry in budget table ? this should not happen, disable campaign
					file_log("advertise", "promo/b: Not a flat deal and no entry in budget table -> disabling campaign #{$c_id}");
					$db->q("UPDATE advertise_campaign SET status = 0 WHERE id = ?", array($c_id));
					//and notify admin
					reportAdmin("AS: advertise/banner: Error: No entry in budget table", "", array("c_id" => $c_id, "account_id" => $account_id, "s" => $s, "cs_id" => $cs_id));
				} else {
					$row = $db->r($res);
					$budget = $row["budget"];
					$new_budget = $budget - $cpm_to_charge;
					if ($new_budget < 0)
						$new_budget = 0;
					//update budget
					$db->q("UPDATE advertise_budget SET budget = ? WHERE account_id = ?", array(round($new_budget, 2), $account_id));
					//if new budget is not big enough to run another 1000 impressions, pause campaign
					if ($new_budget < $cpm) {
						file_log("advertise", "promo/b: Not a flat deal and budget not big enough to run another 1000 impressions -> disabling campaign #{$c_id}");
						$db->q("UPDATE advertise_campaign SET status = 0 WHERE id = ?", array($c_id));
						//if budget is zero now, disable campaign placements
						advertise::budgetZero($account_id);
					}
				}

				if ($dbudget) {
					//lets check if we didnt cross daily budget for this campaign
					//$res = $db->q("SELECT SUM(cost) cost FROM advertise_data WHERE c_id = ? AND date = CURDATE()", array($c_id));
					$today_start = strtotime("00:00:00");
					$today_end = $today_start + 86400;
					$res = $db->q("SELECT SUM(cost) cost FROM advertise_banner_impression WHERE c_id = ? AND stamp >= ? AND stamp <= ?", 
						array($c_id, $today_start, $today_end));
					if (!$db->numrows($res)) {
						//no cost marked for today ? this should not happen, disable campaign
						file_log("advertise", "promo/b: Error: Not a flat deal and no cost found in advertise_banner_impression table ! Disabling campaign #{$c_id}");
						$db->q("UPDATE advertise_campaign SET status = 0 WHERE id = ?", array($c_id));
						//and notify admin
						reportAdmin("AS: advertise/banner: Error: No cost marked for today", "", array("c_id" => $c_id, "s" => $s, "cs_id" => $cs_id));
					} else {
						$row = $db->r($res);
						$cost = $row["cost"];
						if ((int)$cost >= $dbudget) {
							file_log("advertise", "promo/b: Not a flat deal and daily budget reached -> Setting budget_available to 0 on campaign #{$c_id}");
							$db->q("UPDATE advertise_camp_section SET budget_available = 0 WHERE c_id = ?", array($c_id));
						}
					}
				}

			} else if ($flat_deal) {
				//this is flat deal 

				if ($fd_type == "ZI") {
					//substracting impressions from zone-impressions deal
					$db->q("UPDATE advertise_flat_deal SET impressions_left = IF(impressions_left-1000>0, impressions_left-1000, 0) WHERE id = ? LIMIT 1", array($fd_id));
	
					//pausing flat deal after finishing
					if ($fd_left <= 1000) {
						file_log("advertise", "promo/b: Flat deal and less than 1000 impressions left on a deal -> Deactivating flat deal #{$fd_id}");
						$db->q("UPDATE advertise_flat_deal SET status = 0, finished_stamp = ? WHERE id = ?", array(time(), $fd_id));
						$advertise->flatDealStatusUpdate($fd_id);

						//we dont pause flat deal campaigns anymore, disabling placements from flat deal is enough
						//$advertise->flatDealCampaigns($fd_id);

						//if there is next deal starting, start it
						if ($next_deal_id) {
							file_log("advertise", "promo/b: Starting next deal #{$next_deal_id}");
							$db->q("UPDATE advertise_flat_deal SET status = 1, started_stamp = ? WHERE id = ?", array(time(), $next_deal_id));
							$advertise->flatDealStatusUpdate($next_deal_id);
						}

						reportAdmin("AS: Flat deal finished", "Check if campaigns are properly paused !!! Check if next deal #{$next_deal_id} properly started !!!", array("fd_id" => $fd_id, "next_deal_id" => $next_deal_id));
					}
				} else if ($fd_type == "ZT") {
					//zone time flat deal type, we dont need to do anything here
				} else {
					debug_log("advertise/banner: Error: Unknown flat deal type '{$fd_type}' !");
				}
			}

		}

		$db->q("INSERT INTO advertise_data 
				(s_id, date, c_id, impression, realsid) 
				values 
				(?, CURDATE(), ?, 1, ?)
				on duplicate key update impression = impression + 1",
				array($cs_id, $c_id, $s)
				);

		//create HTML code for banner
		$html = get_banner_html($code, $img_src, $a_href, $iframe_src);

	}

	banner_debug("html_len = ".strlen($html).", html=".substr($html, 0, 50)."...");

} else if ($backup_html) {
	banner_debug("Returning backup HTML");

	//adding stat for a zone
	$html = "<html><body style=\"text-align: center;\">{$backup_html}</body></html>";
	$db->q("INSERT INTO advertise_stat 
			(s_id, date, c, backup, profit, mobile, e) 
			VALUES 
			(?, CURDATE(), 0, 1, 0, ?, 0)
			ON DUPLICATE KEY UPDATE backup = backup + 1, mobile = mobile + ?",
			[$s, $mob, $mob]
			);

} else {
	//no advertising found, lets display "Advertise here HTML"
	banner_debug("Returning advertise here HTML");
	//TODO display optimized images for each size of advertise section (300x250, 728x90, ...)
	$html = "<html>
		<style type=\"text/css\">
		body {
		background-color: #333;
		color: #ddd;
		border: 1px solid #222;
		padding: 3px;
		font-family: sans-serif;
		}
		a {
		display: block;
		width: 100%;
		height: 100%;
		color: #ddd;
		text-decoration: none;
		text-align: center;
		}
		</style>
		<body>
		<a href=\"".$config_site_url."/advertise/\" target=\"_blank\"><h1>Advertise here</h1></a>
		</body>
		</html>";
}

echo $html;
die;

?>
