<?php
global $advertise, $smarty;

header('Content-type: application/javascript');

$s_id = intval($_GET['s_id']);
if (!$s_id)
	die;

$id = $advertise->popup($s_id);
$jscode = $smarty->fetch(_CMS_ABS_PATH."/templates/advertise/advertise_epop_nojquery.tpl");

echo $jscode;
die;
?>
