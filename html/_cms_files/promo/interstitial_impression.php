<?php
/**
 * This is supposed to be called via POST AJAX request when interstitial ad is shown to the use
 */

global $db, $account, $mobile, $advertise, $config_site_url;

$cs_id = intval($_GET["cs_id"]);

if (!$cs_id || is_bot())
	die("ERR 1");

//fetch campaign id and zone id
$res = $db->q("SELECT cs.c_id, cs.s_id FROM advertise_camp_section cs WHERE cs.id = ? LIMIT 1", [$cs_id]);
if (!$db->numrows($res))
	die("ERR 2");
$row = $db->r($res);
$c_id = $row["c_id"];
$s_id = $row["s_id"];

//insert impression into DB
$db->q("
	INSERT INTO advertise_data 
	(s_id, date, c_id, impression, realsid) 
	VALUES 
	(?, CURDATE(), ? , 1, ?)
	ON DUPLICATE KEY UPDATE impression = impression + 1
	",
	[$cs_id, $c_id, $s_id]
	); 

$ip = account::getUserIp();
//$db->q("INSERT IGNORE INTO popup_ip (s_id, ip_address, stamp) values (?, ?, ?)", [$s_id, $ip, time()]);

die("OK");

//END
