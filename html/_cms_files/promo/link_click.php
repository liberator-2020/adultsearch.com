<?php

global $db, $account, $mobile, $advertise, $config_site_url;

$cs_id = intval($_GET["cs_id"]);
$u = substr($_REQUEST["u"], 0, 250);
$r = substr($_REQUEST["r"], 0, 250);
if (!$cs_id || empty($u) || is_bot())
	system::moved($config_site_url);
$mob = ($mobile == 1) ? 1 : 0;

//select all necessary data from advertise tables
$res = $db->q("
	SELECT cs.s_id, cs.customurl, cs.bid, cs.flat_deal_status
			, c.type, c.account_id, c.budget dbudget, c.id c_id, c.ad_https, c.ad_url, c.banner_a_href
			, b.budget budget, b.budget_notify
			, ads.currentbid, ads.external, ads.account_id publisher
			, fd.id as fd_id, fd.type as fd_type
	FROM advertise_camp_section cs
	INNER JOIN advertise_campaign c on cs.c_id = c.id
	INNER JOIN advertise_section ads on cs.s_id = ads.id
	LEFT JOIN advertise_budget b on b.account_id = c.account_id
	LEFT JOIN advertise_flat_deal fd on fd.account_id = c.account_id and fd.zone_id = cs.s_id and fd.status = 1 and ( fd.type = 'ZI' OR fd.type = 'ZT' )
	WHERE cs.id = ?",
	[$cs_id]
);
if (!$db->numrows($res)) {
	$db->q("INSERT INTO advertise_stat 
		(s_id, date, c, backup, mobile, e) 
		values 
		(0, CURDATE(), 0, 0, ?, 1) 
		ON DUPLICATE KEY UPDATE e = e + 1, mobile = mobile + ?", 
		[$mob, $mob]
		);
	if (isset($_GET['adurl']) && !empty($_GET['adurl']))
		system::moved($_GET['adurl']);
	system::moved($config_site_url);
}
$row = $db->r($res);
$account_id = $row["account_id"];
$budget = $row["budget"];
$dbudget = $row["dbudget"];
$bid = $row["bid"];
$type = $row["type"];
if ($type == "I")
	$url = $row["banner_a_href"];
else
	$url = $row["ad_url"];
$c_id = $row["c_id"];
$publisher = $row["publisher"];
$external = $row["external"];
$s_id = $row["s_id"];
$ip = account::getUserIp();
$budget_notify = $row["budget_notify"];

$flat_deal = ($row["flat_deal_status"] == 1);
$fd_id = $row["fd_id"];
$fd_type = $row["fd_type"];
if ($flat_deal && (!$fd_id || !$fd_type))
	$flat_deal = false;

if ($bid < 0.1)
	$bid = 0.1;

$go = !empty($row["customurl"]) ? $row["customurl"] : $url;

if ($account->isrealadmin())
	system::go($go, 'Here you go M!');

//if the same user/computer/IP address clicks multiple times on the same link it doesnt count
$db->q("INSERT IGNORE INTO advertise_ip (sid, ip_address) VALUES (?, ?)", array($s_id, $ip));
if ($db->affected < 1)
    system::moved($go);
if (isset($_COOKIE["as_ad_".$s_id]))
	system::moved($go);
ob_end_clean();
$account->setcookie("as_ad_".$s_id, 1, 3);

//compute earning and profit
$earning = $external ? number_format($bid * 0.75, 3, ".", "") : 0;
$profit = number_format($bid - $earning, 3, ".", "");

//adding stat for a zone
$db->q("INSERT INTO advertise_stat 
        (s_id, date, c, backup, profit, mobile, e) 
        VALUES 
        (?, CURDATE(), 1, 0, ?, ?, 0)
        ON DUPLICATE KEY UPDATE c = c + 1, profit = profit + ?, mobile = mobile + ?",
        array($s_id, $profit, $mob, $profit, $mob)
        );

//adding main stats entries
$browser = substr($_SERVER['HTTP_USER_AGENT'], 0, 250);
$db->q("INSERT INTO advertise_click 
		(account_id, c_id, s_id, ip_address, date, u, r, ad_url, browser, cost) 
		values 
		(?, ?, ?, ?, NOW(), ?, ?, ?, ?, ?)",
		array($account_id, $c_id, $s_id, $ip, $u, $r, $url, $browser, $profit));

$db->q("INSERT DELAYED INTO advertise_data 
		(s_id, date, c_id, click, cost, acpc, realsid) 
		VALUES
		(?, CURDATE(), ?, 1, ?, ?, ?) 
		ON DUPLICATE KEY UPDATE click = click + 1, cost = cost + ?, acpc = cost / click",
		array($cs_id, $c_id, $bid, $bid, $s_id, $bid)
		);


if ($flat_deal) {
	//this is flat deal, no publisher accounting and no budget substracting
	system::moved($go);
	die;
}


//-----
//money
//-----

//for external (client set up) advertising zones, track earning and income
if ($external) {
	$db->q("INSERT DELAYED INTO advertise_publish_data 
			(s_id, date, impression, click, earning) 
			VALUES 
			(?, CURDATE(), 1, 1, ?)
			ON DUPLICATE KEY UPDATE click = click + 1, earning = earning + ?",
			array($s_id, $earning, $earning)
			);
	$db->q("INSERT DELAYED INTO advertise_income 
			(account_id, income) 
			VALUES (?, ?)
			ON DUPLICATE KEY UPDATE income = income + ?",
			array($publisher, $earning, $earning)
			);
	$db->q("INSERT DELAYED INTO advertise_fraud 
			(s_id, time, account_id) 
			VALUES 
			(?, CURDATE(), ?)
			ON DUPLICATE KEY UPDATE click = click + 1, ctr = ROUND((click/impression)*100,4)",
			array($s_id, $publisher)
			);
}

//check daily budget
$rex = $db->q("SELECT sum(cost) cost FROM advertise_data WHERE c_id = ? AND `date` = CURDATE()", array($c_id));
if (!$db->numrows($rex)) {
	$m = "daily budget could not find for c_id $c_id";
	reportAdmin("AS: advertise bug", $m, array());
} else {
	$rox = $db->r($rex);
	$cost = $rox["cost"];
	if( (int)$cost >= $dbudget ) {
		$db->q("UPDATE advertise_camp_section SET budget_available = 0 WHERE c_id = ?", array($c_id));
	}
}

//updating current bid on zone
if ($row["currentbid"] != $bid)
	$db->q("UPDATE advertise_section SET currentbid = ? WHERE id = ?", [$bid, $s_id]);

//updating (substracting) budget
$db->q("UPDATE advertise_budget SET budget = if(budget - ? < 0, 0, ROUND(budget - ?, 3)) WHERE account_id = ?", array($bid, $bid, $account_id));

//if budget is zero now, disable campaign placements
if ($budget <= $bid)
	advertise::budgetZero($account_id);

//----------
// notifies
$notify = false;

//we notify only accounts that are not banned nor deleted
$res = $db->q("SELECT banned, deleted FROM account where account_id = ? LIMIT 1", array($account_id));
if ($db->numrows($res) == 1) {
	$row = $db->r($res);
	if ($row["banned"] == 0 && $row["deleted"] == NULL)
		$notify = true;
}

if ($notify) {
	//notify about no budget
	$res = $db->q("SELECT b.budget, b.recurring, b.last_notify_stamp, a.email 
					FROM advertise_budget b 
					INNER JOIN account a using (account_id) 
					WHERE b.account_id = ?",
					array($account_id));
	$row = $db->r($res);
	if ($row["budget"] < 0.2 && $budget > 0 && $row["recurring"] == 0 && ($row["last_notify_stamp"] == NULL || $row["last_notify_stamp"] < (time() - 3600))) {
		$advertise->emailNoBudget($row["email"]);
		$db->q("UPDATE advertise_budget SET last_notify_stamp = ? WHERE account_id = ?", array(time(), $account_id));
	}

	//notify about low budget
	if ($budget_notify > 0 && $budget >= $budget_notify && ($budget-$bid) <= $budget_notify && $row["recurring"] == 0) {
		$advertise->emailLowBudget($row["email"]);
	}
}

system::moved($go);
die;
?>
