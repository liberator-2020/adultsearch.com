<?php
/**
 * This is the script that is run when pop is executed - at the end there is redirect to pop URL
 */
global $db, $account, $mobile, $config_site_url;
$advertise = new advertise();

$log_ip = "197.85.41.100";

$sid = intval($_REQUEST["sid"]);
$u = GetGetParam("u");
$r = GetGetParam("r");

$mob = ($mobile == 1) ? 1 : 0;

//these two are set when backup URL is popping up in zone (when no campaign is currently advertising)
$backup_url = $_REQUEST["backup_url"];
$s_id = intval($_REQUEST["s_id"]);

$ip = account::getUserIp();

if ($ip == $log_ip)
	$h = fopen("/www/virtual/adts/www.adultsearch.com/html/epop.log","a");fputs($h,"\n\n---\n"."popup pop sid='{$sid}' backup_url='{$backup_url}'\n");fclose($h);

if (!$sid && (!$backup_url || !$s_id)) {
	//this should never happen under normal circumstances, just redirect to AS	
	system::moved($config_site_url);
}

if ($ip == $log_ip)
	$h = fopen("/www/virtual/adts/www.adultsearch.com/html/epop.log","a");fputs($h,"pop a\n");fclose($h);

$flat_deal = false;

if ($sid) {
	$res = $db->q("
SELECT ac.bid as lowbid, ac.bidx
	, s.id as sid, s.s_id, s.bid, s.flat_deal_status, s.customurl
	, c.account_id, c.budget dbudget, c.id c_id, c.ad_https, c.ad_url
	, b.budget budget, b.budget_notify
	, ads.currentbid, ads.external, ads.account_id publisher
	, fd1.id as fd1_id, fd1.type as fd1_type, fd1.pops_left as fd1_pops_left, fd1.next_deal_id as fd1_next_deal_id
	, d2_id, d2_type, d2_pops_left, d2_next_deal_id
FROM advertise_current ac 
INNER JOIN advertise_camp_section s on ac.s_id = s.id 
INNER JOIN advertise_campaign c on s.c_id = c.id 
INNER JOIN advertise_budget b on b.account_id = c.account_id 
INNER JOIN advertise_section ads on s.s_id = ads.id

LEFT JOIN advertise_flat_deal fd1 on fd1.account_id = c.account_id AND fd1.zone_id = ads.id AND fd1.status = 1 AND ( fd1.type = 'ZP' OR fd1.type = 'ZT' )

LEFT JOIN 
  (SELECT afd.id as d2_id, afd.type as d2_type, afd.account_id as d2_account_id, afd.pops_left as d2_pops_left, afd.next_deal_id as d2_next_deal_id
    , adz.id as adz_id, adz.zone_id as adz_zone_id
   FROM advertise_flat_deal afd
   INNER JOIN advertise_deal_zone adz ON adz.deal_id = afd.id
   WHERE afd.status = 1 AND (afd.type = 'ZP' OR afd.type = 'ZT')
  ) d2
  ON d2.d2_account_id = c.account_id AND d2.adz_zone_id = s.s_id

WHERE ac.id = ?
		",
		[$sid]
		);
	
	if (!$db->numrows($res)) {

		if ($ip == $log_ip)
			$h = fopen("/www/virtual/adts/www.adultsearch.com/html/epop.log","a");fputs($h,"not found in advertise_current!\n");fclose($h);

		global $gIndexTemplate;
		$gIndexTemplate = "blank.tpl";
		debug_log("promo/p: Error: id '{$sid}' not found in advertise_current ! u='{$u}', r='{$r}'");
		if (array_key_exists('HTTP_REFERER', $_SERVER) && $_SERVER['HTTP_REFERER'])
			debug_log("REFERER='".$_SERVER['HTTP_REFERER']."'");
		echo "<script type='text/javascript'>window.close();</script>";
		return;
		reportAdmin("AS: POPUP error clickthrough on $sid", "", array("sid" => $sid));
		system::moved($config_site_url);
	}
	$row = $db->r($res);
	$account_id = $row["account_id"];
	$budget = $row["budget"];
	$dbudget = $row["dbudget"];
	$bid = $row["currentbid"]>$row["bid"]?$row["bid"]:$row["currentbid"];
	$bid = $row["bidx"];
	$http = $row["ad_https"] ? "https://" : "http://";
	$url = $row["ad_url"];
	$c_id = $row["c_id"];
	$publisher = $row["publisher"];
	$external = $row["external"];
	$s_id = $row["s_id"];
	$budget_notify = $row["budget_notify"];

	$flat_deal = ($row["flat_deal_status"] == 1);
	$fd_id = $fd_type = $fd_left = $next_deal_id = null;
	if ($row["fd1_id"] && $row["fd1_type"]) {
		$fd_id = $row["fd1_id"];
		$fd_type = $row["fd1_type"];
		$fd_left = $row["fd1_pops_left"];
		$next_deal_id = $row["fd1_next_deal_id"];
/*
	} else if ($row["fd2_id"] && $row["fd2_type"]) {
		$fd_id = $row["fd2_id"];
		$fd_type = $row["fd2_type"];
		$fd_left = $row["fd2_pops_left"];
		$next_deal_id = $row["fd2_next_deal_id"];
*/
	} else if ($row["d2_id"] && $row["d2_type"]) {
		$fd_id = $row["d2_id"];
		$fd_type = $row["d2_type"];
		$fd_left = $row["d2_pops_left"];
		$next_deal_id = $row["d2_next_deal_id"];
	}
	if ($flat_deal && (!$fd_id || !$fd_type)) {
//		debug_log("flat_deal={$flat_deal}, fd_id={$fd_id}, fd_type={$fd_type}, row=".print_r($row, true));
		$flat_deal = false;
	}

	if (strstr($url, $http))
		$http = "";

	if (substr($url, 0, 4) != "http")
		$url = $http.$url;

	$go = !empty($row["customurl"]) ? $row["customurl"] : ($url);

	if ($ip == $log_ip)
		$h = fopen("/www/virtual/adts/www.adultsearch.com/html/epop.log","a");fputs($h,"popping go='{$go}' c_id={$c_id} bid={$bid}\n");fclose($h);

	$earning = $external?number_format($bid*0.75, 3):0;
	$profit = number_format($bid-$earning,3);

	$db->q("INSERT INTO advertise_stat 
			(s_id, date, c, backup, profit, mobile, e) 
			values 
			(?, CURDATE(), 1, 0, ?, ?, 0)
			ON DUPLICATE KEY UPDATE c = c + 1, profit = profit + ?, mobile = mobile + ?",
			[$s_id, $profit, $mob, $profit, $mob]
			);

} else {
	//this is when backup_url is set
	$go = $backup_url;
	$profit = 0;
	$earning = 0;
	$publisher = 0;

	$db->q("INSERT INTO advertise_stat 
			(s_id, date, c, backup, profit, mobile, e) 
			values 
			(?, CURDATE(), 0, 1, 0, ?, 0)
			ON DUPLICATE KEY UPDATE backup = backup + 1, mobile = mobile + ?",
			[$s_id, $mob, $mob]
			);
}

if ($external) {
	if ($ip == $log_ip)
		$h = fopen("/www/virtual/adts/www.adultsearch.com/html/epop.log","a");fputs($h,"ext stats\n");fclose($h);

	$db->q("INSERT INTO advertise_publish_data 
			(s_id, date, impression, click, earning) 
			values 
			(?, CURDATE(), 1, 1, ?) 
			ON DUPLICATE KEY UPDATE click = click + 1, earning = earning + $earning",
			array($s_id, $earning)
			);

	$db->q("INSERT INTO advertise_income (account_id, income) 
			VALUES 
			(?, ?)
			ON DUPLICATE KEY UPDATE income = income + $earning",
			array($publisher, $earning)
			);

	$db->q("INSERT INTO advertise_fraud 
			(s_id, time, account_id) 
			VALUES 
			(?, CURDATE(), ?)
			ON DUPLICATE KEY UPDATE click = click + 1, ctr = ( click / impression ) * 100",
			array($s_id, $publisher)
			);
}

ob_end_clean();


if ($sid) {
	//notifying campaign click and substracting budget and notifying advertiser is only valid when real campaign is popping (not when backup url is popping)
	if (!$u)
		$u = "";
	if (!$r)
		$r = "";
	$browser = substr(classifieds::onlyPrintableCharacters($_SERVER['HTTP_USER_AGENT']), 0, 255);
	$db->q("INSERT INTO advertise_click 
			(account_id, c_id, s_id, ip_address, date, u, r, ad_url, browser, cost) 
			values 
			(?, ?, ?, ?, NOW(), ?, ?, ?, ?, ?)",
			array($account_id, $c_id, $s_id, $ip, $u, $r, $url, $browser, $profit));

	$db->q("INSERT INTO advertise_data 
			(s_id, date, c_id, click, cost, acpc, realsid) 
			values 
			('{$row["sid"]}', curdate(), '$c_id', 1, '$bid', '$bid', '$s_id') 
			on duplicate key update click=click+1, cost=cost+'$bid', acpc=cost/click");

	if ($ip == $log_ip)
		$h = fopen("/www/virtual/adts/www.adultsearch.com/html/epop.log","a");fputs($h,"aft int stats\n");fclose($h);

	$rex = $db->q("select sum(cost) cost from advertise_data where c_id = '$c_id' and date = curdate()");
	if( !$db->numrows($rex) ) {
		$m = "daily budget could not find for c_id $c_id";
		reportAdmin("AS: advertise bug", $m);
	} else {
		$rox = $db->r($rex);
		$cost = $rox["cost"];
		if( (int)$cost >= $dbudget ) {
			$db->q("update advertise_camp_section set budget_available = 0 where c_id = '$c_id'");
		}
	}

	if ($row["currentbid"] != $bid)
		$db->q("update advertise_section set currentbid = '$bid' where id = '$s_id'");


	if (!$flat_deal) {

		if ($budget) {

			//updating (substracting) budget
			$db->q("UPDATE advertise_budget SET budget = IF(budget - $bid < 0, 0, budget - $bid) WHERE account_id = '$account_id'");

			//if budget is zero now, disable campaign placements
			if ($budget <= $bid) {
				advertise::budgetZero($account_id);
//				reportAdmin("AS: pop advertiser went out of budget", "Advertiser: #{$account_id} went out of budget");
			}

			//----------
			// notifies
			$notify = false;

			//we notify only accounts that are not banned nor deleted
			$res = $db->q("SELECT banned, deleted FROM account where account_id = ? LIMIT 1", array($account_id));
			if ($db->numrows($res) == 1) {
				$row = $db->r($res);
				if ($row["banned"] == 0 && $row["deleted"] == NULL)
					$notify = true;
			}

			if ($notify) {
				//notify no budget
				$res = $db->q("SELECT b.budget, b.recurring, b.last_notify_stamp, a.email 
								FROM advertise_budget b 
								INNER JOIN account a using (account_id) 
								WHERE b.account_id = ?", array($account_id));
				$row = $db->r($res);
				if ($row["budget"] < $bid && $budget > 0 && $row["recurring"] == 0 && ($row["last_notify_stamp"] == NULL || $row["last_notify_stamp"] < (time() - 3600))) {
					$advertise->emailNoBudget($row["email"]);
					$db->q("UPDATE advertise_budget SET last_notify_stamp = ? WHERE account_id = ?", array(time(), $account_id));
				}

				//notify low budget
				if ($budget_notify > 0 && $budget >= $budget_notify && ($budget-$bid) <= $budget_notify) {
					$advertise->emailLowBudget($row["email"]);
				}
			}
		}

	} else {
		//this is flat deal, do the accounting of pop
		
		if ($fd_type == "ZP") {
			//substracting impressions from zone-impressions deal
			$db->q("UPDATE advertise_flat_deal SET pops_left = IF(pops_left-1>0, pops_left-1, 0) WHERE id = ? LIMIT 1", array($fd_id));

			//pausing flat deal after finishing
			if ($fd_left <= 0) {
				$db->q("UPDATE advertise_flat_deal SET status = 0, finished_stamp = ? WHERE id = ?", array(time(), $fd_id));
				file_log("advertise", "Pop flat deal #{$fd_id} is finished");
				$advertise->flatDealStatusUpdate($fd_id);

				//we dont pause flat deal campaigns anymore, disabling placements from flat deal is enough
				//$advertise->flatDealCampaigns($fd_id);

				//if there is next deal starting, start it
				if ($next_deal_id) {
					file_log("advertise", "Starting next flat deal #{$next_deal_id}");
					$db->q("UPDATE advertise_flat_deal SET status = 1, started_stamp = ? WHERE id = ?", array(time(), $next_deal_id));
					$advertise->flatDealStatusUpdate($next_deal_id);
				}

				reportAdmin("AS: Flat deal finished", "Check if campaigns are properly paused !!! Check if next deal #{$next_deal_id} properly started !!!", array("fd_id" => $fd_id, "next_deal_id" => $next_deal_id));
			}

		} else if ($fd_type == "ZT") {
			//zone time flat deal type, we don't need to do anything here
		} else {
			debug_log("advertise/popup: Error: Unknown flat deal type '{$fd_type}' !");
		}

	}

}

system::moved($go);
die;
?>
