<?php

ob_end_clean();
header("Cache-Control: no-cache, must-revalidate"); 
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

global $db, $smarty, $mobile, $advertise, $sphinx;

$sphinx_index = "advertise";
$sphinx->reset();

$loc_id = isset($_GET["loc_id"])&&intval($_GET["loc_id"]) ? 1 : NULL;
$s = isset($_GET["s"])&&intval($_GET["s"]) ? intval($_GET["s"]) : NULL;
$u = GetGetParam("u");
$r = GetGetParam("r");
$pp = isset($_GET["pp"])?true:NULL;
$loc_name = isset($_GET["ad_loc"]) ? GetGetParam("ad_loc") : NULL;
$ad_query = isset($_GET['ad_query']) && !empty($_GET['ad_query']) ? GetGetParam('ad_query') : NULL;

if (!$s)
	die;

if( !is_null($loc_id) && is_null($pp) ) {
	$locs = array();
	foreach($_GET["loc_id"] as $loc) {
		$loc = intval($loc);
		if( $loc ) $locs[] = $loc;
	}
	if( !empty($locs) ) $sphinx->SetFilter('dont', $locs, true);
}

//jay 25.6.2016 - dallas request skype - on hot display AS text ads
if ($s == 10008)
	$sphinx->SetFilter('s_id', array(16));
else
	$sphinx->SetFilter('s_id', array($s));

$shorturl = false;
$line_limit = 0;
if( !is_null($pp) && $s >= 10000) {
	$ad_width = GetGetParam("ad_width");
	$ad_height = getGetParam("ad_height");
	$color_bg = GetGetParam("color_bg");
	$color_border = GetGetParam("color_border");
	$color_link = GetGetParam("color_link");
	$color_text = GetGetParam("color_text");
	$color_url = GetGetParam("color_url");


	$res = $db->q("select backup, backuphtml, backupcolor, active, parent, external, account_id, newwindow, advertisehere from advertise_section where id = '$s'");
	if( !$db->numrows($res) ) die;
	$row = $db->r($res);
	$smarty->assign("backup", $row["backup"]);
	if( $row["backup"] == 1 ) $smarty->assign("backuphtml", $row["backuphtml"]);
	elseif( $row["backup"] == 2 ) $smarty->assign("backupcolor", $row["backupcolor"]);

	if( $row["newwindow"] == 1 ) $smarty->assign("newwindow", true);
	if( $row["advertisehere"] == 0 ) $smarty->assign("dont_advertisehere", true);

	if( $row["active"] == 0 ) {
		$db->q("update LOW_PRIORITY advertise_section set active = 1 where id = '$s'");
		$db->q("update LOW_PRIORITY advertise_section set active = 1 where id = '{$row["parent"]}'");
	}

	if( $row["external"] ) {
		$db->q("insert delayed into advertise_fraud (s_id, time, account_id) values ('$s', curdate(), '{$row["account_id"]}') on duplicate key update impression=impression+1");
	}

	$smarty->assign("ad_width", $ad_width);
	$smarty->assign("ad_height", $ad_height);
	$smarty->assign("color_bg", $color_bg);
	$smarty->assign("color_border", $color_border);
	$smarty->assign("color_link", $color_link);
	$smarty->assign("color_text", $color_text);
	$smarty->assign("color_url", $color_url);

	if( $ad_width == 160 && $ad_height == 600 ) {
		/* 7 ads */
		$display = _CMS_ABS_PATH."/templates/advertise/advertise_publisher160x600.tpl";
		$line_limit = 20;
		$limit = 5;
	}
	else if( $ad_width == 200 && $ad_height == 600 ) {
		/* 7 ads */
		$display = _CMS_ABS_PATH."/templates/advertise/advertise_publisher200x600.tpl";
		$line_limit = 29;
		$limit = 7;
	}
	else if( $ad_width == 120 && $ad_height == 600 ) {
		$display = _CMS_ABS_PATH."/templates/advertise/advertise_publisher120x600.tpl";
		$limit = 5;
		$shorturl = true;
	}
	else if( $ad_width == 180 && $ad_height == 400 ) {
		/* 5 ads */
		$display = _CMS_ABS_PATH."/templates/advertise/advertise_publisher180x400.tpl";
		$limit = 5;
	}
	else if( $ad_width == 468 && $ad_height == 60 ) {
		$display = _CMS_ABS_PATH."/templates/advertise/advertise_publisher468x60.tpl";
		$limit = 2;
	}
	else if( $ad_width == 728 && $ad_height == 90 ) {
		$display = _CMS_ABS_PATH."/templates/advertise/advertise_publisher728x90.tpl";
		$limit = 3;
	}
	else if( $ad_width == 900 && $ad_height == 90 ) {
		$display = _CMS_ABS_PATH."/templates/advertise/advertise_publisher900x90.tpl";
		$limit = 4;
	}
	else if( $ad_width == 300 && $ad_height == 250 ) {
		/* 4 ads */
		$display = _CMS_ABS_PATH."/templates/advertise/advertise_publisher300x250.tpl";
		$limit = 4;
	} else {
		/* default */
		if ($s == 10008) {
			//hot.com uses customizable template for red colors
			$smarty->assign("color_link", "C5000C");
		    $smarty->assign("color_url", "660000");
			$display = _CMS_ABS_PATH."/templates/advertise/advertise_show_customizable.tpl";
		} else {
			$display = _CMS_ABS_PATH."/templates/advertise/advertise_show.tpl";
		}
	}
} else {
	$display = _CMS_ABS_PATH."/templates/advertise/advertise_show.tpl";
	$limit = 8;
}

if( !$limit ) $limit = 7;
$limit2 = $limit*2;
$sphinx->SetLimits( 0, $limit2, $limit2);
$sphinx->SetSortMode(SPH_SORT_ATTR_DESC, "bid");
$results = $sphinx->Query ( "", $sphinx_index );

$total = $results["total_found"];
$result = $results["matches"];

/* failover */
$result2 = NULL;
if( !$total || $total < $limit) {
	$sphinx->ResetFilter("s_id");
	$sphinx->SetFilter('s_id', array(10050));
	$results = $sphinx->Query ( "", $sphinx_index );
	$total += $results["total_found"];
	$result2 = $results["matches"];
}

if ($total) {

	if( !is_null($pp) && $s >= 10000) {
		$db->q("INSERT INTO advertise_publish_data (s_id, date, impression) values ('$s', curdate(), 1) on duplicate key update impression = impression + 1");
	}

	$ids = "";
	foreach($result as $res) {
		$ids .= $ids ? (",".$res["id"]) : $res["id"];
	}
	if( !is_null($result2) && is_array($result2) ) {
		foreach($result2 as $res) {
			$ids .= $ids ? (",".$res["id"]) : $res["id"];
		}
	}

	$res = $db->q("select s.id sid, s.bid, c.* from advertise_camp_section s inner join advertise_campaign c on s.c_id = c.id where s.id in ($ids) order by field(s.id,$ids)");
	$ad = array();
	$reserve = $hosts = array();
	while($row=$db->r($res)) {

		$bid = $row["bid"];

		if( !is_null($loc_name) ) {
			$ad_title = str_replace("{location}", $loc_name, $row["ad_title"]);
			$ad_line1 = str_replace("{location}", $loc_name, $row["ad_line1"]);
			$ad_line2 = str_replace("{location}", $loc_name, $row["ad_line2"]);
			$ad_durl = str_replace("{location}", $loc_name, $row["ad_durl"]);
		} else {
			$ad_title = str_replace("{location} ", $loc_name, $row["ad_title"]);
			$ad_line1 = str_replace("{location} ", $loc_name, $row["ad_line1"]);
			$ad_line2 = str_replace("{location} ", $loc_name, $row["ad_line2"]);
			$ad_durl = str_replace("{location}", $loc_name, $row["ad_durl"]);
		}

		if( $line_limit && strlen($ad_line1) > $line_limit ) {
			$pos = strrpos(substr($ad_line1, 0, $line_limit), ' ');
			$ad_line1 = substr($ad_line1, 0, $pos) . "<br>" . substr($ad_line1, $pos+1);
		}
		//if( $s == 10008 ) $ad_line1 = "$ad_width x $ad_height";

		$ad_title = ucwords(strtolower($ad_title));

		if( !is_null($ad_query) ) {
			$ad_title = $advertise->boldquery($ad_title, $ad_query);
			$ad_line1 = $advertise->boldquery($ad_line1, $ad_query);
			$ad_line2 = $advertise->boldquery($ad_line2, $ad_query);
		}

		if( $shorturl && strlen($ad_durl) > 20 && strstr($ad_durl, 'www.') ) $ad_durl = str_replace('www.', '', $ad_durl); 

		$hx = strncmp($row["ad_url"], "http", 4) ? ("http://".$row["ad_url"]) : $row["ad_url"];
		$host = parse_url($hx, PHP_URL_HOST);

		$break = 0;
		if( count($hosts) ) {
			foreach($hosts as $h) {
				if( $h["account_id"] == $row["account_id"] && $h["host"] == $host ) {
					$reserve[] = array(
					   	"id"=>$row["sid"],
						"sid"=>$row["id"],
						"ad_title"=>$ad_title,
				  	 	"ad_line1"=>$ad_line1,
						"ad_line2"=>$ad_line2,
						"ad_durl"=>$ad_durl,
						"ad_url"=>rawurlencode(($row["ad_https"]?"https://":"http://").$row["ad_url"]),
						"bid"=>(float)$row["bid"],
						"account_id"=>$row["account_id"]
						);
					$break = 1;
					break;
				}
			}
		}

		if ($break)
			continue;

		$hosts[] = array("host"=>$host, "account_id"=>$row["account_id"]);

		$ad[] = array(
			"id"=>$row["sid"],
			"sid"=>$row["id"],
			"ad_title"=>$ad_title,
			"ad_line1"=>$ad_line1,
			"ad_line2"=>$ad_line2,
			"ad_durl"=>$ad_durl,
			"ad_url"=>rawurlencode(($row["ad_https"]?"https://":"http://").$row["ad_url"]),
			"bid"=>(float)$row["bid"],
			"account_id"=>$row["account_id"]
		);
		
		if (count($ad) >= $limit)
			break;

	}

	$resort = 0;
	if( count($ad) < $limit && count($reserve) ) {
		$l = count($ad);
		for($i=$l;$i<$limit;$i++) {
			foreach($reserve as $row) {
				$ad[] = array(
					"id"=>$row["id"],
					"sid"=>$row["sid"],
					"ad_title"=>$row["ad_title"],
					"ad_line1"=>$row["ad_line1"],
					"ad_line2"=>$row["ad_line2"],
					"ad_durl"=>$row["ad_durl"],
					"ad_url"=>$row["ad_url"],
					"bid"=>(float)$row["bid"],
					"account_id"=>$row["account_id"]
				);
				$resort = 1;
				if( count($ad) == $limit ) { 
					$i = 100;
					break;
				}
			}
		}
	}

	if( $resort ) {
		$ad = $db->arraysort($ad, "bid", 1);
	}

	//BTW: in $bid variable we have lowest bid from ads being displayed
	for($i=0;$i<count($ad);$i++) {
		$bidx = $bid + (($limit-$i) / 100);						//we add 1 cent for each position towards the top
		if( $bidx > $ad[$i]["bid"] ) $bidx = $ad[$i]["bid"];	//but only if the actual(lowest) bid + this position cents is smaller than offered bid
		$db->q("insert into advertise_current (s_id, bid, bidx, stamp) values (?, ?, ?, ?)", array($ad[$i]["id"], $bid, $bidx, time()));
		$current = $db->insertid;
		$ad[$i]["current"] = $current;

		$db->q("INSERT INTO advertise_data 
				(s_id, date, c_id, impression, realsid) 
				values 
				('{$ad[$i]["id"]}', curdate(), (select c_id from advertise_camp_section where id = '{$ad[$i]["id"]}' limit 1), 1, '$s') 
				on duplicate key update impression = impression + 1");

		$ad[$i]["bid"] = (int)($ad[$i]["bid"]*100);

	}
}

if( isset($_GET['al']) ) {
	$al = intval($_GET['al']);
	if( $al < $limit ) {
		if( $al == 1 ) {
			$rand = rand(0, count($ad)-1);
			$rad = $ad[$rand];
			$ad = NULL; $ad[] = $rad;
		} else {
			for($i=$al;$i<$limit-1;$i++) {
				unset($ad[$i]);
			}
		}
	}
}

$smarty->assign("ad", $ad);
$smarty->assign("u", $u);
$smarty->assign("r", $r);
$smarty->assign("s", $s);

$smarty->display($display);

//_darr($ad);
die;

?>
