<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;
$system = new system;

$account->checkMainHost();

if( !($account_id = $account->isloggedin())) {
        $account->asklogin();
        return;
}

$id = GetGetParam("id");
if( !$id ) $system->moved("/publisher/");

$res = $db->q("select * from advertise_section where id = '$id' and account_id = '$account_id'");
if( !$db->numrows($res) ) {
	reportAdmin("publisher - approve return 0");
	$system->moved("/publisher/");
} $row = $db->r($res); 

if( isset($_POST["action"]) ) {
	foreach($_POST as $key=>$value) {
		if( strlen($key)>6 && !strncmp($key, "action", 6) ) {
			$idx = intval(substr($key, 6));
			$value = intval($value);
			if( $idx && $value ) {
				if( $value == 1 ) {
					$db->q("update advertise_camp_section set approved = 1 where id = '$idx' and s_id = '$id'");
					$db->q("delete from advertise_notification where account_id = '$account_id' and s_id = '$id'");
				}
				else {
					$reason = GetPostParam("reason".$idx);
					if( empty($reason) ) {
						$smarty->assign("error", "You need to write the reason");
						$smarty->assign("idx", $idx);
					}
					else {
						$db->q("update advertise_camp_section set approved = -1, reason = '$reason' where id = '$idx' and s_id = '$id'");
						if( $db->affected ) {
							$rex = $db->q("select a.email from advertise_camp_section s inner join advertise_campaign c on s.c_id = c.id inner join account a on c.account_id = a.account_id where s.id = '$idx'");
							if( $db->numrows($rex) ) {
								$rox = $db->r($rex); 
								$db->q("delete from advertise_notification where account_id = '$account_id' and s_id = '$id'");
 								$m = "Dear Advertiser,<br><br>One of your campaigns have been declined by one of our external websites. <br><br>Reason: <b>$reason</b>.<br><br>If you would still like to advertise, please revise your ad and make the necessary changes.<br><br>You may see your current ads at <a href='http://adultsearch.com/advertise/'>http://adultsearch.com/advertise/</a><br><br>Best,<br>adultsearch.com";
								sendEmail("support@adultsearch.com", "AdultSearch - Advertising Notification", $m, $rox["email"]);
							}
						}
					}
				}
			}
		}
	}
}

$ads = array();
$res = $db->q("select s.id, c.ad_title, c.ad_line1, c.ad_line2, c.ad_durl, c.ad_https, c.ad_url from advertise_camp_section s inner join 
advertise_campaign c on (s.c_id = c.id and c.status=1) where s.s_id = '$id' and s.status=1 and s.approved=0 and s.budget_available=1 and s.`show`=1");
while($row=$db->r($res)) {
	$ads[] = array(
		"id"=>$row["id"],
		"ad_title"=>$row["ad_title"],
		"ad_line1"=>$row["ad_line1"],
		"ad_line2"=>$row["ad_line2"],
		"ad_durl"=>$row["ad_durl"],
		"ad_https"=>$row["ad_https"],
		"ad_url"=>$row["ad_url"]
	);
} 

$smarty->assign("ads", $ads);
$smarty->display(_CMS_ABS_PATH."/templates/publisher/publisher_approve.tpl");

?>
