<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $gTitle, $sphinx;
$system = new system;

$gTitle = "Publisher Account";

if( !($account_id = $account->isloggedin())) {
	$account->asklogin();
	return;
}

if( !empty($_POST) ) {
	$action = GetPostParam("action");
	$chk = $_POST["chk"];
	switch($action) {
		case 'Pause':
			foreach($chk as $id) {
				$id = intval($id);
				$db->q("update advertise_section set active = 0 where id = '$id' and account_id = '$account_id'");
			}
			break;
		case 'Activate':
			foreach($chk as $id) {
				$id = intval($id);
				$db->q("update advertise_section set active = 1 where id = '$id' and account_id = '$account_id'");
				if( $db->affected>0 ) {
					$rex = $db->q("SELECT c.*, s.section section2, concat(s2.name,' - ',s.n) n
									FROM advertise_camp_section c
									INNER JOIN (advertise_section s inner join advertise_section s2 on s.parent = s2.id) on s.id = '$s_id'
									WHERE c.s_id = (select parent from advertise_section where id = '$s_id') and recursion = 1");
					while($rox=$db->r($rex)) {
						$db->q("INSERT INTO advertise_camp_section 
								(c_id, s_id, section, status, name, approved, budget_available, `show`)
								values 
								('{$rox["c_id"]}', '$s_id', '{$rox["section2"]}', '{$rox["n"]}', '{$rox["approved"]}', '{$rox["budget_available"]}', '{$rox["show"]}')
								on duplicate key update `show` = '{$rox["show"]}', name = '{$rox["x"]}', approved = '{$rox["approved"]}'");
						reportAdmin("publisher auto added for $s_id");
					} 
				}
			}
			break;
		case 'Delete':
			foreach($chk as $id) {
				$id = intval($id);
				$db->q("update advertise_section set active = -1 where id = '$id' and account_id = '$account_id'");
				if( $db->affected>0 ) {
					$db->q("update advertise_camp_section set approved=0 where s_id = '$id'");
				}
			}
			break;
	}
	$system->moved("/publisher/");
}

//get sphinx advertise data
$sphinx->reset();
$sphinx->SetLimits(0, 1000, 1000);
$results = $sphinx->Query("", "advertise");
$total = $results["total_found"];
$result = $results["matches"];
$zones_campaigns = array();
foreach ($results["matches"] as $match) {
    $zone_id = $match["attrs"]["s_id"];
    $type = $match["attrs"]["type"];
    $campaign_id = $match["attrs"]["c_id"];
    $bid = $match["attrs"]["bid"];
    $cpm =  $match["attrs"]["cpm"];

    if ($type == "B")
        $b = $cpm;
    else
        $b = $bid;

    if (!array_key_exists($zone_id, $zones_campaigns)) {
        $zones_campaigns[$zone_id] = array($campaign_id => $b);
    } else {
        $zone_arr = $zones_campaigns[$zone_id];
        $zone_arr[$campaign_id] = $b;
        $zones_campaigns[$zone_id] = $zone_arr;
    }
}


//get appwaiting numbers
$zones_appwaiting = array();
$res = $db->q("select s.id as id, count(distinct a.cs_id) as appwaiting
				from advertise_section s
				left join (select cs.id as cs_id, cs.s_id
				    from advertise_camp_section cs
				    inner join advertise_campaign c on cs.c_id = c.id and c.status = 1
				    where cs.status=1 and cs.approved=0 and cs.budget_available=1 and cs.`show`=1)
				    a on a.s_id = s.id
				where s.account_id = ?
				group by s.id",
				array($account_id));
while ($row = $db->r($res)) {
    $zone_id = $row["id"];
    $appwaiting = $row["appwaiting"];
    $zones_appwaiting[$zone_id] = $appwaiting;
}


$res = $db->q("select income from advertise_income where account_id = '$account_id'");
$row = $db->r($res);
$smarty->assign("income", number_format($row["income"], 2));

$res = $db->q("SELECT day(date), month(date), year(date)
				FROM advertise_section s
				INNER JOIN advertise_publish_data d on s.id = d.s_id
				WHERE s.account_id = '$account_id'
				ORDER BY date
				LIMIT 1");
if( $db->numrows($res) ) {
	$row = $db->r($res);
	$dayFrom = $row[0];
	$monthFrom = $row[1];
	$yearFrom = $row[2];
} else {
	$dayFrom = date("d");
	$monthFrom = date("m");
	$yearFrom = date("Y");
}

$range_or_period = GetGetParam("range_or_period");
if( $range_or_period != 2 ) {
	$dayTo = date("d");
	$monthTo = date("m");
	$yearTo = date("Y");
	$period = $_GET["period"];
	switch($period) {
		case 1:
			$time_sql = "and (date >= date_sub(curdate(), interval 6 day) or d.s_id is null)";
			$time_sql = "and date >= date_sub(curdate(), interval 6 day)";
			break;
		case 2:
			$time_sql = "and date = date_sub(curdate(), interval 1 day)";
			break;
		case 3:
			$time_sql = "and date >= date_sub(curdate(), interval 29 day)";
			break;
		case 4:
			getMonthRange($start, $end, 0);
			$time_sql = "and date >= '$start' and date <= '$end'";
			break;
		case 5:
			getMonthRange($start, $end, 1);
			$time_sql = "and date >= '$start' and date <= '$end'";
			break;
		default:
			$time_sql = "and date = curdate()";
	}
} else {
	$dayFrom = GetGetParam("dayFrom");
	$monthFrom = GetGetParam("monthFrom");
	$yearFrom = GetGetParam("yearFrom");

	$dayTo = GetGetParam("dayTo");
	$monthTo = GetGetParam("monthTo");
	$yearTo = GetGetParam("yearTo");

	$start_timer = mktime(0,0,1,$monthFrom,$dayFrom,$yearFrom);
	$end_timer = mktime(0,0,1, $monthTo, $dayTo, $yearTo);

	$start = date("Y-m-d", $start_timer);
	$end = date("Y-m-d", $end_timer);

	$time_sql = "and date >= '$start' and date <= '$end'";

	if( $start_timer > $end_timer )
		$smarty->assign("report_error", "Wrong date selection.");
}

$res = $db->q("SELECT s.*, sum(impression) impression, sum(click) as click, sum(earning) as earning
				FROM advertise_section s
				LEFT JOIN advertise_publish_data d on (s.id = d.s_id $time_sql)
				WHERE s.account_id = '$account_id' and s.active > -1 and s.parent != 0
				GROUP BY s.id");
while($row=$db->r($res)) {
	$name = !empty($row["name"]) ? "{$row["name"]} - {$row["n"]}" : $row["n"];
	$campaign[] = array(
		"id"=>$row["id"],
		"name"=>$name,
		"d"=>$row["d"],
		"active"=>$row["active"],
		"currentad" => intval(count($zones_campaigns[$row["id"]])),
		"appwaiting" => intval($zones_appwaiting[$row["id"]]),
		"impression"=>number_format($row["impression"]),
		"click"=>number_format($row["click"]),
		"ctr"=>number_format(($row["click"]/$row["impression"])*100, 2),
		"earning"=>number_format($row["earning"], 2),
	);
}

$smarty->assign("dayFrom", $dayFrom);
$smarty->assign("monthFrom", $monthFrom);
$smarty->assign("yearFrom", $yearFrom);
$smarty->assign("dayTo", $dayTo);
$smarty->assign("monthTo", $monthTo);
$smarty->assign("yearTo", $yearTo);
$smarty->assign("period", $_GET["period"]);
$smarty->assign("range_or_period", $_GET["range_or_period"]);

$smarty->assign("campaign", $campaign);
$smarty->display(_CMS_ABS_PATH."/templates/publisher/publisher_main.tpl");

?>
