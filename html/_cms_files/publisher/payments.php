<?php
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;

if( !($account_id = $account->isloggedin())) {
		$account->asklogin();
		return;
}

$res = $db->q("select income from advertise_income where account_id = '$account_id'");
if( $db->numrows($res) ) {
	$row = $db->r($res); 
	$income = number_format($row[0], 2);
} else $income = "0.00";
$smarty->assign("income", $income);

$res = $db->q("select p.*, date_format(time, '%b %e, %Y') as time from account_payroll p where account_id = '$account_id' order by id desc");
while($row=$db->r($res)) {
	$p[] = array(
		"id"=>$row["id"],
		"time" => $row["time"],
		"amount" => $row["amount"],
		"to" => $row["to"],
		"made"=> $row["made"]==1?"Sent":"In Progress...",
		"track"=> $row["track"] ? $row["track"] : "N/A"
	);
}
$smarty->assign("p", $p);

$smarty->display(_CMS_ABS_PATH."/templates/publisher/publisher_payments.tpl");

?>
