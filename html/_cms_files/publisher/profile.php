<?php

global $db, $smarty, $account;
$system = new System;


if( !($account_id = $account->isloggedin())) {
		$account->asklogin();
		return;
}

$res = $db->q("select income from advertise_income where account_id = '$account_id'");
if( !$db->numrows($res) ) {
		$system = new system;
		$system->moved("/publisher/");
}
$row = $db->r($res); 

if( isset($_POST["profile_change"]) ) {
		$address = GetPostParam("address");
		$zip = GetPostParam("zip"); $zip = preg_replace("/[^0-9a-z]/i", "", $zip);
		$payout = GetPostParam("payout");
		$paypal = GetPostParam("paypal");
		$payableto = GetPostParam("payableto");
		$billingaddr = GetPostParam("billingaddr");
		$ssn = GetPostParam("ssn");

	if( !empty($zip) ) {
		$res = $db->q("select loc_id from core_zip where zip = '$zip'");
		$found = $db->numrows($res); $rox = $db->r($res); 
		$loc_id = $rox[0];
	} else $found = true;
	if( empty($zip) ) $smarty->assign("error", "Please type your zip code");
		else if( !in_array($payout, array(1,3) ) )
				$smarty->assign("error", "Please select a payout method");
		else if( $payout == 1 && empty($paypal) )
				$smarty->assign("error", "Please type your paypal email address.");
		else if( $payout == 3 && (empty($payableto) || empty($billingaddr)) )
				$smarty->assign("error", "Please fill out 'Company Name / Personal Name' to and billing address part");
		else {
				$db->q("update advertise_income set loc_id='$loc_id', address='$address', zip='$zip', ssn='$ssn', payment_type='$payout', paypal='$paypal', 
			payableto='$payableto',  address='$billingaddr'  where account_id = '$account_id'");
		$smarty->assign("success", "Updated.");
		}
}

$res = $db->q("select a.*, z.city, z.state from advertise_income a left join core_zip z using (zip) where a.account_id = '$account_id'");
if (!$db->numrows($res) ) $system->moved("/");
$row = $db->r($res);

$smarty->assign("ssn", $row["ssn"]);
$smarty->assign("zip", $row["zip"]);
$smarty->assign("payout", $row["payment_type"]);
$smarty->assign("paypal", $row["paypal"]);
$smarty->assign("payableto", $row["payableto"]);
$smarty->assign("billingaddr", $row["address"]);
$smarty->assign("city", $row["city"]);
$smarty->assign("state", $row["state"]);

$smarty->display(_CMS_ABS_PATH."/templates/publisher/publisher_profile.tpl");

?>
