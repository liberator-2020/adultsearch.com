<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;
$system = new system; $form = new form;

if( !($account_id = $account->isloggedin())) {
        $account->asklogin();
        return;
}

$new = false;
if( !isset($_GET["id"]) ) {
	$new = true;
}
else {
	$id = intval($_GET["id"]);
	if( !$id ) $system->moved("/publisher/");
	$res = $db->q("select id from advertise_section where id = '$id' and account_id = '$account_id'");
	if( !$db->numrows($res) ) {
		reportAdmin("publisher zone fraud");
		$system->moved("/publisher/");
	}
}

if( isset($_POST["n"]) ) {
	$n = GetPostParam("n");
	$d = GetPostParam("d");
	$appneeded = isset($_POST["appneeded"]) ? 1 : 0;
	$appemail = $appneeded ? GetPostParam("appemail") : "";
	$network = intval($_POST["network"]);
	$backup = intval($_POST["backup"]);
	$backupcolor = GetPostParam("backupcolor");
	$skip_mysql = true;
	$newwindow = 0;
	$advertisehere = 1;

	if( $network ) {
		$res = $db->q("select popup, name, captcha, defaultbid, newwindow, advertisehere from advertise_section where id = '$network' and account_id = '$account_id'");
		if( !$db->numrows($res) ) {
			reportAdmin("publisher zone add network not found");
			$system->moved("/publisher/");
		}
		$row = $db->r($res); 
		$popup = $row['popup'] ? 1 : 0;
		$network_name = $row["name"];
		$captcha = $row["captcha"];
		$defaultbid = $row["defaultbid"];
		$newwindow = $row["newwindow"] ? 1 : 0;
		$advertisehere = !$row["advertisehere"] ? 0 : 1;
	}

	if ($popup)
		$backuphtml = GetPostParam("backupurl");
	else
		$backuphtml = GetPostParam("backuphtml");

	if( $new && !$network ) $error = "Please select the 'Main Advertising Area'";
	else if( empty($n) ) $error = "Please type the 'Zone Name' for your ad.";
	/* elseif( empty($d) ) $error = "Please type a brief description about your zone."; */
	else if( $appneeded && empty($appemail) ) $error = "Please type a notification e-mail address";
	else {
		if( $new ) {
			$db->q("INSERT INTO advertise_section
					(account_id, popup, name, n, d, external, active, captcha, appneeded, appemail, parent, defaultbid, backup, backuphtml, backupcolor, newwindow, advertisehere)
					values
					('$account_id', '$popup', '$network_name', '$n', '$d', 1, 1, '$captcha', '$appneeded', '$appemail', '$network', '$defaultbid', '$backup', '$backuphtml', '$backupcolor', '$newwindow', '$advertisehere')");
			if( ($id=$db->insertid) ) {
				$db->q("update advertise_section set section = '$id' where id = '$id'");

				$rex = $db->q("select id, account_id, dcpc from advertise_campaign where external = '1' and status = '1'");
				while($rox=$db->r($rex)) {

					$continue = 0;
					$ex = array();
					$rex2 = $db->q("select exception from advertise_exception where c_id = '{$rox["id"]}'");
					while($rox2=$db->r($rex2)) {	
						if( strstr($network_name, $rox2["exception"]) ) {
							reportAdmin("compaign {$rox["id"]} doesnt want to advertise with $network_name", "abbow");
							$continue = 1; break;
						}
					} 
					if( $continue ) continue;

					$name = $network_name . " - " . $n;

					$res = $db->q("select if(budget>0,1,0) as budget from advertise_budget where account_id = '{$rox["account_id"]}' limit 1");
					if (!$db->numrows($res))
						$budget_available = 0;
					else {
						$row =$db->r($res);
						$budget_available = intval($row["budget"]);
					}

					$db->q("INSERT INTO advertise_camp_section
							(c_id, s_id, section, status, budget_available, name, bid, `show`, haschild) 
							values 
							(?, ?, ?, ?, ?, ?, ?, ?, ?)
							",
							[$rox["id"], $id, $id, "1", $budget_available, $name, $rox["dcpc"], "1", "1"]
							);
				}

				$system->go("/publisher/", "New zone is added.");
			}
			else {
				$error = "Error! Our technical team has been notified. You may try again.";
				reportAdmin("zone add new error: ".mysql_error());
			}
		} else {
			$db->q("update advertise_section set popup = '$popup', n = '$n', d = '$d', appneeded = '$appneeded', appemail = '$appemail', backup = '$backup', backuphtml = '$backuphtml', backupcolor = '$backupcolor' where id = '$id'");
			$error = "Ad zone has been updated. <a href='/publisher/'>Click here</a> to go back to publisher main page.";
			if (!$appneeded)
				$db->q("update advertise_camp_section set approved = 1, reason = '' where s_id = '$id' and status = 1 and approved = 0 and budget_available = 1 and `show` = 1");
		}
	}

	$smarty->assign("error", $error);
}

if( !$new ) {
	$res = $db->q("select s.*, a.email from advertise_section s inner join account a using (account_id) where s.id = '$id' and s.account_id = '$account_id'");
	if( !$db->numrows($res) ) {
		reportAdmin("publisher zone return 0");
		$system->moved("/publisher/");
	}
	$row = $db->r($res); 

	if( !isset($skip_mysql) ) {
		$name = $row["name"];
		$n = $row["n"];
		$d = $row["d"];
		$appneeded = $row["appneeded"];
		$appemail = !empty($row["appemail"])?$row["appemail"]:$row["email"];
		$email = $row["email"];
		$popup = $row['popup'];
		$backup = $row["backup"];
		if ($popup)
			$backupurl = $row["backuphtml"];
		else
			$backuphtml = $row["backuphtml"];
		$backupcolor = $row["backupcolor"];
	}
} else {
	$res = $db->q("select s.id, s.name, s.popup, a.email from advertise_section s inner join account a using (account_id) where s.account_id = '$account_id' and (s.parent = 0 or s.parent = 81) ");
	if( !$db->numrows($res) ) {
		reportAdmin("zone add initial and user wasnt supposed to be here!");
		$system->moved("/publisher/");
	}
	$nw = array();
	$add = array();
	while($row = $db->r($res)) {
		$email = $row["email"];
		$nw[] = array($row["id"]=>$row["name"]);
		if ($row["popup"] == 1)
			$add[$row["id"]] = array("data-zone-type" => "popup");
		else
			$add[$row["id"]] = array("data-zone-type" => "text");
	} 
	$networks = $form->select_add_select();
	$networks .= $form->select_add_array($nw, $network, $add);
	$smarty->assign("networks", $networks);
	$name = $row["name"];

}

$smarty->assign("name", $name);
$smarty->assign("n", $n);
$smarty->assign("d", $d);
$smarty->assign("appneeded", $appneeded);
$smarty->assign("appemail", !empty($appemail)?$appemail:$email);
$smarty->assign("backup", $backup);
$smarty->assign("backuphtml", isset($_POST["backuphtml"])?htmlspecialchars($_POST["backuphtml"]):htmlspecialchars($backuphtml));
$smarty->assign("backupcolor", empty($backupcolor)?"#0b5394":$backupcolor);

include_html_head("js", "/js/tools/cp/simpleCP.js");
include_html_head("css", "/js/tools/cp/simpleCP.css");
$smarty->display(_CMS_ABS_PATH."/templates/publisher/publisher_zone.tpl");

?>
