<?php
/**
 * Admin controller: deletes place review and all its comments
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $account;
$system = new system;

if (!($account_id=$account->isLoggedIn())) {
	$account->asklogin();
	return;
}

if (!$account->isadmin()) {
	echo "You dont have permissions to perform this action!<br />";
	die;
}

$id = intval($_REQUEST["id"]);
if (!$id) {
	echo "Review id not specified!<br />";
	die;
}

//get review
$review = review::findOneById($id);
if ($review == false) {
	flash::add(flash::MSG_ERROR, "Cant find review id #{$id} !");
	$system->go("/");
}

//get place link
$redirect_url = "/";
$res = $db->q("SELECT place_id, loc_id, place_type_id, name FROM place WHERE place_id = ?", array($row["id"]));
if ($db->numrows($res) == 1) {
	$row = $db->r($res);
	$redirect_url = dir::getPlaceLink($row, true);
}

//remove review
$review->remove();

$system->go($redirect_url);

die;

?>
