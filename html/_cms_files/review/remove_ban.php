<?php
/**
 * Admin controller: deletes place review and all its comments and ban user, who posted it
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $account;
$system = new system;

if (!($account_id=$account->isLoggedIn())) {
	$account->asklogin();
	return;
}

if (!$account->isadmin()) {
	echo "You dont have permissions to perform this action!<br />";
	die;
}

$id = intval($_REQUEST["id"]);
if (!$id) {
	echo "Review id not specified!<br />";
	die;
}

//get review
$review = review::findOneById($id);
if ($review == false) {
	flash::add(flash::MSG_ERROR, "Cant find review id #{$id} !");
	$system->go("/");
}

$owner_id = $review->getAccountId();

//get place link
$redirect_url = "/";
$res = $db->q("SELECT place_id, loc_id, place_type_id, name FROM place WHERE place_id = ?", array($row["id"]));
if ($db->numrows($res) == 1) {
	$row = $db->r($res);
	$redirect_url = dir::getPlaceLink($row, true);
}

//remove review
$review->remove();

//get owner of review
$owner = account::findOnebyId($owner_id);
if ($owner == false) {
	flash::add(flash::MSG_WARNING, "Cant find account of review owner (account_id={$owner_id}) !");
	$system->go($redirect_url);
}

//ban review owner
//TODO after banned and not bnneed account merged, finish this
//if ($owner->isBanned()) {
//	flash::add(flash::MSG_INFO, "Owner of review was already banned.");
//	$system->go("/");
//}

$ret = $owner->ban();
if ($ret) {
	flash::add(flash::MSG_SUCCESS, "Owner of review <strong>{$owner->getEmail()}</strong> (account_id #{$owner->getId()}) successfully banned.");
} else {
	flash::add(flash::MSG_ERROR, "Error: Owner of review <strong>{$owner->getEmail()}</strong> (account_id #{$owner->getId()}) was not banned.");
}
$system->go($redirect_url);

die;

?>
