<?php

global $db, $account, $ctx;
$system = new system;
$account_id = $account->isloggedin();

$where = "";

if (substr($_POST["section"], 0, 1) == "/") {
	$where = GetPostParam("section");
} else {
	switch($_POST["section"]) {
		case 'cl1':
			$where = '/female-escorts/';
			break;
		case 'cl2':
			$where = '/tstv-shemale-escorts/';
			break;
		case 'cl6': 
			$where = '/body-rubs/'; 
			break;
		case 'as': 
		case 'as1': 
			if ($ctx->international)
				$where = '/sex-shop'; 
			else
				$where = '/sex-shops/'; 
			break;
		case 'sc': 
		case 'sc1': 
			if ($ctx->international)
				$where = '/strip-club'; 
			else
				$where = '/strip-clubs/'; 
			break;
		case 'ls': 
			$where = '/lifestyle/'; 
			break;
		case 'ls1': 
			$where = '/swinger-clubs/'; 
			break;
		case 'ls2': 
			$where = '/bdsm/'; 
			break;
		case 'emp': 
		case 'emp1': 
			if ($ctx->international)
				$where = '/erotic-massage-parlor'; 
			else
				$where = '/erotic-massage/'; 
			break;
		case 'brothel1': 
			if ($ctx->international)
				$where = '/brothel'; 
			else
				$where = '/brothels/'; 
			break;

//disabled 2018-01-03
//		case 'webcam': 
//			$where = 'http://rabbits.webcam/?id=121';
//			break;

		default:
			break;
	}
}

if (empty($where)) {
	$system->moved("/");
	die;
}

if( strstr($where, 'http://') ) {
	$site = $where;
} else {
	$url = GetPostParam("url");
	$site = str_replace('//', '/', $url.$where);
}

if (empty($site))
	$site = "/";

$system->moved($site);

?>
