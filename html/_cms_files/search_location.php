<?php
/**
 * searches for location by name
 */

global $db, $ctx, $smarty;

$name = GetGetParamStripped("city");
if (empty($name))
	system::go("/");

//in case we select e.g. "Cape Town, South Africa" and submit the form with button - we take only first part until comma
if ($pos = strpos($name, ","))
	$name = substr($name, 0, $pos);

$name = preg_replace('/\s+/', ' ', $name);

$locations = array();
$res = $db->q("SELECT l.loc_name, l.loc_url, l.country_sub, l2.loc_name as ss
				FROM location_location l
				INNER JOIN location_location l2 on l.loc_parent = l2.loc_id
				WHERE l.loc_type = 3 AND l.has_place_or_ad = 1 AND l.loc_name like ?", array($name."%"));
while($row=$db->r($res)) {
	$sub = $row['country_sub'] ? "{$row['country_sub']}." : "";
	$link = "http://{$sub}{$ctx->domain_base}{$row['loc_url']}";
	$locations[] = array(
		"name" => "{$row['loc_name']}, {$row['ss']}",
		"url" =>  $link
	);
}

if (count($locations) == 1) {
	$loc = array_shift($locations);
	system::go($loc["url"]);
	die;
}

$smarty->assign("city", htmlspecialchars($name));
$smarty->assign("locations", $locations);
$smarty->display(_CMS_ABS_PATH."/templates/search_location.tpl");

?>
