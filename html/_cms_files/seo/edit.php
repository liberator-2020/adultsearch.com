<?php
defined('_CMS_FRONTEND') or die('Restricted access !');

global $ctx, $smarty, $db, $account, $mcache;

if (!permission::access("seo_edit"))
	return;

$location_id = intval($_REQUEST["location_id"]);
if (!$location_id)
	die("No location specified!");
$location = location::findOneById($location_id);
$url = $location->getUrl();

$category = intval($_REQUEST["category"]);
$place_type_id = intval($_REQUEST["place_type_id"]);

if ($place_type_id) {
	$type = "LPT";
	if ($location->isInternational()) {
		$row = $mcache->get("SQL:PTY-ID", array($place_type_id));
		if ($row !== false) {
			$type_label = $row["name"];
			$url .= "/{$row["url"]}/";
		}
	} else {
		switch ($place_type_id) {
			case 1:
				$type_label = "Strip Clubs";
				$url .= "/strip-clubs/";
				break;
			case 15:
				$type_label = "Erotic Massage Parlors";
				$url .= "/erotic-massage/";
				break;
			default: $type_label = $place_type_id; break;
		}
	}
} else if ($category) {
	$type = "LE";
	$type_label = "Escorts";
	$url .= "/female-escorts/";
	$place_type_id = NULL;
} else
	die("No type specified !");

$type_sql = "";
$params = [$location->getId(), $type];
if ($type == "LPT") {
	$type_sql = " AND place_type_id = ? ";
	$params[] = $place_type_id;
}

$id = $description = NULL;
$res = $db->q("SELECT id, description FROM seo WHERE location_id = ? AND type = ? {$type_sql}", $params);
if ($db->numrows($res) > 0) {
	$row = $db->r($res);
	$id = $row["id"];
	$description = $row["description"];	
}

// ------------------
// --- submission ---
// ------------------
if ($account->isrealadmin() && $_REQUEST["submit"] == "Remove") {
	$res = $db->q("DELETE FROM seo WHERE id = ? LIMIT 1", [$id]);
	$aff = $db->affected($res);
	$success = ($aff == 1);
	if ($success)
		return success_redirect("SEO description removed.", $url);
	else
		return error_redirect("Error: Failed to remove SEO description !", $url);

} else if ($_REQUEST["submit"] == "Submit") {
	$description = $_REQUEST["description"];

	$success = false;
	if (is_null($id)) {
		$res = $db->q("INSERT INTO seo (type, location_id, place_type_id, description, created_stamp, created_by) VALUES (?, ?, ?, ?, ?, ?)",
			array($type, $location->getId(), $place_type_id, $description, time(), $account->getId())
			);
		$id = $db->insertid($res);
		$success = intval($id);
	} else {
		$res = $db->q("UPDATE seo SET description = ?, updated_stamp = ?, updated_by = ? WHERE id = ? LIMIT 1", [$description, time(), $account->getId(), $id]);
		$aff = $db->affected($res);
		$success = ($aff == 1);
	}

	if ($success)
		return success_redirect("SEO description has been saved successfully.", $url);
	else
		return error_redirect("Error: Failed to update SEO description ! Please contact administrator.", $url);
}

$smarty->assign("id", $id);
$smarty->assign("location_label", $location->getLabel());
$smarty->assign("type_label", $type_label);
$smarty->assign("type", $type);
$smarty->assign("place_type_id", $place_type_id);
$smarty->assign("category", $category);
$smarty->assign("description", $description);

$smarty->display(_CMS_ABS_PATH."/templates/seo/edit.tpl");

?>
