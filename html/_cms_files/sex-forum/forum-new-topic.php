<?php

defined('_CMS_FRONTEND') or die('Restricted access');

include_html_head("css", "/css/forum.css");

global $ctx, $db, $account, $smarty;

$forum = new Forum();

if( !($account_id=$account->isloggedin()) ) {
	$account->asklogin();
	return;
}

$user = $account->getUserDetail($account_id);
if( $user == NULL ) {
	$account->asklogin();
	return;
}

//get location info
if (($loc_arr = forum::getForumLocation()) === false) {
	echo "Invalid access.";
	die;
}
list($loc_id, $loc_name, $loc_link, $exact_loc_id) = $loc_arr;

//get forum info
if ($ctx->international) {
	$forum_id = $ctx->forum_id;
	$forum_link = $ctx->location_path."/sex-forum";
} else {
	$forum_id = intval(GetRequestParam('forum_id'));
	$forum_link = $account->core_loc_array['loc_url']."sex-forum/";
}

//check that you cant start 2 topics in 1 day
if (!$account->isWorker()) {
	$res = $db->q("SELECT topic_id 
					FROM forum_topic 
					WHERE account_id = ? AND topic_time > date_sub(now(), interval 1 day)
					LIMIT 1",
					array($account_id)
				);
	if ($db->numrows($res)) {
		system::go("/", "You may not start two different forum topics same day");
	}
}

//add topic handler
if (GetPostParam('Submit')=='Submit') {
	$topic_title = classifieds::onlyPrintableCharacters(trim($_POST["topic_title"]));
	$topictext = classifieds::onlyPrintableCharacters(trim($_POST["topictext"]));
	$item_id = intval($_POST["item_id"]);
	$important = ($account->isrealadmin() && (isset($_REQUEST["sticky"]))) ? 1 : 0;

	if ((stripos($topictext, "<script") !== false) || (stripos($topictext, "jQuery") !== false)) {
		//hack protection
		$smarty->assign("error", "Invalid forum text");

	} else if( !empty($topic_title) && !empty($topictext) ) {

		//spam protection
		if( strstr($topictext, "http://") || strstr($topictext, "https://") || strstr($topictext, "www.") ) {
			if( isset($_POST["spam"]) && !empty($_SESSION["captcha_str"]) && strtoupper($_SESSION["captcha_str"]) != strtoupper($_POST["spam"]) ) {
				$smarty->assign("spamerror", true);
				$error = 1;
			} else if (strlen($_SESSION["captcha_str"])>2 && strtoupper($_SESSION["captcha_str"]) == strtoupper($_POST["spam"]) ) {
				$error = 0;
			} else {
				$error = 1;
			}

			if ($error) {
				$hidden = "<input type=\"hidden\" name=\"topictext\" value=\"".htmlspecialchars($topictext)."\" />";
				$hidden .= "<input type=\"hidden\" name=\"topic_title\" value=\"".htmlspecialchars($topic_title)."\" />";
				$hidden .= "<input type=\"hidden\" name=\"forum_id\" value=\"{$forum_id}\" />";
				$hidden .= "<input type=\"hidden\" name=\"loc_id\" value=\"{$loc_id}\" />";
				$hidden .= "<input type=\"hidden\" name=\"item_id\" value=\"{$item_id}\" />";
				$hidden .= "<input type=\"hidden\" name=\"Submit\" value=\"Submit\" />";
				$smarty->assign("hidden", $hidden);
				$smarty->display(_CMS_ABS_PATH."/templates/comment-spam.tpl");
				return;
			}
		}

		$res = $db->q("INSERT INTO forum_topic
						(topic_title, item_id, account_id, topic_time, forum_id, loc_id, topic_last_post_time, important) 
						VALUES 
						(?, ?, ?, now(), ?, ?, unix_timestamp(now()), ?)",
						array($topic_title, $item_id, $account_id, $forum_id, $loc_id, $important));
		$topic_id = $db->insertid($res);
		if (!$topic_id)
			die("Error occured!");

		$res = $db->q("INSERT INTO forum_post 
						(topic_id, account_id, posted, forum_id, loc_id, post_text) 
						VALUES 
						(?, ?, ?, ?, ?, ?)",
						array($topic_id, $account_id, time(), $forum_id, $loc_id, $topictext));
		$post_id = $db->insertid($res);
		if (!$post_id)
			die("Error occured 2");

		forum::updateStats($forum_id, $loc_id);

		$db->q("UPDATE account SET forum_post = forum_post + 1 WHERE account_id = ?", array($account_id));

		$topic = topic::findOneById($topic_id);
		if ($topic)
			$topic->emailAdminNotify();

		$forum->watchAdd($topic_id, $account_id);
		system::moved(forum::getTopicLink($topic_id));
	} else {
		$smarty->assign("error", "Fill the form please");
	}
}

$smarty->assign("forum_link", $forum_link);
if ($ctx->international) {
    $ctx->addToBreadcrumb(array("link" => "{$ctx->location_path}/sex-forum", "name" => "Sex Forum"));
}
if (!empty($forum_id)) {
	$f = $forum->_coreForumBreadReverse($forum_id);
	if (empty($f))
		system::go("/sex-forum/");
	$smarty->assign("f", array_reverse($f));
	if ($ctx->international) {
		$fcat = array_reverse($f);
	    foreach($fcat as $item) {
    	    $ctx->addToBreadcrumb(array("link" => $item['forum_url'], "name" => htmlspecialchars($item['forum_name'])));
	    }
	}
}

//get forum categories for this location
$categories = $forum->getCategories($loc_id);
$smarty->assign("categories", $categories);
if (empty($forum_id)) {
	reset($categories);
	$forum_id = key($categories);
}
$smarty->assign("selected_forum_id", $forum_id);

//get companies for pre-selected category
if ($forum_id) {
	$smarty->assign("category_name", $forum->getForumNameById($forum_id));
	$smarty->assign("loc_id", $exact_loc_id);
	$companies = $forum->getCompanies($exact_loc_id, $forum_id);
	$smarty->assign("companies", $companies);
}

//admins can add sticky topics
if ($account->isadmin())
	$smarty->assign("sticky_allowed", true);

$smarty->assign("edit_topic_title", true);
$smarty->assign("goback", true);
$smarty->assign("noaction", true);
$smarty->assign("new_topic", true);
$smarty->assign("icons", $forum->icons());
$smarty->assign("topic_title", $topic_title);
$smarty->assign("topictext", $topictext);
include_html_head("js", "/js/tools/bbcode.js");
$smarty->display(_CMS_ABS_PATH."/templates/sex-forum/reply.tpl");

?>
