<?php

defined('_CMS_FRONTEND') or die('Restricted access');

include_html_head("js", "/js/tools/bbcode.js");

global $ctx, $db, $account, $smarty;

$system = new system;
$forum = new Forum();

if( !($account_id=$account->isloggedin()) ) {
	$account->asklogin();
	return;
}

//get ids
if ($ctx->international) {
	$forum_id = $ctx->forum_id;
	$topic_id = $ctx->forum_topic_id;
	$post_id = $ctx->forum_post_id;
	$sex_forum_url = "{$ctx->location_path}/sex-forum";
} else {
	$post_id = GetGetParam("post_id"); $post_id = intval($post_id);
	$sex_forum_url = "{$account->core_loc_array['loc_url']}sex-forum/";
}

if( !$post_id )
	$system->go($sex_forum_url);

$res = $db->q("SELECT p.*, p.account_id as postNick, p.forum_id as postForumId, t.* 
				FROM forum_post p 
				INNER JOIN forum_topic t using (topic_id) 
				WHERE p.post_id = '$post_id'
				LIMIT 1");
if (!$db->numrows($res))
	die("post could not be found");
$row = $db->r($res);

if( $account_id != $row["postNick"] && !$account->isadmin() ) {
	die("No Access!");
	return;
}

//------------------------
//------ breadcrumb ------
if ($ctx->international) {
	$f = $forum->_coreForumBreadReverse($forum_id);
	$fcat = array_reverse($f);
	$ctx->addToBreadcrumb(array("link" => "{$ctx->location_path}/sex-forum", "name" => "Sex Forum"));
	foreach($fcat as $item) {
		$ctx->addToBreadcrumb(array("link" => $item['forum_url'], "name" => htmlspecialchars($item['forum_name'])));
	}
	$ctx->addToBreadcrumb(array("link" => forum::getTopicLink(), "name" => htmlspecialchars($row["topic_title"])));
}

//------------------------
//--- is this first post ? if so, id_company should be changed in topic table instead post table
$res_post = $db->q("select post_id, forum_id from forum_post where topic_id = {$row["topic_id"]} order by post_id limit 1");
$row_post = $db->r($res_post);
if ($post_id == $row_post["post_id"]) {
	$smarty->assign("edit_topic", true);
	$smarty->assign("edit_topic_title", true);
	$smarty->assign("topic_title", $row["topic_title"]);

	if ($account->isadmin())
		$smarty->assign("sticky_allowed", true);

	$item_id = $row["item_id"];
	$update_topic = true;

	$exact_loc_id = $ctx->location_id;	//TODO this is kinda hack, we should correctly get city if loc_id is county...
	$companies = $forum->getCompanies($exact_loc_id, $row['forum_id']);
	$smarty->assign("companies", $companies);
	$smarty->assign("item_id", $item_id);
	$smarty->assign("category_name", $forum->getForumNameById($row["forum_id"]));
} 


//processing
if (isset($_POST["topictext"])) {
	$topictext = GetPostParam("topictext");
	$reason = trim(GetPostParam("editreason"));
	$topic_title = GetPostParam("topic_title");

	if( !empty($topictext) && !empty($reason) ) {
		$item_id = intval($_POST["item_id"]);
		if( $update_topic )
			$db->q("update forum_topic set topic_title = '$topic_title', item_id = '$item_id' where topic_id = {$row["topic_id"]}");

		$db->q("update forum_post set post_text = '$topictext', post_edited = '{$_SESSION["username"]}', post_lastedit_date=now(), 
post_edit_count=post_edit_count+1, post_edit_reason='$reason' where post_id = $post_id");
		$link = $forum->makeTopicLink($row["topic_id"], $row["topic_title"]);
		$item_id = intval($_POST["mp"]);
		//$system->go("/sex-forum/viewtopic?topic_id={$row["topic_id"]}&p=".$post_id."#".$post_id, 'Done');
		$system->go(forum::getPostLink($post_id, $row["topic_id"]), 'Done');
	} else if( $account->isadmin() ) {

		$item_id = intval($_POST["item_id"]);
		if( $update_topic )
			$db->q("update forum_topic set topic_title = '$topic_title', item_id = '$item_id' where topic_id = {$row["topic_id"]}");

		if( !empty($topictext) ) {
			$db->q("update forum_post set post_text = '$topictext' where post_id = $post_id");
		}
		//$system->moved("/sex-forum/viewtopic?topic_id={$row["topic_id"]}&p=".$post_id."#".$post_id);
		$system->moved(forum::getPostLink($post_id, $row["topic_id"]));
	}
	else
		$smarty->assign("error", "Please fill all mandatory fields");
}

$topictext = $row["post_text"];
$username = $row["username"];

$smarty->assign("edit_post", true);
$smarty->assign("goback", true);
$smarty->assign("noaction", true);
$smarty->assign("inputReason", true);

$smarty->assign("topictext", $topictext);
$smarty->assign("editreason", $row["post_edit_reason"]);

$smarty->assign("icons", $forum->icons());
$smarty->display(_CMS_ABS_PATH."/templates/sex-forum/reply.tpl");

?>
