<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $ctx, $db, $smarty, $account, $gFilename, $gTitle, $page, $mobile, $gBreadCat;

$forum = new forum();

if (!($account_id=$account->isloggedin())) {
	$account->asklogin();
	return;
}

//get topic id
if ($ctx->forum_topic_id == NULL) {
	system::go('/', 'Invalid access.');
	die;
}
$topic_id = $ctx->forum_topic_id;

if (!$topic_id || !$account->isAdmin())
	system::go("/");

if (($loc_arr = forum::getForumLocation()) === false) {
	echo "Invalid access.";
	die;
}
list($loc_id, $loc_name, $loc_link, $exact_loc_id) = $loc_arr;

//submitted data
if($_POST["move"]=="Move") {
	$forum_id = intval($_POST["forum_id"]);
	$old_forum = intval($_POST["old_forum"]);
	if( $forum_id )
		$res = $forum->move($topic_id, $forum_id, $old_forum, $error);

	if ($res !== false) {
		$topic_link = forum::getTopicLink($topic_id, $forum_id);
		echo "<center><h1>moved</h1><a href='{$topic_link}'>go to the topic</a></center>";
		return;
	}
	
	echo "Could not moved, try again (<b>$error</b>)!";
}

// check if the topic is still there...
$res = $db->q("SELECT * FROM forum_topic WHERE topic_id = ? LIMIT 1", [$topic_id]);
if (!$db->numrows($res))
	system::go("/", "This topic has been deleted.");
$row_topic = $db->r($res);

$forum_id = $row_topic["forum_id"];

// db stuff....
$res = $db->q("SELECT * FROM forum_forum WHERE loc_id = ? AND forum_parent = 0 ORDER BY forum_name", [$loc_id]);
$forums = array();
while ($row=$db->r($res)) {
	$alt = array();
	$res2 = $db->q("select * from forum_forum where loc_id = ? and forum_parent = ? order by forum_name", [$loc_id, $row["forum_id"]]);
	while($row2=$db->r($res2)) {
		$forum_parent = $row2["forum_id"];
	
		$res3 = $db->q("select * from forum_forum where loc_id = ? and forum_parent = ? order by forum_name", [$loc_id, $forum_parent]);
		$alt2 = array();
		while($row3=$db->r($res3)) {
			$alt2[] = array(
				"forum_id" => $row3["forum_id"],
				"forum_name" => $row3["forum_name"]
			);			
		}

		$alt[] = array(
			"forum_id" => $row2["forum_id"],
			"forum_name" => $row2["forum_name"],
			"alt" => $alt2
		);
	}

	$forums[] = array(
		"forum_id" => $row["forum_id"],
		"forum_name" => $row["forum_name"],
		"alt" => $alt
	);
}

$smarty->assign("forums", $forums);
$smarty->assign("forum_id", $row_topic["forum_id"]);
$smarty->assign("topic_id", $topic_id);
$smarty->assign("link", forum::getTopicLink($topic_id, $forum_id));
$smarty->display(_CMS_ABS_PATH."/templates/sex-forum/move.tpl");

?>
