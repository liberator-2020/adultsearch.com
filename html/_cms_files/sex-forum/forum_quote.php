<?php

defined('_CMS_FRONTEND') or die('Restricted access');

include_html_head("css", "/css/forum.css");
include_html_head("js", "/js/tools/bbcode.js");

global $ctx, $db, $account, $smarty;
$system = new system;
$forum = new Forum();

if( !($account_id=$account->isloggedin()) ) {
	$account->asklogin();
        return;
}

//get ids
if ($ctx->international) {
    $forum_id = $ctx->forum_id;
    $topic_id = $ctx->forum_topic_id;
    $post_id = $ctx->forum_post_id;
	$sex_forum_url = "{$ctx->location_path}/sex-forum";
} else {
	$topic_id = GetGetParam("topic_id"); $topic_id = intval($topic_id);
	$post_id = GetGetParam("post_id"); $post_id = intval($post_id);
	$sex_forum_url = "{$account->core_loc_array['loc_url']}sex-forum/";
}

if( !$topic_id || !$post_id )
	$system->go($sex_forum_url);

//fetch topic
$q = "select t.locked, t.topic_id, t.topic_title 
		from forum_post p 
		inner join forum_topic t using (topic_id) 
		where post_id = '$post_id'";
$res = $db->q($q);
if( !$db->numrows($res) )
	$system->go($sex_forum_url);
$row_topic = $db->r($res);

$topic_id = $row_topic["topic_id"];

if( $row_topic["locked"] == 1 )
	$system->go(forum::getTopicLink($topic_id), "This topic is locked, you can not quote");

//------------------------
//------ breadcrumb ------
if ($ctx->international) {
	$f = $forum->_coreForumBreadReverse($forum_id);
	$fcat = array_reverse($f);
    $ctx->addToBreadcrumb(array("link" => "{$ctx->location_path}/sex-forum", "name" => "Sex Forum"));
    foreach($fcat as $item) {
        $ctx->addToBreadcrumb(array("link" => $item['forum_url'], "name" => htmlspecialchars($item['forum_name'])));
    }
	$ctx->addToBreadcrumb(array("link" => forum::getTopicLink(), "name" => htmlspecialchars($row_topic["topic_title"])));
}

//fetch post
$res = $db->q("select forum_post.*, account.username 
				from forum_post 
				inner join (account) on ( account.account_id = forum_post.account_id) 
				where forum_post.post_id = '$post_id' limit 1");
$row = $db->r($res);

$post_text = $row["post_text"];
$username = $row["username"];

// clear extra stuff..
$pat[] = '@\[quote[^]]*?\].*?\[/quote\]@si';
$rep[] = '';
$post_text = preg_replace($pat, $rep, $post_text);

$topictext = "[quote=\"$username\"] $post_text [/quote]";

$smarty->assign("forum_reply_link", forum::getForumReplyLink($topic_id, $forum_id));
$smarty->assign("goback", true);
$smarty->assign("topic_id", $topic_id);
$smarty->assign("topic_title", htmlspecialchars($row_topic["topic_title"]));
$smarty->assign("topictext", $topictext);
$smarty->assign("icons", $forum->icons());
$smarty->display(_CMS_ABS_PATH."/templates/sex-forum/reply.tpl");

?>
