<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $ctx, $db, $account, $smarty;

$forum = new forum();

if( !($account_id=$account->isloggedin()) ) {
	$account->asklogin();
	return;
}

switch($ctx->forum_action) {
	case 'watch':
		$forum->watchAdd($ctx->forum_topic_id, $account_id);
		system::moved(forum::getTopicLink($ctx->forum_topic_id, $ctx->forum_id, true));
		break;
	case 'unwatch':
		$forum->watchDelUser($ctx->forum_topic_id, $account_id);
		system::moved(forum::getTopicLink($ctx->forum_topic_id, $ctx->forum_id, true));
		break;
	case 'deltopic':
		$topic = topic::findOneById($ctx->forum_topic_id);
		if (!$topic)
			return system::go("/", 'Could not delete!');
		$ret = $topic->remove();
		if ($ret) 
			return system::go(forum::getForumLink($ctx->forum_id, false), 'Deleted');
		return system::go($topic->getUrl(), 'Could not delete!');
		break;
	case 'delpost':
		$post = post::findOneById($ctx->forum_post_id);
		if (!$post) {
			return error_redirect("This forum post does not exist anymore.", "/");
		}
		$topic = $post->getTopic();
		$ret = $post->remove();
		if ($ret) {
			return success_redirect("Forum post #{$ctx->forum_post_id} has been deleted.", $topic->getUrl());
		} else {
			return error_redirect("Error deleting forum post #{$ctx->forum_post_id}. Please contact administrator.", "/");
		}
		break;
	default:
		break;
}

//get location info
if ((($loc_arr = forum::getForumLocation()) === false) && (!isset($_GET["topic_id"]))) {
	echo "Invalid access.";
	die;
}
list($loc_id, $loc_name, $loc_link, $exact_loc_id) = $loc_arr;

$topic_id = $ctx->forum_topic_id;
$sex_forum_url = "{$ctx->location_path}/sex-forum";


if (isset($_SESSION["forumspam"]) && $_SESSION["forumspam"]>3)
	die("There is too many suspicious forum post posted in your account in a short time.");

if (!$topic_id)
	system::go($sex_forum_url);

// fetch topic
$sql = "select * from forum_topic where topic_id = $topic_id limit 1";
$res = $db->q($sql);
if( !$db->numrows($res) )
	system::go($sex_forum_url, "This topic has been deleted.");
$row_topic = $db->r($res);

if( $row_topic["locked"] == 1 )
	system::go($sex_forum_url, "This topis has been locked. You can't post.");

$loc_id = $row_topic["loc_id"];
$forum_id = $row_topic["forum_id"];
//$topictext = htmlspecialchars(trim($_POST["topictext"]), ENT_QUOTES);
$topictext = trim(classifieds::onlyPrintableCharacters($_POST["topictext"]));

if( trim($topictext) == "" )
	system::go(forum::getTopicLink($topic_id), "You cant post an empty message.");

if ((stripos($topictext, "<script") !== false) || (stripos($topictext, "jQuery") !== false)) {
	//hack protection
	system::go(forum::getTopicLink($topic_id), "Invalid post message.");
}

//spam link check
if( strstr($topictext, "http://") || strstr($topictext, "https://") || strstr($topictext, "www.") ) {
	if( isset($_POST["spam"]) && !empty($_SESSION["captcha_str"]) && strtoupper($_SESSION["captcha_str"]) != strtoupper($_POST["spam"]) ) {
		$smarty->assign("spamerror", true);
		$error = 1;
	} else if (strlen($_SESSION["captcha_str"])>2 && strtoupper($_SESSION["captcha_str"]) == strtoupper($_POST["spam"]) ) {
		$error = 0;
	} else
		$error = 1;

	if( $error ) {
		$hidden = "<input type=\"hidden\" name=\"topictext\" value=\"$topictext\" />";
		$hidden .= "<input type=\"hidden\" name=\"topic_id\" value=\"$topic_id\" />";
		$hidden .= "<input type=\"hidden\" name=\"loc_id\" value=\"$loc_id\" />";
		$smarty->assign("hidden", $hidden);
		$smarty->assign("nobanner", true);
		$smarty->display(_CMS_ABS_PATH."/templates/sex-forum/comment-spam.tpl");
		return;
	}
}

//check for duplicate posts in this topic
$res = $db->q("SELECT post_id FROM forum_post WHERE topic_id = ? AND account_id = ? AND post_text = ?", array($topic_id, $account_id, $topictext));
if ($db->numrows($res) > 0) {
	//duplicite post
	system::go(forum::getTopicLink($topic_id), "You can't post spam. We are monitoring spam and duplicite posts.");
}

$post = new post();
$post->setTopicId($topic_id);
$post->setAccountId($account_id);
$post->setForumId($forum_id);
$post->setLocId($loc_id);
$post->setPostText($topictext);
$ret = $post->add();
if (!$ret) {
	//error inserting forum post into db
	echo "Error: Failed to add forum post!<br />";
	return;
}
$this_post = $post->getId();

$forum->watchProcess($forum_id, $topic_id, $row_topic["topic_title"], $this_post);
if( isset($_POST["watch"]) )
	$forum->watchAdd($topic_id, $account_id);
else
	$forum->watchDelUser($topic_id, $account_id);

system::go(forum::getPostLink($this_post, $topic_id, $forum_id));

?>
