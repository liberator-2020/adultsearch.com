<?php

defined('_CMS_FRONTEND') or die('Restricted access');

include_html_head("css", "/css/forum.css");

global $ctx, $db, $smarty, $account;

$forum = new Forum();

//get location info
if (($loc_arr = forum::getForumLocation()) === false) {
    echo "Invalid access.";
    die;
}
list($loc_id, $loc_name, $loc_link, $exact_loc_id) = $loc_arr;

$smarty->assign("account_id", $account->isloggedin());
if ($ctx->international) {
	$smarty->assign("forum_search_url", "{$ctx->location_path}/sex-forum/forum_search");
}

//if search query not entered, just display search form
if (!isset($_GET["search"]) || empty($_GET["search"])) {
	$smarty->display(_CMS_ABS_PATH."/templates/sex-forum/search.tpl");
	return;
}

//------ breadcrumb ------
if ($ctx->international) {
    $ctx->addToBreadcrumb(array("link" => "{$ctx->location_path}/sex-forum", "name" => "Sex Forum"));
}

//------------------------
//lets go search
$word = GetGetParam("search");

$posts_on_page = 10;
$cur_page = (isset($_REQUEST["page"])) ? intval($_REQUEST["page"]) : 1;
if ($cur_page == 0)
    $cur_page = 1;
$start = ($cur_page - 1) * $posts_on_page;

$sql ="SELECT SQL_CALC_FOUND_ROWS
		t.topic_id, t.topic_title, t.topic_replies, t.account_id, t.topic_last_post_time as post_time, 
		f.forum_name, f.forum_id, 
		p.post_id, p.post_text,
		a.username, 
		(SELECT account.username FROM forum_post left join account on forum_post.account_id = account.account_id WHERE post_id = t.topic_last_post_id limit 1) as username2
		
		FROM forum_topic t
		INNER JOIN forum_forum f using (forum_id)
		LEFT JOIN account a on t.account_id = a.account_id
		LEFT JOIN forum_post p on t.topic_last_post_id = p.post_id
		WHERE t.loc_id = '$loc_id'
			AND (t.topic_title like '%$word%' or p.post_text like '%$word%') 
		GROUP BY (topic_id) 
		ORDER BY p.post_id desc
		LIMIT {$start}, {$posts_on_page}";

$res = $db->q($sql);

$posts=array();
while($row=$db->r($res)) {

	$forum_link = forum::getForumLink($row["forum_id"]);
	$post_link = forum::getPostLink($row["post_id"], $row["topic_id"], $row["forum_id"]);
	
	$lastlink = $row["username2"] ? substr($row["username2"],0,15) : "Deleted Member";
	$userlink = $row["username"] ? $row["username"]: "";

	$posts[]=array(
		"POSTER_NAME" => $row["username"],
		"POSTER_ID" => $row["account_id"],
		"topic_title" =>  $row["topic_title"],
		"U_FORUM" => forum::getForumLink($row["forum_id"]),
		"topic_id"=>$row["topic_id"],
		"forum_name" => $row["forum_name"],
		"lastPostDate" => $row["post_time"],
		"lastPostUser" => $lastlink,
        "userlink" => $userlink,
		"POST_SUBJECT" => $row["topic_title"],
		"topic_replies" => $row["topic_replies"],
		"account_id" => $row["account_id"],
		"message" => $row["post_text"],
		"post_link" => forum::getPostLink($row["post_id"], $row["topic_id"], $row["forum_id"]),
	);
}

$res = $db->q("SELECT FOUND_ROWS()");
$row = $db->r($res);
$num_posts = $row[0];
$smarty->assign("pager", $forum->getPagingArray($num_posts, $cur_page, "forum_search?search=".urlencode($word), $posts_on_page));

$count = count($posts);
$smarty->assign("forum_posts", $count);
$posts = $db->arrayClear($posts, 20, $count);
//$smarty->assign("paging", $db->paging($count, 20, true, false, NULL));
$smarty->assign("paging", $db->paging($count, 20, false, false, NULL));

$smarty->assign("search", htmlspecialchars($_GET["search"]));

$smarty->assign("L_SEARCH_MATCHES", "Total result: <b>$count</b>");
$smarty->assign("topics", $posts);
$smarty->display(_CMS_ABS_PATH."/templates/sex-forum/search.tpl");

?>
