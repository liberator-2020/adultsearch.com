<?php

defined('_CMS_FRONTEND') or die('Restricted access');

include_html_head("css", "/css/forum/forum.css");

global $ctx, $gTitle, $db, $smarty, $account, $mobile;

$forum = new Forum();

//get location info
if (($loc_arr = forum::getForumLocation()) === false) {
	echo "Invalid access.";
	die;
}
list($loc_id, $loc_name, $loc_link, $exact_loc_id) = $loc_arr;

$gTitle = "{$ctx->location_name} Sex Forum";

if ($ctx->international) {
	$dir = new dir();
	$city_cats = $dir->getLocCategories();
	//_darr($city_cats);
	$cats = array();
	foreach ($city_cats as $cat) {
		$cats[] = $cat['name_plural'];
	}
	//_darr($cats);
}

//MAIN SELECT for forums for this location
$res = $db->q("SELECT * 
				FROM forum_forum 
				WHERE loc_id = '$loc_id' and forum_parent = 0 
				ORDER BY forum_order");
$forums = array();
while($row=$db->r($res)) {
	//SELECt subforums of this forum
	$res2 = $db->q("SELECT *, if(unix_timestamp(forum_last_post_time) > CURRENT_TIMESTAMP(), 1, 0) as updated
					FROM forum_forum
					WHERE forum_parent = {$row["forum_id"]} AND loc_id = '{$loc_id}'
					ORDER BY forum_name");
	$subforums = array();
	while($row2=$db->r($res2)) {
		$subforums[] = array(
				"forum_name" => $row2["forum_name"],
				"forum_url" => forum::getForumLink($row2["forum_id"]),
				"forum_posts" => $row2["forum_posts"],
			);
	}

	$for = array(
		"forum_name" => $row["forum_name"],
		"forum_url" => forum::getForumLink($row["forum_id"]),
		"forum_desc" => $row["forum_desc"],
		"forum_posts" => $row["forum_posts"],
		"subforums" => $subforums,
	);

	if ($ctx->international) {
		if (($for['forum_name'] != "General Talk") && (!in_array($for['forum_name'], $cats)))
			continue;
	}
	
	$forums[] = $for;
}
//_darr($forums);

if ($ctx->international)
	$forums = dir::sortCategoryArray($forums, "forum_name", false);

if ($ctx->international) {
    $ctx->addToBreadcrumb(array("link" => "{$ctx->location_path}/sex-forum", "name" => "Sex Forum"));
}

$smarty->assign("link_back", $ctx->location_path);
$smarty->assign("location_name", $ctx->location_name);
$smarty->assign("forum_search_url", "forum_search");
$smarty->assign("forum_new_topic_url", "forum-new-topic");
$smarty->assign("forums", $forums);
$smarty->display(_CMS_ABS_PATH."/templates/sex-forum/forums.tpl");

?>
