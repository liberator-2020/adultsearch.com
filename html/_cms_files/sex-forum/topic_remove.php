<?php
/**
 * Admin controller: deletes forum topic and all its comments
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $account;
$system = new system;

if (!($account_id=$account->isLoggedIn())) {
	$account->asklogin();
	return;
}

if (!$account->isadmin()) {
	echo "You dont have permissions to perform this action!<br />";
	die;
}

$id = intval($_REQUEST["id"]);
if (!$id) {
	echo "Forum topic id not specified!<br />";
	die;
}

//get forum topic
$topic = topic::findOneById($id);
if ($topic == false) {
	flash::add(flash::MSG_ERROR, "Cant find topic id #{$id} !");
	$system->go("/");
}

//remove topic and its posts
$ret = $topic->remove();

if ($ret) {
	flash::add(flash::MSG_SUCCESS, "Forum topic #{$id} successfully removed");
} else {
	flash::add(flash::MSG_ERROR, "Error: Failed to remove forum topic #{$id} ! Please contact administrator.");
}
$system->go("/");

die;

?>
