<?php
/**
 * Admin controller: deletes forum topic and all its comments and ban user, who posted it
 */
defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $account;
$system = new system;

if (!($account_id=$account->isLoggedIn())) {
	$account->asklogin();
	return;
}

if (!$account->isadmin()) {
	echo "You dont have permissions to perform this action!<br />";
	die;
}

$id = intval($_REQUEST["id"]);
if (!$id) {
	echo "Forum topic id not specified!<br />";
	die;
}

$author_id = intval($_REQUEST["account_id"]);

//get forum topic
$topic = topic::findOneById($id);
if ($topic == false) {
	flash::add(flash::MSG_WARNING, "Can't find topic id #{$id} ! It is probably already deleted.");
	if (!$author_id)
		$system->go("/");
} else {
	$author_id = $topic->getAccountId();

	//remove topic and its posts
	$ret = $topic->remove();
	if ($ret) {
		flash::add(flash::MSG_SUCCESS, "Forum topic #id {$id} successfully removed.");
	} else {
		flash::add(flash::MSG_ERROR, "Error: Failed to remove forum topic #id {$id} ! Please contact administrator.");
	}
}

//get author of forum topic
$author = account::findOnebyId($author_id);
if ($author == false) {
	flash::add(flash::MSG_WARNING, "Cant find account of forum topic author (account_id={$author_id}) !");
	$system->go("/");
}

$ret = $author->ban();
if ($ret) {
	flash::add(flash::MSG_SUCCESS, "Author of forum topic <strong>{$author->getEmail()}</strong> (account_id #{$author->getId()}) successfully banned.");
} else {
	flash::add(flash::MSG_ERROR, "Error: author of forum topic <strong>{$author->getEmail()}</strong> (account_id #{$author->getId()}) was not banned.");
}
$system->go("/");

die;

?>
