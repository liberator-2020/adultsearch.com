<?php

defined('_CMS_FRONTEND') or die('Restricted access');

include_html_head('css', '/css/forum.css');

global $gFilename, $gTitle, $ctx, $db, $smarty, $account, $mobile, $gBreadCat;

$forum = new Forum();
$system = new system;

//get location info
if (($loc_arr = forum::getForumLocation()) === false) {
	echo "Invalid access.";
	die;
}
list($loc_id, $loc_name, $loc_link, $exact_loc_id) = $loc_arr;

$loc_path = $ctx->location_path."/";	 //TODO
$forum_link = $ctx->location_path."/sex-forum";
$forum_id = $ctx->forum_id;


//------------------------
//------ breadcrumb ------
$f = $forum->_coreForumBreadReverse($forum_id);
$fcat = array_reverse($f);
$ctx->addToBreadcrumb(array("link" => "{$ctx->location_path}/sex-forum", "name" => "Sex Forum"));
foreach($fcat as $item) {
	$ctx->addToBreadcrumb(array("link" => $item['forum_url'], "name" => htmlspecialchars($item['forum_name'])));
}

// forum details
//_d("select * from forum_forum where forum_id = $forum_id and loc_id = '$loc_id'");
$res = $db->q("select * from forum_forum where forum_id = ? and loc_id = ?", array($forum_id, $loc_id));
if( $db->numrows($res) == 0 ) { 

	if ($ctx->city_id && $ctx->city_id != $loc_id ) {
		$res = $db->q("select * from forum_forum where forum_id = $forum_id and loc_id = '$ctx->city_id'");
		if ($db->numrows($res) == 0) {
			$db->q("insert ignore into forum_forum (forum_id, loc_id, loc_parent, forum_name, forum_desc, forum_parent, forum_order)
				(select '$forum_id', '$ctx->city_id', '$ctx->state_id', forum_name, forum_desc, forum_parent, forum_order from forum_forum where forum_id 
			= '$forum_id' limit 1)"); 
			//reportAdmin('forum new added', NULL, 'burak');
			$system->reload();
		} $loc_id = $ctx->city_id;
	} else {
		//this forum does not exist for this location, check if "general talk" exists and if yes, then redirect to this forum
		$res = $db->q("SELECT * FROM forum_forum WHERE forum_id = 1 AND loc_id = ?", array($ctx->city_id));
		if ($db->numrows($res) != 0) {
			$system->moved($ctx->location_path."/sex-forum/general-talk");
			die;
		}
		//hmm, no forums for this location ?? should not happen, redirect to city page
		$system->moved($ctx->location_path);
		die;
	}
}
$rox2 = $db->r($res);
$smarty->assign("forum_id", $forum_id);
$smarty->assign("forum_topics", $rox2["forum_topics"]);
$smarty->assign("forum_posts", $rox2["forum_posts"]);

$account_id = $account->isloggedin();
if( $account->isloggedin() ) {
	$new_topics = array();
	$res = $db->q("select t.forum_id, t.topic_id, t.topic_last_post_time from 
		forum_topic t 
		inner join (forum_forum f left join forum_forum f2 on f.forum_id =f2.forum_parent and f.loc_id = f2.loc_id) on t.forum_id = f.forum_id and t.loc_id = f.loc_id 
		where t.topic_last_post_time > '{$_SESSION["last_visit"]}' and f.forum_parent = '$forum_id' and f.loc_id = '$loc_id'");
		while ($cur_topic = $db->r($res)) {
				$new_topics[$cur_topic['forum_id']][$cur_topic['topic_id']] = $cur_topic['topic_last_post_time'];
		}

	$tracked_topics = $forum->get_tracked_topics();
}

$gTitle = "{$rox2["forum_name"]} Forums - {$loc_name}";
$smarty->assign("forum_title",$rox2['forum_name']);

$today = mktime(date("G"),date("i"),date("s"),date("m"),date("d")-1, date("y"));

// alt forums
$sql = "select *, if(unix_timestamp(forum_last_post_time)>$today,1,0) as updated 
	from forum_forum 
	where forum_parent = $forum_id and loc_id = '$loc_id' 
	order by forum_name";
$res = $db->q($sql);
$alt_forums = array();
while($row=$db->r($res)) {

		$new = false;
		if ($account_id 
		&& $new_topics[$row['forum_id']] 
		&& $row['forum_last_post_time'] > $_SESSION['last_visit'] 
		&& (empty($tracked_topics['forums'][$row['forum_id']]) ||$row['forum_last_post_time'] > $tracked_topics['forums'][$row['forum_id']])
		) {
				foreach ($new_topics[$row['forum_id']] as $check_topic_id => $check_last_post) {
						if ((empty($tracked_topics['topics'][$check_topic_id]) || $tracked_topics['topics'][$check_topic_id] < $check_last_post) &&(empty($tracked_topics['forums'][$row['forum_id']]) || $tracked_topics['forums'][$row['forum_id']] < $check_last_post)){
								$new = true;
								break;
						}
				}
		}

	$alt_forums[] = array(
		"forum_id" => $row["forum_id"],
		"forum_name" => $row["forum_name"],
		"forum_link" => '',
		"forum_desc" => $row["forum_desc"],
		"forum_topics" => $row["forum_topics"],
		"forum_posts" => $row["forum_posts"],
		"updated" => $row["updated"] ? true : false,
		"new"=>$new
	);
}
$smarty->assign("alt_forums", $alt_forums);

$subcat_cols=5;
if (!empty($alt_forums)) {
	$subcat_per_column = ceil(count($alt_forums)/$subcat_cols);
	$subcategories_columns = array_chunk($alt_forums, $subcat_per_column);
}

$smarty->assign("states", $subcategories_columns);
$smarty->assign("missing", (count($alt_forums)%5));

//get important sql part
$important = "";
if ($ctx->international) {
	$sql = "select loc_id from location_location where loc_parent = '{$ctx->country_id}' and loc_type = 3";
	$res = $db->q($sql);
	$ids = "";
	while($row=$db->r($res)) {
		$ids .= (empty($ids)) ? $row["loc_id"] : ",".$row["loc_id"];
	}
	if (!empty($ids))
		$important = "or (t.important = 1 and t.loc_id IN ({$ids}))";
}

//--------------
//display topics

$topics_on_page = 10;
$cur_page = (isset($_REQUEST["page"])) ? intval($_REQUEST["page"]) : 1;
if ($cur_page <= 0)
	$cur_page = 1;
$start = ($cur_page - 1) * $topics_on_page;

$sql = "SELECT SQL_CALC_FOUND_ROWS
	t.*, 
	a.username, 
	(SELECT account.username FROM forum_post left join account on forum_post.account_id = account.account_id WHERE post_id = t.topic_last_post_id limit 1) as username2, 
	(SELECT account.account_id FROM forum_post left join account on forum_post.account_id = account.account_id WHERE post_id = t.topic_last_post_id limit 1) as account_id2, 
	p.posted

	FROM forum_post p, forum_topic t 
	LEFT JOIN account a on t.account_id = a.account_id 

	WHERE ((t.forum_id = $forum_id and t.loc_id = '$loc_id') {$important}) 
		AND t.topic_last_post_id = p.post_id 
	ORDER BY important desc, topic_last_post_time desc
	LIMIT {$start}, {$topics_on_page}";

$res = $db->q($sql);
$num_topic = $db->numrows($res); 

$topics = array();
while($row=$db->r($res)) {
	if ($row["moved"])
		$row["topic_id"] = $row["moved"];

	$lastlink = $row["username2"] ? substr($row["username2"],0,15) : "Deleted Member";
	$userlink = $row["username"] ? $row["username"]: "";

	$new = false;
	if ($account_id
		&& $row['topic_last_post_time'] > $_SESSION['last_visit'] 
		&& (!isset($tracked_topics['topics'][$row['topic_id']]) || $tracked_topics['topics'][$row['topic_id']] < $row['topic_last_post_time']) 
		&& (!isset($tracked_topics['forums'][$forum_id]) || $tracked_topics['forums'][$forum_id] <$row['topic_last_post_time'])
		) {
		$new = true;
	}

	$topics[] = array(
		"topic_id" => $row["topic_id"],
		"topic_url" => forum::getTopicLink($row["topic_id"]),
		"topic_title" => $row["topic_title"],
		"important" => $row["important"],
		"topic_replies" => $row["topic_replies"],
		"lastPostDate" => $row["posted"],
		"lastPostUser" => $lastlink,
		"userlink" => $userlink,
		"account_id" => $row["account_id"],
		"moved" => $row["moved"] ? true : false,
		"paging" => "",
		"new" =>$new
	);
}
$smarty->assign("topics", $topics);

$res = $db->q("SELECT FOUND_ROWS()");
$row = $db->r($res);
$num_topic = $row[0];

$smarty->assign("pager", $forum->getPagingArray($num_topic, $cur_page, forum::getForumLink($forum_id), $topics_on_page));

$smarty->assign("forum_search_url", $ctx->location_path."/sex-forum/forum_search");
$smarty->assign("topic_new_link", forum::getForumLink($forum_id)."/new_topic");

if ($mobile == 1) {
	//TODO - make this transparent mobile/desktop
	//for mobile template we need to add links, because there is no breadcrumb
	$smarty->assign("loc_path", $loc_path);
	$smarty->assign("forum_link", $forum_link);
}

$smarty->assign("link_back", $ctx->location_path."/sex-forum");

$smarty->display(_CMS_ABS_PATH."/templates/sex-forum/forum.tpl");

?>
