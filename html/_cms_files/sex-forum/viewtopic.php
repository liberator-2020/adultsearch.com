<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $ctx, $db, $smarty, $account, $gFilename, $gTitle, $page, $mobile, $gBreadCat;

include_html_head("css", "/css/forum.css");
include_html_head("js", "/js/tools/bbcode.js");

$dictionary = new dictionary();
$system = new system;
$forum = new forum();

//get location info
if ((($loc_arr = forum::getForumLocation()) === false) && (!isset($_GET["topic_id"]))) {
	echo "Invalid access.";
	die;
}
list($loc_id, $loc_name, $loc_link, $exact_loc_id) = $loc_arr;

//get topic id
if ($ctx->forum_topic_id == NULL) {
	$system->go('/', 'Invalid access.');
	die;
}
$topic_id = $ctx->forum_topic_id;

//paging
$start = intval($_GET['start']);
if( !$start ) $start = 10;
$start2 = $start;
$start = $start - 10;
$item_per_page = 20;
$IsReply = 0;

//get topic info
$res = $db->q("SELECT * FROM forum_topic WHERE topic_id = ?", [$topic_id]);
if( !$db->numrows($res) )
		$system->go('/', 'Topic has been removed.');

$row_topic = $db->r($res);

$loc_id = $row_topic["loc_id"];
if (empty($loc_name) || empty($loc_link)) {
	//if location was not set in url, get it from fetch it by topic->loc_id
	//this can happen with old domestic places and email links like http://adultsearch.com/sex-forum/viewtopic?topic_id=12951&p=14025#14025
	$res = $db->q("select loc_name, loc_url from location_location where loc_id = ?", [$loc_id]);
	if(!$db->numrows($res)) {
		$system->go('/', 'Topic has been removed.');
	}
	$row = $db->r($res);
	if (empty($loc_name))
		$loc_name = $row['loc_name'];
	if (empty($loc_link))
		$loc_link = $row['loc_url'];
}

$f = $forum->_coreForumBreadReverse($row_topic["forum_id"]);
$forum_parent = $f[1]["forum_id"];
$forum_name = $f[0]["forum_name"];
$forum_id = $row_topic["forum_id"];
$smarty->assign("forum_title", $forum_name);

$gTitle = $row_topic["topic_title"] . ($page>1 ? " - Page $page" : "") . (" - ".htmlspecialchars($forum_name)." - {$loc_name}") ;

$fcat = array_reverse($f);
if ($ctx->international) {
	$ctx->addToBreadcrumb(array("link" => "{$ctx->location_path}/sex-forum", "name" => "Sex Forum"));
	foreach($fcat as $item) {
		$ctx->addToBreadcrumb(array("link" => $item['forum_url'], "name" => htmlspecialchars($item['forum_name'])));
	}
	$ctx->addToBreadcrumb(array("link" => forum::getTopicLink(), "name" => htmlspecialchars($row_topic["topic_title"])));
	$loc_path = $ctx->location_path."/";
	$forum_link = $ctx->location_path."/sex-forum";
} else {
	//$smarty->assign("f", $fcat);
	$gBreadCat = "<a href=\"{$account->core_loc_array['loc_url']}sex-forum/\" title=\"Sex Forum\">Sex Forum</a>";
	foreach($fcat as $for) {
		if( $gBreadCat ) $gBreadCat .= " &raquo; ";
		$gBreadCat .= "<a href=\"{$account->core_loc_array['loc_url']}{$for['forum_link']}\" title=\"{$for['forum_name']}\">{$for['forum_name']}</a>";
	}
	$smarty->assign("breadcat", str_replace('//', '/', $gBreadCat));
	$smarty->assign("breadplace", $row_topic["topic_title"]);
	$loc_path = $account->core_loc_array['loc_url'];
	$forum_link = $account->core_loc_array['loc_url']."/sex-forum";
}


///Find main forums
$posts = array();

$res_f = $db->q("
	SELECT p.*,  
			DATE_FORMAT(post_lastedit_date, '%b %D %Y %H:%i') as edited_date, 
			a.username, a.forum_post, date_format( FROM_UNIXTIME(a.created_stamp), '%b %Y' ) as member_since, a.avatar 
	FROM forum_post p 
	LEFT JOIN account a ON a.account_id = p.account_id
	WHERE p.topic_id = ?
	ORDER BY p.post_id",
	array($topic_id)
	);

if (!empty($row_topic["item_id"])) {
	$item_id = $row_topic["item_id"];
	$res = $db->q("SELECT name, thumb FROM place WHERE place_id = ?", [$item_id]);
	if ($db->numrows($res)) {
		$row = $db->r($res);
		$picture = $row["thumb"] ? "https://img.adultsearch.com/place/t/". $row["thumb"] : "/images/placeholder.gif";
		$first_post = "<a href=\"/dir/place?id={$item_id}\" title=\"{$row["name"]}\"><img border=\"0\" src=\"$picture\" alt=\"{$row["name"]} picture\" /></a> This topic refers to <a href=\"/dir/place?id={$item_id}\" title=\"{$row["name"]}\">{$row["name"]}</a><hr size=\"1\" />";
	}
}


$post_count = 0;
$_dScript = NULL;
$_dContent = NULL;
while ($SQLrow=$db->r($res_f)) {
	$avatar = $SQLrow["avatar"] ? "{$config_image_server}/avatar/".$SQLrow["avatar"] : NULL;
	if( $avatar )
		$avatar = "<img src=\"$avatar\" alt=\"\" />";
	else
		$avatar = "<img src=\"/images/sex-forum/User_UnKnown.gif\" alt=\"\" />";

	$can_edit = $account->isloggedin() == $SQLrow['account_id'] ? true : false;

	$post_text = $forum->message($SQLrow['post_text']);
	$post_text = $forum->smile($post_text);
	$post_text = $forum->auto_url($post_text);
	$post_text = nl2br($post_text);

	$post_text = $dictionary->process($post_text);

	$posts[] = array(
		"topic_id" => $SQLrow['topic_id'],
		"account_id" => $SQLrow['account_id'], 
		"post_time" => date("m/d/Y h:i a", $SQLrow["posted"]),
		"post_text" => $post_count == 0 ? $first_post.$post_text : $post_ex.$post_text,
		"post_id" => $SQLrow['post_id'],
		"author_id" => $SQLrow['account_id'],	
 		"username" => $SQLrow['username'],	
 		"forum_post" => $SQLrow['forum_post'],
 		"member_since" => $SQLrow['member_since'],
		"post_edited" => $SQLrow['post_edited'],	
		"edited_date" => $SQLrow['edited_date'],	
		"post_edit_count" => $SQLrow['post_edit_count'],
		"post_edit_reason" => $SQLrow['post_edit_reason'],
		"avatar" => $avatar,
		"can_edit" => $can_edit,
		"can_del" => $can_del,
		"quote_link" => forum::getForumQuoteLink($SQLrow['post_id'], $topic_id, $forum_id),
		"edit_link" => forum::getForumEditLink($SQLrow['post_id'], $topic_id, $forum_id),
	);
	$post_ex = "";

	$post_count++;
}
if( !empty($_dScript) ) {
	$smarty->assign("tooltipScript", $_dScript);
	$smarty->assign("tooltipContent", $_dContent);
}

$smarty->assign("icons", $forum->icons());

$smarty->assign("can_delete", $account->isadmin());

$smarty->assign("forum_locked", $row_topic["locked"]);
$smarty->assign("topic_id", $topic_id);
$smarty->assign("topic_title", $topic_title);
$smarty->assign("topic_time", $topic_time);
$smarty->assign("topic_last_post_id", $topic_last_post_id);

$count = count($posts);
$cur_page = (isset($_REQUEST["page"])) ? intval($_REQUEST["page"]) : 1;
if ($cur_page == 0)
	$cur_page = 1;
$smarty->assign("posts", pager::filter_array($posts, $cur_page, 20));
$smarty->assign("pager", pager::paging_array($count, $cur_page, forum::getTopicLink($row_topic["topic_id"]), false, 20, 5));

$smarty->assign("forum_topics", $forum_topics);
$smarty->assign("forum_name", htmlspecialchars($forum_name));
$smarty->assign("forum_url", forum::getForumLink($forum_id));

$smarty->assign("account_id", $row_topic["account_id"]);
$smarty->assign("topic_title", $row_topic["topic_title"]);
$smarty->assign("topic_link", $forum->makeTopicLink($row_topic["topic_id"], $row_topic["topic_title"]));

$smarty->assign("count", $count);

$smarty->assign("ismember", $account->isloggedin());

$smarty->assign("forum_reply_link", forum::getForumReplyLink($topic_id, $forum_id));
$smarty->assign("watch_link", forum::getTopicActionLink("watch", $topic_id, $forum_id));
$smarty->assign("unwatch_link", forum::getTopicActionLink("unwatch", $topic_id, $forum_id));
$smarty->assign("deltopic_link", forum::getTopicActionLink("deltopic", $topic_id, $forum_id));
$smarty->assign("move_link", forum::getTopicActionLink("move", $topic_id, $forum_id));
$smarty->assign("delpost_link", forum::getReplyLink("delpost"));

$core_state_name = "<a href='".forum::getForumLink($forum_id)."' title='".htmlspecialchars($forum_name)." Forums - {$loc_name}'>".htmlspecialchars($forum_name)." Forums - {$loc_name}</a>";
$smarty->assign("core_state_name", $core_state_name);

//get previous topic
$res = $db->q("select * from forum_topic where topic_id < '$topic_id' and forum_id = '$forum_id' and loc_id = '$loc_id' order by topic_id desc limit 1");
if( $db->numrows($res) ) {
	$row = $db->r($res);
	$topic_title = strlen($row["topic_title"])>50 ? substr($row["topic_title"],0,50) : $row["topic_title"];
	$previous_topic = "<a href='".forum::getTopicLink($row["topic_id"])."' title='{$row["topic_title"]}'>$topic_title</a>";
	$smarty->assign("previous_topic", $previous_topic);
}

//get next topic
$res = $db->q("select * from forum_topic where topic_id > '$topic_id' and forum_id = '$forum_id' and loc_id = '$loc_id' order by topic_id limit 1");
if( $db->numrows($res) ) {
	$row = $db->r($res);
	$topic_title = strlen($row["topic_title"])>50 ? substr($row["topic_title"],0,50) : $row["topic_title"];
	$next_topic = "<a href='".forum::getTopicLink($row["topic_id"])."' title='{$row["topic_title"]}'>$topic_title</a>";
	$smarty->assign("next_topic", $next_topic);
}

$smarty->assign("loc_name", $loc_name);
$smarty->assign("forum_id", $forum_id);

if( ($account_id=$account->isloggedin()) ) {
	$tracked_topics = $forum->get_tracked_topics();
	$tracked_topics["topics"][$topic_id] = time();
	$forum->set_tracked_topics($tracked_topics);

	if (!$forum->watchTrue($topic_id, $account_id))
		$smarty->assign("watch_enabled", false);
	else
		$smarty->assign("watch_enabled", true);
}

if ($mobile == 1) {
	//TODO - make this transparent mobile/desktop
	//for mobile template we need to add links, because there is no breadcrumb
	$smarty->assign("loc_path", $loc_path);
	$smarty->assign("forum_link", $forum_link);
	$smarty->assign("link_back", forum::getForumLink($forum_id));
}

$smarty->assign("no_wrap", true);
$smarty->display(_CMS_ABS_PATH."/templates/sex-forum/topic.tpl");

?>
