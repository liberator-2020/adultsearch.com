<?php
defined("_CMS_FRONTEND") or die("No Access");

deprecated_controller();

global $db, $smarty, $account, $page, $gTitle, $advertise, $gModule, $gLocation, $gItemID, $mobile, $gDescription, $config_mapquest_key, $config_image_server;

$id = $gItemID;
$account_id = $account->isloggedin();

$review = new review("adult_stores", $id, "sex shops", "", "adultstore");

$res = $db->q("SELECT a.*, l.country_sub, l.loc_url
				FROM adult_stores a 
				LEFT JOIN location_location l on l.loc_id = a.loc_id
				WHERE a.id = '$id'");
if (!$db->numrows($res)) {
	if (empty($account->core_loc_array['loc_url']))
		$account->core_loc_array['loc_url'] = "/";
	system::moved("{$account->core_loc_array['loc_url']}{$gModule}/");
}
$row = $db->r($res);

$name2url = name2url($row['name']);

if ($account->core_sub != $row["country_sub"] || !strstr($_SERVER['REQUEST_URI'], $row['loc_url']) || $gLocation != $name2url)
	system::moved(location::getUrlStatic($row["country_sub"], $row["loc_url"])."{$gModule}/{$name2url}/{$row['id']}");

$smarty->assign('breadplace', $row['name']);
$advertise->showAd(8, $row["loc_id"]);

$gTitle = "{$row["name"]} ". makeproperphonenumber($row['phone']) ." {$row['loc_name']} Sex Shops";

$mobile_class = $mobile == 1 ? " class='link'" : "";

$loc = "<a href='{$account->core_loc_array['loc_url']}$gModule/' title='{$row["loc_name"]} sex shops'$mobile_class>{$row["loc_name"]}</a>, {$row["state"]} {$row["zipcode"]}";
$smarty->assign("link_back", "{$account->core_loc_array["loc_url"]}$gModule/");

//map
$map = prepareMap($id, 'adult_stores', $row, 'adultstore', 'adult_stores');
$smarty->assign("map", $map);
$smarty->assign("link_map_update", "/map?id={$id}&section=adultstore&update=1");

$quickread = $review->reviews();
$smarty->assign("quickread", $quickread);

$phone = makeproperphonenumber($row["phone"]);
if( empty($quickread) && !empty($row['gdescription']) ) {
  $gDescription = $row['gdescription'];
  $smarty->assign("description", $gDescription);
} else {
		if( !empty($quickread) ) $gDescription = substr(htmlspecialchars($quickread[0]['review'], ENT_QUOTES), 0, 100) .". ";
		$gDescription .= "$phone {$row['name']} {$row['address1']} Adult sex shops";
}

global $ctx;
$ctx->item = $row;
$smarty->assign("hours", dir::getHours(($row["byapponly"] == 1) ? true : false));
$smarty->assign("current_weekday", strtolower(date("l")));


if( !empty($row["other_services"]) )
	$smarty->assign("info", $row["other_services"]);

$lingerie = $row["lingerie"] == 2 ? "Yes" : ($row["lingerie"] == 1 ? "No" : 0);
$adulttoys = $row["adult_toys"] == 2 ? "Yes" : ($row["adult_toys"] == 1 ? "No" : 0);
$adultdvd = $row["adult_dvds"] == 2 ? "Yes" : ($row["adult_dvds"] == 1 ? "No" : 0);
$peepshows = $row["peep_shows"] == 2 ? "Yes" : ($row["peep_shows"] == 1 ? "No" : 0);
$adultbooks = $row["adult_books"] == 2 ? "Yes" : ($row["adult_books"] == 1 ? "No" : 0);
$arcade = $row["adult_video_arcade"] == 2 ? "Yes" : ($row["adult_video_arcade"] == 1 ? "No" : 0);

if( $lingerie )
	$c1[] = array("label" => "Lingerie", "val" => "{$lingerie}");
if( $adulttoys )
	$c1[] = array("label" => "Adult Toys", "val" => "{$adulttoys}");
if( $adultdvd )
	$c1[] = array("label" => "Adult DVDs", "val" => "{$adultdvd}");
if( $peepshows )
	$c1[] = array("label" => "Peep Shows", "val" => "{$peepshows}");
if( $adultbooks )
	$c1[] = array("label" => "Adult Books", "val" => "{$adultbooks}");

if( $arcade )
	$c1[] = array("label" => "Adult Video Arcade", "val" => "{$arcade}");
if( $row["theaters"] ) {
	$theater = $row["theaters"] == 2 ? "Yes" : "No";
	$c1[] = array("label" => "Theaters", "val" => "{$theater}");
	if( $row["theater_couple_fee"] == -1)
		$row["theater_couple_fee"] = "Free";
	elseif( $row["theater_couple_fee"] )
		$row["theater_couple_fee"] = "\${$row["theater_couple_fee"]}";
	$single_text = $row["theater_single_fee"] ? "Theater Price Singles" : "Theater Price";
	if( $row["theater_single_fee"] )
		$c1[] = array("label" => "{$single_text}", "val" => "\${$row["theater_single_fee"]}");
	if( $row["theater_couple_fee"] )
		$c1[] = array("label" => "Theater Price Couples", "val" => "{$row["theater_couple_fee"]}");
}
if( $row["fetish"] ) {
	$c1[] = array("label" => "Fetish", "val" => ($row["fetish"] == 2) ? "Yes" : "No");
}
if( $row["lgbt"] ) {
	$c1[] = array("label" => "LGBT", "val" => ($row["lgbt"] == 2) ? "Yes" : "No");
}
if( $row["truckparking"] ) {
	$c1[] = array("label" => "Truck Parking", "val" => ($row["truckparking"] == 1) ? "Yes" : "No");
}
if( $row["gloryhole"] )
	$c1[] = array("label" => "Glory Holes", "val" => ($row["gloryhole"]==2) ? 'Yes':'No');

if( $row["buddy_booth_available"] )
	$c1[] = array("label" => "Buddy Booths", "val" => "Yes");

if( !empty($row["coupon"]) )
	$c1[] = array("label" => "Coupon", "val" => "<a href='coupon?coupon={$row["coupon"]}' title='coupon' target='_blank' rel='nofollow'>Click to see</a>");

$smarty->assign("c1", $c1);

$smarty->assign("category", "sex-shop");
$smarty->assign("gModule",$gModule);
$smarty->assign("what", "Sex Shops");

$smarty->assign("name", $row["name"]);
$smarty->assign("loc_name", $row["loc_name"]);
$smarty->assign("id", $id);
$smarty->assign("editlink", "adultstore");
$smarty->assign("totalreview", $row["review"]);
$smarty->assign("overall", $row["overall"]);

if( strlen($row["website"])>1 ) {
	$url = trim($row["website"]);
	if( strncmp($url, "http://", 7) )  $url = "http://" . $url;
	$web = parse_url($url);
	$web_url = $url;
	$web_host = $web["host"];
}

$phone = makeproperphonenumber($row["phone"]);
if ($row['fax'])
	$fax = makeproperphonenumber($row["fax"]);

$addressLx = "<b>{$phone}</b><br/>";

if ($row["phone2"]) {
	$phone2 = makeproperphonenumber($row["phone2"]);
	$addressLx = "{$phone2}<br/>";
}

if ($fax)
	$addressLx .= " Fax: $fax<br/>";
$addressLx .= "{$row["address1"]}<br />{$loc}<br/>";
if($web_url)
	$addressLx .= "<a href=\"$web_url\" target=\"_blank\" rel=\"nofollow\" class=\"website\">$web_host</a>";
if ($row['email'])
	$addressLx .= "<a href=\"mailto:{$row['email']}\" class=\"website\">{$row['email']}</a>";
$smarty->assign("address", $addressLx);

//photos
$imagedir = "adultstore";

$pics = array();
$rex = $db->q("select picture, filename, thumb from place_picture where id = '$id' and module = 'adultstore'");
while($rox=$db->r($rex)) {
	$pics[] = array(
		"module" => "adultstore",
		"id" => $rox["picture"],
		"image" => "{$config_image_server}/{$imagedir}/".$rox["filename"],
		"thumb" => "{$config_image_server}/{$imagedir}/t/".$rox["thumb"],
		);
}

$photos = array();
if (($row["image"] != "" && $row["thumb"] != "") || count($pics) > 0) {
	if ($row["image"] != "" && $row["thumb"] != "") {
		$photos["image"] = "{$config_image_server}/{$imagedir}/{$row["image"]}";
		$photos["thumb"] = "{$config_image_server}/{$imagedir}/t/{$row["thumb"]}";
	} else {
		$pic = array_shift($pics);
		$photos["image"] = $pic["image"];
		$photos["thumb"] = $pic["thumb"];
	}

	if ($pics) {
		include_html_head("js", "/js/jquery.tools.scrollable.js");
		$pics_in_group = 2;
		$pics_groups = array_chunk($pics, $pics_in_group);
		$photos["pics"]  = $pics_groups;
	}
}
$smarty->assign("photos", $photos);

if (!empty($row["owner"]) && ($row["owner"]==$account_id||$account->isrealadmin()))
	$smarty->assign("isowner", true);

if( empty($row["owner"]) ) {
	$smarty->assign("business_link", true);
	$smarty->assign("business_shortcut", "?section=as&id=$gItemID");
}

$core_state_name = "Click to see <a href='{$account->core_loc_array['loc_url']}$gModule/' title='{$row["loc_name"]} Sex Shops' class='bold'>{$row["loc_name"]} Sex Shops</a> listing.";
$smarty->assign("header_title", $core_state_name);

include_html_head("css", "/css/info.css");

//MapQuest and Leaflet include
include_html_head('js', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.js');
include_html_head('css', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.css');
include_html_head('js', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.js');
include_html_head('css', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.css');

/*
//forum
$forum_id = 129;
$smarty->assign("forum_topic", forum::getLastTopicsForPlace($forum_id, $id));
$smarty->assign("new_topic_link", forum::getNewTopicLinkForCategory($forum_id, $id));
*/

if (permission::has("edit_all_places"))
	$smarty->assign("can_edit_place", true);

$smarty->display(_CMS_ABS_PATH."/templates/place/place_place.tpl");

?>
