<?php

deprecated_controller();

global $db, $smarty, $account, $page;
$system = new system;

if( !($account_id = $account->isloggedin()) ) {
	$account->asklogin();
	return;
}

require_once(_CMS_ABS_PATH."/_cms_files/worker/array_sc.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
$form = new data;
$system = new system;

$form->table = "adult_stores";

$c = new CColumnId("id", "BUSINESS ID");
$form->AppendColumn($c);

$c = new CColumnString("name", "Store Name");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("address1", "Store Address");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "Zip Code");
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Store Phone");
$c->setColMandatory(true);
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnPhone("fax", "Store Fax");
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnHour("hours", "Hours Of Operation");
$form->AppendColumn($c);

$c = new CColumnEnum("lingerie", "Lingerie");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("adult_toys", "Adult Toys");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("adult_dvds", "Adult DVD");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("peep_shows", "Peep Shows");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("adult_books", "Adult Books");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("adult_video_arcade", "Arcade");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("gloryhole", "Glory Hole");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("theaters", "Theaters");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("fetish", "Fetish");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("lgbt", "LGBT");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnBoolean("truckparking", "Truck Parking");
$c->setColTitle("Parking");
$form->AppendColumn($c);

$c = new CColumnBoolean("buddy_booth_available", "Buddy Booths Available");
$c->setColTitle("Buddy Booths");
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setWatermark();
$c->setPath("adultstore");
$form->AppendColumn($c);

$c = new CColumnImage("thumb", "Thumbnail");
$c->setDimensions(220, 165);
$c->setWatermark();
$c->setPath("adultstore/t");
$form->AppendColumn($c);

$c = new CColumnImage("coupon", "Coupon");
$c->setMaxDimensions(700, 500);
$c->setPath("adultstore/c");
$form->AppendColumn($c);

$c = new CColumnPicture("adult_store_picture", "Extra Pictures", "adultstore");
$c->setPath("adultstore");
$c->setPathT("adultstore/t");
$form->AppendColumn($c);

$form->owner_mode = true;
$form->owner_link = "store";
$form->ShowPage();

?>

