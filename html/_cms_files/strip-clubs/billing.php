<?php

defined('_CMS_FRONTEND') or die('Restricted access');

deprecated_controller();

global $smarty, $db, $account;
$system = new system;

$id = intval($_GET["id"]);
if( !$id ) $system->moved("/");

if( !($account_id = $account->isloggedin()) ) {
	$account->asklogin();
	return;
}

$res = $db->q("select * from strip_club2 where id = '$id'");
if( !$db->numrows($res) )  $system->moved("/");
$row = $db->r($res);

if( $row["owner"] != $account_id && !$account->isrealadmin() ) $system->go("/stripclubs/club?id=".$id, "This club is owned by another member.");

if( isset($_GET["action"]) && !empty($_GET["action"]) ) {
	if( $account->isrealadmin() ) $account_id = $row["owner"];
	if( $_GET["action"] == "cancel" ) {
		$res = $db->q("update account_businessowner set active = 0 where place_id = '$id' and section = 'sc' and account_id = '$account_id'");
		if (!$account->isrealadmin())
			reportAdmin("erotic massage owner canceled..", "http://adultsearch.com/stripclubs/club?id=".$id);
	} else 
		$res = $db->q("update account_businessowner set active = 1 where place_id = '$id' and section = 'sc' and account_id = '$account_id'");
	$system->moved("/stripclubs/billing?id=".$id);
}

$res = $db->q("SELECT cc.*, date_format(b.starts, '%M %D %Y %h:%i %p') starts, date_format(b.expires, '%M %D %Y %h:%i %p') expires, b.total, b.c, b.active
				FROM account_businessowner b
				LEFT JOIN account_cc cc using (cc_id)
				WHERE b.place_id = '$id' and b.section = 'sc' and cc.deleted = 0");
if( $db->numrows($res) ) {
	$row = $db->r($res);

	$cc = preg_replace("/\d{12}/", "**** **** **** ", $row["cc"]);
	$ref = rawurlencode("/stripclubs/billing?id=".$id);

	$link = $row["active"] ? "<a href='?id=$id&action=cancel'>Cancel the recurring payment & advertising</a>" : "<a href='?id=$id&action=ok'>Activate Advertising & the payment</a>";

	$form[] = array("name"=>"Advertising status", "value"=>$row["active"]?"Active $link":"Disabled $link");
	$form[] = array("name"=>"Next Billing Cycle", "value"=>$row["active"]&&$row["expires"]?$row["expires"]:"N/A");
	$total = number_format($row["total"], 2);
	$form[] = array("name"=>"Charge", "value"=>"\$$total");
	$form[] = array(
		"name"=>"CC", 
		"value"=>"{$cc} - ({$row["expmonth"]}/{$row["expyear"]}) <a href='https://adultsearch.com/account/updatecc?id={$row["cc_id"]}&ref=$ref'>Update CC Information</a>"
		);
	$smarty->assign("form", $form);
}

$smarty->assign("item_link", "club");
$smarty->assign("id", $id);
$smarty->display(_CMS_ABS_PATH."/templates/place/place_billing.tpl");

?>
