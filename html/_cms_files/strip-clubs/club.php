<?php
defined("_CMS_FRONTEND") or die("No Access");

global $config_site_url;

$old_id = intval($_REQUEST["id"]);
$place_types = [1];

$url = dir::getPlaceLinkByOldId($old_id, $place_types);
if (!$url)
	$url = $config_site_url;

system::moved($url);

