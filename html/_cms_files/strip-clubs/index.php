<?php

deprecated_controller();

defined("_CMS_FRONTEND") or die("No Access");

require_once _CMS_ABS_PATH."/_cms_files/worker/array_sc.php";

global $ctx, $db, $smarty, $page, $account, $gTitle, $advertise, $mobile, $gModule, $gLocation, $gDescription, $sphinx, $config_image_server;
$form = new form;
$system = new system;

session_write_close();

$location = location::findOneById($ctx->location_id);
if (!$location) {
	return $system->go("http://adultsearch.com/homepage");
} else {
	$smarty->assign("link_back", $location->getUrl());
	$smarty->assign("location_video_html", $location->getVideoHtml());
}

//seo
$res = $db->q("SELECT description FROM seo WHERE location_id = ? AND type = 'LPT' AND place_type_id = 1 LIMIT 1", array($location->getId()));
if ($db->numrows($res)) {
	$row = $db->r($res);
	$smarty->assign("category_description", $row["description"]);
}

$core_state_id = $account->corestate(1);
if (!$core_state_id)
	return;

$loc_around = array();
if ($account->core_loc_id && $account->core_loc_type > 2)
	$loc_around = $account->locAround();

$bar = isset($_GET["bar"]) ? GetGetParam("bar") : NULL;
$food = isset($_GET["food"]) ? GetgetParam("food") : NULL;
$clubtype = isset($_GET["clubtype"]) ? GetGetParam("clubtype") : NULL;
$age18 = isset($_GET["age18"]) ? intval($_GET["age18"]) : NULL;
$neighbor = isset($_GET['neighborhood']) ? intval($_GET['neighborhood']) : NULL;

$advertise->showAd(9, $core_state_id);

$core_state_name = $_SESSION["core_state_name"];
$loc_name = $account->core_loc_array['loc_name'];
$gTitle = "Best {$ctx->location_name} Strip Clubs and Gentleman's Clubs in {$ctx->parent_name}";
$gDescription = "Female strip clubs in {$ctx->location_name}. Find gentleman's clubs, topless bars along with images, directions, contact information and reviews.";

$dancer = isset($_GET["dancer"]) ? intval($_GET["dancer"]) : 1;
if (!$dancer)
	$dancer = 1;

switch($gModule) {
	case 'male-strip-clubs':
		$dancer = 3;
		$what = "Male Strip Club";
		break;
	case 'tstv-strip-clubs':
		$dancer = 5;
		$what = "TS/TV Strip Club";
		break;
	default:
		$dancer = 1;
		$what = "Strip Club";
		break;
}

$filter_link = array();
foreach($_GET as $key => $value) {
	if( $key == "ethx" ) {
		$filter["eth"][] = $value; $filter_link[] = array("id"=>$value,"type"=>"eth","name"=>"Ethnicity");
	} else if( in_array($key, array("bar", "food", "clubtype", "age18", "loc_id", "zipcode", "query", "phone", "neighborhood")) && $value != "" ) {
		$ftype = $key;
		$skipfilter = 0;
		if( $key == "bar" ) { 
			$val = $form->arrayValue($bar_array, $value);
			$key = "Alcohol: $val";
			$gTitle .= " Alchol: $val";
		}
		else if( $key == "food" ) {
			$val = $form->arrayValue($food_array, $value);
			$key = "Food: $val";
			$gTitle .= " Food: $val";
		}
		else if( $key == "clubtype" ) {
			$key = "Club Type: ".$form->arrayValue($clubtype_array, $value);
			$gTitle = str_replace("%tags%", $form->arrayValue($clubtype_array, $value), $gTitle);
		}
		else if( $key == "age18" ) {
			$key = "Age: Age 18 and above";
			$gTitle .= " Age 18 and above";
		}
		else if( $key == "zipcode" ) { $key = "Zip Code: $value"; $skipfilter = 1;
			$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key, "link"=>"{$account->core_loc_array['loc_url']}$gModule/");
		}
		else if( $key == "query" ) { $key = "'$value'"; }
		else if( $key == "phone" ) { $key = "Phone #: '$value'"; }
		else if( $key == "loc_id" ) $key = $loc_name;
		else if( $key == "neighborhood" ) {
			$rex = $db->q("select name from location_neighbor where loc_id = '$value'");
			$rox = $db->r($rex);
			$key = "Neighborhood: <b>{$rox['name']}</b>";
		}

		if (!$skipfilter)
			$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key);
	}
}

if( !empty($gLocation) && !strstr($gLocation, "zipcode") ) {
	$system->goModuleBase();
	$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$loc_name, "link"=>"http://$state_link.{$account->core_host}/$gModule/");
}

$item_per_page = isset($_GET["ipp"]) ? intval($_GET["ipp"]) : 30;
if( $item_per_page > 60 ) $item_per_page = 20;
$start = (($page*$item_per_page)-$item_per_page);
if( $start < 0 ) $start = 0;

$sphinx_index = "stripclub";
$sphinx->reset();
$sphinx->SetLimits( $start, $item_per_page, 1000 );

if( isset($_GET["zipcode"]) && !empty($_GET["zipcode"]) ) {
	$zipcode = preg_replace("/[^0-9a-z]/i","", $_GET["zipcode"]);
	$mile_in = intval($_GET["miles"]); if( !$mile_in ) $mile_in = 10; 
	if( $mile_in == -1 ) $mile_in = 1000;
	$res = $db->q("select z.lat, z.lon, radians(z.lat) latt, radians(z.lon) longg, l.loc_parent from core_zip z inner join location_location l on z.loc_id = l.loc_id 
where z.zip = '$zipcode' limit 1");
	if( $db->numrows($res) ) {
		$s = $db->r($res);
		$lat_me = $s["lat"]; $lon_me = $s["lon"];
		$core_state_id = $s["loc_parent"];
		$latt = $s["latt"]; $long = $s["longg"];
		$sphinx->SetGeoAnchor('loc_lat', 'loc_long', $latt, $long);
		$sphinx->SetFilterFloatRange('@geodist', 0, ($mile_in*1609.344));
		$zipfound = 1;
	} else {
		$error = "Zip code($zipcode) could not find. Please make sure that you typed the correct zip code.";	
	}
	$smarty->assign("miles", $mile_in);
	$smarty->assign("zipcode", $_GET["zipcode"]);
} elseif( $account->core_loc_array['loc_lat'] ) {
	$sphinx->SetGeoAnchor('loc_lat', 'loc_long', deg2rad($account->core_loc_array['loc_lat']), deg2rad($account->core_loc_array['loc_long']));
	$sphinx->SetFilterFloatRange('@geodist', 0, (1000*1609.344));
	//$distance = 1;
}

$sphinx->SetFilter('dancer', array($dancer));
if( !is_null($bar) ) $sphinx->SetFilter('bar', array($bar)); 
if( !is_null($food) ) $sphinx->SetFilter('food', array($food)); 
if( !is_null($clubtype) ) $sphinx->SetFilter('clubtype', array($clubtype)); 
if( $age18 ) $sphinx->SetFilter('age18', array(1)); 
if( !is_null($neighbor) ) $sphinx->SetFilter('neighbor', array($neighbor));
if( $account->core_loc_id && $account->core_loc_type > 2 && !$zipfound) {
	$nloc[] = $account->core_loc_id;
	$sphinx->SetFilter('loc_id', $loc_around);
}
elseif( $account->core_loc_id && $account->core_loc_type < 3 ) $sphinx->SetFilter('state_id', array($account->core_loc_id));

if (!$account->isWorker())
	$sphinx->SetFilter('edit', array(5));

$query = "";

if( isset($_GET["phone"]) && !empty($_GET["phone"]) ) {
		$phone = preg_replace("/[^0-9]/", "", $_GET["phone"]);
	if( strlen($phone) == 10 ) $query = $phone;
		else $smarty->assign("err_phone", true);
}

if( isset($_GET["query"]) && !empty($_GET["query"]) ) {
	$name = preg_replace("/(\()?([0-9]{3})(\))?(.+)?([0-9]{3})(.)?([0-9]{4})/s", "$2$5$7", $_GET["query"]);
	$query .= $query ? " $name" : "$name";
	$error = "No strip club found with the word: ".$_GET["query"];
	$smarty->assign("name", $_GET["query"]);
}

if( $zipfound && empty($_GET["order"]) ) $ordex = "distance";
if( $zipfound && !isset($_GET["orderway"]) ) { $orderway = 2; }
if( isset($_GET["order"]) ) $ordex = $_GET["order"];

$orderway = isset($_GET["orderway"])&& intval($_GET["orderway"]) ? intval($_GET["orderway"]) : (($zipfound||$distance)?2:1);
$way = $orderway == 1 ? "DESC" : "ASC";

if($ordex == 'overall') { $order = "overall $way"; }
elseif($ordex == 'review') $order = "review $way";
elseif($ordex == "name") $order = "name $way";
elseif($ordex == "last_review_id") $order = "last_review_id $way";
elseif($ordex == "score") $order = "overall $way";
elseif($_GET["order"] == 'distance' || ($zipfound||$distance) ) { $order = "sponsor DESC, @geodist $way"; }
elseif($_GET["order"] == 'coupon') $order = "coupon $way";
elseif($_GET["order"] == 'video') $order = "video_cnt $way";
else $order = "sponsor DESC, last_review_id DESC";
$sphinx->SetSortMode(SPH_SORT_EXTENDED, $order);

$results = $sphinx->Query ( $query, 'stripclub' );
$total = $results["total_found"];
$result = $results["matches"];

//_darr($result);

if( !$total && $loc_id ) {
	$sphinx->ResetFilter('loc_id');
	$results2 = $sphinx->Query ( $query, 'stripclub' );
	$total2 = $results2["total_found"];
	$alert = array("ul"=>array("You might want to check out the <a href='/stripclubs/' title='$core_state_name strip clubs'>strip clubs in 
$core_state_name</a> ($total2 total) "));
}

$refine = NULL;
$category = NULL;

/* NY neighborhoods under the 5 borough */
		
if( in_array($account->core_loc_array['loc_id'], array(42838, 42840, 42841, 42842, 18310)) ) {
	$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$account->core_loc_array['loc_name'], "link"=>"/new-york/new-york-city/$gModule/$xxx");
}

if( $total && is_null($neighbor) && in_array($account->core_loc_id, array(18308,42838, 42840, 42841, 42842, 18310))) {
		if( $account->core_loc_id == 18308 ) {
				$typex = "neighbor2";
				$filter = array(42838, 42840, 42841, 42842, 18310);
				$table = "location_location";
		} else {
				$typex = "neighbor";
				$filter = array($account->core_loc_id);
				$table = "location_neighbor";
		}
		$type = "neighbor2";
		$neighbor = NULL;
		$sphinx->SetGroupBy($typex, SPH_GROUPBY_ATTR, '@count desc' );
		$sphinx->SetFilter($type, $filter);
		$results = $sphinx->Query ( $query, $sphinx_index );
		$sphinx->ResetFilter($type);
		foreach($results["matches"] as $match) {
				$neighbor[$match["attrs"][$typex]] = array("id"=>$match["attrs"][$typex], "c"=>$match["attrs"]["@count"]);
				$nid[] = $match["attrs"][$typex];
		}
		if( $neighbor ) {
				$rex = $db->q("select * from $table where loc_id in (".implode(',', $nid).")");
				while($row=$db->r($rex)) {
						$url = !empty($row['loc_url']) ? "{$row['loc_url']}{$gModule}/" : "";
						$name = !empty($row['loc_name'])?$row['loc_name']:$row['name'];
						$nb[] = array("id"=>$row['loc_id'], "type"=>"neighborhood", "name"=>$name,
"c"=>$neighbor[$row['loc_id']]["c"], "link"=>$url, "title"=>"$name");
				}
		} 
		if( $nb )  $refine[] = array("name"=>"Neighborhood", "col"=>1, "alt"=>$nb);
}


$sphinx->SetLimits( 0, 10, 10 );

if( is_null($bar) ) {
	$sphinx->SetGroupBy( 'bar', SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, 'stripclub' );
	foreach($results["matches"] as $match) {
		$namex = $form->arrayValue($bar_array, $match["attrs"]["bar"]);
		if( empty($namex) ) $namex = "N/A";
			$bar[] = array("id"=>$match["attrs"]["bar"], "type"=>"bar", "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
	if( $bar ) $refine[] = array("name"=>"Alcohol", "col"=>2, "alt"=>$bar);
	$smarty->assign("bar", $bar);
}

if( is_null($food) ) {
	$sphinx->SetGroupBy( 'food', SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, 'stripclub' );
	foreach($results["matches"] as $match) {
		$namex = $form->arrayValue($food_array, $match["attrs"]["food"]);
		if( empty($namex) ) $namex = "N/A";
		$food[] = array("id"=>$match["attrs"]["food"], "type"=>"food", "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
	if( $food ) $refine[] = array("name"=>"Food", "col"=>2, "alt"=>$food);
	$smarty->assign("food", $food);
}

if( is_null($clubtype) ) {
	$sphinx->SetGroupBy( 'clubtype', SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query ( $query, 'stripclub' );
	foreach($results["matches"] as $match) {
		$namex = $form->arrayValue($clubtype_array, $match["attrs"]["clubtype"]);
		if( empty($namex) ) $namex = "N/A";
		$clubtype[] = array("id"=>$match["attrs"]["clubtype"], "type"=>"clubtype", "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
	if( $clubtype ) $refine[] = array("name"=>"Club Type", "col"=>2, "alt"=>$clubtype);
	$smarty->assign("clubtype", $clubtype);
}

if( is_null($age18) ) {
	$sphinx->SetFilter('age18', array(1));
	$sphinx->SetGroupBy('age18', SPH_GROUPBY_ATTR, '@group desc' );
	$results = $sphinx->Query ( $query, 'stripclub' );
	$sphinx->ResetFilter('age18');
	$age18_[] = array("id"=>"1", "name"=>"Age 18 and Above", "type"=>"age18", "c"=>$results["matches"][0]["attrs"]["@count"]);
	if( $results["matches"][0]["attrs"]["@count"] ) $refine[] = array("name"=>"Age Limit", "col"=>1, "alt"=>$age18_);
	if( $age18_ ) $available[] = array("type"=>"age18","name"=>"Age 18 and Above", "c"=>$results["matches"][0]["attrs"]["@count"]);
}

$smarty->assign("available", $available);

$dancer_title = $form->arrayValue($dancer_array, $dancer);

$gTitle = preg_replace("/%\w+%\s/", "", $gTitle);

if( $page > 1 ) $gTitle .= " - Page $page";

$smarty->assign("refine", $refine);

$phone1 = isset($_GET["phone1"]) && intval($_GET["phone1"]) ? intval($_GET["phone1"]) : "";
$phone2 = !empty($_GET["phone2"]) ? GetGetParam(phone2) : "";
$phone3 = !empty($_GET["phone3"]) ? GetGetParam(phone3) : "";
$phone_sql = NULL;
if( $phone1 || $phone2 || $phone3 ) {
		$phone1 = $phone1 ? (strlen($phone1) == 3 ? $phone1 : "$phone1%") : "%";
		$phone2 = $phone2 ? (strlen($phone2) == 3 ? $phone2 : "%$phone2%") : "%";
		$phone3 = $phone3 ? (strlen($phone3) == 3 ? $phone3 : "%$phone3") : "%";
		$phone_sql = "and (phone like '$phone1$phone2$phone3')";
	$error = "There is no strip clubs found with this phone number";
} else if( isset($_GET["phone"]) && !empty($_GET["phone"]) ) {
	$phone = preg_replace("/[^0-9]/", "", $_GET["phone"]);
	$phone_sql = "and phone like '$phone%'";
	$smarty->assign("phone", $_GET["phone"]);
} else {
		unset($_GET["phone1"]); unset($_GET["phone2"]); unset($_GET["phone3"]);
}

foreach($result as $res) {
	$ids .= $ids ? (",".$res["id"]) : $res["id"];
}

if ($ids)
	$res = $db->q("SELECT sc.*, l.loc_url, l.country_sub, r.comment rc, a2.avatar ra, a2.username as ru, if(length(coupon)>1,1,0) coupon, 
						(select count(*) from place_picture pp where pp.place_file_type_id = 2 and pp.module = 'stripclub' and pp.id = sc.id) as video 
					FROM strip_club2 sc 
					INNER JOIN location_location l on l.loc_id = sc.loc_id
					LEFT JOIN (place_review r inner join account a2 using (account_id)) on sc.last_review_id = r.review_id and r.module = 'stripclub'
					WHERE sc.id in ($ids) 
					ORDER BY field(sc.id,$ids)");

while($row=$db->r($res)) {

	if( $zipcode ) {
		if( !isset($lat_me) ) continue;
		if( $row["loc_lat"] && $row["loc_long"] ) {
			$mile = number_format(distance($lat_me, $lon_me, $row["loc_lat"], $row["loc_long"], "M"),1);
		} else $mile = "unknown";
	} elseif( $distance ) {
		$mile = number_format(distance($account->core_loc_array["loc_lat"], $account->core_loc_array["loc_long"], $row["loc_lat"], $row["loc_long"], "M"),1);
	}

	$link = dir::getPlaceLinkAux($row['country_sub'], $row["loc_id"], $gModule, $row["id"], $row['name'], trim($row['loc_url'], "/"));

	$locname = "{$row["loc_name"]}, {$row["state"]}";
	$extra = $row["edit"] < 5 ? "<span class='nonerotic'>*Not Live*</span>" : NULL;
	$thumbpath = !empty($row["thumb"]) ? "{$config_image_server}/stripclub/t/{$row["thumb"]}" : "/images/placeholder.gif";
	$ra = "{$config_image_server}/avatar/" .  (!empty($row["ra"]) ? $row["ra"] : "no_avatar.jpg");

	$sc[] = array(
		"id"=>$row["id"],
		"name"=>$row["name"],"thumb"=>$row["thumb"],
		"street"=>$row["address1"],
		"address"=>$row["address1"],
		"phone"=>makeproperphonenumber($row["phone"]),
		"loc_name"=>$locname,
		"zipcode"=>$row["zipcode"],
		"distance"=>isset($zipcode)||isset($distance)?$mile:NULL,
		"review"=>$row["review"],
		"recommend"=>$row["recommend"],
		"edit"=>$row["edit"],
		"sponsor"=>$row["sponsor"],
		"linkoverride" => $link,
		"longitude"=>$row['loc_long'],
		"latitude"=>$row['loc_lat'],
		"thumbpath"=>$thumbpath,
		"lr"=>$row['last_review_id'],
		"rc"=>$row['rc'],
		"ra"=>$ra,
		"ru"=>$row['ru'],
		"extra"=>$extra,
		"coupon"=>$row['coupon'],
		"video"=>$row['video']
	);

	if( !isset($country_id) && $row["country_id"] != 16046&&$row["country_id"] ) $country_id = $row["country_id"];
}

$json = json_encode($sc);
$smarty->assign("mapjson", $json);

$order = isset($_GET["order"]) ? $_GET["order"] : ($distance?"distance":"last_review_id");
if( !isset($_GET["orderway"]) && $order == "last_review_id" ) $orderway = 1;
elseif( !isset($_GET["orderway"]) && $distance ) $orderway = 2;
$sort = array();
if( isset($zipfound) || isset($distance)) $sort[] = array("key"=> "Distance", "value"=>"distance", "way"=>$order == "distance" && $orderway == 2 ? 1 : 2,
"selected"=>$order == "distance" ?true : false);
$sort[] = array("key"=> "Rating", "value"=>"score", "way"=>$order == "score" && $orderway == 1 ? 2 : 1, "selected"=>$order == "score" ? true : false);
$sort[] = array("key"=> "# of Reviews", "value"=>"review", "way"=>$order == "review" && $orderway == 1 ? 2 : 1, "selected"=> $order == "review"? true : false);
$sort[] = array("key"=> "Review Date", "value"=>"last_review_id", "way"=>$order == "last_review_id" && $orderway == 1 ? 2 : 1, "selected"=>$order == "last_review_id" ?true : false);
$sort[] = array("key"=> "Video", "value"=>"video", "way"=>$order == "video" && $orderway == 1 ? 2 : 1, "selected"=> $order == "video"? true : false);
$sort[] = array("key"=> "Coupon", "value"=>"coupon", "way"=>$order == "coupon" && $orderway == 1 ? 2 : 1, "selected"=> $order == "coupon"? true : false);
$smarty->assign("sort", $sort);


$smarty->assign("sc", $sc);
$smarty->assign("total", $total);

if( isset($error) ) $smarty->assign("error", $error);
if( isset($alert) ) $smarty->assign("alert", $alert);

if( $zipfound && strstr($gLocation, "zipcode") ) $query = _pagingQuery(array("loc_id", "zipcode"));
else $query = _pagingQuery(array("loc_id"));

$dir = new dir;
$ctx->page = $page;
$smarty->assign("pager_array", $dir->get_pager_array($total, $item_per_page, $account->core_loc_array['loc_url']."/{$gModule}/", true));
$smarty->assign("paging", $db->paging($total, $item_per_page, yes, yes, $query));
$smarty->assign("dancer", $dancer);
$smarty->assign("dancer_title", $dancer_title);

_pagingQueryFilter($query, $filter_link);

$linksort = _pagingQuery(array("order", "orderway"));
$smarty->assign("linksort", $linksort);

$smarty->assign("search_hidden", _pagingQuery(array("loc_id", "zipcode", "dancer", "miles", "phone"), 1));

$smarty->assign("link", $query);
$smarty->assign("filter", $filter_link);

if ($zipfound)
	$loc_name = "$zipcode";

if( isset($country_id) ) $smarty->assign("country_id", $country_id);

$smarty->assign("category", "strip-club");
$smarty->assign("place_type_id", 1);
$smarty->assign("what", $what);
$smarty->assign("worker_link", "sc");
$smarty->assign("image_path", "stripclub");
$smarty->assign("place_link", "club");
$smarty->assign("loc_name", $loc_name);
$smarty->assign("gModule", $gModule);
$smarty->assign("rss_available", true);

$advertise->popup();

$classifieds = new classifieds;
$classifieds->featuredads($account->core_loc_array['loc_id']);

$smarty->assign("imageserver", $config_image_server."/stripclub/");
$file = $smarty->fetch(_CMS_ABS_PATH."/templates/place/place_list.tpl");
echo $file;

?>
