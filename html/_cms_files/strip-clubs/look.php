<?php
defined("_CMS_FRONTEND") or die("No Access");

deprecated_controller();

require_once _CMS_ABS_PATH."/_cms_files/worker/array_sc.php";

global $db, $smarty, $account, $page, $gTitle, $advertise, $mobile, $gModule, $gLocation, $gItemID, $gDescription, $config_mapquest_key, $config_image_server;
$form = new form;

session_write_close();

include_html_head("css", "/css/info.css");

//MapQuest and Leaflet include
include_html_head('js', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.js');
include_html_head('css', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.css');
include_html_head('js', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.js');
include_html_head('css', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.css');

$id = $gItemID;
$account_id = $account->isloggedin();

$review = new review("strip_club2", $id, "stripclub", "", "stripclub");

$res = $db->q("SELECT c.*, l.loc_name, le.loc_name loc_exact_name, upper(l.s) s, l.loc_parent, l.country_sub, l.loc_url
				FROM strip_club2 c 
				LEFT JOIN location_location l on l.loc_id = c.loc_id
				LEFT JOIN location_location le on le.loc_id = c.location_exact_id
				WHERE id = '$id'");
if( !$db->numrows($res) ) {
	if( empty($account->core_loc_array['loc_url']) )
		$account->core_loc_array['loc_url'] = "/";
	system::moved("{$account->core_loc_array['loc_url']}{$gModule}/");
}
$row = $db->r($res);

$dancer_link = "";
switch($row['dancer']) {
	case 3:
		$dancer_link = "Male";
		$dancer_name = "Male ";
		break;
	case 5:
		$real_module = 'tstv-strip-clubs';
		break;
}

$name2url = name2url($row['name']);
$smarty->assign('breadplace', $row['name']);
if ($account->core_sub != $row["country_sub"] 
		|| !strstr($_SERVER['REQUEST_URI'], $row['loc_url']) 
		|| $gLocation != $name2url 
		|| (isset($real_module)&&$real_module != $gModule)) {

	if (isset($real_module))
		$gModule = $real_module;
	system::moved(location::getUrlStatic($row["country_sub"], $row["loc_url"])."{$gModule}/{$name2url}/{$row['id']}");
}

$advertise->showAd(9, $row["loc_id"]);

if (!empty($row["owner"]) && ($row["owner"]==$account_id||$account->isrealadmin()))
	$smarty->assign("isowner", true);

// map
$map = prepareMap($id, 'strip_club2', $row, 'stripclub', 'strip_club2');

$smarty->assign("map", $map);
$smarty->assign("link_map_update", "/map?id={$id}&section=stripclub&update=1");

$phone = makeproperphonenumber($row["phone"]);

$quickread = $review->reviews();
$smarty->assign("quickread", $quickread);

if( empty($quickread) && !empty($row['gdescription']) ) {
	$gDescription = $row['gdescription'];
	$smarty->assign("description", $gDescription);
} else {
	if (!empty($quickread))
		$gDescription = substr(htmlspecialchars($quickread[0]['review'], ENT_QUOTES), 0, 100) .". ";
	$female = $row['dancer'] == 1 ? "Female " : "";
	$gDescription .= "$phone {$row['name']} {$row['address1']} {$female}Strip Clubs";
}

$loc = "<a href=\"{$account->core_loc_array['loc_url']}$gModule/$dancer_link\" title=\"{$row["loc_name"]} {$dancer_name}strip clubs\" class='link'>".(($row["loc_exact_name"]) ? $row["loc_exact_name"] : $row["loc_name"])."</a>,{$row["s"]} {$row["zipcode"]}";
$smarty->assign("link_back", "{$account->core_loc_array["loc_url"]}$gModule/");

$cover2 = json_decode($row["cover"], true);
$cover = array();
for($i=1,$l=0;$i<8;$i++,$l++) {
	switch($cover2[$i]["day"]) {
		case 1: $day = "Monday"; break;
		case 2: $day = "Tuesday"; break;
		case 3: $day = "Wednesday"; break;
		case 4: $day = "Thursday"; break;
		case 5: $day = "Friday"; break;
		case 6: $day = "Saturday"; break;
		case 7: $day = "Sunday"; break;
	}
	$c = $form->arrayValue($cover_array, $cover2[$i]["cover"]);
	if($c) {
		$cover[$cover2[$i]["day"]] = array("day"=>$day,"d"=>$i,"cover"=>$c, "before"=>"","bcover"=>"");
	}
}
/* complicated cover */ 
if( empty($cover) ) {
	$multi_cover = "";
	for($i=1,$l=0; $i<8; $i++,$l++) {
		switch($cover2[$i]["day"]) {
			case 1: $day = "Monday"; break;
			case 2: $day = "Tuesday"; break;
			case 3: $day = "Wednesday"; break;
			case 4: $day = "Thursday"; break;
			case 5: $day = "Friday"; break;
			case 6: $day = "Saturday"; break;
			case 7: $day = "Sunday"; break;
		}

		$before = $form->arrayValue($oh_array, $cover2[$i]["before"]["time"]); 
		$between_1 = $form->arrayValue($oh_array, $cover2[$i]["between"]["1"]);
		$between_2 = $form->arrayValue($oh_array, $cover2[$i]["between"]["2"]);
		$after = $form->arrayValue($oh_array, $cover2[$i]["after"]["time"]);

		$before_cover = $form->arrayValue($cover_array, $cover2[$i]["before"]["cover"]);
		$between_1_cover = $form->arrayValue($cover_array, $cover2[$i]["between"]["cover"]);
		$after_cover = $form->arrayValue($cover_array, $cover2[$i]["after"]["cover"]);

		if( $before && $before_cover && $after ) {
			$multi_cover .= "<b>$day</b>:<br/>Before $before: $before_cover, ";
			$multi_cover .= "After $after: $after_cover<br>";
		}
	}
}
//_darr($cover);

$covex = "";
$i = 1;
$current = 0;
foreach($cover as $c) {
	if( empty($covex) ) $covex = $c["day"];
	else if( $c["cover"] == $cover[$i-1]["cover"] ) {
		// same...
		if( $i == count($cover) ) $covex .= "-{$c["day"]}: {$c["cover"]}";
	} else {
		if( $current != $cover[$i-1]["d"] ) {
			$covex .= "-".$cover[$i-1]["day"]; 
			$current = $c["d"];
		}
		$covex .= ": ".$cover[$i-1]["cover"] . "<br/>";
		$covex .= $c["day"];
		if( !isset($cover[$i+1]) ) $covex .= ": ".$c["cover"];
	}
	$i = $c["d"];
	$i++;
}
$smarty->assign("covex", $covex);

$covexb = "";
$i = 0;
$none = 0;
foreach($cover as $c) {
	if( empty($c["before"]) && !$none ) { $i++; continue; }
	$none = 1;
	if( empty($covexb) ) $covexb = $c["day"];
	else if( $c["bcover"] == $cover[$i-1]["bcover"] && $c["before"] == $cover[$i-1]["before"] ) {
		if( $i == count($cover)-1 ) $covexb .= "-{$c["day"]}: Before {$c["before"]} {$c["bcover"]}";
	} else {
		if( isset($cover[$i-2]) && $cover[$i-2]["d"] != $cover[$i-1]["d"] ) $covexb .= "-".$cover[$i-1]["day"]; 
		$covexb .= ": Before {$cover[$i-1]["before"]} ".$cover[$i-1]["bcover"] . "<br/>";
		if( !empty($c["before"]) ) $covexb .= $c["day"];
		if( !isset($cover[$i+1]) ) $covexb .= ": Before {$c["before"]} ".$c["bcover"];
	}
	$i++;
}
$smarty->assign("covexb", $covexb);

if( $row["owner"] )
	$c1[] = array("label" => "Updated By Business Owner", "val" => "Yes");

global $ctx;
$ctx->item = $row;
$smarty->assign("hours", dir::getHours(($row["byapponly"] == 1) ? true : false));
$smarty->assign("current_weekday", strtolower(date("l")));

if( $cover )
	$c1[] = array("label" => "COVER CHARGE", "val" => "{$covex}");
if( !empty($multi_cover) )
	$c1[] = array("label" => "COVER CHARGE", "val" => "<a href='' onclick=\"$('#mcover').toggle(); return false;\">(Show Cover Charges)</a><br /><span id='mcover' style='display:none;'>{$multi_cover}</span>");
if( $covexb )
	$c1[] = array("label" => "", "val" => "<a href=\"\" onclick=\"$('#covexb').slideDown(); return false;\">Click here to see discounted cover charges</a><div id=\"covexb\" style=\"display:none;\">{$covexb}</div>");

//photos
$imagedir = "stripclub";

$pics = array();
$rex = $db->q("select picture, filename, thumb from place_picture where id = '$id' and module = 'stripclub'");
while($rox=$db->r($rex)) {
	$pics[] = array(
		"module" => "stripclub",
		"id" => $rox["picture"],
		"image" => "{$config_image_server}/{$imagedir}/".$rox["filename"],
		"thumb" => "{$config_image_server}/{$imagedir}/t/".$rox["thumb"],
		);
}

$photos = array();
if ($row["image"] != "" && $row["thumb"] != "") {
	$photos["image"] = "{$config_image_server}/{$imagedir}/{$row["image"]}";
	$photos["thumb"] = "{$config_image_server}/{$imagedir}/t/{$row["thumb"]}";

	if ($pics) {
		include_html_head("js", "/js/jquery.tools.scrollable.js");
		$pics_in_group = 2;
		$pics_groups = array_chunk($pics, $pics_in_group);
		$photos["pics"]  = $pics_groups;
	}
}
$smarty->assign("photos", $photos);

/*
$forum_id = 2;
$smarty->assign("forum_topic", forum::getLastTopicsForPlace($forum_id, $id));
$smarty->assign("new_topic_link", forum::getNewTopicLinkForCategory($forum_id, $id));
*/

$dancer = $form->arrayValue($dancer_array, $row["dancer"]);
$bar = $form->arrayValue($bar_array, $row["bar_service"]);
$food = $form->arrayValue($food_array, $row["food"]);
$clubtype = $form->arrayValue($clubtype_array, $row["clubtype"]);
$vip = $row["vip"]?"Yes":"No";
$vipfee = $form->arrayValue($vipfee_array, $row["vipfee_enum"]);
$lapdance = $form->arrayValue($lapdance_array, $row["lapdance"]);
$privatedance = $form->arrayValue($lapdance_array, $row["privatedance_fee"]);
$privatedancevip = $row["privatedancevip"];

if( $clubtype )
	$c1[] = array("label" => "Club Type", "val" => "{$clubtype}");
if( $dancer )
	$c1[] = array("label" => "Dancers", "val" => "{$dancer}");
if( $bar )
	$c1[] = array("label" => "Alcohol", "val" => "{$bar}");
if( $food )
	$c1[] = array("label" => "Food", "val" => "{$food}");
if( $row["age_18"] )
	$c1[] = array("label" => "Age 18 and Above", "val" => "Yes");

if( $lapdance )
	$c1[] = array("label" => "Lap Dance", "val" => "{$lapdance}");
if( $vip )
	$c1[] = array("label" => "VIP Area", "val" => "{$vip}");
if( $vipfee ) {
	$c1[] = array("label" => "VIP Area Fee", "val" => "{$vipfee}");
	if( $privatedancevip )
		$c1[] = array("label" => "", "val" => "(Private Dance Included)");
}
if( $privatedance )
	$c1[] = array("label" => "Private Dance", "val" => "{$privatedance}");
if( !empty($row["coupon"]) )
	$c1[] = array("label" => "Coupon", "val" => "<a href='coupon?coupon={$row["coupon"]}' title='coupon' target='_blank' rel='nofollow'>Click to see</a>");
if( !empty($row['other_services']) )
	$smarty->assign("info", $row['other_services']);

$smarty->assign("c1", $c1);

$gTitle = "{$row["name"]} ". makeproperphonenumber($row['phone']) ." {$row['loc_name']} Strip Clubs";

$smarty->assign("category", "strip-club");
$smarty->assign("redirect", "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
$smarty->assign("name", $row["name"]);
$smarty->assign("loc_name", $row["loc_name"]);
$smarty->assign("dancer_link", $dancer_link);
$smarty->assign("gModule", $gModule);
$smarty->assign("what", "Strip Clubs");
$smarty->assign("id", $id);
$smarty->assign("totalreview", $row["review"]);
$smarty->assign("paging", $db->paging($row["review"], 10, false, true, "?id={$id}"));
$smarty->assign("overall", $row["overall"]);

$core_state_name = "<a href='{$account->core_loc_array['loc_url']}$gModule/$dancer_link' title='{$row["loc_name"]} Strip Clubs'>{$row["loc_name"]} {$dancer_name}Strip Clubs</a>";
$smarty->assign("core_state_name", $core_state_name);
$smarty->assign("editlink", "sc");

if( strlen($row["website"])>1 ) {
	$row["website"] = trim($row["website"]);
	if( strncmp($row["website"], "http", 4) )  $row["website"] = "http://" . $row["website"];
	$web = parse_url($row["website"]);
	$web_url = $row["website"];
	$web_host = $web["host"];
	$smarty->assign("web_url", $row["website"]);
	$smarty->assign("web_host", $web["host"]);
}

$addressLx = "<b>{$phone}</b>";
if (!empty($row['fax']))
	$addressLx .= ", Fax:" . makeproperphonenumber($row['fax']);
$addressLx .= "<br />{$row["address1"]}<br />{$loc}<br/>";
if ($row["email"])
	$addressLx .= "<a href=\"mailto:{$row["email"]}\" rel=\"nofollow\" >{$row["email"]}</a><br/>";
if ($web_url)
	$addressLx .= "<a href=\"$web_url\" target=\"_blank\" rel=\"nofollow\" class=\"website\">$web_host</a><br/>";
$smarty->assign("address", $addressLx);

if( empty($row['owner']) ) {
	$smarty->assign("business_link", true);
	$smarty->assign("business_shortcut", "?section=sc&id=$gItemID");
}

if (permission::has("edit_all_places"))
	$smarty->assign("can_edit_place", true);

$smarty->display(_CMS_ABS_PATH."/templates/place/place_place.tpl");

?>
