<?php

deprecated_controller();

global $db, $smarty, $account, $page;

if( !($account_id = $account->isloggedin()) ) {
	$account->asklogin();
	return;
}

require_once(_CMS_ABS_PATH."/_cms_files/worker/array_sc.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
$form = new data;

$form->table = "strip_club2";

$c = new CColumnId("id", "BUSINESS ID");
$form->AppendColumn($c);

$c = new CColumnString("name", "Club Name");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("address1", "Club Address");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Club Phone");
$c->setColMandatory(true);
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnPhone("fax", "Club FAX");
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnHour("hours", "Hours Of Operation");
$form->AppendColumn($c);

if( $account->isrealadmin() ) {
$c = new CColumnCover("Cover Charges");
$c->setCoverArray($cover_array);
$form->AppendColumn($c);
}

$c = new CColumnEnum("bar_service", "Bar Service");
foreach($bar_array as $key=>$value) $c->AddEnumOption($key, $value);
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnEnum("food", "Food Service");
foreach($food_array as $key=>$value) $c->AddEnumOption($key, $value);
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnEnum("clubtype", "Club Type");
foreach($clubtype_array as $key=>$value) $c->AddEnumOption($key, $value);
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnEnum("vip", "has VIP Area ?");
$c->AddEnumOption("1", "Yes");
$c->AddEnumOption("0", "No");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnEnum("vipfee_enum", "VIP Area Fee ?");
foreach($vipfee_array as $key=>$value) $c->AddEnumOption($key, $value);
$form->AppendColumn($c);

$c = new CColumnEnum("lapdance", "Lap Dance Fee");
foreach($lapdance_array as $key=>$value) $c->AddEnumOption($key, $value);
$form->AppendColumn($c);

$c = new CColumnEnum("privatedance_fee", "Private dance fee");
foreach($lapdance_array as $key=>$value) $c->AddEnumOption($key, $value);
$form->AppendColumn($c);

$c = new CColumnEnum("privatedancevip", "Is Private Dance Fee included with VIP Area Fee ?");
$c->AddEnumOption("1", "Yes");
$c->AddEnumOption("0", "No");
$form->AppendColumn($c);

$c = new CColumnBoolean("age_18", "Age 18 and above may go");
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setPath("stripclub");
$c->setWatermark();
$form->AppendColumn($c);

$c = new CColumnImage("thumb", "Thumbnail");
$c->setDimensions(220, 165);
$c->setWatermark();
$c->setPath("stripclub/t");
$form->AppendColumn($c);

$c = new CColumnImage("coupon", "Coupon");
$c->setMaxDimensions(700, 500);
$c->setPath("stripclub/c");
$form->AppendColumn($c);

$c = new CColumnPicture("strip_club_picture", "Extra Pictures", "stripclub");
$c->setMaxDimensions(700, 500);
$c->setDimensionsT(220, 165);
$c->setPath("stripclub");
$c->setPathT("stripclub/t");
$form->AppendColumn($c);

$form->owner_mode = true;
$form->owner_link = "club";
$form->ShowPage();

?>

