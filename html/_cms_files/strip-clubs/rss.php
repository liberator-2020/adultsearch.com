<?php

deprecated_controller();

global $db, $smarty, $sphinx, $ctx;

$sphinx_index = "stripclub";
$imagedir = "stripclub";
$module = "strip-clubs";
$places = "Strip Clubs";

$location = location::findOneById($ctx->location_id);
if (!$location)
	die("Error: can't find location !");

if ($ctx->state_id)
	$location_label = "{$ctx->location_name}, {$ctx->state_name}";
else
	$location_label = "{$ctx->location_name}, {$ctx->country_name}";

$cache_filename = "pl_rss_{$sphinx_index}_{$ctx->location_id}.tpl";
$cache_filepath = _CMS_ABS_PATH."/templates/place/rss/{$cache_filename}";

//check if we have recent cached HTML file
if (file_exists($cache_filepath)) {
	$time = filemtime($cache_filepath);
	if ($time > time() - 86400) {
		$handle = fopen($cache_filepath, "r");
		$content = fread($handle, filesize($cache_filepath));
		fclose($handle);
		header('Content-Type: application/rss+xml');
		die($content);
	}
}

$sphinx->reset();
$sphinx->SetLimits( 0, 100, 1048 );
$sphinx->SetFilter('loc_id', [$ctx->location_id]);
$sphinx->SetFilter('state_id', ($ctx->state_id) ? [$ctx->state_id] : [$ctx->country_id]);
$sphinx->SetSortMode(SPH_SORT_ATTR_DESC, "created_stamp");
$results = $sphinx->query("", $sphinx_index);
$result = $results["matches"];

$items = [];
foreach($result as $res) {
	//echo "<pre>".print_r($res, true)."</pre>\n";die;
	$id = $res["attrs"]["id"];
	$link = $location->getUrl()."/{$module}/".name2url($res["attrs"]["name"])."/{$id}";
	if ($res["attrs"]["thumb"])
		$thumb = "{$config_image_server}/{$imagedir}/t/{$res["attrs"]["thumb"]}";
	else
		$thumb = "";
	$created_stamp = ($res["attrs"]["created_stamp"]) ? $res["attrs"]["created_stamp"] : 1325394000;
	$item = [
		"id" => $id,
		"title" => $res["attrs"]["name"],
		"content" => "{$res["attrs"]["name"]} in {$location_label}",
		"link" => $link,
		"date" => date("l, F d, Y", $created_stamp),
		"thumbnail" => $thumb,
		];
	$items[] = $item;
}

$smarty->assign("location_label", $location_label);
$smarty->assign("places", $places);
$smarty->assign("description", "{$places} in {$location_label}");
$smarty->assign("link", $location->getUrl()."/{$module}/rss");
$smarty->assign("items", $items);
$smarty->assign("built", date("r"));
$fetch = $smarty->fetch(_CMS_ABS_PATH."/templates/place/rss.tpl");

//caching
$handle = fopen($cache_filepath, "w");
fputs($handle, $fetch);
fclose($handle);

header('Content-Type: application/rss+xml');
die($fetch);

?>
