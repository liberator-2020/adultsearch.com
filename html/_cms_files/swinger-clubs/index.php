<?php

defined("_CMS_FRONTEND") or die("No Access");

global $ctx, $db, $smarty, $page, $account, $gTitle, $advertise, $gModule, $gLocation, $mobile, $sphinx, $config_image_server;
$form = new form;
$system = new system;

$location = location::findOneById($ctx->location_id);
if ($location) {
	$smarty->assign("link_back", $location->getUrl());
	$smarty->assign("location_video_html", $location->getVideoHtml());
}

$core_state_id = $account->corestate(1);
if( !$core_state_id ) return;

$loc_around = array();
if ($account->core_loc_id && $account->core_loc_type > 2)
    $loc_around = $account->locAround();

if( stristr($gModule, 'swinger') ) {
	$type = 1; 
} elseif( stristr($gModule, 'bdsm') ) {
	$type = 2;
} elseif( stristr($gModule, 'topless-pools') ) {
	$type = 3;
} elseif( stristr($gModule, 'nudist-colonies') ) {
	$type = 4;
} else $type = NULL;

$t = isset($_GET['t']) ? $_GET['t'] : NULL;
if( $type && !$t ) $t = $type;

switch ($type) {
	case 1:
		$cat_name = "Swinger Clubs";
		$gtitle = "Swingers Clubs and Lifestyle Clubs";
		$smarty->assign("category", "swingers-club");
		break;
	case 2:
		$cat_name = "BDSM Clubs";
		$gtitle = "BDSM Clubs and Fetish Dungeons";
		$smarty->assign("category", "bdsm");
		break;
	case 3:
		$cat_name = "Topless Pools";
		$gtitle = "Topless Pools";
		$smarty->assign("category", "topless-pool");
		break;
	case 4:
		$cat_name = "Nudist Colonies";
		$gtitle = "Nudist Colonies";
		$smarty->assign("category", "nudist-colony");
		break;
	default:
		$cat_name = "Lifestyle Clubs";
		$gtitle = "Lifestyle Clubs";
		$smarty->assign("category", "lifestyle-club");
		break;
}

$gTitle = $account->core_loc_array['loc_name'] . " $gtitle {$account->core_loc_array['lp_loc_name']}";

$item_per_page = isset($_GET["ipp"]) ? intval($_GET["ipp"]) : 30;
if( $item_per_page > 100 ) $item_per_page = 30;
$start = (($page*$item_per_page)-$item_per_page);
if( $start < 0 ) $start = 0;

$advertise->showAd(77, $core_state_id);

$filter_link = array();
foreach($_GET as $key => $value) {
	if( in_array($key, array("t", "loc_id", "name", "zipcode", "phone")) && $value != "" ) {
		$ftype = $key;
		$skipfilter = 0;
		if( $key == "t" ) {
			switch ($value) {
				case 1: $key = "Swinger Clubs"; break;
				case 2: $key = "BDSM Clubs"; break;
				case 3: $key = "Topless Pools"; break;
				case 4: $key = "Nudist Colonies"; break;
			}
		}
		else if( $key == "zipcode" ) { $key = "Zip Code: $value"; $skipfilter = 1;
			$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key, "link"=>"{$account->core_loc_array['loc_url']}$gModule/"); }
		else if( $key == "name" ) { $key = "Club Name: '$value'"; }
		else if( $key == "phone" ) { $key = "Phone #: '$value'"; }
		else if( $key == "loc_id" ) $key = $loc_name;

		if( !$skipfilter )
			$filter_link[] = array("id"=>$value,"type"=>$ftype,"name"=>$key);
	}
}

$sphinx_index = "lifestyle";
$sphinx->reset();
$sphinx->SetLimits($start, $item_per_page, 1000);

if( isset($_GET["zipcode"]) && !empty($_GET["zipcode"]) ) {
	$zipcode = preg_replace("/[^0-9a-z]/i","", $_GET["zipcode"]);
	$mile_in = intval($_GET["miles"]); if( !$mile_in ) $mile_in = 10;
	if( $mile_in == -1 ) $mile_in = 1000;
	$res = $db->q("select z.lat, z.lon, radians(z.lat) latt, radians(z.lon) longg, l.loc_parent from core_zip z inner join location_location l on z.loc_id = l.loc_id
where z.zip = '$zipcode' limit 1");
	if( $db->numrows($res) ) {
		$s = $db->r($res); 
		$lat_me = $s["lat"]; $lon_me = $s["lon"];
		$core_state_id = $s["loc_parent"];
		$latt = $s["latt"]; $long = $s["longg"];
		$sphinx->SetGeoAnchor('loc_lat', 'loc_long', $latt, $long);
		$sphinx->SetFilterFloatRange('@geodist', 0, ($mile_in*1609.344));
		$zipfound = 1;
	} else {
		$error = "Zip code($zipcode) could not find. Please make sure that you typed the correct zip code.";
	}
	$smarty->assign("miles", $mile_in);
	$smarty->assign("zipcode", $_GET["zipcode"]);
}  elseif( $account->core_loc_array['loc_lat'] ) {
	$sphinx->SetGeoAnchor('loc_lat', 'loc_long', deg2rad($account->core_loc_array['loc_lat']), deg2rad($account->core_loc_array['loc_long']));
	$sphinx->SetFilterFloatRange('@geodist', 0, (500*1609.344));
	//$distance = 1;
}

$sphinx->SetFilter('state_id', array($core_state_id));

if ($account->core_loc_id && $account->core_loc_type > 2)
	$sphinx->SetFilter('loc_id', $loc_around);

if ($t)
	$sphinx->SetFilter('type', array($t));

$query = "";
if( isset($_GET["phone"]) && !empty($_GET["phone"]) ) {
	$phone = preg_replace("/[^0-9\*]/", "", $_GET["phone"]);
	$query = $phone;
}
if( isset($_GET["query"]) && !empty($_GET["query"]) ) {
	$name = preg_replace("/(\()?([0-9]{3})(\))?(.+)?([0-9]{3})(.)?([0-9]{4})/s", "$2$5$7", $_GET["query"]);
	$query .= $query ? " $name" : "$name";
	$error = "No strip club found with the word: ".$_GET["query"];
	$smarty->assign("name", $_GET["query"]);
}

$orderway = isset($_GET["orderway"])&& intval($_GET["orderway"]) ? intval($_GET["orderway"]) : (($zipfound||$distance)?2:1);
$way = $orderway == 1 ? "DESC" : "ASC";

if($_GET["order"] == 'overall') { $order = "overall $way"; }
elseif($_GET["order"] == 'review') $order = "review $way";
elseif($_GET["order"] == "name") $order = "name $way";
elseif($_GET["order"] == "last_review_id") $order = "last_review_id $way";
elseif($_GET["order"] == "score") $order = "overall $way";
elseif($_GET["order"] == 'distance' || ($zipfound||$distance) ) { $order = "sponsor DESC, @geodist $way"; }
else $order = "sponsor DESC, last_review_id DESC";
$sphinx->SetSortMode(SPH_SORT_EXTENDED, $order);

$results = $sphinx->Query ( $query, 'lifestyle' );
$total = $results["total_found"];
$result = $results["matches"];

if( $total && !isset($zipfound) ) {
	$sphinx->ResetFilter('loc_id');
	$sphinx->SetGroupBy('loc_id', SPH_GROUPBY_ATTR, '@group desc' );
	$namex = "loc_id";

	$results = $sphinx->Query($query, $sphinx_index);
	$rex = $results["matches"];
	$locs = array();
	foreach($rex as $res) {
		$locs[] = array(
			"loc_id"=>$res["attrs"][$namex],
			"count"=>$res["attrs"]["@count"]
		);
	}

	if ($loc_id)
		$sphinx->SetFilter('loc_id', array($loc_id));

	if( count($locs) ) {
		$loc_id = "";
		foreach($locs as $loc)
			$loc_id  .= $loc_id ? (",".$loc["loc_id"]) : $loc["loc_id"];

		$res = $db->q("select loc_id, loc_name, loc_url from location_location where loc_id in ($loc_id) order by loc_name");
		$loc_id = array();
		while($row=$db->r($res)) {
			$count = 0;
			foreach($locs as $l) {
				if( $l["loc_id"] == $row["loc_id"] ) $count = $l["count"];
			}
			$loc_id[] = array($row["loc_id"]=>$row["loc_name"]." ($count)" );
			$category[] = array("id"=>"City", "type"=>"Location", "name"=>$row["loc_name"], "c"=>$count,"link"=>"{$row['loc_url']}$gModule/", "title"=>"{$row["loc_name"]} $cat_name");
		}
		$locs = $form->select_add_select();
		$locs .= $form->select_add_array($loc_id, isset($_GET["loc_id"])?$_GET["loc_id"]:"");
	}
}

$sphinx->SetLimits( 0, 10, 10 );

$refine = NULL;
if( $category ) $refine[] = array("name"=>"Location", "col"=>1, "alt"=>$category);

if( is_null($t) ) {
	$type = "type";
	$sphinx->SetGroupBy( $type, SPH_GROUPBY_ATTR, '@count desc' );
	$results = $sphinx->Query($query, 'lifestyle');
	$category = NULL;
	foreach($results["matches"] as $match) {
		//$namex = $match["attrs"][$type] == 1 ? "Swinger Clubs" : ($match["attrs"][$type]==2?"BDSM":"N/A");
		$t = $match["attrs"][$type];
		//$namex = $t == 1 ? "Swinger Clubs" : ($t == 2 ? "BDSM" : ($t == 3 ? "Topless Pools" : "N/A"));
		switch ($t) {
			case 1: $namex = "Swinger Clubs"; break;
			case 2: $namex = "BDSM"; break;
			case 3: $namex = "Topless Pools"; break;
			case 4: $namex = "Nudist Colonies"; break;
			default: $namex = "N/A"; break;
		}
		//$category[] = array("id"=>$match["attrs"][$type], "type"=>'t', "name"=>$namex, "c"=>$match["attrs"]["@count"]);
		$category[] = array("id"=>$t, "type"=>'t', "name"=>$namex, "c"=>$match["attrs"]["@count"]);
	}
	$refine[] = array("name"=>"Club Types", "col"=>1, "alt"=>$category);
}

$smarty->assign("refine", $refine);

foreach($result as $res) {
	$ids .= $ids ? (",".$res["id"]) : $res["id"];
}

if( $ids )
	$res = $db->q("SELECT ls.*, l.loc_url, l.country_sub, r.comment rc, a2.avatar ra, a2.username as ru
					FROM lifestyle ls
					INNER JOIN location_location l on l.loc_id = ls.loc_id
					LEFT JOIN (place_review r inner join account a2 using (account_id)) on ls.last_review_id = r.review_id and r.module = 'lifestyle'
					WHERE ls.id in ($ids)
					ORDER BY field(ls.id,$ids)");
while($row=$db->r($res)) {
	if( $zipcode ) {
		if( $row["loc_lat"] && $row["loc_long"] ) {
			$mile = number_format(distance($lat_me, $lon_me, $row["loc_lat"], $row["loc_long"], "M"),1);
		} else
			$mile = "unknown";
	} elseif( $distance ) {
		$mile = number_format(distance($account->core_loc_array["loc_lat"], $account->core_loc_array["loc_long"], $row["loc_lat"], $row["loc_long"], "M"),1);
	}

	$link = dir::getPlaceLinkAux($row['country_sub'], $row["loc_id"], $gModule, $row["id"], $row['name'], trim($row['loc_url'], "/"));

	$lox_name = "{$row["loc_name"]}, {$row["state"]}";

	$thumbpath = !empty($row["thumb"]) ? "{$config_image_server}/lifestyle/t/{$row["thumb"]}" : "/images/placeholder.gif";
	$ra = "{$config_image_server}/avatar/" .  (!empty($row["ra"]) ? $row["ra"] : "no_avatar.jpg");

	$sc[] = array(
		"id"=>$row["id"],
		"name"=>$row["name"],"thumb"=>$row["thumb"],"street"=>$row["street"],
		"phone"=>makeproperphonenumber($row["phone"]),
		"address"=>$row["address1"],
		"loc_name"=>$lox_name,
		"zipcode"=>$row["zipcode"],
		"distance"=>isset($zipcode)||$distance?$mile:NULL,
		"review"=>$row["review"],
		"edit"=>$row["edit"],
		"sponsor"=>$row["sponsor"],
		"review"=>$row["review"],
		"recommend"=>$row["recommend"],
		"linkoverride" => $link,
		"longitude"=>$row['loc_long'],
		"latitude"=>$row['loc_lat'],
		"thumbpath"=>$thumbpath,
		"lr"=>$row['last_review_id'],
		"rc"=>$row['rc'],
		"ra"=>$ra,
		"ru"=>$row['ru']
	);
}

$json = json_encode($sc);
$smarty->assign("mapjson", $json);

$smarty->assign("sc", $sc);
$smarty->assign("total", $total);

if( isset($error) ) $smarty->assign("error", $error);

if ($zipfound)
	$loc_name = "$zipcode";

$order = isset($_GET["order"]) ? $_GET["order"] : ($distance?"distance":"last_review_id");
if( !isset($_GET["orderway"]) && $order == "last_review_id" ) $orderway = 1;
elseif( !isset($_GET["orderway"]) && $distance ) $orderway = 2;
$sort = array();
if( isset($zipfound) || isset($distance)) $sort[] = array("key"=> "Distance", "value"=>"distance", "way"=>$order == "distance" && $orderway == 2 ? 1 : 2,
"selected"=>$order == "distance" ?true : false);
$sort[] = array("key"=> "Rating", "value"=>"overall", "way"=>$order == "overall" && $orderway == 1 ? 2 : 1, "selected"=>$order == "overall" ? true : false);
$sort[] = array("key"=> "# of Reviews", "value"=>"review", "way"=>$order == "review" && $orderway == 1 ? 2 : 1, "selected"=> $order == "review"? true : false);
$sort[] = array("key"=> "Club Recently Reviewed", "value"=>"last_review_id", "way"=>$order == "last_review_id" && $orderway == 1 ? 2 : 1, "selected"=>$order =="last_review_id" ?true : false);
$smarty->assign("sort", $sort);


$query = _pagingQuery();
_pagingQueryFilter($query, $filter_link);
$linksort = _pagingQuery(array("order", "orderway"));
$smarty->assign("linksort", $linksort);
$smarty->assign("search_hidden", _pagingQuery(array("loc_id", "zipcode", "miles", "phone"), 1));
$smarty->assign("link", $query);
$smarty->assign("filter", $filter_link);

$dir = new dir;
$ctx->page = $page;
$smarty->assign("pager_array", $dir->get_pager_array($total, $item_per_page, $account->core_loc_array['loc_url']."/{$gModule}/", true));
$smarty->assign("paging", $db->paging($total, $item_per_page, yes, yes, $query));

$party = str_replace("Clubs", "Parties", $cat_name);
$core_state_name = "<a href=\"{$account->core_loc_array['loc_url']}$gModule/\" title=\"{$loc_name} $cat_name, $party\">$loc_name $cat_name</a>";
$smarty->assign("core_state_name", $core_state_name);

$smarty->assign("what", $cat_name);
$smarty->assign("gModule", $gModule);
$smarty->assign("loc_name", $account->core_loc_array['loc_name']);
$smarty->assign("worker_link", "lifestyle");
$smarty->assign("image_path", "lifestyle");
$smarty->assign("place_link", "club");
$smarty->assign("imageserver", $config_image_server."/lifestyle/");
$smarty->assign("rss_available", true);

$classifieds = new classifieds;
$classifieds->featuredads($account->core_loc_array['loc_id']);

$file = $smarty->fetch(_CMS_ABS_PATH."/templates/place/place_list.tpl");
echo $file;
?>
