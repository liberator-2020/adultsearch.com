<?php
defined("_CMS_FRONTEND") or die("No Access");

global $db, $smarty, $account, $page, $gTitle, $advertise, $gModule, $gLocation, $gItemID, $gDescription, $config_mapquest_key, $config_image_server;

$id = $gItemID;
$account_id = $account->isloggedin();

if( stristr($gModule, 'swinger') ) {
	$type = 1;
	$smarty->assign("category", "swingers-club");
} elseif( stristr($gModule, 'bdsm') ) {
	$type = 2;
	$smarty->assign("category", "bdsm");
} elseif( stristr($gModule, 'topless-pools') ) {
	$type = 3;
	$smarty->assign("category", "topless-pool");
} else {
	$type = 4;
	$smarty->assign("category", "nudist-colony");
}

$review = new review("lifestyle", $id, "lifestyle", "", 'lifestyle');

$res = $db->q("SELECT l.*, lo.country_sub, lo.loc_url 
				FROM lifestyle l 
				INNER JOIN location_location lo on lo.loc_id = l.loc_id
				WHERE id = '$id'");
if( !$db->numrows($res) ) {
	if (empty($account->core_loc_array['loc_url']))
		$account->core_loc_array['loc_url'] = "/";
	system::moved("{$account->core_loc_array['loc_url']}{$gModule}/");
}
$row = $db->r($res);

$name2url = name2url($row['name']);

if ($account->core_sub != $row["country_sub"] || !strstr($_SERVER['REQUEST_URI'], $row['loc_url']) || $gLocation != $name2url)
	system::moved(location::getUrlStatic($row["country_sub"], $row["loc_url"])."{$gModule}/{$name2url}/{$row['id']}");

$smarty->assign('breadplace', $row['name']);
$advertise->showAd(77, $row["loc_id"]);

$gTitle = "{$row["name"]} - {$row["loc_name"]}, {$row['state']} {$row["zipcode"]}";
if (!empty($row['phone']))
	$gTitle .= " | ".makeproperphonenumber($row['phone']);
if ($row["review"])
	$gTitle .= " | Total Review: {$row["review"]}";

if( $row["type"] == 1 ) { 
	$type_c = "Swinger Club";
	$cat_name = "Swinger Clubs";
} else if( $row["type"] == 2 ) { 
	$type_c = "BDSM";
	$cat_name = "BDSM Clubs";
} else if( $row["type"] == 3 ) { 
	$type_c = "Topless Pool";
	$cat_name = "Topless Pools";
} else {
	$type_c = "Nudist Colony";
	$cat_name = "Nudist Colonies";
}

$loc = "<a href='{$account->core_loc_array['loc_url']}$gModule/' title='{$row["loc_name"]} $cat_name'>{$row["loc_name"]}</a>, {$row["state"]} {$row["zipcode"]}";
$smarty->assign("link_back", "{$account->core_loc_array["loc_url"]}$gModule/");

//map
$map = prepareMap($id, 'lifestyle', $row, 'lifestyle', 'lifestyle');

$smarty->assign("map", $map);
$smarty->assign("link_map_update", "/map?id={$id}&section=lifestyle&update=1");

$quickread = $review->reviews();
$smarty->assign("quickread", $quickread);
$phone = makeproperphonenumber($row["phone"]);

if (empty($quickread) && !empty($row['gdescription'])) {
	$gDescription = $row['gdescription'];
	$smarty->assign("description", $gDescription);
} else {
	if (!empty($quickread))
		$gDescription = substr(htmlspecialchars($quickread[0]['review'], ENT_QUOTES), 0, 100) .". ";
	$gDescription .= "$phone {$row['name']} {$row['address1']} $cat_name";
}

global $ctx;
$ctx->item = $row;
$smarty->assign("hours", dir::getHours(($row["byapponly"] == 1) ? true : false));
$smarty->assign("current_weekday", strtolower(date("l")));

if( $row['country_id'] == 41973 ) {
		$money_sign = "GBP ";
		$rex = $db->q("select `from` from currency where code = 'GBP'");
		if( $db->numrows($rex) ) {
				$rox = $db->r($rex);
				$exchange = $rox[0];
		}
}
else $money_sign = "$";

if( $row["cover_men"] ) {
	if( $row["cover_men"] == -1 )
		$c1[] = array("label" => "Cover Charge - Men", "val" => "Free");
	else {
		$c1[] = array("label" => "Cover Charge - Men", "val" => "$money_sign{$row["cover_men"]}");
		if( $exchange )
			$c1[] = array("label" => "", "val" => " (\$".number_format($row["cover_men"]*$exchange, 2).")");
	}
}
if( $row["cover_women"] ) { 
	if( $row["cover_women"] == -1 )
		$c1[] = array("label" => "Cover Charge - Women", "val" => "Free");
	else {
		$c1[] = array("label" => "Cover Charge - Women", "val" => "$money_sign{$row["cover_women"]}");
		if( $exchange )
			$c1[] = array("label" => "", "val" => "(\$".number_format($row["cover_women"]*$exchange, 2).")");
	}
}
if( $row["cover_couple"] ) {
	if( $row["cover_couple"] == -1 )
		$c1[] = array("label" => "Cover Charge - Couple", "val" => "Free");
	else {
		$currency = $exchange ? " (\$".number_format($row["cover_couple"]*$exchange, 2).")" : "";
		$c1[] = array("label" => "Cover Charge - Couple", "val" => "$money_sign{$row["cover_couple"]}$currency");
	}
}

if( $row["entry"] == 1 )
	$c1[] = array("label" => "Couples ONLY!", "val" => "");
elseif( $row["entry"] == 2 )
	$c1[] = array("label" => "Couples &amp; Women ONLY!", "val" => "");

if( !empty($row["other_services"]) )
	$smarty->assign("info", nl2br($row["other_services"]));
if( !empty($row["coupon"]) )
	$c1[] = array("label" => "Coupon", "val" => "<a href='coupon?coupon={$row["coupon"]}' title='coupon' target='_blank' rel='nofollow'>Click to see</a>");

$smarty->assign("c1", $c1);

$imagedir = "lifestyle";
$photos = array();
if ($row["image"] != "" && $row["thumb"] != "") {
	$photos["image"] = "{$config_image_server}/{$imagedir}/{$row["image"]}";
	$photos["thumb"] = "{$config_image_server}/{$imagedir}/t/{$row["thumb"]}";

	$pics = array();
	$rex = $db->q("select picture, filename, thumb from place_picture where id = '$id' and module = 'lifestyle'");
	while($rox=$db->r($rex)) {
		$pics[] = array(
			"module" => "lifestyle",
			"id" => $rox["picture"],
			"image" => "{$config_image_server}/{$imagedir}/".$rox["filename"],
			"thumb" => "{$config_image_server}/{$imagedir}/t/".$rox["thumb"],
			);
	}

	if ($pics) {
		include_html_head("js", "/js/jquery.tools.scrollable.js");
		$pics_in_group = 2;
		$pics_groups = array_chunk($pics, $pics_in_group);
		$photos["pics"]  = $pics_groups;
	}
}
$smarty->assign("photos", $photos);

$smarty->assign("name", $row["name"]);
$smarty->assign("loc_name", $row["loc_name"]);
$smarty->assign("id", $id);
$smarty->assign("editlink", "lifestyle");
$smarty->assign("totalreview", $row["review"]);
$smarty->assign("overall", $row["overall"]);

if( strlen($row["website"])>1 ) {
	$url = trim($row["website"]);
	if( strncmp($url, "http://", 7) )  $url = "http://" . $url;
	$web = parse_url($url);
	$web_url = $url;
	$web_host = $web["host"];
}

$phone = makeproperphonenumber($row["phone"]);

$addressLx = "<b>$type_c</b><br/><b>{$phone}</b><br />{$row["address1"]}<br />{$loc}<br/>";
if ($web_url)
	$addressLx .= "<a href=\"$web_url\" target=\"_blank\" rel=\"nofollow\" class=\"website\">$web_host</a>";
if ($row["email"])
	$addressLx .= "<a href=\"mailto:{$row["email"]}\" rel=\"nofollow\" class=\"website\">{$row["email"]}</a><br/>";
$smarty->assign("address", $addressLx);

$core_state_name = "<a href='{$account->core_loc_array['loc_url']}$gModule/' title='{$row["loc_name"]} $cat_name'>{$row["loc_name"]} $cat_name</a>";
$smarty->assign("core_state_name", $core_state_name);

if( $row["private_address"] )
	$smarty->assign("nomap", true);


//MapQuest and Leaflet include
include_html_head('js', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.js');
include_html_head('css', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.css');
include_html_head('js', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.js');
include_html_head('css', 'https://api.mqcdn.com/sdk/mapquest-js/v1.3.2/mapquest-core.css');

include_html_head("css", "/css/info.css");

/*
//forum
$forum_id = 141;
$smarty->assign("forum_topic", forum::getLastTopicsForPlace($forum_id, $id));
$smarty->assign("new_topic_link", forum::getNewTopicLinkForCategory($forum_id, $id));
*/

$smarty->display(_CMS_ABS_PATH."/templates/place/place_place.tpl");

?>
