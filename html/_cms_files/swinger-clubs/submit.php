<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $page, $gModule;
$system = new system;

require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
$form = new data;

$id = NULL;

$form->table = "lifestyle";

$c = new CColumnId("id", "ID");
$form->AppendColumn($c);

$c = new CColumnString("name", "Name");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnLocation("City", "Location", 2);
$c->setLocName("loc_name", "loc_name", "City");
$c->setLocName("state", "s", "City");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("address1", "Address");
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "Zipcode");
$form->AppendColumn($c);

$c = new CColumnEnum("type", "Club Type");
$c->setColMandatory(true);
$c->AddEnumOption("1", "Swinger");
$c->AddEnumOption("2", "BDSM");
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Club Phone");
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnString("email", "E-Mail");
$form->AppendColumn($c);

$c = new CColumnHour("hours", "Hours of Operation:");
$form->AppendColumn($c);

$c = new CColumnFee("cover_men", "Cover Men");
$form->AppendColumn($c);

$c = new CColumnFee("cover_women", "Cover Women");
$form->AppendColumn($c);

$c = new CColumnFee("cover_couple", "Cover For Couples");
$form->AppendColumn($c);

$c = new CColumnEnum("entry", "Restrictions");
$c->AddEnumOption("0", "None");
$c->AddEnumOption("1", "Couples Only");
$c->AddEnumOption("2", "Couples & Women Only");
$form->AppendColumn($c);

$c = new CColumnBoolean("private_address", "Private Address");
$form->AppendColumn($c);

$c = new CColumnText("other_services", "Additional Information");
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setBackup(true);
$c->setPath("lifestyle");
$c->setWatermark();
$form->AppendColumn($c);

$form->itemLink = "/lifestyle/club";

$form->visitor_mode = true;
$form->visitor_url = "/$gModule/";
$form->admin_url = "/worker/lifestyle";

$form->ShowPage();

?>
