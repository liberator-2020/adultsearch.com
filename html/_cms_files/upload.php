<?php

global $smarty;

$ref = $_REQUEST["ref"];
$email = filter_var($_REQUEST["email"], FILTER_VALIDATE_EMAIL);
$note = $_REQUEST["note"];

if ($_REQUEST["submit"] == "Upload") {

	if (empty($_POST["captcha_str"])) {
		$error = "You need to type the security code";
	} else if (strcasecmp($_POST["captcha_str"], $_SESSION["captcha_str"])) {
		$error = "Security code is wrong, please type it again";
	} else {
		$handle = new upload($_FILES["file"]);
		if (!$handle->uploaded) {
			$error = "You haven't picked any file to upload!";
		} else {
			$handle->mime_check = true;
			$handle->allowed = array("image/jpeg", "image/gif", "image/png");
			$handle->file_max_size = '6000000'; // 6MB
			$handle->file_new_name_body = system::_makeText();
			$handle->file_force_extension = true;
			$handle->file_auto_rename = false;
			$handle->file_overwrite = true;
			$handle->process("/tmp/");
			if (!$handle->processed) {
				$error = "Invalid file! Please upload only JPG,GIF or PNG image files smaller than 6MB.";
			} else {
				//create email message
				if (!strstr($ref, 'adultsearch.com'))
					$ref = "adultsearch.com/$ref";
				$href = "http://{$ref}";
				$message = "The upload picture form on adultsearch.com ";
				$message .= " was submitted for place:<br /><a href=\"{$href}\">{$href}</a><br /><br />";
				$message .= " Please check attached photo and if it is correct, add it for the place.<br /><br />";
				if ($note)
					$message .= "Note from user:<br />{$note}<br />";
				$acc_info = _getInfoAccount();
				$message .= "<br />$acc_info";

				//send email
				$params = array(
					"from" => ADMIN_EMAIL,
					"to" => SUPPORT_EMAIL,
					"subject" => "Upload picture form submitted",
					"html" => $message,
					"attachments" => array($handle->file_dst_name => $handle->file_dst_pathname),
					);
				if ($email)
					$params["reply_to"] = $email;
				$error = "";
				$ret = send_email($params, $error);

				@unlink($handle->file_dst_pathname);
				system::go($ref, "Thank you for your support!");
			}
			$handle->clean();	//deleting uploaded tmp file
		}
	}

	$smarty->assign("email", $email);
	$smarty->assign("note", $note);
	$smarty->assign("error", $error);

} elseif (isset($_SESSION["email"]))
	$smarty->assign("email", $_SESSION["email"]);

$smarty->assign("ref", htmlspecialchars($ref));

if (!empty($_POST["captcha_str"]) && !strcasecmp($_POST["captcha_str"], $_SESSION["captcha_str"])) {
	$smarty->assign("captcha_str", $_POST["captcha_str"]);
	$smarty->assign("captcha_ok", true);
}

$smarty->display(_CMS_ABS_PATH."/templates/upload.tpl");

?>
