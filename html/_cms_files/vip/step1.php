<?php

global $db, $smarty, $account, $config_dev_server, $mobile;

if ($mobile != 1)
	include_html_head("css", "/css/adbuild.css?20160719");

include_html_head("js", "/js/adbuildUI.js");
$smarty->assign("adbuild", 1);
$smarty->assign("nobanner", true);

$system = new system;
$vip = new vip;

$account_id = $account->isloggedin() ?: NULL;
if (intval($_REQUEST["account_id"]))
	$account_id = intval($_REQUEST["account_id"]);

$acc = account::findOneById($account_id);
if ($acc && $acc->isVip()) {
	//account is already vip
	if ($account_id == $account->getId())
		system::go("/", "You already have VIP membership");
	else
		system::go("/", "User already has VIP membership");
}

$type = intval($_POST["type"]);

if ($type) {
	$recurring = ($type == 1) ? 1 : 0;
	$total = ($recurring) ? $vip->price_recurring : $vip->price_normal;

	$account_id = intval($account_id);

	$res = $db->q("INSERT INTO vip_payment (account_id, recurring, total, created_stamp) VALUES (?, ?, ?, ?)", array($account_id, $recurring, $total, time()));
	$payment_id = $db->insertid($res);
	if (!$payment_id) {
		//failure
		reportAdmin("AS: vip-step1 error", "cant insert", array($account_id, $recurring, $total, $now));
		system::go("/vip/step1");
	}
	$_SESSION["vip_payment_id"] = $payment_id;

	//success
	system::go("/vip/step2");
}

$smarty->assign('account_id', $account_id);
$smarty->assign('type', $type);

$smarty->assign("csrf", csrf::get_html_code("vip_step1"));

if (isset($_REQUEST["ref"]))
	$smarty->assign("ref", $_REQUEST["ref"]);

$smarty->display(_CMS_ABS_PATH."/templates/vip/step1.tpl");

?>
