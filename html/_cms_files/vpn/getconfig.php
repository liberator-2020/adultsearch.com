<?php
error_reporting(E_ALL);
ini_set('log_errors', true);
ini_set('display_errors', true);
ini_set('max_execution_time', 0);

defined("_CMS_FRONTEND") or die("no access");

global $account, $db, $smarty;
$smarty->assign("nobanner", true);

die("Not used anymore");
/*

$tmp_dirname = uniqid();
//echo "Creating tmp dir '{$tmp_dirname} ... <br />\n";
$tmp_dirpath = _CMS_ABS_PATH."/tmp/".$tmp_dirname;
$ret = mkdir($tmp_dirpath);
if (!$ret)
	die("Can't create tmp dir '{$tmp_dirpath}' !");

$res = $db->q("SELECT * FROM vpn_provider");
while ($row = $db->r($res)) {
	$provider_id = $row["id"];
	$nickname = $row["nickname"];
	$username = $row["username"];
	$password = $row["password"];
//	echo "Provider: #{$provider_id}, nickname={$nickname}, username={$username}, password={$password}<br />\n";

	//create auth file
	$h = fopen($tmp_dirpath."/{$nickname}_auth.txt", "w");
	if (!$h)
		die("Can't open auth file for '{$nickname}' !");
	fputs($h, "{$username}\n{$password}\n");
	fclose($h);

	//create ip openvpn config files
	$i = 0;
	$res2 = $db->q("SELECT h.name, s.id, s.ip_address FROM vpn_server s INNER JOIN vpn_host h on h.id = s.host_id WHERE h.provider_id = ? ORDER BY s.id ASC LIMIT 49", array($provider_id));
	while ($row2 = $db->r($res2)) {
		$i++;
		$server_id = $row2["id"];
		$ip_address = $row2["ip_address"];
		$host_name = $row2["name"];
		$arr = explode(".", $host_name);
		$host = $arr[0];
		$config_name = "{$server_id}-{$host}";
//		echo "IP: {$ip_address}, host={$host}, config_name={$config_name}<br />\n";

		$h = fopen($tmp_dirpath."/{$config_name}.ovpn", "w");
		if (!$h)
			die("Can't open auth file for '{$config_name}' !");
		fputs($h, "client\ndev tun\nproto udp\nremote {$ip_address} 1194\nresolv-retry infinite\nnobind\npersist-key\npersist-tun\nca ca.crt\ntls-client\nremote-cert-tls server\nauth-user-pass\ncomp-lzo\nverb 1\nreneg-sec 0\ncrl-verify crl.pem\nauth-user-pass {$nickname}_auth.txt");
		fclose($h);
	}

}

//pack
chdir($tmp_dirpath);
exec("zip openvpn.zip ./*", $output, $ret);

//stream
ob_clean();
header("Content-Type: application/x-zip" );
header("Content-Disposition: attachment; filename=\"openvpn.zip\"");
$h = fopen($tmp_dirpath."/openvpn.zip", "r");
fpassthru($h);
fclose($h);
die;
*/

?>

