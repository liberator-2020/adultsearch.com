<?php
defined("_CMS_FRONTEND") or die("no access");

global $db;
define("PASSWORD", "Va1ko8a3");
$now = time();

file_log("vpn", "vpn::getnextconfig: _REQUEST='".print_r($_REQUEST, true)."'");

if ($_REQUEST["password"] != PASSWORD) {
	file_log("vpn", "vpn::getnextconfig: error: invalid password");
	die(json_encode(["status" => "error", "reason" => "Invalid password, please contact administrator"]));
}

//get vpn server with oldest reservation, that has not been used for posting ad in last month
$res = $db->q("
	SELECT s.id, s.ip_address, h.name, p.nickname, p.username, p.password
	FROM vpn_server s
	INNER JOIN vpn_host h on h.id = s.host_id
	INNER JOIN vpn_provider p on p.id = h.provider_id
	LEFT JOIN ccbill_post c on c.ip_address = s.ip_address and c.stamp > ?
	WHERE c.id IS NULL
	ORDER BY IFNULL(s.last_reserved_stamp, 1) ASC
	LIMIT 1
	",
	array($now - 86400*30)
	);
if ($db->numrows($res) == 0) {
	//no free ip!
	//log
	file_log("vpn", "vpn::getnextconfig: error: no free ip");
	//notify admin
	reportAdmin("AS: No free vpn ip for ad posting", "", array());
	//return error message
	die(json_encode(["status" => "error", "reason" => "No free ip address, please contact administrator"]));
}
$row = $db->r($res);
//_darr($row);

$vpn_server_id = $row["id"];
$ip_address = $row["ip_address"];
$host_name = $row["name"];
$nickname = $row["nickname"];
$username = $row["username"];
$password = $row["password"];

$auth_name = $auth_content = NULL;
if ($username && $password) {
	$auth_name = "{$nickname}_auth.txt";
	$auth_content = "{$username}\n{$password}\n";	
}
$arr = explode(".", $host_name);
$host = $arr[0];
$config_name = "{$vpn_server_id}-{$host}";
$config_content = "client\ndev tun\nproto udp\nremote {$ip_address} 1194\nresolv-retry infinite\nnobind\npersist-key\npersist-tun\nca ca.crt\ntls-client\nremote-cert-tls server\nauth-user-pass\ncomp-lzo\nverb 1\nreneg-sec 0\ncrl-verify crl.pem";
if ($auth_name)
	$config_content .= "\nauth-user-pass {$auth_name}";

file_log("vpn", "vpn::getnextconfig: vpn_server_id='{$vpn_server_id}', ip_address='{$ip_address}', host='{$host}', config_name='{$config_name}', auth_name='{$auth_name}', config_content_len=".strlen($config_content).", auth_content_len=".strlen($auth_content));

//make reservation for this ip
$db->q("UPDATE vpn_server SET last_reserved_stamp = ? WHERE id = ?", array($now, $vpn_server_id));
$aff = $db->affected($res);
file_log("vpn", "vpn::getnextconfig: reservation with stamp '{$now}' success:'{$aff}'");

$config_files = array();
if (file_exists(_CMS_ABS_PATH."/_cms_files/vpn/ca.crt"))
	$config_files[] = array(
		"filename" => "ca.crt",
		"content" => file_get_contents(_CMS_ABS_PATH."/_cms_files/vpn/ca.crt"),
		);
if (file_exists(_CMS_ABS_PATH."/_cms_files/vpn/crl.pem"))
	$config_files[] = array(
		"filename" => "crl.pem",
		"content" => file_get_contents(_CMS_ABS_PATH."/_cms_files/vpn/crl.pem"),
		);

//return JSON
$response = [
	"status" => "ok",
	"ip_address" => $ip_address,
	"config_name" => $config_name,
	"config_content" => $config_content,
	"auth_name" => $auth_name,
	"auth_content" => $auth_content,
	"config_files" => $config_files,
	];
die(json_encode($response));

?>

