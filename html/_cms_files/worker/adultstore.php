<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $page;
$system = new system;

require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
$form = new data;

$account_id = $account->isloggedin();
if( !isset($_SESSION["worker"]) && !$account->isadmin() ) $system->go("/");

if( isset($_REQUEST["id"]) ) $id = GetREQUESTParam("id");
else $id = NULL;

$form->table = "adult_stores";

$mandatory_for_admin = $account->isadmin() ? false : true; 

$c = new CColumnId("id", "ID");
$form->AppendColumn($c);

$c = new CColumnLocationAjax("City", "Location", 4);
$c->setColMandatory(true);
$c->addLocPair("param", "loc_id", "City", array(array("dst_name"=>"loc_name", "src_name"=>"loc_name"),array("dst_name"=>"state", "src_name"=>"s")));
$form->AppendColumn($c);

$c = new CColumnString("name", "Name");
$form->AppendColumn($c);

$c = new CColumnString("address1", "Address");
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "zipcode");
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Phone");
if(!$account->isadmin())
	$c->setUnique();
$c->setUnique();
$c->skip_unique = true;
$form->AppendColumn($c);

$c = new CColumnPhone("phone2", "Phone 2");
$form->AppendColumn($c);

$c = new CColumnPhone("fax", "Fax");
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnString("facebook", "<img src=\"/images/icons/facebook_30.png\" style=\"width: 16px; vertical-align: middle;\" /> Facebook URL");
$form->AppendColumn($c);

$c = new CColumnString("twitter", "<img src=\"/images/icons/twitter_30.png\" style=\"width: 16px; vertical-align: middle;\" /> Twitter URL");
$form->AppendColumn($c);

$c = new CColumnString("instagram", "<img src=\"/images/icons/instagram_30.png\" style=\"width: 16px; vertical-align: middle;\" /> Instagram URL");
$form->AppendColumn($c);

	$c = new CColumnString("email", "E-Mail");
	$form->AppendColumn($c);

	$c = new CColumnHour("hours", "Hours Of Operation");
	if(!$account->isadmin())
		$c->setMandatory();
	$form->AppendColumn($c);

if( $account->isadmin() ) {
$c = new CColumnEnum("lingerie", "Lingerie");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("adult_toys", "Adult Toys");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("adult_dvds", "Adult DVD");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("peep_shows", "Peep Shows");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("adult_books", "Adult Books");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("adult_video_arcade", "Arcade");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

}

$c = new CColumnEnum("gloryhole", "Glory Hole");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("theaters", "Theaters");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnFee("theater_single_fee", "Theater Price Single");
$c->start = 5; $c->end = 21; $c->interval = 1;
$form->AppendColumn($c);

$c = new CColumnFee("theater_couple_fee", "Theater Price Couple");
$c->start = 5; $c->end = 21; $c->interval = 1;
$form->AppendColumn($c);

$c = new CColumnEnum("fetish", "Fetish");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("lgbt", "LGBT");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnBoolean("truckparking", "Truck Parking");
$c->setColTitle("Parking");
$form->AppendColumn($c);

$c = new CColumnBoolean("buddy_booth_available", "Buddy Booths Available");
$c->setColTitle("Buddy Booths");
$form->AppendColumn($c);


$c = new CColumnText("worker_comment", "Comments");
$c->updateOnSkip();
$c->updateOnDelete();
$form->AppendColumn($c);

if ($account->isadmin() || permission::has("edit_all_places")) {
	$c = new CColumnImage("image", "Main Image");
	$c->setMaxDimensions(700, 500);
	$c->setWatermark();
	$c->setBackup("adultstore");
	$c->setPath("adultstore");
	$form->AppendColumn($c);

	$c = new CColumnImage("thumb", "Thumbnail");
	$c->setDimensions(220, 165);
	$c->setWatermark();
	$c->setPath("adultstore/t");
	$form->AppendColumn($c);

	$c = new CColumnImage("coupon", "Coupon");
	$c->setMaxDimensions(700, 500);
	$c->setPath("adultstore/c");
	$form->AppendColumn($c);

	$c = new CColumnPicture("adult_store_picture", "Extra Pictures", "adultstore");
	$c->setPath("adultstore");
	$c->setPathT("adultstore/t");
	$c->setMaxDimensions(700, 500);
	$c->setDimensionsT(220, 165);
	$c->setWatermark();
	$form->AppendColumn($c);

	$c = new CColumnNumber("loc_lat", "Latitude");
	$c->doubleval = true;
	$form->AppendColumn($c);

	$c = new CColumnNumber("loc_long", "Longitude");
	$c->doubleval = true;
	$form->AppendColumn($c);

	$c = new CColumnenum("edit", "Status");
	$c->AddEnumOption("-1", "Requested to be removed");
	$c->AddEnumOption("0", "New Added");
	$c->AddEnumOption("1", "Worker Mode");
	$c->AddEnumOption("2", "Done");
	$c->setDefaultValue(2);
	$c->setColMandatory(true);
	$form->AppendColumn($c);

	$c = new CColumnBoolean("fake", "Fake");
	$c->setColTitle("Fake Place ?");
	$form->AppendColumn($c);

	$c = new CColumnText("other_services", "Additional Information");
	$form->AppendColumn($c);
}

$form->show_remove = true;

$c = new CColumnText("gdescription", "Description");
$form->AppendColumn($c);


$form->itemLink = "/sex-shops/store";
$form->cache_update = "adultstore";
$form->remove_review = "adultstore";
$form->ShowPage();

?>
