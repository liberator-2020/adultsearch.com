<?php

defined('_CMS_FRONTEND') or die('Restricted access');

require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");

global $db, $smarty, $account;
$form = new data;
$system = new system;

$account_id = $account->IsLoggedIn();
$isadmin = $account->isadmin();
if( !isset($_SESSION["worker"]) && !$account->isadmin() ) $system->go("/");

$form->table = "brothel";

$c = new CColumnId("id", "ID");
$form->AppendColumn($c);

$c = new CColumnLocationAjax("City", "Location", 4);
$c->setColMandatory(true);
$c->addLocPair("param", "loc_id", "City", array(array("dst_name"=>"loc_name", "src_name"=>"loc_name"),array("dst_name"=>"state_name", "src_name"=>"s")));
$c->refreshbyloc = true;
$form->AppendColumn($c);

$c = new CColumnString("name", "Name");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("address1", "Address");
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "zipcode");
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Phone");
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnPhone("phone2", "Phone2");
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnPhone("fax", "Fax");
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnString("email", "E-Mail");
$form->AppendColumn($c);

$c = new CColumnHour("hours", "Hours Of Operation");
$form->AppendColumn($c);

$c = new CColumnFee("rate_15", "15 Minutes Rate");
$form->AppendColumn($c);

$c = new CColumnBoolean("cc_visa_master", "VISA & MC");
$c->setColTitle("Payments");
$c->AddOption("cc_ae", "American Express");
$c->AddOption("cc_dis", "Discover");
$c->AddOption("cc_atm", "ATM Available");
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setWatermark();
$c->setBackup("brothel");
$c->setPath("brothel");
$form->AppendColumn($c);

$c = new CColumnImage("thumb", "Thumbnail");
$c->setDimensions(220, 165);
$c->setPath("brothel/t");
$form->AppendColumn($c);

$c = new CColumnImage("coupon", "Coupon");
$c->setMaxDimensions(700, 500);
$c->setPath("brothel/c");
$form->AppendColumn($c);

$c = new CColumnPicture("brothel_picture", "Extra Pictures", "brothel");
$c->setPath("brothel");
$c->setPathT("brothel/t");
$c->setWatermark();
$form->AppendColumn($c);

$c = new CColumnNumber("loc_lat", "Latitude");
$form->AppendColumn($c);

$c = new CColumnNumber("loc_long", "Longitude");
$form->AppendColumn($c);

$c = new CColumnenum("edit", "Status");
$c->AddEnumOption("-1", "Requested to be removed");
$c->AddEnumOption("0", "New Added (Not Live)");
$c->AddEnumOption("1", "Live");
$c->setDefaultValue(1);
$form->AppendColumn($c);

$form->show_remove = true;
$form->itemLink = "/brothels/club";
$form->cache_update = "brothel";
$form->ShowPage();
?>
