<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $page;
$system = new system;

require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.curl.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.location.php");

class data2 extends data {
	function BeforeDelete() {
		$this->db->q("delete from eroticmassage_tag_assign where id = '$this->itemID'");
		//TODO delete reviews
		return true;
	}
}

$country_id = 0;
if( isset($_GET['ajaxdie']) ) {
	$loc_id = intval($_GET['City']);
	$location = new location($db);
	$loc = $location->_coreLocationBreadArray($loc_id);
	$country_id = $loc[count($loc)-1]['loc_id'];
}

$form = new data2;
$account_id = $account->isloggedin();

$form->table = "eroticmassage";

$c = new CColumnId("id", "ID");
$form->AppendColumn($c);

$c = new CColumnString("name", "Name");
$form->AppendColumn($c);

$c = new CColumnLocationAjax("City", "Location", 4);
$c->setColMandatory(true);
$c->addLocPair("param", "loc_id", "City", array(array("dst_name"=>"loc_name", "src_name"=>"loc_name"),array("dst_name"=>"state_name", "src_name"=>"s")));
$c->refreshbyloc = true;
$form->AppendColumn($c);


$c = new CColumnString("address1", "Address");
$form->AppendColumn($c);

$c = new CColumnString("address2", "Floor/Unit");
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "Zipcode");
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Club Phone");
$c->setUnique();
$c->skip_unique = true;
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnString("facebook", "<img src=\"/images/icons/facebook_30.png\" style=\"width: 16px; vertical-align: middle;\" /> Facebook URL");
$form->AppendColumn($c);

$c = new CColumnString("twitter", "<img src=\"/images/icons/twitter_30.png\" style=\"width: 16px; vertical-align: middle;\" /> Twitter URL");
$form->AppendColumn($c);

$c = new CColumnString("instagram", "<img src=\"/images/icons/instagram_30.png\" style=\"width: 16px; vertical-align: middle;\" /> Instagram URL");
$form->AppendColumn($c);

$c = new CColumnString("email", "E-Mail");
$form->AppendColumn($c);

$c = new CColumnBoolean("byapponly", "Appointment Only");
$form->AppendColumn($c);

$c = new CColumnHour("hours", "Hours of Operation:");
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setBackup("emp");
$c->setPath("eroticmassage");
$c->setWatermark();
$form->AppendColumn($c);

$c = new CColumnImage("thumb", "Thumbnail");
$c->setDimensions(220, 165);
$c->setWatermark();
$c->setPath("eroticmassage/t");
$form->AppendColumn($c);

$c = new CColumnImage("coupon", "Coupon");
$c->setMaxDimensions(700, 500);
$c->setPath("eroticmassage/c");
$form->AppendColumn($c);

$c = new CColumnMultipleForeign('eroticmassage_tag_assign', 'tag_assign_id', 'id', 'tag_id', 'eroticmassage_tag', 'tag_id', 'tag_name', 'Masseuse');
$form->AppendColumn($c);

$c = new CColumnFee("rate_15", "15 Minutes Rate");
$c->country_id = $country_id;
$form->AppendColumn($c);

$c = new CColumnFee("rate_30", "30 Minutes Rate");
$c->country_id = $country_id;
$form->AppendColumn($c);

$c = new CColumnFee("rate_45", "45 Minutes Rate");
$c->country_id = $country_id;
$form->AppendColumn($c);

$c = new CColumnFee("rate_60", "60 Minutes Rate");
$c->country_id = $country_id;
$form->AppendColumn($c);

$c = new CColumnFee("rate_90", "90 Minutes Rate");
$c->country_id = $country_id;
$form->AppendColumn($c);

$c = new CColumnFee("rate_120", "120 Minutes Rate");
$c->country_id = $country_id;
$form->AppendColumn($c);

$c = new CColumnEnum("table_shower", "Table Shower");
$c->AddEnumOption("1", "Yes");
$c->AddEnumOption("0", "No");
$form->AppendColumn($c);

//$c = new CColumnString("rate_table_shower", "Table Shower Fee");
$c = new CColumnFee("rate_table_shower", "Table Shower Fee");
$c->country_id = $country_id;
$form->AppendColumn($c);

$c = new CColumnEnum("sauna", "Sauna");
$c->AddEnumOption("1", "Yes");
$c->AddEnumOption("0", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("jacuzzi", "Jacuzzi");
$c->AddEnumOption("1", "Yes");
$c->AddEnumOption("0", "No");
$form->AppendColumn($c);

$c = new CColumnBoolean("truckparking", "Truck Parking");
$c->setColTitle("Parking");
$c->AddOption("private_parking", "Private Parking");
$form->AppendColumn($c);

$c = new CColumnBoolean("cc_visa", "VISA & MC");
$c->setColTitle("Payments");
$c->AddOption("cc_ae", "American Express");
$c->AddOption("cc_dis", "Discover");
$form->AppendColumn($c);

$c = new CColumnBoolean("fourhand", "Yes");
$c->setColTitle("4 Hand Massage Available");
$form->AppendColumn($c);

$c = new CColumnBoolean("nonerotic", "Yes");
$c->setColTitle("Non-Erotic Massage ?");
$form->AppendColumn($c);

$c = new CColumnString("major_cross_streets", "Major Cross Streets");
$form->AppendColumn($c);

$c = new CColumnText("other_services", "Additional Information");
$c->setMaxLength(255);
$form->AppendColumn($c);

$c = new CColumnNumber("loc_lat", "Latitude");
$c->doubleval = true;
$form->AppendColumn($c);

$c = new CColumnNumber("loc_long", "Longitude");
$c->doubleval = true;
$form->AppendColumn($c);

$c = new CColumnenum("edit", "Status");
$c->AddEnumOption("-1", "Requested to be removed");
$c->AddEnumOption("0", "Not Live");
$c->AddEnumOption("1", "Live");
$c->setDefaultValue(1);
$c->setColMandatory(true);
$form->AppendColumn($c);

if( $account->isadmin() || permission::has("edit_all_places") ) {
	$c = new CColumnBoolean("fake", "Fake");
	$c->setColTitle("Fake MP ?");
	$form->AppendColumn($c);
}

$c = new CColumnPicture("eroticmassage_picture", "Extra Pictures", "eroticmassage");
$c->setPath("eroticmassage");
$c->setPathT("eroticmassage/t");
$form->AppendColumn($c);

$c = new CColumnText("gdescription", "Description");
$form->AppendColumn($c);

if( $account->isadmin() || permission::has("edit_all_places") ) {
	$val = "<div id='empbaba'></div>";
	$c = new CColumnManual("empbaba", "EroticMP.com", $val);
	$form->AppendColumn($c);
}

$form->show_remove = true;
$form->itemLink = "/erotic-massage/parlor";

if( isset($_POST["die_after_delete"]) ) $form->die_after_delete = true;

$form->cache_update = "eroticmassage";
$form->remove_review = "eroticmassage";
$form->ShowPage();

if( isset($_GET['ajaxdie']) ) die;

/*
if( $form->itemID && !isset($_GET['newitem']) && ($account->isadmin() || permission::has("edit_all_places")) ) {
	$res = $db->q("select * from eroticmassage where id = '$form->itemID' and country_id in (16046, 16047)");
	if( $db->numrows($res) ) {
		$row = $db->r($res); 
		if( !empty($row['zipcode']) ) {
		if( strstr($row["name"], "&") ) $row["name"] = rawurlencode($row["name"]);
		$data = "phone={$row["phone"]}&name={$row["name"]}&address1={$row["address1"]}&address2={$row["address2"]}&zipcode={$row["zipcode"]}&rate_30={$row["rate_30"]}&rate_45={$row["rate_45"]}&rate_60={$row["rate_60"]}&rate_90={$row["rate_90"]}&rate_120={$row["rate_120"]}&table_shower={$row["table_shower"]}&rate_table_shower={$row["rate_table_shower"]}&sauna={$row["sauna"]}&jacuzzi={$row["jacuzzi"]}&cc_visa={$row["cc_visa"]}&cc_ae={$row["cc_ae"]}&cc_dis={$row["cc_dis"]}&truckparking={$row["truckparking"]}&private_parking={$row["private_parking"]}&monday_from={$row["monday_from"]}&monday_to={$row["monday_to"]}&tuesday_from={$row["tuesday_from"]}&tuesday_to={$row["tuesday_to"]}&wednesday_from={$row["wednesday_from"]}&wednesday_to={$row["wednesday_to"]}&thursday_from={$row["tuesday_from"]}&thursday_to={$row["tuesday_to"]}&friday_from={$row["friday_from"]}&friday_to={$row["friday_to"]}&saturday_from={$row["saturday_from"]}&saturday_to={$row["saturday_to"]}&sunday_from={$row["sunday_from"]}&sunday_to={$row["sunday_to"]}&website={$row["website"]}&email={$row["email"]}&major_cross_streets={$row["major_cross_streets"]}&nonerotic={$row["nonerotic"]}&country_id={$row["country_id"]}&loc_name={$row["loc_name"]}";

		if( $row["loc_lat"] && $row["loc_long"] ) $data .= "&loc_lat={$row["loc_lat"]}&loc_long={$row["loc_long"]}";

		if( $row["always"] ) $data .= "&always=1";

		$res = $db->q("select tag_id from eroticmassage_tag_assign where id = '$form->itemID'");
		while($row=$db->r($res)) $data .= "&tag_id[]={$row[0]}";
		$curl = new curl("empbaba.txt", 1);
		$r = $curl->post("http://www.eroticmp.com/admin/empbaba", $data);
		if( $r ) { $r = addslashes($r);

echo "
<script type='text/javascript'><!--
	$('#empbaba').html('$r');
-->
</script>
";
		} 
		}
	} else {
echo "
<script type='text/javascript'><!--
	$('#empbaba').html('wont be added on eroticmp');
-->
</script>
";
	}

} elseif( $form->itemID && ($account->isadmin() || permission::has("edit_all_places")) ) {
echo "
<script type='text/javascript'><!--
        $('#empbaba').html(\"This parlor hasnt been updated on EroticMp yet, to process the update, <a href='/worker/emp?id={$form->itemID}'>click here</a>\");
-->
</script>
";
}
*/

?>

