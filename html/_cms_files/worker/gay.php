<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $page;
$system = new system;

require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
require(_CMS_ABS_PATH."/_cms_files/gay/array.php");
$form = new data;

$account_id = $account->isloggedin();
if( !isset($_SESSION["worker"]) && !$account->isadmin() ) $system->go("/");

if( isset($_REQUEST["id"]) ) $id = GetREQUESTParam("id");
else $id = NULL;

$form->table = "gay";

$mandatory_for_admin = $account->isadmin() ? false : true; 

$c = new CColumnId("id", "ID");
$form->AppendColumn($c);

asort($type_array);
$c = new CColumnEnum("type", "Type");
foreach($type_array as $key=>$value) $c->AddEnumOption($key, $value);
$c->setcolmandatory(true);
$form->AppendColumn($c);

$c = new CColumnLocationAjax("City", "Location", 4);
$c->setColMandatory(true);
$c->addLocPair("param", "loc_id", "City", array(array("dst_name"=>"loc_name", "src_name"=>"loc_name"),array("dst_name"=>"state", "src_name"=>"s")));
$form->AppendColumn($c);

$c = new CColumnString("name", "Name");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("address1", "Address");
if( $mandatory_for_admin ) $c->SetColReadOnly(true);
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "zipcode");
if( $mandatory_for_admin ) $c->SetColReadOnly(true);
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Phone");
if( !$mandatory_for_admin ) $c->setUnique();
if( $mandatory_for_admin ) $c->SetColReadOnly(true);
$form->AppendColumn($c);

$c = new CColumnPhone("fax", "Fax");
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnString("email", "E-Mail");
$form->AppendColumn($c);

$c = new CColumnText("other_services", "Additional Information");
$form->AppendColumn($c);

$c = new CColumnHour("hours", "Hours Of Operation");
if( $mandatory_for_admin ) $c->setMandatory();
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setWatermark();
$c->setBackup("gay");
$c->setPath("gay");
$form->AppendColumn($c);

	$c = new CColumnImage("thumb", "Thumbnail");
	$c->setDimensions(220, 165);
	$c->setWatermark();
	$c->setPath("gay/t");
	$form->AppendColumn($c);

	$c = new CColumnImage("coupon", "Coupon");
	$c->setMaxDimensions(700, 500);
	$c->setPath("gay/c");
	$form->AppendColumn($c);

	$c = new CColumnPicture("picture", "Extra Pictures", "gay");
	$c->setPath("gay");
	$c->setPathT("gay/t");
	$c->setMaxDimensions(700, 500);
	$c->setDimensionsT(220, 165);
	$c->setWatermark();
	$form->AppendColumn($c);

if($account->isrealadmin()) {
	$c = new CColumnNumber("loc_lat", "Latitude");
	$form->AppendColumn($c);

	$c = new CColumnNumber("loc_long", "Longitude");
	$form->AppendColumn($c);
}

	$c = new CColumnenum("edit", "Status");
	$c->AddEnumOption("-1", "Requested to be removed");
	$c->AddEnumOption("0", "Not Live");
	$c->AddEnumOption("1", "Live");
	$c->setDefaultValue(1);
	$form->AppendColumn($c);

if( $account->isadmin() ) $form->show_remove = true;

$form->itemLink = "/gay/place";
$form->cache_update = "gay";
$form->ShowPage();

?>
