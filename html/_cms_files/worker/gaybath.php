<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $page;
$system = new system;

require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
$form = new data;

$account_id = $account->isloggedin();
if( !isset($_SESSION["worker"]) && !$account->isadmin() ) $system->go("/");

if( isset($_REQUEST["id"]) ) $id = GetREQUESTParam("id");
else $id = NULL;

$form->table = "gaybath";

$mandatory_for_admin = $account->isadmin() ? false : true; 

$c = new CColumnId("id", "ID");
$form->AppendColumn($c);

$c = new CColumnLocationAjax("City", "Location", 4);
$c->setColMandatory(true);
$c->addLocPair("param", "loc_id", "City", array(array("dst_name"=>"loc_name", "src_name"=>"loc_name"),array("dst_name"=>"state", "src_name"=>"s")));
$form->AppendColumn($c);

$c = new CColumnString("name", "Name");
if( $mandatory_for_admin ) $c->SetColReadOnly(true);
$form->AppendColumn($c);

$c = new CColumnString("address1", "Address");
if( $mandatory_for_admin ) $c->SetColReadOnly(true);
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "zipcode");
if( $mandatory_for_admin ) $c->SetColReadOnly(true);
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Phone");
if( !$mandatory_for_admin ) $c->setUnique();
if( $mandatory_for_admin ) $c->SetColReadOnly(true);
$c->setUnique();
$c->skip_unique = true;
$form->AppendColumn($c);

	$c = new CColumnPhone("fax", "Fax");
	$form->AppendColumn($c);

	$c = new CColumnString("website", "Website URL");
	$form->AppendColumn($c);

	$c = new CColumnString("email", "E-Mail");
	$form->AppendColumn($c);

	$c = new CColumnHour("hours", "Hours Of Operation");
	if( $mandatory_for_admin ) $c->setMandatory();
	$form->AppendColumn($c);

$c = new CColumnFee("daypass", "Day Pass");
$c->start = 5; $c->end = 31; $c->interval = 1;
$form->AppendColumn($c);

$c = new CColumnFee("mem_1month", "Monthlu Membership");
$c->start = 5; $c->end = 42; $c->interval = 1;
$form->AppendColumn($c);

$c = new CColumnFee("mem_3month", "3 Months Membership");
$c->start = 5; $c->end = 42; $c->interval = 1;
$form->AppendColumn($c);

$c = new CColumnFee("mem_6month", "6 Months Membership");
$c->start = 5; $c->end = 62; $c->interval = 1;
$form->AppendColumn($c);

$c = new CColumnFee("mem_annual", "Annual Membership");
$c->start = 3; $c->end = 102; $c->interval = 1;
$form->AppendColumn($c);

$c = new CColumnFee("locker", "Locker");
$c->start = 2; $c->end = 32; $c->interval = 1;
$form->AppendColumn($c);

$c = new CColumnFee("stdroom", "Standard Room");
$c->start = 5; $c->end = 51; $c->interval = 1;
$form->AppendColumn($c);

$c = new CColumnFee("viproom", "Vip Room");
$c->start = 5; $c->end = 51; $c->interval = 1;
$form->AppendColumn($c);

$c = new CColumnFee("slingroom", "Sling Room");
$c->start = 5; $c->end = 51; $c->interval = 1;
$form->AppendColumn($c);

$c = new CColumnFee("parking", "Parking");
$c->start = 2; $c->end = 10; $c->interval = 1;
$form->AppendColumn($c);

$c = new CColumnBoolean("gym", "Gym");
$c->setColTitle("Amenities");
$c->AddOption("pool", "Pool");
$c->AddOption("sauna", "Sauna");
$c->AddOption("jacuzzi", "Jacuzzi");
$c->AddOption("showers", "Showers");
$c->AddOption("internet", "Internet");
$c->AddOption("gloryhole", "Glory Hole");
$form->AppendColumn($c);

$c = new CColumnText("worker_comment", "Comments");
$c->updateOnSkip();
$c->updateOnDelete();
$form->AppendColumn($c);

	$c = new CColumnImage("image", "Main Image");
	$c->setMaxDimensions(700, 500);
	$c->setWatermark();
	$c->setBackup("gaybath");
	$c->setPath("gaybath");
	$form->AppendColumn($c);

	$c = new CColumnImage("thumb", "Thumbnail");
	$c->setDimensions(220, 165);
	$c->setWatermark();
	$c->setPath("gaybath/t");
	$form->AppendColumn($c);

	$c = new CColumnPicture("gaybath", "Extra Pictures", "gaybath");
	$c->setPath("gaybath");
	$c->setPathT("gaybath/t");
	$c->setMaxDimensions(700, 500);
	$c->setDimensionsT(220, 165);
	$c->setWatermark();
	$form->AppendColumn($c);

	$c = new CColumnImage("coupon", "Coupon");
	$c->setMaxDimensions(700, 500);
	$c->setPath("gaybath/c");
	$form->AppendColumn($c);

	$c = new CColumnNumber("loc_lat", "Latitude");
	$form->AppendColumn($c);

	$c = new CColumnNumber("loc_long", "Longitude");
	$form->AppendColumn($c);

	$c = new CColumnenum("edit", "Status");
	$c->AddEnumOption("-1", "Requested to be removed");
	$c->AddEnumOption("0", "Not Live");
	$c->AddEnumOption("1", "Live");
	$c->setDefaultValue(1);
	$form->AppendColumn($c);

if( $account->isadmin() ) $form->show_remove = true;

$c = new CColumnText("gdescription", "Description");
$form->AppendColumn($c);


$form->itemLink = "/gay-bath-houses/place";
$form->cache_update = "gaybath";
$form->remove_review = "gaybath";
$form->ShowPage();

?>
