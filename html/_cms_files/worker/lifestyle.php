<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account, $page;
$system = new system;

require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
$form = new data;

$account_id = $account->isloggedin();
if( !isset($_SESSION["worker"]) && !$account->isadmin() ) $system->go("/");

if( isset($_REQUEST["id"]) ) $id = GetREQUESTParam("id");
else $id = NULL;

$form->table = "lifestyle";

$c = new CColumnId("id", "ID");
$form->AppendColumn($c);

$c = new CColumnString("name", "Name");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnLocationAjax("City", "Location", 4);
$c->setColMandatory(true);
$c->addLocPair("param", "loc_id", "City", array(array("dst_name"=>"loc_name", "src_name"=>"loc_name"),array("dst_name"=>"state", "src_name"=>"s")));
$form->AppendColumn($c);

$c = new CColumnString("address1", "Address");
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "Zipcode");
$form->AppendColumn($c);

$c = new CColumnEnum("type", "Club Type");
$c->setColMandatory(true);
$c->AddEnumOption("1", "Swinger");
$c->AddEnumOption("2", "BDSM");
$c->AddEnumOption("3", "Topless Pool");
$c->AddEnumOption("4", "Nudist Colony");
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Club Phone");
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnString("email", "E-Mail");
$form->AppendColumn($c);

$c = new CColumnHour("hours", "Hours of Operation:");
$form->AppendColumn($c);

$c = new CColumnFee("cover_men", "Cover Men");
$form->AppendColumn($c);

$c = new CColumnFee("cover_women", "Cover Women");
$form->AppendColumn($c);

$c = new CColumnFee("cover_couple", "Cover For Couples");
$form->AppendColumn($c);

$c = new CColumnEnum("entry", "Restrictions");
$c->AddEnumOption("0", "None");
$c->AddEnumOption("1", "Couples Only");
$c->AddEnumOption("2", "Couples & Women Only");
$form->AppendColumn($c);

$c = new CColumnBoolean("private_address", "Private Address");
$form->AppendColumn($c);

$c = new CColumnText("other_services", "Additional Information");
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setBackup(true);
$c->setPath("lifestyle");
$c->setWatermark();
$form->AppendColumn($c);

$c = new CColumnImage("thumb", "Thumbnail");
$c->setDimensions(220, 165);
$c->setPath("lifestyle/t");
$form->AppendColumn($c);

$c = new CColumnImage("coupon", "Coupon");
$c->setMaxDimensions(700, 500);
$c->setPath("lifestyle/c");
$form->AppendColumn($c);

$c = new CColumnPicture("lifestyle_picture", "Extra Pictures", "lifestyle");
$c->setPath("lifestyle");
$c->setPathT("lifestyle/t");
$c->setMaxDimensions(700, 500);
$c->setDimensionsT(220, 165);
$c->setWatermark();
$form->AppendColumn($c);

$c = new CColumnNumber("loc_lat", "Latitude");
$form->AppendColumn($c);

$c = new CColumnNumber("loc_long", "Longitude");
$form->AppendColumn($c);

$c = new CColumnText("gdescription", "Description");
$form->AppendColumn($c);

$c = new CColumnenum("edit", "Status");
$c->AddEnumOption("0", "New Added");
$c->AddEnumOption("1", "Done");
$c->setDefaultValue(1);
$form->AppendColumn($c);


$form->itemLink = "/lifestyle/club";

$form->show_remove = true;
$form->cache_update = "lifestyle";
$form->ShowPage();

?>
