<?php

defined('_CMS_FRONTEND') or die('Restricted access');

global $db, $smarty, $account;
$system = new system;

require_once(_CMS_ABS_PATH."/_cms_files/lingerie-modeling-1on1/array.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");

class data2 extends data {
	function BeforeDelete() {
		return true;
	}
}

$form = new data2;
$account_id = $account->isloggedin();
if( !isset($_SESSION["worker"]) && !$account->isadmin() ) $system->go("/");

$form->table = "lingeriemodeling1on1";

$c = new CColumnId("id", "ID");
$form->AppendColumn($c);

$c = new CColumnString("name", "Name");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnLocationAjax("City", "Location", 4);
$c->setColMandatory(true);
$c->addLocPair("param", "loc_id", "City", array(array("dst_name"=>"loc_name", "src_name"=>"loc_name"),array("dst_name"=>"state_name", "src_name"=>"s")));
$form->AppendColumn($c);

$c = new CColumnString("address1", "Address");
$form->AppendColumn($c);

$c = new CColumnString("address2", "Floor/Unit");
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "Zipcode");
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Club Phone");
$c->setUnique();
$c->skip_unique = true;
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnString("email", "E-Mail");
$form->AppendColumn($c);

$c = new CColumnHour("hours", "Hours of Operation:");
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setBackup("lingeriemodeling1on1");
$c->setPath("lingeriemodeling1on1");
$c->setWatermark();
$form->AppendColumn($c);

$c = new CColumnImage("thumb", "Thumbnail");
$c->setDimensions(220, 165);
$c->setWatermark();
$c->setPath("lingeriemodeling1on1/t");
$form->AppendColumn($c);

$c = new CColumnImage("coupon", "Coupon");
$c->setMaxDimensions(700, 500);
$c->setPath("lingeriemodeling1on1/c");
$form->AppendColumn($c);

$c = new CColumnFee("cover", "Cover");
$form->AppendColumn($c);

$c = new CColumnEnum("bar_service", "Serve alcohol ?");
foreach($bar_array as $key=>$value) $c->AddEnumOption($key, $value);
$form->AppendColumn($c);

$c = new CColumnEnum("adultmovie", "Adult movies in the room");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("toyshow", "Toy Shows");
$c->AddEnumOption("2", "Yes");
$c->AddEnumOption("1", "No");
$form->AppendColumn($c);

$c = new CColumnText("other_services", "Additional Information");
$form->AppendColumn($c);

$c = new CColumnNumber("loc_lat", "Latitude");
$form->AppendColumn($c);

$c = new CColumnNumber("loc_long", "Longitude");
$form->AppendColumn($c);

$c = new CColumnenum("edit", "Status");
$c->AddEnumOption("-1", "Requested to be removed");
$c->AddEnumOption("0", "New Added");
$c->AddEnumOption("1", "Live/Worker Mode");
$c->AddEnumOption("2", "Done");
$c->setDefaultValue(2);
$form->AppendColumn($c);

$form->show_remove = true;
$form->itemLink = "/lingerie-modeling-1on1/place";
$form->cache_update = "lingeriemodeling1on1";

if( isset($_POST["die_after_delete"]) ) $form->die_after_delete = true;

$form->ShowPage();

?>
