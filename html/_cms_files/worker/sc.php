<?php

global $db, $smarty, $account, $page;
$system = new system;

if( !($account_id = $account->isloggedin()) ) {
	$account->asklogin();
	return;
}

require_once(_CMS_ABS_PATH."/_cms_files/worker/array_sc.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");

class data2 extends data {

	function BeforeDelete() {
		$this->db->q("delete r, c from place_review r left join place_review_comment c using (review_id) where r.id = '$this->itemID' and r.module = 'stripclub'");
		if( $this->db->affected ) {
			echo "<p class='note'>{$this->db->affected} review removed.</p>";
		}
		return true;
	}

}

$form = new data2;
$system = new system;

$form->table = "strip_club2";

$c = new CColumnId("id", "BUSINESS ID");
$form->AppendColumn($c);

$c = new CColumnString("name", "Club Name");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnLocationAjax("City", "Location", 4);
$c->setColMandatory(true);
$c->addLocPair("param", "loc_id", "City", array(array("dst_name"=>"loc_name", "src_name"=>"loc_name"),array("dst_name"=>"state", "src_name"=>"s")));
$form->AppendColumn($c);

$c = new CColumnString("address1", "Club Address");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "zipcode");
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Club Phone");
$c->skip_unique = true;
$c->setColMandatory(true);
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnPhone("phone_vip", "Club Phone VIP");
$form->AppendColumn($c);

$c = new CColumnPhone("fax", "Club FAX");
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnString("facebook", "<img src=\"/images/icons/facebook_30.png\" style=\"width: 16px; vertical-align: middle;\" /> Facebook URL");
$form->AppendColumn($c);

$c = new CColumnString("twitter", "<img src=\"/images/icons/twitter_30.png\" style=\"width: 16px; vertical-align: middle;\" /> Twitter URL");
$form->AppendColumn($c);

$c = new CColumnString("instagram", "<img src=\"/images/icons/instagram_30.png\" style=\"width: 16px; vertical-align: middle;\" /> Instagram URL");
$form->AppendColumn($c);

$c = new CColumnString("email", "Club E-Mail");
$form->AppendColumn($c);

$c = new CColumnHour("hours", "Hours Of Operation");
$form->AppendColumn($c);

$c = new CColumnCover("Cover Charges");
$c->setCoverArray($cover_array);
$form->AppendColumn($c);

$c = new CColumnEnum("dancer", "Dancer");
foreach($dancer_array as $key=>$value) $c->AddEnumOption($key, $value);
$c->setColMandatory(true);
$c->setDefaultValue(1);
$form->AppendColumn($c);

$c = new CColumnEnum("bar_service", "Bar Service");
foreach($bar_array as $key=>$value) $c->AddEnumOption($key, $value);
$form->AppendColumn($c);

$c = new CColumnEnum("food", "Food Service");
foreach($food_array as $key=>$value) $c->AddEnumOption($key, $value);
$form->AppendColumn($c);

$c = new CColumnEnum("clubtype", "Club Type");
foreach($clubtype_array as $key=>$value) $c->AddEnumOption($key, $value);
$form->AppendColumn($c);

$c = new CColumnEnum("vip", "has VIP Area ?");
$c->AddEnumOption("1", "Yes");
$c->AddEnumOption("0", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("vipfee_enum", "VIP Area Fee ?");
foreach($vipfee_array as $key=>$value) $c->AddEnumOption($key, $value);
$form->AppendColumn($c);

$c = new CColumnEnum("lapdance", "Lap Dance Fee");
foreach($lapdance_array as $key=>$value) $c->AddEnumOption($key, $value);
$form->AppendColumn($c);

$c = new CColumnEnum("privatedance_fee", "Private dance fee");
foreach($lapdance_array as $key=>$value) $c->AddEnumOption($key, $value);
$form->AppendColumn($c);

$c = new CColumnEnum("privatedancevip", "Is Private Dance Fee included with VIP Area Fee ?");
$c->AddEnumOption("1", "Yes");
$c->AddEnumOption("0", "No");
$form->AppendColumn($c);

$c = new CColumnBoolean("age_18", "Age 18 and above may go");
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setPath("stripclub");
$c->setWatermark();
$form->AppendColumn($c);

$c = new CColumnImage("thumb", "Thumbnail");
$c->setDimensions(220, 165);
$c->setWatermark();
$c->setPath("stripclub/t");
$form->AppendColumn($c);

$c = new CColumnImage("coupon", "Coupon");
$c->setMaxDimensions(700, 500);
$c->setPath("stripclub/c");
$form->AppendColumn($c);

$c = new CColumnPicture("strip_club_picture", "Extra Pictures", "stripclub");
$c->setMaxDimensions(700, 500);
$c->setDimensionsT(220, 165);
$c->setPath("stripclub");
$c->setPathT("stripclub/t");
$c->setWatermark();
$form->AppendColumn($c);

$c = new CColumnText("other_services", "Additional Information");
$form->AppendColumn($c);

$c = new CColumnNumber("loc_lat", "Latitude");
$c->doubleval = true;
$form->AppendColumn($c);

$c = new CColumnNumber("loc_long", "Longitude");
$c->doubleval = true;
$form->AppendColumn($c);

$c = new CColumnenum("edit", "Status");
$c->AddEnumOption("-1", "Requested to be removed");
$c->AddEnumOption("0", "Not Live to users");
$c->AddEnumOption("5", "Live");
$c->setDefaultValue(5);
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnBoolean("fake", "Fake");
$c->setColTitle("Fake Place ?");
$form->AppendColumn($c);

$c = new CColumnBoolean("sponsor", "Sponsor");
$c->setColTitle("Sponsored at the top ?");
$form->AppendColumn($c);

$c = new CColumnText("gdescription", "Description");
$form->AppendColumn($c);

$form->itemLink = "/strip-clubs/club";
$form->show_remove = true;
$form->cache_update = "stripclub";
$form->remove_review = "stripclub";
$form->ShowPage();

?>

