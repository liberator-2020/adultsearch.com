<?php

global $db, $smarty, $account, $page;
$system = new system;

if( !($account_id = $account->isloggedin()) ) {
	$account->asklogin();
	return;
}

require_once(_CMS_ABS_PATH."/_cms_files/worker/array_sc.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");
require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");

class data2 extends data {

        function BeforeDelete() {
                $this->db->q("delete r, c from place_review r left join place_review_comment c using (review_id) where r.id = '$this->itemID' and r.module = 'stripclub'");
		if( $this->db->affected ) {
			echo "<p class='note'>{$this->db->affected} review removed.</p>";
		}
                return true;
        }

}

$form = new data2;
$system = new system;

$form->table = "strip_revues";

$c = new CColumnId("id", "BUSINESS ID");
$form->AppendColumn($c);

$c = new CColumnEnum("type", "Revue Type");
$c->setColMandatory(true);
$c->AddEnumOption("1", "Male Strip Revue");
$c->AddEnumOption("2", "Male Gay Strip Revue");
$form->AppendColumn($c);

$c = new CColumnString("name", "Revue Name");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnLocationAjax("City", "Location", 3);
$c->setColMandatory(true);
$c->addLocPair("param", "loc_id", "City", array(array("dst_name"=>"loc_name", "src_name"=>"loc_name"),array("dst_name"=>"state", "src_name"=>"s")));
$form->AppendColumn($c);

$c = new CColumnString("address1", "Club Address");
$c->setColMandatory(true);
$form->AppendColumn($c);

$c = new CColumnString("zipcode", "zipcode");
$form->AppendColumn($c);

$c = new CColumnPhone("phone", "Club Phone");
$c->skip_unique = true;
$c->setColMandatory(true);
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnPhone("fax", "Club FAX");
$c->setUnique();
$form->AppendColumn($c);

$c = new CColumnString("website", "Website URL");
$form->AppendColumn($c);

$c = new CColumnString("email", "Club E-Mail");
$form->AppendColumn($c);

$c = new CColumnFee("ticket", "Ticket Rate");
$form->AppendColumn($c);

$c = new CColumnText("schedule", "Show Schedule");
$form->AppendColumn($c);

$c = new CColumnEnum("vip", "has VIP Area ?");
$c->AddEnumOption("1", "Yes");
$c->AddEnumOption("0", "No");
$form->AppendColumn($c);

$c = new CColumnEnum("vipfee_enum", "VIP Area Fee ?");
foreach($vipfee_array as $key=>$value) $c->AddEnumOption($key, $value);
$form->AppendColumn($c);

$c = new CColumnBoolean("age_18", "Age 18 and above may go");
$form->AppendColumn($c);

$c = new CColumnImage("image", "Main Image");
$c->setMaxDimensions(700, 500);
$c->setPath("stripclub/revues");
$c->setWatermark();
$form->AppendColumn($c);

$c = new CColumnImage("thumb", "Thumbnail");
$c->setDimensions(220, 165);
$c->setWatermark();
$c->setPath("stripclub/revues/t");
$form->AppendColumn($c);

$c = new CColumnPicture("stripclubrevue_picture", "Extra Pictures", "stripclubrevue");
$c->setMaxDimensions(700, 500);
$c->setDimensionsT(220, 165);
$c->setPath("stripclub/revues");
$c->setPathT("stripclub/revues/t");
$form->AppendColumn($c);

$c = new CColumnNumber("loc_lat", "Latitude");
$form->AppendColumn($c);

$c = new CColumnNumber("loc_long", "Longitude");
$form->AppendColumn($c);

$c = new CColumnenum("edit", "Status");
$c->AddEnumOption("-1", "Requested to be removed");
$c->AddEnumOption("0", "New Added");
$c->AddEnumOption("1", "Live/Worker Mode");
$c->AddEnumOption("5", "Done");
$c->setDefaultValue(5);
$form->AppendColumn($c);

$form->itemLink = "/male-revues/revue";
$form->show_remove = true;
$form->ShowPage();

?>

