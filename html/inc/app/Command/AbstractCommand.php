<?php

namespace App\Command;

abstract class AbstractCommand {

	/** @var array */
	protected $arguments;

	/**
	 * AbstractCommand constructor.
	 *
	 * @param array $arguments
	 */
	public function __construct(array $arguments) {
		$this->arguments = $arguments;
	}

	/**
	 * @return void
	 */
	abstract public function execute();

	/**
	 * @param mixed $output
	 *
	 * @return void
	 */
	protected function writelnAndLog($output) {
		$this->writeln($output);

		file_log($this->getLogFileName(), $output);
	}

	/**
	 * @param mixed $output
	 */
	protected function writeln($output) {
		$output = date('H:i:s')." - $output - ".PHP_EOL;
		print($output);
		flush();
		ob_flush();
	}

	/**
	 * @return string
	 */
	abstract protected function getLogFileName();
}