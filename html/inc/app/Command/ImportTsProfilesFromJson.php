<?php

namespace App\Command;

use App\Util\Image;
use App\Util\TS\Importer\Profile;
use App\Util\TS\Mapper\ProfileMapper;
use Exception;

/**
 *
 *  This command will be used to import profiles from TS
 *  with all the necessary info about them.
 *
 *  You should call it like this:
 *
 *  php bin/console.php import-ts-profiles-from-json <path-to-json-with-profiles>
 *
 */
class ImportTsProfilesFromJson extends AbstractCommand {

	const IMAGES_CLASSIFIEDS_PATH = 'img_html/classifieds';

	/** @var \App\Util\TS\Importer\Profile */
	private $profileImporter;

	/**
	 * ImportTsProfilesFromJson constructor.
	 *
	 * @param array $arguments
	 */
	public function __construct($arguments) {
		parent::__construct($arguments);
		$this->profileImporter = new Profile();
	}

	/**
	 * @return void
	 */
	public function execute() {
		$profilesData = $this->getProfilesData();
		$this->writelnAndLog('Json successfully parsed to array');

		$this->createNecessaryDirectoriesIfNotExist();

		foreach ($profilesData as $profileData) {
			$tsProfileAdapter = ProfileMapper::map($profileData);

			try {
				$this->profileImporter->importProfileToClassified($tsProfileAdapter);
			} catch (Exception $exception) {
				$this->writelnAndLog($exception->getMessage());
			}

			$this->writelnAndLog("Profile {$tsProfileAdapter->getPhone()} imported successfully!");
		}
	}

	/**
	 * @return string
	 */
	protected function getLogFileName() {
		return 'import_ts_profiles_from_json';
	}

	/**
	 * @return array
	 */
	private function getProfilesData() {
		$filePath = $this->getFilePathIfExists();

		return json_decode(file_get_contents($filePath), true);
	}

	/**
	 * @return string
	 */
	private function getFilePathIfExists() {
		if (!isset($this->arguments[0])) {
			$this->writelnAndLog('You should pass json file path!');
			exit;
		}

		$filePath = $this->arguments[0];

		if (!file_exists($filePath)) {
			$this->writelnAndLog("There is no such file: {$filePath}");
			exit;
		}

		return $this->arguments[0];
	}

	private function createNecessaryDirectoriesIfNotExist() {
		$temporaryPath         = Image::DIRECTORY_TEMPORARY;
		$classifiedsImagesPath = self::IMAGES_CLASSIFIEDS_PATH;

		$this->createDirectoryIfNotExists($temporaryPath);
		$this->createDirectoryIfNotExists($classifiedsImagesPath);
	}

	/**
	 * @param string $path
	 */
	private function createDirectoryIfNotExists($path) {
		if (!file_exists($path)) {
			!is_dir($path) && !mkdir($path) && !is_dir($path);
		}
	}
}