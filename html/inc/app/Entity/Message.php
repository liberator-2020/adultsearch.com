<?php

namespace App\Entity;

class Message {

	/** @var string */
	private $toNumber;

	/** @var string */
	private $text;

	/** @var bool */
	private $important = false;

	/**
	 * @return string
	 */
	public function getToNumber() {
		return $this->toNumber;
	}

	/**
	 * @return string
	 */
	public function getText() {
		return $this->text;
	}

	/**
	 * @param bool $important
	 */
	public function setImportant($important) {
		$this->important = $important;
	}

	/**
	 * @param string $toNumber
	 */
	public function setToNumber($toNumber) {
		$this->toNumber = $toNumber;
	}

	/**
	 * @param string $text
	 */
	public function setText($text) {
		$this->text = $text;
	}

	/**
	 * @return bool
	 */
	public function isImportant() {
		return $this->important;
	}
}