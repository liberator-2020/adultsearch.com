<?php

namespace App\Repository;

use db;

abstract class AbstractRepository {

	/**
	 * @var db
	 */
	private $db;

	public function __construct() {
		global $db;

		$this->db = $db;
	}

	/**
	 * @return db
	 */
	protected function db() {
		return $this->db;
	}
}
