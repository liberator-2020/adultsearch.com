<?php

namespace App\Repository;

use App\Util\TS\Model\User;
use PDO;

class AccountRepository extends AbstractRepository {

	/**
	 * @param int[] $ids
	 * @return array
	 */
	public function loadByIds(array $ids) {
		return $this->db()->q('SELECT * FROM `account` WHERE `account_id` IN ?', [$ids])->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param int $id
	 * @return array
	 */
	public function loadById($id) {
		$result = $this->db()->q('SELECT * FROM `account` WHERE `account_id` = ? LIMIT 1;', [$id])->fetch(\PDO::FETCH_ASSOC);
		return is_array($result) ? $result : [];
	}

	/**
	 * @param string $phone
	 *
	 * @return mixed
	 */
	public function findByPhoneId($phone) {
		$query = 'SELECT account_id as id FROM account WHERE phone = ?';

		return $this->db()->q($query, [$phone])->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * @param string $email
	 *
	 * @return mixed
	 */
	public function findByEmailId($email) {
		$query = 'SELECT account_id as id FROM account WHERE email = ?';

		return $this->db()->q($query, [$email])->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * @param \App\Util\TS\Model\User $tsUser
	 */
	public function createByTsUser(User $tsUser) {
		$query = 'INSERT INTO account
				  SET username       = ?,
				  	  created_stamp  = ?,
				  	  password       = ?,
				  	  email          = ?,
				  	  phone          = ?,
				  	  phone_verified = 1';

		$this->db()->q($query, [
			$tsUser->getUname(),
			$tsUser->getCreatedStamp(),
			'',
			$tsUser->getEmail() ?: $tsUser->getEmailRegister(),
			$tsUser->getPhone(),
		]);
	}

	/**
	 * @param \App\Util\TS\Model\User $tsUser
	 */
	public function updateByPhoneAccountWithTsUser(User $tsUser) {
		$query = 'UPDATE account
				  SET banned     = ?,
				      ban_reason = ?      
				  WHERE phone    = ?';

		$this->db()->q($query, [$tsUser->isBanned() ?: 0, $tsUser->getBanReason(), $tsUser->getPhone()]);
	}
}
