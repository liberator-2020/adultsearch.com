<?php

namespace App\Repository;

use PDO;
use App\Service\Classified\WaitingList;

class ClassifiedRepository extends AbstractRepository {

	/**
	 * @param int $accountId
	 * @return array
	 */
	public function loadWaitingListByAccountId($accountId) {
		$query = "SELECT * 
				  FROM `classified_waiting_list` 
				  WHERE `account_id` = ? 
				  AND `status_id` IN ?;";

		$params = [
			$accountId,
			[
				WaitingList::STATUS_PENDING,
				WaitingList::STATUS_RESERVED,
			]
		];
		return $this->db()->q($query, $params)->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * @param int $stickyCountLimit
	 * @return array
	 */
	public function loadWaitingListLocationsWhereStickyCountLessLimit($stickyCountLimit) {
		$query = "SELECT cswl.classified_type_id, cswl.location_id, COUNT(DISTINCT cs.id) as sticky_count
				  FROM `classified_waiting_list` AS cswl
				  LEFT JOIN classified_sticky AS cs 
				  ON cs.loc_id = cswl.location_id 
				  AND cswl.classified_type_id = cs.type
				  AND cs.done = '1'
				  AND cswl.status_id IN ?  
				  GROUP BY cswl.classified_type_id, cswl.location_id
				  HAVING sticky_count < ?;";

		$values = [
			[
				WaitingList::STATUS_PENDING,
				WaitingList::STATUS_RESERVED,
			],
			$stickyCountLimit,
		];
		return $this->db()->q($query, $values)->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * @param int $accountId
	 * @param int $locationId
	 * @param int $classifiedTypeId
	 * @param int $statusId
	 * @return void
	 */
	public function updateWaitingListStatusByAccountAndLocation($accountId, $locationId, $classifiedTypeId, $statusId) {
		$query = "UPDATE `classified_waiting_list` 
					 SET `status_id`          = ?, 
					 	 `updated_stamp`      = ? 
				   WHERE `account_id`         = ? 
					 AND `location_id`        = ? 
					 AND `classified_type_id` = ?;";

		$this->db()->q($query, [
			$statusId,
			time(),
			$accountId,
			$locationId,
			$classifiedTypeId,
		]);
	}

	/**
	 * @param int   $accountId
	 * @param int   $locationId
	 * @param int   $classifiedTypeId
	 * @param array $statusIds
	 * @return bool
	 */
	public function isExistsWaitingListWithParamsAndStatusIds($accountId, $locationId, $classifiedTypeId, array $statusIds) {
		$query = "SELECT `id` 
				  FROM `classified_waiting_list` 
				  WHERE `account_id`       = ? 
				  AND `location_id`        = ? 
				  AND `classified_type_id` = ? 
				  AND `status_id` IN ? 
				  LIMIT 1;";

		$result = $this->db()->q($query, [$accountId, $locationId, $classifiedTypeId, $statusIds])->fetch(PDO::FETCH_ASSOC);
		return is_array($result) && count($result) > 0;
	}

	/**
	 * @param int  $statusId
	 * @param int  $locationId
	 * @param int  $typeId
	 * @param bool $isDesc
	 * @return array
	 */
	public function loadWaitingListByStatusAndLocationAndType($statusId, $locationId, $typeId, $isDesc = true) {
		$order = $isDesc ? 'DESC' : 'ASC';
		$query = "
			SELECT * 
			FROM `classified_waiting_list` 
			WHERE `status_id`      = ? 
			AND location_id        = ? 
			AND classified_type_id = ? 
			ORDER BY `id` {$order} 
			LIMIT 1;";

		$result = $this->db()->q($query, [$statusId, $locationId, $typeId])->fetch(\PDO::FETCH_ASSOC);
		return is_array($result) ? $result : [];
	}

	/**
	 * @param int $statusId
	 * @param int $locationId
	 * @param int $typeId
	 * @return array
	 */
	public function loadWaitingListsByParam($statusId, $locationId, $typeId) {
		$query = "
			SELECT * 
			FROM `classified_waiting_list` 
			WHERE `status_id`      = ? 
			AND location_id        = ? 
			AND classified_type_id = ?";

		return $this->db()->q($query, [$statusId, $locationId, $typeId])->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param int $accountId
	 * @param int $locationId
	 * @param int $statusId
	 * @param int $classifiedTypeId
	 * @return void
	 */
	public function insertWaitingList($accountId, $locationId, $statusId, $classifiedTypeId) {
		$query = "INSERT INTO `classified_waiting_list` 
				  SET `account_id`         = ?,
					  `location_id`        = ?, 
					  `status_id`          = ?,
					  `classified_type_id` = ?,
					  `created_stamp`      = ?,
					  `updated_stamp`      = ?;";

		$currentTimestamp = time(); // @todo :: Replace to the Date Utils and use TIMESTAMP
		$this->db()->q($query, [$accountId, $locationId, $statusId, $classifiedTypeId, $currentTimestamp, $currentTimestamp]);
	}

	/**
	 * @param int[] $ids
	 * @param int   $statusId
	 * @return void
	 */
	public function updateWaitingListsStatus(array $ids, $statusId) {
		$query = 'UPDATE `classified_waiting_list` 
				  SET `status_id`     = ?, 
				  	  `updated_stamp` = ? 
				  WHERE `id` IN ?;';

		$this->db()->q($query, [
			$statusId,
			time(), // @todo :: Replace to the Date Utils and use TIMESTAMP
			$ids,
		]);
	}
}
