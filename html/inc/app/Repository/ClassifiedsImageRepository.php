<?php

namespace App\Repository;

class ClassifiedsImageRepository extends AbstractRepository {

	/**
	 * @param string $image
	 * @param int    $classifiedId
	 * @param string $currentImagePath
	 */
	public function createImage($image, $classifiedId, $currentImagePath) {
		list($w, $h) = getimagesize("{$currentImagePath}/classifieds/$image");

		$x = 'w';
		if ($h > $w) {
			$x = 'h';
		}

		$query = 'INSERT INTO classifieds_image 
				  SET id   = ?,
				  filename = ?,
				  x        = ?,
				  width    = ?,
				  height   = ?;';

		$this->db()->q($query, [
			$classifiedId,
			$image,
			$x,
			$w,
			$h,
		]);
	}
}