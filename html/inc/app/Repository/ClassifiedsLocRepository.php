<?php

namespace App\Repository;

use PDO;

class ClassifiedsLocRepository extends AbstractRepository {
	/**
	 * @param int $classifiedId
	 *
	 * @return mixed
	 */
	public function findByClassifiedId($classifiedId) {
		$query = 'SELECT id FROM classifieds_loc WHERE post_id = ?';

		return $this->db()->q($query, [$classifiedId])->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * @param int $countryId
	 * @param int $cityId
	 * @param int $type
	 * @param int $classifiedId
	 * @param int $now
	 */
	public function createByLocationAndClassifiedId($countryId, $cityId, $type, $classifiedId, $now) {
		$query = 'INSERT INTO classifieds_loc 
				  SET state_id = ?,
				  loc_id = ?,
			      type = ?,
				  post_id = ?,
				  updated = ?,
				  done = ?';

		$this->db()->q($query, [
			$countryId,
			$cityId,
			$type,
			$classifiedId,
			date('Y-m-d H:i:s', $now),
			-1,
		]);
	}

	/**
	 * @param int $classifiedLocId
	 */
	public function publishById($classifiedLocId) {
		$query = 'UPDATE classifieds_loc
				  SET done = 1
				  WHERE id = ?';

		$this->db()->q($query, [$classifiedLocId]);
	}
}