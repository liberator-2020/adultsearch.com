<?php

namespace App\Repository;

use App\Util\TS\Adapter\Profile;
use PDO;

class ClassifiedsRepository extends AbstractRepository {

	/**
	 * @param string $phone
	 * @param int    $locationId
	 *
	 * @return mixed
	 */
	public function findByPhoneAndLocationId($phone, $locationId) {
		$query = 'SELECT id FROM classifieds WHERE phone = ? AND loc_id = ?';

		return $this->db()->q($query, [$phone, $locationId])->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * @param \App\Util\TS\Adapter\Profile $tsProfile
	 * @param int                          $countryId
	 * @param int                          $cityId
	 * @param int                          $accountId
	 * @param string                       $cityName
	 * @param int                          $totalLocations
	 * @param int                          $now
	 */
	public function createClassifiedsByTsProfile(
		Profile $tsProfile,
		$countryId,
		$cityId,
		$accountId,
		$cityName,
		$totalLocations,
		$now
	) {
		$query = 'INSERT INTO classifieds
				  SET state_id     = ?,
				      type         = ?,
				      loc_id       = ?,
				      loc_name     = ?,
				      account_id   = ?,
				      done         = ?, 
					  expires      = ?,
					  expire_stamp = ?,
					  email        = ?,
					  phone        = ?,
					  title        = ?,   
					  date         = ?,
					  content      = ?,
					  incall       = ?,
					  outcall      = ?,
					  age          = ?,
					  total_loc    = ?,
					  firstname    = ?,      
					  avl_men      = ?, 
					  avl_women    = ?,
					  avl_couple   = ?,
					  created      = ?,
					  api          = ?;';

		$this->db()->q($query, [
			$countryId,
			2,
			$cityId,
			$cityName,
			$accountId,
			-1,
			date('Y-m-d H:i:s', $tsProfile->getExpireStamp()),
			$tsProfile->getExpireStamp(),
			$tsProfile->getEmail(),
			$tsProfile->getPhone(),
			$tsProfile->getTagline(),
			date('Y-m-d H:i:s', $now),
			$tsProfile->getAboutme(),
			$tsProfile->isIncall() ?: 0,
			$tsProfile->isOutcall() ?: 0,
			$tsProfile->getAge(),
			$totalLocations,
			$tsProfile->getName(),
			$tsProfile->isMen() ?: 0,
			$tsProfile->isWomen() ?: 0,
			$tsProfile->isCouples() ?: 0,
			$now,
			0,
		]);
	}

	/**
	 * @param \App\Util\TS\Adapter\Profile $tsProfile
	 * @param int                          $classifiedId
	 */
	public function updateByIdWithTsProfile(Profile $tsProfile, $classifiedId) {
		$query = 'UPDATE classifieds 
				  SET incall_rate_hh    = ?,
				      incall_rate_h     = ?,
				      incall_rate_2h    = ?,
				      incall_rate_day   = ?,
				      outcall_rate_hh   = ?,
				      outcall_rate_h    = ?,
				      outcall_rate_2h   = ?,
				      outcall_rate_day  = ?,
				      visiting          = ?,
				      visiting_from     = ?,
				      visiting_to       = ?,
				      location          = ?,
				      gfe               = ?,
				      fetish_dominant   = ?,
				      fetish_submissive = ?,
				      fetish_swith      = ?,
				      ethnicity         = ?,
				      language          = ?,
				      height_feet       = ?,
				      height_inches     = ?,
				      weight            = ?,
				      eyecolor          = ?,
				      haircolor         = ?,
				      build             = ?,
				      measure_1         = ?,
				      measure_2         = ?,
				      measure_3         = ?,
				      cupsize           = ?,
				      penis_size        = ?,
				      kitty             = ?,
				      pornstar          = ?,
				      pregnant          = ?,
				      website           = ?,
				      twitter           = ?,
				      facebook          = ?,
				      instagram         = ?,
				      payment_visa      = ?,
				      payment_amex      = ?,
				      payment_dis       = ?,
				      ter               = ?
				  WHERE id = ?';


		$heightCmData = $tsProfile->getHeightCm();

		$this->db()->q($query, [
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			null,
			null,
			null,
			$tsProfile->getGfe() ?: 0,
			0,
			0,
			0,
			$tsProfile->getEthnicity(),
			null,
			$heightCmData['height_feet'] ?: 0,
			$heightCmData['height_inches'] ?: 0,
			$tsProfile->getWeightKg() ?: 0,
			$tsProfile->getEyeColor() ?: 0,
			$tsProfile->getHairColor() ?: 0,
			$tsProfile->getBodyType() ?: 0,
			$tsProfile->getBustInch() ?: 0,
			$tsProfile->getWaistInch() ?: 0,
			$tsProfile->getHipsInch() ?: 0,
			$tsProfile->getCup() ?: 0,
			$tsProfile->getPenisSize() ?: 0,
			0,
			$tsProfile->isPornstar() ?: 0,
			0,
			$tsProfile->getWebsite(),
			$tsProfile->getTwitter(),
			$tsProfile->getFacebook(),
			$tsProfile->getInstagram(),
			0,
			0,
			0,
			$tsProfile->getTer(),
			$classifiedId,
		]);
	}

	/**
	 * @param string $thumbnailPath
	 * @param int    $classifiedId
	 */
	public function updateThumbnail($thumbnailPath, $classifiedId) {
		$query = 'UPDATE classifieds
		          SET thumb = ? 
		          WHERE id = ?';

		$this->db()->q($query, [
			$thumbnailPath,
			$classifiedId,
		]);
	}

	/**
	 * @param int $classifiedId
	 */
	public function publishById($classifiedId) {
		$query = 'UPDATE classifieds
				  SET done = 1
				  WHERE id = ?';

		$this->db()->q($query, [$classifiedId]);
	}
}