<?php

namespace App\Repository;

use account;
use App\Util\NumberVerifier\PhoneInfo;

class ContactRepository extends AbstractRepository {

	/**
	 * @param \account                           $account
	 * @param \App\Util\NumberVerifier\PhoneInfo $phoneInfo
	 * @param string                             $text
	 * @param string                             $url
	 * @param string                             $agent
	 */
	public function insertContactMessage(account $account, PhoneInfo $phoneInfo, $text, $url, $agent) {
		$query = "INSERT INTO `contact`
				  SET `account_id` = ?,
				      `email`      = ?,
				      `ip_address` = ?,
				      `stamp`      = ?,
				      `phone`      = ?,
				      `text`       = ?,
				      `name`       = ?,
				      `url`        = ?,
				      `agent`      = ?;";

		$this->db()->q($query, [
			$account->getId(),
			$account->getEmail(),
			account::getUserIp(),
			time(),
			$phoneInfo->getPhone(),
			$text,
			$account->getUsername(),
			$url,
			$agent,
		]);
	}
}