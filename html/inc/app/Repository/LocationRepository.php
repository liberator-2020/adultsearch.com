<?php

namespace App\Repository;

use App\Util\TS\Model\Location;
use PDO;

class LocationRepository extends AbstractRepository {

	/**
	 * @param int[] $ids
	 * @return array
	 */
	public function loadByIds(array $ids) {
		return $this->db()->q('SELECT * FROM `location_location` WHERE `loc_id` IN ?', [$ids])
			->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * @param string $name
	 *
	 * @return mixed
	 */
	public function findByName($name) {
		$query = 'SELECT loc_id as id, loc_name as name FROM location_location WHERE loc_name LIKE ?';

		return $this->db()->q($query, ["%{$name}%"])->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * @param string $name
	 * @param int    $parentId
	 *
	 * @return mixed
	 */
	public function findByNameAndParentId($name, $parentId) {
		$query = 'SELECT loc_id as id, loc_name as name FROM location_location WHERE loc_name LIKE ? AND loc_parent = ?';

		return $this->db()->q($query, ["%{$name}%", $parentId])->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * @param \App\Util\TS\Model\Location $city
	 * @param int                         $parentId
	 */
	public function createByTsLocationAndParentId(Location $city, $parentId) {
		$query = "INSERT INTO location_location
				  SET loc_type        = ?,
				      loc_parent      = ?,
				      country_id      = ?,
				      country_sub     = ?,
				      loc_name        = ?,
				      loc_url         = ?,
				      dir             = ?,
				      loc_lat         = ?,
				      loc_long        = ?,
				      has_place_or_ad = 1,
				      description     = ?,
				      s               = '';
				      ";

		$this->db()->q($query, [
			$city->getType(),
			$parentId,
			$parentId,
			strtolower($city->getParent()->getName()),
			$city->getName(),
			"/{$city->getUrl()}/",
			$city->getUrl(),
			$city->getLat(),
			$city->getLng(),
			$city->getDescription(),
		]);
	}
}
