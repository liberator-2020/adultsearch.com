<?php


namespace App\Repository;

use PDO;

class PhoneVerificationRepository extends AbstractRepository {

	/**
	 * @param int $timeout
	 */
	public function deleteExpiredRecords($timeout) {
		global $db;
		
		$db->q("DELETE FROM phone_verification WHERE verified=0 AND ( attempts<=0 OR created_dts < ?)", [$timeout]);
	}

	/**
	 * @param int	$accountId
	 * @param int	$classifiedId
	 * @param string $phone
	 */
	public function deleteAccountUnverifiedRecords($accountId, $classifiedId, $phone) {
		global $db;

		if ($accountId)
			$db->q("DELETE FROM phone_verification WHERE verified = 0 AND account_id = ? AND phone = ?", [$accountId, $phone]);

		if ($classifiedId)
			$db->q("DELETE FROM phone_verification WHERE verified = 0 AND classified_id = ? AND phone = ?", [$classifiedId, $phone]);
	}

	/**
	 * @param int	$accountId
	 * @param string $phone
	 * @param int	$smsCode
	 * @param int	$maxAttempts
	 * @param int	$now
	 */
	public function insertAccountVerificationCode($accountId, $classifiedId, $phone, $smsCode, $maxAttempts, $now) {
		global $db;

		$db->q("INSERT IGNORE INTO phone_verification
				SET verified	  = 0,
					account_id    = ?,
					classified_id = ?,
				  	phone         = ?,
				  	sms_code	  = ?, 
				  	attempts	  = ?, 
				  	created_dts   = ?",
				[$accountId, $classifiedId, $phone, $smsCode, $maxAttempts, $now]
				);
	}

	/**
	 * @param string $phoneInfo
	 * @param int	$accountId
	 * @param string $phone
	 *
	 * @return void
	 */
	public function updatePhoneVerificationByAccountIdAndPhone($phoneInfo, $accountId, $phone) {
		$query = 'UPDATE phone_verification
				  SET numverify		 = ? 
				  WHERE account_id	  = ? 
				  AND phone			 = ?;';

		$this->db()->q($query, [$phoneInfo, $accountId, $phone]);
	}
}
