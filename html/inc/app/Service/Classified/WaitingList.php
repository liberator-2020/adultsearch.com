<?php

namespace App\Service\Classified;

use App\Service\UserNotification;
use App\Service\AbstractBasic;
use App\Repository\LocationRepository;
use App\Repository\AccountRepository;
use App\Repository\ClassifiedRepository;
use classifieds;
use Exception;

/**
 * When advertisers can't buy sticky upgrade because in the city has no free slot they will by sign up for waiting list
 * (They will by added to list  with status PENDING), whenever the some slot in that city becomes free,
 * the first advertiser from waiting list for that city is notified (Will be set RESERVED status)
 * If the notified advertiser (With RESERVED status) won't purchase the sticky in 24 hours, all other advertisers in the
 * list will be notified. (All users with PENDING status)
 */
class WaitingList extends AbstractBasic {

	/**
	 * Is set this status when user added to location waiting list
	 */
	CONST STATUS_PENDING  = 1;

	/**
	 * Is set when we send notification to first user in the waiting list and other users is waiting
	 */
	CONST STATUS_RESERVED = 2;

	/**
	 * Is set after we notified user
	 */
	CONST STATUS_NOTIFIED = 3;

	/**
	 * If user buyed released slot before we processing waiting list
	 */
	CONST STATUS_CANCELED = 4;

	/**
	 * Is set when throw error in list processing or sending notification
	 */
	CONST STATUS_ERROR    = 5;

	/**
	 * Waiting List Processing Interval
	 */
	CONST RESERVATION_PERIOD = 84000; // 24 Hours

	/**
	 * @var \App\Service\UserNotification
	 */
	private $userNotificationService;

	/**
	 * @var \App\Repository\LocationRepository
	 */
	private $locationRepository;

	/**
	 * @var \App\Repository\AccountRepository
	 */
	private $accountRepository;

	/**
	 * @var \App\Repository\ClassifiedRepository
	 */
	private $classifiedRepository;

	public function __construct() {
		$this->userNotificationService = new UserNotification();
		$this->locationRepository      = new LocationRepository();
		$this->accountRepository       = new AccountRepository();
		$this->classifiedRepository    = new ClassifiedRepository();
	}

	/**
	 * Get IDs of locations the user is waiting
	 *
	 * @param int $accountId
	 * @return int[]
	 */
	public function getActualAwaitingLocationsIdsByAccountId($accountId) {
		$classifiedWaitingLists = $this->classifiedRepository->loadWaitingListByAccountId($accountId);

		return array_map(function (array $classifiedWaitingList) {
			return $classifiedWaitingList['location_id'];
		}, $classifiedWaitingLists);
	}

	/**
	 * Add user to location sticky waiting list
	 *
	 * @param int $accountId
	 * @param int $locationId
	 * @param int $typeId
	 * @return void
	 */
	public function add($accountId, $locationId, $typeId) {
		$statusIds = [
			self::STATUS_PENDING,
			self::STATUS_RESERVED,
		];
		if ($this->classifiedRepository->isExistsWaitingListWithParamsAndStatusIds($accountId, $locationId,
			$typeId, $statusIds)
		) {
			return;
		}

		$this->log("Add account #{$accountId}, location #{$locationId}, type #{$typeId}");
		$this->classifiedRepository->insertWaitingList($accountId, $locationId, self::STATUS_PENDING, $typeId);
	}

	/**
	 * Remove user from location sticky waiting list
	 *
	 * @param int $accountId
	 * @param int $locationId
	 * @param int $typeId
	 * @return void
	 */
	public function cancel($accountId, $locationId, $typeId) {
		$this->log("Remove account #{$accountId}, location #{$locationId}, type #{$typeId} (Bought an upgrade)");
		$this->classifiedRepository->updateWaitingListStatusByAccountAndLocation($accountId, $locationId, $typeId, self::STATUS_CANCELED);
	}

	/**
	 * Process waiting list and notify user about released sticky slot on locations (By SMS and Email)
	 *
	 * @return void
	 */
	public function processWaitingList() {
		$this->log('Start waiting list processing');

		$freeStickyLocations = $this->loadFreeLocations();
		$locationsInfoById   = $this->loadFreeLocationsInfo($freeStickyLocations);

		$this->log('Loaded ' . count($freeStickyLocations) . ' free sticky locations');
		foreach ($freeStickyLocations as $freeStickyLocation) {
			$locationId       = $freeStickyLocation['location_id'];
			$classifiedTypeId = $freeStickyLocation['classified_type_id'];

			$this->processLocation($locationsInfoById[$locationId], $classifiedTypeId);
		}

		$this->log('End waiting list processing');
	}

	/**
	 * @return array
	 */
	private function loadFreeLocations() {
		return $this->classifiedRepository->loadWaitingListLocationsWhereStickyCountLessLimit(
			classifieds::$sticky_count_limit
		);
	}

	/**
	 * @param array $freeStickyLocations
	 * @return array
	 */
	private function loadFreeLocationsInfo(array $freeStickyLocations) {
		if (count($freeStickyLocations) === 0) {
			return [];
		}

		$locationsIds = array_map(function (array $freeStickyLocation) {
			return $freeStickyLocation['location_id'];
		}, $freeStickyLocations);

		$result = [];
		$locationsData = $this->locationRepository->loadByIds($locationsIds);
		foreach ($locationsData as $locationData) {
			$result[$locationData['loc_id']] = $locationData;
		}
		return $result;
	}

	/**
	 * @param array $locationData
	 * @param int   $classifiedTypeId
	 * @return void
	 */
	private function processLocation(array $locationData, $classifiedTypeId) {
		$this->log("Start #{$locationData['loc_id']} {$locationData['loc_name']}, type #{$classifiedTypeId} location processing");

		$stickyWaitingListReservation = $this->findStickyReservation($locationData, $classifiedTypeId);

		$isExistsReservation  = count($stickyWaitingListReservation) > 0;
		$isReservationExpired = $this->isReservationExpired($stickyWaitingListReservation);

		if (!$isExistsReservation /** if reservation is not exists - set reservation */) {
			$this->setReservationAndNotify($locationData, $classifiedTypeId);
		} else if ($isExistsReservation && $isReservationExpired) {
			$this->unsetReservation($stickyWaitingListReservation);
			$this->notifyAllAccounts($locationData, $classifiedTypeId);
		}

		$this->log("End #{$locationData['loc_id']} {$locationData['loc_name']}, type #{$classifiedTypeId} location processing");
	}

	/**
	 * @param array $locationData
	 * @param int   $classifiedTypeId
	 * @return array
	 */
	private function findStickyReservation(array $locationData, $classifiedTypeId) {
		return $this->classifiedRepository->loadWaitingListByStatusAndLocationAndType(
			self::STATUS_RESERVED,
			$locationData['loc_id'],
			$classifiedTypeId
		);
	}

	/**
	 * @param array $stickyWaitingList
	 * @return bool
	 */
	private function isReservationExpired(array $stickyWaitingList) {
		if (!array_key_exists('updated_stamp', $stickyWaitingList) || $stickyWaitingList['updated_stamp'] < 1) {
			return false;
		}

		// @todo :: Use DateTime and DateUtils
		$lastUpdateTimestamp = $stickyWaitingList['updated_stamp'];
		$deadlineTimestamp   = ($lastUpdateTimestamp + self::RESERVATION_PERIOD);
		return (time() > $deadlineTimestamp);
	}

	/**
	 * @param array $locationData
	 * @param int   $classifiedTypeId
	 * @return void
	 */
	private function setReservationAndNotify(array $locationData, $classifiedTypeId) {
		$locationId   = $locationData['loc_id'];
		$locationName = $locationData['loc_name'];

		$stickyWaitingList = $this->classifiedRepository->loadWaitingListByStatusAndLocationAndType(
			self::STATUS_PENDING,
			$locationId,
			$classifiedTypeId,
			false
		);

		$accountData = $this->accountRepository->loadById($stickyWaitingList['account_id']);

		try {
			$this->log("Reservation and notify account #{$stickyWaitingList['account_id']}, location #{$locationData['loc_id']}, type #{$classifiedTypeId}");
			$this->notifyAboutSlotReservation($accountData, $locationName);

			$this->classifiedRepository->updateWaitingListsStatus([$stickyWaitingList['id']], self::STATUS_RESERVED);
		} catch (Exception $e) {
			file_log('api', "SMS notification is not sent and caught an exception: {$e->getMessage()}");

			$this->classifiedRepository->updateWaitingListsStatus([$stickyWaitingList['id']], self::STATUS_ERROR);
		}
	}

	/**
	 * @param array $stickyWaitingList
	 * @return void
	 */
	private function unsetReservation(array $stickyWaitingList) {
		$this->log("Unset reservation account #{$stickyWaitingList['account_id']}, location #{$stickyWaitingList['location_id']} type #{$stickyWaitingList['classified_type_id']}");
		$this->classifiedRepository->updateWaitingListsStatus([$stickyWaitingList['id']], self::STATUS_NOTIFIED);
	}

	/**
	 * @param array $locationData
	 * @param int   $classifiedTypeId
	 * @return void
	 */
	private function notifyAllAccounts(array $locationData, $classifiedTypeId) {
		$this->log("Start notification all accounts at location #{$locationData['loc_id']} {$locationData['loc_name']}, type #{$classifiedTypeId}");

		$stickyWaitingLists = $this->classifiedRepository->loadWaitingListsByParam(
			self::STATUS_PENDING,
			$locationData['loc_id'],
			$classifiedTypeId
		);
		$accountsDataById = $this->loadAccountsByStickyWaitingLists($stickyWaitingLists);
		$this->log('Loaded ' . count($accountsDataById) . ' accounts');

		$notifiedIds = [];
		$errorIds = [];
		foreach ($stickyWaitingLists as $index => $stickyWaitingList) {
			$stickyWaitingListId = $stickyWaitingList['id'];
			$accountId = $stickyWaitingList['account_id'];

			try {
				$this->log("Notify account #{$accountId}, location #{$locationData['loc_id']}, type #{$classifiedTypeId}");

				$accountData = $accountsDataById[$accountId];
				$this->notifyAboutAvailableSlot($accountData, $locationData['loc_name']);
				$notifiedIds[] = $stickyWaitingListId;
			} catch (Exception $e) {
				file_log('api', "SMS notification is not sent and caught an exception: {$e->getMessage()}");

				$errorIds[] = $stickyWaitingListId;
			}
		}

		if (count($notifiedIds) > 0) {
			$this->classifiedRepository->updateWaitingListsStatus($notifiedIds, self::STATUS_NOTIFIED);
			$this->log('Marked as successfully notified accounts: ' . count($notifiedIds));
		}
		if (count($errorIds) > 0) {
			$this->classifiedRepository->updateWaitingListsStatus($errorIds, self::STATUS_ERROR);
			$this->log('Errors when notification accounts: ' . count($notifiedIds));
		}

		$this->log("End notification all accounts at location #{$locationData['loc_id']} {$locationData['loc_name']}, type #{$classifiedTypeId}");
	}

	/**
	 * @param array $stickyWaitingLists
	 * @return array
	 */
	private function loadAccountsByStickyWaitingLists(array $stickyWaitingLists) {
		if (count($stickyWaitingLists) === 0) {
			return [];
		}

		$accountsIds = array_map(function (array $stickyWaitingList) {
			return $stickyWaitingList['account_id'];
		}, $stickyWaitingLists);

		$result = [];
		$accounts = $this->accountRepository->loadByIds($accountsIds);
		foreach ($accounts as $account) {
			$result[$account['account_id']] = $account;
		}
		return $result;
	}

	/**
	 * @param array  $accountData
	 * @param string $locationName
	 * @return void
	 */
	private function notifyAboutSlotReservation(array $accountData, $locationName) {
		if ($accountData['banned']) {
			$this->log("Account #{$accountData['account_id']} banned, skip");
			return;
		}

		if ($accountData['phone_verified']) {
			$this->userNotificationService->notifyAboutStickySlotReservationBySms(
				$accountData['phone'],
				$locationName
			);
		}

		if ($accountData['email_confirmed']) {
			$this->userNotificationService->notifyAboutStickySlotReservationByEmail(
				$accountData['email'],
				$locationName
			);
		}
	}

	/**
	 * @param array  $accountData
	 * @param string $locationName
	 * @return void
	 */
	private function notifyAboutAvailableSlot(array $accountData, $locationName) {
		if ($accountData['banned']) {
			$this->log("Account #{$accountData['account_id']} banned, skip");
			return;
		}

		if ($accountData['phone_verified']) {
			$this->userNotificationService->notifyAboutAvailableStickySlotBySms(
				$accountData['phone'],
				$locationName
			);
		}

		if ($accountData['email_confirmed']) {
			$this->userNotificationService->notifyAboutAvailableStickySlotByEmail(
				$accountData['email'],
				$locationName
			);
		}
	}

	/**
	 * @param string $message
	 */
	private function log($message) {
		file_log('waiting_list', $message);
	}
}
