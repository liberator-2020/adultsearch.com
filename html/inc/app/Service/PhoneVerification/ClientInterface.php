<?php

namespace App\Service\PhoneVerification;

interface ClientInterface {

	/**
	 * Validate given in string format number
	 *
	 * @param string $phoneNumber
	 *
	 * @return \App\Util\PhoneVerifier\PhoneInfo
	 * @throws \App\Exception\PhoneCarrierVerificationException
	 */
	public function getInfo($phoneNumber);

}