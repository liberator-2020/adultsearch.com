<?php

namespace App\Service\PhoneVerification;

use App\Service\AbstractBasic;
use App\Exception\PhoneCarrierVerificationException;
use App\Util\NumberVerifier\Numverify\PhoneInfoMapper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;

class NumverifyClient extends AbstractBasic implements ClientInterface {

	const API_URL = 'http://apilayer.net/api/';

	/** @var string */
	private $accessKey;

	/** @var \GuzzleHttp\Client */
	private $client;

	/**
	 * @param string $accessKey
	 */
	public function __construct($accessKey) {
		$this->accessKey = $accessKey;
		$this->client    = new Client([
			'base_uri' => self::API_URL,
			'timeout'  => 5,
		]);
	}

	/**
	 * Validate given in string format number
	 *
	 * @param string $phoneNumber
	 *
	 * @return \App\Util\NumberVerifier\PhoneInfo
	 * @throws \App\Exception\PhoneCarrierVerificationException
	 */
	public function getInfo($phoneNumber) {
		try {
			$responseJson = $this->client->get('validate', $this->getRequestOptions(['number' => $phoneNumber]));

			$response = json_decode($responseJson->getBody()->getContents(), true);

			$line    = isset($response['line_type']) ? $response['line_type'] : '-';
			$carrier = isset($response['carrier']) ? $response['carrier'] : '-';
			$error   = isset($response['error']) ? $response['error']['info'] : '-';

			$logMessage
				= "Numverify API validate: phone={$phoneNumber} response: line={$line}, carrier={$carrier}, error={$error}";


			$this->log($logMessage);

			if (isset($response['error'])) {
				$info = $response['error']['info'];
				$code = $response['error']['code'];
				$type = $response['error']['type'];
				throw new PhoneCarrierVerificationException("{$info}, code: {$code}, type: {$type}");
			}

			return PhoneInfoMapper::map($response);
		} catch (TransferException $exception) {
			throw new PhoneCarrierVerificationException($exception->getMessage(), $exception->getCode(), $exception);
		}
	}

	/**
	 * Generate get query data including access key
	 *
	 * @param array $params
	 *
	 * @return array
	 */
	private function getRequestQuery(array $params) {
		return array_merge([
			'access_key' => $this->accessKey,
		], $params);
	}

	/**
	 * Generate options for Guzzle get request
	 *
	 * @param array $queryData
	 *
	 * @return array
	 */
	private function getRequestOptions(array $queryData) {
		return [
			'query' => $this->getRequestQuery($queryData),
		];
	}

	/**
	 * Log numverify response to phoneCarrierVerification file
	 *
	 * @param string $message
	 */
	private function log($message) {
		file_log('phone_carrier_verification', $message);
	}

}