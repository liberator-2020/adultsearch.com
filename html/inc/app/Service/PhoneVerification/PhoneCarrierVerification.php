<?php

namespace App\Service\PhoneVerification;

use App\Exception\PhoneCarrierVerificationException;
use App\Repository\ContactRepository;
use App\Repository\PhoneVerificationRepository;
use App\Service\AbstractBasic;
use App\Util\NumberVerifier\PhoneInfo;
use account;

class PhoneCarrierVerification extends AbstractBasic {

	/** @var \App\Service\PhoneVerification\NumverifyClient */
	private $client;

	/** @var \App\Repository\ContactRepository */
	private $contactRepository;

	/** @var \App\Repository\PhoneVerificationRepository */
	private $phoneVerificationRepository;

	/**
	 * PhoneCarrierVerification constructor.
	 */
	public function __construct() {
		global $config_numverify_access_key;

		$this->client                      = new NumverifyClient($config_numverify_access_key);
		$this->contactRepository           = new ContactRepository();
		$this->phoneVerificationRepository = new PhoneVerificationRepository();
	}

	/**
	 * @param string   $phone
	 * @param \account $account
	 *
	 * @return void
	 */
	public function checkPhone($phone, account $account) {

		//2019-07-11 Thor: disable checking phone carriers for now
		return;

		try {
			$info = $this->client->getInfo($phone);
			$this->phoneVerificationRepository->updatePhoneVerificationByAccountIdAndPhone(
				$info->getJson(),
				$account->getId(),
				$phone
			);
		} catch (PhoneCarrierVerificationException $exception) {
			$this->log($exception->getMessage());

			$this->sendWarningToIMS(new PhoneInfo(false, $phone), $account);

			return;
		}

		if (!$info->isMobile() || !$info->hasCarrier()) {
			$this->sendWarningToIMS($info, $account);

			return;
		}

		if ($info->isAmerican() && !$this->isPhoneCarrierInAmericanCarriers($info->getCarrier())) {
			$this->sendWarningToIMS($info, $account);

			return;
		}

		$this->log("Account # {$account->getId()} phone is valid: \n {$info} \n");
	}

	/**
	 * @param \App\Util\NumberVerifier\PhoneInfo $info
	 * @param \account                           $account
	 */
	private function sendWarningToIMS(PhoneInfo $info, account $account) {
		$text = "This phone may be a spam one. Information: \n{$info}";

		$this->contactRepository->insertContactMessage($account, $info, $text, $this->getUrl(),
			$this->getHttpUserAgent());

		$this->log("Added new warning to IMS system about phone {$info->getPhone()} set by account #{$account->getId()}.");
	}

	/**
	 * @param string $message
	 */
	private function log($message) {
		file_log('phone_carrier_verification', $message);
	}

	/**
	 * @param string $phoneCarrier
	 *
	 * @return bool
	 */
	private function isPhoneCarrierInAmericanCarriers($phoneCarrier) {
		foreach ($this->getAllowedAmericanCarriersKeywords() as $keyword) {
			if (strpos($phoneCarrier, $keyword) !== false) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @return array
	 */
	private function getAllowedAmericanCarriersKeywords() {
		return [

			/*
			 * Verizon Wireless
			 */

			'Verizon',

			/*
			 * AT&T Mobility
			 */

			'AT&T',

			'T-Mobile',

			/*
			 * Sprint Corporation
			 */

			'Sprint',

			/*
			 * United States Cellular Corporation
			 * Cellular South
			 * Pine Belt Cellular Inc. (Pine Belt Wireless)
			 * Advantage Cellular Systems Inc. (DTC Wireless)
			 */

			'Cellular',
			'Cellcom',

			/*
			 * Southern Communications Services
			 */

			'Southern Communications',

			/*
			 * MetroPCS Communications Inc.
			 */

			'MetroPCS',

			/*
			 * Pine Belt Cellular Inc. (Pine Belt Wireless)
			 */

			'Pine',

			/*
			 * Pine Belt Cellular Inc. (Pine Belt Wireless)
			 */

			'Belt',

			/*
			 * TELUS Corp.
			 */

			'Telus',

			/*
			 * Bell Mobility Inc.
			 */

			'Bell',

			/*
			 * Buffalo - Lake Erie Wireless Systems Co., L.L.C.
			 */

			'Buffalo',

			/*
			 * Buffalo - Lake Erie Wireless Systems Co., L.L.C.
			 */

			'Lake Erie',

			/*
			 * Rogers Communications Partnership (RCP)
			 */

			'Rogers',
			/*
			 * Rogers Communications Partnership (RCP)
			 */

			'RCP',

			/*
			 * Advantage Cellular Systems Inc. (DTC Wireless)
			 */

			'DTC',
		];
	}

	/**
	 * @return bool|string
	 */
	private function getHttpUserAgent() {
		$httpUserAgent = $_SERVER['HTTP_USER_AGENT'];

		return substr($httpUserAgent, 0, 250);
	}

	/**
	 * @return bool|string
	 */
	private function getUrl() {
		$url = $_SERVER['HTTP_HOST'];

		return substr($url, 0, 180);
	}

}
