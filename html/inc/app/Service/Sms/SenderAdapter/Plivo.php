<?php

namespace App\Service\Sms\SenderAdapter;

use App\Service\AbstractBasic;
use App\Service\Sms\SenderAdapterInterface;
use App\Exception\SmsSendingException;
use Plivo\Exceptions\PlivoRestException;
use Plivo\RestClient;

class Plivo extends AbstractBasic implements SenderAdapterInterface {

	/** @var \Plivo\RestClient */
	private $client;

	/** @var string */
	private $fromNumber;

	/** @var string */
	private $deliveryStatusEndpoint;

	/**
	 * @param string $plivoAuthId
	 * @param string $plivoAuthToken
	 * @param string $fromNumber
	 * @param string $deliveryStatusEndpoint
	 */
	public function __construct($plivoAuthId, $plivoAuthToken, $fromNumber, $deliveryStatusEndpoint) {
		$this->client                 = new RestClient($plivoAuthId, $plivoAuthToken);
		$this->fromNumber             = $fromNumber;
		$this->deliveryStatusEndpoint = $deliveryStatusEndpoint;
	}

	/**
	 * @param string $text
	 * @param string $toNumber
	 *
	 * @return array
	 * @throws \App\Exception\SmsSendingException
	 */
	public function send($text, $toNumber) {
		try {
			$response = $this->client->messages->create(
				$this->fromNumber,
				[$toNumber],
				$text,
				['url' => $this->deliveryStatusEndpoint]
			);

			//$response is \Plivo\Resources\Message\MessageCreateResponse;
			$response = ['status_code' => $response->getStatusCode(), 'message_uuid' => $response->getMessageUuid()];

		} catch (PlivoRestException $exception) {
			$code    = $exception->getCode();
			$message = $exception->getMessage();

			throw new SmsSendingException($message, $code, $exception);
		}

		return $response;
	}
}
