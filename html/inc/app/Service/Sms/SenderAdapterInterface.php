<?php

namespace App\Service\Sms;

interface SenderAdapterInterface {

	/**
	 * @param string $text
	 * @param string $toNumber
	 * @return array
	 */
	public function send($text, $toNumber);
}