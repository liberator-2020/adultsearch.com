<?php

namespace App\Service\Sms;

use App\Entity\Message;
use App\Exception\SmsSendingException;
use App\Service\AbstractBasic;
use App\Service\Sms\SenderAdapter\Plivo;
use swiftsms;

class Sms extends AbstractBasic {

	const PROVIDER_NAME_PLIVO = 'Plivo';
	const PROVIDER_NAME_SWIFT = 'Swift';

	/** @var \App\Service\Sms\SenderAdapter\Plivo */
	private $plivoAdapter;

	/** @var \swiftsms */
	private $swiftAdapter;

	/** @var string */
	private $env;

	/**
	 * Sms constructor.
	 */
	public function __construct() {
		global $config_plivo_auth_id, $config_plivo_auth_token, $config_plivo_from_number, $config_plivo_delivery_status_endpoint, $config_env;

		$this->plivoAdapter = new Plivo(
			$config_plivo_auth_id,
			$config_plivo_auth_token,
			$config_plivo_from_number,
			$config_plivo_delivery_status_endpoint
		);

		$this->swiftAdapter = new swiftsms();
		$this->env          = $config_env;
	}

	/**
	 * @param \App\Entity\Message $message
	 *
	 * @return boolean
	 */
	public function send(Message $message) {
		if ($message->isImportant()) {
			return $this->sendImportantMessage($message);
		}

		return $this->sendSimpleMessage($message);
	}

	/**
	 * @param \App\Entity\Message $message
	 *
	 * @return boolean
	 */
	public function sendViaPlivo(Message $message) {
		if ($this->checkIfNotProduction()) {
			$this->sendEmailOnDevEnv($message, self::PROVIDER_NAME_PLIVO);

			return true;
		}

		$this->logMessageInfoBeforeSend($message, self::PROVIDER_NAME_PLIVO);
		try {
			$responseData = $this->plivoAdapter->send($message->getText(), $message->getToNumber());
		} catch (SmsSendingException $exception) {
			$responseData = [
				'status' => 'error',
				'code'   => $exception->getCode(),
				'error'  => $exception->getMessage(),
			];
		}
		$this->logResponse($responseData, self::PROVIDER_NAME_PLIVO);

		return !array_key_exists('error', $responseData);
	}

	/**
	 * @param \App\Entity\Message $message
	 *
	 * @return boolean
	 */
	public function sendViaSwift(Message $message) {
		if ($this->checkIfNotProduction()) {
			$this->sendEmailOnDevEnv($message, self::PROVIDER_NAME_SWIFT);

			return true;
		}

		$this->logMessageInfoBeforeSend($message, self::PROVIDER_NAME_SWIFT);

		$response = $this->swiftAdapter->send($message->getToNumber(), $message->getText());

		$this->logResponse($response, self::PROVIDER_NAME_SWIFT);

		return $response;
	}

	/**
	 * @param \App\Entity\Message $message
	 *
	 * @return boolean
	 */
	private function sendImportantMessage(Message $message) {
		$swiftResponse = $this->sendViaSwift($message);
		$plivoResult   = $this->sendViaPlivo($message);

		return $swiftResponse || $plivoResult;
	}

	/**
	 * @param \App\Entity\Message $message
	 *
	 * @return boolean
	 */
	private function sendSimpleMessage(Message $message) {
		return $this->sendViaSwift($message);
	}

	/**
	 * On DEV, TEST and UAT environments send email instead of SMS (that ends up in virtual mailbox)
	 *
	 * @param \App\Entity\Message $message
	 * @param string              $provider
	 *
	 * @return void
	 */
	private function sendEmailOnDevEnv(Message $message, $provider) {
		$phone          = $message->getToNumber();
		$messageContent = $message->getText();

		$text  = "Phone: {$phone}\nText: {$messageContent}\n";
		$error = null;
		send_email([
			'from'    => SUPPORT_EMAIL,
			'to'      => SUPPORT_EMAIL,
			'subject' => "SMS to {$phone}",
			'html'    => nl2br($text),
			'text'    => $text,
		], $error);

		$logText = "{$provider}: env={$this->env} -> SMS sent as email to ".SUPPORT_EMAIL
			.", text='{$text}', error='{$error}'";

		$this->log($logText);
	}

	/**
	 * @return bool
	 */
	private function checkIfNotProduction() {
		return in_array($this->env, ['dev', 'test']) || strpos($this->env, 'uat') === 0;
	}

	/**
	 * @param \App\Entity\Message $message
	 * @param string              $provider
	 */
	private function logMessageInfoBeforeSend(Message $message, $provider) {
		$this->log("{$provider}: phone='{$message->getToNumber()}' text='{$message->getText()}'");
	}

	/**
	 * @param mixed  $responseData
	 * @param string $provider
	 */
	private function logResponse($responseData, $provider) {
		$responseJson = json_encode($responseData);

		$this->log("{$provider}, Response: {$responseJson}");
	}

	/**
	 * @param string $message
	 */
	private function log($message) {
		file_log('sms', $message);
	}

}
