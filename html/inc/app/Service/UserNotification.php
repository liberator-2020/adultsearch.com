<?php

namespace App\Service;

use swiftsms;
use advertise;

class UserNotification extends AbstractBasic {

	/**
	 * @var \swiftsms
	 */
	private $swiftSMS;

	public function __construct() {
		$this->swiftSMS = new swiftsms();
	}

	/**
	 * Send notification of sticky slot reservation by email
	 *
	 * @param string $email
	 * @param string $locationName
	 * @return void
	 */
	public function notifyAboutStickySlotReservationByEmail($email, $locationName) {
		$this->temporaryDebugLog("Send email notification of sticky slot reservation at {$locationName} to {$email}");

		advertise::sendAgencyEmail(
			'classifieds/email/sticky_upgrade_reservation',
			['location_name' => $locationName],
			'Adultsearch Sticky Ad Upgrade - Availability now!',
			$email
		);
	}

	/**
	 * Send notification of available sticky slot at location by email
	 *
	 * @param string $email
	 * @param string $locationName
	 * @return void
	 */
	public function notifyAboutAvailableStickySlotByEmail($email, $locationName) {
		$this->temporaryDebugLog("Send email notification of available sticky slot at {$locationName} to {$email}");

		advertise::sendAgencyEmail(
			'classifieds/email/sticky_upgrade_available',
			['location_name' => $locationName],
			'Adultsearch Sticky Ad Upgrade - Availability now!',
			$email
		);
	}

	/**
	 * Send notification of available sticky slot at location by phone
	 *
	 * @param string $phone
	 * @param string $locationName
	 * @return void
	 */
	public function notifyAboutStickySlotReservationBySms($phone, $locationName) {
		$this->temporaryDebugLog("Send SMS notification of sticky slot reservation at {$locationName} to {$phone}");

		$message = "Adultsearch Sticky Ad for {$locationName} is available for you to purchase right now! " .
			"You have 24 hours to purchase this sticky. If you do not purchase this ad within 24 hours, " .
			"we will open this ad to the rest of the users on the website.";

		$this->swiftSMS->send($phone, $message);
	}

	/**
	 * Send notification of available sticky slot at location by phone
	 *
	 * @param string $phone
	 * @param string $locationName
	 * @return void
	 */
	public function notifyAboutAvailableStickySlotBySms($phone, $locationName) {
		$this->temporaryDebugLog("Send SMS notification of available sticky slot at {$locationName} to {$phone}");

		$message = "Adultsearch Sticky Ad for {$locationName} is available for you to purchase right now! " .
			"Hurry up and purchase now - it is very rare that these sticky ads become available!";

		$this->swiftSMS->send($phone, $message);
	}

	/**
	 * @param string $message
	 * @return void
	 */
	private function temporaryDebugLog($message) {
		file_log('notification', "UserNotification service. {$message}");
	}
}
