<?php

namespace App\Util;

use App\Util\TS\Model\Profile;
use system;

class Image {

	const DIRECTORY_TEMPORARY = _CMS_ABS_PATH.'/tmp/images';
	const TSESCORTS_PATH_THUMBNAIL = 'https://www.tsescorts.com/images/t/';
	const TSESCORTS_PATH_PROFILE_IMAGES = 'https://www.tsescorts.com/images/p/';

	/**
	 * @param \App\Util\TS\Model\Profile $tsProfile
	 * @param int                        $classifiedId
	 *
	 * @return bool|string|null
	 */
	public function saveThumbnailFromTsProfileToClassified(Profile $tsProfile, $classifiedId) {
		$tsThumbnailUrl = self::TSESCORTS_PATH_THUMBNAIL;

		if (!$tsProfile->getThumbnail()) {
			$this->log("There is no thumbnail for profile #{$tsProfile->getId()}!");

			return false;
		}

		$thumbnail = $this->downloadImageToTmpDir($tsThumbnailUrl.$tsProfile->getThumbnail(), $classifiedId);

		return $thumbnail;
	}

	/**
	 * @param \App\Util\TS\Model\Profile $tsProfile
	 * @param int                        $classifiedId
	 *
	 * @return array|bool
	 */
	public function saveImagesFromTsProfileToClassified(Profile $tsProfile, $classifiedId) {
		$tsProfileUrl = self::TSESCORTS_PATH_PROFILE_IMAGES;

		$images = [];

		$photos = $tsProfile->getTrueImages();

		if (empty($photos)) {
			$this->log("There is no images for profile #{$tsProfile->getId()}!");

			return false;
		}

		$i = 1;
		foreach ($photos as $photo) {
			$photoFileName = $photo->getFilename();
			if (!$photoFileName) {
				$this->log("There is no href for image #{$photo->getId()}!");
				continue;
			}

			$images[] = $this->downloadImageToTmpDir($tsProfileUrl.$photoFileName, $classifiedId, "p{$i}");
			$i++;
		}

		return $images;
	}

	/**
	 * @param string $thumbnailTmpPath
	 * @param int    $classifiedId
	 * @param string $currentImagePath
	 *
	 * @return bool|string
	 */
	public function moveTmpClassifiedThumbnail($thumbnailTmpPath, $classifiedId, $currentImagePath) {
		$thumbnailFinalPath = "{$currentImagePath}/classifieds/{$classifiedId}.jpg";
		$result             = rename($thumbnailTmpPath, $thumbnailFinalPath);
		if (!$result) {
			$this->log('Error: Thumbnail move to final location failed !');

			return false;
		}

		return "{$classifiedId}.jpg";
	}

	/**
	 * @param string $imageTmpPath
	 * @param int    $classifiedId
	 * @param string $currentImagePath
	 *
	 * @return bool|string
	 */
	public function moveTmpClassifiedImage($imageTmpPath, $classifiedId, $currentImagePath) {
		$text = system::_makeText();

		$imageFinalPath = "{$currentImagePath}/classifieds/{$classifiedId}_{$text}.jpg";
		$result         = rename($imageTmpPath, $imageFinalPath);
		if (!$result) {
			$this->log('Error: Image move to final location failed !');

			return false;
		}

		return "{$classifiedId}_{$text}.jpg";

	}

	/**
	 * @param string  $imagePath
	 * @param int     $sizeX
	 * @param int     $sizeY
	 * @param int     $classifiedId
	 * @param boolean $isDeleteOld
	 * @param string  $type
	 *
	 * @return string
	 */
	public function recreateIfWrongDimensions(
		$imagePath,
		$sizeX,
		$sizeY,
		$classifiedId,
		$isDeleteOld = true,
		$type = 't'
	) {
		$now = time();
		list($w, $h) = getimagesize($imagePath);
		if ($w > $sizeX || $h > $sizeY) {
			$recreatedImagePath = self::DIRECTORY_TEMPORARY."/clad_create.{$classifiedId}.{$now}.{$type}.resizeImg.jpg";
			$result             = $this->recreateImage($imagePath, $recreatedImagePath, $sizeX, $sizeY);

			if (!$result) {
				return $imagePath;
			}

			if ($isDeleteOld) {
				unlink($imagePath);
			}

			return $recreatedImagePath;
		}

		return $imagePath;
	}

	/**
	 * @param string $remoteImageUrl
	 * @param int    $classifiedId
	 * @param string $type
	 *
	 * @return string|null
	 */
	private function downloadImageToTmpDir($remoteImageUrl, $classifiedId, $type = 't') {
		if (filter_var($remoteImageUrl, FILTER_VALIDATE_URL) === false) {
			$this->log("Bad request; Thumbnail url is invalid: '{$remoteImageUrl}' !");

			return null;
		}

		$now = time();

		$image = self::DIRECTORY_TEMPORARY."/clad_create.{$classifiedId}.{$now}.{$type}.jpg";

		$result = file_put_contents($image, file_get_contents($remoteImageUrl));

		if (!$result) {
			$this->log("Error: Can't download image from URL '{$remoteImageUrl}' !");

			return null;
		}

		return $image;
	}

	/**
	 * @param string $imagePath
	 * @param string $recreatedImagePath
	 * @param int    $sizeX
	 * @param int    $sizeY
	 *
	 * @return bool
	 */
	private function recreateImage($imagePath, $recreatedImagePath, $sizeX, $sizeY) {
		list($w, $h, $type) = getimagesize($imagePath);

		$srcDim = $w;
		if ($w > $h) {
			$srcDim = $h;
		}

		$source = null;
		switch ($type) {
			case IMAGETYPE_GIF:
				$source = imagecreatefromgif($imagePath);
				break;
			case IMAGETYPE_JPEG:
				$source = imagecreatefromjpeg($imagePath);
				break;
			case IMAGETYPE_PNG:
				$source = imagecreatefrompng($imagePath);
				break;
			default:
				$this->log("Error: Unsupported type of image: {$type} !");

				return false;
		}

		if ($source === false) {
			$this->log("Cannot recreate thumbnail from {$imagePath} to {$recreatedImagePath}");

			return false;
		}

		$target = ImageCreateTrueColor($sizeX, $sizeY);
		$result = imagecopyresampled($target, $source, 0, 0, 0, 0, $sizeX, $sizeY, $srcDim, $srcDim);
		if (!$result) {
			$this->log('Error: Failed to resize image to thumbnail !');

			return false;
		}
		$result = imagejpeg($target, $recreatedImagePath);
		if (!$result) {
			$this->log('Error: Failed to save resizes image to thumbnail!');

			return false;

		}
		imagedestroy($source);
		imagedestroy($target);

		return true;
	}

	/**
	 * @param $message
	 */
	private function log($message) {
		file_log('save_images', $message);
	}
}