<?php

namespace App\Util\NumberVerifier\Numverify;

use App\Util\NumberVerifier\PhoneInfo;

class PhoneInfoMapper {

	/**
	 * @param array $data
	 *
	 * @return \App\Util\NumberVerifier\PhoneInfo
	 */
	public static function map(array $data) {
		return new PhoneInfo(
			$data['valid'],
			$data['number'],
			$data['local_format'],
			$data['international_format'],
			$data['country_prefix'],
			$data['country_code'],
			$data['country_name'],
			$data['location'],
			$data['carrier'],
			self::getPhoneInfoLineTypeByString($data['line_type'])
		);
	}

	/**
	 * @param string $lineType
	 *
	 * @return int
	 */
	private static function getPhoneInfoLineTypeByString($lineType) {
		switch ($lineType) {
			case 'mobile' :
				return PhoneInfo::LINE_TYPE_MOBILE;
			case 'landline' :
				return PhoneInfo::LINE_TYPE_LANDLINE;
			case 'special_services' :
				return PhoneInfo::LINE_TYPE_SPECIAL_SERVICES;
			case 'paging' :
				return PhoneInfo::LINE_TYPE_PAGING;
			case 'premium_rate' :
				return PhoneInfo::LINE_TYPE_PREMIUM_RATE;
			case 'null' :
				return PhoneInfo::LINE_TYPE_NULL;
			default:
				return PhoneInfo::LINE_TYPE_OTHER;
		}

	}
}