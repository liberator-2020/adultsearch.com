<?php

namespace App\Util\NumberVerifier;

class PhoneInfo {

	const LINE_TYPE_MOBILE = 1;
	const LINE_TYPE_LANDLINE = 2;
	const LINE_TYPE_SPECIAL_SERVICES = 3;
	const LINE_TYPE_PAGING = 4;
	const LINE_TYPE_PREMIUM_RATE = 5;
	const LINE_TYPE_NULL = 6;
	const LINE_TYPE_OTHER = 7;

	const COUNTRY_CODE_UNITED_STATES = 'US';
	const COUNTRY_CODE_CANADA = 'CA';

	/** @var bool */
	private $valid;

	/** @var string */
	private $phone;

	/** @var string|null */
	private $localFormat;

	/** @var string|null */
	private $intlFormat;

	/** @var string|null */
	private $countryPrefix;

	/** @var string|null */
	private $countryCode;

	/** @var string|null */
	private $countryName;

	/** @var string|null */
	private $location;

	/** @var string|null */
	private $carrier;

	/** @var string|null */
	private $lineType;

	/**
	 * @param bool        $valid
	 * @param string      $phone
	 * @param string|null $localFormat
	 * @param string|null $intlFormat
	 * @param string|null $countryPrefix
	 * @param string|null $countryCode
	 * @param string|null $countryName
	 * @param string|null $location
	 * @param string|null $carrier
	 * @param string|null $lineType
	 */
	public function __construct(
		$valid,
		$phone,
		$localFormat = null,
		$intlFormat = null,
		$countryPrefix = null,
		$countryCode = null,
		$countryName = null,
		$location = null,
		$carrier = null,
		$lineType = null
	) {
		$this->valid         = $valid;
		$this->phone         = $phone;
		$this->localFormat   = $localFormat;
		$this->intlFormat    = $intlFormat;
		$this->countryPrefix = $countryPrefix;
		$this->countryCode   = $countryCode;
		$this->countryName   = $countryName;
		$this->location      = $location;
		$this->carrier       = $carrier;
		$this->lineType      = $lineType;
	}

	/**
	 * @return bool
	 */
	public function isValid() {
		return $this->valid;
	}

	/**
	 * @return string
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 * @return string|null
	 */
	public function getLocalFormat() {
		return $this->localFormat;
	}

	/**
	 * @return string|null
	 */
	public function getIntlFormat() {
		return $this->intlFormat;
	}

	/**
	 * @return string|null
	 */
	public function getCountryPrefix() {
		return $this->countryPrefix;
	}

	/**
	 * @return string|null
	 */
	public function getCountryCode() {
		return $this->countryCode;
	}

	/**
	 * @return string|null
	 */
	public function getCountryName() {
		return $this->countryName;
	}

	/**
	 * @return string|null
	 */
	public function getLocation() {
		return $this->location;
	}

	/**
	 * @return string|null
	 */
	public function getCarrier() {
		return $this->carrier;
	}

	/**
	 * @return string|null
	 */
	public function getLineType() {
		return $this->lineType;
	}

	/**
	 * @return bool
	 */
	public function isMobile() {
		return $this->lineType === self::LINE_TYPE_MOBILE;
	}

	/**
	 * @return bool
	 */
	public function isAmerican() {
		return $this->countryCode === self::COUNTRY_CODE_UNITED_STATES
			|| $this->countryCode === self::COUNTRY_CODE_CANADA;
	}

	/**
	 * @return bool
	 */
	public function hasCarrier() {
		return $this->carrier !== null && $this->carrier !== '';
	}

	/**
	 * @return string
	 */
	public function getTextLineType() {
		switch ($this->lineType) {
			case 1 :
				return 'mobile';
			case 2 :
				return 'landline';
			case 3 :
				return 'special_services';
			case 4 :
				return 'paging';
			case 5 :
				return 'premium_rate';
			case 6 :
				return 'null';
			case 7 :
				return 'other';
		}
	}

	/**
	 * @return string
	 */
	public function isTextValid() {
		if (!$this->valid) {
			return 'false';
		}

		return 'true';
	}

	/**
	 * @return string
	 */
	public function __toString() {
		$string = "valid: {$this->isTextValid()} \n phone: {$this->getPhone()} \n";

		if ($localFormat = $this->getLocalFormat()) {
			$string .= "local format = {$localFormat} \n";
		}

		if ($intlFormat = $this->getIntlFormat()) {
			$string .= "international format = {$intlFormat} \n";
		}

		if ($countryPrefix = $this->getCountryPrefix()) {
			$string .= "country prefix = {$countryPrefix} \n";
		}

		if ($countryCode = $this->getCountryCode()) {
			$string .= "country code = {$countryCode} \n";
		}

		if ($countryName = $this->getCountryName()) {
			$string .= "country name = {$countryName} \n";
		}

		if ($location = $this->getLocation()) {
			$string .= "location = {$location} \n";
		}

		if ($carrier = $this->getCarrier()) {
			$string .= "carrier = {$carrier} \n";
		}

		if ($lineType = $this->getTextLineType()) {
			$string .= "line type = {$lineType} \n";
		}

		return $string;
	}

	/**
	 * @return array
	 */
	public function getArray() {
		$result = [
			'valid' => $this->isTextValid(),
			'phone' => $this->getPhone(),
		];

		if ($localFormat = $this->getLocalFormat()) {
			$result['localFormat'] = $localFormat;
		}

		if ($intlFormat = $this->getIntlFormat()) {
			$result['intlFormat'] = $intlFormat;
		}

		if ($countryPrefix = $this->getCountryPrefix()) {
			$result['countryPrefix'] = $countryPrefix;
		}

		if ($countryCode = $this->getCountryCode()) {
			$result['countryCode'] = $countryCode;
		}

		if ($countryName = $this->getCountryName()) {
			$result['countryName'] = $countryName;
		}

		if ($location = $this->getLocation()) {
			$result['location'] = $location;
		}

		if ($carrier = $this->getCarrier()) {
			$result['carrier'] = $carrier;
		}

		if ($lineType = $this->getTextLineType()) {
			$result['lineType'] = $lineType;
		}

		return $result;
	}

	/**
	 * @return false|string
	 */
	public function getJSON() {
		return json_encode($this->getArray());
	}

}