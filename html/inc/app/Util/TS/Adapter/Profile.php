<?php

namespace App\Util\TS\Adapter;

use App\Util\TS\Model\Profile as TsProfile;

class Profile extends TsProfile {

	/**
	 * @return int
	 */
	public function getBodyType() {
		$build = parent::getBodyType();

		switch ($build) {
			case 1:
				return self::BUILD_TYPE_TINY;
			case 2:
				return self::BUILD_TYPE_SLIM_SLENDER;
			case 3:
				return self::BUILD_TYPE_AVERAGE;
			case 4:
				return self::BUILD_TYPE_ATHLETIC;
			case 5:
				return self::BUILD_TYPE_CURVY;
			case 6:
				return self::BUILD_TYPE_BBW;
			case 7:
				return self::BUILD_TYPE_BBW;
			case 8:
				return self::BUILD_TYPE_BBW;
		}

		return null;
	}

	/**
	 * @return int|null
	 */
	public function getHairColor() {
		$hairColor = parent::getHairColor();

		if ($hairColor === 'Blond') {
			return self::HAIR_COLOR_BLONDE;
		}

		if ($hairColor === 'Red') {
			return self::HAIR_COLOR_RED;
		}

		if ($hairColor === 'Brown') {
			return self::HAIR_COLOR_DARK_BROWN;
		}

		if ($hairColor === 'Black') {
			return self::HAIR_COLOR_BLACK;
		}

		return null;
	}

	public function getEyeColor() {
		$eyeColor = parent::getEyeColor();

		switch ($eyeColor) {
			case 1:
				return self::EYE_COLOR_BROWN;
			case 2:
				return self::EYE_COLOR_BLUE;
			case 3:
				return self::EYE_COLOR_GREEN;
			case 4:
				return self::EYE_COLOR_GRAY;
			case 6:
				return self::EYE_COLOR_HAZEL;
		}

		return null;
	}

	/**
	 * @return int
	 */
	public function getPenisSize() {
		return (int)$this->getEndowment() + 4;
	}

	/**
	 * @return array
	 */
	public function getHeightCm() {
		$heightCm = parent::getHeightCm();

		if (!$heightCm) {
			$footConverted = $this->getHeightFoot() * self::VALUE_FOOT_TO_CM;
			$inchConverted = $this->getHeightInch() * self::VALUE_FOOT_TO_INCH;

			$heightCm = round($footConverted + $inchConverted);
		}

		return [
			'height_feet'   => substr($heightCm, 0, 1),
			'height_inches' => substr($heightCm, 1),
		];
	}

	/**
	 * @return float
	 */
	public function getWeightKg() {
		$weightKg = parent::getWeightKg();

		if (!$weightKg) {
			$weightKg = round($this->getWeightLb() * self::VALUE_LBS_TO_KG);
		}

		return $weightKg;
	}

	/**
	 * @return int|null
	 */
	public function getEthnicity() {
		$ethnicity = parent::getEthnicity();

		if ($ethnicity === 'Exotic') {
			return self::ETHNICITY_TYPE_MIXED;
		}

		if ($ethnicity === 'Hispanic / Latin') {
			return self::ETHNICITY_TYPE_HISPANIC;
		}

		return null;
	}
}