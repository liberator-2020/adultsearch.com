<?php

namespace App\Util\TS\Importer;

abstract class AbstractImporter {

	/** @var \db */
	private $db;

	/**
	 * AbstractImporter constructor.
	 */
	public function __construct() {
		global $db;

		$this->db = $db;
	}

	/**
	 * @return void
	 */
	protected function startTransaction() {
		$this->db->q('START TRANSACTION;');
	}

	/**
	 * @return void
	 */
	protected function commitTransaction() {
		$this->db->q('COMMIT;');
	}

	/**
	 * @return void
	 */
	protected function rollbackTransaction() {
		$this->db->q('ROLLBACK;');
	}

	/**
	 * @param string $message
	 */
	protected function log($message) {
		file_log($this->getLogFileName(), $message);
	}

	/**
	 * @return string
	 */
	abstract protected function getLogFileName();

}