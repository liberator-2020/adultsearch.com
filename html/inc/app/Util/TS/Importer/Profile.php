<?php

namespace App\Util\TS\Importer;

use App\Repository\AccountRepository;
use App\Repository\ClassifiedsImageRepository;
use App\Repository\ClassifiedsLocRepository;
use App\Repository\ClassifiedsRepository;
use App\Repository\LocationRepository;
use App\Util\Image;
use App\Util\TS\Model\Location;
use App\Util\TS\Adapter\Profile as ProfileAdapter;
use classifieds;
use Exception;
use db;

class Profile extends AbstractImporter {

	/** @var \App\Repository\ClassifiedsRepository */
	private $classifiedsRepository;

	/** @var \App\Repository\ClassifiedsLocRepository */
	private $classifiedsLocRepository;

	/** @var \App\Repository\ClassifiedsImageRepository */
	private $classifiedsImageRepository;

	/** @var \App\Repository\LocationRepository */
	private $locationRepository;

	/** @var \App\Repository\AccountRepository */
	private $accountRepository;

	/** @var \App\Util\Image */
	private $image;

	/** @var string */
	private $currentImagePath;

	/**
	 * ProfileImporter constructor.
	 */
	public function __construct() {
		global $config_image_path;
		parent::__construct();

		$customDb = new db('utf8mb4');

		$this->classifiedsRepository      = new ClassifiedsRepository($customDb);
		$this->classifiedsLocRepository   = new ClassifiedsLocRepository();
		$this->locationRepository         = new LocationRepository();
		$this->accountRepository          = new AccountRepository();
		$this->classifiedsImageRepository = new ClassifiedsImageRepository();
		$this->image                      = new Image();
		$this->currentImagePath           = $config_image_path;
	}

	/**
	 * @param \App\Util\TS\Adapter\Profile $tsProfileAdapter
	 *
	 * @throws \Exception
	 */
	public function importProfileToClassified(ProfileAdapter $tsProfileAdapter) {
		$this->startTransaction();

		$asCountryId = $this->getCountryIdOrException($tsProfileAdapter);
		$asCity      = $this->getCityOrException($tsProfileAdapter, $asCountryId);

		$asCityId   = $asCity['id'];
		$asCityName = $asCity['name'];

		if ($this->isClassifiedExists($tsProfileAdapter, $asCityId)) {
			$this->rollbackTransaction();
			throw new Exception("Profile {$tsProfileAdapter->getPhone()} is already exists. Skipped");
		}

		$asUserId = $this->getUserOrException($tsProfileAdapter);

		$now = time();

		$classifiedId = $this->getClassifiedIdOrException(
			$tsProfileAdapter,
			$asCountryId,
			$asCityId,
			$asUserId,
			$asCityName,
			$now
		);

		$classifiedLocId = $this->getClassifiedLocIdOrException(
			$tsProfileAdapter,
			$asCountryId,
			$asCityId,
			$classifiedId,
			$now
		);

		$thumbnail = $this->saveClassifiedThumbnail($tsProfileAdapter, $classifiedId);

		$images = $this->saveClassifiedImages($tsProfileAdapter, $classifiedId);

		if (!$thumbnail) {
			$this->classifiedsRepository->updateThumbnail($images[0], $classifiedId);
		}

		$this->classifiedsRepository->updateByIdWithtsProfile($tsProfileAdapter, $classifiedId);

		$this->classifiedsRepository->publishById($classifiedId);
		$this->classifiedsLocRepository->publishById($classifiedLocId);

		$this->commitTransaction();

		classifieds::rotateIndex();
	}

	/**
	 * @return string
	 */
	protected function getLogFileName() {
		return 'profile_importer';
	}

	/**
	 * @param \App\Util\TS\Adapter\Profile $tsProfileAdapter
	 *
	 * @return int
	 * @throws \Exception
	 */
	private function getCountryIdOrException(ProfileAdapter $tsProfileAdapter) {
		if (!$city = $tsProfileAdapter->getLocation()) {
			$this->rollbackTransaction();
			throw new Exception("Profile {$tsProfileAdapter->getPhone()} doesn't have location. Skipped");
		}

		/*  TODO

			This should be improved for states and boroughs
		*/
		if ($city->getParent()->getType() === Location::TYPE_COUNTRY) {
			$country = $city->getParent();
		}

		$asCountry = $this->locationRepository->findByName($country->getName());

		if (!$asCountry) {
			$this->rollbackTransaction();
			throw new Exception("There is no location with name {$country->getName()}. Skipped");
		}

		return $asCountry['id'];
	}

	/**
	 * @param \App\Util\TS\Adapter\Profile $tsProfileAdapter
	 * @param int                          $countryId
	 *
	 * @return array
	 * @throws \Exception
	 */
	private function getCityOrException(ProfileAdapter $tsProfileAdapter, $countryId) {
		$tsCity     = $tsProfileAdapter->getLocation();
		$tsCityName = $tsCity->getName();
		$asCity     = $this->locationRepository->findByNameAndParentId($tsCityName, $countryId);

		if (!$asCity) {
			$this->locationRepository->createByTsLocationAndParentId($tsCity, $countryId);
			$asCity = $this->locationRepository->findByName($tsCity->getName());

			if (!$asCity) {
				$this->rollbackTransaction();
				throw new Exception("Cannot create city {$tsCityName}. Skipped");
			}

			$this->log("City #{$asCity['id']} {$asCity['name']} created successfully");
		}

		return $asCity;
	}

	/**
	 * @param \App\Util\TS\Adapter\Profile $tsProfileAdapter
	 * @param                              $asCityId
	 *
	 * @return bool
	 */
	private function isClassifiedExists(ProfileAdapter $tsProfileAdapter, $asCityId) {
		$classifiedOld = $this->classifiedsRepository->findByPhoneAndLocationId(
			$tsProfileAdapter->getPhone(),
			$asCityId
		);

		if (!$classifiedOld) {
			return false;
		}

		return true;
	}

	/**
	 * @param \App\Util\TS\Adapter\Profile $tsProfileAdapter
	 *
	 * @return int
	 * @throws \Exception
	 */
	private function getUserOrException(ProfileAdapter $tsProfileAdapter) {
		$tsUser        = $tsProfileAdapter->getUser();
		$tsUserPhone   = $tsUser->getPhone();
		$asUserByPhone = $this->accountRepository->findByPhoneId($tsUserPhone);
		$asUserByEmail = $this->accountRepository->findByEmailId($tsUser->getEmail());

		$asUser = null;
		if (!$asUserByPhone && !$asUserByEmail) {

			$this->accountRepository->createByTsUser($tsUser);

			$asUser = $this->accountRepository->findByPhoneId($tsUserPhone);
			if (!$asUser) {
				$this->rollbackTransaction();
				throw new Exception("Cannot create user {$tsUserPhone}. Skipped");
			}

			$this->log("Account with number {$tsUserPhone} created successfully: #{$asUser}");

			$this->accountRepository->updateByPhoneAccountWithTsUser($tsUser);

			$this->log("Account with number {$tsUserPhone} updated: #{$asUser}");
		}

		if (!$asUser) {
			if ($asUserByPhone) {
				$asUser = $asUserByPhone;
			}

			if ($asUserByEmail) {
				$asUser = $asUserByEmail;
			}
		}

		return $asUser['id'];
	}

	/**
	 * @param \App\Util\TS\Adapter\Profile $tsProfileAdapter
	 * @param int                          $asCountryId
	 * @param int                          $asCityId
	 * @param int                          $asUserId
	 * @param string                       $asCityName
	 * @param int                          $now
	 *
	 * @return int
	 * @throws \Exception
	 */
	private function getClassifiedIdOrException(
		ProfileAdapter $tsProfileAdapter,
		$asCountryId,
		$asCityId,
		$asUserId,
		$asCityName,
		$now
	) {
		$tsProfileAdapterPhone = $tsProfileAdapter->getPhone();

		$this->classifiedsRepository->createClassifiedsBytsProfile(
			$tsProfileAdapter,
			$asCountryId,
			$asCityId,
			$asUserId,
			$asCityName,
			2,
			$now
		);

		$classified = $this->classifiedsRepository->findByPhoneAndLocationId($tsProfileAdapterPhone, $asCityId);

		if (!$classified) {
			$this->rollbackTransaction();
			throw new Exception("Cannot create classified {$tsProfileAdapterPhone}. Skipped");
		}

		$this->log("Classified #{$classified['id']} was created successfully");

		return $classified['id'];
	}

	/**
	 * @param \App\Util\TS\Adapter\Profile $tsProfileAdapter
	 * @param int                          $asCountryId
	 * @param int                          $asCityId
	 * @param int                          $classifiedId
	 * @param int                          $now
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	private function getClassifiedLocIdOrException(ProfileAdapter $tsProfileAdapter,
		$asCountryId,
		$asCityId,
		$classifiedId,
		$now) {
		$this->classifiedsLocRepository->createByLocationAndClassifiedId(
			$asCountryId,
			$asCityId,
			2,
			$classifiedId,
			$now
		);

		$classifiedLoc = $this->classifiedsLocRepository->findByClassifiedId($classifiedId);

		if (!$classifiedLoc) {
			$this->rollbackTransaction();
			throw new Exception("Cannot create classified_loc for profile {$tsProfileAdapter->getPhone()}. Skipped");
		}

		$this->log("Classified_loc #{$classifiedLoc['id']} created successfully");

		return $classifiedLoc['id'];
	}

	/**
	 * @param \App\Util\TS\Adapter\Profile $tsProfileAdapter
	 * @param int                          $classifiedId
	 *
	 * @return bool|string
	 * @throws \Exception
	 */
	private function saveClassifiedThumbnail(ProfileAdapter $tsProfileAdapter, $classifiedId) {
		$thumbnailTmp = $this->image->saveThumbnailFromtsProfileToClassified($tsProfileAdapter, $classifiedId);

		if (!$thumbnailTmp) {
			return false;
		}

		$this->log("Temporary thumbnail {$thumbnailTmp} created successfully");

		$thumbnailTmp = $this->image->recreateIfWrongDimensions(
			$thumbnailTmp,
			75,
			75,
			$classifiedId
		);

		$thumbnail = $this->image->moveTmpClassifiedThumbnail(
			$thumbnailTmp,
			$classifiedId,
			$this->currentImagePath
		);

		if (!$thumbnail) {
			$this->rollbackTransaction();
			throw new Exception("Cannot create thumbnail for profile: {$tsProfileAdapter->getPhone()}");
		}

		$this->log("Thumbnail {$thumbnail} created successfully");

		$this->classifiedsRepository->updateThumbnail($thumbnail, $classifiedId);

		$this->log("Thumbnail {$thumbnail} saved for classified #{$classifiedId} successfully");

		return $thumbnail;
	}

	/**
	 * @param \App\Util\TS\Adapter\Profile $tsProfileAdapter
	 * @param int                          $classifiedId
	 *
	 * @return array|bool
	 */
	private function saveClassifiedImages(ProfileAdapter $tsProfileAdapter, $classifiedId) {
		$imagesTmp = $this->image->saveImagesFromtsProfileToClassified($tsProfileAdapter, $classifiedId);

		$images = [];
		foreach ($imagesTmp as $imageTmp) {
			$image = $this->image->moveTmpClassifiedImage(
				$imageTmp,
				$classifiedId,
				$this->currentImagePath
			);

			if (!$image) {
				$this->log("Cannot create image for profile {$tsProfileAdapter->getPhone()}. Skipped only this image: {$imageTmp}");
				continue;
			}

			$this->classifiedsImageRepository->createImage($image, $classifiedId, $this->currentImagePath);

			$images[] = $image;
			$this->log("Image {$image} saved successfully");
		}

		return $images;
	}
}