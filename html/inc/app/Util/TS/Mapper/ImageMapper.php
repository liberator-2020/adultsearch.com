<?php

namespace App\Util\TS\Mapper;

use App\Util\TS\Model\Image;

class ImageMapper {

	/**
	 * @param array $data
	 *
	 * @return \App\Util\TS\Model\Image
	 */
	public static function map(array $data) {
		return new Image(
			$data['type'],
			$data['filename'],
			$data['profileThumbnail'],
			$data['thumbnail'],
			$data['title'],
			$data['description'],
			$data['width'],
			$data['height'],
			$data['status'],
			$data['error'],
			$data['createdStamp'],
			$data['source'],
			$data['sourceId'],
			$data['fileHash'],
			$data['id']
		);
	}
}