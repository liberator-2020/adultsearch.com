<?php

namespace App\Util\TS\Mapper;

use App\Util\TS\Model\Location;

class LocationMapper {

	/**
	 * @param array $data
	 *
	 * @return \App\Util\TS\Model\Location
	 */
	public static function map(array $data) {
		return new Location(
			$data['id'],
			$data['type'],
			$data['name'],
			$data['code'],
			$data['url'],
			$data['lat'],
			$data['lng'],
			$data['minor'],
			$data['activeProfile'],
			$data['significant'],
			$data['homepage'],
			$data['colnum'],
			$data['callingCode'],
			$data['h1Title'],
			$data['title'],
			$data['population'],
			$data['description'],
			$data['metaDescription'],
			$data['metaKeywords'],
			$data['featuredPrice'],
			$data['parent'] ? self::map($data['parent']) : null
		);
	}

}