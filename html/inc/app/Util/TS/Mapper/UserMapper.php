<?php

namespace App\Util\TS\Mapper;

use App\Util\TS\Model\User;

class UserMapper {

	/**
	 * @param array $data
	 *
	 * @return \App\Util\TS\Model\User
	 */
	public static function map(array $data) {
		return new User(
			$data['uname'],
			$data['name'],
			$data['email'],
			$data['phone'],
			$data['active'],
			$data['loginType'],
			$data['password'],
			$data['banned'],
			$data['banReason'],
			$data['provider'],
			$data['level'],
			$data['fullName'],
			$data['confirmationToken'],
			$data['passwordRequestedAt'],
			$data['emailRegister'],
			$data['phoneVerified'],
			$data['emailVerified'],
			$data['dateOfBirth'],
			$data['find'],
			$data['lastloginStamp'],
			$data['lastloginAgent'],
			$data['facebookId'],
			$data['facebookConnectStamp'],
			$data['facebookShow'],
			$data['googleId'],
			$data['googleConnectStamp'],
			$data['googleShow'],
			$data['instagramId'],
			$data['instagramConnectStamp'],
			$data['instagramNickname'],
			$data['instagramShow'],
			$data['twitterId'],
			$data['twitterConnectStamp'],
			$data['twitterShow'],
			$data['repost'],
			$data['autologinCode'],
			$data['autologinExpireStamp'],
			$data['acquisitionSource'],
			$data['acquisitionMedium'],
			$data['acquisitionCampaign'],
			$data['createdStamp'],
			$data['updatedStamp'],
			$data['deletedStamp'],
			$data['id'],
			$data['budget']
		);
	}

}