<?php

namespace App\Util\TS\Model;

class Image {

	/** @var string */
	private $type;

	/** @var string */
	private $filename;

	/** @var boolean */
	private $profileThumbnail;

	/** @var string */
	private $thumbnail;

	/** @var string */
	private $title;

	/** @var string */
	private $description;

	/** @var int */
	private $width;

	/** @var int */
	private $height;

	/** @var int */
	private $status;

	/** @var string */
	private $error;

	/** @var int */
	private $createdStamp;

	/** @var string */
	private $source;

	/** @var string */
	private $sourceId;

	/** @var string */
	private $fileHash;

	/** @var int */
	private $id;

	/**
	 * Image constructor.
	 *
	 * @param string $type
	 * @param string $filename
	 * @param bool   $profileThumbnail
	 * @param string $thumbnail
	 * @param string $title
	 * @param string $description
	 * @param int    $width
	 * @param int    $height
	 * @param int    $status
	 * @param string $error
	 * @param int    $createdStamp
	 * @param string $source
	 * @param string $sourceId
	 * @param string $fileHash
	 * @param int    $id
	 */
	public function __construct(
		$type,
		$filename,
		$profileThumbnail = null,
		$thumbnail = null,
		$title = null,
		$description = null,
		$width = null,
		$height = null,
		$status = null,
		$error = null,
		$createdStamp = null,
		$source = null,
		$sourceId = null,
		$fileHash = null,
		$id = null
	) {
		$this->type             = $type;
		$this->filename         = $filename;
		$this->profileThumbnail = $profileThumbnail;
		$this->thumbnail        = $thumbnail;
		$this->title            = $title;
		$this->description      = $description;
		$this->width            = $width;
		$this->height           = $height;
		$this->status           = $status;
		$this->error            = $error;
		$this->createdStamp     = $createdStamp;
		$this->source           = $source;
		$this->sourceId         = $sourceId;
		$this->fileHash         = $fileHash;
		$this->id               = $id;
	}

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getFilename() {
		return $this->filename;
	}

	/**
	 * @return bool
	 */
	public function isProfileThumbnail() {
		return $this->profileThumbnail;
	}

	/**
	 * @return string
	 */
	public function getThumbnail() {
		return $this->thumbnail;
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @return int
	 */
	public function getWidth() {
		return $this->width;
	}

	/**
	 * @return int
	 */
	public function getHeight() {
		return $this->height;
	}

	/**
	 * @return int
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 * @return string
	 */
	public function getError() {
		return $this->error;
	}

	/**
	 * @return int
	 */
	public function getCreatedStamp() {
		return $this->createdStamp;
	}

	/**
	 * @return string
	 */
	public function getSource() {
		return $this->source;
	}

	/**
	 * @return string
	 */
	public function getSourceId() {
		return $this->sourceId;
	}

	/**
	 * @return string
	 */
	public function getFileHash() {
		return $this->fileHash;
	}

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

}