<?php

namespace App\Util\TS\Model;

class Location {

	const TYPE_COUNTRY = 1;
	const TYPE_STATE = 2;
	const TYPE_CITY = 3;
	const TYPE_BOROUGH = 4;

	/** @var int */
	private $id;

	/** @var int */
	private $type;

	/** @var string */
	private $code;

	/** @var string */
	private $name;

	/** @var string */
	private $url;

	/** @var string */
	private $lat;

	/** @var string */
	private $lng;

	/** @var boolean */
	private $minor;

	/** @var boolean */
	private $activeProfile;

	/** @var boolean */
	private $significant;

	/** @var boolean */
	private $homepage;

	/** @var int */
	private $colnum;

	/** @var int */
	private $callingCode;

	/** @var string */
	private $h1Title;

	/** @var string */
	private $title;

	/** @var int */
	private $population;

	/** @var string */
	private $description;

	/** @var string */
	private $metaDescription;

	/** @var string */
	private $metaKeywords;

	/** @var string */
	private $featuredPrice;

	/** @var \App\Util\TS\Location */
	private $parent;

	/**
	 * Location constructor.
	 *
	 * @param int                   $id
	 * @param int                   $type
	 * @param string                $code
	 * @param string                $name
	 * @param string                $url
	 * @param string                $lat
	 * @param string                $lng
	 * @param bool                  $minor
	 * @param bool                  $activeProfile
	 * @param bool                  $significant
	 * @param bool                  $homepage
	 * @param int                   $colnum
	 * @param int                   $callingCode
	 * @param string                $h1Title
	 * @param string                $title
	 * @param int                   $population
	 * @param string                $description
	 * @param string                $metaDescription
	 * @param string                $metaKeywords
	 * @param string                $featuredPrice
	 * @param \App\Util\TS\Location $parent
	 */
	public function __construct(
		$id,
		$type,
		$name,
		$code = null,
		$url = null,
		$lat = null,
		$lng = null,
		$minor = null,
		$activeProfile = null,
		$significant = null,
		$homepage = null,
		$colnum = null,
		$callingCode = null,
		$h1Title = null,
		$title = null,
		$population = null,
		$description = null,
		$metaDescription = null,
		$metaKeywords = null,
		$featuredPrice = null,
		Location $parent = null
	) {
		$this->id              = $id;
		$this->type            = $type;
		$this->code            = $code;
		$this->name            = $name;
		$this->url             = $url;
		$this->lat             = $lat;
		$this->lng             = $lng;
		$this->minor           = $minor;
		$this->activeProfile   = $activeProfile;
		$this->significant     = $significant;
		$this->homepage        = $homepage;
		$this->colnum          = $colnum;
		$this->callingCode     = $callingCode;
		$this->h1Title         = $h1Title;
		$this->title           = $title;
		$this->population      = $population;
		$this->description     = $description;
		$this->metaDescription = $metaDescription;
		$this->metaKeywords    = $metaKeywords;
		$this->featuredPrice   = $featuredPrice;
		$this->parent          = $parent;
	}

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return int
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getCode() {
		return $this->code;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * @return string
	 */
	public function getLat() {
		return $this->lat;
	}

	/**
	 * @return string
	 */
	public function getLng() {
		return $this->lng;
	}

	/**
	 * @return bool
	 */
	public function isMinor() {
		return $this->minor;
	}

	/**
	 * @return bool
	 */
	public function isActiveProfile() {
		return $this->activeProfile;
	}

	/**
	 * @return bool
	 */
	public function isSignificant() {
		return $this->significant;
	}

	/**
	 * @return bool
	 */
	public function isHomepage() {
		return $this->homepage;
	}

	/**
	 * @return int
	 */
	public function getColnum() {
		return $this->colnum;
	}

	/**
	 * @return int
	 */
	public function getCallingCode() {
		return $this->callingCode;
	}

	/**
	 * @return string
	 */
	public function getH1Title() {
		return $this->h1Title;
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @return int
	 */
	public function getPopulation() {
		return $this->population;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @return string
	 */
	public function getMetaDescription() {
		return $this->metaDescription;
	}

	/**
	 * @return string
	 */
	public function getMetaKeywords() {
		return $this->metaKeywords;
	}

	/**
	 * @return string
	 */
	public function getFeaturedPrice() {
		return $this->featuredPrice;
	}

	/**
	 * @return \App\Util\TS\Model\Location
	 */
	public function getParent() {
		return $this->parent;
	}
}