<?php

namespace App\Util\TS\Model;

use App\Util\TS\Mapper\ImageMapper;

class Profile {

	const HAIR_COLOR_BLONDE = 2;
	const HAIR_COLOR_RED = 5;
	const HAIR_COLOR_DARK_BROWN = 3;
	const HAIR_COLOR_BLACK = 4;

	const ETHNICITY_TYPE_MIXED = 7;
	const ETHNICITY_TYPE_HISPANIC = 3;

	const BUILD_TYPE_TINY = 1;
	const BUILD_TYPE_SLIM_SLENDER = 2;
	const BUILD_TYPE_ATHLETIC = 3;
	const BUILD_TYPE_AVERAGE = 4;
	const BUILD_TYPE_CURVY = 5;
	const BUILD_TYPE_BBW = 6;

	const EYE_COLOR_BLUE = 1;
	const EYE_COLOR_BROWN = 2;
	const EYE_COLOR_GREEN = 3;
	const EYE_COLOR_HAZEL = 4;
	const EYE_COLOR_GRAY = 6;

	const VALUE_FOOT_TO_CM = 30.48;
	const VALUE_FOOT_TO_INCH = 2.54;
	const VALUE_LBS_TO_KG = 0.4535924;


	/** @var int */
	private $type;

	/** @var string */
	private $name;

	/** @var string */
	private $url;

	/** @var boolean */
	private $sponsorDesktop;

	/** @var boolean */
	private $sponsorMobile;

	/** @var string */
	private $directLink;

	/** @var string */
	private $phone;

	/** @var string */
	private $phoneToBe;

	/** @var string */
	private $email;

	/** @var string */
	private $thumbnail;

	/** @var string */
	private $thumbnails;

	/** @var string */
	private $tagline;

	/** @var string */
	private $languages;

	/** @var boolean */
	private $incall;

	/** @var boolean */
	private $outcall;

	/** @var int */
	private $incallPrice;

	/** @var int */
	private $incallPrice30;

	/** @var int */
	private $outcallPrice;

	/** @var int */
	private $outcallPrice30;

	/** @var boolean */
	private $men;

	/** @var boolean */
	private $women;

	/** @var boolean */
	private $tstvtg;

	/** @var boolean */
	private $couples;

	/** @var boolean */
	private $groups;

	/** @var string */
	private $locationDetail;

	/** @var string */
	private $aboutme;

	/** @var string */
	private $website;

	/** @var string */
	private $tumblr;

	/** @var string */
	private $facebook;

	/** @var string */
	private $twitter;

	/** @var string */
	private $instagram;

	/** @var int */
	private $ter;

	/** @var string */
	private $whatsapp;

	/** @var string */
	private $line;

	/** @var string */
	private $wechat;

	/** @var int */
	private $dob;

	/** @var int */
	private $age;

	/** @var int */
	private $heightFoot;

	/** @var int */
	private $heightInch;

	/** @var int */
	private $heightCm;

	/** @var int */
	private $weightLb;

	/** @var int */
	private $weightKg;

	/** @var string */
	private $cup;

	/** @var string */
	private $bustInch;

	/** @var string */
	private $bustCm;

	/** @var string */
	private $waistInch;

	/** @var string */
	private $waistCm;

	/** @var string */
	private $drugs;

	/** @var string */
	private $hipsInch;

	/** @var string */
	private $hipsCm;

	/** @var string */
	private $bodyType;

	/** @var string */
	private $eyeColor;

	/** @var string */
	private $hairColor;

	/** @var string */
	private $hairLength;

	/** @var int */
	private $ethnicity;

	/** @var string */
	private $smoking;

	/** @var string */
	private $affiliation;

	/** @var string */
	private $piercings;

	/** @var string */
	private $role;

	/** @var string */
	private $tattoos;

	/** @var string */
	private $endowment;

	/** @var string */
	private $thickness;

	/** @var string */
	private $circumcised;

	/** @var int */
	private $serviceType;

	/** @var boolean */
	private $pornstar;

	/** @var boolean */
	private $massage;

	private $gfe;

	/** @var boolean */
	private $bdsm;

	/** @var string */
	private $youtube;

	/** @var string */
	private $op;

	/** @var int */
	private $createdStamp;

	/** @var int */
	private $exportableStamp;

	/** @var int */
	private $updatedStamp;

	/** @var int */
	private $deletedStamp;

	/** @var boolean */
	private $deletedKeepFeed;

	/** @var int */
	private $checkedStamp;

	/** @var boolean */
	private $active;

	/** @var string */
	private $activeReason;

	/** @var int */
	private $postedStamp;

	/** @var int */
	private $postedTwitterStamp;

	/** @var int */
	private $expireStamp;

	/** @var boolean */
	private $featured;

	/** @var int */
	private $hit;

	/** @var boolean */
	private $disabled;

	/** @var string */
	private $disabledReason;

	/** @var string */
	private $oldUrl;

	/** @var string */
	private $tmpId;

	/** @var string */
	private $asUrl;

	/** @var int */
	private $asCheckStamp;

	/** @var int */
	private $claimEmailSent;

	/** @var int */
	private $verifiedStamp;

	/** @var int */
	private $id;

	/** @var int */
	private $featuredExpireStamp;

	/** @var string */
	private $featuredReason;

	/** @var int */
	private $phoneVerifiedStamp;

	/** @var \App\Util\TS\Location */
	private $location;

	/** @var \App\Util\TS\User */
	private $user;

	/** @var \App\Util\TS\Image[] */
	private $images;

	/**
	 * Profile constructor.
	 *
	 * @param int                         $type
	 * @param string                      $name
	 * @param string                      $url
	 * @param bool                        $sponsorDesktop
	 * @param bool                        $sponsorMobile
	 * @param string                      $directLink
	 * @param string                      $phone
	 * @param string                      $phoneToBe
	 * @param string                      $email
	 * @param string                      $thumbnail
	 * @param string                      $thumbnails
	 * @param string                      $tagline
	 * @param string                      $languages
	 * @param bool                        $incall
	 * @param bool                        $outcall
	 * @param int                         $incallPrice
	 * @param int                         $incallPrice30
	 * @param int                         $outcallPrice
	 * @param int                         $outcallPrice30
	 * @param bool                        $men
	 * @param bool                        $women
	 * @param bool                        $tstvtg
	 * @param bool                        $couples
	 * @param bool                        $groups
	 * @param string                      $locationDetail
	 * @param string                      $aboutme
	 * @param string                      $website
	 * @param string                      $tumblr
	 * @param string                      $facebook
	 * @param string                      $twitter
	 * @param string                      $instagram
	 * @param int                         $ter
	 * @param string                      $whatsapp
	 * @param string                      $line
	 * @param string                      $wechat
	 * @param int                         $dob
	 * @param int                         $age
	 * @param int                         $heightFoot
	 * @param int                         $heightInch
	 * @param int                         $heightCm
	 * @param int                         $weightLb
	 * @param int                         $weightKg
	 * @param string                      $cup
	 * @param string                      $bustInch
	 * @param string                      $bustCm
	 * @param string                      $waistInch
	 * @param string                      $waistCm
	 * @param string                      $drugs
	 * @param string                      $hipsInch
	 * @param string                      $hipsCm
	 * @param string                      $bodyType
	 * @param string                      $eyeColor
	 * @param string                      $hairColor
	 * @param string                      $hairLength
	 * @param int                         $ethnicity
	 * @param string                      $smoking
	 * @param string                      $affiliation
	 * @param string                      $piercings
	 * @param string                      $role
	 * @param string                      $tattoos
	 * @param string                      $endowment
	 * @param string                      $thickness
	 * @param string                      $circumcised
	 * @param int                         $serviceType
	 * @param bool                        $pornstar
	 * @param bool                        $massage
	 * @param                             $gfe
	 * @param bool                        $bdsm
	 * @param string                      $youtube
	 * @param string                      $op
	 * @param int                         $createdStamp
	 * @param int                         $exportableStamp
	 * @param int                         $updatedStamp
	 * @param int                         $deletedStamp
	 * @param bool                        $deletedKeepFeed
	 * @param int                         $checkedStamp
	 * @param bool                        $active
	 * @param string                      $activeReason
	 * @param int                         $postedStamp
	 * @param int                         $postedTwitterStamp
	 * @param int                         $expireStamp
	 * @param bool                        $featured
	 * @param int                         $hit
	 * @param bool                        $disabled
	 * @param string                      $disabledReason
	 * @param string                      $oldUrl
	 * @param string                      $tmpId
	 * @param string                      $asUrl
	 * @param int                         $asCheckStamp
	 * @param int                         $claimEmailSent
	 * @param int                         $verifiedStamp
	 * @param int                         $id
	 * @param int                         $featuredExpireStamp
	 * @param string                      $featuredReason
	 * @param int                         $phoneVerifiedStamp
	 * @param array                       $images
	 *
	 * @param \App\Util\TS\Model\Location $location
	 * @param \App\Util\TS\Model\User     $user
	 */
	public function __construct(
		$name,
		$age,
		$aboutme,
		$email,
		array $images,
		Location $location,
		User $user,
		$phone,
		$type,
		$url = null,
		$sponsorDesktop = null,
		$sponsorMobile = null,
		$directLink = null,
		$phoneToBe = null,
		$thumbnail = null,
		$thumbnails = null,
		$tagline = null,
		$languages = null,
		$incall = null,
		$outcall = null,
		$incallPrice = null,
		$incallPrice30 = null,
		$outcallPrice = null,
		$outcallPrice30 = null,
		$men = null,
		$women = null,
		$tstvtg = null,
		$couples = null,
		$groups = null,
		$locationDetail = null,
		$website = null,
		$tumblr = null,
		$facebook = null,
		$twitter = null,
		$instagram = null,
		$ter = null,
		$whatsapp = null,
		$line = null,
		$wechat = null,
		$dob = null,
		$heightFoot = null,
		$heightInch = null,
		$heightCm = null,
		$weightLb = null,
		$weightKg = null,
		$cup = null,
		$bustInch = null,
		$bustCm = null,
		$waistInch = null,
		$waistCm = null,
		$drugs = null,
		$hipsInch = null,
		$hipsCm = null,
		$bodyType = null,
		$eyeColor = null,
		$hairColor = null,
		$hairLength = null,
		$ethnicity = null,
		$smoking = null,
		$affiliation = null,
		$piercings = null,
		$role = null,
		$tattoos = null,
		$endowment = null,
		$thickness = null,
		$circumcised = null,
		$serviceType = null,
		$pornstar = null,
		$massage = null,
		$gfe = null,
		$bdsm = null,
		$youtube = null,
		$op = null,
		$createdStamp = null,
		$exportableStamp = null,
		$updatedStamp = null,
		$deletedStamp = null,
		$deletedKeepFeed = null,
		$checkedStamp = null,
		$active = null,
		$activeReason = null,
		$postedStamp = null,
		$postedTwitterStamp = null,
		$expireStamp = null,
		$featured = null,
		$hit = null,
		$disabled = null,
		$disabledReason = null,
		$oldUrl = null,
		$tmpId = null,
		$asUrl = null,
		$asCheckStamp = null,
		$claimEmailSent = null,
		$verifiedStamp = null,
		$id = null,
		$featuredExpireStamp = null,
		$featuredReason = null,
		$phoneVerifiedStamp = null
	) {
		$this->type                = $type;
		$this->name                = $name;
		$this->url                 = $url;
		$this->sponsorDesktop      = $sponsorDesktop;
		$this->sponsorMobile       = $sponsorMobile;
		$this->directLink          = $directLink;
		$this->phone               = $phone;
		$this->phoneToBe           = $phoneToBe;
		$this->email               = $email;
		$this->thumbnail           = $thumbnail;
		$this->thumbnails          = $thumbnails;
		$this->tagline             = $tagline;
		$this->languages           = $languages;
		$this->incall              = $incall;
		$this->outcall             = $outcall;
		$this->incallPrice         = $incallPrice;
		$this->incallPrice30       = $incallPrice30;
		$this->outcallPrice        = $outcallPrice;
		$this->outcallPrice30      = $outcallPrice30;
		$this->men                 = $men;
		$this->women               = $women;
		$this->tstvtg              = $tstvtg;
		$this->couples             = $couples;
		$this->groups              = $groups;
		$this->locationDetail      = $locationDetail;
		$this->aboutme             = $aboutme;
		$this->website             = $website;
		$this->tumblr              = $tumblr;
		$this->facebook            = $facebook;
		$this->twitter             = $twitter;
		$this->instagram           = $instagram;
		$this->ter                 = $ter;
		$this->whatsapp            = $whatsapp;
		$this->line                = $line;
		$this->wechat              = $wechat;
		$this->dob                 = $dob;
		$this->age                 = $age;
		$this->heightFoot          = $heightFoot;
		$this->heightInch          = $heightInch;
		$this->heightCm            = $heightCm;
		$this->weightLb            = $weightLb;
		$this->weightKg            = $weightKg;
		$this->cup                 = $cup;
		$this->bustInch            = $bustInch;
		$this->bustCm              = $bustCm;
		$this->waistInch           = $waistInch;
		$this->waistCm             = $waistCm;
		$this->drugs               = $drugs;
		$this->hipsInch            = $hipsInch;
		$this->hipsCm              = $hipsCm;
		$this->bodyType            = $bodyType;
		$this->eyeColor            = $eyeColor;
		$this->hairColor           = $hairColor;
		$this->hairLength          = $hairLength;
		$this->ethnicity           = $ethnicity;
		$this->smoking             = $smoking;
		$this->affiliation         = $affiliation;
		$this->piercings           = $piercings;
		$this->role                = $role;
		$this->tattoos             = $tattoos;
		$this->endowment           = $endowment;
		$this->thickness           = $thickness;
		$this->circumcised         = $circumcised;
		$this->serviceType         = $serviceType;
		$this->pornstar            = $pornstar;
		$this->massage             = $massage;
		$this->gfe                 = $gfe;
		$this->bdsm                = $bdsm;
		$this->youtube             = $youtube;
		$this->op                  = $op;
		$this->createdStamp        = $createdStamp;
		$this->exportableStamp     = $exportableStamp;
		$this->updatedStamp        = $updatedStamp;
		$this->deletedStamp        = $deletedStamp;
		$this->deletedKeepFeed     = $deletedKeepFeed;
		$this->checkedStamp        = $checkedStamp;
		$this->active              = $active;
		$this->activeReason        = $activeReason;
		$this->postedStamp         = $postedStamp;
		$this->postedTwitterStamp  = $postedTwitterStamp;
		$this->expireStamp         = $expireStamp;
		$this->featured            = $featured;
		$this->hit                 = $hit;
		$this->disabled            = $disabled;
		$this->disabledReason      = $disabledReason;
		$this->oldUrl              = $oldUrl;
		$this->tmpId               = $tmpId;
		$this->asUrl               = $asUrl;
		$this->asCheckStamp        = $asCheckStamp;
		$this->claimEmailSent      = $claimEmailSent;
		$this->verifiedStamp       = $verifiedStamp;
		$this->id                  = $id;
		$this->featuredExpireStamp = $featuredExpireStamp;
		$this->featuredReason      = $featuredReason;
		$this->phoneVerifiedStamp  = $phoneVerifiedStamp;
		$this->location            = $location;
		$this->user                = $user;
		$this->images              = [];
		foreach ($images as $image) {
			$this->images[] = ImageMapper::map($image);
		}
	}

	/**
	 * @return int
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * @return bool
	 */
	public function isSponsorDesktop() {
		return $this->sponsorDesktop;
	}

	/**
	 * @return bool
	 */
	public function isSponsorMobile() {
		return $this->sponsorMobile;
	}

	/**
	 * @return string
	 */
	public function getDirectLink() {
		return $this->directLink;
	}

	/**
	 * @return string
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 * @return string
	 */
	public function getPhoneToBe() {
		return $this->phoneToBe;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getThumbnail() {
		return $this->thumbnail;
	}

	/**
	 * @return string
	 */
	public function getThumbnails() {
		return $this->thumbnails;
	}

	/**
	 * @return string
	 */
	public function getTagline() {
		return $this->tagline;
	}

	/**
	 * @return string
	 */
	public function getLanguages() {
		return $this->languages;
	}

	/**
	 * @return bool
	 */
	public function isIncall() {
		return $this->incall;
	}

	/**
	 * @return bool
	 */
	public function isOutcall() {
		return $this->outcall;
	}

	/**
	 * @return int
	 */
	public function getIncallPrice() {
		return $this->incallPrice;
	}

	/**
	 * @return int
	 */
	public function getIncallPrice30() {
		return $this->incallPrice30;
	}

	/**
	 * @return int
	 */
	public function getOutcallPrice() {
		return $this->outcallPrice;
	}

	/**
	 * @return int
	 */
	public function getOutcallPrice30() {
		return $this->outcallPrice30;
	}

	/**
	 * @return bool
	 */
	public function isMen() {
		return $this->men;
	}

	/**
	 * @return bool
	 */
	public function isWomen() {
		return $this->women;
	}

	/**
	 * @return bool
	 */
	public function isTstvtg() {
		return $this->tstvtg;
	}

	/**
	 * @return bool
	 */
	public function isCouples() {
		return $this->couples;
	}

	/**
	 * @return bool
	 */
	public function isGroups() {
		return $this->groups;
	}

	/**
	 * @return string
	 */
	public function getLocationDetail() {
		return $this->locationDetail;
	}

	/**
	 * @return string
	 */
	public function getAboutme() {
		return $this->aboutme;
	}

	/**
	 * @return string
	 */
	public function getWebsite() {
		return $this->website;
	}

	/**
	 * @return string
	 */
	public function getTumblr() {
		return $this->tumblr;
	}

	/**
	 * @return string
	 */
	public function getFacebook() {
		return $this->facebook;
	}

	/**
	 * @return string
	 */
	public function getTwitter() {
		return $this->twitter;
	}

	/**
	 * @return string
	 */
	public function getInstagram() {
		return $this->instagram;
	}

	/**
	 * @return int
	 */
	public function getTer() {
		return $this->ter;
	}

	/**
	 * @return string
	 */
	public function getWhatsapp() {
		return $this->whatsapp;
	}

	/**
	 * @return string
	 */
	public function getLine() {
		return $this->line;
	}

	/**
	 * @return string
	 */
	public function getWechat() {
		return $this->wechat;
	}

	/**
	 * @return int
	 */
	public function getDob() {
		return $this->dob;
	}

	/**
	 * @return int
	 */
	public function getAge() {
		return $this->age;
	}

	/**
	 * @return int
	 */
	public function getHeightFoot() {
		return $this->heightFoot;
	}

	/**
	 * @return int
	 */
	public function getHeightInch() {
		return $this->heightInch;
	}

	/**
	 * @return int
	 */
	public function getHeightCm() {
		return $this->heightCm;
	}

	/**
	 * @return int
	 */
	public function getWeightLb() {
		return $this->weightLb;
	}

	/**
	 * @return string
	 */
	public function getCup() {
		return $this->cup;
	}

	/**
	 * @return string
	 */
	public function getBustInch() {
		return $this->bustInch;
	}

	/**
	 * @return string
	 */
	public function getBustCm() {
		return $this->bustCm;
	}

	/**
	 * @return string
	 */
	public function getWaistInch() {
		return $this->waistInch;
	}

	/**
	 * @return string
	 */
	public function getWaistCm() {
		return $this->waistCm;
	}

	/**
	 * @return string
	 */
	public function getDrugs() {
		return $this->drugs;
	}

	/**
	 * @return string
	 */
	public function getHipsInch() {
		return $this->hipsInch;
	}

	/**
	 * @return string
	 */
	public function getHipsCm() {
		return $this->hipsCm;
	}

	/**
	 * @return string
	 */
	public function getBodyType() {
		return $this->bodyType;
	}

	/**
	 * @return string
	 */
	public function getEyeColor() {
		return $this->eyeColor;
	}

	/**
	 * @return string
	 */
	public function getHairColor() {
		return $this->hairColor;
	}

	/**
	 * @return string
	 */
	public function getHairLength() {
		return $this->hairLength;
	}

	/**
	 * @return int
	 */
	public function getEthnicity() {
		return $this->ethnicity;
	}

	/**
	 * @return string
	 */
	public function getSmoking() {
		return $this->smoking;
	}

	/**
	 * @return string
	 */
	public function getAffiliation() {
		return $this->affiliation;
	}

	/**
	 * @return string
	 */
	public function getPiercings() {
		return $this->piercings;
	}

	/**
	 * @return string
	 */
	public function getRole() {
		return $this->role;
	}

	/**
	 * @return string
	 */
	public function getTattoos() {
		return $this->tattoos;
	}

	/**
	 * @return string
	 */
	public function getEndowment() {
		return $this->endowment;
	}

	/**
	 * @return string
	 */
	public function getThickness() {
		return $this->thickness;
	}

	/**
	 * @return string
	 */
	public function getCircumcised() {
		return $this->circumcised;
	}

	/**
	 * @return int
	 */
	public function getServiceType() {
		return $this->serviceType;
	}

	/**
	 * @return bool
	 */
	public function isPornstar() {
		return $this->pornstar;
	}

	/**
	 * @return bool
	 */
	public function isMassage() {
		return $this->massage;
	}

	/**
	 * @return mixed
	 */
	public function getGfe() {
		return $this->gfe;
	}

	/**
	 * @return bool
	 */
	public function isBdsm() {
		return $this->bdsm;
	}

	/**
	 * @return string
	 */
	public function getYoutube() {
		return $this->youtube;
	}

	/**
	 * @return string
	 */
	public function getOp() {
		return $this->op;
	}

	/**
	 * @return int
	 */
	public function getCreatedStamp() {
		return $this->createdStamp;
	}

	/**
	 * @return int
	 */
	public function getExportableStamp() {
		return $this->exportableStamp;
	}

	/**
	 * @return int
	 */
	public function getUpdatedStamp() {
		return $this->updatedStamp;
	}

	/**
	 * @return int
	 */
	public function getDeletedStamp() {
		return $this->deletedStamp;
	}

	/**
	 * @return bool
	 */
	public function isDeletedKeepFeed() {
		return $this->deletedKeepFeed;
	}

	/**
	 * @return int
	 */
	public function getCheckedStamp() {
		return $this->checkedStamp;
	}

	/**
	 * @return bool
	 */
	public function isActive() {
		return $this->active;
	}

	/**
	 * @return string
	 */
	public function getActiveReason() {
		return $this->activeReason;
	}

	/**
	 * @return int
	 */
	public function getPostedStamp() {
		return $this->postedStamp;
	}

	/**
	 * @return int
	 */
	public function getPostedTwitterStamp() {
		return $this->postedTwitterStamp;
	}

	/**
	 * @return int
	 */
	public function getExpireStamp() {
		return $this->expireStamp;
	}

	/**
	 * @return bool
	 */
	public function isFeatured() {
		return $this->featured;
	}

	/**
	 * @return int
	 */
	public function getHit() {
		return $this->hit;
	}

	/**
	 * @return bool
	 */
	public function isDisabled() {
		return $this->disabled;
	}

	/**
	 * @return string
	 */
	public function getDisabledReason() {
		return $this->disabledReason;
	}

	/**
	 * @return string
	 */
	public function getOldUrl() {
		return $this->oldUrl;
	}

	/**
	 * @return string
	 */
	public function getTmpId() {
		return $this->tmpId;
	}

	/**
	 * @return string
	 */
	public function getAsUrl() {
		return $this->asUrl;
	}

	/**
	 * @return int
	 */
	public function getAsCheckStamp() {
		return $this->asCheckStamp;
	}

	/**
	 * @return int
	 */
	public function getClaimEmailSent() {
		return $this->claimEmailSent;
	}

	/**
	 * @return int
	 */
	public function getVerifiedStamp() {
		return $this->verifiedStamp;
	}

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return int
	 */
	public function getFeaturedExpireStamp() {
		return $this->featuredExpireStamp;
	}

	/**
	 * @return string
	 */
	public function getFeaturedReason() {
		return $this->featuredReason;
	}

	/**
	 * @return int
	 */
	public function getPhoneVerifiedStamp() {
		return $this->phoneVerifiedStamp;
	}

	/**
	 * @return \App\Util\TS\Model\Location
	 */
	public function getLocation() {
		return $this->location;
	}

	/**
	 * @return \App\Util\TS\Model\User
	 */
	public function getUser() {
		return $this->user;
	}

	/**
	 * @return \App\Util\TS\Model\Image[]
	 */
	public function getImages() {
		return $this->images;
	}

	/**
	 * @return int|null
	 */
	public function getWeightKg() {
		return $this->weightKg;
	}

	/**
	 * @return \App\Util\TS\Model\Image[]
	 */
	public function getTrueImages() {
		$result = [];

		foreach ($this->getImages() as $image) {
			if ($image->getType() === 'I') {
				$result[] = $image;
			}
		}

		return $result;
	}
}