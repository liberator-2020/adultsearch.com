<?php

namespace App\Util\TS\Model;


class User {

	/** @var string */
	private $uname;

	/** @var string */
	private $name;

	/** @var string */
	private $loginType;

	/** @var string */
	private $email;

	/** @var string */
	private $phone;

	/** @var string */
	private $password;

	/** @var boolean */
	private $active;

	/** @var boolean */
	private $banned;

	/** @var string */
	private $banReason;

	/** @var boolean */
	private $provider;

	/** @var int */
	private $level;

	/** @var string */
	private $fullName;

	/** @var string */
	private $confirmationToken;

	/** @var int */
	private $passwordRequestedAt;

	/** @var string */
	private $emailRegister;

	/** @var boolean */
	private $phoneVerified;

	/** @var boolean */
	private $emailVerified;

	/** @var int */
	private $dateOfBirth;

	/** @var string */
	private $find;

	/** @var int */
	private $lastloginStamp;

	/** @var int */
	private $lastloginAgent;

	/** @var string */
	private $facebookId;

	/** @var int */
	private $facebookConnectStamp;

	/** @var string */
	private $facebookShow;

	/** @var string */
	private $googleId;

	/** @var int */
	private $googleConnectStamp;

	/** @var string */
	private $googleShow;

	/** @var string */
	private $instagramId;

	/** @var int */
	private $instagramConnectStamp;

	/** @var string */
	private $instagramNickname;

	/** @var string */
	private $instagramShow;

	/** @var string */
	private $twitterId;

	/** @var int */
	private $twitterConnectStamp;

	/** @var string */
	private $twitterShow;

	/** @var int */
	private $repost;

	/** @var string */
	private $autologinCode;

	/** @var int */
	private $autologinExpireStamp;

	/** @var string */
	private $acquisitionSource;

	/** @var string */
	private $acquisitionMedium;

	/** @var string */
	private $acquisitionCampaign;

	/** @var int */
	private $createdStamp;

	/** @var int */
	private $updatedStamp;

	/** @var int */
	private $deletedStamp;

	/** @var int */
	private $id;

	/** @var float */
	private $budget;

	/**
	 * User constructor.
	 *
	 * @param string $uname
	 * @param string $name
	 * @param string $loginType
	 * @param string $email
	 * @param string $phone
	 * @param string $password
	 * @param bool   $active
	 * @param bool   $banned
	 * @param string $banReason
	 * @param bool   $provider
	 * @param int    $level
	 * @param string $fullName
	 * @param string $confirmationToken
	 * @param int    $passwordRequestedAt
	 * @param string $emailRegister
	 * @param bool   $phoneVerified
	 * @param bool   $emailVerified
	 * @param int    $dateOfBirth
	 * @param string $find
	 * @param int    $lastloginStamp
	 * @param int    $lastloginAgent
	 * @param string $facebookId
	 * @param int    $facebookConnectStamp
	 * @param string $facebookShow
	 * @param string $googleId
	 * @param int    $googleConnectStamp
	 * @param string $googleShow
	 * @param string $instagramId
	 * @param int    $instagramConnectStamp
	 * @param string $instagramNickname
	 * @param string $instagramShow
	 * @param string $twitterId
	 * @param int    $twitterConnectStamp
	 * @param string $twitterShow
	 * @param int    $repost
	 * @param string $autologinCode
	 * @param int    $autologinExpireStamp
	 * @param string $acquisitionSource
	 * @param string $acquisitionMedium
	 * @param string $acquisitionCampaign
	 * @param int    $createdStamp
	 * @param int    $updatedStamp
	 * @param int    $deletedStamp
	 * @param int    $id
	 * @param float  $budget
	 */
	public function __construct(
		$uname,
		$name,
		$email,
		$phone,
		$active,
		$loginType = null,
		$password = null,
		$banned = null,
		$banReason = null,
		$provider = null,
		$level = null,
		$fullName = null,
		$confirmationToken = null,
		$passwordRequestedAt = null,
		$emailRegister = null,
		$phoneVerified = null,
		$emailVerified = null,
		$dateOfBirth = null,
		$find = null,
		$lastloginStamp = null,
		$lastloginAgent = null,
		$facebookId = null,
		$facebookConnectStamp = null,
		$facebookShow = null,
		$googleId = null,
		$googleConnectStamp = null,
		$googleShow = null,
		$instagramId = null,
		$instagramConnectStamp = null,
		$instagramNickname = null,
		$instagramShow = null,
		$twitterId = null,
		$twitterConnectStamp = null,
		$twitterShow = null,
		$repost = null,
		$autologinCode = null,
		$autologinExpireStamp = null,
		$acquisitionSource = null,
		$acquisitionMedium = null,
		$acquisitionCampaign = null,
		$createdStamp = null,
		$updatedStamp = null,
		$deletedStamp = null,
		$id = null,
		$budget = null
	) {
		$this->uname                 = $uname;
		$this->name                  = $name;
		$this->loginType             = $loginType;
		$this->email                 = $email;
		$this->phone                 = $phone;
		$this->password              = $password;
		$this->active                = $active;
		$this->banned                = $banned;
		$this->banReason             = $banReason;
		$this->provider              = $provider;
		$this->level                 = $level;
		$this->fullName              = $fullName;
		$this->confirmationToken     = $confirmationToken;
		$this->passwordRequestedAt   = $passwordRequestedAt;
		$this->emailRegister         = $emailRegister;
		$this->phoneVerified         = $phoneVerified;
		$this->emailVerified         = $emailVerified;
		$this->dateOfBirth           = $dateOfBirth;
		$this->find                  = $find;
		$this->lastloginStamp        = $lastloginStamp;
		$this->lastloginAgent        = $lastloginAgent;
		$this->facebookId            = $facebookId;
		$this->facebookConnectStamp  = $facebookConnectStamp;
		$this->facebookShow          = $facebookShow;
		$this->googleId              = $googleId;
		$this->googleConnectStamp    = $googleConnectStamp;
		$this->googleShow            = $googleShow;
		$this->instagramId           = $instagramId;
		$this->instagramConnectStamp = $instagramConnectStamp;
		$this->instagramNickname     = $instagramNickname;
		$this->instagramShow         = $instagramShow;
		$this->twitterId             = $twitterId;
		$this->twitterConnectStamp   = $twitterConnectStamp;
		$this->twitterShow           = $twitterShow;
		$this->repost                = $repost;
		$this->autologinCode         = $autologinCode;
		$this->autologinExpireStamp  = $autologinExpireStamp;
		$this->acquisitionSource     = $acquisitionSource;
		$this->acquisitionMedium     = $acquisitionMedium;
		$this->acquisitionCampaign   = $acquisitionCampaign;
		$this->createdStamp          = $createdStamp;
		$this->updatedStamp          = $updatedStamp;
		$this->deletedStamp          = $deletedStamp;
		$this->id                    = $id;
		$this->budget                = $budget;
	}


	/**
	 * @return string
	 */
	public function getUname() {
		return $this->uname;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getLoginType() {
		return $this->loginType;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 * @return string
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * @return bool
	 */
	public function isActive() {
		return $this->active;
	}

	/**
	 * @return bool
	 */
	public function isBanned() {
		return $this->banned;
	}

	/**
	 * @return string
	 */
	public function getBanReason() {
		return $this->banReason;
	}

	/**
	 * @return bool
	 */
	public function isProvider() {
		return $this->provider;
	}

	/**
	 * @return int
	 */
	public function getLevel() {
		return $this->level;
	}

	/**
	 * @return string
	 */
	public function getFullName() {
		return $this->fullName;
	}

	/**
	 * @return string
	 */
	public function getConfirmationToken() {
		return $this->confirmationToken;
	}

	/**
	 * @return int
	 */
	public function getPasswordRequestedAt() {
		return $this->passwordRequestedAt;
	}

	/**
	 * @return string
	 */
	public function getEmailRegister() {
		return $this->emailRegister;
	}

	/**
	 * @return bool
	 */
	public function isPhoneVerified() {
		return $this->phoneVerified;
	}

	/**
	 * @return bool
	 */
	public function isEmailVerified() {
		return $this->emailVerified;
	}

	/**
	 * @return int
	 */
	public function getDateOfBirth() {
		return $this->dateOfBirth;
	}

	/**
	 * @return string
	 */
	public function getFind() {
		return $this->find;
	}

	/**
	 * @return int
	 */
	public function getLastloginStamp() {
		return $this->lastloginStamp;
	}

	/**
	 * @return int
	 */
	public function getLastloginAgent() {
		return $this->lastloginAgent;
	}

	/**
	 * @return string
	 */
	public function getFacebookId() {
		return $this->facebookId;
	}

	/**
	 * @return int
	 */
	public function getFacebookConnectStamp() {
		return $this->facebookConnectStamp;
	}

	/**
	 * @return string
	 */
	public function getFacebookShow() {
		return $this->facebookShow;
	}

	/**
	 * @return string
	 */
	public function getGoogleId() {
		return $this->googleId;
	}

	/**
	 * @return int
	 */
	public function getGoogleConnectStamp() {
		return $this->googleConnectStamp;
	}

	/**
	 * @return string
	 */
	public function getGoogleShow() {
		return $this->googleShow;
	}

	/**
	 * @return string
	 */
	public function getInstagramId() {
		return $this->instagramId;
	}

	/**
	 * @return int
	 */
	public function getInstagramConnectStamp() {
		return $this->instagramConnectStamp;
	}

	/**
	 * @return string
	 */
	public function getInstagramNickname() {
		return $this->instagramNickname;
	}

	/**
	 * @return string
	 */
	public function getInstagramShow() {
		return $this->instagramShow;
	}

	/**
	 * @return string
	 */
	public function getTwitterId() {
		return $this->twitterId;
	}

	/**
	 * @return int
	 */
	public function getTwitterConnectStamp() {
		return $this->twitterConnectStamp;
	}

	/**
	 * @return string
	 */
	public function getTwitterShow() {
		return $this->twitterShow;
	}

	/**
	 * @return int
	 */
	public function getRepost() {
		return $this->repost;
	}

	/**
	 * @return string
	 */
	public function getAutologinCode() {
		return $this->autologinCode;
	}

	/**
	 * @return int
	 */
	public function getAutologinExpireStamp() {
		return $this->autologinExpireStamp;
	}

	/**
	 * @return string
	 */
	public function getAcquisitionSource() {
		return $this->acquisitionSource;
	}

	/**
	 * @return string
	 */
	public function getAcquisitionMedium() {
		return $this->acquisitionMedium;
	}

	/**
	 * @return string
	 */
	public function getAcquisitionCampaign() {
		return $this->acquisitionCampaign;
	}

	/**
	 * @return int
	 */
	public function getCreatedStamp() {
		return $this->createdStamp;
	}

	/**
	 * @return int
	 */
	public function getUpdatedStamp() {
		return $this->updatedStamp;
	}

	/**
	 * @return int
	 */
	public function getDeletedStamp() {
		return $this->deletedStamp;
	}

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return float
	 */
	public function getBudget() {
		return $this->budget;
	}

}