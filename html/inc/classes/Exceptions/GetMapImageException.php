<?php

class GetMapImageException extends Exception {
	/**
	 * GetMapImageException constructor.
	 * @param string          $message
	 * @param int             $code
	 * @param \Throwable|null $previous
	 */
	public function __construct(
		$message = 'Returned response is not an image',
		$code = 404,
		\Throwable $previous = null
	) {
		parent::__construct($message, $code, $previous);
	}
}