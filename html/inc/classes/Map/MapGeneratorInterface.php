<?php

interface MapGeneratorInterface {
	/**
	 * @param $latitude
	 * @param $longitude
	 * @return string
	 */
	public function getStaticImageUrlForCoordinates($latitude, $longitude);
}