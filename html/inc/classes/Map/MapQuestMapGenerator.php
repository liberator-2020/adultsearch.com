<?php

require_once(_CMS_ABS_PATH."/inc/classes/Map/MapGeneratorInterface.php");

class MapQuestMapGenerator implements MapGeneratorInterface {

	const BASE_URL = 'https://www.mapquestapi.com/staticmap/v4/';

	const GET_STATIC_PATH = 'getplacemap';

	const DEFAULT_STATIC_ZOOM = 14;
	const DEFAULT_STATIC_SIZE = '260,250';
	const DEFAULT_STATIC_ICON = 'red_1-A';

	/** @var string */
	private $key;

	/**
	 * MapQuestMapGenerator constructor.
	 * @param $key
	 */
	public function __construct($key) {
		$this->key = $key;
	}

	/**
	 * @param int    $latitude
	 * @param int    $longitude
	 * @param int    $zoom
	 * @param string $size
	 * @param string $icon
	 * @return string
	 */
	public function getStaticImageUrlForCoordinates(
		$latitude,
		$longitude,
		$zoom = self::DEFAULT_STATIC_ZOOM,
		$size = self::DEFAULT_STATIC_SIZE,
		$icon = self::DEFAULT_STATIC_ICON
	) {
		$queryData = [
			'location' => "{$latitude},{$longitude}",
			'zoom'     => $zoom,
			'size'     => $size,
			'key'      => $this->key,
			'showicon' => $icon,
		];
		return $this->buildUrl(self::GET_STATIC_PATH, $queryData);
	}

	/**
	 * @param $apiPath
	 * @param $queryData
	 * @return string
	 */
	private function buildUrl($apiPath, $queryData) {
		return self::BASE_URL.$apiPath.'?'.http_build_query($queryData);
	}
}