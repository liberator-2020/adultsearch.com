<?php

class account {

	const _session_var = "account";
	const DEFAULT_ADMIN_IP = '127.0.0.1';

	var $db;

	var $id = NULL;
	var $email = NULL;
	var $account_level = NULL;
	var $username = NULL;
	var $password = NULL;
	var $phone = NULL;
	var $phone_verified = NULL;
	var $ip_address = NULL;
	var $whitelisted = NULL;
	var $approved = NULL;
	var $email_confirmed = NULL;
	var $banned = NULL;
	var $ban_reason = NULL;
	var $repost = NULL;
	var $worker = NULL;
	var $advertiser = NULL;
	var $agency = NULL;
	var $ads_limit = NULL;
	var $unread_messages = NULL;
	var $notes = NULL;
	var $ebiz_id = NULL;
	var $ebiz_secret = NULL;
	var $deleted = NULL;
	var $vip = NULL;
	var $vip_end = NULL;
	var $created_stamp = null;
	var $last_login_stamp = null;
	var $avatar = null;

	//cache
	var $loc_around = NULL;

	//cache
	var $permissions = NULL;

	var $_login_failed = false, $_login_error = NULL;
	var $_cookie_set = false, $_cookie_arr = array();

	/* assigned by sub domain information to be used in every section */ 
	var $core_loc_id = NULL, $core_state_id = NULL, $core_loc_lat, $core_loc_long, $core_loc_type = 0, $core_sub;
	var $country_id = NULL;
	var $core_loc_array = array();
	var $core_neighbor_array = array();
	var $core_host = "adultsearch.com";

	var $module = NULL;
	var $international = 0; // set this 1 if the location is not in US, UK and CA

	function __construct() {
	}
	
	public static function init($db, $quiet = 0) {
		global $smarty;

		$acc = new account();
		
		if ($quiet == 1)
			return $acc;

		$acc->initialize();

		return $acc;
	}

	private function mlog($str) {
		if (self::getRealIp() != "184.162.89.41")
			return;

		$h = @fopen("/www/virtual/adts/www.adultsearch.com/html/account.log", "a");
		if ($h) {
			fwrite($h, "[".date("Y-m-d H:i:s T")."] [".self::getUserIp()."] - {$str}\n");
			fclose($h);
		}
	}

	private function initialize() {
		global $db, $smarty;

		//logoff
		if( isset($_GET["logoff"]) ) {
			ob_end_clean();
			$this->logout();
			$redirect = "http://".$_SERVER["HTTP_HOST"].$_GET["redirect"];
			system::moved($redirect);
		}

		//login from signin form
		if (!$this->isloggedin() && isset($_POST["login"])) {
			$email = GetPostParam("login_email");
			if (empty($email))
				$email = GetPostParam("email");
			$password = GetPostParam("login_password");
			if (empty($password))
				$password = GetPostParam("password");
			
/*
			if (!csrf::check("login")) {
				$this->_login_failed = true;
				$error = "Your session has expired, please fill out the form again.";
				$this->_login_error = $error;
				$smarty->assign("login_error", $error);
			} else {
*/
				$true = $this->login($email, $password, $error);
				if(!$true) {
					$this->_login_failed = true;
					$this->_login_error = $error;
					$smarty->assign("login_error", $error);
				} else {

					//session fixation attack prevention - regenerate session after succesful login from form
					session_regenerate_id();
				
					if (isset($_POST['origin']) && !empty($_POST['origin'])) {
						$origin = $_POST['origin'];
						if (substr($origin, 0 , 4) != "http" && (substr($origin, 0 , 1) != "/" || $origin == "/"))
							$origin = "/account/mypage";
						system::moved($origin);
					}
				}
//			}
		} 

		//login from cookie (remember-me function)
		if( !$this->isloggedin() ) {
			if( isset($_COOKIE['email']) && isset($_COOKIE['p']) && (!isset($_GET['ref']) || $_GET['ref'] != 'signin') ) {
				$ret = $_SERVER['HTTP_HOST'];
				$ret = explode(".", $ret); $ret = strtolower($ret[0]);
				if( !in_array($ret, array("www","adultsearch")) ) {
					$where = "?redirect=".rawurlencode("http://".$_SERVER["HTTP_HOST"].$_SERVER['REQUEST_URI'])."&nowait=1";
					system::moved("http://adultsearch.com/account/signin".$where);
				}
				$loginemail = $this->IsCookieRight();
				if ($loginemail) {
					$true = $this->login($loginemail, NULL, $error, 1);
					if ($true && isset($_GET["redirect"])) {
						system::moved($_GET["redirect"]);
					}
				} else
					$this->removeCookie(1);
			}

			if( isset($_GET['ref']) && $_GET['ref'] != 'signin' ) {
				$this->removeCookie(1);
			}
		}

		//setting some basic variables
		if ($this->isloggedin()) {
			$this->id = intval($_SESSION[self::_session_var]);
			$this->email = $_SESSION["email"];
			$this->account_level = $_SESSION["account_level"];
			$this->vip = $_SESSION["vip"];
			$this->fetch();
			if ($this->getBanned()) {
				//we are banned, logout
				file_log("login", "User #{$this->id} is banned, logging off");
				$this->logout();
				return system::go("/");
			}
			$smarty->assign("ismember", true);
			$smarty->assign("isworker", ($this->isWorker() || $this->isAdmin()));
			$smarty->assign("isadmin", $this->isAdmin());
			$smarty->assign("is_super_admin", $this->isrealadmin());
			$smarty->assign("account_level", $_SESSION["account_level"]); 
			$smarty->assign("vip", $_SESSION["vip"]);
			$smarty->assign("agency", $_SESSION["agency"]); 
		}

		$this->core_host = isset($_SERVER["HTTP_HOST"]) && strstr($_SERVER["HTTP_HOST"],"adultsearch.xxx") ? "adultsearch.xxx" : $this->core_host;
		$smarty->assign("core_host", $this->core_host);

		//login from url (autologin links)
		if (isset($_GET["al"]) && !empty($_GET["al"]) && isset($_GET["acc"])) {
			//either we are not logged in or we "force" autologin (this is used for testing purposes)
			$autologinForce = ($_GET["f"] == "1");
			if (!$this->isloggedin() || $autologinForce) {
				$secret = preg_replace('/[^a-zA-Z0-9]/', '', $_REQUEST["al"]);
				$account = intval($_REQUEST["acc"]);
				$res = $db->q("SELECT a.email
								FROM account_autologin l 
								INNER JOIN account a using (account_id) 
								WHERE l.account_id = ? and l.secret = ?", [$account, $secret]);
				if ($db->numrows($res)) {
					$row = $db->r($res);
					$_POST["saveme"] = true;
					if ($this->login($row[0], NULL, $error, 1)) {
						if (!$autologinForce)
							$db->q("delete from account_autologin where account_id = ? and secret = ?", array($account, $secret));
						system::reload(["al" => "", "acc" => ""]);
					} else {
						reportAdmin("AS : account::initialize() : autologin fails", "", array("account" => $account, "secret" => $secret, "row[0]" => $row[0], "error" => $error));
					}
				}
			}
		}

		//mobile template has login form on every page, if the user is not logged in, generate login csfr token
		global $mobile, $smarty;
		if (!$this->isloggedin() && ($mobile == 1)) {
			$code = csrf::get_html_code("login");
			$smarty->assign("csrf_login", $code);
		}

		//export some account variables into template
		if ($this->isloggedin()) {
			if ($this->isAdvertiser())
				$smarty->assign("advertiser", true);
			else
				$smarty->assign("advertiser", false);
			$smarty->assign("unread_messages", intval($this->getUnreadMessages())); 
		}

		$this->refresh();

		if ($true && $this->isrealadmin())
			system::go("/mng/home");
 	}
	

	public static function findOneById($account_id) {
		global $db;

		if (intval($account_id) == 0)
			return false;

		$res = $db->q("SELECT * FROM account WHERE account_id = ? LIMIT 1", array(intval($account_id)));
		if ($db->numrows($res) != 1)
			return false;
		$row = $db->r($res);

		return self::withRow($row);
	}
	
	public static function findOneByUsername($username) {
		global $db;

		$res = $db->q("SELECT * FROM account WHERE username = ?", array($username));
		if ($db->numrows($res) != 1)
			return false;
		$row = $db->r($res);

		return self::withRow($row);
	}
	
	public static function withRow($row) {
		global $db;

		$acc = new account();
		$acc->setId($row["account_id"]);
		$acc->setEmail($row["email"]);
		$acc->setAccountLevel($row["account_level"]);
		$acc->setUsername($row["username"]);
		$acc->setPassword($row["password"]);
		$acc->setPhone($row["phone"]);
		$acc->setPhoneVerified($row["phone_verified"]);
		$acc->setIpAddress($row["ip_address"]);
		$acc->setWhitelisted($row["whitelisted"]);
		$acc->setApproved($row["approved"]);
		$acc->setEmailConfirmed($row["email_confirmed"]);
		$acc->setBanned($row["banned"]);
		$acc->setBanReason($row["ban_reason"]);
		$acc->setRepost($row["repost"]);
		$acc->setWorker($row["worker"]);
		$acc->setAdvertiser($row["advertiser"]);
		$acc->setAgency($row["agency"]);
		$acc->setAdsLimit($row["ads_limit"]);
		$acc->setUnreadMessages($row["unread_messages"]);
		$acc->setNotes($row["notes"]);
		$acc->setEbizId($row["ebiz_id"]);
		$acc->setEbizSecret($row["ebiz_secret"]);
		$acc->setDeleted($row["deleted"]);
		$acc->setVip($row["vip"]);
		$acc->setVipEnd($row["vip_end"]);
		$acc->setCreatedStamp($row["created_stamp"]);
		$acc->setLastLoginStamp($row["last_login_stamp"]);
		$acc->setAvatar($row["avatar"]);

		return $acc;
	}
	
	public function fetch() {
		global $db;

		if (intval($this->id) == 0)
			return false;

		$res = $db->q("SELECT * FROM account WHERE account_id = ?", [$this->id]);
		if ($db->numrows($res) != 1)
			return false;
		$row = $db->r($res);
		
		$this->setEmail($row["email"]);
		$this->setAccountLevel($row["account_level"]);
		$this->setUsername($row["username"]);
		$this->setPassword($row["password"]);
		$this->setPhone($row["phone"]);
		$this->setPhoneVerified($row["phone_verified"]);
		$this->setIpAddress($row["ip_address"]);
		$this->setWhitelisted($row["whitelisted"]);
		$this->setApproved($row["approved"]);
		$this->setEmailConfirmed($row["email_confirmed"]);
		$this->setBanned($row["banned"]);
		$this->setBanReason($row["ban_reason"]);
		$this->setRepost($row["repost"]);
		$this->setWorker($row["worker"]);
		$this->setAdvertiser($row["advertiser"]);
		$this->setAgency($row["agency"]);
		$this->setAdsLimit($row["ads_limit"]);
		$this->setUnreadMessages($row["unread_messages"]);
		$this->setNotes($row["notes"]);
		$this->setEbizId($row["ebiz_id"]);
		$this->setEbizSecret($row["ebiz_secret"]);
		$this->setDeleted($row["deleted"]);
		$this->setVip($row["vip"]);
		$this->setVipEnd($row["vip_end"]);
		$this->setCreatedStamp($row["created_stamp"]);
		$this->setLastLoginStamp($row["last_login_stamp"]);
		$this->setAvatar($row["avatar"]);

		return $this;
	}

	//getters
	function getId() {
		return $this->id;
	}
	function getEmail() {
		return $this->email;
	}
	function getAccountLevel() {
		return $this->account_level;
	}
	function getUsername() {
		return $this->username;
	}
	function getPassword() {
		return $this->password;
	}
	function getPhone() {
		return $this->phone;
	}
	function getPhoneVerified() {
		return ($this->phone_verified) ? true : false;
	}
	function isPhoneVerified() {
		return ($this->phone_verified) ? true : false;
	}
	function getIpAddress() {
		return $this->ip_address;
	}
	function isWhitelisted() {
		return ($this->whitelisted) ? true : false;
	}
	function isApproved() {
		return ($this->approved) ? true : false;
	}
	function isEmailConfirmed() {
		return ($this->email_confirmed) ? true : false;
	}
	function isBanned() {
		return ($this->banned) ? true : false;
	}
	function getBanned() {
		return ($this->banned) ? true : false;
	}
	function getBanReason() {
		return $this->ban_reason;
	}
	function getRepost() {
		return $this->repost;
	}
	function isWorker() {
		return ($this->worker) ? true : false;
	}
	function isAdvertiser() {
		return ($this->advertiser) ? true : false;
	}
	function getAgency() {
		return ($this->agency) ? true : false;
	}
	function getAdsLimit() {
		return $this->ads_limit;
	}
	function isAgency() {
		return ($this->agency) ? true : false;
	}
	function getUnreadMessages() {
		return $this->unread_messages;
	}
	function getNotes() {
		return $this->notes;
	}
	function getEbizId() {
		return $this->ebiz_id;
	}
	function getEbizSecret() {
		return $this->ebiz_secret;
	}
	function getDeleted() {
		return $this->deleted;
	}
	function getVip() {
		return $this->vip;
	}
	function getVipEnd() {
		return $this->vip_end;
	}
	function getCreatedStamp() {
		return $this->created_stamp;
	}
	function getLastLoginStamp() {
		return $this->last_login_stamp;
	}
	function getAvatar() {
		return $this->avatar;
	}

	//setters
	function setId($id) {
		$this->id = $id;
	}
	function setEmail($email) {
		$this->email = $email;
	}
	function setAccountLevel($account_level) {
		$this->account_level = $account_level;
	}
	function setUsername($username) {
		$this->username = $username;
	}
	function setPassword($password) {
		$this->password = $password;
	}
	function setPlainPassword($plain_password) {
		$this->password = password_hash($plain_password, PASSWORD_BCRYPT);
	}
	function setPhone($phone) {
		$this->phone = $phone;
	}
	function setPhoneVerified($phone_verified) {
		$this->phone_verified = ($phone_verified) ? 1 : 0;
	}
	function setIpAddress($ip_address) {
		$this->ip_address = $ip_address;
	}
	function setWhitelisted($whitelisted) {
		$this->whitelisted = ($whitelisted) ? 1 : 0;
	}
	function setApproved($approved) {
		$this->approved = ($approved) ? 1 : 0;
	}
	function setEmailConfirmed($email_confirmed) {
		$this->email_confirmed = ($email_confirmed) ? 1 : 0;
	}
	function setBanned($banned) {
		$this->banned = ($banned) ? 1 : 0;
	}
	function setBanReason($ban_reason) {
		$this->ban_reason = $ban_reason;
	}
	function setRepost($repost) {
		$this->repost = $repost;
	}
	function setWorker($worker) {
		$this->worker = ($worker) ? 1 : 0;
	}
	function setAdvertiser($advertiser) {
		$this->advertiser = ($advertiser) ? 1 : 0;
	}
	function setAgency($agency) {
		$this->agency = ($agency) ? 1 : 0;
	}
	function setAdsLimit($ads_limit) {
		$this->ads_limit = intval($ads_limit) ?: null;
	}
	function setUnreadMessages($unread_messages) {
		$this->unread_messages = $unread_messages;
	}
	function setNotes($notes) {
		$this->notes = $notes;
	}
	function setEbizId($ebiz_id) {
		$this->ebiz_id = $ebiz_id;
	}
	function setEbizSecret($ebiz_secret) {
		$this->ebiz_secret = $ebiz_secret;
	}
	function setDeleted($deleted) {
		$this->deleted = $deleted;
	}
	function setVip($vip) {
		$this->vip = $vip;
	}
	function setVipEnd($vip_end) {
		$this->vip_end = $vip_end;
	}
	function setCreatedStamp($created_stamp) {
		$this->created_stamp = $created_stamp;
	}
	function setLastLoginStamp($last_login_stamp) {
		$this->last_login_stamp = $last_login_stamp;
	}
	function setAvatar($avatar) {
		$this->avatar = $avatar;
	}

	public function add() {
		global $db, $account;

		$res = $db->q("
			INSERT INTO account 
			(created_stamp, username, password, email, email_register, ip_address, approved
			, repost, email_confirmed, account_level, worker, advertiser, agency, ads_limit, vip, vip_end, avatar) 
			VALUES 
			(?, ?, ?, ?, ?, ?, ?
			, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);",
			[time(), $this->username, $this->password, $this->email, $this->email, $this->ip_address, $this->approved
			, $this->repost, $this->email_confirmed, $this->account_level, $this->worker
			, $this->advertiser, $this->agency, $this->ads_limit, $this->vip, $this->vip_end, $this->avatar]
			);
		$newid = $db->insertid($res);

		if ($newid == 0)
			return false;

		$this->id = $newid;

		audit::log("ACC", "Add", $this->id, "Email: {$this->email}", $account->getId());

		return true;
	}

	function update() {
		global $account, $db;

		if (!$this->id)
			return false;

		$original = account::findOneById($this->id);
		if (!$original)
			return false;

		$update_fields = array();
		$update_agency = false;
		if ($this->email != $original->getEmail())
			$update_fields["email"] = $this->email;
		if ($this->account_level != $original->getAccountLevel())
			$update_fields["account_level"] = $this->account_level;
		if ($this->password != $original->getPassword())
			$update_fields["password"] = $this->password;
		if ($this->phone != $original->getPhone())
			$update_fields["phone"] = $this->phone;
		if ($this->phone_verified != $original->isPhoneVerified())
			$update_fields["phone_verified"] = $this->phone_verified;
		if ($this->ip_address != $original->getIpAddress())
			$update_fields["ip_address"] = $this->ip_address;
		if ($this->whitelisted != $original->isWhitelisted())
			$update_fields["whitelisted"] = $this->whitelisted;
		if ($this->repost != $original->getRepost())
			$update_fields["repost"] = $this->repost;
		if ($this->worker != $original->isWorker())
			$update_fields["worker"] = $this->worker;
		if ($this->advertiser != $original->isAdvertiser())
			$update_fields["advertiser"] = $this->advertiser;
		if ($this->agency != $original->isAgency()) {
			$update_fields["agency"] = $this->agency;
			$update_agency = true;
		}
		if ($this->ads_limit != $original->getAdsLimit())
			$update_fields["ads_limit"] = $this->ads_limit;
		if ($this->unread_messages != $original->getUnreadMessages())
			$update_fields["unread_messages"] = $this->unread_messages;
		if ($this->notes != $original->getNotes())
			$update_fields["notes"] = $this->notes;
		if ($this->ebiz_id != $original->getEbizId())
			$update_fields["ebiz_id"] = $this->ebiz_id;
		if ($this->ebiz_secret != $original->getEbizSecret())
			$update_fields["ebiz_secret"] = $this->ebiz_secret;
		if ($this->deleted != $original->getDeleted())
			$update_fields["deleted"] = $this->deleted;
		if ($this->vip != $original->getVip())
			$update_fields["vip"] = $this->vip;
		if ($this->vip_end != $original->getVipEnd())
			$update_fields["vip_end"] = $this->vip_end;
		if ($this->created_stamp != $original->getCreatedStamp())
			$update_fields["created_stamp"] = $this->created_stamp;
		if ($this->last_login_stamp != $original->getLastLoginStamp())
			$update_fields["last_login_stamp"] = $this->last_login_stamp;
		if ($this->avatar != $original->getAvatar())
			$update_fields["avatar"] = $this->avatar;

		if (empty($update_fields))
			return true;	//nothing to update

		$changed_fields = "";
		$update = "";
		$update_arr = array();
		foreach ($update_fields as $key => $val) {
			$changed_fields .= (empty($changed_fields)) ? "" : ", ";
			$update .= (empty($update)) ? "SET " : ", ";
			$changed_fields .= $key;
			$update .= "{$key} = ?";
			$update_arr[] = $val;
		}
		$update_arr[] = $this->id;
		$update = "UPDATE account {$update} WHERE account_id = ? LIMIT 1";
		
		$db->q($update, $update_arr);
		$aff = $db->affected();

		if (!$aff)
			return false;

		if ($update_agency)
			$this->update_agency();

		audit::log("ACC", "Edit", $this->id, "Changed fields: {$changed_fields}", $account->getId());
		return true;
	}

	private function update_agency() {
		global $db;

		//we are not interested if we are "downgrading" agency to normal user
		if (!$this->isAgency())
			return true;

		//we are "upgrading" normal user to agency
		//dallas 28.9.2018
		//stop all recurring card payments (agencies use advertising budget)

		$res = $db->q(
			"SELECT p.id FROM payment p WHERE p.account_id = ? AND p.result = 'A' AND p.subscription_status = 1 AND p.processor != 'budget'", 
			[$this->id]
			);
		while ($row = $db->r($res)) {
			$payment_id = $row["id"];
			file_log("account", "Upgrading account #{$this->id} to agency -> cancelling payment #{$payment_id}");
			file_log("payment", "Upgrading account #{$this->id} to agency -> cancelling payment #{$payment_id}");
			$error = null;
			$ret = payment::cancel($payment_id, "account upgrade to agency", $error);
			if (!$ret) {
				reportAdmin("AS: account agency upgrade, cant cancel payment", "Upgrading account #{$this->id} to agency, but failed to cancel payment #{$payment_id}");
			}
		}

		return true;		
	}

	/**
	 * removes user account from database
	 */
	public function remove($reason) {
		global $db, $account;

		$audit_who = $account->getId();

		if ($reason) {
			$notes = $this->getNotes();
			$notes .= "\n".date("Y-m-d H:i:s")." Deleted, reason: {$reason}";
			$this->setNotes($notes);
			$ret = $this->update();
		}

		$res = $db->q("UPDATE account SET deleted = ? WHERE account_id = ? LIMIT 1", array(time(), $this->id));
		$aff = $db->affected($res);
		if ($aff == 1) {
			audit::log("ACC", "Remove", $this->id, $reason, $audit_who);
			$return = true;
		} else {
			reportAdmin("AS: account::remove - update failed", "", array("aff" => $aff, "time" => time(), "id" => $this->id));
			$return = false;
		}

		return $return;
	}

	public function undelete() {
		global $db, $account;

		$res = $db->q("UPDATE account SET deleted = NULL WHERE account_id = ? LIMIT 1", array($this->id));
		$aff = $db->affected($res);
		if ($aff == 1) {
			audit::log("ACC", "Undelete", $this->id, "", $account->getId());
			$return = true;
		} else {
			reportAdmin("AS: account::remove - update failed", "", array("aff" => $aff, "id" => $this->id));
			$return = false;
		}

		return $return;
	}

	public function getPermissions() {
		global $db;

		if (is_array($this->permissions))
			return $this->permissions;

		$res = $db->q("SELECT * FROM account_permission WHERE account_id = ?", array($this->id));
		$this->permissions = array();
		while($row = $db->r($res)) {
			$permission_id = $row["permission_id"];
			$this->permissions[] = $permission_id;
		}
		
		return $this->permissions;
	}

	//methods
	public static function IsLoggedIn() {
		if ((!isset($_SESSION[self::_session_var])) || ($_SESSION[self::_session_var] == ""))
			return NULL;
		return intval($_SESSION[self::_session_var]);
	}

	public static function ensure_loggedin() {
		global $account;

		if (!$account->isMember()) {
			$account->asklogin();
			return false;
		}

		return true;
	}

	public static function ensure_admin() {
		global $account;

		if (!self::ensure_loggedin())
			return false;

		return $account->isadmin();
	}

	public static function ensure_real_admin() {
		global $account;

		if (!self::ensure_loggedin())
			return false;

		return $account->isrealadmin();
	}

	function isMember() {
		global $db;

		if ((!isset($_SESSION[self::_session_var])) || ($_SESSION[self::_session_var] == ""))
			return NULL;

		$res = $db->q("select * from account where account_id = ?", array($_SESSION[self::_session_var]));
		if ($db->numrows($res) == 0)
			return NULL;

		return intval($_SESSION[self::_session_var]);
	}

	function getUserDetail($account_id = NULL) {
		global $db;

		if ($account_id == NULL) {
			$account_id = intval($_SESSION[self::_session_var]);
		}
		$account_id = intval($account_id);
		$res = $db->q("select * from account where account_id = ?", array($account_id));
		if ($db->numrows($res) > 0) {
			$row = $db->r($res);
			return $row;
		}
		return NULL;
	}

	function session_setup($row) {
		global $db;

		$_SESSION["impersonate_admin_id"] = false;
		$_SESSION[self::_session_var] = $row["account_id"];
		$_SESSION["username"] = $row["username"];
		$_SESSION["email"] = $row["email"];
		$_SESSION["account_level"] = $row["account_level"];
		$_SESSION["last_visit"] = $row["last_visit"];
		$_SESSION["vip"] = $row["vip"];
		$_SESSION["vip_end"] = $row["vip_end"];
		$_SESSION["agency"] = $row["agency"];
		$this->id = $row["account_id"];
		$this->email = $row["email"];
		$this->account_level = $row["account_level"];
		$this->username = $row["username"];
		$this->password = $row["password"];
		$this->phone = $row["phone"];
		$this->phone_verified = $row["phone_verified"];
		$this->ip_address = $row["ip_address"];
		$this->whitelisted = $row["whitelisted"];
		$this->approved = $row["approved"];
		$this->email_confirmed = $row["email_confirmed"];
		$this->banned = $row["banned"];
		$this->ban_reason = $row["ban_reason"];
		$this->repost = $row["repost"];
		$this->worker = $row["worker"];
		$this->advertiser = $row["advertiser"];
		$this->agency = $row["agency"];
		$this->ads_limit = $row["ads_limit"];
		$this->notes = $row["notes"];
		$this->ebiz_id = $row["ebiz_id"];
		$this->ebiz_secret = $row["ebiz_secret"];
		$this->vip = $row["vip"];
		$this->vip_end = $row["vip_end"];
		$this->created_stamp = $row["created_stamp"];
		$this->last_login_stamp = $row["last_login_stamp"];

		$res = $db->q("select id from classifieds where account_id = ? limit 1", array($row["account_id"]));
		if ($db->numrows($res))
			$_SESSION["classified_poster"] = true; 

		$res = $db->q("select id from advertise_section where account_id = ? limit 1", array($row["account_id"]));
		if ($db->numrows($res))
			$_SESSION["publisher"] = true; 

	}

	function login($email, $password, &$error, $safeMode = 0) {
		global $db, $smarty;

		$params = array($email);
		if (strstr($email, "@")) {
			$where = " a.email = ? ";
		} else {
			$where = " a.username = ? ";
		}

		$res = $db->q("SELECT a.*, last_login_stamp as last_visit FROM account a WHERE $where AND a.deleted IS NULL", $params);
		if (!$db->numrows($res)) {
			$this->Logout();
			$error = "The e-mail or password you entered is incorrect.";
			return false;
		}

		//check if account is banned
		$row = $db->r($res);
		if ($row["banned"]) {
			$this->Logout();
			$error = "The e-mail or password you entered is incorrect.";
			return false;
		}

		if (!password_verify($password, $row["password"]) && !$safeMode) {
			$error = "Password is not correct. <a href='/account/reset' target='_blank'>Click here</a> to reset your password for the email address {$email}";
			return false;
		}

		if( $row["email_confirmed"] == 0 ) {
			system::moved("/account/confirm?member=true&i={$row["account_id"]}");
		}

		$this->session_setup($row);

		if( isset($_POST["saveme"]) ) { 
			$this->_setCookie($row["email"]); 
		}

		$db->q("UPDATE account SET last_login_stamp = ?, ip_address = ? WHERE account_id = ?", [time(), self::getUserIp(), $row["account_id"]]);

		$smarty->assign("ismember", true);
		$smarty->assign("isworker", $this->isWorker());
		$smarty->assign("isadmin", $this->isAdmin());
		$smarty->assign("account_level", $_SESSION["account_level"]);

		file_log("login", "Login successful: account_id={$row["account_id"]}, email={$email}, account_level={$_SESSION["account_level"]}");

		//dallas skype request 23.9.2017 - nobody from SA IP address can login execept rache & conquistador
		//admin 23.3.2018 - disabled
		/*
		$ip_address = preg_replace('/[^0-9\.]/', '', self::getRealIp());
		if ($ip_address) {
			$location = geolocation::getLocationByIp($ip_address);
			$banned_country_id = "41944";   //South Africa
			$allowed_account_ids = [226368, 30220]; // rachel & conquistador
			if ($location["country_id"] == $banned_country_id) {
				if (!in_array($row["account_id"], $allowed_account_ids)) {
					file_log("login", "Banned login: country_id={$location["country_id"]}, account_id={$row["account_id"]}");
					//destroy session (logout)
					$this->logout();
					//and redirect to homepage
					system::moved("/");
				}
			}
		}
		*/

		return true;
	}

	public static function is_impersonated() {

		if (array_key_exists("impersonate_admin_id", $_SESSION) && intval($_SESSION["impersonate_admin_id"]))
			return true;

		return false;
	}

	public static function impersonate($new_account_id, &$error) {
		global $account, $db;

		//check if we are already impersonating
		if (array_key_exists("impersonate_admin_id", $_SESSION) && intval($_SESSION["impersonate_admin_id"])) {
			$error = "We are already impersonating, can not double impersonate.";
			return false;
		}

		//check if we have privileges for impersonating
		if (!$account || !permission::has("account_impersonate")) {
			$error = "Missing privileges for impersonating.";
			return false;
		}

		//lookup the user we want to impersonate
		$res = $db->q("SELECT a.* FROM account a WHERE a.account_id = ? LIMIT 1", [$new_account_id]);
		if (!$db->numrows($res)) {
			$error = "Can't find user account #{$new_account_id} in database.";
			return false;
		}
		$row = $db->r($res);

		if ($row["deleted"] || $row["banned"]) {
			$error = "User account #{$new_account_id} deleted and/or banned, can't impersonate.";
			return false;
		}

		//we can only impersonate users on lower level (so workers cant impersonate admins, but only normal users)
		if ($account->getAccountLevel() <= $row["account_level"] && $account->getId() != 3974) {
			$error = "You can't impersonate this user.";
			return false;
		}

		//all good, store our current account_id, reload the session, set admin_id 
		$admin_id = $account->getId();
		$account->session_setup($row);
		$_SESSION["impersonate_admin_id"] = $admin_id;
	
		return true;
	}

	public static function impersonate_return(&$error) {
		global $account, $db;

		//check if we are really impersonating
		if (!array_key_exists("impersonate_admin_id", $_SESSION) || !intval($_SESSION["impersonate_admin_id"])) {
			$error = "We are not impersonating.";
			return false;
		}

		$admin_id = intval($_SESSION["impersonate_admin_id"]);

		//lookup the admin user we want to return to
		$res = $db->q("SELECT a.* FROM account a WHERE a.account_id = ? LIMIT 1", [$admin_id]);
		if (!$db->numrows($res)) {
			$error = "Can't find admin user account #{$admin_id} in database.";
			return false;
		}
		$row = $db->r($res);

		//all good, reload the admin session
		$account->session_setup($row);
	
		return true;
	}

	function auto_register($email, $username, $password, $email_confirmed, &$error = NULL) {
		global $db;

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$error = "Wrong e-Mail address";
			return -2;
		}

		if( strstr($username, ' ') ) {
			$error = "You may not have space in your user name";
			return -5;
		}

		if( empty($password) ) {
			$error = "Password may not be blank";
			return -3;
		}
	
		if( $this->isbannedForLogin($email) ) {
			$error = "This email may not be used.";
			return -2;
		}

		if( $this->email_inuse($email, true) ) {
			$res = $db->q("select account_id from account where email = ? and password = ?", array($email, $password));
			if ($db->numrows($res)) {
				$row = $db->r($res);
				if ($this->login($email, $password, $error))
					return $row[0]; 
				else {
					$error = "Login failed.";
					return -4;
				}
			} else {
				$error = "An account already exists for the email {$email}.  Reset your password <a href='/account/reset' target='_blank'>here</a>";
				return -1;
			}
		} 

		if( empty($username) ) {
			$username = substr($email, 0, strpos($email, "@"));
			do {
				if( $this->username_taken($username) ) $username .= "1";
			} while($this->username_taken($username));
		} else {
			if( $this->username_taken($username) ) {
				$error = "This user name($username) is taken, please try an other one.";
				return -4;
			}
		}

		return $this->register($email, $username, $password, 1);
	}

	function register($email, $username, $password, $email_confirmed = 0, $advertiser = false) {
		global $db, $config_dev_server;

		if ($email == "" || $password == "" || $username == "")
			return false;

		$username = substr($username, 0, 32);

		$password_encoded = password_hash($password, PASSWORD_BCRYPT);
		if (!$password_encoded) {
			debug_log("account::register Error: password_hash failed !");
			return false;
		}

		//spam check - put it in spammer_check function with strict flag
		if (self::getUserIp() == "2.95.133.30" 
			|| self::getUserIp() == "176.65.114.59"
			|| self::getUserIp() == "78.159.101.115"
			|| self::getUserIp() == "176.65.120.157"
			|| self::getUserIp() == "138.197.161.53"
			|| self::getUserIp() == "163.172.148.169"
			|| self::getUserIp() == "176.65.96.113"
			|| preg_match('/.*\.bizml\.ru$/', $email)
			|| preg_match('/.*@rmailgroup\.in$/', $email)
			|| preg_match('/.*@armail\.in$/', $email)
			|| preg_match('/.*@app-expert\.com$/', $email)
			|| preg_match('/.*@euroweb\.email$/', $email)
			|| preg_match('/.*@mailnowapp\.com$/', $email)
			) {
			file_log("account", "User not registered, because it hit strict spam check: email='{$email}', ip_address='".self::getUserIp()."', password='{$password}', username='{$username}'");
			return 1234567;
		}

		//check that 1 IP address cant register more than 5 times during 24 hours on production environment
		if (!$config_dev_server) {
			$dailyLimit = 5;
			$dailyLimitAgo = time() - 86400;
			$count = $db->single("SELECT COUNT(*) FROM account WHERE ip_address = ? AND created_stamp > ?", [self::getUserIp(), $dailyLimitAgo]);
			if ($count >= $dailyLimit) {
				file_log("account", "User not registered, IP address '".self::getUserIp()."' hit daily limit: email='{$email}', password='{$password}', username='{$username}'");
				return false;
			}
		}

		$db->q("INSERT INTO account 
				(created_stamp, ip_address, last_login_stamp, username, password, email, email_confirmed, advertiser)
				value
				( ?, ?, NULL, ?, ?, ?, ?, ?)", 
				array(time(), self::getUserIp(), $username, $password_encoded, $email, $email_confirmed, ($advertiser) ? 1 : 0)
				);
		$account_id = $db->insertid;

		$error = null;
		if (!$email_confirmed) {
			$hash = $this->_getCookieHash();
			$db->q("INSERT INTO account_confirms (account_id, hash, create_time) values (?, ?, CURDATE())", array($account_id, $hash));
			$this->sendRegistrationEmail($email, $password, $hash, $account_id);
		} else {
			//when registering from adbuild wizard, we dont need account confirmation
			$this->sendRegistrationAdbuildEmail($email, $password);
			$this->login($email, $password, $error);
		}

		$this->spammer_check($account_id, $email, $password);

		return $account_id;
	}

	public function spammer_check($account_id, $email, $password) {
		global $account, $db;

		$spammer = false;
		$reason = null;

		$spammer = spammer::findOneByEmail($email);
		if (!$spammer)
			$spammer = spammer::findOneByEmail(spammer::getDomainPart($email));
		if ($spammer) {
			$email_fragment = $spammer->getEmail();
			$msg = "New account <a href=\"https://adultsearch.com/mng/accounts?account_id={$account_id}\">#{$account_id} - {$email}</a> with email from spam list has just registered.<br /><br />";
			$msg .= "<strong>Please check here</strong> in the list of accounts with this email pattern '{$email_fragment}' if the new account is the same guy:<br />";
			$msg .= "<a href=\"https://adultsearch.com/mng/accounts?email={$email_fragment}\">List of accounts with this email pattern</a><br /><br />";
			$msg .= "If it is the same guy/spammer, you should <strong>refund all his sales</strong> and <strong>ban</strong> him.<br />";
			$msg .= "If it is not spammer, you might want to consider to remove this email pattern from spam list: <a href=\"https://adultsearch.com/mng/admin_spammers?action=remove&email={$email_fragment}\">remove {$email_fragment} from spam list</a>.<br /><br />";
			$msg .= "If you have any questions, contact Jay.<br />";
			send_email([
				"from" => "noreply@adultsearch.com",
				"to" => SUPPORT_EMAIL,
				"cc" => SUPPORT_SUPERVISOR_EMAIL,
				"subject" => "AS: Suspicious account registered",
				"html" => $msg,
				]);
			return;
		}

		if (array_key_exists("REMOTE_ADDR", $_SERVER) && $_SERVER['REMOTE_ADDR'])
			$spammer = spammer::findOneByIp(self::getUserIp());
		if ($spammer) {
			$ip = self::getUserIp();
			$msg = "New account <a href=\"https://adultsearch.com/mng/accounts?account_id={$account_id}\">#{$account_id} - {$email}</a> with ip address from spam list has just registered.<br /><br />";
			$msg .= "<strong>Please check here</strong> in the list of accounts with this IP address '{$ip}' if the new account is the same guy:<br />";
			$msg .= "<a href=\"https://adultsearch.com/mng/accounts?ip={$ip}\">List of accounts with this IP address</a><br /><br />";
			$msg .= "If it is the same guy/spammer, you should <strong>refund all his sales</strong> and <strong>ban</strong> him.<br />";
			$msg .= "If it is not spammer, you might want to consider to remove this IP address from spam list: <a href=\"https://adultsearch.com/mng/admin_spammers?action=remove&ip={$ip}\">remove {$ip} from spam list</a>.<br /><br />";
			$msg .= "If you have any questions, contact Jay.<br />";
			send_email([
				"from" => "noreply@adultsearch.com",
				"to" => SUPPORT_EMAIL,
				"cc" => SUPPORT_SUPERVISOR_EMAIL,
				"subject" => "AS: Suspicious account registered",
				"html" => $msg,
				]);
			return;
		}

		$spammer = spammer::findOneByPassword($password);
		if ($spammer) {
			$msg = "New account <a href=\"https://adultsearch.com/mng/accounts?account_id={$account_id}\">#{$account_id} - {$email}</a> with password from spam list has just registered.<br /><br />";
			$msg .= "<strong>Please check here</strong> in the list of accounts with this password '{$password}' if the new account is the same guy:<br />";
			$msg .= "<a href=\"https://adultsearch.com/mng/accounts?password={$password}\">List of accounts with this password</a><br /><br />";
			$msg .= "If it is the same guy/spammer, you should <strong>refund all his sales</strong> and <strong>ban</strong> him.<br />";
			$msg .= "If it is not spammer, you might want to consider to remove this password from spam list: <a href=\"https://adultsearch.com/mng/admin_spammers?action=remove&password={$password}\">remove {$password} from spam list</a>.<br /><br />";
			$msg .= "If you have any questions, contact Jay.<br />";
			send_email([
				"from" => "noreply@adultsearch.com",
				"to" => SUPPORT_EMAIL,
				"cc" => SUPPORT_SUPERVISOR_EMAIL,
				"subject" => "AS: Suspicious account registered",
				"html" => $msg,
				]);
			return;
		}

		return true;
	}

	public function sendRegistrationEmail($email, $password, $hash, $account_id, $to = NULL) {
		$smarty = GetSmartyInstance();
		$smarty->assign("email", $email);
		$smarty->assign("password", $password);
		$smarty->assign("hash", $hash);
		$smarty->assign("account_id", $account_id);

		if (!$to)
			$to = $email;

		$html = $smarty->fetch(_CMS_ABS_PATH."/templates/account/email/registration.html.tpl");
		$text = $smarty->fetch(_CMS_ABS_PATH."/templates/account/email/registration.txt.tpl");
		$subject = "AdultSearch.com - Account Confirmation";

		return send_email(array(
			"from" => "support@adultsearch.com",
			"to" => $to,
//			"bcc" => ADMIN_EMAIL,
			"subject" => $subject,
			"html" => $html,
			"text" => $text
			));
	}
	
	public function sendRegistrationAdbuildEmail($email, $password, $to = NULL) {
		$smarty = GetSmartyInstance();
		$smarty->assign("email", $email);
		$smarty->assign("password", $password);

		if (!$to)
			$to = $email;

		$html = $smarty->fetch(_CMS_ABS_PATH."/templates/account/email/registration_adbuild.html.tpl");
		$text = $smarty->fetch(_CMS_ABS_PATH."/templates/account/email/registration_adbuild.txt.tpl");
		$subject = "Welcome to AdultSearch, save this email!";

		return send_email(array(
			"from" => "support@adultsearch.com",
			"to" => $to,
//			"bcc" => ADMIN_EMAIL,
			"subject" => $subject,
			"html" => $html,
			"text" => $text
			));
	}

	function sendConfirmation($account_id) {
		global $db;

		$res = $db->q("SELECT email, password FROM account WHERE account_id = ?", array($account_id));
		if (!$db->numrows($res))
			return false;

		$row = $db->r($res);
		$email = $row["email"];
		$password = $row["password"];

		$hash = $this->_getCookieHash();
		$db->q("insert into account_confirms (account_id, hash, create_time) values ('$account_id', '$hash', curdate())");

		$this->sendRegistrationEmail($email, $password, $hash, $account_id);
	}

	function username_taken($username) {
		global $db;

		$sql = "";
		$params = array();

		if ($this->IsLoggedIn()) {
			$sql = "account_id != ? AND";
			$params[] = $this->IsLoggedIn();
		}

		$params[] = $username;
		$res = $db->q("SELECT account_id FROM account WHERE $sql username = ?", $params);
		if ($db->numrows($res)) {
			return true;
		}
		return false;
	}

	function email_inuse($email, $except_myself = false) {
		global $db;

		$sql = "";
		$params = array();

		if ($except_myself && $this->IsLoggedIn()) {
			$sql = "account_id != ? AND";
			$params[] = $this->IsLoggedIn();
		}

		$params[] = $email;
		$res = $db->q("SELECT account_id FROM account WHERE $sql email = ?", $params);
		if ($db->numrows($res)) {
			return true;
		}

		return false;
	}

	function Logout() {
		global $db;

		$this->id = NULL;
		$this->email = NULL;
		$this->account_level = NULL;
		$this->password = NULL;
		$this->removeCookie(0);

		if (is_array($_SESSION) && array_key_exists(self::_session_var, $_SESSION))
			$db->q("delete from account_usersonline where account_id = ?", array(intval($_SESSION[self::_session_var])));

		unset($_SESSION[self::_session_var]);
		unset($_SESSION["username"]);
		unset($_SESSION["email"]);
		unset($_SESSION["vip_member"]);
		unset($_SESSION["account_level"]);
		unset($_SESSION["agency"]);
	}

	function _setCookie($email) {
		global $db;

		$old_cookie = NULL;
		if( isset($_COOKIE["p"]) ) {
			$old_cookie = addslashes($_COOKIE["p"]);
		}
		$db->q("delete from account_cookie where lastused < date_sub(now(), interval 30 day)");

		$p = $this->_getCookieHash();
		if (setcookie('email', $email, time()+2592000, '/', '.adultsearch.com', false, true) 
			&& setcookie('p', $p, time()+2592000, '/', '.adultsearch.com', false, true) ) {
			$db->q("insert into account_cookie (email, p, lastused) values (?, ?, now())", array($email, $p));
			if ($old_cookie && strcmp($old_cookie, $p))
				$db->q("delete from account_cookie where email = ? and p = ?", array($email, $old_cookie));
		}
	}

	function setcookie($what, $p, $day = 30, $host = null) {
		global $config_domain;

		if (!$host)
			$host = $config_domain;

		//store also in SESSION
		$_SESSION[$what] = $p;

		if (in_array($what, $this->_cookie_arr))
			return;

		$time = time()+($day*60*60*24);

		setcookie($what, $p, $time, '/', $host, false, true);

		$this->_cookie_arr[] = $what;
	}

	function getcookie($what, $cookieonly = 0) {
		if (!$cookieonly && isset($_SESSION[$what]))
			return $_SESSION[$what];
		if (isset($_COOKIE[$what]))
			return addslashes($_COOKIE[$what]);
		return NULL;
	}

	function corestate($force=0) {
		global $smarty, $mobile;

		if ($this->core_state_id)
			return $this->core_state_id;

		if ($this->core_loc_id)
			return $this->core_loc_type == 3 ? $this->core_loc_array['loc_parent'] : $this->core_loc_array['loc_id'];

		if ($this->getsub() != "www") {
			if ($_COOKIE['def_loc_id']) {
				return intval($_COOKIE['def_loc_id']);
			}
		}

		if (!$force)
			return NULL;

		if( $mobile == 1 ) {
			$smarty->display(_CMS_ABS_PATH."/templates/mobile/location/location_home_mobile.tpl");
			return NULL;
		}

		system::moved("http://{$this->core_host}");
	}

	function isCookieRight() {
		global $db;

		if( isset($_COOKIE['email']) && isset($_COOKIE['p']) ) {
			$email = htmlspecialchars($_COOKIE['email'], ENT_QUOTES);
			$p = htmlspecialchars($_COOKIE['p'], ENT_QUOTES);
		} else
			return NULL;

		$res = $db->q("select * from account_cookie where email = '$email' and p = '$p' limit 1");
		if ($db->numrows($res)) {
			$sql = $db->r($res);
			$email = $sql["email"];
			$this->_setCookie($email);
			return $email;
		}
		return NULL;
	}	

	function removeCookie($force = 0) {
		global $db;

		if( isset($_COOKIE["p"]) ) {
			$email = GetCookieParam("email");
			$p = GetCookieParam("p");
			$db->q("delete from account_cookie where email = '$email' and p = '$p'");
		}
		setcookie('email', "", time()-864000, '/', '.adultsearch.com', false, true);
		setcookie('p', "", time()-864000, '/', '.adultsearch.com', false, true);
	}

	public static function _getCookieHash($len = 20, $digit = 0) {
		if ($digit)
			$al = '0123456789';
		else
			$al = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
		$p = "";
		for ($index = 1; $index <= $len; $index++) {
			$randomNumber = rand(1,strlen($al));
			$p .= substr($al,$randomNumber-1,1);
		}
		return $p;
	}

	function isAdmin() {
		if( $this->IsLoggedIn() && isset($_SESSION["account_level"]) && $_SESSION["account_level"] > 1) {
			return true;
		} 
		return false;
	}

	function isrealadmin() {
		if ($this->IsLoggedIn() && 
			isset($_SESSION["account_level"]) && 
			$_SESSION["account_level"] > 2 && 
			($_SESSION[self::_session_var] == 3 || $_SESSION[self::_session_var] == 3974)
		   ) {
			return true;
		} 
		return false;
	}

	/**
	 * @return string|null
	 */
	public static function getRealIp() {

		//cloudflare & other forwarding proxy support
		$ip = null;
		if (array_key_exists("HTTP_TRUE_CLIENT_IP", $_SERVER))
			$ip = $_SERVER["HTTP_TRUE_CLIENT_IP"];
		else if (array_key_exists("HTTP_CF_CONNECTING_IP", $_SERVER))
			$ip = $_SERVER["HTTP_CF_CONNECTING_IP"];
		else if (array_key_exists("HTTP_X_FORWARDED_FOR", $_SERVER))
			$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		else if (array_key_exists("REMOTE_ADDR", $_SERVER))
			$ip = $_SERVER["REMOTE_ADDR"];

		$ip = preg_replace('/[^0-9a-zA-Z\.:]/', '', $ip);

		return $ip;
	}
	/**
	 * @return string|null
	 */
	public static function getUserIp() {
		if (isset($_SESSION['account'], $_SESSION['account_level']) 
			&& ($_SESSION['account'] !== '')
			&& ($_SESSION['account_level'] > 1)
		) {
			return self::DEFAULT_ADMIN_IP;
		}

		return self::getRealIp();
	}

	public function isVip() {
		if ($this->vip && $this->vip_end < time())
			return true;
		return false;
	}

	public static function isImpersonatedAdmin() {
		return self::is_impersonated();
	}

	function asklogin($message = null) {

		if (!array_key_exists("HTTP_HOST", $_SERVER) || !array_key_exists("REQUEST_URI", $_SERVER))
			system::go("/");
		$current_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER["HTTP_HOST"]}{$_SERVER["REQUEST_URI"]}";

		$login_url = "/account/signin?origin=".urlencode($current_url);
		if ($message)
			$login_url .= "&message=".urlencode($message);

		system::go($login_url);
		die;
	}

	function askverify() {

		if (!array_key_exists("HTTP_HOST", $_SERVER) || !array_key_exists("REQUEST_URI", $_SERVER))
			system::go("/");
		$current_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER["HTTP_HOST"]}{$_SERVER["REQUEST_URI"]}";

		$verify_url = "/account/verify?origin=".urlencode($current_url);

		system::go($verify_url);
		die;
	}

	function checkMainHost() {
		$ret = $_SERVER['HTTP_HOST'];
		$ret = explode(".", $ret);
		$ret = strtolower($ret[0]);
		if( !in_array($ret, array("www","adultsearch")) ) {
			$where = $_SERVER["REQUEST_URI"][0] == "/" ? $_SERVER["REQUEST_URI"] : ("/".$_SERVER["REQUEST_URI"]);
			system::moved("http://adultsearch.com".$where);
		}
	}

	function clearForumByUser($user2) {
		global $db;

		/* how many post deleted ? */
		$count = 0;
		$res = $db->q("SELECT * FROM `forum_post` WHERE account_id = ?", array($user2));
		if (!$db->numrows($res))
			return 0;

		$otheruserpost_ids_sql = '';
		while ($row = $db->r($res))	{
			$otheruserpost_ids_sql .= (( !empty($otheruserpost_ids_sql) ) ? ',' : '') . $row["post_id"];
			$db->q("update forum_topic set topic_replies=topic_replies-1 where topic_id  = ".$row["topic_id"]." limit 1");
			$count++;
		}

		if (empty($otheruserpost_ids_sql))
			return 0;

		$db->q("DELETE FROM `forum_post` WHERE post_id IN (". $otheruserpost_ids_sql .")");

		$res = $db->q("SELECT * FROM forum_topic where topic_last_post_id IN (". $otheruserpost_ids_sql .") and topic_replies>=0");
		if ($db->numrows($res)) {
			$last_post = '';
			while ($row = $db->r($res)) {
				 $db->q("UPDATE forum_topic 
							SET topic_last_post_id = (SELECT post_id from forum_post where topic_id=".$row["topic_id"]." order by post_id desc limit 1 ) 
							WHERE topic_id=".$row["topic_id"]." 
							LIMIT 1");
			}
		}
		$db->q("delete t, p from forum_topic t inner join forum_post p on t.topic_id = p.topic_id WHERE (t.account_id = ?)", array($user2));

		return $count;
	}

	function getsub() {
		$ret = $_SERVER['HTTP_HOST'];
		$ret = explode(".", $ret);
		$ret = strtolower($ret[0]);
		if( in_array($ret, array("www","adultsearch")) ) {	
			return "www";
		}
		return $ret;
	}

	function accountOpt($account_id = null, $loginsession = 0) {
		global $db;

		if (!$account_id)
			$account_id = $this->id;
		if (!$account_id)
			return false;

		$opt = [];
		$res = $db->q("SELECT opt, x FROM account_opt WHERE account_id = ? ".($loginsession ? " AND loginsession = 1" : ""), [$account_id]);
		while ($row = $db->r($res))
			$opt[$row["x"]] = (int)$row["opt"];
		return $opt;
	}

	function refresh() {
		global $db;

		if( ($account_id = $this->isLoggedin()) && (!isset($_SESSION['refresh']) || $_SESSION['refresh'] < (time()-60) ) ) {
			$provider = isset($_SESSION["classified_poster"]) ? 1 : 0;
			$db->q("delete from account_usersonline where lastactivity < (unix_timestamp(now())-60*5)");
			$db->q("insert into account_usersonline 
					(account_id, lastactivity, member, provider) 
					values 
					('$account_id', unix_timestamp(now()), 1, '$provider') 
					on duplicate key update lastactivity=unix_timestamp(now()), provider = '$provider'");
			$_SESSION['refresh'] = time();
		} 
	}

	function findDefLoc() {
		global $db, $config_dev_server;

		if ($this->core_loc_type != 1)
			return $this->core_loc_id;

		$loc_id = $this->getcookie("def_loc_id", 1);
		if (!$loc_id) {
			if ($this->core_loc_id)
				return $this->core_loc_id;
			return NULL;
		} 

		if ($this->core_loc_id != $loc_id) {
			$res = $db->q("select country_sub, loc_url, loc_type from location_location where loc_id = '$loc_id'");
			if (!$db->numrows($res))
				return NULL;
			$row = $db->r($res);
			if ($row['loc_type'] > 1) {
				if (!strstr($_SERVER['REQUEST_URI'], '?'))
					$_SERVER['REQUEST_URI'] = NULL;
				elseif ($_SERVER['REQUEST_URI'][0] == "/")
					$_SERVER['REQUEST_URI'] = substr($_SERVER['REQUEST_URI'], 1);
				$sub = !empty($row['country_sub']) ? "{$row['country_sub']}." : "";
				$url="http://{$sub}".(($config_dev_server) ? "dev." : "").$this->core_host.$row[1];
				system::moved($url);
			}
		}

		if ($this->core_loc_id)
			return $this->core_loc_id;

		if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == '/homepage')
			return NULL;

		$loc_id = $this->getcookie("def_loc_id", 1);
		if ($loc_id) {
			$res = $db->q("select country_sub, loc_url from location_location where loc_id = '$loc_id'");
			if (!$db->numrows($res))
				return NULL;
			$row = $db->r($res);
			if ($row['loc_type'] > 1) {
				if (!strstr($_SERVER['REQUEST_URI'], '?'))
					$_SERVER['REQUEST_URI'] = NULL;
				elseif ($_SERVER['REQUEST_URI'][0] == "/")
					$_SERVER['REQUEST_URI'] = substr($_SERVER['REQUEST_URI'], 1);
				$sub = !empty($row['country_sub']) ? "{$row['country_sub']}." : "";
				system::moved("http://{$sub}".$this->core_host.$row[1]);
			}
		}

		return NULL;
	}

	/**
	 * ban account by account_id (mandatory parameter)
	 */
	function banAccount($account_id = NULL) {
		global $db;

		if (is_null($account_id))
			return false;
		$account_id = intval($account_id);

		$res = $db->q("UPDATE account SET banned = 1 WHERE account_id = ? LIMIT 1", array($account_id));

		return true;		
	}
	
	/**
	 * ban account
	 */
	function ban($reason = null) {
		global $account, $db;

		if (is_null($this->id))
			return false;

		if ($this->getId() == $account->getId()) {
			//user cant ban himself
			flash::add(flash::MSG_ERROR, "You can't ban yourself.");
			return false;
		}

		if ($this->isBanned()) {
			flash::add(flash::MSG_WARNING, "Account #{$this->id} {$this->getEmail()} is already banned!");
			return true;
		}
	
		if ($this->getAccountLevel() > 2) {
			//superadmin cant be banned
			flash::add(flash::MSG_ERROR, "You can't ban superadmin.");
			return false;
		}

		//$res = $db->q("UPDATE account SET banned = 1 WHERE account_id = ? LIMIT 1", array($this->getId()));
		$update = "banned = 1";
		$params = [];
		if (!$this->ban_reason && ($reason == "spammer" || $reason == "chargeback" || substr($reason, 0, 9) == "integrate")) {
			$update .= ", ban_reason = ?";
			$params[] = $reason;
		}
		$params[] = $this->id;
		$res = $db->q("UPDATE account SET {$update} WHERE account_id = ? LIMIT 1", $params);
		audit::log("ACC", "Ban", $this->id, "Reason: {$reason}", $account->getId());

		return true;		
	}
	
	/**
	 * unban account
	 */
	function unban() {
		global $db;

		if (is_null($this->id))
			return false;

		global $account;
		if ($this->getId() == $account->getId()) {
			//user cant unban himself
			flash::add(flash::MSG_ERROR, "You can't unban yourself.");
			return false;
		}

		$res = $db->q("UPDATE account SET banned = 0 WHERE account_id = ? LIMIT 1", array($this->getId()));

		return true;		
	}

	//used in login fucntion, uses also ip, not only email !!!!
	function isBannedForLogin($email) {
		global $db;

		$ip = self::getUserIp();

		//9.7.2015 jay disabling for now, mark complaining it wont register them accounts (due to some banned account has the ip of the office)
		//31.7.2015 jay enabling again - we need to fight spammers
		//$res = $db->q("SELECT banned FROM account WHERE email = '$email' or ip_address = '$ip' ORDER BY banned DESC LIMIT 1");
		$res = $db->q("SELECT banned FROM account WHERE email = ? ORDER BY banned DESC LIMIT 1", [$email]);

		if (!$db->numrows($res))
			return false;	//not in account yet (probably trying to register)
		$row = $db->r($res);
		$banned = $row["banned"];
		if ($banned)
			return true;
		
		return false;
	}

	public function getMngUrl() {
		return "http://adultsearch.com/mng/accounts?account_id={$this->id}";
	}

	function locAround($mile = 10) {
		global $sphinx;

		if (is_array($this->loc_around)) {
			return $this->loc_around;
		}
		$nloc = NULL;
		if( $this->core_loc_id && $this->core_loc_type > 2) {
			/* orange county */
			if( $this->core_loc_id == 42354 )
				return array(2723, 2730, 2731, 2732, 2733, 2734, 2736, 2737, 2738, 2742, 2743, 2747, 2749, 2752, 2726, 33716, 42354);
			//NYC
			/*
			elseif( $this->core_loc_id == 18308 )
				return array(18308, 42838, 18310, 11441, 42370, 42841, 42842, 42840);
			*/
			/* toronto */
			elseif( $this->core_loc_id == 16059 )
				return array(16059, 38889, 38890, 38887, 16167);

			/*
			$sphinx->reset();
			$sphinx->SetLimits(0, 10, 10);
			$sphinx->SetGeoAnchor('loc_lat', 'loc_long', deg2rad($this->core_loc_array['loc_lat']), deg2rad($this->core_loc_array['loc_long']));
			$sphinx->SetFilterFloatRange('@geodist', 0, ($mile*1609.344));
			$sphinx->SetFilter('@id', array($this->core_loc_id), true);
			$sphinx->SetSortMode(SPH_SORT_EXTENDED, "@geodist ASC");
			$sphinx->SetFilter('loc_parent', array($this->core_loc_array['loc_parent']));
			$resultx = $sphinx->Query('', 'home');
			$result = $resultx["matches"];
			foreach($result as $r) {
				$nloc[] = $r["id"];
			}
			*/
			$nloc[] = $this->core_loc_id;
		} elseif ($this->core_loc_id) {
			$nloc[] = $this->core_loc_id;
		}
		$this->loc_around = $nloc;
		return $nloc;
	}

	function hasPermission($module = '') {
		global $db, $gModule;

		$account_id = $this->isloggedin();
		$res = $db->q("select account_id from account_permission_cat where account_id = '$account_id' and (module like '$gModule' or module like '$module') limit 1");
		if (!$db->numrows($res)) {
			return false;
		}
		return true;
	}

	public function createautologin($account_id, $link) {
		global $db;

		$secret = $this->_getCookieHash(20);
		$db->q("delete from account_autologin where time < date_sub(now(), interval 15 day)");
		$db->q("insert into account_autologin (account_id, secret, time) values (?, ?, now())", array($account_id, $secret));

		$link .= (strpos($link, "?") !== false) ? "&" : "?";
		$link .= "al={$secret}&acc={$account_id}";	

		return $link;
	}

	public static function email_obfuscate($email) {
		$at = strpos($email, "@");
		if ($at === false)
			return "???";

		$before = substr($email, 0, $at);

		if (strlen($before) < 3)
			return "??".substr($email, $at);

		return substr($before, 0, 1).str_pad("", strlen($before)-2, "?").substr($before, strlen($before)-1).substr($email, $at);
	}

	/**
	 * Returns phone in normalized format, assuming local number to be in US/CA and adding +1 to them.
	 *
	 * 
	 * @return string
	 */
	public function getNormalizedPhone() {
		$phone = preg_replace('/[^0-9\+]/', '', $this->phone );

		if( strpos($phone,'+')!==0 ) {
			return '+1' . $phone;
		} else {
			return $phone;
		}
	}
}

