<?php

class advertise {

	private $test_zone_id = NULL;
	private $test_campaign_id = NULL;

	public static function init() {
		global $smarty, $db, $account;

		$smarty->assign("noshare", true);
		$smarty->assign("nobanner", true);

		//setting up publisher flag in session
		if ($account->IsLoggedIn() && !isset($_SESSION["advertise_publisher"])) {
			if ($account->isadmin()) {
				$_SESSION["advertise_publisher"] = true;
			} else {
				$res = $db->q("SELECT id FROM advertise_section WHERE account_id = ? LIMIT 1", array($account->getId()));
				if ($db->numrows($res) > 0)
					$_SESSION["advertise_publisher"] = true;
				else
					$_SESSION["advertise_publisher"] = false;
			}
		}
	}

	public static function isPublisher() {
		if (isset($_SESSION["advertise_publisher"]))
			return $_SESSION["advertise_publisher"];
		return false;
	}

	// exports sid for text ads on adultsearch (template page_ads_2.tpl)
	// loc_id is not used anymore (used to be used for "dynamic ads")
	function showAd($sid, $loc_id = null) {
		global $smarty;
		
		if (!$sid)
			return;
		$smarty->assign("as_sid", $sid);
		return;
	}

	/**
	 * $type is "popup" or "interstitial"
	 */
	function debug($type, $zone_id, $conclusion) {
		return true;

		if ($type == "popup") {
//			if (isset($_SESSION["debug_popup"]))
//				return true;
			$_SESSION["debug_popup"] = true;
		} else if ($type == "interstitial") {
//			if (isset($_SESSION["debug_interstitial"]))
//				return true;
			$_SESSION["debug_interstitial"] = true;
		}

		$datetime = date("Y-m-d H:i:s T");
		$ip = account::getUserIp();
		$agent = $_SERVER["HTTP_USER_AGENT"];
		$message = "\"{$datetime}\",\"{$ip}\",\"{$agent}\",\"{$conclusion}\"\n";

		$filename = "adv_zone_{$zone_id}.log";
		$filepath = _CMS_ABS_PATH."/".$filename;
		$h = @fopen($filepath, "a");
		if ($h) {
			fputs($h, $message);
			fclose($h);
			return true;
		}
		return false;
	}

	function popup($s_id = NULL) {
		global $db, $smarty, $mobile, $account, $sphinx, $ctx;

		$now = time();
		$ip = account::getUserIp();

		if (is_null($s_id)) {
			if ($mobile == 1)
				$s_id = 10086;
			else
				$s_id = 79;
		}
		$this->debug("popup",$s_id, "{$s_id}");

		if (is_bot()) {
			$this->debug("popup",$s_id, "N bot");
			return;
		}

		//29.1.2019 - Popunder is only enabled on real browsers except of chrome (firefox, edge, msie, samsungbrowser, safari, opera)
		//We can't pop on chrome, because of Chrome Abusive Experience Report enforcement - Google will put site on blacklist and block all ads
		//30.5.2019 - Zach: enable again pops on chrome - commenting out the code
		//16.7.2019 - Zach disable pop again on chrome
		$browser = get_browser_name();
		if (!in_array($browser, ["Firefox", "Edge", "MSIE", "SamsungBrowser", "Safari", "Opera"])) {
			$this->debug("popup",$s_id, "N chrome");
			return;
		}

		//dallas: 14.07.2018 signal - no pops on AS for people from NYC when browsing NYC
		$geolocation = $ctx->getGeoLocation();
		$smarty->assign("popup_decision", $geolocation["city_name"]);
		if (($s_id == 79 || $s_id == 10086)
			&& is_array($geolocation) 
			&& array_key_exists("city_name", $geolocation) 
			&& $geolocation["city_name"] == "New York City"
			&& $ctx->city_name == "New York City"
			&& $ctx->state_name == "New York") {
			$this->debug("popup",$s_id, "N NYC");
			return;
		}
	
		$force = 0;
		if( isset($_GET['showpopup']) && $account->isrealadmin() ) $force = 1;

		//for AS zones:
		//if we are logged in or if we cant specify IP address ---> dont display popup ad
		if (($s_id == 79 || $s_id == 10086) && ($account->isloggedin() || empty($ip))) {
			if (!$force) {
				$this->debug("popup",$s_id, "N loggedin|noip");
				return;
			}
		}

		//check if we didnt diplay popup already today for this IP address	
		$_SESSION["popupstupidad"] = true;
		if( !$force ) {
			$res = $db->q("select ip_address from popup_ip where ip_address = ? AND s_id = ?", [$ip, $s_id]);
			if ($db->numrows($res)) {
				$this->debug("popup",$s_id, "N ip");
				return;
			}
		}

		$db->q("INSERT IGNORE INTO popup_ip (s_id, ip_address, stamp) values (?, ?, ?)", [$s_id, $ip, time()]);

		//get zone information
		$exclusive_access = [];
		$res = $db->q("SELECT exclusive_access FROM advertise_section WHERE id = ?", [$s_id]);
		if (!$db->numrows($res)) {
			$this->debug("popup",$s_id, "N cant find zone '{$s_id}'");
			return;
		}
		$row = $db->r($res);
		if ($row["exclusive_access"]) {
			if (strpos($row["exclusive_access"], ",") !== false)
				$exclusive_access = explode(",", $row["exclusive_access"]);
			else
				$exclusive_access[] = $row["exclusive_access"];
		}

		$sphinx_index = "advertise";
		$sphinx->reset();
		$sphinx->SetFilter('s_id', array($s_id));
		if (!empty($exclusive_access))
			$sphinx->SetFilter('account_id', $exclusive_access);

		//------------------------------
		//hardcoded ratios for AS zones
		/*
		$mod = $now % 8;
		if ($s_id == 79) {
			//50% for ruby and 50% for AFF
			//2018-07-14 - found out that aff paused their desktop pop campaign -> all zone to ruby
			//if ($mod < 4)
			//	$sphinx->SetFilter('account_id', [109226]);
			//else
			//	$sphinx->SetFilter('account_id', [400344]);
		}
		if ($s_id == 10086) {
			//50% for ruby and 50% for AFF
			if ($mod < 4)
				$sphinx->SetFilter('account_id', [109226]);
			else
				$sphinx->SetFilter('account_id', [400344]);
		}
		*/
		//------------------------------

		$sphinx->SetLimits( 0, 2, 2);
		$sphinx->SetSortMode(SPH_SORT_ATTR_DESC, "bid");
		$results = $sphinx->Query ( "", $sphinx_index );

		$total = $results["total_found"];
		$result = $results["matches"];

		if (!$total) {
			//jay - if there is no advertising campaign for this section, check if there is backup default URL
			$res = $db->q("select backuphtml from advertise_section where id = '".intval($s_id)."'");
			if (!$db->numrows($res)) {
				$this->debug("popup",$s_id, "N no campaign, no backup");
				return;
			}
			$row = $db->r($res);
			$backup_url = $row["backuphtml"];
			if (!empty($backup_url)) {
				//$smarty->assign("popup_url", $backup_url);
				$smarty->assign("url", $backup_url);
				$smarty->assign("s_id", $s_id);
				$this->debug("popup",$s_id, "N no campaign, backup");
			} else {
				$this->debug("popup",$s_id, "N no campaign, no backup 2");
			}
			return;
		}

		$ids = NULL;
		foreach($result as $res) {
			$ids .= $ids ? (",".$res["id"]) : $res["id"];
		}
		if (is_null($ids)) {
			$this->debug("popup",$s_id, "N campaign, no ids");
			return;
		}

		//select the campaign ids and bids for placements from sphinx index
		$res = $db->q("SELECT s.id, s.bid, s.s_id, s.c_id
						FROM advertise_camp_section s
						WHERE s.id in ($ids)
						ORDER BY field(s.id,$ids)");
		if (!$db->numrows($res)) {
			$this->debug("popup",$s_id, "N camp_section not found");
			return;
		}
		//it can still happen that we have multiple campaigns with the same bid
		//first store in array all campaigns with maximum bid
		$max_bid = NULL; $c = 0; $max_bid_items = [];
		while($row = $db->r($res)) {
			$bid = $row["bid"];
			if ($max_bid == NULL)
				$max_bid = $bid;
			if (abs($max_bid - $bid) < 0.001) {
				//this is (one of) max bid campaigns
				$max_bid_items[] = [
					"id" => $row["id"],
					"cid" => $row["c_id"],
					"hbid" => $row["bid"],
					"s_id" => $row["s_id"],
					];
			}
		}
		//if there is more campaigns with the same max bid, choose random one from them
		if (count($max_bid_items) > 1) {
			$rkey = array_rand($max_bid_items);
			$max_bid_item = $max_bid_items[$rkey];
		} else {
			$max_bid_item = array_shift($max_bid_items);
		}
		$id = $max_bid_item["id"];
		$cid = $max_bid_item["cid"];
		$hbid = $max_bid_item["hbid"];
		$s_id = $max_bid_item["s_id"];
		//insert impression into DB
		$db->q("INSERT INTO advertise_data 
				(s_id, date, c_id, impression, realsid) 
				values 
				('$id', curdate(), '{$cid}', 1, '{$s_id}') 
				on duplicate key update impression = impression + 1");

		$bidx = $bid + (1 / 1000);
		if ($bidx > $hbid)
			$bidx = $hbid;

		$db->q("insert into advertise_current (s_id, bid, bidx, stamp) values (?, ?, ?, ?)", array($id, $bid, $bidx, $now));
		$current = $db->insertid();
		if ($current) {
			$smarty->assign("popup", $current);
			$smarty->assign("popup_value", $bidx);
		}

		$this->debug("popup",$s_id, "Y {$current} cs_ids={$ids}");
		return $current;
	}

	function interstitial($s_id = NULL) {
		global $db, $smarty, $mobile, $account, $sphinx, $ctx;

		if (!$account->isadmin())
			return;

		$now = time();
		$ip = account::getUserIp();

		if (is_null($s_id)) {
			if ($mobile == 1)
				$s_id = 10119;
			else
				$s_id = 10118;
		}
		$this->debug("interstitial",$s_id, "{$s_id}");

		if (is_bot()) {
			$this->debug("interstitial",$s_id, "N bot");
			return;
		}

		$force = 0;
		if( isset($_GET['showpopup']) && $account->isrealadmin() ) $force = 1;

		//for AS zones:
		//if we are logged in or if we cant specify IP address ---> dont display popup ad
		if (($s_id == 79 || $s_id == 10086) && ($account->isloggedin() || empty($ip))) {
			if (!$force) {
				$this->debug("interstitial",$s_id, "N loggedin|noip");
				return;
			}
		}

		//check if we didnt diplay interstitial already today for this IP address	
		$_SESSION["interstitialstupidad"] = true;
		if( !$force ) {
			$res = $db->q("select ip_address from popup_ip where ip_address = ? AND s_id = ?", [$ip, $s_id]);
			if ($db->numrows($res)) {
				$this->debug("interstitial",$s_id, "N ip");
				return;
			}
		}

		//get zone information
		$exclusive_access = [];
		$res = $db->q("SELECT exclusive_access FROM advertise_section WHERE id = ?", [$s_id]);
		if (!$db->numrows($res)) {
			$this->debug("interstitial",$s_id, "N cant find zone '{$s_id}'");
			return;
		}
		$row = $db->r($res);
		if ($row["exclusive_access"]) {
			if (strpos($row["exclusive_access"], ",") !== false)
				$exclusive_access = explode(",", $row["exclusive_access"]);
			else
				$exclusive_access[] = $row["exclusive_access"];
		}

		$sphinx_index = "advertise";
		$sphinx->reset();
		$sphinx->SetFilter('s_id', array($s_id));
		if (!empty($exclusive_access))
			$sphinx->SetFilter('account_id', $exclusive_access);

		$sphinx->SetLimits( 0, 2, 2);
		$sphinx->SetSortMode(SPH_SORT_ATTR_DESC, "bid");
		$results = $sphinx->Query ( "", $sphinx_index );

		$total = $results["total_found"];
		$result = $results["matches"];

		if (!$total) {
			//jay - if there is no advertising campaign for this section, check if there is backup default URL
			$res = $db->q("select backuphtml from advertise_section where id = '".intval($s_id)."'");
			if (!$db->numrows($res)) {
				$this->debug("interstitial",$s_id, "N no campaign, no backup");
				return;
			}
			$row = $db->r($res);
			$backup_url = $row["backuphtml"];
			if (!empty($backup_url)) {
				$smarty->assign("url", $backup_url);
				$smarty->assign("s_id", $s_id);
				$this->debug("interstitial",$s_id, "N no campaign, backup");
			} else {
				$this->debug("interstitial",$s_id, "N no campaign, no backup 2");
			}
			return;
		}

		$ids = NULL;
		foreach($result as $res) {
			$ids .= $ids ? (",".$res["id"]) : $res["id"];
		}
		if (is_null($ids)) {
			$this->debug("interstitial",$s_id, "N campaign, no ids");
			return;
		}

		//select the campaign ids and bids for placements from sphinx index
		$res = $db->q("SELECT s.id, s.bid, s.s_id, s.c_id, c.banner_img_src
						FROM advertise_camp_section s
						INNER JOIN advertise_campaign c on c.id = s.c_id
						WHERE s.id in ($ids)
						ORDER BY field(s.id,$ids)");
		if (!$db->numrows($res)) {
			$this->debug("interstitial",$s_id, "N camp_section not found");
			return;
		}
		//it can still happen that we have multiple campaigns with the same bid
		//first store in array all campaigns with maximum bid
		$max_bid = NULL; $c = 0; $max_bid_items = [];
		while($row = $db->r($res)) {
			$bid = $row["bid"];
			if ($max_bid == NULL)
				$max_bid = $bid;
			if (abs($max_bid - $bid) < 0.001) {
				//this is (one of) max bid campaigns
				$max_bid_items[] = [
					"id" => $row["id"],
					"cid" => $row["c_id"],
					"s_id" => $row["s_id"],
					"img_src" => $row["banner_img_src"],
					];
			}
		}
		//if there is more campaigns with the same max bid, choose random one from them
		if (count($max_bid_items) > 1) {
			$rkey = array_rand($max_bid_items);
			$max_bid_item = $max_bid_items[$rkey];
		} else {
			$max_bid_item = array_shift($max_bid_items);
		}
		$id = $max_bid_item["id"];
		$cid = $max_bid_item["cid"];
		$s_id = $max_bid_item["s_id"];
		$img_src = $max_bid_item["img_src"];

		$smarty->assign("interstitial_img_src", $img_src);
		$smarty->assign("interstitial_cs_id", $id);

		$this->debug("interstitial",$s_id, "Y {$current} cs_id={$id}, img_src={$img_src}");
		return $current;
	}

	// if advertise_section.debug == 1, debug some information into logfile
	private function link_debug($str) {
		global $debug;
		if (!$debug)
			return;
		$h = fopen(_CMS_ABS_PATH."/link.log", "a");
		fputs($h, "[".date("Y-m-d H:i:s")."] [".account::getUserIp()."] {$str}\n");
		fclose($h);
	}

	public function get_one_navlink_data($cs_id) {
		global $db, $config_site_url;

		//TODO implement flat deal handling !!!

		$res = $db->q("START TRANSACTION", []);

		$res = $db->q("SELECT s.bid, s.flat_deal_status, s.s_id, c.* 
						FROM advertise_camp_section s 
						INNER JOIN advertise_campaign c on s.c_id = c.id 
						WHERE s.id = ?
						FOR UPDATE",
						[$cs_id]
						);
		if ($db->numrows($res)) {
			$row = $db->r($res);

			$now = time();
			$s_id = $row["s_id"];
			$c_id = $row["id"];
			$bid = $row["bid"];

			$account_id = $row["account_id"];   //campaign owner
			$title = $row["ad_title"];
			//$url = $row["ad_url"];
			$u = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER["HTTP_HOST"]}{$_SERVER["REQUEST_URI"]}";
			$url = $config_site_url."/promo/link_click?cs_id={$cs_id}&u=".urlencode($u)."&r=".time();

			$this->link_debug("ACS found, cs_id={$cs_id}, account_id={$account_id}, c_id={$c_id}, bid={$bid}, url={$url}");

			$res = $db->q("COMMIT", array());

			if (is_null($c_id)) {
				//if we fucked up and we are getting lock timeouts from previous select, dont continue in running this script!
				debug_log("Error: promo/l : c_id is null!");
				return;
			}

			if (!is_bot()) {
				//we only mark impression if this is not a bot
				$db->q("INSERT INTO advertise_data 
						(s_id, date, c_id, impression, realsid) 
						values 
						(?, CURDATE(), ?, 1, ?)
						on duplicate key update impression = impression + 1",
						array($cs_id, $c_id, $s_id)
						);
			}

		} else {
			$db->q("COMMIT", array());
		}

		$this->link_debug("title = '{$title}', url = '{$url}'");
		
		return ["title" => $title, "url" => $url, "account_id" => $account_id, "c_id" => $c_id, "bid" => $bid];
	}

	public function get_navlink_data($s, $limit = false) {
		global $db, $sphinx, $ctx, $config_site_url;

		if (!$s)
			die;

		$limit = intval($limit);
		if ($limit < 1 || $limit > 5)
			$limit = 5;

		if ($this->get_test_zone_id() && $this->get_test_campaign_id()) {
			if ($this->get_test_zone_id() != $s)
				return ["title" => "Test link", "url" => $config_site_url."/test_url"];
			$res = $db->q("SELECT c.* FROM advertise_campaign c WHERE c.id = ?", array($this->get_test_campaign_id()));
			if (!$db->numrows($res))
				return ["title" => "Error Nav Link", "url" => $config_site_url."/error_nav_link"];
			$row = $db->r($res);
			return ["title" => $row["ad_title"], "url" => $row["ad_url"]];
		}

		//fetch section info
		$res = $db->q("SELECT active, account_id, debug, backuphtml, exclusive_access FROM advertise_section WHERE id = ?", array($s));
		if (!$db->numrows($res))
			die;
		$row = $db->r($res);
		$debug = intval($row["debug"]);
		$backup_html = $row["backuphtml"];

		$exclusive_access = array();	//array of account_ids of advertisers, who are only permitted to advertise in this zone
		$exclusive_access_str = $row["exclusive_access"];
		if ($exclusive_access_str)
			$exclusive_access = explode(",", $exclusive_access_str);

		$this->link_debug("s={$s} active={$row["active"]} account_id={$row["account_id"]}, exclusive_access_str={$exclusive_access_str}");

		//if section active flag not set, set it
		if ($row["active"] == 0)
			$db->q("UPDATE LOW_PRIORITY advertise_section SET active = 1 WHERE id = ?", array($s));


		//-------------
		//search link
		$sphinx->reset();
		$sphinx->SetFilter('s_id', array($s));
		$sphinx->SetLimits(0, $limit, $limit);
		if (!empty($exclusive_access))
			$sphinx->SetFilter('account_id', $exclusive_access);
		$sphinx->SetSortMode(SPH_SORT_ATTR_DESC, "bid");
		//$sphinx->SetSortMode(SPH_SORT_ATTR_DESC, "c_id");
		$results = $sphinx->Query("", "advertise");
		$total = $results["total_found"];
		$result = $results["matches"];

		$this->link_debug("total={$total}");

		if (!$total) 
			return [];

		//------------------------------------------------------------
		//dallas 8.8.2018 - show eccie link only in dallas and houston
		//NASTY HACK
		$result2 = [];
		foreach ($result as $i) {
			$c_id = $i["attrs"]["c_id"];
			if ($c_id == 1937 && !in_array($ctx->location_id, [14288, 14402]))
				continue;
			$result2[] = $i;
		}
		$result = $result2;
		//------------------------------------------------------------

		if ($total == 1) {
			//we have only one advertiser in this section - display that campaign
			$res = array_shift($result);
			if ($res)
				$id = $res["id"];
			$arr = $this->get_one_navlink_data($id);
			$links[] = $arr;
		} else {
			//we have more advertiser, show all links
			//TODO add link number limit per zone ....
			foreach ($result as $match) {
				$id = $match["id"];
				$arr = $this->get_one_navlink_data($id);
				$links[] = $arr;
			}
		}

		return $links;	
	}

	public function navlinks_as() {
		global $ctx, $mobile, $smarty, $account;

		$html = "";
		if ($mobile == 1) {
		
			if (!in_array($ctx->page_type, ["city", "list", "item", "erotic_services_index", "erotic_services_look"]) && $_SERVER["REQUEST_URI"] != "/m/location")
				return;
			$links = $this->get_navlink_data(10111, 3);
			//debug_log("navlinks:".print_r($links,true));
			foreach ($links as $arr) {
				if ($arr["url"] && $arr["title"]) {
					$additional_html = "";
					if ($account->isadmin()) {
						if (array_key_exists("account_id", $arr))
							$additional_html .= " data-account-id=\"{$arr["account_id"]}\"";
						if (array_key_exists("c_id", $arr)) {
							$additional_html .= " data-campaign-id=\"{$arr["c_id"]}\"";
							$additional_html .= " data-tippy=\"".htmlspecialchars("<a href=\"/advertise/campaign_detail?id={$arr["c_id"]}\" style=\"color: white;\" target=\"_blank\">Campaign detail</a>")."\" data-tippy-interactive=\"true\" data-tippy-delay=\"[null,2000]\"";
						}
						if (array_key_exists("bid", $arr))
							$additional_html .= " data-bid=\"{$arr["bid"]}\"";
					}
					$html .= "<a href=\"{$arr["url"]}\" id=\"mobile-navbar-link\" class=\"ui-btn ui-btn-right ui-btn-inline ui-mini\" data-ajax=\"false\" target=\"_blank\" rel=\"nofollow\"{$additional_html}>{$arr["title"]}</a>";
				}
			}
		} else {
			$links = $this->get_navlink_data(10110, 3);
			//debug_log("navlinks:".print_r($links,true));
			foreach ($links as $arr) {
				if ($arr["url"] && $arr["title"]) {
					$additional_html = "";
					if ($account->isadmin()) {
						if (array_key_exists("account_id", $arr))
							$additional_html .= " data-account-id=\"{$arr["account_id"]}\"";
						if (array_key_exists("c_id", $arr)) {
							$additional_html .= " data-campaign-id=\"{$arr["c_id"]}\"";
							$additional_html .= " data-tippy=\"".htmlspecialchars("<a href=\"/advertise/campaign_detail?id={$arr["c_id"]}\" style=\"color: white;\" target=\"_blank\">Campaign detail</a>")."\" data-tippy-interactive=\"true\" data-tippy-delay=\"[null,2000]\"";
						}
						if (array_key_exists("bid", $arr))
							$additional_html .= " data-bid=\"{$arr["bid"]}\"";
					}
					$html .= "<a href=\"{$arr["url"]}\" target=\"_blank\" rel=\"nofollow\"{$additional_html}>{$arr["title"]}</a>";
				}
			}
		}

		$smarty->assign("navlink_html", $html);
	}

	public function navlinks($s) {

		$html = "";
		$links = $this->get_navlink_data($s);
		foreach ($links as $link) {

			if (!$link["url"] || !$link["title"])
				continue;

			//TODO add this to database so it is not hardcoded for each domain
			switch ($s) {
				case 10112: $html .= "<li><a href=\"{$link["url"]}\" target=\"_blank\" rel=\"nofollow\">{$link["title"]}</a></li>"; break;	// www.tsescorts.com
				default: $html .= "<a href=\"{$link["url"]}\" target=\"_blank\" rel=\"nofollow\">{$link["title"]}</a>"; break;
			}
		}

		return $html;
	}

	/**
	 * Used from advertise/show.php when drawing text ads
	 * if query term is inside ad title or ad line, this term is printed in bold
	 */
	function boldquery($text, $query) { 
		if( empty($query) ) return $text;
		$result = "";
		$last = 0;
		while( ($pos = stripos($text, $query, $last)) !== false ) {
			$result .= substr($text, $last, ($pos-$last));
			$result .= "<b>".substr($text, $pos, strlen($query))."</b>";
			$last = $pos + strlen($query);
		}
		$result .= substr($text, $last);
		return $result;
	}

	function emailLowBudget($email) {
		$smarty = GetSmartyInstance();
		$html = $smarty->fetch(_CMS_ABS_PATH."/templates/advertise/email/low_budget.html.tpl");
		$text = $smarty->fetch(_CMS_ABS_PATH."/templates/advertise/email/low_budget.txt.tpl");
		$subject = "Adultsearch Advertising - Your campaigns prepaid balance is low";

		return send_email(array(
			"from" => "support@adultsearch.com",
			"to" => $email,
//		  "bcc" => ADMIN_EMAIL,
			"subject" => $subject,
			"html" => $html,
			"text" => $text
			));
	}

	function emailNoBudget($email) {
		$smarty = GetSmartyInstance();
		$html = $smarty->fetch(_CMS_ABS_PATH."/templates/advertise/email/no_budget.html.tpl");
		$text = $smarty->fetch(_CMS_ABS_PATH."/templates/advertise/email/no_budget.txt.tpl");
		$subject = "AdultSearch Advertising - Prepay Balance Exhausted";

		return send_email(array(
			"from" => "support@adultsearch.com",
			"to" => $email,
//		  "bcc" => ADMIN_EMAIL,
			"subject" => $subject,
			"html" => $html,
			"text" => $text
			));
	}

	//TODELETE
	/*
	function banner() {
		return false;
	}
	*/

	/**
	 * This should be called whenever we upped advertise budget of someone, to unpause his campaigns which ran out of budget
	 */
	public static function budgetUpped($account_id) {
		global $db;

		debug_log("advertise::budgetUpped: account_id={$account_id}");
		$res = $db->q("SELECT c.id FROM advertise_campaign c WHERE c.account_id = ?", array($account_id));
		$c_ids = array();
		while ($row = $db->r($res))
			$c_ids[] = $row["id"];
		
		if (empty($c_ids))
			return 0;

		$c_ids_str = implode(",", $c_ids);
		$res = $db->q("UPDATE advertise_camp_section SET status = 1 WHERE c_id IN ({$c_ids_str})", array());
		$aff = $db->affected();
		debug_log("advertise::budgetUpped: affected={$aff}");
		return $aff;
	}

	/**
	 * This should be called whenever we reached zero budget, to pause all campaigns which ran out of budget
	 */
	public static function budgetZero($account_id) {
		global $db;

		debug_log("advertise::budgetZero: account_id={$account_id}");
		$res = $db->q("SELECT c.id FROM advertise_campaign c WHERE c.account_id = ?", array($account_id));
		$c_ids = array();
		while ($row = $db->r($res))
			$c_ids[] = $row["id"];
		
		if (empty($c_ids))
			return 0;

		$c_ids_str = implode(",", $c_ids);
		$res = $db->q("UPDATE advertise_camp_section SET status = 0 WHERE c_id IN ({$c_ids_str})", array());
		$aff = $db->affected();
		debug_log("advertise::budgetZero: affected={$aff}");
		return $aff;
	}

	/**
	 * This updates flat_deal_status field on campaign placements
	 */
	function flatDealStatusUpdate($flat_deal_id) {
		global $db;

		//get budget of the account
		$budget = $db->single("
			SELECT ab.budget 
			FROM advertise_flat_deal afd
			INNER JOIN advertise_budget ab on ab.account_id = afd.account_id
			WHERE afd.id = ?",
			[$flat_deal_id]
			);
		$acs_status_update = "";
		if ($budget > 5)
			$acs_status_update = " , status = 0 ";
		
		$res = $db->q("
			SELECT fd.*, GROUP_CONCAT(adz.zone_id) as zone_ids
			FROM advertise_flat_deal fd 
			LEFT JOIN advertise_deal_zone adz on adz.deal_id = fd.id
			WHERE fd.id = ?
			GROUP BY fd.id",
			[$flat_deal_id]
			);
		if (!$db->numrows($res))
			return false;

		$row = $db->r($res);
		$type = $row["type"];
		$status = $row["status"];
		$account_id = $row["account_id"];
		$started_stamp = $row["started_stamp"];
		$finished_stamp = $row["finished_stamp"];
		$zone_ids = [];
		if ($row["zone_ids"]) {
			$zone_ids = explode(",", $row["zone_ids"]);
		} else if (intval($row["zone_id"])) {
			$zone_ids[] = intval($row["zone_id"]);
		} else {
			return false;
		}

		//we only update placements if we are pausing/unpausing currently running flat deal, or the one that has just finished
		if (!$started_stamp)
			return false;
		if ($finished_stamp && $finished_stamp < (time() - 10))
			return false;

		if ($type != "ZI" && $type != "ZP" && $type != "ZT")
			return false;		//currently we only know zone impressions and zone pops flat deal type

		foreach ($zone_ids as $zone_id) {

			//get zone type
			$res = $db->q("SELECT type FROM advertise_section WHERE id = ? LIMIT 1", array($zone_id));
			if (!$db->numrows($res))
				continue;
			$row = $db->r($res);
			$zone_type = $row["type"];

			if ($zone_type == "A")
				continue;		//TODO - make classified ad live/expired ?

			//get placements
			$res = $db->q("SELECT cs.id 
							FROM advertise_camp_section cs
							INNER JOIN advertise_campaign c on c.id = cs.c_id
							WHERE cs.s_id = ? AND c.account_id = ?",
							array($zone_id, $account_id)
							);
			$cs_ids = array();
			while ($row = $db->r($res)) {
				$cs_ids[] = $row["id"];
			}
			if (empty($cs_ids))
				return false;
			$cs_ids_csv = implode(",", $cs_ids);

			if ($status == 0) {
				//flat deal is not active, lets nullify flat_deal_status attribute on all the placements
				$db->q("UPDATE advertise_camp_section SET flat_deal_status = 0 {$acs_status_update} WHERE id IN ({$cs_ids_csv})");
				$aff = $db->affected($db);
			} else if ($status == 1) {
				//flat deal is active, lets set flat_deal_status attribute on all the placements
				$db->q("UPDATE advertise_camp_section SET flat_deal_status = 1, status = 1 WHERE id IN ({$cs_ids_csv})");
				$aff = $db->affected($db);
			}

			file_log("advertise", "advertise::flatDealStatusUpdate(): fd_id={$flat_deal_id} zone_id={$zone_id} - set status {$status} on {$aff} placements (ids: {$cs_ids_csv})");
		}

		return true;
	}

	/**
	 * This pauses or unpauses all campaigns in zone which flat deal is created upon
	 */
	function flatDealCampaigns($flat_deal_id) {
		global $db;

		$res = $db->q("
			SELECT fd.*, GROUP_CONCAT(adz.zone_id) as zone_ids
			FROM advertise_flat_deal fd 
			LEFT JOIN advertise_deal_zone adz on adz.deal_id = fd.id
			WHERE fd.id = ?
			GROUP BY fd.id",
			[$flat_deal_id]
			);
		if (!$db->numrows($res))
			return false;

		$row = $db->r($res);
		$type = $row["type"];
		$status = $row["status"];
		$account_id = $row["account_id"];
		$zone_ids = [];
		if ($row["zone_ids"]) {
			$zone_ids = explode(",", $row["zone_ids"]);
		} else if (intval($row["zone_id"])) {
			$zone_ids[] = intval($row["zone_id"]);
		} else {
			return false;
		}

		if ($type != "ZI" && $type != "ZP" && $type != "ZT")
			return false;		//currently we only know zone impressions and zone pops flat deal type

		foreach ($zone_ids as $zone_id) {

			//get campaigns and placements
			$c_ids = $cs_ids = array();
			$aff1 = $aff2 = 0;
			$res = $db->q("SELECT cs.id, cs.c_id 
							FROM advertise_camp_section cs
							INNER JOIN advertise_campaign c on c.id = cs.c_id
							WHERE cs.s_id = ? AND c.account_id = ? AND c.status != -1 AND c.flat_deal = 1",
							array($zone_id, $account_id)
							);
			while ($row = $db->r($res)) {
				$cs_id = $row["id"];
				$c_id = $row["c_id"];

				$cs_ids[] = $row["id"];

				if (!in_array($c_id, $c_ids))
					$c_ids[] = $c_id;
			}

			if (!empty($c_ids)) {
				$c_ids_csv = implode(",", $c_ids);
				$res1 = $db->q("UPDATE advertise_campaign SET status = ? WHERE id IN ({$c_ids_csv})", array($status));
				$aff1 = $db->affected($res1);
			}

			if (!empty($cs_ids)) {
				$cs_ids_csv = implode(",", $cs_ids);
				$res2 = $db->q("UPDATE advertise_camp_section SET status2 = ? WHERE id IN ({$cs_ids_csv})", array($status));
				$aff2 = $db->affected($res2);
			}

			file_log("advertise", "advertise::flatDealCampaigns(): fd_id={$flat_deal_id}, zone_id={$zone_id} - set status {$status} on {$aff1} campaigns (ids: {$c_ids_csv}) and {$aff2} placements (ids: {$cs_ids_csv})");
		}
		
		return true;
	}

	/**
	 * this creates payment in db, returns payment_id or false in case of error
	 */
	public static function create_payment($account_id, $email, $amount) {
		global $account, $db;

		file_log("advertise", "create_payment(): account_id={$account_id}, email={$email}, amount={$amount}");

		$now = time();
		$ip = account::getUserIp();

		$res = $db->q("
			INSERT INTO payment 
			(created_stamp, account_id, author_id, p1, amount, email, result, ip_address)
			VALUES
			(?, ?, ?, ?, ?, ?, ?, ?)",
			[$now, $account_id, $account->getId(), "advertise", number_format($amount, 2, ".", ""), $email, "I", $ip]
			);
		$payment_id = $db->insertid($res);
		if (!$payment_id) {
			file_log("advertise", "create_payment(): Error: Can't create payment in db, account_id={$account_id}, email={$amount}, amount={$amount}");
			return false;
		}

		$res = $db->q("
			INSERT INTO payment_item
			(payment_id, type, created_stamp)
			VALUES
			(?, ?, ?)",
			[$payment_id, "advertise", $now]
			);
		$pi_id = $db->insertid($res);
		if (!$pi_id) {
			file_log("advertise", "create_payment(): Error: Can't create payment_item in db, payment_id={$payment_id}");
			return false;
		}

		file_log("advertise", "create_payment(): Success: Payment #{$payment_id} created successfully");

		return $payment_id;
	}

	public static function paymentSuccessful($account_id, $amount) {
		global $db;

		$notify = intval($total/10);

		$db->q("INSERT INTO advertise_budget 
				(account_id, budget, budget_notify) 
				values 
				(?, ?, ?) 
				ON DUPLICATE KEY UPDATE budget = budget + ?, budget_notify = if(budget_notify > 0, budget_notify, ?)",
				array($account_id, $amount, $notify, $amount, $notify)
			);

		//update things around
		self::budgetUpped($account_id);

		//mark in operation log
		$now = time();
		$op_msg = "Account #{$account_id} upped advertise budget by \${$amount}";
		$db->q("INSERT INTO admin_operation_log 
				(code, stamp, msg) 
				values 
				('last_advertise_budget', ?, ?) 
				on duplicate key update stamp = ?, msg = ?", 
				array($now, $op_msg, $now, $op_msg)
				);

		system::go("/advertise/", "Thank You! Your funds has been posted to your account.");
		die;
	}

	/**
	 * This is executed from payline class, as soon as purchase is successful
	 */
	public static function purchaseSuccessful($purchase_id, $processor = "anet") {
		global $db;

		$system = new system();

		if ($processor == "anet") {
			$res = $db->q("SELECT p.account_id, p.total, p.cc_id, pp.p2, pp.p3
							FROM account_purchase p
							INNER JOIN payment pp on pp.id = p.payment_id
							WHERE p.id = ?",
							array($purchase_id));
		} else {
			debug_log("Error in businessowner::purchaseSuccessful(): Unsupported processor: {$processor}");
			reportAdmin("AS: businessowner::purchaseSuccessful(): Error Unsupported processor", "", array("processor" => $processor, "purchase_id" => $purchase_id));
			return false;
		}

		if ($db->numrows($res) != 1) {
			debug_log("Error in advertise::purchaseSuccessful(): cant find purchase & post, purchase_id={$purchase_id}");
			reportAdmin("AS: advertise::purchaseSuccessful(): Error cant find purchase & post", "", array("purchase_id" => $purchase_id));
			return false;
		}
		$row = $db->r($res);
		$account_id = $row["account_id"];
		$total = $row["total"];

		return self::paymentSuccessful($account_id, $total);

		/*
		//TODO following code is taken from advertise/addfunds
		$notify = intval($total/10);
		$db->q("INSERT INTO advertise_budget 
				(account_id, budget, cc_id, budget_notify) 
				values 
				(?, ?, ?, ?) 
				ON DUPLICATE KEY UPDATE budget = budget + ?, cc_id = ?, budget_notify = if( budget_notify > 0, budget_notify, ?)",
				array($account_id, $total, $cc_id, $notify, $total, $cc_id, $notify)
			);

		//update things around
		self::budgetUpped($account_id);

		//mark in operation log
		$now = time();
		$op_msg = "Account #{$account_id} upped advertise budget by \${$total}";
		$db->q("INSERT INTO admin_operation_log 
				(code, stamp, msg) 
				values 
				('last_advertise_budget', ?, ?) 
				on duplicate key update stamp = ?, msg = ?", 
				array($now, $op_msg, $now, $op_msg)
				);

		$system->go("/advertise/", "Thank You!, Your fund has been posted to your account.");
	
		$system->go("/");
		die;
		*/
	}

	public static function purchaseFailed($post_id, $processor = "payline") {
		global $db;

		if ($processor == "payline") {
			$res = $db->q("SELECT * FROM payline_post WHERE id = ?", array($post_id));
		} else if ($processor == "ccbill") {
			$res = $db->q("SELECT * FROM ccbill_post WHERE id = ?", array($post_id));
		} else if ($processor == "anet") {
			$res = $db->q("SELECT * FROM payment WHERE id = ?", array($post_id));
		} else {
			debug_log("Error in businessowner::purchaseSuccessful(): Unsupported processor: {$processor}");
			reportAdmin("AS: businessowner::purchaseSuccessful(): Error Unsupported processor", "", array("processor" => $processor, "purchase_id" => $purchase_id));
			return false;
		}

		if ($db->numrows($res) != 1) {
			debug_log("Error in advertise::purchaseFailed(): cant find post, post_id={$post_id}");
			reportAdmin("AS: advertise::purchaseFailed(): Error cant find post", "", array("post_id" => $post_id));
			return false;
		}
		$row = $db->r($res);
		$p2 = $row["p2"];
		$p3 = $row["p3"];

		$post_xml = @new \SimpleXMLElement((string)$row["post"]);

		//try to get amount value from post
		$amount = "";
		if ($post_xml) {
			$amt = intval($post_xml->amount);
			if ($amt)
				$amount = "&amount={$amt}";
		}

		$reason = "";
		if ($row["response"])
			$reason = "&reason=".urlencode($row["response"]);
		

		//go back to advertise add funds page
		system::go("/advertise/addfunds?error=1{$amount}{$reason}");
		die;
	}


	/**
	 * TEST interface
	 * 
	 * So we can test how does some banner look/render on real website
	 */
	/**
	 * Returns link to test page for specified zone and campaign, button with this test link is intended to display on certain admin pages, only to admin
	 */
	public static function get_test_link($zone_id, $campaign_id) {
		global $db;

		if (!$zone_id || !$campaign_id)
			return false;

		$res = $db->q("SELECT test_link FROM advertise_section WHERE id = ? LIMIT 1", array($zone_id));
		if ($db->numrows($res) != 1)
			return false;	//can't find zone

		$row = $db->r($res);
		$test_link = $row["test_link"];
		if (!$test_link)
			return false;	//this zone has not specified test link

		$appendix = "aatest=".base64url_encode("{$zone_id},{$campaign_id}");

		$test_link .= (strpos($test_link, "?") !== false) ? "&" : "?";
		$test_link .= $appendix;

		return $test_link;
	}

	private function init_test() {
		$this->test_zone_id = 0;
		$this->test_campaign_id = 0;
		if ($_REQUEST["u"]) {
			$url = urldecode($_REQUEST["u"]);
			echo "<!-- {$url} -->";
			$arr = parse_url($url);
			parse_str($arr["query"], $arr2);
 			$aatest = base64url_decode($arr2["aatest"]);
			echo "<!-- {$aatest} -->";
			if ($aatest) {
				$arr = explode(",", $aatest);
				if (count($arr) == 2) {
					$this->test_zone_id = intval($arr[0]);
					$this->test_campaign_id = intval($arr[1]);
				}
			}
		}
	}

	public function get_test_zone_id() {
		if (is_null($this->test_zone_id))
			$this->init_test();
		return $this->test_zone_id;
	}

	public function get_test_campaign_id() {
		if (is_null($this->test_campaign_id))
			$this->init_test();
		return $this->test_campaign_id;
	}

	/**
	 * Sends out agency notification email 
	 */
	public static function sendAgencyEmail($template_filename_base, $params, $subject, $email) {
		global $twig;

		if (!$template_filename_base || !$subject || !$email) {
			reportAdmin("AS: Error sending agency email", "template_filename_base='{$template_filename_base}', subject='{$subject}', email='{$email}'", $params);
			return false;
		}

		$template = $twig->load("{$template_filename_base}.html.twig");
		$html = $template->render($params);

		$template = $twig->load("{$template_filename_base}.txt.twig");
		$text = $template->render($params);

		$error = null;
		$ret = send_email([
			"from" => "agency@adultsearch.com",
			"to" => $email,
//		"bcc" => ADMIN_EMAIL,
			"subject" => $subject,
			"html" => $html,
			"text" => $text,
			], $error);
		if (!$ret) {
			reportAdmin("AS: Error sending agency email", "template_filename_base='{$template_filename_base}', subject='{$subject}', email='{$email}', error='{$error}'", $params);
		}
		return $ret;
	}

	/**
	 * Sends out funds added email notification
	 */
	public static function sendFundsAddedNotification($account_id, $amount, $email = null) {

		if (!$email) {
			$acc = account::findOnebyId($account_id);
			if (!$acc) {
				file_log("advertise", "sendFundsAddedNotification: Error: cant find account by id #{$account_id}");
				return false;
			}
			$email = $acc->getEmail();
		}

		if (!$email) {
			file_log("advertise", "sendFundsAddedNotification: Error: email not specified: account_id='{$account_id}', email='{$email}'");
			return false;
		}
		
		$params = [
			"amount" => $amount,
			];

		$subject = "Funds added";

		return self::sendAgencyEmail("advertise/email/funds_added", $params, $subject, $email);
	}

}

