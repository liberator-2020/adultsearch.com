<?php

//require_once(_CMS_ABS_PATH."/inc/classes/vendor/autoload.php");

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\util\LogFactory;
use net\authorize\api\controller as AnetController;

//sandbox
//DEFINE("MERCHANT_LOGIN_ID", "79Y5eQLLea");
//DEFINE("MERCHANT_TRANSACTION_KEY", "87T6Yp6y6HSd72P4");
//DEFINE("ANET_ENV", \net\authorize\api\constants\ANetEnvironment::SANDBOX);

//production
DEFINE("MERCHANT_LOGIN_ID", "66JcMf5Ad9Jj");
DEFINE("MERCHANT_TRANSACTION_KEY", "59HjC2g2m9L7ykF8");
DEFINE("ANET_ENV", \net\authorize\api\constants\ANetEnvironment::PRODUCTION);

DEFINE("AUTHORIZENET_LOG_FILE", _CMS_ABS_PATH."/cms/log/anet_sdk.log");

class anet {

	//transaction params
	var $total;
	var $period = 30;			//subscription length in days, normally its 30 days (for ads), but for bussinessowner it could be 360 days 
	var $recurring = false;		//if set to some number (float), this will be recurring price set to, used in step4.php and recurring.php
	var $recurringPeriod = 30;	//recurring period in days, normally it is 30 days
	var $what = NULL;
	var $page = NULL;
	var $item_id = NULL;
	var $p2 = NULL;
	var $p3 = NULL;
	var $template = NULL;		// in case to show different template

	//TODO review these
	var $Address, $Zip, $City, $State, $Country, $Email, $FirstName, $LastName, $Description;

	var $register_as_user = false, $cc_id, $no_promotion = false, $preset_promotion = false, $promo = NULL, $saved = 0, $promo_code = 0;
	var $promo_notify = 0, $promo_notify_to = NULL, $promo_left = 0, $promo_id = 0, $phone = NULL, $loclimit = NULL, $promo_section, $cl_ad = NULL;
	var $setassponsor = 0;
	var $sidesponsor = 0;
	var $citythumbnail = 0;
	var $reposts = 0;
	var $original_price = 0, $discount = 0, $promo_recurring = 0, $promo_day = 0, $promo_upgrade_only = false, $no_trial = false, $subcat = 0;
	var $promo_row = NULL;

	var $duplicate_txn_window = 1;		//in seconds

	public function __construct() {
	}

	private function handlepromo(&$error) {
		global $account, $smarty, $db;

		$account_id = $account->isloggedin();

		$this->original_price = $this->total;

		if (empty($_POST["cc_promo"]) || ($this->no_promotion && !$this->preset_promotion)) {
			return;
		}

		$promo = GetPostParam("cc_promo");
		if (empty($promo)) {
			$error = "You did not type any code, if you do not have a promotion code, use the submit button under the payment form.";
			return;
		}

		//10.1.2017 dallas special request-  promo backpage should work if they put space inside or mess up uppercase/lowercase...
		if (str_replace(' ', '', strtolower(trim($promo))) == "backpage")
			$promo = "backpage";

		$rex = $db->q("SELECT * FROM classifieds_promocodes WHERE code = ? AND deleted IS NULL", array($promo));
		if( !$db->numrows($rex) ) {
			$error = "Promotion code could not found. If you do not have a promotion code, leave that field empty.";
			return;
		}
		$this->promo_row = $rox = $db->r($rex);

		if( $this->promo_upgrade_only && ($rox['setassponsor']||$rox['recurring']||$rox['citythumbnail']||$rox['sidesponsor']||$rox['reposts']) ) {
			$error = "This promo code is not eligible for this purchase.";
			return;
		}

		if( $this->no_trial && $rox['free'] == 1 && ($rox['day'] > 0 && $rox['day'] < 30) ) {
			$error = "This promo code is not eligible for this purchase.";
			return;
		}

		if( $this->subcat && $rox['subcat'] && $this->subcat != $row['subcat'] ) {
			$error = "This promo code is not eligible for this category.";
			return;
		}

		$this->promo_section = $rox['section'];
		$this->setassponsor = $rox['setassponsor'];
		$this->citythumbnail = $rox['citythumbnail'];
		$this->sidesponsor = $rox['sidesponsor'];
		$this->reposts = $rox['reposts'];
		$this->promo_recurring = $rox['recurring'];
		$this->promo_day = $rox['day'];
		if( $rox["limit_per_account"] == 1 ) {
			$ip = account::getUserIp();
			$phone_sql = !empty($this->phone) ? "or phone = '{$this->phone}'" : '';
			$rexx = $db->q("select id from classifieds_promocodes_usage where code_id = '{$rox["id"]}' and (account_id = '$account_id' or ip = inet_aton('$ip') $phone_sql) limit 1");
			if( $db->numrows($rexx) ) {
				$error = "You may not use this promotion code more than once.";
				reportAdmin("You may not use this promotion code more than once.");
				return;
			} 
		}

		if( $rox["left"] < 1 ) {
			$error = "This promotion code is not valid anymore.";
			return;
		} else if( $rox["max"] > 0 && $this->total > $rox["max"] ) {
			$error = "This promotion code is only valid up to \${$rox["max"]} amount of payments.";
			return;
		} else if( !empty($rox['section']) && strcmp($this->what, $rox['section']) ) {
			$error = "This promotion code is not eligible for this section.";
			return;
		} else if( $rox['loclimit'] && $this->loclimit && $rox['loclimit'] < $this->loclimit ) {
			$error = "This promotion code can not be used to post in more than 1 location";
			reportAdmin('This promotion code can not be used to post in more than 1 location');
			return;
		}

		$this->promo_id = $rox["id"];
		$this->promo = $promo;
		$this->promo_code = $rox['id'];
		if( $rox["notify"] == 1 ) {
			$this->promo_notify = 1;
			$this->promo_notify_to = $rox["notify_to"];
			$this->promo_left = $rox["left"];
		}

		if( $rox["discount"] > 0 ) {
			if( !$this->preset_promotion )
				$this->total -= $rox["discount"];
			if ($this->total < 0)
				$this->total = 0;
			$this->saved = $rox["discount"];
		} else if( $rox["percentage"] > 0 ) { 
			$this->saved =  $this->total - ($this->total-($this->total*$rox["percentage"]/100)); 
			if( !$this->preset_promotion )
				$this->total -= ($this->total*$rox["percentage"]/100);
			$this->discount = $rox["percentage"];
		} elseif( $rox["addup"] > 0 ) {
			if( !$this->preset_promotion )
				$this->total += $rox["addup"];
			$this->saved = $rox["addup"];
		} elseif ( $rox["fixedprice"] > 0 ) {
			if( !$this->preset_promotion )
				$this->total = $rox["fixedprice"];
		} else {
			$this->saved = $this->total;
			if( !$this->preset_promotion )
				$this->total = 0;
		}

		if (!is_int($this->total))
			$this->total = number_format($this->total, 2, '.', '');

		if( !is_int($this->saved) && $this->saved )
			$this->saved = number_format($this->saved, 2, '.', '');

		if( $this->preset_promotion )
			return true;

		$smarty->assign("cc_promo", $promo);
		$smarty->assign("cc_redeemed", number_format($this->saved, 2));
		if( isset($_POST["redeem"]) )
			$error = "";				
		return true;
	}


	/**
	 * Updates all classified tables after successful purchase
	 */
	private function finishup() {
		global $account, $db;

		file_log("anet", "anet:finishup(): code='{$this->promo_row['code']}'");

		if (empty($this->promo_row['code']))
			return;

		$account_id = $account->isloggedin();
		$ip = account::getUserIp();
		$today = mktime(0, 0, 0);

		$db->q("UPDATE classifieds_promocodes SET `left` = `left` - 1 WHERE code = ?", array($this->promo_row['code']));

		$db->q("INSERT INTO classifieds_promocodes_usage
				(code_id, account_id, ip, date, paid, saved) 
				values 
				(?, ?, inet_aton('$ip'), curdate(), ?, ?)",
				array($this->promo_row['id'], $account_id, $this->total, $this->saved));

		if ($this->promo_row['setassponsor'] && $this->what == 'classifieds' && $this->item_id) {
			$db->q("UPDATE classifieds
					SET sponsor = 1, sponsor_mobile = 1
					WHERE id = ?", 
					array($this->item_id)
					);
		}
		if ($this->promo_row['citythumbnail'] && $this->what == 'classifieds' && $this->item_id) {
			$res = $db->q("SELECT * from classifieds_loc WHERE post_id = ?", array($this->item_id));
			while ($row = $db->r($res)) {
				$expire_stamp = time() + 30*86400;
				$db->q("INSERT INTO classifieds_sponsor
						(loc_id,state_id, post_id, day, type, expire_stamp, done, notified)
						VALUES
						(?, ?, ?, ?, ?, ?, ?, ?)
						on duplicate key update type = ?, expire_stamp = ?, done = ?
						",
						array($row["loc_id"], $row["state_id"], $row["post_id"], 30, $row["type"], $expire_stamp, $row["done"], 0
						, $row["type"], $expire_stamp, $row["done"])
						);
			}
		}
		if ($this->promo_row['sidesponsor'] && $this->what == 'classifieds' && $this->item_id) {
			$res = $db->q("SELECT * from classifieds_loc WHERE post_id = ?", array($this->item_id));
			while ($row = $db->r($res)) {
				$db->q("INSERT INTO classifieds_side
						(loc_id,state_id, post_id, day, type, expire_stamp, done, notified)
						VALUES
						(?, ?, ?, ?, ?, ?, ?, ?)
						on duplicate key update type = ?, expire_stamp = ?, done = ?
						",
						array($row["loc_id"], $row["state_id"], $row["post_id"], 30, $row["type"], time()+(3600*24*30), $row["done"], 0, $row["type"], time()+(3600*24*30), $row["done"])
						);
			}
		}
		if ($this->promo_row['recurring'] == 1 && $this->what == 'classifieds' && $this->item_id) {
			$db->q("UPDATE classifieds
					SET auto_renew_fr = 1, time_next = $today+60*60*24+60*auto_renew_time 
					WHERE id = ?",
					array($this->total, $this->item_id)
					);
		} elseif (intval($this->promo_row['reposts']) > 0 && $this->what == 'classifieds' && $this->item_id) {
			$db->q("UPDATE classifieds
					SET auto_renew_fr = 1, time_next = $today+60*60*24+60*auto_renew_time 
					WHERE id = ?",
					array(intval($this->promo_row['reposts']), $this->item_id)
					);
		}

		if ($this->promo_row['day'] && $this->what == 'classifieds' && $this->item_id) {
			$db->q("UPDATE classifieds SET expires = date_add(now(), interval {$this->promo_row['day']} day) WHERE id = ?", array($this->item_id));
		} 

		if ($this->promo_row['notify']) {
			$this->promo_left = $this->promo_row['left'] - 1;
			$emailm = "Promo code <b>{$this->promo_row['code']}</b> is used to post the following ad ";

			if ($this->promo_row['section'] == 'classifieds') {
				$emailm .= "<a href='http://adultsearch.com/classifieds/look?id={$this->item_id}'>http://adultsearch.com/classifieds/look?id={$this->item_id}</a>.";
			} elseif ($this->promo_row['section'] == 'businessowner') {
				$emailm .= "<a href='{$this->item_link}'>{$this->item_link}</a>.";
			}

			$emailm .= "<br>Total paid: $<b>{$this->total}</b><br>Account ID: <b>{$account_id}</b><br>This promo code can be used <b>{$this->promo_left}</b> times more.";
			if (!empty($this->promo_row['notify_to'])) {
				sendEmail("support@adultsearch.com", "Promotion Used", $emailm, $this->promo_row['notify_to']);
			}
		}

		file_log("anet", "anet:finishup(): done");
	}

	/**
	 * Used in:
	 * ./_cms_files/adbuild/step4.php
	 * ./_cms_files/classifieds/sponsor.php
	 * ./_cms_files/classifieds/side.php
	 * ./_cms_files/classifieds/upgrade.php
	 * ./_cms_files/classifieds/recurring.php
	 * ./_cms_files/businessowner/payment.php
	 * ./_cms_files/advertise/addfunds.php
	 */
	public function fullcharge(&$error) {
		global $smarty, $account, $db, $config_dev_server;

		$account_id = $account->isloggedin();

		if ($this->p2 == "adbuild")
			$this->p3 = $this->item_id;

		$this->handlepromo($error);

		if ($this->total == 0 ) {
			$this->finishup();
			//TODO
//			$this->afterPurchase(true, false, 0, $this->what, $this->item_id, $this->page);
			return 1;
		}

		file_log("anet", "anet::fullcharge()");

		$cre = null;
		$saved_card_id = intval($_REQUEST["saved_card_id"]);
		if ($saved_card_id) {
			$cre = creditcard::findOneById($saved_card_id);
			if (!$cre) {
				$error = "Wrong credit card";
			}
		}

		$image_front_data = $image_back_data = null;

		if ($cre) {
			$firstname = $cre->getFirstname();
			$lastname = $cre->getLastname();
			$address = $cre->getAddress();
			$zipcode = $cre->getZipcode();
			$city = $cre->getCity();
			$state = $cre->getState();
			$country = $cre->GetCountry();
			$cc_month = $cre->getExpMonth();
			$cc_year = $cre->getExpYear();
		} else {
			$firstname = GetPostParam("cc_firstname");
			$lastname = GetPostParam("cc_lastname");
			$address = GetPostParam("cc_address");
			$zipcode = preg_replace('/[^0-9A-Z ]/', '', strtoupper(trim($_REQUEST["cc_zipcode"])));
			$city = GetPostParam("cc_city");
			$state = GetPostParam("cc_state");
			$country = GetPostParam("cc_country");
			$cc_month = preg_replace('/[^0-9]/', '', $_REQUEST["cc_month"]);
			$cc_year  = preg_replace('/[^0-9]/', '', $_REQUEST["cc_year"]);
			$image_front_data = $_REQUEST["image_front_data"];
			$image_back_data = $_REQUEST["image_back_data"];
		}

		$cc_cc	= preg_replace('/[^0-9]/', '', $_REQUEST["cc_cc"]);
		$cc_cvc2  = preg_replace('/[^0-9]/', '', $_REQUEST["cc_cvc2"]);

		if (strlen($cc_year) == 2)
			$cc_year = "20{$cc_year}";

		file_log("anet", "anet::fullcharge(): account_id={$account_id} clad_id={$this->item_id}, banned=".intval($account->isBanned()));
		if (isset($error))
			$error = $error;
		else if ($this->recurring && $_REQUEST["recurring_confirm"] != 1)
			$error = "Please confirm the recurring charge";
		else if (empty($firstname))
			$error = "Please type your first name";
		else if (empty($lastname))
			$error = "Please type your last name";
		else if (empty($address))
			$error = "Please type your address associated with you credit card";
		else if (empty($zipcode))
			$error = "Please type your 'ZIP/Postal Code'";
		else if (!empty($zipcode) && $country == "United States" && !preg_match('/^[0-9]{5}$/', $zipcode))
			$error = "Invalid ZIP Code";
		else if (empty($city))
			$error = "Please type your city";
//		else if (empty($state))
//			$error = "Please type your state";
		else if (empty($country))
			$error = "Please select your country";
		else if (empty($cc_cc))
			$error = "Please type your credit card number";
		else if (empty($cc_month))
			$error = "Please select credit card expiration month";
		else if (empty($cc_year))
			$error = "Please select credit card expiration year";
		else if ($cc_year < date("Y") || ($cc_year == date("Y") && $cc_month < date("m")))
			$error = "You entered wrong expiration month/year or your credit card is already expired";
		else if (empty($cc_cvc2))
			$error = "Please type your credit card verification code";
		else if (!$cre && (!$image_front_data || !$image_back_data))
			$error = "Please upload photo of front and back face of credit card";
		else if (empty($_REQUEST["captcha_str"]))
			$error = "You need to type the security code";
		else if (strcasecmp($_REQUEST["captcha_str"], $_SESSION["captcha_str"]))
			$error = "Security code is wrong, please type it again";
		else if (creditcard::usedByOtherAccount($cc_cc, $cc_cvc2, $cc_month, $cc_year, $account_id)) {
			$error = "This credit card is used by other account. Don't create multiple accounts on our website. If you want to post more ads, please contact support@adultsearch.com";
		} else if (creditcard::isBannedAux($cc_cc, $cc_cvc2, $zipcode)) {
			//card is (probably) banned
			//notify support
			creditcard::sendBannedCardNotification($account_id, $cc_cc, $cc_month, $cc_year, $firstname, $lastname, $zipcode);
			$error = "This credit card may not be used on our website. (abuse)";
		} else if ($account->isBanned()) {
			//user is banned and trying to pay with a new card, report him
			//notify admin
			reportAdmin("AS: Banned user trying to pay", "Check if this is really spammer, if yes, add this card to banned cards", [
				"account_id" => $account_id,
				"what" => $this->what,
				"p1" => $this->p1,
				"p2" => $this->p2,
				"p3" => $this->p3,
				"cc" => creditcard::ccnoFirstLast($cc_cc),
				"cvc" => creditcard::obfuscateCvc2($cc_cvc2),
				"zipcode" => $zipcode,
				"firstname" => $firstname,
				"lastname" => $lastname,
				"total" => $this->total,
				"cc_month" => $cc_month,
				"cc_year" => $cc_year,
				]);
			$error = "This credit card may not be used on our website. (abuse)";
		} else {
			$params = array(
				"card" => $cre,
				"address" => $address,
				"zip" => $zipcode,
				"firstname" => $firstname,
				"lastname" => $lastname,
				"email" => $_SESSION["email"],
				"city" => $city,
				"state" => $state,
				"country" => $country,
				"total" => $this->total,
				"cc_cc" => $cc_cc,
				"cc_month" => $cc_month,
				"cc_year" => $cc_year,
				"cc_cvc2" => $cc_cvc2,
				"image_front_data" => $image_front_data,
				"image_back_data" => $image_back_data,
				);

			//recurring payment
			if ($this->recurring && $this->recurringPeriod) {
				$params["recurring_amount"] = $this->recurring;
				$params["recurring_period"] = $this->recurringPeriod;
			}

			//TODO we leave this for now (might be used outside of class payment), but going to be removed
			$this->Address = $address;
			$this->Zip = $zipcode;
			$this->Email = $_SESSION["email"];
			$this->FirstName = $firstname;
			$this->LastName = $lastname;
			$this->City = $city;
			$this->State = $state;
			$this->Country = $country;

			$this->charge($params, $error);
		}

		$smarty->assign("country_options", location::getCountryHtmlOptions());
		if (!empty($_POST)) {
			$smarty->assign("recurring_confirm", ($_REQUEST["recurring_confirm"] == 1));
			$smarty->assign("cc_firstname", htmlspecialchars($_POST["cc_firstname"]));
			$smarty->assign("cc_lastname", htmlspecialchars($_POST["cc_lastname"]));
			$smarty->assign("cc_address", htmlspecialchars($_POST["cc_address"]));
			$smarty->assign("cc_city", htmlspecialchars($_POST["cc_city"]));
			$smarty->assign("cc_state", htmlspecialchars($_POST["cc_state"]));		
			$smarty->assign("cc_zipcode", htmlspecialchars($_POST["cc_zipcode"]));
			$smarty->assign("cc_country", htmlspecialchars($_POST["cc_country"]));
			$smarty->assign("cc_cc", htmlspecialchars($_POST["cc_cc"]));
			$smarty->assign("cc_month", htmlspecialchars($_POST["cc_month"]));
			$smarty->assign("cc_year", htmlspecialchars($_POST["cc_year"]));
			$smarty->assign("cc_cvc2", htmlspecialchars($_POST["cc_cvc2"]));
			$smarty->assign("image_front_data", htmlspecialchars($_POST["image_front_data"]));
			$smarty->assign("image_back_data", htmlspecialchars($_POST["image_back_data"]));
			$smarty->assign("error", $error);
			if (!empty($_POST["captcha_str"]) && !strcasecmp($_POST["captcha_str"], $_SESSION["captcha_str"])) {
 				$smarty->assign("captcha_str", htmlspecialchars($_POST["captcha_str"]));
				$smarty->assign("captcha_ok", true);
			}
		}

		$smarty->assign("total", $this->total);
		if ($this->recurring && $this->recurringPeriod) {
			$smarty->assign("recurring_amount", $this->recurring);
			$smarty->assign("recurring_period", $this->recurringPeriod);
		}
		$smarty->assign("register", $account_id<1 && $this->register_as_user);
		$smarty->assign("no_promotion", $this->no_promotion);

		$smarty->assign("cards", creditcard::get_cards());

		file_log("anet", "anet::fullcharge(): displaying payment page error='{$error}'");
		if ($this->template)
			$smarty->display(_CMS_ABS_PATH.$this->template);
		else 
			$smarty->display(_CMS_ABS_PATH."/templates/payment_gen_anet.tpl");

		return false;
	}
	
	/**
	 * Used in:
	 * ./_cms_files/payment/pay.php
	 */
	public function pay($payment_id, &$error = "") {
		global $smarty, $account, $db, $config_dev_server;

		$now = time();

		file_log("anet", "anet::pay(): payment_id={$payment_id}");

		$res = $db->q("SELECT * FROM payment WHERE id = ? LIMIT 1", [$payment_id]);
		if ($db->numrows($res) != 1){
			$error = "Invalid payment!";
			return false;
		}
		$row = $db->r($res);

		$result = $row["result"];
		$amount = $row["amount"];
		$account_id = $row["account_id"];

		if ($result == "A") {
			//TODO
			return true;
		} 
		if ($amount == 0) {
			//TODO
			//finish up (mark result as 'A')
			return true;
		}

		$smarty->assign("total", $amount);
		$smarty->assign("country_options", location::getCountryHtmlOptions());

		//if we have advertiser balance available, offer to pay with balance
		$budget = $db->single("SELECT budget FROM advertise_budget WHERE account_id = ?", [$account_id]);
		$smarty->assign("budget", $budget);
		if ($budget > $amount)
			$smarty->assign("budget_available", true);

		if (!isset($_REQUEST["submitted"])) {
			//this is first display of the form (and not submission)
			//display the template
			file_log("anet", "anet::pay(): not submitted, displaying payment form");
			return $smarty->display(_CMS_ABS_PATH."/templates/payment_gen_anet.tpl");
		}

		if (($budget >= $amount) && ($_REQUEST["pay_budget"] == 1) && !$error) {
			//substract budget
			$res = $db->q("UPDATE advertise_budget SET budget = budget - ? WHERE account_id = ? LIMIT 1", [$amount, $account_id]);
			if ($db->affected($res) != 1) {
				file_log("anet", "anet::pay(): failed to substract budget: payment_id={$payment_id}, amount={$amount}, account_id={$account_id}");
				reportAdmin("Payment failed - cannot substract budget", "payment_id={$payment_id}, amount={$amount}, account_id={$account_id}");
				$error = "Error in payment, please contact support.";
			} else {
				//budget substracted, approve payment
				$res = $db->q("UPDATE payment SET response = ?, responded_stamp = ?, result = 'A' WHERE id = ? LIMIT 1",
					["Paid with budget", $now, $payment_id]
					);
				if ($db->affected($res) != 1) {
					file_log("anet", "anet::pay(): failed to update payment: payment_id={$payment_id}, result=A, response=Paid with budget");
					reportAdmin("Payment failed - cannot update payment", "payment_id={$payment_id}, result=A, response=Paid with budget");
					$error = "Error in payment, please contact support.";
				} else {
					//add dummy transaction
					$res = $db->q("INSERT INTO transaction 
						(type, payment_id, stamp, trans_id, amount)
						VALUES
						(?, ?, ?, ?, ?)",
						["N", $payment_id, $now, "budget", $amount]
						);
					//all good, do after successful purchase stuff
					$this->paymentSuccessful($payment_id);
			        $this->afterPayment($payment_id);
				}
			}
		}

		$cre = null;
		$saved_card_id = intval($_REQUEST["saved_card_id"]) ?: $row["cc_id"];
		if ($saved_card_id) {
			$cre = creditcard::findOneById($saved_card_id);
			if (!$cre) {
				$error = "Wrong credit card";
			}
		}

		$image_front_data = $image_back_data = null;

		if ($cre) {
			$firstname = $cre->getFirstname();
			$lastname = $cre->getLastname();
			$address = $cre->getAddress();
			$zipcode = $cre->getZipcode();
			$city = $cre->getCity();
			$state = $cre->getState();
			$country = $cre->GetCountry();
			$cc_month = $cre->getExpMonth();
			$cc_year = $cre->getExpYear();
		} else {
			$firstname = GetPostParam("cc_firstname");
			$lastname = GetPostParam("cc_lastname");
			$address = GetPostParam("cc_address");
			$zipcode = GetPostParam("cc_zipcode");
			$city = GetPostParam("cc_city");
			$state = GetPostParam("cc_state");
			$country = GetPostParam("cc_country");
			$cc_month = preg_replace('/[^0-9]/', '', $_REQUEST["cc_month"]);
			$cc_year  = preg_replace('/[^0-9]/', '', $_REQUEST["cc_year"]);
			$image_front_data = $_REQUEST["image_front_data"];
			$image_back_data = $_REQUEST["image_back_data"];
		}

		$cc_cc	= preg_replace('/[^0-9]/', '', $_REQUEST["cc_cc"]);
		$cc_cvc2  = preg_replace('/[^0-9]/', '', $_REQUEST["cc_cvc2"]);

		if (strlen($cc_year) == 2)
			$cc_year = "20{$cc_year}";

		if ($error)
			$error = $error;
		else if (empty($firstname))
			$error = "Please type your first name";
		else if (empty($lastname))
			$error = "Please type your last name";
		else if (empty($address))
			$error = "Please type your address associated with you credit card";
		else if (empty($zipcode))
			$error = "Please type your 'ZIP/Postal Code'";
		else if (empty($city))
			$error = "Please type your city";
		else if (empty($country))
			$error = "Please select your country";
		else if (empty($cc_cc))
			$error = "Please type your credit card number";
		else if (strlen($cc_cc) < 12)
			$error = "Please type correct credit card number";
		else if (empty($cc_month))
			$error = "Please select credit card expiration month";
		else if (empty($cc_year))
			$error = "Please select credit card expiration year";
		else if ($cc_year < date("Y") || ($cc_year == date("Y") && $cc_month < date("m")))
			$error = "You entered wrong expiration month/year or your credit card is already expired";
		else if (empty($cc_cvc2))
			$error = "Please type your credit card verification code";
		else if (!$cre && (!$image_front_data || !$image_back_data))
			$error = "Please upload photo of front and back face of credit card";
		else if (!$account->isAdmin() && empty($_REQUEST["captcha_str"]))
			$error = "You need to type the security code";
		else if (!$account->isAdmin() && strcasecmp($_REQUEST["captcha_str"], $_SESSION["captcha_str"]))
			$error = "Security code is wrong, please type it again";
		else if (creditcard::usedByOtherAccount($cc_cc, $cc_cvc2, $cc_month, $cc_year, $account_id)) {
			$error = "This credit card is used by other account. Don't create multiple accounts on our website. If you want to post more ads, please contact support@adultsearch.com";
		} else if (creditcard::isBannedAux($cc_cc, $cc_cvc2, $zipcode)) {
			//card is (probably) banned
			//notify support
			creditcard::sendBannedCardNotification($account_id, $cc_cc, $cc_month, $cc_year, $firstname, $lastname, $zipcode);
			$error = "This credit card may not be used on our website. (abuse)";
		} else if ($account->isBanned()) {
			//user is banned and trying to pay with a new card, report him
			//notify admin
			reportAdmin("AS: Banned user trying to pay", "Check if this is really spammer, if yes, add this card to banned cards", [
				"payment_id" => $payment_id,
				"cc" => creditcard::ccnoFirstLast($cc_cc),
				"cvc" => creditcard::obfuscateCvc2($cc_cvc2),
				"zipcode" => $zipcode,
				"firstname" => $firstname,
				"lastname" => $lastname,
				"total" => $amount,
				"cc_month" => $cc_month,
				"cc_year" => $cc_year,
				]);
			$error = "This credit card may not be used on our website. (abuse)";
		} else {

			//insert or update credit card
			if (!$cre) {
				$cre = new creditcard();
				$cre->setAccountId($account_id);
				$cre->setCc($cc_cc);
				$cre->setExpMonth($cc_month);
				$cre->setExpYear($cc_year);
				$cre->setCvc2($cc_cvc2);
				$cre->setFirstName($firstname);
				$cre->setLastName($lastname);
				$cre->setAddress($address);
				$cre->setZipcode($zipcode);
				$cre->setCity($city);
				$cre->setState($state);
				$cre->setCountry($country);
				$ret = $cre->add();
				if (!$ret) {
					reportAdmin("AS: anet pay() error", "Cannot insert/update cc in db.", [
						"payment_id" => $payment_id,
						"cc" => creditcard::ccnoFirstLast($cc_cc),
						"cvc" => creditcard::obfuscateCvc2($cc_cvc2),
						"cc_month" => $cc_month,
						"cc_year" => $cc_year,
						"firstname" => $firstname,
						"lastname" => $lastname,
						"address" => $address,
						"city" => $city,
						"state" => $state,
						"zipcode" => $zipcode,
						"country" => $country,
						]);
					$error = "Error in payment, please contact support.";
				} else {
					file_log("anet", "anet::pay(): ok, cc_id={$cre->getId()}");
					$cc_id = $cre->getId();
					$ret = $cre->store_card_images($image_front_data, $image_back_data);
					if (!$ret) {
						file_log("anet", "anet::pay(): failed to store card images");
						reportAdmin("Payment failed - cannot set CC image front & back", "cc_id={$cc_id}");
						$error = "Error in payment, please contact support.";
						return 0;	
					}
				}
			} else {
				$cc_id = $cre->getId();
			}

			$res = $db->q("UPDATE payment SET cc_id = ? WHERE id = ? LIMIT 1", [$cc_id, $payment_id]);
			$this->do_charge($payment_id, $cc_cc, $cc_cvc2, $error);
		}

		if (!empty($_POST)) {
			$smarty->assign("cc_firstname", htmlspecialchars($_POST["cc_firstname"]));
			$smarty->assign("cc_lastname", htmlspecialchars($_POST["cc_lastname"]));
			$smarty->assign("cc_address", htmlspecialchars($_POST["cc_address"]));
			$smarty->assign("cc_city", htmlspecialchars($_POST["cc_city"]));
			$smarty->assign("cc_state", htmlspecialchars($_POST["cc_state"]));		
			$smarty->assign("cc_zipcode", htmlspecialchars($_POST["cc_zipcode"]));
			$smarty->assign("cc_country", htmlspecialchars($_POST["cc_country"]));
			$smarty->assign("cc_cc", htmlspecialchars($_POST["cc_cc"]));
			$smarty->assign("cc_month", htmlspecialchars($_POST["cc_month"]));
			$smarty->assign("cc_year", htmlspecialchars($_POST["cc_year"]));
			$smarty->assign("cc_cvc2", htmlspecialchars($_POST["cc_cvc2"]));
			$smarty->assign("image_front_data", htmlspecialchars($_POST["image_front_data"]));
			$smarty->assign("image_back_data", htmlspecialchars($_POST["image_back_data"]));
			$smarty->assign("error", $error);
			if (!empty($_POST["captcha_str"]) && !strcasecmp($_POST["captcha_str"], $_SESSION["captcha_str"])) {
 				$smarty->assign("captcha_str", htmlspecialchars($_POST["captcha_str"]));
				$smarty->assign("captcha_ok", true);
			}
		}

		file_log("anet", "anet::pay(): displaying payment page error='{$error}'");
		$smarty->display(_CMS_ABS_PATH."/templates/payment_gen_anet.tpl");

		return false;
	}
	
	/**
	 * this method does actual charge for params
	 */	
	public function charge($params, &$error) {
		global $account, $db;

		//check completeness of params
		if (!array_key_exists("total", $params) || $params["total"] == ""
				|| !array_key_exists("firstname", $params) || $params["firstname"] == ""
				|| !array_key_exists("lastname", $params) || $params["lastname"] == ""
				|| !array_key_exists("address", $params) || $params["address"] == ""
				|| !array_key_exists("zip", $params) || $params["zip"] == ""
				|| !array_key_exists("email", $params) || $params["email"] == ""
				|| !array_key_exists("cc_cc", $params) || $params["cc_cc"] == ""
				|| !array_key_exists("cc_month", $params) || $params["cc_month"] == ""
				|| !array_key_exists("cc_year", $params) || $params["cc_year"] == ""
				|| !array_key_exists("cc_cvc2", $params) || $params["cc_cvc2"] == ""
			) {

			$m = "Payment params:<br /><pre>total={$params["total"]}, firstname={$params["firstname"]}, lastname={$params["lastname"]}, address={$params["address"]}, city={$params["city"]}, state={$params["state"]}, zip={$params["zip"]}, email={$params["email"]}, cc_cc=".creditcard::ccnoFirstLast($params["cc_cc"]).", expmonth={$params["cc_month"]}, expyear={$params["cc_year"]}, cvc2_len=".strlen($params["cc_cvc2"])."<pre><br />";
			reportAdmin("Payment failed - mandatory fields not filled.", $m);
			$error = "Something is missing.";
			return 0;
		}

		if ($params["total"] <= 0)
			return 1;

		//this is used when making purchase from hotleads - we are not logged in on AS, but we're doing purchase on behalf of the owner of the ad
		if (array_key_exists("account_id", $params) && $params["account_id"])
			$account_id = $params["account_id"];
		else
			$account_id = $account->getId();

		//prepare some variables
		$now = time();
		$author_id = $account->getId();
		$recurring_amount = $recurring_period = null;
		if (array_key_exists("recurring_amount", $params) && array_key_exists("recurring_period", $params)) {
			$recurring_amount = $params["recurring_amount"];
			$recurring_period = intval($params["recurring_period"]);
		}
		$promocode_id = null;
		if ($this->promo_row && array_key_exists("id", $this->promo_row) && intval($this->promo_row["id"]))
			$promocode_id = intval($this->promo_row["id"]);
		$ip_address = account::getUserIp();
		$total = number_format($params["total"], 2, ".", "");
		if (strlen($params["cc_year"]) == 2)
			$params["cc_year"] = "20".$params["cc_year"];

		if ($params["card"]) {
			$cre = $params["card"];
			$cc_id = $cre->getId();
		} else {
			//create credit card
			$cre = new creditcard();
			$cre->setAccountId($account_id);
			$cre->setCc($params["cc_cc"]);
			$cre->setExpMonth($params["cc_month"]);
			$cre->setExpYear($params["cc_year"]);
			$cre->setCvc2($params["cc_cvc2"]);
			$cre->setFirstName($params["firstname"]);
			$cre->setLastName($params["lastname"]);
			$cre->setAddress($params["address"]);
			$cre->setZipcode($params["zip"]);
			$cre->setCity($params["city"]);
			$cre->setState($params["state"]);
			$cre->setCountry($params["country"]);
			$cre->setTotal($total);
			$ret = $cre->add();
			if ($ret) {
				$cc_id = $cre->getId();
			} else {
				$m = "New cc params:<br /><pre>account_id={$account_id}, total={$total}, firstname={$params["firstname"]}, lastname={$params["lastname"]}, address={$params["address"]}, city={$params["city"]}, state={$params["state"]}, zip={$params["zip"]}, email={$params["email"]}, cc_cc=".creditcard::ccnoFirstLast($params["cc_cc"]).", expmonth={$params["cc_month"]}, expyear={$params["cc_year"]}, cvc2_len=".strlen($params["cc_cvc2"])."<pre><br />";
				reportAdmin("Payment failed - cannot insert cc into db.", $m);
				$error = "Error in payment, please contact support.";
				return 0;	
			}
			$ret = $cre->store_card_images($params["image_front_data"], $params["image_back_data"]);
			if (!$ret) {
				reportAdmin("Payment failed - cannot set CC image front & back", "cc_id={$cc_id}");
				$error = "Error in payment, please contact support.";
				return 0;	
			}
		}
		
		//create payment
		$res = $db->q("
		INSERT INTO payment 
		(created_stamp, account_id, author_id, cc_id, p1, p2, p3, amount, recurring_amount, recurring_period, 
			promocode_id, email, sent_stamp, result, ip_address)
		VALUES
		(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 
			?, ?, ?, ?, ?)",
		[$now, $account_id, $author_id, $cc_id, $this->what, $this->p2, $this->p3, $total, $recurring_amount, $recurring_period,
			$promocode_id, $params["email"], $now, "S", $ip_address]
		);
		$payment_id = $db->insertid($res);
		if (!$payment_id) {
			$m = "New payment params:<br /><pre>account_id={$account_id}, author_id={$author_id}, cc_id={$cc_id}, p1={$this->what}, p2={$this->p2}, p3={$this->p3}, amount={$total}, recurring_amount={$recurring_amount}, recurring_period={$recurring_period}, promocode_id={$promocode_id}, email={$params["email"]}, now={$now}, ip_address={$ip_address}<pre><br />";
			reportAdmin("Payment failed - cannot insert payment into db.", $m);
			$error = "Error in payment, please contact support.";
			return 0;	
		}

		file_log("anet", "anet::charge(): cc_id={$cc_id}, payment_id={$payment_id}");	


		//----------------------------------------------
		$res = $db->q("UPDATE payment SET result = 'P', responded_stamp = ? WHERE id = ? LIMIT 1", [$now, $payment_id]);
		$aff = $db->affected($res);
		if ($aff != 1)
			reportAdmin("Payment pending - error: cant update payment", "cant update payment_id={$payment_id}, now={$now}, aff={$aff}");
		//system::go("/payment/done?id={$payment_id}");
		system::go("/payment/verification?id={$payment_id}");
		//----------------------------------------------


		//----------------------
		//authorize.net SDK code
		//----------------------

		//some vars init
		$ref_id = "AS{$payment_id}_".time();
		$invoice_number = "AS{$payment_id}";
		$expiration_date = $cre->getExpYear()."-".str_pad($cre->getExpMonth(), 2, "0", STR_PAD_LEFT);
		$customer_id = "AS{$account_id}";

		//init
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);
		$logger = LogFactory::getLog(get_class($this));

		$cc = new AnetAPI\CreditCardType();
		$cc->setCardNumber($params["cc_cc"]);
		$cc->setExpirationDate($expiration_date);
		$cc->setCardCode($params["cc_cvc2"]);

		$p = new AnetAPI\PaymentType();
		$p->setCreditCard($cc);

		$o = new AnetAPI\OrderType();
		$o->setInvoiceNumber($invoice_number);
		$o->setDescription("Advertisement");

		$ca = new AnetAPI\CustomerAddressType();
		$ca->setFirstName($cre->getFirstname());
		$ca->setLastName($cre->getLastname());
		$ca->setAddress($cre->getAddress());
		$ca->setCity($cre->getCity());
		$ca->setState($cre->getState());
		$ca->setZip($cre->getZipcode());
		$ca->setCountry($cre->getCountry());

		$cd = new AnetAPI\CustomerDataType();
		$cd->setType("individual");
		$cd->setId($customer_id);
		$cd->setEmail($params["email"]);

		$trt = new AnetAPI\TransactionRequestType();
		$trt->setTransactionType("authCaptureTransaction");
		$trt->setAmount($total);
		$trt->setOrder($o);
		$trt->setPayment($p);
		$trt->setBillTo($ca);
		$trt->setCustomer($cd);

		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($ref_id);
		$request->setTransactionRequest($trt);

		$controller = new AnetController\CreateTransactionController($request);

		$response = $controller->executeWithApiResponse(ANET_ENV);

		if (!$response) {
			//No response returned, this is error, should not happen
			file_log("anet", "anet::charge(): Error: no response returned from anet API: cc_id={$cc_id}, payment_id={$payment_id}");	
			$m = "No response returned from authorize.net API! cc_id={$cc_id}, payment_id={$payment_id}";
			reportAdmin("Payment failed - no response from anet", $m);
			$error = "Error in payment, please contact support.";
			return 0;	
		}


		//-------------------------
		//API requst was successful
		// handling is different depending whether it was recurrint or non-recurrent transaction
		$now = time();

		//look for a transaction response
		// and parse it to display the results of authorizing the card
		$tresponse = $response->getTransactionResponse();

		$trans_id = $response_code = $auth_code = $avs_result_code = $cvv_result_code = $cavv_result_code = null;
		$trans_hash = $account_number = $account_type = null;
		if ($tresponse) {
			$trans_id = $tresponse->getTransId();
			$response_code = $tresponse->getResponseCode();		//1 = Approved, 2 = Declined, 3 = Error, 4 = Held for Review
			$auth_code = $tresponse->getAuthCode();				//Authorization or approval code - 6 characters
			$avs_result_code = $tresponse->getAvsResultCode();	//Address Verification Service (AVS) response code
			$cvv_result_code = $tresponse->getCvvResultCode();	//Card code verification (CCV) response code
			$cavv_result_code = $tresponse->getCavvResultCode();	//Cardholder authentication verification response code
			$trans_hash = $tresponse->getTransHash();
			$account_number = $tresponse->getAccountNumber();
			$account_type = $tresponse->getAccountType();
		}

		if ($account_type) {
			//update credit card type
			$cre->setType($account_type);
			$ret = $cre->update();
			if (!$ret) {
				file_log("anet", "anet::charge(): Can't update credit card type, cc_id={$cc_id}, account_type={$account_type}, payment_id={$payment_id}");
				reportAdmin("AS: anet:charge(): err - cant update card type", "cc_id={$cc_id}, account_type={$account_type}, payment_id={$payment_id}");
			}
		}

		if ($response->getMessages()->getResultCode() != "Ok") {
			//API request wasn't successful

			$error_code = $error_message = null;
			$tresponse = $response->getTransactionResponse();
			if ($tresponse != null && $tresponse->getErrors() != null) {
				$error_code = $tresponse->getErrors()[0]->getErrorCode();
				$error_message = $tresponse->getErrors()[0]->getErrorText();
			} else {
				$error_code = $response->getMessages()->getMessage()[0]->getCode();
				$error_message = $response->getMessages()->getMessage()[0]->getText();
			}

			$resp = "error_code={$error_code}, error_message='{$error_message}', ref_id={$ref_id}, trans_id={$trans_id}, response_code={$response_code}, auth_code={$auth_code}, avs_result_code={$avs_result_code}, cvv_result_code={$cvv_result_code}, cavv_result_code={$cavv_result_code}, trans_hash={$trans_hash}, account_number={$account_number}, account_type={$account_type}";
		
			file_log("anet", "anet::charge(): Transaction failed: cc_id={$cc_id}, payment_id={$payment_id}, {$resp}");

			//update payment
			$res = $db->q(
				"UPDATE payment SET result = ?, response = ?, responded_stamp = ?, decline_code = ?, decline_reason = ? WHERE id = ? LIMIT 1",
				["D", $resp, $now, $error_code, $error_message, $payment_id]
				);
			$aff = $db->affected($res);
			if ($aff != 1) {
				reportAdmin("Payment failed - error: cant update payment", "cant update payment about transaction decline, payment_id={$payment_id}, now={$now}, {$resp}, aff={$aff}");
			}
			$db->q("INSERT INTO account_cc_fail (account_id, stamp, payment_id) values (?, ?, ?)", [$account_id, $now, $payment_id]);

			//display error message to client
			$error = "Transaction declined ({$error_code} - {$error_message}). Please double-check all your billing and credit card details and/or try to use different credit card";
			return 0;
		}

		if (!$tresponse || $tresponse->getMessages() == null) {
			//transaction failed
			$error_code = $error_message = null;
			if ($tresponse->getErrors() != null) {
				$error_code = $tresponse->getErrors()[0]->getErrorCode();
				$error_message = $tresponse->getErrors()[0]->getErrorText();
			}

			$resp = "error_code={$error_code}, error_message='{$error_message}', ref_id={$ref_id}, trans_id={$trans_id}, response_code={$response_code}, auth_code={$auth_code}, avs_result_code={$avs_result_code}, cvv_result_code={$cvv_result_code}, cavv_result_code={$cavv_result_code}, trans_hash={$trans_hash}, account_number={$account_number}, account_type={$account_type}";
		
			file_log("anet", "anet::charge(): Transaction failed: cc_id={$cc_id}, payment_id={$payment_id}, {$resp}");

			//update payment
			$res = $db->q(
				"UPDATE payment SET result = ?, response = ?, responded_stamp = ?, decline_code = ?, decline_reason = ? WHERE id = ? LIMIT 1",
				["D", $resp, $now, $error_code, $error_message, $payment_id]
				);
			$aff = $db->affected($res);
			if ($aff != 1) {
				reportAdmin("Payment failed - error: cant update payment", "cant update payment about transaction decline, payment_id={$payment_id}, now={$now}, {$resp}, aff={$aff}");
			}
			$db->q("INSERT INTO account_cc_fail (account_id, stamp, payment_id) values (?, ?, ?)", [$account_id, $now, $payment_id]);

			//display error message to client
			$error = "Transaction declined ({$error_code} - {$error_message}). Please double-check all your billing and credit card details and/or try to use different credit card";
			return 0;
		}

		//transaction succeeded
		$description = $tresponse->getMessages()[0]->getDescription();

		file_log("anet", "anet::charge(): Transaction succeeded: cc_id={$cc_id}, payment_id={$payment_id}, trans_id={$trans_id}, response_code={$response_code}, auth_code={$auth_code}, avs_result_code={$avs_result_code}, cvv_result_code={$cvv_result_code}, cavv_result_code={$cavv_result_code}, description='{$description}'");

		//update payment
		$resp = "ref_id={$ref_id}, trans_id={$trans_id}, response_code={$response_code}, auth_code={$auth_code}, avs_result_code={$avs_result_code}, cvv_result_code={$cvv_result_code}, cavv_result_code={$cavv_result_code}, trans_hash={$trans_hash}, account_number={$account_number}, account_type={$account_type}, description='{$description}'";
		$res = $db->q(
			"UPDATE payment SET result = ?, response = ?, responded_stamp = ?, trans_id = ? WHERE id = ? LIMIT 1",
			["A", $resp, $now, $trans_id, $payment_id]
			);
		$aff = $db->affected($res);
		if ($aff != 1) {
			reportAdmin("Payment succeeded - error: cant update payment", "cant update payment about transaction success, payment_id={$payment_id}, now={$now}, resp='{$resp}', aff={$aff}");
		}

		//insert transaction
		$res = $db->q(
			"INSERT INTO transaction 
			(type, payment_id, stamp, trans_id, amount)
			VALUES
			(?, ?, ?, ?, ?)",
			["N", $payment_id, $now, $trans_id, $total]
			);
		$transaction_id = $db->insertid($res);
		if (!$transaction_id) {
			reportAdmin("AS: Payment succeeded - error: cant insert transaction", "cant insert transaction into db, payment_id={$payment_id}, trans_id={$trans_id}");
		}

		//insert purchase
		$db->q("INSERT INTO account_purchase
				(account_id, cc_id, `time`, `transaction_time`, payment_id, transaction_id, `what`, `total`, `saved`, `item_id`, `promo_code`) 
				values 
				(?, ?, CURDATE(), ?, ?, ?, ?, ?, ?, ?, ?)",
				[$account_id, $cc_id, $now, $payment_id, $trans_id, $this->what, $total, '', $this->item_id, intval($promocode_id)]
				);
		$purchase_id = $db->insertid();
		if (!$purchase_id) {
			file_log("anet", "anet::charge(): Error: cant insert purchase into db, payment_id={$payment_id}");
			reportAdmin("Payment succeeded - error: cant insert purchase", "cant insert purchase, payment_id={$payment_id}");
		} else {
			file_log("anet", "anet::charge(): purchase_id={$purchase_id}");
		}



		//-----------------------------
		// RECURRING CODE
		//-----------------------------
		if ($recurring_amount && $recurring_period) {
			// if payment has also recurring amount, create subscription

			$recurring_ref_id = "AS{$payment_id}R_".time();

			$s = new AnetAPI\ARBSubscriptionType();
			$s->setName("AS Advertisement subscription");

			$i = new AnetAPI\PaymentScheduleType\IntervalAType();
			$i->setLength($recurring_period);
			$i->setUnit("days");

			$ps = new AnetAPI\PaymentScheduleType();
			$ps->setInterval($i);
			$start_date = new \DateTime("now");
			$start_date->add(new \DateInterval("P{$recurring_period}D"));
			$next_renewal_date = $start_date->format("Y-m-d");
			$ps->setStartDate($start_date);
			$ps->setTotalOccurrences("9999");

			$s->setPaymentSchedule($ps);
			$s->setAmount($recurring_amount);

			$recurring_invoice_number = "AS{$payment_id}R";
			$or = new AnetAPI\OrderType();
			$or->setInvoiceNumber($recurring_invoice_number);
			$or->setDescription("Advertisement");

			$s->setOrder($or);
			$s->setPayment($p);
			$s->setBillTo($ca);

			$request = new AnetAPI\ARBCreateSubscriptionRequest();
			$request->setMerchantAuthentication($merchantAuthentication);
			$request->setRefId($recurring_ref_id);
			$request->setSubscription($s);
	
			$controller = new AnetController\ARBCreateSubscriptionController($request);
		
			$response = $controller->executeWithApiResponse(ANET_ENV);
		
			if (!$response) {
				//No response returned, this is error, should not happen
				file_log("anet", "anet::charge(): Recurring: Error: no response returned from anet API: cc_id={$cc_id}, payment_id={$payment_id}");	
				$m = "Recurring: No response returned from authorize.net API! cc_id={$cc_id}, payment_id={$payment_id}";
				reportAdmin("Payment failed - no response from anet", $m);
				//do not throw error, after all initial transaction succeeded, let admin take care of this and clean up the mess

			} else if ($response->getMessages()->getResultCode() != "Ok") {
				//API request wasn't successful, this should not happen, notify admin

				$error_code = $error_message = null;
				$errorMessages = $response->getMessages()->getMessage();
				$error_code = $errorMessages[0]->getCode();
				$error_message = $errorMessages[0]->getText();

				file_log("anet", "anet::charge(): Recurring: Error: Anet API request failed: cc_id={$cc_id}, payment_id={$payment_id}, error_code='{$error_code}', error_message='{$error_message}'");
				$m = "Recurring: Anet API request failed: cc_id={$cc_id}, payment_id={$payment_id}, error_code='{$error_code}', error_message='{$error_message}'";
				reportAdmin("Payment failed - anet api failed", $m);

				//do not throw error, after all initial transaction succeeded, let admin take care of this and clean up the mess

			} else {
			
				$code = $message = null;
				if (count($response->getMessages()->getMessage()) > 0) {
					$code = $response->getMessages()->getMessage()[0]->getCode();
					$message = $response->getMessages()->getMessage()[0]->getText();
				}
				$subscription_id = $response->getSubscriptionId();

				if (!$subscription_id) {
					//transaction failed
					file_log("anet", "anet::charge(): Recurring: Transaction failed: cc_id={$cc_id}, payment_id={$payment_id}, error_code='{$code}', error_message='{$message}'");

					//update payment
					$res = $db->q("UPDATE payment SET last_rebill_result = ? WHERE id = ? LIMIT 1", ["D", $payment_id]);
					$aff = $db->affected($res);
					if ($aff != 1) {
						reportAdmin("Payment failed - error: cant update payment", "Recurring: cant update payment about transaction decline, payment_id={$payment_id}, now={$now}, error_code={$code}, error_message={$message}, aff={$aff}");
					}

					//do not throw error, after all initial transaction succeeded, let admin take care of this and clean up the mess

				} else {

					//recurring transaction succeeded !

					//update payment
					$resp = "subscription_id={$subscription_id}, code={$code}, message='{$message}'";
					$res = $db->q(
						"UPDATE payment SET last_rebill_result = 'A', subscription_id = ?, subscription_status = 1, next_renewal_date = ? WHERE id = ? LIMIT 1",
						[$subscription_id, $next_renewal_date, $payment_id]
						);
					$aff = $db->affected($res);
					if ($aff != 1) {
						reportAdmin("AS: Payment ok, but recurr error: cant update payment", "payment_id={$payment_id}, now={$now}, resp='{$resp}', aff={$aff}");
					}

					//update transaction
					$res = $db->q("UPDATE transaction SET subscription_id = ? WHERE id = ? LIMIT 1", [$subscription_id, $transaction_id]);
					$aff = $db->affected($res);
					if ($aff != 1) {
						reportAdmin("AS: Payment ok, but recurr error: cant update transaction", "payment_id={$payment_id}, now={$now}, resp='{$resp}', aff={$aff}");
					}
				}
			}
		}
		//end of recurring processing

		if ($this->what == "classifieds")
			$db->q("INSERT INTO classifieds_stat (`time`, made) VALUES (curdate(), ?) on duplicate key update made = made + ?",	[$total, $total]);

		$this->purchaseSuccessful($this->what, $purchase_id);

		$this->afterPurchase(true, true, $payment_id, $this->what, $this->item_id, $this->page, $this->p2, $this->p3);

		return true;
	}

	/**
	 * this method does actual charge for params
	 */	
	public function do_charge($payment_id, $cardno, $cvc2, &$error) {
		global $account, $db;

		file_log("anet", "anet::do_charge(): payment_id={$payment_id}");

		$res = $db->q("
			SELECT p.*
				, cc.firstname, cc.lastname, cc.address, cc.zipcode, cc.city, cc.state, cc.country, cc.expmonth, cc.expyear
			FROM payment p
			INNER JOIN account_cc cc on cc.cc_id = p.cc_id 
			WHERE p.id = ? 
			LIMIT 1", 
			[$payment_id]
			);
		if ($db->numrows($res) != 1) {
			//this should not happen at this point
			file_log("anet", "anet::do_charge(): Error: cant find payment by payment_id #{$payment_id} !");
			$error = "Invalid payment id";
			reportAdmin("AS: Payment failed", "do_charge(): cant find payment by payment_id #{$payment_id} !");
			return false;
		}

		$row = $db->r($res);
		$account_id = $row["account_id"];
		$cc_id = $row["cc_id"];
		$email = $row["email"];
		$firstname = $row["firstname"];
		$lastname = $row["lastname"];
		$address = $row["address"];
		$city = $row["city"];
		$state = $row["state"];
		$zipcode = $row["zipcode"];
		$country = $row["country"];
		$expmonth = $row["expmonth"];
		$expyear = $row["expyear"];

		//prepare some variables
		$now = time();
		$author_id = $account->getId();
		$recurring_amount = $row["recurring_amount"];
		$recurring_period = $row["recurring_period"];
		$promocode_id = $row["promocode_id"];
		$ip_address = account::getUserIp();
		$total = number_format($row["amount"], 2, ".", "");
		if (strlen($expyear) == 2)
			$expyear = "20{$expyear}";
		$expmonth = str_pad($expmonth, 2, "0", STR_PAD_LEFT);

		$res = $db->q("UPDATE payment SET result = 'P', responded_stamp = ? WHERE id = ? LIMIT 1", [$now, $payment_id]);
		$aff = $db->affected($res);
		if ($aff != 1)
			reportAdmin("Payment pending - error: cant update payment", "cant update payment_id={$payment_id}, now={$now}, aff={$aff}");
		//system::go("/payment/done?id={$payment_id}");
		system::go("/payment/verification?id={$payment_id}");

		//----------------------
		//authorize.net SDK code
		//----------------------

		//some vars init
		$ref_id = "AS{$payment_id}_".time();
		$invoice_number = "AS{$payment_id}";
		$expiration_date = "{$expyear}-{$expmonth}";
		$customer_id = "AS{$account_id}";

		//init
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);
		$logger = LogFactory::getLog(get_class($this));

		$cc = new AnetAPI\CreditCardType();
		$cc->setCardNumber($cardno);
		$cc->setExpirationDate($expiration_date);
		$cc->setCardCode($cvc2);

		$p = new AnetAPI\PaymentType();
		$p->setCreditCard($cc);

		$o = new AnetAPI\OrderType();
		$o->setInvoiceNumber($invoice_number);
		$o->setDescription("Advertisement");

		$ca = new AnetAPI\CustomerAddressType();
		$ca->setFirstName($firstname);
		$ca->setLastName($lastname);
		$ca->setAddress($address);
		$ca->setCity($city);
		$ca->setState($state);
		$ca->setZip($zipcode);
		$ca->setCountry($country);

		$cd = new AnetAPI\CustomerDataType();
		$cd->setType("individual");
		$cd->setId($customer_id);
		$cd->setEmail($email);

		$trt = new AnetAPI\TransactionRequestType();
		$trt->setTransactionType("authCaptureTransaction");
		$trt->setAmount($total);
		$trt->setOrder($o);
		$trt->setPayment($p);
		$trt->setBillTo($ca);
		$trt->setCustomer($cd);

		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($ref_id);
		$request->setTransactionRequest($trt);

		$controller = new AnetController\CreateTransactionController($request);

		$response = $controller->executeWithApiResponse(ANET_ENV);
		//$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);

		if (!$response) {
			//No response returned, this is error, should not happen
			file_log("anet", "anet::do_charge(): Error: no response returned from anet API: cc_id={$cc_id}, payment_id={$payment_id}");	
			reportAdmin("Payment failed - no response from anet", "No response returned from authorize.net API! cc_id={$cc_id}, payment_id={$payment_id}");
			$error = "Error in payment, please contact support.";
			return false;
		}

		if ($response->getMessages()->getResultCode() != "Ok") {
			//API request wasn't successful, this should not happen, notify admin

			$error_code = $error_message = null;
			$tresponse = $response->getTransactionResponse();
			if ($tresponse != null && $tresponse->getErrors() != null) {
				$error_code = $tresponse->getErrors()[0]->getErrorCode();
				$error_message = $tresponse->getErrors()[0]->getErrorText();
			} else {
				$error_code = $response->getMessages()->getMessage()[0]->getCode();
				$error_message = $response->getMessages()->getMessage()[0]->getText();
			}

			file_log("anet", "anet::do_charge(): Error: Anet API request failed: cc_id={$cc_id}, payment_id={$payment_id}, error_code='{$error_code}', error_message='{$error_message}'");
			$m = "Anet API request failed: cc_id={$cc_id}, payment_id={$payment_id}, error_code='{$error_code}', error_message='{$error_message}'";
			reportAdmin("Payment failed - anet api failed", $m);
			$error = "Error in payment, please contact support.";
			return 0;	
		}


		//-------------------------
		//API requst was successful
		// handling is different depending whether it was recurrint or non-recurrent transaction
		$now = time();

		//look for a transaction response
		// and parse it to display the results of authorizing the card
		$tresponse = $response->getTransactionResponse();

		$trans_id = $response_code = $auth_code = $avs_result_code = $cvv_result_code = $cavv_result_code = null;
		$trans_hash = $account_number = $account_type = null;
		if ($tresponse) {
			$trans_id = $tresponse->getTransId();
			$response_code = $tresponse->getResponseCode();		//1 = Approved, 2 = Declined, 3 = Error, 4 = Held for Review
			$auth_code = $tresponse->getAuthCode();				//Authorization or approval code - 6 characters
			$avs_result_code = $tresponse->getAvsResultCode();	//Address Verification Service (AVS) response code
			$cvv_result_code = $tresponse->getCvvResultCode();	//Card code verification (CCV) response code
			$cavv_result_code = $tresponse->getCavvResultCode();	//Cardholder authentication verification response code
			$trans_hash = $tresponse->getTransHash();
			$account_number = $tresponse->getAccountNumber();
			$account_type = $tresponse->getAccountType();
		}

		if ($account_type) {
			//update credit card type
			$res = $db->q("UPDATE account_cc SET type = ? WHERE cc_id = ? LIMIT 1", [creditcard::convertType($account_type), $cc_id]);
		}

		if (!$tresponse || $tresponse->getMessages() == null) {
			//transaction failed
			$error_code = $error_message = null;
			if ($tresponse->getErrors() != null) {
				$error_code = $tresponse->getErrors()[0]->getErrorCode();
				$error_message = $tresponse->getErrors()[0]->getErrorText();
			}

			$resp = "error_code={$error_code}, error_message='{$error_message}', ref_id={$ref_id}, trans_id={$trans_id}, response_code={$response_code}, auth_code={$auth_code}, avs_result_code={$avs_result_code}, cvv_result_code={$cvv_result_code}, cavv_result_code={$cavv_result_code}, trans_hash={$trans_hash}, account_number={$account_number}, account_type={$account_type}";
		
			file_log("anet", "anet::do_charge(): Transaction failed: cc_id={$cc_id}, payment_id={$payment_id}, {$resp}");

			//$m = "TODO - remove me: Transaction failed: cc_id={$cc_id}, payment_id={$payment_id}, {$resp}";
			//reportAdmin("Payment failed - transaction failed", $m);

			//update payment
			$res = $db->q(
				"UPDATE payment SET result = ?, response = ?, responded_stamp = ?, decline_code = ?, decline_reason = ? WHERE id = ? LIMIT 1",
				["D", $resp, $now, $error_code, $error_message, $payment_id]
				);
			$aff = $db->affected($res);
			if ($aff != 1) {
				reportAdmin("Payment failed - error: cant update payment", "cant update payment about transaction decline, payment_id={$payment_id}, now={$now}, {$resp}, aff={$aff}");
			}
			$db->q("INSERT INTO account_cc_fail (account_id, stamp, payment_id) values (?, ?, ?)", [$account_id, $now, $payment_id]);

			//display error message to client
			$error = "Transaction declined ({$error_code} - {$error_message}). Please double-check all your billing and credit card details and/or try to use different credit card";
			return 0;
		}

		//transaction succeeded
		$description = $tresponse->getMessages()[0]->getDescription();

		file_log("anet", "anet::do_charge(): Transaction succeeded: cc_id={$cc_id}, payment_id={$payment_id}, trans_id={$trans_id}, response_code={$response_code}, auth_code={$auth_code}, avs_result_code={$avs_result_code}, cvv_result_code={$cvv_result_code}, cavv_result_code={$cavv_result_code}, description='{$description}'");

		//update payment
		$resp = "ref_id={$ref_id}, trans_id={$trans_id}, response_code={$response_code}, auth_code={$auth_code}, avs_result_code={$avs_result_code}, cvv_result_code={$cvv_result_code}, cavv_result_code={$cavv_result_code}, trans_hash={$trans_hash}, account_number={$account_number}, account_type={$account_type}, description='{$description}'";
		$res = $db->q(
			"UPDATE payment SET result = ?, response = ?, responded_stamp = ?, trans_id = ? WHERE id = ? LIMIT 1",
			["A", $resp, $now, $trans_id, $payment_id]
			);
		$aff = $db->affected($res);
		if ($aff != 1) {
			reportAdmin("Payment succeeded - error: cant update payment", "cant update payment about transaction success, payment_id={$payment_id}, now={$now}, resp='{$resp}', aff={$aff}");
		}

		//TODO to remove
		//$m = "TODO - remove me: Transaction succeeded: cc_id={$cc_id}, payment_id={$payment_id}, {$resp}";
		//reportAdmin("AS: Payment suceeded", $m);
	
		//insert transaction
		$res = $db->q(
			"INSERT INTO transaction 
			(type, payment_id, stamp, trans_id, amount)
			VALUES
			(?, ?, ?, ?, ?)",
			["N", $payment_id, $now, $trans_id, $total]
			);
		$transaction_id = $db->insertid($res);
		if (!$transaction_id) {
			reportAdmin("AS: Payment succeeded - error: cant insert transaction", "cant insert transaction into db, payment_id={$payment_id}, trans_id={$trans_id}");
		}

		//insert purchase
		$db->q("INSERT INTO account_purchase
				(account_id, cc_id, `time`, `transaction_time`, payment_id, transaction_id, `what`, `total`, `saved`, `item_id`, `promo_code`) 
				values 
				(?, ?, CURDATE(), ?, ?, ?, ?, ?, ?, ?, ?)",
				[$account_id, $cc_id, $now, $payment_id, $trans_id, null, $total, '', null, intval($promocode_id)]
				);
		$purchase_id = $db->insertid();
		if (!$purchase_id) {
			file_log("anet", "anet::do_charge(): Error: cant insert purchase into db, payment_id={$payment_id}");
			reportAdmin("Payment succeeded - error: cant insert purchase", "cant insert purchase, payment_id={$payment_id}");
		} else {
			file_log("anet", "anet::do_charge(): purchase_id={$purchase_id}");
		}


		//-----------------------------
		// RECURRING CODE
		//-----------------------------
		if ($recurring_amount && $recurring_period) {
			// if payment has also recurring amount, create subscription

			$recurring_ref_id = "AS{$payment_id}R_".time();

			$s = new AnetAPI\ARBSubscriptionType();
			$s->setName("AS Advertisement subscription");

			$i = new AnetAPI\PaymentScheduleType\IntervalAType();
			$i->setLength($recurring_period);
			$i->setUnit("days");

			$ps = new AnetAPI\PaymentScheduleType();
			$ps->setInterval($i);
			$start_date = new \DateTime("now");
			$start_date->add(new \DateInterval("P{$recurring_period}D"));
			$next_renewal_date = $start_date->format("Y-m-d");
			$ps->setStartDate($start_date);
			$ps->setTotalOccurrences("9999");

			$s->setPaymentSchedule($ps);
			$s->setAmount($recurring_amount);

			$recurring_invoice_number = "AS{$payment_id}R";
			$or = new AnetAPI\OrderType();
			$or->setInvoiceNumber($recurring_invoice_number);
			$or->setDescription("Advertisement");

			$s->setOrder($or);
			$s->setPayment($p);
			$s->setBillTo($ca);

			$request = new AnetAPI\ARBCreateSubscriptionRequest();
			$request->setMerchantAuthentication($merchantAuthentication);
			$request->setRefId($recurring_ref_id);
			$request->setSubscription($s);
	
			$controller = new AnetController\ARBCreateSubscriptionController($request);
		
			$response = $controller->executeWithApiResponse(ANET_ENV);
		
			if (!$response) {
				//No response returned, this is error, should not happen
				file_log("anet", "anet::do_charge(): Recurring: Error: no response returned from anet API: cc_id={$cc_id}, payment_id={$payment_id}");	
				$m = "Recurring: No response returned from authorize.net API! cc_id={$cc_id}, payment_id={$payment_id}";
				reportAdmin("Payment failed - no response from anet", $m);
				//do not throw error, after all initial transaction succeeded, let admin take care of this and clean up the mess

			} else if ($response->getMessages()->getResultCode() != "Ok") {
				//API request wasn't successful, this should not happen, notify admin

				$error_code = $error_message = null;
				$errorMessages = $response->getMessages()->getMessage();
				$error_code = $errorMessages[0]->getCode();
				$error_message = $errorMessages[0]->getText();

				file_log("anet", "anet::do_charge(): Recurring: Error: Anet API request failed: cc_id={$cc_id}, payment_id={$payment_id}, error_code='{$error_code}', error_message='{$error_message}'");
				$m = "Recurring: Anet API request failed: cc_id={$cc_id}, payment_id={$payment_id}, error_code='{$error_code}', error_message='{$error_message}'";
				reportAdmin("Payment failed - anet api failed", $m);

				//do not throw error, after all initial transaction succeeded, let admin take care of this and clean up the mess

			} else {
			
				$code = $message = null;
				if (count($response->getMessages()->getMessage()) > 0) {
					$code = $response->getMessages()->getMessage()[0]->getCode();
					$message = $response->getMessages()->getMessage()[0]->getText();
				}
				$subscription_id = $response->getSubscriptionId();

				if (!$subscription_id) {
					//transaction failed
					file_log("anet", "anet::do_charge(): Recurring: Transaction failed: cc_id={$cc_id}, payment_id={$payment_id}, error_code='{$code}', error_message='{$message}'");

					//update payment
					$res = $db->q("UPDATE payment SET last_rebill_result = ? WHERE id = ? LIMIT 1", ["D", $payment_id]);
					$aff = $db->affected($res);
					if ($aff != 1) {
						reportAdmin("Payment failed - error: cant update payment", "Recurring: cant update payment about transaction decline, payment_id={$payment_id}, now={$now}, error_code={$code}, error_message={$message}, aff={$aff}");
					}

					//do not throw error, after all initial transaction succeeded, let admin take care of this and clean up the mess

				} else {

					//recurring transaction succeeded !
				
					//update payment
					$resp = "subscription_id={$subscription_id}, code={$code}, message='{$message}'";
					$res = $db->q(
						"UPDATE payment SET last_rebill_result = 'A', subscription_id = ?, subscription_status = 1, next_renewal_date = ? WHERE id = ? LIMIT 1",
						[$subscription_id, $next_renewal_date, $payment_id]
						);
					$aff = $db->affected($res);
					if ($aff != 1) {
						reportAdmin("AS: Payment ok, but recurr error: cant update payment", "payment_id={$payment_id}, now={$now}, resp='{$resp}', aff={$aff}");
					}

					//update transaction
					$res = $db->q("UPDATE transaction SET subscription_id = ? WHERE id = ? LIMIT 1", [$subscription_id, $transaction_id]);
					$aff = $db->affected($res);
					if ($aff != 1) {
						reportAdmin("AS: Payment ok, but recurr error: cant update transaction", "payment_id={$payment_id}, now={$now}, resp='{$resp}', aff={$aff}");
					}
				}
			}
		}
		//end of recurring processing

		if ($this->what == "classifieds")
			$db->q("INSERT INTO classifieds_stat (`time`, made) VALUES (curdate(), ?) on duplicate key update made = made + ?",	[$total, $total]);

		$this->purchaseSuccessful(null, $purchase_id);

		$this->afterPayment($payment_id);

		return true;
	}

	public static function purchaseSuccessful($what, $purchase_id) {
		global $db;

		file_log("anet", "anet::purchaseSuccessful(): what={$what}, purchase_id={$purchase_id}");

		if ($what == "classifieds") {
			$classifieds = new classifieds;
			$classifieds->purchaseSuccessful($purchase_id, "anet");
		} else if ($what == "businessowner") {
			$bo = new businessowner;
			$bo->purchaseSuccessful($purchase_id, "anet");
		} else if ($what == "advertise") {
			$adv = new advertise;
			$adv->purchaseSuccessful($purchase_id, "anet");
		} else if ($what == "vip") {
			$vip = new vip;
			$vip->purchaseSuccessful($purchase_id, "anet");
		} else if ($what == null) {
			//new way, we need to go through payment items
			$classifieds = new classifieds;
			$res = $db->q("SELECT pi.*, p.amount as total_amount, p.account_id
				FROM payment_item pi 
				INNER JOIN payment p on p.id = pi.payment_id
				INNER JOIN account_purchase ap on ap.payment_id = p.id
				WHERE ap.id = ?",
				[$purchase_id]
				);
			while ($row = $db->r($res)) {
				$payment_item_id = $row["id"];
				$account_id = $row["account_id"];
				$total_amount = $row["total_amount"];
				$payment_id = $row["payment_id"];
				$type = $row["type"];
				if ($type == "classified") {
					$clad_id = $row["classified_id"];
					$ret = $classifieds->aftersuccessfulPayment($payment_id, $clad_id);
					if (!$ret) {
						file_log("anet", "purchaseSuccessful(): classifieds::afterSuccessfulPayment() failed for payment_id={$payment_id}, clad_id={$clad_id} !");
						reportAdmin("AS: anet::purchaseSuccessful(): error", "classifieds::afterSuccessfulPayment() failed for payment_id={$payment_id}, clad_id={$clad_id} !");
					}
				} else if ($type == "advertise") {
					advertise::paymentSuccessful($account_id, $total_amount);

				} else if ($type == "repost") {
				
					$auto_renew = intval($row["auto_renew"]);
					if ($auto_renew) {
						$res2 = $db->q("UPDATE account SET repost = IFNULL(repost,0) + ? WHERE account_id = ? LIMIT 1", [$auto_renew, $account_id]);
						$aff = $db->affected($res2);
						if ($aff != 1) {
							file_log("anet", "purchaseSuccessful(): Can't add {$auto_renew} topup credits for account_id={$account_id} !");
							reportAdmin("AS: anet::purchaseSuccessful(): ERR", "Can't add {$auto_renew} topup credits for account_id={$account_id} !");
						}
					} else {
						file_log("anet", "purchaseSuccessful(): auto_renew not set for payment_item with type 'repost' ! payment_item_id={$payment_item_id}");
						reportAdmin("AS: anet::purchaseSuccessful(): ERR", "auto_renew not set for payment_item with type 'repost' ! payment_item_id={$payment_item_id}");
					}

				} else {
					file_log("anet", "purchaseSuccessful(): unknown payment item type'{$type}' !");
					reportAdmin("AS: anet::purchaseSuccessful(): unknown payment item type", "Implement handling for payment item type '{$type}' !");
				}
			}
		} else {
			debug_log("Error: anet::purchaseSuccessful() Unknown what: what='{$what}'");
			reportAdmin("AS: Error in anet::purchaseSuccessful() Unknown what", "what='{$what}'", array("what" => $what, "purchase_id" => $purchase_id));
		}
		return true;
	}

	public static function paymentSuccessful($payment_id) {
		global $db;

		file_log("anet", "anet::paymentSuccessful(): payment_id={$payment_id}");

		$classifieds = new classifieds;
		$res = $db->q("SELECT pi.*, p.amount as total_amount, p.account_id
			FROM payment_item pi 
			INNER JOIN payment p on p.id = pi.payment_id
			WHERE p.id = ?",
			[$payment_id]
			);
		while ($row = $db->r($res)) {
			$payment_item_id = $row["id"];
			$account_id = $row["account_id"];
			$total_amount = $row["total_amount"];
			$type = $row["type"];
			if ($type == "classified") {
				$clad_id = $row["classified_id"];
				$ret = $classifieds->afterSuccessfulPayment($payment_id, $clad_id);
				if (!$ret) {
					file_log("anet", "paymentSuccessful(): classifieds::afterSuccessfulPayment() failed for payment_id={$payment_id}, clad_id={$clad_id} !");
					reportAdmin("AS: anet::paymentSuccessful(): error", "classifieds::afterSuccessfulPayment() failed for payment_id={$payment_id}, clad_id={$clad_id} !");
				}
			} else if ($type == "advertise") {
				advertise::paymentSuccessful($account_id, $total_amount);

			} else if ($type == "repost") {
				
				$auto_renew = intval($row["auto_renew"]);
				if ($auto_renew) {
					$res2 = $db->q("UPDATE account SET repost = IFNULL(repost,0) + ? WHERE account_id = ? LIMIT 1", [$auto_renew, $account_id]);
					$aff = $db->affected($res2);
					if ($aff != 1) {
						file_log("anet", "purchaseSuccessful(): Can't add {$auto_renew} topup credits for account_id={$account_id} !");
						reportAdmin("AS: anet::purchaseSuccessful(): ERR", "Can't add {$auto_renew} topup credits for account_id={$account_id} !");
					}
				} else {
					file_log("anet", "purchaseSuccessful(): auto_renew not set for payment_item with type 'repost' ! payment_item_id={$payment_item_id}");
					reportAdmin("AS: anet::purchaseSuccessful(): ERR", "auto_renew not set for payment_item with type 'repost' ! payment_item_id={$payment_item_id}");
				}

			} else {
				file_log("anet", "paymentSuccessful(): unknown payment item type'{$type}' !");
				reportAdmin("AS: anet::paymentSuccessful(): unknown payment item type", "Implement handling for payment item type '{$type}' !");
			}
		}
		return true;
	}

	public static function afterPayment($payment_id) {
		global $account, $db;

		file_log("anet", "anet::afterPayment(): payment_id={$payment_id}");

		if ($account->isadmin()) {
			//if we are admin, redirect to sales page
			system::go("/mng/sales");
		}

		//get first payment item and redirect accordingly
		$res = $db->q("SELECT * FROM payment_item WHERE payment_id = ? LIMIT 1", [$payment_id]);
		if (!$db->numrows($res))
			system::go("/classifieds/myposts");

		$row = $db->r($res);
		//TODO
		if ($row["type"] == "classified") {
			if ($row["classified_status"] == "N")
				system::go("/adbuild/step5?ad_id={$row["classified_id"]}");
			else
				system::go("/classifieds/myposts");
		} else if ($row["type"] == "advertise") {
			system::go("/advertise");
		} else {
			system::go("/classifieds/myposts");
		}

		return true;
	}

	/**
	 * This is called when user after successful purchase get back on our website
	 * We need to find out what kind of purchase we have made and redirect user accordingly
	 */
	public function afterPurchase($succeeded = false, $real_purchase = true, $payment_id = NULL, $what = NULL, $item_id = NULL, $page = NULL, $p2 = NULL, $p3 = NULL) {
		global $db, $account, $ctx;

		$system = new system();

		if (!$payment_id || !$what) {
			reportAdmin("AS: anet::afterPurchase(): Error - no payment_id or what", "payment_id={$payment_id}, what={$what}");
			return false;
		}

		file_log("anet", "anet::afterPurchase(): succeeded=".intval($succeeded).", payment_id={$payment_id}, what={$what}, item_id={$item_id}, page={$page}, p2={$p2}, p3={$p3}");

		$p21 = $p22 = $p23 = $p24 = "";
		if (strpos($p2, "|") !== false) {
			$arr = explode("|", $p2);
			$p21 = $arr[0];
			$p22 = $arr[1];
			$p23 = $arr[2];
			$p24 = $arr[3];
		}

		$p31 = $p32 = $p33 = "";
		if (strpos($p3, "|") !== false) {
			$arr = explode("|", $p3);
			$p31 = $arr[0];
			$p32 = $arr[1];
			$p33 = $arr[2];
		}

		if ($succeeded && $real_purchase) {

			if (!$payment_id && is_bot()) {
				file_log("anet", "anet::afterPurchase(): is_bot");
				die;
			}

			//check if we have correct account_purchase entry for these params
			$res = $db->q("SELECT * FROM account_purchase WHERE `what` = ? AND payment_id = ?", [$what, $payment_id]);
			if ($db->numrows($res) != 1) {
				debug_log("anet:afterPurchase(): Error: Cant find purchase ! account_id={$account->getId()}, what={$what}, payment_id={$payment_id}");
				reportAdmin("AS: anet:afterPurchase(): Error: Cant find purchase", "", 
					array("account_id" => $account->getId(), "what" => $what, "payment_id" => $payment_id, "p2" => $p2, "p3" => $p3)
					);
				$system->go("/");
			}
			$row = $db->r($res);
			file_log("anet", "anet::afterPurchase(): purchase_id=".$row["id"]);
		}

		if ($what == "classifieds") {

			if ($p2 == "hotleads") {
				//this purchase was made from hotleads website
				file_log("anet", "anet::afterPurchase(): hotleads");
				$clad = clad::findOneById($item_id);

				//if ad was BP ad, remove this flag
				if ($clad->getBp())
					$res = $db->q("UPDATE classifieds SET bp = 0 WHERE id = ?", [$clad_id]);

				//redirect back to hotleads site
				$data = [
					"as_clad_id" => $item_id,
					"as_status" => "live",
					"as_url" => $clad->getUrl(),
					];
				$redirect_url = "https://hotleads.com/after-purchase?".http_build_query($data);
				return system::go($redirect_url);

			} else if ($p2 == "adbuild") {

				if ($succeeded) {
					$clad = clad::findOneById($item_id);
					if (!$clad) {
						debug_log("anet:afterPurchase(): Error: Cant find clad ! account_id={$account->getId()}, what={$what}, payment_id={$payment_id}, item_id={$item_id}");
						reportAdmin("AS: anet:afterPurchase(): Error: Cant find clad", "", 
							array("account_id" => $account->getId(), "what" => $what, "payment_id" => $payment_id, "item_id" => $item_id, "p2" => $p2, "p3" => $p3)
						);
						$system->go("/");
					}

					$system->moved("/adbuild/step5?ad_id={$item_id}");
					
					die;
				} else {
					$system->go("/adbuild/step4?ad_id={$item_id}&denied=1");
					die;
				}

			} else if ($p21 == "side") {

				if ($succeeded)
					$system->go("/classifieds/myposts", "you have successfully purchase side sponsor for your ad.");
				else
					$system->go("/classifieds/myposts", "you have failed to purchase side sponsor for your ad.");

			} else if ($p21 == "city_thumbnail") {

				if ($succeeded)
					$system->go("/classifieds/myposts", "You have successfully purchased city thumbnail for your ad.");
				else
					$system->go("/classifieds/myposts", "You have failed to purchase city thumbnail for your ad.");

			} else if ($p21 == "upgrade") {

				if ($succeeded)
					$system->go("/classifieds/myposts", "You have successfully purchased reposts for your ad.");
				else
					$system->go("/classifieds/myposts", "You have failed to purchase reposts for your ad.");

			} else if ($p21 == "recurring") {

				if ($succeeded)
					$system->go("/classifieds/myposts", "You have successfully purchased recurring reposting for your ad.");
				else
					$system->go("/classifieds/myposts", "You have failed to purchase recurring reposting for your ad.");

			} else {
				file_log("classifieds", "classifieds::purchaseSuccessful(): Error: Unknown p2='{$p2}' !");
				reportAdmin("AS: classifieds::purchaseSuccessful(): Error: Unknown p2", "P2='{$p2}'",
					array("account_id" => $acc->getId(), "purchase_id" => $purchase_id, "p2" => $p2)
				);
			}

		} else if ($what == "advertise") {

			if ($succeeded) {
				$system->go("/advertise/", "Thank You!, Your funds have been added to your account.");
				die;
			} else {
				$system->go("/advertise/addfunds", "Error: Your transaction has been denied. Please check the payment details and try again.");
				die;
			}

		} else if ($what == "businessowner") {

			if ($succeeded) {
				switch($p31) {
					case 'sc':
						$system->go("http://{$ctx->domain_base}/strip-clubs/club?id=".$p2, "Thank you for your payment. Your business will be upgraded in next 5 minutes.");
						break;
					case 'emp':
						$system->go("http://{$ctx->domain_base}/erotic-massage/parlor?id=".$p2, "Thank you for your payment. Your business will be upgraded in next 5 minutes.");
						break;
					case 'as':
						$system->go("http://{$ctx->domain_base}/sex-shops/store?id=".$p2, "Thank you for your payment. Your business will be upgraded in next 5 minutes.");
						break;
					case 'lingerie':
						$system->go("http://{$ctx->domain_base}/lingerie-modeling-1on1/store?id=".$p2, "Thank you for your payment. Your business will be upgraded in next 5 minutes.");
						break;
				}
			}
			$system->go("/");

		} else if ($what == "vip") {

			if ($succeeded) {
				$system->go("/", "Thank You!, You are VIP member now.");
				die;
			} else {
				$system->go("/vip/step2", "Error: Your transaction has been denied. Please check the payment details and try again.");
				die;
			}

		} else {
			debug_log("anet:afterPurchase(): Error: Unknown what '{$what}' ! payment_id={$payment_id}, item_id={$item_id}");
			reportAdmin("AS: anet:afterPurchase(): Error: Unknown what", "", 
				array("account_id" => $account->getId(), "what" => $what, "payment_id" => $payment_id, "item_id" => $item_id, "p2" => $p2, "p3" => $p3)
				);
			$system->go("/");
		}

		//default action after not succeeding
		reportAdmin("AS: anet::afterPurchase(): no action for not succeeding", "", 
			array("account_id" => $account->getId(), "what" => $what, "payment_id" => $payment_id, "item_id" => $item_id, "p2" => $p2, "p3" => $p3));

		$system->go("/");
	}

	public function updatecc($cc_id) {
		return false;
	}

	public function auth($params, &$error = NULL) {
		return false;
	}

	private function refundTransaction($trans_id, $cc_row, $cardno_last_4, $amount, &$error) {

		//init
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);
		$logger = LogFactory::getLog(get_class($this));
		
		$ref_id = "AS_REF_".time();

		$cc = new AnetAPI\CreditCardType();
		$cc->setCardNumber($cardno_last_4);
		$cc->setExpirationDate("XXXX");

		$p = new AnetAPI\PaymentType();
		$p->setCreditCard($cc);

		$ca = new AnetAPI\CustomerAddressType();
		$ca->setFirstName($cc_row["firstname"]);
		$ca->setLastName($cc_row["lastname"]);
		$ca->setAddress($cc_row["address"]);
		$ca->setCity($cc_row["city"]);
		$ca->setState($cc_row["state"]);
		$ca->setZip($cc_row["zipcode"]);
		$ca->setCountry($cc_row["country"]);

		$set1 = new AnetAPI\SettingType();
		$set1->setSettingName("duplicateWindow");
		$set1->setSettingValue($this->duplicate_txn_window);

		$tr = new AnetAPI\TransactionRequestType();
		$tr->setTransactionType("refundTransaction"); 
		$tr->setAmount($amount);
		$tr->setPayment($p);
		$tr->setBillTo($ca);
		$tr->setRefTransId($trans_id);
		$tr->addToTransactionSettings($set1);
	 
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($ref_id);
		$request->setTransactionRequest($tr);
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse(ANET_ENV);

		if (!$response) {
			//No response returned, this is error, should not happen
			file_log("anet", "anet::refundTransaction(): Error: no response returned from anet API: trans_id={$trans_id}, cardno_last_4={$cardno_last_4}, amount={$amount}, ");
			$m = "No response returned from authorize.net API! trans_id={$trans_id}, cardno_last_4={$cardno_last_4}, amount={$amount}, ";
			reportAdmin("AS: Refund failed - no response from anet", $m);
			$error = "ANET API Error: no response returned";
			return false;
		}

		$tresponse = $response->getTransactionResponse();

		$refund_trans_id = $response_code = $trans_hash = $account_number = $account_type = null;
		if ($tresponse) {
			$refund_trans_id = $tresponse->getTransId();
			$response_code = $tresponse->getResponseCode();		//1 = Approved, 2 = Declined, 3 = Error, 4 = Held for Review
			$trans_hash = $tresponse->getTransHash();
			$account_number = $tresponse->getAccountNumber();
			$account_type = $tresponse->getAccountType();
		}

		if($response->getMessages()->getResultCode() != "Ok") {
			//API request wasn't successful, this should not happen, notify admin

			$error_code = $error_message = null;
			if ($tresponse != null && $tresponse->getErrors() != null) {
				$error_code = $tresponse->getErrors()[0]->getErrorCode();
				$error_message = $tresponse->getErrors()[0]->getErrorText();
			} else {
				$error_code = $response->getMessages()->getMessage()[0]->getCode();
				$error_message = $response->getMessages()->getMessage()[0]->getText();
			}

			file_log("anet", "anet::refundTransaction(): Error: Anet API request failed: trans_id={$trans_id}, cardno_last_4={$cardno_last_4}, amount={$amount}, error_code='{$error_code}', error_message='{$error_message}'");
			$m = "Anet API request failed: trans_id={$trans_id}, cardno_last_4={$cardno_last_4}, amount={$amount}, error_code='{$error_code}', error_message='{$error_message}'";
			reportAdmin("AS: Refund failed - anet api failed", $m);
			$error = "Refund failed: {$error_message} (error_code = {$error_code})";
			return false;

		} else {
			// API request succeeeded, lets see if refund succeeded or failed
	
			if ($tresponse == null || $tresponse->getMessages() == null) {
				//refund failed
				if ($tresponse->getErrors() != null) {
					$error_code = $tresponse->getErrors()[0]->getErrorCode();
					$error_message = $tresponse->getErrors()[0]->getErrorText();		
				}
				file_log("anet", "anet::refundTransaction(): Refund failedError: Anet API request failed: trans_id={$trans_id}, cardno_last_4={$cardno_last_4}, amount={$amount}, error_code='{$error_code}', error_message='{$error_message}'");
				$m = "Anet API request failed: trans_id={$trans_id}, cardno_last_4={$cardno_last_4}, amount={$amount}, error_code='{$error_code}', error_message='{$error_message}'";
				reportAdmin("AS: Refund failed - anet api failed", $m);
				$error = "Refund failed: {$error_message} (error_code = {$error_code})";
				return false;
			}

			//refund succeeded
			$code = $tresponse->getMessages()[0]->getCode();
			$description = $tresponse->getMessages()[0]->getDescription();

			$desc = "Refund succeeded: amount={$amount}, code={$code}, ref_id={$ref_id}, refund_trans_id={$refund_trans_id}, response_code={$response_code}, trans_hash={$trans_hash}, account_number={$account_number}, account_type={$account_type}, description='{$description}";

			file_log("anet", "anet::refundTransaction(): {$desc}");

		   	return $desc;
		}

		return false;
	}

	private function voidTransaction($trans_id, &$error) {

		//TODO - disable
		file_log("anet", "anet::voidTransaction(): disabled");
        return true;
		
		//init
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);
		$logger = LogFactory::getLog(get_class($this));
		
		$ref_id = "AS_VOI_".time();

		$tr = new AnetAPI\TransactionRequestType();
		$tr->setTransactionType("voidTransaction"); 
		$tr->setRefTransId($trans_id);

		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($ref_id);
		$request->setTransactionRequest($tr);
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse(ANET_ENV);

		if (!$response) {
			//No response returned, this is error, should not happen
			file_log("anet", "anet::voidTransaction(): Error: no response returned from anet API: trans_id={$trans_id}");
			reportAdmin("AS: Void failed - no response from anet", "No response returned from authorize.net API! trans_id={$trans_id}");
			$error = "ANET API Error: no response returned";
			return false;
		}

		$tresponse = $response->getTransactionResponse();

		$void_trans_id = $response_code = $trans_hash = $account_number = $account_type = null;
		if ($tresponse) {
			$void_trans_id = $tresponse->getTransId();
			$response_code = $tresponse->getResponseCode();		//1 = Approved, 2 = Declined, 3 = Error, 4 = Held for Review
			$trans_hash = $tresponse->getTransHash();
			$account_number = $tresponse->getAccountNumber();
			$account_type = $tresponse->getAccountType();
		}

		if($response->getMessages()->getResultCode() != "Ok") {
			//API request wasn't successful, this should not happen, notify admin

			$error_code = $error_message = null;
			if ($tresponse != null && $tresponse->getErrors() != null) {
				$error_code = $tresponse->getErrors()[0]->getErrorCode();
				$error_message = $tresponse->getErrors()[0]->getErrorText();
			} else {
				$error_code = $response->getMessages()->getMessage()[0]->getCode();
				$error_message = $response->getMessages()->getMessage()[0]->getText();
			}

			$resp = "trans_id={$trans_id}, error_code='{$error_code}', error_message='{$error_message}'";
			file_log("anet", "anet::voidTransaction(): Error: Anet API request failed: {$resp}");
			reportAdmin("AS: Void failed - anet api failed", "Anet API request failed: {$resp}");
			$error = "Void failed: {$error_message} (error_code = {$error_code})";
			return false;

		} else {
			// API request succeeeded, lets see if void succeeded or failed
	
			if ($tresponse == null || $tresponse->getMessages() == null) {
				//void failed
				if ($tresponse->getErrors() != null) {
					$error_code = $tresponse->getErrors()[0]->getErrorCode();
					$error_message = $tresponse->getErrors()[0]->getErrorText();		
				}
				$resp = "trans_id={$trans_id}, error_code='{$error_code}', error_message='{$error_message}'";
				file_log("anet", "anet::voidTransaction(): Void failedError: Anet API request failed: {$resp}");
				reportAdmin("AS: Void failed - anet api failed", "Anet API request failed: {$resp}");
				$error = "Void failed: {$error_message} (error_code = {$error_code})";
				return false;
			}

			//void succeeded
			$code = $tresponse->getMessages()[0]->getCode();
			$description = $tresponse->getMessages()[0]->getDescription();

			$desc = "Void succeeded: amount={$amount}, code={$code}, ref_id={$ref_id}, void_trans_id={$void_trans_id}, response_code={$response_code}, trans_hash={$trans_hash}, account_number={$account_number}, account_type={$account_type}, description='{$description}";

			file_log("anet", "anet::voidTransaction(): {$desc}");

		   	return $desc;
		}

		return false;
	}

	public static function cancelSubscription($subscription_id, &$error = "") {

		//TODO - disable
		file_log("anet", "anet::cancelSubscription(): disabled");
        return true;
		
		//init
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);
		$logger = LogFactory::getLog(get_class($this));
		
		$ref_id = "AS_SCA_".time();

		$request = new AnetAPI\ARBCancelSubscriptionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($ref_id);
		$request->setSubscriptionId($subscription_id);
		$controller = new AnetController\ARBCancelSubscriptionController($request);
		$response = $controller->executeWithApiResponse(ANET_ENV);

		if (!$response) {
			//No response returned, this is error, should not happen
			file_log("anet", "anet::cancelSubscription(): Error: no response returned from anet API: subscription_id={$subscription_id}");
			$m = "No response returned from authorize.net API! subscription_id={$subscription_id} ";
			reportAdmin("AS: Subscription cancel failed - no response from anet", $m);
			$error = "ANET API Error: no response returned";
			return false;
		}

		if ($response->getMessages()->getResultCode() != "Ok") {
			//API request wasn't successful, this should not happen, notify admin

			$errorMessages = $response->getMessages()->getMessage();
			$error_code = $errorMessages[0]->getCode();
			$error_message = $errorMessages[0]->getText();

			file_log("anet", "anet::cancelSubscription(): Error: Anet API request failed: subscription_id={$subscription_id}, error_code='{$error_code}', error_message='{$error_message}'");
			$m = "Anet API request failed: subscription_id={$subscription_id}, error_code='{$error_code}', error_message='{$error_message}'";
			reportAdmin("AS: Subscription cancel failed - anet api failed", $m);
			$error = "Subscription cancel failed: {$error_message} (error_code = {$error_code})";
			return false;

		}

		//subscription cancel request succeeded
		$code = $message = null;
		if (count($response->getMessages()->getMessage()) > 0) {
			$code = $response->getMessages()->getMessage()[0]->getCode();
			$message = $response->getMessages()->getMessage()[0]->getText();
		}
		file_log("anet", "anet::cancelSubscription(): Success: subscription_id={$subscription_id}, code='{$code}', message='{$message}'");

		return true;
	}

	private function transactionDetails($trans_id, &$error = "") {
		
		//init
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);
		$logger = LogFactory::getLog(get_class($this));
		
		$ref_id = "AS_TDE_".time();

		$request = new AnetAPI\GetTransactionDetailsRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setTransId($trans_id);
		$request->setRefId($ref_id);

		$controller = new AnetController\GetTransactionDetailsController($request);
		$response = $controller->executeWithApiResponse(ANET_ENV);

		if (!$response) {
			//No response returned, this is error, should not happen
			file_log("anet", "anet::transactionDetails(): Error: no response returned from anet API: trans_id={$trans_id}");
			$m = "No response returned from authorize.net API! trans_id={$trans_id} ";
			reportAdmin("AS: ANET Txn Details failed - no response from anet", $m);
			$error = "ANET API Error: no response returned";
			return false;
		}

		if ($response->getMessages()->getResultCode() != "Ok") {
			//API request wasn't successful, this should not happen, notify admin

			$errorMessages = $response->getMessages()->getMessage();
			$error_code = $errorMessages[0]->getCode();
			$error_message = $errorMessages[0]->getText();

			$resp = "trans_id={$trans_id}, error_code='{$error_code}', error_message='{$error_message}'";
			file_log("anet", "anet::transactionDetails(): Error: Anet API request failed: {$resp}");
			reportAdmin("AS: Transaction Details Anet API failed", "Anet API request failed: {$resp}");
			$error = "Transaction Details failed: {$error_message} (error_code = {$error_code})";
			return false;
		}

		//transaction detail request succeeded
		$code = $message = $transaction = null;
		if (count($response->getMessages()->getMessage()) > 0) {
			$code = $response->getMessages()->getMessage()[0]->getCode();
			$message = $response->getMessages()->getMessage()[0]->getText();
		}
		$transaction = $response->getTransaction();
		file_log("anet", "anet::transactionDetails(): Success: trans_id={$trans_id}, code='{$code}', message='{$message}', status='{$transaction->getTransactionStatus()}', response_code={$transaction->getResponseCode()}");

		return $transaction;
	}

	public static function getSubscription($subscription_id, &$error = "") {
		
		//init
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);
		$logger = LogFactory::getLog(get_class($this));
		
		$ref_id = "AS_GSU_".time();

		$request = new AnetAPI\ARBGetSubscriptionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($ref_id);
		$request->setSubscriptionId($subscription_id);
		$request->setIncludeTransactions(true);

		$controller = new AnetController\ARBGetSubscriptionController($request);
		$response = $controller->executeWithApiResponse(ANET_ENV);

		if (!$response) {
			//No response returned, this is error, should not happen
			file_log("anet", "anet::getSubscription(): Error: no response returned from anet API: subscription_id={$subscription_id}");
			$m = "No response returned from authorize.net API! subscription_id={$subscription_id} ";
			reportAdmin("AS: ANET getSubscription failed - no response from anet", $m);
			$error = "ANET API Error: no response returned";
			return false;
		}

		if ($response->getMessages()->getResultCode() != "Ok") {
			//API request wasn't successful, this should not happen, notify admin

			$errorMessages = $response->getMessages()->getMessage();
			$error_code = $errorMessages[0]->getCode();
			$error_message = $errorMessages[0]->getText();

			$resp = "subscription_id={$subscription_id}, error_code='{$error_code}', error_message='{$error_message}'";
			file_log("anet", "anet::getSubscription(): Error: Anet API request failed: {$resp}");
			reportAdmin("AS: getSubscription Anet API failed", "Anet API request failed: {$resp}");
			$error = "getSubscription failed: {$error_message} (error_code = {$error_code})";
			return false;
		}

		//getSubscription request succeeded
		$code = $message = $subscription = null;
		//if (count($response->getMessages()->getMessage()) > 0) {
		//	$code = $response->getMessages()->getMessage()[0]->getCode();
		//	$message = $response->getMessages()->getMessage()[0]->getText();
		//}
		$subscription = $response->getSubscription();
		file_log("anet", "anet::getSubscription(): Success: subscription_id={$subscription_id}, status='{$subscription->getStatus()}', amount={$subscription->getAmount()}, #transactions=".count($subscription->getArbTransactions()));

		return $subscription;
	}

	public static function getSubscriptionStatus($subscription_id, &$error = "") {
		
		//init
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(MERCHANT_LOGIN_ID);
		$merchantAuthentication->setTransactionKey(MERCHANT_TRANSACTION_KEY);
		$logger = LogFactory::getLog(get_class($this));
		
		$ref_id = "AS_GSS_".time();

		$request = new AnetAPI\ARBGetSubscriptionStatusRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($ref_id);
		$request->setSubscriptionId($subscription_id);

		$controller = new AnetController\ARBGetSubscriptionStatusController($request);
		$response = $controller->executeWithApiResponse(ANET_ENV);

		if (!$response) {
			//No response returned, this is error, should not happen
			file_log("anet", "anet::getSubscriptionStatus(): Error: no response returned from anet API: subscription_id={$subscription_id}");
			$m = "No response returned from authorize.net API! subscription_id={$subscription_id} ";
			reportAdmin("AS: ANET getSubscriptionStatus failed - no response from anet", $m);
			$error = "ANET API Error: no response returned";
			return false;
		}

		if ($response->getMessages()->getResultCode() != "Ok") {
			//API request wasn't successful, this should not happen, notify admin

			$errorMessages = $response->getMessages()->getMessage();
			$error_code = $errorMessages[0]->getCode();
			$error_message = $errorMessages[0]->getText();

			$resp = "subscription_id={$subscription_id}, error_code='{$error_code}', error_message='{$error_message}'";
			file_log("anet", "anet::getSubscriptionStatus(): Error: Anet API request failed: {$resp}");
			reportAdmin("AS: getSubscriptionStatus Anet API failed", "Anet API request failed: {$resp}");
			$error = "getSubscriptionStatus failed: {$error_message} (error_code = {$error_code})";
			return false;
		}

		//getSubscriptionStatus request succeeded
		$status = $response->getStatus();
		file_log("anet", "anet::getSubscriptionStatus(): Success: subscription_id={$subscription_id}, status='{$status}'");

		return $status;
	}

	public function refundByPurchaseId($purchase_id, $amount, $refund_reason = "", $description = "", $cancel_subscription = false, &$error) {
		global $db, $account;

		file_log("anet", "anet::refundByPurchaseId(): purchase_id={$purchase_id}, amount={$amount}, refund_reason='{$refund_reason}', description='{$description}', cancel_subscription=".intval($cancel_subscription));
		
		//TODO - disable
		file_log("anet", "anet::refundByPurchaseId(): disabled");
        return true;
		
		$res = $db->q("SELECT ap.*
						FROM account_purchase ap
						WHERE ap.id = ?
						LIMIT 1",
						[$purchase_id]);
		if ($db->numrows($res) != 1) {
			$error = "Can't find purchase by id #{$purchase_id}";
			return false;
		}

		$row = $db->r($res);
		$purchase_trans_id = $row["transaction_id"];
		$payment_id = $row["payment_id"];
		$total = $row["total"];
		$email = $row["email"];
		$account_id = $row["account_id"];
		$cc_id = $row["cc_id"];
		$item_id = $row["item_id"];

		if ($total < 0.01) {
			//either this was zero purchase (should not happen ?) - and there is nothing to refund
			//or this was already refunded
			//do not refund
			$error = "Purchase #{$purchase_id} was already refunded ?";
			return false;
		}

		if (!$purchase_trans_id || !$payment_id) {
			$error = "Invalid trans_id or payment_id";
			return false;
		}

		if (!$amount)
			$amount = $total;
		$new_total = number_format($total - $amount, 2, ".", "");
		if ($new_total < 0)
			$new_total = 0;

		//fetch payment
		$res = $db->q("SELECT p.* FROM payment p WHERE p.id = ? LIMIT 1", [$payment_id]);
		if ($db->numrows($res) != 1) {
			$error = "Can't find payment by id #{$payment_id}";
			return false;
		}
		$row = $db->r($res);
		$cc_id = $row["cc_id"];
		$payment_trans_id = $row["trans_id"];
		$subscription_id = $row["subscription_id"];

		//fetch cc
		$res = $db->q("SELECT cc.* FROM account_cc cc WHERE cc.cc_id = ? LIMIT 1", [$cc_id]);
		if ($db->numrows($res) != 1) {
			$error = "Can't find cc by id #{$cc_id}";
			return false;
		}
		$cc_row = $row = $db->r($res);
		$cardno_last_4 = substr($row["cc"], -4);

		//fetch transaction
		$res = $db->q("SELECT t.* FROM transaction t WHERE t.trans_id = ? LIMIT 1", [$purchase_trans_id]);
		if ($db->numrows($res) != 1) {
			$error = "Can't find transaction by trans_id #{$purchase_trans_id}";
			return false;
		}
		$row = $db->r($res);
		$transaction_id = $row["id"];
		$trans_date_time = date("m/d/Y H:i:s T", $row["stamp"]);	//for receipt	


		//--------------------------------------------------------------
		//find out status of this transaction (if it was settled or not)
		$transaction = $this->transactionDetails($purchase_trans_id, $error);
		if ($transaction === false) {
			//transaction details anet api call failed ?
			return false;
		}
		if ($transaction->getResponseCode() != 1 && $transaction->getResponseCode() != 4) {
			//overall status of transactin is not approved ? (it could be declined)
			$error = "Overall status of transaction #{$purchase_trans_id} is not approved, but it is '{$transaction->getResponseCode()}' !";
			return false;
		}
		$type = $event_message = $verb = "";
		if ($transaction->getTransactionStatus() == "capturedPendingSettlement") {
			//this transaction is not settled yet, do void
			$ret = $this->voidTransaction($purchase_trans_id, $error);
			$type = "V";
			$event_message = "Voided transaction #{$purchase_trans_id} (amount {$amount})";
			$verb = "voided";
		} else if ($transaction->getTransactionStatus() == "settledSuccessfully") {
			//--------------------------------------
			//this transaction is settled, do refund
			$ret = $this->refundTransaction($purchase_trans_id, $cc_row, $cardno_last_4, $amount, $error);
			$type = "R";
			$event_message = "Refunded transaction #{$purchase_trans_id} in amount {$amount}";
			$verb = "refunded";
		} else if ($transaction->getTransactionStatus() == "voided") {
			//transaction already voided (probably we did this void manually through anet admin panel)
			$type = "V";
			$event_message = "Voided transaction #{$purchase_trans_id} (amount {$amount})";
			$verb = "voided";
		} else {
			//handling of this transaction status not implemented
			reportAdmin("AS: anet::refundByPurchaseId error", "Handling of this transaction status: '{$transaction->getTransactionStatus()}' not implemented", ["purchase_trans_id" => $purchase_trans_id, "transactionStatus" => $transaction->getTransactionStatus()]);
			$error = "Transaction status '{$transaction->getTransactionStatus()}' not supported by refund procedure, please contact admin";
			return false;
		}


		if ($ret === false) {
			//refund failed
			return false;
		}
		$desc = $ret;

		//refund succeeded

		//update account_purchase table
		$db->q("UPDATE account_purchase SET total = ? WHERE id = ? LIMIT 1", [$new_total, $purchase_id]);
		if ($db->affected() != 1) {
			//this should not happen
			reportAdmin("AS: anet::refundByPurchaseId : cant update original purchase", "", ["new_total" => $new_total, "purchase_id" => $purchase_id]);
		}

		//insert refund into account_refund table
		$now = time();
		$now_datetime = date("Y-m-d H:i:s", $now);
		$db->q("INSERT INTO account_refund 
				(type, transaction_time, author_id, payment_id, account_id, `time`, cc_id, purchase_id, item_id, amount, reason)
				VALUES
				(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
				array($type, $now, $account->getId(), $payment_id, $account_id, $now_datetime, $cc_id, $purchase_id, $item_id, $amount, $refund_reason));
		$id = $db->insertid();
		if (!$id) {
			//this should not happen
			reportAdmin("AS: anet::refund : cant insert into account_refund", "", ["type" => $type, "now" => $now, "author_id" => $account->getId(), "payment_id" => $payment_id, "account_id" => $account_id, "now_datetime" => $now_datetime, "cc_id" => $cc_id, "item_id" => $item_id, "amount" => $amount, "reason" => $reason, "purchase_id" => $purchase_id]);
		}

		//insert payment event
		$db->q("INSERT INTO payment_event
				(payment_id, transaction_id, author_id, stamp, type, message, post)
				VALUES
				(?, ?, ?, ?, ?, ?, ?)
				",
				[$payment_id, $transaction_id, $account->getId(), $now, $type, $event_message, $desc]
				);
		$id = $db->insertid();
		if (!$id) {
			//this should not happen
			reportAdmin("AS: anet::refund : cant insert into payment_event", "", ["stamp" => $now, "author_id" => $account->getId(), "payment_id" => $payment_id, "type" => $type, "event_message" => $event_message, "purchase_trans_id" => $purchase_trans_id, "amount" => $amount]);
		}

		//update transaction table
		$db->q("UPDATE transaction SET amount = ? WHERE id = ? LIMIT 1", [$new_total, $transaction_id]);
		$aff = $db->affected($res);
		if ($aff != 1) {
			//this should not happen
			reportAdmin("AS: anet::refund : cant update transaction", "", ["new_total" => $new_total, "id" => $transaction_id]);
		}

		$msg = "Transaction '{$purchase_trans_id}' successfully {$verb} (\${$amount}).";
		$this->refundReceipt($email, $purchase_trans_id, $trans_date_time, $amount, $cardno_last_4);


		//---------------------------------------------------------
		//if we also want to wish to cancel subscription, cancel it
		//if ($cancel_subscription && $subscription_id && $payment_trans_id == $purchase_trans_id) {
		if ($cancel_subscription && $subscription_id) {
			file_log("anet", "anet::refundByPurchaseId(): cancelling subscription subscription_id={$subscription_id}");
			$ret = $this->cancelSubscription($subscription_id, $error);
			if (!$ret) {
				$error = "Transaction refunded successfully, but subscription cancellation failed: {$error} !";
				return false;
			}
			//insert payment event about cancelling subscription
			$db->q("INSERT INTO payment_event
					(payment_id, subscription_id, author_id, stamp, type, message)
					VALUES
					(?, ?, ?, ?, ?, ?)
					",
					[$payment_id, $subscription_id, $account->getId(), $now, "SC", "Subscription cancelled"]
					);
			$id = $db->insertid();
			if (!$id) {
				//this should not happen
				file_log("anet", "anet::refundByPurchaseId(): Error: Cant insert SC into payment_event! author_id={$account->getId()}, payment_id={$payment_id}, subscription_id={$subscription_id}");
				reportAdmin("AS: anet::refundByPurchaseId(): Error", "Cant insert SC into payment_event", ["stamp" => $now, "author_id" => $account->getId(), "payment_id" => $payment_id, "type" => "SC", "subscription_id" => $subscription_id]);
			}
			//update payment
			$res = $db->q("UPDATE payment SET subscription_status = 2 WHERE id = ? LIMIT 1", [$payment_id]);
			$aff = $db->affected($res);
			if ($aff != 1) {
				//this should not happen
				file_log("anet", "anet::refundByPurchaseId(): Error: Cant update subscription_status in payment table! payment_id={$payment_id}, subscription_id={$subscription_id}");
				reportAdmin("AS: anet::refundByPurchaseId error", "Cant update subscription_status in payment table", ["payment_id" => $payment_id, "subscription_id" => $subscription_id]);
			}
			$msg .= " Subscription '{$subscription_id}' cancelled.";

//		} else if ($cancel_subscription) {
//			//TODO this needs work
//			reportAdmin("AS: anet::refundbyPurchaseId(): cancelling subscription on not first transaction ?", "", ["subscription_id" => $subscription_id, "purchase_trans_id" => $purchase_trans_id, "payment_trans_id" => $payment_trans_id]);
		}

		//everything allright
		return $msg;
	}

	private function refundReceipt($email, $trans_id, $trans_date_time, $amount, $cardno_last_4) {

		if (!$email || !$trans_id || !$amount)
			return false;

		$smarty = GetSmartyInstance();
		$smarty->assign("trans_id", $trans_id);
		$smarty->assign("trans_date_time", $trans_date_time);
		$smarty->assign("amount", $amount);
		$smarty->assign("cardno_last_4", $cardno_last_4);

		$html = $smarty->fetch(_CMS_ABS_PATH."/templates/payment/refund_viphost.html.tpl");
		$text = $smarty->fetch(_CMS_ABS_PATH."/templates/payment/refund_viphost.txt.tpl");
		$subject = "Transaction #{$trans_id} refunded - receipt";
		$embedded_images = array("logo_email" => _CMS_ABS_PATH."/images/logo_email_vip.png");

		return email_gmail("receipts@viphost.com", "Receipts", $email, $subject, $html, $text, $embedded_images);
	}

	public function chargebackByPurchaseId($purchase_id, $amount, $cancel_subscription = false, &$error) {
		global $db, $account;

		file_log("anet", "anet::chargebackByPurchaseId(): purchase_id={$purchase_id}, amount={$amount}, cancel_subscription=".intval($cancel_subscription));
		
		$res = $db->q("SELECT ap.*
						FROM account_purchase ap
						WHERE ap.id = ?
						LIMIT 1",
						[$purchase_id]);
		if ($db->numrows($res) != 1) {
			$error = "Can't find purchase by id #{$purchase_id}";
			return false;
		}

		$row = $db->r($res);
		$purchase_trans_id = $row["transaction_id"];
		$payment_id = $row["payment_id"];
		$total = $row["total"];
		$email = $row["email"];
		$account_id = $row["account_id"];
		$cc_id = $row["cc_id"];
		$item_id = $row["item_id"];

		if ($total < 0.01) {
			//either this was zero purchase (should not happen ?) - and there is nothing to chargeback
			//or this was already refunded/chargebacked
			//do not chargeback
			$error = "Purchase #{$purchase_id} was already refunded/chargebacked ?";
			return false;
		}

		if (!$purchase_trans_id || !$payment_id) {
			$error = "Invalid trans_id or payment_id";
			return false;
		}

		if (!$amount)
			$amount = $total;
		$new_total = number_format($total - $amount, 2, ".", "");
		if ($new_total < 0)
			$new_total = 0;

		//fetch payment
		$res = $db->q("SELECT p.* FROM payment p WHERE p.id = ? LIMIT 1", [$payment_id]);
		if ($db->numrows($res) != 1) {
			$error = "Can't find payment by id #{$payment_id}";
			return false;
		}
		$row = $db->r($res);
		$cc_id = $row["cc_id"];
		$payment_trans_id = $row["trans_id"];
		$subscription_id = $row["subscription_id"];

		//fetch cc
		$res = $db->q("SELECT cc.* FROM account_cc cc WHERE cc.cc_id = ? LIMIT 1", [$cc_id]);
		if ($db->numrows($res) != 1) {
			$error = "Can't find cc by id #{$cc_id}";
			return false;
		}
		$cc_row = $row = $db->r($res);
		$cardno_last_4 = substr($row["cc"], -4);

		//fetch transaction
		$res = $db->q("SELECT t.* FROM transaction t WHERE t.trans_id = ? LIMIT 1", [$purchase_trans_id]);
		if ($db->numrows($res) != 1) {
			$error = "Can't find transaction by trans_id #{$purchase_trans_id}";
			return false;
		}
		$row = $db->r($res);
		$transaction_id = $row["id"];
		$trans_date_time = date("m/d/Y H:i:s T", $row["stamp"]);	//for receipt	

		//we don't actually do anything with the transaction on authorize.net side - in there transactions is normally successfully settled
		//so when we are doing reports from ANET, we need to adjust for chargebacks ...

		//update account_purchase table
		$db->q("UPDATE account_purchase SET total = ? WHERE id = ? LIMIT 1", [$new_total, $purchase_id]);
		if ($db->affected() != 1) {
			//this should not happen
			reportAdmin("AS: anet::chargebackByPurchaseId : cant update original purchase", "", ["new_total" => $new_total, "purchase_id" => $purchase_id]);
		}

		//insert chargeback into account_refund table
		$now = time();
		$now_datetime = date("Y-m-d H:i:s", $now);
		$db->q("INSERT INTO account_refund 
				(type, transaction_time, author_id, payment_id, account_id, `time`, cc_id, purchase_id, item_id, amount, reason)
				VALUES
				(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
				array("CH", $now, $account->getId(), $payment_id, $account_id, $now_datetime, $cc_id, $purchase_id, $item_id, $amount, "chargeback")
				);
		$id = $db->insertid();
		if (!$id) {
			//this should not happen
			reportAdmin("AS: anet::chargebackByPurchaseId : cant insert into account_refund", "", ["type" => "CH", "now" => $now, "author_id" => $account->getId(), "payment_id" => $payment_id, "account_id" => $account_id, "now_datetime" => $now_datetime, "cc_id" => $cc_id, "item_id" => $item_id, "amount" => $amount, "reason" => "chargeback", "purchase_id" => $purchase_id]);
		}

		//insert payment event
		$db->q("INSERT INTO payment_event
				(payment_id, transaction_id, author_id, stamp, type)
				VALUES
				(?, ?, ?, ?, ?)
				",
				[$payment_id, $transaction_id, $account->getId(), $now, "CH"]
				);
		$id = $db->insertid();
		if (!$id) {
			//this should not happen
			reportAdmin("AS: anet::chargebackByPurchaseId : cant insert into payment_event", "", ["stamp" => $now, "author_id" => $account->getId(), "payment_id" => $payment_id, "type" => "CH", "purchase_trans_id" => $purchase_trans_id, "amount" => $amount]);
		}

		//update transaction table
		$db->q("UPDATE transaction SET amount = ? WHERE id = ? LIMIT 1", [$new_total, $transaction_id]);
		$aff = $db->affected($res);
		if ($aff != 1) {
			//this should not happen
			reportAdmin("AS: anet::chargebackByPurchaseId : cant update transaction", "", ["new_total" => $new_total, "id" => $transaction_id]);
		}

		$msg = "Transaction '{$purchase_trans_id}' successfully marked as chargeback (\${$amount}).";


		//---------------------------------------------------------
		//if we also want to wish to cancel subscription, cancel it
		//TODO - cancel disabled
		//if ($cancel_subscription && $subscription_id) {
		if (false && $cancel_subscription && $subscription_id) {
		
			file_log("anet", "anet::chargebackByPurchaseId(): cancelling subscription subscription_id={$subscription_id}");
			$ret = $this->cancelSubscription($subscription_id, $error);
			if (!$ret) {
				$error = "Transaction chargebacked successfully, but subscription cancellation failed: {$error} !";
				return false;
			}
			//insert payment event about cancelling subscription
			$db->q("INSERT INTO payment_event
					(payment_id, subscription_id, author_id, stamp, type, message)
					VALUES
					(?, ?, ?, ?, ?, ?)
					",
					[$payment_id, $subscription_id, $account->getId(), $now, "SC", "Subscription cancelled"]
					);
			$id = $db->insertid();
			if (!$id) {
				//this should not happen
				file_log("anet", "anet::chargebackByPurchaseId(): Error: Cant insert SC into payment_event! author_id={$account->getId()}, payment_id={$payment_id}, subscription_id={$subscription_id}");
				reportAdmin("AS: anet::chargebackByPurchaseId(): Error", "Cant insert SC into payment_event", ["stamp" => $now, "author_id" => $account->getId(), "payment_id" => $payment_id, "type" => "SC", "subscription_id" => $subscription_id]);
			}
			//update payment
			$res = $db->q("UPDATE payment SET subscription_status = 2 WHERE id = ? LIMIT 1", [$payment_id]);
			$aff = $db->affected($res);
			if ($aff != 1) {
				//this should not happen
				file_log("anet", "anet::chargebackByPurchaseId(): Error: Cant update subscription_status in payment table! payment_id={$payment_id}, subscription_id={$subscription_id}");
				reportAdmin("AS: anet::chargebackByPurchaseId error", "Cant update subscription_status in payment table", ["payment_id" => $payment_id, "subscription_id" => $subscription_id]);
			}
			$msg .= " Subscription '{$subscription_id}' cancelled.";

		}

		//everything allright
		return $msg;
	}

	public static function cancelSubscriptionById($subscription_id, $reason = "", &$error) {
		global $db, $account;

		file_log("anet", "anet::cancelSubscriptionById(): subscription_id={$subscription_id}, reason={$reason}");
		
		//TODO - disable
		file_log("anet", "anet::cancelSubscriptionById(): disabled");
        return true;
		
		$res = $db->q("SELECT p.* FROM payment p WHERE subscription_id = ?", [$subscription_id]);
		$numrows = $db->numrows($res);
		if ($numrows == 0) {
			file_log("anet", "anet::cancelSubscriptionById(): Error: Can't find subscription by subscription_id #{$subscription_id}");
			$error = "Can't find subscription by subscription_id #{$subscription_id}";
			return false;
		} else if ($numrows > 1) {
			file_log("anet", "anet::cancelSubscriptionById(): Error: Too many payments found for subscription_id #{$subscription_id}: {$numrows}");
			$error = "Too many payments found for subscription_id #{$subscription_id}: {$numrows}";
			return false;
		}
		$row = $db->r($res);
		$payment_id = $row["id"];
		$subscription_status = $row["subscription_status"];
		$email = $row["email"];
		$next_renewal_date = $row["next_renewal_date"];
		$amount = number_format($row["recurring_amount"], 2, ".", "");
		$trans_id = $row["trans_id"];
		$trans_date_time = date("m/d/Y", $row["responded_stamp"]);
		$now = time();

		if ($subscription_status != 1) {
			if ($subscription_status == 2) {
				file_log("anet", "anet::cancelSubscriptionById(): Error: Subscription #{$subscription_id} is already canceled");
				$error = "Subscription #{$subscription_id} is already canceled";
				return false;
			}
			file_log("anet", "anet::cancelSubscriptionById(): Error: Unknown status of subscription #{$subscription_id}: {$subscription_status}");
			$error = "Unknown status of subscription #{$subscription_id}: {$subscription_status}";
			return false;
		}

		$ret = self::cancelSubscription($subscription_id, $error);
		if (!$ret) {
			file_log("anet", "anet::cancelSubscriptionById(): Error: Canceling subscription #{$subscription_id} failed: '{$error}'");
			$error = "Subscription cancellation failed: {$error} !";
			return false;
		}

		if ($reason != "revert") {
			//insert payment event about cancelling subscription
			$db->q("INSERT INTO payment_event
					(payment_id, subscription_id, author_id, stamp, type, message)
					VALUES
					(?, ?, ?, ?, ?, ?)
					",
					[$payment_id, $subscription_id, $account->getId(), $now, "SC", "Subscription canceled, reason: '{$reason}'"]
					);
			$id = $db->insertid();
			if (!$id) {
				//this should not happen
				file_log("anet", "anet::cancelSubscriptionById(): Error: Cant insert SC into payment_event! author_id={$account->getId()}, payment_id={$payment_id}, subscription_id={$subscription_id}");
				reportAdmin("AS: anet::cancelSubscriptionById(): Error", "Cant insert SC into payment_event", ["stamp" => $now, "author_id" => $account->getId(), "payment_id" => $payment_id, "type" => "SC", "subscription_id" => $subscription_id]);
			}
		}

		//update payment
		$res = $db->q("UPDATE payment SET subscription_status = 2, next_renewal_date = NULL WHERE id = ? LIMIT 1", [$payment_id]);
		$aff = $db->affected($res);
		if ($aff != 1) {
			//this should not happen
			file_log("anet", "anet::cancelSubscriptionById(): Error: Cant update subscription_status in payment table! payment_id={$payment_id}, subscription_id={$subscription_id}");
			reportAdmin("AS: anet::cancelSubscriptionById error", "Cant update subscription_status in payment table", ["payment_id" => $payment_id, "subscription_id" => $subscription_id]);
		}

		$msg = "Subscription #{$subscription_id} cancelled.";

		if ($reason != "revert")
			self::cancelReceipt($email, $subscription_id, $amount, $trans_id, $trans_date_time);

		//everything allright
		return $msg;
	}

	private static function cancelReceipt($email, $subscription_id, $amount, $trans_id, $trans_date_time) {

		if (!$email || !$subscription_id)
			return false;

		$smarty = GetSmartyInstance();
		$smarty->assign("subscription_id", $trans_id);
		$smarty->assign("amount", $amount);
		$smarty->assign("trans_id", $trans_id);
		$smarty->assign("trans_date_time", $trans_date_time);

		$html = $smarty->fetch(_CMS_ABS_PATH."/templates/payment/cancel_viphost.html.tpl");
		$text = $smarty->fetch(_CMS_ABS_PATH."/templates/payment/cancel_viphost.txt.tpl");
		$subject = "Subscription #{$subscription_id} canceled - receipt";
		$embedded_images = array("logo_email" => _CMS_ABS_PATH."/images/logo_email_vip.png");

		return email_gmail("receipts@viphost.com", "Receipts", $email, $subject, $html, $text, $embedded_images);
	}

	public function refund($total, $cre) {
		return false;
	}

	/**
	 * Webhooks should be only checking the status of subscriptions
	 * E.g. when we get subscription_suspend webhook, the subscription in question should be already canceled
	 */
	public static function webhook() {
		global $db;

		$now = time();
		file_log("anet", "webhook: request=".print_r($_REQUEST, true));

		$type = $_REQUEST["type"];
		$subscription_id = $_REQUEST["subscription_id"];

		if ($type == "subscription_suspend" || $type == "subscription_cancel" || $type == "subscription_terminate") {
		
			if (!$subscription_id) {
				file_log("anet", "webhook: Error: Unspecified subscription_id ! type='{$type}'");
				reportAdmin("AS: anet:webhook: error", "Unspecified subscription_id ! type='{$type}'");
				return false;
			}

			//look up payment by subscription id
			$res = $db->q("SELECT p.* FROM payment p WHERE p.subscription_id = ?", [$subscription_id]);
			if ($db->numrows($res) == 0) {
				file_log("anet", "webhook: Can't find payment by subscription_id #{$subscription_id} !");
				reportAdmin("AS: anet:webhook: error", "Can't find payment by subscription_id #{$subscription_id} !");
				return false;
			}
			$row = $db->r($res);
			$subscription_status = $row["subscription_status"];

			//check the status
			if ($subscription_status == 2) {
				file_log("anet", "webhook: Subscription #{$subscription_id} is already canceled, all good");
				return true;
			}

			//subscription not canceled - cancel it
			// (this could happen if for some reason we dont get anet silent post about renewal failure - and this happens sometime !)
			file_log("anet", "webhook: Subscription #{$subscription_id} is not canceled, going to cancel");
			$error = null;
			$ret = self::cancelSubscriptionById($subscription_id, "", $error);
			if (!$ret) {
				reportAdmin("AS: anet:webhook: error", "Subscription #{$subscription_id} should be canceled in first place, and now when trying to cancel, it failed, error = '{$error} ! Fix this issue.");
				file_log("anet", "webhook: Error cancelling subscription #{$subscription_id} !");
				return false;
			}
			file_log("anet", "webhook: Subscription #{$subscription_id} canceled successfully");

			return true;

		} else {
			file_log("anet", "webhook: Unknown webhook type: '{$type}' !");
			reportAdmin("AS: anet:webhook: error", "Unknown webhook type: '{$type}' !");
			return false;
		}

		//we should never get here
		return false;
	}

	public static function silent_post() {
		global $db;

		$now = time();
		file_log("anet", "silent_post: request=".print_r($_REQUEST, true));


		//-----------------
		//TODO TEMP disable
		return true;
		//-----------------


		$subscription_id = $_REQUEST["x_subscription_id"];
		$response_code = $_REQUEST["x_response_code"];
		$type = $_REQUEST["x_type"];
		$trans_id = $_REQUEST["x_trans_id"];
		$amount = $_REQUEST["x_amount"];

		if (!$subscription_id) {
			//this is not subscription renewal
			file_log("anet", "anet::silent_post: No subscription_id -> not a subscription renewal, ignoring");
			return;
		}

		//look up payment by subscription id
		$res = $db->q("SELECT p.* FROM payment p WHERE p.subscription_id = ?", [$subscription_id]);
		if ($db->numrows($res) == 0) {
			file_log("anet", "anet::silent_post: Error: can't find payment by subscription_id #{$subscription_id} !");
			reportAdmin("AS: anet::silent_post Error", "can't find payment by subscription_id #{$subscription_id}");
			return;
		}
		$row = $db->r($res);
		$payment_id = $row["id"];
		$account_id = $row["account_id"];
		$subscription_status = $row["subscription_status"];
		$cc_id = $row["cc_id"];

		//create db post field
		$post = "response_code={$response_code}, x_response_subcode={$_REQUEST["x_response_subcode"]}, x_response_reason_code={$_REQUEST["x_response_reason_code"]}, x_response_reason_text={$_REQUEST["x_response_reason_text"]}, x_auth_code={$_REQUEST["x_auth_code"]}, x_avs_code={$_REQUEST["x_avs_code"]}, x_trans_id={$_REQUEST["x_trans_id"]}, x_invoice_num={$_REQUEST["x_invoice_num"]}, x_description={$_REQUEST["x_description"]}, x_amount={$_REQUEST["x_amount"]}, x_method={$_REQUEST["x_method"]}, type={$type}, x_cust_id={$_REQUEST["x_cust_id"]}, x_first_name={$_REQUEST["x_first_name"]}, x_last_name={$_REQUEST["x_last_name"]}, x_address={$_REQUEST["x_address"]}, x_city={$_REQUEST["x_city"]}, x_state={$_REQUEST["x_state"]}, x_zip={$_REQUEST["x_zip"]}, x_country={$_REQUEST["x_country"]}, x_phone={$_REQUEST["x_phone"]}, x_email={$_REQUEST["x_email"]}, x_MD5_Hash={$_REQUEST["x_MD5_Hash"]}, x_cavv_response={$_REQUEST["x_cavv_response"]}, x_subscription_paynum={$_REQUEST["x_subscription_paynum"]}";

		//lets check if this transaction was successful
		if ($response_code == "3") {
			//response code 3 = error, notify admin
			file_log("anet", "anet::silent_post: Error - this needs to be handled manually by admin");
			reportAdmin("AS: anet::silent_post(): API Error", "Response_code = 3 -> Error - find out what happened to this transaction and update AS DB accrodingly");
			return true;

		} else if ($response_code != "1" && $response_code != "4") {
			//1 = success, 4 = held for review, otherwise it is declined
			//renewal failed
			file_log("anet", "anet::silent_post: Renewal failed");

			//add payment event
			$res = $db->q("
				INSERT INTO payment_event 
				(payment_id, subscription_id, stamp, type, message, post)
				VALUES
				(?, ?, ?, ?, ?, ?)",
				[$payment_id, $subscription_id, $now, "RF", "Renewal failed", $post]
				);

			// we need to explicitely cancel subscription, because otherwise subscription goes to suspended state, but
			// in a month they try would to charge again - and it could succeed then and ad would go live again after being expired for a month 
			// this would be confusing (meanwhile user could have already purchased another subscription...)
			$error = null;
			$ret = self::cancelSubscription($subscription_id, $error);
			if (!$ret) {
				//cancel failed, notify admin so he can fix it, but continue
				file_log("anet", "anet::silent_post: Failed to explicitely cancel subscription! payment_id={$payment_id} subscription_id={$subscription_id}");
				reportAdmin(
					"AS: anet::silentPost(): Error", 
					"Renewal failed, but we failed to cancel the subscription ! Error = '{$error}' Fix this manually", 
					["payment_id" => $payment_id, "subscription_id" => $subscription_id, "error" => $error]
					);
			} else {
				file_log("anet", "anet::silent_post: Subscription #{$subscription_id} explicitely canceled.");
				//insert payment event about cancelling subscription
				$db->q("INSERT INTO payment_event
						(payment_id, subscription_id, author_id, stamp, type, message)
						VALUES
						(?, ?, ?, ?, ?, ?)
						",
						[$payment_id, $subscription_id, null, $now, "SC", "Subscription cancelled"]
						);
				//update payment
				$res = $db->q("UPDATE payment SET subscription_status = 2 WHERE id = ? LIMIT 1", [$payment_id]);
			}

			//check if payment was for classified ads and handle expiration
			$clad_ids = [];
			$res = $db->q("SELECT DISTINCT pi.classified_id FROM payment_item pi WHERE pi.payment_id = ? AND pi.type = 'classified'", [$payment_id]);
			if ($db->numrows($res) == 0) {
				//this might be old way of payment
				$res = $db->q("SELECT ap.item_id FROM account_purchase ap WHERE ap.`what` = 'classifieds' AND ap.payment_id = ? ORDER BY ap.id DESC LIMIT 1",
					[$payment_id]
					);
				if ($db->numrows($res) == 1) {
					$row = $db->r($res);
					$clad_ids[] = $row["item_id"];
				}
			} else {
				while ($row = $db->r($res)) {
					$clad_ids[] = $row["classified_id"];
				}
			}
			foreach ($clad_ids as $clad_id) {
				$clad = clad::findOneById($clad_id);
				if (!$clad) {
					file_log("anet", "anet::silent_post: Error: can't find ad #{$clad_id}");
					continue;
				}
				//make sure expiration date is close (the expiration should be set on next day after payment
				//if expiration is too far, send out email to admin
				$expire_stamp = $clad->getExpireStamp();
				if (!$expire_stamp)
					$expire_stamp = strtotime($clad->getExpires());
				$limit = time() + 5*86400;
				if ($expire_stamp > $limit) {
					file_log("anet", "anet::silent_post: Error: Renewal failed, but clad #{$clad_id} expiring way in future !");
					reportAdmin("AS: anet silent post error", "Error: Renewal of payment #{$payment_id} failed, but clad #{$clad_id} expiring way in future !");
				} else {
					file_log("anet", "anet::silent_post: Classified ad #{$clad_id} expiring in ".round(($expire_stamp - time()) / 3600)." hours - ok");
				}
			}

			//we dont do any expiration and notificatoin here, because client might have bought some non-recurring upgrades that prolong the ad
			//the ad will expire when the time comes and client will be notified

			return true;
		}

		$expire_stamp = $now + 31*86400;
		$expires = date("Y-m-d H:i:s", $expire_stamp);
		file_log("anet", "anet::silent_post: Renewal successful, expire_stamp={$expire_stamp}, expires={$expires}");

		//----------------------
		//renewal was successful

		//make sure subscription is active
		if ($subscription_status != 1) {
			reportAdmin("AS: anet:silent_post(): Renewal success but subscription canceled?", "Manually check this subscription, renewal was successful, but siubscription is already marked as canceled in DB ? Meanwhile updating subscription status to active");
			$db->q("UPDATE payment SET subscription_status = 1 WHERE id = ? LIMIT 1", [$payment_id]);
		}

		$new_purchase_what = $new_purchase_item_id = null;

		$items = self::get_payment_items($payment_id);
		foreach ($items as $item) {
			if (!array_key_exists("type", $item))
				continue;

			if ($item["type"] == "classified") {

				if (!array_key_exists("clad_id", $item) || !intval($item["clad_id"]))
					continue;
				$clad_id = intval($item["clad_id"]);
				
				if (array_key_exists("sponsor", $item) && $item["sponsor"] == 1)
					$db->q("UPDATE classifieds_sponsor SET done = 1, expire_stamp = ? WHERE post_id = ?", [$expire_stamp, $clad_id]);

				if (array_key_exists("side", $item) && $item["side"] == 1)
					$db->q("UPDATE classifieds_side SET done = 1, expire_stamp = ? WHERE post_id = ?", [$expire_stamp, $clad_id]);

				if (!array_key_exists("locations", $item))
					$db->q("UPDATE classifieds_loc SET done = 1, updated = NOW() WHERE post_id = ?", [$clad_id]);

				if (array_key_exists("locations", $item)) {
					foreach ($item["locations"] as $location) {

						if (!array_key_exists("loc_id", $location) || !intval($location["loc_id"]))
							continue;
						$loc_id = intval($location["loc_id"]);

						if (array_key_exists("sponsor", $location) && $location["sponsor"] == 1)
							$db->q("UPDATE classifieds_sponsor SET done = 1, expire_stamp = ? WHERE post_id = ? AND loc_id = ?", [$expire_stamp, $clad_id, $loc_id]);

						if (array_key_exists("side", $location) && $location["side"] == 1)
							$db->q("UPDATE classifieds_side SET done = 1, expire_stamp = ? WHERE post_id = ? AND loc_id = ?", [$expire_stamp, $clad_id, $loc_id]);

						$db->q("UPDATE classifieds_loc SET done = 1, updated = NOW() WHERE post_id = ? AND loc_id = ?", [$clad_id, $loc_id]);
					}
				} else {
					$db->q("UPDATE classifieds_loc SET done = 1, updated = NOW() WHERE post_id = ?", [$clad_id]);
				}

				if (array_key_exists("auto_renew", $item) && intval($item["auto_renew"])) {
					$res = $db->q(
						"UPDATE classifieds SET done = 1, expires = ?, expire_stamp = ?, auto_renew = auto_renew + ? WHERE id = ?",
						[$expires, $expire_stamp, intval($item["auto_renew"]), $clad_id]
						);
				} else {
					$res = $db->q("UPDATE classifieds SET done = 1, expires = ?, expire_stamp = ? WHERE id = ?", [$expires, $expire_stamp, $clad_id]);
				}

				//repost the ad
				$classifieds = new classifieds;
				$classifieds->repost($clad_id);

				audit::log("CLA", "Renewal", $clad_id, "Paid:{$amount}", "S-anet");
				$new_purchase_what = "classifieds";
				$new_purchase_item_id = $clad_id;

			} else if ($item["type"] == "repost") {
				
				if (!array_key_exists("credits", $item) || !intval($item["credits"]))
					continue;
				$credits = intval($item["credits"]);
				
				$res = $db->q("UPDATE account SET repost = repost + ? WHERE account_id = ? LIMIT 1", [$credits, $account_id]);

			} else {
				file_log("anet", "anet::silent_post: Error: Unknown type of payment item renewal: '{$item["type"]}' !");
			}
		}

		//insert transaction
		$res = $db->q(
			"INSERT INTO transaction 
			(type, payment_id, stamp, trans_id, subscription_id, amount)
			VALUES
			(?, ?, ?, ?, ?, ?)",
			["N", $payment_id, $now, $trans_id, $subscription_id, $amount]
			);
		$transaction_id = $db->insertid($res);
		if (!$transaction_id) {
			file_log("anet", "anet::silent_post: Error: cant insert transaction into db, payment_id={$payment_id}, trans_id={$trans_id}");
			reportAdmin("AS: Silent post Error", "cant insert transaction into db, payment_id={$payment_id}, trans_id={$trans_id}");
		}

		//insert purchase
		$res = $db->q("
				INSERT INTO account_purchase
				(account_id, cc_id, `time`, `transaction_time`, payment_id, transaction_id, `what`, `total`, `saved`, `item_id`, `promo_code`) 
				values 
				(?, ?, CURDATE(), ?, ?, ?, ?, ?, ?, ?, ?)",
				[$account_id, $cc_id, $now, $payment_id, $trans_id, $new_purchase_what, $amount, '', $new_purchase_item_id, null]
				);
		$id = $db->insertid($res);
		if (!$id) {
			file_log("anet", "anet::silent_post: Error: cant insert purchase into db, payment_id={$payment_id}, trans_id={$trans_id}, amount={$amount}");
			reportAdmin("AS: Silent post Error", "Error: cant insert purchase into db, payment_id={$payment_id}, trans_id={$trans_id}, amount={$amount}");
		}

		//add event
		$res = $db->q("
			INSERT INTO payment_event 
			(payment_id, transaction_id, subscription_id, stamp, type, message, post)
			VALUES
			(?, ?, ?, ?, ?, ?, ?)",
			[$payment_id, $transaction_id, $subscription_id, $now, "RS", "Renewal successful", $post]
			);
		$id = $db->insertid($res);
		if (!$id) {
			file_log("anet", "anet::silent_post: Error: cant insert renewal success payment event into db, payment_id={$payment_id}, subscription_id={$subscription_id}");
			reportAdmin("AS: Silent post Error", "Error: cant insert renewal success payment event into db, payment_id={$payment_id}, subscription_id={$subscription_id}");
		}

		//update next renewal date
		$last_renewal_date = date("Y-m-d", $now);
		$next_renewal_stamp = $now + 86400*30;
		$next_renewal_date = date("Y-m-d H:i:s", $next_renewal_stamp);
		$res = $db->q(
			"UPDATE payment SET last_renewal_date = ?, next_renewal_date = ? WHERE id = ? LIMIT 1", 
			[$last_renewal_date, $next_renewal_date, $payment_id]
			);
		$aff = $db->affected($res);
		if ($aff != 1) {
			file_log("anet", "anet::silent_post: Error: cant update payment next_renewal_date, payment_id={$payment_id}, next_renewal_date={$next_renewal_date}");
			reportAdmin("AS: Silent post Error", "Error: cant update payment next_renewal_date, payment_id={$payment_id}, next_renewal_date={$next_renewal_date}");
		}

		//send renewal success receipt email from VIP
		$ret = self::emailRenewalSuccess($transaction_id);

		file_log("anet", "anet::silent_post: done");
		return true;
	}

	/**
	 * this function encapsulates old and new way of defining payment items
	 */
	public static function get_payment_items($payment_id) {
		global $db, $account;

		$items = [];

		file_log("anet", "anet::get_payment_items: payment_id={$payment_id}");
		//find items that this payment is for
		$res = $db->q("SELECT pi.id FROM payment_item pi WHERE pi.payment_id = ?", [$payment_id]);
		if ($db->numrows($res) == 0) {
			//this is the old way with classified_id in purchase table
			$res = $db->q("SELECT ap.* FROM account_purchase ap WHERE ap.payment_id = ?", [$payment_id]);
			if ($db->numrows($res) == 0) {
				file_log("anet", "anet::get_payment_items: Error: can't find purchase for payment_id #{$payment_id} !");
				reportAdmin("AS: anet::get_payment_items Error", "Error: can't find purchase for payment_id #{$payment_id} !");
				return;
			}
			$row = $db->r($res);
			$purchase_id = $row["id"];
			$what = $row["what"];
			$item_id = $row["item_id"];
			$purchase_total = number_format($row["total"], 2, ".", "");
			file_log("anet", "anet::get_payment_items: Old way, purchase_id={$purchase_id}, what={$what}, item_id={$item_id}, purchase_total={$purchase_total}");
			if ($what == "classifieds") {
				$clad_id = $item_id;
				$auto_renew = $sponsor = $side = null;
				if ($purchase_total == "9.99" || $purchase_total == "0.99" || $purchase_total == "19.98" || $purchase_total == "29.97") {
					//normal ad(s)
				} else if ($purchase_total == "34.99") {
					//normal + side
					$side = 1;
				} else if ($purchase_total == "49.99") {
					//recurring
					$auto_renew = 30;
				} else if ($purchase_total == "84.99" || $purchase_total == "74.99") {
					//sponsor + side
					$sponsor = $side = 1;
				} else if ($purchase_total == "51.49") {
					//recurring + side
					$auto_renew = 30;
					$side = 1;
				} else if ($purchase_total == "26.49") {
					//5 reposts + side
					$auto_renew = 5;
					$side = 1;
				} else if ($purchase_total == "59.99") {
					//normal + sponsor
					$sponsor = 1;
				} else if ($purchase_total == "59.99" || $purchase_total = "80.00") {
					//recurring + sponsor
					$auto_renew = 30;
					$sponsor = 1;
				} else if ($purchase_total == "105.00") {
					//recurring + sponsor
					$auto_renew = 30;
					$sponsor = $side = 1;
				} else if ($purchase_total == "42.99") {
					//10 reposts
					$auto_renew = 10;
				} else if ($purchase_total == "105.00") {
					//5 reposts + sponsor
					$auto_renew = 5;
					$sponsor = 1;
				} else if ($purchase_total == "75.99") {
					//20 reposts + side
					$auto_renew = 20;
					$side = 1;
				}
				$items[] = [
					"type" => "classified",
					"clad_id" => $clad_id,
					"sponsor" => $sponsor,
					"side" => $side,
					"auto_renew" => $auto_renew,
					];

			} else {
				$items[] = [
					"type" => $what,
					"item_id" => $item_id,
					];
			}
		} else {
			//this is the new way with payment items
			//---------------------------------------------------------------
			$res = $db->q("SELECT DISTINCT pi.classified_id FROM payment_item pi WHERE pi.payment_id = ? and pi.type = 'classified'", [$payment_id]);
			while ($row = $db->r($res)) {
				$clad_id = $row["classified_id"];

				file_log("anet", "anet::get_payment_items: New way, clad_id={$clad_id}");
				$res2 = $db->q("SELECT pi.* FROM payment_item pi WHERE pi.payment_id = ? AND pi.classified_id = ? ", [$payment_id, $clad_id]);
				$ar = null;
				$location_count = 0;
				$locations = [];
				while ($row2 = $db->r($res2)) {
					$payment_item_id = $row2["id"];
					$auto_renew = $row2["auto_renew"];
					$loc_id = $row2["loc_id"];
					$sponsor = $row2["sponsor"];
					$side = $row2["side"];

					if (!$loc_id) {
						debug_log("classifieds::get_payment_items: error: loc_id not specified in payment_item #{$payment_item_id}!");
						reportAdmin("as: classifieds::get_payment_items error", "loc_id not specified in payment_item #{$payment_item_id}!", []);
						return false;
					}

					if ($auto_renew) {
						if ($ar == null) {
							$ar = $auto_renew;
						} else if ($ar != $auto_renew) {
							debug_log("classifieds::get_payment_items: error: auto_renew value differs between location ar={$ar} auto_renew={$auto_renew} !");
							reportAdmin("AS: classifieds::get_payment_items error", "auto_renew val differs between location ar={$ar} auto_renew={$auto_renew}");
							return false;
						}
					}

					$locations[] = [
						"loc_id" => $loc_id,
						"sponsor" => $sponsor,
						"side" => $side,
						];
				}

				$items[] = [
					"type" => "classified",
					"clad_id" => $clad_id,
					"locations" => $locations,
					"auto_renew" => $auto_renew,
					];
			}

			//other payment items
			$res = $db->q("SELECT pi.* FROM payment_item pi WHERE pi.payment_id = ? and pi.type <> 'classified'", [$payment_id]);
			while ($row = $db->r($res)) {
				$payment_item_id = $row["id"];
				$type = $row["type"];

				if ($type == "repost") {
					//add global repost credits
					$credits = intval($row["auto_renew"]);
					if (!$credits) {
						file_log("anet", "anet::get_payment_items: Error: zero repost credits for repost type payment item ? payment_item_id={$payment_item_id}");
						reportAdmin("AS: anet:get_payment_items: error", "Error: zero repost credits for repost type payment item ?", ["payment_item_id" => $payment_item_id]);
						continue;
					}
					$items[] = [
						"type" => "repost",
						"credits" => $credits,
						];
				} else {
					file_log("anet", "anet::get_payment_items: Error: Unknown type of payment item renewal: '{$type}' !, payment_item_id={$payment_item_id}");
					reportAdmin("AS: anet:get_payment_items: error", "Unknown type of payment item renewal: '{$type}' !", ["payment_id" => $payment_id, "payment_item_id" => $payment_item_id]);
				}
			}
		}

		file_log("anet", "anet::get_payment_items: items='".print_r($items, true)."'");
		return $items;
	}

	/**
	 * this function encapsulates old and new way of defining payment items
	 */
	public static function get_payment_classified_id($payment_id) {
		global $db, $account;

		$items = [];

		file_log("anet", "anet::get_payment_classified_id: payment_id={$payment_id}");
		//find items that this payment is for
		$res = $db->q("SELECT pi.* FROM payment_item pi WHERE pi.payment_id = ?", [$payment_id]);
		if ($db->numrows($res) > 0) {
			while ($row = $db->r($res)) {
				if ($row["type"] == "classified")
					return $row["classified_id"];
			}
			return null;
		}

		//this is the old way with classified_id in purchase table
		$res = $db->q("SELECT p.p1, p.p2, p.p3 FROM payment p WHERE p.id = ?", [$payment_id]);
		if ($db->numrows($res) == 0) {
			file_log("anet", "anet::get_payment_classified_id: Error: can't find payment for payment_id #{$payment_id} !");
			reportAdmin("AS: anet::get_payment_classified_id Error", "Error: can't find payment for payment_id #{$payment_id} !");
			return;
		}
		$row = $db->r($res);
		$p1 = $row["p1"];
		$p2 = $row["p2"];
		$p3 = $row["p3"];
		if ($p1 == "classifieds" && is_numeric($p3))
			return $p3;
		return null;
	}

	public static function emailRenewalSuccess($transaction_id, $email = null) {
		global $account, $db;
		$smarty = GetSmartyInstance();

		$res = $db->q("
			SELECT t.amount, t.payment_id, t.trans_id, t.subscription_id 
				, p.email
				, cc.type, cc.cc
			FROM transaction t 
			INNER JOIN payment p on p.id = t.payment_id
			LEFT JOIN account_cc cc on cc.cc_id = p.cc_id
			WHERE t.id = ?
			", 
			[$transaction_id]
		);
		if ($db->numrows($res) != 1) {
			reportAdmin("AS: anet::emailRenewalSuccess() error", "cant find transaction for transaction_id#{$transaction_id} !");
			return false;
		}
		$row = $db->r($res);
		$trans_id = $row["trans_id"];
		$amount = $row["amount"];
		$payment_id = $row["payment_id"];
		$card_description = "";
		if ($row["cc"]) {
			$card_description = creditcard::convertType($row["type"])." ".substr($row["cc"], -5);
		}
		
		$smarty->assign("trans_id", $trans_id);
		$smarty->assign("amount", number_format($amount, 2, ".", ""));
		$smarty->assign("card_description", $card_description);

		if (!$email) {
			$email = $row["email"];
		}


		$email_items = [];
		$items = self::get_payment_items($payment_id);
		foreach ($items as $item) {
			if (!array_key_exists("type", $item))
				continue;

			if ($item["type"] == "classified") {

				if (!array_key_exists("clad_id", $item) || !intval($item["clad_id"]))
					continue;
				$clad_id = intval($item["clad_id"]);

				if (!array_key_exists("locations", $item)) {
					$res = $db->q("
						SELECT cl.loc_id, l.loc_name, l.s 
						FROM classifieds_loc cl
						INNER JOIN location_location l on l.loc_id = cl.loc_id
						WHERE cl.post_id = ? AND cl.done >= 0
						",
						[$clad_id]
						);
					$location_count = $db->numrows($res);
					$locations_text = "";
					while ($row = $db->r($res)) {
						$locations_text .= (empty($locations_text)) ? "" : "; ";
						$locations_text .= $row["loc_name"].", ".$row["s"];
					}
					if ($location_count > 1)
						$locations_text = "{$location_count} locations: ";
					$email_items[] = "Classified ad: {$locations_text}";
				} else {

					$location_count = 0;
					$locations_text = "";
					foreach ($item["locations"] as $location) {

						if (!array_key_exists("loc_id", $location) || !intval($location["loc_id"]))
							continue;
						$loc_id = intval($location["loc_id"]);

						$res = $db->q("
							SELECT l.loc_name, l.s 
							FROM location_location l
							WHERE l.loc_id = ?
							",
							[$loc_id]
							);
						if ($db->numrows($res) != 1)
							continue;
						$row = $db->r($res);
						$locations_text .= (empty($locations_text)) ? "" : "; ";
						$locations_text .= $row["loc_name"].", ".$row["s"];
						$location_count++;
						
						if (array_key_exists("sponsor", $location) && $location["sponsor"] == 1)
							$email_items[] = "City Thumbnail ".$row["loc_name"].", ".$row["s"];

						if (array_key_exists("side", $location) && $location["side"] == 1)
							$email_items[] = "Side Sponsor ".$row["loc_name"].", ".$row["s"];

					}
					if ($location_count > 1)
						$locations_text = "{$location_count} locations: ";
					$email_items[] = "Classified ad: {$locations_text}";
				}

				if (array_key_exists("sponsor", $item) && $item["sponsor"] == 1)
					$email_items[] = "City Thumbnail";

				if (array_key_exists("side", $item) && $item["side"] == 1)
					$email_items[] = "Side Sponsor";

				if (array_key_exists("auto_renew", $item) && intval($item["auto_renew"]))
					$email_items[] = intval($item["auto_renew"])." Top Of The List credits";

			} else if ($item["type"] == "repost") {

				if (!array_key_exists("credits", $item) || !intval($item["credits"]))
					continue;
				$credits = intval($item["credits"]);

				$email_items[] = "{$credits} Top Of The List credits";
			}
		}
		$smarty->assign("email_items", $email_items);

		$html = $smarty->fetch(_CMS_ABS_PATH."/templates/payment/renewal_success.html.tpl");
		$text = $smarty->fetch(_CMS_ABS_PATH."/templates/payment/renewal_success.txt.tpl");
		$subject = "Membership Renewal Success";
		$embedded_images = array("logo_email" => _CMS_ABS_PATH."/images/logo_email_vip.png");

		return email_gmail("receipts@viphost.com", "Receipts", $email, $subject, $html, $text, $embedded_images);
	}

	//temp function that is used in adbuild and purchase pages (classifieds/sponsor, ...) to disallow creating more than 1 pending payment
	public static function has_pending_payment($account_id) {
		global $db;
		$payment_id = $db->single("SELECT p.id FROM payment p WHERE p.result = 'P' AND p.account_id = ? LIMIT 1", [$account_id]);
		return ($payment_id) ? true : false;
	}

}

//END 
