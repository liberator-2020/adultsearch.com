<?php


/**
 * Class flash is used for logging all events in AS system into database
 * @author jay
 */
class audit {

	var $id = NULL;
	var $created = NULL;
	var $who = NULL;
	var $type = NULL;
	var $subtype = NULL;
	var $p1 = NULL;
	var $message = NULL;

	function __construct() {
		$this->created = date("Y-m-d H:i:s");
	}

	public function setWho($who) {
		$this->who = $who;
	}
	public function setType($type) {
		$this->type = $type;
	}
	public function setSubtype($subtype) {
		$this->subtype = $subtype;
	}
	public function setP1($p1) {
		$this->p1 = $p1;
	}
	public function setMessage($message) {
		$this->message = $message;
	}

	public function getId() {
		return $this->id;
	}
	public function getWho() {
		return $this->who;
	}
	public function getType() {
		return $this->type;
	}
	public function getSubtype() {
		return $this->subtype;
	}
	public function getP1() {
		return $this->p1;
	}
	public function getMessage() {
		return $this->message;
	}

	public function getInitiator() {
		if (substr($this->who, 0, 2) == "S-")
			return "System";
		$acc = account::findOneById($this->who);
		if (!$acc)
			return false;
		return "User, email={$acc->getEmail()}";
	}

	private function getParameterNames() {
		if ($this->type == "ACC" && $this->subtype == "Ban")
			return array("Account id");
		return array("Parameter1");
	}

	public static function create($type, $subtype, $p1 = NULL, $message = NULL, $who = NULL) {
		global $account;

		if (strlen($type) > 3 || strlen($type) < 1)
			return false;

		if (!$subtype)
			return false;
		$subtype = substr($subtype, 0, 30);

		if ($p1 != NULL)
			$p1 = intval($p1);

		$message = substr($message, 0, 255);

		if ($who == NULL) {
			$account_id = $account->isloggedin();
			if (!$account_id)
				return false;
			$who = $account_id;
		} else
			$who = substr($who, 0, 30);
		
		$audit = new audit();
		$audit->setWho($who);
		$audit->setType($type);
		$audit->setSubtype($subtype);
		$audit->setP1($p1);
		$audit->setMessage($message);

		return $audit;
	}

	private function persist() {
		global $db;

		if ($this->id == NULL) {
			$db->q("INSERT INTO audit
					(created, who, type, subtype, p1, message)
					VALUES
					(?, ?, ?, ?, ?, ?)",
					array($this->created, $this->who, $this->type, $this->subtype, $this->p1, $this->message));
			$id = $db->insertid();
			if (!$id)
				return false;
			return true;
		}

		return false;
	}

	/**
	 * adds audit log message
	 */
	public static function log($type, $subtype, $p1 = NULL, $message = NULL, $who = NULL) {

		$audit = self::create($type, $subtype, $p1, $message, $who);
		if (!$audit)
			return false;

		if (!$audit->persist())
			return false;

		return true;
	}

}

