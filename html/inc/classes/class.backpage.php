<?php

define('BPPROMO_IMG_DIR', _CMS_ABS_PATH."/../data/bppromo_images");
define('BP_LOGIN_EMAIL', "backpage@adultsearch.com");
define('BP_LOGIN_PASSWORD', "lasvegas123");

define('BP_MAILBOX_SERVER', "node5.atmailcloud.com");
define('BP_MAILBOX_EMAIL', "backpage@adultsearch.com");
define('BP_MAILBOX_PASSWORD', "lasvegas123");

class backpage {

	public static $ad_types = array(
		"1" => "Escort",
		"2" => "Bodyrubs",
		"3" => "TS/TV",
		"4" => "Stripclub",
		);

	public static function get_ad_types() {
		return self::$ad_types;
	}

	public static function get_ad_type_name($type) {
		if (!array_key_exists($type, self::$ad_types))
			return false;
		else
			return self::$ad_types[$type];
	}

	/**
	 * returns image data in HTTP response
	 */
	public static function bppromo_image($params) {
		//echo print_r($params, true);
		$dir = BPPROMO_IMG_DIR."/".$params["type"];
		if (!is_dir($dir))
			die("ERROR: '{$dir}' not directory !");
		$filepath = $dir."/".$params["filename"];
		if (!is_file($filepath))
			die("ERROR: '{$filepath}' not file !");
		$fp = fopen($filepath, 'rb');
		header("Content-Type: image/jpeg");
		header("Content-Length: ".filesize($filepath));
		fpassthru($fp);
		exit;
	}

	private static function cmp_filename($a, $b) {
		$aa = $a["filename"];
		$bb = $b["filename"];
		return strcmp($aa, $bb);
	}

	/**
	 * Returns JSON response of list of images for specified type
	 */ 
	public static function bppromo_list_images($params) {
		global $db;

		$type = $params["type"];
		$dir = BPPROMO_IMG_DIR."/".$type;
		if (!is_dir($dir)) {
			echo json_encode(array("status" => "error"));
			die;
		}
		if (!($handle = opendir($dir))) {
			echo json_encode(array("status" => "error"));
			die;
		}

		$used_list = array();
		$res = $db->q("SELECT type, filename, ad_id FROM bp_promo_ad_image");
		while ($row = $db->r($res)) {
			$key = $row["type"]."|".$row["filename"];
			$val = $row["ad_id"];
			$used_list[$key] = $val;
		}

		$images = array();
 		while (false !== ($entry = readdir($handle))) {
			if ($entry == "." || $entry == "..")
				continue;
			if (array_key_exists("{$type}|{$entry}", $used_list))
				$used = "- used in ad #".$used_list["{$type}|{$entry}"];
			else
				$used = ""; 
			$images[] = array("type" => $type, "filename" => $entry, "url" => "/backpage/bppromo_image?type={$type}&filename={$entry}", "used" => $used);
		}
		closedir($handle);
		usort($images, array('backpage','cmp_filename'));
		$response = array("status" => "success", "images" => $images);
		echo json_encode($response);
		die;
	}

	private static function bp_login($account_id) {
		global $db;

		$res = $db->q("SELECT * FROM bp_promo_account WHERE id = ?", array($account_id));
		if ($db->numrows($res) != 1) {
			reportAdmin("AS: backpage::bp_login(): Error can't find bp account by id", "", array("account_id" => $account_id));
			return false;
		}
		$row = $db->r($res);
		$login_email = $row["email"];
		$login_password = $row["password"];

		chdir(_CMS_ABS_PATH."../cron/promo/");
		
		echo "Logging into backpage.com, account #{$account_id}: {$login_email} ...\n";

		$curl = new curl("_bp.cookie");
		$result = $curl->get("http://posting.www.backpage.com/classifieds/central");
		preg_match('/<input type="hidden" name="session" value="(.*)?">/', $result, $ses);
		$session = $ses[1];
		
		//$data = "loginAttempt=true&type=user&email=".urlencode(BP_LOGIN_EMAIL)."&password=".urlencode(BP_LOGIN_PASSWORD)."&session=$session";
		$data = "loginAttempt=true&type=user&email=".urlencode($login_email)."&password=".urlencode($login_password)."&session=$session";
		$result = $curl->post("http://posting.www.backpage.com/classifieds/central/ManageAds", $data, 1);
		
		return $curl;
	}

	/*
	 * This function is called from cron file <WEBROOT>/../cron/bp_promo.php
	 */
	public static function rotate_all_active_ad_slots() {
		global $db;

		//run only during business hours
		$hour = date("H");
		if( $hour < 5 || $hour > 23 ) {
			echo "Hour={$hour} - not the right time\n";
			return;
		}

//		$curl = self::bp_login();

		$res = $db->q("SELECT * FROM bp_promo_ad_slot WHERE active = 1");
		while($row = $db->r($res)) {
			$asl = bpadslot::withRow($row);
			echo "Rotating ad slot #{$asl->getId()}...\n";
			$ret = self::rotate_ad_slot($asl, $curl);
		}

	}

	public static function rotate_ad_slot($asl, $curl = NULL) {
		global $db;

		$curl = self::bp_login($asl->getAccountId());

		$ad = $asl->get_next_ad();
		if (!$ad) {
			echo "Error: Not able to get next ad for ad slot #{$asl->getId()} !\n";
			file_log("bp_promo", "Rotate ERROR: reason=Not able to get next ad!, ad_slot_id={$asl->getId()}");
			return false;
		}

		chdir(BPPROMO_IMG_DIR);

		$post = array();
		$url = "http://posting.www.backpage.com/online/classifieds/EditAd?".$asl->getBpCode();
	
		$result = $curl->get($url);
		preg_match_all('/<input type="hidden" name="(.*)" value="(.*)">/', $result, $hidden);
		for($y=0; $y < count($hidden[1]); $y++) {
			$post[$hidden[1][$y]] = $hidden[2][$y];
		}

		$images = $ad->getImages();
		$l = 1;
		foreach($images as $image) {
			echo "Making fake AJAX POST image upload request #{$l} ... ";
			$postimg = array();
			$postimg["action"] = "processImage";
			$postimg["origName"] = $image["filename"];
			$postimg["lang"] = "en-us";
			$value = curl::getCurlValue($image["type"]."/".$image["filename"], "image/jpeg");
			$postimg["image"] = $value;

			//jay 10.01.2017 - need to modify www to real city (lasvegas) ? - otherwise we got "The requested ad could not be found." error
			//$rex = $curl->post("http://posting.www.backpage.com/online/Classifieds/ImageMacros.html", $postimg, 1);
			$rex = $curl->post("http://posting.lasvegas.backpage.com/online/Classifieds/ImageMacros.html", $postimg, 1);
			//echo "Result:\n{$rex}\n";
			$ret = preg_match('/data-oid=\\\\"([0-9]*)\\\\"/', $rex, $matches);
			if (!$ret) {
				echo "Error: did not match oid!\n";
				return false;
			}
			$image_oid = $matches[1];
			echo "   Image #{$l} added succesfully, imageOid{$l} = '{$image_oid}'\n";
			$post["imageOid{$l}"] = $image_oid;
			$l++;
		}

		//echo "Going to update position: {$asl->getId()}, with ad_id {$ad->getId()}, bp url = {$url}\n";
		$link_text = $ad->getLinkText();
		if (empty($link_text))
			$link_text = "more pics and contact info";	//some sane default if no link text is defined

		$ad_content = $ad->getContent()."<br /><a href=\"http://adultsearch.com/classifieds/landing?cat=".$asl->getTypeUrl()."&id={$asl->getId()}\" rel=\"nofollow\">{$link_text}</a><img src=\"http://adultsearch.com/classifieds/bpadview?id={$asl->getId()}\" />";

		$post["lang"] = "en-us";
        $post["oid"] = $asl->getBpOid();
        $post["id"] = $asl->getBpId();
        $post["nextPage"] = "save";
		$post["title"] = htmlentities($ad->getTitle());
		//$post["ad"] = "<iframe src='http://adultsearch.com/classifieds/bp?id={$asl->getId()}' width='500' height='500' frameborder='0'></iframe>";
		$post["ad"] = $ad_content;
		$post["socialMediaUrl"] = "";
		$post["regionOther"] = "";
		$post["age"] = "19";
		$post["allowReplies"] = "No";
		$post["showAdLinks"] = "No";
		$post["myAction"] = "editAd";

		//jay added 2 new fields on 12.10.2016 ?
        $post["name"] = "Janet";
        $post["companyPhone"] = "+17029351688";   //our twilio phone num.

		echo "POST array: ".print_r($post, true)."\n";
		//jay 10.01.2017 - need to modify www to real city (lasvegas) ? - otherwise we got "The requested ad could not be found." error
		//$rex = $curl->post("http://posting.www.backpage.com/online/classifieds/EditAd", $post, 1);
		$rex = $curl->post("http://posting.lasvegas.backpage.com/online/classifieds/EditAd", $post, 1);
		//echo "RESPONSE:\n{$rex}\n";

		if (!preg_match('/Your posting is processing/', $rex) 				//old success response
			&& !preg_match('/Your posting is currently pending/', $rex)		//new success message (3.10.2014)
			&& !preg_match('/Your changes have been saved./', $rex)     //new success message (10.01.2017)
			) {
			//probably error
			if (preg_match('/<li class="error">([^<]*)<\/li>/', $rex, $matches)) {
				$error = trim($matches[1]);
				reportAdmin("Promo BP ad updater - cant update position {$asl->getId()} with ad_id {$ad->getId()}", "", array("error" => $error));
				echo "Error: {$error} !\nContinue\n";
				file_log("bp_promo", "Rotate ERROR: reason={$error}!, ad_slot_id={$asl->getId()}, ad_id={$ad->getId()}");
				return;
			}
			//ad is expired
			if (preg_match('/This ad is expired./', $rex)) {
				//jay comment 26.1.2015 - dallas didnt renew 2 ads and these notification are bothering me
				/*
				reportAdmin("Promo BP ad updater - Ad in ad slot #{$asl->getId()} is expired", 
							"Was trying to update ad slot {$asl->getId()} with ad_id {$ad->getId()}, but update failed.<br />BP says ad is expired<br />Log in to BP and check: <a href=\"https://my.backpage.com/classifieds/central/index\">https://my.backpage.com/classifieds/central/index</a>.<br />", 
							array("error" => "This ad is expired.", "ad_slot_id" => $asl->getId(), "ad_slot_bp_code" => $asl->getBpCode())
							);
				*/
				echo "Error: Ad in this ad_slot is expired on BP !\nContinue\n";
				file_log("bp_promo", "Rotate ERROR: reason=Ad is expired!, ad_slot_id={$asl->getId()}, ad_id={$ad->getId()}");
				return;
			}
			//ad not found ?
			if (preg_match('/The requested ad could not be found./', $rex)) {
				reportAdmin("Promo BP ad updater - cant update position {$asl->getId()} with ad_id {$ad->getId()}", "", 
							array("error" => "The requested ad could not be found", "ad_slot_bp_code" => $asl->getBpCode()));
				echo "Error: The requested ad could not be found (maybe ad is expired on BP ?) !\nContinue\n";
				file_log("bp_promo", "Rotate ERROR: reason=requested ad could not be found (expired?), ad_slot_id={$asl->getId()}, ad_id={$ad->getId()}");
				return;
			}
			if (preg_match('/A problem occurred in transferring your image to Backpage.com/', $rex)) {
				reportAdmin("Promo BP ad updater - didnt update position {$asl->getId()} with ad_id {$ad->getId()}", 
							"Reason: A problem occurred in transferring your image to Backpage.com  Please try again in a few minutes.", 
							array("error" => "A problem occurred in transferring your image to Backpage.com  Please try again in a few minutes."));
				echo "Error: A problem occurred in transferring your image to Backpage.com  Please try again in a few minutes. !\nContinue\n";
				file_log("bp_promo", "Rotate ERROR: reason=A problem occurred in transferring your image to Backpage.com Please try again in a few minutes, ad_slot_id={$asl->getId()}, ad_id={$ad->getId()}");
				return;
			}
			echo "Processing msg not found - success or failure ? Sending email to admin and continuing. Response is catched in ./cms/log/bp_promo.log\n";
			reportAdmin(
				"AS: backpage::rotate_ad_slot(): error updating #{$asl->getId()} with ad#{$ad->getId()}", 
				"Did not detect error nor success after POSTing ad change, probably backpage.com webapp posting success message changed, check attached HTML response and fix backpage::rotate_ad_slot()",
				array("ad_slot_id" => $asl->getId(), "ad_id" => $ad->getId(), "post_array" => print_r($post, true), "HTML_response" => htmlspecialchars($rex))
				);
			file_log("bp_promo", "Rotate ERROR: reason=Processing msg not found!, ad_slot_id={$asl->getId()}, ad_id={$ad->getId()}, response={$rex}");
			return;
		}

		echo "Posting is probably processed successfully, resetting stats...\n";
		file_log("bp_promo", "Rotate success: ad_slot_id={$asl->getId()}, ad_id={$ad->getId()}");
		//reset stats
		$ret = $asl->reset_stats($ad);
		//TODO check return value

		echo "Ad at position {$asl->getId()} successfully updated with ad_id {$ad->getId()}.\n";
		return true;
	}

	public static function getLocation($url_name) {
		global $mcache;

		//first check redir table
		$key = "SQL:BPL-URL";
		$params = array($url_name);
		$res = $mcache->get($key, $params);
		if ($res === false) {
			//not found in BP redir table, try to find location by dir
			$key = "SQL:LOC-DIR-HAS";
			$res2 = $mcache->get($key, $params);
			if ($res2 !== false) {
				$loc_id = intval($res2["loc_id"]);
			} else {
				//not found unique result by dir
				if ($mcache->multiple_results) {
					debug_log("GEOLOC failed - Multiple results for url_name '{$url_name}' !");
				} else {
					debug_log("GEOLOC failed - No result for url_name '{$url_name}' !");
				}
				return false;
			}
		} else {
			$loc_id = intval($res["loc_id"]);
		}
		
		//search for location row by loc_id
		$key = "SQL:LOC-ID";
		$params = array($loc_id);
		$res = $mcache->get($key, $params);
		if ($res === false) {
			debug_log("GEOLOC failed - Not found loc_id '{$loc_id}' in location_location table !");
			return false;
		}

		return $res;
	}

	public static function display_ad() {
		global $db;

		if (isset($_REQUEST["ref"])) {
			$url_name = preg_replace("/[^a-zA-Z0-9]/i", "", $_REQUEST["ref"]);
		} else {
			$url = $_SERVER["HTTP_REFERER"];
			$ret = explode(".", $url);

			if (!strcasecmp($ret[0], "espanol") && stristr($_SERVER["HTTP_REFERER"], 'backpage'))
				$ret = strtolower($ret[1]);
			else
				$ret = strtolower($ret[0]);

			$ret = str_replace(" ", "%", $ret);
			$p = strrpos($ret, '/');
			if ($p)
				$ret = substr($ret, $p+1);

			$url_name = preg_replace("/[^a-zA-Z0-9]/i", "", $ret);
		}


		$loc = false;
		if (!empty($url_name) && !in_array($url_name, array("www", "backpage.com", "posting"))) {
			$loc_row = self::getLocation($url_name);
			if ($loc_row) {
				$loc = location::withRow($loc_row);
			}
		}

		if ($loc) {
			$db->q("INSERT IGNORE INTO classifieds_bp_link (subdomain, url, loc_id, c) values (?, ?, ?, 1) on duplicate key update c = c + 1", 
				array($url_name, $url, $loc->getId()));
		}

		self::display_ad_for_loc($loc);
		die;
	}

	private static function display_ad_for_loc($loc) {
		global $db;

		$id = intval($_REQUEST['id']);
		if (!$id)
			die;

		$res = $db->q("SELECT ad.id, ad.type, ad.content, ad.link_text
						FROM bp_promo_ad_slot asl 
						INNER JOIN bp_promo_ad ad on ad.id = asl.curr_ad_id
						WHERE asl.id = ?", array($id));
		if (!$db->numrows($res))
			die;
		$row = $db->r($res);
		$ad_id = $row["id"];

		//TODO move to class function
		switch($row["type"]) {
			case 1: $module = "female-escorts"; break;
			case 2: $module = "body-rubs"; break;
			case 3: $module = "tstv-shemale-escorts"; break;
			case 4: $module = "strip-clubs"; break;
			default: $module = "female-escorts"; break;
		}

		//dena skype 8.8.2013 17:15 - show homepage id cant find city so las vegas escorts dont get calls from all over US
		if (!$loc)
			$link = "http://adultsearch.com/homepage";
		else
			$link = $loc->getUrl()."/{$module}";

		$link .= "?bpp_s={$id}&bpp_a={$ad_id}";

		$content = nl2br($row["content"])."<br /><a target='_top' href='{$link}'>{$row["link_text"]}</a>";
		echo $content;

		$db->q("UPDATE bp_promo_ad_slot SET impressions_since = impressions_since + 1, impressions_total = impressions_total + 1 WHERE id = ?", array($id));
		$db->q("UPDATE bp_promo_ad SET impressions_since = impressions_since + 1 WHERE id = ?", array($ad_id));
		$db->q("INSERT INTO bp_promo_stats (ad_slot_id, ad_id, day, impressions, hits) VALUES (?, ?, NOW(), 1, 0) ON DUPLICATE KEY UPDATE impressions = impressions + 1", 
				array($id, $ad_id));
		die;
	}

	/**
	 * Marks "impression" in the bp promo stats
	 */
	public static function check_view($asl_id) {
		global $db;

		$res = $db->q("SELECT curr_ad_id as ad_id FROM bp_promo_ad_slot WHERE id = ?", array($asl_id));
		if ($db->numrows($res) != 1)
			return false;
		$row = $db->r($res);
		$ad_id = $row["ad_id"];

		$db->q("UPDATE bp_promo_ad_slot SET impressions_since = impressions_since + 1, impressions_total = impressions_total + 1 WHERE id = ?", array($asl_id));
		$db->q("UPDATE bp_promo_ad SET impressions_since = impressions_since + 1 WHERE id = ?", array($ad_id));
		$db->q("INSERT INTO bp_promo_stats (ad_slot_id, ad_id, day, impressions, hits) VALUES (?, ?, NOW(), 1, 0) ON DUPLICATE KEY UPDATE impressions = impressions + 1", 
				array($asl_id, $ad_id));

		return true;
	}

	/**
	 * Marks "hit" in the bp promo stats
	 */
	public static function check_hit($asl_id) {
		global $db;

		$res = $db->q("SELECT curr_ad_id as ad_id FROM bp_promo_ad_slot WHERE id = ?", array($asl_id));
		if ($db->numrows($res) != 1)
			return false;
		$row = $db->r($res);
		$ad_id = $row["ad_id"];

		$db->q("UPDATE bp_promo_ad_slot SET hits_since = hits_since + 1, hits_total = hits_total + 1 WHERE id = ?", array($asl_id));
		$db->q("UPDATE bp_promo_ad SET hits_since = hits_since + 1 WHERE id = ?", array($ad_id));
		$db->q("INSERT INTO bp_promo_stats (ad_slot_id, ad_id, day, impressions, hits) VALUES (?, ?, NOW(), 0, 1) ON DUPLICATE KEY UPDATE hits = hits + 1", 
				array($asl_id, $ad_id));

		return true;
	}

	/**
	 * Processes mailbox backpage@adultsearch.com
	 */ 
	public static function processMailbox() {
		//$conn = imap_open('{mail.adultsearch.com:143/novalidate-cert}INBOX', 'backpage@adultsearch.com', 'B4ckP4ge');
		$conn = imap_open('{'.BP_MAILBOX_SERVER.':143/novalidate-cert}INBOX', BP_MAILBOX_EMAIL, BP_MAILBOX_PASSWORD);
		if ($conn == false) {
			echo "Error: ".imap_last_error();
			return;
		}

		$criteria = "UNSEEN";
		//$criteria = 'SEEN SINCE "1 June 2014"';

		$msgs = imap_sort($conn, SORTARRIVAL, 1, 0, $criteria);
		if (empty($msgs)) {
			echo "No messages to process\n";
			return;
		}
		//echo "Emails found: ".print_r($msgs, true)."<br />";
		echo "# of emails to be processed: ".count($msgs)."\n";

		foreach ($msgs as $msg_num) {
			$headerinfo = imap_headerinfo($conn, $msg_num);
			$from = $headerinfo->fromaddress;
			$date = $headerinfo->date;
			echo "{$msg_num}. from='{$from}', date='{$date}'\n";

			if ($from == "\"www.backpage.com\" <support@relay1.backpage.com>" || $from == "\"www.backpage.com\" <support@relay2.backpage.com>") {
				//new format of BP official email ? 25.2.2015
				self::bpEmail($conn, $msg_num);
				continue;

			} else if (strpos($from, " via ")) {
				$ret = self::getEmailFromBody($conn, $msg_num);
				if ($ret === false)
					continue;

				if ($ret["from"]) {
					$from = $ret["from"];
				} else {
					self::bpEmail($conn, $msg_num);
					continue;
				}

			} else if ($from == "mail@backpage.com" || $from == "www.backpage.com") {
				$ret = self::getEmailFromBody($conn, $msg_num);
				if ($ret === false)
					continue;

				if ($ret["from"]) {
					$from = $ret["from"];
				} else {
					self::bpEmail($conn, $msg_num);
					continue;
				}
		
			} else if (strstr($from, "@backpage.com") !== false) {
				//old format of backpage email - forward to dalaas - not used since 29.4.2014 ???
				self::bpEmail($conn, $msg_num);
				continue;
			}
		
			if (strstr($from, "auxpay.com") || strstr($from, "cashfastfunding.com")) {
				//exception, spam emails
				echo "Spam email, from = '{$from}', deleting...\n";
				imap_delete($conn, $msg_num);
				continue;
			}
	
			if ((strlen($from) < 3) || (filter_var($from, FILTER_VALIDATE_EMAIL) === false)) {
				//error?
				reportAdmin("AS : class backpage - processMailbox error", "Please check txt email content and update decision rules, whether this is client email or backpage email", array("msg_num" => $msg_num, "from" => $from));
				continue;
			}
				
			//old format of client email - client email address is directly in "From" field - not used since 29.4.2014 ???
			$ret = self::insertEmail($from);
			echo "Client email, insert=".intval($ret)."\n";
			if ($ret)
				imap_delete($conn, $msg_num);
		}

		imap_expunge($conn);
		imap_close($conn);
	}

	/**
	 * Processing of Official BP email, that needs to be forwarded to owner
	 */
	private static function bpEmail($conn, $msg_num) {
		//get some info about email
		$ret = self::getEmailFromBody($conn, $msg_num);
		$error = "";
		$txt = "";
		$html = "";
		if ($ret === false) {
			$error = "Cant get Body of Email #{$msg_num} !";
		} else {
			$txt = $ret["txt"];
			$html = $ret["html"];
		}
		reportAdmin("AS : class backpage - check", "This email was forwarded to Maarten. Please check if this is really backpage email, and if not update the rules. Otherwise this email can be deleted.", array("msg_num" => $msg_num, "error" => $error, "txt" => $txt, "html" => $html));
		$ret = self::forward($conn, $msg_num);
		echo "BP email, forward=".intval($ret)."\n";
		return;
	}

	/**
	 * Processing of "new" BP format of client email
	 */
	private static function getEmailFromBody($conn, $msg_num) {
		$bodies = self::getMsgBody($conn, $msg_num);
		if ($bodies === false)
			return false;

		$txt = $bodies["txt"];
		$html = $bodies["html"];
		$txt_lines = explode(PHP_EOL, $txt);
		$txt_first_line = $txt_lines[0];

		$return = array("txt" => $txt, "html" => $html, "from" => "");

		if (preg_match('/^[A-Z][a-z]* *: /', $txt_first_line, $matches)) {
			$len = strlen($matches[0]);
			$return["from"] = substr($txt_first_line, $len, -1);
			//echo "matches, matched content = '".$matches[0]."', email='{$from}'\n";
		}
		
		return $return;
	}

	private static function getMsgBody($conn, $msg_num) {
		$s = imap_fetchstructure($conn, $msg_num);
		//echo print_r($s, true)."\n";
		if ($s->type == 0) {
			if ($s->subtype != "HTML") {
				echo "E0\n";
				reportAdmin("class backpage - forward - unimplemented subtype", "class backpage - forward - unimplemented subtype", array("structure" => print_r($s, true)));
				return false;
			}
			$html = imap_fetchbody($conn, $msg_num, 1);
			if ($s->encoding == 4)
				$html = quoted_printable_decode($html);
			$txt = $html;
		} else {
			if ($s->type != 1) {
				echo "E1\n";
				reportAdmin("class backpage - forward - unimplemented msg type", "class backpage - forward - unimplemented msg type", array("structure" => print_r($s, true)));
				return false;
			}
			if (count($s->parts) != 2) {
				echo "E2\n";
				reportAdmin("class backpage - forward - unimplemented part count", "class backpage - forward - unimplemented part count", array("structure" => print_r($s, true)));
				return false;
			}
			$part1 = $s->parts[0];
			$part2 = $s->parts[1];

			if ($part1->type == 1 && $part1->subtype == "ALTERNATIVE" && count($part1->parts) == 2) {
				//complex structure, part1 is ALTERNATIVE and contains both TXT and HTML
				$part11 = $part1->parts[0];
				$part12 = $part1->parts[1];
				if ($part11->type == 0 && $part11->subtype == "PLAIN") {
					if ($part12->type == 0 && $part12->subtype == "HTML") {
						$txt_partno = "1.1";
						$html_partno = "1.2";
					} else {
						echo "E3\n";
						reportAdmin("class backpage - forward - unimplemented part12 type|subtype", "class backpage - forward - unimplemented part12 type|subtype E3", array("structure" => print_r($s, true)));
						return false;
					}
				} else if ($part11->type == 0 && $part11->subtype == "HTML") {
					if ($part12->type == 0 && $part12->subtype == "PLAIN") {
						$txt_partno = "1.2";
						$html_partno = "1.1";
					} else {
						echo "E4\n";
						reportAdmin("class backpage - forward - unimplemented part11 type|subtype", "class backpage - forward - unimplemented part11 type|subtype E4", array("structure" => print_r($s, true)));
						return false;
					}
				} else {
					echo "E5\n";
					reportAdmin("class backpage - forward - unimplemented part11 type|subtype", "class backpage - forward - unimplemented part11 type|subtype E5", array("structure" => print_r($s, true)));
					return false;
				}
			} else {
				//simple structure, part1 is TXT, part2 is HTML
				if ($part1->type == 0 && $part1->subtype == "PLAIN") {
					if ($part2->type == 0 && $part2->subtype == "HTML") {
						$txt_partno = "1";
						$html_partno = "2";
					} else {
						echo "E6\n";
						reportAdmin("class backpage - forward - unimplemented part1 type|subtype", "class backpage - forward - unimplemented part1 type|subtype E6", array("structure" => print_r($s, true)));
						return false;
					}
				} else if ($part1->type == 0 && $part1->subtype == "HTML") {
					if ($part2->type == 0 && $part2->subtype == "PLAIN") {
						$txt_partno = "2";
						$html_partno = "1";
					} else{
						echo "E7\n";
						reportAdmin("class backpage - forward - unimplemented part2 type|subtype", "class backpage - forward - unimplemented part2 type|subtype E7", array("structure" => print_r($s, true)));
						return false;
					}
				} else {
						echo "E8\n";
						reportAdmin("class backpage - forward - unimplemented part2 type|subtype", "class backpage - forward - unimplemented part2 type|subtype E8", array("structure" => print_r($s, true)));
						return false;
				}
			}
			$txt = imap_fetchbody($conn, $msg_num, $txt_partno);
			$html = imap_fetchbody($conn, $msg_num, $html_partno);
		}
		//echo "\n\nText:\n{$txt}\n";
		//echo "\n\nHTML:\n{$html}\n";
		return array("txt" => $txt, "html" => $html);
	}

	private static function forward($conn, $msg_num) {
		$bodies = self::getMsgBody($conn, $msg_num);
		if ($bodies === false)
			return false;
		$txt = $bodies["txt"];
		$html = $bodies["html"];

		$hi = imap_headerinfo($conn, $msg_num);

		//send email
		$params = array(
			"from" => $hi->fromaddress,
			"to" => "eroticmp@gmail.com",
			"reply_to" => $hi->fromaddress,
			"subject" => "Fwd: ".$hi->subject,
			"html" => $html,
			"text" => $txt,
			);
		$ret = send_email($params, $error);
		if (!$ret) {
			echo "ER6\n";
			reportAdmin("class backpage - forward - send failed", "class backpage - forward - send failed", array("error" => $error, "structure" => print_r($s, true)));
			return false;
		}
		return true;
	}

	private static function insertEmail($email) {
		global $db;

		$res = $db->q("SELECT id FROM bp_emails WHERE email like ?", array($email));
		if ($db->numrows($res))
			return true;

		$res = $db->q("INSERT INTO bp_emails (inserted, email) values (?, ?)", array(time(), $email));
		$aff = $db->affected($res);
		if ($aff == 1)
			return true;
		return false;
	}
}

