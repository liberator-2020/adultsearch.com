<?php
/*
 * bpad
 */
class bpad {

	private $id = NULL,
			$type = NULL,
			$title = NULL,
			$content = NULL,
			$link_text = NULL,
			$paused = NULL,
			$since = NULL,
			$impressions_since = NULL,
			$hits_since = NULL,
			$images = NULL;
			
	function __construct() {
	}

	public static function findOneById($id) {
		global $db;

		$res = $db->q("SELECT * FROM bp_promo_ad WHERE id = ?", array(intval($id)));
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

 		return self::withRow($row);
	}

	public static function withRow($row) {

		$as = new bpad();
		$as->setId($row["id"]);
		$as->setType($row["type"]);
		$as->setTitle($row["title"]);
		$as->setContent($row["content"]);
		$as->setLinkText($row["link_text"]);
		$as->setPaused($row["paused"]);
		$as->setSince($row["since"]);
		$as->setImpressionsSince($row["impressions_since"]);
		$as->setHitsSince($row["hits_since"]);
		
		return $as;
	}

	//getters and setters
	public function getId() {
		return $this->id;
	}
	public function getType() {
		return $this->type;
	}
	public function getTitle() {
		return $this->title;
	}
	public function getContent() {
		return $this->content;
	}
	public function getLinkText() {
		return $this->link_text;
	}
	public function getPaused() {
		return $this->paused;
	}
	public function getSince() {
		return $this->since;
	}
	public function getImpressionsSince() {
		return $this->impressions_since;
	}
	public function getHitsSince() {
		return $this->hits_since;
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	public function setType($type) {
		$this->type = $type;
	}
	public function setTitle($title) {
		$this->title = $title;
	}
	public function setContent($content) {
		$this->content = $content;
	}
	public function setLinkText($link_text) {
		$this->link_text = $link_text;
	}
	public function setPaused($paused) {
		$this->paused = $paused;
	}
	public function setSince($since) {
		$this->since = $since;
	}
	public function setImpressionsSince($impressions_since) {
		$this->impressions_since = $impressions_since;
	}
	public function setHitsSince($hits_since) {
		$this->hits_since = $hits_since;
	}

	//images functions
	private function loadImages() {
		global $db;
		$res = $db->q("SELECT * FROM bp_promo_ad_image WHERE ad_id = ? ORDER BY id ASC", array($this->id));
		$this->images = array();
		while ($row = $db->r($res)) {
			$this->images[] = array("type" => $row["type"], "filename" => $row["filename"]);
		}
	}
	public function getImages() {
		if ($this->images === NULL) {
			$this->loadImages();
		}
		return $this->images;
	}
	public function setImages($images) {
		$this->images = $images;
	}
	public function getImageString() {
		$img_str = "";
		$images = $this->getImages();
		foreach($images as $image) {
			if (!empty($img_str))
				$img_str .= "|";
			$img_str .= $image["type"]."@".$image["filename"];
		}
		return $img_str;
	}
	public function setImageString($img_string) {
		$this->images = array();
		$imgs = explode('|', $img_string);
		foreach ($imgs as $img) {
			if ($img == "")
				continue;
			$params = explode('@', $img);
			if (count($params) != 2)
				continue;
			$this->images[] = array("type" => $params[0], "filename" => $params[1]);
		}
	}
	
	public function add() {
		global $db, $account;

		$res = $db->q("INSERT INTO bp_promo_ad (type, title, content, link_text, paused) VALUES (?, ?, ?, ?, ?);", 
			array($this->type, $this->title, $this->content, $this->link_text, $this->paused));
		$newid = $db->insertid($res);

		if ($newid == 0)
			return false;

		$this->id = $newid;

		//add images
		$cnt_images = $this->addImages();
		if ($cnt_images === false) {	
			audit::log("BPA", "Add", $this->id, "No images", $account->getId());
		} else {
			audit::log("BPA", "Add", $this->id, "{$cnt_images} images", $account->getId());
		}

		return true;
	}

	public function update() {
		global $account, $db;

		if (!$this->id)
			return false;

		$original = bpad::findOneById($this->id);
		if (!$original)
			return false;

		$audit_msg = "";

		//update ad fields
		$update_fields = array();
		if ($this->type != $original->getType())
			$update_fields["type"] = $this->type;
		if ($this->title != $original->getTitle())
			$update_fields["title"] = $this->title;
		if ($this->content != $original->getContent())
			$update_fields["content"] = $this->content;
		if ($this->link_text != $original->getLinkText())
			$update_fields["link_text"] = $this->link_text;
		if ($this->paused != $original->getPaused())
			$update_fields["paused"] = $this->paused;
		if ($this->since != $original->getSince())
			$update_fields["since"] = $this->since;
		if ($this->impressions_since != $original->getImpressionsSince())
			$update_fields["impressions_since"] = $this->impressions_since;
		if ($this->hits_since != $original->getHitsSince())
			$update_fields["hits_since"] = $this->hits_since;
		if (!empty($update_fields)) {
			$changed_fields = "";
			$update = "";
			$update_arr = array();
			foreach ($update_fields as $key => $val) {
				$changed_fields .= (empty($changed_fields)) ? "" : ", ";
				$update .= (empty($update)) ? "SET " : ", ";
				$changed_fields .= $key;
				$update .= "{$key} = ?";
				$update_arr[] = $val;
			}
			$update_arr[] = $this->id;
			$update = "UPDATE bp_promo_ad {$update} WHERE id = ? LIMIT 1";

			$db->q($update, $update_arr);
			$aff = $db->affected();

			if (!$aff)
				return false;

			$audit_msg = "Changed fields: {$changed_fields} ";
		}

		// update ad images
		$imageSelChanged = ($this->images != $original->getImages());
		if ($imageSelChanged) {
			$this->removeImages();
			$cnt_added = $this->addImages();
			if ($cnt_added === false) {
				//we failed adding images
				//lets check if we successfully updated ad field and if yes, then store audit message at least about updated fields
				if (!empty($audit_msg))
					audit::log("BPA", "Edit", $this->id, $audit_msg, $account->getId());
				//and return false (display fail message)
				return false;
			}
			$audit_msg .= "Changed images, currently {$cnt_added} images";
		}

		//if we changed anything, lets store audit message
		if (!empty($audit_msg))
			audit::log("BPA", "Edit", $this->id, $audit_msg, $account->getId());

		return true;
	}

	
	public function remove() {
		global $db, $account;

		$this->removeImages();
		$res = $db->q("DELETE FROM bp_promo_ad WHERE id = ? LIMIT 1", array($this->id));
		$aff = $db->affected($res);
		if ($aff == 1) {
			audit::log("BPA", "Remove", $this->id, "", $account->getId());
			return true;
		} else {
			return false;
		}
	}

	private function addImages() {
		global $db;

		$images_added = 0;
		foreach ($this->images as $image) {
			$res = $db->q("INSERT INTO bp_promo_ad_image (ad_id, type, filename) VALUES (?, ?, ?)", array($this->id, $image["type"], $image["filename"]));
			$aff = $db->affected($res);
			if ($aff != 1)
				return false;
			$images_added++;
		}
		return $images_added;
	}

	private function removeImages() {
		global $db;

		$res = $db->q("DELETE FROM bp_promo_ad_image WHERE ad_id = ? ", array($this->id));
		return true;
	}

}

