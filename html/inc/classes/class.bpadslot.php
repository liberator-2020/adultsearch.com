<?php
/*
 * bpadslot
 */
class bpadslot {

	private $id = NULL,
			$account_id = NULL,
			$type = NULL,
			$bp_code = NULL,
			$bp_oid = NULL,
			$bp_id = NULL,
			$active = NULL,
			$curr_ad_id = NULL,
			$curr_ad_since = NULL,
			$impressions_since = NULL,
			$hits_since = NULL,
			$impressions_total = NULL,
			$hits_total = NULL;
			
	function __construct() {
	}

	public static function findOneById($id) {
		global $db;

		$res = $db->q("SELECT * FROM bp_promo_ad_slot WHERE id = ? LIMIT 1", array(intval($id)));
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

		return self::withRow($row);
	}

	public static function findOneByBpCode($bp_code) {
		global $db;

		$res = $db->q("SELECT * FROM bp_promo_ad_slot WHERE bp_code = ? LIMIT 1", array($bp_code));
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

		return self::withRow($row);
	}

	public static function withRow($row) {

		$as = new bpadslot();
		$as->setId($row["id"]);
		$as->setAccountId($row["account_id"]);
		$as->setType($row["type"]);
		$as->setBpCode($row["bp_code"]);
		$as->setActive($row["active"]);
		$as->setCurrAdId($row["curr_ad_id"]);
		$as->setCurrAdSince($row["curr_ad_since"]);
		$as->setImpressionsSince($row["impressions_since"]);
		$as->setHitsSince($row["hits_since"]);
		$as->setImpressionsTotal($row["impressions_total"]);
		$as->setHitsTotal($row["hits_total"]);
		
		return $as;
	}

	//getters and setters
	public function getId() {
		return $this->id;
	}
	public function getAccountId() {
		return $this->account_id;
	}
	public function getType() {
		return $this->type;
	}
	public function getBpCode() {
		return $this->bp_code;
	}
	public function isActive() {
		return $this->active;
	}
	public function getCurrAdId() {
		return $this->curr_ad_id;
	}
	public function getCurrAdSince() {
		return $this->curr_ad_since;
	}
	public function getImpressionsSince() {
		return $this->impressions_since;
	}
	public function getHitsSince() {
		return $this->hits_since;
	}
	public function getImpressionsTotal() {
		return $this->impressions_total;
	}
	public function getHitsTotal() {
		return $this->hits_total;
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	public function setAccountId($account_id) {
		$this->account_id = $account_id;
	}
	public function setType($type) {
		$this->type = $type;
	}
	public function setBpCode($bp_code) {
		$this->bp_code = $bp_code;
	}
	public function setActive($active) {
		$this->active = $active;
	}
	public function setCurrAdId($curr_ad_id) {
		$this->curr_ad_id = $curr_ad_id;
	}
	public function setCurrAdSince($curr_ad_since) {
		$this->curr_ad_since = $curr_ad_since;
	}
	public function setImpressionsSince($impressions_since) {
		$this->impressions_since = $impressions_since;
	}
	public function setHitsSince($hits_since) {
		$this->hits_since = $hits_since;
	}
	public function setImpressionsTotal($impressions_total) {
		$this->impressions_total = $impressions_total;
	}
	public function setHitsTotal($hits_total) {
		$this->hits_total = $hits_total;
	}
	
	public function getTypeUrl() {
		$url = "";
		switch($this->type) {
			case 1: $url = "female-escorts"; break;
			case 2: $url = "body-rubs"; break;
			case 3: $url = "tstv-shemale-escorts"; break;
			case 4: $url = "strip-clubs"; break;
		}
		return $url;
	}

	public function getBpOid() {
		if (is_null($this->bp_oid))
			$this->fillBpOidId();
		return $this->bp_oid;
	}
	public function getBpId() {
		if (is_null($this->bp_id))
			$this->fillBpOidId();
		return $this->bp_id;
	}
	private function fillBpOidId() {
		$arr = explode("&", $this->bp_code);
		if (!is_array($arr))
			return false;
		foreach ($arr as $item) {
			$arr2 = explode("=", $item);
			if ($arr2[0] == "oid")
				$this->bp_oid = $arr2[1];
			if ($arr2[0] == "id")
				$this->bp_id = $arr2[1];
		}
		return true;
	}

	public function add() {
		global $db, $account;

		$res = $db->q("INSERT INTO bp_promo_ad_slot (type, bp_code) VALUES (?, ?);", array($this->type, $this->bp_code));
		$newid = $db->insertid($res);

		if ($newid > 0) {
			$this->id = $newid;
			audit::log("BPS", "Add", $this->id, "Type: {$this->type}, Code: {$this->bp_code}", $account->getId());
			return true;
		}

		return false;
	}

	public function update() {
		global $account, $db;

		if (!$this->id)
			return false;

		$original = bpadslot::findOneById($this->id);
		if (!$original)
			return false;

		$update_fields = array();
		if ($this->type != $original->getType())
			$update_fields["type"] = $this->type;
		if ($this->account_id != $original->getAccountId())
			$update_fields["account_id"] = $this->account_id;
		if ($this->bp_code != $original->getBpCode())
			$update_fields["bp_code"] = $this->bp_code;
		if ($this->active != $original->isActive())
			$update_fields["active"] = ($this->active) ? 1 : 0;
		if ($this->curr_ad_id != $original->getCurrAdId())
			$update_fields["curr_ad_id"] = $this->curr_ad_id;
		if ($this->curr_ad_since != $original->getCurrAdSince())
			$update_fields["curr_ad_since"] = $this->curr_ad_since;
		if ($this->impressions_since != $original->getImpressionsSince())
			$update_fields["impressions_since"] = $this->impressions_since;
		if ($this->hits_since != $original->getHitsSince())
			$update_fields["hits_since"] = $this->hits_since;

		$changed_fields = "";
		$update = "";
		$update_arr = array();
		foreach ($update_fields as $key => $val) {
			$changed_fields .= (empty($changed_fields)) ? "" : ", ";
			$update .= (empty($update)) ? "SET " : ", ";
			$changed_fields .= $key;
			$update .= "{$key} = ?";
			$update_arr[] = $val;
		}
		$update_arr[] = $this->id;
		$update = "UPDATE bp_promo_ad_slot {$update} WHERE id = ? LIMIT 1";

		$db->q($update, $update_arr);
		$aff = $db->affected();

		if (!$aff)
			return false;

		audit::log("BPS", "Edit", $this->id, "Changed fields: {$changed_fields}", $account->getId());
		return true;
	}

	
	public function remove() {
		global $db;

		$res = $db->q("DELETE FROM bp_promo_ad_slot WHERE id = ? LIMIT 1", array($this->id));
		$aff = $db->affected($res);
		if ($aff == 1) {
			audit::log("BPS", "Remove", $this->id, "", $account->getId());
			return true;
		} else {
			return false;
		}
	}

	//find next ad for this slot
	public function get_next_ad() {
		global $db;

		$res = $db->q("SELECT * from bp_promo_ad WHERE type = ? AND paused = 0 ORDER BY since ASC LIMIT 1", array($this->type));
		if ($db->numrows($res) == 0) {
			return false;
		}
		$row = $db->r($res);
		$ad = bpad::withRow($row);
		return $ad;
	}

	//TODO redo so ad is not param ...
	public function reset_stats($ad) {
		global $db;

		//update our db stats
		$now = time();
		$ad->setSince($now);
		$ad->setImpressionsSince(0);
		$ad->setHitsSince(0);
		$ret = $ad->update();
		//TODO check ret value

		$this->setCurrAdId($ad->getId());
		$this->setCurrAdSince($now);
		$this->setImpressionsSince(0);
		$this->setHitsSince(0);
		$ret = $this->update();
		//TODO check ret value

		return true;
	}

}

