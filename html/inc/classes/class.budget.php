<?php

class budget {

	public function __construct() {
	}

	public function get_name() {
		return "budget";
	}

	public function get_template_path() {
		return _CMS_ABS_PATH."/templates/payment_gen_budget.tpl";
	}

	public function get_budget($account_id) {
		global $db;
		$budget = $db->single("SELECT budget FROM advertise_budget WHERE account_id = ?", [$account_id]);
		return $budget;
	}

	public function export_to_template() {
		global $smarty;
	}

	//payment needs to contain unobfuscated card number and csc
	public function charge($payment, &$error) {
		global $db;

		$now = time();

		$payment_id = $payment["payment_id"];
		$account_id = $payment["account_id"];
		$amount = $payment["amount"];

		$budget = $this->get_budget($account_id);

		if ($budget < $amount) {
			file_log("budget", "charge: budget is less than amount !payment_id={$payment_id}, amount={$amount}, account_id={$account_id}, budget={$budget}");
			reportAdmin("AS: Payment: Budget:charge() error", "budget is less than amount !payment_id={$payment_id}, amount={$amount}, account_id={$account_id}, budget={$budget}");
			$error = "You don't have enough of balance in your account to do this charge.";
			return false;
		}

		//substract budget
		$res = $db->q("UPDATE advertise_budget SET budget = budget - ? WHERE account_id = ? LIMIT 1", [$amount, $account_id]);
		if ($db->affected($res) != 1) {
			file_log("budget", "charge: failed to substract budget: payment_id={$payment_id}, amount={$amount}, account_id={$account_id}");
			reportAdmin("AS: Payment: Budget error", "failed to substract budget:  payment_id={$payment_id}, amount={$amount}, account_id={$account_id}");
			$error = "Error in payment, please contact support.";
			return false;
		}

		//budget substracted, approve payment
		$res = $db->q("UPDATE payment SET response = ?, responded_stamp = ?, result = 'A', processor = 'budget', trans_id = 'budget' WHERE id = ? LIMIT 1",
			["Paid with budget", $now, $payment_id]
			);
		if ($db->affected($res) != 1) {
			file_log("budget", "payment::pay(): failed to update payment: payment_id={$payment_id}, result=A, response=Paid with budget");
			reportAdmin("AS: Payment: Budget error", "cannot update payment payment_id={$payment_id}, result=A, response=Paid with budget");
			$error = "Error in payment, please contact support.";
			return false;
		}

		//add transaction
		$res = $db->q(
			"INSERT INTO transaction (type, payment_id, stamp, trans_id, amount) VALUES (?, ?, ?, ?, ?)",
			["N", $payment_id, $now, "budget", $amount]
   			);
	   	$transaction_id = $db->insertid($res);
		if (!$transaction_id) {
   			reportAdmin("AS: Payment succeeded - error: cant insert transaction", "cant insert transaction into db, payment_id={$payment_id}, trans_id={$trans_id}");
		}

		return true;
	}

	public function capture($payment, &$error) {
		$error = "Not implemented";
		return false;
	}

	public function refund_transaction($transaction, $amount = null, &$error) {
		$error = "Not implemented";
		return false;
	}

	public function refund_charge($account_id, $amount = null, &$error) {
		global $db;

        file_log("budget", "refund_charge: account_id={$account_id}, amount={$amount}");

		$res = $db->q("SELECT budget FROM advertise_budget WHERE account_id = ? LIMIT 1", [$account_id]);
		if ($db->numrows($res) != 1) {
			//account does not have an entry in advertise_budget table ? this should not happen, but it is not critical
			$res = $db->q("INSERT INTO advertise_budget (account_id, budget) VALUES (?, ?)", [$account_id, $amount]);
			$id = $db->insertid($db);
			if (!$id) {
				file_log("budget", "refund_charge: error inserting budget to advertise_budget table !");
				return false;
			}
		}
		$row = $db->r($res);
		$budget_before = $row["budget"];
		$budget_after = $budget_before + $amount;

		$res = $db->q("UPDATE advertise_budget SET budget = IFNULL(budget, 0) + ? WHERE account_id = ? LIMIT 1", [$amount, $account_id]);
		$aff = $db->affected($res);
		if ($aff != 1) {
			file_log("budget", "refund_charge: error updating budget in advertise_budget table !");
			return false;
		}

        file_log("budget", "refund_charge: success, budget_before={$budget_before}, budget_after={$budget_after}");

        return [
            "refund_transaction_id" => "budget",
            ];
	}

	public function rebill($payment_id, $amount, $account_id, &$error) {
		global $db;

		$now = time();

		file_log("budget", "rebill(): payment_id={$payment_id}, account_id={$account_id}, amount={$amount}");

		//check if we have recurring amount specified and card has card id and customer id assigned
		if (!$amount) {
			file_log("budget", "rebill: Error: Payment is not specified as recurring, payment_id={$payment_id}");
			reportAdmin("AS: Rebill Error", "Payment is not specified as recurring, payment_id={$payment_id}");
			$error = "Payment is not specified as recurring";
			return false;
		}

		$budget = $this->get_budget($account_id);

		if ($budget < $amount) {
			file_log("budget", "rebill: budget is less than amount !payment_id={$payment_id}, amount={$amount}, account_id={$account_id}, budget={$budget}");
			$error = "You don't have enough of balance in your account to do this charge.";
			return false;
		}

		//substract budget
		$res = $db->q("UPDATE advertise_budget SET budget = budget - ? WHERE account_id = ? LIMIT 1", [$amount, $account_id]);
		if ($db->affected($res) != 1) {
			file_log("budget", "rebill: failed to substract budget: payment_id={$payment_id}, amount={$amount}, account_id={$account_id}");
			reportAdmin("AS: Payment: Budget error", "failed to substract budget:  payment_id={$payment_id}, amount={$amount}, account_id={$account_id}");
			$error = "Error in payment, please contact support.";
			return false;
		}

		//"rebill" was successful

		//update payment
		$res = $db->q("UPDATE payment SET subscription_status = 1 WHERE id = ? LIMIT 1", [$payment_id]);

		//add transaction
		$res = $db->q(
			"INSERT INTO transaction (type, payment_id, stamp, trans_id, amount) VALUES (?, ?, ?, ?, ?)",
			["N", $payment_id, $now, "budget", $amount]
			);
		$transaction_id = $db->insertid($res);
		if (!$transaction_id)
			reportAdmin("AS: Budget rebill success, but cant insert transaction", "cant insert transaction in db, payment_id={$payment_id}, trans_id={$trans_id}");

		return $transaction_id;
	}

}

//END 
