<?php

class businessowner {

	/**
	 * $row - payment_item row
	 */
	public static function get_html_description($row) {
		global $db;

		$place_id = $row["place_id"];
		$place_sponsor = $row["place_sponsor"];

		$res = $db->q("SELECT name FROM place WHERE place_id = ? LIMIT 1", [$place_id]);
		$row = $db->r($res);
		$html = "Place <a href=\"/dir/place?id={$place_id}\">{$row["name"]}</a>";
		
		if ($place_sponsor)
			$html .= " - SPONSOR";
			
		return $html;
	}

	/**
	 * This is executed from payment class, as soon as purchase is successful
	 */
	public static function paymentSuccessful($payment_item_id) {
		global $db;

		file_log("businessowner", "paymentSuccessful: payment_item_id={$payment_item_id}");

		$res = $db->q("
			SELECT p.account_id, p.amount, p.recurring_period, pi.payment_id, pi.place_id, pi.place_sponsor
			FROM payment_item pi
			INNER JOIN payment p on p.id = pi.payment_id
			WHERE pi.id = ?
			LIMIT 1",
			[$payment_item_id]
			);
		if ($db->numrows($res) != 1) {
			file_log("businessowner", "paymentSuccessful: Error - can't fetch info from db, payment_item_id={$payment_item_id}");
			reportAdmin("AS: businessowner:paymentSuccessful error", "Error - can't fetch info from db, payment_item_id={$payment_item_id}");
			return false;
		}
		$row = $db->r($res);
		$payment_id = $row["payment_id"];
		$account_id = $row["account_id"];
		$amount = $row["amount"];
		$recurring_period = $row["recurring_period"];
		$place_id = $row["place_id"];
		$place_sponsor = ($row["place_sponsor"] == 1) ? 1 : 0;
				
		file_log("businessowner", "paymentSuccessful: payment_id={$payment_id}, payment_item_id={$payment_item_id}, account_id={$account_id}, place_id={$place_id}, place_sponsor={$place_sponsor}, recurring_period={$recurring_period}");

		if ($recurring_period != 30 && $recurring_period != 360) {
			file_log("businessowner", "paymentSuccessful: Error - invalid period: '{$recurring_period}', payment_item_id={$payment_item_id}");
			reportAdmin("AS: businessowner:paymentSuccessful error", "Error - invalid period: '{$recurring_period}', payment_item_id={$payment_item_id}");
			return false;
		}

		//add account_businessowner entry
		$res = $db->q("INSERT INTO account_businessowner 
			(place_id, account_id, starts, expires, day) 
			values 
			(?, ?, NOW(), DATE_ADD(NOW(), INTERVAL {$recurring_period} DAY), ?)",
			[$place_id, $account_id, $recurring_period]
			);
		$id = $db->insertid($res);
		if (!$id) {
			file_log("businessowner", "paymentSuccessful: Error - failed to insert account_businessowner entry: place_id={$place_id}, account_id={$account_id}, day={$recurring_period}");
			reportAdmin("AS: businessowner:paymentSuccessful error", "Error - failed to insert account_businessowner entry: payment_item_id={$payment_item_id}, place_id={$place_id}, account_id={$account_id}, day={$recurring_period}");
			return false;
		}

		//mark in operation log
		$now = time();
		$op_msg = "Account #{$account_id} paid \${$amount} for {$recurring_period} days, id {$place_id}";
		$db->q("INSERT INTO admin_operation_log 
				(code, stamp, msg) 
				values 
				('last_businessowner', ?, ?) 
				on duplicate key update stamp = ?, msg = ?",
				array($now, $op_msg, $now, $op_msg)
				);

		//update of place table
		file_log("businessowner", "paymentSuccessful: update place table: owner={$account_id}, sponsor={$place_sponsor}, place_id={$place_id}");
		$db->q("UPDATE place SET owner = ?, sponsor = ? WHERE place_id = ?", [$account_id, $place_sponsor, $place_id]);

		return true;
	}

	public static function afterPayment($payment_item_id) {
		global $db, $config_site_url;

		file_log("businessowner", "afterPayment: payment_item_id={$payment_item_id}");

		$res = $db->q("SELECT pi.place_id FROM payment_item pi WHERE pi.id = ? LIMIT 1", [$payment_item_id]);
		if ($db->numrows($res) != 1) {
			file_log("businessowner", "afterPayment: Error - can't fetch info from db, payment_item_id={$payment_item_id}");
			reportAdmin("AS: businessowner:afterPayment error", "Error - can't fetch info from db, payment_item_id={$payment_item_id}");
			return false;
		}
		$row = $db->r($res);
		$place_id = $row["place_id"];

		//redirect to that place
		$url = $config_site_url."/dir/place?id=".$place_id;
		file_log("businessowner", "afterPayment: redirecting to url '{$url}'");
		system::go($url, "Thank you for your payment. Your business will be upgraded in next 5 minutes.");
		die;
	}
	
}

