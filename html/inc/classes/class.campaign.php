<?php
/*
 * campaign = advertising campaign
 */
class campaign {

	private $id = NULL,
			$type = NULL,
			$account_id = NULL,
			$name = NULL,
			$status = NULL,
			$created_stamp = NULL;

	private $account = NULL;
	private $placements = NULL;

	function __construct() {
	}

	public static function findOneById($id) {
		global $db;

		$res = $db->q("SELECT * FROM advertise_campaign WHERE id = ?", array(intval($id)));
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

		return self::withRow($row);
	}
	
	public static function withRow($row) {

		$campaign = new campaign();
		$campaign->setId($row["id"]);
		$campaign->setType($row["type"]);
		$campaign->setAccountId($row["account_id"]);
		$campaign->setName($row["name"]);
		$campaign->setStatus($row["status"]);
		$campaign->setCreatedStamp($row["created_stamp"]);

		return $campaign;
	}
	
	//getters and setters
	public function getId() {
		return $this->id;
	}
	public function getType() {
		return $this->type;
	}
	public function getAccountId() {
		return $this->account_id;
	}
	public function getName() {
		return $this->name;
	}
	public function getStatus() {
		return $this->status;
	}
	public function getCreatedStamp() {
		return $this->created_stamp;
	}

	public function setId($id) {
		$this->id = $id;
	}
	public function setType($type) {
		$this->type = $type;
	}
	public function setAccountId($account_id) {
		$this->account_id = $account_id;
	}
	public function setName($name) {
		$this->name = $name;
	}
	public function setStatus($status) {
		$this->status = $status;
	}
	public function setCreatedStamp($created_stamp) {
		$this->created_stamp = $created_stamp;
	}

	//relationships
	public function getAccount() {
		if ($this->account)
			return $this->account;
		$acc = account::findOneById($this->account_id);
		if (!$acc)
			return NULL;
		$this->account = $acc;
		return $this->account;
	}	

	public function getPlacements() {
		global $db;

		if ($this->placements !== NULL)
			return $this->placements;

		$this->placements = array();
		$res = $db->q("SELECT id, s_id FROM advertise_camp_section WHERE c_id = ? ORDER BY id ASC", array($this->id));
		while ($row = $db->r($res)) {
			$this->placements[$row["id"]] = $row["s_id"];
		}

		return $this->placements;
	}


	//CRUD functions
	function update($audit = false) {
		global $account, $db;

		if (!$this->id)
			return false;

		$original = campaign::findOneById($this->id);
		if (!$original)
			return false;

		$update_type = false;
		$update_fields = array();

		if ($this->type != $original->getType())
			$update_fields["type"] = $this->type;
		$update_account = false;
        if ($this->account_id != $original->getAccountId()) {
            $update_fields["account_id"] = $this->account_id;
            $update_account = true;
        }
		if ($this->name != $original->getName())
			$update_fields["name"] = $this->name;

		$update_status = false;
		if ($this->status != $original->getStatus()) {
			$update_fields["status"] = $this->status;
			$update_status = true;
		}

		if ($this->created_stamp != $original->getCreatedStamp())
			$update_fields["created_stamp"] = $this->created_stamp;

		if (empty($update_fields))
			return true;	//nothing to update

		$changed_fields = "";
		$update = "";
		$update_arr = array();
		foreach ($update_fields as $key => $val) {
			$changed_fields .= (empty($changed_fields)) ? "" : ", ";
			$update .= (empty($update)) ? "SET " : ", ";
			$changed_fields .= "{$key}";
			if (method_exists($original, get_getter_name($key)))
				$changed_fields .= ": ".call_user_func(array($original, get_getter_name($key)))."->".call_user_func(array($this, get_getter_name($key)));
			$update .= "`{$key}` = ?";
			$update_arr[] = $val;
		}
		$update_arr[] = $this->id;
		$update = "UPDATE advertise_campaign {$update} WHERE id = ? LIMIT 1";

		$db->q($update, $update_arr);
		$aff = $db->affected();

		if (!$aff)
			return false;

		if ($update_status)
			$this->update_status();

		if ($audit)
			audit::log("ACA", "Edit", $this->id, "Changed fields: {$changed_fields}", $account->getId());

		return true;
	}

	public function duplicate($new_campaign_name, $status, &$error) {
		global $db;

		$now = time();

		//TODO until this class supports all attrs, do another select
		$res = $db->q("SELECT * FROM advertise_campaign WHERE id = ? LIMIT 1", [$this->id]);
		if ($db->numrows($res) != 1) {
			$error = "Can't fetch campaign from DB!";
			return false;
		}
		$row = $db->r($res);

		//insert new campaign
        $res = $db->q("INSERT INTO advertise_campaign
            (account_id, type, name
                , ad_title, ad_line1, ad_line2, ad_durl, ad_https, ad_url
                , budget, category, status, dcpc, external, expire
                , code, cpm, banner_iframe_src, banner_a_href, banner_img_src
                , created_stamp, locked)
            VALUES
            (?, ?, ?
                , ?, ?, ?, ?, ?, ?
                , ?, ?, ?, ?, ?, ?
                , ?, ?, ?, ?, ?
                , ?, ?)
            ",
            [$row["account_id"], $row["type"], $new_campaign_name
				, $row["ad_title"], $row["ad_line1"], $row["ad_line2"], $row["ad_durl"], $row["ad_https"], $row["ad_url"]
				, $row["budget"], $row["category"], $status, $row["dcpc"], $row["external"], $row["expire"]
				, $row["code"], $row["cpm"], $row["banner_iframe_src"], $row["banner_a_href"], $row["banner_img_src"]
				, $now, $row["locked"]
			]);
		$new_id = $db->insertid($res);
		if (!$new_id) {
			$error = "Can't insert new campaign to DB!";
			return false;
		}
		file_log("advertise", "campaign::duplicate(): Campaign #{$this->id} duplicated into new campaign #{$new_id}.");

		//copy placements
		$i = 0; $ids = [];
		$res = $db->q("SELECT * FROM advertise_camp_section WHERE c_id = ? ORDER BY id", [$this->id]);
		while ($row = $db->r($res)) {
			$res2 = $db->q("INSERT INTO advertise_camp_section
				(c_id, s_id, section, status, status2, approved, budget_available
					, name, bid, `show`
					, haschild, customurl, reason, recursion
					, cpm, impressions_since, flat_deal_status)
				VALUES
				(?, ?, ?, ?, ?, ?, ?
					, ?, ?, ?
					, ?, ?, ?, ?
					, ?, ?, ?)",
				[$new_id, $row["s_id"], $row["section"], $row["status"], $status, $row["approved"], $row["budget_available"]
					, $row["name"], $row["bid"], $row["show"]
					, $row["haschild"], $row["customurl"], $row["reason"], $row["recursion"]
					, $row["cpm"], 0, $row["flat_deal_status"]
				]);
			$new_cs_id = $db->insertid($res2);
			if (!$new_cs_id) {
				file_log("advertise", "campaign::duplicate(): Error: can't duplicate placement #{$row["id"]} under duplicated campaign #{$new_id} !");
				$error = "Can't insert copy of placement #{$row["id"]} under cloned campaign #{$new_id} !";
				return false;
			}
			$ids[] = $new_cs_id;
			$i++;
		}

		file_log("advertise", "campaign::duplicate(): {$i} placements also duplicated (ids=".implode(",", $ids).")");

		return $new_id;
	}

	private function update_status() {
		global $db;

		$res2 = $db->q("UPDATE advertise_camp_section SET status2 = ? WHERE c_id = ?", array($this->status, $this->id));
		$aff2 = $db->affected($res2);

		if ($aff2 > 0)
			return true;
		return false;
	}

	//additional functions

	//this function sends admin notification if this campaign has any flat deal active
	public function notifyIfHasFlatDeal($action) {
		global $db;

		$res = $db->q("SELECT count(*) as cnt FROM advertise_camp_section WHERE c_id = ? AND flat_deal_status = 1", [$this->id]);
		if (!$db->numrows($res))
			return false;

		$row = $db->r($res);
		if (!$row["cnt"])
			return true;

		reportAdmin("AS: Campaign with flat deal modified", "Campaign #{$this->id} was modified: '{$action}', but it has flat deal on some placement, please manually check that change is ok !");
		return true;
	}

	
}

