<?php

class ccbill {

	//CCBill params
	var $ccbill_post_url = "https://bill.ccbill.com/jpost/signup.cgi";
	var $formName = "211cc";
	var $language = "English";
	var $currencyCode = "840";	//https://www.ccbill.com/cs/wiki/tiki-index.php?page=Webhooks%20User%20Guide#Appendix_A:_Currency_Codes
	var $salt = "Ga38qc91";
	var $pepper = "A43fgA4a91q";

	//transaction params
	var $total;
	var $period = 30;			//subscription length in days, normally its 30 days (for ads), but for bussinessowner it could be 360 days 
	var $recurring = false;		//if set to some number (float), this will be recurring price set to, used in step4.php and recurring.php
	var $recurringPeriod = 30;	//recurring period in days, normally it is 30 days
	var $what = NULL;
	var $page = NULL;
	var $item_id = NULL;
	var $p2 = NULL;
	var $p3 = NULL;
	var $template = NULL;		// in case to show different template

	//TODO review these
	var $Address, $Zip, $City, $State, $Country, $Email, $FirstName, $LastName, $Description;

	var $register_as_user = false, $cc_id, $no_promotion = false, $preset_promotion = false, $promo = NULL, $saved = 0, $promo_code = 0;
	var $promo_notify = 0, $promo_notify_to = NULL, $promo_left = 0, $promo_id = 0, $phone = NULL, $loclimit = NULL, $promo_section, $cl_ad = NULL;
	var $setassponsor = 0;
	var $sidesponsor = 0;
	var $citythumbnail = 0;
	var $reposts = 0;
	var $original_price = 0, $discount = 0, $promo_recurring = 0, $promo_day = 0, $promo_upgrade_only = false, $no_trial = false, $subcat = 0;
	var $promo_row = NULL;

	public function __construct() {
	}

	private function handlepromo(&$error) {
		global $account, $smarty, $db;

		$account_id = $account->isloggedin();

		$this->original_price = $this->total;

		if (empty($_POST["cc_promo"]) || ($this->no_promotion && !$this->preset_promotion)) {
			return;
		}

		$promo = GetPostParam("cc_promo");
		if (empty($promo)) {
			$error = "You did not type any code, if you do not have a promotion code, use the submit button under the payment form.";
			return;
		}

		if (str_replace(' ', '', strtolower(trim($promo))) == "backpage")
			$promo = "backpage";

		$rex = $db->q("SELECT * FROM classifieds_promocodes WHERE code = ? AND deleted IS NULL", array($promo));
		if( !$db->numrows($rex) ) {
			$error = "Promotion code could not found. If you do not have a promotion code, leave that field empty.";
			return;
		}
		$this->promo_row = $rox = $db->r($rex);

		if( $this->promo_upgrade_only && ($rox['setassponsor']||$rox['recurring']||$rox['citythumbnail']||$rox['sidesponsor']||$rox['reposts']) ) {
			$error = "This promo code is not eligible for this purchase.";
			return;
		}

		if( $this->no_trial && $rox['free'] == 1 && ($rox['day'] > 0 && $rox['day'] < 30) ) {
			$error = "This promo code is not eligible for this purchase.";
			return;
		}

		if( $this->subcat && $rox['subcat'] && $this->subcat != $row['subcat'] ) {
			$error = "This promo code is not eligible for this category.";
			return;
		}

		$this->promo_section = $rox['section'];
		$this->setassponsor = $rox['setassponsor'];
		$this->citythumbnail = $rox['citythumbnail'];
		$this->sidesponsor = $rox['sidesponsor'];
		$this->reposts = $rox['reposts'];
		$this->promo_recurring = $rox['recurring'];
		$this->promo_day = $rox['day'];
		if( $rox["limit_per_account"] == 1 ) {
			$ip = account::getUserIp();
			$phone_sql = !empty($this->phone) ? "or phone = '{$this->phone}'" : '';
			$rexx = $db->q("select id from classifieds_promocodes_usage where code_id = '{$rox["id"]}' and (account_id = '$account_id' or ip = inet_aton('$ip') $phone_sql) limit 1");
			if( $db->numrows($rexx) ) {
				$error = "You may not use this promotion code more than once.";
				reportAdmin("You may not use this promotion code more than once.");
				return;
			} 
		}

		if( $rox["left"] < 1 ) {
			$error = "This promotion code is not valid anymore.";
			return;
		} else if( $rox["max"] > 0 && $this->total > $rox["max"] ) {
			$error = "This promotion code is only valid up to \${$rox["max"]} amount of payments.";
			return;
		} else if( !empty($rox['section']) && strcmp($this->what, $rox['section']) ) {
			$error = "This promotion code is not eligible for this section.";
			return;
		} else if( $rox['loclimit'] && $this->loclimit && $rox['loclimit'] < $this->loclimit ) {
			$error = "This promotion code can not be used to post in more than 1 location";
			reportAdmin('This promotion code can not be used to post in more than 1 location');
			return;
		}

		$this->promo_id = $rox["id"];
		$this->promo = $promo;
		$this->promo_code = $rox['id'];
		if( $rox["notify"] == 1 ) {
			$this->promo_notify = 1;
			$this->promo_notify_to = $rox["notify_to"];
			$this->promo_left = $rox["left"];
		}

		if( $rox["discount"] > 0 ) {
			if( !$this->preset_promotion )
				$this->total -= $rox["discount"];
			if ($this->total < 0)
				$this->total = 0;
			$this->saved = $rox["discount"];
		} else if( $rox["percentage"] > 0 ) { 
			$this->saved =  $this->total - ($this->total-($this->total*$rox["percentage"]/100)); 
			if( !$this->preset_promotion )
				$this->total -= ($this->total*$rox["percentage"]/100);
			$this->discount = $rox["percentage"];
		} elseif( $rox["addup"] > 0 ) {
			if( !$this->preset_promotion )
				$this->total += $rox["addup"];
			$this->saved = $rox["addup"];
		} elseif ( $rox["fixedprice"] > 0 ) {
			if( !$this->preset_promotion )
				$this->total = $rox["fixedprice"];
		} else {
			$this->saved = $this->total;
			if( !$this->preset_promotion )
				$this->total = 0;
		}

		if (!is_int($this->total))
			$this->total = number_format($this->total, 2, '.', '');

		if( !is_int($this->saved) && $this->saved )
			$this->saved = number_format($this->saved, 2, '.', '');

		if( $this->preset_promotion )
			return true;

		$smarty->assign("cc_promo", $promo);
		$smarty->assign("cc_redeemed", number_format($this->saved, 2));
		if( isset($_POST["redeem"]) )
			$error = "";				
		return true;
	}

	public function CL_stat($what = '', $count = 1) {
		global $account, $db;

		if( $what == '' ) {
			reportAdmin('Cl_Stat() with no param'); return;
		}

		if (!$this->item_id || $this->what != 'classifieds')
			return;

		$res = $db->q("select * from classifieds where id = '$this->item_id'");
		if (!$db->numrows($res))
			return;

		$account_id = $account->isloggedin();
		$rowcl = $db->r($res);
		$rex = $db->q("select loc_id from classifieds_loc where post_id = '$this->item_id'");
		if ($this->discount)
			$discount = $this->discount;
		else
			$discount = $this->total == 0 ? 100 : floor(($this->saved/$this->total)*100);

		while($row=$db->r($rex)) {
			if($what == 'new')
				$query = "INSERT INTO classifieds_log
							(post_id, time, account_id, loc_id, original, made, recurring, repost, promo_code, promo, discount, total_loc, firstad) 
							values
							('{$this->item_id}', now(), '$account_id', '{$row['loc_id']}', '$this->original_price', '$this->total', '0', '{$rowcl['auto_renew']}', '$this->promo_code', '$this->promo', '$discount', '{$rowcl['total_loc']}', if((select count(*) from classifieds where account_id = '$account_id' limit 1),0,1))";

			elseif ($what == 'recurring')
				$query = "INSERT INTO classifieds_log 
							(post_id, time, account_id, loc_id, original, made, recurring, repost, promo_code, promo, discount, total_loc, firstad) 
							values
							('{$this->item_id}', now(), '$account_id', '{$row['loc_id']}', '$this->original_price', '$this->total', '1', '0', '$this->promo_code', '$this->promo', '$discount', '{$rowcl['total_loc']}', if((select count(*) from classifieds where account_id = '$account_id' limit 1),0,1))";

			elseif ($what == 'upgrade')
				$query = "INSERT INTO classifieds_log
							(post_id, time, account_id, loc_id, original, made, recurring, repost, promo_code, promo, discount, total_loc, firstad) 
							values
							('{$this->item_id}', now(), '$account_id', '{$row['loc_id']}', '$this->original_price', '$this->total', '0', '$count', '$this->promo_code', '$this->promo', '$discount', '{$rowcl['total_loc']}', if((select count(*) from classifieds where account_id = '$account_id' limit 1),0,1))";

			$db->q($query);
		}
	}


	/**
	 * Updates all classified tables after successful purchase
	 */
	private function finishup() {
		global $account, $db;

		file_log("ccbill", "ccbill:finishup(): code='{$this->promo_row['code']}'");

		if (empty($this->promo_row['code']))
			return;

		$account_id = $account->isloggedin();
		$ip = account::getUserIp();
		$today = mktime(0, 0, 0);

		$db->q("UPDATE classifieds_promocodes SET `left` = `left` - 1 WHERE code = ?", array($this->promo_row['code']));

		$db->q("INSERT INTO classifieds_promocodes_usage
				(code_id, account_id, ip, date, paid, saved) 
				values 
				(?, ?, inet_aton('$ip'), curdate(), ?, ?)",
				array($this->promo_row['id'], $account_id, $this->total, $this->saved));

		if ($this->promo_row['setassponsor'] && $this->what == 'classifieds' && $this->item_id) {
			$db->q("UPDATE classifieds
					SET sponsor = 1, sponsor_mobile = 1
					WHERE id = ?", 
					array($this->item_id)
					);
		}
		if ($this->promo_row['citythumbnail'] && $this->what == 'classifieds' && $this->item_id) {
			$res = $db->q("SELECT * from classifieds_loc WHERE post_id = ?", array($this->item_id));
			while ($row = $db->r($res)) {
				$expire_stamp = time() + 86400*30;
				$db->q("INSERT INTO classifieds_sponsor
						(loc_id,state_id, post_id, day, type, expire_stamp, done, notified)
						VALUES
						(?, ?, ?, ?, ?, ?, ?, ?)
						on duplicate key update type = ?, expire_stamp = ?, done = ?
						",
						array($row["loc_id"], $row["state_id"], $row["post_id"], 30, $row["type"], $expire_stamp, $row["done"], 0, $row["type"], $expire_stamp, $row["done"])
						);
			}
		}
		if ($this->promo_row['sidesponsor'] && $this->what == 'classifieds' && $this->item_id) {
			$res = $db->q("SELECT * from classifieds_loc WHERE post_id = ?", array($this->item_id));
			while ($row = $db->r($res)) {
				$db->q("INSERT INTO classifieds_side
						(loc_id,state_id, post_id, day, type, expire_stamp, done, notified)
						VALUES
						(?, ?, ?, ?, ?, ?, ?, ?)
						on duplicate key update type = ?, expire_stamp = ?, done = ?
						",
						array($row["loc_id"], $row["state_id"], $row["post_id"], 30, $row["type"], time()+(3600*24*30), $row["done"], 0, $row["type"], time()+(3600*24*30), $row["done"])
						);
			}
		}
		if ($this->promo_row['recurring'] == 1 && $this->what == 'classifieds' && $this->item_id) {
			$db->q("UPDATE classifieds
					SET recurring = 1, recurring_total = ?, auto_renew = 30, auto_renew_fr = 1, time_next = $today+60*60*24+60*auto_renew_time 
					WHERE id = ?",
					array($this->total, $this->item_id)
					);
		} elseif (intval($this->promo_row['reposts']) > 0 && $this->what == 'classifieds' && $this->item_id) {
			$db->q("UPDATE classifieds
					SET auto_renew = ?, auto_renew_fr = 1, time_next = $today+60*60*24+60*auto_renew_time 
					WHERE id = ?",
					array(intval($this->promo_row['reposts']), $this->item_id)
					);
		}

		if ($this->promo_row['day'] && $this->what == 'classifieds' && $this->item_id) {
			$db->q("UPDATE classifieds SET expires = date_add(now(), interval {$this->promo_row['day']} day) WHERE id = ?", array($this->item_id));
		} 

		if ($this->promo_row['notify']) {
			$this->promo_left = $this->promo_row['left'] - 1;
			$emailm = "Promo code <b>{$this->promo_row['code']}</b> is used to post the following ad ";

			if ($this->promo_row['section'] == 'classifieds') {
				$emailm .= "<a href='http://adultsearch.com/classifieds/look?id={$this->item_id}'>http://adultsearch.com/classifieds/look?id={$this->item_id}</a>.";
			} elseif ($this->promo_row['section'] == 'businessowner') {
				$emailm .= "<a href='{$this->item_link}'>{$this->item_link}</a>.";
			}

			$emailm .= "<br>Total paid: $<b>{$this->total}</b><br>Account ID: <b>{$account_id}</b><br>This promo code can be used <b>{$this->promo_left}</b> times more.";
			if (!empty($this->promo_row['notify_to'])) {
				sendEmail("support@adultsearch.com", "Promotion Used", $emailm, $this->promo_row['notify_to']);
			}
		}

		file_log("ccbill", "ccbill:finishup(): done");
	}

	/**
	 * Used in:
	 * ./_cms_files/adbuild/step4.php
	 * ./_cms_files/classifieds/sponsor.php
	 * ./_cms_files/classifieds/side.php
	 * ./_cms_files/classifieds/upgrade.php
	 * ./_cms_files/classifieds/recurring.php
	 * ./_cms_files/businessowner/payment.php
	 * ./_cms_files/advertise/addfunds.php
	 */
	public function fullcharge(&$error) {
		global $smarty, $account, $db, $config_dev_server;

		$account_id = $account->isloggedin();

		if ($this->p2 == "adbuild")
			$this->p3 = $this->item_id;

		$this->handlepromo($error);

		if ($this->total == 0 ) {
			$this->finishup();
			//TODO
//			$this->afterPurchase(true, false, 0, $this->what, $this->item_id, $this->page);
			return 1;
		}

		file_log("ccbill", "ccbill::fullcharge()");

		$firstname = GetPostParam("cc_firstname");
		$lastname = GetPostParam("cc_lastname");
		$address = GetPostParam("cc_address");
		$zipcode = GetPostParam("cc_zipcode");
		$city = GetPostParam("cc_city");
		$state = GetPostParam("cc_state");
		$country = GetPostParam("cc_country");

		if (isset($error))
			$error = $error;
		else if (empty($firstname))
			$error = "Please type your first name";
		else if (empty($lastname))
			$error = "Please type your last name";
		else if (empty($address))
			$error = "Please type your address associated with you credit card";
		else if (empty($zipcode))
			$error = "Please type your 'ZIP/Postal Code'";
		else if (empty($city))
			$error = "Please type your city";
		else if (empty($state))
			$error = "Please type your state";
		else if (empty($country))
			$error = "Please select your country";
		//new spammer test
		else if (spammer::isCurrentSpammer())
			$error = "System evaluated subject of this payment as spam. Please contact support@adultsearch.com";
		else {

			$params = array(
				"address" => $address,
				"zip" => $zipcode,
				"firstname" => $firstname,
				"lastname" => $lastname,
				"email" => $_SESSION["email"],
				"city" => $city,
				"state" => $state,
				"country" => $country,
				"total" => $this->total,
				);

			//recurring payment
			if ($this->recurring && $this->recurringPeriod) {
				$params["recurring_amount"] = $this->recurring;
				$params["recurring_period"] = $this->recurringPeriod;
			}

			//TODO we leave this for now (might be used outside of class payment), but going to be removed
			$this->Address = $address;
			$this->Zip = $zipcode;
			$this->Email = $_SESSION["email"];
			$this->FirstName = $firstname;
			$this->LastName = $lastname;
			$this->City = $city;
			$this->State = $state;
			$this->Country = $country;

//			if (in_array($_SESSION["account"], array(3974,74069,156889,156890,158546,184431,184432))) {
//				file_log("ccbill", "ccbill::fullcharge(): account_id in between test accont ids, skipping actual charge!");
//				$this->process();
//			} else {
				file_log("ccbill", "ccbill::fullcharge(): before charge(): params=".print_r($params, true));
				$this->charge($params, $error);
//			}
		}

		$smarty->assign("country_options", location::getCountryHtmlOptions());
		if (!empty($_POST)) {
			$smarty->assign("cc_firstname", htmlspecialchars($_POST["cc_firstname"]));
			$smarty->assign("cc_lastname", htmlspecialchars($_POST["cc_lastname"]));
			$smarty->assign("cc_address", htmlspecialchars($_POST["cc_address"]));
			$smarty->assign("cc_city", htmlspecialchars($_POST["cc_city"]));
			$smarty->assign("cc_state", htmlspecialchars($_POST["cc_state"]));		
			$smarty->assign("cc_zipcode", htmlspecialchars($_POST["cc_zipcode"]));
			$smarty->assign("cc_country", htmlspecialchars($_POST["cc_country"]));
			$smarty->assign("error", $error);
		}

		$smarty->assign("total", $this->total);
		$smarty->assign("register", $account_id<1 && $this->register_as_user);
		$smarty->assign("no_promotion", $this->no_promotion);

		file_log("ccbill", "ccbill::fullcharge(): displaying payment page error='{$error}'");
		if ($this->template)
			$smarty->display(_CMS_ABS_PATH.$this->template);
		else 
			$smarty->display(_CMS_ABS_PATH."/templates/payment_gen_ccbill.tpl");

		return false;
	}
	
	/**
	 * this method does actual charge for params
	 */	
	public function charge($params, &$error) {
		global $account, $db, $config_ccbill_clientAccnum, $config_ccbill_clientSubacc;

		//check completeness of params
		if (!array_key_exists("total", $params) || $params["total"] == ""
				|| !array_key_exists("firstname", $params) || $params["firstname"] == ""
				|| !array_key_exists("lastname", $params) || $params["lastname"] == ""
				|| !array_key_exists("address", $params) || $params["address"] == ""
				|| !array_key_exists("zip", $params) || $params["zip"] == ""
				|| !array_key_exists("email", $params) || $params["email"] == ""
			) {

			$m = "Payment params:<br /><pre>".print_r($params, true)."</pre><br />\n";
			sendEmail("support@adultsearch.com", "Payment failed - mandatory fields not filled.", $m, ADMIN_EMAIL);
			$error = "Something is missing.";
			return 0;
		}

		if ($params["total"] <= 0)
			return 1;

		//this is used when making purchase from hotleads - we are not logged in on AS, but we're doing purchase on behalf of the owner of the ad
		if (array_key_exists("account_id", $params) && $params["account_id"])
			$account_id = $params["account_id"];
		else
			$account_id = $account->getId();

		//form request
		$total = number_format($params["total"], 2, ".", "");
		$url = $this->ccbill_post_url."?clientAccnum={$config_ccbill_clientAccnum}&clientSubacc={$config_ccbill_clientSubacc}&formName={$this->formName}&language={$this->language}";
		$url .= "&customer_fname=".urlencode($params["firstname"])."&customer_lname=".urlencode($params["lastname"])."&address1=".urlencode($params["address"]);
		$url .= "&city=".urlencode($params["city"])."&state=".urlencode($params["state"])."&zipcode=".urlencode($params["zip"]);
		$url .= "&email=".urlencode($params["email"])."&formPrice={$total}&formPeriod={$this->period}&currencyCode={$this->currencyCode}";
//		if ($this->recurring) {
		if (array_key_exists("recurring_amount", $params) && array_key_exists("recurring_period", $params)) {
			$formRecurringPrice = number_format($params["recurring_amount"], 2, ".", "");
			$formRebills = "99";
			$url .= "&formRecurringPrice={$formRecurringPrice}&formRecurringPeriod={$params["recurring_period"]}&formRebills={$formRebills}";
			$formDigest = md5($total.$this->period.$formRecurringPrice.$params["recurring_period"].$formRebills.$this->currencyCode.$this->salt);
		} else {
			$formDigest = md5($total.$this->period.$this->currencyCode.$this->salt);
		}
		$url .= "&formDigest={$formDigest}";
		//custom vars
		$url .= "&account_id={$account_id}&what={$this->what}&item_id={$this->item_id}&promo_code={$this->promo_code}&page={$this->page}&p2={$this->p2}&p3={$this->p3}";

		//store ccbill post request into DB
		$now = time();
		$db->q("INSERT INTO ccbill_post (stamp, p1, p2, p3, post) VALUES (?, ?, ?, ?, ?)", array($now, $this->what, $this->p2, $this->p3, $url));
		$ccbill_post_id = $db->insertid();
		if (!$ccbill_post_id) {
			reportAdmin("AS: ccbill::charge(): Can't store post into db", "", array("stamp" => $now, "what" => $what, "p2" => $this->p2, "p3" => $this->p3, "post" => $url));
			return false;
		}
		$p2_until_pipe = $this->p2;
		if (($pos = strpos($p2_until_pipe, "|")) !== false) {
			$p2_until_pipe = substr($p2_until_pipe, 0, $pos);
		}
		$p3_until_pipe = $this->p3;
		if (($pos = strpos($p3_until_pipe, "|")) !== false) {
			$p3_until_pipe = substr($p3_until_pipe, 0, $pos);
		}
		$checksum = md5($account_id.$this->what.$this->item_id.$ccbill_post_id.$this->promo_code.$p2_until_pipe.$p3_until_pipe.$this->pepper);
		$url .= "&ccbill_post_id={$ccbill_post_id}&checksum={$checksum}";

		file_log("ccbill", "ccbill::charge(): ccbill_post_id={$ccbill_post_id}, url={$url}");

		//store important things into session
		$_SESSION["ccbill_post_id"] = $ccbill_post_id;
		$_SESSION["what"] = $this->what;
		$_SESSION["item_id"] = $this->item_id;
		$_SESSION["page"] = $this->page;
		$_SESSION["p2"] = $this->p2;
		$_SESSION["p3"] = $this->p3;

		file_log("ccbill", "ccbill::charge(): SESSION store: ccbill_post_id={$_SESSION["ccbill_post_id"]}, what={$_SESSION["what"]}, item_id={$_SESSION["item_id"]}, page={$_SESSION["page"]}, p2={$_SESSION["p2"]}, p3={$_SESSION["p3"]}");

		//empty payment forward session variables - for example if escort yesterday paid on TS website and today sheis buying ad on AS, so we dont redirect her to TS !
		$_SESSION["pf_what"] = NULL;
		$_SESSION["pf_payment_id"] = NULL;

		if (!$this->checkip($url)) {
			system::go("/payment/checkip");
		}
		return true;
	}

	/*
	 * Checks if post IP is from VPN list or from office list, and if it is, then checks if it has not been used lately
	 * If check is successfull, redirection to ccbill takes place, if not it returns false
	 */
	public function checkip($url = NULL) {
		global $db;

//		reportAdmin("AS: checkip() please check log", "candidate for removal", array("url" => $url));

		if ($url)
			$_SESSION["ccbill_checkip_url"] = $url;

		if (!$_SESSION["ccbill_checkip_url"]) {
			//url of ccbill post is not in session, this should never happen
			file_log("ccbill", "ccbill::checkip(): Error: ccbill_checkip_url not in session ! user_agent='".$_SERVER["HTTP_USER_AGENT"]."'");
			reportAdmin("AS: ccbill::checkip(): Error - ccbill_checkip_url not in session", "", array());
			return false;
		}
		$url = $_SESSION["ccbill_checkip_url"];

		$ipaddress = account::getUserIp();

		$ip_vpn_or_office = false;
		$res = $db->q("SELECT id FROM vpn_server WHERE ip_address = ?", array($ipaddress));
		if ($db->numrows($res) > 0)
			$ip_vpn_or_office = true;

		if (!$ip_vpn_or_office) {
			$res = $db->q("SELECT value FROM admin_settings WHERE name = 'clad_poster_ip'");
			if ($row = $db->r($res)) {
				$arr = explode(",", $row["value"]);
				if (in_array($ipaddress,$arr))
					$ip_vpn_or_office = true;
			}
		}

		if (!$ip_vpn_or_office) {
			file_log("ccbill", "ccbill::checkip(): IP '{$ipaddress}' is not VPN nor office, redirecting to ccbill, url='{$url}'");
			system::go($url);	//ip is not vpn, nor office ip address, redirect to ccbill
			return true;
		}

		//ip is vpn or from office
		//check if this ip was not used in ccbill purchase in last period
		$stamp_period = time() - (86400 * 30);	//period is 30 days
		$res = $db->q("SELECT id, stamp FROM ccbill_post WHERE stamp > ? AND result = 'A' AND ip_address = ?", array($stamp_period, $ipaddress));
		if ($db->numrows($res) > 0) {
			$row = $db->r($res);
			file_log("ccbill", "ccbill::checkip(): IP '{$ipaddress}' was already used on ".date("Y-m-d", $row["stamp"])." (ccbill_post #{$row["id"]}), checkip() returning false");
			return false;
		}
		
		//ip was not recently used, redirect to ccbill
		file_log("ccbill", "ccbill::checkip(): IP '{$ipaddress}' is VPN or office, but was not recently used, redirecting to ccbill");
		system::go($url);
		return true;
	}

	public static function getCardTypeByName($name) {
		$type = "";
		switch (strtolower($name)) {
			case "visa": $type = "VI"; break;
			case "mastercard": $type = "MC"; break;
			case "discover": $type = "DI"; break;
			case "jcb": $type = "JC"; break;
			case "maestro": $type = "MA"; break;
			case "unknown": $type = NULL; break;
			default:
				reportAdmin("AS: ccbill:getCardTypeByName(): Unknown cardType", "cardType = '{$name}'", array("cardType" => $name));
				$type = "OT";
				break;
		}
		return $type;
	}

	public static function getCardNameByType($type) {
		$name = "";
		switch ($type) {
			case "VI": $name = "Visa"; break;
			case "MC": $name = "Mastercard"; break;
			case "DI": $name = "Discover"; break;
			case "JC": $name = "JCB"; break;
			case "MA": $name = "Maestro"; break;
			case NULL: $name = ""; break;
			default:
				reportAdmin("AS: ccbill:getCardNameByType(): Unknown cardType", "cardType = '{$type}'", array("type" => $type));
				$name = "Other";
				break;
		}
		return $name;
	}

	/**
	 * This is called by postback of CCBILL, we dont have user session !
	 */
	public function process($succeeded = false) {
		global $db;

		$system = new system();

		if ($_REQUEST["what"] == "ts") {
			//this is TS payment, redirect to TS
			file_log("ccbill", "ccbill::process(): PF: succeeded=".intval($succeeded).", request='".print_r($_REQUEST, true)."'");
			$post_data = $_REQUEST;
			$post_data["succeeded"] = intval($succeeded);
			$curl = new curl();
			$redirect_url = "http://www.tsescorts.com/p/payment/process.php";
			file_log("ccbill", "ccbill::process(): PF: redirecting to '{$redirect_url}'");
			$response = $curl->post($redirect_url, $post_data);
			echo $response;
			die;
		}

		//get our custom variables from postback
		$account_id = $_REQUEST["account_id"];
		$what = $_REQUEST["what"];
		$item_id = $_REQUEST["item_id"];
		$ccbill_post_id = $_REQUEST["ccbill_post_id"];
		$promo_code = $_REQUEST["promo_code"];
		$p2 = $_REQUEST["p2"];
		$p3 = $_REQUEST["p3"];
		$checksum = $_REQUEST["checksum"];

		file_log("ccbill", "ccbill::processed(): succeeded=".intval($succeeded).", account_id={$account_id}, what={$what}, item_id={$item_id}, ccbill_post_id={$ccbill_post_id}, promo_code={$promo_code}, checksum={$checksum}, p2={$p2}, p3={$p3}");

		//these custom variables need to be set!
		if (!$account_id || !$what || !$ccbill_post_id || !$checksum) {
			debug_log("ccbill:process(): Error: Custom variable(s) not set in postback! account_id={$account_id}, what={$what}, item_id={$item_id}, ccbill_post_id={$ccbill_post_id}, checksum={$checksum}");
			reportAdmin("AS: ccbill:process() Error - Custom variable(s) not set in postback", "", 
				array("account_id" => $account_id, "what" => $what, "item_id" => $item_id, "ccbill_post_id" => $ccbill_post_id, "checksum" => $checksum)
				);
			return false;
		}

		//check checksum if nobody tampered with our custom variables
		if (md5($account_id.$what.$item_id.$ccbill_post_id.$promo_code.$p2.$p3.$this->pepper) != $checksum) {
			debug_log("ccbill:process(): Error: Checksum does not match! account_id={$account_id}, what={$what}, item_id={$item_id}, ccbill_post_id={$ccbill_post_id}, checksum={$checksum}, p2={$p2}, p3={$p3}");
			reportAdmin("AS: ccbill:process() Error - Checksum does not match", "", 
				array("account_id" => $account_id, "what" => $what, "item_id" => $item_id, "ccbill_post_id" => $ccbill_post_id, "promo_code" => $promo_code, "p2" => $p2, "p3" => $p3, "pepper" => $this->pepper, "checksum" => $checksum)
				);
			return false;
		}

		//get initiator account
		$account = account::findOneById($account_id);
		if (!$account) {
			debug_log("ccbill:process(): Error: Can't find payment initiator account! account_id={$account_id}");
			reportAdmin("AS: ccbill:process() Error - Can't find payment initiator account", "", 
				array("account_id" => $account_id, "what" => $what, "item_id" => $item_id, "ccbill_post_id" => $ccbill_post_id, "checksum" => $checksum)
				);
			return false;
		}

		//get payment variables
		$email = $_REQUEST["email"];
		$firstname = $_REQUEST["customer_fname"];
		$lastname = $_REQUEST["customer_lname"];
		$address = $_REQUEST["address1"];
		$city = $_REQUEST["city"];
		$state = $_REQUEST["state"];
		$zipcode = $_REQUEST["zipcode"];
		$country = $_REQUEST["country"];
		$cardType = $_REQUEST["cardType"];
		$type = self::getCardTypeByName($cardType);
		$total = $_REQUEST["initialPrice"];
		$ip_address = $_REQUEST["ip_address"];
		$subscription_id = $_REQUEST["subscription_id"];

		file_log("ccbill", "ccbill::processed(): type={$type}, total={$total}, subscription_id={$subscription_id}");

		//check if this subscription_id was not already posted by CCBill ! (we had some multiple posts before) - to prevent multiple purchases
		$res = $db->q("SELECT result FROM ccbill_post WHERE id = ? LIMIT 1", array($ccbill_post_id));
		if (!$db->numrows($res)) {
			//did not find ccbil post in db, this should not happen
			debug_log("ccbill:process(): Error: Can't find ccbill post in DB! ccbill_post_id={$ccbill_post_id}");
			reportAdmin("AS: ccbill:process() Error - Can't find ccbill post in DB", "",
				array("account_id" => $account_id, "what" => $what, "item_id" => $item_id, "ccbill_post_id" => $ccbill_post_id, "checksum" => $checksum)
				);
			return false;
		}
		$row = $db->r($res);
		$result = $row["result"];
		if ($result == "D" && !$succeeded) {
			//result is already set, this is multiple denial post from ccbill
			//finish script
			file_log("ccbill", "ccbill::processed(): multiple denial");
			echo "Multiple denial";
			return true;
		}
		if ($result == "A" && $succeeded) {
			//result is already set, this is multiple succeeded post from ccbill
			//finish script
			file_log("ccbill", "ccbill::processed(): multiple succeeded");
			echo "Multiple succeeded";
			return true;
		}
		
		if (!$succeeded) {
			//payment failed
			$db->q("UPDATE ccbill_post SET response = ?, result = ? WHERE id = ? LIMIT 1", 
				array(json_encode($_REQUEST), 'D', $ccbill_post_id)
				);

			$db->q("INSERT INTO account_cc_fail (account_id, time, ccbill_post_id) values (?, NOW(), ?)", array($account_id, $ccbill_post_id));
			file_log("ccbill", "ccbill::processed(): not succeeded, inserted into account_cc_fail row #".$db->insertid());
			echo "OK";
			return true;
		}

		//payment succeeded
		$db->q("UPDATE ccbill_post SET response = ?, result = ?, ip_address = ?, subscription_id = ? WHERE id = ? LIMIT 1", 
				array(print_r($_REQUEST, true), 'A', $ip_address, $subscription_id, $ccbill_post_id)
				);

		//lookup CC in db
		$cc_id = NULL;
		$res = $db->q("SELECT cc_id	
						FROM account_cc	
						WHERE account_id = ? AND type = ? AND firstname = ? AND lastname = ? AND zipcode = ?
						ORDER BY cc_id DESC 
						LIMIT 1", 
						array($account_id, $type, $firstname, $lastname, $zipcode)
					);
		if ($db->numrows($res)) {
			$row = $db->r($res);
			$cc_id = $row["cc_id"];
		} else {
			$cre = new creditcard();
			$cre->setAccountId($account_id);
			$cre->setType($type);
			$cre->setFirstName($firstname);
			$cre->setLastName($lastname);
			$cre->setAddress($address);
			$cre->setZipcode($zipcode);
			$cre->setCity($city);
			$cre->setState($state);
			$cre->setCountry($country);
			$cre->setTransId($subscription_id);
			$cre->setTotal($total);
			debug_log("PAY: Going to add new CC: ty={$type}, fn={$firstname}, ln={$lastname}, address={$address}, ti={$subscription_id}");
			$ret = $cre->add();
			if ($ret) {
				$cc_id = $cre->getId();
			}
		}
		
		file_log("ccbill", "ccbill::processed(): succeeded, cc_id={$cc_id}");

		$db->q("INSERT INTO account_purchase
				(account_id, cc_id, `time`, `transaction_time`, ccbill_post_id, transaction_id, `what`, `total`, `saved`, `item_id`, `promo_code`) 
				values 
				(?, ?, curdate(), UNIX_TIMESTAMP(), ?, ?, ?, ?, ?, ?, ?)",
				array($account_id, $cc_id, $ccbill_post_id, $subscription_id, $what, $total, '', $item_id, $promo_code));

		$purchase_id = $db->insertid();

		file_log("ccbill", "ccbill::processed(): purchase_id={$purchase_id}");

		if ($this->what == "classifieds") {
			$db->q("INSERT INTO classifieds_stat (`time`, made) VALUES (curdate(), ?) on duplicate key update made = made + ?",
					array($total, $total)
					);
		}

		$this->purchaseSuccessful($what, $purchase_id);

		echo "OK";
		return true;
	}

	public static function purchaseSuccessful($what, $purchase_id) {
		global $db;

		file_log("ccbill", "ccbill::purchaseSuccessful(): what={$what}, purchase_id={$purchase_id}");

		if ($what == "classifieds") {
			$classifieds = new classifieds;
			$classifieds->purchaseSuccessful($purchase_id, "ccbill");
		} else if ($what == "businessowner") {
			$bo = new businessowner;
			$bo->purchaseSuccessful($purchase_id, "ccbill");
		} else if ($what == "advertise") {
			$adv = new advertise;
			$adv->purchaseSuccessful($purchase_id, "ccbill");
		} else if ($what == "vip") {
			$vip = new vip;
			$vip->purchaseSuccessful($purchase_id, "ccbill");
		} else {
			debug_log("Error: ccbill::purchaseSuccessful() Unknown what: what='{$what}'");
			reportAdmin("AS: Error in ccbill::purchaseSuccessful() Unknown what", "what='{$what}'", array("what" => $what, "purchase_id" => $purchase_id));
		}
		return false;
	}

	/**
	 * This is called when user after successful purchase get back on our website
	 * We need to find out what kind of purchase we have made and redirect user accordingly
	 */
	public function afterPurchase($succeeded = false, $real_purchase = true, $ccbill_post_id = NULL, $what = NULL, $item_id = NULL, $page = NULL, $p2 = NULL, $p3 = NULL) {
		global $db, $account, $ctx;

		$system = new system();

		if ($_SESSION["pf_what"] == "ts") {
			//this is TS payment, redirect to TS
			file_log("ccbill", "ccbill::afterPurchase(): PF: what='{$_SESSION["pf_what"]}', pf_payment_id='{$_SESSION["pf_payment_id"]}', succeeded=".intval($succeeded));
			$redirect_url = "http://www.tsescorts.com/p/payment/afterpurchase.php?succeeded=".intval($succeeded)."&payment_id=".$_SESSION["pf_payment_id"];
			file_log("ccbill", "ccbill::afterPurchase(): PF: redirecting to '{$redirect_url}'");
			system::go($redirect_url);
			die;
		}

		if ($ccbill_post_id == NULL)
			$ccbill_post_id = $_SESSION["ccbill_post_id"];
		if ($what == NULL)
			$what = $_SESSION["what"];
		if ($item_id == NULL)
			$item_id = $_SESSION["item_id"];
		if ($page == NULL)
			$page = $_SESSION["page"];
		
		$p2 = $_SESSION["p2"];
		$p3 = $_SESSION["p3"];

		file_log("ccbill", "ccbill::afterPurchase(): succeeded=".intval($succeeded).", ccbill_post_id={$ccbill_post_id}, what={$what}, item_id={$item_id}, page={$page}, p2={$p2}, p3={$p3}");

		$p21 = $p22 = $p23 = $p24 = "";
		if (strpos($p2, "|") !== false) {
			$arr = explode("|", $p2);
			$p21 = $arr[0];
			$p22 = $arr[1];
			$p23 = $arr[2];
			$p24 = $arr[3];
		}

		$p31 = $p32 = $p33 = "";
		if (strpos($p3, "|") !== false) {
			$arr = explode("|", $p3);
			$p31 = $arr[0];
			$p32 = $arr[1];
			$p33 = $arr[2];
		}

		if ($succeeded && $real_purchase) {

			if (!$ccbill_post_id && is_bot()) {
				file_log("ccbill", "ccbill::afterPurchase(): is_bot");
				die;
			}

			//check if we have correct account_purchase entry for these params
			$res = $db->q("SELECT * FROM account_purchase WHERE `what` = ? AND ccbill_post_id = ?", [$what, $ccbill_post_id]);
			if ($db->numrows($res) != 1) {
				debug_log("ccbill:afterPurchase(): Error: Cant find purchase ! account_id={$account->getId()}, what={$what}, ccbill_post_id={$ccbill_post_id}");
				if ((empty($_SERVER["HTTP_REFERER"])) || (strpos($_SERVER["HTTP_REFERER"], "ccbill") !== false))
					reportAdmin("AS: ccbill:afterPurchase(): Error: Cant find purchase", "", 
						array("account_id" => $account->getId(), "what" => $what, "ccbill_post_id" => $ccbill_post_id, "p2" => $p2, "p3" => $p3)
						);
				$system->go("/");
			}
			$row = $db->r($res);
			file_log("ccbill", "ccbill::afterPurchase(): purchase_id=".$row["id"]);
		}

		if ($what == "classifieds") {

			if ($p2 == "hotleads") {
				//this purchase was made from hotleads website
				file_log("ccbill", "ccbill::afterPurchase(): hotleads");
				$clad = clad::findOneById($item_id);

				//if ad was BP ad, remove this flag
				if ($clad->getBp())
					$res = $db->q("UPDATE classifieds SET bp = 0 WHERE id = ?", [$clad_id]);

				//redirect back to hotleads site
				$data = [
					"as_clad_id" => $item_id,
					"as_status" => "live",
					"as_url" => $clad->getUrl(),
					];
				$redirect_url = "https://hotleads.com/after-purchase?".http_build_query($data);
				return system::go($redirect_url);

			} else if ($p2 == "adbuild") {

				if ($succeeded) {
					$clad = clad::findOneById($item_id);
					if (!$clad) {
						debug_log("ccbill:afterPurchase(): Error: Cant find clad ! account_id={$account->getId()}, what={$what}, ccbill_post_id={$ccbill_post_id}, item_id={$item_id}");
						reportAdmin("AS: ccbill:afterPurchase(): Error: Cant find clad", "", 
							array("account_id" => $account->getId(), "what" => $what, "ccbill_post_id" => $ccbill_post_id, "item_id" => $item_id, "p2" => $p2, "p3" => $p3)
						);
						$system->go("/");
					}

					$system->moved("/adbuild/step5?ad_id={$item_id}");
					
					die;
				} else {
					$system->go("/adbuild/step4?ad_id={$item_id}&denied=1");
					die;
				}

			} else if ($p21 == "side") {

				if ($succeeded)
					$system->go("/classifieds/myposts", "you have successfully purchase side sponsor for your ad.");
				else
					$system->go("/classifieds/myposts", "you have failed to purchase side sponsor for your ad.");

			} else if ($p21 == "city_thumbnail") {

				if ($succeeded)
					$system->go("/classifieds/myposts", "You have successfully purchased city thumbnail for your ad.");
				else
					$system->go("/classifieds/myposts", "You have failed to purchase city thumbnail for your ad.");

			} else if ($p21 == "upgrade") {

				if ($succeeded)
					$system->go("/classifieds/myposts", "You have successfully purchased reposts for your ad.");
				else
					$system->go("/classifieds/myposts", "You have failed to purchase reposts for your ad.");

			} else if ($p21 == "recurring") {

				if ($succeeded)
					$system->go("/classifieds/myposts", "You have successfully purchased recurring reposting for your ad.");
				else
					$system->go("/classifieds/myposts", "You have failed to purchase recurring reposting for your ad.");

			} else {
				file_log("classifieds", "classifieds::purchaseSuccessful(): Error: Unknown p2='{$p2}' !");
				reportAdmin("AS: classifieds::purchaseSuccessful(): Error: Unknown p2", "P2='{$p2}'",
					array("account_id" => $acc->getId(), "purchase_id" => $purchase_id, "p2" => $p2)
				);
			}

		} else if ($what == "advertise") {

			if ($succeeded) {
				$system->go("/advertise/", "Thank You!, Your funds have been added to your account.");
				die;
			} else {
				$system->go("/advertise/addfunds", "Error: Your transaction has been denied. Please check the payment details and try again.");
				die;
			}

		} else if ($what == "businessowner") {

			if ($succeeded) {
				switch($p31) {
					case 'sc':
						$system->go("http://{$ctx->domain_base}/strip-clubs/club?id=".$p2, "Thank you for your payment. Your business will be upgraded in next 5 minutes.");
						break;
					case 'emp':
						$system->go("http://{$ctx->domain_base}/erotic-massage/parlor?id=".$p2, "Thank you for your payment. Your business will be upgraded in next 5 minutes.");
						break;
					case 'as':
						$system->go("http://{$ctx->domain_base}/sex-shops/store?id=".$p2, "Thank you for your payment. Your business will be upgraded in next 5 minutes.");
						break;
					case 'lingerie':
						$system->go("http://{$ctx->domain_base}/lingerie-modeling-1on1/store?id=".$p2, "Thank you for your payment. Your business will be upgraded in next 5 minutes.");
						break;
				}
			}
			$system->go("/");

		} else if ($what == "vip") {

			if ($succeeded) {
				$system->go("/", "Thank You!, You are VIP member now.");
				die;
			} else {
				$system->go("/vip/step2", "Error: Your transaction has been denied. Please check the payment details and try again.");
				die;
			}

		} else {
			debug_log("ccbill:afterPurchase(): Error: Unknown what '{$what}' ! ccbill_post_id={$ccbill_post_id}, item_id={$item_id}");
			reportAdmin("AS: ccbill:afterPurchase(): Error: Unknown what", "", 
				array("account_id" => $account->getId(), "what" => $what, "ccbill_post_id" => $ccbill_post_id, "item_id" => $item_id, "p2" => $p2, "p3" => $p3)
				);
			$system->go("/");
		}

		//default action after not succeeding
		reportAdmin("AS: ccbill::afterPurchase(): no action for not succeeding", "", 
			array("account_id" => $account->getId(), "what" => $what, "ccbill_post_id" => $ccbill_post_id, "item_id" => $item_id, "p2" => $p2, "p3" => $p3));

		$system->go("/");
	}

	public function updatecc($cc_id) {
		return false;
	}

	public function auth($params, &$error = NULL) {
		return false;
	}

	public static function refundByPurchaseId($purchase_id, $type = "R", $reason = "", $amount = NULL) {
		global $db, $account;

		$res = $db->q("SELECT ap.*
						FROM account_purchase ap
						WHERE ap.id = ?
						LIMIT 1",
						array($purchase_id));
		if ($db->numrows($res) != 1) {
			return false;
		}

		$row = $db->r($res);
		$transaction_id = $row["transaction_id"];
		$ccbill_post_id = $row["ccbill_post_id"];
		$total = $row["total"];
		$email = $row["email"];
		$account_id = $row["account_id"];
		$cc_id = $row["cc_id"];
		$item_id = $row["item_id"];
		if (!$transaction_id) {
			//this is probably historic purchase, where we dont have transaction id
			//fail now
			return false;
		}

		if (!$amount)
			$amount = $total;
		$new_total = number_format($total - $amount, 2, ".", "");
		if ($total < 0.01) {
			//either this was zero purchase (should not happen ?) - and there is nothing to refund
			//or this was already refunded
			//do not refund
			return false;
		}
		if ($new_total < 0)
			$new_total = 0;

		//update account_purchase table
		$db->q("UPDATE account_purchase SET total = ? WHERE id = ? LIMIT 1", array($new_total, $purchase_id));
		if ($db->affected() != 1) {
			//this should not happen
			reportAdmin("AS: ccbill::refund : cant update original purchase", "",
				array("purchase_id" => $purchase_id));
		}

		//insert refund into account_refund table
		$now = time();
		$now_datetime = date("Y-m-d H:i:s", $now);
		$db->q("INSERT INTO account_refund 
				(type, transaction_time, author_id, ccbill_post_id, account_id, `time`, cc_id, purchase_id, item_id, amount, reason)
				VALUES
				(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
				array($type, $now, $account->getId(), $ccbill_post_id, $account_id, $now_datetime, $cc_id, $purchase_id, $item_id, $amount, $reason));
		$id = $db->insertid();
		if (!$id) {
			//this should not happen
			reportAdmin("AS: ccbill::refund : cant insert into account_refund", "",
				array("purchase_id" => $purchase_id));
		}

		return true;
	}

	public function refund($total, $cre) {
		return false;
	}

	public static function webhook() {
		global $db;

		//first check if we have referred subscription_id in db, if not then it is probably TS transaction
		$subscription_id = $_REQUEST["subscriptionId"];
		if (!$subscription_id) {
			file_log("ccbill", "ccbill::webhook : subscription_id empty ?!, request='".print_r($_REQUEST, true)."'");
			reportAdmin("AS: ccbill::webhook: subscription_id empty ?!", "", $_REQUEST);
			die("Error");
		}
		$res = $db->q("SELECT id FROM ccbill_post WHERE subscription_id = ? LIMIT 1", [$subscription_id]);
		if ($db->numrows($res) == 0) {
			//no such subscription in AS db, redirect webhook to TS, lets cross our fingers it is TS transaction
			file_log("ccbill", "ccbill::webhook(): PF: request='".print_r($_REQUEST, true)."'");
			$post_data = $_REQUEST;
			$curl = new curl();
			$redirect_url = "http://www.tsescorts.com/p/payment/webhook.php";
			file_log("ccbill", "ccbill::webhook(): PF: redirecting to '{$redirect_url}'");
			$response = $curl->post($redirect_url, $post_data);
			echo $response;
			die;
		}

		$event_type = $_REQUEST["eventType"];

		if ($event_type == "RenewalSuccess") {
			$orig_transaction_id = $_REQUEST["subscriptionId"];
			$amount = $_REQUEST["billedAmount"];
			$transaction_id = $_REQUEST["transactionId"];
			$timestamp = strtotime($_REQUEST["timestamp"]);
			$date = date("Y-m-d", $timestamp);
			$next_renewal_date = $_REQUEST["nextRenewalDate"];

			//find account_id / clad_id by original transaction_id
			$res = $db->q("SELECT ap.id, ap.account_id, ap.cc_id, ap.item_id 
							FROM account_purchase ap
							WHERE ap.transaction_id = ? AND ap.total > 0",
							array($orig_transaction_id));
			if ($db->numrows($res) != 1) {
				//this should not happen, notify admin
				file_log("ccbill", "ccbill::webhook : RenewalSuccess : Can't find original transaction, orig_transaction_id='{$orig_transaction_id}'");
				reportAdmin("AS: ccbill::webhook : RenewalSuccess : Can't find original transaction", "", 
					array("orig_transaction_id" => $orig_transaction_id, "amount" => $amount, "transaction_id" => $transaction_id, "timestamp" => $timestamp, "next_renewal_date" => $next_renewal_date)
					);
				return false;
			}
			$row = $db->r($res);
			$account_id = $row["account_id"];
			$cc_id = $row["cc_id"];
			$item_id = $row["item_id"];

			//create new ccbill post 
			$res = $db->q("INSERT INTO ccbill_post
							(stamp, p1, p2, p3, post, response, result, subscription_id)
							VALUES
							(?, ?, ?, ?, ?, ?, ?, ?)",
							array($timestamp, "classifieds", "renewal", $item_id, NULL, json_encode($_REQUEST), "A", $transaction_id)
							);
			$ccbill_post_id = $db->insertid();
			if (!$ccbill_post_id) {
				//this should never happen
				file_log("ccbill", "ccbill::webhook : RenewalSuccess : Can't insert ccbill post, orig_transaction_id='{$orig_transaction_id}', item_id = '{$item_id}'");
				reportAdmin("AS: ccbill::webhook : RenewalSuccess : Can't insert ccbill post", "", 
					array("orig_transaction_id" => $orig_transaction_id, "amount" => $amount, "transaction_id" => $transaction_id, "timestamp" => $timestamp, "next_renewal_date" => $next_renewal_date, "account_id" => $account_id, "cc_id" => $cc_id, "item_id" => $item_id)
					);
				return false;
			}

			//create purchase
			$res = $db->q("INSERT INTO account_purchase
							(account_id, cc_id, time, transaction_time, ccbill_post_id, transaction_id, what, total, item_id)
							VALUES
							(?, ?, ?, ?, ?, ?, ?, ?, ?)",
							array($account_id, $cc_id, $date, $timestamp, $ccbill_post_id, $transaction_id, 'classifieds', $amount, $item_id)
							);
			$purchase_id = $db->insertid();
			if (!$purchase_id) {
				//this should never happen
				file_log("ccbill", "ccbill::webhook : RenewalSuccess : Can't insert purchase, orig_transaction_id='{$orig_transaction_id}', account_id='{$account_id}', date='{$date}', timestamp='{$timestamp}', ccbill_post_id='{$ccbill_post_id}', transaction_id='{$transaction_id}', total='{$amount}', item_id = '{$item_id}'");
				reportAdmin("AS: ccbill::webhook : RenewalSuccess : Can't insert purchase", "", 
							array("orig_transaction_id" => $orig_transaction_id, "amount" => $amount, "transaction_id" => $transaction_id, "timestamp" => $timestamp, "date" => $date, "next_renewal_date" => $next_renewal_date, "account_id" => $account_id, "cc_id" => $cc_id, "item_id" => $item_id, "ccbill_post_id" => $ccbill_post_id)
							);
				return false;
			}

			//everything good, run classifieds hook
			$classifieds = new classifieds;
			$classifieds->purchaseSuccessful($purchase_id, "ccbill");

			//TODO to be removed after checking its working
			reportAdmin("AS: ccbill::webhook : RenewalSuccess : Check automatic renewal", "Candidate for removal", array("orig_transaction_id" => $orig_transaction_id, "amount" => $amount, "transaction_id" => $transaction_id, "timestamp" => $timestamp, "date" => $date, "next_renewal_date" => $next_renewal_date, "account_id" => $account_id, "cc_id" => $cc_id, "item_id" => $item_id, "ccbill_post_id" => $ccbill_post_id));
			return true;

		} else if ($event_type == "RenewalFailure") {
			$orig_transaction_id = $_REQUEST["subscriptionId"];
			$transaction_id = $_REQUEST["transactionId"];
			$timestamp = strtotime($_REQUEST["timestamp"]);
			$date = date("Y-m-d", $timestamp);
			$next_retry_date = $_REQUEST["nextRetryDate"];

			//find account_id / clad_id by original transaction_id
			$res = $db->q("SELECT ap.id, ap.account_id, ap.cc_id, ap.item_id 
							FROM account_purchase ap
							WHERE ap.transaction_id = ? AND ap.total > 0",
							array($orig_transaction_id));
			if ($db->numrows($res) != 1) {
				//this should not happen, notify admin
				file_log("ccbill", "ccbill::webhook : RenewalFailure : Can't find original transaction, orig_transaction_id='{$orig_transaction_id}'");
				reportAdmin("AS: ccbill::webhook : RenewalFailure : Can't find original transaction", "", 
					array("orig_transaction_id" => $orig_transaction_id, "transaction_id" => $transaction_id, "timestamp" => $timestamp, "next_retry_date" => $next_retry_date)
					);
				return false;
			}
			$row = $db->r($res);
			$account_id = $row["account_id"];
			$cc_id = $row["cc_id"];
			$item_id = $row["item_id"];

			//create new ccbill post 
			$res = $db->q("INSERT INTO ccbill_post
							(stamp, p1, p2, p3, post, response, result, subscription_id)
							VALUES
							(?, ?, ?, ?, ?, ?, ?, ?)",
							array($timestamp, "classifieds", "renewal", $item_id, NULL, json_encode($_REQUEST), "D", $transaction_id)
							);
			$ccbill_post_id = $db->insertid();
			if (!$ccbill_post_id) {
				//this should never happen
				file_log("ccbill", "ccbill::webhook : RenewalFailure : Can't insert ccbill post, orig_transaction_id='{$orig_transaction_id}', item_id = '{$item_id}'");
				reportAdmin("AS: ccbill::webhook : RenewalFailure : Can't insert ccbill post", "", 
					array("orig_transaction_id" => $orig_transaction_id, "transaction_id" => $transaction_id, "timestamp" => $timestamp, "next_retry_date" => $next_retry_date, "account_id" => $account_id, "cc_id" => $cc_id, "item_id" => $item_id)
					);
				return false;
			}

			//create failed purchase
			$db->q("INSERT INTO account_cc_fail (account_id, time, ccbill_post_id) values (?, NOW(), ?)", array($account_id, $ccbill_post_id));
			$fail_id = $db->insertid();
			if (!$fail_id) {
				//this should never happen
				file_log("ccbill", "ccbill::webhook : RenewalFailure : Can't insert failed purchase, orig_transaction_id='{$orig_transaction_id}', account_id='{$account_id}', date='{$date}', timestamp='{$timestamp}', ccbill_post_id='{$ccbill_post_id}', transaction_id='{$transaction_id}', item_id = '{$item_id}'");
				reportAdmin("AS: ccbill::webhook : RenewalFailure : Can't insert purchase", "", 
							array("orig_transaction_id" => $orig_transaction_id, "transaction_id" => $transaction_id, "timestamp" => $timestamp, "date" => $date, "next_retry_date" => $next_retry_date, "account_id" => $account_id, "cc_id" => $cc_id, "item_id" => $item_id, "ccbill_post_id" => $ccbill_post_id)
							);
				return false;
			}

			//everything good, run classifieds hook
//			$classifieds = new classifieds;
//		  $classifieds->purchaseSuccessful($purchase_id, "ccbill");

			return true;

		} else if ($event_type == "Refund" || $event_type == "Void") {
			$amount = $_REQUEST["amount"];
			//$transaction_id = $_REQUEST["subscriptionId"];	//in case of recurring payment, this subscription id is initial transaction
			$transaction_id = $_REQUEST["transactionId"];		//this is actual transaction that is being refunded
			//make sure we have this refund in database
			$res = $db->q("SELECT ar.id 
							FROM account_refund ar 
							INNER JOIN account_purchase ap on ap.id = ar.purchase_id
							WHERE ar.amount = ? AND ap.transaction_id = ?", 
							array($amount, $transaction_id)
							);
			if ($db->numrows($res) != 1) {
				//this should theoretically not happen, as we do refunds from AS
				file_log("ccbill", "ccbill::webhook : Refund : Can't find refund for amount '{$amount}' and transaction_id = '{$transaction_id}', numrows=".$db->numrows($res));
				reportAdmin("AS: Unexpected Void/Refund", "Most probably client voided/refunded directly through CCBILL, please add this refund manually to AS system: <a href=\"http://adultsearch.com/mng/sales?transaction_id={$transaction_id}\">view transaction {$transaction_id}</a><br />Thanks.", array("event_type" => $event_type, "amount" => $amount, "transaction_id" => $transaction_id));
				return false;
			}
			return true;

		} else if ($event_type == "Chargeback") {

			$amount = $_REQUEST["amount"];
			$transaction_id = $_REQUEST["subscriptionId"];
			//fetch purchase and check if we have this chargeback in database
			$res = $db->q("SELECT ap.id as purchase_id, ap.item_id as clad_id, ap.account_id, ar.id as refund_id
							FROM account_purchase ap
							LEFT JOIN account_refund ar on ap.id = ar.purchase_id
							WHERE ap.transaction_id = ?", 
							array($transaction_id)
							);
			if ($db->numrows($res) != 1) {
				file_log("ccbill", "ccbill::webhook : Chargeback: Can't find purchase in db: transaction_id='{$transaction_id}', numrows=".$db->numrows($res));
				reportAdmin("AS: ccbill::webhook : Chargeback: Can't find purchase in db", "", array("amount" => $amount, "transaction_id" => $transaction_id));
				return false;
			}
			$row = $db->r($res);
			$purchase_id = $row["purchase_id"];
			$refund_id = $row["refund_id"];
			$clad_id = $row["clad_id"];
			$account_id = $row["account_id"];

			if (!$refund_id) {
				//this is normal, chargeback came from ccbill without expectation
				//do chargeback
				$ret = self::refundByPurchaseId($purchase_id, "CH", "");
				if (!$ret) {
					file_log("ccbill", "ccbill::webhook : Chargeback: Can't insert chargeback into db: transaction_id='{$transaction_id}', numrows=".$db->numrows($res));
					reportAdmin("AS: ccbill::webhook : Chargeback: Can't insert chargeback into db", "", array("amount" => $amount, "transaction_id" => $transaction_id));
					return false;
				}
			}

			//make sure ad is deleted and user is banned and not whitelisted
			$db->q("UPDATE classifieds SET deleted = ? WHERE id = ? AND deleted IS NULL LIMIT 1", array(time(), $clad_id));

			//$db->q("UPDATE account SET banned = 1, whitelisted = 0 WHERE account_id = ? LIMIT 1", array($account_id));
			$acc = account::findOneById($account_id);
			if (!$acc) {
				file_log("ccbill", "ccbill::webhook : Chargeback: Can't find account by id: account_id={$account_id}");
				reportAdmin("AS: ccbill::webhook : Chargeback: Can't find account by id", "", array("account_id" => $account_id));
				return false;
			}
			$acc->setWhitelisted(0);
			$acc->setNotes($acc->getNotes()."\n[".date("Y-m-d H:i:s T")."] - User banned because he did chargeback for ad #{$clad_id}");
			$acc->update();
			$acc->ban();
			
			return true;

		} else if ($event_type == "Expiration") {
			$transaction_id = $_REQUEST["subscriptionId"];
			//fetch purchase
			$res = $db->q("SELECT ap.id, ap.what FROM account_purchase ap WHERE ap.transaction_id = ?", array($transaction_id));
			if ($db->numrows($res) != 1) {
				//this should not happen
				file_log("ccbill", "ccbill::webhook : Expiration : Can't find purchase: transacation_id = '{$transaction_id}', numrows=".$db->numrows($res));
				reportAdmin("AS: ccbill::webhook : Expiration : Can't find purchase", "", array("transaction_id" => $transaction_id));
				return false;
			}
			$row = $db->r($res);
			$purchase_id = $row["id"];
			$what = $row["what"];

			if ($what == "advertise")
				return true;		//advertise purchases are not really subscriptions, we dont care about any expiration

			if ($what == "classifieds")
				return classifieds::expiration($purchase_id);
			
			reportAdmin("AS: ccbill::webhook : Expiration : Unknown what", "", array("transaction_id" => $transaction_id, "purchase_id" => $purchase_id, "what" => $what));
			return false;

		} else if ($event_type == "Cancellation") {
			$transaction_id = $_REQUEST["subscriptionId"];
			$reason = $_REQUEST["reason"];

			//fetch purchase
			$res = $db->q("SELECT ap.id, ap.what FROM account_purchase ap WHERE ap.transaction_id = ?", array($transaction_id));
			if ($db->numrows($res) != 1) {
				//this should not happen
				file_log("ccbill", "ccbill::webhook : Cancellation : Can't find purchase: transacation_id = '{$transaction_id}', numrows=".$db->numrows($res));
				reportAdmin("AS: ccbill::webhook : Cancellation : Can't find purchase", "", array("transaction_id" => $transaction_id));
				return false;
			}
			$row = $db->r($res);
			$purchase_id = $row["id"];
			$what = $row["what"];
			if ($what == "classifieds")
				return classifieds::cancellation($purchase_id, $reason);
			
			reportAdmin("AS: ccbill::webhook : Cancellation : Unknown what", "", array("transaction_id" => $transaction_id, "purchase_id" => $purchase_id, "what" => $what));
			return false;

		} else {
			reportAdmin("AS: ccbill::webhook : Unknown type of event", "EventType='{$event_type}'", $_REQUEST);
			return false;
		}

		//we should never get here
		return false;
	}

}

//END
