<?php
/*
 * clad = classified ad
 */
class clad {

	private $id = NULL,
			$type = NULL,
			$account_id = NULL,
			$title = NULL,
			$date = NULL,
			$thumb = NULL,
			$multiple_thumbs = NULL,
			$phone = NULL,
			$email = NULL,
			$website = NULL,
			$ter = NULL,
			$reply = NULL,
			$cc_id = NULL,
			$done = NULL,
			$expires = NULL,
			$expire_stamp = NULL,
			$bp = NULL,
			$hash = NULL,
			$deleted = NULL,
			$allowlinks = NULL,
			$content = NULL,
			$created = NULL,
			$sponsor = NULL,
			$sponsor_mobile = NULL,
			$sponsor_position = NULL,
			$auto_repost = NULL,
			$auto_renew_fr = NULL,
			$auto_renew_time = NULL,
			$time_next = NULL,
			$ethnicity = NULL,
			$promo = NULL,
			$promo_code = NULL,
			$hit = NULL,
			$ter_check_stamp = NULL,
			$ter_check_unapproved = NULL,
			$firstname = NULL,
			$lastname = NULL,
			$eccie_reviews_link = NULL,
			$direct_link = NULL;

	private $account = NULL;
	private $url = NULL;
	private $locations = NULL;
	private $loc_labels = NULL;
	private $total_payment = NULL;

	function __construct() {
	}

	public static function findOneById($id) {
		global $db;

		$res = $db->q("SELECT * FROM classifieds WHERE id = '".intval($id)."'");
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

		return self::withRow($row);
	}
	
	public static function withRow($row) {

		$clad = new clad();
		$clad->setId($row["id"]);
		$clad->setType($row["type"]);
		$clad->setAccountId($row["account_id"]);
		$clad->setTitle($row["title"]);
		$clad->setDate($row["date"]);
		$clad->setThumb($row["thumb"]);
		$clad->setMultipleThumbs($row["multiple_thumbs"]);
		$clad->setPhone($row["phone"]);
		$clad->setEmail($row["email"]);
		$clad->setWebsite($row["website"]);
		$clad->setTer($row["ter"]);
		$clad->setReply($row["reply"]);
		$clad->setCcId($row["cc_id"]);
		$clad->setDone($row["done"]);
		$clad->setExpires($row["expires"]);
		$clad->setExpireStamp($row["expire_stamp"]);
		$clad->setBp($row["bp"]);
		$clad->setHash($row["hash"]);
		$clad->setDeleted($row["deleted"]);
		$clad->setAllowlinks($row["allowlinks"]);
		$clad->setContent($row["content"]);
		$clad->setCreated($row["created"]);
		$clad->setSponsor($row["sponsor"]);
		$clad->setSponsorMobile($row["sponsor_mobile"]);
		$clad->setSponsorPosition($row["sponsor_position"]);
		$clad->setAutoRepost($row["auto_repost"]);
		$clad->setAutoRenewFr($row["auto_renew_fr"]);
		$clad->setAutoRenewTime($row["auto_renew_time"]);
		$clad->setTimeNext($row["time_next"]);
		$clad->setEthnicity($row["ethnicity"]);
		$clad->setPromo($row["promo"]);
		$clad->setPromoCode($row["promo_code"]);
		$clad->setHit($row["hit"]);
		$clad->setTerCheckStamp($row["ter_check_stamp"]);
		$clad->setTerCheckUnapproved($row["ter_check_unapproved"]);
		$clad->setFirstname($row["firstname"]);
		$clad->setLastname($row["lastname"]);
		$clad->setEccieReviewsLink($row["ECCASapi_revsearch_url"]);
		$clad->setDirectLink($row["direct_link"]);

		return $clad;
	}
	
	public static function findAllByIds($ids_array) {
		global $db;

		//make sure we have only integers in array
		$ids = array();
		foreach($ids_array as $id) {
			$id2 = intval($id);
			if ($id2 != 0)
				$ids[] = $id2;
		}

		$clads = array();
		$res = $db->q("SELECT * FROM classifieds c WHERE c.id IN (".implode(",",$ids).")");
		while ($row = $db->r($res)) {
			$clad = self::withRow($row);
			if (!$clad)
				continue;
			$clads[] = $clad;
		}
		return $clads;
	}

	//getters and setters
	public function getId() {
		return $this->id;
	}
	public function getType() {
		return $this->type;
	}
	public function getAccountId() {
		return $this->account_id;
	}
	public function getTitle() {
		return $this->title;
	}
	public function getDate() {
		return $this->date;
	}
	public function getThumb() {
		return $this->thumb;
	}
	public function getMultipleThumbs() {
		return $this->multiple_thumbs;
	}
	public function getPhone() {
		return $this->phone;
	}
	public function getEmail() {
		return $this->email;
	}
	public function getWebsite() {
		return $this->website;
	}
	public function getTer() {
		return $this->ter;
	}
	public function getReply() {
		return $this->reply;
	}
	public function getCcId() {
		return $this->cc_id;
	}
	public function getDone() {
		return $this->done;
	}
	public function getExpires() {
		return $this->expires;
	}
	public function getExpireStamp() {
		return $this->expire_stamp;
	}
	public function getBp() {
		return $this->bp;
	}
	public function getHash() {
		return $this->hash;
	}
	public function getDeleted() {
		return $this->deleted;
	}
	public function getAllowlinks() {
		return $this->allowlinks;
	}
	public function getContent() {
		return $this->content;
	}
	public function getCreated() {
		return $this->created;
	}
	public function getSponsor() {
		return $this->sponsor;
	}
	public function getSponsorMobile() {
		return $this->sponsor_mobile;
	}
	public function getSponsorPosition() {
		return $this->sponsor_position;
	}
	public function getAutoRepost() {
		return $this->auto_repost;
	}
	public function getAutoRenewFr() {
		return $this->auto_renew_fr;
	}
	public function getAutoRenewTime() {
		return $this->auto_renew_time;
	}
	public function getTimeNext() {
		return $this->time_next;
	}
	public function getEthnicity() {
		return $this->ethnicity;
	}
	public function getPromo() {
		return $this->promo;
	}
	public function getPromoCode() {
		return $this->promo_code;
	}
	public function getHit() {
		return $this->hit;
	}
	public function getTerCheckStamp() {
		return $this->ter_check_stamp;
	}
	public function getTerCheckUnapproved() {
		return $this->ter_check_unapproved;
	}
	public function getFirstname() {
		return $this->firstname;
	}
	public function getLastname() {
		return $this->lastname;
	}
	public function getEccieReviewsLink() {
		return $this->eccie_reviews_link;
	}
	public function getDirectLink() {
		return $this->direct_link;
	}


	public function setId($id) {
		$this->id = $id;
	}
	public function setType($type) {
		$this->type = $type;
	}
	public function setAccountId($account_id) {
		$this->account_id = $account_id;
	}
	public function setTitle($title) {
		$this->title = $title;
	}
	public function setDate($date) {
		$this->date = $date;
	}
	public function setThumb($thumb) {
		$this->thumb = $thumb;
	}
	public function setMultipleThumbs($multiple_thumbs) {
		$this->multiple_thumbs = $multiple_thumbs;
	}
	public function setPhone($phone) {
		$this->phone = $phone;
	}
	public function setEmail($email) {
		$this->email = $email;
	}
	public function setWebsite($website) {
		$this->website = $website;
	}
	public function setTer($ter) {
		$this->ter = $ter;
	}
	public function setReply($reply) {
		$this->reply = $reply;
	}
	public function setCcId($cc_id) {
		$this->cc_id = $cc_id;
	}
	public function setDone($done) {
		$this->done = $done;
	}
	public function setExpires($expires) {
		$this->expires = $expires;
	}
	public function setExpireStamp($expire_stamp) {
		$this->expire_stamp = $expire_stamp;
	}
	public function setBp($bp) {
		$this->bp = $bp;
	}
	public function setHash($hash) {
		$this->hash = $hash;
	}
	public function setDeleted($deleted) {
		$this->deleted = $deleted;
	}
	public function setAllowlinks($allowlinks) {
		$this->allowlinks = $allowlinks;
	}
	public function setContent($content) {
		$this->content = $content;
	}
	public function setCreated($created) {
		$this->created = $created;
	}
	public function setSponsor($sponsor) {
		$this->sponsor = $sponsor;
	}
	public function setSponsorMobile($sponsor_mobile) {
		$this->sponsor_mobile = $sponsor_mobile;
	}
	public function setSponsorPosition($sponsor_position) {
		$this->sponsor_position = $sponsor_position;
	}
	public function setAutoRepost($auto_repost) {
		$this->auto_repost = $auto_repost;
	}
	public function setAutoRenewFr($auto_renew_fr) {
		$this->auto_renew_fr = $auto_renew_fr;
	}
	public function setAutoRenewTime($auto_renew_time) {
		$this->auto_renew_time = $auto_renew_time;
	}
	public function setTimeNext($time_next) {
		$this->time_next = $time_next;
	}
	public function setEthnicity($ethnicity) {
		$this->ethnicity = $ethnicity;
	}
	public function setPromo($promo) {
		$this->promo = $promo;
	}
	public function setPromoCode($promo_code) {
		$this->promo_code = $promo_code;
	}
	public function setHit($hit) {
		$this->hit = $hit;
	}
	public function setTerCheckStamp($ter_check_stamp) {
		$this->ter_check_stamp = $ter_check_stamp;
	}
	public function setTerCheckUnapproved($ter_check_unapproved) {
		$this->ter_check_unapproved = $ter_check_unapproved;
	}
	public function setFirstname($firstname) {
		$this->firstname = $firstname;
	}
	public function setLastname($lastname) {
		$this->lastname = $lastname;
	}
	public function setEccieReviewsLink($eccie_reviews_link) {
		$this->eccie_reviews_link = $eccie_reviews_link;
	}
	public function setDirectLink($direct_link) {
		$this->direct_link = $direct_link;
	}

	//relationships
	public function setAccount($acc) {
		$this->account = $acc;
		return true;
	}
	public function getAccount() {
		if ($this->account)
			return $this->account;
		$acc = account::findOneById($this->account_id);
		if (!$acc)
			return NULL;
		$this->account = $acc;
		return $this->account;
	}	

	public function getThumbFilepath() {
		global $config_image_path;
		return "{$config_image_path}/classifieds/".$this->getThumb();
	}
	public function getThumbUrl() {
		global $config_image_server;
		return $config_image_server . '/classifieds/'.$this->getThumb();
	}
	public function getMultipleThumbsUrls() {
		global $config_image_server;
		$urls = [];
		if ($this->getMultipleThumbs()) {
			$arr = explode(",", $this->getMultipleThumbs());
			foreach ($arr as $thumb_filename)
				$urls[] = $config_image_server . '/classifieds/'.$thumb_filename;
		}
		if (empty($urls) && $this->getThumb())
			$urls[] = $config_image_server . '/classifieds/'.$this->getThumb();
		return $urls;
	}

	public function getImages() {
		global $db;

		$images = array();
		$res = $db->q("SELECT image_id, filename FROM classifieds_image WHERE id = ? ORDER BY image_id ASC", array($this->id));
		while ($row = $db->r($res)) {
			$images[$row["image_id"]] = $row["filename"];
		}
		return $images;
	}

	public function getVideos() {
		global $db;

		$videos = array();
		$res = $db->q("SELECT thumbnail, filename , width, height, converted, FROM_UNIXTIME(created_stamp) AS created
					   FROM classified_video WHERE classified_id = ? ORDER BY created_stamp DESC", array($this->id));
		while ($row = $db->r($res)) {
			$row['thumbnail_uri'] = vid::thumbnailUri($row['thumbnail'], true);
			$row['filename_uri'] = vid::videoUri($row['filename'], true);
			$videos[] = $row;
		}
		return $videos;
	}

	/**
	 * Returns URL to classified ad, loc_id is preferred location (ad can be posted in multiple locations)
	 */
	public function getUrl($loc_id = NULL) {
		global $db;

		if ($this->url && $loc_id == NULL)
			return $this->url;

		if ($loc_id == NULL) {
			//TODO - not working for international locations ?
			$res = $db->q("SELECT loc_id FROM classifieds_loc WHERE post_id = ? ORDER BY done LIMIT 1", array($this->id));
			if (!$db->numrows($res))
				return false;
			$row = $db->r($res);
			$loc_id = $row["loc_id"];
		}
		$loc = location::findOneById($loc_id);
		if (!$loc)
			return false;
		$type = classifieds::getModuleByType($this->type);
		$this->url = $loc->getUrl()."/".$type."/".$this->id;

		return $this->url;
	}

	/**
	 * @return Location[]
	 */
	public function getLocations() {
		global $db;

		if ($this->locations !== NULL)
			return $this->locations;

		$this->locations = array();
		$res = $db->q("SELECT l.* 
						FROM classifieds_loc cl 
						INNER JOIN location_location l on cl.loc_id = l.loc_id 
						WHERE cl.post_id = ?", 
						array($this->id)
						);
		while ($row = $db->r($res)) {
			$loc = location::withRow($row);
			if (!$loc)
				continue;
			$this->locations[$row["loc_id"]] = $loc;
		}

		return $this->locations;
	}

	//returns array of city names, e.g. ["Las Vegas, NV", "San Francisco, CA", ...]
	public function getLocLabels() {
		global $db;

		if ($this->loc_labels !== NULL)
			return $this->loc_labels;

		$this->loc_labels = array();
		$res = $db->q("SELECT l.loc_id, l.loc_name, l.s 
						FROM classifieds_loc cl 
						INNER JOIN location_location l on cl.loc_id = l.loc_id
						WHERE cl.post_id = ?", 
						array($this->id)
						);
		while ($row = $db->r($res)) {
			$this->loc_labels[$row["loc_id"]] = $row["loc_name"].", ".$row["s"];
		}

		return $this->loc_labels;
	}

	public function getTotalPayment() {
		global $db;

		if ($this->total_payment !== NULL)
			return $this->total_payment;

		//get last transaction for payment for this ad
		$paid = $db->single("SELECT t.amount
			FROM transaction t
			INNER JOIN payment p on p.id = t.payment_id
			INNER JOIN payment_item pi on pi.payment_id = p.id
			WHERE pi.type = 'classified' AND pi.classified_id = ?
			ORDER BY t.stamp DESC
			LIMIT 1",
			[$this->id]
			);

		if (!$paid)
			return false;

		$this->total_payment = $paid;

		return $this->total_payment;
	}

	//CRUD functions
	function update($audit = false) {
		global $account, $db;

		if (!$this->id)
			return false;

		$original = clad::findOneById($this->id);
		if (!$original)
			return false;

		$update_type = false;
		$update_fields = array();
		if ($this->type != $original->getType()) {
			$update_fields["type"] = $this->type;
			$update_type = true;
		}
		$update_account = false;
		if ($this->account_id != $original->getAccountId()) {
			$update_fields["account_id"] = $this->account_id;
			$update_account = true;
		}
		if ($this->title != $original->getTitle())
			$update_fields["title"] = $this->title;
		if ($this->date != $original->getDate())
			$update_fields["date"] = $this->date;
		if ($this->thumb != $original->getThumb())
			$update_fields["thumb"] = $this->thumb;
		if ($this->multiple_thumbs != $original->getMultipleThumbs())
			$update_fields["multiple_thumbs"] = $this->multiple_thumbs;
		if ($this->phone != $original->getPhone())
			$update_fields["phone"] = $this->phone;
		if ($this->email != $original->getEmail())
			$update_fields["email"] = $this->email;
		if ($this->website != $original->getWebsite())
			$update_fields["website"] = $this->website;
		if ($this->ter != $original->getTer())
			$update_fields["ter"] = $this->ter;
		if ($this->reply != $original->getReply())
			$update_fields["reply"] = $this->reply;
		if ($this->cc_id != $original->getCcId())
			$update_fields["cc_id"] = $this->cc_id;
		$update_done = false;
		$update_updated = false;
		if ($this->done != $original->getDone()) {
			$update_fields["done"] = $this->done;
			$update_done = true;
			if ($this->done > 0 && $original->getDone() <= 0)
				$update_updated = true;
		}
		if ($this->expires != $original->getExpires())
			$update_fields["expires"] = $this->expires;
		if ($this->expire_stamp != $original->getExpireStamp())
			$update_fields["expire_stamp"] = $this->expire_stamp;
		if ($this->bp != $original->getBp())
			$update_fields["bp"] = $this->bp;
		if ($this->hash != $original->getHash())
			$update_fields["hash"] = $this->hash;
		if ($this->deleted != $original->getDeleted())
			$update_fields["deleted"] = $this->deleted;
		if ($this->allowlinks != $original->getAllowlinks())
			$update_fields["allowlinks"] = $this->allowlinks;
		if ($this->content != $original->getContent())
			$update_fields["content"] = $this->content;
		if ($this->created != $original->getCreated())
			$update_fields["created"] = $this->created;
		if ($this->sponsor != $original->getSponsor())
			$update_fields["sponsor"] = $this->sponsor;
		if ($this->sponsor_mobile != $original->getSponsorMobile())
			$update_fields["sponsor_mobile"] = $this->sponsor_mobile;
		if ($this->sponsor_position != $original->getSponsorPosition())
			$update_fields["sponsor_position"] = $this->sponsor_position;
		if ($this->auto_repost != $original->getAutoRepost())
			$update_fields["auto_repost"] = $this->auto_repost;
		if ($this->auto_renew_fr != $original->getAutoRenewFr())
			$update_fields["auto_renew_fr"] = $this->auto_renew_fr;
		if ($this->auto_renew_time != $original->getAutoRenewTime())
			$update_fields["auto_renew_time"] = $this->auto_renew_time;
		if ($this->time_next != $original->getTimeNext())
			$update_fields["time_next"] = $this->time_next;
		if ($this->ethnicity != $original->getEthnicity())
			$update_fields["ethnicity"] = $this->ethnicity;
		if ($this->promo != $original->getPromo())
			$update_fields["promo"] = $this->promo;
		if ($this->promo_code != $original->getPromoCode())
			$update_fields["promo_code"] = $this->promo_code;
		if ($this->hit != $original->getHit())
			$update_fields["hit"] = $this->hit;
		if ($this->ter_check_stamp != $original->getTerCheckStamp())
			$update_fields["ter_check_stamp"] = $this->ter_check_stamp;
		if ($this->ter_check_unapproved != $original->getTerCheckUnapproved())
			$update_fields["ter_check_unapproved"] = $this->ter_check_unapproved;
		if ($this->firstname != $original->getFirstname())
			$update_fields["firstname"] = $this->firstname;
		if ($this->lastname != $original->getLastname())
			$update_fields["lastname"] = $this->lastname;
		if ($this->eccie_reviews_link != $original->getEccieReviewsLink()) {
			$update_fields["ECCASapi_revsearch_url"] = $this->eccie_reviews_link;
			$update_fields["ECCASapi_revsearch_url_lastrequest"] = time();
		}
		if ($this->direct_link != $original->getDirectLink())
			$update_fields["direct_link"] = $this->direct_link;

		if (empty($update_fields))
			return true;	//nothing to update

		$changed_fields = "";
		$update = "";
		$update_arr = array();
		foreach ($update_fields as $key => $val) {
			$changed_fields .= (empty($changed_fields)) ? "" : ", ";
			$update .= (empty($update)) ? "SET " : ", ";
			$changed_fields .= "{$key}";
			if (method_exists($original, get_getter_name($key)))
				$changed_fields .= ": ".call_user_func(array($original, get_getter_name($key)))."->".call_user_func(array($this, get_getter_name($key)));
			$update .= "`{$key}` = ?";
			$update_arr[] = $val;
		}
		$update_arr[] = $this->id;
		$update = "UPDATE classifieds {$update} WHERE id = ? LIMIT 1";

		$db->q($update, $update_arr);
		$aff = $db->affected();

		file_log("classifieds", "UPDATE: '{$update}' aff={$aff}");
		if (!$aff)
			return false;

		if ($update_type)
			$this->update_type();

		if ($update_done)
			$this->update_done($update_updated);

		if ($update_account)
			$this->update_account();

		if ($audit)
			audit::log("CLA", "Edit", $this->id, "Changed fields: {$changed_fields}", $account->getId());

		return true;
	}

	private function update_type() {
		global $db;

		$res2 = $db->q("UPDATE classifieds_loc SET type = ? WHERE post_id = ?", array($this->type, $this->id));
		$aff2 = $db->affected($res2);

		$res3 = $db->q("UPDATE classifieds_sponsor SET type = ? WHERE post_id = ?", array($this->type, $this->id));
		$res3 = $db->q("UPDATE classifieds_side SET type = ? WHERE post_id = ?", array($this->type, $this->id));
		$res3 = $db->q("UPDATE classified_sticky SET type = ? WHERE classified_id = ?", array($this->type, $this->id));

		if ($aff2 > 0)
			return true;
		return false;
	}

	private function update_done($update_updated = false) {
		global $db;

		if ($update_updated)
			$res2 = $db->q("UPDATE classifieds_loc SET done = ?, updated = NOW() WHERE post_id = ?", array($this->done, $this->id));
		else
			$res2 = $db->q("UPDATE classifieds_loc SET done = ? WHERE post_id = ?", array($this->done, $this->id));
		$aff2 = $db->affected($res2);

		$res3 = $db->q("UPDATE classifieds_sponsor SET done = ? WHERE post_id = ?", array($this->done, $this->id));
		$res3 = $db->q("UPDATE classifieds_side SET done = ? WHERE post_id = ?", array($this->done, $this->id));
		$res3 = $db->q("UPDATE classified_sticky SET done = ? WHERE classified_id = ?", array($this->done, $this->id));

		debug_log("clad:update_done(): id={$this->id}, done={$this->done}, update_updated=".intval($update_updated));

		if ($aff2 > 0)
			return true;
		return false;
	}

	private function update_account() {
		global $db;

		$res2 = $db->q("UPDATE classifieds_payment SET account_id = ? WHERE post_id = ?", array($this->account_id, $this->id));
		debug_log("clad:update_account(): id={$this->id}, account_id={$this->account_id}");

		if ($aff2 > 0)
			return true;
		return false;
	}

	//additional functions

	//this function changes done to 1 for classified
	public function makeLive() {
		global $db, $account;

		$res = $db->q("UPDATE classifieds SET done = 1 WHERE id = ? LIMIT 1", array($this->id));
		$aff = $db->affected($res);

		$res2 = $db->q("UPDATE classifieds_loc SET done = 1, updated = NOW() WHERE post_id = ?", array($this->id));
		$aff2 = $db->affected($res2);

		$res3 = $db->q("UPDATE classifieds_sponsor SET done = 1 WHERE post_id = ?", array($this->id));
		$res3 = $db->q("UPDATE classifieds_side SET done = 1 WHERE post_id = ?", array($this->id));
		$res3 = $db->q("UPDATE classified_sticky SET done = 1 WHERE classified_id = ?", array($this->id));

		debug_log("clad:makeLive(): id={$this->id}, who={$account->getId()}");
		audit::log("CLA", "MakeLive", $this->id, "", $account->getId());

		classifieds::rotateIndex();

		if ($aff == 1 && $aff2 > 0)
			return true;

		return false;
	}
	
	public function expire($send_email = true, $send_sms = true, $email = null, $email_message = null) {
		global $db, $account;

		$now = time();
		$res1 = $db->q(
			"UPDATE classifieds SET done = 0, expires = NOW(), expire_stamp = ?  WHERE id = ? LIMIT 1", 
			[$now, $this->id]
			);
		$aff1 = $db->affected($res1);
		$res2 = $db->q("UPDATE classifieds_loc SET done = 0 WHERE post_id = ?", [$this->id]);
		$aff2 = $db->affected($res2);
		$res3 = $db->q("UPDATE classifieds_sponsor SET done = 0 WHERE post_id = ?", [$this->id]);
		$aff3 = $db->affected($res3);
		$res4 = $db->q("UPDATE classifieds_side SET done = 0 WHERE post_id = ?", [$this->id]);
		$aff4 = $db->affected($res4);
		$res5 = $db->q("UPDATE classified_sticky SET done = 0 WHERE classified_id = ?", [$this->id]);
		$aff5 = $db->affected($res5);

		debug_log("clad:expire(): id={$this->id}, aff1={$aff1}, aff2={$aff2}, aff3={$aff3}, aff4={$aff4}, aff5={$aff5}");
		audit::log("CLA", "Expire", $this->id, "", $account->getId());

		if ($send_email) {
			if (!$email)
				$email = $this->email;
			if (!$email) {
				$acc = $this->getAccount();
				if ($acc)
					$email = $acc->getEmail();
			}
			if ($email)
				$this->sendExpirationEmail($email, $email_message);
		}
		if ($send_sms) {
			$phone = makeOnlyDigitPhoneNumber($this->getPhone());
			if ($phone)
				$this->sendExpirationSms($phone);
		}
		audit::log("CLA", "Expire", $id, "", "S-cron/classifieds");

		return true;
	}

	public function sendExpirationEmail($email, $email_message = null) {
		global $account, $db;

		if (!$email)
			return false;

		$autologin = $account->createautologin($this->account_id, 'https://adultsearch.com/classifieds/myposts');
		$autologin .= "&mirsec={$this->id}";	//jay secretly add classified id to find out whats going on

		if (!$email_message)
			$email_message = "There are new clients looking for you on AdultSearch, but unfortunately your ad with us has expired.  Get back online now and don't miss out on any more phone calls from eager new clients.";

		$m = "Hello {$this->getFirstname()},<br><br>";
		$m .= "{$email_message}<br><br>";
		$m .= "To get your ad back up just go to adultsearch.com and login with your email and password. It's easy!<br><br>";
		$m .= "For your convenience, 1-click login link: <a href='{$autologin}'>{$autologin}</a><br><br>";
		$m .= "If you have any questions or need special help please feel free to contact me anytime.  If I am not immediately available I will call you back as soon as possible.<br><br>Have a great day!<br>support@adultsearch.com";
		sendEmail(SUPPORT_EMAIL, "A client is looking for you now - get back online!", $m, $email);

		return true;
	}

	public function sendExpirationSms($phone) {
		global $account, $db;

		if (!$phone)
			return false;

		if (strlen($phone) != 10)
			return false;

		$sms = new swiftsms();
		$text = "Your ad on adultsearch.com has expired. Post today so you don't miss out on new business!";
		$ret = $sms->send($phone, $text);

		return $ret;
	}

	public function changeType($new_type) {
		global $db, $account;

		$res = $db->q("UPDATE classifieds SET type = ? WHERE id = ? LIMIT 1", array($new_type, $this->id));
		$aff = $db->affected($res);

		$res2 = $db->q("UPDATE classifieds_loc SET type = ? WHERE post_id = ?", array($new_type, $this->id));
		$aff2 = $db->affected($res2);

		$res3 = $db->q("UPDATE classifieds_sponsor SET type = ? WHERE post_id = ?", array($new_type, $this->id));
		$res3 = $db->q("UPDATE classifieds_side SET type = ? WHERE post_id = ?", array($new_type, $this->id));
		$res3 = $db->q("UPDATE classified_sticky SET type = ? WHERE classified_id = ?", array($new_type, $this->id));

		audit::log("CLA", "Edit", $this->id, "New type: {$new_type}", $account->getId());

		classifieds::rotateIndex();

		if ($aff == 1 && $aff2 > 0)
			return true;

		return false;
	}

	/**
	 * removes classified from database
	 * @param audit_who - text representation of who should be audited as actor of this removal
	 */
	public function remove($audit_who = NULL) {
		global $db, $account;

		if ($audit_who == NULL)
			$audit_who = $account->getId();

		if ($this->getDone() == -1) {
			//if we removing this ad which was never finished, do hard delete
			$classifieds = new classifieds();
			$classifieds->_delImages($this->id);
			$res = $db->q("DELETE FROM classifieds WHERE id = ? LIMIT 1", array($this->id));
			$aff = $db->affected($res);
			if ($aff == 1) {
				$return = true;
			} else {
				$return = false;
			}
		
			$db->q("DELETE FROM classifieds_loc WHERE post_id = ?", array($this->id));
			$db->q("DELETE FROM classifieds_sponsor WHERE post_id = ?", array($this->id));
			$db->q("DELETE FROM classifieds_side WHERE post_id = ?", array($this->id));
			$db->q("DELETE FROM classified_sticky WHERE classified_id = ?", array($this->id));
			$db->q("DELETE FROM classifieds_refer WHERE id = ?", array($this->id));
			@unlink(classifieds::getUploadDir().$this->id.".jpg");

		} else {
			//this ad was live/waiting at some point in past, lets do soft delete
			$res = $db->q("UPDATE classifieds SET deleted = ? WHERE id = ? LIMIT 1", array(time(), $this->id));
			$aff = $db->affected($res);
			if ($aff == 1) {
				audit::log("CLA", "Remove", $this->id, "", $audit_who);
				$return = true;

				//cancel all payments for this ad
				payment::cancel_for_clad($this->id);

			} else {
				reportAdmin("clad::remove - update failed", "", array("aff" => $aff, "time" => time(), "id" => $this->id));
				$return = false;
			}
		}

		classifieds::rotateIndex();

		return $return;
	}
	
	/**
	 * undeletes classified ad
	 * @param audit_who - text representation of who should be audited as actor of this removal
	 */
	public function undelete($audit_who = NULL) {
		global $db, $account;

		if ($audit_who == NULL)
			$audit_who = $account->getId();

		$res = $db->q("UPDATE classifieds SET deleted = NULL WHERE id = ? LIMIT 1", array($this->id));
		$aff = $db->affected($res);
		if ($aff == 1) {
			classifieds::rotateIndex();
			audit::log("CLA", "Undelete", $this->id, "", $audit_who);
			$return = true;
		} else {
			$return = false;
		}

		return $return;
	}

	public function getClaimUrl() {
		global $db;

		if ($this->getAccountId())
			return false;

		$this->setHash(account::_getCookieHash());
		$ret = $this->update();
		if ($ret)
			return "http://adultsearch.com/classifieds/claim?id={$this->id}&hash={$this->hash}";
		else
			return false;
	}

	public function claim($acc = NULL) {
		global $account, $db;

		if ($acc == NULL)
			$acc = $account;

		$this->setAccountId($acc->getId());
		$this->setEmail($acc->getEmail());
		$this->setBp(0);
		$ret = $this->update();

		if ($ret === false) {
			reportAdmin("AS: clad/claim - Error", "Error while claiming classified ad", array("id" => $this->getId(), "account_id" => $acc->getId(), "email" => $acc->getEmail()));
			return false;
		}

		if ($this->getPhone() != "") {
			//dont grab this users ads from BP anymore
			$db->q("INSERT IGNORE INTO classifieds_dont (phone) values (?)", array($this->getPhone()));
		}
  
		audit::log("CLA", "Claim", $this->id, "", $account->getId());

		//mark in operation log
		$now = time();
		$op_msg = "Account #".$acc->getId()." claimed cl.ad #".$this->getId();
		$db->q("INSERT INTO admin_operation_log (code, stamp, msg) values ('last_clad_claim', ?, ?) on duplicate key update stamp = ?, msg = ?", array($now, $op_msg, $now, $op_msg)); 

		return true;
	}

	public function getLocCount() {
		global $db;

		$res = $db->q("SELECT count(id) as loc_count FROM classifieds_loc WHERE post_id = ? AND done = ?", array($this->id, $this->done));
		if ($db->numrows($res) == 0)
			return 0;
		$row = $db->r($res);
		return intval($row["loc_count"]);
	}

	public function emailReceipt($email = NULL, $trans_id = null) {

		if (!$email) {
			$acc = $this->getAccount();
			if (!$acc) {
				reportAdmin("AS: Error sending clad receipt", "invalid account for clad ?", ["clad_id" => $this->id, "account_id" => $this->getAccountId()]);
				return false;
			}
			file_log("classifieds", "emailReceipt: acc_id={$acc->getId()}");
			$email = $acc->getEmail();
		}
		//file_log("classifieds", "emailReceipt: email={$email}, trans_id={$trans_id}, id={$this->id}, locations=".print_r($this->getLocLabels(), true).", paid=".number_format($this->getTotalPayment(), 2)."'");

		$params = [
			"id" => $this->id,
			"locations" => $this->getLocLabels(),
			"paid" => number_format($this->getTotalPayment(), 2),
			"trans_id" => $trans_id,
			];

		$subject = "Receipt for your advertisement";

		return payment::sendPaymentEmail("classifieds/email/receipt", $params, $subject, $email);
	}

	public function emailAdLive($email = NULL) {

		if (!$email) {
			$acc = $this->getAccount();
			if (!$acc) {
				reportAdmin("AS: Error sending ad live email", "invalid account for clad ?", ["clad_id" => $this->id, "account_id" => $this->getAccountId()]);
				return false;
			}
			file_log("classifieds", "emailAdLive: acc_id={$acc->getId()}");
			$email = $acc->getEmail();
		}

		$params = [
			"id" => $this->id,
			"locations" => $this->getLocLabels(),
			];

		$subject = "Your ad is live now";

		return payment::sendPaymentEmail("classifieds/email/ad_live", $params, $subject, $email);
	}

	public function emailAdminNotify($email = NULL, $edit = false, &$error = NULL) {
		global $db, $config_image_path;

		$acc = $this->getAccount();
		if (!$acc)
			return false;

		$embedded_images = array();

		$smarty = GetSmartyInstance();

		//basic ad attributes
		$smarty->assign("id", $this->id);
		$smarty->assign("url", $this->getUrl());
		$smarty->assign("title", htmlspecialchars($this->getTitle()));
		$smarty->assign("category", classifieds::getcatnamebytype($this->type));

		$phone = $this->getPhone();
		if ($phone)
			$phone = makeproperphonenumber($phone);
		$smarty->assign("phone", $phone);
		$smarty->assign("is_phone_in_whitelist", $this->isPhoneInWhitelist());
		file_log("classifieds", "clad::emailAdminNotify: phone='{$this->phone}', area_code_not_valid='".intval(area_code_not_valid($this->phone))."'");
		$smarty->assign("area_code_not_valid", area_code_not_valid($this->phone));

		$smarty->assign("locations", $this->getLocLabels());
		$smarty->assign("content", classifieds::sanitizeContentForOutput($this->getContent(), $this->getAllowlinks(), $this->getAccountId()));
		$smarty->assign("paid", number_format($total_payment, 2));

		//get credit card info
		$res = $db->q("
			SELECT cc.type, cc.cc, cc.firstname, cc.lastname, cc.city, cc.state, p.ip_address
			FROM payment p
			INNER JOIN account_cc cc on cc.cc_id = p.cc_id
			WHERE p.account_id = ?
			ORDER BY p.id DESC
			LIMIT 1
			",
			[$acc->getId()]
			);
		if ($db->numrows($res) == 1) {
			$row = $db->r($res);
			$smarty->assign("cc_info", $row["type"]." ".substr($row["cc"], -5)." - ".$row["firstname"]." ".$row["lastname"].", ".$row["city"].", ".$row["state"]);
		}

		//account info
		$smarty->assign("account_id", $this->getAccountId());
		$smarty->assign("acc_email", $acc->getEmail());
		if ($acc->isWhitelisted())
			$smarty->assign("whitelisted", true);
		if ($acc->getAccountLevel() > 2)
			$smarty->assign("admin", true);
		$ip_address = $ip_address_text = preg_replace('/[^0-9\.]/', '', $row["ip_address"]);
		$location = geolocation::getLocationByIp($ip_address);
		if ($location["city_name"])
			$ip_address_text .= " : {$location["city_name"]}";
		if ($location["state_name"])
			$ip_address_text .= ", ".$location["state_name"];
		$smarty->assign("ip_address_text", $ip_address_text);

		//thumbnail
		if ($this->getThumb()) {
			$smarty->assign("thumb", $this->getThumb());
			$embedded_images["thumb_".$this->getThumb()] = $this->getThumbFilepath();
		} else {
			$smarty->assign("thumb", "placeholder");
			$embedded_images["thumb_placeholder"] = _CMS_ABS_PATH."/images/placeholder.gif";
		}

		//images
		$i = 0;
		foreach ($this->getImages() as $image) {
			if ($i > 4)
				break;
			$imageFilepath = "{$config_image_path}/classifieds/t/{$image}";
			$ext = pathinfo($imageFilepath, PATHINFO_EXTENSION);
			if ($ext != "jpg")
				continue;
			$embedded_images["image_{$image}"] = $imageFilepath;
			$i++;
		}
		$smarty->assign("images", $this->getImages());
	
		$smarty->assign("paid", number_format($this->getTotalPayment(), 2));
	
		//get number of ads user already has
		$res = $db->q("SELECT id FROM classifieds WHERE account_id = ? AND done = 1", array($acc->getId()));
		$nr = $db->numrows($res);
		$smarty->assign("user_ads_count", $nr);

		//get city cover information
		$res = $db->q("SELECT count(id) as loc_cnt, SUM(day) as days FROM classifieds_sponsor WHERE post_id = ? AND done > 0", array($this->getId()));
		if ($db->numrows($res)) {
			$row = $db->r($res);
			$loc_cnt = intval($row["loc_cnt"]);
			$days = intval($row["days"]);
			$smarty->assign("city_cover_loc_cnt", $loc_cnt);
			$smarty->assign("city_cover_days", $days);
		}

		if ($edit) {
			$subject = "AS: Classified ad #{$this->getId()} edited";
			$smarty->assign("edit", true);
		} else {
			$subject = "New classified ad on AS: #{$this->getId()}";
		}
		$smarty->assign("subject", $subject);

		$html = $smarty->fetch(_CMS_ABS_PATH."/templates/classifieds/email/clad_admin_notify.html.tpl");
		$text = $smarty->fetch(_CMS_ABS_PATH."/templates/classifieds/email/clad_admin_notify.txt.tpl");
			
		if (!$email) {
			$email = SUPPORT_EMAIL;
		}
		//for testing purposes on production
		if ($acc->getId() == 3974)
			$email = ADMIN_EMAIL;

		$error = "";
		$ret = send_email(array(
			"from" => SUPPORT_EMAIL,
			"to" => $email,
//			"bcc" => ADMIN_EMAIL,
			"subject" => $subject,
			"html" => $html,
			"text" => $text,
			"embedded_images" => $embedded_images
			), $error);
		return $ret;
	}

	/**
	 * updates 'posted' timestamp in classified_loc and classifieds table to current timestamp
	 * -> and therefore moving the ad to the top of the list
	 */
	public function bump() {
		global $db, $account;
	
		$datetime = date("Y-m-d H:i:s", time());

		$db->q("UPDATE classifieds SET `date` = ? WHERE id = ? LIMIT 1", array($datetime, $this->id));
		$aff1 = $db->affected();

		$db->q("UPDATE classifieds_loc SET `updated` = ? WHERE post_id = ?", array($datetime, $this->id));
		$aff2 = $db->affected();

		classifieds::rotateIndex();

		if ($aff1 == 1 && $aff2 > 0) {
			audit::log("CLA", "Bump", $this->id, "", $account->getId());
			return true;
		}
		return false;
	}

	public function getActiveSubscription() {
		global $db, $account;

		$res = $db->q("
			SELECT DISTINCT pi.payment_id, p.subscription_id
			FROM payment_item pi
			INNER JOIN payment p on p.id = pi.payment_id
			WHERE pi.type = 'classified' AND pi.classified_id = ? AND p.subscription_status = 1
			",
			[$this->id]
			);
		if ($db->numrows($res) > 1) {
			reportAdmin("AS: clad:getActiveSubscription Error", "More than one active subscriptions for clad #{$this->id} !");
			$row = $db->r($res);
		} else if ($db->numrows($res) == 1) {
			$row = $db->r($res);
		} else {
			//try old way
			$res = $db->q("
				SELECT p.id, p.subscription_id
				FROM payment p
				WHERE p.p1 = 'classifieds' and p.p3 = ? AND p.subscription_status = 1
				",
				[$this->id]
				);
			if ($db->numrows($res) > 1) {
				reportAdmin("AS: clad:getActiveSubscription Error", "More than one active subscriptions for clad #{$this->id} !");
				$row = $db->r($res);
			} else if ($db->numrows($res) == 1) {
				$row = $db->r($res);
			} else {
				//we havent found any active subscription
				return false;
			}
		}
		
		$resp = [
			"payment_id" => $row["payment_id"],
			"subscription_id" => $row["subscription_id"],
			];

		return $resp;
	}

	public function getLastPaymentsAndActiveSubscriptions() {
		global $db, $account;

		$payments = [];

		//get active subscriptions and last payments, new
		$res = $db->q("
			SELECT t.payment_id, t.stamp, t.amount, t.trans_id
				, p.subscription_status, p.subscription_id, p.next_renewal_date
				, pi.id as pi_id
			FROM transaction t
			INNER JOIN payment p on p.id = t.payment_id
			INNER JOIN payment_item pi on pi.payment_id = p.id AND pi.type = 'classified'
			WHERE pi.classified_id = ?
			",
			[$this->id]
			);
		while ($row = $db->r($res)) {

			$payment_id = $row["payment_id"];
			$transaction_stamp = $row["stamp"];
			$transaction_amount = $row["amount"];
			$transaction_id = $row["trans_id"];

			//only transactions/payments from last 6 months
			if ($transaction_stamp < $six_months_ago)
				continue;

			if (array_key_exists($payment_id, $payments)) {
				if ($payments[$payment_id]["last_transaction_stamp"] < $transaction_stamp) {
					$payments[$payment_id]["last_transaction_stamp"] = $transaction_stamp;
					$payments[$payment_id]["last_transaction_amount"] = $transaction_amount;
					$payments[$payment_id]["last_transaction_id"] = $transaction_id;
				}
			} else {
				$payments[$payment_id] = [
					"payment_id" => $payment_id,
					"last_transaction_stamp" => $transaction_stamp,
					"last_transaction_amount" => $transaction_amount,
					"last_transaction_id" => $transaction_id,
					"subscription_id" => $row["subscription_id"],
					"subscription_status" => $row["subscription_status"],
					"next_renewal_date" => $row["next_renewal_date"],
					];
			}
		}

		return $payments;
	}

	public function deleteAdCancelSubscriptions() {
		global $db, $account;

		file_log("classifieds", "clad:deleteAdCancelSubscriptions(): clad_id={$this->id}");

		$three_days_ago = time() - 86400*3;
		$twentyfour_hours_ago = time() - 86400;

		$payments = $this->getLastPaymentsAndActiveSubscriptions();

		foreach ($payments as $payment_id => $p) {

			$amount = $p["last_transaction_amount"];
			$purchase_id = $p["last_transaction_purchase_id"];

			//if payment has active subscription, cancel it
			if ($p["subscription_status"] == 1) {
				$error = null;
				$ret = payment::cancel($p["payment_id"], "client_cancel", $error);
				if ($ret === false) {
					//dont do anything visible, this is called by user
					//notify admin
					reportAdmin("AS: clad::deleteAdCancelSubscriptions() error", "Can't cancel subscription payment #{$p["payment_id"]} !", [
						"clad_id" => $this->id,
						"payment_id" => $payment_id,
						"subscription_id" => $subscription_id,
						]);
				} else {
					file_log("classifieds", "clad:deleteAdCancelSubscriptions(): Canceled subscription payment #{$p["payment_id"]}");
					audit::log("SUB", "Cancel", $payment_id, "", $account->getId());
				}
			}

			//if payment was done more than 3 days ago, no void/refund
			if ($p["last_transaction_stamp"] < $three_days_ago) {
				file_log("classifieds", "clad:deleteAdCancelSubscriptions(): Transaction #{$p["last_transaction_id"]} was done ({$p["last_transaction_stamp"]}) more than 3 days ago - no refund");
				continue;
			}

			//if payment was done in last 24 hours, do void/refund
			//WE DONT DO THIS, IT WAS ABUSED (people were abusing it to post ads free for a day)

			//else send email to admin to consider refund
			file_log("classifieds", "clad:deleteAdCancelSubscriptions(): Sending email to support");
			$html = "User deleted his ad day or two after he created it. That is suspicious. Please check if this is not spammer. If appropriate, refund him his transaction:<br /><a href=\"https://adultsearch.com/mng/classifieds?cid={$this->id}&deleted=all\">Mng ad #{$this->id}</a><br /><a href=\"https://adultsearch.com/mng/sales?item_id={$this->id}\">Transactions of ad #{$this->id}</a><br />";
			$text = "User deleted his ad day or two after he created it. That is suspicious. Please check if this is not spammer. If appropriate, refund him his transaction:\nMng ad #{$this->id}: https://adultsearch.com/mng/classifieds?cid={$this->id}&deleted=all\nTransactions of ad #{$this->id}: https://adultsearch.com/mng/sales?item_id={$this->id}";
			$ret = send_email(array(
				"from" => WEB_EMAIL,
				"to" => SUPPORT_EMAIL,
				"bcc" => ADMIN_EMAIL,
				"subject" => "Please check this deleted ad",
				"html" => $html,
				"text" => $text,
				), $error);
		}
	}

	public function isPhoneInWhitelist() {
		global $db;

		$error = null;

		if ($this->phone) {
			$phone = phone_to_international($this->phone, $error);
			if ($phone) {
				$res = $db->q("SELECT id FROM phone_whitelist WHERE phone = ?", [$phone]);
				if ($db->numrows($res) > 0)
					return "Classified ad phone in whitelist";
			}
		}

		$acc = $this->getAccount();
		if ($acc && $acc->getPhone() && $acc->isPhoneVerified()) {
			$phone = phone_to_international($acc->getPhone(), $error);
			if ($phone) {
				$res = $db->q("SELECT id FROM phone_whitelist WHERE phone = ?", [$phone]);
				if ($db->numrows($res) > 0)
					return "Account phone in whitelist";
			}
		}

		return false;
	}

	/**
	 * @return int[]
	 */
	public function getStickyLocations(){
		global $db;
		$loc_ids = [];
		$query = $db->q("select loc_id from classified_sticky where classified_id = $this->id and done > 0");
		while ($row = $db->r($query)){
			$loc_ids[] = $row['loc_id'];
		}
		return $loc_ids;
	}

	/**
	 * @return bool
	 */
	public function hasActiveSticky() {
		global $db;

		$count = $db->single("
			SELECT COUNT(DISTINCT cs.id) AS sticky_count
			FROM classified_sticky cs
			WHERE cs.classified_id = ?
			",
			[$this->id]
		);

		return ($count > 0);
	}

	/**
	 * @return bool
	 */
	public function isExistsUpgrades() {
		global $db;

		$query = $db->q('
			SELECT 
				cs.id,  
				COUNT(DISTINCT sticky.id)  AS sticky_count,
				COUNT(DISTINCT sponsor.id) AS sponsor_count,  
				COUNT(DISTINCT side.id)	AS side_count 
			FROM classifieds AS cs 
			LEFT JOIN classified_sticky   AS sticky  ON cs.id = sticky.classified_id AND sticky.done > 0
			LEFT JOIN classifieds_sponsor AS sponsor ON cs.id = sponsor.post_id AND sponsor.done > 0
			LEFT JOIN classifieds_side	AS side	ON cs.id = side.post_id AND side.done > 0
			WHERE cs.id = ? 
			LIMIT 1;',
			[
				$this->id,
			]
		);
		$result = $db->r($query);

		return ($result['sticky_count'] > 0 || $result['sponsor_count'] > 0 || $result['side_count'] > 0);
	}
}

