<?php

use App\Service\Classified\WaitingList;

class classifieds {

	var $price_per_repost = 3.30;

	var $price_topup = 4.99;
	var $price_topup_agency = 9.99;

	//recurring every-day repost for a month
	var $price_per_recurring = 49.99;

	//maximum number of city thumbnails per city
	public static $city_thumbnail_count_limit = 8;
	//city thumbnail is for 30 days
	var $city_thumbnail_days = 30;
	var $price_per_home = 1;
	//var $home_price_month = 50.00;
	//increase to $75/month (2018-04-16)
	//increase to $150/month (2018-06-05)
	public static $home_price_month = 100.00;

	//maximum number of side sponsors per city
	public static $side_sponsor_count_limit = 15;
	//public static $price_side_sponsor = 25.00;
	//increase to 50.00 - zach's request 2019-08-07
	public static $price_side_sponsor = 50.00;

	public static $sticky_count_limit = 5;

	var $price_combo_city_recurring = 80.00;

	var $discount = 0;

	var $default_time = 0, $default_timezone = "PST";

	var $picture_limit = 8;

	var $blocked_urls = array(
		"xmatch.com", "ashleymadison.com", "ihookup.co", "verifiedmeet.com", "stiffdays.com", "justhookup.com", "rudefinder.com", "fuckbookhookups.com", 
		"ashleyrnadison.com", "hornywife.com", "review-adult-dating.com", "greatenhancement.com", "upscalealerts.com", "pickthechick.com", "tvwoo.com", 
		"hookup.com", "foreplaylinks.com", "fuckbookofsex.mobi", "alexxisdubois.info", "localbangbuddy.com"
		);
	var $shouldAllowUser = false;	

	var $adbuild_id = NULL;
	var $adbuild_type = NULL;
	var $adbuild_row = NULL;
	var $adbuild_editonly = NULL;
	var $adbuild_locs = NULL;
	var $adbuild_updating = false;
	var $adbuild_loclimit = 1;
	var $adbuild_locparentlimit = 2;
	var $adbuild_totalcost = 0;
	var $adbuild_post_price = 0;
	var $adbuild_repost_price = 0;
	var $adbuild_sponsor_city = 0;
	var $adbuild_sponsor_total = 0;
	var $adbuild_side_city = 0;
	var $adbuild_side_total = 0;
	var $adbuild_sticky_city = 0;
	var $adbuild_sticky_total = 0;
	var $adbuild_international = false;
	var $adbuild_metric = false;
	var $adbuild_currency = NULL;
	var $adbuild_country = NULL;
	var $adbuild_param_process = false;
	var $adbuild_cat_questions = array(
		// female escorts
		"1" => array('avl_men', 'avl_women', 'avl_couple', 'incall', 'outcall', 'incall_rate_hh', 'incall_rate_h', 'incall_rate_2h', 'incall_rate_day', 
				'outcall_rate_hh', 'outcall_rate_h', 'outcall_rate_2h', 'outcall_rate_day', 'visiting', 'visiting_from', 'visiting_to', 'location', 'gfe', 'gfe_limited', 
				'fetish_dominant', 'fetish_submissive', 'fetish_swith', 'age', 'ethnicity', 'language', 'height_feet', 'height_inches', 'weight', 'eyecolor', 'haircolor', 
				'build', 'measure_1', 'measure_2', 'measure_3', 'cupsize', 'kitty', 'pornstar', 'pregnant'),
		// ts-tv
		"2" => array('avl_men', 'avl_women', 'avl_couple', 'incall', 'outcall', 'incall_rate_hh', 'incall_rate_h', 'incall_rate_2h', 'incall_rate_day', 
				'outcall_rate_hh', 'outcall_rate_h', 'outcall_rate_2h', 'outcall_rate_day', 'visiting', 'visiting_from', 'visiting_to', 'location', 'gfe', 'gfe_limited', 
				'fetish_dominant', 'fetish_submissive', 'fetish_swith', 'age', 'ethnicity', 'language', 'height_feet', 'height_inches', 'weight', 'eyecolor', 'haircolor', 
				'build', 'measure_1', 'measure_2', 'measure_3', 'cupsize', 'penis_size', 'pornstar'),
		// male for female
		"3" => array('avl_men', 'avl_women', 'avl_couple', 'incall', 'outcall', 'incall_rate_hh', 'incall_rate_h', 'incall_rate_2h', 'incall_rate_day', 
				'outcall_rate_hh', 'outcall_rate_h', 'outcall_rate_2h', 'outcall_rate_day', 'visiting', 'visiting_from', 'visiting_to', 'location', 
				'fetish_dominant', 'fetish_submissive', 'fetish_swith', 'age', 'ethnicity', 'language', 'height_feet', 'height_inches', 'weight', 'eyecolor', 'haircolor', 
				'build', 'penis_size', 'pornstar'),
		// male for male
		"4" => array('avl_men', 'avl_women', 'avl_couple', 'incall', 'outcall', 'incall_rate_hh', 'incall_rate_h', 'incall_rate_2h', 'incall_rate_day', 
				'outcall_rate_hh', 'outcall_rate_h', 'outcall_rate_2h', 'outcall_rate_day', 'visiting', 'visiting_from', 'visiting_to', 'location', 
				'fetish_dominant', 'fetish_submissive', 'fetish_swith', 'age', 'ethnicity', 'language', 'height_feet', 'height_inches', 'weight', 'eyecolor', 'haircolor', 
				'build', 'penis_size', 'pornstar'),
		// escort agency
		"5" => array('avl_men', 'avl_women', 'avl_couple', 'incall', 'outcall', 'incall_rate_hh', 'incall_rate_h', 'incall_rate_2h', 'incall_rate_day', 
				'outcall_rate_hh', 'outcall_rate_h', 'outcall_rate_2h', 'outcall_rate_day', 'visiting', 'visiting_from', 'visiting_to', 'location', 
				'fetish_dominant', 'fetish_submissive', 'fetish_swith'),
		// body rubs
		"6" => array('avl_men', 'avl_women', 'avl_couple', 'incall', 'outcall', 'incall_rate_hh', 'incall_rate_h', 'incall_rate_2h', 'incall_rate_day', 
				'outcall_rate_hh', 'outcall_rate_h', 'outcall_rate_2h', 'outcall_rate_day', 'visiting', 'visiting_from', 'visiting_to', 'location', 
				'gfe', 'gfe_limited', 'tantra', 'fetish_dominant', 'fetish_submissive', 'fetish_swith', 'age', 'ethnicity', 'language', 'height_feet', 'height_inches', 
				'weight', 'eyecolor', 'haircolor', 'build', 'measure_1', 'measure_2', 'measure_3', 'cupsize', 'kitty', 'pornstar', 'pregnant'),
		// m4m body rubs
		"7" => array('avl_men', 'avl_women', 'avl_couple', 'incall', 'outcall', 'incall_rate_hh', 'incall_rate_h', 'incall_rate_2h', 'incall_rate_day', 
				'outcall_rate_hh', 'outcall_rate_h', 'outcall_rate_2h', 'outcall_rate_day', 'visiting', 'visiting_from', 'visiting_to', 'location', 
				'fetish_dominant', 'fetish_submissive', 'fetish_swith', 'age', 'ethnicity', 'language', 'height_feet', 'height_inches', 'weight', 'eyecolor', 'haircolor', 
				'build', 'penis_size', 'pornstar'),
		// stripper for hire
		"12" => array('avl_men', 'avl_women', 'avl_couple', 'incall', 'outcall', 'incall_rate_hh', 'incall_rate_h', 'incall_rate_2h', 'incall_rate_day', 
				'outcall_rate_hh', 'outcall_rate_h', 'outcall_rate_2h', 'outcall_rate_day', 'visiting', 'visiting_from', 'visiting_to', 'location', 
				'fetish_dominant', 'fetish_submissive', 'fetish_swith', 'age'),
		// bdsm
		"13" => array('avl_men', 'avl_women', 'avl_couple', 'incall', 'outcall', 'incall_rate_hh', 'incall_rate_h', 'incall_rate_2h', 'incall_rate_day', 
				'outcall_rate_hh', 'outcall_rate_h', 'outcall_rate_2h', 'outcall_rate_day', 'visiting', 'visiting_from', 'visiting_to', 'location', 
				'fetish_dominant', 'fetish_submissive', 'fetish_swith', 'fetish', 'age', 'ethnicity', 'language', 'height_feet', 'height_inches', 'weight', 'eyecolor', 
				'haircolor', 'build', 'measure_1', 'measure_2', 'measure_3', 'cupsize', 'pornstar'),
		// adult help wanted
		"14" => array('location'),
		// phone & web - the same fields as female escorts
		"16" => array('avl_men', 'avl_women', 'avl_couple', 'incall', 'outcall', 'incall_rate_hh', 'incall_rate_h', 'incall_rate_2h', 'incall_rate_day', 
				'outcall_rate_hh', 'outcall_rate_h', 'outcall_rate_2h', 'outcall_rate_day', 'visiting', 'visiting_from', 'visiting_to', 'location', 'gfe', 'gfe_limited', 
				'fetish_dominant', 'fetish_submissive', 'fetish_swith', 'age', 'ethnicity', 'language', 'height_feet', 'height_inches', 'weight', 'eyecolor', 'haircolor', 
				'build', 'measure_1', 'measure_2', 'measure_3', 'cupsize', 'kitty', 'pornstar', 'pregnant')
	);
	var $adbuild_type_array = array("1"=>"Female Escorts", "2"=>"TS / TV Shemale Escorts", "6"=>"Body Rubs", "12"=>"Strippers For Hire", "13"=>"BDSM / 
Fetish Escorts", "14"=>"Adult Help Wanted", "3"=>"Male Escorts for Female", "4"=>"Male for Male Escorts", "7"=>"Male For Male Body Rubs", "15"=>"Webcam Sites", "5"=>"Escort 
Agencies", "16" => "Phone & Web");
	var $adbuild_ethnicity = array('1'=>'Asian','2'=>'Black','4'=>'White','5'=>'East Indian','3'=>'Hispanic','22'=>'Middle Eastern','7'=>'Mixed');
	var $adbuild_eyecolor = array('5'=>'Black', '1'=>'Blue', '2'=>'Brown', '6'=>'Gray', '3'=>'Green', '4'=>'Hazel');
	var $adbuild_haircolor = array('2'=>'Blonde', '1'=>'Light Brown', '3'=>'Dark Brown', '5'=>'Auburn/Red', '4'=>'Black', 
		'6'=>'Gray/White', '7'=>'Wild');
	var $adbuild_language = array('1'=>'Albanian', '2'=>'Arabic', '3'=>'Armenian', '4'=>'Belarusian', '5'=>'Bosnian', '6'=>'Celtic', 
'7'=>'Chinese (Simplified)', '8'=>'Chinese (Traditional)', '9'=>'Croatian', '10'=>'Czech', '11'=>'Danish', '12'=>'Dutch', '13'=>'English', 
'14'=>'Finnish', '15'=>'French', '16'=>'German', '17'=>'Greek', '18'=>'Italian', '19'=>'Japanese', '20'=>'Korean', '21'=>'Lao', 
'22'=>'Latvian', '23'=>'Lithuanian', '24'=>'Persian', '25'=>'Polish', '26'=>'Portuguese', '27'=>'Norwegian', '28'=>'Russian', 
'29'=>'Serbian', '30'=>'Spanish', '31'=>'Swedish', '32'=>'Thai', '33'=>'Turkish', '34'=>'Ukranian', '35'=>'Vietnamese');
	var $adbuild_cupsize = array('1'=>'A', '2'=>'B', '3'=>'C', '4'=>'D', '5'=>'DD', '6'=>'DDD', '7'=>'Just Plain Huge');
	var $adbuild_penis_size = array('3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'Monster');
	var $adbuild_kitty = array('1'=>'Bald', '2'=>'Partially Shaved', '3'=>'Trimmed', '4'=>'Natural');
	var $adbuild_build = array('1'=>'Tiny', '2'=>'Slim/Slender', '3'=>'Athletic', '4'=>'Average', '5'=>'Curvy', '6'=>'BBW');
	var $adbuild_error = NULL;

	const TYPE_FEMALE_ESCORT = 1;
	const TYPE_TRANS_ESCORT = 2;
	const TYPE_BODYRUB = 6;

	public function __construct() {
	}

	public static function getUploadDir() {
		global $config_image_path;
		return $config_image_path."classifieds/";
	}

	//default prices
	//if you need to modify ad prices (depending on location) go to static method classifieds::getPostPriceForLocation()
	public static function getPriceNormal() {
		return 9.99;
	}
	public static function getPriceAgency() {
		return 49.99;
	}

	function _delImage($image_id, $id) {
		global $db;

		$res = $db->q("select thumb, filename from classifieds_image i inner join classifieds c using (id) where i.image_id = '$image_id' and i.id = '$id'");
		if( !$db->numrows($res) ) {
			return;
		}
		$row = $db->r($res);

		@unlink(self::getUploadDir().$row["filename"]);
		@unlink(self::getUploadDir()."t/".$row["filename"]);
		$db->q("delete from classifieds_image where image_id = '$image_id'");
		if( $row["thumb"] == ($id.".jpg") && file_exists(self::getUploadDir().$row["thumb"]) ) {
			@unlink(self::getUploadDir().$row["thumb"]);
			$db->q("update classifieds set thumb = '' where id = '$id'");
			$this->createThumb($id);
		}

		return;

		$res = $db->q("select filename from classifieds_image where id = '$id' limit 1");
		if( $db->numrows($res) ) {
			$row = $db->r($res);
			$db->q("update classifieds set thumb = '{$row["filename"]}' where id = '$id'");
		}
		return;
	}

	function getNumPic($id) {
		global $db;

		$res = $db->q("select count(*) from classifieds_image where id = ?", [$id]);
		$row = $db->r($res);
		return $row[0];
	}

	public static function remove($post_id, $audit_who = NULL) {

		$clad = clad::findOneById($post_id);
		if (!$clad)
			return false;

		return $clad->remove($audit_who);
	}

	function _delImages($id) {
		global $db;

		$res = $db->q("select filename from classifieds_image i where i.id = '$id'");
		if( !$db->numrows($res) ) {
			return;
		}
		while($row = $db->r($res)) {
			@unlink(self::getUploadDir().$row["filename"]);
			@unlink(self::getUploadDir()."t/".$row["filename"]);
		}
		$db->q("delete from classifieds_image where id = '$id'");

		self::sponsorpicremove($id);
		return;
	}

	function setThumb($id, $thumb) {
		global $db;
		$db->q("update classifieds set thumb = ? WHERE id = ? AND thumb = ''", [$thumb, $id]);
	}

	public static function html($text) {
		$pattern[] = '@<script[^>]*?>.*?</script>@si';
		$replace[] = '';

		$pattern[] = '@<center[^>]*?>.*?(</center>)?@si';
		$replace[] = '\\1';

		$pattern[] = '@<div[^>]*?>.*?(</div>)?@si';
		$replace[] = '\\1';

		$pattern[] = '@<h[^>]*?>.*?(</h)?@si';
		$replace[] = '\\1';

		$pattern[] = '@intr\.com\/(.*)/.jpg@si';
		$replace[] = '';

		$pattern[] = "@http://(.*)\/pic(tures)?\.jpg@si";
		$replace[] = "";

		$text = preg_replace($pattern, $replace, $text);

		$pattern = "#([\n ])([a-z]+?)://([^, <\n\r]+)#i"; 
		$text = preg_replace_callback( 
			$pattern,  
			function ($matches) { 
				return "{$matches[1]}<a href=\"{$matches[2]}://{$matches[3]}\" target=\"_blank\" rel=\"nofollow\">{$matches[2]}://".substr($matches[3], 0, 50)."</a>"; 
			}, 
			$text);

		$pattern = "#([\n ])www\.([a-z0-9\-]+)\.([a-z0-9\-.\~]+)((?:/[^, <\n\r]*)?)#i";
		$replace = "\\1<a href=\"http://www.\\2.\\3\\4\" target=\"_blank\" rel=\"nofollow\">www.\\2.\\3\\4</a>";
		$text = preg_replace($pattern, $replace, $text);

		return $text;
	}

	//handles UTF8 string on input
	public static function onlyPrintableCharacters($str) {

		//this removes 4byte UTF-8 characters (because we have ut8 everywhere in db, and not utf8mb4)
		$str = preg_replace('/[\xF0-\xF7].../s', '', $str);

		//removes UTF8 control characters
		$str = preg_replace('/[\x00-\x1F\x80-\x9F]/u', '', $str);	// http://stackoverflow.com/questions/1176904/php-how-to-remove-all-non-printable-characters-in-a-string

		return $str;
	}

	public static function noHtml($str) {
		$str = preg_replace('/[<>]/', '', $str);
		return $str;
	}

	public static function sanitizeContentForOutput($content, $allowlinks, $account_id) {
		$content = self::html($content);
		if ($allowlinks == 0) {
			$content = strip_tags($content, "<br><p><b><u><i><strong><em><ul><ol><li>");
			//remove links in text
			$content = preg_replace("|https?://(?:www\.)?[a-z\.0-9/]+|i", "", $content);
			$content = preg_replace("|bit\.ly/[a-z\.0-9/]+|i", "", $content);
			//TODO following 4 lines are probably excessive
			$content = str_replace("\n", "", $content);
			$content = preg_replace("/<a([^>]*)>.*<\/a>/i", "", $content);
			$content = preg_replace("/http:\/\/(www\.)?videobam\.com/i", "", $content);
			$content = preg_replace("/<iframe[^>]*>.*<\/iframe>/i", "", $content);
		} else {
			$content = preg_replace("/<a /i", "<a target=\"_blank\" ", $content);
		}
		if (strstr($content, "<embet")) {
			$content = preg_replace('/<embed /i', '<embed wmode="transparent" ', $content);
		}
		return $content;
	}

	public static function sanitizeWebsite($website) {
		$block_patterns = array("bit.ly", "tinyurl.com");
		foreach($block_patterns as $pattern) {
			if (strpos($website, $pattern) !== false)
				return "";
		}
		return $website;
	}

	/**
	 * Returns true if person is logged in from clad poster ip address (minions)
	 */
	public static function isCladPoster() {
		global $db;

		if (array_key_exists("clad_poster", $_SESSION))
			return $_SESSION["clad_poster"];

		$clad_poster = false;
		$res = $db->q("SELECT value FROM admin_settings WHERE name = 'clad_poster_ip' LIMIT 1");
		if ($db->numrows($res) == 1) {
			$row = $db->r($res);
			//debug_log("IP fetched: '{$row["value"]}'");
			$ips = explode(",", $row["value"]);
			//debug_log("IP ips=".print_r($ips, true));
			if (is_array($ips) && in_array(account::getUserIp(), $ips)) {
				//debug_log("IP found.");
				$clad_poster = true;
			}
		}
		$_SESSION["clad_poster"] = $clad_poster;

		return $_SESSION["clad_poster"];
	}

	function isOwner($rowcl) {
		global $account;
		$account_id = intval($account->isloggedin());
		if( !empty($rowcl["account_id"]) && $account_id == $rowcl["account_id"] ) return true;
		else if( empty($rowcl["account_id"]) && isset($_SESSION["cl_owner_".$rowcl["id"]]) ) return true;
		else if( $account->isadmin() ) return true;
		return false;
	}

	public static function sponsorpicremove($id) {
		global $db;

		$res = $db->q("select images from classifieds_sponsor where post_id = ? LIMIT 1", [$id]);
		if (!$db->numrows($res))
			return;
		$row = $db->r($res);
		if (empty($row['images']))
			return;
		@unlink(self::getUploadDir()."s/".$row["images"]);
		$db->q("update classifieds_sponsor set images = '' where post_id = ?", [$id]);
	}

	function sponsorpicupdate($id) {
		global $db, $account;

		if (!class_exists('upload'))
			return;

		$handle = new upload($_FILES['sponsorpic']);
		if ($handle->uploaded) {

			$res = $db->q("SELECT images FROM `classifieds_sponsor` WHERE post_id = '$id' limit 1");
			if ($db->numrows($res)) {
				$row = $db->r($res);
				if( !empty($row['images']) ) {
					@unlink(self::getUploadDir()."s/".$row['images']);
					$db->q("update classifieds_sponsor set images = '' where post_id = '$id'");
				}
			}

			$handle->image_convert = "jpg";
			$handle->image_resize		  = true;
			$handle->image_ratio_crop	  = 'T';
			$handle->image_y			   = 60;
			$handle->image_x			   = 60;
			$handle->file_overwrite		= false;
			$handle->file_new_name_body = $id . "_" . $account->_getCookieHash(10, 1);
			$handle->Process(self::getUploadDir()."/s/");
			if($handle->processed){
				$db->q("update classifieds_sponsor set images = '{$handle->file_dst_name}' where post_id = '$id'");
				return true;
			}
		}
		return;
	}

	/**
	 * Reposts ad by id
	 * If free_repost is set, then repost is done even if account doesnt have top-up credit
	 */
	public static function repost($id, $free_repost = false) {
		global $account, $db;

		$now = time();

		//get number of global repost credits and classified data
		$repost = 0;
		$res = $db->q("
			SELECT a.repost, c.expire_stamp, c.expires, c.auto_renew_fr, c.auto_renew_time, c.account_id, c.auto_repost, c.done, COUNT(cl.id) as cnt
			FROM classifieds c 
			INNER JOIN classifieds_loc cl on cl.post_id = c.id
			LEFT JOIN account a on a.account_id = c.account_id
			WHERE c.id = ?
			GROUP BY c.id",
			[$id]
			);
		if (!$db->numrows($res))
			return false;
		$row = $db->r($res);
		$repost = intval($row["repost"]);
		$auto_repost = $row["auto_repost"];
		$auto_renew_fr = intval($row["auto_renew_fr"]);
		$auto_renew_time = intval($row["auto_renew_time"]);
		$expire_stamp = $row["expire_stamp"];
		$expires = $row["expires"];
		$account_id = $row["account_id"];
		$done = $row["done"];
		$locationCount = intval($row["cnt"]);
		
		if ($done < 0 || $done > 1)
			return false;	//ad is to be removed or ad is invisible -> cant repost

		if (!$expire_stamp)
			$expire_stamp = strtotime($expires);

		if ($repost <= 0) {
			//we dont have top-up credit
			if (!$free_repost)
				return false;
		} else if ($repost < $locationCount) {
			//we dont have enough oftop-up credits for all the ad locations
			//because we need 1 repost credit for each location of the ad
			if (!$free_repost)
				return false;
		} else {
			//substract 1 top-up credit for each location
			$db->q("UPDATE account SET repost = repost - ? WHERE account_id = ? LIMIT 1", [$locationCount, $account_id]);
		}

		$time_next = null;
		if ($auto_repost)
			$time_next = self::compute_next_repost_stamp($id, $auto_renew_time, $auto_renew_fr);

		$db->q("UPDATE classifieds SET done = 1, date = NOW(), time_next = ? WHERE id = ? LIMIT 1", [$time_next, $id]);
		$db->q("UPDATE classifieds_loc SET done = 1, updated = NOW() WHERE post_id = ?", [$id]);

		//log this repost into repost log
		$author_id = intval($account->isloggedin());
		$db->q("
			INSERT INTO repost_log 
			(classified_id, location_count, day, stamp, author_id) 
			VALUES 
			(?, ?, CURDATE(), ?, ?)
			", 
			[$id, $locationCount, $now, $author_id]
			);

		self::rotateIndex();

		return true;
	}

	/**
	 * Used to prolong ad by week, used on non-BP ads which have been visited directly from backpage, called from cron/classifieds and look.php
	 */
	public static function prolong($id, $days, $updated_in_past = false) {
		global $db;

		$days = intval($days);
		if ($days == 0)
			return false;

		if ($updated_in_past) {
			$updated = "date_sub(now(), interval 90 day)";
		} else {
			$updated = "NOW()";
		}

		$db->q("update classifieds set done = 1, `date` = {$updated}, expires = date_add(now(), interval {$days} day) where id = '{$id}'");
		$db->q("update classifieds_loc set done = 1, updated = {$updated} where post_id = '{$id}'");
		$db->q("insert into classifieds_stat (time, top) values (curdate(), 1) on duplicate key update top = top + 1");

		return true;
	}

	public static function getNumberLiveAdsByAccountId($account_id) {
		global $db;
		$res = $db->q("SELECT count(*) as cnt FROM classifieds WHERE done = 1 AND account_id = ?", array($account_id));
		if (!$db->numrows($res))
			return 0;
		$row = $db->r($res);
		$cnt = intval($row["cnt"]);
		return $cnt;
	}

	function createThumb($id, $ct = NULL) {
		global $db;

		$pic = $id.".jpg";
		if( !file_exists(self::getUploadDir().$pic) ) {
			if( !class_exists('Upload') ) {
				reportAdmin("classifieds::createThumb() : Class Upload does not exist", "", array("id" => $id, "ct" => $ct));
				debug_log("classifieds::createThumb() Error: Class Upload does not exist!");
				return;
			}

			$res = $db->q("SELECT filename FROM `classifieds_image` WHERE id = '$id' order by image_id limit 1");
			if (!$db->numrows($res)) {
				debug_log("classifieds::createThumb() Error: no image found for ad id#{$id} !");
				return false;
			}
			$row = $db->r($res);
			$handle = new Upload(self::getUploadDir().$row["filename"]);
			if ($handle->uploaded) {

				$w = $handle->image_src_x;
				$h = $handle->image_src_y;
				if ($w > $h) {
					$right_crop = $w - $h;
					$handle->image_precrop = array(0, $right_crop, 0, 0);
				} else if ($h > $w) {
					$bottom_crop = $h - $w;
					$handle->image_precrop = array(0, 0, $bottom_crop, 0);
				}
				$handle->image_convert = "jpg";
				$handle->image_resize		  = true;
				$handle->image_y			   = 75;
				$handle->image_x			   = 75;
				$handle->file_overwrite		= true;
				$handle->file_new_name_body = $id;
				$ret = $handle->process(self::getUploadDir());
				if($handle->processed){
					$db->q("update classifieds set thumb = '$handle->file_dst_name' where id = '$id'");
					return true;
				}
			}
		} elseif(empty($ct)) {
			debug_log("classifieds::createThumb(): Pic for ad id#{$id} already exists: '{$pic}' !");
			$db->q("update classifieds set thumb = '$pic' where id = '$id'");
		}

		return false;
	}

	private static function getNewThumbnailFilename($curr_thumb, $clad_id) {
		global $config_image_path;

		$filename_base = "{$clad_id}";
		$i = 0;
		do {
			$filename = $filename_base;
			if ($i)
				$filename .= "_{$i}";
			$filename .= ".jpg";
			$filepath = $config_image_path."classifieds/{$filename}";
			//we loop through names until filename is not current one and not already existing on disk 
			//(this means providing we dont delete old thumbnails, thumbnail has always new name, so its not cached by browser and therefore change displays immediately)
			$i++;
		} while (($curr_thumb && $curr_thumb == $filename) || file_exists($filepath));

		return $filename;
	}

	/*
	 * (re)-creates thumbnail for specific ad
	 * by default it takes first image, or source image file path can be specified
	 */
	public static function createThumbnail($clad_id, $src_filepath = null) {
		global $db, $config_image_path;

		//get current thumbnail
		$res = $db->q("SELECT thumb FROM classifieds WHERE id = ? LIMIT 1", [$clad_id]);
		if ($db->numrows($res) != 1)
			return false;
		$row = $db->r($res);
		$curr_thumb = $row["thumb"];

		if ($curr_thumb) {
			//if thumb is set, lets check if the filename exists, if not, unset thumbnail in db
			$curr_thumb_filepath = $config_image_path."classifieds/{$curr_thumb}";
			if (!file_exists($curr_thumb))
				$db->q("UPDATE classifieds SET thumb = NULL WHERE id = ? LIMIT 1", [$clad_id]);
		}

		//generate new thumbnail filename
		$filename = self::getNewThumbnailFilename($curr_thumb, $clad_id);
		$filepath = $config_image_path."classifieds/{$filename}";

		//if source file is not specified, get first image
		if (!$src_filepath) {
			$res = $db->q("SELECT filename FROM classifieds_image WHERE id = ? AND width > 0 ORDER BY image_id LIMIT 1", [$clad_id]);
			if ($db->numrows($res) != 1)
				return false;
			$row = $db->r($res);
			$src_filepath = $config_image_path."classifieds/{$row["filename"]}";
		}
		//check if source filepath exists
		if (!file_exists($src_filepath))
			return false;

		//create thumbnail with SimpleImage
		require_once(_CMS_ABS_PATH."/inc/classes/SimpleImage.php");
		$simpleimage = new \claviska\SimpleImage();
		try {
			$simpleimage
				->fromFile($src_filepath)
				->thumbnail(75, 75, "center")
				->toFile($filepath, 'image/jpeg');
		} catch (\Exception $e) {
			return false;
		}

		$res = $db->q("UPDATE classifieds SET thumb = ? WHERE id = ? LIMIT 1", [$filename, $clad_id]);
		$aff = $db->affected($res);
		if ($aff != 1)
			return false;

		return $filename;
	}

	/*
	 * (re)-creates multiple thumbnails for specific ad
	 * it creates thumbnails for all images on classified ad
	 * also sets "classic" clad thumbnail for the first of the generated thumbnails
	 */
	public static function createMultipleThumbnails($clad_id, $src_filepath = null) {
		global $db, $config_image_path;

		require_once(_CMS_ABS_PATH."/inc/classes/SimpleImage.php");

		//get current thumbnail
		$res = $db->q("SELECT thumb FROM classifieds WHERE id = ? LIMIT 1", [$clad_id]);
		if ($db->numrows($res) != 1)
			return false;
		$row = $db->r($res);
		$curr_thumb = $row["thumb"];

		if ($curr_thumb) {
			//if thumb is set, lets check if the filename exists, if not, unset thumbnail in db
			$curr_thumb_filepath = $config_image_path."classifieds/{$curr_thumb}";
			if (!file_exists($curr_thumb))
				$db->q("UPDATE classifieds SET thumb = NULL WHERE id = ? LIMIT 1", [$clad_id]);
		}

		$first_filename = $multiple_thumbs = null;
		//--------------
		//for all images
		$res = $db->q("SELECT filename FROM classifieds_image WHERE id = ? ORDER BY image_id", [$clad_id]);
		if ($db->numrows($res) == 0)
			return false;
		while ($row = $db->r($res)) {
			$src_filepath = $config_image_path."classifieds/{$row["filename"]}";
			//check if source filepath exists
			if (!file_exists($src_filepath))
				return false;

			//generate new thumbnail filename
			$filename = self::getNewThumbnailFilename($curr_thumb, $clad_id);
			$filepath = $config_image_path."classifieds/{$filename}";

			//create thumbnail with SimpleImage
			$simpleimage = new \claviska\SimpleImage();
			try {
				$simpleimage
					->fromFile($src_filepath)
					->thumbnail(75, 75, "center")
					->toFile($filepath, 'image/jpeg');
			} catch (\Exception $e) {
				return false;
			}
		
			if (is_null($first_filename))
				$first_filename = $filename;

			$multiple_thumbs = (!$multiple_thumbs) ? $filename : "{$multiple_thumbs},{$filename}";
		}

		$res = $db->q("UPDATE classifieds SET thumb = ?, multiple_thumbs = ? WHERE id = ? LIMIT 1", [$first_filename, $multiple_thumbs, $clad_id]);
		$aff = $db->affected($res);
		if ($aff != 1)
			return false;

		return $first_filename;
	}

	/**
	 * Hardcode prolongation, only accessible by real admin to month from now
	 */
	function adminPost($id) {
		global $account, $db;

		$db->q("update classifieds set done = 1, `date` = now(), expires = date_add(now(), interval 30 day), time_next = unix_timestamp(expires) where id = ?", array($id));
		$db->q("update classifieds_loc set done = 1, updated = now() where post_id = ?", array($id));
		audit::log("CLA", "Prolong", $id, "One month", $account->getId());	
	}

	/**
	 * Returns inverse (?) time offset in minutes from PST and timezone abbreviation for specified ad
	 * If ad is posted in multiple cities, which are in different timezones, returns default timezone (PST)
	 */
	function timezone($id, &$time, &$timezone) {
		global $db;

		$res = $db->q("
			SELECT time, timezone 
			FROM classifieds_loc l 
			INNER JOIN timezone t on l.state_id = t.loc_id 
			WHERE l.post_id = ?
			GROUP BY l.state_id",
			array($id));
		if (!$db->numrows($res)) {
			$time = $this->default_time;
			$timezone = $this->default_timezone;
			return;
		}
		$c = 0;
		while ($row = $db->r($res)) {
			if ($c && $time != $row['time']) {
				$time = $this->default_time;
				$timezone = $this->default_timezone;
				return;
			}
			$c++; 
			$time = $row['time'];
			$timezone = $row['timezone'];
		}
	}

	/**
	 * Gets next repost UTC timestamp based on ad's location's timezone
	 * If ad is posted in multiple cities, it takes timezone of first location
	 */
	public static function compute_next_repost_stamp($clad_id, $auto_renew_time, $auto_renew_fr) {
		global $db;

		$res = $db->q("
			SELECT timezone 
			FROM classifieds_loc l 
			INNER JOIN timezone t on l.state_id = t.loc_id 
			WHERE l.post_id = ?
			LIMIT 1",
			[$clad_id]
			);
		if (!$db->numrows($res)) {
			//if we can't find timezone for this location, its going to be PST
			$timezone = "PST";
		} else {
			$row = $db->r($res);
			$timezone = $row["timezone"];
		}

		$hours = floor($auto_renew_time / 60);
		$minutes = $auto_renew_time - ($hours * 60);
		
		$nr = new \DateTime("tomorrow", new \DateTimeZone($timezone));
		if ($auto_renew_fr > 1)
			$nr->modify("+".($auto_renew_fr - 1)." day");
		$nr->setTime($hours, $minutes);
		$stamp = $nr->getTimestamp();

		//double check if timestamp is after now (or otherwise we will waste all the reposts as we will keep reposting today again if we set stamp to past)
		if ($stamp < time())
			$stamp = time() + 24*3600;

		return $stamp;
	}

	function createpromo($promo, $user) {
		global $db;

		switch($promo) {
			case '30': $p = 1; break;
			case '50': $p = 2; break;
			case 'FREEAD': 
			case 'FREE': 
				$p = 3; break;
			case 'HIGHLIGHT': $p = 4; break;
			case 'RECURRING': $p = 5; break;
			default:
				return NULL;
		}
		$id = 0;
		do {
			$code = rand(100000, 999999);
			if( $p == 1 /* 30% */ ) {
				$db->q("insert ignore into classifieds_promocodes (code, section, `left`, can_auto_renew, percentage) values ('$code', 
'classifieds', 1, 1, 30)");
			}
			elseif( $p == 2 /* 50% */ ) {
				$db->q("insert ignore into classifieds_promocodes (code, section, `left`, can_auto_renew, percentage) values ('$code',
'classifieds', 1, 1, 50)");
			}
			elseif( $p == 3 /* FREEAD */ ) {
				$db->q("insert ignore into classifieds_promocodes (code, section, `left`, free, day) values ('$code', 'classifieds', 1, 1, 14)");
			}
			elseif( $p == 4 /* HIGHLIGHT */ ) {
				$db->q("insert ignore into classifieds_promocodes (code, section, `left`, discount, can_auto_renew) values ('$code', 'classifieds', 
1,'0.01', 1)");
			} elseif( $p == 5 /* RECURRING */ ) {
				$db->q("insert ignore into classifieds_promocodes (code, section, `left`, discount, recurring, day) values ('$code', 'classifieds', 
1, 5, 1, 30)");
			}

			if( ($error=mysql_error()) ) {
				reportAdmin($error);
				return NULL;
			}
			$id = $db->insertid;
		} while( !$id );

		return $code;
	}

	function blockedContent($new, $old, &$error = NULL) {
		if( !is_array($this->blocked_urls) ) return 0;
		foreach($this->blocked_urls as $url) {
			if( strstr($new, $url) && !strstr($old, $url)) {
				//$error = "You are not allowed to post an ad to promote the following url: $url. For more info please contact to support@adultsearch.com";
				//reportAdmin($error);
				return 1;
			}
		}
		return 0;
	}

	//returns true if user is able to post ad which contain forbidden URLs
	function shouldAllow($account_id) {
		global $account;

		if (!$account_id)
			return;

		if ($account->isadmin())
			$this->shouldAllowUser = true;

		return;
	}

	function getTypeByModule($module) {
		$type = NULL;
		switch($module) {
			case 'female-escorts': $type = 1; break;
			case 'tstv-shemale-escorts': $type = 2; break;
			case 'body-rubs': $type = 6; break;
			case 'bdsm-fetish-escorts': $type = 13; break;
			case 'strippers-for-hire': $type = 12; break;
			case 'male-escorts-for-female': $type = 3; break;
			case 'male-for-male-escorts': $type = 4; break;
			case 'male-for-male-body-rubs': $type = 7; break;
			case 'adult-help-wanted': $type = 14; break;
			case 'escort-agencies': $type = 5; break;
			case 'phone-and-web': $type = 16; break;
		}
		return $type;
	}

	public static function getModuleByType($type) {
		$module = NULL;
		switch($type) {
			case 1: $module = 'female-escorts'; break;
			case 2: $module = 'tstv-shemale-escorts'; break;
			case 6: $module = 'body-rubs'; break;
			case 13: $module = 'bdsm-fetish-escorts'; break;
	   		case 12: $module = 'strippers-for-hire'; break;
	   		case 3: $module = 'male-escorts-for-female'; break;
	   		case 4: $module = 'male-for-male-escorts'; break;
	   		case 7: $module = 'male-for-male-body-rubs'; break;
	   		case 14: $module = 'adult-help-wanted'; break;
	   		case 5: $module = 'escort-agencies'; break;
			case 16: $module = 'phone-and-web'; break;
		}
		return $module;
	}

	public function gethomepricebyloc($loc_id) {
		//TODO different prices of city thumbnail per location
		return classifieds::get_city_thumbnail_upgrade_price();
	}

	public static function getcatnamebytype($type) {
		$module = "";
		switch($type) {
			case 1: $module = "Female Escorts"; break;
			case 2: $module = "TS/TV Escorts"; break;
			case 3: $module = "Male For Female"; break;
			case 4: $module = "Male For Male Escorts"; break;
			case 5: $module = "Escort Agency"; break;
			case 6: $module = "Body Rubs"; break;
			case 7: $module = "Male For Male Body Rubs"; break;
			case 12: $module = "Strippers for Hire"; break;
			case 13: $module = "BDSM / Fetish"; break;
			case 14: $module = "Adult Help Wanted"; break;
			case 16: $module = "Phone & Web"; break;
		}
		return $module;
	}

	function featuredads($loc_id, $type = 1) {
		global $db, $smarty, $config_image_server;

		$now = time();
		$res = $db->q("(SELECT n.images, l.country_sub, l.loc_url, c.type, c.id, c.thumb, c.available, c.ethnicity 
						FROM classifieds_sponsor n 
						INNER JOIN classifieds c on n.post_id = c.id 
						INNER JOIN location_location l on n.loc_id = l.loc_id 
						WHERE n.expire_stamp > {$now} and n.done > 0 and n.loc_id = '$loc_id' and n.type = '$type' 
						ORDER BY rand() 
						LIMIT 8) 
						UNION
						(SELECT '', l.country_sub, l.loc_url, c.type, c.id, c.thumb, c.available, c.ethnicity 
						FROM classifieds c
						INNER JOIN (classifieds_loc n inner join location_location l on n.loc_id = l.loc_id) on c.id = n.post_id 
						WHERE c.done = 1 and c.bp = 0 and c.total_loc < 5 and n.loc_id = '$loc_id' and n.type = '$type' 
						ORDER BY c.id desc 
						LIMIT 8) 
						LIMIT 8");

		if (!$db->numrows($res))
			return;

		$featuredads = NULL;
		$form = new form;
		while ($row=$db->r($res)) {

			if (!empty($row['images']))
				$row["avatar"] = $config_image_server."/classifieds/s/{$row["images"]}";
			else if (!empty($row['thumb']))
				$row["avatar"] = $config_image_server."/classifieds/{$row["thumb"]}";

			$info1 = $row['available']==1?"Incall":($row['available']==2?"Outcall":"In/Out Call");
			$info2 = $form->arrayValue($ethnicity_array, $row['ethnicity']);
			$info = $info1 . (!empty($info1)&&!empty($info2)?", ":"") . $info2;

			$module = $this->getcatnamebytype($row['type']);
			$sub = isset($row['country_sub']) && !empty($row['country_sub']) ? "{$row['country_sub']}." : "";
			$cat = $this->getModuleByType($row['type']);
			$link = isset($row['link']) && !empty($row['link']) ? $row['link'] : "http://{$sub}adultsearch.com{$row['loc_url']}{$cat}/{$row['id']}";
				
			$featuredads[] = array(
				"category"=>$module,
				"avatar"=>$row["avatar"],
				"link"=>htmlspecialchars($link),
				"what"=>$what,
				"info"=>$info
			);
		}
		$smarty->assign("featuredads", $featuredads);
	}

	/*
	/  new adbuild section
	*/

	// ad is being built/changed, lets handle init the process
	public function adbuild_init($createnew = false) {
		global $db, $account, $smarty;
		$system = new system;

		$this->adbuild_id = isset($_REQUEST["ad_id"]) ? intval($_REQUEST["ad_id"]) : NULL;

		if( $this->adbuild_id ) {
			$res = $db->q("select * from classifieds where id = '{$this->adbuild_id}'");
			if (!$db->numrows($res))
				$system->moved("/adbuild/");
			$this->adbuild_row = $db->r($res);

			if( !$this->adbuild_haspermission() ) {
				$this->adbuild_id = $this->adbuild_row = NULL;
				$system->moved("/adbuild/");
			} else {
				$this->adbuild_type = $this->adbuild_row['type'];

				$res = $db->q("select loc_id from classifieds_loc where post_id = '{$this->adbuild_id}'");
				while($row=$db->r($res)) {
					$this->adbuild_locs[] = $row['loc_id'];
				}

				// ad is live. no need to show every step or require a payment...
				if( $this->adbuild_row['done'] > 0 )
					$this->adbuild_editonly = true;

				// todo - $classifieds is null!!

				if( $createnew && $createnew != $classifieds->adbuild_type && !$classifieds->adbuild_editonly ) {
					$this->adbuild_type = $this->adbuild_row['type'] = $createnew;
					$db->q("UPDATE classifieds SET type = ? WHERE id = ?", [$createnew, $this->adbuild_id]);
					$db->q("UPDATE classifieds_loc SET type = ? WHERE post_id = ?", [$createnew, $this->adbuild_id]);
					$db->q("UPDATE classifieds_sponsor SET type = ? WHERE post_id = ?", [$createnew, $this->adbuild_id]);
					$db->q("UPDATE classifieds_side SET type = ? WHERE post_id = ?", [$createnew, $this->adbuild_id]);
					$db->q("UPDATE classified_sticky SET type = ? WHERE classified_id =?", [$createnew, $this->adbuild_id]);
				}
			}
		} elseif( $this->adbuild_step > 2 || ($this->adbuild_step == 2 && !$createnew) ) {
			$system->moved("/adbuild/?m");
		}

		require_once(_CMS_ABS_PATH."/_cms_files/classifieds/array.php"); 
		$form = new form;

		if ($createnew && !$this->adbuild_id && isset($this->adbuild_type_array[$createnew])) {
			//we are creating new ad

			if (!$this->posting_new_ad_allowed())
				$system->moved("/adbuild/?error=".urlencode("You can't post another ad, please contact us at support@adultsearch.com"));

			$this->adbuild_type = $createnew;
			$db->q("INSERT INTO classifieds 
					(type, loc_id, account_id, title, date, content, created) 
					VALUES 
					(?, ?, ?, '', NOW(), '', ?)", 
					[$this->adbuild_type, 0, intval($account->isloggedin()), time()]
					);
			$this->adbuild_id = $db->insertid();

			// non registered visitor, create some session for the hottie(not likely but hopefully :P)....
			if( $this->adbuild_id && !$account->isloggedin() ) {
				$_SESSION["cl_owner_".$this->adbuild_id] = true;
			}

			// from now on we should be parsing the ad ID through the URLs 
			$system->moved("/adbuild/step2?ad_id={$this->adbuild_id}");
		}
		if( $this->adbuild_type ) {
			$this->adbuild_cat_name = $form->arrayvalue($this->adbuild_type_array, $this->adbuild_type);
		}

		$smarty->assign("cat_name", $this->adbuild_cat_name);
		$smarty->assign("ad_id", $this->adbuild_id);
		$smarty->assign("cat_id", $this->adbuild_type);
		$smarty->assign("adbuild_type", $this->adbuild_type);

		$this->adbuild_step_check();
		if (isset($_POST['updatethead']))
			$this->adbuild_updating = true;

		if (!$this->adbuild_editonly)
			$this->adbuild_cart_process();
		else 
			$smarty->assign('editonly', true);

		$smarty->assign("adbuild_step", $this->adbuild_step);

		if (self::isCladPoster())
			$smarty->assign("clad_poster", true);

		$smarty->assign("not_posted_yet", (!is_array($this->adbuild_row) || ($this->adbuild_row['done'] < 0)));

		return $this->adbuild_id;
	}

	public static function get_cl_owner() {
		$cladIds = [];
		foreach ($_SESSION as $key => $val) {
			if (substr($key, 0, 9) == "cl_owner_" && $val)
				$cladIds[] = intval(substr($key, 9));
		}
		if (empty($cladIds))
			return false;
		if (count($cladIds) == 1)
			return $cladIds[0];
		$requestCladId = null;
		if (isset($_REQUEST["ad_id"]) && intval($_REQUEST["ad_id"]))
			$requestCladId = intval($_REQUEST["ad_id"]);
		if ($requestCladId && in_array($requestCladId, $cladIds))
			return $requestCladId;
		return $cladIds[0];
	}

	//this function returns true if we are allowed to post an ad, false otherwise
	public function posting_new_ad_allowed() {
		global $account, $db;

		if ($account->isAdmin())
			return true;

		$limit = null;
		if (!is_null($account->getAdsLimit()))
			$limit = $account->getAdsLimit();
		else if ($account->isWhitelisted() || $account->isAgency())
			$limit = 10;
		else
			$limit = 2;
	
		$cnt = $db->single("SELECT count(*) as cnt FROM classifieds WHERE account_id = ? AND done > -1 AND deleted IS NULL", [$account->getId()]);

		if ($cnt >= $limit)
			return false;

		//we are not allowed to post a new ad if we have any ad in waiting state (done=3)
		$cnt = $db->single("SELECT count(*) as cnt FROM classifieds WHERE account_id = ? AND done = 3 AND deleted IS NULL", [$account->getId()]);
		if ($cnt > 0)
			return false;

		return true;
	}

	private function adbuild_step_check() {
		global $account;

		if( $this->adbuild_editonly && !in_array($this->adbuild_step, array(3,45,5)) ) {

			// admin user should be able to change things anytime... should they ? 
			if ($account->isadmin())
				return;

			$system = new system; 
			if ($this->adbuild_id)
				$system->moved("/adbuild/step3?ad_id={$this->adbuild_id}");
			$system->moved("/");
		}
	}

	private function adbuild_haspermission() {
		global $account;

		// not sure if this check is necessary yet ...
		if (!$this->adbuild_id || empty($this->adbuild_row)) {
			$system->moved("/adbuild/");
			return false;
		}

		//are we owner of the ad ?
		$account_id = intval($account->isloggedin());

		if (!empty($this->adbuild_row["account_id"]) && $account_id == $this->adbuild_row["account_id"])
			return true;

		//non registered visitor is creating the ad....
		else if (empty($this->adbuild_row["account_id"]) && isset($_SESSION["cl_owner_".$this->adbuild_id]))
			return true;

		//we have permission to edit ads
		//TODELETE
		else if (permission::has("edit_all_ads"))
			return true;

		//we have permission to edit ads
		else if (permission::has("classified_edit"))
			return true;

		//admin users should have a full access on the ads..
		else if ($account->isadmin())
			return true;

		return false;
	}

	public function adbuild_imgload() {
		global $db, $config_image_server;

		if (!$this->adbuild_id)
			return NULL;

		$img = NULL;
		$res = $db->q("SELECT *  FROM `classifieds_image` WHERE `id` = '{$this->adbuild_id}' order by image_id");
		while($row=$db->r($res)) {
			$url = "{$config_image_server}/classifieds/{$row['filename']}";
			$img[] = array(
				"name"=>$row['filename'], 
				"size"=>"",
				"url"=>$url,
				"thumbnail_url"=>str_replace('/classifieds/','/classifieds/t/',$url), 
				"delete_url"=>"/adbuild/upload?ad_id={$this->adbuild_id}&delete={$row['image_id']}",
				"delete_type"=>"DELETE"
				);
		}

		return $img;
	}

	public function adbuild_imgdel($image_id) {
		global $db;

		if( !$this->adbuild_id ) return NULL;

		$res = $db->q("select filename from classifieds_image where image_id = '$image_id' and id = '{$this->adbuild_id}'");
		if( !$db->numrows($res) ) {
			return;
		}
		$row = $db->r($res);

		@unlink(self::getUploadDir().$row["filename"]);
		@unlink(self::getUploadDir()."t/".$row["filename"]);
		$db->q("delete from classifieds_image where image_id = '$image_id'");
		if( $this->adbuild_row["thumb"] == ($this->adbuild_id.".jpg") && file_exists(self::getUploadDir().$this->adbuild_row["thumb"]) ) {
			@unlink(self::getUploadDir().$this->adbuild_row["thumb"]);
			$db->q("update classifieds set thumb = '' where id = '{$this->adbuild_id}'");
			$this->createThumb($this->adbuild_id);
		}

		return;
	}

	/**
	 * This method is called from _cms_files/adbuild/upload.php which just handles multiple file upload and calls this method to do all the image stuff
	 * $i = image_id to write on a current image_id instead of a new row to keep the order as they are uploaded..
	 */
	public function adbuild_upload($file, $i = NULL) {

		global $db, $config_image_server; $system = new system;

		if( !$this->adbuild_id ) return NULL;

		if( !class_exists('Upload') ) {
			require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");
		}

		$f = $_FILES[$file][0];
		$handle = new Upload($f);

		$num_pic = $this->getNumPic($this->adbuild_id);
		$create_thumb = false;

		if ($handle->uploaded && $num_pic < $this->picture_limit) {
			list($width, $height, $t, $attr) = getimagesize($handle->file_src_pathname);
			if ($height > $width && $height > 632) {
				$handle->image_resize = true;
				$handle->image_ratio_x = true;
				$handle->image_y = 632;
			} else if ($width >= $height && $width > 600) {
				$handle->image_resize = true;
				$handle->image_ratio_y = true;
				$handle->image_x = 600;
			}
			$handle->image_convert = "jpg";
			$text = $system->_makeText(20);
			$handle->file_new_name_body = $this->adbuild_id . "_" . $text;
			$handle->Process(self::getUploadDir());
			if ($height > $width)
				$xx = "h";
			else
				$xx = "w";

			if (!$handle->processed)
				die("$handle->error $handle->file_src_size $handle->file_max_size error");

			if ($i < 5) {
				if ($i == 1)
					$create_thumb = true;
				$i = "NULL";
			}

			$db->q("INSERT IGNORE INTO classifieds_image 
					(image_id, id, filename, x, width, height, uploaded_stamp)
					values
					(?, ?, ?, ?, ?, ?, ?)",
					array($i, $this->adbuild_id, $handle->file_dst_name, $xx, $width, $height, time())
					);

			if ($db->insertid < 1) {
				$db->q("INSERT INTO classifieds_image 
						(id, filename, x, width, height, uploaded_stamp) 
						values 
						(?, ?, ?, ?, ?, ?)",
						array($this->adbuild_id, $handle->file_dst_name, $xx, $width, $height, time())
						);
			}
			$i = $db->insertid;
			$name = $handle->file_dst_name;

			/* lets see if there is an other image for this ad with small image_id to make sure that we have thumbnail.. */
			$rex = $db->q("select image_id from classifieds_image where image_id < '$i' and id = '{$this->adbuild_id}'");
			if (!$db->numrows($rex))
				$create_thumb = true;

			$handle->image_convert = "jpg";
			$handle->image_resize = true;
			$handle->image_x = 200;
			$handle->image_ratio = true;
			//$handle->image_y = NULL;
			$handle->file_new_name_body = $this->adbuild_id . "_" . $text;
			$handle->Process(self::getUploadDir()."t");
			
			if( $create_thumb ) {
				$handle->image_convert = "jpg";
				$handle->image_resize		  = true;
				$handle->image_ratio_crop	  = 'T';
				$handle->image_y			   = 75;
				$handle->image_x			   = 75;
				$handle->file_overwrite		= true;
				$handle->file_new_name_body = $this->adbuild_id;
				$handle->Process(self::getUploadDir());
				$this->setThumb($this->adbuild_id, $this->adbuild_id.".jpg");
			}
			$handle->Clean();
		
			return array(
				"name"=>$name,
				"size"=>$f['size'],
				"type"=>$f['type'], 
				"url"=>"{$config_image_server}/classifieds/t/{$name}",
				"thumbnail_url"=>"{$config_image_server}/classifieds/t/{$name}", 
				"delete_url"=>"http://adultsearch.com/adbuild/upload?ad_id={$this->adbuild_id}&delete={$i}",
				"delete_type"=>"DELETE"
				);

		} else if( $num_pic >= $this->picture_limit )
			$handle->Clean();
		return array("name"=>$f['name']);

	}

	private function add_location($loc_id) {
		global $db, $smarty, $account;

		if (is_array($this->adbuild_locs) && in_array($loc_id, $this->adbuild_locs))
			return false;	//we already have this location

		$whitelisted = false;
		if ($account->isloggedin() && $account->isWhitelisted())
			$whitelisted = true;

		if (!$whitelisted && (count($this->adbuild_locs) >= $this->adbuild_loclimit)) {
			if (!$account->isloggedin())
				$smarty->assign("localert", 1);
			else
				$smarty->assign("localert", 2);
			return false;	//we cant add more locations
		}

		//lets check, if we're not adding location from many different states
		$res = $db->q("SELECT loc_parent FROM location_location WHERE loc_id = ?", array($loc_id));
		if ($db->numrows($res) != 1) {
			$smarty->assign("localert", 9);
			return false;
		}
		$row = $db->r($res);
		$loc_parent = $row["loc_parent"];

		$ids = "";
		foreach ($this->adbuild_locs as $loc) {
			$ids .= (empty($ids)) ? "" : ",";
			$ids .= $loc;
		}
		$parents = array();
		if ($ids) {
			$res = $db->q("SELECT DISTINCT loc_parent FROM location_location WHERE loc_id IN ({$ids})");
			if ($db->numrows($res) == 0) {
				$smarty->assign("localert", 9);
				return false;
			}
			while ($row = $db->r($res)) {
				$parents[] = $row["loc_parent"];
			}
			if (!$whitelisted && !in_array($loc_parent, $parents) && count($parents) >= $this->adbuild_locparentlimit) {
				$smarty->assign("localert", 3);
				return false;	//we cant add more locations from different states
			}
		}

		//were good
		$this->adbuild_locs[] = $loc_id;
		return true;
	}

	public function adbuild_cart_process() {

		global $db, $smarty, $account, $mcache;

		if( !$this->adbuild_id || $this->adbuild_editonly ) return;

		$now = time();
		$total = 0;
		$picked = NULL;
		if( !$this->adbuild_updating ) {
			$updateloc = false;

			//adding or removing a location (ajax call from step2 to ./_cms_files/adbuild/loc.php)
			if (isset($_POST['addloc']) && intval($_POST['addloc'])) {
				$updateloc = $this->add_location(intval($_POST['addloc']));
			} elseif (isset($_POST['removeloc']) 
				&& intval($_POST['removeloc']) 
				&& in_array(intval($_POST['removeloc']), $this->adbuild_locs)
				&& count($this->adbuild_locs) > 1
				) {
				foreach($this->adbuild_locs as $key=> $loc) {
					if( $loc == intval($_POST['removeloc']) ) { 
						unset($this->adbuild_locs[$key]);
						$updateloc = true;
						break;
					}
				}
			}

			//if there were changes in locations, flush and fill classifieds_loc tbale for this ad
			if ($updateloc) {
				$res = $db->q("DELETE FROM classifieds_loc WHERE post_id = ?", array($this->adbuild_id));
				if( is_array($this->adbuild_locs) && count($this->adbuild_locs) ) {
					foreach($this->adbuild_locs as $loc) {
						$res = $db->q("INSERT INTO classifieds_loc
								(loc_id, state_id, type, post_id)
								values
								('$loc', (select loc_parent from location_location where loc_id = '$loc'), '{$this->adbuild_type}', '{$this->adbuild_id}')");
					}
				}
			}
	
			if( isset($_POST['removecover']) ) {
				$loc = intval($_POST['removecover']);
				$db->q("delete from classifieds_sponsor where post_id = '{$this->adbuild_id}' and loc_id = '$loc' and done < 0");
			}

			if( isset($_POST['cancelcover']) ) {
				$db->q("delete from classifieds_sponsor where post_id = '{$this->adbuild_id}' and done < 0");
			}

			if (isset($_POST['cancelside'])) {
				$db->q("DELETE FROM classifieds_side WHERE post_id = ? and done < 0", array($this->adbuild_id));
			}
			if (isset($_POST['removeside'])) {
				$loc_id = intval($_POST['removeside']);
				$db->q("DELETE FROM classifieds_side WHERE post_id = ? and loc_id = ? and done < 0", array($this->adbuild_id, $loc_id));
			}

			if (isset($_POST['cancelsticky'])) {
				$db->q("DELETE FROM classified_sticky WHERE classified_id = ? and done < 0", array($this->adbuild_id));
			}
			if (isset($_POST['removesticky'])) {
				$loc_id = intval($_POST['removesticky']);
				$db->q("DELETE FROM classified_sticky WHERE classified_id = ? and loc_id = ? and done < 0", array($this->adbuild_id, $loc_id));
			}

			if( isset($_POST['cancelrepost']) ) {
			} elseif( isset($_POST['repost']) && $_POST['repost'] == 1 ) {
				$art = intval($_POST['art']);
				$db->q("update classifieds set auto_renew_time='$art' where id = '{$this->adbuild_id}'");
			} elseif( isset($_POST['repost']) && $_POST['repost'] == 2 ) {
				$art = intval($_POST['art']);
				$fr = intval($_POST['fr']);
				$ar = intval($_POST['ar']);
				$db->q("update classifieds set auto_renew_time='$art',auto_renew_fr='$fr' where id = '{$this->adbuild_id}'");
				$this->adbuild_row['auto_renew_fr'] = $fr;
				$this->adbuild_row['auto_renew_time'] = $art;
			} elseif( isset($_POST['cancelcoupon']) ) {
				$this->adbuild_row['promo'] = "";
				$this->adbuild_row['promo_code'] = "";
				$db->q("update classifieds set promo_code = '', promo = 0 where id = '{$this->adbuild_id}'");
			}

			if (isset($_POST['homespx']) ) {
				$db->q("DELETE FROM classifieds_sponsor WHERE post_id = ? AND done < 1", [$this->adbuild_id]);
				$homespday = 30;
				foreach($_POST as $key=>$value) {
					if(strncmp($key, "homesp", 6))
						continue;
					$value = intval($value);
					if (!$value)
						continue;
					$loc = intval(substr($key, 6));
					if (!$loc)
						continue;
					$type = intval($this->adbuild_type);
					$expire_stamp = time() + 86400*intval($homespday);
					$db->q("INSERT INTO classifieds_sponsor
							(loc_id, state_id, post_id, day, type, expire_stamp) 
							values 
							('$loc', (select loc_parent from location_location where loc_id = '$loc'), '{$this->adbuild_id}', '$homespday', {$type}, {$expire_stamp})");
				}
			}

			if (isset($_POST['side_sponsor'])) {
				$db->q("DELETE FROM classifieds_side WHERE post_id = ? and done < 1", array($this->adbuild_id));
				$day = 30;	//1 month - 30 days
				$value = floatval(self::get_side_upgrade_price());
				$type = intval($this->adbuild_type);
				foreach($_POST as $key => $value) {
					if (strncmp($key, "side_sponsor_", 13))
						continue;
					$loc_id = intval(substr($key, 13));
					if (!$loc_id)
						continue;
					$db->q("INSERT INTO classifieds_side
							(loc_id, state_id, post_id, day, type, expire_stamp) 
							values 
							(?, (select loc_parent from location_location where loc_id = ?), ?, ?, ?, ?)",
							array($loc_id, $loc_id, $this->adbuild_id, $day, $type, time()+(86400*$day)));
				}
			}

			if (isset($_POST['sticky_sponsor'])) {
				$db->q("DELETE FROM classified_sticky WHERE classified_id = ? and done < 1", array($this->adbuild_id));
				$type = intval($this->adbuild_type);
				foreach($_POST as $key => $value) {
					if (strncmp($key, "sticky_sponsor_", 15))
						continue;
					$loc_id = intval(substr($key, 15));
					$days = intval($_POST[$key]);
					if (!$loc_id)
						continue;
					$expire_stamp = $now + (86400 * $days);
					$db->q("INSERT INTO classified_sticky
							(classified_id, loc_id, type, days, created_stamp, expire_stamp, done) 
							values 
							(?, ?, ?, ?, ?, ?, -1)",
							[$this->adbuild_id, $loc_id, $type, $days, $now, $expire_stamp]
							);
				}
			}

		}

		$one_location = false;
		if (is_array($this->adbuild_locs) && count($this->adbuild_locs) == 1)
			$one_location = true;


		//----------------------------------------
		//-- compute price of items in the cart --
		//----------------------------------------
		if (is_array($this->adbuild_locs) && count($this->adbuild_locs)) {
			foreach ($this->adbuild_locs as $loc_id) {
				$loc = $mcache->get("SQL:LOC-ID",array($loc_id));
				if ($loc === false)
					continue;
				$post_price = self::getPostPrice(self::getPostPriceForLocation($loc_id));
				$picked[] = array(
					"loc_id"	 => $loc_id,
					"loc_name"   => "{$loc['loc_name']}, {$loc['s']}",
					"post_price" => $post_price,
					"type"	   => "loc"
					);
				$total += $post_price;
			}
		}
		$this->adbuild_post_price = $total;

		//--------------------
		// add city thumbnails
		$res = $db->q("
			SELECT l.loc_id, concat(l.loc_name, ', ', s) loc_name, s.id, s.day
			FROM classifieds_sponsor s
			INNER JOIN location_location l on l.loc_id = s.loc_id
			WHERE s.post_id = ?	AND s.done < 1 AND s.expire_stamp > ?",
			[$this->adbuild_id, $now]
			);
		while($row = $db->r($res)) {
			$id = $row["id"];
			$loc_id = $row["loc_id"];

			//make sure there is free slot for city thumbnail in this location
			if (!self::locationHasFreeSlotCityThumbnail($loc_id)) {
				//if not, remove this entry from cs table
				$db->q("DELETE FROM classifieds_sponsor WHERE id = ? LIMIT 1", [$id]);
				continue;
			}

			$p = $this->gethomepricebyloc($loc_id);

			$picked[] = [
				"loc_id" => $loc_id,
				"loc_name" => "City Thumbnail {$row['loc_name']}",
				"post_price" => number_format($p, 2),
				"type" => "cover",
				];
			$this->adbuild_sponsor_city++;
			$this->adbuild_sponsor_total += $p;
			$total += $p;
		}

		//jay 30.12.2015 - dallas requested combo price for recurring and city thumbnail
		/*
		if ($one_location && $this->adbuild_row['recurring'] && $this->adbuild_sponsor_city == 1 && ($this->price_combo_city_recurring < ($this->home_price_month + $this->price_per_recurring))) {
			$diff = $this->home_price_month + $this->price_per_recurring - $this->price_combo_city_recurring;
			$picked[] = array(
				"loc_id"	 => "",
				"loc_name"   => "City thumbnail + Recurring Discount",
				"post_price" => $diff*-1,
				"type"	 => "combo_city_recurring"
				);
			$total -= $diff;
		}
		*/

		//------------------
		// add side sponsors
		$res = $db->q("
			SELECT l.loc_id, concat(l.loc_name, ', ', s) loc_name, s.id, s.day
			FROM classifieds_side s
			INNER JOIN location_location l on l.loc_id = s.loc_id
			WHERE s.post_id = ? AND s.done < 1 AND s.expire_stamp > ?",
			[$this->adbuild_id, $now]
			);
		while($row = $db->r($res)) {
			$id = $row["id"];
			$loc_id = $row["loc_id"];

			//make sure there is free slot for side sponsor in this location
			if (!self::locationHasFreeSlotSideSponsor($loc_id, $this->adbuild_type)) {
				//if not, remove this entry from cs table
				$db->q("DELETE FROM classifieds_side WHERE id = ? LIMIT 1", [$id]);
				continue;
			}

			$picked[] = [
				"loc_id" => $loc_id,
				"loc_name"=>"SideSponsor {$row['loc_name']}",
				"post_price" => number_format(self::get_side_upgrade_price(), 2),
				"type" => "side",
				];
			$this->adbuild_side_city++;
			$this->adbuild_side_total += self::get_side_upgrade_price();
			$total += self::get_side_upgrade_price();
		}

		//--------------------
		// add sticky upgrades
		$res = $db->q("SELECT s.id, s.days, l.loc_id, concat(l.loc_name, ', ', s) loc_name
						FROM classified_sticky s
						INNER JOIN location_location l on l.loc_id = s.loc_id
						WHERE s.classified_id = ? AND s.done < 1 AND s.expire_stamp > ?", array($this->adbuild_id, time()));
		while($row = $db->r($res)) {
			$id = $row["id"];
			$loc_id = $row["loc_id"];

			$price = self::get_sticky_upgrade_price($this->adbuild_type, $loc_id, $row['days']);

			//make sure there is free slot for sticky ad in this location
			if (!self::locationHasFreeSlotSticky($loc_id, $this->adbuild_type)) {
				//if not, remove this entry from cs table
				$db->q("DELETE FROM classified_sticky WHERE id = ? LIMIT 1", [$id]);
				continue;
			}

			$picked[] = array(
				'loc_id' => $loc_id,
				'loc_name'=>"Sticky Ad {$row['loc_name']}",
				"post_price" => number_format($price, 2),
				"type" => "sticky"
			);
			$this->adbuild_sticky_city++;
			$this->adbuild_sticky_total += $price;
			$total += $price;
		}

		$this->adbuild_totalcost = $total;

		// - either we put in promocode in form (i guess this is valid only for step 4), 
		// - or it is taken from classifieds row, but this is only for live/waiting/invisible ads (so this is basically edit of already posted ad)
		//	 - (we need to exclude here renewals of ads posted with free promocodes etc...)
		if (isset($_POST['promo']) && !empty($_POST['promo']) || ($this->adbuild_row['promo'] && $this->adbuild_row['done'] > 0)) {
			$px = !empty($_POST['promo']) ? GetPostParam("promo") : $this->adbuild_row['promo_code'];
			$promo = $this->adbuild_promo($px, $p, $this->adbuild_error);
			if( $promo ) {
				$picked[] = array('loc_id'=>'', 'loc_name'=>"{$p} coupon applied", "post_price"=>'', "type"=>"coupon");
				$smarty->assign('promo', $px);
				$db->q("update classifieds set promo_code = '$px', promo = '{$this->adbuild_promo_row['id']}' where id = '{$this->adbuild_id}'");
			}
			else $smarty->assign('error', $this->adbuild_error);
		}

		$account_id = intval($account->isloggedin());

		$this->timezone($this->adbuild_id, $timezone_time, $timezone_name);

		$db->q("DELETE FROM classifieds_payment
				WHERE time < date_sub(now(), interval 2 hour)");
		$db->q("INSERT INTO classifieds_payment 
				(post_id, account_id, owner, promo, promo_id, 
				auto_renew_fr,
				sponsor_city, sponsor_total, side_city, side_total, sticky_city, sticky_total,
				auto_renew_time, timezone_time, total, time) 
				values
				('{$this->adbuild_id}', '$account_id', '$account_id', '{$this->adbuild_promo_row['code']}', '".intval($this->adbuild_promo_row['id'])."', 
				'{$this->adbuild_row['auto_renew_fr']}',  
				'{$this->adbuild_sponsor_city}', '{$this->adbuild_sponsor_total}', '{$this->adbuild_side_city}', '{$this->adbuild_side_total}', '{$this->adbuild_sticky_city}', '{$this->adbuild_sticky_total}',
				'{$this->adbuild_row['auto_renew_time']}', '$timezone_time', '{$this->adbuild_totalcost}', now()) 
				ON DUPLICATE KEY 
				UPDATE promo = '{$this->adbuild_promo_row['code']}', promo_id = '".intval($this->adbuild_promo_row['id'])."', total = '{$this->adbuild_totalcost}', 
				time = now(), 
				auto_renew_fr = '{$this->adbuild_row['auto_renew_fr']}',  
				sponsor_city = '{$this->adbuild_sponsor_city}', sponsor_total = '{$this->adbuild_sponsor_total}', 
				side_city = '{$this->adbuild_side_city}', side_total = '{$this->adbuild_side_total}', 
				sticky_city = '{$this->adbuild_sticky_city}', sticky_total = '{$this->adbuild_sticky_total}', 
				auto_renew_time = '{$this->adbuild_row['auto_renew_time']}', timezone_time = '$timezone_time'");

		if ($picked) {
			$this->adbuild_param_by_loc();
			$totax = number_format($this->adbuild_totalcost, 2);
			$smarty->assign("selectedloc", $picked);
			$smarty->assign("total", $totax);
			if( $this->adbuild_currency && $this->adbuild_currency != 'USD') {
				$exchange = currency_convert::convert('USD', $this->adbuild_currency, $totax);
				$exchange = currency_convert::getPriceTag($this->adbuild_currency, $exchange);
				$smarty->assign("exchange", "$exchange");
			}
		}
		$smarty->assign("ad_id", $this->adbuild_id);
		$smarty->assign("cat_name", $this->adbuild_cat_name);
	}

	private function adbuild_promo($promo, &$p, &$error = NULL) {
		global $db;

		if (empty($promo))
			return 0;

		//10.1.2017 dallas special request-  promo backpage should work if they put space inside or mess up uppercase/lowercase...
		if (str_replace(' ', '', strtolower(trim($promo))) == "backpage")
			$promo = "backpage";

		$rex = $db->q("SELECT * FROM classifieds_promocodes WHERE code like ? AND deleted IS NULL LIMIT 1", [$promo]);
		if( !$db->numrows($rex) ) {
			$expires = strtotime($this->adbuild_row["expires"]);
			if ($expires && $expires < time()) {
				//this is probably renewing expired ad with promo - we need to delete promo code from classifieds table
				$db->q("UPDATE classifieds SET promo = 0, promo_code = '' WHERE id = '{$this->adbuild_id}'");
				$this->adbuild_row["promo"] = '';
				$this->adbuild_row["promo_code"] = '';
				return 0;
			}
			$error = "Promo code is not valid.";
			$this->adbuild_promo_row = NULL;
			return 0;
		}
		$this->adbuild_promo_row = $rox = $db->r($rex);
		if( $rox["limit_per_account"] == 1 ) {
			global $account;
			$ip = account::getUserIp();
			$account_id = intval($account->isloggedin());
			$params = [$rox["id"], $account_id, $ip];
			$phone_sql = "";
			if (!empty($this->adbuild_row['phone'])) {
				$phone_sql = " OR phone = ? ";
				$params[] = $this->adbuild_row['phone'];
			}
			$rexx = $db->q("SELECT id FROM classifieds_promocodes_usage WHERE code_id = ? AND ( account_id = ? OR ip_address = ? {$phone_sql}) LIMIT 1",
				$params
				);
			if( $db->numrows($rexx) ) {
				$error = "You may not use this promo code more than once"; 
				return 0;
			}
		}
		if ($rox["left"] < 1) {
			$error = "This promo code is expired.";
			return 0;
		} else if ($rox["max"] > 0 && $this->adbuild_totalcost > $rox["max"]) {
			$error = "This promotion code is only valid up to \${$rox["max"]} amount of payments.";
			return 0;
		} else if (!empty($rox['section']) && strcmp('classifieds', $rox['section'])) {
			$error = "This promotion code is not eligible for this section.";
			return 0;
		} else if ($rox['loclimit'] && count($this->adbuild_locs) && $rox['loclimit'] < count($this->adbuild_locs)) {
			$error = "This promotion code can not be used to post in more than {$rox['loclimit']} location(s)";
			return 0;
		} else if( $rox['subcat'] && $rox['subcat'] != $this->adbuild_row['type'] ) {
			$error = "This promo code is not eligible for this category"; return 0;
		} else {
			$this->adbuild_row['promo'] = $rox['id'];
			/*
			if ($rox["recurring"] == 1 && $this->adbuild_row['recurring'] == 0)
				$this->adbuild_row['recurring'] = 1;
			*/
			if ($rox["discount"] > 0) {
				$this->adbuild_totalcost -= $rox["discount"];
				if ($this->adbuild_totalcost < 0)
					$this->adbuild_totalcost = 0;
				$p = "\${$rox["discount"]} off";
				return 1;
			} else if( $rox["percentage"] > 0 ) {
				$this->adbuild_totalcost -= number_format($this->adbuild_totalcost*$rox["percentage"]/100, 2);
				$p = "%{$rox["percentage"]} off";
				return 1;
			} elseif( $rox["addup"] > 0 ) {
				$this->adbuild_totalcost += $rox["addup"];
				$p = "+\${$rox["addup"]} add up";
				return 1;
			} elseif( $rox["fixedprice"] > 0 ) {
				$p = "\${$rox["fixedprice"]} fixed price";
				$this->adbuild_totalcost = $rox["fixedprice"];
				return 1;
			} elseif( $rox['free'] ) {
				$p = "Free Ad";
				$this->adbuild_totalcost = 0;
				return 1;
			} elseif ($rox["eccie_promo"]) {
				//eccie free promo codes
				//dena email Jan 5, 2015 at 7:36 AM
				//When an escort on Eccie uses this promo code to post an ad on AdultSearch the ad on AdultSearch should NOT expire and will only stop appearing on AdultSearch if the escort deactivates or deletes it (or an admin does). These ads should appear BELOW ads that are placed directly with us (aka PAID), unless the escort pays for any upgrades to her ad and in this case regular reposting rules should apply
				$p = "Free Basic Ad";
				$this->adbuild_totalcost -= $this->adbuild_post_price;
				return 1;
			} elseif ($rox["tracking"]) {
				return 1;
			}
			return 0;
		}
	}

	/**
	 * Shows classifieds preview page in adbuild wizard
	 */
	public function adpreview() {
		global $db, $smarty;
		$form = new form;
		require_once(_CMS_ABS_PATH."/_cms_files/classifieds/array.php");

		if (!$this->adbuild_id)
			return 'no ad is specified';

		$res = $db->q("select * from classifieds where id = '{$this->adbuild_id}'");
		if (!$db->numrows($res))
			die('ad is not available');
		$row = $db->r($res);

		if ($row['ethnicity'])
			$row['ethnicity'] = $form->arrayValue($this->adbuild_ethnicity, $row['ethnicity']);

		$smarty->assign("globals", $GLOBALS);
		$smarty->assign('post', $row);
		$smarty->assign('adpreview', true);

		$rex = $db->q("select * from classifieds_image where id = '{$this->adbuild_id}' order by image_id");
		$image = array();
		while($rox=$db->r($rex)) {
			$resize = $rox["width"]>200||!$rox["width"]?true:false;
			$image[] = array("filename"=>$rox["filename"], "resize"=>$resize);
		}
		$smarty->assign("images", $image);

		// videos
		$rex = $db->q("SELECT cv.id, cv.filename, cv.thumbnail, cv.converted, cv.width, cv.height
						FROM classified_video AS cv
						WHERE converted=2 AND classified_id=?", array($this->adbuild_id));

		$videos = array();
		while($rox=$db->r($rex)) {
			$resize = $rox["width"]>200||!$rox["width"]?true:false;
			$videos[] = array(
				"file_uri"=>vid::videoUri( $rox["filename"] ),
				"width"=>$rox['width'],
				"height"=>$rox['height'],
				"thumbnail_uri"=>vid::thumbnailUri($rox["thumbnail"]),
				"resize"=>$resize
				);
		}
		$smarty->assign("videos", $videos);

		$pop = isset($_GET['pop']) ? true : false;
		$smarty->assign("pop", $pop);
		$fetch = $smarty->fetch(_CMS_ABS_PATH."/templates/adbuild/preview.tpl");
		echo $fetch;
		if ($pop)
			die;
		return;
	}	

	public function adbuild_param_by_loc() {
		global $db;

		if (!is_array($this->adbuild_locs) || count($this->adbuild_locs) < 1 || $this->adbuild_param_process)
			return;

		$metric = $international = NULL;
		$this->adbuild_country = null;

		$res = $db->q("select country_id, country_sub from location_location where loc_id in (".implode(',', $this->adbuild_locs).")");
		$j = 0;
		while($row=$db->r($res)) {

			if( $j === 0 ) { // use first row to determine country & currency
				$this->adbuild_currency = dir::getCurrencyCodeByCountry($row[1]);
				$this->adbuild_country = location::getCountryCodeByCountryId((int)$row[0]); // if there are more than 1 location, we assume first

				if ( in_array( $this->adbuild_country, array('uk', 'us'))) {
					$this->adbuild_metric = $metric = $international = false;
				} else {
					$this->adbuild_international  = $metric = $international = true;
				}

				$j++;
			}
		}

		$this->adbuild_param_process = true;
	}

	public static function getPostPriceForLocation($loc_id, $country_id = NULL) {
		global $mcache, $account;

		$price = self::getPriceNormal();
		if ($account->getAgency())
			$price = self::getPriceAgency();

		return $price;

		//if (is_null($country_id)) {
		//	//get location
		//	$loc = $mcache->get("SQL:LOC-ID",array($loc_id));
		//	if ($loc === false) {
		//		//this should not happe, but return current default price in case of some error
		//		return ;
		//	}
		//	$country_id = $loc["country_id"];
		//}

		//if (in_array($country_id, array(16046, 16047, 41973)))
		//	return 9.99;
		//else
		//	return 0.99;
		//return 9.99;
	}

	/**
	 * Function handles exceptions in post prices
	 */
	public static function getPostPrice($standard_post_price) {
		global $account;

		//jay debug
//		if ($account->getEmail() == "jle@email.cz")
//			return 0;

		return $standard_post_price;
	}

	public static function getStates($current_state = NULL) {
		global $db;

		$other = $sel = NULL;
		$res = $db->q("SELECT loc_id, loc_name FROM location_location WHERE loc_type = 1 ORDER BY loc_id");
		while ($row = $db->r($res)) {
			$select = NULL;
			$res2 = $db->q("SELECT s.loc_id, s.loc_name FROM location_location s WHERE s.loc_parent = ? AND s.loc_type = 2 ORDER BY s.loc_name", array($row["loc_id"]));
			if (!$db->numrows($res2)) {
				$other[] = array(
					"loc_name"=>$row['loc_name'],
					"loc_id"=>$row['loc_id'],
					);
			} else {
				while ($row2 = $db->r($res2)) {
					$state = array(
						"loc_name"=>$row2["loc_name"],
						"loc_id"=>$row2["loc_id"]
						);
					if ($current_state == $row2["loc_id"])
						$state["selected"] = true;
					$select[] = $state;	
				}
			}
			if (!is_null($select)) {
				$sel[$row['loc_id']] = array("name"=>$row['loc_name'], "list"=>$select);
			}
		}
		if (!is_null($other)) {
			$sel[0] = array("name"=>'Other Countries', "list"=>$other);
		}

		return $sel;
	}

	public static function getCities($parent = NULL, $current_city = NULL, $limit = false, $move = false) {
		global $db;

		$cities = NULL;
		$limit_post_price = NULL;

		if ($move)
			//classifieds/move page
			$where = "WHERE (has_place_or_ad = 1 OR country_id NOT IN (16046, 16047, 41973)) ";
		else
			//adbuild
			$where = "WHERE (bp = 1 OR country_id NOT IN (16046, 16047, 41973)) ";

		if ($parent)
			$where .= " AND loc_parent = '".intval($parent)."'";

		if ($limit) {
			//we can only choose from cities with the same or smaller value of ad post price
			$limit_post_price = self::getPostPriceForLocation($current_city);
		}

		$sql = "SELECT * FROM location_location {$where} ORDER BY loc_name";
		$res = $db->q($sql);
		while($row=$db->r($res)) {
			$post_price = self::getPostPrice(self::getPostPriceForLocation($row["loc_id"], $row["country_id"]));
			if (!is_null($limit_post_price) && ($post_price > ($limit_post_price + 0.01)))
				continue;
			$sub = !empty($row['country_sub']) ? "{$row['country_sub']}." : "";
			$boldx = ($post_price > 2) ? "true" : false;
			$city = array(
				"loc_id" => $row['loc_id'],
				"loc_name" => $row['loc_name'],
				"loc_url" => $row['loc_url'],
				"sub" => $sub,
				"state" => $row['loc_parent'],
				"bold" => $boldx,
				"post_price" => $post_price,
				"exchange" => ""
			);
			if ($row['loc_id'] == $current_city)
				$city["selected"] = true;
			$cities[$row['loc_parent']][] = $city;
		}

		return $cities;
	}

	public static function getUrl($id, $type, $loc_id) {
		global $mcache, $config_dev_server;

		//get location
		$loc = $mcache->get("SQL:LOC-ID",array($loc_id));
		if ($loc === false)
			return false;
		$loc_name = $loc["loc_name"];
		$s = $loc["s"];

		$module = self::getModuleByType($type);

		$dev_sub = "";
		if ($config_dev_server)
			$dev_sub = "dev.";

		if (in_array($loc["country_id"], array(16046, 16047, 41973))) {
			//domestic
			if ($loc["country_id"] == 16046)
				$url = "http://{$dev_sub}adultsearch.com{$loc["loc_url"]}{$module}/{$id}";
			else if ($loc["country_id"] == 16047)
				$url = "http://ca.{$dev_sub}adultsearch.com{$loc["loc_url"]}{$module}/{$id}";
			else if ($loc["country_id"] == 41973)
				$url = "http://uk.{$dev_sub}adultsearch.com{$loc["loc_url"]}{$module}/{$id}";
		} else {
			//international
			$url = "http://{$loc["country_sub"]}.{$dev_sub}adultsearch.com/{$loc["dir"]}/{$module}/{$id}";
		}
		return $url;
	}

	public static function rotateIndex() {
		rotate_index("escorts");
	}

	/**
	 * Called from cron every hour to email fake interests for recently posted ads
	 */
	public static function send_fake_interests() {
		global $db;

		//disabling 2018-04-17
		return true;

		//get ads that has set email posted more than 30 minutes ago for not whitelisted users
		$res = $db->q("SELECT c.*
						FROM classifieds c 
						INNER JOIN account a on a.account_id = c.account_id
						WHERE c.deleted IS NULL AND c.email <> '' AND c.fake_interest = 0 AND c.reply <> 2 AND c.done > 0 
								AND c.created < UNIX_TIMESTAMP()-1800 AND c.created > UNIX_TIMESTAMP()-86400 
								AND a.whitelisted = 0");
		$ads = array();
		$emails_sent = array();
		while($row = $db->r($res)) {
			$clad = clad::withRow($row);
			if (!$clad)
				continue;
			$id = $row["id"];
			$created = $row["created"];
			$email = $row["email"];
			$reply = $row["reply"];
			$account_id = $row["account_id"];
			echo "Id={$clad->getId()}, created @ ".date("Y-m-d H:i", $clad->getCreated())." (".(number_format((time()-$clad->getCreated())/60, 0))." minutes ago), email={$clad->getEmail()}, reply={$clad->getReply()}, account_id={$clad->getAccountId()}<br />\n";

			//test if there was some fake interest email sent to this user in the past (so we dont send the same fake email to the person that got it 1 month ago)
			$res2 = $db->q("SELECT c.id FROM classifieds c WHERE c.account_id = ? AND c.fake_interest = 1 AND c.created < UNIX_TIMESTAMP()-86400", array($account_id));
			if ($db->numrows($res2) > 0) {
				//ignore this person
				continue;
			}

			//sent only one email to every email (if person posts 2 ads, only one email is sent)
			if (!in_array($clad->getEmail(), $emails_sent)) {
				echo "Sending email to {$clad->getEmail()}....<br />\n";
				self::email_fake_interest($clad);
				$emails_sent[] = $email;
			}

			//mark ad as "fake_interest sent"
			$db->q("UPDATE classifieds SET fake_interest = 1 WHERE id = ?", array($clad->getId()));
			break;
		}
	}

	public static function email_fake_interest($clad) {
		global $global_email_log;

		$subject = "Next week";
		//$text = "Hey, I'm next weekend in the city, do you have couple hours you could squeeze me in ?\nJ.";
		$text = "Hello, I was wondering if you are available? I'll be in the city next weekend.\nJoe";
		if ($clad->getReply() == 1) {
			//email anonymous & reply forwarder
			$text = "Email sent from Adultsearch.com reply form regarding ad {$clad->getUrl()} :\n\n";
			//$text .= "Hey, I'm next weekend in the city, do you have couple hours you could squeeze me in ?\nJ.";
			$text .= "Hello, I was wondering if you are available? I'll be in the city next weekend.\nJoe";
		} else {
			//$text = "Hey, I saw your ad here: {$clad->getUrl()}\nI'm next weekend in the city, do you have couple hours you could squeeze me in ?\nJ.";
			$text = "Hello, I saw your ad here: {$clad->getUrl()}\nI was wondering if you are available? I'll be in the city next weekend.\nJoe";
		}
		$email = $clad->getEmail();
		$ret  = email_gmail("joemolson84@gmail.com", "Joe Molson", $email, $subject, nl2br($text), $text);
		if ($ret !== true) {
			echo "ERROR sending email to '{$email}': {$ret}<br /> (clad #{$clad->getId()})\n";
			reportAdmin("AS: classifieds::email_fake_interest(): Error sending email", "Error: {$ret}, details below in log:", 
						array("clad_id" => $clad->getId(), "email" => $email, "error" => $ret, "log" => $global_email_log));
		} else {
			echo "Email sent successfully.\n";
		}
	}

	public function getSideSponsors($type, $loc_id) {
		global $db, $config_image_server;

		$result = array();
		//$res = $db->q("SELECT cs.post_id, c.title, c.content, GROUP_CONCAT(ci.filename) as images
		$res = $db->q("SELECT c.*, GROUP_CONCAT(ci.filename) as images
						FROM classifieds_side cs
						INNER JOIN classifieds c on c.id = cs.post_id
						LEFT JOIN classifieds_image ci on ci.id = cs.post_id
						WHERE cs.loc_id = ? AND cs.type = ? AND c.deleted IS NULL AND c.done = 1
						GROUP BY cs.post_id
						ORDER BY RAND() 
						LIMIT 18", array($loc_id, $type));
		while ($row = $db->r($res)) {

			$clad = clad::withRow($row);
			if (!$clad)
				continue;

			//$text = substr(self::sanitizeContentForOutput($clad->getContent(), $clad->getAllowlinks(), $clad->getAccountId()), 0, 150);
			$text = substr(strip_tags($clad->getContent()), 0, 150);
			if (strlen($text) == 150)
				$text .= "...";

			$images = array();
			$arr = explode(",", $row["images"]);
			$i = 1;
			foreach ($arr as $fi) {
				$images[] = "{$config_image_server}/classifieds/t/".$fi;
				if ($i >= 3)
					break;
				$i++;
			}

			$side_item = array(
				"title" => $row["title"],
				//"text" => htmlspecialchars($text),
				"text" => $text,
				"link" => $clad->getUrl($loc_id),
				"images" => $images,
				);
			$result[] = $side_item;
		}
		
		return $result;
	}

	public function CL_stat($clad_id, $total, $what = '', $count = 1) {
		global $account, $db;

		if( $what == '' ) {
			reportAdmin('CL_stat() with no param'); return;
		}

		$res = $db->q("select * from classifieds where id = '$clad_id'");
		if (!$db->numrows($res))
			return;

		$account_id = $account->isloggedin();
		$rowcl = $db->r($res);
		$rex = $db->q("select loc_id from classifieds_loc where post_id = ?", array($clad_id));
		//TODO
		/*
		if ($this->discount)
			$discount = $this->discount;
		else
			$discount = $this->Total == 0 ? 100 : floor(($this->saved/$this->Total)*100);
		*/

		while($row=$db->r($rex)) {
			if($what == 'new')
				$query = "INSERT INTO classifieds_log
							(post_id, time, account_id, loc_id, original, made, recurring, repost, promo_code, promo, discount, total_loc, firstad) 
							values
							('{$clad_id}', now(), '$account_id', '{$row['loc_id']}', 0, '$total', '0', 0, 0, '', 0, '{$rowcl['total_loc']}', if((select count(*) from classifieds where account_id = '$account_id' limit 1),0,1))";

			elseif ($what == 'recurring')
				$query = "INSERT INTO classifieds_log 
							(post_id, time, account_id, loc_id, original, made, recurring, repost, promo_code, promo, discount, total_loc, firstad) 
							values
							('{$clad_id}', now(), '$account_id', '{$row['loc_id']}', 0, '$total', '1', '0', 0, '', 0, '{$rowcl['total_loc']}', if((select count(*) from classifieds where account_id = '$account_id' limit 1),0,1))";

			elseif ($what == 'upgrade')
				$query = "INSERT INTO classifieds_log
							(post_id, time, account_id, loc_id, original, made, recurring, repost, promo_code, promo, discount, total_loc, firstad) 
							values
							('{$clad_id}', now(), '$account_id', '{$row['loc_id']}', 0, '$total', '0', '$count', 0, '', 0, '{$rowcl['total_loc']}', if((select count(*) from classifieds where account_id = '$account_id' limit 1),0,1))";

			$db->q($query);
		}
	}

	public function afterSuccessfulPayment($payment_id, $clad_id, $rebill = false) {
		global $db, $account;

		file_log("classifieds", "afterSuccessfulPayment: payment_id={$payment_id}, clad_id={$clad_id}");
		$now = time();

		$res = $db->q("
			SELECT p.account_id, p.author_id, p.cc_id, p.trans_id, p.recurring_period
			FROM payment p
			LEFT JOIN classifieds_promocodes cp on cp.id = p.promocode_id
			WHERE p.id = ?
			", 
			[$payment_id]
			);
		if ($db->numrows($res) != 1) {
			debug_log("classifieds::afterSuccessfulPayment: Error: Cant find payment by id {$payment_id}!");
			reportAdmin("AS: classifieds::afterSuccessfulPayment Error", "Cant find payment by id {$payment_id}!", []);
			return false;
		}
		$row = $db->r($res);
		$account_id = $row["account_id"];
		$trans_id = $row["trans_id"];
		//eccie promo codes
		//dena email Jan 5, 2015 at 7:36 AM
		//When an escort on Eccie uses this promo code to post an ad on AdultSearch the ad on AdultSearch should NOT expire and will only stop appearing on AdultSearch if the escort deactivates or deletes it (or an admin does). These ads should appear BELOW ads that are placed directly with us (aka PAID), unless the escort pays for any upgrades to her ad and in this case regular reposting rules should apply
		$eccie_promo = intval($row["eccie_promo"]);
		$recurring_period = intval($row["recurring_period"]);

		$clad = clad::findOneById($clad_id);
		if (!$clad) {
			debug_log("classifieds::afterSuccessfulPayment: Error: Cant find clad by id {$clad_id}!");
			reportAdmin("AS: classifieds::afterSuccessfulPayment Error", "Cant find clad by id {$clad_id}!", []);
			return false;
		}

		$done_status = 1;
		//if account is not whitelisted, ad will be in waiting status
		// (ad status could depend whether ad is posted by cladposter or admin or normal user)
		if (!$account->isWhitelisted() && !$account->getAgency())
			$done_status = 3;
		/*
			|| $account->isWorker() 
			|| ($clad->getEthnicity() == 2) 
			|| ($clad->getType() == 2) 
			|| ($clad->getType() == 6)
			)
			$done_status = 1;
		//2015-10-19 Sha: if NOT U.S. ad,approve
		$is_us_ad = false;
		foreach($clad->getLocations() as $loc) {
			if ($loc->getCountryId() == 16046) {
				$is_us_ad = true;
				break;
			}
		}
		if (!$is_us_ad) {
			debug_log("Cl. ad #{$clad_id} is not posted in U.S. => approving");
			$done_status = 1;
		}
		if (self::isCladPoster()) {
			$done_status = 1;
		}
		*/
		//for jay to test sms verification
		//if ($account->isrealadmin())
		//  $done_status = 3;

		if ($clad->getExpireStamp() && $clad->getExpireStamp() > $now) {
			//this is mainly for rebills
			//rebills are done usually few days before the actual renewal date, so expiration should be measured from current expire_stamp
			$default_expire_stamp = $clad->getExpireStamp();
		} else {
			$default_expire_stamp = $now;
		}
		if (!$recurring_period)
			$recurring_period = 30;
		//even for initial payments, we need to consider recurring period (e.g. if we make special payment for 90 days)
		$default_expire_stamp += 86400 * $recurring_period;

		$audit_subtype = "Upgrade";
		$audit_msg = [];

		//---------------------------------------------------------------
		//go through all payment items for this clad_id and all locations
		$res = $db->q("
			SELECT pi.*, c.type as clad_type, l.loc_parent 
			FROM payment_item pi 
			INNER JOIN classifieds c on c.id = pi.classified_id
			INNER JOIN location_location l on l.loc_id = pi.loc_id
			WHERE pi.payment_id = ? AND pi.classified_id = ?", 
			[$payment_id, $clad_id]);
		$ar = null;
		while ($row = $db->r($res)) {
			$payment_item_id = $row["id"];
			$auto_renew = $row["auto_renew"];
			$loc_id = $row["loc_id"];
			$sponsor = $row["sponsor"];
			$side = $row["side"];
			$sticky = $row["sticky"];
			$sticky_position = $row["sticky_position"];
			$sticky_days = $row["sticky_days"];
			$classified_status = $row["classified_status"];
			$clad_type = $row["clad_type"];
			$loc_parent_id = $row["loc_parent"];

			if ($classified_status != "U")
				$audit_subtype = "New";
			$audit_msg[] = "Location #{$loc_id}";

			if (!$loc_id) {
				debug_log("classifieds::aftersuccesfulpayment: error: loc_id not specified in payment_item #{$payment_item_id}!");
				reportadmin("as: classifieds::aftersuccesfulpayment error", "loc_id not specified in payment_item #{$payment_item_id}!", []);
				return false;
			}

			if ($auto_renew) {
				if ($ar == null) {
					$ar = $auto_renew;
				} else if ($ar != $auto_renew) {
					debug_log("classifieds::aftersuccesfulpayment: error: auto_renew value differs between location ar={$ar} auto_renew={$auto_renew} !");
					reportAdmin("AS: classifieds::aftersuccesfulpayment error", "auto_renew val differs between location ar={$ar} auto_renew={$auto_renew}");
					return false;
				}
			}

			if ($sponsor) {
				$expire_stamp = null;

				//check if we already have city thumbnail row for this ad and location
				$res2 = $db->q("SELECT id FROM classifieds_sponsor WHERE post_id = ? AND loc_id = ?", [$clad_id, $loc_id]);
				if ($db->numrows($res2)) {
					//yes we do, do an update
					$row2 = $db->r($res2);
					$expire_stamp = $row2["expire_stamp"];
					if ($expire_stamp < $now)
						$expire_stamp = $now;
					$expire_stamp += 30 * 86400;

					file_log("classifieds", "afterSuccessfulPayment: updating classifieds_sponsor: clad_id={$clad_id}, loc_id={$loc_id}, done={$done_status}, expire_stamp={$expire_stamp}");
					$res2 = $db->q(
						"UPDATE classifieds_sponsor SET done = ?, expire_stamp = ? WHERE post_id = ? AND loc_id = ?", 
						[$done_status, $expire_stamp, $clad_id, $loc_id]
						);
				} else {
					//no we dont, do an insert
					$expire_stamp = $now + (30 * 86400);
					file_log("classifieds", "afterSuccessfulPayment: inserting classifieds_sponsor: clad_id={$clad_id}, loc_id={$loc_id}, done={$done_status}, expire_stamp={$expire_stamp}");
					$res2 = $db->q(
						"INSERT INTO classifieds_sponsor 
						(loc_id, state_id, post_id, day, type, expire_stamp, done)
						VALUES
						(?, ?, ?, 30, ?, ?, ?)",
						[$loc_id, $loc_parent_id, $clad_id, $clad_type, $expire_stamp, $done_status]
						);
				}
				$audit_msg[] = "City thumbnail in #{$loc_id} until ".date("m/d/Y", $expire_stamp);
			}

			if ($side) {
				$expire_stamp = null;

				//check if we already have side sponsor row for this ad and location
				$res2 = $db->q("SELECT id, expire_stamp FROM classifieds_side WHERE post_id = ? AND loc_id = ?", [$clad_id, $loc_id]);
				if ($db->numrows($res2)) {
					//yes we do, do an update
					$row2 = $db->r($res2);
					$expire_stamp = $row2["expire_stamp"];
					if ($expire_stamp < $now)
						$expire_stamp = $now;
					$expire_stamp += 30 * 86400;
	
					file_log("classifieds", "afterSuccessfulPayment: updating classifieds_side: clad_id={$clad_id}, loc_id={$loc_id}, done={$done_status}, expire_stamp={$expire_stamp}");
					$res2 = $db->q(
						"UPDATE classifieds_side SET done = ?, expire_stamp = ? WHERE post_id = ? AND loc_id = ?", 
						[$done_status, $expire_stamp, $clad_id, $loc_id]
						);
				} else {
					//no we dont, do an insert
					$expire_stamp = $now + (30 * 86400);
					file_log("classifieds", "afterSuccessfulPayment: inserting classifieds_side: clad_id={$clad_id}, loc_id={$loc_id}, done={$done_status}, expire_stamp={$expire_stamp}");
					$res2 = $db->q(
						"INSERT INTO classifieds_side
						(loc_id, state_id, post_id, day, type, expire_stamp, done)
						VALUES
						(?, ?, ?, 30, ?, ?, ?)",
						[$loc_id, $loc_parent_id, $clad_id, $clad_type, $expire_stamp, $done_status]
						);
				}
				$audit_msg[] = "Side sponsor in #{$loc_id} until ".date("m/d/Y", $expire_stamp);
			}


			if ($sticky) {
				$expire_stamp = null;

				//check if we already have active sticky upgrade row for this ad and location
				$res2 = $db->q("
					SELECT id, days, expire_stamp 
					FROM classified_sticky 
					WHERE classified_id = ? AND loc_id = ? AND done = 1
					", 
					[$clad_id, $loc_id]
					);
				if ($db->numrows($res2)) {
					$row2 = $db->r($res2);
					$expire_stamp = $row2["expire_stamp"];
				}

				if (!$expire_stamp || $expires_stamp < $now)
					$expire_stamp = $now;

				if (!intval($sticky_days))
					$sticky_days = 30;

				$expire_stamp = $expire_stamp + 86400 * $sticky_days;

				file_log("classifieds", "afterSuccessfulPayment: setting up sticky: clad_id={$clad_id}, loc_id={$loc_id}, days={$sticky_days}, done={$done_status}, expire_stamp={$expire_stamp}");

				$res2 = $db->q("DELETE FROM classified_sticky WHERE classified_id = ? AND loc_id = ?", [$clad_id, $loc_id]);

				$res2 = $db->q(
					"INSERT INTO classified_sticky
					(classified_id, loc_id, type, days, created_stamp, expire_stamp, done)
					VALUES
					(?, ?, ?, ?, ?, ?, ?)",
					[$clad_id, $loc_id, $clad_type, $sticky_days, $now, $expire_stamp, $done_status]
					);

				$audit_msg[] = "{$sticky_days} days sticky in #{$loc_id} until ".date("m/d/Y", $expire_stamp);

				$stickyWaitingListService = new WaitingList();
				$stickyWaitingListService->cancel($account_id, $loc_id, $clad_type);
			}

			file_log("classifieds", "afterSuccessfulPayment: updating classifieds_loc: clad_id={$clad_id}, loc_id={$loc_id}, done={$done_status}");
			$db->q("UPDATE classifieds_loc SET done = ?, updated = NOW() WHERE post_id = ? AND loc_id = ?", [$done_status, $clad_id, $loc_id]);
		}

		if ($rebill) {
			//update all locations
			//during rebill, these locations might differ from original payment location because ad might have been moved out to different city)
			$db->q("UPDATE classifieds_loc SET done = ?, updated = NOW() WHERE post_id = ?", [$done_status, $clad_id]);
		}

		//prepare values fo main classifieds entry update
		$expires = date("Y-m-d H:i:s", $default_expire_stamp);

		$clad_account_id = $clad->getAccountId();
		if (!$clad_account_id)
			$clad_account_id = $account_id;	//from payment

		$tn = $default_expire_stamp;
		if ($ar)
			$tn = self::compute_next_repost_stamp($clad_id, $clad->getAutoRenewTime(), $clad->getAutoRenewFr());

		//final update of classified
		file_log("classifieds", "afterSuccessfulPayment: final classifieds update: done={$done_status}, expire_stamp={$default_expire_stamp}, expires={$expires}, tn={$tn}, ar={$ar}, eccie_promo={$eccie_promo}");
		$res = $db->q("
				UPDATE classifieds
                SET account_id = ?, date = NOW(), done = ?, expire_stamp = ?, expires = ?, time_next = ?, deleted = NULL, eccie_promo = ? 
                WHERE id = ?", 
				[$clad_account_id, $done_status, $default_expire_stamp, $expires, $tn, $eccie_promo, $clad_id]
				);
		$aff = $db->affected($res);

		if ($ar)
			$db->q("UPDATE account SET repost = IFNULL(repost,0) + {$ar} WHERE account_id = ?", [$clad_account_id]);

		self::rotateIndex();

		$audit_msg = implode(", ", $audit_msg);

		if ($rebill) {
			//this is rebill (recurring payment renewal)
			audit::log("CLA", "Renewal", $clad_id, $audit_msg, $account->getId());

		} else {
			//this is not rebill -> this is initial payment

			//update invisible upgrades
			$db->q("UPDATE classifieds_sponsor SET done = ? WHERE post_id = ? AND done = 2", [$done_status, $clad_id]);
			$db->q("UPDATE classifieds_side SET done = ? WHERE post_id = ? AND done = 2", [$done_status, $clad_id]);
			$db->q("UPDATE classified_sticky SET done = ? WHERE classified_id = ? AND done = 2", [$done_status, $clad_id]);

			if ($aff == 1) {
				if ($done_status == 1)
					audit::log("CLA", $audit_subtype, $clad_id, $audit_msg, $account->getId());
			} else {
				reportAdmin("Error at final update of classifieds table in adbuild step 4", "", ["affected" => $aff, "done_status" => $done_status, "tn" => $tn, "ar" => $ar, "cc_id" => $cc_id, "clad_id" => $clad_id]);
			}

			//disable and redirect BP classifieds with the same phone number
			if ($clad->getPhone()) {
				$db->q("insert ignore into classifieds_redirect (id, new_id, time) (select id, {$clad_id}, now() from classifieds where phone = '{$clad->getPhone()}' and bp = 1)");
				$db->q("update classifieds c inner join classifieds_loc l on c.id = l.post_id set c.done=-1, l.done=-1 where c.phone = '{$clad->getPhone()}' and bp = 1");
			}

			//check whitelists
			file_log("classifieds", "aftersuccessfulPayment: before check whitelists");
			$whitelisted = false;
			if (clbw::isWhitelisted($clad->getEmail(), clbw::EMAIL)) {
				$whitelisted = true;
				$bwitem = clbw::findOneByField($clad->getEmail(), clbw::EMAIL);
				if ($bwitem) {
					$bwitem->hit($clad_id);
					debug_log("CLBW: WL hit, id={$bwitem->getId()}, email={$clad->getEmail()}, ad_id={$clad_id}");
				}
			}
			if (clbw::isWhitelisted($clad->getPhone(), clbw::PHONE)) {
				$whitelisted = true;
				$bwitem = clbw::findOneByField($clad->getPhone(), clbw::PHONE);
				if ($bwitem) {
					$bwitem->hit($clad_id);
					debug_log("CLBW: WL hit, id={$bwitem->getId()}, phone={$clad->getPhone()}, ad_id={$clad_id}");
				}
			}
			if (clbw::isWhitelisted($clad->getWebsite(), clbw::WEBSITE)) {
				$whitelisted = true;
				$bwitem = clbw::findOneByField($clad->getWebsite(), clbw::WEBSITE);
				if ($bwitem) {
					$bwitem->hit($clad_id);
					debug_log("CLBW: WL hit, id={$bwitem->getId()}, website={$clad->getWebsite()}, ad_id={$clad_id}");
				}
			}

			//send email receipt
			file_log("classifieds", "aftersuccessfulPayment: before send receipt");
			$clad->emailReceipt(null, $trans_id);

			//if account is not whitelisted, notify dallas
			if ($clad->isPhoneInWhitelist()) {
				file_log("classifieds", "account is whitelisted, not sending admin notify email");
				if (!$account->isWhitelisted()) {
					$account->setWhitelisted(1);
					$account->update();
				}
			} else {
				file_log("classifieds", "account is not whitelisted, sending admin notify email");
				$ret = $clad->emailAdminNotify();
				if (!$ret)
					reportAdmin("AS: Error sending clad admin notify", "", array("id" => $clad_id));
			}
		}

		file_log("classifieds", "aftersuccessfulPayment: done");

		return true;
	}

	/**
	 * Makes sure social profile links are valid and not malicious,
	 * enhance $row with handles and urls
	 */
	public static function social_links_valid(&$row) {

		if ($row["facebook"]) {
			$facebook_handle = $facebook_url = null;
			if (substr($row["facebook"], 0, 4) == "http") {
				$facebook_handle = $facebook_url = trim($row["facebook"]);
				if (substr($facebook_handle, -1) == "/")
					$facebook_handle = substr($facebook_handle, 0, -1);
				if (($pos = strrpos($facebook_handle, "/")) !== false)
					$facebook_handle = substr($facebook_handle, $pos + 1);
			} else {
				$facebook_handle = trim($row["facebook"]);
				$pos = strpos($facebook_handle, "@");
				if ($pos !== false)
					$facebook_handle = substr($facebook_handle, $pos + 1);
				$facebook_url = "https://facebook.com/{$facebook_handle}";
			}
			$row["facebookHandle"] = $facebook_handle;
			$row["facebookUrl"] = $facebook_url;
		}

		if ($row["twitter"]) {
			$twitter_handle = $twitter_url = null;
			if (substr($row["twitter"], 0, 4) == "http") {
				$twitter_handle = $twitter_url = trim($row["twitter"]);
				if (substr($twitter_handle, -1) == "/")
					$twitter_handle = substr($twitter_handle, 0, -1);
				if (($pos = strrpos($twitter_handle, "/")) !== false)
					$twitter_handle = substr($twitter_handle, $pos + 1);
			} else {
				$twitter_handle = trim($row["twitter"]);
				$pos = strpos($twitter_handle, "@");
				if ($pos !== false) 
					$twitter_handle = substr($twitter_handle, $pos + 1);
				$twitter_url = "https://twitter.com/{$twitter_handle}";
			}
			$row["twitterHandle"] = $twitter_handle;
			$row["twitterUrl"] = $twitter_url;
		}

		if ($row["instagram"]) {
			$instagram_handle = $instagram_url = null;
			if (substr($row["instagram"], 0, 4) == "http") {
				$instagram_handle = $instagram_url = trim($row["instagram"]);
				if (substr($instagram_handle, -1) == "/")
					$instagram_handle = substr($instagram_handle, 0, -1);
				if (($pos = strrpos($instagram_handle, "/")) !== false)
					$instagram_handle = substr($instagram_handle, $pos + 1);
			} else {
				$instagram_handle = trim($row["instagram"]);
				//if (substr($instagram_handle, 0, 1) == "@")
				//  $instagram_handle = substr($instagram_handle, 1);
				$pos = strpos($instagram_handle, "@");
				if ($pos !== false)
					$instagram_handle = substr($instagram_handle, $pos + 1);
				$instagram_url = "https://www.instagram.com/{$instagram_handle}/";
			}
			$row["instagramHandle"] = $instagram_handle;
			$row["instagramUrl"] = $instagram_url;
		}

		return;
	}

	//get number of thumbnails in each city
	//we need this so we can disallow to create another thumbnail in city which already has all city thumbnail spots filled
	//used in adbuild/step3 and classifieds/sponsor
	public function getCityThumbCounts() {
		global $db;

		$city_thumb_counts = [];
		$res = $db->q("SELECT loc_id, count(*) as cnt FROM classifieds_sponsor WHERE done = 1 GROUP BY loc_id", []);
		while ($row = $db->r($res)) {
			$city_thumb_counts[$row["loc_id"]] = $row["cnt"];
		}

		return $city_thumb_counts;
	}

	/**
	 * Returns true if there is free spot for city thumbnail in requested city
	 */
	public static function locationHasFreeSlotCityThumbnail($loc_id) {
		global $db;

		$current_count = $db->single("SELECT count(*) as cnt FROM classifieds_sponsor WHERE done = 1 AND loc_id = ?", [$loc_id]);

		return ($current_count < self::$city_thumbnail_count_limit);
	}

	//get number of side sponsors in each city
	//we need this so we can disallow to create another side sponsorl in city which already has all side sponsor spots filled
	//used in adbuild/step3 and classifieds/side
	public function getSideSponsorCounts($ad_type) {
		global $db;

		$side_sponsor_counts = [];
		$res = $db->q("SELECT loc_id, count(*) as cnt FROM classifieds_side WHERE done = 1 and type = ? GROUP BY loc_id", [$ad_type]);
		while ($row = $db->r($res)) {
			$side_sponsor_counts[$row["loc_id"]] = $row["cnt"];
		}

		return $side_sponsor_counts;
	}

	/**
	 * Returns true if there is free spot for side sponsor in requested city
	 */
	public static function locationHasFreeSlotSideSponsor($loc_id, $ad_type) {
		global $db;

		$current_count = $db->single("SELECT count(*) as cnt FROM classifieds_side WHERE done = 1 AND loc_id = ? AND type = ?", [$loc_id, $ad_type]);

		return ($current_count < self::$side_sponsor_count_limit);
	}

	//get number of stickies in each city
	//used in adbuild/step3 and classifieds/sticky
	public function getStickyCounts($ad_type) {
		global $db;

		$sticky_counts = [];
		$res = $db->q("SELECT loc_id, count(*) as cnt FROM classified_sticky WHERE done = 1 and type = ? GROUP BY loc_id", [$ad_type]);
		while ($row = $db->r($res)) {
			$sticky_counts[$row["loc_id"]] = $row["cnt"];
		}

		return $sticky_counts;
	}

	/**
	 * Returns true if there is free spot for sticky upgrade in requested city for requested ad type
	 */
	public static function locationHasFreeSlotSticky($loc_id, $ad_type) {
		global $db;

		$current_count = $db->single("SELECT count(*) as cnt FROM classified_sticky WHERE done = 1 AND loc_id = ? AND type = ?", [$loc_id, $ad_type]);

		return ($current_count < self::$sticky_count_limit);
	}

	/**
	 * @param $ad_type 1 - FE, 2 - TS, 3 - BR (bodyrub)
	 * @param $loc_id
	 * @param $days
	 * @return int
	 */
	public static function get_sticky_upgrade_price($ad_type, $loc_id, $days){

		$is_big_city = location::is_big_city($loc_id);
		$price = 0;

		switch ($ad_type){
			case self::TYPE_FEMALE_ESCORT:
				// loc_id 18308 is NYC
				$price = ($loc_id == 18308) ? 1500 : ($is_big_city ? 1000 : 400);
				if($days == 7)
					$price = ($loc_id == 18308) ? 500 : ($is_big_city ? 350 : 100);
				break;
			case self::TYPE_TRANS_ESCORT:
				$price = 150;
				if($days == 7)
					$price = 50;
				break;
			case self::TYPE_BODYRUB:
				$price = $is_big_city ? 350 : 250;
				if($days == 7)
					$price = $is_big_city ? 100 : 75;
				break;
		}

		return $price;
	}


    /**
     *
     * @return int
     */
    public static function get_side_upgrade_price($acc = null) {
        global $account;

		if (is_null($acc))
			$acc = $account;

        // regular price
        $price = self::$price_side_sponsor;

        // price is double for agencies
        if ($acc->isAgency())
            $price = $price * 2;


        return intval($price);
    }

    /**
     *
     * @return int
     */
    public static function get_city_thumbnail_upgrade_price($acc = null) {
        global $account;

		if (is_null($acc))
			$acc = $account;

        // regular price
        $price = self::$home_price_month;

        // price is double for agencies
        if ($acc->isAgency())
            $price = $price * 2;

        return intval($price);
    }

}

