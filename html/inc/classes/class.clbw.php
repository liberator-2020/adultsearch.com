<?php
/*
 * clbw = classified blacklist/whitelist
 */
class clbw {
	const BLACKLIST = 0;
	const WHITELIST = 1;
	const EMAIL = 0;
	const PHONE = 1;
	const WEBSITE = 2;
	
	private $id = NULL,
			$type = NULL,
			$value = NULL,
			$field_type = NULL,
			$count_matches = NULL,
			$last_matched_at = NULL,
			$last_matched_id = NULL,
			$add_date = NULL;
			
	function __construct() {
	}

	public static function findOneById($id) {
		global $db;

		$res = $db->q("SELECT * FROM classifieds_bw_list WHERE id = ?", array(intval($id)));
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

		return self::withRow($row);
	}

	public static function findOneByField($val, $field_type, $exclude_ids = '') {
		global $db;

		$val = trim($val);
		if($field_type == self::PHONE){
			$val = preg_replace("/\D/", "", $val);
		}

		if($val == NULL || $val == '') return NULL;

		$sql = "SELECT * FROM classifieds_bw_list WHERE field_type = ? AND value = ?";
		$values = array($field_type, $val);
		if($exclude_ids != ''){
			$sql .= " AND id NOT IN (?) ";
			$values[] = $exclude_ids;
		}

		$res = $db->q($sql, $values);
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

		return self::withRow($row);
	}

	public static function withRow($row) {

		$clbw = new clbw();
		$clbw->setId($row["id"]);
		$clbw->setType($row["type"]);
		$clbw->setFieldType($row["field_type"]);
		$clbw->setCountMatches($row["count_matches"]);
		$clbw->setLastMatchedAt($row["last_matched_at"]);
		$clbw->setLastMatchedId($row["last_matched_id"]);
		$clbw->setAddDate($row["add_date"]);
		$clbw->setValue($row["value"]);
		
		return $clbw;
	}

	public static function checkBW($val, $field_type, $type) {
		global $db;

		$val = trim($val);
		if ($field_type == self::PHONE) {
			$val = preg_replace("/\D/", "", $val);
		}

		if ($val == NULL || $val == '')
			return false;

		$res = $db->q("SELECT * FROM classifieds_bw_list WHERE type = ? AND value = ? AND field_type = ?", array($type, $val, $field_type));
		if ($db->numrows($res) > 0)
			return true;

		//if checking blacklist email, check only domain match
		if ($type == self::BLACKLIST && $field_type == self::EMAIL) {
			$pos = strpos("@", $val);
			if (!$pos !== false) {
				$val_domain = substr($val, $pos);
				$res = $db->q("SELECT * FROM classifieds_bw_list WHERE type = ? AND value = ? AND field_type = ?", array($type, $val_domain, $field_type));
				if ($db->numrows($res) > 0)
					return true;
			}
		}

		return false;
	}

	public static function isWhitelisted($val, $field_type) {
		return self::checkBW($val, $field_type, self::WHITELIST);
	}

	public static function isBlacklisted($val, $field_type) {
		return self::checkBW($val, $field_type, self::BLACKLIST);
	}
	
	//getters and setters
	public function getId() {
		return $this->id;
	}
	public function getType() {
		return $this->type;
	}
	public function getAddDate() {
		return $this->add_date;
	}
	public function getValue() {
		return $this->value;
	}
	public function getFieldType() {
		return $this->field_type;
	}
	public function getCountMatches() {
		return $this->count_matches;
	}
	public function getLastMatchedId() {
		return $this->last_matched_id;
	}
	public function getLastMatchedAt() {
		return $this->last_matched_at;
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	public function setType($type) {
		$this->type = $type;
	}
	public function setAddDate($date) {
		$this->add_date = $date;
	}
	public function setValue($value) {
		$this->value = $value;
	}
	public function setFieldType($field_type) {
		$this->field_type = $field_type;
	}
	public function setCountMatches($count_matches) {
		$this->count_matches = $count_matches;
	}
	public function setLastMatchedId($last_matched_id) {
		$this->last_matched_id = $last_matched_id;
	}
	public function setLastMatchedAt($last_matched_at) {
		$this->last_matched_at = $last_matched_at;
	}
	public function setData($value, $field_type, $type) {
		$this->value = $value;
		$this->field_type = $field_type;
		$this->type = $type;
	}
	
	//add data to B/W list
	public function addBWList() {
		global $db;

		$res = $db->q("INSERT INTO classifieds_bw_list (value, field_type, type) VALUES (?, ? , ?);", array($this->value, $this->field_type, $this->type));
		$newid = $db->insertid($res);

		if ($newid > 0)
			return true;

		return false;
	}

	//update B/W list item
	public function updateBWList() {
		global $db;

		$res = $db->q("UPDATE classifieds_bw_list SET value = ?, field_type = ?, count_matches = ?, last_matched_at = ?, last_matched_id = ? WHERE id = ?", 
			array($this->value, $this->field_type, $this->count_matches, $this->last_matched_at, $this->last_matched_id, $this->id));
		$aff = $db->affected($res);

		if ($aff == 1)
			return true;

		return false;
	}

	public function hit($last_matched_id) {
		$this->setCountMatches($this->getCountMatches() + 1);
		$this->setLastMatchedAt(date('Y-m-d H:i:s'));
		$this->setLastMatchedId($last_matched_id);
		$this->updateBWList();
	}
	
	/**
	 * remove an item from B/W list
	 */
	public function remove() {
		global $db;

		$res = $db->q("DELETE FROM classifieds_bw_list WHERE id = ? LIMIT 1", array($this->id));
		$aff = $db->affected($res);
		if ($aff == 1) {
			return true;
		} else {
			return false;
		}
	}
}

