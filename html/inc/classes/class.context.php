<?php
/**
 * context
 * Class 
 * @author jay
 *
 */
class context {
	
	// URL context variables
	public $domain = NULL;		//full current domain, e.g. germany.dev.adultsearch.com or uk.adultsearch.com
	public $domain_link = NULL;	//absolute link to full current domain, e.g. http://germany.dev.adultsearch.com, or https://uk.adultsearch.com

	public $domain_base = NULL;	//base domain, without www and without location subdomain(s), e.g. dev.adultsearch.com (for www.germany.dev.adultsearch.com)
	public $domain_base_link = NULL;//absolute link to base domain, see above

	public $env = NULL;	// currently "dev" or "test"	
	
//	public $domain_original = NULL;		//original request URL ()	//TODO

	public $path_original = NULL;		// current URL path e.g. /tijuana/sex-shop/search/query/Bar/order/review/orderway/1/page/2

	
	//--location type context variables
	public $international = NULL;		// true
	public $location_type = NULL;		// 1 (for country) or 2 (for state) or 3 (for city) or 4 (for neighborhood)
	public $location_path = NULL;		// '/nevada/las-vegas' or '/tijuana', ...

	public $country = NULL;			// germany	(dir)
	public $country_id = NULL;		// 12		(loc_id)
	public $country_name = NULL;		// Germany	(loc_name)
	public $country_has_states = NULL;	// true if country has states (US,Canada,UK,Australia), false if not

	public $county_id = NULL;		// counties for US and CA (county_id)
	
	public $state = NULL;			// nevada	(dir)
	public $state_id = NULL;		// 28		(loc_id)
	public $state_name = NULL;		// Nevada	(loc_name)
	public $state_lat = NULL;		// 38.8026	(loc_lat)
	public $state_long = NULL;		// -116.419	(loc_long)
	public $state_loc_url = NULL;	// /nevada/ TODO - will be removed after unification
	
	public $city = NULL;			// las-vegas	(dir)
	public $city_id = NULL;			// 10991	(loc_id)
	public $city_name = NULL;		// Las Vegas	(loc_name)
	public $city_lat = NULL;		// 36.1146	(loc_lat)
	public $city_long = NULL;		// -115.173	(loc_long)
	public $city_loc_url = NULL;	// /nevada/las-vegas/ TODO - will be removed after unification
	public $city_s = NULL;			// NV (s)

	public $location = NULL;		// las-vegas or nevada or united-states	(dir)
	public $location_id = NULL;		// 10991				(loc_id)
	public $location_name = NULL;		// Las Vegas or Nevada or Unites States	(loc_name)
	public $location_lat = NULL;		// 36.1146				(loc_lat)
	public $location_long = NULL;		// -115.173				(loc_long)

	public $parent = NULL;
	public $parent_id = NULL;
	public $parent_name = NULL;
	public $parent_lat = NULL;
	public $parent_long = NULL;
	public $parent_loc_url = NULL;

	//business info
	public $page_type	= NULL;		//type of page, possible values: "list", "item", "search"

	public $module		= NULL;		// = gModule	- this is old way of calling directory categories
	public $category	= NULL;		// sex-shop	url name of category
	public $category_id	= NULL;		// = type	- this is old way of calling international categories, in new unified module, this will be filled with node_type_id
	public $category_name	= NULL;		// Sex shop	singular name of category

	public $search		= NULL;		// array of search conditions: array('id'=>'db_value', 'type'=>'db_column', 'name'=>'display_name', 'link'=>'remove_link')

	public $order_by	= NULL;		// review	- order by attribute (review, overall, last_review_id)
	public $order_way	= NULL;		// 1 or 2	- order by way

	public $page		= NULL;		//number of page in listing (e.g. in old URL scheme ?page=2)
	public $path_no_page	= NULL;		// current URL path without paging information, e.g. /tijuana/sex-shop/search/query/Bar

	public $node_id		= NULL;		// = gItemId if module is NULL, node_id is id in concrete table (eroticmassage, germany, ...)
	public $item		= NULL;		// array of full row from DB table

	public $review_id	= NULL;		// = id of review (table place_review)
	
	public $forum_id	= NULL;		// = id of forum 
	public $forum_topic_id	= NULL;	// = id of forum topic
	public $forum_post_id	= NULL;	// = id of forum post
	public $forum_action	= NULL;	// forum action

	// other context variables
	public $mobile = NULL;
	
	
	/**
	 * The name of domain, without www and location subdomain
	 * E.g. adultsearch.com, dev.adultsearch.com
	 * @var string
	 */
//	public function getDomain() {}
	
	/**
	* The full name of domain, without www
	* E.g. germany.adultsearch.com, germany.dev.adultsearch.com
	* @var string
	*/
//	public function getDomainFull() {}
	

	private $breadcrumb_array = array();



	//empty constructor	
	public function __construct() {
	}

	
	public function addToBreadcrumb($arr) {
		$this->breadcrumb_array[] = $arr;
	}

	public function getBreadcrumb() {
		$bread = "";
		$cnt = count($this->breadcrumb_array);
		$i = 0;
		foreach($this->breadcrumb_array as $item) {
			$bold = ($cnt == ++$i) ? " class=\"bold\"" : "";
			$raquo = $item['name'] == 'All Locations' ? "" : "&raquo;";
			$bread .= " $raquo <a href=\"{$item['link']}\" title=\"{$item['name']}\"{$bold}>{$item['name']}</a>";
		}
		return $bread;
	}

	public function getGeoLocation() {
		global $smarty;

		if (!array_key_exists("geolocation", $_SESSION)) {
			$smarty->assign("geolocation", "calculated");
			$_SESSION["geolocation"] = geolocation::getLocationByIp(account::getRealIp());
			//$_SESSION["geolocation"] = geolocation::getLocationByIp("72.229.28.185");
		} else {
			$smarty->assign("geolocation", "fromsession");
		}

		return $_SESSION["geolocation"];
	}

}

