<?php

class creditcard {

	private $id = NULL,
			$account_id = NULL,
			$record_id = NULL,
			$trans_id = NULL,
			$type = NULL,
			$cc = NULL,
			$exp_month = NULL,
			$exp_year = NULL,
			$cvc2 = NULL,
			$first_name = NULL,
			$last_name = NULL,
			$address = NULL,
			$zipcode = NULL,
			$city = NULL,
			$state = NULL,
			$country = NULL,
			$bin_info = null,
			$gift_prepaid = null,
			$securionpay_token = null,
			$securionpay_customer_id = null,
			$securionpay_card_id = null,
			$stripe_token = null,
			$total = NULL,
			$shared = 0,
			$approvedStamp = NULL,
			$approvedBy = NULL,
			$image_front = null,
			$image_back = null,
			$deleted = 0,
			$deleted_stamp = null,
			$deleted_by = null;

	private $account = NULL;

	function __construct() {
	}

	public static function findOneById($id) {
		global $db;

		$res = $db->q("SELECT * FROM account_cc WHERE cc_id = ? LIMIT 1", [$id]);
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

		return self::withRow($row);
	}
	
	public static function withRow($row) {

		$cc = new creditcard();
		$cc->setId($row["cc_id"]);
		$cc->setAccountId($row["account_id"]);
		$cc->setRecordId($row["record_id"]);
		$cc->setTransId($row["trans_id"]);
		$cc->setType($row["type"]);
		$cc->setCc($row["cc"]);
		$cc->setExpMonth($row["expmonth"]);
		$cc->setExpYear($row["expyear"]);
		$cc->setCvc2($row["cvc2"]);
		$cc->setFirstName($row["firstname"]);
		$cc->setLastName($row["lastname"]);
		$cc->setAddress($row["address"]);
		$cc->setZipcode($row["zipcode"]);
		$cc->setCity($row["city"]);
		$cc->setState($row["state"]);
		$cc->setCountry($row["country"]);
		$cc->setBinInfo($row["bin_info"]);
		$cc->setGiftPrepaid($row["gift_prepaid"]);
		$cc->setSecurionpayToken($row["securionpay_token"]);
		$cc->setSecurionpayCustomerId($row["securionpay_customer_id"]);
		$cc->setSecurionpayCardId($row["securionpay_card_id"]);
		$cc->setStripeToken($row["stripe_token"]);
		$cc->setTotal($row["total"]);
		$cc->setShared($row["shared"]);
		$cc->setApprovedStamp($row["approved_stamp"]);
		$cc->setApprovedBy($row["approved_by"]);
		$cc->setImageFront($row["image_front"]);
		$cc->setImageBack($row["image_back"]);
		$cc->setDeleted($row["deleted"]);
		$cc->setDeletedStamp($row["deleted_stamp"]);
		$cc->setDeletedBy($row["deleted_by"]);

		return $cc;
	}

	//getters and setters
	public function getId() {
		return $this->id;
	}
	public function getAccountId() {
		return $this->account_id;
	}
	public function getRecordId() {
		return $this->record_id;
	}
	public function getType() {
		return $this->type;
	}
	public function getCc() {
		return $this->cc;
	}
	public function getFirst4() {
		return substr($this->cc, 0, 4);
	}
	public function getLast4() {
		return substr($this->cc, -4);
	}
	public function getTransId() {
		return $this->trans_id;
	}
	public function getExpMonth() {
		return $this->exp_month;
	}
	public function getExpYear() {
		return $this->exp_year;
	}
	public function getCvc2() {
		return $this->cvc2;
	}
	public function getFirstName() {
		return $this->first_name;
	}
	public function getLastName() {
		return $this->last_name;
	}
	public function getAddress() {
		return $this->address;
	}
	public function getZipcode() {
		return $this->zipcode;
	}
	public function getCity() {
		return $this->city;
	}
	public function getState() {
		return $this->state;
	}
	public function getCountry() {
		return $this->country;
	}
	public function getBinInfo() {
		return $this->bin_info;
	}
	public function getGiftPrepaid() {
		return $this->gift_prepaid;
	}
	public function getSecurionpayToken() {
		return $this->securionpay_token;
	}
	public function getSecurionpayCustomerId() {
		return $this->securionpay_customer_id;
	}
	public function getSecurionpayCardId() {
		return $this->securionpay_card_id;
	}
	public function getStripeToken() {
		return $this->stripe_token;
	}
	public function getTotal() {
		return $this->total;
	}
	public function getShared() {
		return $this->shared;
	}
	public function getApprovedStamp() {
		return $this->approved_stamp;
	}
	public function getApprovedBy() {
		return $this->approved_by;
	}
	public function getImageFront() {
		return $this->image_front;
	}
	public function getImageBack() {
		return $this->image_back;
	}
	public function getDeleted() {
		return $this->deleted;
	}
	public function getDeletedStamp() {
		return $this->deleted_stamp;
	}
	public function getDeletedBy() {
		return $this->deleted_by;
	}

	public function setId($id) {
		$this->id = $id;
	}
	public function setAccountId($account_id) {
		$this->account_id = $account_id;
	}
	public function setRecordId($record_id) {
		$this->record_id = $record_id;
	}
	public function setType($type) {
		$this->type = self::convertType($type);
	}
	public function setCc($cc) {
		$this->cc = $cc;
	}
	public function setTransId($trans_id) {
		$this->trans_id = $trans_id;
	}
	public function setExpMonth($exp_month) {
		$this->exp_month = $exp_month;
	}
	public function setExpYear($exp_year) {
		$this->exp_year = $exp_year;
	}
	public function setCvc2($cvc2) {
		$this->cvc2 = substr($cvc2, 0, 4);
	}
	public function setFirstName($first_name) {
		$this->first_name = $first_name;
	}
	public function setLastName($last_name) {
		$this->last_name = $last_name;
	}
	public function setAddress($address) {
		$this->address = $address;
	}
	public function setZipcode($zipcode) {
		$this->zipcode = substr($zipcode, 0, 11);
	}
	public function setCity($city) {
		$this->city = $city;
	}
	public function setState($state) {
		$this->state = $state;
	}
	public function setCountry($country) {
		$this->country = $country;
	}
	public function setBinInfo($bin_info) {
		$this->bin_info = $bin_info;
	}
	public function setGiftPrepaid($gift_prepaid) {
		$this->gift_prepaid = $gift_prepaid;
	}
	public function setSecurionpayToken($securionpay_token) {
		$this->securionpay_token = $securionpay_token;
	}
	public function setSecurionpayCustomerId($securionpay_customer_id) {
		$this->securionpay_customer_id = $securionpay_customer_id;
	}
	public function setSecurionpayCardId($securionpay_card_id) {
		$this->securionpay_card_id = $securionpay_card_id;
	}
	public function setStripeToken($stripe_token) {
		$this->stripe_token = $stripe_token;
	}
	public function setTotal($total) {
		$this->total = $total;
	}
	public function setShared($shared) {
		$this->shared = $shared;
	}
	public function setApprovedStamp($approved_stamp) {
		$this->approved_stamp = $approved_stamp;
	}
	public function setApprovedBy($approved_by) {
		$this->approved_by = $approved_by;
	}
	public function setImageFront($image_front) {
		$this->image_front = $image_front;
	}
	public function setImageBack($image_back) {
		$this->image_back = $image_back;
	}
	public function setDeleted($deleted) {
		$this->deleted = $deleted;
	}
	public function setDeletedStamp($deleted_stamp) {
		$this->deleted_stamp = $deleted_stamp;
	}
	public function setDeletedBy($deleted_by) {
		$this->deleted_by = $deleted_by;
	}

	//aux functions
	/**
	 * This function returns country ISO 3166 2 character code (e.g. "US" for "United States" etc...)
	 */
	public function getCountryCode() {
		global $db;

		$country = $this->country;
		if (preg_match('/^[A-Z][A-Z]$/', $country))
			return $country;

		$res = $db->q("SELECT s FROM location_location WHERE loc_type = 1 AND loc_name = ?", [$country]);
		if (!$db->numrows($res))
			return false;
		$row = $db->r($res);
		$country_code = $row["s"];
		if (preg_match('/^[A-Z][A-Z]$/', $country_code))
			return $country_code;
		return false;
	}

	//relationships
	public function getAccount() {
		if ($this->account)
			return $this->account;
		$acc = account::findOneById($this->account_id);
		if (!$acc)
			return NULL;
		$this->account = $acc;
		return $this->account;
	}	


	public function add() {
		global $db, $account;

		//first store into our db
		$cc = self::ccnoFirstLast($this->cc);
		if ($cc == "")
			$cc = NULL;
		$cvc2 = self::obfuscateCvc2($this->cvc2);
		$this->updateBinInfo();
		$res = $db->q("INSERT INTO account_cc
						(account_id, trans_id, type, cc, expmonth, expyear, cvc2
						, firstname, lastname, address, zipcode, total, city, state, country
						, bin_info, gift_prepaid
						, securionpay_token, securionpay_customer_id, securionpay_card_id, stripe_token
						, approved_stamp, approved_by, image_front, image_back) 
						VALUES
						(?, ?, ?, ?, ?, ?, ?
						, ?, ?, ?, ?, ?, ?, ?, ?
						, ? , ?
						, ?, ?, ?, ?
						, ?, ?, ?, ?);",
						[$this->account_id, $this->trans_id, $this->type, $cc, $this->exp_month, $this->exp_year, $cvc2
						, $this->first_name, $this->last_name, $this->address, $this->zipcode, $this->total, $this->city, $this->state, $this->country
						, $this->bin_info, $this->gift_prepaid
						, $this->securionpay_token, $this->securionpay_customer_id, $this->securionpay_card_id, $this->stripe_token
						, $this->approved_stamp, $this->approved_by, $this->image_front, $this->image_back]
						);
		$newid = $db->insertid($res);
		if ($newid == 0)
			return false;
		$this->id = $newid;

		//backup sensitive data into safe storage
		$ret = safestore::addEntry("cc", $this->id, [
			"cc" => $this->cc, 
			"cvc2" => $this->cvc2,
			]);
		//if failed do nothing, admin should be notified from inside of safestore class
		
		//overwrite sensitive data with obfuscated version
		$this->cc = $cc;
		$this->cvc2 = $cvc2;

		audit::log("CC", "New", $this->id, "", $account->getId());

		return true;
	}

	//update function
	public function update() {
		global $account, $db;

		if (!$this->id)
			return false;

		$original = creditcard::findOneById($this->id);
		if (!$original)
			return false;

		//if we have unobfuscated cc and cvc2, then we must have updated with real ones, update safestore and obfuscate
		if (strpos($this->cc, "x") === false) {
			//update sensitive data in safe storage
			$ret = safestore::updateEntry("cc", $this->id, [
				"cc" => $this->cc,
				"cvc2" => $this->cvc2,
				]);
		}
		//obfuscate cc and cvc2
		$this->cc = self::ccnoFirstLast($this->cc);
		$this->cvc2 = self::obfuscateCvc2($this->cvc2);

		$update_bin_info = false;

		$update_fields = array();
		if ($this->account_id != $original->getAccountId())
			$update_fields["account_id"] = $this->account_id;
		if ($this->record_id != $original->getRecordId())
			$update_fields["record_id"] = $this->record_id;
		if ($this->trans_id != $original->getTransId())
			$update_fields["trans_id"] = $this->trans_id;
		if ($this->type != $original->getType())
			$update_fields["type"] = $this->type;
		if ($this->cc != $original->getCc()) {
			$update_fields["cc"] = $this->cc;
			$update_bin_info = true;
		}
		if ($this->exp_month != $original->getExpMonth())
			$update_fields["expmonth"] = $this->exp_month;
		if ($this->exp_year != $original->getExpYear())
			$update_fields["expyear"] = $this->exp_year;
		if ($this->cvc2 != $original->getCvc2())
			$update_fields["cvc2"] = $this->cvc2;
		if ($this->first_name != $original->getFirstName())
			$update_fields["firstname"] = $this->first_name;
		if ($this->last_name != $original->getLastName())
			$update_fields["lastname"] = $this->last_name;
		if ($this->address != $original->getAddress())
			$update_fields["address"] = $this->address;
		if ($this->zipcode != $original->getZipcode())
			$update_fields["zipcode"] = $this->zipcode;
		if ($this->city != $original->getCity())
			$update_fields["city"] = $this->city;
		if ($this->state != $original->getState())
			$update_fields["state"] = $this->state;
		if ($this->country != $original->getCountry())
			$update_fields["country"] = $this->country;
		if ($this->bin_info != $original->getBinInfo())
			$update_fields["bin_info"] = $this->bin_info;
		if ($this->gift_prepaid != $original->getGiftPrepaid())
			$update_fields["gift_prepaid"] = $this->gift_prepaid;
		if ($this->securionpay_token != $original->getSecurionpayToken())
			$update_fields["securionpay_token"] = $this->securionpay_token;
		if ($this->securionpay_customer_id != $original->getSecurionpayCustomerId())
			$update_fields["securionpay_customer_id"] = $this->securionpay_customer_id;
		if ($this->securionpay_card_id != $original->getSecurionpayCardId())
			$update_fields["securionpay_card_id"] = $this->securionpay_card_id;
		if ($this->stripe_token != $original->getStripeToken())
			$update_fields["stripe_token"] = $this->stripe_token;
		if ($this->total != $original->getTotal())
			$update_fields["total"] = $this->total;
		if ($this->shared != $original->getShared())
			$update_fields["shared"] = $this->shared;
		if ($this->approved_stamp != $original->getApprovedStamp())
			$update_fields["approved_stamp"] = $this->approved_stamp;
		if ($this->approved_by != $original->getApprovedBy())
			$update_fields["approved_by"] = $this->approved_by;
		if ($this->image_front != $original->getImageFront())
			$update_fields["image_front"] = $this->image_front;
		if ($this->image_back != $original->getImageBack())
			$update_fields["image_back"] = $this->image_back;
		if ($this->deleted != $original->getDeleted())
			$update_fields["deleted"] = $this->deleted;
		if ($this->deleted_stamp != $original->getDeletedStamp())
			$update_fields["deleted_stamp"] = $this->deleted_stamp;
		if ($this->deleted_by != $original->getDeletedBy())
			$update_fields["deleted_by"] = $this->deleted_by;

		if ($update_bin_info) {
			$this->updateBinInfo();
			$update_fields["bin_info"] = $this->bin_info;
			$update_fields["gift_prepaid"] = $this->gift_prepaid;
		}

		$changed_fields = "";
		$update = "";
		$update_arr = array();
		foreach ($update_fields as $key => $val) {
			$changed_fields .= (empty($changed_fields)) ? "" : ", ";
			$update .= (empty($update)) ? "SET " : ", ";
			$changed_fields .= $key;
			$update .= "{$key} = ?";
			$update_arr[] = $val;
		}
		$update_arr[] = $this->id;
		if (!$update)
			return true;	//nothing to update
		$update = "UPDATE account_cc {$update} WHERE cc_id = ? LIMIT 1";

		$db->q($update, $update_arr);
		$aff = $db->affected();

		if (!$aff)
			return false;

		audit::log("CC", "Edit", $this->id, "Changed fields: {$changed_fields}", $account->getId());
		return true;
	}

	//additional functions

	public static function convertType($type) {
		if (strlen($type) > 2) {
			$type = strtolower(trim($type));
			switch ($type) {
				case "visa": $type = "VI"; break;
				case "mastercard": $type = "MC"; break;
				case "discover": $type = "DI"; break;
				case "jcb": $type = "JC"; break;
				case "amex": $type = "AM"; break;
				case "maestro": $type = "MA"; break;
				default: $type = "OT"; break;
			}
		}
		return $type;
	}

	public static function getTypeName($type) {
		if (strlen($type) != 2)
			return "";
		switch ($type) {
			case "VI": $name = "Visa"; break;
			case "MC": $name = "Mastercard"; break;
			case "DI": $name = "Discover"; break;
			case "JC": $name = "JCB"; break;
			case "AM": $name = "Amex"; break;
			case "MA": $name = "Maestro"; break;
			default: $name = "Other"; break;
		}
		return $name;
	}

	public function remove($audit_who = NULL) {
		global $account, $db;

		$db->q(
			"UPDATE account_cc SET deleted = 1, deleted_stamp = ?, deleted_by = ? WHERE cc_id = ? LIMIT 1", 
			[time(), $account->getId(), $this->id]
			);
		$aff = $db->affected();
		if ($aff == 1) {
			if ($audit_who == NULL)
				$audit_who = $account->getId();
			audit::log("CC", "Remove", $this->id, "", $audit_who);
			return true;
		}
		return false;
	}

	public function ban($reason = "", $total = NULL, $module = NULL) {
		global $account, $db;

		if ($total == NULL)
			$total = $this->total;
		if ($module == NULL)
			$module = $this->module;

		$db->q("INSERT IGNORE INTO account_cc_ban 
				(cc, `month`, `year`, cvc, firstname, lastname, 
				zipcode, city, state, country, address, total, module,
				`time`, reason)
				values
				(?, ?, ?, ?, ?, ?, 
				?, ?, ?, ?, ?, ?, ?, 
				NOW(), ?)
				ON DUPLICATE KEY UPDATE total = total + ?, module = ?",
				array(
					$this->cc, $this->exp_month, $this->exp_year, $this->cvc2, $this->first_name, $this->last_name, 
					$this->zipcode, $this->city, $this->state, $this->country, $this->address, $total, $module, 
					$reason,
					$total, $module
					)
				);
		$aff = $db->affected();

		if ($aff) {
			//audit this operation
			$msg = "";
			if ($reason != "") {
				$msg = "Reason: {$reason}";
			}
			audit::log("CC", "Ban", $this->id, $msg, $account->getId());
		}

		return $aff;
	}
	
	public function unban() {
		global $account, $db;

		$db->q("DELETE FROM account_cc_ban WHERE cc = ? AND cvc = ?", array($this->cc, $this->cvc2));
		$aff = $db->affected();
		if ($aff) {
			//audit this operation
			audit::log("CC", "Unban", $this->id, "Affected: {$aff}", $account->getId());
		}

		return $aff;
	}

	public static function usedByOtherAccount($cc, $cvc, $exp_month, $exp_year, $account_id) {
		global $db;

		$cc = self::ccnoFirstLast($cc);
		$cvc = self::obfuscateCvc2($cvc);
		
		$res = $db->q(
			"SELECT account_id FROM account_cc WHERE cc = ? AND cvc2 = ? AND expmonth = ? AND expyear = ? AND account_id <> ? AND deleted_stamp IS NULL",
			[$cc, $cvc, $exp_month, $exp_year, $account_id]
			);
		if ($db->numrows($res))
			return true;

		return false;
	}

	public static function isBannedAux($cc, $cvc, $zipcode, $module = null) {
		global $db;

		$cc = self::ccnoFirstLast($cc);
		$cvc = self::obfuscateCvc2($cvc);
		$zipcode = preg_replace('/[^0-9A-Z]/', '', strtoupper($zipcode));
		
		$res = $db->q("SELECT module
						FROM account_cc_ban
						WHERE cc = ? AND cvc = ? AND zipcode = ?
						LIMIT 1",
						[$cc, $cvc, $zipcode]
					);
		if (!$db->numrows($res))
			return false;

		$rox = $db->r($res);

		if (!$module || empty($rox['module']) || $rox['module'] == $module)
			return true;

		return false;
	}

	public function isBanned($module = NULL) {
		return self::isBannedAux($this->cc, $this->cvc2, $this->zipcode, $module);
	}


	/**
	 * converts cc number from 4111222233334444 to 411122xxxxxx4444
	 */
	public static function ccnoFirstLast($ccno) {
		$ccno = preg_replace('/[^0-9x]/', '', $ccno);
		$len = strlen($ccno);
		if ($len < 10)
			return $ccno;
		$ccno = substr($ccno, 0, 6).str_repeat("x",$len-10).substr($ccno, -4);
		return $ccno;
	}

	public static function obfuscateCvc2($cvc2) {
		if (strlen($cvc2) < 2)
			return $cvc2;
		$cvc2 = str_repeat("x", strlen($cvc2) - 1).substr($cvc2, -1);
		return $cvc2;
	}

	public function decode($ss_private_key) {
		$item = null;
		$ret = safestore::getEntryById("cc", $this->id, $ss_private_key, $item);
		if (!$ret)
			return false;
		return $item;
	}

	public static function sendBannedCardNotification($account_id, $cc, $cc_month, $cc_year, $firstname, $lastname, $zipcode) {

		$to = SUPPORT_EMAIL;
		if ($account_id == 3974)
			$to = ADMIN_EMAIL;

		$cc = self::ccnoFirstLast($cc);

		$msg = "Account <a href=\"https://adultsearch.com/mng/accounts?account_id={$account_id}\">#{$account_id}</a> tried to pay with credit card that is on ban list:<br />";
		$msg .= "{$cc} - {$cc_month}/{$cc_year}<br />";
		$msg .= "Billing details:<br />";
		$msg .= "Name: {$firstname} {$lastname}<br />";
		$msg .= "Zip: {$zipcode}<br />";

		$msg .= "<br />Please <a href=\"https://adultsearch.com/mng/creditcards?cc_first4=".substr($cc, 0, 4)."&cc_last4=".substr($cc, -4)."&firstname={$firstname}&lastname={$lastname}\">check here</a> if this is the same card that was banned before, and check the accounts that were trying to pay with this card before. You need to evaluate and consider (e.g. if these accounts are banned: spammers / chargebacks) <strong>banning this account and refunding/cancelling all transactions</strong> as well.<br />";

		$msg .= "<br />If you have any questions, contact Jay.<br />";

		send_email([
			"from" => "noreply@adultsearch.com",
			"to" => $to,
			"bcc" => ADMIN_EMAIL,
			"subject" => "AS: Attempt to use banned CC",
			"html" => $msg,
			]);

		return true;
	}

	public static function get_cards() {
		global $db, $account;

		$cards = [];
		$res = $db->q("
			SELECT * 
			FROM account_cc 
			WHERE account_id = ? AND approved_stamp IS NOT NULL AND (banned IS NULL OR banned = 0) AND banned_stamp IS NULL AND deleted_stamp IS NULL 
			ORDER BY cc_id desc",
			[$account->getId()]
			);
		while ($row = $db->r($res)) {
			$cards[] = [
				"id" => $row["cc_id"],
				"label" => substr($row["cc"], -5)." ".$row["expmonth"]."/".$row["expyear"],
				"firstname" => $row["firstname"],
				"lastname" => $row["lastname"],
				"address" => $row["address"],
				"city" => $row["city"],
				"state" => $row["state"],
				"zipcode" => $row["zipcode"],
				"country" => $row["country"],
				"month" => $row["expmonth"],
				"year" => $row["expyear"],
				"securionpay_token" => $row["securionpay_token"],
				"stripe_token" => $row["stripe_token"],
				"type" => $row["type"]
				];
		}

		return $cards;
	}

	//store submitted card images
	public function store_card_images($image_front_data, $image_back_data) {
		global $config_image_path;

		$cards_dir = $config_image_path."/cards/";

		if (!$image_front_data || !$image_back_data)
			return false;

		//front
		$encoded = str_replace(' ', '+', substr($image_front_data,strpos($image_front_data,",")+1));
		if (!$encoded) {
			reportAdmin("AS: creditcard::store_card_images error", "can get encoded part");
			return false;
		}
		do {
			$filename = $this->getId()."_front_".system::_makeText(8).".jpg";
			$filepath = $cards_dir."/".$filename;
		} while(file_exists($filepath));
		$ret = file_put_contents($filepath, base64_decode($encoded));
		if ($ret) {
			if ($this->getImageFront() && $card->getImageFront() != $filename) {
				@unlink($cards_dir."/".$this->getImageFront());
			}
			$this->setImageFront($filename);
		} else {
			reportAdmin("AS: pay::charge error - failed to save card front photo", "filepath='{$filepath}', strlen(encoded)=".strlen($encoded));
		}

		//back
		$encoded = str_replace(' ', '+', substr($image_back_data,strpos($image_back_data,",")+1));
		if (!$encoded) {
			reportAdmin("AS: creditcard::store_card_images error", "can get encoded part");
			return false;
		}
		do {
			$filename = $this->getId()."_back_".system::_makeText(8).".jpg";
			$filepath = $cards_dir."/".$filename;
		} while(file_exists($filepath));
		$ret = file_put_contents($filepath, base64_decode($encoded));
		if ($ret) {
			if ($this->getImageBack() && $card->getImageBack() != $filename) {
				@unlink($cards_dir."/".$this->getImageBack());
			}
			$this->setImageBack($filename);
		} else {
			reportAdmin("AS: pay::charge error - failed to save card front photo", "filepath='{$filepath}', strlen(encoded)=".strlen($encoded));
		}

		$this->update();
		return true;
	}

	public function getFrontImageThumbData() {
		global $config_image_path;

		$cards_dir = $config_image_path."/cards/";

		if (!$this->getApprovedStamp())
			return null;

		$filename = $payment->getCard()->getImageFront();
		if (!$filename)
			return null;

		$filepath = $cards_dir."/".$filename;
		if (!file_exists($filepath))
			return null;

		$thumbdata = $this->resizeImage($filepath, 150, 100);

		return "data:image/jpeg;base64,".$thumbdata;
	}

	public function getBackImageThumbData() {
		global $config_image_path;

		$cards_dir = $config_image_path."/cards/";

		if (!$this->getApprovedStamp())
			return null;

		$filename = $payment->getCard()->getImageBack();
		if (!$filename)
			return null;

		$filepath = $cards_dir."/".$filename;
		if (!file_exists($filepath))
			return null;

		$thumbdata = $this->resizeImage($filepath, 150, 100);

		return "data:image/jpeg;base64,".$thumbdata;
	}

	private function resizeImage($file, $w, $h, $crop=FALSE) {
		list($width, $height) = getimagesize($file);
		$r = $width / $height;
		if ($crop) {
			if ($width > $height) {
				$width = ceil($width-($width*abs($r-$w/$h)));
			} else {
				$height = ceil($height-($height*abs($r-$w/$h)));
			}
			$newwidth = $w;
			$newheight = $h;
		} else {
			if ($w/$h > $r) {
				$newwidth = $h*$r;
				$newheight = $h;
			} else {
				$newheight = $w/$r;
				$newwidth = $w;
			}
		}
		$src = imagecreatefromjpeg($file);
		$dst = imagecreatetruecolor($newwidth, $newheight);
		imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

		ob_clean();
		ob_start();
		imagejpeg($dst);
		$thumb_raw_data = ob_get_clean();

		return base64_encode($thumb_raw_data);
	}

	public static function get_cards_active_subscription()
	{
		global $db, $account;
		$cards = [];
		$res = $db->q(
			"SELECT cc.*, p.subscription_status, DATE_FORMAT(p.next_renewal_date, '%m/%d/%Y') as next_renewal_date, c.id as ad_id, c.title as ad_desc
				FROM account_cc cc
				LEFT JOIN payment p ON cc.cc_id = p.cc_id
				LEFT JOIN payment_item pi ON p.id = pi.payment_id
				LEFT JOIN classifieds c ON pi.classified_id = c.id
				WHERE cc.account_id = ? 
				AND cc.approved_stamp IS NOT NULL 
				AND (cc.banned IS NULL OR cc.banned = 0) 
				AND cc.banned_stamp IS NULL 
				AND cc.deleted_stamp IS NULL
				ORDER BY cc.cc_id DESC",
			[$account->getId()]
		);

		while ($row = $db->r($res)) {
			$cards[] = [
				"id" => $row["cc_id"],
				"label" => substr($row["cc"], -5) . " " . $row["expmonth"] . "/" . $row["expyear"],
				"firstname" => $row["firstname"],
				"lastname" => $row["lastname"],
				"address" => $row["address"],
				"city" => $row["city"],
				"state" => $row["state"],
				"zipcode" => $row["zipcode"],
				"country" => $row["country"],
				"month" => $row["expmonth"],
				"year" => $row["expyear"],
				"securionpay_token" => $row["securionpay_token"],
				"stripe_token" => $row["stripe_token"],
				"type" => $row["type"],
				"subscription_status" => $row["subscription_status"],
				"cc_type" => self::getTypeName($row["type"]),
				"last4" => substr($row["cc"], -5),
				"ad_id" => $row["ad_id"],
				"ad_desc" => $row["ad_desc"],
				"next_renewal_date" => $row["next_renewal_date"]
			];
		}

		return $cards;
	}

	public static function delete($id){
		$cre = self::findOneById($id);
		if($cre)
			return $cre->remove();
		return false;
	}

	public function updateBinInfo() {
		global $db;

		$prefix = intval(substr($this->cc, 0, 6));
		$res = $db->q("SELECT b.* FROM binbase b WHERE b.bin = ?", [$prefix]);

		if ($db->numrows($res) == 0) {
			$this->bin_info = null;
			$this->gift_prepaid = null;
			return true;
		} else if ($db->numrows($res) > 1) {
			//this is error
			reportAdmin("AS: creditcard:updateBinInfo error", "multiple results for prefix '{$prefix}' !");
			$this->bin_info = null;
			$this->gift_prepaid = null;
			return false;
		}

		$row = $db->r($res);
		$bin_info = [
			"id"	=> strval($row["id"]),
			"bin"   => strval($row["bin"]),
			"brand" => $row["brand"],
			"bank"  => $row["bank"],
			"type"  => $row["type"],
			"level" => $row["level"],
			"isocountry" => $row["isocountry"],
			"isoa2" => $row["isoa2"],
			"isoa3" => $row["isoa3"],
			"isonumber" => strval($row["isonumber"]),
			"www"   => $row["www"],
			"phone" => $row["phone"],
			];
		$this->bin_info = json_encode($bin_info);

		$prepaid_gift = null;
		$level = $row["level"];
		if (strpos($level, "gift") !== false
			|| strpos($level, "prepaid") !== false
			|| strpos($level, "prepago") !== false
			)
			$prepaid_gift = 1;
		$this->prepaid_gift = $prepaid_gift;
		return true;
	}

}

?>
