<?php

class csrf {

	function __construct() {
    }

	private static function get_name() {
		return "csrf_".substr(md5(rand(1,200).time()), 0, 5);
	}

	private static function get_value() {
		return md5(rand(201,1000).time());
	}

	public static function get_html_code($form_identifier) {
		if (!$form_identifier)
			return false;

		$session_csrf = $_SESSION["csrf"];
		if (!is_array($session_csrf))
			$session_csrf = array();

		$name = self::get_name();
		$value = self::get_value();

		$session_csrf[$form_identifier] = "{$name}:{$value}";
		$_SESSION["csrf"] = $session_csrf;
		
		return "<input type=\"hidden\" name=\"{$name}\" value=\"{$value}\" />";
	}

	private static function log ($str) {
		$str = replaceIpInStr($str);
		error_log(date("[d-m-Y G:i:s T]")." [".account::getUserIp()."] [".$_SERVER["HTTP_REFERER"]."] [".$_SERVER["HTTP_USER_AGENT"]."] [".$_SERVER["HTTPS"]."]{$str}\n", 3, _CMS_ABS_PATH."/cms/log/php_debug_log");
	}

	public static function check($form_identifier) {
		if (!$form_identifier)
			return false;

		$session_csrf = $_SESSION["csrf"];
		if (!is_array($session_csrf)) {
			self::log("CSRF::check SES[csrf] does not exist, SES=".print_r($_SESSION, true).", REQ=".print_r($_REQUEST, true));
			return false;
		}

		if (!array_key_exists($form_identifier, $session_csrf)) {
			self::log("CSRF::check form_ident '{$form_identifier}' does not exist in SES[csrf], SES[csrf]=".print_r($_SESSION["csrf"], true).", REQ=".print_r($_REQUEST, true));
			return false;
		}

		$csrf = $session_csrf[$form_identifier];
		$arr = explode(":", $csrf);
		if (!is_array($arr) || count($arr) != 2)
			return false;

		$name = $arr[0];
		$value = $arr[1];

		if (!array_key_exists($name, $_REQUEST)) {
			self::log("CSRF::check not exist in REQ (name='{$name}')");
			return false;
		}

		if ($_REQUEST[$name] != $value) {
			self::log("CSRF::check not equal: REQ='{$_REQUEST[$name]}', SES='{$value}'");
			return false;
		}

		return true;
	}

}

