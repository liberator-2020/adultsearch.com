<?php

require_once(_CMS_ABS_PATH.'/inc/classes/Exceptions/GetMapImageException.php');

class curl {

	const IMAGE_CONTENT_TYPE = 'image/jpeg';
	const HTTP_CODE_200 = 200;

	var $path_to_cookie = NULL;
	var $browser_agent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.113 Safari/534.30";
	var $email = NULL, $pass = NULL;
	var $null = false, $ip, $proxy = NULL, $debug = false;

	var $errno = NULL;
	var $error = NULL;

	public function __construct($cookie = NULL, $delold = 0) {

		$this->path = _CMS_ABS_PATH."/inc/classes/";
		$this->path = "./";

		if( $delold )
			@unlink($this->path.$cookie);
		
		if( $cookie )
			$this->path_to_cookie = $this->path.$cookie;

	}

	/**
	 * @param string $url
	 * @param int    $follow
	 * @param bool   $debug
	 * @param null   $headers
	 * @return bool|string
	 * @throws \GetMapImageException
	 */
	public function getImage($url, $follow = 1, $debug = false, $headers = null) {
		$this->errno = $this->error = null;
		$curl        = curl_init();

		$response = $this->curlExec($url, $follow, $debug, $headers, $curl);

		if (curl_errno($curl)) {
			if ($debug) {
				echo curl_error($curl);
			}
			$this->errno = curl_errno($curl);
			$this->error = curl_error($curl);
		}

		if (curl_getinfo($curl)['http_code'] !== self::HTTP_CODE_200) {
			throw new GetMapImageException($response, curl_getinfo($curl)['http_code']);
		}

		if (curl_getinfo($curl)['content_type'] !== self::IMAGE_CONTENT_TYPE) {
			throw new GetMapImageException();
		}

		curl_close($curl);

		return $response;
	}

	/**
	 * @param string $url
	 * @param int    $follow
	 * @param bool   $debug
	 * @param null   $headers
	 * @return bool|string
	 */
	public function get($url, $follow = 1, $debug = false, $headers = null) {

		$this->errno = $this->error = null;
		$curl        = curl_init();

		$result = $this->curlExec($url, $follow, $debug, $headers, $curl);

		if (curl_errno($curl)) {
			if ($debug) {
				echo curl_error($curl);
			}
			$this->errno = curl_errno($curl);
			$this->error = curl_error($curl);
		}

		curl_close($curl);

		return $result;
	}

	/**
	 * @param string         $url
	 * @param int|bool       $follow
	 * @param bool           $debug
	 * @param                $headers
	 * @param resource|false $curl
	 * @return bool|string
	 */
	private function curlExec($url, $follow, $debug, $headers, &$curl) {
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

		if ($this->debug) {
			curl_setopt($curl, CURLOPT_VERBOSE, true);
			curl_setopt($curl, CURLOPT_HEADER, true);
		}

		if (!is_null($headers)) {
			curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		}

		if ($this->ip) {
			if (is_array($this->ip)) {
				$ip = $this->ip[rand(1, count($this->ip) - 1)];
			} else {
				$ip = $this->ip;
			}
			curl_setopt($curl, CURLOPT_INTERFACE, $ip);
			if ($debug) {
				echo "cURL ip is $ip\n";
			}
		}

		if ($this->path_to_cookie) {
			if ($debug) {
				echo "what ? ";
			}
			curl_setopt($curl, CURLOPT_COOKIEJAR, $this->path_to_cookie);
			curl_setopt($curl, CURLOPT_COOKIEFILE, $this->path_to_cookie);
		}

		if ($this->proxy) {
			if ($debug) {
				echo "proxy is enabled..\n";
			}
			curl_setopt($curl, CURLOPT_PROXY, $this->proxy);
		}

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $follow);
		curl_setopt($curl, CURLOPT_USERAGENT, $this->browser_agent);

		return curl_exec($curl);
	}

	public static function getCurlValue($filename, $content_type) {
		// PHP 5.5 introduced a CurlFile object that deprecates the old @filename syntax
		// See: https://wiki.php.net/rfc/curl-file-upload
		if (function_exists('curl_file_create')) {
			return curl_file_create($filename, $content_type);
		}

		// Use the old style if using an older version of PHP
		$value = "@{$filename}";
		if ($content_type) {
			$value .= ';type='.$content_type;
		}

		return $value;
	}

	public function post($url, $postal_data, $follow = 1, $headers = NULL){
		$ch=curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postal_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		if (!is_null($headers)) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}
		if( $this->debug ) {
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_HEADER, true);
		}
		if( $this->ip ) {
			if( is_array($this->ip) ) curl_setopt($ch,CURLOPT_INTERFACE, $this->ip[rand(count(1,$this->ip))]);
			else curl_setopt($ch,CURLOPT_INTERFACE, $this->ip);
		}
		if( $this->path_to_cookie ) {
			curl_setopt($ch,CURLOPT_COOKIEJAR,$this->path_to_cookie);
			curl_setopt($ch,CURLOPT_COOKIEFILE,$this->path_to_cookie);
		}
		if( $this->proxy ) {
			echo "proxy is enabled..\n";
			curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
		}
		if( $follow ) curl_setopt($ch,CURLOPT_FOLLOWLOCATION, $follow);
		curl_setopt($ch,CURLOPT_USERAGENT, $this->browser_agent);
		$result=curl_exec($ch);
		if( $result == false ) echo "CURL ERROR: ".curl_error($ch) ."\npst: ".print_r($postal_data)."\n";
		curl_close($ch);
		return $result;
	}

	function download($file_source, $file_target, &$error = NULL) {
		$timeout = 15;
		$old = ini_set('default_socket_timeout', $timeout);
		$rh = fopen($file_source, 'rb');
		$wh = fopen($file_target, 'wb');

		if ($rh===false || $wh===false) {
			@fclose($rh);
			@fclose($wh);
			$error = "rh or wh is false";
			return false;
		}
		ini_set('default_socket_timeout', $old);
		stream_set_timeout($rh, $timeout);
		stream_set_blocking($rh, 0);

		while (!feof($rh)) {
			if (fwrite($wh, fread($rh, 1024)) === FALSE) {
				$error = "Cannot write to file ($file_target)";
				return false;
			}
		}
		fclose($rh);
		fclose($wh);
		return true;
	}
}

