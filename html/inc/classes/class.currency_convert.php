<?php
/**
 * currency convert
 * 
 * Class responsible for converting currency. Used only as a service -> all methods are static
 * 
 * @TODO Daily resetting values in memcached !
 * @TODO Domain names set on setcookie function ?
 * @TODO redo db class and redo / simplify the queries in get_conversion_rate ?
 */
class currency_convert {

	//memcached cache
	private static $mc = NULL;
	
	//this cache is requst context only (in case we want to convert multiple amounts on one page)
	private static $conversion_cache = array();

	private static $smarty = null;
	private static $amount_id = 1;


	/**
	 * Function returns TRUE, if we are able to convert this currency, FALSE otherwise
	 */
	public static function isConvertible($currency) {
		global $db;

		//currency code must consist of exactly 3 capital characters
		if (preg_match("/^[A-Z]{3}$/", $currency) !== 1)
			return false;

		$res = $db->q("select * from currency where code = '{$currency}'");
		if ($db->NumRows($res) == 1) {
			return true;
		}

		return false;
	}

	/**
	 * Method get_conversion_rate returns conversion rate from one currency to another
	 * Method expect the currencies are valid 3 characer strings (ISO 4217 currency codes)
	 * 
	 * @param string $curr_from Currency we are converting from
	 * @param string $curr_to Currency we are converting to
	 */
	private static function get_conversion_rate($curr_from, $curr_to) {
		global $config, $db;
		
		if( $curr_from == $curr_to ) return 1;
	
		$key = "$curr_from|$curr_to";
		
		//check if conversion rate is in class cache
		if (array_key_exists($key, self::$conversion_cache))
			return self::$conversion_cache[$key];
		
		//check if conversion rate is in memcached cache
		if (self::$mc === NULL) {
			self::$mc = new Memcached();
			self::$mc->addServer($config->memcached_server, $config->memcached_port);
		}
		$mc_key = "curr_rate_".$key;
		if (($rate = self::$mc->get($mc_key)) !== FALSE) {
			self::$conversion_cache[$key] = $rate;	//store rate in class cache
			return $rate;
		}

		//conversion rate not in cache, pull it up from DB
		if ($curr_from == "USD") {
			$res = $db->Q("select `to` from currency where code = '$curr_to'");
			if ($db->NumRows($res) == 0)
				return FALSE;
			$rate = $db->R($res);
			$rate = $rate[0];
		} else if ($curr_to == "USD") {
			$res = $db->Q("select `from` from currency where code = '$curr_from'");
			if ($db->NumRows($res) == 0)
				return FALSE;
			$rate = $db->R($res);
			$rate = $rate[0];
		} else {
			$res = $db->Q("select `from` from currency where code = '$curr_from'");
			if ($db->NumRows($res) == 0)
				return FALSE;
			$tousd_rate = $db->R($res);
			$tousd_rate = $tousd_rate[0];
			$res = $db->Q("select `to` from currency where code = '$curr_to'");
			if ($db->NumRows($res) == 0)
				return FALSE;
			$fromusd_rate = $db->R($res);
			$fromusd_rate = $fromusd_rate[0];
			$rate = $tousd_rate * $fromusd_rate;
		}
		//_d("Rate calculated from db: ".$rate);
		
		//store rate in class cache
		self::$conversion_cache[$key] = $rate;
		
		//store rate in memcached cache
		self::$mc->set($mc_key, $rate, 60*60*24 + 60);	//expiration set to:  +1 day +1 minute
		
		return $rate;
	}

	/**
	 * Function convert returns converted amount
	 */
	public static function convert($curr_from, $curr_to, $value) {
		$rate = self::get_conversion_rate($curr_from, $curr_to);
		if ($rate === FALSE)
			return FALSE;
		//_d("Rate $curr_from -> $curr_to = $rate");
		return round($rate * $value, 2);
	}

	/**
	 * Ajax server handler of currency convert widget
	 *
	 * Recalculates all numbers from specified currency to other currency
	 */
	public static function ajax($url_args, $req_args) {

		//synchronize with currency_convert.js !
		$curr_to = $req_args['to'];
		$what = $req_args['what'];
		
		//if we want to disable currency conversion, remove cookie preferred currency
		if ($curr_to == "DISABLE") {
			setcookie('preferredCurrency', '', time()-7*24*60*60, '/', '.adultsearch.com');		//, 'as');
			die;
		}
		
		//currency code must otherwise consist of exactly 3 capital characters
		if (preg_match("/^[A-Z]{3}$/", $curr_to) !== 1) {
			echo "Wrong currency!";
			die;
		}
		
		if (empty($what)) {
			echo "Wrong subject!";
			die;
		}
		
		$items = explode("@", $what);
		$result = '';
		foreach($items as $item) {
			$parts = explode("|", $item);
			if (count($parts) != 4) {
				echo "Wrong input!";
				die;
			}
			
			$id = $parts[0];
			if (preg_match("/^[A-Za-z0-9-_]+$/", $id) !== 1)
				die;
			
			$curr_from = $parts[1];
			//currency code must consist of exactly 3 capital characters
			if (preg_match("/^[A-Z]{3}$/", $curr_from) !== 1)
				die;
			
			$amount = $parts[2];
			if (preg_match("/^[0-9\.]+$/", $amount) !== 1)
				die;
			
			$amount_transformed = self::convert($curr_from, $curr_to, $amount);
			if ($amount_transformed === FALSE) {
				echo "Convert failed!";
				continue;
			}

			$res = "{$id}|".self::getPriceTag($curr_to, $amount_transformed)."|".self::getPriceTag($curr_from, $amount);
			if ($result != "")
				$result .= "@";
			$result .= $res;
		}
		if ($curr_to != "DISABLE")
			setcookie('preferredCurrency', $curr_to, time()+31*24*60*60, '/', '.adultsearch.com');	//, 'as');
		echo $result;
		die;
	}

	public static function getPriceTag($currency, $amt) {
		switch ($currency) {
			case 'USD': return '$ '.number_format($amt, 2); break;
			case 'GBP': return '&pound; '.number_format($amt, 2); break;
			case 'JPY': return '&yen; '.number_format($amt, 2); break;
			case 'EUR': return '&euro; '.number_format($amt, 2); break;
			case 'BRL': return "$amt Real"; break;
			case "CZK": return "$amt CZK"; break;
			case 'CNY': return "$amt Yuan"; break;
			case "MUR": return "$amt Rupee"; break;
			case "MYR": return "$amt Ringgit"; break;
			case "PHP": return "$amt Pesos"; break;
			case "SGD": return "$amt Sing. Dollar"; break;
			case "THB": return "$amt Baht"; break;
			case "TRY": return "$amt TL"; break;
			default:	return "$amt $currency"; break;
		}
		return false;
	}

	public static function getPriceTagOnly($currency) {
		switch ($currency) {
			case 'USD': return '$';
			case 'GBP': return '&pound;';
			case 'JPY': return '&yen;';
			case 'EUR': return '&euro;';
			case 'BRL': return "Real"; break;
			case "CZK": return "CZK"; break;
			case 'CNY': return "Yuan"; break;
	   		case "MUR": return "Rupee"; break;
	   		case "MYR": return "Ringgit"; break;
	   		case "PHP": return "Pesos"; break;
			case "SGD": return "Sing. Dollar"; break;
			case "THB": return "Baht"; break;
			case "TRY": return "TL"; break;
			default:	return "$currency"; break;
		}
		return false;
	}

	/**
	 * display_amount
	 * 
	 * Method returns HTML code with currency amount, which is suitable for automatic currency changing 
	 * 
	 * @param string $currency Code of the original currency, e.g. 'USD', 'GBP', etc...
	 * @param number $amount The amount in the original currency, e.g. 12.50
	 */
	public static function display_amount($currency, $amount) {
		if (self::$smarty == null)
			self::$smarty = GetSmartyInstance();
		$preset = $_COOKIE["preferredCurrency"];

		self::$smarty->assign("id", "currency_amount_id".self::$amount_id++);
		$orig_pricetag = self::getPriceTag($currency, $amount);
		self::$smarty->assign("data_original", "$currency|$amount|$orig_pricetag");
		
		if (!empty($preset) && $preset != $currency) {
			$amount_converted = self::convert($currency, $preset, $amount);
			if ($amount_converted !== FALSE) {
				self::$smarty->assign("display_amount", self::getPriceTag($preset, $amount_converted));
				self::$smarty->assign("original_amount", $orig_pricetag);
				return self::$smarty->fetch(_CMS_ABS_PATH."/templates/currency_convert/currency_convert_amt.tpl");
			}
		}
		
		self::$smarty->assign("display_amount", $orig_pricetag);
		return self::$smarty->fetch(_CMS_ABS_PATH."/templates/currency_convert/currency_convert_amt.tpl");
	}
}

