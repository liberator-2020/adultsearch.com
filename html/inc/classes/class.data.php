<?php

define("COL_ID",		0);
define("COL_STRING",	1);
define("COL_TEXT",	  3);
define("COL_NUMBER",	4);
define("COL_BOOL",	  5);
define("COL_FOREIG",	6);
define("COL_FOREIGN",   6);
define("COL_DATE",	7);
define("COL_IMAGE",	 9);
define("COL_ENUM",	  10);
define("COL_MUL_FOREIGN",21);
define("COL_LOCATION",  27);
define("COL_FEE",  28);
define("COL_HOUR",  29);
define("COL_PICTURE",  30);
define("COL_LATLONG",  31);
define("COL_PHONE",  32);
define("COL_COVER", 33);
define("COL_MANUAL", 35);
define("COL_HH", 36);
define("COL_LOCATIONAJAX",  37);
define("COL_BOOL_3",	38);
define("COL_FILES",  39);
define("COL_FEE_RANGE",  40);
define("COL_PHONE_2",  41);

global $config_image_path, $config_image_backup, $config_image_server;

if( !defined("IMAGE_URL") ) define("IMAGE_URL", $config_image_server?$config_image_server:"//img.adultsearch.com");
if( !defined("IMAGE_URL_PATH") ) define("IMAGE_URL_PATH", "/");
if( !defined("IMAGE_PATH") ) define("IMAGE_PATH", $config_image_path);
define("IMAGE_BACKUP", $config_image_backup);

function null_or_intval($post_name) {
	$val = GetPostParam($post_name);
	if (is_numeric($val))
		$val = intval($val);
	else
		$val = "NULL";
	return $val;
}

function null_or_doubleval($post_name) {
	$val = GetPostParam($post_name);
	if (is_numeric($val))
		$val = doubleval($val);
	else
		$val = "NULL";
	return $val;
}

class CColumn {
	var $colName, $colTitle, $colType;

	var $dlist;

	/**
	 * Value of column (used in UPDATE and INSERT statements)
	 * @access private
	 * @var mixed
	 */
	var $colValue;

	/**
	 * Full name (with table name) of column in SQL table (e.g. users.username )
	 * @access private
	 * @var string
	 */
	var $colNameFull;

	/**
	 * Array of column parameters (use SetParameter() and GetParameter() methods to set/get parameters)
	 * @access private
	 * @var array
	 */
	var $colParameters;
	var $colMandatory;
	var $updateOnSkip;
	var $updateOnDelete;
	var $colReadOnly;
	var $colHidden;

	/**
	 * Show remove link for the item
	*/
	var $show_remove = false;

	public function __construct() {
		$this->colName = '';
		$this->colTitle = '';
		$this->colType = '';
		$this->dlist = "";
		$this->colValue = "";
		$this->colUpdate = true;
		$this->colInsert = true;
		$this->colEditField = true;
		$this->colParameters = array();
		$this->colMandatory	 = false;
		$this->colIdentifier = '';
		$this->updateOnSkip = false;
		$this->updateOnDelete = false;
		$this->colReadOnly	 = false;		
		$this->colIndex 	= '';
	}

	function DrawEditFieldCaption() {
		if( $this->colMandatory ) 
			$this->colTitle = "<span style=\"color: red;\">*</span>{$this->colTitle}";
	}

	function DrawEditField() {
		echo "<span style=\"color: red; font-weight: bold;\">! DrawEditField() for this column type not defined !</span>";
		die();
	}

	function SetColName($full_name) {
		$this->colNameFull = $full_name;

		if (($pos = strpos($full_name, ".")) !== false) {
			$this->colName = substr($full_name, $pos+1);
		} else {
			$this->colName = $full_name;
		}
	}

	function SetColInsert($val) {
		$this->colInsert = $val;
	}
	function SetColUpdate($val) {
		$this->colUpdate = $val;
	}
	function SetColMandatory($mandatory) {
		$this->colMandatory = $mandatory;
	}
	function SetColIdentifier($identifier) {
		$this->colIdentifier = $identifier;
	}
	function GetColIdentifier() {
		return $this->colIdentifier;
	}
	function updateOnSkip() {
		$this->updateOnSkip = true;
	}
	function updateOnDelete() {
		$this->updateOnDelete = true;
	}
	function SetColReadOnly($val) {
		$this->colReadOnly = $val;
	}
	function SetColHidden($val) {
		$this->colHidden = $val;
	}
	function GetColReadOnly() {
		return $this->colReadOnly;
	}
	function GetQuotedValue() {
		echo "<span style=\"color: red; font-weight: bold;\">! GetQuotedValue() for this column type not defined !</span>";
		die();
	}
	
	function update() {
		echo "<span style=\"color: red; font-weight: bold;\">! update() for this column type not defined !</span>";
		die();
	}

	function insert(&$insert) {
		echo "<span style=\"color: red; font-weight: bold;\">! insert() for this column type not defined !</span>";
		die();
	}

	function BeforeSave() {
		return true;
	}
	function _BeforeSave() {
		return true;
	}
	function AfterSave() {
		return true;
	}
	function _AfterSave() {
		return true;
	}
	function _BeforeDelete() {
		return true;
	}
	function _AfterDelete() {
		return true;
	}
}

class CColumnId extends CColumn {

	public function __construct($col_name, $col_title) {
		parent::__construct();
		$this->colName = $col_name;
		$this->colTitle = $col_title;
		$this->colType = COL_ID;
	}

	function GetQuotedValue() {
		if ($this->colValue == "")
			return "NULL";
		else
			return $this->colValue;
	}

	function DrawEditField() {
		return true;
	}

	function update() {
		return false;
	}

	function insert(&$insert) {
		$insert["colName"][] = $this->colName;
		$insert["colValue"][] = "NULL";
		return true;
	}
}

class CColumnString extends CColumn {

	public function __construct($col_name, $col_title) {
		parent::__construct();
		$this->colName = $col_name;
		$this->colTitle = $col_title;
		$this->colType = COL_STRING;
	}

	function GetQuotedValue() {
		return get_magic_quotes_gpc() != 1 ? addslashes($this->colValue) : $this->colValue;
	}

	function DrawEditField() {
		$value = $this->dlist->colTextBox($this);
		$this->dlist->f[] = $this->dlist->addForm($this->colTitle, $value);
		return true;
	}

	function update() {
		return " {$this->colName} = '".$this->GetQuotedValue()."'";
	}

	function insert(&$insert) {
		$insert["colName"][] = $this->colName;
		$insert["colValue"][] = $this->GetQuotedValue();
		return true;
	}
}

class CColumnLocation extends CColumn {

	var $deep, $out, $extraName = NULL, $extraVal = NULL;
	var $country_id, $state_id, $loc_id, $neighbor_id;

	var $extra_col_name, $extra_update_name, $extra_from_val;

	public function __construct($col_name, $col_title, $deep) {
		parent::__construct();
		$this->colName = $col_name;
		$this->colTitle = $col_title;
		$this->deep = $deep;
		$this->colType = COL_LOCATION;
	}	

	function GetQuotedValue() {
		return get_magic_quotes_gpc() != 1 ? addslashes($this->colValue) : $this->colValue;
	}

	function askCountry() {
		$loc_porent = 0;
		$this->out .= "<span id=\"country_select\"></span>";
		$class = $this->colMandatory ? "&class=mandatory&" : "";
		$this->dlist->javascript .= "_ajax(\"get\", \"/_ajax/loc\",\"parent_name=0&select_name=Country&Country={$this->country_id}{$class}\",\"country_select\");";
	}

	function askState() {
		$this->out .= "<span id=\"state_select\"></span>";
		$class = $this->colMandatory ? "&class=mandatory&" : "";
		$this->dlist->javascript .= "_ajax(\"get\", \"/_ajax/loc\",\"parent_name={$this->country_id}&select_name=State&State={$this->state_id}{$class}\",\"state_select\");";
	}

	function askCity() {
		$this->out .= "<span id=\"city_select\"></span>";
		$class = $this->colMandatory ? "&class=mandatory&" : "";
		if( !$this->state_id ) return;
		if( $this->deep == 4 ) $class .= "&deep=4";
		$this->dlist->javascript .= "_ajax(\"get\", \"/_ajax/loc\",\"parent_name={$this->state_id}&select_name=City&City={$this->loc_id}{$class}\",\"city_select\");";
	}

	function askNeighbor() {
		$this->out .= "<span id=\"neighbor_select\"></span>";
		//$class = $this->colMandatory ? "&class=mandatory&" : "";
		if( !$this->loc_id ) return;
		$this->dlist->javascript .= "_ajax(\"get\", \"/_ajax/loc\",\"parent_name={$this->loc_id}&select_name=Neighbor&Neighbor={$this->neighbor_id}{$class}\",\"neighbor_select\");";
	}

	function DrawEditField() {
		global $mcache;

		if (!$this->country_id) {
			if (isset($this->dlist->row["country_id"]) && $this->dlist->row["country_id"]) {
				$this->country_id = $this->dlist->row["country_id"];
			} else {
				$this->country_id = isset($_GET["Country"]) ? $_GET["Country"] : 16046;
			}
		}
		
		if (!$this->state_id)
			$this->state_id = isset($this->dlist->row["state_id"]) ? $this->dlist->row["state_id"] : (isset($_REQUEST["State"]) ? $_REQUEST["State"] : 0);

		$this->loc_id = isset($this->dlist->row["loc_id"]) ? $this->dlist->row["loc_id"] : 0;
		$this->neighbor_id = isset($this->dlist->row["neighbor_id"]) ? $this->dlist->row["neighbor_id"] : 0;
		
		if ($this->deep >= 3) {
			$this->askCountry(); 
			$this->askState(); 
		} else if ($this->deep == 2) {
			if (!$this->state_id) {
				$row = $mcache->get("SQL:LOC-ID",array($this->loc_id));
				if ($row !== false) {
					$this->state_id = $row["loc_parent"];
				}
			}
			$this->askState();
		}
		$this->askCity();
		if ($this->deep == 4)
			$this->askNeighbor();

		$this->dlist->f[] = $this->dlist->addForm($this->colTitle, $this->out);
		
		return true;
	}

	function setLocName($col_name, $update_name, $from_val) {
		$this->extra_col_name[] = $col_name;
		$this->extra_update_name[] = $update_name;
		$this->extra_from_val[] = $from_val;
	}

	function getLocName($type, &$return) {
		if( !is_array($this->extra_col_name) ) return;

		for($i=0;$i<count($this->extra_col_name);$i++) {
			$val = GetPostParam($this->extra_from_val[$i]);
			if( $type == "update" ) {
				$return .= ", {$this->extra_col_name[$i]} = (select {$this->extra_update_name[$i]} from location_location where loc_id = '$val')";
			} else if( $type == "insert" ) {
				$return["colName"][] = $this->extra_col_name[$i];
				$return["colValue"][] = "(select {$this->extra_update_name[$i]} from location_location where loc_id = '$val')";
			}
		}
	}

	function setExtraSQL($name, $val) {
		$this->extraName = $name;
		$this->extraVal = $val;
	}

	function getExtraSQL($type, &$return) {
		if( !$this->extraName ) return;
		if( $type == "update" ) {
			$return .= ", `{$this->extraName}` = {$this->extraVal}";
		} else if( $type == "insert" ) {
			$return["colName"][] = $this->extraName;
			$return["colValue"][] = $this->extraVal;			
		}
		return true;
	}

	function update() {
//		$neighbor_id = GetPostParam("Neighbor");
		$loc_id = GetPostParam("City");
//		$state_id = GetPostParam("State");
//		$country_id = GetPostParam("Country");
////		if( $this->deep == 4 ) $return = "neighbor_id='$neighbor_id', loc_id='$loc_id', state_id='$state_id', country_id='$country_id'";
//		if( $this->deep == 3 ) $return = "loc_id='$loc_id', state_id='$state_id', country_id='$country_id'";
//
//		if ($this->deep == 2) {
//			//TODO adjust following after unification
//			if (in_array($this->country_id, array(16046, 16047, 41973)))
//				$return = "loc_id='$loc_id', state_id='$state_id'";
//			else
//				$return = "loc_id='$loc_id'";
//		}
//
//		if( $this->deep == 1 )
			$return = "loc_id='$loc_id'";
		$this->getLocName("update", $return);
		return $return;
	}

	function insert(&$insert) {
		global $ctx;

		//TODO adjust following after unification
//		if (in_array($this->country_id, array(16046, 16047, 41973))) {
//			$neighbor_id = GetPostParam("Neighbor");
//			$country_id = GetPostParam("Country");
//			$state_id = GetPostParam("State");
//			/*
//			if( $this->deep > 4 ) {
//				$insert["colName"][] = "neighbor_id";
//				$insert["colValue"][] = $neighbor_id;
//			}
//			*/
//			if( $this->deep >= 3 ) {
//				$insert["colName"][] = "country_id";
//				$insert["colValue"][] = $country_id;
//				// state_id already not exist in place table - get from parent location
//			} if ($this->deep >= 2) {
//				$insert["colName"][] = "state_id";
//				$insert["colValue"][] = $state_id;
//			}
//		}

		$loc_id = GetPostParam("City");
		$insert["colName"][] = "loc_id";
		$insert["colValue"][] = $loc_id;

		//TODO unified handler
		//if (is_null($ctx->country) || !dir::isCountryUnified())
		if (!$ctx->international)
			$this->getLocName("insert", $insert);

		return true;
	}
}

class CColumnLocationAjax extends CColumn {

	var $deep, $out, $extraName = NULL, $extraVal = NULL;
	var $country_id, $state_id, $loc_id, $neighbor_id;

	var $extra_type, $extra_from_name, $extra_from_id, $extra_column, $extra = array();
	var $refreshbyloc = false;

	public function __construct($col_name, $col_title, $deep) {
		parent::__construct();
		$this->colName = $col_name;
		$this->colTitle = $col_title;
		$this->deep = $deep;
		$this->colType = COL_LOCATIONAJAX;
	}	

	function GetQuotedValue() {
		return get_magic_quotes_gpc() != 1 ? addslashes($this->colValue) : $this->colValue;
	}


	function askLoc() {
		$class = $this->colMandatory ? "class='mandatory'" : "";
		$this->out .= "<input type='text' name='$this->colName' id='$this->colName' value='{$this->loc_id}' size='50' $class />";
		$extra = $this->loc_id ? ",prePopulate:[{id: {$this->loc_id}, name: \"{$this->loc_name}\"}]" : ($this->refreshbyloc ? ",onAdd: refreshLoc" : "");
		$this->dlist->javascript .= "$('#$this->colName').tokenInput('/_ajax/la', {tokenLimit: 1, minChars: 3, hintText: 'Example Search: Las Vegas, NV',
			noResultsText: 'Couldnt Find',searchingText: 'Searching...'$extra});";
	}

	function DrawEditField() {

		global $db;

		if( isset($this->dlist->row["loc_id"]) || $this->colValue ) {
			if( $this->colValue ) $loc_id = $this->colValue;
			else $loc_id = isset($this->dlist->row["neighbor_id"])&&$this->dlist->row["neighbor_id"]?$this->dlist->row["neighbor_id"]:$this->dlist->row["loc_id"];
			$res = $db->q("select concat(loc_name, ', ',s, if(loc_type=4,' (neighbor)','')) from location_location where loc_id = '{$loc_id}'"); 
			if( $db->numrows($res) ) {
				$row = $db->r($res);
				$this->loc_id = $loc_id;
				$this->loc_name = $row[0];
			}
		}

		$this->askLoc();
		$this->dlist->f[] = $this->dlist->addForm($this->colTitle, $this->out);
		return true;
	}

	function addLocPair($type, $from_name, $value, $param) {
		$par = NULL;
		foreach($param as $k=>$v) {
			$par[] = array($k=>$v);
		}

		$this->extra[] = array(
			"type"=>$type,
			"from_name"=>$from_name,
			"value"=>$value,
			"param"=>$par
		);
	}

	function setLocName($col_name, $update_name, $from_val) {
		$this->extra_col_name[] = $col_name;
		$this->extra_update_name[] = $update_name;
		$this->extra_from_val[] = $from_val;
	}

	function getLocName($type, &$return) {
		if( !is_array($this->extra) ) return;

		global $db;

		foreach($this->extra as $ex) {
			if( $ex["type"] == "param" ) {
				$val = GetPostParam($ex['value']);
				$res = $db->q("select * from location_location where {$ex['from_name']} = '$val' limit 1");
				if( !$db->numrows($res) ) {
					echo "<p>select * from location_location where {$ex['from_name']} = '$val' limit 1</p>";
					return false;
				}
				$row=$db->r($res);
				if( $type == "update" ) {
					foreach($ex['param'] as $param) {
						foreach($param as $par) {
							$return .= ", {$par['dst_name']} = '{$row[$par['src_name']]}'";
						}
					}
				} else if( $type == "insert" ) {
					foreach($ex['param'] as $param) {
						foreach($param as $par) {
							$return["colName"][] = $par['dst_name'];
							$return["colValue"][] = "{$row[$par['src_name']]}";
						}
					}
				}
			}
			if( $ex["type"] == "sql" ) {
				$val = GetPostParam($ex['value']);
				$row=$db->r($res);
				if( $type == "update" ) {
					foreach($ex['param'] as $param) {
						foreach($param as $par) {
							$return .= ", {$par['dst_name']} = (select {$par['src_name']} from location_location where {$ex['from_name']} = {$ex['value']})";
						}
					}
				} else if( $type == "insert" ) {
					foreach($ex['param'] as $param) {
						foreach($param as $par) {
							$return["colName"][] = $par['dst_name'];
							$return["colValue"][] = "(select {$par['src_name']} from location_location where {$ex['from_name']} = {$this->dlist->table}.{$ex['value']})";
						}
					}
				}
			}
		}
	}

	function setExtraSQL($name, $val) {
		$this->extraName = $name;
		$this->extraVal = $val;
	}

	function getExtraSQL($type, &$return) {
		if( !$this->extraName ) return;
		if( $type == "update" ) {
			$return .= ", `{$this->extraName}` = {$this->extraVal}";
		} else if( $type == "insert" ) {
			$return["colName"][] = $this->extraName;
			$return["colValue"][] = $this->extraVal;			
		}
		return true;
	}

	function update() {
		global $db;

		$val = GetPostParam($this->colName);
		$res = $db->q("select * from location_location where loc_id = '$val'");
		if( !$db->numrows($res) ) {
			echo "error: select * from location_location where loc_id = '$val'";
			die;
		}
		$row = $db->r($res);

		/*
		if( $row['loc_type'] == 4 && $this->deep == 4 ) {
			$res = $db->q("select l2.* from location_location l inner join location_location l2 on l.loc_parent = l2.loc_id where l.loc_id = '{$row['loc_parent']}'");
			if( !$db->numrows($res) ) {
				echo "select * from location_location where loc_id = '{$row['loc_parent']}'";
				die;
			}
			$row2 = $db->r($res);

			$return = "neighbor_id='{$row['loc_id']}', loc_id='{$row['loc_parent']}', state_id='{$row2['loc_id']}', country_id = '{$row2['loc_parent']}'";
		} else {
		*/
			$res = $db->q("select l2.* from location_location l inner join location_location l2 on l.loc_parent = l2.loc_id where l.loc_id = '{$row['loc_id']}'");
			if( !$db->numrows($res) ) {
				echo "erx2 select * from location_location where loc_id = '{$row['loc_parent']}'";
				die;
			}
			$row2 = $db->r($res);

			$return = "loc_id='{$row['loc_id']}', state_id='{$row['loc_parent']}', country_id='{$row2['loc_parent']}'";
		//	if( $this->deep == 4 ) $return .= ",neighbor_id=0";
		//}

		$this->getLocName("update", $return);
		return $return;
	}

	function insert(&$insert) {
		global $db;

		$val = GetPostParam($this->colName);
		$res = $db->q("select * from location_location where loc_id = '$val'");
		if( !$db->numrows($res) ) {
			echo "select * from location_location where loc_id = '$val'";
			die;
		}
		$row = $db->r($res);

		/*
		if( $row['loc_type'] == 4 && $this->deep == 4 ) {
			$res = $db->q("select l2.* from location_location l inner join location_location l2 on l.loc_parent = l2.loc_id where l.loc_id = '{$row['loc_parent']}'");
			if( !$db->numrows($res) ) {
				echo "select * from location_location where loc_id = '{$row['loc_parent']}'";
				die;
			}
			$row2 = $db->r($res);

			$insert["colName"][] = "neighbor_id";
			$insert["colValue"][] = "{$row['loc_id']}";
			$insert["colName"][] = "loc_id";
			$insert["colValue"][] = "{$row['loc_parent']}";
			$insert["colName"][] = "state_id";
			$insert["colValue"][] = "{$row2['loc_id']}";
			$insert["colName"][] = "country_id";
			$insert["colValue"][] = "{$row2['loc_parent']}";
		} else {
		*/
			$res = $db->q("select l2.* from location_location l inner join location_location l2 on l.loc_parent = l2.loc_id where l.loc_id = '{$row['loc_id']}'");
			if( !$db->numrows($res) ) {
				echo "select * from location_location where loc_id = '{$row['loc_parent']}'";
				die;
			}
			$row2 = $db->r($res);

			$insert["colName"][] = "loc_id";
			$insert["colValue"][] = "{$row['loc_id']}";
			$insert["colName"][] = "state_id";
			$insert["colValue"][] = "{$row['loc_parent']}";
			$insert["colName"][] = "country_id";
			$insert["colValue"][] = "{$row2['loc_parent']}";

		//	if( $this->deep == 4 ) {
		//		$insert["colName"][] = "neighbor_id";
		//		$insert["colValue"][] = "0";
		//	}
		//}
		$this->getLocName("insert", $insert);
		return true;
	}
}

class CColumnText extends CColumn {
	var $Cols = 75, $Rows = 8, $maxLength = false;

	public function __construct($col_name, $col_title = '') {
		parent::__construct();
		$this->colType		  = COL_TEXT;
		$this->SetColName($col_name);
		$this->colTitle		 = !empty($col_title) ? $col_title : $col_name;
		$this->colSelect		= true;
		$this->colInsert		= true;
		$this->colUpdate		= true;
		$this->colEditField	 = true;
		$this->colShowTable	 = false;
	}

	function setMaxLength($ml) {
		$this->maxLength = intval($ml);
	}

	function GetQuotedValue() {
		return get_magic_quotes_gpc() != 1 ? addslashes($this->colValue) : $this->colValue;
	}

	function DrawEditField() {
		if ($this->colReadOnly)
			$readonly = "readonly=\"readonly\"";

		$value = $this->dlist->GetTextArea($this);
		$this->dlist->addForm($this->colTitle, $value);
	}

	function update() {
		return " {$this->colName} = '".$this->GetQuotedValue()."'";
	}

	function insert(&$insert) {
		$insert["colName"][] = $this->colName;
		$insert["colValue"][] = $this->GetQuotedValue();
		return true;
	}
}

class CColumnNumber extends CColumn {
	var $doubleval = false;

	public function __construct($col_name, $col_title = '') {
		parent::__construct();
		$this->colType		  = COL_NUMBER;
		$this->SetColName($col_name);
		$this->colTitle		 = !empty($col_title) ? $col_title : $col_name;
		$this->colSelect		= true;
		$this->colInsert		= true;
		$this->colUpdate		= true;
		$this->colEditField	 = true;
		$this->colShowTable	 = true;
		$this->recheck		 = false;
	}

	function GetQuotedValue() {
		if ($this->colValue == "")
			return ($this->doubleval)?'NULL':0;
		else
			return ($this->doubleval)?doubleval($this->colValue):$this->colValue;
	}

	function DrawEditField() {
		if($this->doubleval) {
			if($this->dlist->action == "edit") {
				$value = $this->dlist->GetRecheckCoordBox($this);
			} else {
				$value = $this->dlist->getTextBox(doubleval($this->colName), "", 10);
			}
		} else {
			$value = $this->dlist->getTextBox($this->colName, "", 10);
		}
		$this->dlist->addForm($this->colTitle, $value);
	}

	function update() {
		global $db;

		if ($this->doubleval) {
			$val = null_or_doubleval($this->colName);

			$aux = "";
			if ($this->colName == "loc_lat" || $this->colName == "loc_long") {
				//get original value of column to see if it is different
				//if lat or long changes, we need to drop cached image of map
				$res = $db->q("SELECT $this->colName FROM {$this->dlist->table} WHERE place_id = {$this->dlist->itemID} LIMIT 1", []);
				$row = $db->r($res);
				$orig_val = $row[$this->colName];
				if ($val != $orig_val) {
					//_d("resetting map_image");
					$aux = " , map_image = NULL ";
				}
			}
			return " {$this->colName} = {$val}".$aux;
		}
		return " {$this->colName} = '".$this->GetQuotedValue()."'";
	}

	function insert(&$insert) {
		$insert["colName"][] = $this->colName;
		$insert["colValue"][] = $this->GetQuotedValue();
		return true;
	}
}


class CColumnBoolean extends CColumn {
	var $options = array(), $title;

	public function __construct($col_name, $col_title = '') {
		parent::__construct();
		$this->colType		  = COL_BOOL;
		$this->SetColName($col_name);
		$this->colTitle		 = !empty($col_title) ? $col_title : $col_name;
		$this->colSelect		= true;
		$this->colInsert		= true;
		$this->colUpdate		= true;
		$this->colEditField	 = true;
		$this->colShowTable	 = true;
		$this->colValue		= NULL;
	}

	function GetQuotedValue() {
		if ($this->colValue == "")
			return 0;
		else
			return $this->colValue;
	}

	function setColTitle($title) {
		$this->title = $title;
	}

	function AddOption($value, $label) {
		$this->options[] = array( "0"=>$value, "1" => $label);
	}

	function DrawEditField() {
		$title = $this->title ? $this->title : $this->colTitle;
		if( $this->colMandatory ) $title = "*".$title;
		$val = $this->dlist->getCheckbox($this->colName, $this->dlist->row[$this->colName], $this->colTitle);
		foreach($this->options as $option) {
			$val .= "<br>".$this->dlist->getCheckbox($option[0], $this->dlist->row[$option[0]], $option[1]);
		}
		$this->dlist->addForm($title, $val);
	}

	function update() {
		$return = " {$this->colName} = '".$this->GetQuotedValue()."'";
		foreach($this->options as $option) {
			$val = GetPostParam($option[0]);
			//$return .= ", `$option[0]` = '".htmlspecialchars($val, ENT_QUOTES)."'";
			$return .= ", `$option[0]` = ".intval($val);
		}
		return $return;
	}

	function insert(&$insert) {
		foreach($this->options as $option) {
			$val = GetPostParam($option[0]);
			$insert["colName"][] = $option[0];
			$insert["colValue"][] = intval($val);
		}

		$insert["colName"][] = $this->colName;
		$insert["colValue"][] = $this->GetQuotedValue();
		return true;
	}

}


class CColumnForeign extends CColumn {
	var $colForeignType;
	var $colForeignIdName;
	var $colForeignDataName;
	var $colForeignWhere;
	var $colForeignOrder = NULL;
	var $setExtraColumn = false;

	var $foreign_options;

	public function __construct($col_name, $col_foreign_table, $col_foreign_id_name, $col_foreign_data_name, $col_title = '') {
		parent::__construct();
		$this->colType		  = COL_FOREIG;
		$this->SetColName($col_name);
		$this->colForeignTable   = $col_foreign_table;
		$this->colForeignIdName = $col_foreign_id_name;
		$this->colForeignDataName= $col_foreign_data_name;
		$this->colForeignWhere  = '';
		$this->colTitle		 = !empty($col_title) ? $col_title : $col_name;
		$this->colSelect		= 1;
		$this->colInsert		= 1;
		$this->colUpdate		= 1;
		$this->colEditField	 = 1;
		$this->colShowTable	 = 0;

		$this->extraValues	  = array();
	}

	function SetExtraValues($array) {
		$this->extraValues = $array;
	}

	function setExtraColumn($col_name) {
		$this->setExtraColumn = $col_name;
	}

	function SetForeignWhere($where) {
		$this->colForeignWhere = $where;
	}

	function SetForeignOrder($order) {
		$this->colForeignOrder = $order;
	}

	function GetQuotedValue() {
		if ($this->colValue == "")
			return "NULL";
		else
			return $this->colValue;
	}

	function update() {
		$return = "`{$this->colName}` = '".$this->GetQuotedValue()."'";
		if( $this->setExtraColumn ) $return .= ", `{$this->setExtraColumn}` = (select {$this->colForeignDataName} from {$this->colForeignTable} where {$this->colForeignIdName} = '".$this->GetQuotedValue()."')";
		return $return;
	}

	function insert(&$insert) {
		$insert["colName"][] = $this->colName;
		$insert["colValue"][] = $this->GetQuotedValue();

		if( $this->setExtraColumn ) {
			$insert["colName"][] = $this->setExtraColumn;
			$insert["colValue"][] = "(select {$this->colForeignDataName} from {$this->colForeignTable} where {$this->colForeignIdName} = '".$this->GetQuotedValue()."')";
		}
		return true;
	}

	function DrawEditField() {
		$sql = "select {$this->colForeignIdName}, {$this->colForeignDataName} from {$this->colForeignTable}";
		if( !empty($this->colForeignOrder) ) $sql .= " order by {$this->colForeignOrder}";
		$res = $this->dlist->db->q($sql);
		while($row=$this->dlist->db->r($res)) {
			$this->foreign_options[] = array("0" => $row[0], "1" => $row[1]);			
		}
		$class = $this->colMandatory ? "mandatory":"";
		$this->dlist->addForm($this->colTitle, $this->dlist->getSelect($this->colName, $this->colValue, $this->foreign_options, true, $class));
	}
}

class CColumnDate extends CColumn {

	var $colFilterStart;			//filter phrase for starting date
	var $colFilterEnd;			  //filter phrase for ending date
	var $colFilterStartDefault;	 //default filter phrase for starting date
	var $colFilterEndDefault;	   //default filter phrase for ending date

	var $colDateFormat = null;
	var $initialValue = NULL;
	var $readonly;

	public function __construct($col_name, $col_title = '') {
		parent::__construct();
		$this->colType		  = COL_DATE;
		$this->SetColName($col_name);
		$this->colTitle		 = !empty($col_title) ? $col_title : $col_name;
		$this->colSelect		= true;
		$this->colInsert		= true;
		$this->colUpdate		= true;
		$this->colEditField	 = true;
		$this->colShowTable	 = true;
		$this->colFilterStart   = "";
		$this->colFilterEnd	 = "";
		$this->colFilterStartDefault	= "";
		$this->colFilterEndDefault	  = "";
	}

	function SetFormat($format) {
		$this->colDateFormat	= $format;
	}

	function SetInitial($val) {
		$this->initialValue	= $val;
	}

	function GetQuotedValue() {
		return get_magic_quotes_gpc() != 1 ? addslashes($this->colValue) : $this->colValue;
	}

	function DrawEditField() {
		if( $this->initialValue ) {
			$val = $this->colValue = $this->initialValue;
		} else
			$val = "";
				if($this->colReadOnly) $value = $val;

		if( $this->colHidden ) {
			$this->dlist->hidden($this->colName, $val);
			return;
		}
		else $value = $this->dlist->getTextBox($this->colName, $val, 10);

		$this->dlist->addForm($this->colTitle, $value);
	}

	function update() {
		return "";
	}

	function insert(&$insert) {
		$insert["colName"][] = $this->colName;
		$insert["colValue"][] = $this->GetQuotedValue();
		return false;
	}
}

class CColumnImage extends CColumn {

	var $path = NULL, $handle, $type, $attr;

	var $max = false, $set = false, $new_width, $new_height, $width, $height, $backup, $backup_module, $watermark = false;
	var $local = false, $local_path, $main_image_column = '', $path_main_image = '';

	public function __construct($col_name, $col_title) {
		parent::__construct();
		$this->colName = $col_name;
		$this->colTitle = $col_title;
		$this->colType = COL_IMAGE;
	}	

	function GetQuotedValue() {
		return get_magic_quotes_gpc() != 1 ? addslashes($this->colValue) : $this->colValue;
	}

	function setPath($path) {
		$this->path = $path;
		if( !is_dir(IMAGE_PATH.$this->path) ) mkdir(IMAGE_PATH.$this->path, 0755);
	}

	function setMaxDimensions($width = NULL, $height = NULL) {
		if( $width ) $this->new_width = $width;
		if( $height ) $this->new_height = $height;
		$this->max = true;
	}

	function setDimensions($width, $height) {
		$this->new_width = $width;
		$this->new_height = $height;
		$this->set = true;
	}

	function setBackup($val = '') {
		$this->backup = true;
		$this->backup_module = $val;
	}

	function setMainImage($image = '') {
		$this->main_image_column = $image;
	}

	function setPathMainImage($path) {
		$this->path_main_image = $path;
		if( !is_dir(IMAGE_PATH.$this->path) ) mkdir(IMAGE_PATH.$this->path, 0755);
	}

	function setWatermark() {
		$this->watermark = true;
	}

	function setLocal($path) {
		$this->local = true;
		$this->local_path = $path;
	}

	function uploadImage() {
		if (is_null($this->path)) {
			die("CColumnImage path is not defined!");
		}

		$this->handle = new Upload($_FILES[$this->colName]);
		$id           = $this->dlist->itemID ? $this->dlist->itemID : "";
		if (!$this->handle->uploaded) {
			// creating thumbnail from main_image_column file
			if (!empty($this->main_image_column) && empty($this->dlist->row[$this->colName])) {
				if (is_file($_FILES[$this->main_image_column]['tmp_name'])) {
					$tmpfname = tempnam("/tmp", "thumbnail");
					copy($_FILES[$this->main_image_column]['tmp_name'], $tmpfname);
				} else {
					$image 		   = $this->dlist->db->single($q = "SELECT {$this->main_image_column} as image FROM {$this->dlist->table} WHERE {$this->dlist->columns[0]->colName} = ? ",[$id]);
					$image = IMAGE_PATH.'/'.$this->path_main_image.'/'.$image;
					if (is_file($image)) {
						$tmpfname = tempnam("/tmp", "thumbnail");
						copy($image, $tmpfname);
					} else {

						return null;
					}
				}
				$this->handle = new Upload($tmpfname);
			} else {

				return null;
			}
		}
		if ($this->handle->uploaded) {
			if ($this->dlist->action == "edit" && isset($this->dlist->row[$this->colName])
				&& !empty($this->dlist->row[$this->colName])
			) {
				unlink(IMAGE_PATH.$this->path."/".$this->dlist->row[$this->colName]);
				$this->dlist->db->q("update {$this->dlist->table} set `{$this->colName}` = '' where {$this->dlist->columns[0]->colName} = '{$this->itemID}'");
			}
			if ($this->backup == true) {
				$this->handle->Process(IMAGE_BACKUP);
				if ($this->handle->processed) {
					$text = "";
					foreach ($_POST as $key => $value) {
						$text .= "$key: $value";
					}
					$this->dlist->db->q("
INSERT INTO image_backup (`id`, `module`, `filename`, `time`, `post`) VALUES (?, ?, ?, now(), ?)
",
						[$id, $this->backup_module, $this->handle->file_dst_name, $text]);
				}
			}

			$this->handle->image_convert = "jpg";
			list($this->width, $this->height, $this->type, $this->attr)
				= getimagesize($this->handle->file_src_pathname);
			if ($this->max && ($this->width > $this->new_width || $this->height > $this->new_height)) {
				$this->handle->image_resize = true;
				if ($this->width > $this->new_width) {
					$this->handle->image_x       = $this->new_width;
					$this->handle->image_ratio_y = true;
				} else {
					$this->handle->image_y       = $this->new_height;
					$this->handle->image_ratio_x = true;
				}
			} else {
				if ($this->set && $this->new_width && $this->new_height) {
					$this->handle->image_resize     = true;
					$this->handle->image_ratio_crop = true;
					$this->handle->image_x          = $this->new_width;
					$this->handle->image_y          = $this->new_height;
				}
			}
			$this->handle->file_auto_rename   = false;
			$this->handle->file_overwrite     = true;
			$this->handle->file_new_name_body = $this->dlist->makeText()."_".$id;
			if ($this->watermark) {
				$this->handle->image_watermark             = _CMS_ABS_PATH.'/images/watermark.png';
				$this->handle->image_watermark_x           = 0;
				$this->handle->image_watermark_y           = 0;
				$this->handle->image_watermark_no_zoom_in  = true;
				$this->handle->image_watermark_no_zoom_out = true;
			}
			$this->handle->Process(IMAGE_PATH.$this->path);
			if ($this->handle->processed) {
				$this->handle->Clean();
			}
			$this->colValue = $this->handle->file_dst_name;

			return $this->colValue;
		}

		return null;
	}


	/**
	 * Returns an array of latitude and longitude from the Image file
	 *
	 * @return array|bool
	 */
	function readGeoLocation() {
		$this->handle = new Upload($_FILES[$this->colName]);
		if ($this->handle->uploaded) {
			return read_geo($this->handle->file_src_pathname);
		}

		return false;
	}

	function remove() {
		if( $this->dlist->action == "edit" && !empty($this->dlist->row[$this->colName]) ) {
			unlink(IMAGE_PATH.$this->path."/".$this->dlist->row[$this->colName]);
			if ($this->colName != "City" && !dir::isBaseColumn($this->colName)) {
				dir::storeAttributeByName($this->dlist->itemID, $this->colName, null);
			} else {
				$this->dlist->db->q("update {$this->dlist->table} set `{$this->colName}` = '' where {$this->dlist->columns[0]->colName} = '{$this->dlist->itemID}'");
			}
			$this->dlist->row[$this->colName] = null;
			echo "<p>{$this->colTitle} is removed.</p>";
			return true;
		}
		return false;
	}

	function _AfterDelete() {
		if( !empty($this->dlist->row[$this->colName]) ) {
			unlink(IMAGE_PATH.$this->path."/".$this->dlist->row[$this->colName]);
		}
		return true;
	}

	function AfterSave() {
		$geo_coords = read_geo($_FILES[$this->colName]['tmp_name']);
		$val        = $this->uploadImage();
		if ($val) {
			if (!dir::isBaseColumn($this->colName)) {
				dir::storeAttributeByName($this->dlist->itemID, $this->colName, $val);
			} else {
				$string_coords = '';
				$res
							   = $this->dlist->db->q("SELECT loc_lat, loc_long FROM {$this->dlist->table} WHERE {$this->dlist->columns[0]->colName} = '{$this->dlist->itemID}'");
				$row           = $this->dlist->db->r($res, MYSQL_ASSOC);
				if ((empty($row['loc_lat']) && empty($row['loc_long']))
					|| ($row['loc_lat'] == 0 && $row['loc_long'] == 0)
				) {
					if (!empty($geo_coords)) {
						$string_coords = ", loc_lat='{$geo_coords['lat']}', loc_long='{$geo_coords['lng']}'";
					}
				}
				$this->dlist->db->q("update {$this->dlist->table} set {$this->colName} = '$val' {$string_coords} where {$this->dlist->columns[0]->colName} = '{$this->dlist->itemID}'");
			}
		}

		return true;
	}

	function DrawEditField() {
		$value = $this->dlist->getFile($this->colName);

		if( $this->dlist->action == "edit" ) {
			if( isset($this->dlist->row[$this->colName]) && !empty($this->dlist->row[$this->colName]) ) {
				$value .= "<br/><img src='".IMAGE_URL.IMAGE_URL_PATH.$this->path."/".$this->dlist->row[$this->colName]."' alt='' /><a href='?id={$this->dlist->itemID}&removeimage={$this->colName}'>Remove this image</a>\n";
			}
		}

		if( $this->local ) {
			if ($handle = opendir(_CMS_ABS_PATH.$this->local_path)) {

				$value .= "<p>";
				while (false !== ($file = readdir($handle))) {
					if( strstr($file, ".jpg") || strstr($file, ".jpeg") ) {
						$value .= "<a href='{$this->local_path}{$file}' target='_blank'>{$file}</a><br/>";
					}
				}
				$value .= "</p>";

			closedir($handle);
			}
		}

		$this->dlist->addForm($this->colTitle, $value);
		return true;
	}

	function update() {
		return "";
	}

	function insert(&$insert) {
		return false;
	}
}

class CColumnEnum extends CColumn {

	var $enum_options, $class, $show_null = true, $default_value;

	public function __construct($col_name, $col_title = '') {
		parent::__construct();
		$this->colType = COL_ENUM;
		$this->SetColName($col_name);
		$this->colTitle = $col_title;
	}

	function GetQuotedValue() {
		//return get_magic_quotes_gpc() != 1 ? addslashes($this->colValue) : $this->colValue;
		if ($this->colValue == "")
			return "NULL";
		else
			return addslashes($this->colValue);
	}

	function AddEnumOption($value, $label) {
		$this->enum_options[] = array("0" => $value, "1" => $label);
	}

	function setDefaultValue($val) {
		$this->default_value = $val;
	}

	function DrawEditField() {
		$title = $this->colTitle;
		$this->class = $this->colMandatory ? "mandatory":"";

		$this->dlist->f[] = $this->dlist->addForm($title, $this->dlist->getSelectBox($this));
		return true;
	}

	function update() {
		$val = $this->GetQuotedValue();
		if ($val == "NULL")
			return " {$this->colName} = NULL";
		else
			return " {$this->colName} = '{$val}'";
	}

	function insert(&$insert) {
		$insert["colName"][] = $this->colName;
		$insert["colValue"][] = $this->GetQuotedValue();
		return;
	}

}

/**
 * Class CColumnBoolean3 represents 3-way boolean selector (N/A,No,Yes) drawn by selectbox
 * @author jay
 */
class CColumnBoolean3 extends CColumnEnum {

	public function __construct($col_name, $col_title = '') {
		parent::__construct($col_name, $col_title);
		$this->colType = COL_BOOL_3;
		$this->SetColName($col_name);
		$this->colTitle = $col_title;
		$this->enum_options[] = array("0" => "1", "1" => "No");
		$this->enum_options[] = array("0" => "2", "1" => "Yes");
	}
	
	function AddEnumOption($value, $label) {return;}
}

class CColumnMultipleForeign extends CColumnEnum {
	public function __construct($col_name, $col_title = '') {
		parent::__construct($col_name, $col_title);
		$this->colType = COL_MUL_FOREIGN;
		$this->SetColName($col_name);
		$this->colTitle = $col_title;
	}

	function GetQuotedValue() {
		$this->colValue = GetPostParam($this->colName);
		if(is_array($this->colValue)) {
			$this->colValue = implode(',', $this->colValue);
		}
		if ($this->colValue == "")
			return "NULL";
		else
			return addslashes($this->colValue);
	}

	function DrawEditField() {
		$title = $this->colTitle;
		$this->class = $this->colMandatory ? "mandatory":"";

		$this->dlist->f[] = $this->dlist->addForm($title, $this->dlist->GetMultiSelectBox($this));

		return true;
	}
}

class CColumnMultipleForeignOld extends CColumn {
	var $link_table_name;
	var $link_table_id_col_name;
	var $link_table_entry_id_col_name;
	var $link_table_foreign_id_col_name;

	var $foreign_table_name;
	var $foreign_table_id_col_name;
	var $foreign_table_name_col_name;

	public function __construct($link_table_name, $link_table_id_col_name, $link_table_entry_id_col_name, $link_table_foreign_id_col_name,
		$foreign_table_name, $foreign_table_id_col_name, $foreign_table_name_col_name, $col_title = '', $col_name = "") {

		parent::__construct();
		$this->colType		  = COL_MUL_FOREIGN;
		$this->link_table_name = $link_table_name;
		$this->link_table_id_col_name = $link_table_id_col_name;
		$this->link_table_entry_id_col_name = $link_table_entry_id_col_name;
		$this->link_table_foreign_id_col_name = $link_table_foreign_id_col_name;
		$this->foreign_table_name = $foreign_table_name;
		$this->foreign_table_id_col_name = $foreign_table_id_col_name;
		$this->foreign_table_name_col_name = $foreign_table_name_col_name;

		$this->colTitle		 = $col_title;
		$this->colSelect		= 0;
		$this->colInsert		= 0;
		$this->colUpdate		= 0;
		$this->colEditField	 = 1;
		$this->colShowTable	 = 0;

		if ($col_name)
			$this->SetColName($col_name);			
	}

	function DrawEditField() {
		global $ctx;

		if ($this->colReadOnly)
			$readonly = "disabled=\"disabled\"";

		$return = "";
		$ids_value = "";
		$names_value = "";

		//get assigned foreign values
		$return .= "<ul id=\"mulcol_list_{$this->colIndex}\">";
		if ($this->dlist->action == "edit" && $this->dlist->itemID) {
			if (in_array($ctx->country_id, array(16046, 16047, 41973)))
				$module = " IS NULL";
			else
				$module = " = '$ctx->country'";
			$sql = "select F.{$this->foreign_table_id_col_name}, F.{$this->foreign_table_name_col_name}
					from {$this->foreign_table_name} F, {$this->link_table_name} L
					where F.{$this->foreign_table_id_col_name} = L.{$this->link_table_foreign_id_col_name} 
					and L.{$this->link_table_entry_id_col_name} = {$this->dlist->itemID}
					and L.module {$module}";
			$SQLresult = $this->dlist->db->q($sql);
			while($SQLrow=$this->dlist->db->r($SQLresult)) {
				$return .= "<li>".htmlspecialchars($SQLrow[1]);
				if (!$this->colReadOnly)
				   $return .= "&nbsp;<a href=\"#\" onclick=\"MulForeign_DelItem('".$SQLrow[0]."', 'mulcol_list_{$this->colIndex}','_mulcol_{$this->colIndex}_ids', '_mulcol_{$this->colIndex}_names'); return false;\">delete</a>";
				$return .= "</li>\n";
				if ($ids_value == "") {
					$ids_value = $SQLrow[0];
					$names_value = htmlspecialchars($SQLrow[1]);
				} else {
					$ids_value .= "|".$SQLrow[0];
					$names_value .= "|".htmlspecialchars($SQLrow[1]);
				}
			}
		}
		$return .= "</ul>\n";

		$return .= "<input type=\"hidden\" name=\"_mulcol_{$this->colIndex}_ids\" value=\"{$ids_value}\" />\n";
		$return .= "<input type=\"hidden\" name=\"_mulcol_{$this->colIndex}_names\" value=\"{$names_value}\" />\n";

		//show dropdown for add new
		$sql = "select distinct F.{$this->foreign_table_id_col_name}, F.{$this->foreign_table_name_col_name}
				from {$this->foreign_table_name} F";
		$SQLresult = $this->dlist->db->q($sql);
		$return .= "<select id=\"mulforeign_select{$this->colIndex}\" name=\"".$this->colName."\" {$readonly} >";
		$return .= "<option value=\"\">- SELECT -</option>\n";
		while($SQLrow=$this->dlist->db->r($SQLresult)) {
			$return .= "<option value=\"".htmlspecialchars($SQLrow[0])."\">".htmlspecialchars($SQLrow[1])."</option>\n";
		}
		$return .= "</select>\n";
		$return .= "<input type=\"button\" value=\"Add\" name=\"add\" onclick=\"MulForeign_AddItem('mulforeign_select{$this->colIndex}', 'mulcol_list_{$this->colIndex}','_mulcol_{$this->colIndex}_ids', '_mulcol_{$this->colIndex}_names'); return false;\"/><br />\n";

		$this->dlist->addForm($this->colTitle, $return);
	}

	function _AfterSave() {
		global $ctx;

		if (in_array($ctx->country_id, array(16046, 16047, 41973))) {
			$module = "NULL";
			$module_cond = " IS NULL";
		} else {
			$module = "'$ctx->country'";
			$module_cond = " = '$ctx->country'";
		}

		$ids = explode("|", GetRequestParamStripped("_mulcol_{$this->colIndex}_ids"));
		$this->dlist->db->q("delete from {$this->link_table_name} 
					where {$this->link_table_entry_id_col_name} = '{$this->dlist->itemID}'
					and module {$module_cond}");
				if (empty($ids[0]))
						return true;
				foreach ($ids as $id) {
						$sql = "insert into {$this->link_table_name}
								({$this->link_table_id_col_name}, {$this->link_table_entry_id_col_name}, {$this->link_table_foreign_id_col_name}, module)
								VALUES
				(0, {$this->dlist->itemID}, {$id}, $module)";
						$this->dlist->db->q($sql);
				}
				return true;
		}

		function _BeforeDelete() {
		global $ctx;

		if (in_array($ctx->country_id, array(16046, 16047, 41973)))
			$module = "IS NULL";
		else
			$module = "= '$ctx->country'";
		$this->dlist->db->q("delete from {$this->link_table_name} 
					where {$this->link_table_entry_id_col_name} = {$this->dlist->itemID}
					and module {$module}");
				return true;
		}

	function update() {
		return "";
	}

	function insert(&$insert) {
		return;
	}

}

class CColumnPhone extends CColumn {

	var $unique = false, $skip_unique = false;

	public function __construct($col_name, $col_title = '') {
		parent::__construct();
		$this->colType = COL_PHONE;
		$this->SetColName($col_name);
		$this->colTitle = $col_title;
		$this->unique = false;
		$this->colMandatory = false;
	}


	function GetQuotedValue() {
		$value = makeProperPhoneNumber($this->colValue, true);
		return get_magic_quotes_gpc() != 1 ? addslashes($value) : $value;
	}

	function DrawEditField() {
		$this->dlist->f[] = $this->dlist->addForm($this->colTitle, $this->dlist->GetPhoneBox($this));
		return true;
	}

	function setUnique() {
		$this->unique = true;
	}

	function update() {
		return " {$this->colName} = '".$this->GetQuotedValue()."'";
	}

	function insert(&$insert) {
		$insert["colName"][] = $this->colName;
		$insert["colValue"][] = $this->GetQuotedValue();
		return true;
	}

}

/**
 * New advanced version of CColumnPhone, that allows unique check on international places
 */
class CColumnPhone2 extends CColumn {

	var $unique = false, $skip_unique = false;

	public function __construct($col_name, $col_title = '') {
		parent::__construct();
		$this->colType = COL_PHONE_2;
		$this->SetColName($col_name);
		$this->colTitle = $col_title;
		$this->unique = false;
		$this->colMandatory = false;
	}


	function GetQuotedValue() {
		$value = makeProperPhoneNumber($this->colValue, true);
		return get_magic_quotes_gpc() != 1 ? addslashes($value) : $value;
	}

	function DrawEditField() {
		$this->dlist->f[] = $this->dlist->addForm($this->colTitle, $this->dlist->GetPhoneBox($this));
		return true;
	}

	function setUnique() {
		$this->unique = true;
	}

	function update() {
		return " {$this->colName} = '".$this->GetQuotedValue()."'";
	}

	function insert(&$insert) {
		$insert["colName"][] = $this->colName;
		$insert["colValue"][] = $this->GetQuotedValue();
		return true;
	}

}

class CColumnFee extends CColumn {
	var $free_included = true;
	var $money_sign = "$";
	var $money_format = "{%money_sign}{%money}.00";
	var $start = 5, $end = 505, $interval = 5;
	var $country_id = 0;

	public function __construct($col_name, $col_title = '') {
		parent::__construct();
		$this->colType = COL_FEE;
		$this->SetColName($col_name);
		$this->colTitle = $col_title;
	}
	
	function SetLocales() {
		global $ctx;

		if ($ctx->country_id != NULL) {
			$country_id = $ctx->country_id;
		} else if ($this->country_id != 0) {		//TODO transition, will be removed in future
			$country_id = $this->country_id;
		} else {
			return;
		}

		if ($country_id == 41973 ) {
			$this->money_sign = "GBP";
			$this->money_format = "{%money}.00 {%money_sign}";
		} elseif ($country_id == 41966 ) {
			$this->money_sign = "TL";
			$this->money_format = "{%money} {%money_sign}";
		} elseif (in_array($country_id, array(41764, 41823, 41826, 41897, 41954)) ) {
			$this->money_sign = "EUR";
			$this->money_format = "{%money} {%money_sign}";
			$this->start = 5;
			$this->end = 3000;
			$this->interval = 5;
		} elseif ($country_id == 41972 ) {
			$this->money_sign = "AED";
			$this->money_format = "{%money} {%money_sign}";
		} elseif ($country_id == 41954 ) {
			$this->money_sign = "CHF";
			$this->money_format = "{%money} {%money_sign}";
		} elseif ($country_id == 41799 ) {
			$this->money_sign = "CZK";
			$this->money_format = "{%money} {%money_sign}";
			$this->start = 100;
			$this->end = 10000;
			$this->interval = 50;
		} elseif ($country_id == 41959 ) {
			$this->money_sign = "Baht";
			$this->money_format = "{%money} {%money_sign}";
			$this->start = 50;
			$this->end = 20000;
			$this->interval = 50;
		} elseif ($country_id == 41916 ) {
			$this->money_sign = "Pesos";
			$this->money_format = "{%money} {%money_sign}";
			$this->start = 50;
			$this->end = 6000;
			$this->interval = 10;
		} elseif ($country_id == 41875 ) {
			$this->money_sign = "Ringgit";
			$this->money_format = "{%money} {%money_sign}";
			$this->start = 50;
			$this->end = 6000;
			$this->interval = 10;
		} elseif ($country_id == 41939 ) {
			$this->money_sign = "SGD";
			$this->money_format = "{%money} {%money_sign}";
			$this->start = 10;
			$this->end = 1000;
			$this->interval = 10;
		} elseif ($country_id == 41841) {
            //hungary
            $this->money_sign = "HUF";
            $this->money_format = "{%money} {%money_sign}";
            $this->start = 2500;
            $this->end = 150000;
            $this->interval = 2500;
		}
	}

	function GetQuotedValue() {
		return get_magic_quotes_gpc() != 1 ? addslashes($this->colValue) : $this->colValue;
	}

	function DrawEditField() {
		$value = $this->SetSelectMoneyOption();
		$this->dlist->f[] = $this->dlist->addForm($this->colTitle, $this->dlist->getSelect($this->colName, $this->colValue, $value, true));
		return true;
	}

	function update() {
		if (is_numeric($this->colValue))
			$val = intval($this->colValue);
		else
			$val = "NULL";
		return " {$this->colName} = {$val}";
	}

	function insert(&$insert) {
		$insert["colName"][] = $this->colName;
		if (is_numeric($this->colValue))
			$val = intval($this->colValue);
		else
			$val = "NULL";
		$insert["colValue"][] = $val;
	}

	function SetSelectMoneyOption() {

		if( !$this->country_id ) {
			foreach($this->dlist->columns as $col) {
				if( $col->colType == COL_LOCATIONAJAX ) {
					$this->country_id = $this->dlist->country_id ? $this->dlist->country_id : ($col->dlist->row['country_id'] ? $col->dlist->row['country_id'] : 0);
					break;
				}
			}
		}

		$this->SetLocales();	//TODO should be moved to constructor

		if($this->free_included) $output[] = array("0"=>-1, "1"=>"Free");
		for($i = $this->start; $i < $this->end; $i += $this->interval) {
			$ix = number_format($i);
			$value = str_replace("{%money_sign}", $this->money_sign, $this->money_format);
			$value = str_replace("{%money}", $ix, $value);
			$output[] = array("0"=>$i, "1"=>$value);
		}
		return $output;
	}
}

class CColumnFeeRange extends CColumn {
	var $fee_from = NULL;
	var $fee_to = NULL;

	public function __construct($col_name, $col_title = '') {
		parent::__construct();
		$this->colType = COL_FEE_RANGE;
		$this->SetColName($col_name);
		$this->colTitle = $col_title;
		$this->fee_from = new CColumnFee($col_name."_from");
		$this->fee_to = new CColumnFee($col_name."_to");
	}

	function GetQuotedValue() {
		return get_magic_quotes_gpc() != 1 ? addslashes($this->colValue) : $this->colValue;
	}

	function DrawEditField() {
		$parts = explode('|', $this->colValue);
		$values = $this->fee_from->SetSelectMoneyOption();
		$this->dlist->f[] = $this->dlist->addForm(
			$this->colTitle, 
			$this->dlist->getSelect($this->fee_from->colName, $parts[0], $values, true)." goes to ".$this->dlist->getSelect($this->fee_to->colName, $parts[1], $values, true)
			);
		return true;
	}

	function update() {
		return " {$this->colName} = '".$this->GetQuotedValue()."'";
	}

	function insert(&$insert) {
		$insert["colName"][] = $this->colName;
		$insert["colValue"][] = $this->GetQuotedValue();
	}
}

class CColumnHour extends CColumn {
	var $oh = array(), $opens = array(), $closes = array(), $closed = array(), $man;

	public function __construct($col_name, $col_title = '') {
		parent::__construct();
		$this->colType = COL_HOUR;
		$this->SetColName($col_name);
		$this->colTitle = $col_title;
	}

	function GetQuotedValue() {
		return get_magic_quotes_gpc() != 1 ? addslashes($this->colValue) : $this->colValue;
	}

	function setMandatory() {
		$this->man = true;
	}

	function DrawEditField() {
		if( $this->dlist->action == "edit") {
			if($this->dlist->row["monday_from"]||$this->dlist->row["monday_to"]) $oh[1] = array("open"=>$this->dlist->row["monday_from"],"close"=>$this->dlist->row["monday_to"]);
			if($this->dlist->row["tuesday_from"]||$this->dlist->row["tuesday_to"]) $oh[2] = array("open"=>$this->dlist->row["tuesday_from"],"close"=>$this->dlist->row["tuesday_to"]);
			if($this->dlist->row["wednesday_from"]||$this->dlist->row["wednesday_to"])$oh[3] = array("open"=>$this->dlist->row["wednesday_from"],"close"=>$this->dlist->row["wednesday_to"]);	
			if($this->dlist->row["thursday_from"]||$this->dlist->row["thursday_to"])$oh[4] = array("open"=>$this->dlist->row["thursday_from"],"close"=>$this->dlist->row["thursday_to"]);
			if($this->dlist->row["friday_from"]||$this->dlist->row["friday_to"])$oh[5] = array("open"=>$this->dlist->row["friday_from"],"close"=>$this->dlist->row["friday_to"]);
			if($this->dlist->row["saturday_from"]||$this->dlist->row["saturday_to"])$oh[6] = array("open"=>$this->dlist->row["saturday_from"],"close"=>$this->dlist->row["saturday_to"]);
			if($this->dlist->row["sunday_from"]||$this->dlist->row["sunday_to"])$oh[7] = array("open"=>$this->dlist->row["sunday_from"],"close"=>$this->dlist->row["sunday_to"]);
		}
		$this->hours($oh, $this->opens, $this->closes, $this->closed);
		$this->dlist->addForm($this->colTitle, "", "worker/worker_singleoh.tpl");
		return true;
	}

	function update() {
		$always = intval(GetPostParam("always"));
		$day1o = null_or_intval("day1o");
	   	$day1c = null_or_intval("day1c");
		$day2o = null_or_intval("day2o");
		$day2c = null_or_intval("day2c");
		$day3o = null_or_intval("day3o");
		$day3c = null_or_intval("day3c");
		$day4o = null_or_intval("day4o");
		$day4c = null_or_intval("day4c");
		$day5o = null_or_intval("day5o");
		$day5c = null_or_intval("day5c");
		$day6o = null_or_intval("day6o");
		$day6c = null_or_intval("day6c");
		$day7o = null_or_intval("day7o");
		$day7c = null_or_intval("day7c");

		if( isset($_POST["day1closed"]) ) $day1o = $day1c = "-1";
		if( isset($_POST["day2closed"]) ) $day2o = $day2c = "-1";
		if( isset($_POST["day3closed"]) ) $day3o = $day3c = "-1";
		if( isset($_POST["day4closed"]) ) $day4o = $day4c = "-1";
		if( isset($_POST["day5closed"]) ) $day5o = $day5c = "-1";
		if( isset($_POST["day6closed"]) ) $day6o = $day6c = "-1";
		if( isset($_POST["day7closed"]) ) $day7o = $day7c = "-1";

		return "always = {$always}, monday_from = {$day1o}, monday_to = {$day1c}, tuesday_from = {$day2o}, tuesday_to = {$day2c}, wednesday_from = {$day3o}, wednesday_to = {$day3c}, thursday_from = {$day4o}, thursday_to = {$day4c}, friday_from = {$day5o}, friday_to = {$day5c}, saturday_from = {$day6o}, saturday_to = {$day6c}, sunday_from = {$day7o}, sunday_to = {$day7c} ";
	}

	function insert(&$insert) {
		$always = intval(GetPostParam("always"));
		$day1o = null_or_intval("day1o");
	   	$day1c = null_or_intval("day1c");
		$day2o = null_or_intval("day2o");
		$day2c = null_or_intval("day2c");
		$day3o = null_or_intval("day3o");
		$day3c = null_or_intval("day3c");
		$day4o = null_or_intval("day4o");
		$day4c = null_or_intval("day4c");
		$day5o = null_or_intval("day5o");
		$day5c = null_or_intval("day5c");
		$day6o = null_or_intval("day6o");
		$day6c = null_or_intval("day6c");
		$day7o = null_or_intval("day7o");
		$day7c = null_or_intval("day7c");

		if( isset($_POST["day1closed"]) ) $day1o = $day1c = "-1";
		if( isset($_POST["day2closed"]) ) $day2o = $day2c = "-1";
		if( isset($_POST["day3closed"]) ) $day3o = $day3c = "-1";
		if( isset($_POST["day4closed"]) ) $day4o = $day4c = "-1";
		if( isset($_POST["day5closed"]) ) $day5o = $day5c = "-1";
		if( isset($_POST["day6closed"]) ) $day6o = $day6c = "-1";
		if( isset($_POST["day7closed"]) ) $day7o = $day7c = "-1";

		$insert["colName"][] = "always";
		$insert["colValue"][] = $always;

		$insert["colName"][] = "monday_from"; $insert["colValue"][] = $day1o;
		$insert["colName"][] = "monday_to"; $insert["colValue"][] = $day1c;

		$insert["colName"][] = "tuesday_from"; $insert["colValue"][] = $day2o;
		$insert["colName"][] = "tuesday_to"; $insert["colValue"][] = $day2c;

		$insert["colName"][] = "wednesday_from"; $insert["colValue"][] = $day3o;
		$insert["colName"][] = "wednesday_to"; $insert["colValue"][] = $day3c;

		$insert["colName"][] = "thursday_from"; $insert["colValue"][] = $day4o;
		$insert["colName"][] = "thursday_to"; $insert["colValue"][] = $day4c;

		$insert["colName"][] = "friday_from"; $insert["colValue"][] = $day5o;
		$insert["colName"][] = "friday_to"; $insert["colValue"][] = $day5c;

		$insert["colName"][] = "saturday_from"; $insert["colValue"][] = $day6o;
		$insert["colName"][] = "saturday_to"; $insert["colValue"][] = $day6c;

		$insert["colName"][] = "sunday_from"; $insert["colValue"][] = $day7o;
		$insert["colName"][] = "sunday_to"; $insert["colValue"][] = $day7c;

		return;
	}

	function get_label($minutes) {
		global $ctx;

		$hr = floor($minutes/60);
		$mi = str_pad($minutes - ($hr*60), 2, "0", STR_PAD_LEFT);
		$ampm = ($hr >= 12 && $hr < 24) ? "PM" : "AM";
		$hr_us = ($hr > 12) ? $hr - 12 : $hr;
		if ($hr_us == 0)
			$hr_us = 12;
		$hr_us = str_pad($hr_us, 2, "0", STR_PAD_LEFT);
		$hr_int = str_pad($hr, 2, "0", STR_PAD_LEFT);

		$label = "{$hr_us}:{$mi} {$ampm}";
//		if ($ctx->international)
//			$label .= " - {$hr_int}:{$mi}";

		return $label;
	}

	function hours($oh = array(), &$opens, &$closes, &$closed) {
		global $ctx;

		$opens = array();
		$closes = array();

		if ($this->dlist->row["always"] == 1)
			$this->dlist->smarty->assign("always", true);

		for($day=0;$day<7;$day++) {

			$open = "<option value=\"\">-- don't know --</option>";
			$close = "<option value=\"\">-- don't know --</option>";

			if( ($oh[$day+1]["open"] == -1 && $oh[$day+1]["close"] == -1) || isset($_POST["day".($day+1)."closed"]) ) { 
				$closed[$day] = true; 
				$_POST["day".($day+1)."o"] = NULL; 
				$_POST["day".($day+1)."c"] = NULL;
			}

			$post_name_o = "day".($day+1)."o";
			$post_name_c = "day".($day+1)."c";

			for($minutes = 0; $minutes < 1440; $minutes += 30) {

				$selected = "";
				if (
					(isset($_POST[$post_name_o]) && $_POST[$post_name_o] == $minutes && $_POST[$post_name_c])
					|| (!isset($_POST[$post_name_o]) && isset($oh[$day+1]["open"]) && $oh[$day+1]["open"] == $minutes)
					) {
					$selected = "selected=\"selected\"";
				}

				$label = $this->get_label($minutes);

				$open .= "<option value=\"$minutes\" {$selected}>{$label}</option>";
			}
			
			$opens[$day] = $open;

			for($i = 30; $i <= 1440; $i += 30) {

				$minutes = $i;
				if ($minutes < 720)
					$minutes += 1440;

				$selected = "";
				if (
					(isset($_POST[$post_name_c]) && $_POST[$post_name_c] == $minutes)
					|| (!isset($_POST[$post_name_c]) && isset($oh[$day+1]["close"]) && $oh[$day+1]["close"] == $minutes)
					) {
					$selected = "selected=\"selected\"";
				}

				$label = $this->get_label($i);

				$close .= "<option value=\"$minutes\" {$selected}>{$label}</option>";
			}
	
			$closes[$day] = $close;
		}
	}

	function _BeforeSave() {
		if( $this->man ) {
			if( !isset($_POST["always"]) ) {
				reset($_POST);
				$oh = 0;
				foreach($_POST as $key => $value) {
					if( preg_match("/day(\d){1}(o|c)?/", $key, $m) ) {
						$i = $m[1]; $t = $m[2];
						if( $t == "c" ) {
							$close = $value;
							if( isset($_POST["day".$i."closed"]) || $close ) $oh++;
						}
					}
				}
				if( $oh < 7 ) {
					echo "<span style=\"color: red; font-weight: bold;\">! You need to set 'Hours Of Operation'!</span>";			
					return false;
				} 
			}
		}
		return true;
	}

}

class CColumnHH extends CColumn {
	var $hh = array(), $man;

	public function __construct($col_name, $col_title = '') {
		parent::__construct();
		$this->colType = COL_HH;
		$this->SetColName($col_name);
		$this->colTitle = $col_title;
	}

	function AddHH($title, $col_start, $col_end) {
		$this->hh[] = array("title" => $title, "col_start" => $col_start, "col_end" => $col_end);
	}

	function GetQuotedValue() {
		return get_magic_quotes_gpc() != 1 ? addslashes($this->colValue) : $this->colValue;
	}

	function setMandatory() {
		$this->man = true;
	}

	function DrawEditField() {
		if( $this->dlist->action == "edit") {
			foreach($this->hh as &$col) {
				if($this->dlist->row[$col['col_start']]||$this->dlist->row[$col['col_end']]) {
					$col['open'] = $this->dlist->row[$col['col_start']];
					$col['close'] = $this->dlist->row[$col['col_end']];
				}
			}
		}
		$this->hours($this->hh);
		$this->dlist->addForm($this->colTitle, "", "worker/worker_singlehh.tpl");
		return true;
	}

	function update() {
		$return = "";
		foreach($this->hh as $col) {
			$col_start = GetPostParam($col['col_start']);
			$col_end = GetPostParam($col['col_end']);
			$return .= ( $return  ? "," : "") . ("`{$col['col_start']}`='$col_start',`{$col['col_end']}`='$col_end'");
		}
		return $return;
	}

	function insert(&$insert) {
		foreach($this->hh as $col) {
			$insert["colName"][] = $col['col_start'];
			$insert["colValue"][] = $col['open'];
			$insert["colName"][] = $col['col_end'];
			$insert["colValue"][] = $col['close'];
		}
		return;
	}

	function hours(&$hh) {

		for($c=0;$c<count($hh);$c++) {
				$open = "<option value=\"\">-- don't know --</option>";
				$close = "<option value=\"\">-- don't know --</option>";
				$first = true;

			if( ($hh[$c]["open"] == -1 && $hh[$c]["close"] == -1) || isset($_POST[$hh[$c]["col_start"]."closed"]) ) { 
				$_POST[$hh[$c]["col_start"]] = NULL; 
				$_POST[$hh[$c]["col_end"]] = NULL;
			}

				for($i=0,$s=12,$x="AM";$i<24;$i++)
				{
						for($l=0;$l<60;$l=$l+30)
						{
								$t = (($i*60)+$l);
								if( $l == 0 ) $l = "00";
								if( isset($_POST[$hh[$c]["col_start"]]) && $_POST[$hh[$c]["col_start"]] == $t && $_POST[$hh[$c]["col_end"]]) {
										$open .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
					} else if( !isset($_POST[$hh[$c]["col_start"]]) && isset($hh[$c]["close"]) && $hh[$c]["close"] && $hh[$c]["open"] == $t ) 
										$open .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
								else
										$open .= "<option value=\"$t\">{$s}:{$l} {$x}</option>";
						}
						if( $s == 12 && $first ) { $s = 1; $first = false; }
						else if( $s == 12 ) { $s = 1; }
						else if( $s == 11 && !$first ) { $s++; $x = "PM"; }
						else $s++;
				}
			$hh[$c]["open"] = $open;

				$first = true;
				for($i=0,$s=12,$x="AM";$i<24;$i++)
				{
						for($l=0;$l<60;$l=$l+30)
						{
								if( $i==0 && $l==0) continue;
								$t = (($i*60)+$l);
								if( $i < 12 ) $t += 1440;
								if( $l == 0 ) $l = "00";
								if( isset($_POST[$hh[$c]["col_end"]]) && $_POST[$hh[$c]["col_end"]] == $t )
										$close .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
								else if( !isset($_POST[$hh[$c]["col_end"]]) && $hh[$c]["close"] == $t )
										$close .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
								else
										$close .= "<option value=\"$t\">{$s}:{$l} {$x}</option>";
						}
						if( $s == 12 && $first ) { $s = 1; $first = false; }
						else if( $s == 12 ) { $s = 1; }
						else if( $s == 11 && !$first ) { $s++; $x = "PM"; }
						else $s++;
				}
				if( $hh[$c]["close"] ==  1440 )
						$close .= "<option value=\"1440\" selected=\"selected\">12:00 AM</option>";
				else
						$close .= "<option value=\"1440\">12:00 AM</option>";
			$hh[$c]["close"] = $close;
		}
	}

	function _BeforeSave() {
		return true;
		if( $this->man ) {
			if( !isset($_POST["always"]) ) {
				reset($_POST);
				$oh = 0;
				foreach($_POST as $key => $value) {
					if( preg_match("/day(\d){1}(o|c)?/", $key, $m) ) {
						$i = $m[1]; $t = $m[2];
						if( $t == "c" ) {
							$close = $value;
							if( isset($_POST["day".$i."closed"]) || $close ) $oh++;
						}
					}
				}
				if( $oh < 7 ) {
					echo "<span style=\"color: red; font-weight: bold;\">! You need to set 'Hours Of Operation'!</span>";			
					return false;
				} 
			}
		}
		return true;
	}

}

class CColumnPicture extends CColumn {

	var $path = NULL, $patht = NULL, $handle, $type, $attr, $module = NULL;

	var $max = false, $set = false, $new_width, $new_height, $new_widtht = 220, $new_heightt = 165, $width, $height, $backup, $backup_module, 
		$sett = false, $watermark = false;

	public function __construct($col_name, $col_title, $module = NULL) {
		parent::__construct();
		$this->colName = $col_name;
		$this->colTitle = $col_title;
		$this->colType = COL_PICTURE;
		if( $module ) $this->module = $module;
	}	

	function GetQuotedValue() {
		return get_magic_quotes_gpc() != 1 ? addslashes($this->colValue) : $this->colValue;
	}

	function setPath($path) {
		$this->path = $path;
		if( !is_dir(IMAGE_PATH.$this->path) ) mkdir(IMAGE_PATH.$this->path, 0755);
	}

	function setPathT($path) {
		$this->patht = $path;
		if( !is_dir(IMAGE_PATH.$this->path) ) mkdir(IMAGE_PATH.$this->path, 0755);
	}

	function setWatermark() {
		$this->watermark = true;
	}

	function setMaxDimensions($width = NULL, $height = NULL) {
		if( $width ) $this->new_width = $width;
		if( $height ) $this->new_height = $height;
		$this->max = true;
	}

	function setDimensions($width, $height) {
		$this->new_width = $width;
		$this->new_height = $height;
		$this->set = true;
	}

	function setDimensionsT($width, $height) {
		$this->new_widtht = $width;
		$this->new_heightt = $height;
		$this->sett = true;
	}

	function setBackup($val = '') {
		$this->backup = true;
		$this->backup_module = $val;
	}

	function uploadImage() {
		if( is_null($this->path) || is_null($this->patht) ) die("CColumnPicture path||patht is not defined!");

		if( !$this->dlist->itemID ) return;

		for($x=1,$y=0;$x<=10;$x++) {
			$fileupname = "extra" . $x;
					$this->handle = new Upload($_FILES[$fileupname]);
					if ($this->handle->uploaded) {
				$id = $this->dlist->itemID;
				if( $this->backup == true ) { 
								$this->handle->Process(IMAGE_BACKUP);
								if ($this->handle->processed) {
						$text = "";
						foreach($_POST as $key => $value)
							$text .= "$key: $value";
						$text = get_magic_quotes_gpc() != 1 ? addslashes($text) : $text;
						$this->dlist->db->q("insert into image_backup (`id`, `module`, `filename`, `time`, `post`) values ('$id', 
'$this->backup_module', '{$this->handle->file_dst_name}', now(), '$text')");
					}
				}

				//$this->handle->image_convert = "jpg";
							list($this->width, $this->height, $this->type, $this->attr) = getimagesize($this->handle->file_src_pathname);
							if(  $this->max && ($this->width > $this->new_width || $this->height>$this->new_height) ) {
									$this->handle->image_resize = true;
									if( $this->width>$this->new_width ) { $this->handle->image_x = $this->new_width; $this->handle->image_ratio_y = true; }
									else { $this->handle->image_y = $this->new_height; $this->handle->image_ratio_x = true; }
							} else if( $this->set && $this->new_width && $this->new_height ) {
									$this->handle->image_resize = true;
									$this->handle->image_ratio_crop = true;
									$this->handle->image_x = $this->new_width;
									$this->handle->image_y = $this->new_height;
				}
							$this->handle->file_auto_rename = false;
							$this->handle->file_overwrite = true;
							$this->handle->file_new_name_body = $this->dlist->makeText() . "_" . $id;

							if( $this->watermark ) {
									$this->handle->image_watermark = _CMS_ABS_PATH.'/images/watermark.png';
									$this->handle->image_watermark_x = 0;
									$this->handle->image_watermark_y = 0;
							}
							$this->handle->Process(IMAGE_PATH.$this->path);
				$filename = $this->handle->file_dst_name;
						$this->handle = new Upload($_FILES[$fileupname]);
							if( $this->sett && ($this->width != $this->new_widtht || $this->height != $this->new_heightt) ) {
									$this->handle->image_resize = true;
									$this->handle->image_ratio_crop = true;
									$this->handle->image_x = $this->new_widtht;
									$this->handle->image_y = $this->new_heightt;
							}

							$this->handle->image_convert = "jpg";
							$this->handle->file_new_name_body = $this->dlist->makeText() . "_" . $id;
				$this->handle->Process(IMAGE_PATH.$this->path."/t");
				$thumb = $this->handle->file_dst_name;
				if( $this->module ) {
					global $account;
					$this->dlist->db->q("insert into place_picture (id, module, filename, thumb, account_id) values ('$id', 
'$this->module', '$filename', '$thumb', '{$account->isloggedin()}') ");
				}
				else $this->dlist->db->q("insert into {$this->colName} (id, filename, thumb) values ('$id', '$filename', '$thumb') ");
			   					$this->handle->Clean();
					}
		}
	}


	/**
	 * Returns an array of latitude and longitude from the Image file
	 *
	 * @return array|bool
	 */
	function readGeoLocation() {
		for($x=1,$y=0;$x<=10;$x++) {
			$fileupname = "extra" . $x;
			$this->handle = new Upload($_FILES[$fileupname]);
			if ($this->handle->uploaded) {
				if($result = read_geo($this->handle->file_src_pathname)) {
					return $result;
				}
			}
		}

		return false;
	}

	function remove($id) {
		if( $this->dlist->action == "edit" && intval($id) ) {
			$id = intval($id);
			if( $this->module ) $res = $this->dlist->db->q("select filename, thumb from place_picture where picture = '$id' and module = '$this->module'");
			else $res = $this->dlist->db->q("select filename, thumb from {$this->colName} where picture = '$id'");
			if( $this->dlist->db->numrows($res) ) {
				$row = $this->dlist->db->r($res);
				unlink(IMAGE_PATH.$this->path."/".$row["filename"]);
				unlink(IMAGE_PATH.$this->patht."/".$row["thumb"]);
				if( $this->module ) $this->dlist->db->q("delete from place_picture where picture = '$id'");
				else $this->dlist->db->q("delete from {$this->colName} where picture = '$id'");
				echo "<p>Picture is removed.</p>";
			}
			return true;
		}
		return false;
	}

	function AfterSave() {
		$geo_coords = $this->readGeoLocation();
		$this->uploadImage();
		if (!empty($geo_coords)) {
			$res = $this->dlist->db->q("SELECT loc_lat, loc_long FROM {$this->dlist->table} WHERE {$this->dlist->columns[0]->colName} = '{$this->dlist->itemID}'");
			$row = $this->dlist->db->r($res, MYSQL_ASSOC);
			if (empty($row['loc_lat']) && empty($row['loc_long'])) {
				$this->dlist->db->q("update {$this->dlist->table} set loc_lat='{$geo_coords['lat']}', loc_long='{$geo_coords['lng']}' where {$this->dlist->columns[0]->colName} = '{$this->dlist->itemID}'");
			}
		}
		return true;
	}

	function _AfterDelete() {
		if( $this->module ) $res = $this->dlist->db->q("select filename, thumb from place_picture where id = '{$this->dlist->itemID}' and module = '$this->module'");
		else $res = $this->dlist->db->q("select filename, thumb from {$this->colName} where id = '{$this->dlist->itemID}'");
		while($row=$this->dlist->db->r($res)) {
			unlink(IMAGE_PATH.$this->path."/".$row["filename"]);
			unlink(IMAGE_PATH.$this->patht."/".$row["thumb"]);
		}
		if( $this->module ) $this->dlist->db->q("delete from place_picture where id = '{$this->dlist->itemID}' and module = '$this->module'");
		else $this->dlist->db->q("delete from {$this->colName} where id = '{$this->dlist->itemID}'");
		return true;
	}

	function DrawEditField() {
		$value = $this->dlist->getFile("extra1");
		$value .= "<div id=\"extra\"></div><a href=\"javascript:void(0);\" onclick=\"addMore(); return false;\">add more files</a>";

		if( $this->dlist->action == "edit" ) {
			if( $this->module ) $res = $this->dlist->db->q("select * from place_picture where id = '{$this->dlist->itemID}' and module = '$this->module' and (place_file_type_id = 0 or place_file_type_id IS NULL)");
			else $res = $this->dlist->db->q("select * from {$this->colName} where id = '{$this->dlist->itemID}'");
			while($row=$this->dlist->db->r($res)) {
				$thumb = $row["thumb"];
				$value .= "<br/><img src='".IMAGE_URL.IMAGE_URL_PATH.$this->patht."/".$thumb."' alt='' /><a href='?id={$this->dlist->itemID}&removepicture={$row["picture"]}'>Remove this picture</a>\n";
			}
		}


		$this->dlist->addForm($this->colTitle, $value);
		return true;
	}

	function update() {
		return "";
	}

	function insert(&$insert) {
		return false;
	}
}

class CColumnFiles extends CColumn {

	var $module = NULL;

	public function __construct($col_name, $col_title, $module = NULL) {
		parent::__construct();
		$this->colName = $col_name;
		$this->colTitle = $col_title;
		$this->colType = COL_FILES;
		if( $module ) $this->module = $module;
	}	

	function GetQuotedValue() {
		return get_magic_quotes_gpc() != 1 ? addslashes($this->colValue) : $this->colValue;
	}

	function AfterSave() {
		//all manipulation with this column is AJAXoid - we dont need to do anything at save time
		return true;
	}

	function _AfterDelete() {
		//we need to delete all extra files for this business
		files::deleteAll($this->module, $this->dlist->itemID);
		return true;
	}

	function DrawEditField() {
		if ($this->dlist->itemID == NULL) {
			$value = "<em id=\"files_listing\">Please SAVE new place first for adding extra files.</em><br />";
		} else {
			$value = "<div id=\"files_listing\"></div>";
			$value .= "<script type=\"text/javascript\">\nfunction FilesRefreshListing() {_ajax('post', '/ajax/files/{$this->module}/{$this->dlist->itemID}/listing', '', 'files_listing'); }\nFilesRefreshListing();\n</script>";
		}
		$this->dlist->addForm($this->colTitle, $value);
		return true;
	}

	function update() {
		return "";
	}

	function insert(&$insert) {
		return false;
	}
}

class CColumnLatLong extends CColumn {
	var $address, $city, $state, $zipcode;

	public function __construct($col_name, $col_title) {
		parent::__construct();
		$this->colType		  = COL_LATLONG;
		$this->SetColName($col_name);
		$this->colTitle		 = !empty($col_title) ? $col_title : $col_name;
		$this->colSelect		= true;
		$this->colInsert		= true;
		$this->colUpdate		= true;
		$this->colEditField	 = true;
		$this->colShowTable	 = true;
	}

	function GetQuotedValue() {
		// @todo result not use - makes an unnecessary request only
        // if( $this->colValue ) return $this->colValue;
		// list($lat, $lon, $city, $zip) = get_coord($this->address, $this->city, $this->state, $this->zipcode);
		return $this->colValue;
	}

	function setAddressSource($source) {
		$this->address = $this->dlist->GetColValueByIdentifier($source);
	}
	function setCitySource($source) {
		$this->city = $this->dlist->GetColValueByIdentifier($source);
	}
	function setStateSource($source) {
		$this->state = $this->dlist->GetColValueByIdentifier($source);
	}
	function setZipcodeSource($source) {
		$this->zipcode = $this->dlist->GetColValueByIdentifier($source);
	}

	function DrawEditField() {
		$value = $this->dlist->getTextBox($this->colName, "", 10);
		$this->dlist->addForm($this->colTitle, $value);
	}

	function update() {
		return " {$this->colName} = '".$this->GetQuotedValue()."'";
	}

	function insert(&$insert) {
		$insert["colName"][] = $this->colName;
		$insert["colValue"][] = $this->GetQuotedValue();
		return true;
	}
}

class CColumnCover extends CColumn {
	var $oh = array(), $opens = array(), $closes = array(), $closed = array(), $man;

	var $oh_array = array('0'=>'12:00 AM', '30'=>'12:30 AM', '60'=>'1:00 AM', '90'=>'1:30 AM', '120'=>'2:00 AM', '150'=>'2:30 AM', '180'=>'3:00 AM', 
'210'=>'3:30 AM', '240'=>'4:00 AM', '270'=>'4:30 AM', '300'=>'5:00 AM', '330'=>'5:30 AM', '360'=>'6:00 AM', '390'=>'6:30 AM', '420'=>'7:00 AM', '450'=>'7:30 AM', 
'480'=>'8:00 AM',
'510'=>'8:30 AM', '540'=>'9:00 AM', '570'=>'9:30 AM', '600'=>'10:00 AM', '630'=>'10:30 AM', '660'=>'11:00 AM', '690'=>'11:30 AM', '720'=>'12:00 PM', '750'=>'12:30 PM',
'780'=>'1:00 PM', '810'=>'1:30 PM', '840'=>'2:00 PM', '870'=>'2:30 PM', '900'=>'3:00 PM', '930'=>'3:30 PM', '960'=>'4:00 PM', '990'=>'4:30 PM', '1020'=>'5:00 PM',
'1050'=>'5:30 PM', '1080'=>'6:00 PM', '1110'=>'6:30 PM', '1140'=>'7:00 PM', '1170'=>'7:30 PM', '1200'=>'8:00 PM', '1230'=>'8:30 PM', '1260'=>'9:00 PM', '1290'=>'9:30 PM',
'1320'=>'10:00 PM', '1350'=>'10:30 PM', '1380'=>'11:00 PM', '1410'=>'11:30 PM' ), $cover_array;

	var $cover = array();

	public function __construct($col_name) {
		parent::__construct();
		$this->colType = COL_COVER;
		$this->SetColName($col_name);
		$this->colTitle = $col_name;
	}

	function GetQuotedValue() {
		return get_magic_quotes_gpc() != 1 ? addslashes($this->colValue) : $this->colValue;
	}

	function setMandatory() {
		$this->man = true;
	}

	function setCoverArray($arr) {
		$this->cover_array = $arr;
	}

	function DrawEditField() {

		if( $this->dlist->action == "edit" ) {
			$this->getCover();
			//echo "<pre>".print_r($this->cover, true)."</pre><br />\n";
		}

		$beforetime = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->oh_array, $this->cover[1]["before"]["time"]);
		$time1 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->oh_array, $this->cover[1]["between"]["1"]);
		$time2 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->oh_array, $this->cover[1]["between"]["2"]);
		$after = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->oh_array, $this->cover[1]["after"]["time"]);

		$beforetime_2 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->oh_array, $this->cover[5]["before"]["time"]);
		$time1_2 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->oh_array, $this->cover[5]["between"]["1"]);
		$time2_2 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->oh_array, $this->cover[5]["between"]["2"]);
		$after_2 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->oh_array, $this->cover[5]["after"]["time"]);

		$this->dlist->smarty->assign("beforetime", $beforetime);
		$this->dlist->smarty->assign("time1", $time1);
		$this->dlist->smarty->assign("time2", $time2);
		$this->dlist->smarty->assign("after", $after);

		$this->dlist->smarty->assign("beforetime_2", $beforetime_2);
		$this->dlist->smarty->assign("time1_2", $time1_2);
		$this->dlist->smarty->assign("time2_2", $time2_2);
		$this->dlist->smarty->assign("after_2", $after_2);

		$_day1 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[1]["cover"]);
		$_day2 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[2]["cover"]);
		$_day3 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[3]["cover"]);
		$_day4 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[4]["cover"]);
		$_day5 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[5]["cover"]);
		$_day6 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[6]["cover"]);
		$_day7 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[7]["cover"]);

		$_day1_1 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[1]["before"]["cover"]);
		$_day2_1 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[2]["before"]["cover"]);
		$_day3_1 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[3]["before"]["cover"]);
		$_day4_1 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[4]["before"]["cover"]);
		$_day5_1 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[5]["before"]["cover"]);
		$_day6_1 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[6]["before"]["cover"]);
		$_day7_1 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[7]["before"]["cover"]);

		$_day1_2 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[1]["between"]["cover"]);
		$_day2_2 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[2]["between"]["cover"]);
		$_day3_2 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[3]["between"]["cover"]);
		$_day4_2 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[4]["between"]["cover"]);
		$_day5_2 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[5]["between"]["cover"]);
		$_day6_2 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[6]["between"]["cover"]);
		$_day7_2 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[7]["between"]["cover"]);

		$_day1_3 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[1]["after"]["cover"]);
		$_day2_3 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[2]["after"]["cover"]);
		$_day3_3 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[3]["after"]["cover"]);
		$_day4_3 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[4]["after"]["cover"]);
		$_day5_3 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[5]["after"]["cover"]);
		$_day6_3 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[6]["after"]["cover"]);
		$_day7_3 = $this->dlist->select_add_select() . $this->dlist->select_add_array($this->cover_array, $this->cover[7]["after"]["cover"]);

		if( $this->cover[1]["before"]["cover"] || $this->cover[2]["before"]["cover"] || $this->cover[3]["before"]["cover"] || 
$this->cover[4]["before"]["cover"] || $this->cover[5]["before"]["cover"] || $this->cover[6]["before"]["cover"] || $this->cover[7]["before"]["cover"] ) 
			$this->dlist->smarty->assign("multicover", true);

		$this->dlist->smarty->assign("_day1", $_day1);
		$this->dlist->smarty->assign("_day2", $_day2);
		$this->dlist->smarty->assign("_day3", $_day3);
		$this->dlist->smarty->assign("_day4", $_day4);
		$this->dlist->smarty->assign("_day5", $_day5);
		$this->dlist->smarty->assign("_day6", $_day6);
		$this->dlist->smarty->assign("_day7", $_day7);

		$this->dlist->smarty->assign("_day1_1", $_day1_1);
		$this->dlist->smarty->assign("_day2_1", $_day2_1);
		$this->dlist->smarty->assign("_day3_1", $_day3_1);
		$this->dlist->smarty->assign("_day4_1", $_day4_1);
		$this->dlist->smarty->assign("_day5_1", $_day5_1);
		$this->dlist->smarty->assign("_day6_1", $_day6_1);
		$this->dlist->smarty->assign("_day7_1", $_day7_1);

		$this->dlist->smarty->assign("_day1_2", $_day1_2);
		$this->dlist->smarty->assign("_day2_2", $_day2_2);
		$this->dlist->smarty->assign("_day3_2", $_day3_2);
		$this->dlist->smarty->assign("_day4_2", $_day4_2);
		$this->dlist->smarty->assign("_day5_2", $_day5_2);
		$this->dlist->smarty->assign("_day6_2", $_day6_2);
		$this->dlist->smarty->assign("_day7_2", $_day7_2);

		$this->dlist->smarty->assign("_day1_3", $_day1_3);
		$this->dlist->smarty->assign("_day2_3", $_day2_3);
		$this->dlist->smarty->assign("_day3_3", $_day3_3);
		$this->dlist->smarty->assign("_day4_3", $_day4_3);
		$this->dlist->smarty->assign("_day5_3", $_day5_3);
		$this->dlist->smarty->assign("_day6_3", $_day6_3);
		$this->dlist->smarty->assign("_day7_3", $_day7_3);

		$this->dlist->addForm($this->colTitle, "", "worker/worker_cover.tpl");
		return true;
	}

	function getCover() {
		$this->cover = json_decode($this->dlist->row["cover"], true);
	}

	function update() {

		$cover = $this->returnPostValues();

		$cover = json_encode($cover);
		return " cover = '$cover'";
	}

	function insert(&$insert) {

		$cover = $this->returnPostValues();

		$cover = json_encode($cover);
		$insert["colName"][] = "cover";
		$insert["colValue"][] = $cover;

		return;
	}

	function returnPostValues() {
		$beforetime = strlen($_POST["beforetime"])>0? intval($_POST["beforetime"]):"";
		$beforetime_2 = strlen($_POST["beforetime_2"])>0?intval($_POST["beforetime_2"]):"";
		$time1 = strlen($_POST["time1"])>0?intval($_POST["time1"]) : "";
		$time1_2 = strlen($_POST["time1_2"])>0?intval($_POST["time1_2"]) : "";
		$time2 = strlen($_POST["time2"])>0?intval($_POST["time2"]) : "";
		$time2_2 = strlen($_POST["time2_2"])>0?intval($_POST["time2_2"]) : "";
		$after = strlen($_POST["after"])>0?intval($_POST["after"]) : "";
		$after_2 = strlen($_POST["after_2"])>0?intval($_POST["after_2"]) : "";

		$_day1 = intval($_POST["_day1"]);
		$_day2 = intval($_POST["_day2"]);
		$_day3 = intval($_POST["_day3"]);
		$_day4 = intval($_POST["_day4"]);
		$_day5 = intval($_POST["_day5"]);
		$_day6 = intval($_POST["_day6"]);
		$_day7 = intval($_POST["_day7"]);

		$_day1_1 = intval($_POST["_day1_1"]);
		$_day2_1 = intval($_POST["_day2_1"]);
		$_day3_1 = intval($_POST["_day3_1"]);
		$_day4_1 = intval($_POST["_day4_1"]);
		$_day5_1 = intval($_POST["_day5_1"]);
		$_day6_1 = intval($_POST["_day6_1"]);
		$_day7_1 = intval($_POST["_day7_1"]);

		$_day1_2 = intval($_POST["_day1_2"]);
		$_day2_2 = intval($_POST["_day2_2"]);
		$_day3_2 = intval($_POST["_day3_2"]);
		$_day4_2 = intval($_POST["_day4_2"]);
		$_day5_2 = intval($_POST["_day5_2"]);
		$_day6_2 = intval($_POST["_day6_2"]);
		$_day7_2 = intval($_POST["_day7_2"]);

		$_day1_3 = intval($_POST["_day1_3"]);
		$_day2_3 = intval($_POST["_day2_3"]);
		$_day3_3 = intval($_POST["_day3_3"]);
		$_day4_3 = intval($_POST["_day4_3"]);
		$_day5_3 = intval($_POST["_day5_3"]);
		$_day6_3 = intval($_POST["_day6_3"]);
		$_day7_3 = intval($_POST["_day7_3"]);

		$cover[1] = array("day"=>1, "cover"=>"$_day1", "before"=>array("time"=>$beforetime, "cover"=>$_day1_1), 
"between"=>array("1"=>$time1,"2"=>$time2,"cover"=>$_day1_2),"after"=>array("time"=>$after,"cover"=>$_day1_3));
		$cover[2] = array("day"=>2, "cover"=>"$_day2", "before"=>array("time"=>"", "cover"=>$_day2_1), 
"between"=>array("1"=>"","2"=>"","cover"=>$_day2_2),"after"=>array("time"=>"","cover"=>$_day2_3));
		$cover[3] = array("day"=>3, "cover"=>"$_day3", "before"=>array("time"=>"", "cover"=>$_day3_1), 
"between"=>array("1"=>"","2"=>"","cover"=>$_day3_2),"after"=>array("time"=>"","cover"=>$_day3_3));
		$cover[4] = array("day"=>4, "cover"=>"$_day4", "before"=>array("time"=>"", "cover"=>$_day4_1), 
"between"=>array("1"=>"","2"=>"","cover"=>$_day4_2),"after"=>array("time"=>"","cover"=>$_day4_3));
		$cover[7] = array("day"=>7, "cover"=>"$_day7", "before"=>array("time"=>"", "cover"=>$_day7_1), 
"between"=>array("1"=>"","2"=>"","cover"=>$_day7_2),"after"=>array("time"=>"","cover"=>$_day7_3));
		$cover[5] = array("day"=>5, "cover"=>"$_day5", "before"=>array("time"=>$beforetime_2, "cover"=>$_day5_1), 
"between"=>array("1"=>$time1_2,"2"=>$time2_2,"cover"=>$_day5_2),"after"=>array("time"=>$after_2,"cover"=>$_day5_3));
		$cover[6] = array("day"=>6, "cover"=>"$_day6", "before"=>array("time"=>"", "cover"=>$_day6_1), 
"between"=>array("1"=>"","2"=>"","cover"=>$_day6_2),"after"=>array("time"=>"","cover"=>$_day6_3));

		return $cover;
	}

	function hours($oh = array(), &$opens, &$closes, &$closed) {
		$opens = array();
		$closes = array();

		if( $this->dlist->row["always"] == 1 ) $this->dlist->smarty->assign("always", true);;

		for($day=0;$day<7;$day++) {
				$open = "<option value=\"\">-- don't know --</option>";
				$close = "<option value=\"\">-- don't know --</option>";
				$first = true;

			if( ($oh[$day+1]["open"] == -1 && $oh[$day+1]["close"] == -1) || isset($_POST["day".($day+1)."closed"]) ) { 
				$closed[$day] = true; 
				$_POST["day".($day+1)."o"] = NULL; 
				$_POST["day".($day+1)."c"] = NULL;
			}

				for($i=0,$s=12,$x="AM";$i<24;$i++)
				{
						for($l=0;$l<60;$l=$l+30)
						{
								$t = (($i*60)+$l);
								if( $l == 0 ) $l = "00";
								if( isset($_POST["day".($day+1)."o"]) && $_POST["day".($day+1)."o"] == $t && $_POST["day".($day+1)."c"]) {
										$open .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
					} else if( !isset($_POST["day".($day+1)."o"]) && isset($oh[$day+1]["open"]) && $oh[$day+1]["open"] == $t )
										$open .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
								else
										$open .= "<option value=\"$t\">{$s}:{$l} {$x}</option>";
						}
						if( $s == 12 && $first ) { $s = 1; $first = false; }
						else if( $s == 12 ) { $s = 1; }
						else if( $s == 11 && !$first ) { $s++; $x = "PM"; }
						else $s++;
				}
				$opens[$day] = $open;

				$first = true;
				for($i=0,$s=12,$x="AM";$i<24;$i++)
				{
						for($l=0;$l<60;$l=$l+30)
						{
								if( $i==0 && $l==0) continue;
								$t = (($i*60)+$l);
								if( $i < 12 ) $t += 1440;
								if( $l == 0 ) $l = "00";
								if( isset($_POST["day".($day+1)."c"]) && $_POST["day".($day+1)."c"] == $t )
										$close .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
								else if( !isset($_POST["day".($day+1)."c"]) && $oh[$day+1]["close"] == $t )
										$close .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
								else
										$close .= "<option value=\"$t\">{$s}:{$l} {$x}</option>";
						}
						if( $s == 12 && $first ) { $s = 1; $first = false; }
						else if( $s == 12 ) { $s = 1; }
						else if( $s == 11 && !$first ) { $s++; $x = "PM"; }
						else $s++;
				}
				if( $oh[$day+1]["close"] ==  1440 )
						$close .= "<option value=\"1440\" selected=\"selected\">12:00 AM</option>";
				else
						$close .= "<option value=\"1440\">12:00 AM</option>";
				$closes[$day] = $close;
		}
	}

	function _BeforeSave() {
		if( $this->man ) {
			if( !isset($_POST["always"]) ) {
				reset($_POST);
				$oh = 0;
				foreach($_POST as $key => $value) {
					if( preg_match("/day(\d){1}(o|c)?/", $key, $m) ) {
						$i = $m[1]; $t = $m[2];
						if( $t == "c" ) {
							$close = $value;
							if( isset($_POST["day".$i."closed"]) || $close ) $oh++;
						}
					}
				}
				if( $oh < 7 ) {
					echo "<span style=\"color: red; font-weight: bold;\">! You need to set 'Hours Of Operation'!</span>";			
					return false;
				} 
			}
		}
		return true;
	}

}


class CColumnManual extends CColumn {

	var $value;

	public function __construct($col_name, $col_title, $value) {
		parent::__construct();
		$this->colType		  = COL_MANUAL;
		$this->SetColName($col_name);
		$this->colTitle		 = !empty($col_title) ? $col_title : $col_name;

		$this->colSelect		= false;
		$this->colInsert		= false;
		$this->colUpdate		= false;
		$this->colEditField	 = empty($value) ? true : false;
		$this->colShowTable	 = false;
		$this->value		= $value;
	}

	function GetQuotedValue() {
		return $this->colValue;
	}

	function DrawEditField() {
		if( empty($this->value) ) $this->value = html_entity_decode(strip_tags($this->colValue));
		$this->dlist->addForm($this->colTitle, $this->value);
	}

	function update() {
		return "";
	}

	function insert(&$insert) {
		return true;
	}
}

class data {
	
	var $table, $where, $smarty, $db, $action;

	var $javascript, $hidden, $row = NULL;

	var $save_and_add_new;  //boolean

	var $saving;			//can be 0 or 1
	var $cbutton;

	/*
	* DOM element
	*/
	var $form;

	var $columns;

	var $visitor_mode = false, $visitor_url = NULL, $admin_url = NULL;
	var $owner_mode = false, $owner_link;

	var $work_item = null;
	var $work_action = null;

	var $notify_admin = true;

	var $die_after_delete = false;
	var $browse = false, $browse_item, $browsing = false;
	var $itemLink = NULL;
	var $save_button_value = NULL;
	var $redirectToItem = false;
	var $check_permission = false;
	var $remove_review = NULL;

	var $country_id = 0;

	var $cache_update = false;

	public function __construct() {
		global $smarty, $db;

		$this->db = $db;

		$this->smarty = $smarty;
		$this->save_and_add_new = false;

		if( isset($_GET['ajaxdie']) && isset($_GET['City']) && !empty($_GET['City']) ) {
			if( !is_object($location) ) {
				require_once(_CMS_ABS_PATH."/inc/classes/class.location.php");
				$location = new location($db);
			}
			$loc_id = intval($_GET['City']);
			$loc = $location->_coreLocationBreadArray($loc_id);
			$this->country_id = $loc[count($loc)-1]['loc_id'];
		}

	}

	function addForm($name, $value, $include = NULL) {
		$this->form[] = array("name"=>$name, "value"=>$value, "include"=>$include);
	}

	function hidden($name, $value) {
		$this->hidden .= "<input type='hidden' name='$name' value='$value' />\n";
	}
	
	/**
	 * Method GetTextBox() returns HTML code of text input box
	 *
	 * @param string $name input box name
	 * @param string $value predefined value of textbox
	 * @param integer $size size of input box
	 * @return string HTML code of input box
	 */
	function GetTextBox($name, $value, $size = 60, $class = "") {
		if (empty($value))
			$value = isset($this->row[$name]) ? stripslashes($this->row[$name]) : $default_value;
		if( $class ) $class = "class='$class'";
		return "<input type=\"text\" name=\"".htmlspecialchars($name)."\" size=\"{$size}\" value=\"".htmlspecialchars($value)."\" $class />\n";
	}

	function colTextBox($column) {
		$name = htmlspecialchars($column->colName);
		$value = $column->colValue ? htmlspecialchars($column->colValue) : (isset($this->row[$column->colName]) ? $this->row[$column->colName] : "");
		$class = $column->colMandatory ? "class='mandatory'" : "";
		$readonly = $column->colReadOnly ? "readonly=\"readonly\"" : "";
		return "<input type=\"text\" name=\"$name\" size=\"60\" value=\"$value\" $class $readonly />\n";
	}
	
	function GetPhoneBox($column) {
		$value = empty($column->colValue) ? (isset($this->row[$column->colName]) ? makeproperphonenumber($this->row[$column->colName], true) : "") :
makeproperphonenumber($column->colValue, true);

		if( $column->unique ) {
			$id = $this->columns[0]->colValue ? $this->columns[0]->colValue : $this->row[$this->columns[0]->colName];
			$script = "onblur=\"_ajax('get', '', 'ajax=true&field={$column->colName}&id={$id}&phone='+encodeURIComponent($('#phone{$column->colName}').val()), 'span{$column->colName}');\"";
			$span = "<span id='span$column->colName' class='error'>&nbsp;</span>";
			if( $column->skip_unique && !$this->visitor_mode) $span .= "<div><input type='checkbox' name='phonesu$column->colName' " . (GetPostParam("phonesu$column->colName")!=false?"checked='checked'":'') . "/> Skip Unique phone number 
control </div>";
		}
		$readonly = $column->colReadOnly == true ? "readonly=\"readonly\"" : "";
		$return = "<input type=\"text\" name=\"".htmlspecialchars($column->colName)."\" id=\"phone$column->colName\" size=\"15\" 
value=\"".htmlspecialchars($value)."\" $script $readonly />$span\n";
		return $return;
	}

	function GetRecheckCoordBox($column) {
		$value = empty($column->colValue) ? (isset($this->row[$column->colName]) ? doubleval($this->row[$column->colName]) : "") :
			doubleval($column->colValue);
		if( $column->recheck ) {
			$id = $this->columns[0]->colValue ? $this->columns[0]->colValue : $this->row[$this->columns[0]->colName];
			$script = "onclick=\"_ajaxj('get', '', 'ajax=true&field={$column->colName}&id={$id}');\"";
			$span = "<span id='span$column->colName' class='error'>&nbsp;</span>";
			$span .= "<button  type='button' name='coord_$column->colName' style='padding-left: 8px;' $script> Re-checking coordinates (makes a request)  </button>";
		}
		$return = "<input type=\"text\" name=\"".htmlspecialchars($column->colName)."\" id=\"coord_$column->colName\" size=\"10\" 
value=\"".htmlspecialchars($value)."\"/>$span\n";
		return $return;
	}

	/**
	 * Method GetPasswordBox() returns HTML code of password input box
	 *
	 * @param string $name input box name
	 * @param string $value predefined value of password box
	 * @param integer $size size of input box
	 * @return string HTML code of input box
	 */
	function GetPasswordBox($name, $value, $size = 60) {
		if (empty($value))
			$value = $default_value;
		return "<input type=\"password\" name=\"".htmlspecialchars($name)."\" size=\"{$size}\" value=\"".htmlspecialchars($value)."\" />\n";
	}

	function GetTextArea($column) {
		if (empty($column->colValue))
			$column->colValue = isset($this->row[$name]) ? stripslashes($this->row[$name]) : "";
		$readonly = $column->colReadOnly ? "readonly='readonly'" : "";
		$maxlength = $column->maxLength ? "maxlength='{$column->maxLength}' " : "";
		return "<textarea name=\"".htmlspecialchars($column->colName)."\" cols=\"{$column->Cols}\" rows=\"{$column->Rows}\" {$maxlength}{$readonly}>$column->colValue</textarea>\n";
	}

	/**
	 * Method GetRadioBox() returns HTML code of radio box
	 *
	 * @param string $name input box name
	 * @param string $value predefined value of textbox
	 * @param boolean $checked checked ?
	 * @return string HTML code of radio box
	 */
	function GetRadioBox($name, $value, $checked = false) {
		$html = "<input type=\"radio\" name=\"".htmlspecialchars($name)."\" value=\"".htmlspecialchars($value)."\" ";
		if ($checked)
			$html .= "checked=\"checked\" ";
		$html .= "/>\n";
		return $html;
	}

	/**
	 * Method GetRadioBoxes() returns HTML code of multiple radio boxes
	 *
	 * @param string $name input box name
	 * @param string $value predefined value of textbox
	 * @param mixed $options | delimited string of select options or two dimensional array of all options (value & name)
	 * @return string HTML code of radio box
	 */
	function GetRadioBoxes($name, $value, $options) {
		$html = "";
		if (is_string($options)) {
			$temp = explode("|", $options);
			$options = array();
			foreach ($temp as $tmp) {
				$options[] = array($tmp, $tmp);
			}
		}
		$count = 1;
		foreach ($options as $option) {
			if( is_array($option) ) {
				foreach($option as $k => $v)
					$html .= $this->GetRadioBox($name, $k, ($k == $value) ? true : false)." ".htmlspecialchars($v)."<br />\n";
			} else {
				$html .= $this->GetRadioBox($name, $count, ($count == $value) ? true : false)." ".htmlspecialchars($option)."<br />\n";
				$count++;
			}
		}

		return $html;
	}

	/**
	 * Method GetCheckBox() returns HTML code of check box
	 *
	 * @param string $name input box name
	 * @param boolean $checked checked ?
	 * @param string $title not mandatory title of checkbox (will be printed after checkbox)
	 * @return string HTML code of check box
	 */
	function GetCheckBox($name, $checked = false, $title = '') {
		$html = "";
		if ($checked === "on")
			$checked = 1;
		$html = "<input type=\"checkbox\" name=\"".htmlspecialchars($name)."\" value=\"1\" ";
		if ($checked)
			$html .= "checked=\"checked\" ";
		$html .= "/>";
		if (!empty($title))
			$html .= "&nbsp;".htmlspecialchars($title)."\n";
		return $html;
	}

	/**
	 * Method GetSelect() returns HTML code of select dropdown
	 *
	 * @param string $name input box name
	 * @param mixed $value - pre -selected value of dropdown
	 * @param mixed $options | delimited string of select options or two dimensional array of all options (value & name)
	 * @param boolean $show_null if set to true, null option will be drawn
	 * @return string HTML code of check box
	 */
	function GetSelect($name, $value, $options, $show_null = false, $class = "") {
		if (is_string($options)) {
			$temp = explode("|", $options);
			$options = array();
			foreach ($temp as $tmp) {
				$options[] = array($tmp, $tmp);
			}
		}
		if (!is_array($options)) {
			echo "Wrong argument for SELECT options !<br />\n";
			return false;
		}
		if (empty($value))
			$value = $this->postorsql($name);

		$html = "<select name=\"".htmlspecialchars($name)."\"";
		if( $class ) $html .= " class=\"$class\"";
		$html .= " >";
		if ($show_null)
			$html .= "<option value=\"\">- Select -</option>\n";
		foreach ($options as $option) {
			$html .= "<option value=\"".htmlspecialchars($option[0])."\" ";
			if ($option[0] == $value)
				$html .= "selected=\"selected\" ";
			$html .= ">".htmlspecialchars($option[1])."</option>";
		}
		$html .= "</select>\n";
		return $html;
	}

	function GetSelectBox($column) {
		if (is_string($column->enum_options)) {
			$temp = explode("|", $column->enum_options);
			$options = array();
			foreach ($temp as $tmp) {
				$column->enum_options[] = array($tmp, $tmp);
			}
		}
		if (!is_array($column->enum_options)) {
			echo "Wrong argument for SELECT options !<br />\n";
			return false;
		}
		if (empty($column->colValue)) {
			if( $column->default_value && $column->colValue !== '0' ) {
				$column->colValue = $column->default_value;
			}
			else $column->colValue = $this->postorsql($column->colName);
		}

		$html = "<select name=\"".htmlspecialchars($column->colName)."\"";
		if( $column->class ) $html .= " class=\"$column->class\"";
		$html .= " >";
		if ($column->show_null)
			$html .= "<option value=\"\">- Select -</option>\n";
		foreach ($column->enum_options as $option) {
			$html .= "<option value=\"".htmlspecialchars($option[0])."\" ";
			if ($option[0] == $column->colValue)
				$html .= "selected=\"selected\" ";
			$html .= ">".htmlspecialchars($option[1])."</option>";
		}
		$html .= "</select>\n";
		return $html;
	}

	function GetMultiSelectBox($column) {
		if (is_string($column->enum_options)) {
			$temp = explode("|", $column->enum_options);
			foreach ($temp as $tmp) {
				$column->enum_options[] = array($tmp, $tmp);
			}
		}

		if (!is_array($column->enum_options)) {
			echo "Wrong argument for SELECT options !<br />\n";

			return false;
		}
		if (empty($column->colValue)) {
			if ($column->default_value && $column->colValue !== '0') {
				$column->colValue = $column->default_value;
			} else {
				$column->colValue = $this->postorsql($column->colName);
			}
		}
		$values = explode(',', $column->colValue);
		$html   = "<select name=\"".htmlspecialchars($column->colName)."[]\" multiple=\"multiple\"";
		if ($column->class) {
			$html .= " class=\"multi-select $column->class\"";
		} else {
			$html .= " class=\"multi-select\"";
		}
		$html .= " >";
		if ($column->show_null) {
			$html .= "<option value=\"\">- Select -</option>\n";
		}
		foreach ($column->enum_options as $option) {
			$html .= "<option value=\"".htmlspecialchars($option[0])."\" ";
			if (in_array($option[0], $values)) {
				$html .= "selected=\"selected\" ";
			}
			$html .= ">".htmlspecialchars($option[1])."</option>";
		}
		$html .= "</select>\n";

		return $html;
	}

	function getFile($name) {
		return "<input type=\"file\" name=\"$name\" />";
	}

	function SetSelectMoneyOption($start, $end, $interval) {
		$output[] = array("0"=>-1, "1"=>"Free");
		for($i=$start;$i<$end;$i+=$interval) {
			$output[] = array("0"=>$i, "1"=>"\${$i}.00");
		}
		return $output;
	}

	function select_box($name, $value) {
		return "<select name='$name'>$value</select>";
	}

	function select_add_yesno($value = -1)  {
		if( $value == 1 )
			return "<option value=\"1\" selected=\"selected\">Yes</option><option value=\"0\">No</option>";
		else if( $value == 0 && $value != "" ) {
			return "<option value=\"1\">Yes</option><option value=\"0\" selected=\"selected\">No</option>";
		} else
			return "<option value=\"1\">Yes</option><option value=\"0\">No</option>";
	}

	function select_add_select() {
		return "<option value=\"\">-- select --</option>";
	}

	function select_add_option($value, $name, $selected) {
		$selected = $selected ? "selected=\"selected\"" : "";
		return "<option value=\"$value\" $selected>$name</option>";
	}

	function select_add_array($arr, $d) {
		$options = "";
		foreach( $arr as $key => $a ) {
			if( is_array($a) ) {
				foreach($a as $x => $y ) {
					$key = $x;
					$a = $y;
				}
			}
			if( !strcmp($d, $key) )
				$options .= "<option value=\"$key\" selected=\"selected\">$a</option>";
			else
				$options .= "<option value=\"$key\">$a</option>";
		}
		return $options;
	}

	function arrayValue($arr, $value1) {
		foreach($arr as $key => $value) {
			if( $key == $value1 )
				return $value;
		}
		return NULL;
	}

	function BeforeDelete() {
		return true;
	}

	function hours($oh = array(), &$opens, &$closes) {
		$opens = array();
		$closes = array();

		for($day=0;$day<7;$day++) {
				$open = "<option value=\"\">-- don't know --</option>";
				$close = "<option value=\"\">-- don't know --</option>";
				$first = true;
				for($i=0,$s=12,$x="AM";$i<24;$i++)
				{
						for($l=0;$l<60;$l=$l+30)
						{
								$t = (($i*60)+$l);
								if( $l == 0 ) $l = "00";
								if( isset($_POST["day".($day+1)."o"]) && $_POST["day".($day+1)."o"] == $t && $_POST["day".($day+1)."c"]) {
										$open .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
					} else if( !isset($_POST["day".($day+1)."o"]) && isset($oh[$day+1]["open"]) && $oh[$day+1]["open"] == $t )
										$open .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
								else
										$open .= "<option value=\"$t\">{$s}:{$l} {$x}</option>";
						}
						if( $s == 12 && $first ) { $s = 1; $first = false; }
						else if( $s == 12 ) { $s = 1; }
						else if( $s == 11 && !$first ) { $s++; $x = "PM"; }
						else $s++;
				}
				$opens[$day] = $open;

				$first = true;
				for($i=0,$s=12,$x="AM";$i<24;$i++)
				{
						for($l=0;$l<60;$l=$l+30)
						{
								if( $i==0 && $l==0) continue;
								$t = (($i*60)+$l);
								if( $i < 12 ) $t += 1440;
								if( $l == 0 ) $l = "00";
								if( isset($_POST["day".($day+1)."c"]) && $_POST["day".($day+1)."c"] == $t )
										$close .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
								else if( !isset($_POST["day".($day+1)."c"]) && $oh[$day+1]["close"] == $t )
										$close .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
								else
										$close .= "<option value=\"$t\">{$s}:{$l} {$x}</option>";
						}
						if( $s == 12 && $first ) { $s = 1; $first = false; }
						else if( $s == 12 ) { $s = 1; }
						else if( $s == 11 && !$first ) { $s++; $x = "PM"; }
						else $s++;
				}
				if( $oh[$day+1]["close"] ==  1440 )
						$close .= "<option value=\"1440\" selected=\"selected\">12:00 AM</option>";
				else
						$close .= "<option value=\"1440\">12:00 AM</option>";
				$closes[$day] = $close;
		}
	}

	function getLocation($deep = 1, $ids) {
		$output = "";
		while($deep>0) {
			$loc_parent = $ids[$deep+1] ? $ids[$deep+1] : 0;
			$select_name = $deep == 2 ? "state_select" : ($deep == 1 ? "city_select" : "c_select");
			$output .= "<span id=\"{$select_name}\"></span>";
			$what = $deep == 3 ? "Country" : ($deep == 2 ? "State" : "City");
			$whatr = $deep == 3 ? "country_id" : ($deep == 2 ? "state_id" : "loc_id");
			if( !$ids[$deep] && isset($this->row[$whatr]) ) $ids[$deep] = $this->row[$whatr];
			if( $ids[$deep] )
				$this->javascript .= "_ajax(\"get\", \"/_ajax/loc\", 
\"parent_name={$loc_parent}&select_name={$what}&{$what}={$ids[$deep]}\",\"{$select_name}\");";
			$deep--;
		}
		return $output;
	}

	function makeText($limit = 20, $intonly = 0) {
		if( $intonly )
			$m = "1234567890";
		else
			$m = "abcdefghijklmnoprstuwyxzABCDEFGHIJKLMOPRSTUYW1234567890";

		$s = "";
		for($i=0;$i<$limit;$i++)
			$s .= $m[rand(0, strlen($m))];
		return $s;
	}

	function postorsql($name, $type = 0) {
		if ($type == COL_FEE_RANGE) {
			if (isset($_POST[$name."_from"]) && isset($_POST[$name."_to"]))
				return GetPostParamStripped($name."_from")."|".GetPostParamStripped($name."_to");
		} else {
			if (isset($_POST[$name]))
				return GetPostParamStripped($name);
		}
		if (isset($_POST['id']))
			return NULL;
		else if (isset($this->row[$name]))
			return $this->row[$name];
		else
			return NULL;
	}

	function addtextbox($name) {
		return "<input type=\"text\" name=\"".htmlspecialchars($name)."\" size=\"{$size}\" value=\"".htmlspecialchars($value)."\" />\n";
	}

	function CCustomButton($name, $value, $onclick = NULL) {
		$this->cbutton[] = array("name"=>$name, "value"=>$value, "onclick"=>$onclick);
	}

	function AppendColumn($column) {
		$column->dlist = &$this;
		$column->colIndex = count($this->columns);
		$this->columns[] = $column;
	}

	function GetColumnIndexByIdentifier($identifier) {
		$i = 0;
		foreach ($this->columns as $column) {
			if ($column->colIdentifier == $identifier)
				return $i;
			$i++;
		}
		return false;
	}

	function GetColValueByIdentifier($identifier) {
		if (($index = $this->GetColumnIndexByIdentifier($identifier)) !== false)
			return $this->columns[$index]->colValue;
		else
			return false;
	}

	function Check() {

		//check that we have exactly one CColumnId column
		$id_count = 0;
		foreach ($this->columns as $column) {
			if ($column->colType == COL_ID)
			$id_count++;
		}
		if ($id_count == 0) {
			echo "<span class=\"highlight\">No CColumnId defined (there must be exactly one CColumnId defined and it must be first column defined) !</span><br />\n";
			return false;
		}
		if ($id_count > 1) {
			echo "<span class=\"highlight\">Too much ID columns defined (CColumnId) defined (there must be exactly one CColumnId defined and it must be first column defined) !</span><br />\n";
			return false;
		}
		return true;
	}

	function processAjax() {

		ob_end_clean();
		//header('Content-type: application/javascript');

		if( isset($_GET["phone"]) ) {
			$phone = preg_replace("/[^0-9+]/", "", $_GET["phone"]);
			if (!$phone)
				return;
			
			//hardcoded fix - dont alarm on internatoinal phone numbers
			if (substr($phone, 0, 2) != "+1")
				return;

			$id = intval($_GET["id"]);

			$res = $this->db->q("
					SELECT p.place_id 
					FROM place p 
					INNER JOIN location_location l on l.loc_id = p.loc_id 
					WHERE p.deleted IS NULL AND p.place_id != ? AND p.phone1 = ?",
				array($id, $phone)
			);

			if ($this->db->numrows($res)) {
				$rox = $this->db->r($res);
				$id = $rox[0];
				if ($this->itemLink)
					die("There is a <a href='$this->itemLink?id={$id}' target='_blank'>place</a> using this phone number in our system. You do not need to submit this place again.");
				else
					die("There is a <a href=\"?id={$id}\" target=\"_blank\">place</a> using this phone number in our system. You do not need to submit this place again.");
			}
		}
		if (isset($_GET["field"]) && $_GET["field"] == 'loc_lat') {
			global $ctx;

			$id = intval($_GET["id"]);

			$res = $this->db->q("
					SELECT p.loc_lat, p.loc_long, p.address1, p.zipcode
					FROM place p 
					WHERE p.deleted IS NULL AND p.place_id = ?",
				[$id]
			);

			if ($this->db->numrows($res)) {
				$row = $this->db->r($res);

				list($lat, $lon, $city, $zip) = get_coord($row["address1"], $ctx->location_name, $ctx->country,
					$row["zipcode"]);

				if ($lat != "error") {

					//found latlong, update place table
//					$db->q("UPDATE place SET loc_lat = ?, loc_long = ? WHERE place_id = ?",
//						[$lat, $lon, $id]);
					die("$('#coord_loc_lat').val('{$lat}');$('#coord_loc_long').val('{$lon}');");
				} else {
					die("$('#spanloc_lat').html('{$lon}')");
				}
			}
		}
	}

	function createItemCol()  {
		global $ctx;

		if( !$this->itemID ) return;

		$newlink = $_SERVER['REQUEST_URI'];
		if ($p = strpos($newlink, '?'))
			$newlink = substr($newlink, 0, $p);

		if( $this->owner_mode ) {
			$this->addForm("ID", $this->itemID);
			return;
		}
		if ($ctx->international) {
			$this->addForm("ID", "<a href='".dir::getPlaceLink($this->row)."'>{$this->itemID}</a> - <a href='$newlink'>(Add a NEW Place)</a>");
		} else if ($this->itemLink) {
			$place_link = "{$this->itemLink}?id={$this->itemID}";
			$this->addForm("ID", "<a href='{$place_link}'>{$this->itemID}</a> - <a href='$newlink'>(Add a NEW Place)</a>");
		} else
			$this->addForm("ID", $this->itemID);
	}

	function ParseRequest() {
		global $ctx;

		$this->action = "";
		$this->itemID = NULL;

		if( isset($_GET["ajax"]) ) {
			$this->processAjax();
			die;
		}

		if( isset($_GET["browse"]) && empty($_POST["data_submit"]) ) {
			$this->browsing = true;
			return true;
		}

		//jay
		//TODO reassigns are hardcoded
		if (intval($_REQUEST["work_item"])) {
			$this->work_item = intval($_REQUEST["work_item"]);
			if (isset($_REQUEST["Save_Completed"])) {
				$this->saving = true;
				$this->action = "edit";
				$this->work_action = "save_completed";
			} else if (isset($_REQUEST["Skip"])) {
				$this->work_action = "skip";
			} else if (isset($_REQUEST["Remove"])) {
				$this->saving = false;
				$this->action = "del";
				$this->work_action = "del_completed";
			} else if (isset($_REQUEST["reassign_30220"])) {
				$this->work_action = "reassign_maarten";
			} else if (isset($_REQUEST["reassign_226368"])) {
				$this->work_action = "reassign_rachel";
			}
		}

		if (!$this->work_action) {
			if (isset($_REQUEST["data_action"]) && !empty($_REQUEST["data_action"]) && !$this->visitor_mode ) {
				$this->action = $_REQUEST["data_action"];
			} else if (isset($_REQUEST["data_submit"]))
				$this->action = "add";

			if (isset($_POST["data_action"]) && (isset($_POST["Save"])||isset($_POST["SaveAddNew"])))
				$this->saving = true;
		}

		if( isset($_REQUEST["id"]) && !$this->visitor_mode ) {
			$this->itemID = GetRequestParam("id");

			//load place to row array
			$this->loadRow();

			if (!is_null($this->row) && ($this->row !== false)) {
				if( isset($_POST["Remove"])&&$this->show_remove ) {
					$this->action = "del";
					return true;
				}
				$this->action = "edit";
				if( !$this->saving ) {
					$this->hidden("id", $this->itemID);
					$this->createItemCol();
				}
			}
		}

		if( isset($_REQUEST["removeimage"]) && !empty($_REQUEST["removeimage"]) && $this->itemID ) {
			if( $this->owner_mode ) {
				if( !$this->checkIfOwner() ) {
					$system = new system; $system->moved("/");
					die;
				}
			}
			foreach ($this->columns as $column) {
				if( $column->colType == COL_IMAGE && $_REQUEST["removeimage"] == $column->colName )  {
					$column->remove();
					break;
				}
			}
			$system = new system;
			$system->moved($_SERVER["FILENAME"]."?id=".$this->itemID);
		}

		if( isset($_REQUEST["removepicture"]) && !empty($_REQUEST["removepicture"]) && $this->itemID ) {
			if( $this->owner_mode ) {
				if( !$this->checkIfOwner() ) {
					reportAdmin("AS: owner hack");
					$system = new system; $system->moved("/");
					die;
				}
			}
			foreach ($this->columns as $column) {
				if( $column->colType == COL_PICTURE ) {
					$column->remove($_REQUEST["removepicture"]);
					break;
				}
			}
			$system = new system;
			$system->moved($_SERVER["FILENAME"]."?id=".$this->itemID);
		}

		foreach ($this->columns as &$column) {
			if( ($column->colUpdate) || ($column->colInsert) || ($column->colEditField)) {
				$column->colValue = GetPostParamStripped($column->colName);
				//jay
				if ($column->colType == COL_FEE_RANGE) {
					$column->colValue = GetPostParamStripped($column->colName."_from")."|".GetPostParamStripped($column->colName."_to");
				}

				if( $this->itemID && $this->action == "edit" && !isset($_GET['ajaxdie'])) {
					$column->colValue = $this->postorsql($column->colName, $column->colType);
				} elseif( /*!$this->itemID &&*/ isset($_GET['ajaxdie']) && isset($_GET[$column->colName]) ) {
					$column->colValue = $_GET[$column->colName];
				}
			}
		}

		return true;
	}

	function mapUpdate() {
		if( isset($_GET['lat']) && !empty($_GET['lat']) ) {
			$lat = $this->row["loc_lat"];
			$long = $this->row["loc_long"];
			$val = "<p><b>Current Location</b></br><img 
src='http://maps.google.com/maps/api/staticmap?center={$lat},{$long}&amp;zoom=15&amp;size=359x345&amp;maptype=roadmap&amp;markers=color:red|label:A|{$lat},{$long}&amp;sensor=false' 
/ >
			<br>{$lat}<br>{$long}</p>";
			echo $val;

			$val = "<p><b>Requested Location</b></br><img 
src='http://maps.google.com/maps/api/staticmap?center={$_GET['lat']},{$_GET['long']}&amp;zoom=15&amp;size=359x345&amp;maptype=roadmap&amp;markers=color:red|label:A|{$_GET['lat']},{$_GET['long']}&amp;sensor=false' 
/ >
			<br>{$_GET['lat']}<br>{$_GET['long']}</p>";
			echo $val;

			echo <<<EOF
<script type='text/javascript'>
function updateloc() {
		$('input[name=loc_lat]').val('{$_GET['lat']}');
		$('input[name=loc_long]').val('{$_GET['long']}');
		$('#SaveButton').click();
}
</script>
<input type='button' value='change' onclick='updateloc();' />
EOF;
		}
	}

	function _DeleteItem() {
		global $ctx;

		for ($i = 0; $i < count($this->columns); $i++) {
			if (!$this->columns[$i]->_BeforeDelete()) return false;
		}

		if (!$this->BeforeDelete())
			return false;

		//TODO unified code
		if ($this->table == "place") {
			//first delete attributes
			$this->db->q("delete from place_attribute where place_id = '{$this->itemID}'");
			//we dont delete entry from main place table
			$this->db->q("update place set deleted = UNIX_TIMESTAMP() where {$this->columns[0]->colName} = '{$this->itemID}'");
		} else if ($this->table == "eroticmassage") {
			//we dont delete entry from eroticmassage table
			$this->db->q("UPDATE eroticmassage SET deleted = NOW() WHERE {$this->columns[0]->colName} = ?", [$this->itemID]);
		} else {
			$this->db->q("delete from {$this->table} where {$this->columns[0]->colName} = '{$this->itemID}'");
		}

		for ($i = 0; $i < count($this->columns); $i++) {
			if (!$this->columns[$i]->_AfterDelete()) return false;
			$this->columns[$i]->colValue = NULL;
		}

		if ($this->remove_review) {
			$this->db->q("delete r, p, c from place_review r left join place_review_provider p using (provider_id) left join place_review_comment c using 
(review_id) where r.id = '{$this->itemID}' and r.module = '{$this->remove_review}'");
		}

		if ($this->work_item) {
			flash::add(flash::MSG_SUCCESS, "You have successfully deleted item {$this->itemID}");
		} else {
			echo "<span class=\"ok_highlight\">You have successfully deleted item {$this->itemID}</span><br />\n";
		}

		if ($this->cache_update)
			rotate_index($this->cache_update);

		if (isset($_GET["ref"]))
			system::moved($_GET["ref"]);

		//after deleting item try to display the same category in that city
		if ($ctx->international) {
			global $system; if( !is_object($system) ) $system = new system;

			$place_link = dir::getPlaceLinkAux($ctx->country, $this->row['loc_id'], $this->row['place_type_id'], $this->row['place_id'], $this->row['name']);
			$last_slash = strrpos($place_link, "/");
			$second_slash = strrpos($place_link, "/", ((strlen($place_link) - $last_slash + 1) * -1));
			$redirect_path = substr($place_link, 0, $second_slash);
			$system->moved($redirect_path);
		}
		
		//legacy behavior
		$this->itemID = NULL;
		$this->row = array();
		$this->action = "add";

		return true;
	}

	function checkIfOwner() {
		global $account;
		if (!$this->itemID) {
			return false;
		}
		if ($account->isrealadmin()) {
			return true;
		}
		$account_id = $account->isloggedin();
		if (empty($this->row["owner"]) || $account_id != $this->row["owner"]) {
			return false;
		}

		return true;
	}

	function checkUpdateLocationCity($loc_id) {
		$res = $this->db->q("select * from location_location where loc_type = 3 AND has_place_or_ad = 1 AND loc_id = ?", array($loc_id));
		if ($this->db->numrows($res))
			return;
		$location = new location($this->db);
		$location->createcitiesindb();
		return;
	}

	function _SaveItem() {
		global $ctx;

		for ($i = 0; $i < count($this->columns); $i++) {
			if (!$this->columns[$i]->_BeforeSave()) // _BeforeSave() is internal method, shouldn't be overloaded by user !
				return false;
			if (!$this->columns[$i]->BeforeSave())  // BeforeSave() can be overloaded by user
				return false;
		}

		//check mandatory fields
		$mand_array = array();
		$mand_failed = false;
		foreach ($this->columns as $column) {
			if ($column->colMandatory && (strlen(trim($column->colValue)) == 0) && $column->colType != COL_BOOLEAN) {
				if( $column->colType == COL_LOCATION && isset($_POST["Country"]) && !in_array($_POST["Country"], array(16046, 16047)) ) {
					$_POST["City"] = $_POST["State"];
					continue;
				} else if (($column->colType == COL_PHONE || $column->colType == COL_PHONE_2) && $column->skip_unique)
					continue;
				$mand_failed = true;
				$mand_array[] = $column->colTitle;
			}
		}
		if ($mand_failed) {
			if (count($mand_array) == 1) {
				echo "<div class=\"error\">Field '".htmlspecialchars($mand_array[0])."' is mandatory !</div><br />\n";
			} else {
				echo "<div class=\"error\">Fields ";
				foreach ($mand_array as $key => $mand_field) {
					if ($key != 0)
						echo ", ";
					echo "'".htmlspecialchars($mand_field)."' ";
				}
				echo " are mandatory !</div><br />\n";
			}
			if( $this->itemID ) {
				$this->hidden("id", $this->itemID);
				$this->addForm("ID", $this->itemID);
			}
			return false;
		}

		//make sure phone number is unique
		foreach ($this->columns as $column) {
			if ($column->colType != COL_PHONE_2)
				continue;
			if (!$column->unique)
				continue;
			if (isset($_POST["phonesu".$column->colName]))
				continue;
			if (($this->action == "add" && $column->colInsert) || ($this->action == "edit" && $column->colUpdate)) {
				$sql_place = "
							SELECT p.place_id 
							FROM place p 
							WHERE p.deleted IS NULL AND p.{$column->colName} = ?";
				if ($this->action == "add") {
					$res = $this->db->q($sql_place, [$column->GetQuotedValue()]);
				} else {
					if ($this->action == "edit") {
						$res = $this->db->q($sql_place.' AND p.place_id != ?', [$column->GetQuotedValue(), $this->itemID]);
					}
				}
				if ($this->db->numrows($res) ) {
					$rox = $this->db->r($res);
					echo "<div class=\"error\">This phone number '".$column->GetQuotedValue()."' is in use. You may not add 
						this phone number again.! <a href='?id={$rox[0]}' target='_blank'>Other place with the same phone number</a></div><br />\n"; 
					if( $this->itemID ) {
						$this->hidden("id", $this->itemID);
						$this->addForm("ID", $this->itemID);
					}
					return false; 
				}
			}
		}

		if (isset($_REQUEST["SaveAddNew"])) {
			$this->save_and_add_new = true;
		}

		if( $this->action == "add" ) {
			$key_lat    = false;
			$key_long   = false;
			$geo_coords = false;
			foreach($this->columns as $key=>$column) {
				if($column->colName === 'loc_lat') {
					$key_lat = $key;
				}
				if($column->colName === 'loc_long') {
					$key_long = $key;
				}
				if(in_array($column->colType, [COL_IMAGE, COL_PICTURE])) {
					$geo_coords = $column->readGeoLocation();
				}
				if($key_lat !== false && $key_long !== false && $geo_coords !== false) {
					$this->columns[$key_lat]->colValue = $geo_coords['lat'];
					$this->columns[$key_long]->colValue = $geo_coords['lng'];
					break;
				}
			}
			$insert_ = NULL;
			$insert_attrs = NULL;
			$first = true;
			$loc_id = NULL;
			foreach($this->columns as $column) {
				if (!$column->colInsert)
					continue;

				//TODO unified handler
				//if (!is_null($ctx->country) && dir::isCountryUnified() && !dir::isBaseColumn($column->colName) && $column->colName != "City") {
				if ($ctx->international && dir::isCountryUnified() && !dir::isBaseColumn($column->colName) && $column->colName != "City") {
					//TODO little bit hack with City column, clean
					//_d("column {$column->colName} is additional");
					$column->insert($insert_attrs);
					continue;
				}

				$column->insert($insert_);
		
				if ($column->colName == "City")
					$loc_id = intval($column->colValue);
			}

			//newly added places submitted by users have edit=0 (newly_added)
			if ($this->visitor_mode && !in_array("edit", $insert_["colName"])) {
				$insert_["colName"][] = "edit";
				$insert_["colValue"][] = 0;
			}

			//construct insert sql nad run it
			$sql = $col_names = $col_values = ""; $params = [];
			for ($i = 0; $i < count($insert_["colName"]); $i++) {
				$col_names .= (empty($col_names)) ? "" : ", ";
				$col_names .= "`".$insert_["colName"][$i]."`";
			}
			for ($i = 0; $i < count($insert_["colValue"]); $i++) {
				$col_values .= (empty($col_values)) ? "" : ", ";
				if (substr($insert_["colValue"][$i], 0, 8) == "(select "
					|| strtoupper($insert_["colValue"][$i]) == "NULL"
					) {
					$col_values .= " ".$insert_["colValue"][$i];
				} else {
					$col_values .= " ? ";
					$params[] = $insert_["colValue"][$i];
				}
			}
			$sql = "INSERT INTO {$this->table} ( {$col_names} ) VALUES ( {$col_values} )";
			$this->db->q($sql, $params);
			$insert_id = $this->db->insertid;
	
			//mark down we need to update sphinx indexes
			if ($this->cache_update)
				rotate_index($this->cache_update);

			//_d("loc_id = {$loc_id}'");
			$this->checkUpdateLocationCity($loc_id);

			if( $insert_id ) {
				//store attributes
				for ($i=0; $i<count($insert_attrs["colValue"]); $i++) {
					dir::storeAttributeByName($insert_id, $insert_attrs["colName"][$i], $insert_attrs["colValue"][$i]);
				}

				$this->itemID = $insert_id;
				if( !$this->visitor_mode ) {
					echo "<span class=\"ok_highlight\">You have successfully added item with the ID number: {$this->itemID}</span><br />\n";
					$redirectToItem = true;
				} else if( !$this->visitor_mode && !$this->save_and_add_new ) {
					$system = new system; $system->moved($_SERVER["REQUEST_URI"]."?id=".$this->itemID);
				}
			} else {
				echo "<span class=\"ok_highlight\">Error occured! Could not save the form!</span><br />\n";
				return false;
			}

		} else if( $this->action == "edit" ) {
			$update = "";
			$first = true;
			$loc_id = NULL;
//			_darr($this->columns);
			foreach($this->columns as $column) {
				if( !$column->colUpdate ) continue;
				$colu = $column->update();
				if( $column->colType == COL_ID ) { $id = $column->colName; continue; }
				if (empty($colu))
					continue;

				//TODO unified handler
				//if (!is_null($ctx->country) && dir::isCountryUnified() && $column->colName != "City" && !dir::isBaseColumn($column->colName)) {
				if ($ctx->international && dir::isCountryUnified() && $column->colName != "City" && !dir::isBaseColumn($column->colName)) {
//					_d("column {$column->colName} is additional");
					$in = NULL;
					$column->insert($in);
					for($i=0; $i<count($in["colName"]); $i++) {
//						_d("column '{$in["colName"][$i]}' '{$in["colValue"][$i]}'");
						if (dir::storeAttributeByName($this->itemID, $in["colName"][$i], $in["colValue"][$i]) === false) {
							echo "<span class=\"ok_highlight\">Error: can't update column '{$in["colName"][$i]}' with value '{$in["colValue"][$i]}'.</span><br />\n";
						}
					}
					continue;
				}

				if( $first ) {
					$update .= "update {$this->table} set $colu";
					$first = false;
				} else {
					$update .= ", ".$colu;
				}
				
				if ($column->colName == "City")
					$loc_id = intval($column->colValue);
			} 
			if ($this->table == "place") {
				$update .= ", updated = UNIX_TIMESTAMP()";
			}
			$update .= " where $id = '$this->itemID'";
			$this->db->q($update);
			if( $this->owner_mode ) echo "<span class=\"ok_highlight\">You have successfully updated your business information.</span><br />\n";
			else {
				echo "<span class=\"ok_highlight\">You have successfully edited item ID {$this->itemID}.</span><br />\n";
			}
			
			//mark down we need to update sphinx indexes
			if ($this->cache_update)
				rotate_index($this->cache_update);
			
			//_d("loc_id = {$loc_id}'");
			$this->checkUpdateLocationCity($loc_id);
			
			if( isset($_GET["ref"]) ) {
				global $system;
				if (!is_object($system))
					$system = new system;
				$system->moved($_GET["ref"]);
			}

			$this->createItemCol();
		}

		foreach ($this->columns as $column) {
			if (!$column->_AfterSave()) return false;
			if (!$column->AfterSave()) return false;
		}

		if( $this->visitor_mode ) {
			global $system;
			if( !is_object($system) ) $system = new system;
			$url = $this->visitor_url;
			if( isset($_POST["State"]) ) $url .= "?core_state_id=".$_POST["State"];

			//TODO update when directory module set for domestic modules
			global $account, $ctx;
			$edit_link = "https://adultsearch.com/worker/{$ctx->country}?id={$this->itemID}";
			$u = "<a href='{$edit_link}'>{$edit_link}</a>";

			$subject = "New Item Added";

			if( isset($_GET["owner_candidate"]) || isset($_GET["ref"]) ) {
				$subject .= " - Only YOU";
			}
			if( $this->notify_admin ) { 
				sendEmail(WEB_EMAIL, $subject, $u, SUPPORT_EMAIL);
			}	
			if( isset($_GET["ref"]) ) {
				global $system; if( !is_object($system) ) $system = new system;
				$system->moved($_GET["ref"]);
			} else if( isset($_GET["owner_candidate"]) ) {
				global $system; if( !is_object($system) ) $system = new system;
				$system->moved($_GET["owner_candidate"].$this->itemID);
			}
			$system->go($url, "Thank you for submitting the place, It will be live soon");
		}

		$this->action = "edit";
		if ($this->save_and_add_new) {
			$this->action = "add";
			foreach ($this->columns as $column) {
				$column->colValue = NULL;
			}

			if( $this->save_and_add_new ) {
				$url = preg_replace("/(\?|&)id=\d+/i", "", $_SERVER["REQUEST_URI"]);
				$system = new system; $system->moved($url);
			}

			$this->itemID = NULL;

		} else {
			$this->hidden("id", $this->itemID);
			if( $redirectToItem ) {	
				$system = new system; $system->moved($_SERVER["REQUEST_URI"]."?id=".$this->itemID);
			}
		}

		return true;
	}

	function Browsing() {
		$letter = GetGetParam("browse");
		$filename = GetGetParam("filename");
		$res = $this->db->q("select id, $this->browse_item from {$this->table} where $this->browse_item like '$letter%'");
		while($row=$this->db->r($res)) {
			echo "<a href='/{$filename}?id={$row[0]}'>{$row[1]}</a>";
		}
	}

	function loadRow() {
		global $ctx;

		//TODO legacy handler
		// now data for all countries are unified
//		if (!$ctx->international) {
//			$res = $this->db->q("select * from {$this->table} where {$this->columns[0]->colName} = '{$this->itemID}'");
//			if( $this->db->numrows($res) ) {
//				$this->row = $this->db->r($res);
//			}
//		} else {
			$ctx->node_id = $this->itemID;
//			_d("loading unified row");
			$this->row = dir::loadNode();
//		}
	}

	function ShowPage() {
		global $ctx;

		if( !$this->Check() ) return;

		if (!$this->ParseRequest()) return;

		//jay
		if ($this->work_item) {
			$next_item_edit_url = "";
			if ($this->work_action == "skip" || $this->work_action == "reassign_maarten" || $this->work_action == "reassign_rachel")
				$next_item_edit_url = worker::getNextItemEditUrl($this->work_item);

			//TODO
			if ($this->work_action == "reassign_maarten")
				worker::reassignMaarten($this->work_item);
			else if ($this->work_action == "reassign_rachel")
				worker::reassignRachel($this->work_item);

			if ($this->work_action == "skip" || $this->work_action == "reassign_maarten" || $this->work_action == "reassign_rachel") {
				if ($next_item_edit_url)
					return system::go($next_item_edit_url);
				else
					return system::go("/mng/work_items", "You dont have any more work items assigned");
			}
		}

		if( $this->owner_mode ) {
			if( !$this->checkIfOwner() ) {
				$system = new system; $system->moved("/");
				die;
			}
		} elseif( !$this->visitor_mode ) {
			global $account;

			if( !($account_id = $account->isloggedin()) ) {
				$account->asklogin();
				return;
			}

			if( !$account->isadmin() ) {
				$this->check_permission = true;
				$cat = $_SERVER['REQUEST_URI'];
				if( strstr($cat, '?') ) $cat = substr($cat, 0, strpos($cat, '?'));
				$res = $this->db->q(
					"select account_id from account_permission_cat where account_id = ? and cat like ? limit 1",
					[$account_id, $cat]
					);
				if( !$this->db->numrows($res) ) {

					//TODO
					if (strpos($cat, "/worker/emp") !== false) {
						if (!permission::has("edit_all_places")) {
							echo "You do not have permission to display this page.";
							reportAdmin("class.data.php - access denied", "cat='{$cat}'");
							return;
						}
					} else if (strpos($cat, "/worker/adultstore") !== false) {
						if (!permission::has("edit_all_places")) {
							echo "You do not have permission to display this page.";
							reportAdmin("class.data.php - access denied", "cat='{$cat}'");
							return;
						}
					} else if (strpos($cat, "/worker/gaybath") !== false) {
						if (!permission::has("edit_all_places")) {
							echo "You do not have permission to display this page.";
							reportAdmin("class.data.php - access denied", "cat='{$cat}'");
							return;
						}
					} else if (!permission::has("edit_all_places")) {
						echo "You do not have permission to display this page.";
						reportAdmin("class.data.php - access denied", "cat='{$cat}'");
						return;
					}
				}
			}
		}

		if ((($this->action == "add") || ($this->action == "edit")) && $this->saving == true ) {
			if( $this->check_permission ) {
				$p_loc_id = isset($_POST['City']) ? intval($_POST['City']) : (isset($_POST['loc_id']) ? intval($_POST['loc_id']) : 0);
				$p_loc_ids = array();
				while($p_loc_id>0) {
					$res = $this->db->q("select loc_parent, loc_id from location_location where loc_id = '$p_loc_id'");
					$row = $this->db->r($res);
					$p_loc_id = $row[0];
					$p_loc_ids[] = $row[1];
					if( $p_loc_id == 0 ) break;
				}
				if (count($p_loc_ids) > 0) {
					$res = $this->db->q("select account_id from account_permission_loc where account_id = '$account_id' and loc_id in (".implode(',', $p_loc_ids).") limit 1");
					if( !$this->db->numrows($res) ) {
						//TODO
						if (strpos($cat, "/worker/emp") !== false) {
							if (!permission::has("edit_all_places")) {
								echo "You do not have permission to display this page.";
								reportAdmin("class.data.php - access denied", "cat='{$cat}'");
								return;
							}
						} else if (strpos($cat, "/worker/adultstore") !== false) {
							if (!permission::has("edit_all_places")) {
								echo "You do not have permission to display this page.";
								reportAdmin("class.data.php - access denied", "cat='{$cat}'");
								return;
							}
						} else if (strpos($cat, "/worker/gaybath") !== false) {
							if (!permission::has("edit_all_places")) {
								echo "You do not have permission to display this page.";
								reportAdmin("class.data.php - access denied", "cat='{$cat}'");
								return;
							}
						} else if (!permission::has("edit_all_places")) {
							echo "You do not have permission to display this page.";
							reportAdmin("class.data.php - access denied", "cat='{$cat}'");
							return;
						}
					}
				}
			}
			$this->_SaveItem();
			if ($this->work_item && $this->work_action == "save_completed") {
				worker::markCompleted($this->work_item);
				$next_item_edit_url = worker::getNextItemEditUrl($this->work_item);
                if ($next_item_edit_url)
                    return system::go($next_item_edit_url);
                else
                    return system::go("/mng/work_items", "You dont have any more work items assigned");
			}
		}
	
		if( $this->browsing ) {
			$this->Browsing();
		}

		//load place
		//TODO this is duplicate (already loaded in ParseRequest()) - always ????
//		_d("loading row");
//		_darr($this->row);
		$ctx->item = NULL;
		$this->loadRow();	//(loads info about newly added place also)
//		_darr($this->row);

		//delete item
		if ($this->action == "del") {

			//jay
			$next_item_edit_url = "";
			if ($this->work_item && $this->work_action == "del_completed") {
				worker::markCompleted($this->work_item);
				$next_item_edit_url = worker::getNextItemEditUrl($this->work_item);
				unset($_GET["ref"]);
			}

			if ($this->check_permission) {
				$p_loc_ids = array();
				if ($this->row['loc_id']) $p_loc_ids[] = $this->row['loc_id'];
				if ($this->row['state_id']) $p_loc_ids[] = $this->row['state_id'];
				if ($this->row['country_id']) $p_loc_ids[] = $this->row['country_id'];

				$fail = true;
				if (!empty($p_loc_ids)) {
					$res = $this->db->q("select account_id from account_permission_loc where account_id = '$account_id' and loc_id in (".implode(',', $p_loc_ids).") limit 1");
					if( $this->db->numrows($res) ) {
						$fail = false;
					}
				}
				if( $fail ) {
					
					//TODO
					if (strpos($cat, "/worker/emp") !== false) {
						if (!permission::has("edit_all_places")) {
							echo "You do not have permission to remove content for this location.";
							reportAdmin("class.data.php - access denied", "cat='{$cat}'");
							return;
						}
					} else if (strpos($cat, "/worker/adultstore") !== false) {
						if (!permission::has("edit_all_places")) {
							echo "You do not have permission to remove content for this location.";
							reportAdmin("class.data.php - access denied", "cat='{$cat}'");
							return;
						}
					} else if (strpos($cat, "/worker/gaybath") !== false) {
						if (!permission::has("edit_all_places")) {
							echo "You do not have permission to remove content for this location.";
							reportAdmin("class.data.php - access denied", "cat='{$cat}'");
							return;
						}
					} else if (!permission::has("edit_all_places")) {
						echo "You do not have permission to remove content for this location.";
						reportAdmin("class.data.php - access denied", "cat='{$cat}'");
						return;
					}
				}
			}
			$this->_DeleteItem();

			if ($next_item_edit_url)
				return system::go($next_item_edit_url);

			if( $this->die_after_delete ) return;
		}

		if( $this->itemID && $this->show_remove ) $this->smarty->assign("show_remove", true);

		foreach($this->columns as $col) {
			$col->DrawEditFieldCaption();
			$col->DrawEditField();
			if( $col->colType == COL_HOUR ) {
				$this->smarty->assign("opens", $col->opens);
				$this->smarty->assign("closes", $col->closes);
				$this->smarty->assign("closed", $col->closed);
			} elseif(  $col->colType == COL_HH ) {
				$this->smarty->assign("hh", $col->hh);
			} elseif(  $col->colType == COL_LOCATIONAJAX ) {
				$this->smarty->assign("loc_ajax", true);
			}
		}

		$this->smarty->assign("international", $ctx->international ? true : false);
		$this->smarty->assign("form", $this->form);
		$this->smarty->assign("form_javascript", $this->javascript);
		$this->smarty->assign("form_hidden", $this->hidden);
		$this->smarty->assign("data_action", $this->action);
		$this->smarty->assign("visitor_mode", $this->visitor_mode);

		//jay
		$this->smarty->assign("deleted", $this->row["deleted"]);

		if( $this->browse ) {
			$letters = array();
			for($i=48;$i<58;$i++) {
				$letters[] = chr($i);
			}
			for($i=65;$i<91;$i++) {
				$letters[] = chr($i);
			}
			$this->smarty->assign("letters", $letters);
			$this->smarty->assign("browse", true);
		}

		if( $this->owner_mode ) {
			$this->smarty->assign("owner_mode", true);
			$this->smarty->assign("owner_link", $this->owner_link);
			$this->smarty->assign("id", $this->itemID);
		}
		$this->smarty->assign("cbutton", $this->cbutton);
		$this->smarty->assign("save_button_value", $this->save_button_value);

		//jay
		//TODO redo this isWorkItem -> getWorkItem so it returns object
		if ($wi_id = worker::isWorkItem($this->table, $this->itemID)) {
			$this->smarty->assign("work_item", $wi_id);
			//TODO redo so it is only one function
			$reassign_name = worker::getReassignName($wi_id);
			$reassign_label = worker::getReassignLabel($wi_id);
			if ($reassign_name && $reassign_label) {
				$this->smarty->assign("reassign_name", $reassign_name);
				$this->smarty->assign("reassign_label", $reassign_label);
			}
		}

		$this->smarty->display(_CMS_ABS_PATH."/templates/worker/worker_worker.tpl");

		if( isset($_GET['ajaxdie']) ) die;

		if( isset($_GET['lat']) ) {
			$this->mapUpdate();
		}
	}

} //class Form


