<?php

class db {

	var $res;
	var $start_t;
	var $insertid;
	var $affected;
	var $error = NULL;
	var $error_msg;
	var $numrows = NULL;

	var $logger_enabled = true;
	var $logger_time_start;
	var $logger_time_end;
	var $logger_time_limit = 20;
	var $total_sql = 0;
	var $total_time = 0;
	var $sqls = array();

	var $connected = false;
	var $charset = NULL;

	var $pdo = NULL;

	public function __construct($charset = NULL) {
		$this->charset = $charset;
		$this->connect();
	}

	function connect($host = NULL, $name = NULL, $user = NULL, $pass = NULL) {
		global $config_db_host, $config_db_name, $config_db_user, $config_db_pass;

		$host = (is_null($host)) ? $config_db_host : $host;
		$name = (is_null($name)) ? $config_db_name : $name;
		$user = (is_null($user)) ? $config_db_user : $user;
		$pass = (is_null($pass)) ? $config_db_pass : $pass;

		//PDO
		$dsn = "mysql:host={$host};dbname={$name}";
		if (!is_null($this->charset)) {
			$dsn .= ";charset={$this->charset}";
		} else {
			$dsn .= ";charset=latin1";
		}
		$opt = array(
//			PDO::ATTR_ERRMODE			=> PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_ERRMODE			=> PDO::ERRMODE_SILENT,
//			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_BOTH,
			PDO::ATTR_PERSISTENT => true
		);
		try {
			$this->pdo = new PDO($dsn, $user, $pass, $opt);
		} catch (PDOException $e) {
			$this->dblog("Connect Error: ".$e->getMessage());
			die("We are sorry, but the website is under maintenance. Please come back later. (DB Error)");
		}

		list($usec, $sec) = explode(" ",microtime());
		$this->start_t = ((float)$usec + (float)$sec);
		$this->connected = true;
	}

	function exeTime() {
		list($usec, $sec) = explode(" ",microtime());
		$this->stop_t = ((float)$usec + (float)$sec);

		return number_format(($this->stop_t-$this->start_t),2);
	}

	/**
	 *
	 * @global bool $config_dev_server
	 * @param string $sql
	 * @param array $params
	 * @return \PDOStatement
	 */
	function q($sql, $params = []) {
		global $config_dev_server;

		$result = false;

		if (!$this->connected) {
			$this->connect();
			$this->connected = true;
		}

		$this->total_sql++;
		$this->sql = $sql;
		$this->error = false;

		$this->timerStart();

		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// Expander of "IN ?" where condition
		// currently works only for one IN condition
		if (($pos = strpos(strtolower($sql), " in ?")) !== false) {
			$arr_index = $arr = null;
			foreach ($params as $key => $val) {
				if (!is_array($val))
					continue;
				$arr_index = $key;
				$arr = $val;
				break;
			}
			if (!is_null($arr_index)) {
				$questionmarks = str_repeat('?,', count($arr) - 1) . '?';
				$sql = substr($sql, 0, $pos)." IN ({$questionmarks}) ".substr($sql, $pos + 5);
				array_splice($params, $arr_index, 1, $arr);
			}
		}
		
		$stm = $this->pdo->prepare($sql);
		$stm->execute($params);

		if ($stm->errorCode() != "00000") {
			$this->error = true;
			$info = $stm->errorInfo();
			$this->error_msg = $info[2]." (SQL={$sql})";
			$this->dblog($info[2], $sql, $params);
			if ($config_dev_server)
				echo "<strong>DB Error</strong>: {$this->error_msg}<br />\n";
		}
		$result = $stm;
		
		$this->insertid = $this->pdo->lastInsertId();
		$this->affected = $stm->rowCount();
 
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// Due to backward compatibility with legacy code
		// If we call PDO version of this function (with param array) AND there is SQL_CALC_FOUND_ROWS inside sql statement, 
		//		rowcount is taken from $stm->rowCount() (which is not guaranteed for all databases !)
		//		And SELECT FOUND ROWS is not called, because it is called from controller (if its called twice, it would not return anything second time)
		if (strpos($sql, "SQL_CALC_FOUND_ROWS") !== false) {
			$this->numrows = $stm->rowCount();
		} else {
			$ob = $this->pdo->query("SELECT FOUND_ROWS()");
			if (is_object($ob))
				$this->numrows = $ob->fetchColumn();
			else
				$this->numrows = 0;
		}
		
		$totaltime = $this->getTimerElapsed();
		$this->total_time += $totaltime;
		$this->sqls[] = array("sql" => $sql, "time"=>$totaltime);

		return $result;
	} 

	function error() {
		return $this->error;
	}


	function r($res = NULL, $type = MYSQL_BOTH) {
		if ($res == NULL)
			if (!$this->res)
				return NULL;
			else
				$res = $this->res;

		if (is_object($res) && $res instanceof \PDOStatement) {
			if ($type == MYSQL_NUM)
				$type = PDO::FETCH_NUM;
			elseif ($type == MYSQL_ASSOC)
				$type = PDO::FETCH_ASSOC;
			else
				$type = PDO::FETCH_BOTH;
			return $res->fetch($type);
		} else {
			return mysql_fetch_array($res, $type);
		}
	}

	function o($res = NULL) {
		if ($res)
			return mysql_fetch_object($res);
		if (!$this->res)
			return NULL;
		return mysql_fetch_object($this->res);
	}

	function single($sql, $params = null) {
		$res = $this->q($sql, $params);
		if ($this->numrows($res) != 1)
			return false;
		$row = $this->r($res);
		return array_shift($row);
	}

	function numrows($res = NULL) {
		if ($res == NULL)
			if (!$this->res)
				return NULL;
			else
				$res = $this->res;
		if (is_object($res) && $res instanceof \PDOStatement) {
			return $this->numrows;
		} else {
			return mysql_num_rows($res);
		}
	}
	
	function affected() {
		return $this->affected;
	}

	function insertid() {
		return $this->insertid;
	}


	/* 
	 * Lastest paging system 
	 * Use $gFilename and $page pre-defined parameters for 100% cms compatible
	 * style to add it css class options default
	 * add_prevnext_text to show prev next links
	 * query to add $_GET parameter to the links
	 */
	function paging($total_item, $item_per_page, $style = false, $add_prevnext_text = TRUE, $query = NULL, $ajax = NULL) {
		global $gFilename, $page, $gModule, $gLocation, $mobile, $account, $ctx;

		if( $mobile == 1 ) {
			$style = false;
			$add_prevnext_text = TRUE;
		}

		if( in_array($gFilename, array("index", "home")) )
			$gFilename = "";

		$myLink = "";
		if( !empty($gModule) )
			$myLink =  "/$gModule/";
		if( !empty($gLocation) )
			$myLink .= "$gLocation/";

		$myLink .= $gFilename;

		if( $account->core_loc_id && $account->core_loc_type > 1 ) {
			$myLink = $account->core_loc_array['loc_url'] . $myLink;
			$myLink = str_replace('//', '/', $myLink);
		}

		if ($ctx->international) {
			$myLink = $ctx->path_no_page."/";
		}

		$total_pages = ceil($total_item/$item_per_page);
		$on_page = $page;
		$per_page = $item_per_page;

		$max_num_pages_in_pager = 5;
		if ($max_num_pages_in_pager >= $total_pages) {
			$start_page = 1;
			$end_page = $total_pages;
		} else if ($page < (ceil($max_num_pages_in_pager/2))) {
			$start_page = 1;
			$end_page = $max_num_pages_in_pager;
		} else if ($page > ($total_pages - ceil($max_num_pages_in_pager/2))) {
			$start_page = $total_pages - $max_num_pages_in_pager;
			$end_page = $total_pages;
		} else {
			$start_page = $page - ceil($max_num_pages_in_pager/2);
			$end_page = $start_page + $max_num_pages_in_pager;
			if( $start_page == 0 ) $start_page = 1;
		}

		$pager = "";
		if ($page > 1 && $add_prevnext_text) {
			$prev = $page-1;
			$ajax_link = is_array($ajax) ? "onclick=\"_ajax('{$ajax["method"]}', '{$ajax["url"]}', '{$ajax["data"]}&page={$prev}', '{$ajax["div"]}');return false;\"" : (!empty($ajax)?$ajax:"");
			if( !empty($ajax_link) ) $ajax_link = str_replace("{page}", $prev, $ajax_link);
			if( $prev == 1 )
				$queryl = !empty($query) ? $query : NULL;
			else
				$queryl = !empty($query) ? ($query . "&page=$prev") : "?page=$prev";
			if( empty($myLink) && empty($queryl) ) $myLink = '/';
			if( $style )
				$pager .= "<li class=\"prev-page\"><a href=\"{$myLink}{$queryl}\" $ajax_link><span>Prev</span></a></li>";
			elseif($mobile==1)
				$pager .= "<a href=\"{$myLink}{$queryl}\" $ajax_link><img src=\"/images/mobile/pager-left.png\" align=\"left\" width=\"40\" height=\"33\" border=\"0\" /></a>";
			else
				$pager .= "<a href=\"{$myLink}{$queryl}\" $ajax_link>Prev</a> ";
		}
		
		if ($start_page > 1) {
			$queryl = !empty($query) ? ($query . "&page=1") : "?page=1";
			$queryl = !empty($query) ? $query : NULL;
			$ajax_link = is_array($ajax) ? "onclick=\"_ajax('{$ajax["method"]}', '{$ajax["url"]}', '{$ajax["data"]}&page=1', '{$ajax["div"]}');return false;\"" : (!empty($ajax)?$ajax:"");
			if( !empty($ajax_link) ) $ajax_link = str_replace("{page}", 1, $ajax_link);
			if( empty($myLink) && empty($queryl) ) $myLink = '/';
			if( $style )
				$pager .= "<li><a href=\"{$myLink}{$queryl}\" $ajax_link>1</a></li>";
			else
				$pager .= "<a href=\"{$myLink}{$queryl}\" $ajax_link>1</a>";
			if ($start_page > 2) {
				if( $style )
					$pager .= "<li>...</li>";
				else
					$pager .= " ... ";
			}
		}

		for ($i = $start_page; $i <= $end_page; $i++) {
			if ($i == $page) {
				if( $style )
					$pager .= "<li><a class=\"sel\">$i</a></li>";
				else
					$pager .= "<b>$i</b>" . ($i<$end_page ? ", " : "");
			} else {
				$ajax_link = is_array($ajax) ? "onclick=\"_ajax('{$ajax["method"]}', '{$ajax["url"]}', '{$ajax["data"]}&page={$i}', '{$ajax["div"]}');return false;\"" : (!empty($ajax)?$ajax:"");
				if( !empty($ajax_link) ) $ajax_link = str_replace("{page}", $i, $ajax_link);
				if( $i == 1 ) $queryl = !empty($query) ? $query : NULL;
				else $queryl = !empty($query) ? ($query . "&page=$i") : "?page=$i";
				if( empty($myLink) && empty($queryl) ) $myLink = '/';
				if( $style )
					$pager .= "<li><a href=\"{$myLink}{$queryl}\" $ajax_link>".$i."</a></li>";
				else
					$pager .= "<a href=\"{$myLink}{$queryl}\" $ajax_link>$i</a>, ";
			}
		}

		if ($end_page < $total_pages) {
			if ($end_page < ($total_pages - 1)) {
				if( $style ) $pager .= "<li>...</li>"; else $pager .= " ... ";
			}
			$ajax_link = is_array($ajax) ? "onclick=\"_ajax('{$ajax["method"]}', '{$ajax["url"]}', '{$ajax["data"]}&page={$total_pages}', '{$ajax["div"]}');return false;\"" : (!empty($ajax)?$ajax:"");
			if( !empty($ajax_link) ) $ajax_link = str_replace("{page}", $total_pages, $ajax_link);
			$queryl = !empty($query) ? ($query . "&page=$total_pages") : "?page=$total_pages";
			if( $style )
				$pager .= "<li><a href=\"{$myLink}{$queryl}\" $ajax_link>{$total_pages}</a></li>";
			else
				$pager .= "<a href=\"{$myLink}{$queryl}\" $ajax_link>{$total_pages}</a> ";			
		}

		if ($page < $total_pages && $add_prevnext_text) {
			$next = $page+1;
			$ajax_link = is_array($ajax) ? "onclick=\"_ajax('{$ajax["method"]}', '{$ajax["url"]}', '{$ajax["data"]}&page={$next}', '{$ajax["div"]}');return false;\"" : (!empty($ajax)?$ajax:"");
			if( !empty($ajax_link) ) $ajax_link = str_replace("{page}", $next, $ajax_link);
			$queryl = !empty($query) ? ($query . "&page=$next") : "?page=$next";
			if( $style )
				$pager .= "<li class=\"next-page\"><a href=\"{$myLink}{$queryl}\" $ajax_link><span>Next</span></a></li>";
			elseif($mobile==1)
				$pager .= "<a href=\"{$myLink}{$queryl}\" $ajax_link><img src=\"/images/mobile/pager-right.png\" align=\"right\" width=\"40\" height=\"33\" border=\"0\" /></a>";
			else
				$pager .= " <a href=\"{$myLink}{$queryl}\" $ajax_link>Next</a>";
		}

		if( $style )
			$pager = "<ul class=\"paging\">" . $pager . "</ul>";

		return $pager;
	}

	/* this function is clear the array filled up by mysql data by using the global variable $page and $item_per_page value. 
	 * total number of results is assigned to $count.
	 */
	function arrayClear($array, $item_per_page, $count) {
		global $page;

		if( $item_per_page >= count($array) ) return $array;

		$begin = ($page * $item_per_page) - ($item_per_page - 1);
		while( $begin > 1 ) {
			array_shift($array);
			$begin--;
		}

		$end = $page * $item_per_page;
		while( $end < $count ) {
			$end++;
			array_pop($array);
		}

		return $array;
	}

	/*
	 * sort the array by the index $sort 
	 * if $sort is an array then it sorts the array by using all indexed in $sort.
	 * $way = 1 (bigger to smaller value)
	 * $way = 0 (smaller to bigger)
	 *
	 *  Usage; according to sorting way; $sort array values must be assigned 0 or >=1 (at least the first level). 
	 *  if all the $sort values >= 1, then we can't use multi-level sorting. 
	 *  its best for sorting by "review numbers, paid or not or values like that. so we know not all the values >= 1, so we can multi-level sort them
	 *  example for escort sorting; 
	 *   $sort = array("reviews", "paid", "viewed"). 
	 *	  first we show all the listing with reviews count>0, then listed the girls paying and then how many times their ads viewed. 
	 *   $sort = array("viewed", "anything"); 
	 *	now i assume all the listings has been viewed at least 1 times, so multi-level sorting is not working at this time. dont need to waste the cpu, just
	 *	use one-level sorting like $sort = "viewed";
	 *
	 *  august 19th 2009 - new option is added. if the value of the sort index is < 0 , then that item will be the last item in the returning array. - burak
	 * december 15th 2009 - $way now supports array for each individual item...
	 */
	function arraySort($array, $sort, $way = 1)	{
		global $usort_cmp_index;

		if( !is_array($way) ) $func = $way == 1 ? "usort2_cmp_key" : "usort_cmp_key";

		if( !is_array($sort) )
		{
			$usort_cmp_index = $sort;
			usort($array, $func);
		}
		else
		{
			$returnArray = array();
			$banned = array();
			$count = 0;
			foreach($sort as $s)
			{
				$usort_cmp_index = $s;
				if( is_array($way) ) { $func = $way[$count] == 0 ? "usort_cmp_key" : "usort2_cmp_key"; }
				usort($array, $func);
				$this->arrayClearIndex($array, $returnArray, $banned, $s, !is_array($way) ? $way : $way[$count]);
				$count++;
			}

			/* this are the last listing escorts */
			$returnArray[] = $array;

			$array = array();
			foreach($returnArray as $r) 
				foreach($r as $r1)
					$array[] = $r1;

			foreach($banned as $r) {
				foreach($r as $r1) {
					$array[] = $r1;
				}
			}

		}
		return $array;
	}

	function arrayClearIndex(&$array, &$returnArray, &$banned, $s, $way) {
		$indexed = array();
		$ban = array();
		foreach($array as $a) {
			if( ($way == 1 && $a[$s] > 0) || ($way == 0 && $a[$s] >= 0 ) ) {
				$indexed[] = $a;
				array_shift($array);
			} else if( $a[$s] < 0 ) { 
				$ban[] = $a;
				$this->arrayClearElement($array, key($a), $a[key($a)]);
			}
		}

		if( !empty($indexed) )
			$returnArray[] = $indexed;
		if( !empty($ban) )
			$banned[] = $ban;
	}

	function arrayClearElement(&$arr, $key, $value) {	
		foreach($arr as $k=>$a) {
			if( $a[$key] == $value )
				unset($arr[$k]);
		}
	}

	/* DEBUGGER FUNCTIONS */

	function getMicroTime() {
		return microtime(true);
		$array = explode(" ", microtime());
		return $array[1] + $array[0];
	}

	function timerStart() {
		if (!$this->logger_enabled)
			return false;
		$this->logger_time_start = $this->getMicroTime();
		return true;
	}

	function getTimerElapsed() {
		if (!$this->logger_enabled)
			return false;
		$this->logger_time_end = $this->getMicroTime();
		return ($this->logger_time_end - $this->logger_time_start);
		return round($this->logger_time_end - $this->logger_time_start, 4);
	}

	function dblog($buf, $sql = "", $params = array()) {
		$place = "";
		$bt = debug_backtrace();
		if ($bt[count($bt)-1]["function"] == "parse_php_file") {
			$file = $bt[count($bt)-1]["args"][0];
			if (strstr($bt[count($bt)-3]["file"], "eval()'d code"))
				$line = $bt[count($bt)-3]["line"];
			$place = " [ {$file}:{$line} ]";
		} else {
			$place = " [ {$bt[count($bt)-1]["file"]}:{$bt[count($bt)-1]["line"]} ]";
		}

		$buf = replaceIpInStr($buf);

		$f = fopen(_CMS_PHP_ERROR_LOG_FILE, "a");
		if ($f) {
			$time = "[". date("d-m-Y G:i:s T") ."] [". account::getUserIp() ."]{$place} $buf [$sql]";
			if (!empty($params)) {
				$par_string = "";
				foreach ($params as $par) {
					$par_str = (string) $par;
					if (strlen($par_str) > 50)
						$par_str = substr($par_str,0,50)."...";
					$par_string .= (empty($par_string)) ? "" : ",";
					$par_string .= "'{$par_str}'";
				}
				if (!empty($par_string))
					$time .= " [{$par_string}]";
			}
			fwrite($f, $time. "\n");
			fclose($f);
		}
		//echo $time;
		return true;
	}

	function makeTime($time) {
		$current = time();
		$diff = $current - $time;

		$ex = $diff > 0 ? " ago" : "";
		if( $diff < 0 ) $diff = abs($diff);

		if($diff < 60 )
			return "$diff seconds" . $ex;
		if($diff < 3600)
			return floor($diff/60) . " mins" . $ex;
		if($diff < 86400)
			return floor($diff/3600) . " hours " . floor(($diff % 3600) / 60) . " minutes". $ex;
		if($diff < 2592000)
			return floor($diff/(60*60*24)) . " days " . floor(($diff % (60*60*24)) / 3600) . " hours". $ex;
		if($diff < 31536000)
			return floor($diff/(60*60*24*30)) . " months " . floor(($diff % (60*60*24*30)) / (60*60*24)) . " days". $ex;
		else
			return floor($diff/31536000) . " year " . floor(($diff % (60*60*24*30)) / (60*60*24)) . " month " . floor($diff % (60*60*24) / 84600) . " day". $ex;
	}
}

//END
