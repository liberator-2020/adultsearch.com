<?php
/*
 * deal = advertise flat deal
 */
class deal {

	private $id = NULL,
			$type = NULL,
			$status = NULL,
			$account_id = NULL,
			$zone_id = NULL,
			$from_stamp = NULL,
			$impressions_deal = NULL,
			$impressions_left = NULL,
			$pops_deal = NULL,
			$pops_left = NULL,
			$to_stamp = NULL,
			$clad_sponsor_position = NULL,
			$clad_id = NULL,
			$paid = NULL,
			$created = NULL,
			$created_by = NULL,
			$description = NULL,
			$started_stamp = NULL,
			$finished_stamp = NULL,
			$next_deal_id = NULL;

	private $account = NULL;
	private $zone = NULL;
	private $zones = null;
	private $clad = NULL;

	function __construct() {
	}

	public static function findOneById($id) {
		global $db;

		$res = $db->q("SELECT * FROM advertise_flat_deal WHERE id = '".intval($id)."'");
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

		return self::withRow($row);
	}
	
	public static function withRow($row) {

		$deal = new deal();
		$deal->setId($row["id"]);
		$deal->setType($row["type"]);
		$deal->setStatus($row["status"]);
		$deal->setAccountId($row["account_id"]);
		$deal->setZoneId($row["zone_id"]);
		$deal->setFromStamp($row["from_stamp"]);
		$deal->setImpressionsDeal($row["impressions_deal"]);
		$deal->setImpressionsLeft($row["impressions_left"]);
		$deal->setPopsDeal($row["pops_deal"]);
		$deal->setPopsLeft($row["pops_left"]);
		$deal->setToStamp($row["to_stamp"]);
		$deal->setCladSponsorPosition($row["clad_sponsor_position"]);
		$deal->setCladId($row["clad_id"]);
		$deal->setPaid($row["paid"]);
		$deal->setCreated($row["created"]);
		$deal->setCreatedBy($row["created_by"]);
		$deal->setDescription($row["description"]);
		$deal->setStartedStamp($row["started_stamp"]);
		$deal->setFinishedStamp($row["finished_stamp"]);
		$deal->setNextDealId($row["next_deal_id"]);

		return $deal;
	}
	
	//getters and setters
	public function getId() {
		return $this->id;
	}
	public function getType() {
		return $this->type;
	}
	public function getStatus() {
		return $this->status;
	}
	public function getAccountId() {
		return $this->account_id;
	}
	public function getZoneId() {
		return $this->zone_id;
	}
	public function getFromStamp() {
		return $this->from_stamp;
	}
	public function getImpressionsDeal() {
		return $this->impressions_deal;
	}
	public function getImpressionsLeft() {
		return $this->impressions_left;
	}
	public function getPopsDeal() {
		return $this->pops_deal;
	}
	public function getPopsLeft() {
		return $this->pops_left;
	}
	public function getToStamp() {
		return $this->to_stamp;
	}
	public function getCladSponsorPosition() {
		return $this->clad_sponsor_position;
	}
	public function getCladId() {
		return $this->clad_id;
	}
	public function getPaid() {
		return $this->paid;
	}
	public function getCreated() {
		return $this->created;
	}
	public function getCreatedBy() {
		return $this->created_by;
	}
	public function getDescription() {
		return $this->description;
	}
	public function getStartedStamp() {
		return $this->started_stamp;
	}
	public function getFinishedStamp() {
		return $this->finished_stamp;
	}
	public function getNextDealId() {
		return $this->next_deal_id;
	}

	public function setId($id) {
		$this->id = $id;
	}
	public function setType($type) {
		$this->type = $type;
	}
	public function setStatus($status) {
		$this->status = $status;
	}
	public function setAccountId($account_id) {
		$this->account_id = $account_id;
	}
	public function setZoneId($zone_id) {
		$this->zone_id = $zone_id;
	}
	public function setFromStamp($from_stamp) {
		$this->from_stamp = $from_stamp;
	}
	public function setImpressionsDeal($impressions_deal) {
		$this->impressions_deal = $impressions_deal;
	}
	public function setImpressionsLeft($impressions_left) {
		$this->impressions_left = $impressions_left;
	}
	public function setPopsDeal($pops_deal) {
		$this->pops_deal = $pops_deal;
	}
	public function setPopsLeft($pops_left) {
		$this->pops_left = $pops_left;
	}
	public function setToStamp($to_stamp) {
		$this->to_stamp = $to_stamp;
	}
	public function setCladSponsorPosition($clad_sponsor_position) {
		$this->clad_sponsor_position = $clad_sponsor_position;
	}
	public function setCladId($clad_id) {
		$this->clad_id = $clad_id;
	}
	public function setPaid($paid) {
		$this->paid = $paid;
	}
	public function setCreated($created) {
		$this->created = $created;
	}
	public function setCreatedBy($created_by) {
		$this->created_by = $created_by;
	}
	public function setDescription($description) {
		$this->description = $description;
	}
	public function setStartedStamp($started_stamp) {
		$this->started_stamp = $started_stamp;
	}
	public function setFinishedStamp($finished_stamp) {
		$this->finished_stamp = $finished_stamp;
	}
	public function setNextDealId($next_deal_id) {
		$this->next_deal_id = $next_deal_id;
	}

	//relationships
	public function getAccount() {
		if ($this->account)
			return $this->account;
		$acc = account::findOneById($this->account_id);
		if (!$acc)
			return NULL;
		$this->account = $acc;
		return $this->account;
	}
	//TODO delete following one
	public function getZone() {
		if ($this->zone)
			return $this->zone;
		$zone = zone::findOneById($this->zone_id);
		if (!$zone)
			return NULL;
		$this->zone = $zone;
		return $this->zone;
	}	
	public function getZones() {
		global $db;

		if ($this->zones)
			return $this->zones;

		$zones = [];
		if ($this->zone_id) {
			$zone = zone::findOneById($this->zone_id);
			if (!$zone)
				return null;
			$zones[] = $zone;
		} else {
			$res = $db->q("SELECT z.* FROM advertise_deal_zone adz INNER JOIN advertise_section z on z.id = adz.zone_id WHERE adz.deal_id = ?", [$this->id]);
			while($row = $db->r($res)) {
				$zone = zone::withRow($row);
				if (!$zone)
					continue;
				$zones[] = $zone;
			}
		}
		$this->zones = $zones;
		return $this->zones;
	}	
	public function getClad() {
		if ($this->clad)
			return $this->clad;
		$clad = clad::findOneById($this->clad_id);
		if (!$clad)
			return NULL;
		$this->clad = $clad;
		return $this->clad;
	}	

	//CRUD functions
	function update($audit = false) {
		global $account, $db;

		if (!$this->id)
			return false;

		$original = deal::findOneById($this->id);
		if (!$original)
			return false;

		$update_fields = array();
		if ($this->type != $original->getType())
			$update_fields["type"] = $this->type;
		if ($this->status != $original->getStatus())
			$update_fields["status"] = $this->status;
		if ($this->account_id != $original->getAccountId())
			$update_fields["account_id"] = $this->account_id;
		if ($this->zone_id != $original->getZoneId())
			$update_fields["zone_id"] = $this->zone_id;
		if ($this->from_stamp != $original->getFromStamp())
			$update_fields["from_stamp"] = $this->from_stamp;
		if ($this->impressions_deal != $original->getImpressionsDeal())
			$update_fields["impressions_deal"] = $this->impressions_deal;
		if ($this->impressions_left != $original->getImpressionsLeft())
			$update_fields["impressions_left"] = $this->impressions_left;
		if ($this->pops_deal != $original->getPopsDeal())
			$update_fields["pops_deal"] = $this->pops_deal;
		if ($this->pops_left != $original->getPopsLeft())
			$update_fields["pops_left"] = $this->pops_left;
		if ($this->to_stamp != $original->getToStamp())
			$update_fields["to_stamp"] = $this->to_stamp;

		$update_clad_sponsor_position = false;
		$update_clad_id = false;
		if ($this->clad_sponsor_position != $original->getCladSponsorPosition()) {
			$update_fields["clad_sponsor_position"] = $this->clad_sponsor_position;
			$update_clad_sponsor_position = true;
		}
		if ($this->clad_id != $original->getCladId()) {
			$update_fields["clad_id"] = $this->clad_id;
			$update_clad_id = true;
		}

		if ($this->paid != $original->getPaid())
			$update_fields["paid"] = $this->paid;
		if ($this->created != $original->getCreated())
			$update_fields["created"] = $this->created;
		if ($this->created_by != $original->getCreatedBy())
			$update_fields["created_by"] = $this->created_by;
		if ($this->description != $original->getDescription())
			$update_fields["description"] = $this->description;
		if ($this->started_stamp != $original->getStartedStamp())
			$update_fields["started_stamp"] = $this->started_stamp;
		if ($this->finished_stamp != $original->getFinishedStamp())
			$update_fields["finished_stamp"] = $this->finished_stamp;
		if ($this->next_deal_id != $original->getNextDealId())
			$update_fields["next_deal_id"] = $this->next_deal_id;

		if (empty($update_fields))
			return true;	//nothing to update

		$changed_fields = "";
		$update = "";
		$update_arr = array();
		foreach ($update_fields as $key => $val) {
			$changed_fields .= (empty($changed_fields)) ? "" : ", ";
			$update .= (empty($update)) ? "SET " : ", ";
			$changed_fields .= "{$key}";
			if (method_exists($original, get_getter_name($key)))
				$changed_fields .= ": ".call_user_func(array($original, get_getter_name($key)))."->".call_user_func(array($this, get_getter_name($key)));
			$update .= "`{$key}` = ?";
			$update_arr[] = $val;
		}
		$update_arr[] = $this->id;
		$update = "UPDATE advertise_flat_deal {$update} WHERE id = ? LIMIT 1";

		$db->q($update, $update_arr);
		$aff = $db->affected();

		if (!$aff)
			return false;

		//updates depending on adverrtise_zone
		//TODO redo with zone::findOneBy()....
		//TODO solve problem with changing clad_id or even changing completely zone and unsponsoring old ad ???
		$zone_type = $zone_name = NULL;
		$res = $db->q("SELECT ads.type, ads.name FROM advertise_section ads WHERE ads.id = ?", array($this->zone_id));
		if ($db->numrows($res) == 1) {
			$row = $db->r($res);
			$zone_type = $row["type"];
			$zone_name = $row["name"];
		}
		if ($zone_type == "A") {
			if ($update_clad_sponsor_position || $update_clad_id) {
				$clad = $this->getClad();
				$clad_sponsor_desktop = $clad_sponsor_mobile = false;
				if (stripos($zone_name, "desktop") !== false)
					$clad_sponsor_desktop = true;
				if (stripos($zone_name, "mobile") !== false)
					$clad_sponsor_mobile = true;
				if (!$clad_sponsor_desktop && !$clad_sponsor_mobile) {
					reportAdmin("AS: deal::update: cant determine desktop or mobile zone for sponsor ad", "", 
						array("deal_id" => $this->id, "zone_id" => $this->zone_id, "clad_id" => $this->clad_id, "zone_name" => $zone_name));
				} else {
					if ($clad_sponsor_desktop)
						$clad->setSponsor(1);
					if ($clad_sponsor_mobile)
						$clad->setSponsorMobile(1);
					$clad->setSponsorPosition($this->clad_sponsor_position);
					$ret = $clad->update();
					if (!$ret) {
						reportAdmin("AS: deal::update: cant update clad for sponsor", "", 
							array("deal_id" => $this->id, "zone_id" => $this->zone_id, "clad_id" => $this->clad_id, "zone_name" => $zone_name));
					} else {
						//sponsor ad successfully updated, rotate index
						rotate_index("escorts");
					}
				}
			}
		}

		$advertise = new advertise();
		$advertise->flatDealStatusUpdate($this->id);

		//this is wrong - just because we update something about flat deal doesnt mean we need to unpause campaigns, which might have been paused by the advertiser !!
		//comment out for now
		//$advertise->flatDealCampaigns($this->id);

		if ($audit)
			audit::log("AFD", "Edit", $this->id, "Changed fields: {$changed_fields}", $account->getId());

		return true;
	}

	public function add() {
		global $db, $account;

		$now = time();

		$res = $db->q("
			INSERT INTO advertise_flat_deal 
			(type, status, account_id, zone_id, from_stamp, impressions_deal, impressions_left, pops_deal, pops_left, 
				to_stamp, clad_sponsor_position, clad_id, paid, created, created_by, description, started_stamp, finished_stamp, 
				next_deal_id) 
			VALUES 
			(?, ?, ?, ?, ?, ?, ?, ?, ?, 
				?, ?, ?, ?, ?, ?, ?, ?, ?, 
				?);",
			array($this->type, $this->status, $this->account_id, $this->zone_id, $this->from_stamp, $this->impressions_deal, $this->impressions_left, $this->pops_deal, $this->pops_left, 
				$this->to_stamp, $this->clad_sponsor_position, $this->clad_id, $this->paid, $now, $account->getId(), $this->description, $this->started_stamp, $this->finished_stamp, 
				$this->next_deal_id));
		$newid = $db->insertid($res);

		if ($newid == 0)
			return false;

		$this->id = $newid;

		audit::log("AFD", "Add", $this->id, "Type: {$this->type}, Account#: {$this->account_id}, Zone#: {$this->zone_id}", $account->getId());

		return true;
	}

	//additional functions

	/**
	 * Returns number of seconds until deal is over
	 */
	public function getSecondsLeft() {
		if ($this->type == "ZI" || $this->type == "ZP") {
			//compute how much time left in this deal - APPROXIMATELY!
        	//TODO this does not take in account pauses in this deal or changes in volume - correctly it should count only impression/pop numbers from last period - REDO
            $started = $this->started_stamp;
			if (!$started)
				$started = $this->created;
            $seconds_spent = time() - $started;

            if ($this->type == "ZI") {
	            $deal_spent = $this->impressions_deal - $this->impressions_left;
    	        $deal_left = $this->impressions_left;
            } else if ($this->type == "ZP") {
                $deal_spent = $this->pops_deal - $this->pops_left;
                $deal_left = $this->pops_left;
            }

			if ($deal_spent == 0)
				$deal_spent = 1;

            $deal_left_to_spent_ratio = $deal_left / $deal_spent;
            $seconds_left = $deal_left_to_spent_ratio * $seconds_spent;
        } else if ($this->type == "ZT") {
            $seconds_left = $this->to_stamp - time();
		}
		return $seconds_left;
	}

}

