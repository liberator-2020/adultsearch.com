<?php

class dictionary {
	var $db;
	var $words = array();
	var $pointer = 0;
	var $css_included = false;

	public function __construct() {
		global $db;
		$this->db = $db;
	}

	function wordsGet()	{
		if (!empty($this->words))
			return $this->words;

		$res = $this->db->q("select keyword, meaning, length(keyword) as length from dictionary ORDER BY length DESC");
		while($row=$this->db->r($res)) {
			$this->words[] = array(
				"keyword" => $row["keyword"],
				"meaning" => $row["meaning"]
				);
		}
		return $this->words;
	}

	//function process($text, &$script, &$content) {
	function process($text) {
		$words = $this->wordsGet();

		foreach($this->words as $d)	{
			$pointer_old = $this->pointer;
			$text = $this->_converter($text, $d["keyword"], $d["meaning"]);
			while($pointer_old<$this->pointer) {
			//	$script .= "TooltipManager.addHTML(\"tooltip{$pointer_old}\", \"tooltip_content{$pointer_old}\");\n";
			//	$content .= $this->_AddContent("<b>".strtoupper($d["keyword"]) . "</b>: ". $d["meaning"], $pointer_old);
				$pointer_old++;
			}
		}
		return $text;
	}

	function _converter($text, $word, $meaning) {
		//$word = strtoupper($word);
		$result = "";
		$last = 0;
		while (($pos = stripos($text, $word, $last)) !== false) {
			if( ($pos !== false && preg_match("/[a-z0-9&]/i", $text[$pos-1])) || preg_match("/[a-z0-9]/i", substr($text, $pos+strlen($word), 1)) ) {
				$result .= substr($text, $last, ($pos-$last)+(strlen($word)));
			} else {
				//add previos portion of string
				$result .= substr($text, $last, ($pos-$last));
				//add highlighted word
				$result .= "<span class=\"acronym\"><abbr title=\"$meaning\">".substr($text, $pos, strlen($word))."</abbr></span>";
				$this->pointer++;
				//update last searched character
			}
			$last = $pos + strlen($word);
		}
		//add rest of string
		$result .= substr($text, $last);
		return $result;
	}

	function _AddContent($txt, $number)	{
		global $gHtmlHeadInclude, $gHtmlBottomInclude;

		$return = "";
		if( !$this->css_included ) {
			$gHtmlHeadInclude .= "<script type=\"text/javascript\" src=\"/js/tools/tooltip.js\"></script>";
			$gHtmlBottomInclude .= ";
<script type=\"text/javascript\">
\$(document).ready(function() {	
\$('abbr').each(function(){	
\$(this).data('title',\$(this).attr('title'));
\$(this).removeAttr('title');
});

\$('abbr').mouseover(function() {	
\$('abbr').next('.dictionary').remove();
\$(this).after('<span class=\"dictionary\">' + \$(this).data('title') + '</span>');
var left = \$(this).position().left + \$(this).width() + 4;
var top = \$(this).position().top - 4;
\$(this).next().css('left',left);
\$(this).next().css('top',top);	
});
\$('abbr').click(function(){
\$(this).mouseover();
// after a slight 2 second fade, fade out the tooltip for 1 second
\$(this).next().animate({opacity: 0.9},{duration: 3000, complete: function(){
\$(this).fadeOut(1000);
}});
});
\$('abbr').mouseout(function(){
\$(this).next('.dictionary').remove();	
 
});	
});</script>";
			$this->css_included = true;
		}

		$return .= "<span title='{$txt}' ref='tool'>{$txt}</span>";
		return $return;
	}
}

