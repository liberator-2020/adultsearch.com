<?php

class dir {
	var $total_items;

	public function __construct() {
		$this->total_items = 0;
	}
	
	//function used in transition from legacy to unified db
	public static function isCountryUnified($country = NULL) {
		global $ctx, $account;

		// now data for all countries are unified
//		if (in_array($ctx->country_id, array(16046,16047,41973)))
//			return false;

		if (is_null($country))
			$country = $ctx->country;

		if (is_null($country))
			return false;

		return true;
	}

	public static function getBaseColumns() {
		$base_cols = array('place_id', 'place_type_id', 'name', 'loc_id', 'address1', 'address2', 'zipcode', 'phone1', 'phone2', 'fax', 'email', 'website', 'facebook', 'twitter', 'instagram', 'loc_lat', 'loc_long', 'image', 'thumb', 'review', 'recommend', 'overall', 'last_review_id', 'edit', 'visible', 'fake', 'worker_comment');
		return $base_cols;
	}
	
	public static function isBaseColumn($col_name) {
		$base_cols = self::getBaseColumns();
		return in_array($col_name, $base_cols);
	}

	public static function getTypes() {
		global $ctx, $db;

		if (is_null($ctx->country))
			return false;

		$type_array = array();
		$sql = "select pt.place_type_id, pt.name from place_type pt order by name asc";
		$res = $db->q($sql);
		while ($row = $db->r($res)) {
			$type_array[$row['place_type_id']] = $row['name'];
		}
		//_darr($type_array);
		return $type_array;		
	}
	
	public static function getTypesPlural() {
		$types = self::getTypes();
		$types_plural = array();
		foreach ($types as $key => $val) {
			$types_plural[$key] = self::getCategoryPlural($val);
		}
		return $types_plural;
	}

	public function getTypesForCountry($country_id) {
		global $ctx, $account, $sphinx;

		$sphinx->reset();
		$sphinx->SetFilter('country_id', array($country_id));
		$sphinx->SetGroupBy("place_type_id", SPH_GROUPBY_ATTR, '@count desc');
		$results = $sphinx->Query("", "dir");

		$types = array();
		foreach($results["matches"] as $match) {
			$place_type_id = $match["attrs"]["@groupby"];
			$name = $this->getCategoryNameById($place_type_id);
			$name_pl = $this->getCategoryPlural($name);
		  $types[] = array(
				"place_type_id" => $place_type_id,
				"name" => $name,
				"name_pl" => $name_pl,
				);
		}
		return $types;
	}

	public static function getColumns() {
		global $db, $ctx, $config_db_name;

		$cols = array();

		//columns from place table	
		$res = $db->q("select column_name from information_schema.columns where table_name = 'place' and table_schema = ?", [$config_db_name]);
		if (!$db->numrows($res)) {
			reportAdmin("class dir : getColumns() - table place doesnt exist!");
			return false;
		}
		while ($row = $db->r($res, MYSQL_ASSOC)) {
			$col_name = $row["column_name"];
			if (!array_key_exists($col_name, $cols))
				$cols[$col_name] = array("col_name" => $col_name, "col_title" => self::getBaseColumnTitle($col_name));
		}
		//columns from attribute table
		//2019-07-19 - lets not use location_attribute_table, use all attributes for all countries
		/*
		$res = $db->q("SELECT a.attribute_id, a.name, a.title, a.datatype, a.options 
						FROM attribute a 
						INNER JOIN location_attribute la on la.attribute_id = a.attribute_id 
						WHERE la.location_id = ?
						ORDER BY a.attribute_id ASC",
						[$ctx->country_id]
					);
		*/
		$res = $db->q("SELECT a.attribute_id, a.name, a.title, a.datatype, a.options FROM attribute a ORDER BY a.attribute_id ASC", []);
		while ($row = $db->r($res)) {
			$cols[$row["name"]] = [
				"attribute_id" => $row["attribute_id"],
				"col_name" => $row['name'],
				"col_title" => $row["title"], 
				"col_datatype" => $row["datatype"],
				"col_values" => $row["options"],
				];
		}
		//_darr($cols);
		//order fields
		$columnOrder = ["place_id", "place_type_id", "name", "loc_id", "neighbor_id", "location_exact_id", "neighborhood_area", "address1", "address2", "building_name"];
		$cols2 = [];
		foreach ($columnOrder as $colName) {
			if (!array_key_exists($colName, $cols))
				continue;
			$cols2[$colName] = $cols[$colName];
			unset($cols[$colName]);
		}
		$cols2 = array_merge($cols2, $cols);
		//_darr($cols2);
		return $cols2;
	}
	
	public static function getBaseColumnTitle($col_name) {
		$base_column_titles = array(
			"address1" => "Address 1",
			"address2" => "Address 2",
			"zipcode" => "Zipcode",
			"phone1" => "Phone 1",
			"phone2" => "Phone 2",
			"fax" => "Fax",
			"email" => "Email",
			"website" => "Website",
			"facebook" => "<img src=\"/images/icons/facebook_30.png\" style=\"width: 16px; vertical-align: middle;\" /> Facebook",
			"twitter" => "<img src=\"/images/icons/twitter_30.png\" style=\"width: 16px; vertical-align: middle;\" /> Twitter",
			"instagram" => "<img src=\"/images/icons/instagram_30.png\" style=\"width: 16px; vertical-align: middle;\" /> Instagram",
			"loc_lat" => "Latitude",
			"loc_long" => "Longitude",
		);
		if (!array_key_exists($col_name, $base_column_titles))
			return false;
		return $base_column_titles[$col_name];
	}

	public static function setItemsPerPage($ipp) {
		$ipp = intval($ipp);
		if ($ipp > 0 && $ipp < 200)
			$_SESSION["dir_ipp"] = $ipp;
	}
	
	public static function getItemsPerPage() {
		if (isset($_SESSION["dir_ipp"]))
			return $_SESSION["dir_ipp"];
		else
			return 30;
	}

	public static function getBiggestCityInCountry($country_id = NULL) {
		global $ctx, $sphinx;

		if (is_null($country_id))
			$country_id = $ctx->country_id;

		if (is_null($country_id))
			return false;

		$loc_id = NULL;

		//TODO delete after full unification
		if ($country_id == 16046) {
			//US
			return "/california/los-angeles";
		} else if ($country_id == 41973) {
			//UK
			return "/england/london";
		} else if ($country_id == 16047) {
			//Canada
			return "/ontario/toronto";
		}

		$sphinx->reset();
		$sphinx->SetLimits(0, 1, 1);
		$sphinx->SetFilter('country_id', array($country_id));
		$sphinx->SetGroupBy('loc_id', SPH_GROUPBY_ATTR, '@count desc');
		$results = $sphinx->Query("", "dir");
		$loc_id = intval($results["matches"][0]["attrs"]["loc_id"]);
		$loc_url = self::getLocationUrl($loc_id);
		if (empty($loc_url))
			return false;
		//debug_log("loc_id = $loc_id");
		return "/{$loc_url}";
	}

	public static function getLocationUrl($loc_id) {
		global $mcache;

		if (is_null($loc_id))
			return false;

		$res = $mcache->get("SQL:LOC-ID",array($loc_id));
		if ($res === false)
			return false;

		return self::getLocationUrlByRow($res);
	}

	public static function getLocationUrlByRow($location_row) {
		global $mcache;

		if ($location_row['loc_parent'] == $location_row['country_id'])	//international cities with no state
			return $location_row['dir'];

		//for domestic locations look up also state
		$res2 = $mcache->get("SQL:LOC-ID",array($location_row['loc_parent']));
		if ($res2 === false)
			return false;
		return $res2['dir']."/".$location_row['dir'];
	}

	//TODO probably remove $type_array from parameter here and get from context
	public function getRefineLocations($type_array = NULL) {
		global $db, $account, $ctx, $mcache, $sphinx;

		if ($ctx->location_type > 2)
			return array();

		//_d("country_id={$ctx->country_id} loc_id={$ctx->location_id}");
		$locations = array();
		$sphinx->reset();
		$sphinx->SetFilter('country_id', array($ctx->country_id));
		if ($ctx->location_type == 2)
			$sphinx->SetFilter('state_id', array($ctx->state_id));
		if (!$account->isWorker()) 
			$sphinx->SetFilter('edit', array(1,2,3,5));
		if ($ctx->category_id)
			$sphinx->SetFilter('place_type_id', [$ctx->category_id]);
		$sphinx->SetGroupBy('loc_id', SPH_GROUPBY_ATTR, '@group desc');
		$results = $sphinx->Query("", "dir");
		//_darr($results["matches"]);
		if (empty($results["matches"]))
			return array();
		foreach($results["matches"] as $row) {
			$loc_id = $row["attrs"]["loc_id"];
			$res = $mcache->get("SQL:LOC-ID",array($loc_id));
			if ($res === false) {
				continue;
			}
			$loc_name = $res['loc_name'];
			$loc_url = self::getLocationUrlByRow($res);

			$link = "/".$loc_url;
			if ($ctx->category)
				$link .= "/".$ctx->category;

			$locations[] = array(
				"id"	=> "City",
				"type"  => "Location",
				"name"  => $loc_name,
				"c"	 => $row["attrs"]["@count"],
				"link"  => $link,
				"title" => $loc_name,
				);	
		}

		$locs_by_name = function($l1, $l2) { return strcmp($l1["name"], $l2["name"]); };
		usort($locations, $locs_by_name);

		return $locations;
	}

	private function getCategory($place_type_id) {
		global $ctx;

		$name = $this->getCategoryNameById($place_type_id);
		if (!$name)
			return false;

		return array(
			"id"		  => $place_type_id,
			"parent"	  => $this->getCategoryParentById($place_type_id),
			"name"		=> $name,
			"name_plural" => $this->getCategoryPlural($name),
			"link"		=> $ctx->location_path."/".self::getCategoryUrlNameById($place_type_id),
			);
	}

	//TODO remove link in class forum and then remove this function
	public function getLocCategories() {
		global $ctx, $account, $sphinx;

		$sphinx->reset();
		if ($ctx->location_type == 3)
			$sphinx->SetFilter('loc_id', array($ctx->city_id));
		else if ($ctx->location_type == 2)
			$sphinx->SetFilter('state_id', array($ctx->state_id));
		else
			return array();
		$sphinx->SetGroupBy("place_type_id", SPH_GROUPBY_ATTR, '@count desc');
		$results = $sphinx->Query("", "dir");

		$categories = array();
		$subcategories = array();
		foreach($results["matches"] as $match) {
			$place_type_id = $match["attrs"]["@groupby"];
			$cat = $this->getCategory($place_type_id);

			$count = $match["attrs"]["@count"];
			//when more than one place and name not already in plural form. we add 's' at the end
			if ($count > 1) {
				$cat["name"] = $cat["name_plural"];
			}
			
			$cat["type"] = "type";
			$cat["c"] = $count;
			$cat["subcats"] = array();

			if (!$cat["parent"])
				$categories[$place_type_id] = $cat;
			else
				$subcategories[$place_type_id] = $cat;
		}

		//assign subcategories to categories
		foreach ($subcategories as $subcat) {
			$parent_id = $subcat["parent"];
			if (!array_key_exists($parent_id, $categories)) {
				//we have places in subcategory, but no places in main category, lets fetch data about main category
				$cat = $this->getCategory($parent_id);
				if ($cat == false)
					continue;		//we couldnt find parent category .. theres nothing we can do now
				$cat["type"] = "type";
				$cat["c"] = 0;
				$cat["subcats"] = array();
				$categories[$parent_id] = $cat;
			}
			//assign subcategory under main category
			if (empty($categories[$parent_id]["subcats"]))
				$categories[$parent_id]["subcats"] = array($categories[$parent_id]);
			$categories[$parent_id]["subcats"] = array_merge($categories[$parent_id]["subcats"], array($subcat));
			$categories[$parent_id]["c"] = $categories[$parent_id]["c"] + $subcat["c"];
		}

		$categories = $this->sortCategoryArray($categories, 'name_plural');
		//_darr($categories);
		return $categories;
	}
	
	public function getLocCategories2($populars = true) {
		global $ctx, $account, $sphinx, $db;

		$sphinx->reset();
		if ($ctx->location_type == 3) {
			$sphinx->SetFilter('loc_id', array($ctx->city_id));
		} else if ($ctx->location_type == 2) {
			$sphinx->SetFilter('state_id', array($ctx->state_id));
		} else {
			return array();
		}
		$sphinx->SetGroupBy("place_type_id", SPH_GROUPBY_ATTR, '@count desc');
		$sphinx->SetFilter('edit', array(1,2,3,5));
		$results = $sphinx->Query("", "dir");

		$categories = array();
		$subcategories = array();
		foreach($results["matches"] as $match) {
			$place_type_id = $match["attrs"]["@groupby"];
			$cat = $this->getCategory($place_type_id);

			$count = $match["attrs"]["@count"];
			//when more than one place and name not already in plural form. we add 's' at the end
			if ($count > 1) {
				$cat["name"] = $cat["name_plural"];
			}

			$cat["type"] = "type";
			$cat["c"] = $count;
			$cat["subcats"] = array();

			if (!$cat["parent"])
				$categories[$place_type_id] = $cat;
			else
				$subcategories[$place_type_id] = $cat;
		}		
		
		//assign subcategories to categories
		foreach ($subcategories as $subcat) {
			$parent_id = $subcat["parent"];
			if (!array_key_exists($parent_id, $categories)) {
				//we have places in subcategory, but no places in main category, lets fetch data about main category
				$cat = $this->getCategory($parent_id);
				if ($cat == false)
					continue;		//we couldnt find parent category .. theres nothing we can do now
				$cat["type"] = "type";
				$cat["c"] = 0;
				$cat["subcats"] = array();
				$categories[$parent_id] = $cat;
			}
			//assign subcategory under main category
			if (empty($categories[$parent_id]["subcats"]))
				$categories[$parent_id]["subcats"] = array($categories[$parent_id]);
			$categories[$parent_id]["subcats"] = array_merge($categories[$parent_id]["subcats"], array($subcat));
			$categories[$parent_id]["c"] = $categories[$parent_id]["c"] + $subcat["c"];
		}

		//add populars for each category
		if ($populars) {
			$sphinx->SetLimits(0, 5, 5);
			$sphinx->ResetGroupBy();
			$sphinx->SetSortMode(SPH_SORT_EXTENDED, "last_review_id DESC");
			$map_query_type = array();
			foreach ($categories as $category) {
				$sphinx->ResetFilters();

				//todo unite
				if ($ctx->location_type == 3)
					$sphinx->SetFilter('loc_id', array($ctx->city_id));
				else if ($ctx->location_type == 2)
					$sphinx->SetFilter('state_id', array($ctx->state_id));

				$sphinx->SetFilter('place_type_id', array($category["id"]));
				
				$sphinx->SetFilter('edit', array(1,2,3,5));

				$map_query_type[$sphinx->AddQuery("", "dir")] = $category["id"];
			}
			$results = $sphinx->RunQueries();
			$i = 0;
			foreach($results as $result) {
				$ids = NULL;
				foreach($result["matches"] as $res) {
					$ids .= $ids ? (",".$res["id"]) : $res["id"];
				}
				if (is_null($ids)) {
					$categories[$map_query_type[$i]]["populars"] = array();
				} else {
					$res = $db->q("SELECT p.place_id, p.loc_id, p.place_type_id, p.name, p.thumb, p.review, p.recommend, r.reviewdate as reviewdate 
									FROM place p
									LEFT JOIN place_review r on (p.last_review_id = r.review_id and r.module = 'place')
									WHERE p.place_id in ({$ids})
									ORDER BY field (p.place_id, {$ids})");
					$populars = array();
					while($row = $db->r($res)) {
						$rank = !$row['review'] ? "Not reviewed yet" : ($row['recommend'] . " positive reviews");
						$populars[] = array(
							"id"	=> $row['place_id'],
							"name"  => $row['name'],
							"thumb" => $this->getPlaceThumbUrl($row),
							"url"	=> $this->getPlaceLink($row),
							"rank"  => $rank,
							"date"  => $row['reviewdate'],
						);
					}
					$categories[$map_query_type[$i]]["populars"] = $populars;
				}
				$i++;
			}
		}

		$categories = $this->sortCategoryArray($categories, 'name_plural');
		return $categories;
	}

	public static function cmp_sort_order_value($a, $b) {
		if ($a['sort_order_value'] == $b['sort_order_value']) {
			return 0;
		}
		return ($a['sort_order_value'] < $b['sort_order_value']) ? -1 : 1;
	}

	public static function sortCategoryArray($arr, $name_key, $keep_keys = true) {
		$sort_order = array(
			"General Talk" => "-1",

			"Female Escorts" => "1",
			"Male Escorts for Female" => "2",
			"Male for Male Escorts" => "3",
			"TS/TG Shemale Escorts" => "4",

			"Strip Clubs" => "11",
			"Go Go Bars" => "12",
			"Girlie Bars" => "13",
			"Beer Bars" => "14",
			"Erotic Massage Parlors" => "15",
			"Hostess Bars" => "16",
			"Night Clubs" => "17",
			"Night Clubs - Freelance Escorts" => "18",
			"Clubs" => "19",
			"Freelance Bars" => "20",
			"KTV Bars" => "22",
			"Cabaret Shows" => "24",
			"Cabarets" => "25",

			"Sex Shops" => "28",

			"Short Times" => "30",
			"Hotels" => "31",
			"Hotels - Freelance Escorts" => "32",

			"Bordellos" => "40",
			"Privats" => "41",
			"Laufhaus" => "42",

			"Sex Clubs" => "50",
			"Live Sex Shows" => "51",
			"Prives - Brothels" => "52",
			"Swingers Clubs" => "53",
			"FKK Clubs" => "54",
			"Personnes" => "55",

			"Eros Centers" => "60",
			"Sex Kinos" => "62",
			"Adult Theatres" => "63",

			"Fetish Clubs" => "70",
			"Studios" => "71",

			"Lady Boy Massage Parlors" => "80",
			"Lady Boy Go Go Bars" => "81",
			"Lady Boy Bars" => "82",
			"Lady Boy Cabaret Shows" => "83",
			"Tranny Shows" => "84",

			"Gay Erotic Massage Parlors" => "100",
			"Gay Go Go Bars" => "101",
			"Gay Night Clubs" => "102",
			"Gay Bars" => "103",
			"Gay Bars of Prostitution" => "104",
			"Gay KTV Bars" => "105",
			"Gay Saunas" => "106",
			);
		foreach($arr as $item) {
			if (array_key_exists($item[$name_key], $sort_order))
				$item['sort_order_value'] = $sort_order[$item[$name_key]];
			else
				$item['sort_order_value'] = "99";
			$arr2[] = $item;
		}
		if ($keep_keys)
			uasort($arr2, "dir::cmp_sort_order_value");
		else
			usort($arr2, "dir::cmp_sort_order_value");
		return $arr2;		
	}

	public static function getCategoryPlural($namex) {
		//TODO incorporate plural names of categories
		if ($namex == "Girlie Bar-Bar of Prostitution")
			$namex = "Girlie Bar-Bars of Prostitution";
		else if ($namex == "Gay Bar of Prostitution")
			$namex = "Gay Bars of Prostitution";
		else if ($namex == "Bar of Prostitution")
			$namex = "Bars of Prostitution";
		else if ($namex == "Gay Fetish")
			$namex = "Gay Fetishes";
		else if ($namex == "Restaurant Boom Boom")
			$namex = "Restaurants Boom Boom";
		else if ($namex == "Salon Boom Boom")
			$namex = "Salons Boom Boom";
//		else if ($namex == "Strip Club - Brothel")
//			$namex = "Strip Clubs - Brothels";
		else if ($namex == "Strip Club with Erotic Massage")
			$namex = "Strip Clubs with Erotic Massage";
		else if ($namex == "Night Club - Freelance Escorts")
			$namex = "Night Clubs - Freelance Escorts";
		else if ($namex == "Nudist Colony")
			$namex = "Nudist Colonies";
		else if (substr($namex, -1) != "s")
			$namex .= "s";
		return $namex;
	}

	public function getRefineCategories($type_array = NULL) {
		//TODO - col=1 should not be here, thats presentation logic, probably change whole return array
		return array("name" => "Category", "col" => 1, "alt" => $this->getLocCategories2(false));
	}

	public function getRefineFacets() {
		global $ctx;

		$facets = array();

		//TODO add here fetch facet attributes from database
		$facet_attributes = array(
			"neighbor_id" => "Neighborhoods",
//			"masseuse" => "Masseuse",
//			"" => "",
			);

		foreach ($facet_attributes as $attr => $attr_group_name) {
			if (array_key_exists($attr, $ctx->search))
				continue;
			$facet_values = $this->getRefineFacetValues($attr);
			if (!$facet_values)
				continue;
			$facets[] = [
				"name" => $attr_group_name, 
				"col" => 1, 
				"alt" => $facet_values
				];
		}

		$services_facet_attributes = ["table_shower", "sauna", "jacuzzi", "truckparking", "private_parking", "fourhand", "always"];
		$services = [];
		foreach ($services_facet_attributes as $attr) {
			if (array_key_exists($attr, $ctx->search))
				continue;
			$facet_values = $this->getRefineFacetValues($attr);
			if (!$facet_values)
				continue;
			$services = array_merge($services, $facet_values);
		}
		if (!empty($services))
			$facets[] = [
				"name" => "Services",
				"col" => 1, 
				"alt" => $services,
				];

		return $facets;
	}

	public function getMultiOptionsArray($attrName) {
		global $mcache;
	
	   	$res = $mcache->get("SQL:ATT-NAM", [$attrName]);
		if ($res === false)
			return false;
		$options = $res["options"];
		$arr1 = explode("|", $options);
		$result = [];
		foreach ($arr1 as $item) {
			$arr2 = explode("@", $item);
			if (count($arr2) != 2)
				continue;
			$result[$arr2[0]] = $arr2[1];
		}
		return $result;
	}

	public function getRefineFacetValues($attr) {
		global $ctx, $mcache, $sphinx;

//		_d("facet attr_name={$attr}");

		//TODO following code is similar to dir->getLocCategories, unify...
		$sphinx->reset();
		if ($ctx->location_type == 3)
			$sphinx->SetFilter('loc_id', array($ctx->city_id));
		else if ($ctx->location_type == 2)
			$sphinx->SetFilter('state_id', array($ctx->state_id));
		else
			return array();

		if ($ctx->category_id != NULL)
			$sphinx->SetFilter('place_type_id', array($ctx->category_id));

		$sphinx->SetGroupBy($attr, SPH_GROUPBY_ATTR, '@count desc');
		$results = $sphinx->Query("", "dir");
//		_darr($results);

		$services = [];
		foreach($results["matches"] as $match) {
			$attr_id = $match["attrs"][$attr];
			$type = $attr;
			$link = null;
			if ($attr == "neighbor_id") {
				$loc = $mcache->get("SQL:LOC-ID", array($attr_id));
				if ($loc === false)
					continue;
				$attr_title = $loc["loc_name"];
				$link = $this->getFilterAddLink($attr, $attr_id);
			} else if (in_array($attr, ["table_shower", "sauna", "jacuzzi", "truckparking", "private_parking", "fourhand", "always"])) {
				if ($attr_id != 2)
					continue;
				$type = "services";
				$attr_title = $this->getFilterTitle($attr, null);
				$link = $this->getFilterAddLink($attr, $attr_id);
			} else if ($attr == "masseuse") {
continue;
//TODO
/*
				if (!$attr_id)
					continue;
				$options = $this->getMultiOptionsArray($attr);
				$attr_title = $options[$attr_id];
				_d(print_r($attr_id, true)." {$attr_title}");
				$link = $this->getFilterAddLink($attr, $attr_id);
*/
			} else {
				$attr_title = "Not implemented yet";
			}

			$facet_values[] = array(
				"id" => $attr_id, 
				"type" => $type, 
				"name" => $attr_title,
				"link" => $link,
				"c" => $match["attrs"]["@count"]
				);
		}

		return $facet_values;
	}

	/*
	 * get absolute URL for thumbnail, e.g. https://img.adultsearch.com/place/t/el6ySxA9tK3H3x6DlJKY_389.jpg
	 * parameter can be thumb filename, or $row from DB
	 */
	public function getPlaceThumbUrl($thumb) {
		global $config_image_server;

		if (is_array($thumb))
			$thumb = $thumb["thumb"];
		
		if (empty($thumb))
			return "/images/placeholder.gif";
		
		return $config_image_server."/place/t/{$thumb}";
	}

	public function agetVideosForCity() {
		global $account, $ctx, $config_image_server, $db, $sphinx;

		$vids = array();

		//TODO lets have a limit for displayed videos ?
		$sphinx->reset();
		$sphinx->SetLimits( 0, 60, 60);
		if ($ctx->location_type == 3)
			//TODO incorporate loc_around mechanism: $this->sphinx_client->SetFilter('loc_id', $account->locAround());
			$sphinx->SetFilter('loc_id', array($ctx->city_id));
		else if ($ctx->location_type == 2)
			$sphinx->SetFilter('state_id', array($ctx->state_id));
		else
			return array();
		$sphinx->SetFilterRange('video_cnt', 1, 1000);
		$results = $sphinx->Query("", "dir");
		$ids = NULL;
		foreach($results["matches"] as $res)
			//$ids .= $ids ? (",".$res["place_id"]) : $res["place_id"];
			$ids .= $ids ? (",".$res["id"]) : $res["id"];

		//add videos for location
		//TODO move to sphinx search
		//TODO this sql is redundant (the same join later in getVideosByPictures) ...
		$sql = "SELECT picture 
				FROM place_picture 
				WHERE place_file_type_id = 2 and id = 0 and (loc_id = '{$ctx->location_id}' or loc_id = '{$ctx->state_id}' or loc_id = '{$ctx->country_id}') and visible = 1";
		//_d("sql = ".$sql);
		$res = $db->q($sql);
		$pictures = NULL;
		while ($rox = $db->r($res))
			$pictures .= $pictures ? (",".$rox["picture"]) : $rox["picture"];

		//ok first videos for this location (city)
		if (!is_null($pictures))
			$vids = array_merge($vids, $this->agetVideosByPictures($pictures));
		//_d("count after ByPictures:".count($vids));

		//get videos for places
		if (!is_null($ids))
			$vids = array_merge($vids, $this->agetVideosByIds($ids, true));
		//_d("count after ByIds:".count($vids));

		//set names of videos
		foreach ($vids as $vid) {
			$name = $vid->getName();
			if (empty($name)) {
				$name = $vid->getPlaceName();
			}
			if (!empty($name) && $vid->getType() != VIDEO_TYPE_LOCATION && $vid->getLocationName() != "") {
				//$name .= " - ".$vid->getLocationName().", ".$ctx->country_name;
				$name .= " - ".$vid->getLocationName();
			} else if (empty($name)) {
				$name = $vid->getLocationName();
			}
				$vid->setName($name);
		}

		//if no internal videos, get youtube videos
		if (empty($vids) && (!is_null($ids))) {
			$vids = $this->agetVideosByIds($ids);
		}
		
		//sort by orde desc
		$r = usort($vids, array("dir", "cmp_video_orden_desc"));
		//_darr($vids);
		return $vids;
	}

	public static function cmp_video_orden_desc($a, $b) {
		$ao = $a->getOrden();
		if ($ao == NULL)
			$ao = 0;
		$bo = $b->getOrden();
		if ($bo == NULL)
			$bo = 0;
		return ($ao == $bo) ? 0 : (($ao < $bo) ? -1 : 1);
		return $ret;
	}

	public static function place() {
		global $ctx;

		if ($ctx->country == NULL) {
			echo "Error: country not set in ctx!";
			die;
		}

		$id = intval($_REQUEST["id"]);
		if ($id == 0) {
			echo "Error: missing id!";
			die;
		}
		$ctx->node_id = $id;
		$link = self::getPlaceLink();
		if ($link == "") {
			echo "Error: cant create place link!";
			die;
		}

		system::moved($link);	
		die;
	}
	
	public static function loadNode() {
		global $db, $ctx, $account;

		//item already loaded
		if ($ctx->item != NULL)
			return $ctx->item;

		if (!$ctx->node_id)
			return false;

		$item = array();
		$res = $db->q("
			SELECT p.*, l.loc_name, l.s, l.country_id, lc.dir as country, ls.loc_id as state_id
			FROM place p
			INNER JOIN location_location l ON p.loc_id = l.loc_id
			INNER JOIN location_location lc ON l.country_id = lc.loc_id
			LEFT JOIN location_location ls ON l.loc_parent = ls.loc_id
			WHERE p.place_id = ?
			",
			[$ctx->node_id]
		);
		if (!$db->numrows($res)) {
			return false;
		}
		$item = $db->r($res, MYSQL_ASSOC);

		$loc_label = $item["loc_name"];
		if ($item["s"])
			$loc_label .= ", ".$item["s"];
		$item["loc_label"] = $loc_label;
		
		$res = $db->q("
			SELECT a.name, (CASE WHEN a.datatype = 'text' OR a.datatype = 'cover' THEN pa.value_text ELSE pa.value END) as value
			FROM place_attribute pa
			INNER JOIN attribute a ON a.attribute_id = pa.attribute_id
			WHERE pa.place_id = ?
			",
			[$ctx->node_id]
		);
		while ($row = $db->r($res, MYSQL_ASSOC)) {
			$name = $row['name'];
			$value = $row['value'];
			if (in_array($name, ["monday_to", "tuesday_to", "wednesday_to", "thursday_to", "friday_to", "saturday_to", "sunday_to"]) && $value > 1440)
				$value -= 1440;
			$item[$name] = $value;
		}

		//update gps location if not entered
		self::updateGeolocation($item);

		$ctx->item = $item;
		//_darr($item);
		return $item;
	}
	
	public static function isOldNode($old_id, $loc_id, $url_name) {
		global $db;

		$sql = "SELECT p.*
				FROM place p
				WHERE p.old_id = '{$old_id}' and p.loc_id = '{$loc_id}'";
		$res = $db->q($sql);
		// may return multiple results with $old_id + $loc_id, but with different names
		if (!$db->numrows($res)) {
			return false;
		}
		while($row = $db->r($res)) {
			// check the url_name for the old style slug - if it exists, so this is the old URL,
			// and we need to redirect to this new URL
			if (self::getPlaceUrlName($row["name"], false) == $url_name) {
				return $row;
			}											//this row can be used for redirection, but not as ctx->item (doesnt have attributes)
		}
		return false;
	}

	/*
	 * Stores value of attribute - upsert
	 */
	public static function storeAttributeByName($place_id, $attr_name, $attr_db_value) {
		global $mcache, $db;

		if ($attr_db_value === "") {
//			_d("NULLing value of $attr_name");
			$attr_db_value = NULL;
		}

		$res = $mcache->get("SQL:ATT-NAM", array($attr_name));
		if ($res === false) {
			return false;
		}

		$attribute_id = $res["attribute_id"];
		$value_col_name = ($res["datatype"] == "text" || $res["datatype"] == "cover") ? "value_text" : "value";

		$sql = "";
		$res = $db->q("SELECT place_id FROM place_attribute WHERE place_id = '{$place_id}' AND attribute_id = '{$attribute_id}'");
		if ($db->numrows($res)) {
			if (is_null($attr_db_value)) {
				//delete
				$sql = "DELETE FROM place_attribute WHERE place_id = '{$place_id}' AND attribute_id = '{$attribute_id}'";
				//_d("SQL='{$sql}'");
				$db->q($sql);
				if ($db->affected() == 1)
					return true;
			} else {
				//update
				$sql = "UPDATE place_attribute SET {$value_col_name} = '{$attr_db_value}' WHERE place_id = '{$place_id}' AND attribute_id = '{$attribute_id}'";
				//_d("SQL='{$sql}'");
				$db->q($sql);
				return true;
			}
		} else if (!is_null($attr_db_value)){
			//insert
			$sql = "INSERT INTO place_attribute (place_id, attribute_id, {$value_col_name}) VALUES ('{$place_id}', '{$attribute_id}', '{$attr_db_value}')";
			//_d("SQL='{$sql}'");
			$db->q($sql);
			if ($db->affected() == 1)
				return true;
		}
		
		if (empty($sql))
			return true;
		return false;
	}

	private static function updateGeolocation(&$row) {
		global $ctx, $db, $account;

		//do not auto-update when bot is indexing the website
		if (is_bot())
			return;

		//_d("updateGeolocation 1");
		//if(!empty($row["loc_lat"]) && !empty($row["loc_long"]))
		if(!is_null($row["loc_lat"]) && !is_null($row["loc_long"]))
			return;

		//_d("updateGeolocation add='{$row["address1"]}', city='{$ctx->location_name}', country='{$ctx->country}', zip='{$row["zipcode"]}'");

		list($lat, $lon, $city, $zip) = get_coord($row["address1"], $ctx->location_name, $ctx->country, $row["zipcode"]);

		if ($lat != "error") {
			//found latlong, update place table
			$db->q("UPDATE place SET loc_lat = ?, loc_long = ? WHERE place_id = ?", array($lat, $lon, $ctx->node_id));
			//and update row result
			$row["loc_lat"] = $lat;
			$row["loc_long"] = $lon;
		} else {
			$row["loc_lat"] = 0.0;
			$row["loc_long"] = 0.0;
			$db->q("UPDATE place SET loc_lat = ?, loc_long = ? WHERE place_id = ?", array($lat, $lon, $ctx->node_id));
			if (strstr($lon, "ZERO_RESULTS") !== false) {
				//zero results error, do nothing
			} else {
				//other error, notify admin
/*
				if (!$account->isadmin() && !is_bot()) {
					reportAdmin("AS: dir::updateGeoLocation: error", 
						"{$ctx->node_id} gmap error<br />{$ctx->domain_base_link}/dir/place?id={$ctx->node_id}", 
						array("lat" => $lat, "lon" => $lon, "city" => $city, "zip" => $zip)
						);
				}
*/
			}
		}
	}

	public static function setPositionCity(&$loc_lat, &$loc_long) {
		global $db, $ctx;

		$sql = "select loc_lat, loc_long from location_location where loc_id = '{$ctx->location_id}'";
		$res = $db->q($sql);
		if ($db->numrows($res) != 1)
			return;
		$row = $db->r($res);

		$loc_lat = $row["loc_lat"];
		$loc_long = $row["loc_long"];
	}

	/**
	 *  Converting a name for a link in a new and old way
	 *
	 * @param null $name
	 * @param bool $newStyle
	 * @return string|null
	 */
	public static function getPlaceUrlName($name = NULL, $newStyle = true) {
		global $ctx;
		if (is_null($name))
			$name = $ctx->item['name'];
		if($newStyle) {
			$name = trim($name);
			$name_url = preg_replace('/[^a-z0-9-]+/', '-', strtolower($name));
		} else {
			$name_url = preg_replace('/[^a-z0-9\-\s]/', '', strtolower($name));
			$name_url = preg_replace('/\s+/', '-', $name_url);
		}
		return $name_url;
	}

	public function getTitle() {
		global $ctx, $account, $page;

		//concrete business listing (place)
		if (!is_null($ctx->node_id)) {
			$title = $ctx->item['name']." - ".$this->getCategoryNameById($ctx->item["type"])." - ".$ctx->item['loc_name'];
			if (!empty($ctx->item['phone1'])) {
				$title .= " | ".makeproperphonenumber($ctx->item['phone1']);
			}
			if ($ctx->item['review'])
				$title .= " | Total Review: ".$ctx->item['review'];
			return $title;
		}

		//other directory pages (city page, list page, ...)
		if ($ctx->category_name) {
			$title = self::getCategoryPlural($ctx->category_name)." in ".$ctx->location_name;
			if ($ctx->category_id == 15)
				$title .= " and Happy Endings";
			if ($ctx->city_s)
				$title .= " {$ctx->city_s}";
		} else {
			//$title = "{$ctx->location_name} Sex Tourism Destinations Escorts, Strip Clubs, Massage Parlors and Sex Shops";
			if ($ctx->international)
				$title = "{$ctx->location_name} {$ctx->country_name} Escorts, Strip Clubs, Massage Parlors and Sex Shops";
			else
				$title = "{$ctx->location_name} {$ctx->state_name} Escorts, Strip Clubs, Massage Parlors and Sex Shops";
		}
		if ($page > 1)
			$title .= " - Page $page";
		return $title;
	}

	public function getMetaDescription() {
		global $ctx;

		if (!empty($ctx->node_id)) {
			$desc = "";
			$review = new review("place", $ctx->node_id, $ctx->country." place", "/".$ctx->country."/place", "place");	//TODO it seems we are creating $review multiple times
			$quickread = $review->reviews();
			if (!empty($quickread))
				$desc = $ctx->item['name']." - ".substr(htmlspecialchars($quickread[0]['review'], ENT_QUOTES), 0, 100) .". ";
			$desc .= $ctx->item['name']." - ".$ctx->item['address1'].", ".$ctx->item['loc_name'].", ".ucwords($ctx->country).". ".$this->getCategoryNameById($ctx->item["place_type_id"]);
			return $desc;
		}

		if ($ctx->item["place_type_id"])
			$cat_id = $ctx->item["place_type_id"];
		else if ($ctx->category_id)
			$cat_id = $ctx->category_id;

		if ($cat_id)
			$cat = $this->getCategoryPlural($this->getCategoryNameById($cat_id));

		switch ($ctx->country) {
			case "belgium":
				if ($cat)
					return "Find the hottest {$cat} in Belgium.";
				return "Find the hottest sex tourism spots in Belgium including escorts, brothels, adult theatres, sex shops, night clubs and more.";
			case "cambodia":
				if ($cat)
					return "If you're looking for {$cat}, then Cambodia is possibly the best place in the world go and adultsearch.com is where to find it!";
				return "If you're looking for escorts, sex shops, erotic massage and girly bars then Cambodia is possibly the best place in the world go and adultsearch.com is where to find it!";
			case "china":
				if ($cat)
					return "The complete list of {$cat} in China manually curated and updated daily.";
				return "The complete list of escorts, erotic massage, KTV bars, sex shops, adult stores and freelance bars in China manually curated and updated daily.";
			case "costarica":
				if ($cat)
					return "All the hottest sex tourism spots in Costa Rica along with tons of {$cat}. 100% accurate!";
				return "All the hottest sex tourism spots in Costa Rica along with tons of listings of escorts, erotic massage parlors, sex shops and brothels. 100% accurate!";
			case "cyprus":
				if ($cat)
					return "Get in on the all the adult action in Cyprus. We have a manually curated database of all the {$cat}.";
				return "Get in on the all the adult action in Cyprus. We have a manually curated database of all the escorts, strip clubs, whore houses, massage parlors, sex shops and more.";
			case "germany":
				if ($cat)
					return "Legalized prostitution makes Germany one of the hottest sex tourism destinations in Europe with the hottest {$cat}.";
				return "Legalized prostitution makes Germany one of the hottest sex tourism destinations in Europe with the hottest escorts, massage parlors, strip clubs sex shops and more.";
			case "greece":
				if ($cat)
					return "Prostitution and sex tourism locations in Greece along with a complete list of {$cat} and reviews.";
				return "Prostitution and sex tourism locations in Greece along with a complete list of bordellos, strip clubs,  sex shops massage parlors and reviews.";
			case "guam":
				if ($cat)
					return "Find {$cat} on the beautiful little island of Guam using our Guamanian adult search directory.";
				return "Find escorts, adult stores and erotic massage parlors on the beautiful little island of Guam using our Guamanian adult search directory.";
			case "hungary":
				if ($cat)
					return "We have the most complete list of adult venues in Hungary including {$cat} !";
				return "We have the most complete list of adult venues in Hungary including escorts, brothels, strip clubs adult stores freelance bars and many more!";
			case "malaysia":
				if ($cat)
					return "Looking for {$cat} in Malaysia? Well look no further because we have it all with daily updates and reviews!";
				return "Looking for erotic massage, brothels, escorts or sex shops in Malaysia? Well look no further because we have it all with daily updates and reviews!";
			case "mexico":
				if ($cat)
					return "The best guide and reviews on Mexico's sexual tourist spots in Tijuana for {$cat}.";
				return "The best guide and reviews on Mexico's sexual tourist spots in Tijuana for strip clubs, erotic massage parlors, sex shops and secret brothels.";
			case "netherlands":
				if ($cat)
					return "Netherlands first adult search engine and directory with thousands of listings of {$cat}.";
				return "Netherlands first adult search engine and directory with thousands of listings of escorts, sex shops, massage parlors, red light districts, sex clubs and brothels.";
			case "panama":
				if ($cat)
					return "Panama's most accurate and complete list of {$cat}. Manually curated and updated daily with reviews.";
				return "Panama's most accurate and complete list of sex tourism destinations, escorts, freelance bars, brothels and much more. Manually curated and updated daily with reviews.";
			case "philippines":
				if ($cat)
					return "Your guide to sex tourism in one of the best mongering spots in the world. Find {$cat} on adult services.";
				return "Your guide to sex tourism in one of the best mongering spots in the world. Find escorts, girlie bars, massage parlors and daily reviews on adult services.";
			case "romania":
				if ($cat)
					return "Looking for some action in Romania? We have personally indexed {$cat} and all the hot sex tourism destinations in RO.";
				return "Looking for some action in Romania? We have personally indexed secret brothels, sex shops, freelance bars and all the hot sex tourism destinations in RO.";
			case "singapore":
				if ($cat)
					return "We have indexed and personally visited all the {$cat} in Singapore so we could bring you this awesome site along with listings for escorts and reviews.";
				return "We have indexed and personally visited all the whorehouses, massage parlors, sex shops in Singapore so we could bring you this awesome site along with listings for escorts and reviews.";
			case "switzerland":
				if ($cat)
					return "Adult tourism locations in Switzerland including {$cat}. We also have escort listings and updated reviews.";
				return "Adult tourism locations in Switzerland including bordellos, sex stores, massage parlors and cabarets. We also have escort listings and updated reviews.";
			case "thailand":
				if ($cat)
					return "The most complete and up-to-date list of Thailand's sexual tourism attractions including {$cat}.";
				return "The most complete and up-to-date list of Thailand's sexual tourism attractions including escorts, girlie bars, erotic massage spas and much more.";
			case "vietnam":
				if ($cat)
					return "Browse our directory of Vietnam's hottest sex tourism destinations including {$cat}.";
				return "Browse our directory of Vietnam's hottest sex tourism destinations ranging from freelance bars and girlie bars to massage parlors, secret brothels and escorts.";
		}
		return "";
	}

	public function showAd() {
		global $advertise, $ctx;

		switch ($ctx->category) {
			case 'brothel':	$advertise->showAd(16); return; break;
			case 'erotic-massage-parlor':	$advertise->showAd(7); return; break;
			case 'sex-shop':	$advertise->showAd(8); return; break;
			case 'strip-club':	$advertise->showAd(9); return; break;
			default: return;
		}

		return;
	}

	/**
	 * Returns link to place in parameter
	 * parameter array must contain:
	 * id, loc_id, type, name
	 * e.g. http://mexico.adultsearch.com/tijuana/sex-shop/sex-in-the-city/56
	 */
	public static function getPlaceLink($row = NULL, $absolute = false) {
		global $ctx, $db, $config_domain;

		if (is_null($row)) {
			$row = self::loadNode();
			if ($row === false) {
				debug_log("getPlaceLink() failed at URL '{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}' (referer='".$_SERVER['HTTP_REFERER']."')");
				return "";
			}
		}

		if (isset($row['loc_id']) && $row['loc_id'] == $ctx->location_id) {
			//we are in the same location, we need just path in link
			return $ctx->location_path."/".self::getCategoryUrlNameById($row["place_type_id"])."/".self::getPlaceUrlName($row['name'])."/".$row["place_id"];
		} else {
			//we want link for place and we are not currently in that location where the place is
			$link = "";
			if (array_key_exists("country_sub", $row) && array_key_exists("loc_url", $row) && array_key_exists("dir", $row)) {
				$link = location::getUrlStatic($row["country_sub"], $row["loc_url"], $row["dir"]);
			} else {
				$location = location::findOneById($row["loc_id"]);
				if (!$location)
					return "";
				$link = $location->getUrl();
			}
			$cat_url_name = self::getCategoryUrlNameById($row["place_type_id"]);
			$place_url_name = self::getPlaceUrlName($row['name']);
			if (($cat_url_name === false) || ($place_url_name === false)) {
				debug_log("getPlaceLink() failed: link={$link} cat_url_name=$cat_url_name place_url_name=$place_url_name referer='".$_SERVER['HTTP_REFERER']."'");
				return "";
			}
			$link .= "/$cat_url_name/$place_url_name/".$row["place_id"];

			return $link;
		}
		//we were not able to make link !
		return "#";
	}
		
	public static function getPlaceLinkAux($loc_id, $type, $id, $name) {

		$cat_url_name = self::getCategoryUrlNameById($type);
		$place_url_name = self::getPlaceUrlName($name);
		$location = location::findOneById($loc_id);
		
		if (!$location || !$cat_url_name || !$place_url_name) {
			debug_log("getPlaceLinkAux() failed: loc_id={$loc_id} type={$type}, id={$id}, name={$name} referer='".$_SERVER['HTTP_REFERER']."'");
			return "";
		}
		return $location->getUrl()."/{$cat_url_name}/{$place_url_name}/{$id}";
	}

	/**
	 * Returns link to place in old-school domestic system
	 */
	public static function getPlaceLinkById($id) {
		global $db;

		$res = $db->q("SELECT loc_id, place_type_id, name FROM place WHERE place_id = ?", [$id]);
		if ($db->numrows($res) != 1)
			return null;
	
		$row = $db->r($res);
		return self::getPlaceLinkAux($row["loc_id"], $row["place_type_id"], $id, $row["name"]);
	}

	public static function getPlaceLinkStatic($country_sub, $loc_url, $dir, $type, $id, $name) {

		$location_url = location::getUrlStatic($country_sub, $loc_url, $dir);
		$cat_url_name = self::getCategoryUrlNameById($type);
		$place_url_name = self::getPlaceUrlName($name);
		
		if (!$location_url || !$cat_url_name || !$place_url_name) {
			debug_log("getPlaceLinkstatic() failed: country_sub={$country_sub}, loc_url={$loc_url}, dir={$dir}, type={$type}, id={$id}, name={$name}");
			return "";
		}
		return "{$location_url}/{$cat_url_name}/{$place_url_name}/{$id}";
	}

	public static function getPlaceLinkByOldId($old_id, $place_types) {
		global $db;

		if (!$old_id || !is_array($place_types))
			return null;

		$res = $db->q("
			SELECT p.place_id, p.place_type_id, p.loc_id, p.name, l.country_sub, l.loc_url, l.dir
			FROM place p
			INNER JOIN location_location l on l.loc_id = p.loc_id
			WHERE p.old_id = ? AND p.place_type_id IN ?
			", 
			[$old_id, $place_types]
			);
		if (!$db->numrows($res))
			return null;
		$row = $db->r($res);
		return self::getPlaceLink($row, true);
	}

	/**
	 * Returns link to current category
	 * e.g. http://mexico.adultsearch.com/tijuana/sex-shop
	 */
	public function getCurrentCategoryLink() {
		global $ctx;
		if (($ctx->location_path != NULL) && ($ctx->category_id != NULL)) {
			return $ctx->location_path."/".self::getCategoryUrlNameById();
		}
		//we were not able to make link !
		//return old-style line
		return $ctx->location_path."/?type=".$ctx->category_id;
	}

	/**
	 * Returns breadcrumb HTML for pages handled by new dir class
	 * @TODO: move to URL class ??? (forum things should not be here....)
	 */
	public static function getBreadcrumb() {
		global $ctx, $gBreadCat;

		if ($ctx->page_type == NULL)
			return false;	//we know how to make breadcrumbs only for known types of pages

		$bread = "<a href=\"{$ctx->domain_base_link}/homepage\" title=\"Adult Search Engine\">All Locations</a>";

		if ($ctx->country)
			$bread .= " &raquo; ".$ctx->country_name;

		if ($ctx->state)
			$bread .= " &raquo; <a href=\"/".$ctx->state."\" title=\"".$ctx->state_name."\">".$ctx->state_name."</a>";
		
		if ($ctx->city)
			$bread .= " &raquo; <a href=\"".$ctx->location_path."\" title=\"".$ctx->city_name."\">".$ctx->city_name."</a>";
		
	
		//TODO incorporate neighborhoods into class.url & context
		if (array_key_exists("neighbor_id", $ctx->search)) {
			global $mcache;
			$loc = $mcache->get('SQL:LOC-ID', array($ctx->search['neighbor_id']));
			if ($loc !== false) {
				$neighbor_name = $loc['loc_name'];
				$link = $ctx->location_path;
				if ($ctx->category)
					$link .= "/{$ctx->category}";
				$link .= "/search/neighbor_id/{$ctx->search['neighbor_id']}";
				$bread .= " &raquo; <a href=\"{$link}\" title=\"{$neighbor_name}\">{$neighbor_name}</a>";
			}
		}
		
		if ($ctx->category) {
			$namex = self::getCategoryPlural($ctx->category_name);
			$bread .= " &raquo; <a href=\"".$ctx->location_path."/".$ctx->category."\" title=\"".$namex."\">".$namex."</a>";
		}

		if ($ctx->item != NULL && $ctx->page_type == "item") {
			//business listing page
			$place_name = $ctx->item['name'];
			$bread .= " &raquo; <a href=\"\" title=\"$place_name\" class=\"bold\">$place_name</a>";
		} else if ($ctx->item != NULL && $ctx->page_type == "upload_video") {
			//upload video page for business listing
			$place_name = $ctx->item['name'];
			$place_link = self::getPlaceLink();
			$bread .= " &raquo; <a href=\"{$place_link}\" title=\"{$place_name}\" class=\"bold\">{$place_name}</a>";
		} else if ($ctx->page_type == "worker" && $ctx->node_id != NULL) {
			$link = self::getPlaceLink();
			$place_name = $ctx->item['name'];
			$bread .= " &raquo; <a href=\"".self::getPlaceLink()."\" title=\"$place_name\" class=\"bold\">{$place_name}</a>";
		} else if ($ctx->page_type == "erotic_services_look") {
			if (!empty($ctx->item["phone"]))
				$phone = makeproperphonenumber($ctx->item["phone"]);
			else
				$phone = $ctx->node_id;
			$bread .= " &raquo; <a href=\"{$ctx->domain_link}{$ctx->location_path}/{$ctx->category}/{$ctx->node_id}\" title=\"{$phone}\" class=\"bold\">{$phone}</a>";
		}
		
		//forum - TODO this probably does not belong here
		/*
		if (substr($ctx->page_type, 0, 5) == "forum") {
			$bread .= $ctx->getBreadcrumb();
		}
		*/

		return $bread;
	}

	public function getFilterRemoveLink($search_key) {
		global $ctx;

		$link = $ctx->location_path;
		if ($search_key != "category" && $ctx->category != NULL)
			$link .= "/".$ctx->category;
		$link_search = "";
		foreach($ctx->search as $key => $val) {
			if ($key != $search_key)
				$link_search .= "/$key/$val";
		}
		if (!empty($link_search))
			$link .= "/search".$link_search;
		return $link;
	}
	
	public function getFilterAddLink($filter_key, $filter_value) {
		global $ctx;

		$link = $ctx->location_path;
		if ($ctx->category != NULL)
			$link .= "/".$ctx->category;
		$filter_link = "";
		foreach($ctx->search as $key => $val) {
			if ($key != $filter_key)
				$filter_link .= "/$key/$val";
		}
		$link .= "/search{$filter_link}/{$filter_key}/{$filter_value}";
		return $link;
	}

	private function getFilterTitle($filter_name, $filter_value) {
		global $mcache;

		if ($filter_name == "neighbor_id") {
			$res = $mcache->get("SQL:LOC-ID", array($filter_value));
			if ($res !== false)
				return $res['loc_name'];	
		} else if ($filter_name == "table_shower") {
			return "Table Shower";
		} else if ($filter_name == "sauna") {
			return "Sauna";
		} else if ($filter_name == "jacuzzi") {
			return "Jacuzzi";
		} else if ($filter_name == "truckparking") {
			return "Truck Parking";
		} else if ($filter_name == "private_parking") {
			return "Private Parking";
		} else if ($filter_name == "fourhand") {
			return "4 Hand Massage";
		} else if ($filter_name == "always") {
			return "Open 24/7";
		}

		return $filter_value;
	}

	/*
`	 * Returns array of filters that are shown on directory search page
	 */
	public function getFilters() {
		global $ctx;	
		$filters = array();

		//currently category is also "removable filter", even its not technically part of search part of URL
		if ($ctx->category) {
			$filters[] = array(
					"type" => "type", 
					"id" => $ctx->category_id,
					"name" => $ctx->category_name,
					"link" => $this->getFilterRemoveLink("category"),
					);
		}
		//for all selected search filters
		foreach($ctx->search as $key => $val) {
			$filters[] = array(
					"type" => $key, 
					"id" => $val,
					"name" => $this->getFilterTitle($key, $val),
					"link" => $this->getFilterRemoveLink($key),
					);
				}

		return $filters;
	}

	public function getSearchQueryLink() {
		$search_query_link = $this->getFilterRemoveLink("query");
		if (strpos($search_query_link, "search") === false)
			$search_query_link .= "/search";
		$search_query_link .= "/query";
		return $search_query_link;
	}

	private function getOrderBy() {
		global $ctx;

		//backward compatibility, will be removed
		if ($ctx->order_by == NULL && isset($_GET["order"]))
			$ctx->order_by = preg_replace('/[^A-Za-z0-9_-]+/', '', $_GET["order"]);	//TODO uniform sanitizing ?

		switch ($ctx->order_by) {
		case 'overall':
		case 'review':
		case 'last_review_id':
		case 'video_cnt':
			$order_by = $ctx->order_by;
			break;
		default:
			$order_by = "last_review_id";
			break;
		}

		return $order_by;
	}

	private function getOrderWay() {
		global $ctx;

		//backward compatibility, will be removed
		if ($ctx->order_way == NULL && isset($_GET["orderway"]))
						$ctx->order_way = intval($_GET["orderway"]);

		$order_way = "";
		if ($ctx->order_way != NULL) {
			if ($ctx->order_way == 1)
				$order_way = "DESC";
			else if ($ctx->order_way == 2)
				$order_way = "ASC";
		}
		if (empty($order_way))
			$order_way = "DESC";		//default order way

		return $order_way;
	}

	public function getOrderClause() {
		return $this->getOrderBy()." ".$this->getOrderWay();
	}
	
	public function getSortArray() {
		$arr = array(
			array("title"=> "Rating", "link" => $this->getSortLink("overall"), "selected_way" => $this->getSortSelectedWay("overall")),
			array("title"=> "# of Reviews", "link" => $this->getSortLink("review"), "selected_way" => $this->getSortSelectedWay("review")),
			array("title"=> "Review date", "link" => $this->getSortLink("last_review_id"), "selected_way" => $this->getSortSelectedWay("last_review_id")),
			array("title"=> "Videos", "link" => $this->getSortLink("video_cnt"), "selected_way" => $this->getSortSelectedWay("video_cnt")),
			);
		return $arr;
	}

	private function getSortLink($order_by)	{
		$link_no_order = $this->getLinkWithoutOrder();
		$curr_order_by = $this->getOrderBy();
		$curr_order_way = $this->getOrderWay();
		return $link_no_order."/order/".$order_by."/orderway/".(($order_by == $curr_order_by && $curr_order_way == "DESC") ? 2 : 1);
	}

	private function getLinkWithoutOrder() {
		global $ctx;
		$a = explode("/order/", $ctx->path_original);
				return $a[0];
	}

	private function getSortSelectedWay($order_by)	{
		$curr_order_by = $this->getOrderBy();
		$curr_order_way = $this->getOrderWay();
		if ($order_by == $curr_order_by) {
			if ($curr_order_way == "DESC")
				$selected_way = "descend";
			else
				$selected_way = "ascend";
		} else {
			$selected_way = "";
		}
		return $selected_way;
	}

	private function paging_html($paging, $style, $add_prevnext_text, $ajax) {
		global $mobile;

		if( $mobile == 1 ) {
			$style = false;
			$add_prevnext_text = TRUE;
		}

		$pager ="";

		foreach ($paging as $item) {
			
			$ajax_link = is_array($ajax) ? "onclick=\"_ajax('{$ajax["method"]}', '{$ajax["url"]}', '{$ajax["data"]}&page={$item['page']}', '{$ajax["div"]}');return false;\"" : (!empty($ajax)?$ajax:"");
			if (!empty($ajax_link)) $ajax_link = str_replace("{page}", $item['page'], $ajax_link);

			switch ($item['type']) {
				case 'prev': 
					if (!$add_prevnext_text)
						break;
					if ($style)
						$pager .= "<li class=\"prev-page\"><a href=\"{$item['link']}\" $ajax_link><span>Prev</span></a></li>";
					elseif ($mobile == 1)
						$pager .= "<a href=\"{$item['link']}\" $ajax_link><img src=\"/images/mobile/pager-left.png\" align=\"left\" width=\"40\" height=\"33\" border=\"0\" /></a>";
					else
//						$pager .= "<a href=\"{$item['link']}\" $ajax_link>Prev</a> ";
						$pager .= "<a href=\"{$item['link']}\" $ajax_link>&laquo;</a>";
					break;

				case 'page':
					if( $style )
						$pager .= "<li><a href=\"{$item['link']}\" $ajax_link>{$item['page']}</a></li>";
					else
						$pager .= "<a href=\"{$item['link']}\" $ajax_link>{$item['page']}</a>";
					break;

				case 'dots':
					if ($style)
						$pager .= "<li>...</li>";
					else
						$pager .= " ... ";
					break;

				case 'current':
					if ($style) {
						$pager .= "<li><a class=\"sel\">{$item['page']}</a></li>";
					} else {
						$pager .= "<b>{$item['page']}</b>";
						if (!$item['last'])
//							$pager .= ", ";
							$pager .= " ";
					}
					break;

				case 'next':
					if (!$add_prevnext_text)
						break;
					if ($style)
						$pager .= "<li class=\"next-page\"><a href=\"{$item['link']}\" $ajax_link><span>Next</span></a></li>";
					elseif($mobile==1)
						$pager .= "<a href=\"{$item['link']}\" $ajax_link><img src=\"/images/mobile/pager-right.png\" align=\"right\" width=\"40\" height=\"33\" border=\"0\" /></a>";
					else
//						$pager .= " <a href=\"{$item['link']}\" $ajax_link>Next</a>";
//						$pager .= " <a href=\"{$item['link']}\" class=\"nextPage\" $ajax_link>&nbsp;</a>";
						$pager .= " <a href=\"{$item['link']}\" $ajax_link>&raquo;</a>";
					break;

				default:
					break;
			}
		}

		if ($style)
			$pager = "<ul class=\"paging\">".$pager."</ul>";

		return $pager;
	}

	/**
	 * TODO will be removed after full unification
	 */
	private function build_path($path, $page, $old_style, $query_str) {
		if ($old_style) {
			if (!$page) {
				return $path."?".$query_str;
			} else {
				if (strlen($query_str)) {
					return $path."?".$query_str."&page=".$page;
				} else {
					return $path."?page=".$page;
				}
			}
		} else {
			if ($page)
				return $path."/page/".$page;
			else
				return $path;
		}
	}

	/* 
	 * Latest paging system 
	 */
	public function get_pager_array($total_item, $item_per_page = 0, $path = NULL, $old_style = false) {
		global $ctx;

		//settings		
		$max_num_pages_in_pager = 4;

		//TODO - put to url/context ? - the same is in function paging
		if (!$item_per_page)
			$item_per_page = self::getItemsPerPage();

		$total_pages = ceil($total_item/$item_per_page);
		$on_page = $ctx->page;
		$per_page = $item_per_page;

		if ($max_num_pages_in_pager >= $total_pages) {
			$start_page = 1;
			$end_page = $total_pages;
		} else if ($ctx->page < (ceil($max_num_pages_in_pager/2))) {
			$start_page = 1;
			$end_page = $max_num_pages_in_pager;
		} else if ($ctx->page > ($total_pages - ceil($max_num_pages_in_pager/2))) {
			$start_page = $total_pages - $max_num_pages_in_pager;
			$end_page = $total_pages;
		} else {
			$start_page = $ctx->page - ceil($max_num_pages_in_pager/2);
			$end_page = $start_page + $max_num_pages_in_pager;
			if( $start_page == 0 ) $start_page = 1;
		}

		$from = $item_per_page*($on_page-1) + 1;
		if ($from < 1)
			$from = 1;
		$to = $from + $item_per_page - 1;
		if ($to > $total_item)
			$to = $total_item;

		if ($old_style) {
			parse_str($_SERVER['QUERY_STRING'], $output);
			unset($output["page"]);
			unset($output["filename"]);
			$query_str = http_build_query($output);
		}
		
		$paging = array();

		if ($ctx->page > 1) {
			$prev = $ctx->page - 1;
			$paging[] = array(
					"type" => "prev",
					"page" => $prev,
					//"link" => (($prev == 1) ? $path : $path."/page/$prev"),
					"link" => (($prev == 1) ? $this->build_path($path, false, $old_style, $query_str) : $this->build_path($path, $prev, $old_style, $query_str)),
				);
		}

		if ($start_page > 1) {
			$paging[] = array(
					"type" => "page",
					"page" => 1,
					//"link" => $path,
					"link" => $this->build_path($path, false, $old_style, $query_str),
				);
			if ($start_page > 2) {
				$paging[] = array(
					"type" => "dots"
					);
			}
		}
		
		for ($i = $start_page; $i <= $end_page; $i++) {
			if ($i == $ctx->page) {
				$paging[] = array(
					"type" => "current",
					"page" => $i,
					"last" => (($i == $end_page) ? true : false),
					);
			} else {
				$paging[] = array(
					"type" => "page",
					"page" => $i,
					//"link" => (($i == 1) ? $path : $path."/page/$i"),
					"link" => (($i == 1) ? $this->build_path($path, false, $old_style, $query_str) : $this->build_path($path, $i, $old_style, $query_str)),
				);
			}
		}

		if ($end_page < $total_pages) {
			if ($end_page < ($total_pages - 1)) {
				$paging[] = array(
					"type" => "dots"
					);
			}
			$paging[] = array(
					"type" => "page",
					"page" => $total_pages,
					//"link" => $path."/page/$total_pages",
					"link" => $this->build_path($path, $total_pages, $old_style, $query_str),
				);
		}

		if ($ctx->page < $total_pages) {
			$next = (($ctx->page == NULL) ? 1 : $ctx->page) + 1;	//if page not set, it is first page
			$paging[] = array(
					"type" => "next",
					"page" => $next,
					//"link" => (($next == 1) ? $path : $path."/page/$next"),
					"link" => (($next == 1) ? $this->build_path($path, false, $old_style, $query_str) : $this->build_path($path, $next, $old_style, $query_str)),
				);
				}

		return array("from" => $from, "to" => $to, "pager" => $paging);
	 }
	
	/* 
	 * Lastest paging system 
	 * Use $gFilename and $page pre-defined parameters for 100% cms compatible
	 * style to add it css class options default
	 * add_prevnext_text to show prev next links
	 * query to add $_GET parameter to the links
	 */
	 public function paging($total_item, $item_per_page = 0, $style = false, $add_prevnext_text = TRUE, $path = NULL, $ajax = NULL) {

		$pager_array = $this->get_pager_array($total_item, $item_per_page, $path);

		return $this->paging_html($pager_array["pager"], $style, $add_prevnext_text, $ajax);
	 }

	/**
	 * Get info for markers
	 *
	 * @param array $types
	 * @param int   $limit
	 * @param int   $mile
	 * @param array $loc_array
	 * @return array|\PDOStatement
	 */
	public function getListMarkers($types = array(), $limit = 0, $mile = 10, $loc_array = array()) {
		global $ctx, $account, $db, $sphinx;

		if ($limit > 0) {
			$item_per_page = $limit;
		} else {
			$item_per_page = self::getItemsPerPage();
		}

		$sphinx->reset();
		$this->total_items = 0;

		$start = ((((is_null($ctx->page)) ? 1 : $ctx->page) * $item_per_page) - $item_per_page);
		if ($start < 0) {
			$start = 0;
		}
		$sphinx->SetLimits($start, $item_per_page, 2000);

		// types
		if (!empty($types)) {
			//_d("place_type_id={$ctx->category_id}");
			$sphinx->SetFilter('place_type_id', is_array($types) ? $types : array($types));
		}

		if (!empty($loc_array)) {
			// geo
			$sphinx->SetGeoAnchor('loc_lat', 'loc_long', deg2rad($loc_array['loc_lat']),
				deg2rad($loc_array['loc_long']));
			$sphinx->SetFilterFloatRange('@geodist', 0, ($mile * 1609.344));
//		$sphinx->SetFilter('@id', array($ctx->city_id), true);
			$sphinx->SetSortMode(SPH_SORT_EXTENDED, "@geodist ASC");
//		$sphinx->SetArrayResult(true);

		} else {
			// loc_id
			if ($ctx->location_type == 3) {
				//_d("loc_id={$ctx->city_id}");
				$sphinx->SetFilter('loc_id', array($ctx->city_id));
			} else {
				if ($ctx->location_type == 2) {
					$sphinx->SetFilter('state_id', array($ctx->state_id));
				} else {
					if ($ctx->location_type == 1) {
						$sphinx->SetFilter('country_id', array($ctx->country_id));
					}
				}
			}
		}

		if (!$account->isWorker()) {
			$sphinx->SetFilter('edit', array(1, 2, 3, 5));
		}

		$query = "";
		foreach ($ctx->search as $key => $val) {
			if ($key == "query") {
				$query = $val;
				continue;
			}
			$sphinx->SetFilter($key, array($val));
		}

		$sphinx->SetSortMode(SPH_SORT_EXTENDED, $this->getOrderClause());

		$results = $sphinx->Query($query, "dir");
//_darr($results);
		$this->total_items = $results["total_found"];
		$result			= $results["matches"];

		if (empty($result)) {
			return array();
		}

		$ids = '';
		foreach ($result as $res) {
			//$ids .= $ids ? (",".$res["place_id"]) : $res["place_id"];
			$ids .= $ids ? (",".$res["id"]) : $res["id"];
		}

		$res = $db->q("
			SELECT p.*, r.comment rc, a2.avatar ra, a2.username as ru
			FROM place p
			LEFT JOIN (place_review r INNER JOIN account a2 using (account_id)) on p.last_review_id = r.review_id and r.module = 'place'
			WHERE p.place_id in ($ids) 
			ORDER BY field(p.place_id,$ids)
			");

		return $res;
	}

	/**
	 * main function to change after unification
	 */
	public function getListRows() {
		global $ctx, $account, $db, $sphinx;

		//TODO we can pull the video_cnt from sphinx and dont need to jpin place_picture table!

		$sphinx->reset();
		$this->total_items = 0;

		$item_per_page = self::getItemsPerPage();

		$start = ((((is_null($ctx->page)) ? 1 : $ctx->page) * $item_per_page) - $item_per_page);
		if ($start < 0)
			$start = 0;
		$sphinx->SetLimits($start, $item_per_page, 2000);

		if (!is_null($ctx->category_id)) {
			//_d("place_type_id={$ctx->category_id}");
			$sphinx->SetFilter('place_type_id', array($ctx->category_id));
		}
		if ($ctx->location_type == 3) {
			//_d("loc_id={$ctx->city_id}");
			$sphinx->SetFilter('loc_id', array($ctx->city_id));
		} else if ($ctx->location_type == 2) {
			$sphinx->SetFilter('state_id', array($ctx->state_id));
		} else if ($ctx->location_type == 1) {
			$sphinx->SetFilter('country_id', array($ctx->country_id));
		}
		if (!$account->isWorker()) {
			$sphinx->SetFilter('edit', array(1,2,3,5));
		}

		$query = "";
		foreach ($ctx->search as $key => $val) {
			if ($key == "query") {
				$query = $val;
				continue;
			}
			$sphinx->SetFilter($key, array($val));
		}

		$sphinx->SetSortMode(SPH_SORT_EXTENDED, $this->getOrderClause());

		$results = $sphinx->Query($query, "dir");
		//_darr($results);
		$this->total_items = $results["total_found"];
		$result = $results["matches"];

		if (empty($result))
			return array();

		$ids = '';
		foreach($result as $res) {
			 //$ids .= $ids ? (",".$res["place_id"]) : $res["place_id"];
			 $ids .= $ids ? (",".$res["id"]) : $res["id"];
		}

		$res = $db->q("
			SELECT p.*, r.comment rc, a2.avatar ra, a2.username as ru, 
			(select count(*) from place_picture pp where pp.place_file_type_id = 2 and pp.module = 'place' and pp.id = p.place_id) as video_cnt
			FROM place p
			LEFT JOIN (place_review r INNER JOIN account a2 using (account_id)) on p.last_review_id = r.review_id and r.module = 'place'
			WHERE p.place_id in ($ids) 
			ORDER BY field(p.place_id,$ids)
			");

		return $res;
	}

	public function getTotalItems() {
		return $this->total_items;
	}

	public static function getHourOptions($to = false) {
		$options = [];
		$start = ($to) ? 30 : 0;
		$interval = 30;
		$end = ($to) ? 1440 : 1410;
		for ($min = $start; $min <= $end; $min += 30) {
			$hours = floor($min / 60);
			$minutes = $min - ($hours * 60);
			$ampm = ($min >= 720 && $min < 1440) ? "PM" : "AM";
			//formatting
			if ($hours > 12)
				$hours -= 12;
			if ($hours == 0)
				$hours = 12;
			$options[$min] = str_pad($hours, 2, "0", STR_PAD_LEFT).":".str_pad($minutes, 2, "0", STR_PAD_LEFT)." {$ampm}";
		}
		return $options;
	}

	public static function convertHours($in_minutes) {
		global $ctx;

		if ($in_minutes > 1440)
			$in_minutes -= 1440;

		$ampm = ($in_minutes / 60 >= 24 || $in_minutes / 60 < 12) ? "AM" : "PM";
		while ($in_minutes > 720)
			$in_minutes -= 720;
		$in_minutes = $in_minutes ? (intval($in_minutes / 60) ? intval($in_minutes / 60) : "12") . ":".($in_minutes % 60 == 0 ? "00" : ($in_minutes % 60)) : "12:00";
		return $in_minutes.$ampm;
	}

	private static function getHoursForDay($open, $close) {
		if ($open == 0 && $close == 1440) {
			return "Open 24 Hours";
		} elseif ($open == 0 && $close == 720) {
			return "Open 24 Hours";
		} elseif ($open == -1 && $close == -1) {
			return "Closed";
		} elseif (is_null($open) || is_null($close)) {
			return "---";
		}

		return self::convertHours($open)." - ".self::convertHours($close);
	}

	public static function getHours($byapponly = false) {
		global $ctx;
		$hours = array();
	
		if ($byapponly) {
			$hours["monday"] = "By appointment only";
			$hours["sunday"] = $hours["saturday"] = $hours["friday"] = $hours["thursday"] = $hours["wednesday"] = $hours["tuesday"] = $hours["monday"];
			return $hours;
		}
		
		if ($ctx->item["always"]) {
			$hours["monday"] = "Open 24 Hours";
			$hours["sunday"] = $hours["saturday"] = $hours["friday"] = $hours["thursday"] = $hours["wednesday"] = $hours["tuesday"] = $hours["monday"];
			return $hours;
		}

		$hours["monday"]	= self::getHoursForDay($ctx->item["monday_from"], $ctx->item["monday_to"]);
		$hours["tuesday"]	= self::getHoursForDay($ctx->item["tuesday_from"], $ctx->item["tuesday_to"]);
		$hours["wednesday"] = self::getHoursForDay($ctx->item["wednesday_from"], $ctx->item["wednesday_to"]);
		$hours["thursday"]  = self::getHoursForDay($ctx->item["thursday_from"], $ctx->item["thursday_to"]);
		$hours["friday"]	= self::getHoursForDay($ctx->item["friday_from"], $ctx->item["friday_to"]);
		$hours["saturday"]  = self::getHoursForDay($ctx->item["saturday_from"], $ctx->item["saturday_to"]);
		$hours["sunday"]	= self::getHoursForDay($ctx->item["sunday_from"], $ctx->item["sunday_to"]);

		return $hours;
	}

	public function getLoc() {
		global $ctx;

		if ($ctx->item["zipcode"])
			$zipcode = " ".$ctx->item["zipcode"];
		$loc = "<a href='".$this->getCurrentCategoryLink()."' title='{$ctx->item["loc_name"]}'>{$ctx->item["loc_name"]}</a>{$zipcode}, {$ctx->country_name}";

		return $loc;
	}

	//TODO will be removed after db unification
	public function getPhone() {
		global $ctx;

		$phones = [];

		foreach (['phone1', 'phone2', 'phone3'] as $key) {
			if (!empty($ctx->item[$key])) {
				$phones[] = makeProperPhoneNumber($ctx->item[$key]);
			}
		}

		return implode(' or ', $phones);
	}

	public function getAddressLx() {
		global $ctx;

		if($this->getPhone())
			$addressLx = "<b>".$this->getPhone()."</b><br />";
		$addressLx .= $ctx->item["address1"];
		$addressLx .= $ctx->item["address2"]?" (".$ctx->item["address2"].")":"";
		$addressLx .= "<br />";
		$addressLx .= $ctx->item["building_name"] ? $ctx->item["building_name"]."<br />" : "";
		$addressLx .= $ctx->item["neighborhood_area"] ? $ctx->item["neighborhood_area"]."<br />" : "";
		$addressLx .= $this->getLoc()."<br/>";
		if($ctx->item["website"]) {
			$website_url = $ctx->item["website"];
			if (substr($website_url, 0, 4) != "http")
				$website_url = "http://".$website_url;
//			if (strpos())
			$addressLx .= "<a href=\"{$website_url}\" target=\"_blank\" rel=\"nofollow\" class=\"website\">".$ctx->item["website"]."</a><br/>";
		}
		return $addressLx;
	}

	public function getQuickreadReviews() {
		global $ctx;

		$review = new review("place", $ctx->node_id, $ctx->country." place", "/".$ctx->country."/place", "place");
		return self::insertVideoHtmlForReviewVideos($review->reviews());
	}
		
	//TODO think over, maybe rewrite more nicely
	private static function insertVideoHtmlForReviewVideos($reviews) {
		global $ctx;

		foreach($reviews as $key=>$review) {
			if (!empty($review["videos"])) {
				$video = $review["videos"][0];	//TODO multiple videos in review not supported (need to change class.review too)
				$reviews[$key]["video"] = $video->agetInternalVideoHtmlCode();
				$reviews[$key]["video_edit_link"] = "<a href=\"javascript:void(0)\" onclick=\"return win_popup('/ajax/files/review/{$review["review_id"]}/edit/".$video->getFileId()."', 'Edit video', 650, 300);\">Edit video</a>";
				$reviews[$key]["video_delete_link"] = "<a href=\"javascript:void(0)\" onclick=\"_ajaxc('post', '/ajax/files/review/{$review["review_id"]}/delete/".$video->getFileId()."', {listing: '0'}, 'rev_".$review["review_id"]."_vid');\">Delete video</a>";
			}
		}	
		return $reviews;
	}

	//TODO following functions should be in files class !
	public function getExtraPics($imageurl) {
		global $db, $ctx;

		$rex = $db->q("select place_file_type_id, picture, filename, thumb from place_picture where id = '{$ctx->node_id}' and module = 'place'");
		$module = 'place';

		//TODO add visible = 1 condition
		$pics = array();
		while ($rox = $db->r($rex)) {
			$type = $rox["place_file_type_id"];
			if ($type == 1 || $type == 0 || $type == NULL)	//type = 0 -> extra pictures, NULL - backward compatibility
				$pics[] = array(
//						"picture"  => $rox["picture"],
//						"module"	=> $module, 
//						"m"		=> $rox["filename"], 
//						"filename" => $rox["thumb"],
						"id"	  => $rox["picture"],
						"module"	=> $module, 
						"image"	=> $imageurl."/".$rox["filename"], 
						"thumb"	=> $imageurl."/t/".$rox["thumb"],
						);
		}

		return $pics;
	}
	
	public static function getMapImage() {
		global $db, $ctx;

		$rex = $db->q("select filename from place_picture where id = '{$ctx->node_id}' and module = 'place' and place_file_type_id = 1");
		//TODO add visible = 1 condition

		if ($rox = $db->r($rex)) {
			return $rox["filename"];
		}

		return "";
	}

	public static function getVideo() {
		global $db, $ctx;

		$rex = $db->q("select filename from place_picture where id = '{$ctx->node_id}' and module = 'place' and place_file_type_id = 2 and visible = 1");
		//TODO only first video ? what if we have multiple videos for a place ?
		if ($rox = $db->r($rex)) {
			return $rox["filename"];
		}

		return "";
	}

	public static function getVideosForPlace() {
		global $db, $ctx, $account;
		
		$videos = array();

		$sql = "select * from place_picture where id = '{$ctx->node_id}' and module = 'place' and place_file_type_id = 2";
		if (!$account->isWorker())
			$sql .= " and visible = 1";
		$rex = $db->q($sql);

		if ($db->numrows($rex) == 0)
			return false;

		while ($rox = $db->r($rex)) {
			$videos[] = video::withAll($rox["picture"], $rox["module"], $rox["loc_id"], $ctx->country, $rox["filename"], $rox["thumb"], $rox["name"], $rox["description"], $rox["visible"]);
		}
		return $videos;
	}
	
	public static function agetVideosByIds($ids, $onlyinternal = false) {
		global $ctx;

		$ret_arr = array();

		$sql = "select pp.*, p.place_id, p.name as place_name, l.loc_name as location_name, '{$ctx->country}' as country
				from place_picture pp
				inner join place p on p.place_id = pp.id
				inner join location_location l on p.loc_id = l.loc_id
				where pp.id IN ({$ids}) and pp.module = 'place' and pp.place_file_type_id = 2 and pp.visible = 1";

		if ($onlyinternal)
			$sql .= " and LEFT(pp.filename, 4) <> 'http'";

		return self::agetVideosBySql($sql);
	}
	
	public static function agetVideosByPictures($pictures) {
		global $ctx;

		$sql = "select pp.*, pp.id as place_id, l.loc_name as location_name, '{$ctx->country}' as country
				from place_picture pp
				inner join location_location l on l.loc_id = pp.loc_id
				where pp.picture IN ({$pictures}) and pp.place_file_type_id = 2 and pp.module = 'place' and pp.visible = 1";

		return self::agetVideosBySql($sql);
	}

	private static function agetVideosBySql($sql) {
		global $db;
		$videos = array();		

		$rex = $db->q($sql);
		while ($rox = $db->r($rex)) {
			$videos[] = video::withRow($rox);
		}

		return $videos;
	}

	public static function hasVideo() {
		$video = self::getVideosForPlace();
		if ($video === false)
			return false;
		return true;
	}

	public function displayEditLink() {
		$display_edit_link = false;
		if (intval($_SESSION["account_level"]) > 2)
			$display_edit_link = true;
		return $display_edit_link;
	}
	
	public function getCategoryNameById($cat_id = NULL) {
		global $ctx, $mcache;
		if (is_null($cat_id))
			$cat_id = $ctx->category_id;
		if (is_null($cat_id))
			return false;

		$res = $mcache->get("SQL:PTY-ID", array($cat_id));
		if ($res === false)
			return false;
		return $res['name'];
	}
	
	public function getCategoryParentById($cat_id = NULL) {
		global $ctx, $mcache;
		if (is_null($cat_id))
			$cat_id = $ctx->category_id;
		if (is_null($cat_id))
			return false;

		$res = $mcache->get("SQL:PTY-ID", array($cat_id));
		if ($res === false)
			return false;
		return $res['parent_id'];
	}

	public static function getCategoryUrlNameById($cat_id = NULL) {
		global $ctx, $mcache;

		if (is_null($cat_id))
			$cat_id = $ctx->category_id;
		if (is_null($cat_id))
			return false;

		$res = $mcache->get("SQL:PTY-ID", array($cat_id));
		if ($res === false)
			return false;
		return $res['url'];
	}
	
	public function getCategoryIdByUrlName($cat = NULL) {
		global $ctx, $mcache;

		if (is_null($cat))
			$cat = $ctx->category;
		if (is_null($cat))
			return false;
		if (is_null($ctx->country))
			return false;

		$res = $mcache->get("SQL:PTY-URL", array($cat));
		if ($res === false)
			return false;
		return $res['place_type_id'];
	}

	/*
	 * Returns currency, which is used for fees for that country
	 * */
	public static function getCurrencyCodeByCountry($country = NULL) {
		global $ctx;

		if ($country == NULL)
			$country = $ctx->country;

		switch ($country) {
			case "belgium":
			case "cyprus":
			case "germany":
			case "greece":
			case "netherlands":
				return "EUR";
				break;
			case "brazil":
				return "BRL";
				break;
			case "czechrepublic":
				return "CZK";
				break;
			case "china":
				return "CNY";
				break;
			case "hungary":
				return "HUF";
				break;
			case "malaysia":
				return "MYR";
				break;
			case "mauritius":
				return "MUR";
				break;
			case "philippines":
				return "PHP";
				break;
			case "romania":
				return "RON";
				break;
			case "singapore":
				return "SGD";
				break;
			case "switzerland":
				return "CHF";
				break;
			case "thailand":
				return "THB";
				break;
			case "turkey":
				return "TRY";
				break;
			case "vietnam":
				return "VND";
				break;
			case "costarica":
			case "mexico":
			case "panama":
			case "unitedstates":
				return "USD";
				break;
			case "uk":
				return "GBP";
				break;
			case "ca":
				return "CAD";
				break;
			case "southafrica":
				return "ZAR";
				break;
			case "uae":
				return "AED";
				break;
			default:
				return "USD";
				break;
		}
		return false;
	}

	/**
	 * Returns array of forums for CITY page
	 * If specified array parameter $only_these_forums, then only these forums will be displayed (+ General Talk)
	 */
	public function getForumsForCity($only_these_forums = NULL) {
		global $ctx, $db;
	
		$total = 0;	
		$forums = array();
		$forums_gt = array();

		//TODO move to forum class?
		$res = $db->q("SELECT forum_id, forum_name, forum_topics FROM forum_forum WHERE loc_id = ? ORDER BY forum_topics DESC", [$ctx->location_id]);
		while($row = $db->r($res, MYSQL_ASSOC)) {
			$total += intval($row['forum_topics']);
			if (!empty($only_these_forums) && !in_array($row['forum_name'], $only_these_forums) && $row['forum_id']!=1)
				continue;
			$f = array(
				'name'	 => htmlspecialchars($row['forum_name']),
				'name_raw' => $row['forum_name'],
				'count'	=> intval($row['forum_topics']),
				'link'	 => forum::getForumLink($row['forum_id']),
				);
			if ($row['forum_id'] == 1)
				$forums_gt[] = $f;
			else
				$forums[] = $f;
		}
		$forums = $this->sortCategoryArray($forums, 'name_raw');
		$forums = array_merge($forums_gt, $forums);

		$forum = array(
				'forums' => $forums,
				'total_topics' => $total,
				'forum_index_link' => "{$ctx->location_path}/sex-forum",
				);

		//_darr($forum);
		return $forum;
	}
	
	/**
	 * Returns array of erotic services for CITY page
	 */
	public function getEroticServicesForCity() {
		global $ctx, $db, $sphinx, $config_site_url, $config_image_server, $mobile;

		$eroticservices = false;

		$sphinx->reset();
		if ($ctx->location_type == 3) {
			$sphinx->SetFilter('loc_id', array($ctx->location_id));
		} else if ($ctx->location_type == 2) {
			$sphinx->SetFilter('state_id', array($ctx->location_id));
		}
		if ($mobile == 1)
			$sphinx->SetFilter('sponsor', [1,0]);			//only non-sponsor ads
		else
			$sphinx->SetFilter('sponsor_mobile', [1,0]);	//only non-sponsor ads
		$sphinx->SetGroupBy('type', SPH_GROUPBY_ATTR, '@group desc');
		$sphinx->SetSortMode(SPH_SORT_EXTENDED, "type ASC");
		$results = $sphinx->Query("", "escorts");

		$tmp = array();
		foreach($results["matches"] as $row) {
			$key = "cl{$row["attrs"]["type"]}";
			$val = $row["attrs"]["@count"];
			$tmp[$key] = $val;
		}

		$count = 0;
		$frm = array();
		for ($i = 1; $i <= 16; $i++) {

			//disabling services but FE(1), tstv(2), rubs(6)
			if (!in_array($i, [1,2,6]))
				continue;

			if (array_key_exists("cl{$i}", $tmp)) {
				$frm["cl{$i}"] = $tmp["cl{$i}"];
				$count += intval($tmp["cl{$i}"]);
			} else {
				$frm["cl{$i}"] = 0;
			}
		}
		$eroticservices = array(
			"homelink" => $ctx->location_path,
			"loc_name" => $ctx->location_name,
			"count" => $count?$count:0,
			);
		$eroticservices = array_merge($eroticservices, $frm);

		//get news
		require_once(_CMS_ABS_PATH."/_cms_files/classifieds/array.php");
		$classifieds = new classifieds;
		$form = new form;
		$news = array();
		$now = time();
		$sql = "(SELECT 1 as sortkey, n.images, l.country_sub, l.loc_url, c.type, c.id, c.thumb, c.available, c.ethnicity 
				FROM classifieds_sponsor n 
				INNER JOIN classifieds c on n.post_id = c.id 
				INNER JOIN location_location l on n.loc_id = l.loc_id 
				WHERE n.expire_stamp > {$now}  and n.done > 0 and n.loc_id = '{$ctx->location_id}'
				ORDER BY rand() 
				LIMIT 8) 
				UNION ALL
				(SELECT 2 as sortkey, '' as images, l.country_sub, l.loc_url, c.type, c.id, c.thumb, c.available, c.ethnicity 
				FROM classifieds c
				INNER JOIN (classifieds_loc n INNER JOIN location_location l on n.loc_id = l.loc_id) on c.id = n.post_id 
				WHERE c.deleted IS NULL AND c.done = 1 AND c.bp = 0 AND c.total_loc < 5 AND n.loc_id = '{$ctx->location_id}' AND c.thumb <> ''
				ORDER BY c.type ASC, c.id DESC 
				LIMIT 8)
				ORDER BY sortkey ASC";
//				LIMIT 7";
//		_d("sql='{$sql}'");
		$res = $db->q($sql);
		$real_sponsor_ids = array();
		while ($row=$db->r($res)) {
			if ($row["sortkey"] == 1)
				$real_sponsor_ids[] = $row["id"];
			else if (in_array($row["id"], $real_sponsor_ids))
				continue;

			$avatar = "{$config_site_url}/images/icons/User_1.jpg";
			if (!empty($row['images']))
				$avatar = "{$config_image_server}/classifieds/s/{$row["images"]}";
			else if (!empty($row['thumb']))
				$avatar = "{$config_image_server}/classifieds/{$row["thumb"]}";

			$info1 = $row['available']==1?"Incall":($row['available']==2?"Outcall":"In/Out Call");
			$info2 = $form->arrayValue($ethnicity_array, $row['ethnicity']);
			$info = $info1 . (!empty($info1)&&!empty($info2)?", ":"") . $info2;

			$cat = $classifieds->getModuleByType($row["type"]);
			$cat_name = $classifieds->getcatnamebytype($row["type"]);
			$link = $ctx->domain_link.$ctx->location_path."/".$cat."/".$row["id"];

			$news[] = array(
				"category" => $cat_name,
				"avatar"	=> $avatar,
				"link"	 => htmlspecialchars($link),
				"info"	 => $info
			);

			if (count($news) >= 8)
				break;
		}

		//dallas skype 2.6.2015 - increase city thumbnails from 5 to 7
		//dallas 16.11.2017 - increase from 7 to 8
		$news2 = array();
		if (count($news) > 5) {
			$chunks = array_chunk($news, 5);
			$news = $chunks[0];
			$news2 = $chunks[1];
		}

		$eroticservices["news"] = $news;
		$eroticservices["news2"] = $news2;

//		_darr($eroticservices);
		return $eroticservices;	
	}

	public static function isColSubmit($col_name, $type) {
		switch ($type) {
			case 0: // new place - unknown type
				$submit_cols = array("place_id", "place_type_id", "name", "phone1", "address1", "address2", "loc_id", "zipcode", "website", "facebook", "twitter", "instagram", "email", "monday_from", "image", "edit");
				break;
			case 2: // Sex Shop
				$submit_cols = array( 'place_id', 'name', 'phone1', 'phone2', 'address1', 'address2', 'zipcode', 'website', 'facebook', 'twitter', 'instagram',
									  'always', 'monday_from', 'day1o', 'day1c',
									  'parking',
									  'lingerie', 'adult_toys', 'adult_dvds', 'peep_shows', 'adult_books', 'adult_video_arcade', 'gloryhole',
									  'theaters', 'fetish', 'lgbt', 'buddy_booth_available',
									  'image', 'thumb', 'extra1', 'coupon', 'files_listing');
				break;
			case 15: // Erotic Massage Parlor
				$submit_cols = array( 'place_id', 'name', 'phone1', 'phone2', 'address1', 'address2', 'zipcode', 'email', 'website', 'facebook', 'twitter', 'instagram',
									  'always', 'monday_from', 'day1o', 'day1c',
									  'cc', /*'cc_visa_master',*/ 'cc_visa', 'cc_master', 'cc_ae', 'cc_dis', 'cc_jcb', 'payment_ec',
									  'masseuse',
									  'rate_15', 'rate_30', 'rate_45', 'rate_60', 'rate_90', 'rate_120',
									  'table_shower', 'rate_table_shower',
									  'sauna', 'jacuzzi', 'truckparking', 'private_parking',
									  'image', 'thumb', 'extra1', 'coupon', 'files_listing');
				break;
			case 1: // Strip Club
				$submit_cols = array( 'place_id', 'name', 'phone1', 'phone2', 'address1', 'address2', 'zipcode', 'email', 'website', 'facebook', 'twitter', 'instagram',
									  'always', 'monday_from', 'day1o', 'day1c',
									  'clubtype', 'bar_service', 'food',
									  'lapdance',
									  'vip', 'vipfee_enum', 'privatedance_fee', 'privatedancevip', 'age_18',
									  'image', 'thumb', 'extra1', 'coupon', 'files_listing');
				break;
			default:
				$submit_cols = array("place_id", "name", "phone1", "address1", "address2", "zipcode", "email", "website", "facebook", "twitter", "instagram", "monday_from", "image", 'thumb');
		}
		return in_array($col_name, $submit_cols);
	}

	public static function displayEditCol($col, $form, $worker = false, $type = null) {
		global $ctx;

		$col_name = $col["col_name"];
		$col_title = $col["col_title"];
		$col_datatype = $col["col_datatype"];
		$col_values = $col["col_values"];

		require_once(_CMS_ABS_PATH."/inc/classes/class.data.php");

		if (!$worker && !self::isColSubmit($col_name, $type))
			return;

		$c = NULL;

		switch ($col_name) {
		case 'place_id':
			$c = new CColumnId("place_id", "ID");
			break;
		case 'place_type_id':
			$c = new CColumnEnum("place_type_id", "Type");
			foreach(dir::getTypes() as $key=>$value)
				$c->AddEnumOption($key, $value);
			$c->setcolmandatory(true);
			break;
		case 'name':
			$c = new CColumnString("name", "Name");
			$c->setcolmandatory(true);
			break;
		case 'loc_id':
			$deep = 1;
			if ($ctx->country_has_states)
				$deep = 2;
			if (in_array($ctx->country_id, array(16046,16047,41973))) {
				$deep = 3;
			}
			$c = new CColumnLocation("City", "Location", $deep);
			$c->setLocName($col_name, $col_name, "City");
			$c->country_id = $ctx->country_id;
			if ($deep == 1)
				$c->state_id = $ctx->country_id;
			$c->setcolmandatory(true);
			break;
		case 'always':
			break;
		case 'monday_from':
			$c = new CColumnHour("hours", "Hours of Operation");
			break;
		case 'image':
			$c = new CColumnImage($col_name, "Main Image");
			$c->setMaxDimensions(700, 500);
			$c->setBackup($form->table);
			$c->setPath($form->table);
			$c->setWatermark();
			break;
		case 'thumb':
			$c = new CColumnImage($col_name, "Thumbnail");
			$c->setDimensions(327, 245);
			$c->setPath("{$form->table}/t");
			$c->setWatermark();
			// for empty thumbnail
			$c->setMainImage('image');
			$c->setPathMainImage($form->table);
			break;
		case 'coupon':
			$c = new CColumnImage("coupon", "Coupon");
			$c->setMaxDimensions(700, 500);
			$c->setPath("{$form->table}/c");
			break;
		case 'cc_visa_master':
		case 'cc_visa':
		case 'cc_master':
		case 'cc_ae':
		case 'cc_dis':
		case 'cc_jcb':
		case 'payment_ec':
			$colTitle = "Specify payments taken";
			$optTitle=$col_title;
			$colCC = NULL;
			foreach ($form->columns as $col) {
				if ($col->colType == COL_BOOL && $col->title == $colTitle) {
					$colCC = $col; break;
				}
			}
			if ($colCC == NULL) {
				$c = new CColumnBoolean($col_name, $optTitle);
				$c->setColTitle($colTitle);
			} else {
				$colCC->AddOption($col_name, $optTitle);
			}
			break;
		case 'phone2':
			$c = new CColumnPhone2($col_name, $col_title);
			break;
		case 'phone1':
			$c = new CColumnPhone2($col_name, $col_title);
			$c->setUnique();
			$c->skip_unique = true;
			break;
		case 'address1':
		case 'address2':
		case 'zipcode':
		case 'fax':
		case 'email':
		case 'website':
		case 'facebook':
		case 'twitter':
		case 'instagram':
			$c = new CColumnString($col_name, $col_title);
			break;
		case 'loc_lat':
			$c = new CColumnNumber($col_name, $col_title);
			$c->doubleval = true;
			$c->recheck = true;
			break;
		case 'loc_long':
			$c = new CColumnNumber($col_name, $col_title);
			$c->doubleval = true;
			break;
		default:
			switch ($col_datatype) {
			case 'boolean':
				$c = new CColumnBoolean($col_name, "Yes");
				$c->setColTitle($col_title);
				break;
			case 'boolean3':
				$c = new CColumnBoolean3($col_name, $col_title);
				break;
			case 'number':
				$c = new CColumnNumber($col_name, $col_title);
				break;
			case 'string':
				$c = new CColumnString($col_name, $col_title);
				break;
			case 'text':
				$c = new CColumnText($col_name, $col_title);
				break;
			case 'enum':
				$c = new CColumnEnum($col_name, $col_title);
				$items = explode('|', $col_values);
				foreach ($items as $item) {
					$i = explode('@', $item);
					$c->AddEnumOption($i[0], $i[1]);
				}
				break;
			case 'multi':
				$c = new CColumnMultipleForeign($col_name, $col_title);
				$items = explode('|', $col_values);
				foreach ($items as $item) {
					$i = explode('@', $item);
					$c->AddEnumOption($i[0], $i[1]);
				}
				break;
			case 'fee':
				$c = new CColumnFee($col_name, $col_title);
				$c->country_id = $ctx->country_id;  //TODO move to CColumnFee
				break;
			case 'fee_range':
				$c = new CColumnFeeRange($col_name, $col_title);
				$c->fee_from->country_id = $ctx->country_id;  //TODO move to CColumnFee
				$c->fee_to->country_id = $ctx->country_id;  //TODO move to CColumnFee
				break;
			case 'cover':
				$c = new CColumnCover($col_name);
				//this cover array is taken from cyprus
				//TODO
				$cover_array=array(
1=>"Free", 
12=>"$1.00", 
13=>"$2.00",
14=>"$3.00",
15=>"$4.00",
2=>"$5.00",
16=>"$6.00",
17=>"$7.00",
18=>"$8.00",
19=>"$9.00",
3=>"$10.00",
4=>"$15.00",
5=>"$20.00",
6=>"$25.00",
7=>"$30.00",
8=>"$35.00",
9=>"$40.00",
10=>"$45.00",
11=>"$50.00",
20 => "$60.00",
21 => "$70.00",
22 => "$80.00",
23 => "$90.00",
24 => "$100.00",
25 => "$110.00",
26 => "$120.00",
27 => "$130.00",
28 => "$140.00",
29 => "$150.00",
);
				$c->setCoverArray($cover_array);
				break;
			default:
				//column from table attribute with unknown datatype
				//or column from place table not explicitly mentioned (technical columns...)
				//do nothing, this is not error
				break;
			}
		}
		if (!is_null($c))
			$form->AppendColumn($c);
	}

	/*
	 * Exceptions in naming of some attributes for some countries
	 * @TODO these things should be somewhere in database !!!!
	 */
	public static function getAttributeTitle($attr_name, $attr_title) {
		global $ctx;
		if ($ctx->country == "thailand" && $attr_name == "girl_fee")
			return "Girl Tip";
		if ($ctx->country == "thailand" && $attr_name == "short_time_room_fee")
			return "How much for short time room";

		return $attr_title;
	}

	/**
	 * Return array of quicksearch links for page header
	 */
	public function getQuickSearchLinks() {
		global $ctx, $mcache, $sphinx;

		//_d("loc_type={$ctx->location_type}, loc_id={$ctx->location_id}");
		$qslinks = array();

		//add classifieds categories
		$cl = new classifieds;
		$sphinx->reset();
		if ($ctx->location_type == 3) {
			$sphinx->SetFilter('loc_id', array($ctx->location_id));
		} else if ($ctx->location_type == 2) {
			$sphinx->SetFilter('state_id', array($ctx->location_id));
		}
		$sphinx->SetGroupBy('type', SPH_GROUPBY_ATTR, '@group desc');
		$sphinx->SetSortMode(SPH_SORT_EXTENDED, "type ASC");
		$results = $sphinx->Query("", "escorts");
		//_darr($results);
		foreach($results["matches"] as $row) {
			$title = $cl->getcatnamebytype($row["attrs"]["type"]);
			if (empty($title))
				$title = $row["attrs"]["type"]." ";
			$url = $cl->getModuleByType($row["attrs"]["type"]);
			$qslinks[] = array(
				"link" => $ctx->location_path."/".$url,
				"title" => $title. " (".$row["attrs"]["@count"].")",
				//TODO add "selected" entry
				);
		}

		//add places
		$sphinx->ResetFilters();
		$sphinx->SetFilter('loc_id', array($ctx->location_id));	//basically the filter can be for city or state
		$sphinx->SetGroupBy('place_type_id', SPH_GROUPBY_ATTR, '@group desc');
		$sphinx->SetSortMode(SPH_SORT_EXTENDED, "place_type_id ASC");
		$results = $sphinx->Query("", "dir");
		//_darr($results["matches"]);
		foreach($results["matches"] as $row) {
			$res = $mcache->get("SQL:PTY-ID",array($row["attrs"]["place_type_id"]));
			if ($res === false)
				continue;
			$qslinks[] = array(
				"link" => $ctx->location_path."/".$res["url"],
				"title" => $res["name"]. " (".$row["attrs"]["@count"].")",
				"selected" => ($ctx->category == $res["url"]) ? true : false,
				);
		}

		return $qslinks;
	}			

	public function exportQuickSearch() {
		global $account, $smarty, $ctx;

		$smarty->assign("international", $ctx->international);
//		if ($account->isrealadmin())
			$smarty->assign("qslinks", $this->getQuickSearchLinks());
	}

	/*
	 * Returns number of adult websites
	 * this function will be removed when adult websites will be integrated into places
	 */
	public static function getAdultWebsitesCount() {
		global $db;
		$sql = "SELECT count(*) as cnt from adult_websites where edit > 0";
		$res = $db->q($sql);
		if (!$db->numrows($res))
			return 0;
		$row = $db->r($res);
		return $row["cnt"];
	}

	public static function create_map_image_filename($module, $id) {
		global $config_image_path;
		$filename = "{$id}.png";
		return $filename;
	}
	public static function create_map_image_filepath($module, $id) {
		global $config_image_path;
		$dirpath = $config_image_path."/{$module}/maps";
		if (!is_dir($dirpath)) {
			$ret = @mkdir($dirpath, 0777, true);
			if (!$ret)
				return false;
		}
		$filename = create_map_image_filename($module, $id);
		$filepath = "{$dirpath}/{$filename}";
		return $filepath;
	}
	public static function get_map_image_filepath($module, $map_image) {
		global $config_image_path;
		$dirpath = $config_image_path."/{$module}/maps";
		$filepath = "{$dirpath}/{$map_image}";
		return $filepath;
	}
	public static function get_map_image_url($module, $map_image) {
		global $config_image_server;
		$url = $config_image_server."/{$module}/maps/{$map_image}";
		return $url;
	}
	public static function get_map_image_update_url($module, $id, $lat, $lng) {
		$url = "/dir/updatemap?module={$module}&id={$id}&lat={$lat}&lng={$lng}";
		return $url;
	}

	public static function getFeeOptions($countryId = null, $freeIncluded = true) {

		//default values -> USD
		$moneySign = "$";
		$moneyFormat = "{%moneySign}{%money}.00";
		$start = 5;
		$end = 505;
		$interval = 5;

		if ($countryId == 41973 ) {
			$moneySign = "GBP";
			$moneyFormat = "{%money}.00 {%moneySign}";
		} elseif ($countryId == 41966 ) {
			$moneySign = "TL";
			$moneyFormat = "{%money} {%moneySign}";
		} elseif (in_array($countryId, array(41764, 41823, 41826, 41897, 41954)) ) {
			$moneySign = "EUR";
			$moneyFormat = "{%money} {%moneySign}";
			$start = 5;
			$end = 3000;
			$interval = 5;
		} elseif ($countryId == 41972 ) {
			$moneySign = "AED";
			$moneyFormat = "{%money} {%moneySign}";
		} elseif ($countryId == 41954 ) {
			$moneySign = "CHF";
			$moneyFormat = "{%money} {%moneySign}";
		} elseif ($countryId == 41799 ) {
			$moneySign = "CZK";
			$moneyFormat = "{%money} {%moneySign}";
			$start = 100;
			$end = 10000;
			$interval = 50;
		} elseif ($countryId == 41959 ) {
			$moneySign = "Baht";
			$moneyFormat = "{%money} {%moneySign}";
			$start = 50;
			$end = 20000;
			$interval = 50;
		} elseif ($countryId == 41916 ) {
			$moneySign = "Pesos";
			$moneyFormat = "{%money} {%moneySign}";
			$start = 50;
			$end = 6000;
			$interval = 10;
		} elseif ($countryId == 41875 ) {
			$moneySign = "Ringgit";
			$moneyFormat = "{%money} {%moneySign}";
			$start = 50;
			$end = 6000;
			$interval = 10;
		} elseif ($countryId == 41939 ) {
			$moneySign = "SGD";
			$moneyFormat = "{%money} {%moneySign}";
			$start = 10;
			$end = 1000;
			$interval = 10;
		} elseif ($countryId == 41841) {
			//hungary
			$moneySign = "HUF";
			$moneyFormat = "{%money} {%moneySign}";
			$start = 2500;
			$end = 150000;
			$interval = 2500;
		}

		$options = [];
		if ($freeIncluded)
			$options["-1"] = "Free";
		for ($i = $start; $i < $end; $i += $interval) {
			$ix = number_format($i);
			$value = str_replace("{%moneySign}", $moneySign, $moneyFormat);
			$value = str_replace("{%money}", $ix, $value);
			$options[$i] = $value;
		}

		return $options;
	}

	public static function look_array_add_bool(&$arr, $row, $db_name, $label) {
		if (isset($row[$db_name])) {
			$val = ($row[$db_name] == 1) ? "YES" : "NO";
			$arr[] = array("label" => $label, "val" => $val);
		}
	}
	public static function look_array_add_bool3(&$arr, $row, $db_name, $label) {
		if (isset($row[$db_name]) && ($row[$db_name] > 0)) {
			$val = $row[$db_name] == 2 ? "YES" : "NO";
			$arr[] = array("label" => $label, "val" => $val);
		}
	}
	public static function look_array_add_text(&$arr, $row, $db_name, $label) {
		if (isset($row[$db_name]) && (!empty($row[$db_name]))) {
			$arr[] = array("label" => $label, "val" => $row[$db_name]);
		}
	}
	public static function look_array_add_coupon(&$arr, $row, $db_name, $label) {
		if (isset($row[$db_name]) && (!empty($row[$db_name]))) {
			$arr[] = array("label" => $label, "val" => "<a href='/dir/coupon?coupon={$row[$db_name]}' title='coupon' target='_blank' rel='nofollow'>Click to see</a>");
		}
	}
	public static function look_array_add_multi(&$arr, $row, $attr, $items, $label) {
		$info = "";
		if (isset($row[$attr])) {
			$row_items = explode(',', $row[$attr]);
			foreach ($row_items as $row_item) {
				if (isset($items[$row_item])) {
					if (!empty($info)) {
						$info .= ", ";
					}
					$info .= $items[$row_item];
				}
			}
		}

		if (!empty($info)) {
			$arr[] = array("label" => $label, "val" => $info);
		}
	}
	public static function look_array_add_creditcards(&$arr, $row) {
		$visa = $mastercard = $amex = $discover = $jcb = $electronic_cash = false;
		$info = "";
		if ($row["cc_visa"] == 1 || $row["cc_visa_master"] == 1)
			$info .= "<img width=\"46\" height=\"25\" alt=\"Visa\" src=\"/images/i_visa.png\" style=\"vertical-align: middle;\"/> ";
		if ($row["cc_master"] == 1 || $row["cc_visa_master"] == 1)
			$info .= "<img width=\"46\" height=\"25\" alt=\"Mastercard\" src=\"/images/i_mastercard.png\" style=\"vertical-align: middle;\"/ /> ";
		if ($row["cc_ae"] == 1)
			$info .= "<img width=\"46\" height=\"25\" alt=\"American Express\" src=\"/images/i_amex.png\" style=\"vertical-align: middle;\"/ /> ";
		if ($row["cc_discover"] == 1)
			$info .= "<img width=\"46\" height=\"25\" alt=\"Discover\" src=\"/images/i_discover.png\" style=\"vertical-align: middle;\"/>";
		if ($row["cc_jcb"] == 1)
			$info .= "<img width=\"46\" height=\"25\" alt=\"JCB\" src=\"/images/i_jcb.png\" style=\"vertical-align: middle;\" />";
		if ($row["payment_ec"] == 1)
			$info .= "Electronic cash";
		if (!empty($info))
			$arr[] = array("label" => "Accepted Cards", "val" => $info);
	}
	public static function look_array_add_fee(&$arr, $row, $db_name, $label) {
		global $at_least_one_fee;

		if (isset($row[$db_name]) && ($row[$db_name] != 0)) {
			if ($row[$db_name] == "-1")
				$arr[] = array("label" => $label, "val" => "FREE");
			else {
				$at_least_one_fee = true;
				$arr[] = array("label" => $label, "val" => currency_convert::display_amount(dir::getCurrencyCodeByCountry(), $row[$db_name]));
			}
		}
	}
	public static function look_array_add_fee_range(&$arr, $row, $db_name, $label) {
		global $at_least_one_fee;

		if (isset($row[$db_name]) && ($row[$db_name] != 0)) {
			if ($row[$db_name] == "-1")
				$arr[] = array("label" => $label, "val" => "FREE");
			else {
				$at_least_one_fee = true;
				$parts = explode('|',$row[$db_name]);
				if (count($parts) == 1 || $parts[1] == "")
					$arr[] = array("label" => $label, "val" => currency_convert::display_amount(dir::getCurrencyCodeByCountry(), $parts[0]));
				else {
					//from-to
					$arr[] = array("label" => $label, "val" => currency_convert::display_amount(dir::getCurrencyCodeByCountry(), $parts[0]).", go to ".currency_convert::display_amount(dir::getCurrencyCodeByCountry(), $parts[1]));
				}
			}
		}
	}
	public static function look_array_add_array(&$arr, $row, $db_name, $items, $label) {
		if (isset($row[$db_name]) && (!empty($row[$db_name])) && (!empty($items[$row[$db_name]]))) {
			$arr[] = array("label" => $label, "val" => $items[$row[$db_name]]);
		}
	}
	public static function look_array_add_cover(&$arr, $row, $db_name, $label) {
		global $at_least_one_fee;	//TODO set this one properly

		//this is taken from cyprus
		$cover_array=array(
			1=>"Free",
			12=>"$1.00",
			13=>"$2.00",
			14=>"$3.00",
			15=>"$4.00",
			2=>"$5.00",
			16=>"$6.00",
			17=>"$7.00",
			18=>"$8.00",
			19=>"$9.00",
			3=>"$10.00",
			4=>"$15.00",
			5=>"$20.00",
			6=>"$25.00",
			7=>"$30.00",
			8=>"$35.00",
			9=>"$40.00",
			10=>"$45.00",
			11=>"$50.00",
			20 => "$60.00",
			21 => "$70.00",
			22 => "$80.00",
			23 => "$90.00",
			24 => "$100.00",
			25 => "$110.00",
			26 => "$120.00",
			27 => "$130.00",
			28 => "$140.00",
			29 => "$150.00",
			);

		$form = new form;

		if (!isset($row[$db_name]) || $row[$db_name] == "")
			return;

		$cover2 = json_decode($row[$db_name], true);
		//_darr($cover2);
		$cover = array();
		for($i=1,$l=0; $i<8; $i++,$l++) {
			switch ($cover2[$i]["day"]) {
				case 1: $day = "Monday"; break;
				case 2: $day = "Tuesday"; break;
				case 3: $day = "Wednesday"; break;
				case 4: $day = "Thursday"; break;
				case 5: $day = "Friday"; break;
				case 6: $day = "Saturday"; break;
				case 7: $day = "Sunday"; break;
			}
			$c = $form->arrayValue($cover_array, $cover2[$i]["cover"]);
			if( $c ) {
				$cover[$cover2[$i]["day"]] = array("day"=>$day,"d"=>$i,"cover"=>$c, "before"=>"","bcover"=>"");
			}
		}
		//_darr($cover);

		//complicated cover
		if( empty($cover) ) {
			$multi_cover = "";
			for ($i=1,$l=0; $i<8; $i++,$l++) {
				switch($cover2[$i]["day"]) {
				case 1: $day = "Monday";
				$before = $form->arrayValue($oh_array, $cover2[$i]["before"]["time"]);
				$between_1 = $form->arrayValue($oh_array, $cover2[$i]["between"]["1"]);
				$between_2 = $form->arrayValue($oh_array, $cover2[$i]["between"]["2"]);
				$after = $form->arrayValue($oh_array, $cover2[$i]["after"]["time"]);

				$before_cover = $form->arrayValue($cover_array, $cover2[$i]["before"]["cover"]);
				$between_1_cover = $form->arrayValue($cover_array, $cover2[$i]["between"]["cover"]);
				$after_cover = $form->arrayValue($cover_array, $cover2[$i]["after"]["cover"]);
				break;
				case 2: $day = "Tuesday"; break;
				case 3: $day = "Wednesday"; break;
				case 4: $day = "Thursday"; break;
				case 5: $day = "Friday";
				$before = $form->arrayValue($oh_array, $cover2[$i]["before"]["time"]);
				$between_1 = $form->arrayValue($oh_array, $cover2[$i]["between"]["1"]);
				$between_2 = $form->arrayValue($oh_array, $cover2[$i]["between"]["2"]);
				$after = $form->arrayValue($oh_array, $cover2[$i]["after"]["time"]);

				$before_cover = $form->arrayValue($cover_array, $cover2[$i]["before"]["cover"]);
				$between_1_cover = $form->arrayValue($cover_array, $cover2[$i]["between"]["cover"]);
				$after_cover = $form->arrayValue($cover_array, $cover2[$i]["after"]["cover"]);
				break;
				case 6: $day = "Saturday";
				break;
				case 7: $day = "Sunday"; break;
				}
				if( $before && $before_cover && $after ) {
					$multi_cover .= "<b>$day</b>:<br/>Before $before: $before_cover, ";
					$multi_cover .= "After $after: $after_cover<br>";
				}
			}
		}
		//_darr($cover);

		//create display text from array
		$covex = "";
		$i = 1;
		$current = 0;
		foreach($cover as $c) {
			if( empty($covex) ) $covex = $c["day"];
			else if( $c["cover"] == $cover[$i-1]["cover"] ) {
				// same...
				if( $i == count($cover) ) $covex .= "-{$c["day"]}: {$c["cover"]}";
			} else {
				if( $current != $cover[$i-1]["d"] ) {
					$covex .= "-".$cover[$i-1]["day"];
					$current = $c["d"];
				}
				$covex .= ": ".$cover[$i-1]["cover"] . "<br/>";
				$covex .= $c["day"];
				if( !isset($cover[$i+1]) ) $covex .= ": ".$c["cover"];
			}
			$i = $c["d"];
			$i++;
		}

		if (!empty($covex))
			$arr[] = array("label" => $label, "val" => $covex);
	}

//end class dir
}

