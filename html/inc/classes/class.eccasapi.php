<?php

class ECCASapi {


    function __construct()
    {
			global $db,$ECCApiConfig;
            $this->ECCApiConfig = $ECCApiConfig;
			$this->db = $db;
    }


    /*
    *	Classified Ad Search
	*	Search for an AdultSearch classified based on AdultSearch Ad
	*
	*	@param     AdultSearch adinfo. required: id, ECCASapi_revsearch_url, ECCASapi_revsearch_url_lastrequest, email and/or phone
	*/
    public function revsearch_url($adinfo){

        // Set Default Results
        $revsearch_url['ECCASapi_revsearch_url'] = $adinfo['ECCASapi_revsearch_url'];
        $revsearch_url['ECCASapi_revsearch_url_lastrequest'] = $adinfo['ECCASapi_revsearch_url_lastrequest']; 
    
    	// Require fields 
    	if(!isset($adinfo['id'], $adinfo['ECCASapi_revsearch_url'], $adinfo['ECCASapi_revsearch_url_lastrequest']) AND !($adinfo['email'] OR $adinfo['phone'])){

            // Data for api not given
    		$revsearch_url['rawdata'] = 'Invalid adinfo';
            return $revsearch_url;

    	}

        // Scam limits based on having a url
        $revsearch_url_scantime = $adinfo['ECCASapi_revsearch_url'] ? $this->ECCApiConfig['revsearch_url_scantime_existing'] : $this->ECCApiConfig['revsearch_url_scantime_new'];

        $revsearch_url_timetillnextscan = (($revsearch_url_scantime * 3600) + $adinfo['ECCASapi_revsearch_url_lastrequest']) - time();
    	
        // Due for scan?
    	if($revsearch_url_timetillnextscan <= 0)
    	{
    		// Curl API
    		$ECCASapi_Fields = array(
                'method' => 'url',
    			email  => $adinfo['email'],
    			phone => $adinfo['phone']
    		);
    		$revsearch_url_rawdata = $this->ECCASapi_Curl($ECCASapi_Fields, 'reviews');
            $revsearch_url_result = json_decode($revsearch_url_rawdata); 

            // Successful?
            if($revsearch_url_result->url){
        		$revsearch_url['ECCASapi_revsearch_url'] = $revsearch_url_result->url; 
        		$revsearch_url['ECCASapi_revsearch_url_lastrequest'] = time(); 
            }

            if(!$this->ECCApiConfig['test_mode']){
                // Update Ad
                $this->db->q("UPDATE classifieds
                        SET ECCASapi_revsearch_url = '$revsearch_url[ECCASapi_revsearch_url]',
                        ECCASapi_revsearch_url_lastrequest = '" . time() . "'
                        WHERE id = $adinfo[id]
                        LIMIT 1
                ");
            }

            // Keep raw result for debugging
            $revsearch_url['rawdata'] = json_decode($revsearch_url_rawdata);

    	} else {
            // If they made it this far, they're not due for a scan
            $revsearch_url['rawdata'] = 'Not due for scan for ';
            $revsearch_url['rawdata'] .= round($revsearch_url_timetillnextscan) > 3600 ? round(($revsearch_url_timetillnextscan / 3600)) . ' hours' : round(($revsearch_url_timetillnextscan / 60)) . ' minutes';
        }

        return $revsearch_url;

    }

    protected function ECCASapi_Curl($ECCASapi_Fields,$ECCASapi_Method){

        // Auth
        $ECCASapiauth = array(client=> $this->ECCApiConfig['client'],password=> $this->ECCApiConfig['password']);
       	// General Field Post
		$ECCASapi_CurlFields = array(
		    CURLOPT_URL            => "http://eccie.net/eccvb/api/$ECCASapi_Method/",
		    CURLOPT_POST           => true,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POSTFIELDS     => array_merge($ECCASapiauth,$ECCASapi_Fields)
		);
        
        $ECCASapi_Curl = curl_init();
		curl_setopt_array($ECCASapi_Curl, $ECCASapi_CurlFields);
		$result = curl_exec($ECCASapi_Curl);
		curl_close($ECCASapi_Curl);
		return $result;

	}


}

?>
