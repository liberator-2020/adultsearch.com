<?php

define('ESCORTBIZ_API_URL', 'https://www.escorts.biz/api/create_account');
define('ESCORTBIZ_API_KEY', 'sOPzaMZfvfkgzbAjDvtu');

class escortsbiz {

	private static function non_zero($val) {
		if ($val)
			return $val;
		else
			return "";
	}
	
	private static function non_null($val) {
		if (!is_null($val))
			return $val;
		else
			return "";
	}

	public static function ajax($par1, $par2) {
		global $account;

		if ($par1[0] == "create") {
			$clad_id = intval($par2["ad_id"]);
			debug_log("EBIZ: create_website_request: account_id=".$account->getId().", ad_id={$clad_id}, REQUEST_URI='".$_SERVER["REQUEST_URI"]."'");
			$result = self::create_website_request($clad_id);
			if ($result["status"] == "success")
				$result_html = "Your website has been successfully created, you can view it <a href=\"{$result["link"]}\" target=\"_blank\">here</a>.<br />You can always login to Escorts.biz and change your website by clicking on link in your account page.<br />";
			else
				$result_html = "We apologize, but there has been error creating your website. We are going to investigate this error.";
			debug_log("EBIZ: create_website_request: result=".print_r($result, true).", result_html=".$result_html);
			echo print_r($result, true);
			return;
		}
		
		if ($par1[0] == "create_json") {
			$clad_id = intval($par2["ad_id"]);
			debug_log("EBIZ: create_website_request_json: account_id=".$account->getId().", ad_id={$clad_id}, REQUEST_URI='".$_SERVER["REQUEST_URI"]."'");
			$result = self::create_website_request($clad_id);
			debug_log("EBIZ: create_website_request_json: result=".$result);
			echo json_encode($result);
			return;
		}
	}

	public static function getEbizAdId() {
		global $account, $db;
		
		$res = $db->q("SELECT c.id, CASE WHEN (c.done = 1 OR c.done = 2) THEN c.done WHEN c.done = 0 THEN 3 ELSE 4 END as priority 
						FROM classifieds c 
						WHERE c.account_id = ? AND c.done IN (0,1,2) 
						ORDER BY 2 ASC, date DESC 
						LIMIT 1", array($account->getId()));
		if ($db->numrows($res) != 1)
			return false;
		$row = $db->r($res);
		return intval($row["id"]);
	}

	public static function isEbizAvailable() {
		global $account;
		$account_id = $account->getId();
		$ad_id = self::getEbizAdId();
		if ($ad_id && (in_array($account_id, array(3974, 156889, 156890, 158543,158546)) || $account_id > 100000))
			return $ad_id;
		return false;
	}

	public static function getMyLoginUrl($id = "", $secret = "") {
		global $account;

		if (!$id || !$secret) {
			if (!$account->getEbizId() || !$account->getEbizSecret())
				return false;
			$id = $account->getEbizId();
			$secret = $account->getEbizSecret();
		}

		return "https://www.escorts.biz/api/auto_login/{$secret}/user/{$id}";;
	}

	public static function exportEbizLink() {
		global $smarty;

		//escort.biz website hookups
		if ($url = self::getMyLoginUrl()) {
			//we already have ebiz account, show button with login URL
			$smarty->assign('ebiz_login_url', $url);
		} else {
			if ($ad_id = self::isEbizAvailable()) {
				//we dont have ebiz account yet, but we qualify for ebiz account
				$smarty->assign('ebiz_available', true);
				$smarty->assign('ebiz_ad_id', $ad_id);
			}
		}

		return;
	}

	public static function create_website_request($clad_id) {
		global $db, $account;
		
		$classifieds = new classifieds;

		$res = $db->q("SELECT a.username, a.email as account_email, a.password, a.ebiz_id, a.ebiz_secret, c.*
						FROM classifieds c
						INNER JOIN account a on a.account_id = c.account_id
						WHERE c.id = ? LIMIT 1", array($clad_id));
		if ($db->numrows($res) != 1)
			return array("status" => "error", "detail" => "Can't find ad id#{$clad_id}");
		$row = $db->r($res);

		if ($row["ebiz_id"]) {
			$ebiz_login_url = "https://www.escorts.biz/api/auto_login/{$row["ebiz_secret"]}/user/{$row["ebiz_id"]}";
			return array("status" => "error", "detail" => "You already have account at Escorts.biz", "link" => $ebiz_login_url);
		}

		//permission check = we can create website only for our own ad, or we are admin
		if ($row["account_id"] != $account->getId() && !$account->isAdmin()) {
			return array("status" => "error", "detail" => "You can only create website for your own ads!");
		}

		$hasher = new PasswordHash(8, TRUE);
		$hashed_password = $hasher->HashPassword($row["password"]);

		//content -> profile
		//stay on safe side, deliver only <br /> tags
		$profile = str_replace("<br>", "\n", str_replace("<br/>", "\n", str_replace("<br />", "\n", $row["content"])));
		$profile = nl2br(strip_tags($profile));

		$height = "";
		if ($row["height_feet"] || $row["height_inches"])
			$height = intval($row["height_feet"])."'".intval($row["height_inches"])."\"";

		$langs = explode(',', $row["language"]);
		$lang1 = $lang2 = $lang3 = "";
		if (count($langs) > 0)
			$lang1 = $classifieds->adbuild_language[$langs[0]];
		if (count($langs) > 1)
			$lang2 = $classifieds->adbuild_language[$langs[1]];
		if (count($langs) > 2)
			$lang3 = $classifieds->adbuild_language[$langs[2]];

		$cupsizes = array(1=>"A", 2=>"B", 3=>"C", 4=>"D", 5=>"DD", 6=>"DDD", 7=>"Just Plain Huge");
		$build = array(1=>"Tiny", 2=>"Slim/Slender", 3=>"Athletic", 4=>"Average", 5=>"Curvy", 6=>"BBW");

		$signup_refer_details = "/adbuild/step5?ad_id={$clad_id}";

		$available_to = "";
		if ($row["avl_men"]) {
			$available_to .= (!empty($available_to)) ? ", " : "";
			$available_to .= "Men";
		}
		if ($row["avl_women"]) {
			$available_to .= (!empty($available_to)) ? ", " : "";
			$available_to .= "Women";
		}
		if ($row["avl_couple"]) {
			$available_to .= (!empty($available_to)) ? ", " : "";
			$available_to .= "Couple";
		}
		
		$credit_cards = "";
		if ($row["payment_visa"]) {
			$credit_cards .= (!empty($credit_cards)) ? ", " : "";
			$credit_cards .= "VISA/MasterCard";
		}
		if ($row["payment_amex"]) {
			$credit_cards .= (!empty($credit_cards)) ? ", " : "";
			$credit_cards .= "American Express";
		}
		if ($row["payment_dis"]) {
			$credit_cards .= (!empty($credit_cards)) ? ", " : "";
			$credit_cards .= "Discover";
		}

		$request = array(
			"User.username" => $row["username"],
			"User.password" => $hashed_password,
			"User.site_id" => "escorts.biz",
			"User.partner_id" => "adultsearch",
			"User.signup_refer_details" => $signup_refer_details,
			"User.http_agent" => $_SERVER["HTTP_USER_AGENT"],

			"UserProfile.name" => $row["firstname"],
			"UserProfile.email" => $row["account_email"],
			"UserProfile.profile" => $profile,
			"UserProfile.phone" => $row["phone"],
			"UserProfile.available_to" => $available_to,
			"UserProfile.height" => $height,
			"UserProfile.weight" => self::non_zero($row["weight"]),
			"UserProfile.chest_size" => self::non_zero($row["measure_1"]),
			"UserProfile.waist_size" => self::non_zero($row["measure_2"]),
			"UserProfile.hip_size" => self::non_zero($row["measure_3"]),
			"UserProfile.cup_size" => self::non_null($cupsizes[$row["cupsize"]]),
			"UserProfile.hair_color" => self::non_null($classifieds->adbuild_haircolor[$row["haircolor"]]),
			"UserProfile.eye_color" => self::non_null($classifieds->adbuild_eyecolor[$row["eyecolor"]]),
			"UserProfile.body_type" => $build[$row["build"]],
			"UserProfile.ethnic" => self::non_null($classifieds->adbuild_ethnicity[$row["ethnicity"]]),
			"UserProfile.language1" => $lang1,
			"UserProfile.language2" => $lang2,
			"UserProfile.language3" => $lang3,
			"UserProfile.age" => $row["age"],
			"UserProfile.credit_cards" => $credit_cards,
			);

		$rates = array();
		if ($row["incall_rate_hh"])
			$rates[] = array("rate" => $row["incall_rate_hh"], "session" => "30 minutes", "location" => "Incall");
		if ($row["incall_rate_h"])
			$rates[] = array("rate" => $row["incall_rate_h"], "session" => "1 hour", "location" => "Incall");
		if ($row["incall_rate_2h"])
			$rates[] = array("rate" => $row["incall_rate_2h"], "session" => "2 hours", "location" => "Incall");
		if ($row["incall_rate_day"])
			$rates[] = array("rate" => $row["incall_rate_day"], "session" => "1 day", "location" => "Incall");
		if ($row["outcall_rate_hh"])
			$rates[] = array("rate" => $row["outcall_rate_hh"], "session" => "30 minutes", "location" => "Outcall");
		if ($row["outcall_rate_h"])
			$rates[] = array("rate" => $row["outcall_rate_h"], "session" => "1 hour", "location" => "Outcall");
		if ($row["outcall_rate_hh"])
			$rates[] = array("rate" => $row["outcall_rate_hh"], "session" => "2 hours", "location" => "Outcall");
		if ($row["outcall_rate_day"])
			$rates[] = array("rate" => $row["outcall_rate_day"], "session" => "1 day", "location" => "Outcall");

		$i = 0;
		foreach ($rates as $rate) {
			$request["UserRate.rate{$i}"] = $rate;
			$i++;
		}
	
		$res = $db->q("SELECT cl.loc_id, l.loc_name, lp1.loc_type type1, lp1.loc_name name1, lp2.loc_type type2, lp2.loc_name name2
						FROM classifieds_loc cl
						INNER JOIN location_location l on l.loc_id = cl.loc_id
						LEFT JOIN location_location lp1 on l.loc_parent = lp1.loc_id
						LEFT JOIN location_location lp2 on lp1.loc_parent = lp2.loc_id
						WHERE cl.post_id = ?", array($clad_id));
		$i = 0;
		while ($row = $db->r($res)) {
			$loc = array(
				"loc_id" => $row["loc_id"],
				"city" => $row["loc_name"]
				);
			if ($row["type1"] == 2 && $row["type2"] == 1) {
				$loc["state"] = $row["name1"];
				$loc["country"] = $row["name2"];
			} else if ($row["type1"] == 1) {
				$loc["country"] = $row["name1"];
			}
			$request["UserLocation.location{$i}"] = $loc;
			$i++;
		}

		$res = $db->q("SELECT filename FROM classifieds_image WHERE id = ?", array($clad_id));
		$i = 0;
		while ($row = $db->r($res)) {
			$photo_url = "{$config_image_server}/classifieds/{$row["filename"]}";
			$request["UserPhoto.photo{$i}"] = $photo_url;
			$i++;
		}

		$post_data = array(
			"api_key" => ESCORTBIZ_API_KEY,
			"json_data" => json_encode($request)
			);

		$curl = new curl;
		$ret = $curl->post(ESCORTBIZ_API_URL, $post_data);
		//debug_log("ESCORTBIZ API: result={$ret}, req=".print_r($post_data, true));
		if ($ret === false)
			return self::error($post_data, $ret);	//curl failed

		$response = json_decode($ret, true);
		if (is_null($ret))
			return self::error($post_data, $ret, "Error while decoding JSON !");	//error while decoding json ?
		
		$success = $response["success"];
		if (!$success)
			return self::error($post_data, $ret, "Success variable in response not present or false: '{$secret}' !");	//success variable not in response, or set to false

		$owner_id = intval($response["owner_id"]);
		$secret = $response["secret_key"];

		if (strlen($secret) < 1)
			return self::error($post_data, $ret, "Secret '{$secret}' does not have minimum length!");

		$res = $db->q("UPDATE account SET ebiz_id = ?, ebiz_secret = ? WHERE account_id = ? LIMIT 1", array($owner_id, $secret, $account->getId()));
		$aff = $db->affected($res);
		if ($aff != 1)
			return self::error($post_data, $ret, "UPDATE account failed, secret='{$secret}', account_id='{$account->getId()}'");

		return self::success($post_data, $ret, self::getMyLoginUrl($owner_id, $secret));
	}

	private static function error($post_data, $ret, $error = "") {
		debug_log("ESCORTBIZ API ERROR: result={$ret}, req=".print_r($post_data, true).", error={$error}");
		$ret_arr = json_decode($ret, true);
		if ($ret_arr && strpos($ret_arr["error"], " already exists") === false)
			reportAdmin("Error creating website on Escorts.biz", "", array("error" => $error, "post_data" => $post_data, "ret" => $ret));
		return array("status" => "error");
	}
	
	private static function success($post_data, $ret, $link) {
		debug_log("ESCORTBIZ API SUCCESS: result={$ret}, req=".print_r($post_data, true));
		return array("status" => "success", "link" => $link);
	}

	/**
	 * Called from /mng/ebiz page
	 */
	public static function generate_codes($how_many, $has_website = false) {
		global $db;
		
		$system = new system();
		$now = time();

		$how_many = intval($how_many);
		if (!$how_many)
			return false;

		if ($has_website)
			$where = " AND NOT c.website IS NULL AND c.website <> '' ";
		else
			$where = " AND (c.website IS NULL or c.website = '') ";
	
		$res = $db->q("SELECT a.account_id, a.email, a.username, c.firstname, c.lastname
						FROM account a
						INNER JOIN classifieds c on c.account_id = a.account_id
						LEFT JOIN ebiz_newsletter e on e.account_id = a.account_id
						WHERE e.account_id IS NULL AND c.done = 1 {$where}
						GROUP BY a.account_id, a.email, a.username, c.firstname, c.lastname
						ORDER BY 1 DESC
						LIMIT {$how_many}");
		$results = array();
		while ($row = $db->r($res)) {
			$code = strtolower($system->_makeText(20));
			$account_id = $row["account_id"];
			$email = $row["email"];
			$username = $row["username"];
			$firstname = $row["firstname"];
			$lastname = $row["lastname"];

			$res2 = $db->q("INSERT INTO ebiz_newsletter (account_id, email, code, stamp) VALUES (?, ?, ?, ?)", array($account_id, $email, $code, $now));
			$id = $db->insertid();
			if (!$id)
				continue;

			if ($firstname) {
				$name = $firstname;
				if ($lastname)
					$name .= " {$lastname}";
			} else {
				$name = $username;
			}

			$results[] = array(
				"email" => $email,
				"name" => $name,
				"code" => $code
				);
		}

		return $results;
	}
}

