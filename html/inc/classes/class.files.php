<?php
/**
 * place_files widget
 * Class 
 * @author jay
 */
class files {

	public static $files_table_name = "place_picture";

	public static function getFileTypeName($file_type_id) {
		switch($file_type_id) {
			case 1:
				return "map";
				break;
			case 2:
				return "video";
				break;
			case 3:
				return "other";
				break;
			case NULL:
			case "":
			case 0:
			default:
				return "picture";
				break;
		}
	}

	/**
	 * Ajax server handler of files widget
	 *
	 * /ajax/files/par1/par2?par3=val3&par4=val4
	 *	arg1 = array('par1', 'par2')
	 *	arg2 = array('par3' => 'val3', 'par4' => 'val4')
	 */
	public static function ajax($url_args, $req_args) {
		global $db;

		self::assert_worker_access();	//only for workers

		if ($url_args[0] == "internal_video_tree") {
			echo self::getInternalVideoTreeHtml();
			die;
		}

		if (count($url_args) < 3) {
			echo "Incorrect ajax params!";
			die;
		}

		$module = $url_args[0];
		$place_id = $url_args[1];
		$action = $url_args[2];

		switch ($action) {
			case 'listing':
				echo self::getListingHtml($module, $place_id);
				die;
				break;
			case 'edit':
				if (count($url_args) < 4) {
					echo "Missing file_id param for delete!";
					die;
				}
				$file_id = intval($url_args[3]);
				if (isset($_REQUEST['upl_vid_details']))		//XXX TODO change functions from upl_vid to edit_vid, eventually edit_file
					echo self::updateVideoFormSubmit();
				self::updateVideoFormGet($file_id);
				die;
				break;	
			case 'delete':
				if (count($url_args) < 4) {
					echo "Missing file_id param for delete!";
					die;
				}
				$file_id = intval($url_args[3]);
				$ret = self::delete($file_id);
				if ($ret === false) {
					$html = "<span class=\"error\">Problems deleting file!</span><br />\n";
				} else if (substr($ret, 0, 5) == "Error") {
					$html = "<span class=\"error\">$ret</span><br />\n";
				} else {
					$html = "<span class=\"success\">$ret</span><br />\n";
				}
//				$html .= "<pre>".print_r($_REQUEST, true)."</pre>";
				if ($module == "review") {
					echo "window.location.reload();";
					die;
				}
				if (!isset($_REQUEST["listing"]) || ($_REQUEST["listing"] != "0")) {
					//for example when deleting video for review from business page, we dont need file listing, jsut message
					$html .= self::getListingHtml($module, $place_id);
				}
				echo $html;
				die;
				break;
			case 'add':
				$html = "";
				if (isset($_REQUEST['submit']))
					$html .= self::addFile($module, $place_id);
				$html .= self::getAddHtml($module, $place_id);
				echo $html;
				die;
				break;
			default:
				break;
		}
	}

	private static function getListingHtml($module, $place_id) {
		global $db, $config_image_server, $config_image_path;

		$res = $db->q("select picture as file_id, place_file_type_id, module, filename, thumb, name, description, visible, count_play
			from ".self::$files_table_name."
			where module = '{$module}' and id = '{$place_id}'
			order by orden");
		$files = array();
		while ($row = $db->r($res)) {
			$files[] = array(
				'file_id' => $row['file_id'],
				'type_id' => $row['place_file_type_id'],
				'typename' => self::getFileTypeName($row['place_file_type_id']),
				'module' => $row['module'],
				'filename' => $row['filename'],
				'thumb' => $row['thumb'],
				'name' => $row['name'],
				'description' => $row['description'],
				'visible' => $row['visible'],
				'count_play' => $row['count_play'],
			);
		}

		$html = "";
		$html .= "<table>";
		if (count($files) > 0)
			$html .= "<tr><th>Type</th><th>Thumbnail</th><th>Info</th><th>Visible</th><th /><th /></tr>\n";
		foreach ($files as $file) {
			$html .= "<tr><td>{$file['typename']}</td>";

			$info = "";

			//thumbnail
			if ($file['type_id'] == 0 || $file['type_id'] == 1) {
				//image or map
			
				$res = $db->q("select lc.dir as country from place p 
								left join location_location l on p.loc_id = l.loc_id
								left join location_location lc on l.country_id = lc.loc_id
								where p.place_id = ?",
								[$place_id]);
				$row = $db->r($res);
				$country = $row["country"];
				$info .= "Country: {$country}<br />";
				$dir = $country;
				
				$html .= (!empty($file['thumb'])) ? "<td><img src=\"".$config_image_server."/".$dir."/t/".$file['thumb']."\" /></td>" : "<td />";
				$info .= "Module: {$module}<br />";

				$image_path = $config_image_path."/".$dir."/".$file['filename'];
				$image_url = $config_image_server."/".$dir."/".$file['filename'];
				$info .= "<br />Filename: <a href=\"{$image_url}\" target=\"_new\">{$file['filename']}</a><br />";
				$ret = video::getInfoThumbnail($image_path, $size, $format, $resolution, $output, $error, $error_detail);
				if (!$ret)
					$info .= "Error getting image info!<br />";
				else {
					$info .= "$format, $resolution, $size kB<br />"; 
				}
					 
				$thumb_path = $config_image_path."/".$dir."/t/".$file['thumb'];
				$thumb_url = $config_image_server."/".$dir."/t/".$file['thumb'];
				$info .= "<br />Thumbnail: <a href=\"{$thumb_url}\" target=\"_new\">{$file['thumb']}</a><br />";
				$ret = video::getInfoThumbnail($thumb_path, $size, $format, $resolution, $output, $error, $error_detail);
				if (!$ret)
					$info .= "Error getting thumbnail info!<br />";
				else {
					$info .= "$format, $resolution, $size kB<br />"; 
				}

			} else if($file['type_id'] == 2) {
				//video
				$video = new video($file["file_id"]);
				$video->fetch();
				$html .= "<td>".$video->agetInternalVideoHtmlCode()."</td>";
				$info .= $video->getFilename()."<br/ >"; 
				$info .= "<br />Played: ".intval($file["count_play"])." times";
			} else {
				$html .= "<td/>";
				$info .= $file['filename']."<br/ >";
			}

			//$html .= "<td>{$file['filename']}</td>";
			$html .= "<td>{$info}</td>";
			$html .= ($file['visible']) ? "<td>YES</td>" : "<td>NO</td>";
			$html .= "<td><a href=\"javascript:void(0)\" onclick=\"return win_popup('/ajax/files/{$module}/{$place_id}/edit/{$file['file_id']}', 'Edit extra file', 800, 650);\">Edit</a></td>";
			$html .= "<td><a href=\"javascript:void(0)\" onclick=\"_ajaxc('post', '/ajax/files/{$module}/{$place_id}/delete/{$file['file_id']}', '', 'files_listing');\">Delete</a></td>";
			$html .= "</tr>\n";
		}
		$html .= "</table>\n";
		$html .= "<a href=\"javascript:void(0);\" onclick=\"return win_popup('/ajax/files/{$module}/{$place_id}/add', 'Add File', 800, 650);\">add file</a><br />\n";
		$html .= "<a href=\"javascript:void(0);\" onclick=\"return win_popup('/files/uploadVideo?node_id=$place_id', 'Upload video', 800, 650);\">upload video</a><br />\n";
		return $html;
	}

	/*
	 * Function meant to be called from backend, renders whole HTML for popup
	 */
	public static function uploadVideo() {
		global $ctx;

		self::assert_worker_access();

		//TODO this is kinda hack
		if (($node_id = intval(GetGetParam("node_id"))) > 0)
			$ctx->node_id = $node_id;

		echo "<html><head><title>Upload videos</title>\n";
		echo "<script type=\"text/javascript\" src=\"/js/jquery.js\"></script>\n";
		echo "<script type=\"text/javascript\" src=\"/js/main.js\"></script>\n";
		echo "</head><body>\n";
		echo "<h1>Upload videos</h1>";

		if (isset($_REQUEST["upl_vid_details"])) {
			echo files::updateVideoFormSubmit();
			files::updateVideoFormGet($_REQUEST["upl_vid_file_id"]);
			return;
		}
		
		echo "<em>In case there will be some error during  upload, please <a href=\"javascript:history.go(0)\">refresh</a> this page and try to upload file again. The upload won't be going from the start again, but will be resumed from the checkpoint.</em><br />";

		echo "<div id=\"content\">\n";

		$uploader = GetGetParam("uploader");
		switch($uploader) {
			case 'flash':		 echo swfupload::getHtml();
									break;
			case 'jumploader':	echo jumploader::getJumploaderHtml(400,200);
									  break;
			default:				echo resumable::getHtml();
								break;
		}

		echo "</div>\n";
		echo "</body></html>";
		die;
	}

	public static function deleteAll($module, $place_id) {
		global $db;

		$res = $db->q("select picture from ".self::$files_table_name." where module = '$module' and id = '$place_id'");
		while ($rox = $db->r($res)) {
			self::delete($rox['picture']);		//TODO DB pulling of each place in self::delete is redundant
			//we dont care about success or failure, it is too late when deleting whole business
		}

		return;
	}

	private static function delete($file_id) {
		global $config_image_path, $db;

		//select some information about file to be deleted
		$res = $db->q("select id, place_file_type_id, module, filename, thumb from ".self::$files_table_name." where picture = '".intval($file_id)."'");
		$rox = $db->r($res);
		$id = intval($rox['id']);
		$type_id = intval($rox['place_file_type_id']);
		$module = $rox['module'];
		$filename = $rox['filename'];
		$thumb = $rox['thumb'];
		$file_path = "";
		$thumb_path = "";

        $res = $db->q("select lc.dir as country from place p 
                        left join location_location l on p.loc_id = l.loc_id
                        left join location_location lc on l.country_id = lc.loc_id
                        where p.place_id = ?",
						[$id]
						);
        $row = $db->r($res);
        $dir = $row["country"];

		//check if physiscal files are accessible
		if (self::getFileTypeName($type_id) != "video") {
			$file_path = $config_image_path."/".$dir."/".$filename;
			if (!file_exists($file_path))
				return "Error: can't find file $file_path !";
			if (!empty($thumb)) {
				$thumb_path = $config_image_path."/".$dir."/t/".$thumb;
				if (!file_exists($thumb_path))
					return "Error: can't find thumbnail file $thumb_path !";
			}
		}

		//delete entry
		$sql = "delete from ".self::$files_table_name." where picture = '$file_id' limit 1";
		$db->q($sql);
		if ($db->affected() != 1)
			return "Error: Can't delete db entry for this file !";

		//deleting from disk

		if (self::getFileTypeName($type_id) == "video") {
			//videos are not deleted from disk
			if (substr($filename, 0, 4) == "http")
				return "External video was unassigned from this business.";
			else
				return "Internal video was unassigned from this business (file itself is kept on disk, ask Burak or jay to delete).";
		}

		//other files like : picture, map or other file are going to be deleted from disk !
		if (unlink($file_path) === false)
			return "Error: can't delete file $file_path !";
		if (!empty($thumb_path)) {
			if (unlink($thumb_path) === false)
				return "Error: can't delete thumbnail file $thumb_path !";
		}

		return "File was successfully deleted.";
	}

	private static function getAddHtml($module, $place_id) {
		$html = "";
		$html .= <<< END
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/php_file_tree.js"></script>
<script type="text/javascript">
function onChangeFileType() {
		var type_val = $('select[name="type"]').val();
		if (type_val == 0 || type_val == 1) {
		$('div#file_add').html('<input type="file" name="file" />');
		} else if (type_val == 2) {
		$('div#file_add').html('<input type="radio" name="video_type" value="external" /> External Video (Youtube, ...) <br /><div id="video_external_div" style="display: none;"><em>Please enter complete URL of the video (e.g. http://www.youtube.com/watch?v=v0gwOoKJOJc):</em><br /><input type="textfield" name="video_link_external" style="width: 400px;" /></div> <input type="radio" name="video_type" value="internal" /> Internal Video (Flowplayer) <div id="video_internal_div" style="display: none;"><em>Choose video file from the tree list below:</em><div id="internal_video_tree" style="display: none;" data-initialized="no"><img src="/images/icons/loading.gif" /></div><input type="textfield" name="video_link_internal" style="width: 400px;" /><br /></div>');
			$('input[name="video_type"]').change(function(e) {
					  e.preventDefault();
					  onChangeVideoType();
					  return false;
			});
		} else {
		$('div#file_add').html('<span style="color: red;">Unknown file type.</span><br />');
	}

}

function hookLinks() {
	$("#internal_video_tree li.pft-file a").click(function(e) {
		e.preventDefault();
		$("input[name=video_link_internal]").val($(this).attr("data-path"));
	});
}

function onChangeVideoType() {
		var type_val = $('input[name="video_type"]:checked').val();
		$('input[name="video_link"]').hide();
		$('div#internal_video_tree').hide();
	if (type_val == 'external') {
			$('div#video_external_div').show();
			$('div#video_internal_div').hide();
	} else {
			$('div#video_external_div').hide();
			$('div#video_internal_div').show();
		$('div#internal_video_tree').show();
		if ($('div#internal_video_tree').attr("data-initialized") == "no") {
			$('div#internal_video_tree').attr("data-initialized", "yes");
			_ajax('post', '/ajax/files/internal_video_tree', '', 'internal_video_tree');
		}
	}
}

$(document).ready(function() {
		$('select[name="type"]').change(function(e) {
				 e.preventDefault();
				 onChangeFileType();
				 return false;
		});
		onChangeFileType($('select[name="type"]'));
});

</script>
<link href="/css/phpFileTree/default/default.css" rel="stylesheet" type="text/css" />
<link href="/css/control.css" rel="stylesheet" type="text/css" />
END;
		$html .= "<h3>Add file</h3>\n<form method=\"post\" action=\"\" enctype=\"multipart/form-data\">\nType: <select name=\"type\">";
		for($i = 0; $i < 3; $i++)
			$html .= "<option value=\"$i\">".self::getFileTypeName($i)."</option>";
		$html .= "</select><br />";
		$html .= "<div id=\"file_add\"></div>\n";
		$html .= "<hr />\n";
		$html .= "<input type=\"submit\" name=\"submit\" value=\"Add\" />";
		$html .= "<input type=\"button\" name=\"close\" value=\"Close\" onclick=\"javascript:window.close();\" style=\"margin-left: 200px;\"/></form>";
		return $html;
	}

	private static function addFile($object, $id) {
		$type_id = GetRequestParam("type");
		$type_name = self::getFileTypeName($type_id);

		$module = "place";	
		$place_id = NULL;
		$loc_id = NULL;

		if ($object == "location") {
			$loc_id = $id;
		} else if ($object == "place") {
            $place_id = $id;
        } else {
			echo "Error: unsupported object type: '{$object}' !<br />";
			die;
		}

		switch ($type_name) {
			case 'video':
				$external_file = GetRequestParam("video_link_external");
				$internal_file = GetRequestParam("video_link_internal");
				if (!empty($external_file)) {
					if (strpos($external_file, "http") === false)
						return "<span style=\"error\">Address of external file must be full URL (must contain http://) : $external_file</span><br />";
					//TODO put away directory information only if place or location is in the same country as selected directory
					if (($pos = strpos($external_file, "/")) !== false) {
						$external_file = substr($external_file, $pos+1);
					}
					//TODO probably it is ok
					$ret = self::addFileDb($type_id, $module, $place_id, $external_file, '', 1, $loc_id);
					if ($ret === false)
						return "<span style=\"error\">There has been problem saving external video!</span><br />\n";
					else
						//return "<span style=\"success\">External video has been successfully added (id=$ret).</span><br />\n";
						return self::returnSuccessAndRefresh("External video has been successfully added (id=$ret).");
				} else if (!empty($internal_file)) {
					$file_path = video::getVideoFolder()."/".$internal_file;
					if (!file_exists($file_path))
						return "<span style=\"error\">Internal video file '$file_path' does not exist !</span><br />\n";
					//TODO probably it is ok
					$ret = self::addFileDb($type_id, $module, $place_id, $internal_file, '', 1, $loc_id);
					if ($ret === false)
						return "<span style=\"error\">There has been problem saving internal video!</span><br />\n";
					else
						//return "<span style=\"success\">Internal video has been successfully assigned (id=$ret).</span><br />\n";
						return self::returnSuccessAndRefresh("Internal video has been successfully assigned (id=$ret).");
				} else {
					return "<span style=\"error\">You have to enter either external video URL or internal video file !</span><br />\n";
				}
				break;
			case 'picture':
				$ret = self::uploadImage(0, $module, $place_id, "file", $loc_id);
				if ($ret === false)
					return "<span style=\"error\">There has been problem saving extra picture!</span><br />\n";
				else
					return self::returnSuccessAndRefresh("Extra picture has been successfully assigned (id=$ret).");
				break;
			case 'map':
				$ret = self::uploadImage(1, $module, $place_id, "file", $loc_id);
				if ($ret === false)
					return "<span style=\"error\">There has been problem saving map picture!</span><br />\n";
				else
					return self::returnSuccessAndRefresh("Map picture has been successfully assigned (id=$ret).");
				break;
			default:
				break;
		}
		return "NOT OK";
	}

	private static function backupImage($handle, $module, $id) {
		global $config_image_backup, $db;

		//save image into backup dir
		$handle->Process($config_image_backup);
		if (!$handle->processed)
			return false;

		//store info to DB
		$post = "";
		foreach($_POST as $key => $value)
			$text .= "$key: $value, ";
		$text = get_magic_quotes_gpc() != 1 ? addslashes($text) : $text;
		$db->q("insert into image_backup (`id`, `module`, `filename`, `time`, `post`) values ('$id', '$module', '{$handle->file_dst_name}', now(), '$text')");
		if ($db->affected() != 1)
			return false;
		return true;
	}

	public static function uploadImage($type_id, $module, $place_id, $file_field_name, $loc_id = null) {
		global $config_image_path, $db;

		 $handle = new Upload($_FILES[$file_field_name]);

		 if (!$handle->uploaded)
			return false;

		if (!$place_id && !$loc_id) {
			echo "Error: Missing param p_id or l_id !<br />";
			die;
		}

		//get image dir
		$params = [];
		if ($place_id) {
			$id = $place_id;
        	$sql = "select lc.dir as country from place p 
					left join location_location l on p.loc_id = l.loc_id
					left join location_location lc on l.country_id = lc.loc_id
					where p.place_id = ?";
			$params = [$place_id];
		} else if ($loc_id) {
			$id = $loc_id;
        	$sql = "select lc.dir as country
					FROM location_location l
					LEFT JOIN location_location lc on lc.loc_id = l.country_id
					WHERE l.loc_id = ?";
			$params = [$loc_id];
		}
        $res = $db->q($sql, $params);
        $row = $db->r($res);
        $country = $row["country"];
        $dir = $country;
		if (empty($dir)) {
			echo "Error: cannot resolve upload dir name!<br />";
			die;
		}

		//always make backup
		self::backupImage($handle, $dir, $id);

		list($width, $height, $type, $attr) = getimagesize($handle->file_src_pathname);

		$max_width = 700;
		$max_height = 500;
		$t_width = 220;
		$t_height = 165;
		$path = $dir;
		$t_path = $dir."/t";

		//-----
		//image
		if ($width > $max_width || $height > $max_height) {
			$handle->image_resize = true;
			if ($width > $max_width) {
				$handle->image_x = $max_width;
				$handle->image_ratio_y = true;
			} else {
				$handle->image_y = $max_height;
				$handle->image_ratio_x = true;
			}
		} 
		$handle->file_auto_rename = false;
		$handle->file_overwrite = true;
		$handle->file_new_name_body = self::getRandomImageFilename()."_".$id;
		if (true) {
			//always watermark
			$handle->image_watermark = _CMS_ABS_PATH.'/images/watermark.png';
			$handle->image_watermark_x = 0;
			$handle->image_watermark_y = 0;
		}
		$handle->Process($config_image_path."/".$path);
		$filename = $handle->file_dst_name;

		//-----
		//thumb
		 $handle = new Upload($_FILES[$file_field_name]);
		if ($width != $t_width || $height != $t_height) {
			$handle->image_resize = true;
			$handle->image_ratio_crop = true;
			$handle->image_x = $t_width;
			$handle->image_y = $t_height;
		}
		$handle->image_convert = "jpg";
		$handle->file_new_name_body = self::getRandomImageFilename()."_".$id;

		$handle->Process($config_image_path."/".$t_path);
		$thumb = $handle->file_dst_name;

		$handle->Clean();

		//DB
		return self::addFileDb($type_id, $module, $place_id, $filename, $thumb, 1, $loc_id);

		return true;
	}

	private static function getRandomImageFilename() {
		$m = "abcdefghijklmnoprstuwyxzABCDEFGHIJKLMOPRSTUYW1234567890";
		$s = "";
		for ($i = 0; $i < 20; $i++)
			$s .= $m[rand(0, strlen($m))];
		return $s;
	}


	private static function returnSuccessAndRefresh($msg) {
		$html = "";
		$html .= "<span class=\"success\">$msg</span><br />\n";
		$html .= "<script type=\"text/javascript\">window.opener.FilesRefreshListing();</script>";
		return $html;
	}
	
	private static function returnErrorAndRefresh($msg) {
		$html = "";
		$html .= "<span class=\"error\">$msg</span><br />\n";
		$html .= "<script type=\"text/javascript\">window.opener.FilesRefreshListing();</script>";
		return $html;
	}

	private static function addFileDb($type_id, $module, $id, $filename, $thumb, $visible, $loc_id = NULL) {
		global $db, $account;

		if (is_null($loc_id))
			$loc_id = "NULL";
		else
			$loc_id = "'".intval($loc_id)."'";

		$sql = "INSERT INTO ".self::$files_table_name." 
			(place_file_type_id, id, module, filename, thumb, visible, loc_id, account_id) 
			values
			('$type_id', '$id', '$module', '$filename', '$thumb', '$visible', $loc_id, '".intval($account->isloggedin())."')";
		$res = $db->q($sql);
		if ($db->affected($res) == 1) 
			return $db->insertid();
		else
			return false;
	}

	private static function getInternalVideoTreeHtml() {
		$tree_html = self::php_file_tree(video::getVideoFolder(), "javascript:void(0);", array('mp4','flv'));
		$tree_html .= "<script type=\"text/javascript\">hookLinks();</script>";
		return $tree_html;
	}

	//taken from http://www.abeautifulsite.net/blog/2007/06/php-file-tree/
	private static function php_file_tree($directory, $return_link, $extensions = array()) {
		// Generates a valid XHTML list of all directories, sub-directories, and files in $directory
		// Remove trailing slash
		if( substr($directory, -1) == "/" ) $directory = substr($directory, 0, strlen($directory) - 1);
		$code .= self::php_file_tree_dir($directory, $return_link, $extensions);
		return $code;
	}

	//taken from http://www.abeautifulsite.net/blog/2007/06/php-file-tree/
	private static function php_file_tree_dir($directory, $return_link, $extensions = array(), $first_call = true, $relpath = "") {
		// Recursive function called by php_file_tree() to list directories/files

		// Get and sort directories/files
		$file = scandir($directory);
		natcasesort($file);
		// Make directories first
		$files = $dirs = array();
		foreach($file as $this_file) {
			if( is_dir("$directory/$this_file" ) ) $dirs[] = $this_file; else $files[] = $this_file;
		}
		$file = array_merge($dirs, $files);

		// Filter unwanted extensions
		if( !empty($extensions) ) {
			foreach( array_keys($file) as $key ) {
				if( !is_dir("$directory/$file[$key]") ) {
					$ext = substr($file[$key], strrpos($file[$key], ".") + 1);
					if( !in_array($ext, $extensions) ) unset($file[$key]);
				}
			}
		}

		if( count($file) > 2 ) { // Use 2 instead of 0 to account for . and .. "directories"
			$php_file_tree = "<ul";
			if( $first_call ) { $php_file_tree .= " class=\"php-file-tree\""; $first_call = false; }
			$php_file_tree .= ">";
			foreach( $file as $this_file ) {
				if( $this_file != "." && $this_file != ".." ) {
					$path = ($relpath == "") ? $this_file : "{$relpath}/{$this_file}";
					if( is_dir("$directory/$this_file") ) {
						// Directory
						$php_file_tree .= "<li class=\"pft-directory\"><a href=\"#\">" . htmlspecialchars($this_file) . "</a>";
						$php_file_tree .= self::php_file_tree_dir("$directory/$this_file", $return_link ,$extensions, false, $path);
						$php_file_tree .= "</li>";
					} else {
						// File
						// Get extension (prepend 'ext-' to prevent invalid classes from extensions that begin with numbers)
						$ext = "ext-" . substr($this_file, strrpos($this_file, ".") + 1);
						$link = str_replace("[link]", "$directory/" . urlencode($this_file), $return_link);
						$php_file_tree .= "<li class=\"pft-file " . strtolower($ext) . "\"><a href=\"$link\" data-path=\"$path\" >" . htmlspecialchars($this_file) . "</a></li>";
					}
				}
			}
			$php_file_tree .= "</ul>";
		}
		return $php_file_tree;
	}

	private static function assert_worker_access() {
		global $account;

		if ($account->isWorker() || $account->isrealadmin())
			return true;

		header('HTTP/1.0 403 Forbidden');
		echo "403 Forbidden - Only for workers and superadmins (not logged in? maybe your session has expired, please log back in and try again)";
		die;
		return false;
	}

	public static function uploadVideoResult() {
		global $account;

		$type = $_SESSION["upl_vid_type"];
		$filepath = $_SESSION["upl_vid_filepath"];
		$module = $_SESSION["upl_vid_module"];
		$country = $_SESSION["upl_vid_country"];
		$id = $_SESSION["upl_vid_id"];

		debug_log("files::uploadVideoResult(): type=$type module=$module, id=$id, filepath=$filepath");

		if (empty($module) || empty($id) || empty($filepath))
			self::uploadVideoResult_error("Error: Video upload process did not finish successfully");

		//check whether it is video
		if (!self::isFileVideo($filepath)) {
			self::uploadVideoResult_error("Upload file does not seem to be video file. We will review this file, in case it is correct and valid, we will put it live. Thank you.");
		}

		if ($module == "place")
			$dir = $country;
		else
			$dir = $module;
		$video_folder = video::getVideoFolder($dir, $country);
		if (strpos($filepath, $video_folder) === 0)
			$filename = substr($filepath, strlen($video_folder) + 1);	// + 1 because of additional /
		else
			$filename = basename($filepath);	//safe fallback

		$worker = false;
		if ($account->isWorker())
			$worker = true;

		//TODO sanitize !!!!???
		$loc_id = NULL;
		switch ($type) {
			case "location":
				$loc_id = $id;
				$id = 0;
				break;
			default:
				break;
		}
		$ret = self::addFileDb(2, $module, $id, $filename, '', (($worker) ? 1 : 0), $loc_id);
		if ($ret === false)
			self::uploadVideoResult_error("Error: Video upload process did not finish successfully");

		if ($loc_id == NULL) {
			//place video
			reportAdmin("Upload video success : $filepath for $module,$id", "");
		} else {
			//location video
			$loc = location::findOneById($loc_id);
			if (!$loc)
				reportAdmin("Upload video success, but cant find location by id {$loc_id} !!!", "");
			else
				reportAdmin("Video uploaded for location {$loc->getName()}", "Video file {$filename} was successfully uploaded for location {$loc->getName()}.", array(
					"location link" => "<a href=\"{$loc->getUrl()}\">{$loc->getUrl()}</a>",
					));
		}

		self::uploadVideoResult_ok($ret, $worker);
		die;
	}

	public static function errorUploadVideo($message = "") {
		//for now just log into debug log
		debug_log("files:errorUploadVideo: ".$message);
		die;
	}

	private static function notFileVideo($filepath, $extension, $mime_type, $contains_pattern) {
		$new_filepath = $filepath.".".time().".bak";
		rename($filepath, $new_filepath);
		$link = preg_replace('/.*img\.adultsearch\.net\/html\//', 'http://img.adultsearch.com/', $new_filepath);
		$msg = "File extension was <strong>{$extension}</strong>.<br />";
		$msg .= "Mimetype for $filepath is <strong>$mime_type</strong>.<br />";
		if ($contains_pattern)
			$msg .= "File contains pattern <strong>{$contains_pattern}</strong>.<br />";
		$msg .= "File was renamed due to safety reasons from original '{$filepath}' to '{$new_filepath}'.<br /><br />";
		$msg .= "Please check if <a href=\"{$link}\">file</a> is video file and if it is not, delete the file.<br /><br />";
		reportAdmin("Upload video error - uploaded file is probably not video file", $msg, array(
			"original_path" => $filepath, 
			"new_path" => $new_filepath,
			"extension" => $extension,
			"mime_type" => $mime_type, 
			"contains_pattern" => $contains_pattern,
			"link" => "<a href=\"{$link}\">{$link}</a>",
			"file_id" => ""));
	}

	private static function isFileVideo($filepath) {
		//check extension
		$extension = pathinfo($filepath, PATHINFO_EXTENSION);

		//check mime type
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mime_type = finfo_file($finfo, $filepath);

		//check for dangerous patterns
		$contains_pattern = false;
		$h = @fopen($filepath, "r");
		if ($h) {
			$first_100 = fread($h, 100);
			fseek($h, -100, SEEK_END);
			$last_100 = fread($h, 100);
			fclose($h);
			if ((strpos($first_100, "<?php") !== false) || (strpos($last_100, "<?php") !== false))
				$contains_pattern = "<?php";
		}

		if ((substr($mime_type, 0, 5) != "video") || (in_array($extension, array("php", "php3", "php4", "php5", "phtml", "jsp", "htaccess"))) || $contains_pattern) {
			self::notFileVideo($filepath, $extension, $mime_type, $contains_pattern);
			return false;
		}
		return true;
	}

	private static function uploadVideoResult_error($error_msg) {
		//TODO put CSS into CSS file
		echo <<<END
<style type="text/css">
div#upl_vid_result_error {
	background: #fff9f9;
	background: -moz-linear-gradient(top, #fff9f9 0%, #fdccbf 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fff9f9), color-stop(100%,#fdccbf));
	background: -webkit-linear-gradient(top, #fff9f9 0%,#fdccbf 100%);
	background: -o-linear-gradient(top, #fff9f9 0%,#fdccbf 100%);
	background: -ms-linear-gradient(top, #fff9f9 0%,#fdccbf 100%);
	background: linear-gradient(to bottom, #fff9f9 0%,#fdccbf 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fff9f9', endColorstr='#fdccbf',GradientType=0 );
	color: black;
	padding: 10px;
	border: 1px solid red;
	width: 600px;
	line-height: 1.8em;
}
</style>
<div id="upl_vid_result_error">
<strong style="font-size: 1.2em;">An error has occured:</strong><br />
END;
		echo $error_msg;
		echo "</div>";
		die;
	}

	private static function uploadVideoResult_ok($video_file_id, $worker) {

		//TODO put CSS into CSS file
		echo <<<END
<style type="text/css">
div#upl_vid_result_success {
	background: #e8ffbf;
	background: -moz-linear-gradient(top, #e8ffbf 0%, #bbf98e 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#e8ffbf), color-stop(100%,#bbf98e));
	background: -webkit-linear-gradient(top, #e8ffbf 0%,#bbf98e 100%);
	background: -o-linear-gradient(top, #e8ffbf 0%,#bbf98e 100%);
	background: -ms-linear-gradient(top, #e8ffbf 0%,#bbf98e 100%);
	background: linear-gradient(to bottom, #e8ffbf 0%,#bbf98e 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e8ffbf', endColorstr='#bbf98e',GradientType=0 );
	color: black;
	padding: 10px;
	border: 1px solid green;
	width: 600px;
	line-height: 1.8em;
	margin: auto;
}
</style>
<div id="upl_vid_result_success">
<strong style="font-size: 1.2em;">Video file was uploaded successfully.</strong><br />
END;
		
		if ($worker)
			echo "The video is live on page, please check at <a href=\"".$_SESSION["upl_vid_place_link"]."\">".$_SESSION["upl_vid_place_link"]."</a>";
		else {
			echo "The video is going to be reviewed by our staff and if it is valid, it will be put live. Thank you.";

			//send email to review the video
			$m = "Link to video: <a href=\"".$_SESSION["upl_vid_place_link"]."\">".$_SESSION["upl_vid_place_link"]."</a><br />\n";
			$m .= "Video type: {$_SESSION["upl_vid_type"]}<br />\n";
			$m .= "Country: {$_SESSION["upl_vid_country"]}<br />\n";

			$video = new video($_SESSION["upl_vid_id"]);
			if ($video->fetch()) {
				$video_url = $video->getVideoUrlPath();
				$m .= "Raw link to video: <a href=\"{$video_url}\">{$video_url}</a><br />\n";
			}

			switch ($_SESSION["upl_vid_type"]) {
				case 'place':
					$m .= "Place: <a href=\"http://adultsearch.com/dir/place?id={$_SESSION["upl_vid_id"]}\">#{$_SESSION["upl_vid_id"]}</a><br />\n";
					break;
				case 'location':
					$m .= "Location id: {$_SESSION["upl_vid_id"]}<br />\n";
					break;
				case 'review':
					$m .= "Review id: {$_SESSION["upl_vid_id"]}<br />\n";
					break;
				default:
					$m .= "upl_vid_id: {$_SESSION["upl_vid_id"]}<br />\n";
					break;
			}
			sendEmail(SUPPORT_EMAIL, "New video uploaded by user", $m, ADMIN_EMAIL);

		}
		echo "</div>";
		echo "<div style=\"width: 100%; margin-top: 20px;\"><span style=\"display: block; width: 85px; margin: auto; \"><a href=\"".$_SESSION["upl_vid_place_link"]."\" style=\"font-size: 1.6em;\" >Continue</a></span></div><br/>";

		if (!$worker)
			die;

		//worker only stuff
		//TODO make some template or tidy somehow
		echo "You can see preview and optionally edit video details in the form below.<br /><br />";
		self::updateVideoFormGet($video_file_id);
		die;
	}

	public static function updateVideoFormGet($video_file_id) {
		global $gHtmlHeadInclude, $account;

		$html = "<!DOCTYPE html>";

		$video = new video($video_file_id);
		if (!$video->fetch())
			return false;
		$name = htmlspecialchars($video->getName());
		$description = htmlspecialchars($video->getDescription());
		$thumb_url = $video->getThumbUrlPath();
		$orden = $video->getOrden();

		$html .= "<script type=\"text/javascript\" src=\"/js/jquery.js\"></script>\n";
		$html .= "<script type=\"text/javascript\" src=\"/js/ui/jquery-ui-1.9.1.custom.min.js\"></script>\n";
		$html .= "<link href=\"/css/control.css\" rel=\"stylesheet\" type=\"text/css\" />\n";
		$html .= "<link href=\"/js/ui/start/jquery-ui-1.9.1.custom.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n";

		if ($video->isVisible()) {
			$visible_msg = "This video is currently <span style=\"color: red; font-weight: bold;\">live</span> on site.<br />";
			$visible_checked = "checked=\"checked\"";
			$change_yes_msg = "<strong>YES</strong>, keep this video live";
			$change_no_msg = "<strong>NO</strong>, for now don't display this video on live site";
		} else {
			$visible_msg = "This video is currently <span style=\"color: black; font-weight: bold;\">not live</span> on site.<br />";
			$not_visible_checked = "checked=\"checked\"";
			$change_yes_msg = "<strong>Make</strong> this video live";
			$change_no_msg = "<strong>Keep</strong> this video not live on site";
		}

		$html .= "<form name=\"upl_vid_details\" action=\"\" method=\"post\" enctype=\"multipart/form-data\" >";
		$html .= "<table caption=\"Uploaded video details\" id=\"upl_vid_details_table\">";
		$html .= "<tr><td colspan=\"2\">{$visible_msg}</td><td rowspan=\"5\">".$video->agetInternalVideoHtmlCode()."</td></tr>";
		$html .= "<tr><td colspan=\"2\"><input type=\"radio\" name=\"upl_vid_visible\" value=\"1\" {$visible_checked} />{$change_yes_msg}<br />";
		$html .= "<input type=\"radio\" name=\"upl_vid_visible\" value=\"0\" {$not_visible_checked} />{$change_no_msg}</td></tr>";
		$html .= "<tr><td colspan=\"2\">";
		//testing operation, just for me		
		if ($account->isrealadmin()) {
			$type = ($video->getType() == VIDEO_TYPE_PLACE) ? "place" : "location";
			$object_name = ($video->getType() == VIDEO_TYPE_PLACE) ? $video->getPlaceName() : $video->getLocationName();
			$html .= "This video is for {$type} {$object_name}&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" id=\"change_switch\" name=\"change_object\" />Change ?<br />";
			$html .= "<div id=\"change_div\" style=\"display: none;\" >";
			$html .= "<input type=\"radio\" id=\"change_place\" name=\"object_type\" value=\"place\"/>Place&nbsp;&nbsp;&nbsp;<div id=\"change_place_div\" style=\"display: none;\">place_id:<input type=\"text\" name=\"place_id\" value=\"\" /></div><br />";
			$html .= "<input type=\"radio\" id=\"change_location\" name=\"object_type\" value=\"location\" />Location&nbsp;&nbsp;&nbsp;<div id=\"change_location_div\" style=\"display: none;\">Location:".location::getLocationSelect("location","loc_id")."</div>";
			$html .= "</div>";
		}
		$html .= "</td></tr>";
		$html .= "<tr><td><label for=\"upl_vid_name\">Name</label></td><td><input type=\"text\" name=\"upl_vid_name\" value=\"{$name}\" style=\"width: 250px;\" /></td></tr>";
		$html .= "<tr><td><label for=\"upl_vid_description\">Description</label></td><td><textarea rows=\"10\" cols=\"30\" name=\"upl_vid_description\">{$description}</textarea></td></tr>";
		$html .= "<tr><td><label for=\"upl_vid_thumbnail\">Screenshot</label></td><td class=\"video_ss\" style=\"background-color: #FFB300;\">";

		if ($thumb_url)
			$html .= "<input type=\"radio\" name=\"thumb\" value=\"current\" checked=\"checked\" /><span class=\"ss_time_label\">Current image</span><img src=\"{$thumb_url}\" width=\"200\" /><br /></td>";

		$html .= "<td class=\"video_ss\">";
		$t1 = $video->grabThumbnail("0.1");
		if ($t1 !== false) {
			$t1_url = video::getStageUrlFolder($module)."/".$t1;
			$html .= "<input type=\"radio\" name=\"thumb\" value=\"{$t1}\" /><span class=\"ss_time_label\">Time 0.1s</span><img src=\"{$t1_url}\" width=\"200\" /><br />";
		}
		$html .= "</td></tr><tr><td/><td class=\"video_ss\">";
		$t2 = $video->grabThumbnail("1");
		if ($t2 !== false) {
			$t2_url = video::getStageUrlFolder()."/".$t2;
			$html .= "<input type=\"radio\" name=\"thumb\" value=\"{$t2}\" /><span class=\"ss_time_label\">Time 1s</span><img src=\"{$t2_url}\" width=\"200\" /><br />";
		}

		$html .= "</td><td class=\"video_ss\">";
		$t3 = $video->grabThumbnail("10");
		if ($t3 !== false) {
			$t3_url = video::getStageUrlFolder()."/".$t3;
			$html .= "<input type=\"radio\" name=\"thumb\" value=\"{$t3}\" /><span class=\"ss_time_label\">Time 10s</span><img src=\"{$t3_url}\" width=\"200\" /><br />";
		}

		$html .= "</td></tr><tr><td/><td colspan=\"2\" class=\"video_ss\">";
		$html .= "<input type=\"radio\" id=\"thumb_custom\" name=\"thumb\" value=\"custom\" />Upload own image: <input type=\"file\" name=\"upl_vid_thumbnail\" /></td></tr>";
		$html .= "<tr><td colspan=\"3\"><em>Note: The screnshot will be used as an image before playing the video.</em></td></tr>";

		if ($account->isrealadmin())
			$html .= "<tr><td colspan=\"3\">Orden:<input type=\"text\" name=\"orden\" size=\"5\" value=\"{$orden}\" /></td></tr>";

		$html .= "<tr><td colspan=\"3\">";
		$html .= "<input type=\"hidden\" name=\"upl_vid_file_id\" value=\"{$video_file_id}\" />";
		$html .= "<input type=\"submit\" name=\"upl_vid_details\" value=\"Save video details\" />";
		$html .= "</td></tr></table>";
		$html .= "</form>\n";
$html .= <<<END
<script type="text/javascript">
$('#change_switch').click(function() {
	if ($(this).is(':checked'))
		$('#change_div').show();
	else
		$('#change_div').hide();
});
$('input[name="object_type"]').click(function() {
	if ($(this).attr('id') == "change_place") {
		$('#change_place_div').css('display', 'inline-block');
		$('#change_location_div').hide();
	} else {
		$('#change_place_div').hide();
		$('#change_location_div').css('display', 'inline-block');
	}		
});
$('input[type="file"]').click(function() {
  $("#thumb_custom").click();
  $('td.video_ss').css('background-color', 'white');
  $('td.video_ss:has(input[type="radio"]:checked)').css('background-color', '#FFB300');
});
$('td.video_ss input[type="radio"]').click(function() {
  $('td.video_ss').css('background-color', 'white');
  $('td.video_ss:has(input[type="radio"]:checked)').css('background-color', '#FFB300');
});
</script>
END;

		echo $gHtmlHeadInclude;
		echo $html;
	}

	public static function updateVideoFormSubmit() {
		global $account, $db;

		$error = "";

		if (!isset($_REQUEST["upl_vid_details"]))
			return false;

		if (!$account->isWorker() && !$account->isrealadmin())
			return false;

		if (($id = intval(GetRequestParam("upl_vid_file_id"))) == 0)
			return false;

		$video = new video(GetRequestParam("upl_vid_file_id"));
		if (!$video->fetch())
			return false;
		$module = $video->getModule();

		$update_clausule = "";

		$visible = GetRequestParam("upl_vid_visible");
		if ($visible === "0" || $visible === "1") {
			if (!empty($update_clausule)) $update_clausule .= ", ";
			$update_clausule .= " visible = '{$visible}'";
		}

		if ($_REQUEST["change_object"] == "on") {
			$place_id = intval($_REQUEST["place_id"]);
			$loc_id = intval($_REQUEST["loc_id"]);
			if ($_REQUEST["object_type"] == "place") {
				if (!empty($update_clausule)) $update_clausule .= ", ";
				$update_clausule .= " module = 'place', id = '{$place_id}', loc_id = NULL ";
			} else if ($_REQUEST["object_type"] == "location") {
				if (!empty($update_clausule)) $update_clausule .= ", ";
				$update_clausule .= " module = 'place', id = NULL, loc_id = '{$loc_id}' ";
			}
		}

		if (isset($_REQUEST["orden"])) {
			$orden = (intval($_REQUEST["orden"]) > 0) ? intval($_REQUEST["orden"]) : "NULL";
			if (!empty($update_clausule)) $update_clausule .= ", ";
			$update_clausule .= " orden = {$orden} ";
		}

		$name = GetRequestParam("upl_vid_name");
		if (!empty($update_clausule)) $update_clausule .= ", ";
		$update_clausule .= " name = '{$name}'";
		
		$description = GetRequestParam("upl_vid_description");
		if (!empty($update_clausule)) $update_clausule .= ", ";
		$update_clausule .= " description = '{$description}'";

		$handle = new upload($_FILES['upl_vid_thumbnail']);
		if ($handle->uploaded) {
			$handle->file_new_name_body = $id;

			//check if resolution is the same as video
			$image_res = "{$handle->image_src_x}x{$handle->image_src_y}";
//			_d("imr=$image_res");
			$ret = video::getInfoVideo($video->getVideoAbsPath(), $vc, $video_res, $ac, $o, $err, $err_det);
//			_d("vi:$ret res=$vr");
			if ($video_res != "" && $image_res != $video_res) {
				$error = "The resolution of screenshot ({$image_res}) is not the same as video resolution ({$video_res}), please upload correct screenshot !";
			} else {

				if ($video_res == "") {
					echo "The resolution of screenshot ({$image_res}) is not the same as video resolution ({$video_res}) !";
				}
				//echo "image type = ".$handle->file_src_mime."<br />";
				//if image is PNG, convert to JPG (smaller filesize)
				if ($handle->file_src_mime == "image/png") {
					$handle->image_convert = 'jpg';
					$handle->jpeg_quality = 85;
				}

				$handle->process($video->getFolderAbsPath());
				if ($handle->processed) {
					//echo 'Video screenshot uploaded.<br />';
					
					$thumb = $handle->file_dst_name;
					if (!empty($update_clausule)) $update_clausule .= ", ";
					$update_clausule .= " thumb = '{$thumb}'";
					
					$handle->clean();
				} else {
					//echo 'Video screenshot upload error : ' . $handle->error . '<br />';
					return "<span style=\"error\">Error while uploading video screenshot: ".$handle->error." !</span><br />\n";
				}
			}
		} else if (GetRequestParam("thumb") != "current") {
			debug_log("Video thumbnail chosen from options");
			$thumb_filename = GetRequestParam("thumb");
			$thumb_filepath = video::getStageDir().$thumb_filename;
			$tgt_filename = $id.".jpg";
			$tgt_filepath = $video->getFolderAbsPath()."/".$tgt_filename;
			if (is_file($thumb_filepath)) {
				if (rename($thumb_filepath, $tgt_filepath)) {
					debug_log("Moved video thumbnail '$thumb_filepath' to '$tgt_filepath'");
					if (!empty($update_clausule)) $update_clausule .= ", ";
											$update_clausule .= " thumb = '{$tgt_filename}'";
				} else {
					debug_log("Error moving video thumbnail '$thumb_filepath' to '$tgt_filepath'");
				}
			} else {
				debug_log("Error: video thumbnail file '$thumb_filepath' does not exist!");
			}
		}
		//debug_log("GetRequestParam(\"thumb\") = ".GetRequestParam("thumb"));
	
		if (!empty($update_clausule)) {
			$sql = "update place_picture set {$update_clausule} where picture = '{$id}'";
			debug_log("upd vid sql = '{$sql}'");
			$db->q($sql);
			$aff = $db->affected();
			if (($aff != 1) && ($aff != 0)) {
				//echo "Error while saving video detail information !<br />";
				return "<span style=\"error\">Error while saving video information !</span><br />\n";
			} else {
				if (!empty($error))
					return self::returnErrorAndRefresh($error);
				else
					return self::returnSuccessAndRefresh("Video information has been successfully updated.");
			}
		}

	}
}

