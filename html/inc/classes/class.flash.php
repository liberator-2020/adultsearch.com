<?php

define(FLASH_MSG_SESSION_VAR_NAME, "flash_messages");

/**
 * Class flash is used for displaying flash messages - one time message usually used in admin interface
 * @author jay
 */
class flash {

	//flash message types
	const MSG_DEFAULT =  0;		//default is currently rendered as INFO
	const MSG_ERROR   = -1;
	const MSG_WARNING = -2;
	const MSG_SUCCESS =  1;
	const MSG_INFO    =  2;

	private static $request_identifier = NULL;


	private function getReqId() {
		if (is_null(self::$request_identifier))
			self::$request_identifier = uniqid();

		return self::$request_identifier;
	}

	/**
	 * adds message of defined type. content can be HTML
	 */
	public static function add($type, $content) {

		if (!in_array($type, array(self::MSG_DEFAULT, self::MSG_ERROR, self::MSG_WARNING, self::MSG_SUCCESS, self::MSG_INFO)))
			$type = self::MSG_DEFAULT;

		//TODO some check for malicious HTML in content ???

		$msg = array(
			"req_id" => self::getReqId(),
			"type" => $type,
			"content" => $content,
			);

		if (empty($_SESSION[FLASH_MSG_SESSION_VAR_NAME])) {
			$_SESSION[FLASH_MSG_SESSION_VAR_NAME] = array($msg);
		} else {
			$_SESSION[FLASH_MSG_SESSION_VAR_NAME][] = $msg;
		}

		return true;
	}
	
	private function getTypeCode($type) {
		switch ($type) {
			case self::MSG_ERROR:   return "error";   break;
			case self::MSG_WARNING: return "warning"; break;
			case self::MSG_SUCCESS: return "success"; break;
			case self::MSG_INFO:    return "info";    break;
		}
		return "info";
	}

	/**
	 * returns HTML for displaying all messages, this HTML code is supposed to be used directly in index.tpl template
	 */
	public static function getDisplayHtml() {
		$html = "";
		//$smarty = GetSmartyInstance();

		//display flash messages from last request
		foreach ($_SESSION[FLASH_MSG_SESSION_VAR_NAME] as $msg) {
			if ($msg["req_id"] == self::getReqId())
				continue;
			//$smarty->assign("type", self::getTypeCode($msg["type"]));
			//$smarty->assign("content", $msg["content"]);
			//$html .= $smarty->fetch(_CMS_ABS_PATH."/templates/flash_message.tpl")."\n";
			$html .= "<div class=\"flash_msg flash_msg_".self::getTypeCode($msg["type"])."\">".$msg["content"]."</div>";
		}

		//keep in session only flash messages from current request
		$only_new = array();
		foreach ($_SESSION[FLASH_MSG_SESSION_VAR_NAME] as $msg) {
			if ($msg["req_id"] != self::getReqId())
				continue;
			$only_new[] = $msg;
		}
		$_SESSION[FLASH_MSG_SESSION_VAR_NAME] = $only_new;

		return $html;
	}

}

