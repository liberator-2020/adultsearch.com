<?php
/**
 * class Form
 * Implementation of Form class (various functions for drawing html forms, ...)
 *
 * @package Core-Libraries
 * @subpackage Form
 */
class Form {
	
	var $javascript, $hidden, $row;

	function hidden($name, $value) {
		$this->hidden .= "<input type='hidden' name='$name' value='$value' />\n";
	}
	
	/**
	 * Method GetTextBox() returns HTML code of text input box
	 *
	 * @param string $name input box name
	 * @param string $value predefined value of textbox
	 * @param integer $size size of input box
	 * @return string HTML code of input box
	 */
	function GetTextBox($name, $value, $size = 60) {
		if (empty($value))
			$value = isset($this->row[$name]) ? $this->row[$name] : $default_value;
		return "<input type=\"text\" name=\"".htmlspecialchars($name)."\" size=\"{$size}\" value=\"".htmlspecialchars($value)."\" />\n";
	}
	
	function GetPhoneBox($name, $value, $size = 15) {
		if (empty($value))
			$value = isset($this->row[$name]) ? makeproperphonenumber($this->row[$name]) : $default_value;
		return "<input type=\"text\" name=\"".htmlspecialchars($name)."\" size=\"{$size}\" value=\"".htmlspecialchars($value)."\" />\n";
	}

	/**
	 * Method GetPasswordBox() returns HTML code of password input box
	 *
	 * @param string $name input box name
	 * @param string $value predefined value of password box
	 * @param integer $size size of input box
	 * @return string HTML code of input box
	 */
	function GetPasswordBox($name, $value, $size = 60) {
		if (empty($value))
			$value = $default_value;
		return "<input type=\"password\" name=\"".htmlspecialchars($name)."\" size=\"{$size}\" value=\"".htmlspecialchars($value)."\" />\n";
	}

	/**
	 * Method GetTextArea() returns HTML code of text input box
	 *
	 * @param string $name input box name
	 * @param string $value predefined value of textarea
	 * @param integer $cols width of textarea
	 * @param integer $rows height of textarea
	 * @return string HTML code of input box
	 */
	function GetTextArea($name, $value, $cols = 60, $rows = 5) {
		if (empty($value))
			$value = $default_value;
		return "<textarea name=\"".htmlspecialchars($name)."\" cols=\"{$cols}\" rows=\"{$rows}\" >".htmlspecialchars($value)."</textarea>\n";
	}

	/**
	 * Method GetRadioBox() returns HTML code of radio box
	 *
	 * @param string $name input box name
	 * @param string $value predefined value of textbox
	 * @param boolean $checked checked ?
	 * @return string HTML code of radio box
	 */
	function GetRadioBox($name, $value, $checked = false) {
		$html = "<input type=\"radio\" name=\"".htmlspecialchars($name)."\" value=\"".htmlspecialchars($value)."\" ";
		if ($checked)
			$html .= "checked=\"checked\" ";
		$html .= "/>\n";
		return $html;
	}

	/**
	 * Method GetRadioBoxes() returns HTML code of multiple radio boxes
	 *
	 * @param string $name input box name
	 * @param string $value predefined value of textbox
	 * @param mixed $options | delimited string of select options or two dimensional array of all options (value & name)
	 * @return string HTML code of radio box
	 */
	function GetRadioBoxes($name, $value, $options) {
		$html = "";
		if (is_string($options)) {
			$temp = explode("|", $options);
			$options = array();
			foreach ($temp as $tmp) {
				$options[] = array($tmp, $tmp);
			}
		}
		$count = 1;
		foreach ($options as $option) {
			if( is_array($option) ) {
				foreach($option as $k => $v)
					$html .= $this->GetRadioBox($name, $k, ($k == $value) ? true : false)." ".htmlspecialchars($v)."<br />\n";
			} else {
				$html .= $this->GetRadioBox($name, $count, ($count == $value) ? true : false)." ".htmlspecialchars($option)."<br />\n";
				$count++;
			}
		}

		return $html;
	}

	/**
	 * Method GetCheckBox() returns HTML code of check box
	 *
	 * @param string $name input box name
	 * @param boolean $checked checked ?
	 * @param string $title not mandatory title of checkbox (will be printed after checkbox)
	 * @return string HTML code of check box
	 */
	function GetCheckBox($name, $checked = false, $title = '') {
		$html = "";
		if ($checked === "on")
			$checked = 1;
		$html = "<input type=\"checkbox\" name=\"".htmlspecialchars($name)."\" ";
		if ($checked)
			$html .= "checked=\"checked\" ";
		$html .= "/>";
		if (!empty($title))
			$html .= "&nbsp;".htmlspecialchars($title)."\n";
		return $html;
	}

	/**
	 * Method GetSelect() returns HTML code of select dropdown
	 *
	 * @param string $name input box name
	 * @param mixed $value - pre -selected value of dropdown
	 * @param mixed $options | delimited string of select options or two dimensional array of all options (value & name)
	 * @param boolean $show_null if set to true, null option will be drawn
	 * @return string HTML code of check box
	 */
	function GetSelect($name, $value, $options, $show_null = false) {
		if (is_string($options)) {
			$temp = explode("|", $options);
			$options = array();
			foreach ($temp as $tmp) {
				$options[] = array($tmp, $tmp);
			}
		}
		if (!is_array($options)) {
			echo "Wrong argument for SELECT options !<br />\n";
			return false;
		}
		if (empty($value))
			$value = isset($this->row[$name]) ? $this->row[$name] : $default_value;

		$html = "<select name=\"".htmlspecialchars($name)."\" >";
		if ($show_null)
			$html .= "<option value=\"\">- Select -</option>\n";
		foreach ($options as $option) {
			$html .= "<option value=\"".htmlspecialchars($option[0])."\" ";
			if ($option[0] == $value)
				$html .= "selected=\"selected\" ";
			$html .= ">".htmlspecialchars($option[1])."</option>";
		}
		$html .= "</select>\n";
		return $html;
	}

	function SetSelectMoneyOption($start, $end, $interval) {
		$output[] = array("0"=>-1, "1"=>"Free");
		for($i=$start;$i<$end;$i+=$interval) {
			$output[] = array("0"=>$i, "1"=>"\${$i}.00");
		}
		return $output;
	}

	function select_box($name, $value) {
		return "<select name='$name'>$value</select>";
	}

	function select_add_yesno($value = -1)  {
		if( $value == 1 )
			return "<option value=\"1\" selected=\"selected\">Yes</option><option value=\"0\">No</option>";
		else if( $value == 0 && $value != "" ) {
			return "<option value=\"1\">Yes</option><option value=\"0\" selected=\"selected\">No</option>";
		} else
			return "<option value=\"1\">Yes</option><option value=\"0\">No</option>";
	}

	function select_add_select($name = '-- select --') {
		return "<option value=\"\">$name</option>";
	}

	function select_add_option($value, $name, $selected) {
		$selected = $selected ? "selected=\"selected\"" : "";
		return "<option value=\"$value\" $selected>$name</option>";
	}

	function select_add_array($arr, $d = "", $additional_attributes = []) {
		$options = "";
		foreach( $arr as $key => $a ) {
			if( is_array($a) ) {
				foreach($a as $x => $y ) {
					$key = $x;
					$a = $y;
				}
			}
			$add_attrs = "";
			if (array_key_exists($key, $additional_attributes)) {
				foreach($additional_attributes[$key] as $attr_name => $attr_val) {
					$add_attrs .= " {$attr_name}=\"{$attr_val}\"";
				}
			}
			if (
				(is_array($d) && in_array($key, $d)) //multiple select box
				|| (!is_array($d) && !strcmp($d, $key)) //normal select box
				)
				$options .= "<option value=\"$key\"{$add_attrs} selected=\"selected\">$a</option>";
			else
				$options .= "<option value=\"$key\"{$add_attrs}>$a</option>";
		}
		return $options;
	}

	function arrayValue($arr, $value1) {
		foreach($arr as $key => $value)	{
			if( $key == $value1 )
				return $value;
		}
		return NULL;
	}

	function hours($oh = array(), &$opens, &$closes) {
		$opens = array();
		$closes = array();

		for($day=0;$day<7;$day++) {
			$open = "<option value=\"\">-- don't know --</option>";
			$close = "<option value=\"\">-- don't know --</option>";
			$first = true;
			for($i=0,$s=12,$x="AM";$i<24;$i++)
			{
				for($l=0;$l<60;$l=$l+30)
				{
					$t = (($i*60)+$l);
					if( $l == 0 ) $l = "00";
					if( isset($_POST["day".($day+1)."o"]) && $_POST["day".($day+1)."o"] == $t && $_POST["day".($day+1)."c"]) {
						$open .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
					} else if( !isset($_POST["day".($day+1)."o"]) && isset($oh[$day+1]["open"]) && $oh[$day+1]["open"] == $t )
						$open .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
					else
						$open .= "<option value=\"$t\">{$s}:{$l} {$x}</option>";
				}
				if( $s == 12 && $first ) { $s = 1; $first = false; }
				else if( $s == 12 ) { $s = 1; }
				else if( $s == 11 && !$first ) { $s++; $x = "PM"; }
				else $s++;
			}
			$opens[$day] = $open;

			$first = true;
			for($i=0,$s=12,$x="AM";$i<24;$i++)
			{
				for($l=0;$l<60;$l=$l+30)
				{
					if( $i==0 && $l==0) continue;
					$t = (($i*60)+$l);
					if( $i < 12 ) $t += 1440;
					if( $l == 0 ) $l = "00";
					if( isset($_POST["day".($day+1)."c"]) && $_POST["day".($day+1)."c"] == $t )
						$close .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
					else if( !isset($_POST["day".($day+1)."c"]) && $oh[$day+1]["close"] == $t )
						$close .= "<option value=\"$t\" selected=\"selected\">{$s}:{$l} {$x}</option>";
					else
						$close .= "<option value=\"$t\">{$s}:{$l} {$x}</option>";
				}
				if( $s == 12 && $first ) { $s = 1; $first = false; }
				else if( $s == 12 ) { $s = 1; }
				else if( $s == 11 && !$first ) { $s++; $x = "PM"; }
				else $s++;
			}
			if( $oh[$day+1]["close"] ==  1440 )
				$close .= "<option value=\"1440\" selected=\"selected\">12:00 AM</option>";
			else
				$close .= "<option value=\"1440\">12:00 AM</option>";
			$closes[$day] = $close;
		}
	}

	function hr2($ohrow, $always = 0) {
		$hr = NULL;

		if( $always ) { $hr = "Open 24 Hours"; return $hr; }

		$hr_open = NULL;
		$hr_before = NULL;
		$count = 0;
		$through = 1;
		$d = 0;

		//_darr($ohrow);

		foreach($ohrow as $r) {
			$open = $r["open"];
			$close = $r["close"];
			$count++;

			if( !$open && !$close ) continue;

			switch($r["day"]) {
				case 1: $day = "Monday"; break;
				case 2: $day = "Tuesday"; break;
				case 3: $day = "Wednesday"; break;
				case 4: $day = "Thursday"; break;
				case 5: $day = "Friday"; break;
				case 6: $day = "Saturday"; break;
				default: $day = "Sunday";
			}
		
			$open_time = $r["open"] / 60 >= 12 ? "PM" : "AM";
			while($open>720) $open -= 720;
			$open = $open ? (intval($open / 60) ? intval($open / 60) : "12") . ":".($open % 60 == 0 ? "00" : ($open % 60)) : "12:00";

			$close_time = $close > 720 && $close < 1440 ? "PM" : "AM";
			while($close>720) $close -= 720;
			$close = (intval($close / 60) ? intval($close / 60) : "12") . ":".($close % 60 == 0 ? "00" : ($close % 60));
			if( isset($ohrow[$d-1]) && $ohrow[$d-1]["open"] == $r["open"] && $ohrow[$d-1]["close"] == $r["close"] ) {
				if( !isset($ohrow[$count]) ) {
					$hr .= "-".$day;
					$hour_text = $r["open"] == -1 && $r["close"] == -1 ? "Closed" : "$open$open_time  - $close$close_time";
					$hr .= ":  $hour_text";
				} 
//_d("1: ".$hr);
				$hr_before_day = $d;
				$hr_before = $day;
				$hr_open_before = $open . $open_time;
				$hr_close_before = $close . $close_time;
				$through++;
			} else {
				if( !$hr ) $hr = $day;
				else if(!isset($ohrow[$count])) {
					if( $ohrow[$count-2]["open"] == $ohrow[$count-3]["open"] && $ohrow[$count-2]["close"] == $ohrow[$count-3]["close"]) $hr .= " - ".$hr_before;
					$hr .= ": $hr_open_before - $hr_close_before<br/>";
					if( $r["close"] ) {
						$hour_text = $r["open"] == -1 && $r["close"] == -1 ? "Closed" : "$open$open_time  - $close$close_time";
						$hr .= "$day: $hour_text";
					}
				}
				else {
					if( $ohrow[$d-1]["open"] == -1 && $ohrow[$d-1]["close"] == -1 ) {
						if( $ohrow[$d-1]["open"] != $ohrow[$d-2]["open"] )
							$hr .= ": Closed<br/>$day";
						else $hr .= " - $hr_before: Closed<br/>$day";
					}
					else $hr .= ($through>1&&$day!=$hr_before?" - $hr_before":"").": $hr_open_before - $hr_close_before<br/>$day";
				}
//_d("2: ".$hr);
				$hr_before = $day;
				$hr_open_before = $hr_open = $open . $open_time;
				$hr_close_before = $hr_close = $close . $close_time;
				$through=1;
			}
			$d++;
		}

		//if( $through > 1 ) $hr .= " : $hr_open_before - $hr_close_before";
		return $hr;
	}

	function hr($ohrow, $always = 0) {
		$hr = NULL;
		if( $always ) { $hr = "Open 24 Hours"; return $hr; }

		$h = NULL;
		foreach($ohrow as $r) {
			$open = $r["open"];
			$close = $r["close"];

			if( !$open && !$close ) continue;
			switch($r["day"]) {
				case 1: $day = "Monday"; break;
				case 2: $day = "Tuesday"; break;
				case 3: $day = "Wednesday"; break;
				case 4: $day = "Thursday"; break;
				case 5: $day = "Friday"; break;
				case 6: $day = "Saturday"; break;
				default: $day = "Sunday";
			}

			if( $open == 0 && $close == 1440 ) {
				$h[] = array(0=>$day, 1=>'Open 24 Hours');
				continue;
			} elseif ( $open == -1 && $close == -1 ) {
				$h[] = array(0=>$day, 1=>'Closed');
				continue;
			}

			$open_time = $r["open"] / 60 >= 12 ? "PM" : "AM";
			while($open>720) $open -= 720;
			$open = $open ? (intval($open / 60) ? intval($open / 60) : "12") . ":".($open % 60 == 0 ? "00" : ($open % 60)) : "12:00";

			$close_time = $close > 720 && $close < 1440 ? "PM" : "AM";
			while($close>720) $close -= 720;
			$close = (intval($close / 60) ? intval($close / 60) : "12") . ":".($close % 60 == 0 ? "00" : ($close % 60));

			$h[] = array(0=>$day, 1=>"{$open}{$open_time} - {$close}{$close_time}");
		}

		$x = $y = NULL;
		foreach($h as $key) {
			if( empty($x) ) {
				$y = $key[0]; $x = $key[1];
			} else {
				if( $x == $key[1] ) {
					if( ($p = strpos($y, '-')) ) $y = substr($y, 0, $p) . " - {$key[0]}";
					else $y .= " - {$key[0]}";
				} else {
					$hr .= "$y: $x<br/>"; 
					$y = $key[0]; $x = $key[1];
				}
			}
		}
		if( $y ) $hr .= "$y: $x";
		return $hr;
	}

	function getLocation($deep = 1, $ids) {
		$output = "";
		while($deep>0) {
			$loc_parent = $ids[$deep+1] ? $ids[$deep+1] : 0;
			$select_name = $deep == 2 ? "state_select" : ($deep == 1 ? "city_select" : "c_select");
			$output .= "<span id=\"{$select_name}\"></span>";
			$what = $deep == 3 ? "Country" : ($deep == 2 ? "State" : "City");
			$whatr = $deep == 3 ? "country_id" : ($deep == 2 ? "state_id" : "loc_id");
			if( !$ids[$deep] && isset($this->row[$whatr]) ) $ids[$deep] = $this->row[$whatr];
			if( $ids[$deep] )
				$this->javascript .= "_ajax(\"get\", \"/locAjax.htm\", 
\"parent_name={$loc_parent}&select_name={$what}&{$what}={$ids[$deep]}\",\"{$select_name}\");";
			$deep--;
		}
		return $output;
	}

	function postorsql($name) {
		return isset($_POST[$name]) ? $_POST[$name] : (isset($this->row[$name]) ? $this->row[$name] : NULL);
	}

	function hh(&$open, &$close) {
		$open_time = $open / 60 >= 12 ? "PM" : "AM";
		while($open>720) $open -= 720;
		$open = $open ? (intval($open / 60) ? intval($open / 60) : "12") . ":".($open % 60 == 0 ? "00" : ($open % 60)) : "12:00";
		$open .= $open_time;
		$close_time = $close > 720 && $close < 1440 ? "PM" : "AM";
		while($close>720) $close -= 720;
		$close = (intval($close / 60) ? intval($close / 60) : "12") . ":".($close % 60 == 0 ? "00" : ($close % 60));
		$close .= $close_time;
	}

}

//END class Form
