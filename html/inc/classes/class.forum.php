<?php

class forum {

	var $post_id = NULL,
		$forum_id = NULL,
		$topic_id = NULL,
		$topicNick = NULL,
		$postNick = NULL,
		$postPostText = NULL;
	var $icons = array();
	var $modForums = array();

	function getDetailByPost($post_id) {
		global $db;

		$res = $db->q("
			select p.post_id, p.account_id, t.*, p.post_text 
			from forum_post p 
			inner join forum_topic t using (topic_id) where post_id = ?",
			[$post_id]
			);
		if (!$db->numrows($res))
			return false;
		
		$r = $db->r($res);
		$this->post_id = $r["post_id"];
		$this->forum_id = $r["forum_id"];
		$this->topic_id = $r["topic_id"];
		$this->topicNick = $r["account_id"];
		$this->postNick = $r["yazar"];
		$this->postPostText = $r["post_text"];
		return true;
	}

	function getDetailByTopic($topic_id) {
		global $db;

		$res = $db->q("select * from forum_topic where topic_id = ?", [$topic_id]);
		if (!$db->numrows($res))
			return false;
		
		$r = $db->r($res);
		$this->post_id = $r["post_id"];
		$this->forum_id = $r["forum_id"];
		$this->topic_id = $r["topic_id"];
		$this->topicNick = $r["account_id"];
		$this->postNick = $r["yazar"];
		return true;
	}

	function delPost($post_id, &$error)	{
		global $account, $db;

		if (!$this->getDetailByPost($post_id)) {
			$error = "getDetailByPost failed..";
			return false;
		}

		if ($account->isadmin()) {
			$res = $db->q("DELETE forum_post FROM forum_post WHERE forum_post.post_id = ?", array($this->post_id));

			if($db->affected < 1) {
				$error = "affected < 1";
				return false;
			}

			$db->q("UPDATE forum_topic SET topic_replies=topic_replies-1 WHERE topic_id='$this->topic_id'");

			$res = $db->q("DELETE FROM forum_topic WHERE topic_id=$this->topic_id and topic_replies < 0");

			$check_post = $db->q("SELECT post_id, posted FROM forum_post WHERE topic_id=$this->topic_id ORDER BY post_id DESC limit 1");
			$data = $db->r($check_post);
			if($data && $data["post_id"]<$this->post_id )
				$db->q("UPDATE forum_topic SET topic_last_post_id = {$data["post_id"]}, topic_last_post_time='{$data["posted"]}' WHERE topic_id = $this->topic_id");

			return $this->topic_id;
		}
		$error = "No Access";
		return false;
	}

	function makeForumLink($id, $name) {
		$forum_link = preg_replace("/[^a-z0-9_\- ]/i", "", strtolower($name));
		$forum_link = "/sex-forum/f".$id."-".preg_replace("/\s+/i", "-", $forum_link).".htm";
		return $forum_link;
	}

	function makeTopicLink($id, $name) {
		$topic_link = preg_replace("/[^a-z0-9_\- ]/i", "", strtolower($name));
		$topic_link = "/sex-forum/t".$id."-".preg_replace("/\s+/i", "-", $topic_link).".htm";
		return $topic_link;
	}

	function makeTime($time) {
		$current = time();
		$diff = $current - $time;

		if($diff < 60 )
			return "$diff seconds ago";
		if($diff < 3600)
			return floor($diff/60) . " mins ago";
		if($diff < 86400)
			return floor($diff/3600) . " hours " . ceil(($diff % 3600) / 60) . " minutes ago";
		if($diff < 2592000)
			return floor($diff/(60*60*24)) . " days " . ceil(($diff % (60*60*24)) / 3600) . " hours ago";
		if($diff < 31536000)
			return floor($diff/(60*60*24*30)) . " months " . ceil(($diff % (60*60*24*30)) / (60*60*24)) . " days ago";
		else
			return floor($diff/31536000) . " year " . ceil(($diff % (60*60*24*30)) / (60*60*24)) . " month " . ceil($diff % (60*60*24) / 84600) . " day ago";
	}

	function auto_url($text) {
		$ret = " " . $text;
		$ret = preg_replace("#([\n ])([a-z]+?)://([^ <\n\r]+)#i", "'\\1<a href=\"\\2://\\3\" target=\"_blank\" rel=\"nofollow\">\\2://".substr('\\3', 0, 50)."</a>", $ret);
		$ret = preg_replace("#([\n ])www\.([a-z0-9\-]+)\.([a-z0-9\-.\~]+)((?:/[^, <\n\r]*)?)#i", "\\1<a href=\"http://www.\\2.\\3\\4\" target=\"_blank\" rel=\"nofollow\">www.\\2.\\3\\4</a>", $ret);
		$ret = preg_replace("#([\n ])([a-z0-9\-_.]+?)@([^, <\n\r]+)#i", "\\1<a href=\"mailto:\\2@\\3\">\\2@\\3</a>", $ret);
		$ret = substr($ret, 1);
		return($ret);
	}

	function smile($text) {
		global $db;

		if (!count($this->icons)) {
			$res = $db->q("select code, icon from forum_icon");
			while($row=$db->r($res)) {
				$this->icons[] = array(
					"code" => $row["code"],
					"icon" => $row["icon"]
				);
			}
		}

		foreach($this->icons as $icon) {
			$icon_code = preg_quote($icon[code]);
			$icon_code = str_replace('/', '//', $icon_code);
			$text = preg_replace("/([\n\\ \\.])?$icon_code/si", '\1<img src="' . $icon[icon] . '" class="emoticon" border="0">', $text);
		}
		return $text;
	}

	function message($m) {
		$pattern[] = '/\[b\](.*?)\[\/b\]/si';
		$replace[] = '<b>$1</b>';

		$pattern[] = '/\[i\](.*?)\[\/i\]/si';
		$replace[] = '<i>$1</i>';

		$pattern[] = '/\[u\](.*?)\[\/u\]/si';
		$replace[] = '<u>$1</u>';

		// convert [url]url_link[/url]
		$pattern[] = '/\[url=(.*?)\](.*?)\[\/url\]/si';
		$replace[] = '<a href="$1" rel="nofollow" target="_blank" title="external link">$2</a>';
		$pattern[] = '/\[url\](.*?)\[\/url\]/si';
		$replace[] = '<a href="$1" rel="nofollow" target="_blank" title="external link">$1</a>';

		// convert [img]image_link[/img]
		$pattern[] = '/\[img\](.*?)\[\/img\]/i';
		$replace[] = '<img src="$1" border="0">';
		$pattern[] = '/\[img src="(.*?)"\]/i';
		$replace[] = '<img src="$1" border="0">';
		$pattern[] = '/\[img src=([^\]]*)\]/i';
		$replace[] = '<img src="$1" border="0">';

		$pattern[] = '/\[quote=("|&quot;)(.*?)("|&quot;)\](.*?)\[\/quote\]/si';
		$replace[] = '<div class="quotebox"><cite>$2 wrote:</cite><blockquote>$4</blockquote></div>';
		return preg_replace($pattern, $replace, $m);
	}

	function icons() {
		global $db;

		$res = $db->q("SELECT code, icon FROM forum_icon WHERE id >= 10");
		$icons = array();
		while($row=$db->r($res))
		{
			$icons[] = array(
				"code" => $row["code"],
				"icon" => $row["icon"]
			);
		}
		return $icons;
	}

	function move($topic_id, $forum_id, $old_forum, &$error) {
		global $account, $db;

		if(!$topic_id || !$forum_id) {
			$error = "No topic or forum id specified !";
			return false;
		}

		if( $forum_id == $old_forum )
		{
			$error = "You need to select a different category";
			return false;
		}

		if (!$account->isadmin()) {
			$error = "No Access For That";
			return false;
		}

		$res = $db->q("UPDATE forum_topic SET forum_id = ? WHERE topic_id = ?", [$forum_id, $topic_id]);

		if ($db->affected($res) == 1) {
			$res = $db->q("update forum_post set forum_id = ? where topic_id = ?", [$forum_id, $topic_id]);
			return $db->affected($res);
		} else {
			$error = "DB error";
			return false;
		}
	}

	function returnForumRecursive($forum_id) {
		global $db;

		$forums = array();
		$forums[] = $forum_id;
		$res2=$db->q("select forum_id from forum_forum where forum_parent = ?", [$forum_id]);

		while( $row2=$db->r($res2) ) {
			$forums[] = $row2["forum_id"];

			$forum_parent = $row2["forum_id"];
			while ($db->numrows($res3 = $db->q("select forum_id from forum_forum where forum_parent = ?", [$forum_parent])) > 0) {
				$row3=$db->r($res3);
				$forums[] = $row3["forum_id"];
				$forum_parent = $row3["forum_id"];
			}
		}
		return $forums;
	}

//function commented, we have to put also loc_id into condition !!
/*
	function forumRemove($forum_id) {
		global $db;

		$forums = $this->returnForumRecursive($forum_id);
		foreach($forums as $f)
		{
			$db->q("delete forum_post, forum_postext from forum_post inner join forum_postext using (post_id) where forum_id = ".$f);
			$db->q("delete from forum_topic where forum_id = ".$f);
			$db->q("delete from forum_mod where forum_id = ".$f);
			$db->q("delete from forum_forum where forum_id = ".$f);
		}
		return true;
	}
*/

	function _coreForumBreadReverse($parentId) {
		global $ctx, $account, $db;

		$bread = "";
		if( !$parentId )
			return $bread;

		$fs = array();
		$res = $db->q("select forum_parent, forum_name, forum_id from forum_forum where forum_id = ?", [$parentId]);
		while($row = $db->r($res)) {
			$parentId = $row["forum_parent"];

			$f = array(
				"forum_id" => $row["forum_id"],
				"forum_name" => $row["forum_name"],
				"forum_link" => "/sex-forum/viewforum?forum_id=".$row["forum_id"],
				"forum_parent" => $row["forum_parent"]
			);
			$f["forum_url"] = self::getForumLink($row["forum_id"]);
			$fs[] = $f;
			if( $parentId == 0 )
				break;

			$res = $db->q("select forum_parent, forum_name, forum_id from forum_forum where forum_id='$parentId'");

		}
		return $fs;
	}

	public static function sectionTopic($forum_id = NULL, $loc_id = NULL) {
		global $db;

		if (!$forum_id || !$loc_id)
			return;

		$res = $db->q("
    		SELECT t.topic_id, t.topic_title, t.topic_replies
    		FROM forum_topic t 
    		WHERE t.forum_id = ? AND t.loc_id = ?
			ORDER BY t.important DESC, t.topic_last_post_time DESC
			LIMIT 10",
			[$forum_id, $loc_id]
			);
		$topics = [];
		while ($row = $db->r($res)) {
			$c = $row["topic_replies"] > 1 ? "{$row["topic_replies"]} replies" : ($row["topic_replies"] ? "1 reply" : "");
			$topics[] = [
				"link" => self::getTopicLink($row["topic_id"], $forum_id),
				"name" => $row["topic_title"], 
				"c" => $c
				];
		}

		if (count($topics) > 0)
			return [
				"forum_id" => $forum_id, 
				"posts" => $topics
				];
		else
			return null;
	}

	function get_tracked_topics() {
		$cookie_data = isset($_COOKIE['forum_track']) ? $_COOKIE['forum_track'] : false;
		if (!$cookie_data)
			return array('topics' => array(), 'forums' => array());

		if (strlen($cookie_data) > 4048)
			return array('topics' => array(), 'forums' => array());

		$tracked_topics = array('topics' => array(), 'forums' => array());
		foreach (explode(';', $cookie_data) as $id_data) {
			switch (substr($id_data, 0, 1)) {
				case 'f': $type = 'forums'; break;
				case 't': $type = 'topics'; break;
				default: continue;
			}

			$id = intval(substr($id_data, 1));

			if (($pos = strpos($id_data, '=')) === false)
				continue;
			$timestamp = intval(substr($id_data, $pos + 1));

			if ($id > 0 && $timestamp > 0)
				$tracked_topics[$type][$id] = $timestamp;
		}
		return $tracked_topics;
	}

	function set_tracked_topics($tracked_topics) {
		$cookie_data = '';
		if (!empty($tracked_topics)) {
			arsort($tracked_topics['topics'], SORT_NUMERIC);
			arsort($tracked_topics['forums'], SORT_NUMERIC);

			foreach ($tracked_topics['topics'] as $id => $timestamp)
				$cookie_data .= 't'.$id.'='.$timestamp.';';
			foreach ($tracked_topics['forums'] as $id => $timestamp)
				$cookie_data .= 'f'.$id.'='.$timestamp.';';

			if (strlen($cookie_data) > 4048) {
				$cookie_data = substr($cookie_data, 0, 4048);
				$cookie_data = substr($cookie_data, 0, strrpos($cookie_data, ';')).';';
			}
		}
		setcookie('forum_track', $cookie_data, time() + 18000, '/', $_SERVER["HTTP_HOST"]);
	}

	function watchAdd($topic_id, $account_id) {
		global $db;

		$db->q("delete from forum_watch where topic_id = ? and account_id = ?", [$topic_id, $account_id]);
		$db->q("insert into forum_watch (topic_id, account_id) values (?, ?)", [$topic_id, $account_id]);
	}

	function watchDelUser($topic_id, $account_id) {
		global $db;

		$db->q("delete from forum_watch where topic_id = ? and account_id = ?", [$topic_id, $account_id]);
	}

	function watchDelUserAll($account_id) {
		global $db;

		$db->q("delete from forum_watch where account_id = ?", [$account_id]);
	}

	function watchDelTopic($topic_id) {
		global $db;

		$db->q("delete from forum_watch where topic_id = ?", [$topic_id]);
	}

	function watchTrue($topic_id, $account_id) {
		global $db;

		if (!$account_id)
			return;

		$res = $db->q("select count(watch_id) as cnt from forum_watch where topic_id = '$topic_id' and account_id = '$account_id'");
		if ($db->numrows($res)) {
			$row = $db->r($res);
			return $row["cnt"];
		}
		return 0;
	}

	function watchProcess($forum_id, $topic_id, $topic_title, $post_id) {
		global $account, $db;

		$topic_title = addslashes($topic_title);
		$res = $db->q("select account_id from forum_watch where topic_id = '$topic_id'");
		$c = 0;
		while ($row = $db->r($res)) {
			$account_id = $row["account_id"];
			if ($account->isloggedin() == $account_id)
				continue;

			$user = $account->getUserDetail($account_id);
			$post_link = self::getPostLink($post_id, $topic_id, $forum_id, true);
			$unwatch_link = self::getTopicActionLink("unwatch", $topic_id, $forum_id, true);
			$m = "Dear {$user["username"]}<br><br>One of the forum topics <b>'{$topic_title}'</b> that you are subscribed to has received a new post.  To read the post click on the link below.<br><br><a href='{$post_link}'>{$post_link}</a><br><br>PS: You may change your subscription to this topic by clicking the following link <a href='{$unwatch_link}'>{$unwatch_link}</a><br><br>Best,<br>adultsearch.com";
			sendEmail(SUPPORT_EMAIL, "New Forum Message on AdultSearch.com", $m, $user["email"]);
		}
	}

	public static function forumcatlink($forum_url = null) {
		global $ctx;

		if (!$forum_url)
			$forum_url = $ctx->category;

		return $ctx->location_path."/sex-forum/{$forum_url}";
	}

	function forumidbycat() {
		global $gModule;

		if( strstr($gModule, 'female-escorts') ) return 7;
		if( strstr($gModule, 'strip-cl') || strstr($gModule, '-revues') ) return 2;
		if( strstr($gModule, 'tstv') ) return 8;
		if( strstr($gModule, 'male-') ) return 9;
		if( strstr($gModule, 'erotic-mas') ) return 3;
		if( strstr($gModule, 'sex-shop') ) return 4;
		if( strstr($gModule, 'lifestyle') || strstr($gModule, 'swinger') || strstr($gModule, 'bdsm') ) return 5;
		return 1;
	}

	public static function exportPlaceListVars() {
		global $ctx, $smarty;

		$forums = null;
		//$ctx->category == place_type_id
		switch ($ctx->category_id) {
			case 1:
				//strip-clubs
				$forums = self::sectionTopic(2, $ctx->location_id);
				$smarty->assign("forumlink", self::forumcatlink("strip-clubs-strippers"));
				break;
			case 15:
				//emps
				$forums = self::sectionTopic(3, $ctx->location_id);
				$smarty->assign("forumlink", self::forumcatlink("erotic-massage-parlors-masseuses"));
				break;
			case 2:
				//sex-shops
				$forums = self::sectionTopic(4, $ctx->location_id);
				$smarty->assign("forumlink", self::forumcatlink("adult-stores"));
				break;
			case 141:
				//lifestyle
				$forums = self::sectionTopic(5, $ctx->location_id);
				$smarty->assign("forumlink", self::forumcatlink("lifestyle-clubs"));
				break;
			default:
				break;
		}
		
		if ($forums)
			$smarty->assign("forum", $forums);
	}

	public static function getForumLocation() {
		global $ctx, $smarty; 

		$exact_loc_id = $loc_id = $ctx->location_id;
		$loc_name = $ctx->location_name;
		$loc_link = "/";
			
		$smarty->assign("loc_name", $loc_name);
		$smarty->assign("loc_link", $loc_link);

		return array($loc_id, $loc_name, $loc_link, $exact_loc_id);
	}

	public static function getForumUrlNameById($forum_id) {
		global $mcache;

		$res = $mcache->get("SQL:FNA-ID", array($forum_id));
		if ($res === false)
			return false;
		else
			return $res['forum_url'];
	}
	
	public static function getForumUrlNameByName($forum_name) {
		global $mcache;

		$res = $mcache->get("SQL:FNA-NAM", array($forum_name));
		if ($res === false)
			return false;
		else
			return $res['forum_url'];
	}
	
	public static function getForumNameById($forum_id) {
		global $mcache;

		$res = $mcache->get("SQL:FNA-ID", array($forum_id));
		if ($res === false)
			return false;
		else
			return $res['forum_name'];
	}

	public static function getForumIdByUrlName($forum_url) {
		global $mcache;

		$res = $mcache->get("SQL:FNA-URL", array($forum_url));
		if ($res === false)
			return false;
		else
			return $res['forum_id'];
	}

	public static function getForumLink($forum_id = NULL, $absolute = false) {
		global $ctx, $account;

		if (is_null($forum_id))
			$forum_id = $ctx->forum_id;

		if (($forum_url = self::getForumUrlNameById($forum_id)) === false) {
			$link = "{$ctx->location_path}/sex-forum/{$forum_id}";
		} else {
			$link = "{$ctx->location_path}/sex-forum/{$forum_url}";
		}
		
		if ($absolute)
			return $ctx->domain_link.$link;
		else
			return $link;
	}
	
	public static function getTopicLink($topic_id = NULL, $forum_id = NULL, $absolute = false) {
		global $ctx, $account;

		if (is_null($topic_id))
			$topic_id = $ctx->forum_topic_id;

		return self::getForumLink($forum_id, $absolute)."/topic/{$topic_id}";
	}
	
	public static function getPostLink($post_id, $topic_id = NULL, $forum_id = NULL, $absolute = false) {
		$topic_link = self::getTopicLink($topic_id, $forum_id, $absolute);
		return $topic_link."#{$post_id}";
	}

	public static function getForumReplyLink($topic_id = NULL, $forum_id = NULL, $absolute = false) {
		return self::getTopicLink($topic_id, $forum_id, $absolute)."/reply";
	}
	
	public static function getForumQuoteLink($post_id, $topic_id = NULL, $forum_id = NULL, $absolute = false) {
		return self::getTopicLink($topic_id, $forum_id, $absolute)."/quote/{$post_id}";
	}
	
	public static function getForumEditLink($post_id, $topic_id = NULL, $forum_id = NULL, $absolute = false) {
		return self::getTopicLink($topic_id, $forum_id, $absolute)."/edit/{$post_id}";
	}

	public static function getTopicActionLink($action, $topic_id = NULL, $forum_id = NULL, $absolute = false) {

		switch ($action) {
			case "watch":
				return self::getTopicLink($topic_id, $forum_id, $absolute)."/watch";
				break;
			case "unwatch":
				return self::getTopicLink($topic_id, $forum_id, $absolute)."/unwatch";
				break;
			case "deltopic":
				return self::getTopicLink($topic_id, $forum_id, $absolute)."/delete";
				break;
			case "move":
				return self::getTopicLink($topic_id, $forum_id, $absolute)."/move";
				break;
			default:
				return "";
				break;
		}
	}

	public static function getReplyLink($action, $absolute = false) {
		global $ctx, $account;

		switch ($action) {
			case 'delpost':
				$link = "{$ctx->location_path}/sex-forum/delpost/";	//TODO do we need location_path here? 
				break;
			default:
				$link = "{$ctx->location_path}/sex-forum/reply";	//TODO do we need location_path here? 
				break;
		}
		
		if ($absolute)
			return $ctx->domain_link.$link;
		else
			return $link;
	}

	public static function getNewTopicLinkForCategory($forum_id = NULL, $company_id = NULL) {
		global $ctx;

		$forum_url = self::getForumUrlNameByName(dir::getCategoryPlural($ctx->category_name));
		if ($forum_url === false)
			return "";
		return $ctx->location_path."/sex-forum/{$forum_url}/new_topic?item_id={$ctx->node_id}";
	}

	public static function getLastTopicsForPlace($place_id) {
		global $db;
		
		$last_topics = [];

		$rex = $db->q("
			SELECT topic_id, forum_id, topic_title, date_format(topic_last_post_time, '%m-%d-%Y') topic_time, a.username 
			FROM forum_topic t 
			LEFT JOIN account a using (account_id) 
			WHERE item_id = ?
			ORDER BY topic_last_post_id desc 
			LIMIT 5",
			[$place_id]
			);
		while ($rox = $db->r($rex)) {
			$last_topics[] = array(
				"topic_link"  => self::getTopicLink($rox["topic_id"], $rox["forum_id"]),
				"topic_title" => $rox["topic_title"],
				"topic_time"  => $rox["topic_time"],
				"username"	  => $rox["username"],
			);
		}
		return $last_topics;
	}

	/**
	 * Returns array of forum_id -> forum_name of forums defined for this location in database
	 */
	public static function getCategories($loc_id) {
		global $db;

		$loc_id = intval($loc_id);
		if ($loc_id == 0)
			return false;

		$res = $db->q("
			SELECT p.forum_id id, pn.forum_name name, f.forum_id child_id, n.forum_name child_name
			FROM forum_forum p
			INNER JOIN forum_name pn on pn.forum_id = p.forum_id
			LEFT JOIN forum_forum f on p.forum_id = f.forum_parent and p.loc_id = f.loc_id
			LEFT JOIN forum_name n on n.forum_id = f.forum_id
			WHERE p.loc_id = ? AND p.forum_parent = 0
			ORDER BY pn.forum_name, n.forum_name",
			[$loc_id]
			);
		$categories = array();
		$parent_array = NULL;
		$parent_name = NULL;
		while ($row = $db->r($res)) {
			if (empty($row["child_id"])) {
				if (!is_null($parent_name)) {
					$categories[$parent_name] = $parent_array;
					$parent_name = NULL;
				}
				$categories[$row["id"]] = $row["name"];
			} else {
				if (is_null($parent_name)) {
					$parent_array = array();
					$parent_array[$row["child_id"]] = $row["child_name"];
					$parent_name = $row["name"];
				} else if ($parent_name != $row["name"]) {
					$categories[$parent_name] = $parent_array;
					$parent_array = array();
					$parent_array[$row["child_id"]] = $row["child_name"];
					$parent_name = $row["name"];
				}
				$parent_array[$row["child_id"]] = $row["child_name"];
			}
		}
		if (!is_null($parent_name)) {
			$categories[$parent_name] = $parent_array;
		}

		return $categories;
	}

	public function getCompanies($loc_id, $forum_id) {
		global $db, $ctx;

		$companies = array();

		//get forum name
		$forum_name = self::getForumNameById($forum_id);
		if ($forum_name === false)
			return array();
		
		//find out type_idget types for current country
		$type_id = NULL;
		$types_plural = dir::getTypesPlural();
		foreach ($types_plural as $key => $val) {
			if ($val == $forum_name) {
				$type_id = $key;
				break;
			}
		}
		if (is_null($type_id))
			return array();

		//fetch companies for this type and location id
		//$companies[0] = "This forum topic does not relate to concrete company";
		$companies[0] = "No";
		$sql = "SELECT place_id, name, address1
				FROM place
				WHERE loc_id = '{$loc_id}' and place_type_id = '{$type_id}' and edit = 1
				ORDER BY name ASC";
		$res = $db->q($sql);
		while ($row = $db->r($res)) {
			$companies[$row["place_id"]] = $row["name"]." - ".$row["address1"]." - #".$row["place_id"];
		}

		return $companies;
	}

	function addDomesticLoc($loc_id, $loc_parent) {
		global $db;

		$db->q("INSERT ignore INTO `forum_forum` (`forum_id`, `loc_id`, `loc_parent`, `forum_name`, `forum_desc`, `forum_parent`, `forum_order`) VALUES
				(1, '$loc_id', '$loc_parent', 'General Talk', 'General Talk', 0, 1),
				(2, '$loc_id', '$loc_parent', 'Strip Clubs & Strippers', 'Strip Clubs & Strippers \nTalk', 0, 2),
				(3, '$loc_id', '$loc_parent', 'Erotic Massage Parlors & Masseuses', 'Erotic \nMassage Parlors Talk', 0, 3),
				(4, '$loc_id', '$loc_parent', 'Adult Stores', 'Adult Stores Talk', 0, 4),
				(5, '$loc_id', '$loc_parent', 'LifeStyle Clubs', 'Swinger Clubs, BDSM', 0, 5),
				(6, '$loc_id', '$loc_parent', 'Escorts', 'Female, Male, Shemale, TS, TV', 0, 1),
				(7, '$loc_id', '$loc_parent', 'Female Escorts', 'Female Escorts', 6, 0),
				(8, '$loc_id', '$loc_parent', 'TS / TV Shemale Escorts', 'TS / TV \nShemale Escorts', 6, 1),
				(9, '$loc_id', '$loc_parent', 'Male Escorts', 'Male', 6, 2)");
	}

	function getPagingArray($num_topic, $cur_page, $path, $items_on_page) {
		return pager::paging_array($num_topic, $cur_page, $path, false, $items_on_page, 5);
	}

	function getSingular($forum_name) {
		if ($forum_name == "Strip Clubs & Strippers")
			return "Strip Club & Stripper";
		else if ($forum_name == "Erotic Massage Parlors & Masseuses")
			return "Erotic Massage Parlor & Masseuse";
		else if (substr($forum_name, -1) == "s")
			return substr($forum_name, 0, strlen($forum_name) - 1);
		return $forum_name;
	}

	/**
	 * Function updates compute columns in forum_forum and forum_topic tables
	 * This should be called whenever new forum post is added or deleted
	 */
	public static function updateStats($p_forum_id = null, $p_loc_id = null) {
		global $db;

		//CREATE STATS
		$forum_loc_stats = [];
		$topic_stats = [];
		$where = ""; $params = [];
		if ($p_forum_id && $p_loc_id) {
			$where = " WHERE f.forum_id = ? AND f.loc_id = ? ";
			$params = [$p_forum_id, $p_loc_id];
		}
		$res = $db->q("
			SELECT f.*, l.loc_parent
			FROM forum_post f
			LEFT JOIN location_location l on l.loc_id = f.loc_id
			{$where}
			ORDER BY f.post_id DESC
			", 
			$params
			);
		$i = 0;
		while ($row = $db->r($res)) {
			$i++;

			$forum_id = $row["forum_id"];
			$loc_id = $row["loc_id"];
			$topic_id = $row["topic_id"];
			$post_id = $row["post_id"];
			$posted = $row["posted"];
			$poster = $row["account_id"];

			$key = $forum_id."_".$loc_id;

			if (array_key_exists($key, $forum_loc_stats)) {
				$item = $forum_loc_stats[$key];
				$item["forum_posts"] = $item["forum_posts"] + 1;
				$ft = $item["forum_topics"];
				if (!in_array($topic_id, $ft))
					$ft[] = $topic_id;
				$item["forum_topics"] = $ft;
			} else {
				$item = [
					"forum_posts" => 1,
					"forum_topics" => [$topic_id],
					"last_post_id" => $post_id,
					"last_posted" => $posted,
					"last_poster" => $poster,
				];
			}
			$forum_loc_stats[$key] = $item;

			if (array_key_exists($topic_id, $topic_stats)) {
				$item = $topic_stats[$topic_id];
				$item["topic_replies"] = $item["topic_replies"] + 1;
			} else {
				$item = [
					"topic_replies" => 0,
					"last_post_id" => $post_id,
					"last_posted" => $posted,
					"last_poster" => $poster,
				];
			}
			$topic_stats[$topic_id] = $item;
		}
		//echo "done, #posts={$i}, forum_loc_stats_count=".count($forum_loc_stats).", topic_stats_count=".count($topic_stats)."<br />\n";

		//UPDATE forum_forum ENTRY
		$res = $db->q("SELECT * FROM forum_forum f {$where}", $params);
		$j = 0;
		while ($row = $db->r($res)) {
			$j++;

			$forum_id = $row["forum_id"];
			$loc_id = $row["loc_id"];
			$loc_parent = $row["loc_parent"];
			$forum_posts = $row["forum_posts"];
			$forum_topics = $row["forum_topics"];
			$last_post_id = $row["forum_last_post_id"];
			$last_posted = $row["forum_last_post_time"];
			$last_poster = $row["forum_last_poster_id"];

			$key = $forum_id."_".$loc_id;

			if (array_key_exists($key, $forum_loc_stats)) {
				$item = $forum_loc_stats[$key];
				if ($forum_posts == $item["forum_posts"] 
					&& $forum_topics == count($item["forum_topics"])
					&& $last_post_id == $item["last_post_id"]
					&& $last_posted == $item["last_posted"]
					&& $last_poster == $item["last_poster"]
					) {
					//stats are ok
					//echo "key={$key} #posts={$forum_posts}-{$item["forum_posts"]}, #topics={$forum_topics}-".count($item["forum_topics"]).", last_post_id={$last_post_id}-{$item["last_post_id"]}, last_posted={$last_posted}-{$item["last_posted"]}, last-poster={$last_poster}-{$item["last_poster"]} <span style=\"color: green;\">OK</span><br />\n";
				} else {
					//stats are not ok, update
					//echo "key={$key} #posts={$forum_posts}-{$item["forum_posts"]}, #topics={$forum_topics}-".count($item["forum_topics"]).", last_post_id={$last_post_id}-{$item["last_post_id"]}, last_posted={$last_posted}-{$item["last_posted"]}, last-poster={$last_poster}-{$item["last_poster"]} <span style=\"color: red;\">No match</span><br />\n";
					$res2 = $db->q("
						UPDATE forum_forum 
						SET forum_posts = ?, forum_topics = ?, forum_last_post_id = ?, forum_last_post_time = ?, forum_last_poster_id = ? 
						WHERE forum_id = ? AND loc_id = ? 
						LIMIT 1",
						[$item["forum_posts"], count($item["forum_topics"]), $item["last_post_id"], $item["last_posted"], $item["last_poster"], $forum_id, $loc_id]
					);
					$aff = $db->affected($res2);
					if ($aff != 1) {
						//echo "<span style=\"color: red;\">UPDATE ERROR</span><br />\n";
					} else {
						//echo "<span style=\"color: green;\">UPDATE OK</span><br />\n";
					}
				}
			} else if ($forum_posts == 0 && $forum_topics == 0) {
				//echo "key={$key} empty OK<br />\n";
			} else {
				//echo "key={$key} forum_posts={$forum_posts} forum_topics={$forum_topics}  <span style=\"color: red;\">Not in stats array</span><br />\n";
			}
		}
		//echo "Processed {$i} forums.<br />\n";

		//UPDATE forum_topic ENTRIES
		$res = $db->q("SELECT * FROM forum_topic f {$where}", $params);
		$j = 0;
		while ($row = $db->r($res)) {
			$j++;

			$topic_id = $row["topic_id"];
			$loc_id = $row["loc_id"];
			$topic_replies = $row["topic_replies"];
			$last_post_id = $row["topic_last_post_id"];
			$last_posted = $row["topic_last_post_time"];
			$last_poster = $row["topic_last_post_nick"];

			if (array_key_exists($topic_id, $topic_stats)) {
				$item = $topic_stats[$topic_id];
				if ($topic_replies == $item["topic_replies"] 
					&& $last_post_id == $item["last_post_id"]
					&& $last_posted == $item["last_posted"]
					&& $last_poster == $item["last_poster"]
					) {
					//stats are ok
					//echo "topic_id={$topic_id} #replies={$topic_replies}-{$item["topic_replies"]}, last_post_id={$last_post_id}-{$item["last_post_id"]}, last_posted={$last_posted}-{$item["last_posted"]}, last-poster={$last_poster}-{$item["last_poster"]} <span style=\"color: green;\">OK</span><br />\n";
				} else {
					//stats are not ok, update
					//echo "topic_id={$topic_id} #replies={$topic_replies}-{$item["topic_replies"]}, last_post_id={$last_post_id}-{$item["last_post_id"]}, last_posted={$last_posted}-{$item["last_posted"]}, last-poster={$last_poster}-{$item["last_poster"]} <span style=\"color: red;\">No match</span><br />\n";
					$res2 = $db->q("
						UPDATE forum_topic 
						SET topic_replies = ?, topic_last_post_id = ?, topic_last_post_time = ?, topic_last_post_nick = ? 
						WHERE topic_id = ?
						LIMIT 1",
						[$item["topic_replies"], $item["last_post_id"], $item["last_posted"], $item["last_poster"], $topic_id]
					);
					$aff = $db->affected($res2);
					if ($aff != 1) {
						//echo "<span style=\"color: red;\">UPDATE ERROR</span><br />\n";
					} else {
						//echo "<span style=\"color: green;\">UPDATE OK</span><br />\n";
					}
				}
			} else {
				//echo "topic_id={$topic_id}  <span style=\"color: red;\">Not in stats array</span><br />\n";
			}
		}
		//echo "Processed {$j} topics.<br />\n";

		return;
	}

}

?>
