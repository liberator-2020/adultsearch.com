<?php

class geolocation {

	public static function lookup_ip2location_v8($ip_address) {
		global $config_dev_server;

		require_once(_CMS_ABS_PATH."/inc/classes/class.ip2locationv8.php");

		$i2l_bin_file = _CMS_ABS_PATH."/inc/classes/bin/ip.bin";

		if ($config_dev_server) {
			$database = new \IP2Location\Database($i2l_bin_file, \IP2Location\Database::FILE_IO);
		} else {
			//uses shmop php extension, needs additional memory (increased memory_limit in php.ini)
			$database = new \IP2Location\Database($i2l_bin_file, \IP2Location\Database::SHARED_MEMORY);
		}

		$records = $database->lookup($ip_address, \IP2Location\Database::ALL);

		return $records;
	}

	public static function lookup_ip2location($ip_address) {
		global $db;

		$result = array();

		$records = self::lookup_ip2location_v8($ip_address);

		if ($records['cityName'] && strlen($records['cityName']) > 3) {
			//geolocation gave us city, lets see if we have this city in database
			$res = $db->q("SELECT l.loc_id, l.country_id, l.country_sub, l.loc_url, l.dir
							FROM location_location l 
							INNER JOIN location_location p on p.loc_id = l.loc_parent 
							WHERE l.loc_name like ? AND (p.loc_name like ? OR p.loc_name like ?)
							LIMIT 1", array($records['cityName'], $records['regionName'], $records['countryName']));
			if ($db->numrows($res)) {
				$row = $db->r($res);
				$result = array(
					"location_id" => $row["loc_id"],
					"city_name" => $records['cityName'],
					"state_name" => $records['regionName'],
					"latitude" => $records['latitude'],
					"longitude" => $records['longitude'],
					"country_id" => $row["country_id"],
					"country_sub" => $row["country_sub"],
					"country_name" => $records['countryName'],
					"city_url" => location::getUrlStatic($row["country_sub"], $row["loc_url"], $row["dir"]),
					);
				return $result;
			}
		}

		//if geolocation didnt find city, or we dont have city in database, lets try to look up for country at least
		if ($records['countryName']) {
			$country = $records['countryName'];

			//TODO standardize this ?
			if ($country == "RUSSIAN FEDERATION")
				$country = "RUSSIA";
			else if ($country == "VENEZUELA, BOLIVARIAN REPUBLIC OF")
				$country = "VENEZUELA";
			else if ($country == "MOLDOVA, REPUBLIC OF")
				$country = "MOLDOVA";
			else if ($country == "AFGHANISTAN")
				$country = "PAKISTAN";

			$res = $db->q("SELECT l.loc_id, l.country_sub, lc.loc_url, lc.dir
							FROM location_location l 
							LEFT JOIN location_location lc on lc.loc_parent = l.loc_id
							WHERE l.loc_type = 1 AND l.loc_name like ?
							GROUP BY l.loc_id, l.country_sub, lc.loc_url, lc.dir
							LIMIT 1", array($country));
			if ($db->numrows($res)) {
				$row = $db->r($res);
				$result = array(
					"city_name" => $records['cityName'],
					"state_name" => $records['regionName'],
					"country_id" => $row["loc_id"],
					"country_sub" => $row["country_sub"],
					"country_name" => $country,
					"city_url" => location::getUrlStatic($row["country_sub"], $row["loc_url"], $row["dir"]),
					);
				return $result;
			}
		}

		return array();
	}

	public static function getLocationByIp($ip_address) {
		global $db;

		//TODO TO DELETE this
		//tmp production hotfix dont allow ipv6 addresses
		//if (strpos($ip_address, ":") !== false || strlen($ip_address) > 15)
		//	return [];

		$result = self::lookup_ip2location($ip_address);
		return $result;
	}

	/**
	 * This finds closest (by latitude & longitude) city with some place or ad
	 */
	public static function getClosestNonEmptyLocationByIp($ip_address) {
		global $db;

		//TODO TO DELETE this
		//tmp production hotfix dont allow ipv6 addresses
		//if (strpos($ip_address, ":") !== false || strlen($ip_address) > 15)
		//	return [];

		$records = self::lookup_ip2location_v8($ip_address);

		if (!array_key_exists("latitude", $records) || !array_key_exists("longitude", $records))
			return false;

		$lat = preg_replace('/[^0-9\-\.]/', '', $records["latitude"]);
		$lng = preg_replace('/[^0-9\-\.]/', '', $records["longitude"]);

		if (!$lat || !$lng || $lat == "." || $lng == ".")
			return false;
	
		$res = $db->q("SELECT l.*
						, (3959 * acos (
						      cos ( radians({$lat}) )
					    	  * cos( radians( l.loc_lat ) )
						      * cos( radians( l.loc_long ) - radians({$lng}) )
    						  + sin ( radians({$lat}) )
						      * sin( radians( l.loc_lat ) )
						    )) as distance
						FROM location_location l
						WHERE l.loc_lat IS NOT NULL AND l.loc_long IS NOT NULL AND l.has_place_or_ad = 1 AND l.significant = 1
						ORDER BY distance ASC
						LIMIT 1;");
		if (!$db->numrows($res))
			return false;

		$row = $db->r($res);
		$location = location::withRow($row);

		return $location;
	}

	public static function getLocationLabel($ip_address) {

		//TODO TO DELETE this
		//tmp production hotfix dont allow ipv6 addresses
		//if (strpos($ip_address, ":") !== false || strlen($ip_address) > 15)
		//	return [];

		$ip_address = preg_replace('/[^0-9\.]/', '', $ip_address);
		if (!$ip_address)
			return false;

		$location = self::getLocationByIp($ip_address);

		$html = "";
		if ($location["city_name"])
			$html .= $location["city_name"];

		if ($location["state_name"])
			$html .= ", ".$location["state_name"];

		if ($location["country_id"] || $location["country_sub"]) {
			$code = "";
			switch ($location["country_id"]) {
				case 16046: $code = "us"; break;
				case 16047: $code = "ca"; break;
				case 41973: $code = "gb"; break;
				default: break;
			}
			if (!$code && $location["country_sub"] && strlen($location["country_sub"]) == 2)
				$code = strtolower($location["country_sub"]);
			if ($code)
				$html .= ", <img src=\"/images/flags/{$code}.gif\" />";
		}

		return $html;
	}

}

//END
