<?php
/**
 * jumploader upload widget functions
 * Class 
 * @author jay
 */
class jumploader {

	public static $file_param_name = 'file';

	public static function getJumploaderHtml($width = 400, $height = 200) {
		$width = intval($width);
		$height = intval($height);

		video::storeVideoInfoToSession();

		$html = "";
	
$html .= <<<END
<script language="javascript" type="text/javascript">
function uploaderFileAdded( uploader, file ) {
	setTimeout(doUploaderFileAdded, 200, uploader, file);
}

function doUploaderFileAdded( uploader, file ) {
	var num_files_limit = 3;
	if (uploader.getFileCount() > num_files_limit) {
		alert('Maximal number of uploaded files is ' + num_files_limit + ' !');
		if (uploader.removeFile(file) != null) {
			alert('Error removing file.');
		}
	}
}
function uploaderStatusChanged( uploader ) {
	var some_failed = false;
	var failed_msg = '';
	var debug_info = '';

	if (uploader.getStatus() == 1)
		return;

	//uploading finished
	var cnt_total = uploader.getFileCount();
	var cnt_finished = uploader.getFileCountByStatus('2');
	var cnt_failed = uploader.getFileCountByStatus('3');
	for (var i = 0; i < cnt_total; i++) {  
		var file = uploader.getFile(i.toString());
		if (file.isFailed()) {
			some_failed = true;
			failed_msg += file.getError() + '\\n';
			debug_info += 'path=' + file.getPath() + ', mimetype=' + file.getMimeType() + ', isDirectory=' + file.isDirectory() + ', isFile=' + file.isFile() + ', length=' + file.getLength() + ', status=' + file.getStatus() + ', error=' + file.getError() + '\\n';
		}
	}
	if (some_failed) {
		upload_video_error(failed_msg, debug_info);
	} else {
		upload_video_ok();
	}
}

function upload_video_ok() {
	$('div#upload_ok').html("Please wait...");
	$('applet').css('display', 'none');
	_ajax('post', '/files/uploadVideoResult', '', 'upl_vid_result');
}
function upload_video_error(error_msg, debug_info) {
	$('div#upload_error .error_detail').html(error_msg);
	$('div#upload_error').show();
	_ajaxs('post', '/files/errorUploadVideo', debug_info);
}
</script>
END;

		$html .= '<applet id="jumpLoaderApplet" name="jumpLoaderApplet" code="jmaster.jumploader.app.JumpLoaderApplet.class" archive="/js/jumploader/jumploader_z.jar" width="'.$width.'"  height="'.$height.'" alt="Please install java plugin to your browser to use this applet" mayscript allowscriptaccess=always >';

$html .= <<<END
<param name="vc_uploadListViewName" value="_compact"/>
<param name="vc_useThumbs" value="false"/>
<param name="vc_uploadViewPasteActionVisible" value="false"/>
<param name="vc_uploadViewRemoveActionVisible" value="false"/>
<param name="vc_uploadViewRetryActionVisible" value="false"/>
<param name="vc_uploadViewFilesSummaryBarVisible" value="false"/>
<param name="vc_lookAndFeel" value="system"/>
<param name="uc_logServerResponse" value="true"/>
<param name="uc_uploadUrl" value="/jumploader/uploadHandler"/>
<param name="uc_partitionLength" value="1000000"/>
<param name="uc_resumeCheckUrl" value="/jumploader/resumeCheckHandler"/>
<param name="vc_uploadViewStartUploadButtonText" value="Start upload"/>
<param name="vc_uploadViewStopUploadButtonText" value="Stop upload"/>
<param name="ac_fireUploaderFileAdded" value="true"/>
<param name="ac_fireUploaderStatusChanged" value="true"/>
</applet>
<div id="upl_vid_result"></div>
END;

		return $html;
	}

	public static function uploadHandler() {
		//debug_log('UP jump');
		//debug_log("GET=".print_r($_GET,true)." POST=".print_r($_POST,true)." FILES=".print_r($_FILES,true));
		if (intval($_FILES['file']['error']) > 0 ) {
			echo "Error:Upload error ".$_FILES['file']['error'];
			debug_log("Jumploader error: Upload error ".$_FILES['file']['error']);
			return;
		}

		//retrieve request parameters
		$file_name = $_POST['fileName'];
		$file_id = $_POST[ 'fileId' ];
		$partition_index = $_POST[ 'partitionIndex' ];
		$partition_count = $_POST[ 'partitionCount' ];
		$file_length = $_POST[ 'fileLength' ];

		$client_id = "jmpldr".substr(session_id(), 0, 8);

		//debug_log("creating file identifier: file_name=$file_name file_length=$file_length file_identifier=$file_identifier");
		if ($partition_index == 0) {
			self::storeIdentifier($file_name, $file_length, $client_id, $file_id);
		} else {
			if (($client_id = self::getClientId($file_name, $file_length)) === false)
				return;
		}

		//move uploaded partition to the staging folder 
		$source_file_path = $_FILES[ self::$file_param_name ][ 'tmp_name' ];
		$target_file_path = video::getStageDir() . $client_id . "." . $file_id . "." . $partition_index;
		//debug_log("Moving partition '$source_file_path' '$target_file_path'");
		if( !move_uploaded_file( $source_file_path, $target_file_path ) ) {
			echo "Error:Can't move uploaded file";
			debug_log("Jumploader error:Can't move uploaded file '$source_file_path' '$target_file_path'");
			return;
		}

		//check if we have collected all partitions properly
		$all_in_place = true;
		$partitions_length = 0;
		for( $i = 0; $all_in_place && $i < $partition_count; $i++ ) {
			$partition_file = video::getStageDir() . $client_id . "." . $file_id . "." . $i;
			if( file_exists( $partition_file ) ) {
				$partitions_length += filesize( $partition_file );
			} else {
				$all_in_place = false;
			}
		}

		//issue error if last partition uploaded, but partitions validation failed
		if( $partition_index == $partition_count - 1 &&
			( !$all_in_place || $partitions_length != intval( $file_length ) ) ) {
				echo "Error:Upload validation error";
				debug_log("Jumploader error: Upload validation error all_in_place=$all_in_place partitions_length=$partitions_length file_length=$file_length");
				return;
			}

		if ($_SESSION["upl_vid_module"] == "place") {
            $dir = $_SESSION["upl_vid_country"];
        } else {
            $dir = $_SESSION["upl_vid_module"];
        }

		//reconstruct original file if all ok
		if( $all_in_place ) {

			//first create tmpfile
			$tmp_filepath = video::getDestFilePath($dir, $file_name . "." . $client_id . "." . $file_id);
			$file_handle = fopen( $tmp_filepath, 'w' );
			
			//debug_log("UP jump: complete file creating at '$tmp_filepath', file handle = $file_handle");
			
			//append all partition files to tmpfile
			for( $i = 0; $all_in_place && $i < $partition_count; $i++ ) {
				$partition_file = video::getStageDir() . $client_id . "." . $file_id . "." . $i;
				$partition_file_handle = fopen( $partition_file, "rb" );
				$contents = fread( $partition_file_handle, filesize( $partition_file ) );
				fclose( $partition_file_handle );
				$ret = fwrite( $file_handle, $contents );
				//debug_log("UP jump: appending part file '$partition_file', written $ret bytes, deleting part file.");
				unlink( $partition_file );
			}
			fclose( $file_handle );

			//rename tmpfile to original file
		
			$dest_filename = video::getUniqueFilename($dir, $file_name);
			$dest_filepath = video::getDestFilePath($dir, $dest_filename);
			
			$ret = rename($tmp_filepath, $dest_filepath);

			//debug_log("UP jump: renaming final file from '$tmp_filepath' to '$dest_filepath' (success=$ret)");

			//remove file identifier file
			unlink(video::getStageDir() . self::getIdentifierFilename($file_name, $file_length));

			//store data about successful upload to session
			$_SESSION["upl_vid_filename"] = $dest_filename;
            $_SESSION["upl_vid_filepath"] = $dest_filepath;

			//remove old files from stage directory
			video::removeOldStageFiles();
		}

		header ("Connection: close");

		return;
	}

	public static function resumeCheckHandler() {
		//debug_log("class Jumploader: resumeCheckHandler(): GET=".print_r($_GET,true)." POST=".print_r($_POST,true)." FILES=".print_r($_FILES,true));

		$file_name = $_POST['fileName'];
		$file_length = $_POST['fileLength'];

		if (($handle = fopen(video::getStageDir().self::getIdentifierFilename($file_name, $file_length), "r")) === false)
			return;
		$client_id = substr(fgets($handle), 0, -1);
		$file_id = substr(fgets($handle), 0, -1);
		fclose($handle);
		//debug_log("read from identifier file: client_id=$client_id file_id=$file_id");

		$max_partition = -1;
		if ($handle = opendir(video::getStageDir())) {
			while (false !== ($entry = readdir($handle))) {
				$pattern = "/^{$client_id}\.{$file_id}\.([0-9]*)$/";
				if (!preg_match($pattern, $entry, $matches))
					continue;
				$partition = $matches[1];
				//debug_log("found file for partition $partition");
				if ((intval($partition) == 0) && ($partition != "0"))
					continue;
				$partition = intval($partition);
				if ($partition > $max_partition)
					$max_partition = $partition;
			}
			closedir($handle);
		}

		if ($max_partition == -1) {
			//debug_log("not found any partition file");
			return;
		}

		$start_partition = $max_partition + 1;
		//debug_log("max_partition=$max_partition start_partition=$start_partition");

		echo "fileId:$file_id\n";
		echo "partitionIndex:$start_partition\n";
		return;
	}

	/*
`	 * Creating file identifier which enables us to resume donwloads
	 */
	private static function getIdentifierFilename($file_name, $file_length) {
		$file_identifier = md5($file_name.$file_length);
		return $file_identifier;
	}

	private static function storeIdentifier($file_name, $file_length, $client_id, $file_id) {

		$identifier_filename = self::getIdentifierFilename($file_name, $file_length);

		if (($handle = fopen(video::getStageDir().$identifier_filename, "w")) !== false) {
			fputs($handle, $client_id."\n");
			fputs($handle, $file_id."\n");
			fclose($handle);
		}
	}

	private static function getClientId($file_name, $file_length) {
		
		$identifier_filename = self::getIdentifierFilename($file_name, $file_length);

		if (($handle = fopen(video::getStageDir().$identifier_filename, "r")) !== false) {
			//read client_id from file (in case this is resumed download after some while, it differs from session_id generated client_id)
			$client_id = substr(fgets($handle), 0, -1);
			fclose($handle);
			return $client_id;
		} else {
			//we should never get here
			echo "Error: Can't find file identifier file for this upload!";
			debug_log("Jumploader error: Can't find file identifier file for this upload: file_identifier=$file_identifier");
			return false;
		}

	}

}

