<?php

class location {

	private $loc_id = NULL,
			$loc_type = NULL,
			$loc_name = NULL,
			$loc_url = NULL,
			$loc_parent = NULL,
			$s = NULL,
			$dir = NULL,
			$country_id = NULL,
			$country_sub = NULL,
			$significant = null,
			$description = NULL,
			$image_filenames = NULL,
			$video_html = NULL,

			$parent = NULL,
			$country = NULL;

	var $locs = array(); /* locs we have worldwide */

	function __construct($db = NULL) {
	}

	public static function findOneById($loc_id) {
		global $mcache;

		$loc_row = $mcache->get("SQL:LOC-ID", array($loc_id));
		if ($loc_row === false)
			return NULL;

		return self::withRow($loc_row);
	}
	
	public static function withRow($loc_row) {

		$loc = new location();
		$loc->setId($loc_row["loc_id"]);
		$loc->setType($loc_row["loc_type"]);
		$loc->setName($loc_row["loc_name"]);
		$loc->setLocUrl($loc_row["loc_url"]);
		$loc->setLocParent($loc_row["loc_parent"]);
		$loc->setS($loc_row["s"]);
		$loc->setDir($loc_row["dir"]);
		$loc->setCountryId($loc_row["country_id"]);
		$loc->setCountrySub($loc_row["country_sub"]);
		$loc->setSignificant($loc_row["significant"]);
		$loc->setDescription($loc_row["description"]);
		$loc->setImageFilenames($loc_row["image_filenames"]);
		$loc->setVideoHtml($loc_row["video_html"]);

		return $loc;
	}

	//getters and setters
	public function getId() {
		return $this->loc_id;
	}
	public function getType() {
		return $this->loc_type;
	}
	public function getName() {
		return $this->loc_name;
	}
	public function getLocUrl() {
		return $this->loc_url;
	}
	public function getLocParent() {
		return $this->loc_parent;
	}
	public function getS() {
		return $this->s;
	}
	public function getDir() {
		return $this->dir;
	}
	public function getCountryId() {
		return $this->country_id;
	}
	public function getCountrySub() {
		return $this->country_sub;
	}
	public function getSignificant() {
		return $this->significant;
	}
	public function getDescription() {
		return $this->description;
	}
	public function getImageFilenames() {
		return $this->image_filenames;
	}
	public function getVideoHtml() {
		return $this->video_html;
	}

	public function setId($loc_id) {
		$this->loc_id = $loc_id;
	}
	public function setType($loc_type) {
		$this->loc_type = $loc_type;
	}
	public function setName($loc_name) {
		$this->loc_name = $loc_name;
	}
	public function setLocUrl($loc_url) {
		$this->loc_url = $loc_url;
	}
	public function setLocParent($loc_parent) {
		$this->loc_parent = $loc_parent;
	}
	public function setS($s) {
		$this->s = $s;
	}
	public function setDir($dir) {
		$this->dir = $dir;
	}
	public function setCountryId($country_id) {
		$this->country_id = $country_id;
	}
	public function setCountrySub($country_sub) {
		$this->country_sub = $country_sub;
	}
	public function setSignificant($significant) {
		$this->significant = $significant;
	}
	public function setDescription($description) {
		$this->description = $description;
	}
	public function setImageFilenames($image_filenames) {
		$this->image_filenames = $image_filenames;
	}
	public function setVideoHtml($video_html) {
		$this->video_html = $video_html;
	}


	//relationship functions
	public function getParent() {
		if ($this->parent == NULL) {
			$this->parent = $this->findOneById($this->loc_parent);
		}
		return $this->parent;
	}

	public function getCountry() {
		if ($this->loc_type == 1)
			return $this;
		if ($this->country == NULL) {
			$this->country = $this->findOneById($this->country_id);
		}
		return $this->country;
	}

	//CRUD functions
	function update() {
		global $account, $db;

		if (!$this->loc_id)
			return false;

		$original = location::findOneById($this->loc_id);
		if (!$original)
			return false;

		$update_fields = array();
		if ($this->loc_id != $original->getId())
			$update_fields["loc_id"] = $this->loc_id;
		if ($this->loc_type != $original->getType())
			$update_fields["loc_type"] = $this->loc_type;
		if ($this->loc_name != $original->getName())
			$update_fields["loc_name"] = $this->loc_name;
		if ($this->loc_url != $original->getLocUrl())
			$update_fields["loc_url"] = $this->loc_url;
		if ($this->loc_parent != $original->getLocParent())
			$update_fields["loc_parent"] = $this->loc_parent;
		if ($this->s != $original->getS())
			$update_fields["s"] = $this->s;
		if ($this->dir != $original->getDir())
			$update_fields["dir"] = $this->dir;
		if ($this->country_id != $original->getCountryId())
			$update_fields["country_id"] = $this->country_id;
		if ($this->country_sub != $original->getCountrySub())
			$update_fields["country_sub"] = $this->country_sub;
		if ($this->significant != $original->getSignificant())
			$update_fields["significant"] = $this->significant;
		if ($this->description != $original->getDescription())
			$update_fields["description"] = $this->description;
		if ($this->image_filenames != $original->getImageFilenames())
			$update_fields["image_filenames"] = $this->image_filenames;
		if ($this->video_html != $original->getVideoHtml())
			$update_fields["video_html"] = $this->video_html;

		if (empty($update_fields))
			return true;	//nothing to update

		$changed_fields = "";
		$update = "";
		$update_arr = array();
		foreach ($update_fields as $key => $val) {
			$changed_fields .= (empty($changed_fields)) ? "" : ", ";
			$update .= (empty($update)) ? "SET " : ", ";
			$changed_fields .= $key;
			$update .= "{$key} = ?";
			$update_arr[] = $val;
		}
		$update_arr[] = $this->loc_id;
		$update = "UPDATE location_location {$update} WHERE loc_id = ? LIMIT 1";

		$db->q($update, $update_arr);
		$aff = $db->affected();

		if (!$aff)
			return false;

		audit::log("LOC", "Edit", $this->loc_id, "Changed fields: {$changed_fields}", $account->getId());
		return true;
	}


	//additional functions

	public function addImageFilename($filename) {
		if ((strlen($this->image_filenames) + strlen($filename) + 1) > 254)
			return false;
		if (!empty($this->image_filenames))
			$this->image_filenames .= "|";
		$this->image_filenames .= $filename;
		return true;
	}

	public function getLabel() {
		$label = $this->getName();
		if ($this->getType() == 3)
			$label .= ", ".$this->getParent()->getS();
		return $label;
	}

	public function isInternational() {
		if (in_array($this->country_id, array(16046, 16047, 41973)))
			return false;
		return true;
	}

	public function getPath($trailing_slash = false) {
		if ($this->isInternational()) {
			$path = "";
			if ($this->getLocParent() != $this->getCountryId()) {
				$parent = $this->getParent();
				if ($parent) {
					$path .= "/".$parent->getDir();
				}
			}
			$path .= "/".$this->getDir();
		} else {
			$path = substr($this->getLocUrl(),0,-1);
		}

		if ($trailing_slash)
			$path .= "/";

		return $path;
	}

	public function getUrl($trailing_slash = false) {
		global $ctx, $config_domain;

		$url = "https://";

		if ($this->getCountryId() != 16046) {
			$url .= $this->getCountry()->getDir().".";
		}
		
		$url .= $config_domain;

		$url .= $this->getPath($trailing_slash);

		return $url;		
	}

	public static function getUrlStatic($country_sub, $loc_url, $dir = "") {
		global $ctx, $config_domain;

		$url = "https://";

		if (empty($country_sub) || $country_sub == "united-states" || $country_sub == "ca" || $country_sub == "uk")
			$domestic = true;
		else
			$domestic = false;

		if (!empty($country_sub) && ($country_sub != "united-states"))
			$url .= $country_sub.".";
		
		$url .= $config_domain;

		if (!$domestic && !empty($dir))
			$url .= "/".$dir."/";
		else
			$url .= $loc_url;

		return $url;		
	}

	//----------------
	//LEGACY functions

	public function _coreLocationBreadArray($parentId) {
		global $db;

		if( !$parentId )
			return NULL;

		$locs = array();
		while($parentId>0) {
			$res = $db->q("select loc_parent, loc_name, loc_id, s, loc_type from location_location where loc_id='$parentId'");
			$row = $db->r($res);

			$parentId = $row[0];

			$locs[] =  array(
				"loc_id" => $row[2],
				"loc_name" => $row[1],
				"loc_link" => preg_replace("/\s/i", "-", $row[1]),
				"loc_parent" => $row[0],
				"loc_type" => $row["loc_type"],
				"s" => $row[3]
			);
			if( $parentId == 0 )
				break;
		}

		if( count($locs)>0 )
			return $locs;
		
		return NULL;
	}

	public function returnZip($zip, $loc_id) {
		global $db;

		if( !empty($zip) ) {
			$zip = preg_replace("/\s+/i", "", $zip);
			$zipsql = $db->q("select id from core_zip where zip = '$zip' limit 1");
			if( $db->numrows($zipsql)>0 )
				return $zip;
			else
				unset($zip);
		}

		if( !isset($zip) || empty($zip)) {
			$zipsql = $db->q("select zip from core_zip where loc_id = $loc_id limit 1");
			if( $db->numrows($zipsql)>0 ) {
				$zipr = $db->r($zipsql);
				$zip = $zipr["zip"];
					return $zip;
			}
		}
		return NULL;
	}

	public static function returnLocId($zip) {
		global $db;

		$zip = preg_replace("/\s+/i", "", $zip);
		$zipsql = $db->q("select loc_id from core_zip where zip = '$zip' limit 1");
		if( $db->numrows($zipsql)>0 ) {
			$row = $db->r($zipsql);
			return $row["loc_id"];
		}
		return NULL;
	}

	public static function isZipValid($zip) {
		return (self::returnLocId($zip) == NULL) ? false : true;
	}

	/* $table = name of the table in the db
	* $loclevel = loc_id = 1, state_id = 2, country_id = 3 */
	private static function createModuleList($table, $loclevel, $country_id = 0) {
		return array("table"=>"$table", "loclevel"=>"$loclevel", "country_id"=>$country_id);
	}

	public static function createModules() {
		$modules = array();
		$modules[] = self::createModuleList('classifieds_loc', 2);
		return $modules;
	}

	public static function createcitiesindb() {
		global $db;

		$modules = self::createModules();

//		$db->q("truncate table home_cities");

		//Unified database
		$modules[] = array("table" => "place", "loclevel" => 1, "country_id" => NULL);

		$db->q("UPDATE location_location SET has_place_or_ad = 0");

		//foreach($this->modules as $mod) {
		foreach($modules as $mod) {
			if ($mod['table'] == "classifieds_loc") {
				$res = $db->q("SELECT DISTINCT x.loc_id, l.loc_parent, l.country_id, l.loc_name
								FROM {$mod['table']} x
								INNER JOIN location_location l using (loc_id)
								WHERE l.loc_type = 3 AND x.done > 0");
			} else {
				$res = $db->q("SELECT DISTINCT x.loc_id, l.loc_parent, l.country_id, l.loc_name
								FROM {$mod['table']} x
								INNER JOIN location_location l using (loc_id)
								WHERE l.loc_type = 3 AND x.deleted IS NULL AND x.edit = 1");
			}
			while($row = $db->r($res)) {
				if( $row[1] == $row[2] || $row[1] == 0 ) {
					$row[1] = 0;
				}
//				if ($row[2] == 41756)
//					continue;
//				$db->q("INSERT IGNORE INTO home_cities 
//						(loc_id, state_id, country_id, `order`) 
//						values
//						(?, ?, ?, ?)", array($row[0], $row[1], $row[2], $order));
				
				$db->q("UPDATE location_location SET has_place_or_ad = 1 WHERE loc_id = ?", array($row[0]));
			}
		}
	}


	public static function getLocationPath($loc_id) {

		$loc = self::findOneById($loc_id);
		if (!$loc)
			return false;

		return $loc->getUrl();
	}

	public static function ajax($par1, $par2) {
		global $db;

		if ($par1[0] == "search") {

			$type = $par2["type"];

			$term = $par2['term'];
			if (empty($term))
				return;

			if ($type == "place_edit") {
				//type == 'place_edit' on worker place edit page location loookup
				$res = $db->q("
					SELECT l.loc_id, l.loc_type, l.loc_name, l.s 
					FROM location_location l
					INNER JOIN location_location lp on lp.loc_id = l.loc_parent
					WHERE l.loc_type IN (3,4) 
						AND ((lp.loc_type = 2 AND l.significant = 1) OR (lp.loc_type = 1))
						AND l.loc_name like ?
					LIMIT 10",
					["{$term}%"]
					);
			} else {
				$onlycountry = ($par2["onlycountry"] == 1) ? true : false;
				$whereadd = "";
				if ($onlycountry) {
					$whereadd = " AND loc_type = 1 ";
				}
				$res = $db->q("SELECT loc_id, loc_type, loc_name, s FROM location_location WHERE loc_name like ? {$whereadd} LIMIT 10",
						["%{$term}%"]
						);
			}

			$results = [];
			while ($row = $db->r($res)) {
				switch ($row["loc_type"]) {
					case 1: $label = "Country: "; break;
					case 2: $label = "State: "; break;
					case 3: $label = "City: "; break;
					case 4: $label = "Neighborhood: "; break;
				}
				$label .= $row["loc_name"];
				if (!empty($row["s"]))
					$label .= ", ".$row["s"];
				$results[] = array("id" => $row["loc_id"], "value" => $row["loc_name"], "label" => $label);
			}
			echo json_encode($results);
			return;
		}
	
		return "";	
	}

	public static function setDefaultLocation($loc_id) {
		global $account, $ctx;
		if (!isset($_COOKIE["def_loc_id"]) || $_COOKIE["def_loc_id"] != $loc_id)
			$account->setcookie("def_loc_id", $loc_id, 30);
	}

	public static function getLocationSelect($id, $input_name, $onlycountry = false) {
		$mod = "";
		if ($onlycountry)
			$mod = "?onlycountry=1";

		return "
<script>
$(function() {
	$( \"#{$id}\" ).autocomplete({
		source: \"/ajax/location/search{$mod}\",
		minLength: 2,
		change: function(event, ui) {
			$('#{$id}_hidden').val(ui.item.id);
		}
	});
});
</script>
<style>
.ui-autocomplete-loading {
	background: white url('/images/ui-anim_basic_16x16.gif') right center no-repeat;
}
</style>
<input id=\"{$id}\" /><input type=\"hidden\" name=\"{$input_name}\" id=\"{$id}_hidden\" value=\"\" />";
	}


	public static function getHomeCities($all = false, $columnize = true) {
		global $db, $ctx;

		$column_boundaries = array(
			"16046" => array(
				"1" => "Florida",
				"2" => "Maryland",
				"3" => "New York",
				"4" => "South Dakota"
				),
			"16047" => array(
				"1" => "Manitoba",
				"2" => "Nova Scotia",
				"3" => "Prince",
				"4" => "Saskat"
				),
			"41973" => array(
				"1" => "Scotland",
				"2" => "Wales",
				"3" => "Ireland",
				"4" => "Z"
				)
			);

		$columns = 5;

		$cities_in_state = array();
		$last_state = NULL;
		$mainGroupCityCount = array();
		$mainGroups = [];
		$last_mainGroupKey = null;
		$res = $db->q("
			SELECT l.country_id, l.loc_parent, p.loc_name as state_name, p.loc_url as state_url, l.loc_id, l.loc_name, l.s, l.country_sub, l.loc_url, l.significant
			FROM location_location l
			INNER JOIN location_location p on p.loc_id = l.loc_parent
			WHERE l.has_place_or_ad = 1
			ORDER BY l.country_id, l.loc_parent, l.loc_name
			", []);
		while ($row = $db->r($res)) {
			$country_id = $row["country_id"];
			$state_id = $row["loc_parent"];
			$significant = $row["significant"];

			if (!$all && in_array($country_id, [16046,16047,41973]) && !$significant)
				continue;

			switch ($country_id) {
				case 16046: $mainGroupKey = "us"; break;
				case 16047: $mainGroupKey = "ca"; break;
				case 41973: $mainGroupKey = "uk"; break;
				case 41744:
				case 41757:
				case 41763:
				case 41764:
				case 41770:
				case 41776:
				case 41796:
				case 41799:
				case 41800:
				case 41810:
				case 41812:
				case 41816:
				case 41817:
				case 41823:
				case 41826:
				case 41841:
				case 41842:
				case 41850:
				case 41863:
				case 41868:
				case 41869:
				case 41870:
				case 41872:
				case 41878:
				case 41886:
				case 41887:
				case 41889:
				case 41897:
				case 41907:
				case 41918:
				case 41919:
				case 41923:
				case 41924:
				case 41936:
				case 41940:
				case 41941:
				case 41947:
				case 41953:
				case 41954:
				case 41971:
				case 41993:
				case 100559:
					$mainGroupKey = "europe"; break;
				case 41752:
				case 41759:
				case 41767:
				case 41769:
				case 41773:
				case 41773:
				case 41786:
				case 41790:
				case 41794:
				case 41797:
				case 41803:
				case 41805:
				case 41807:
				case 41818:
				case 41835:
				case 41836:
				case 41839:
				case 41851:
				case 41884:
				case 41901:
				case 41912:
				case 41915:
				case 41920:
				case 41964:
				case 41975:
				case 41978:
				case 41980:
				case 41981:
				case 41996:
				case 41997:
				case 100237:
					$mainGroupKey = "latin"; break;
				case 41798:
				case 41849:
				case 41854:
				case 41908:
				case 41921:
				case 41934:
				case 41966:
				case 41972:
					$mainGroupKey = "middle_east"; break;
				case 41779:
				case 41787:
				case 41843:
				case 41844:
				case 41845:
				case 41846:
				case 41852:
				case 41858:
				case 41858:
				case 41871:
				case 41875:
				case 41888:
				case 41893:
				case 41895:
				case 41896:
				case 41899:
				case 41909:
				case 41916:
				case 41937:
				case 41939:
				case 41956:
				case 41959:
				case 41979:
				case 99903:
					$mainGroupKey = "asia"; break;
				case 41756:
				case 41788:
				case 41793:
				case 41804:
				case 41815:
				case 41819:
				case 41830:
				case 41857:
				case 41885:
				case 41900:
					$mainGroupKey = "australia"; break;
				case 41780:
				case 41785:
				case 41795:
				case 41801:
				case 41806:
				case 41809:
				case 41811:
				case 41856:
				case 41866:
				case 41873:
				case 41882:
				case 41891:
				case 41902:
				case 41903:
				case 41944:
					$mainGroupKey = "africa"; break;
				default:
					$mainGroupKey = "other"; break;
			}

			$link_base = "https://";
			$link_base .= ($country_id == 16046) ? "" : $row["country_sub"].".";
			$link_base .= $ctx->domain_base;

			$mainGroupCityCount[$mainGroupKey] = intval($mainGroupCityCount[$mainGroupKey]) + 1;

			if ($last_state && $last_state["state_id"] != $state_id) {
				$last_state["cities"] = $cities_in_state;
				$mainGroups[$last_mainGroupKey][] = $last_state;
				$cities_in_state = array();
			}
			$cities_in_state[] = array(
				"loc_name" => $row["loc_name"],
				"title" => "{$row["loc_name"]}, {$row["s"]}",
				"loc_url" => $link_base.$row["loc_url"]
				);

			$last_state = array(
				"state_id" => $row["loc_parent"],
				"state_name" => $row["state_name"],
				"state_url" => $link_base.$row["state_url"]
				);
			$last_mainGroupKey = $mainGroupKey;
		}
		$last_state["cities"] = $cities_in_state;
		$mainGroups[$last_mainGroupKey][] = $last_state;

		if (!$columnize)
			return $mainGroups;

		//--------------------------
		//even chunking into columns
		$mainGroupsColumnized = [];
		foreach ($mainGroups as $mainGroupKey => $states) {
			
			if (array_key_exists($mainGroupKey, $column_boundaries)) {
				//manual column splitting
				$columnized = array();
				$curr_index = 0;
				foreach($states as $state) {
					$ret = strcmp($state["state_name"], $column_boundaries[$mainGroupKey][($curr_index+1)]);
					if ($ret != -1)
						$curr_index++;
					$columnized[$curr_index][] = $state;
				}
				$mainGroupsColumnized[$mainGroupKey] = $columnized;
			} else {
				//automatic column splitting
				$loc_per_column = ceil($mainGroupCityCount[$mainGroupKey] / $columns);
				$columnized = array();
				$running_count = 0;
				foreach($states as $state) {
					$column_number = floor($running_count / $loc_per_column);
					if ($column_number >= $columns)
						$column_number = ($columns - 1);
					$columnized[$column_number][] = $state;
					$running_count += count($state["cities"]);
				}
				$mainGroupsColumnized[$mainGroupKey] = $columnized;
			}

		}

		return $mainGroupsColumnized;
	}

	public static function getCountryHtmlOptions() {
		global $db;

		$country_options = array("" => "- Select Country -");
		$res = $db->q("SELECT loc_name FROM location_location WHERE loc_type = 1 ORDER BY loc_name ASC");
		while ($row = $db->r($res)) {
			$country_options[$row["loc_name"]] = $row["loc_name"];
		}
		return $country_options;
	}

	public static function getCountryCodeHtmlOptions() {
		global $db;

		$country_options = array("" => "- Select Country -");
		$res = $db->q("SELECT s, loc_name FROM location_location WHERE loc_type = 1 AND s <> '' AND s IS NOT NULL ORDER BY loc_name ASC");
		while ($row = $db->r($res)) {
			$country_options[$row["s"]] = $row["loc_name"];
		}
		return $country_options;
	}

	/**
	 * Return 2-character country code
	 * @staticvar array $a
	 * @param int $country_id
	 * @return string
	 */
	public static function getCountryCodeByCountryId($country_id) {
		global $db;


		$res = $db->q("SELECT s FROM location_location WHERE loc_id=?", array($country_id));
		$row = $db->r($res);
		if( isset($row['s'])) {
			return strtolower($row['s']);
		} else  {
			error_log(printf("Country not found for country ID %d", $country_id));
			return 'us';
		}
	}

	public static function is_big_city($loc_id){
		global $db;
		$res = $db->q("SELECT * FROM location_topcities WHERE big = 1 AND loc_id=?", [$loc_id]);
		return $db->numrows($res) > 0 ? true : false;
	}

	public function getStickyPrice($ad_type, $days) {
		return classifieds::get_sticky_upgrade_price($ad_type, $this->getId(), $days);
	}

}

