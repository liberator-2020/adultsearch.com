<?php

define("LOG_DIR", _CMS_ABS_PATH."/tmp/log");

/**
 * Simple calss for logging
 * Autiomatically adds timestamp and actor identification
 * Automatically rotates logs (1 quarterly, 1 backup file)
 */
class log {
	var $log_file_name_base = NULL;	//base name of log file

	public function __construct($type) {
		switch ($type) {
			case "classifieds": $this->log_file_name_base = "classifieds"; break;
			default: $this->log_file_name_base = "default"; break;
		}

		//check existence of log directory
		if (!is_dir(LOG_DIR))
			return mkdir(LOG_DIR, 0755, true);
		
		return true;
	}
	
	private function getLogPath() {
		return LOG_DIR."/".$this->log_file_name_base.".log";
	}

	private function getOldPath() {
		return LOG_DIR."/".$this->log_file_name_base.".old";
	}

	private function quarter($timestamp) {
		$month = date("n", $timestamp);
		switch ($month) {
			case 1:
			case 2:
			case 3:
				return 1; break;
			case 4:
			case 5:
			case 6:
				return 2; break;
			case 7:
			case 8:
			case 9:
				return 3; break;
			case 10:
			case 11:
			case 12:
				return 4; break;
		}
		return false;
	}

	public function append($msg) {
		global $account;
		
		//first backup log file if there is new quarter
		$file_stamp = filectime($this->getLogPath());
		$curr_stamp = time();
		if (date("Y", $file_stamp) != date("Y", $curr_stamp) 
			|| $this->quarter($file_stamp) != $this->quarter($curr_stamp)) {
			$ret = rename($this->getLogPath(), $this->getOldPath());
			if (!$ret)
				return false;
		}

		$file = $this->getLogPath();
		if (($handle = fopen($filepath, "w")) === false)
			return false;

		$email = $account->getEmail();
		$id = $account->getId();
		if ($email == "" && $id == "")
			$user_str = "no_auth";
		else
			$user_str = "{$email}(account_id={$id})";

		$ret = fputs($handle, date("[Y-m-d H:i:s T]")." {$user_str} : {$msg}\n");
		fclose($handle);

		if ($ret === false)
			return false;

		return true;
	}

}

//end class log
