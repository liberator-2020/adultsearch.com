<?php

class mcache {
	var $db;
	var $mc_client;
	var $log = array();
	var $multiple_results = false;

	var $queries = array(
		"SQL:LOC-ID" =>	   "select * from location_location where loc_id = ?",
		"SQL:LOC-SUB" =>	  "select * from location_location where dir = ? and loc_type = 1",
		"SQL:LOC-DIR" =>	  "select * from location_location where dir = ?",
		"SQL:LOC-DIR-HAS" =>  "select * from location_location where dir = ? and has_place_or_ad = 1",
		"SQL:LOC-DIR-CTR" =>  "select * from location_location where dir = ? and loc_type = 1",
		"SQL:LOC-DIR-PAR" =>  "select * from location_location where dir = ? and loc_parent = ?",
		"SQL:LOC-NAM"	 =>  "select * from location_location where loc_name like ?",
		"SQL:LOC-CTR-NAM" =>  "select * from location_location where loc_name like ? and loc_type = 1",
		"SQL:LOC-NAM-CTR" =>  "select * from location_location where loc_name like ? and country_id = ?",
		"SQL:LOC-NAM-PAR" =>  "select * from location_location where loc_name like ? and loc_parent = ?",
		"SQL:LOC2-S-CTR" =>   "select * from location_location where loc_type = 2 and s like ? and country_id = ?",
		"SQL:LOC2-NAM-CTR" => "select * from location_location where loc_type = 2 and loc_name like ? and country_id = ?",
		"SQL:LUZ-ID" =>	   "select county from location_uszip where county_id = ? group by county",
		
		"SQL:PTY-ID" =>	   "select * from place_type where place_type_id = ?",
		"SQL:PTY-URL" =>	  "select * from place_type where url = ?",
		"SQL:ATT-ID" =>	   "select * from attribute where attribute_id = ?",
		"SQL:ATT-NAM" =>	  "select * from attribute where name = ?",
		
		"SQL:FNA-ID" =>	   "select * from forum_name where forum_id = ?",
		"SQL:FNA-URL" =>	  "select * from forum_name where forum_url = ?",
		"SQL:FNA-NAM" =>	  "select * from forum_name where forum_name = ?",
		
		"SQL:BPL-URL" =>	  "select * from bp_promo_loc_lookup where url_name = ?",

		"SQL:PER-NAM" =>	  "select * from permission where name = ?",
		);

	public function __construct($db) {
		global $config_memcached_server, $config_memcached_port;

		$this->mc_client = new Memcached();
		$this->mc_client->addServer($config_memcached_server, $config_memcached_port);

		$this->db = $db;
	}
	
	/**
	 *
	 * $first - if set to true, then in case of multiple results dont return error, but return first result
	 */
	public function get($code, $params, $first = false) {
		global $account;

		if (!is_array($params))
			$params = array($params);

		//prepare key (we dont care about escaping keys here)
		$key = $code.":".implode(":",$params);

		
		//if ($account->isrealadmin() && substr($key, 0, 10) == "SQL:PTY-ID") {
			//$ip = exec("ifconfig | grep 173.239.54 | sed -e 's/.*\\(173\\.239\\.54\\.[0-9]*\\)[^0-9].*/\\1/'");	
		/*	$all_keys = $this->mc_client->getAllKeys();
			$ptids = array();
			foreach ($all_keys as $mk) {
				if (substr($mk, 0, 10) != "SQL:PTY-ID")
					continue;
				$ptids[] = substr($mk, 11);
			}
			sort($ptids);
			$ptid_str = "";
			foreach ($ptids as $ptid) {
				$ptid_str .= $ptid.", ";
			}
			debug_log("IP: '{$ip}', cnt_total=".count($all_keys).", cnt_ptids=".count($ptids).", PTIds: ".$ptid_str." !");
		}
		*/

		$val = $this->mc_client->get($key);
			global $account;
		//var_dump(->getResultCode());	 //RES_SUCCESS : 0   //RES_NOTFOUND : 16
		if ($this->mc_client->getResultCode() == Memcached::RES_SUCCESS) {
			//debug_log("MC - key $key found in cahce");
			$row = unserialize($val);
			//$row = $val; 
			$this->log[] = "hit - {$key}";
			return $row;
		}
		//if ($account->isrealadmin()) {
		//	debug_log("MC - key $key NOT found in cache on ip '{$ip}', rescode=".$this->mc_client->getResultCode().", val='".print_r($val, true)."'");
		//}

		//prepare db query - we escape all parameters
		if (!array_key_exists($code, $this->queries)) {
			$this->log[] = "unknown_code - {$key}";
			return false;
		}

		$sql = $this->queries[$code];
		$res = $this->db->q($sql, $params);

		$this->multiple_results = false;
		if ($this->db->numrows($res) > 1)
			$this->multiple_results = true;

		if ($this->db->numrows($res) == 0 || (!$first && $this->db->numrows($res) > 1)) {
			//debug_log("MC lkp fail: '$key' -> # results = ".$this->db->numrows($res));
			$this->log[] = "mcfail - {$key}";
			return false;
		}
		$row = $this->db->r($res, MYSQL_ASSOC);	//by default there is MYSQL_BOTh, but we want to save space ...

		//store value to memcache
		//$this->mc_client->set($key, $row, 60 * 60);
		$this->mc_client->set($key, serialize($row), 60 * 60);  //TODO store for how long ?
		
		$this->log[] = "miss - {$key}";
		return $row;
	}

	public function flush() {
		return $this->mc_client->flush();
	}

	public function getLog() {
		return $this->log;
	}

}

