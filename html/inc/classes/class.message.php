<?php
/*
 * message = private message
 */
class message {

	private $id = NULL,
			$author_id = NULL,
			$recipient_id = NULL,
			$sent_stamp = NULL,
			$subject = NULL,
			$content = NULL,
			$conversation_id = NULL,
			$ip_address = NULL,
			$read_stamp = NULL,
			$author_deleted_stamp = NULL,
			$recipient_deleted_stamp = NULL;

	private $author = NULL;
	private $recipient = NULL;
	private $images = NULL;

	function __construct() {
	}

	public static function findOneById($id) {
		global $db;

		$res = $db->q("SELECT * FROM message WHERE id = ?", array(intval($id)));
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

		return self::withRow($row);
	}
	
	public static function withRow($row) {

		$message = new message();
		$message->setId($row["id"]);
		$message->setAuthorId($row["author_id"]);
		$message->setRecipientId($row["recipient_id"]);
		$message->setSentStamp($row["sent_stamp"]);
		$message->setSubject($row["subject"]);
		$message->setContent($row["content"]);
		$message->setConversationId($row["conversation_id"]);
		$message->setIpAddress($row["ip_address"]);
		$message->setReadStamp($row["read_stamp"]);
		$message->setAuthorDeletedStamp($row["author_deleted_stamp"]);
		$message->setRecipientDeletedStamp($row["recipient_deleted_stamp"]);

		return $message;
	}
	
	public static function getReceivedMessages($recipient_id = NULL) {
		global $db, $account;

		if (is_null($recipient_id)) {
			$recipient_id = $account->getId();
		}

		$messages = [];
		$res = $db->q("SELECT * FROM message WHERE recipient_id = ? AND recipient_deleted_stamp IS NULL ORDER BY sent_stamp DESC", array(intval($recipient_id)));
		while ($row = $db->r($res)){
			$messages[] = self::withRow($row);
		}

		return $messages;
	}
	
	public static function getSentMessages($author_id = null) {
		global $db, $account;

		if (is_null($author_id)) {
			$author_id = $account->getId();
		}

		$messages = [];
		$res = $db->q("SELECT * FROM message WHERE author_id = ? AND author_deleted_stamp IS NULL ORDER BY sent_stamp DESC", array(intval($author_id)));
		while ($row = $db->r($res)){
			$messages[] = self::withRow($row);
		}

		return $messages;
	}
	
	//getters and setters
	public function getId() {
		return $this->id;
	}
	public function getType() {
		return $this->type;
	}
	public function getAuthorId() {
		return $this->author_id;
	}
	public function getRecipientId() {
		return $this->recipient_id;
	}
	public function getSentStamp() {
		return $this->sent_stamp;
	}
	public function getSubject() {
		return $this->subject;
	}
	public function getContent() {
		return $this->content;
	}
	public function getConversationId() {
		return $this->conversation_id;
	}
	public function getIpAddress() {
		return $this->ip_address;
	}
	public function getReadStamp() {
		return $this->read_stamp;
	}
	public function getAuthorDeletedStamp() {
		return $this->author_deleted_stamp;
	}
	public function getRecipientDeletedStamp() {
		return $this->recipient_deleted_stamp;
	}

	public function setId($id) {
		$this->id = $id;
	}
	public function setAuthorId($author_id) {
		$this->author_id = $author_id;
	}
	public function setRecipientId($recipient_id) {
		$this->recipient_id = $recipient_id;
	}
	public function setSentStamp($sent_stamp) {
		$this->sent_stamp = $sent_stamp;
	}
	public function setSubject($subject) {
		$this->subject = $subject;
	}
	public function setContent($content) {
		$this->content = $content;
	}
	public function setConversationId($conversation_id) {
		$this->conversation_id = $conversation_id;
	}
	public function setIpAddress($ip_address) {
		$this->ip_address = $ip_address;
	}
	public function setReadStamp($read_stamp) {
		$this->read_stamp = $read_stamp;
	}
	public function setAuthorDeletedStamp($author_deleted_stamp) {
		$this->author_deleted_stamp = $author_deleted_stamp;
	}
	public function setRecipientDeletedStamp($recipient_deleted_stamp) {
		$this->recipient_deleted_stamp = $recipient_deleted_stamp;
	}

	//relationships
	public function getAuthor() {
		if ($this->author)
			return $this->author;
		$acc = account::findOneById($this->author_id);
		if (!$acc)
			return NULL;
		$this->author = $acc;
		return $this->author;
	}
	public function getRecipient() {
		if ($this->recipient)
			return $this->recipient;
		$acc = account::findOneById($this->recipient_id);
		if (!$acc)
			return NULL;
		$this->recipient = $acc;
		return $this->recipient;
	}	

	public function getImages() {
		global $db;

		if ($this->images)
			return $this->images;

		$images = [];
		$res = $db->q("SELECT id, filename FROM message_file WHERE message_id = ? ORDER BY id ASC", array($this->id));
		while ($row = $db->r($res)) {
			$images[] = "/pm/file?id={$row["id"]}&filename={$row["filename"]}";
		}

		$this->images = $images;
		return $this->images;
	}

	//CRUD functions
	function update($audit = false) {
		global $account, $db;

		if (!$this->id)
			return false;

		$original = message::findOneById($this->id);
		if (!$original)
			return false;

		$update_fields = array();
		if ($this->author_id != $original->getAuthorId()) {
			$update_fields["author_id"] = $this->author_id;
			$this->author = NULL;
		}
		if ($this->recipient_id != $original->getRecipientId()) {
			$update_fields["recipient_id"] = $this->recipient_id;
			$this->recipient = NULL;
		}
		if ($this->sent_stamp != $original->getSentStamp())
			$update_fields["sent_stamp"] = $this->sent_stamp;
		if ($this->subject != $original->getSubject())
			$update_fields["subject"] = $this->subject;
		if ($this->content != $original->getContent())
			$update_fields["date"] = $this->content;
		if ($this->ip_address != $original->getIpAddress())
			$update_fields["ip_address"] = $this->ip_address;
		if ($this->read_stamp != $original->getReadStamp())
			$update_fields["read_stamp"] = $this->read_stamp;
		if ($this->author_deleted_stamp != $original->getAuthorDeletedStamp())
			$update_fields["author_deleted_stamp"] = $this->author_deleted_stamp;
		if ($this->recipient_deleted_stamp != $original->getRecipientDeletedStamp())
			$update_fields["recipient_deleted_stamp"] = $this->recipient_deleted_stamp;

		if (empty($update_fields))
			return true;	//nothing to update

		$changed_fields = "";
		$update = "";
		$update_arr = array();
		foreach ($update_fields as $key => $val) {
			$changed_fields .= (empty($changed_fields)) ? "" : ", ";
			$update .= (empty($update)) ? "SET " : ", ";
			$changed_fields .= "{$key}";
			if (method_exists($original, get_getter_name($key)))
				$changed_fields .= ": ".call_user_func(array($original, get_getter_name($key)))."->".call_user_func(array($this, get_getter_name($key)));
			$update .= "`{$key}` = ?";
			$update_arr[] = $val;
		}
		$update_arr[] = $this->id;
		$update = "UPDATE message {$update} WHERE id = ? LIMIT 1";

		$db->q($update, $update_arr);
		$aff = $db->affected();

		if (!$aff)
			return false;

		if ($audit)
			audit::log("PME", "Edit", $this->id, "Changed fields: {$changed_fields}", $account->getId());

		return true;
	}

	public function add() {
		global $db, $account;

		$res = $db->q("
			INSERT INTO message 
			(author_id, recipient_id, sent_stamp, subject, content, conversation_id, 
			ip_address, read_stamp, author_deleted_stamp, recipient_deleted_stamp) 
			VALUES 
			(?, ?, ?, ?, ?, ?, 
			?, ?, ?, ?);",
			array($this->author_id, $this->recipient_id, $this->sent_stamp, $this->subject, $this->content, $this->conversation_id, 
			$this->ip_address, $this->read_stamp, $this->author_deleted_stamp, $this->recipient_deleted_stamp)
		);
		$newid = $db->insertid($res);

		if ($newid == 0)
			return false;

		$this->id = $newid;

		audit::log("PME", "Add", $this->id, "", $account->getId());

		return true;
	}

	//additional functions

	/**
	 * Fucntions "sends" a message and takes care of unreadmessages counter of the recipient
	 */
	public static function send($author, $recipient, $subject, $content, $conversation_id = NULL) {
		global $db;

		$now = time();

		if ($recipient->getDeleted())
			return false;

		$message = new message();
		$message->setAuthorId($author->getId());
		$message->setRecipientId($recipient->getId());
		$message->setSubject($subject);
		$message->setContent($content);
		$message->setConversationId($conversation_id);
		$message->setSentStamp($now);
		$message->setIpAddress(account::getUserIp());
		$ret = $message->add();	

		if (!$ret)
			return false;

		//update unread messages field on recipient - we do this directly and not vie account::update function because we dont want to audit this - TODO ?
		$unread_messages = intval($recipient->getUnreadMessages()) + 1;
		$db->q("UPDATE account SET unread_messages = ? WHERE account_id = ? LIMIT 1", array($unread_messages, $recipient->getId()));
		$aff = $db->affected($res);
		if ($aff != 1)
			return false;

		//send email notification to recipient
		self::emailRecipientNotify($message->getId(), $author, $recipient);

		return $message->getId();
	}
	
	public function emailRecipientNotify($message_id, $author, $recipient) {
		global $db, $config_image_path, $config_site_url;

		$smarty = GetSmartyInstance();
		$smarty->assign("author", $author->getUsername());
		$smarty->assign("recipient", $recipient->getUsername());
		$smarty->assign("read_link", $config_site_url."/pm/read?id={$message_id}");

		$email = $recipient->getEmail();

		$html = $smarty->fetch(_CMS_ABS_PATH."/templates/pm/email/notify.html.tpl");
		$text = $smarty->fetch(_CMS_ABS_PATH."/templates/pm/email/notify.txt.tpl");
		$subject = "You have new private message";
		$embedded_images = array("logo_email" => _CMS_ABS_PATH."/images/logo_email.png");

		return send_email(array(
			"from" => "support@adultsearch.com",
			"to" => $email,
//			"bcc" => ADMIN_EMAIL,
			"subject" => $subject,
			"html" => $html,
			"text" => $text,
			"embedded_images" => $embedded_images
			));
	}

	/**
	 * Method checks if this is first time reading message and mark as read accordingly, also updates unread messages counter of recipient accordingly
	 * Returns number of unread messages
	 */
	public function checkReadStamp() {
		global $account, $db;

		if ($this->getReadStamp())
			return;		//this message was already read in past, nothing changes

		if ($this->getRecipientId() != $account->getId())
			return;		//we are not recipient of this message, nothing changes

		//update read stamp
		$this->setReadStamp(time());
		$this->update();
	
		//update unread message counter for recipient
		$unread = 0;
		$res = $db->q("SELECT count(*) as cnt FROM message WHERE recipient_id = ? AND read_stamp IS NULL", [$account->getId()]);
		if ($db->numrows($res)) {
			$row = $db->r($res);
			$unread = $row["cnt"];
		}
		$db->q("UPDATE account SET unread_messages = ? WHERE account_id = ? LIMIT 1", [$unread, $account->getId()]);
		
		return;
	}

	/**
	 * Method exports number of unread messages into smarty template as "unread" variable
	 */
	public static function exportUnread() {
		global $account, $smarty;
		$smarty->assign("unread", $account->getUnreadMessages());
	}

	/**
	 * Marks message as deleted by author or recipient.
	 */
	public function delete() {
		global $db, $account;

		if ($this->getAuthorId() == $account->getId()) {
			//deleting by author
			if ($this->getAuthorDeletedStamp())
				return true;	//message already deleted
			$res = $db->q("UPDATE message SET author_deleted_stamp = ? WHERE id = ? LIMIT 1", array(time(), $this->getId()));
		} elseif ($this->getRecipientId() == $account->getId()) {
			//deleting by recipient
			if ($this->getRecipientDeletedStamp())
				return true;	//message already deleted
			$res = $db->q("UPDATE message SET recipient_deleted_stamp = ? WHERE id = ? LIMIT 1", array(time(), $this->getId()));
		} else {
			//deleting by admin ? not supported now
			return true;
		}

		$aff = $db->affected($res);
		if ($aff == 1)
			return true;
		else
			return false;
	}

	public static function ajax($par1, $par2) {
		global $db;

		if ($par1[0] == "block") {
			$offender_id = $par2["offender_id"];
			self::blockOffender($offender_id);
			return;
		} else if ($par1[0] == "unblock") {
			$offender_id = $par2["offender_id"];
			self::unblockOffender($offender_id);
			return;
		}

		return "";
	}

	public static function blockOffender($offender_id) {
		global $account, $db;
	}

	public static function unblockOffender($offender_id) {
		global $account, $db;
	}

}

