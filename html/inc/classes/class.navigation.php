<?php

class navigation {

	function quickcategory() {
		global $db, $account, $smarty, $gTitle, $gDescription, $gKeywords, $sphinx, $ctx;

		//TS backlinks
		$ts_city_link = NULL;
		if (in_array($ctx->country, ["thailand", "philippines", "malaysia", "indonesia", "japan", "china"]))
			$ts_term = "Lady Boy";
		else
			$ts_term = "Shemale";
		$ts_link_label = "{$ctx->city_name} {$ts_term} Escorts";
		$ts_link_title = "{$ts_term} & Transsexual Escorts";
		if ($ctx->state && $ctx->city) {
			$ts_city_link = "https://www.tsescorts.com/{$ctx->state}/{$ctx->city}/shemale-escorts";
			$ts_link_title = "{$ctx->city_name}, {$ctx->state_name} Shemale & Transsexual Escorts";
		} else if ($ctx->country_name && $ctx->city) {
			$country_url = preg_replace('/ /', '-', strtolower($ctx->country_name));
			$ts_city_link = "https://www.tsescorts.com/{$country_url}/{$ctx->city}/shemale-escorts";
			$ts_link_title = "{$ctx->city_name}, {$ctx->country_name} Shemale & Transsexual Escorts";
		}

		$smarty->assign("ts_city_link", $ts_city_link);
		$smarty->assign("ts_link_label", $ts_link_label);
		$smarty->assign("ts_link_title", $ts_link_title);

		if (!$account->core_loc_id)
			return;

		$id = $account->core_loc_type > 2 ? implode(',', $account->locAround()) : $account->core_loc_id;

		$sphinx->reset();
		$sphinx->SetLimits( 0, 1, 1);
		$sphinx->SetFilter('@id', array($id));
		if ($account->core_loc_type > 2) {
			$sphinx->SetFilter('loc_parent', array($account->core_loc_array['loc_parent']));
		}
		$results = $sphinx->Query("", "home");

		if( is_array($account->core_neighbor_array) && $account->core_neighbor_array['loc_id'] ) {
			$smarty->assign("state_link", "http://{$account->core_neighbor_array['sub_parent']}.adultsearch.com");
		}

		$total = $results["total_found"];
		$result = $results["matches"];
		if( !$total ) {
			return;
		}

		$res = $db->q("select * from home_cache where state_id in ($id)");
		if (!$db->numrows($res))
			return;
		$row2 = NULL;
		while ($row = $db->r($res)) {
			if (!isset($row2)) {
				$row2 = $row;
			} else {
				foreach($row as $key=>$value) $row2[$key] += $value;
			}
		}

		$row = $row2;

		$title = NULL;
		$cats = array();
		if( $row["cl1"] > 0 ) {
			$category[] = array("section"=>"cl1", "title"=>"Female Escorts ({$row["cl1"]})", "selected"=>$account->module=="cl1"?true:false);
			$title[] = "Female Escorts ({$row["cl1"]})";
			$cats[] = "Female Escorts";
		}
		if( $row["cl2"] > 0 ) {
			$category[] = array("section"=>"cl2", "title"=>"TS/TV Shemale Escorts ({$row["cl2"]})", "selected"=>$account->module=="cl2"?true:false);
			$title[] = "TS/TV Shemale Escorts ({$row["cl2"]})";
			$cats[] = "TS/TV Shemale Escorts";
		}
		if( $row["emp1"] > 0 ) {
			$category[] = array("section"=>"emp1", "title"=>"Erotic Massage Parlors ({$row["emp1"]})", "selected"=>$account->module=="emp1"?true:false);
			$title[] = "Erotic Massage Parlors ({$row["emp1"]})";
			$cats[] = "Erotic Massage Parlors";
		}
		if( $row["sc1"] > 0 ) {
			$category[] = array("section"=>"sc1", "title"=>"Strip Clubs ({$row["sc1"]})", "selected"=>$account->module=="sc1"?true:false);
			$title[] = "Strip Clubs ({$row["sc1"]})";
			$cats[] = "Strip Clubs";
		}
		if( $row["as1"] > 0 ) {
			$category[] = array("section"=>"as1", "title"=>"Sex Shops ({$row["as1"]})", "selected"=>$account->module=="as1"?true:false);
			$title[] = "Sex Shops ({$row["as1"]})";
			$cats[] = "Sex Shops";
		}
		if( $row["ls1"] > 0 ) {
			$category[] = array("section"=>"ls1", "title"=>"Swinger Clubs ({$row["ls1"]})", "selected"=>$account->module=="ls1"?true:false);
			$title[] = "Swinger Clubs ({$row["ls1"]})";
			$cats[] = "Swinger Clubs";
		}
		if( $row["ls2"] > 0 ) {
			$category[] = array("section"=>"ls2", "title"=>"BDSM Clubs ({$row["ls2"]})", "selected"=>$account->module=="ls2"?true:false);
			$title[] = "BDSM Clubs ({$row["ls2"]})";
			$cats[] = "BDSM Clubs";
		} 
		if( $row["cl6"] > 0 ) {
			$category[] = array("section"=>"cl6", "title"=>"Body Rubs ({$row["cl6"]})", "selected"=>$account->module=="cl6"?true:false);
			$title[] = "Body Rubs ({$row["cl6"]})";
			$cats[] = "Body Rubs";
		}
		if( $row["brothel1"] > 0 ) {
			$category[] = array("section"=>"brothel1", "title"=>"Brothels ({$row["brothel1"]})", "selected"=>$account->module=="brothel1"?true:false);
			$title[] = "Brothels ({$row["brothel1"]})";
			$cats[] = "Brothels";
		}
		if( $row["cl4"] > 0 ) {
			$category[] = array("section"=>"cl4", "title"=>"Male for Male Escorts ({$row["cl4"]})", "selected"=>$account->module=="cl4"?true:false);
			$title[] = "Male for Male Escorts ({$row["cl4"]})";
			$cats[] = "Male for Male Escorts";
		}
		//2019-06-21 zach: disable dropdown
		//$smarty->assign("quickcategory", $category);

		if ($account->core_loc_id) {
		    $loc_name = $account->core_loc_array["loc_name"]." ";
		}

		if (empty($gTitle)) {
			$gTitle = implode(', ', $title);
		}
		if (empty($gDescription)) {
			$gDescription = $loc_name.implode(", {$loc_name}", $cats);
			if (strlen($gDescription) > 160) {
				//if description is longer than 160 chars, try to find last comma before 170 character count and cut the description there
				$last_comma_pos = strrpos($gDescription, ", ", 160 - strlen($gDescription));
				if ($last_comma_pos > 100)
					$gDescription = substr($gDescription, 0, $last_comma_pos);
			}
		}
		if (strlen($gDescription) < 50) {
			$gDescription .= (empty($gDescription)) ? "" : ", ";
			$gDescription .= "{$loc_name} Sensual Massage";
		}
		if (strlen($gDescription) < 50) {
			$gDescription .= (empty($gDescription)) ? "" : ", ";
			$gDescription .= "{$loc_name} Erotic Services";
		}
		if (empty($gKeywords)) {
			$gKeywords = $loc_name.implode(", {$loc_name}", $cats);
		}

		//jay 19.12.2017 - slixa backlinks
		//jay 08.08.2018 - slixa backlinks disabled
		/*
		if ($ctx->country_id == 41973)
			$res = $db->q("SELECT url, label, hover FROM backlink WHERE country = ? AND city = ?", [$ctx->country_name, $ctx->city_name]);
		else
			$res = $db->q("SELECT url, label, hover FROM backlink WHERE country = ? AND state = ? AND city = ?", [$ctx->country_name, $ctx->state_name, $ctx->city_name]);
		if ($row = $db->r($res))
			$smarty->assign("backlink", $row);
		*/

	}

}

//END
