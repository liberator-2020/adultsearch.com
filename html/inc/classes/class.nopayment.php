<?php

class nopayment {

	public function __construct() {
	}

	public function get_name() {
		return "nopayment";
	}

	public function get_template_path() {
		return _CMS_ABS_PATH."/templates/payment_gen_nopayment.tpl";
	}

	public function export_to_template() {
		global $smarty;
	}

	//payment needs to contain unobfuscated card number and csc
	public function charge($payment, $cardno_real, $csc_real, $captured = true, &$error) {
		global $db;
		return false;
	}

	public function capture($payment, &$error) {
		return false;
	}

	public function refund_transaction($transaction, $amount = null, &$error) {
		return false;
	}

	public function refund_charge($charge_id, $amount = null, &$error) {
		return false;
	}

}

//END 
