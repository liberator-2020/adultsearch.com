<?php

class pager {

	function __construct() {
	}

	private static function path_with_page($path, $page, $page_dir) {
		if ($page == 1)
			return $path;
		if ($page_dir)
			return $path."/page/{$page}";
		if (strpos($path, "?") === false)
			return $path."?page={$page}";
		return $path."&page={$page}";
	}

	/**
	 * page_dir: false = page is query parameter, true = page is added to URL path 
	 */
	public static function paging_array($total_item, $current_page, $path, $page_dir = false, $item_per_page = 20, $max_num_pages_in_pager = 5) {

		$total_pages = ceil($total_item/$item_per_page);
		$per_page = $item_per_page;

		if ($max_num_pages_in_pager >= $total_pages) {
			$start_page = 1;
			$end_page = $total_pages;
		} else if ($current_page < (ceil($max_num_pages_in_pager/2))) {
			$start_page = 1;
			$end_page = $max_num_pages_in_pager;
		} else if ($current_page > ($total_pages - ceil($max_num_pages_in_pager/2))) {
			$start_page = $total_pages - $max_num_pages_in_pager;
			$end_page = $total_pages;
		} else {
			$start_page = $current_page - ceil($max_num_pages_in_pager/2);
			$end_page = $start_page + $max_num_pages_in_pager;
			if( $start_page == 0 ) $start_page = 1;
		}

		$paging = array();

		if ($current_page > 1) {
			$prev = $current_page - 1;
			$paging[] = array(
					"type" => "prev",
					"page" => $prev,
					"link" => (($prev == 1) ? $path : self::path_with_page($path, $prev, $page_dir)),
				);
		}

		if ($start_page > 1) {
			$paging[] = array(
					"type" => "page",
					"page" => 1,
					"link" => $path,
				);
			if ($start_page > 2) {
				$paging[] = array(
					"type" => "dots"
					);
			}
		}
		
		for ($i = $start_page; $i <= $end_page; $i++) {
			if ($i == $current_page) {
				$paging[] = array(
					"type" => "current",
					"page" => $i,
					"last" => (($i == $end_page) ? true : false),
					);
			} else {
				$paging[] = array(
					"type" => "page",
					"page" => $i,
					"link" => (($i == 1) ? $path : self::path_with_page($path, $i, $page_dir)),
				);
			}
		}

		if ($end_page < $total_pages) {
			if ($end_page < ($total_pages - 1)) {
				$paging[] = array(
					"type" => "dots"
					);
			}
			$paging[] = array(
					"type" => "page",
					"page" => $total_pages,
					"link" => self::path_with_page($path, $total_pages, $page_dir),
				);
		}

		if ($current_page < $total_pages) {
			$next = (($current_page == NULL) ? 1 : $current_page) + 1;	//if page not set, it is first page
			$paging[] = array(
					"type" => "next",
					"page" => $next,
					"link" => (($next == 1) ? $path : self::path_with_page($path, $next, $page_dir)),
				);
				}

		return $paging;
	}

	public function filter_array($array, $current_page, $item_per_page) {

		//sanity check
		$current_page = intval($current_page);
		if ($current_page == 0)
			$current_page = 1;

        if ($item_per_page >= count($array))
			return $array;

        $begin = ($current_page - 1) * $item_per_page;

		return array_slice($array, $begin, $item_per_page);
    }

}

//END
