<?php

class payline {

	private static $gatewayURL = 'https://secure.paylinedatagateway.com/api/v2/three-step';
	private static $APIKey = 'dyzKArPUm5UTe6h8Tgyz6668hU2Fg2R3';

	//transaction params
	var $total;
	var $period = 30;			//subscription length in days, normally its 30 days (for ads), but for bussinessowner it could be 360 days 
	var $recurring = false;		//if set to some number (float), this will be recurring price set to, used in step4.php and recurring.php
	var $recurringPeriod = 30;	//recurring period in days, normally it is 30 days
	var $what = NULL;
	var $p2 = NULL;
	var $p3 = NULL;
	var $item_id = NULL;
	var $template = NULL;		// in case to show different template

	//TODO review these
	var $Address, $Zip, $City, $State, $Country, $Email, $FirstName, $LastName, $Description;
	var $account_id;
	var $register_as_user = false, $cc_id, $no_promotion = false, $preset_promotion = false, $promo = NULL, $saved = 0, $promo_code = 0;
	var $promo_notify = 0, $promo_notify_to = NULL, $promo_left = 0, $promo_id = 0, $phone = NULL, $loclimit = NULL, $promo_section, $cl_ad = NULL, $setassponsor = 0;
	var $original_price = 0, $discount = 0, $promo_recurring = 0, $promo_day = 0, $promo_upgrade_only = false, $no_trial = false, $subcat = 0;
	var $promo_row = NULL;

	public function __construct() {
	}

	private function handlepromo(&$error) {
		global $account, $smarty, $db;

		$account_id = $account->isloggedin();

		$this->original_price = $this->total;

		if (empty($_POST["cc_promo"]) || ($this->no_promotion && !$this->preset_promotion)) {
			return;
		}

		$promo = GetPostParam("cc_promo");
		if (empty($promo)) {
			$error = "You did not type any code, if you do not have a promotion code, use the submit button under the payment form.";
			return;
		}

		$rex = $db->q("SELECT * FROM classifieds_promocodes WHERE code = ?", array($promo));
		if( !$db->numrows($rex) ) {
			$error = "Promotion code could not found. If you do not have a promotion code, leave that field emtpy.";
			return;
		}
		$this->promo_row = $rox = $db->r($rex);

		if( $this->promo_upgrade_only && ($rox['setassponsor']||$rox['recurring']) ) {
			$error = "This promo code is not eligible for this purchase.";
			return;
		}

		if( $this->no_trial && $rox['free'] == 1 && ($rox['day'] > 0 && $rox['day'] < 30) ) {
			$error = "This promo code is not eligible for this purchase.";
			return;
		}

		if( $this->subcat && $rox['subcat'] && $this->subcat != $row['subcat'] ) {
			$error = "This promo code is not eligible for this category.";
			return;
		}

		$this->promo_section = $rox['section'];
		$this->setassponsor = $rox['setassponsor'];
		$this->promo_recurring = $rox['recurring'];
		$this->promo_day = $rox['day'];
		if( $rox["limit_per_account"] == 1 ) {
			$ip = account::getUserIp();
			$phone_sql = !empty($this->phone) ? "or phone = '{$this->phone}'" : '';
			$rexx = $db->q("select id from classifieds_promocodes_usage where code_id = '{$rox["id"]}' and (account_id = '$account_id' or ip = inet_aton('$ip') $phone_sql) limit 1");
			if( $db->numrows($rexx) ) {
				$error = "You may not use this promotion code more than once.";
				reportAdmin("You may not use this promotion code more than once.");
				return;
			} 
		}

		if( $rox["left"] < 1 ) {
			$error = "This promotion code is not valid anymore.";
			return;
		} else if( $rox["max"] > 0 && $this->total > $rox["max"] ) {
			$error = "This promotion code is only valid up to \${$rox["max"]} amount of payments.";
			return;
		} else if( !empty($rox['section']) && strcmp($this->what, $rox['section']) ) {
			$error = "This promotion code is not eligible for this section.";
			return;
		} else if( $rox['loclimit'] && $this->loclimit && $rox['loclimit'] < $this->loclimit ) {
			$error = "This promotion code can not be used to post in more than 1 location";
			reportAdmin('This promotion code can not be used to post in more than 1 location');
			return;
		}

		$this->promo_id = $rox["id"];
		$this->promo = $promo;
		$this->promo_code = $rox['id'];
		if( $rox["notify"] == 1 ) {
			$this->promo_notify = 1;
			$this->promo_notify_to = $rox["notify_to"];
			$this->promo_left = $rox["left"];
		}

		if( $rox["discount"] > 0 ) {
			if( !$this->preset_promotion )
				$this->total -= $rox["discount"];
			$this->saved = $rox["discount"];
		} else if( $rox["percentage"] > 0 ) { 
			$this->saved =  $this->total - ($this->total-($this->total*$rox["percentage"]/100)); 
			if( !$this->preset_promotion )
				$this->total -= ($this->total*$rox["percentage"]/100);
			$this->discount = $rox["percentage"];
		} elseif( $rox["addup"] > 0 ) {
			if( !$this->preset_promotion )
				$this->total += $rox["addup"];
			$this->saved = $rox["addup"];
		} elseif ( $rox["fixedprice"] > 0 ) {
			if( !$this->preset_promotion )
				$this->total = $rox["fixedprice"];
		} else {
			$this->saved = $this->total;
			if( !$this->preset_promotion )
				$this->total = 0;
		}

		if (!is_int($this->total))
			$this->total = number_format($this->total, 2, '.', '');

		if( !is_int($this->saved) && $this->saved )
			$this->saved = number_format($this->saved, 2, '.', '');

		if( $this->preset_promotion )
			return true;

		$smarty->assign("cc_promo", $promo);
		$smarty->assign("cc_redeemed", number_format($this->saved, 2));
		if( isset($_POST["redeem"]) )
			$error = "";				
		return true;
	}

	public function CL_stat($what = '', $count = 1) {
		global $account, $db;

		if( $what == '' ) {
			reportAdmin('Cl_Stat() with no param'); return;
		}

		if (!$this->item_id || $this->what != 'classifieds')
			return;

		$res = $db->q("select * from classifieds where id = '$this->item_id'");
		if (!$db->numrows($res))
			return;

		$account_id = $this->account_id ? $this->account_id : $account->isloggedin();
		$rowcl = $db->r($res);
		$rex = $db->q("select loc_id from classifieds_loc where post_id = '$this->item_id'");
		if ($this->discount)
			$discount = $this->discount;
		else
			$discount = $this->total == 0 ? 100 : floor(($this->saved/$this->total)*100);

		while($row=$db->r($rex)) {
			if($what == 'new')
				$query = "INSERT INTO classifieds_log
							(post_id, time, account_id, loc_id, original, made, recurring, repost, promo_code, promo, discount, total_loc, firstad) 
							values
							('{$this->item_id}', now(), '$account_id', '{$row['loc_id']}', '$this->original_price', '$this->total', '0', '{$rowcl['auto_renew']}', '$this->promo_code', '$this->promo', '$discount', '{$rowcl['total_loc']}', if((select count(*) from classifieds where account_id = '$account_id' limit 1),0,1))";

			elseif ($what == 'recurring')
				$query = "INSERT INTO classifieds_log 
							(post_id, time, account_id, loc_id, original, made, recurring, repost, promo_code, promo, discount, total_loc, firstad) 
							values
							('{$this->item_id}', now(), '$account_id', '{$row['loc_id']}', '$this->original_price', '$this->total', '1', '0', '$this->promo_code', '$this->promo', '$discount', '{$rowcl['total_loc']}', if((select count(*) from classifieds where account_id = '$account_id' limit 1),0,1))";

			elseif ($what == 'upgrade')
				$query = "INSERT INTO classifieds_log
							(post_id, time, account_id, loc_id, original, made, recurring, repost, promo_code, promo, discount, total_loc, firstad) 
							values
							('{$this->item_id}', now(), '$account_id', '{$row['loc_id']}', '$this->original_price', '$this->total', '0', '$count', '$this->promo_code', '$this->promo', '$discount', '{$rowcl['total_loc']}', if((select count(*) from classifieds where account_id = '$account_id' limit 1),0,1))";

			$db->q($query);
		}
	}


	/**
	 * Updates all classified tables after successful purchase
	 */
	private function finishup() {
		global $account, $db;

		if (empty($this->promo_row['code']))
			return;

		$account_id = $this->account_id ? $this->account_id : $account->isloggedin();
		$ip = account::getUserIp();
		$today = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));

		$db->q("UPDATE classifieds_promocodes SET `left` = `left` - 1 WHERE code = ?", array($this->promo_row['code']));

		$db->q("INSERT INTO classifieds_promocodes_usage
				(code_id, account_id, ip, date, paid, saved) 
				values 
				(?, ?, inet_aton('$ip'), curdate(), ?, ?)",
				array($this->promo_row['id'], $account_id, $this->total, $this->saved));

		if ($this->promo_row['setassponsor'] && $this->what == 'classifieds' && $this->item_id) {
			$db->q("UPDATE classifieds
					SET sponsor = 1, sponsor_mobile = 1
					WHERE id = ?", 
					array($this->item_id)
					);
		} elseif ($this->promo_row['recurring'] == 1 && $this->what == 'classifieds' && $this->item_id) {
			$db->q("UPDATE classifieds
					SET recurring = 1, recurring_total = ?, auto_renew = 30, auto_renew_fr = 1, time_next = $today+60*60*24+60*auto_renew_time 
					WHERE id = ?",
					array($this->total, $this->item_id)
					);
		}

		if ($this->promo_row['day'] && $this->what == 'classifieds' && $this->item_id) {
			$db->q("UPDATE classifieds SET expires = date_add(now(), interval {$this->promo_row['day']} day) WHERE id = ?", array($this->item_id));
		} 

		if ($this->promo_row['left'] == 0) {
			$db->q("UPDATE classifieds_jeff
					SET used = 1, spent = ?, saved = ?, time = curdate(), item_id = ?
					WHERE code = ?",
					array($this->total, $this->saved, $this->item_id, $this->promo_row['code']));
		}

		if ($this->promo_row['notify']) {
			$this->promo_left = $this->promo_row['left'] - 1;
			$emailm = "Promo code <b>{$this->promo_row['code']}</b> is used to post the following ad ";

			if ($this->promo_row['section'] == 'classifieds') {
				$emailm .= "<a href='http://adultsearch.com/classifieds/look?id={$this->item_id}'>http://adultsearch.com/classifieds/look?id={$this->item_id}</a>.";
			} elseif ($this->promo_row['section'] == 'businessowner') {
				$emailm .= "<a href='{$this->item_link}'>{$this->item_link}</a>.";
			}

			$emailm .= "<br>Total paid: $<b>{$this->total}</b><br>Account ID: <b>{$account_id}</b><br>This promo code can be used <b>{$this->promo_left}</b> times more.";
			if (!empty($this->promo_row['notify_to'])) {
				sendEmail("support@adultsearch.com", "Promotion Used", $emailm, $this->promo_row['notify_to']);
			}
		}
	}

	/**
	 * Used in:
	 * ./_cms_files/adbuild/step4.php
	 * ./_cms_files/classifieds/sponsor.php
	 * ./_cms_files/classifieds/upgrade.php
	 * ./_cms_files/classifieds/renew.php
	 * ./_cms_files/classifieds/recurring.php
	 * ./_cms_files/businessowner/payment.php
	 * ./_cms_files/advertise/addfunds.php
	 */
	public function fullcharge(&$error) {
		global $smarty, $account, $db, $config_dev_server, $config_site_url;

		$account_id = $account->isloggedin();

		if ($this->p2 == "adbuild")
			$this->p3 = $this->item_id;

		$this->handlepromo($error);

		if ($this->total == 0 ) {
			$this->finishup();
			return 1;
		}

		$firstname = GetPostParam("cc_firstname");
		$lastname = GetPostParam("cc_lastname");
		$address = GetPostParam("cc_address");
		$zipcode = GetPostParam("cc_zipcode");
		$city = GetPostParam("cc_city");
		$state = GetPostParam("cc_state");
		$country = GetPostParam("cc_country");

		if (isset($error))
			$error = $error;
		else if (empty($firstname))
			$error = "Please type your first name";
		else if (empty($lastname))
			$error = "Please type your last name";
		else if (empty($address))
			$error = "Please type your address associated with you credit card";
		else if (empty($zipcode))
			$error = "Please type your 'ZIP/Postal Code'";
		else if (empty($city))
			$error = "Please type your city";
		else if (empty($state))
			$error = "Please type your state";
		else if (empty($country))
			$error = "Please select your country";
		//new spammer test
		else if (spammer::isCurrentSpammer())
			$error = "System evaluated subject of this payment as spam. Please contact support@adultsearch.com";
		else {

			$redirect_url = str_replace("http:", "https:", $config_site_url)."/payment/step3";

			$params = array(
				"address" => $address,
				"zip" => $zipcode,
				"firstname" => $firstname,
				"lastname" => $lastname,
				"email" => $_SESSION["email"],
				"city" => $city,
				"state" => $state,
				"country" => $country,
				"total" => $this->total,
				"account_id" => $account_id,
				"redirect_url" => $redirect_url,
				);

			//recurring payment
			if ($this->recurring && $this->recurringPeriod) {
				$params["recurring_amount"] = $this->recurring;
				$params["recurring_period"] = $this->recurringPeriod;
			}

			//TODO we leave this for now (might be used outside of class payment), but going to be removed
			$this->Address = $address;
			$this->Zip = $zipcode;
			$this->Email = $_SESSION["email"];
			$this->FirstName = $firstname;
			$this->LastName = $lastname;
			$this->City = $city;
			$this->State = $state;
			$this->Country = $country;

//			if (in_array($_SESSION["account"], array(3974,74069,156889,156890,158546,184431,184432))) {
//				file_log("payline", "payline::fullcharge(): account_id in between test accont ids, skipping actual charge!");
//				$this->process();
//			} else {

			file_log("payline", "payline::fullcharge(): before charge(): params=".print_r($params, true));

			$ret = $this->getStep2FormUrl($params);

			//if we are here, it means some error happened
			$error = "There has been error processing this transaction. Please contact our support at support@adultsearch.com.";

//			}
		}

		$smarty->assign("country_options", location::getCountryHtmlOptions());
		if (!empty($_POST)) {
			$smarty->assign("cc_firstname", htmlspecialchars($_POST["cc_firstname"]));
			$smarty->assign("cc_lastname", htmlspecialchars($_POST["cc_lastname"]));
			$smarty->assign("cc_address", htmlspecialchars($_POST["cc_address"]));
			$smarty->assign("cc_city", htmlspecialchars($_POST["cc_city"]));
			$smarty->assign("cc_state", htmlspecialchars($_POST["cc_state"]));		
			$smarty->assign("cc_zipcode", htmlspecialchars($_POST["cc_zipcode"]));
			$smarty->assign("cc_country", htmlspecialchars($_POST["cc_country"]));
			$smarty->assign("error", $error);
		}

		$smarty->assign("total", $this->total);
		$smarty->assign("register", $account_id<1 && $this->register_as_user);
		$smarty->assign("no_promotion", $this->no_promotion);

		if ($this->template)
			$smarty->display(_CMS_ABS_PATH.$this->template);
		else 
			$smarty->display(_CMS_ABS_PATH."/templates/payment/step1.tpl");

		return false;
	}

	public function getStep2FormUrl($params) {
		global $db;

		$xmlRequest = new \DOMDocument('1.0','UTF-8');

		$xmlRequest->formatOutput = true;
		$xmlMain = $xmlRequest->createElement('sale');

		// Amount, authentication, and Redirect-URL are typically the bare minimum.
		$this->appendXmlNode($xmlRequest, $xmlMain, 'api-key', self::$APIKey);
		$this->appendXmlNode($xmlRequest, $xmlMain, 'redirect-url', $params["redirect_url"]);
		$this->appendXmlNode($xmlRequest, $xmlMain, 'currency', 'USD');
		$this->appendXmlNode($xmlRequest, $xmlMain, 'amount', number_format($params["total"], 2, ".", ""));
		$this->appendXmlNode($xmlRequest, $xmlMain, 'ip-address', account::getUserIp());

		// Some additonal fields may have been previously decided by user
		//$this->appendXmlNode($xmlRequest, $xmlMain, 'order-id', '1234');
		//$this->appendXmlNode($xmlRequest, $xmlMain, 'order-description', 'Small Order');
		//$this->appendXmlNode($xmlRequest, $xmlMain, 'merchant-defined-field-1' , 'Red');
		//$this->appendXmlNode($xmlRequest, $xmlMain, 'merchant-defined-field-2', 'Medium');
		//$this->appendXmlNode($xmlRequest, $xmlMain, 'tax-amount' , '0.00');
		//$this->appendXmlNode($xmlRequest, $xmlMain, 'shipping-amount' , '0.00');

		// Set the Billing and Shipping from what was collected on initial shopping cart form
		$xmlBillingAddress = $xmlRequest->createElement('billing');
		$this->appendXmlNode($xmlRequest, $xmlBillingAddress,'first-name', $params["firstname"]);
		$this->appendXmlNode($xmlRequest, $xmlBillingAddress,'last-name', $params["lastname"]);
		$this->appendXmlNode($xmlRequest, $xmlBillingAddress,'address1', $params["address"]);
		$this->appendXmlNode($xmlRequest, $xmlBillingAddress,'city', $params["city"]);
		$this->appendXmlNode($xmlRequest, $xmlBillingAddress,'state', $params["state"]);
		$this->appendXmlNode($xmlRequest, $xmlBillingAddress,'country', $params["country"]);
		$this->appendXmlNode($xmlRequest, $xmlBillingAddress,'postal', $params["zip"]);
		//billing-address-email
		$this->appendXmlNode($xmlRequest, $xmlBillingAddress,'email', $params["email"]);
		//$this->appendXmlNode($xmlRequest, $xmlBillingAddress,'phone', $_POST['billing-address-phone']);
		//$this->appendXmlNode($xmlRequest, $xmlBillingAddress,'company', $_POST['billing-address-company']);
		//$this->appendXmlNode($xmlRequest, $xmlBillingAddress,'address2', $_POST['billing-address-address2']);
		//$this->appendXmlNode($xmlRequest, $xmlBillingAddress,'fax', $_POST['billing-address-fax']);
		$xmlMain->appendChild($xmlBillingAddress);

		if (array_key_exists("recurring_amount", $params) && array_key_exists("recurring_period", $params)) {
			$xmlSub = $xmlRequest->createElement('add-subscription');
			$xmlPlan = $xmlRequest->createElement('plan');
			$this->appendXmlNode($xmlRequest, $xmlPlan, 'payments', 0);
			$this->appendXmlNode($xmlRequest, $xmlPlan, 'amount', $params["recurring_amount"]);
			$this->appendXmlNode($xmlRequest, $xmlPlan, 'day-frequency', $params["recurring_period"]);
			$xmlSub->appendChild($xmlPlan);
			$xmlMain->appendChild($xmlSub);
		}

		// Products already chosen by user
		//$xmlProduct = $xmlRequest->createElement('product');
		//$this->appendXmlNode($xmlRequest, $xmlProduct,'product-code' , 'SKU-123456');
		//$this->appendXmlNode($xmlRequest, $xmlProduct,'description' , 'test product description');
		//$this->appendXmlNode($xmlRequest, $xmlProduct,'commodity-code' , 'abc');
		//$this->appendXmlNode($xmlRequest, $xmlProduct,'unit-of-measure' , 'lbs');
		//$this->appendXmlNode($xmlRequest, $xmlProduct,'unit-cost' , '5.00');
		//$this->appendXmlNode($xmlRequest, $xmlProduct,'quantity' , '1');
		//$this->appendXmlNode($xmlRequest, $xmlProduct,'total-amount' , '7.00');
		//$this->appendXmlNode($xmlRequest, $xmlProduct,'tax-amount' , '2.00');

		//$this->appendXmlNode($xmlRequest, $xmlProduct,'tax-rate' , '1.00');
		//$this->appendXmlNode($xmlRequest, $xmlProduct,'discount-amount', '2.00');
		//$this->appendXmlNode($xmlRequest, $xmlProduct,'discount-rate' , '1.00');
		//$this->appendXmlNode($xmlRequest, $xmlProduct,'tax-type' , 'sales');
		//$this->appendXmlNode($xmlRequest, $xmlProduct,'alternate-tax-id' , '12345');

		//$xmlMain->appendChild($xmlProduct);

		$xmlRequest->appendChild($xmlMain);

		// Process Step One: Submit all transaction details to the Payment Gateway except the customer's sensitive payment information.
		// The Payment Gateway will return a variable form-url.
		$data = self::sendXMLviaCurl($xmlRequest, self::$gatewayURL);
		if (!$data)
			return false;
		file_log("payline", "data='{$data}'");

		// Parse Step One's XML response
		$form_url = "";
		$gwResponse = @new \SimpleXMLElement($data);
		if ((string)$gwResponse->result ==1 ) {
			file_log("payline", "result=1");
			// The form url for used in Step Two below
			$form_url = (string)$gwResponse->{'form-url'};
			file_log("payline", "form_url='{$form_url}'");
		} else {
			file_log("payline", "Payline getStep2FormUrl Error: Data cannot be parsed: data='{$data}'");
			debug_log("Payline getStep2FormUrl Error: Data cannot be parsed: data='{$data}'");
			reportAdmin("AS: Payline getStep2FormUrl Error", "Data cannot be parsed: data='{$data}'", array("params" => print_r($params, true), "data" => $data));
			return false;
		}

		$user = $params["user"];

		//create new card in db
		$cre = new creditcard();
		$cre->setAccountId($params["account_id"]);
		$cre->setFirstname($params["firstname"]);
		$cre->setLastname($params["lastname"]);
		$cre->setAddress($params["address"]);
		$cre->setCity($params["city"]);
		$cre->setState($params["state"]);
		$cre->setCountry($params["country"]);
		$cre->setZipcode($params["zip"]);
		$cre->setTotal($params["total"]);
		$ret = $cre->add();
		if (!$ret) {
			//failed to add card
			file_log("payline", "payline::getStep2FormUrl(): Error inserting card into db, account_id='{$params["account_id"]}', fn='{$params["firstname"]}', ln='{$params["lastname"]}', ad='{$params["address"]}', ci='{$params["city"]}', zi='{$params["zip"]}', st='{$params["state"]}', country='{$params["country"]}'");
			debug_log("Error: payline::getStep2FormUrl(): Error inserting card into db, account_id='{$params["account_id"]}', fn='{$params["firstname"]}', ln='{$params["lastname"]}', ad='{$params["address"]}', ci='{$params["city"]}', zi='{$params["zip"]}', st='{$params["state"]}', country='{$params["country"]}'");
			reportAdmin("AS: payline::getStep2FormUrl(): Error - failed to insert card", "",	array(
				"account_id" => $params["account_id"],
				"fn" => $params["firstname"],
				"ln" => $params["lastname"],
				"ad" => $params["address"],
				"ci" => $params["city"],
				"zi" => $params["zip"],
				"st" => $params["state"],
				"co" => $params["country"],
			));
			return false;
		}

		//create new payline_post
		$now = time();
		file_log("payline", "payline::getStep2FormUrl(): going to insert: now={$now}, card_id={$cre->getId()}, p1={$this->what}, p2={$this->p2}, p3={$this->p3}, form_url={$form_url}");
		$res = $db->q("INSERT INTO payline_post
				(stamp, card_id, p1, p2, p3, form_url, post)
				VALUES
				(?, ?, ?, ?, ?, ?, ?)",
				array($now, $cre->getId(), $this->what, $this->p2, $this->p3, $form_url, $xmlRequest->saveXML())
				);
		if ($db->affected($res) != 1) {
			file_log("payline", "payline::getStep2FormUrl(): Error inserting payline post into db, card_id='{$cre->getId()}', wh='{$this->what}', p2='{$this->p2}', p3='{$this->p3}', fu='{$form_url}'");
			debug_log("Error: payline::getStep2FormUrl(): Error inserting payline post into db, card_id='{$cre->getId()}', wh='{$this->what}', p2='{$this->p2}', p3='{$this->p3}', fu='{$form_url}'");
			reportAdmin("AS: payline::getStep2FormUrl(): Error - failed to insert payline post", "",	array(
				"card_id" => $cre->getId(),
				"wh" => $this->what,
				"p2" => $this->p2,
				"p3" => $this->p3,
				"fu" => $form_url,
			));
			return false;
		}
		$payline_post_id = $db->insertid($res);
		$_SESSION["payline_post_id"] = $payline_post_id;

		system::go("/payment/step2");
		die;
	}

	public static function step2() {
		global $account, $smarty, $db;

		$payline_post_id = intval($_SESSION["payline_post_id"]);
		if (!$payline_post_id) {
			file_log("payline", "payline::step2(): Error: payline_post_id not set in session !");
			debug_log("Error: payline::step2(): Error: payline_post_id not set in session !");
			echo "<strong>An Error has occurred processing your order.</strong><br /><br />";
			echo "Please <button type=\"button\" onclick=\"javascript:history.go(-1);\">Go back</button> to restart your order.";
			return false;
		}

		$res = $db->q("SELECT p.*, c.firstname, c.lastname, c.address, c.city, c.state, c.country, c.zipcode 
						FROM payline_post p 
						INNER JOIN account_cc c on c.cc_id = p.card_id
						WHERE p.id = ?", array($payline_post_id));
		if ($db->numrows($res) != 1) {
			file_log("payline", "payline::step2(): Error: cant find payline post for id#{$payline_post_id} !");
			debug_log("Error: payline::step2(): Error: cant find payline post for id#{$payline_post_id} !");
			reportAdmin("AS: payline::step2(): Error - cant find payline post", "cant find payline post for id#{$payline_post_id} !",	array("payline_post_id" => $payline_post_id));
			return false;
		}
		$row = $db->r($res);

		$smarty->assign("name", $row["firstname"]." ".$row["lastname"]);
		$smarty->assign("address", $row["address"]);
		$smarty->assign("city", $row["city"]);
		$smarty->assign("state", $row["state"]);
		$smarty->assign("country", $row["country"]);
		$smarty->assign("zip", $row["zipcode"]);
		$smarty->assign("form_url", $row["form_url"]);
		$smarty->display(_CMS_ABS_PATH."/templates/payment/step2.tpl");
		return;
	}

	public static function step3() {
		global $account, $smarty, $db;

		file_log("payline", "payline::step3(): payline_post_id=".intval($_SESSION["payline_post_id"]).", token_id='{$_REQUEST["token-id"]}'");

		$token_id = $_REQUEST["token-id"];
		if (!$token_id) {
			file_log("payline", "payline::step3(): Error: token_id not set in request !");
			debug_log("Error: payline::step3(): Error: token_id not set in request !");
			reportAdmin("AS: payline::step3(): Error: token_id not set in request", "",	array());
			return false;
		}

		$payline_post_id = intval($_SESSION["payline_post_id"]);
		if (!$payline_post_id) {
			//check if token belongs to successfully finished transaction (this can happen if user refreshes the step 3 page after couple hours of inactivity - which expires the session)
			$res = $db->q("SELECT result FROM payline_post WHERE form_url LIKE ? LIMIT 1", array("%/{$token_id}"));
			if ($db->numrows($res) == 1) {
				$row = $db->r($res);
				if ($row["result"] == 1) {
					//yeah this is successfully finished transaction, redirect to profile
					system::go("/classifieds/myposts");
					die;
				}
			}
			file_log("payline", "payline::step3(): Error: payline_post_id not set in session !");
			debug_log("Error: payline::step3(): Error: payline_post_id not set in session !");
			reportAdmin("AS: payline::step3(): Error: payline_post_id not set in session", "",	array());
			return false;
		}

		$res = $db->q("SELECT p.* FROM payline_post p WHERE p.id = ?", array($payline_post_id));
		if ($db->numrows($res) != 1) {
			file_log("payline", "payline::step3(): Error: cant find payline post for id#{$payline_post_id} !");
			debug_log("Error: payline::step3(): Error: cant find payline post for id#{$payline_post_id} !");
			reportAdmin("AS: payline::step3(): Error - cant find payline post", "cant find payline post for id#{$payline_post_id} !",	array("payline_post_id" => $payline_post_id));
			return false;
		}
		$row = $db->r($res);
		$cc_id = $row["card_id"];
		$what = $row["p1"];
		$item_id = NULL;
		if (is_numeric($row["p2"]))
			$item_id = intval($row["p2"]);
		else if (is_numeric($row["p3"]))
			$item_id = intval($row["p3"]);
		$transaction_id = NULL;

		$data = self::getResultForToken($token_id);
		$gwResponse = @new \SimpleXMLElement((string)$data);

		$result = intval((string)$gwResponse->result);
		$result_text = (string)$gwResponse->{'result-text'};

		//save result of payline post in DB
		$params = array($result, $result_text);
		$transaction_sql = "";
		if ($result == 1) {
			$transaction_sql = ", transaction_id = ?";
			$action_type = (string)$gwResponse->{'action-type'};
			if ($action_type == "sale") {
				$transaction_id = (string)$gwResponse->{'transaction-id'};
			} else if ($action_type == "add_subscription") {
				$transaction_id = (string)$gwResponse->{'subscription-id'};
			} else {
				file_log("payline", "Step 3: Unknown action-type in response: '{$action_type}' !");
				reportAdmin("AS: payline::step3(): Error: unknown action-type", "", array("token-id" => $token_id, "action-type" => $action_type, "payline_post_id" => $payline_post_id));
				return false;
			}
			if (empty($transaction_id)) {
				file_log("payline", "Step 3: Empty transaction-id/subscription-id !");
				reportAdmin("AS: payline::step3(): Error: Empty transaction-id/subscription-id", "", array("token-id" => $token_id, "action-type" => $action_type, "payline_post_id" => $payline_post_id, "transaction_id" => $transaction_id));
				return false;
			}
			$params[] = $transaction_id;
		}
		$params[] = $payline_post_id;
		$db->q("UPDATE payline_post SET result = ?, response = ? {$transaction_sql} WHERE id = ?", $params);

		//save purchase
		$now = time();
		$purchase_id = "";
		if ($result == 1) {
			//success, store purchase in db
			if ($action_type == "sale") {
				$total = (float)$gwResponse->{'amount'};
			} else if ($action_type == "add_subscription") {
				$total = (float)$gwResponse->{'plan'}->{'amount'};
			}
			file_log("payline", "Step 3 SUCCESS: transaction_id={$transaction_id}, total={$total}"); 
			$res = $db->q("INSERT INTO account_purchase
							(account_id, cc_id, `time`, transaction_time, payline_post_id, transaction_id, `what`, total, item_id)
							VALUES
							(?, ?, ?, ?, ?, ?, ?, ?, ?)",
							array($account->getId(), $cc_id, date("Y-m-d H:i:s", $now), $now, $payline_post_id, $transaction_id, $what, $total, $item_id)
							);
			$purchase_id = $db->insertid($res);

			//update card details
			$cre = creditcard::findOneById($cc_id);
			if ($cre) {
				$cre->setCc((string)$gwResponse->{'billing'}->{'cc-number'});
				$ccexp = preg_replace('/[^0-9]/', '', (string)$gwResponse->{'billing'}->{'cc-exp'});
				if (strlen($ccexp) == 4) {
					$cre->setExpmonth(substr($ccexp, 0 , 2));
					$cre->setExpyear(substr($ccexp, 2 , 2));
				}
				$cre->update();
			} else {
				reportAdmin("AS: payline::step3: Error: cant find credit card", "", array("payline_post_id" => $payline_post_id));
			}

			//this should redirect
			self::purchaseSuccessful($what, $purchase_id);

		} else {
			//failure, store attempt in account_cc_fail

			file_log("payline", "Step 3 FAILED, result={$result}"); 

			if ($result == 2) {
				//result == 2- transaction was denied
				$db->q("INSERT INTO account_cc_fail 
						(account_id, `time`, payline_post_id)
						VALUES
						(?, ?, ?)",
						array($account->getId(), date("Y-m-d H:i:s", $now), $payline_post_id)
						);
			} else {
				//if result == 3, then that is some kind of transaction error, that should not happen, notify admin
				$result_code = (string)$gwResponse->{'result-code'};
				file_log("payline", "Step 3 Transaction error, payline_post_id={$payline_post_id}, result-code={$result_code}, result-text={$result_text}");
				if (strpos($result_text, "Invalid Credit Card Number") === 0) {
					//do not notify admin, this is "normal" error - person didnt put in correct/valid credit card number
				} else if (strpos($result_text, "Transaction previously completed") === 0) {
					//do not notify the admin - this happens quite often - person refreshes page while transaction already succeeded in past
					//check if token belongs to successfully finished transaction (this can happen if user refreshes the step 3 page after couple hours of inactivity - which expires the session)
					$res = $db->q("SELECT result FROM payline_post WHERE form_url LIKE ? LIMIT 1", array("%/{$token_id}"));
					if ($db->numrows($res) == 1) {
						$row = $db->r($res);
						if ($row["result"] == 1) {
							//yeah this is successfully finished transaction, redirect to profile
							system::go("/classifieds/myposts");
							die;
						}
					}
				} else {
					//some error - notify admin
					reportAdmin("AS: payline::step3: Transaction error", "result-code: {$result_code}<br />\nresult-text: {$result_text}", 
						array("payline_post_id" => $payline_post_id, "response" => (string)$data)
						);
				}
			}

			//this should redirect
			self::purchaseFailed($what, $payline_post_id);
		}

		reportAdmin("AS: payline::step3(): Error: not processed result of step3", "",  array("payline_post_id" => $payline_post_id));
		system::go("/");
		die;
	}

	public static function getResultForToken($tokenId) {

		$xmlRequest = new \DOMDocument('1.0','UTF-8');
		$xmlRequest->formatOutput = true;
		$xmlCompleteTransaction = $xmlRequest->createElement('complete-action');
		self::appendXmlNode($xmlRequest, $xmlCompleteTransaction,'api-key', self::$APIKey);
		self::appendXmlNode($xmlRequest, $xmlCompleteTransaction,'token-id', $tokenId);
		$xmlRequest->appendChild($xmlCompleteTransaction);

		// Process Step Three
		$data = self::sendXMLviaCurl($xmlRequest, self::$gatewayURL);
		file_log("payline", "Step 3 response data = '".(string)$data."'");

		return $data;

	}

	// helper function demonstrating how to send the xml with curl
	private static function sendXMLviaCurl($xmlRequest, $gatewayURL) {

		$ch = curl_init(); // Initialize curl handle
		curl_setopt($ch, CURLOPT_URL, $gatewayURL); // Set POST URL

		$headers = array();
		$headers[] = "Content-type: text/xml";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); // Add http headers to let it know we're sending XML
		$xmlString = $xmlRequest->saveXML();
		curl_setopt($ch, CURLOPT_FAILONERROR, 1); // Fail on errors
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // Allow redirects
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Return into a variable
		curl_setopt($ch, CURLOPT_PORT, 443); // Set the port number
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // Times out after 30s
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlString); // Add XML directly in POST

		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

		// This should be unset in production use. With it on, it forces the ssl cert to be valid
		// before sending info.
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

		if (!($data = curl_exec($ch))) {
			$error = curl_error($ch);
			file_log("payline", "Payline sendXMLviaCurl Error: '{$error}'");
			debug_log("Payline sendXMLviaCurl Error: '{$error}'");
			reportAdmin("AS: Payline sendXMLviaCurl Error", "curl error: '{$error}'", array("xmlRequest" => $xmlRequest->saveXML(), "error" => $error));
			return false;
		}
		curl_close($ch);

		return $data;
	}

	// Helper function to make building xml dom easier
	private static function appendXmlNode($domDocument, $parentNode, $name, $value) {
		$childNode = $domDocument->createElement($name);
		$childNodeValue = $domDocument->createTextNode($value);
		$childNode->appendChild($childNodeValue);
		$parentNode->appendChild($childNode);
	}

	public static function purchaseSuccessful($what, $purchase_id) {
		global $db;

		file_log("payline", "payline::purchaseSuccessful(): what={$what}, purchase_id={$purchase_id}");

		if ($what == "classifieds") {
			$classifieds = new classifieds;
			$classifieds->purchaseSuccessful($purchase_id);
		} else if ($what == "businessowner") {
			$bo = new businessowner;
			$bo->purchaseSuccessful($purchase_id);
		} else if ($what == "advertise") {
			$adv = new advertise;
			$adv->purchaseSuccessful($purchase_id);
		} else {
			debug_log("Error: payline::purchaseSuccessful() Unknown what: what='{$what}'");
			reportAdmin("AS: Error in payline::purchaseSuccessful() Unknown what", "what='{$what}'", array("what" => $what, "purchase_id" => $purchase_id));
		}
		return false;
	}

	public static function purchaseFailed($what, $payline_post_id) {
		global $db;

		file_log("payline", "payline::purchaseFailed(): what={$what}, payline_post_id={$payline_post_id}");

		if ($what == "classifieds") {
			$classifieds = new classifieds;
			$classifieds->purchaseFailed($payline_post_id);
		} else if ($what == "businessowner") {
			$bo = new businessowner;
			$bo->purchaseFailed($payline_post_id);
		} else if ($what == "advertise") {
			$adv = new advertise;
			$adv->purchaseFailed($payline_post_id);
		} else {
			debug_log("Error: payline::purchaseFailed() Unknown what: what='{$what}'");
			reportAdmin("AS: Error in payline::purchaseFailed() Unknown what", "what='{$what}'", array("what" => $what, "payline_post_id" => $payline_post_id));
		}
		return false;
	}

	public function updatecc($cc_id) {
		return false;
	}

	public function auth($params, &$error = NULL) {
		return false;
	}

	public function refundByPurchaseId($purchase_id) {
		global $db, $account;

		$res = $db->q("SELECT ap.*
						FROM account_purchase ap
						WHERE ap.id = ?
						LIMIT 1",
						array($purchase_id));
		if ($db->numrows($res) != 1) {
			return false;
		}

		$row = $db->r($res);
		$transaction_id = $row["transaction_id"];
		$payline_post_id = $row["payline_post_id"];
		$total = $row["total"];
		$email = $row["email"];
		$account_id = $row["account_id"];
		$cc_id = $row["cc_id"];
		$item_id = $row["item_id"];
		if (!$transaction_id) {
			//this is probably historic purchase, where we dont have transaction id
			//fail now
			return false;
		}
		if ($total < 0.01) {
			//either this was zero purchase (should not happen ?) - and there is nothing to refund
			//or this was already refunded
			//do not refund
			return false;
		}

		$params = array(
			"transaction_id" => $transaction_id,
			"amount" => $total,
		);
		$ret = $this->do_refund($params);
		if (!$ret)
			return false;

		//update account_purchase table
		$db->q("UPDATE account_purchase SET total = 0 WHERE id = ? LIMIT 1", array($purchase_id));
		if ($db->affected() != 1) {
			//this should not happen
			reportAdmin("AS: payline::refund : cant update original purchase", "",
				array("purchase_id" => $purchase_id));
		}

		//insert refund into account_refund table
		$now = time();
		$now_datetime = date("Y-m-d H:i:s", $now);
		$db->q("INSERT INTO account_refund 
				(transaction_time, author_id, payline_post_id, account_id, `time`, cc_id, purchase_id, item_id, amount)
				VALUES
				(?, ?, ?, ?, ?, ?, ?, ?, ?)",
				array($now, $account->getId(), $payline_post_id, $account_id, $now_datetime, $cc_id, $purchase_id, $item_id, $total));
		$id = $db->insertid();
		if (!$id) {
			//this should not happen
			reportAdmin("AS: payline::refund : cant insert into account_refund", "",
				array("purchase_id" => $purchase_id));
		}

		return true;
	}

	public function do_refund($params) {
		global $db;

		file_log("payline", "do_refund: transaction_id={$params["transaction_id"]}, amount={$params["amount"]}");

		if (!array_key_exists("transaction_id", $params) || !array_key_exists("amount", $params))
			return false;

		$xmlRequest = new \DOMDocument('1.0','UTF-8');

		$xmlRequest->formatOutput = true;
		$xmlMain = $xmlRequest->createElement('refund');
		$this->appendXmlNode($xmlRequest, $xmlMain, 'api-key', self::$APIKey);
		$this->appendXmlNode($xmlRequest, $xmlMain, 'transaction-id', $params["transaction_id"]);
		$this->appendXmlNode($xmlRequest, $xmlMain, 'amount', $params["amount"]);
		$xmlRequest->appendChild($xmlMain);

		$data = self::sendXMLviaCurl($xmlRequest, self::$gatewayURL);
		file_log("payline", "do_refund: data='{$data}'");
		if (!$data) {
			reportAdmin("AS: payline::do_refund(): Gateway returned empty response", "", array("transaction_id" => $transaction_id, "amount" => $amount, "data" => $data));
			return false;
		}

		//parse response to see if refund was successful
		$gwResponse = @new \SimpleXMLElement((string)$data);
        $result = intval((string)$gwResponse->result);
        $result_text = (string)$gwResponse->{'result-text'};
		if ($result != 1) {
			reportAdmin("AS: payline::do_refund(): Refund failed", "Result = {$result}<br />Result text: '{$result_text}'", 
				array("transaction_id" => $transaction_id, "amount" => $amount, "result" => $result, "result_text" => $result_text, "data" => $data)
				);
			return false;
		}
		
		return true;
	}

	public function refund($total, $cre) {
		return false;
	}
}

//END
