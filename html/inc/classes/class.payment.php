<?php


class payment {

	//transaction params
	var $total;
	var $recurring = false;		//if set to some number (float), this will be recurring price set to, used in step4.php and recurring.php
	var $what = NULL;

	var $no_promotion = false, $preset_promotion = false, $promo = NULL, $saved = 0, $promo_code = 0;
	var $promo_id = 0, $phone = NULL, $loclimit = NULL;
	var $promo_upgrade_only = false;
	var $promo_row = NULL;

	const SUBSCRIPTION_ACTIVE = 1;
	const SUBSCRIPTION_CANCELED = 2;

	public function __construct() {
	}

	private function handlepromo(&$error) {
		global $account, $smarty, $db;

		//TODO fix this function so it correctly takes all amount and payment details from payment & payment_item
		return;

		$account_id = $account->isloggedin();

		if (empty($_POST["cc_promo"]) || ($this->no_promotion && !$this->preset_promotion)) {
			return;
		}

		$promo = GetPostParam("cc_promo");
		if (empty($promo)) {
			$error = "You did not type any code, if you do not have a promotion code, use the submit button under the payment form.";
			return;
		}

		//10.1.2017 dallas special request-  promo backpage should work if they put space inside or mess up uppercase/lowercase...
		if (str_replace(' ', '', strtolower(trim($promo))) == "backpage")
			$promo = "backpage";

		$rex = $db->q("SELECT * FROM classifieds_promocodes WHERE code = ? AND deleted IS NULL", array($promo));
		if( !$db->numrows($rex) ) {
			$error = "Promotion code could not found. If you do not have a promotion code, leave that field empty.";
			return;
		}
		$this->promo_row = $rox = $db->r($rex);

		if( $this->promo_upgrade_only && ($rox['setassponsor']||$rox['recurring']||$rox['citythumbnail']||$rox['sidesponsor']||$rox['reposts']) ) {
			$error = "This promo code is not eligible for this purchase.";
			return;
		}

		if( $rox["limit_per_account"] == 1 ) {
			$ip = account::getUserIp();
			$params = [$rox["id"], $account_id, $ip];
			$phone_sql = "";
			if (!empty($this->phone)) {
				$phone_sql = " OR phone = ? ";
				$params[] = $this->phone;
			}
			$rexx = $db->q("SELECT id FROM classifieds_promocodes_usage WHERE code_id = ? AND ( account_id = ? OR ip_address = ? {$phone_sql} ) LIMIT 1",
					$params
					);
			if ($db->numrows($rexx)) {
				$error = "You may not use this promotion code more than once.";
				reportAdmin("You may not use this promotion code more than once.");
				return;
			} 
		}

		if( $rox["left"] < 1 ) {
			$error = "This promotion code is not valid anymore.";
			return;
		} else if( $rox["max"] > 0 && $this->total > $rox["max"] ) {
			$error = "This promotion code is only valid up to \${$rox["max"]} amount of payments.";
			return;
		} else if( !empty($rox['section']) && strcmp($this->what, $rox['section']) ) {
			$error = "This promotion code is not eligible for this section.";
			return;
		} else if( $rox['loclimit'] && $this->loclimit && $rox['loclimit'] < $this->loclimit ) {
			$error = "This promotion code can not be used to post in more than 1 location";
			reportAdmin('This promotion code can not be used to post in more than 1 location');
			return;
		}

		$this->promo_id = $rox["id"];
		$this->promo = $promo;
		$this->promo_code = $rox['id'];
		if( $rox["notify"] == 1 ) {
			//TODO
		}

		if( $rox["discount"] > 0 ) {
			if( !$this->preset_promotion )
				$this->total -= $rox["discount"];
			if ($this->total < 0)
				$this->total = 0;
			$this->saved = $rox["discount"];
		} else if( $rox["percentage"] > 0 ) { 
			$this->saved =  $this->total - ($this->total-($this->total*$rox["percentage"]/100)); 
			if( !$this->preset_promotion )
				$this->total -= ($this->total*$rox["percentage"]/100);
		} elseif( $rox["addup"] > 0 ) {
			if( !$this->preset_promotion )
				$this->total += $rox["addup"];
			$this->saved = $rox["addup"];
		} elseif ( $rox["fixedprice"] > 0 ) {
			if( !$this->preset_promotion )
				$this->total = $rox["fixedprice"];
		} else {
			$this->saved = $this->total;
			if( !$this->preset_promotion )
				$this->total = 0;
		}

		if (!is_int($this->total))
			$this->total = number_format($this->total, 2, '.', '');

		if( !is_int($this->saved) && $this->saved )
			$this->saved = number_format($this->saved, 2, '.', '');

		if( $this->preset_promotion )
			return true;

		$smarty->assign("cc_promo", $promo);
		$smarty->assign("cc_redeemed", number_format($this->saved, 2));
		if( isset($_POST["redeem"]) )
			$error = "";				
		return true;
	}

	/**
	 * This function determines which payment processor are we going to use:
	 * For verified customers & gift cards, use securionpay
	 * Otherwise, use stripe
	 */
	public function get_processor($payment_id) {
		global $db, $account;

		if ($account->isadmin() && $_REQUEST["processor"]) {
			switch ($_REQUEST["processor"]) {
				case "stripe": return new stripe(); break;
				case "securionpay": return new securionpay(); break;
				case "budget": return new budget(); break;
				case "nopayment": return new nopayment(); break;
			}
		}

		$res = $db->q("
			SELECT a.agency, a.whitelisted, a.account_id
			FROM account a 
			INNER JOIN payment p on p.account_id = a.account_id
			WHERE p.id = ?
			",
			[$payment_id]
			);
		if ($db->numrows($res) == 1) {
			$row = $db->r($res);
			$agency = $row["agency"];
			$whitelisted = $row["whitelisted"];
			$account_id = $row["account_id"];

			if ($agency)
				return new budget();

			//2019-09-15 everybody pays with securionpay
			//if ($whitelisted)
			//	return new securionpay();
			return new securionpay();

			//for new accounts verification!
			//if ($account_id > 459230)
			//	return new nopayment();
		}

		return new nopayment();
	}

	/**
	 * This function validates the payment. It checks payment items and test whether all of them are still valid
	 */
	public static function validate($payment_id, &$error = "") {
		global $db, $account;

		$res = $db->q("
			SELECT pi.*
			FROM payment_item pi
			WHERE pi.payment_id = ?", 
			[$payment_id]
			);
		if (!$db->numrows($res)) {
			$error = "No payment items in database";
			return false;
		}
		while ($row = $db->r($res)) {
			$type = $row["type"];
			$clad_id = $row["classified_id"];
			$loc_id = $row["loc_id"];
			$sponsor = $row["sponsor"];
			$side = $row["side"];
			$sticky = $row["sticky"];

			if ($type != "classified")
				continue;
			
			$clad = clad::findOneById($clad_id);
			if (!$clad) {
				$error = "Can't find clad #{$clad_id}";
				return false;
			}
			if ($sponsor && !classifieds::locationHasFreeSlotCityThumbnail($loc_id) && $account->isloggedin() && !$account->isadmin()) {
				$error = "No free slot for city thumbnail for clad #{$clad_id} in loc #{$loc_id}";
				return false;
			}
			if ($side && !classifieds::locationHasFreeSlotSideSponsor($loc_id, $clad->getType()) && $account->isloggedin() && !$account->isadmin()) {
				$error = "No free slot for side sponsor for clad #{$clad_id} in loc #{$loc_id}";
				return false;
			}
			if ($sticky && !classifieds::locationHasFreeSlotSticky($loc_id, $clad->getType()) && $account->isloggedin() && !$account->isadmin()) {
				$error = "No free slot for sticky for clad #{$clad_id} in loc #{$loc_id}";
				return false;
			}
		}

		return true;
	}

	/**
	 * Used in:
	 * ./_cms_files/payment/pay.php
	 */
	public function pay($payment_id, &$error = "") {
		global $smarty, $account, $db, $config_dev_server;

		$now = time();

		file_log("payment", "pay(): payment_id={$payment_id}");

		$res = $db->q("
			SELECT p.*, a.email as account_email, a.whitelisted
			FROM payment p
			INNER JOIN account a on a.account_id = p.account_id
			WHERE p.id = ? 
			LIMIT 1", 
			[$payment_id]
			);
		if ($db->numrows($res) != 1) {
			$error = "Invalid payment!";
			return false;
		}
		$row = $db->r($res);

		$result = $row["result"];
		$amount = $row["amount"];
		$account_id = $row["account_id"];
		$whitelisted = $row["whitelisted"];
		$responded_stamp = $row["responded_stamp"];

		$email = $row["email"];
		if (!$email)
			$email = $row["account_email"];
		if (!$email) {
			$error = "Invalid account, please contact support@adultsearch.com !";
			reportAdmin("AS: payment:pay error", "Account doesnt have email specified ?? payment_id={$payment_id}, account_id={$account_id}");
			return false;
		}

		//if user account is not whitelisted, we only authorise
		$captured = true;
		if (!$whitelisted)
			$captured = false;

		if (!self::validate($payment_id, $error)) {
			file_log("payment", "pay: payment #{$payment_id} not valid: '{$error}'");
			system::go("/account/mypage", "This payment is not valid anymore, please create a new payment");
			return false;
		}

		$this->handlepromo($error);

		if ($result == "A") {
			//TODO
			return true;
		} 
		if ($amount == 0) {
			//TODO
			//finish up (mark result as 'A')
			return true;
		}

		//throttling
		//do not allow to run another try of declined payment if less than 5 seconds from last try passed
		if ($result == "D" && ($now < ($responded_stamp + 5))) {
			$error = "Please thoroughly check if all submitted data is correct";
		}

		$amount = number_format($amount, 2, ".", "");
		$amount_minor = round($amount * 100); //Charge amount in minor units of given currency (for USD in cents)

		$smarty->assign("amount", $amount);
		$smarty->assign("amount_minor", $amount_minor);
		$smarty->assign("total", $amount);	//backwards compatibility

		//TODO move to export_to_template function so we dont waste db queries 
		$smarty->assign("country_options", location::getCountryHtmlOptions());
		$smarty->assign("country_code_options", location::getCountryCodeHtmlOptions());

		$processor = $this->get_processor($payment_id);


		//---------------------------------------
		//---------------------------------------
		if ($processor->get_name() == "budget") {
			//if the processor is budget, then we skip all shenanigans with checking if address is filled, and just call process method of budget processor
			$budget = $processor->get_budget($account_id);
			$smarty->assign("budget", number_format($budget, 2, ".", ","));
			if ($budget > $amount)
				$smarty->assign("budget_available", true);
			if (!isset($_REQUEST["submitted"]) || $budget < $amount) {
				//this is not submission, display the template
				file_log("payment", "pay(): not submitted, displaying payment form");
				return $smarty->display($processor->get_template_path());
			}
			$params = [
				"payment_id" => $payment_id,
				"account_id" => $account_id,
				"amount" => $amount,
				];
			$payment_succeeded = $processor->charge($params, $error);
			if ($payment_succeeded) {
				$this->paymentSuccessful($payment_id);
				$this->afterPayment($payment_id);
				die;
			}
			$smarty->assign("error", $error);
			return $smarty->display($processor->get_template_path());
		}
		//---------------------------------------


		if (!isset($_REQUEST["submitted"])) {
			//this is first display of the form (and not submission)
			//display the template
			file_log("payment", "pay(): not submitted, displaying payment form");
			$processor->export_to_template();
			return $smarty->display($processor->get_template_path());
		}

		$cre = null;
		//$saved_card_id = intval($_REQUEST["saved_card_id"]) ?: $row["cc_id"];
		$saved_card_id = intval($_REQUEST["saved_card_id"]);
		if ($saved_card_id) {
			$cre = creditcard::findOneById($saved_card_id);
			if (!$cre || $cre->getDeletedStamp()) {
				$error = "Wrong credit card";
			}
		}

		$image_front_data = $image_back_data = null;

		if(! empty($_REQUEST['tokenObj'])){
			$tokenArr = json_decode(urldecode($_REQUEST['tokenObj']), true);
			$nameSplit = explode(' ', $tokenArr['card']['name']);
		}


		if ($cre) {
			file_log("payment", "pay(): Using saved cc from db: cc_id={$cre->getId()}");
			$firstname = $cre->getFirstname();
			$lastname = $cre->getLastname();
			$address = $cre->getAddress();
			$zipcode = $cre->getZipcode();
			$city = $cre->getCity();
			$state = $cre->getState();
			$country = $cre->GetCountry();
			$cc_month = $cre->getExpMonth();
			$cc_year = $cre->getExpYear();
			if ($processor->get_name() == "securionpay") {
				$token = $cre->getSecurionpayToken();
			} else if ($processor->get_name() == "stripe") {
				$token = $cre->getStripeToken();
			}
		} else {
			$lastname = GetPostParam("cc_lastname") ? GetPostParam("cc_lastname") : array_pop($nameSplit);
			$firstname = GetPostParam("cc_firstname") ? GetPostParam("cc_firstname") : implode(' ', $nameSplit);
			$address = GetPostParam("cc_address") ? GetPostParam("cc_address") : $tokenArr['card']['address_line1'] . "\n" . $tokenArr['card']['address_line2'];
			$zipcode = GetPostParam("cc_zipcode") ? GetPostParam("cc_zipcode") : $tokenArr['card']['address_zip'];
			$city = GetPostParam("cc_city") ? GetPostParam("cc_city") : $tokenArr['card']['address_city'];
			$state = GetPostParam("cc_state") ? GetPostParam("cc_state") : $tokenArr['card']['address_state'];
			$country = GetPostParam("cc_country") ? GetPostParam("cc_country") : $tokenArr['card']['address_country'];
			$cc_month = ! empty($_REQUEST["cc_month"]) ? preg_replace('/[^0-9]/', '', $_REQUEST["cc_month"]) : $tokenArr['card']['exp_month'];
			$cc_year  = ! empty($_REQUEST["cc_year"]) ? preg_replace('/[^0-9]/', '', $_REQUEST["cc_year"]) : $tokenArr['card']['exp_year'];
			$image_front_data = $_REQUEST["image_front_data"];
			$image_back_data = $_REQUEST["image_back_data"];
		}


		if (!$token)
			$token = GetPostParam("token") ? GetPostParam("token") : GetPostParam("stripeToken");

		$cc_cc	= ! empty($_REQUEST["cc_cc"]) ? preg_replace('/[^0-9]/', '', $_REQUEST["cc_cc"]) : 'xxxxxxxxxxxx' . $tokenArr['card']['last4'];
		$cc_cvc2  = ! empty($_REQUEST["cc_cvc2"]) ? preg_replace('/[^0-9]/', '', $_REQUEST["cc_cvc2"]) : ' '; // todo - check how to get it from stripe before the charge


		if (strlen($cc_year) == 2)
			$cc_year = "20{$cc_year}";

		if ($error)
			$error = $error;
		else if (empty($firstname))
			$error = "Please type your first name";
		else if (empty($lastname))
			$error = "Please type your last name";
		else if (empty($address))
			$error = "Please type your address associated with you credit card";
		else if (empty($zipcode))
			$error = "Please type your 'ZIP/Postal Code'";
		else if (empty($city))
			$error = "Please type your city";
		else if (empty($country))
			$error = "Please select your country";
		else if (empty($cc_cc))
			$error = "Please type your credit card number";
		else if (strlen($cc_cc) < 12)
			$error = "Please type correct credit card number";
		else if (empty($cc_month))
			$error = "Please select credit card expiration month";
		else if (empty($cc_year))
			$error = "Please select credit card expiration year";
		else if ($cc_year < date("Y") || ($cc_year == date("Y") && $cc_month < date("m")))
			$error = "You entered wrong expiration month/year or your credit card is already expired";
		else if (empty($cc_cvc2))
			$error = "Please type your credit card verification code";
		else if (empty($token))
			$error = "Invalid card token";
//		else if (!$cre && (!$image_front_data || !$image_back_data))
//			$error = "Please upload photo of front and back face of credit card";
		else if (!$account->isAdmin() && empty($_REQUEST["captcha_str"]) && empty($_REQUEST['stripeToken']))
			$error = "You need to type the security code";
		else if (!$account->isAdmin() && strcasecmp($_REQUEST["captcha_str"], $_SESSION["captcha_str"]) && empty($_REQUEST['stripeToken']))
			$error = "Security code is wrong, please type it again";
		else if (creditcard::usedByOtherAccount($cc_cc, $cc_cvc2, $cc_month, $cc_year, $account_id)) {
			$error = "This credit card is used by other account. Don't create multiple accounts on our website. If you want to post more ads, please contact support@adultsearch.com";
		} else if (creditcard::isBannedAux($cc_cc, $cc_cvc2, $zipcode)) {
			//card is (probably) banned
			//notify support
			creditcard::sendBannedCardNotification($account_id, $cc_cc, $cc_month, $cc_year, $firstname, $lastname, $zipcode);
			$error = "This credit card may not be used on our website. (abuse)";
		} else if ($account->isBanned()) {
			//user is banned and trying to pay with a new card, report him
			//notify admin
			reportAdmin("AS: Banned user trying to pay", "Check if this is really spammer, if yes, add this card to banned cards", [
				"payment_id" => $payment_id,
				"cc" => creditcard::ccnoFirstLast($cc_cc),
				"cvc" => creditcard::obfuscateCvc2($cc_cvc2),
				"zipcode" => $zipcode,
				"firstname" => $firstname,
				"lastname" => $lastname,
				"total" => $amount,
				"cc_month" => $cc_month,
				"cc_year" => $cc_year,
				]);
			$error = "This credit card may not be used on our website. (abuse)";
		} else {

			//insert or update credit card
			if (!$cre) {
				$cre = new creditcard();
				$cre->setAccountId($account_id);
				$cre->setCc($cc_cc);
				$cre->setExpMonth($cc_month);
				$cre->setExpYear($cc_year);
				$cre->setCvc2($cc_cvc2);
				$cre->setFirstName($firstname);
				$cre->setLastName($lastname);
				$cre->setAddress($address);
				$cre->setZipcode($zipcode);
				$cre->setCity($city);


				if(empty($state))
					$state = ''; // col can't be null
				$cre->setState($state);

				$cre->setCountry($country);
				if ($processor->get_name() == "securionpay") {
					$token = $cre->setSecurionpayToken($token);
				} else if ($processor->get_name() == "stripe") {
					$token = $cre->setStripeToken($token);
				}

				$cre->setApprovedStamp($now); // card is already approved by this time

				$ret = $cre->add();
				if (!$ret) {
					reportAdmin("AS: payment pay() error", "Cannot insert/update cc in db.", [
						"payment_id" => $payment_id,
						"cc" => creditcard::ccnoFirstLast($cc_cc),
						"cvc" => creditcard::obfuscateCvc2($cc_cvc2),
						"cc_month" => $cc_month,
						"cc_year" => $cc_year,
						"firstname" => $firstname,
						"lastname" => $lastname,
						"address" => $address,
						"city" => $city,
						"state" => $state,
						"zipcode" => $zipcode,
						"country" => $country,
						]);
					$error = "Error in payment, please contact support.";
				} else {
					file_log("payment", "pay(): ok, CC inserted, cc_id={$cre->getId()}, securionpay_token={$cre->getSecurionpayToken()}, stripe_token={$cre->getStripeToken()}");
					$cc_id = $cre->getId();
					//$ret = $cre->store_card_images($image_front_data, $image_back_data);
					//if (!$ret) {
					//	file_log("payment", "pay(): failed to store card images");
					//	reportAdmin("Payment failed - cannot set CC image front & back", "cc_id={$cc_id}");
					//	$error = "Error in payment, please contact support.";
					//	return 0;	
					//}
				}
			} else {
				$cc_id = $cre->getId();

				//update token if not set
				if ($processor->get_name() == "securionpay") {
					if ($token != $cre->getSecurionpayToken()) {
						$cre->setSecurionpayToken($token);
						$cre->update();
					}
				} else if ($processor->get_name() == "stripe") {
					if ($token != $cre->getStripeToken()) {
						$cre->setStripeToken($token);
						$cre->update();
					}
				}
			}

			$res = $db->q("UPDATE payment SET cc_id = ? WHERE id = ? LIMIT 1", [$cc_id, $payment_id]);

			$params = [
				"payment_id" => $payment_id,
				"account_id" => $account_id,
				"card" => $cre,
				"email" => $email,
				"amount" => $amount,
				"recurring_amount" => $recurring_amount,
				];
			$payment_succeeded = $processor->charge($params, $cc_cc, $cc_cvc2, $captured, $error);
		
			if ($payment_succeeded) {
				$this->paymentSuccessful($payment_id);

				$this->afterPayment($payment_id);
				die;
			}

		}

		if (!empty($_POST)) {
			$smarty->assign("cc_firstname", htmlspecialchars($_POST["cc_firstname"]));
			$smarty->assign("cc_lastname", htmlspecialchars($_POST["cc_lastname"]));
			$smarty->assign("cc_address", htmlspecialchars($_POST["cc_address"]));
			$smarty->assign("cc_city", htmlspecialchars($_POST["cc_city"]));
			$smarty->assign("cc_state", htmlspecialchars($_POST["cc_state"]));		
			$smarty->assign("cc_zipcode", htmlspecialchars($_POST["cc_zipcode"]));
			$smarty->assign("cc_country", htmlspecialchars($_POST["cc_country"]));
			$smarty->assign("cc_cc", htmlspecialchars($_POST["cc_cc"]));
			$smarty->assign("cc_month", htmlspecialchars($_POST["cc_month"]));
			$smarty->assign("cc_year", htmlspecialchars($_POST["cc_year"]));
			$smarty->assign("cc_cvc2", htmlspecialchars($_POST["cc_cvc2"]));
			$smarty->assign("image_front_data", htmlspecialchars($_POST["image_front_data"]));
			$smarty->assign("image_back_data", htmlspecialchars($_POST["image_back_data"]));
			$smarty->assign("token", $token);
			$smarty->assign("error", $error);
			if (!empty($_POST["captcha_str"]) && !strcasecmp($_POST["captcha_str"], $_SESSION["captcha_str"])) {
 				$smarty->assign("captcha_str", htmlspecialchars($_POST["captcha_str"]));
				$smarty->assign("captcha_ok", true);
			}
		}

		file_log("payment", "pay(): displaying payment page error='{$error}'");

		$processor->export_to_template();

		$smarty->display($processor->get_template_path());

		return false;
	}

	/**
	 * This method is called when payment succeeded and this function takes care about all things that need to be done when payment succeeds:
	 * - making classified ad(s) live
	 * - convert payment into recurring subscription if it was concerning new classified ad or renewal
	 * - upping advertiser budget
	 * - sending email notifications etc
	 */
	public static function paymentSuccessful($payment_id, $rebill = false) {
		global $db, $config_ref_timezone;

		file_log("payment", "paymentSuccessful(): payment_id={$payment_id}");

		$classifieds = new classifieds;
		$res = $db->q("
			SELECT pi.*
				, p.amount as total_amount, p.account_id, p.recurring_amount, p.recurring_period
			FROM payment_item pi 
			INNER JOIN payment p on p.id = pi.payment_id
			WHERE p.id = ?",
			[$payment_id]
			);
		$clad_ids_processed = [];
		while ($row = $db->r($res)) {
			$payment_item_id = $row["id"];
			$type = $row["type"];

			$account_id = $row["account_id"];
			$total_amount = $row["total_amount"];
			$recurring_amount = $row["recurring_amount"];
			$recurring_period = $row["recurring_period"];

			if ($type == "classified") {
				$clad_id = $row["classified_id"];
				if (in_array($clad_id, $clad_ids_processed)) {
					file_log("payment", "paymentSuccessful(): payment_id={$payment_id} clad id#{$clad_id} was already processed, skipping.");
				} else {
					file_log("payment", "paymentSuccessful(): payment_id={$payment_id} processing clad id#{$clad_id}");
					$ret = $classifieds->afterSuccessfulPayment($payment_id, $clad_id, $rebill);
					if (!$ret) {
						file_log("payment", "paymentSuccessful(): classifieds::afterSuccessfulPayment() failed for payment_id={$payment_id}, clad_id={$clad_id} !");
						reportAdmin("AS: payment::paymentSuccessful(): error", "classifieds::afterSuccessfulPayment() failed for payment_id={$payment_id}, clad_id={$clad_id} !");
					}
					$clad_ids_processed[] = $clad_id;
				}
			} else if ($type == "advertise") {
				advertise::paymentSuccessful($account_id, $total_amount);

			} else if ($type == "repost") {
				
				$auto_renew = intval($row["auto_renew"]);
				if ($auto_renew) {
					$res2 = $db->q("UPDATE account SET repost = IFNULL(repost,0) + ? WHERE account_id = ? LIMIT 1", [$auto_renew, $account_id]);
					$aff = $db->affected($res2);
					if ($aff != 1) {
						file_log("payment", "paymentSuccessful(): Can't add {$auto_renew} topup credits for account_id={$account_id} !");
						reportAdmin("AS: payment::paymentSuccessful(): ERR", "Can't add {$auto_renew} topup credits for account_id={$account_id} !");
					}
				} else {
					file_log("payment", "paymentSuccessful(): auto_renew not set for payment_item with type 'repost' ! payment_item_id={$payment_item_id}");
					reportAdmin("AS: payment::paymentSuccessful(): ERR", "auto_renew not set for payment_item with type 'repost' ! payment_item_id={$payment_item_id}");
				}

			} else if ($type == "businessowner") {
				businessowner::paymentSuccessful($payment_item_id);

			} else {
				file_log("payment", "paymentSuccessful(): unknown payment item type'{$type}' !");
				reportAdmin("AS: payment::paymentSuccessful(): unknown payment item type", "Implement handling for payment item type '{$type}' !");
			}

			//if this is initial payment and we have recurring_amount set, "enable" recurring subscription
			if (!$rebill && $recurring_amount) {
				//determine next renewal date
				$today = new \DateTime("today", new \DateTimeZone($config_ref_timezone));
				$next_renewal_date = clone $today;
				$next_renewal_date->modify("+{$recurring_period} days");
				file_log("payment", "paymentSuccessful(): Recurring amount '{$recurring_amount}' -> enabling subscription. payment_id={$payment_id}, today=".$today->format("Y-m-d").", recurring_period={$recurring_period}, next_renewal_date=".$next_renewal_date->format("Y-m-d"));
				$db->q("UPDATE payment SET subscription_status = 1, next_renewal_date = ? WHERE id = ? LIMIT 1", [$next_renewal_date->format("Y-m-d"), $payment_id]);
			}

		}
		return true;
	}

	/**
	 * This function takes care of redirection to proper thank you page after successful payment
	 */
	public static function afterPayment($payment_id) {
		global $account, $db;

		file_log("payment", "afterPayment(): payment_id={$payment_id}");

		if ($account->isadmin()) {
			//if we are admin, redirect to sales page
			system::go("/mng/sales");
		}

		//get first payment item and redirect accordingly
		$res = $db->q("
			SELECT a.agency, p.amount, c.done, pi.* 
			FROM payment_item pi 
			INNER JOIN payment p on p.id = pi.payment_id 
			INNER JOIN account a on a.account_id = p.account_id
			LEFT JOIN classifieds c on pi.classified_id = c.id
			WHERE pi.payment_id = ? 
			LIMIT 1", 
			[$payment_id]
			);
		if (!$db->numrows($res))
			system::go("/classifieds/myposts");

		$row = $db->r($res);

		//google analytics purchase goals initialization
		$agency = $row["agency"];
		$amount = preg_replace('/[^0-9\.]/', '', $row["amount"]);
		$ga_goal = "other";

		//TODO
		if ($row["type"] == "classified") {
			if ($row["classified_status"] == "N") {
				//new ad purchase or renewal
				//google analytics purchase goals
				if ($amount == 9.99) {
					$ga_goal = "ad-9";
				} else if ($agency && $amount == 49.99) {
					$ga_goal = "ad-49";
				}
				system::go("/adbuild/step5?ad_id={$row["classified_id"]}&purchase={$ga_goal}");
			} else {
				//upgrade
				if (!$row["sponsor"] && !$row["side"] && $row["sticky"]) {
					//sticky
					if ($amount == 600) {
						$ga_goal = "sticky-600";
					} else if ($amount == 400) {
						$ga_goal = "sticky-400";
					} else {
						$ga_goal = "sticky-other";
					}
				} else if ($row["sponsor"]&& !$row["side"] && !$row["sticky"]) {
					//city thumbnail
					if ($amount == 200) {
						$ga_goal = "city-200";
					} else if ($amount == 100) {
						$ga_goal = "city-100";
					}
				} else if (!$row["sponsor"] && $row["side"] && !$row["sticky"]) {
					//side
					if ($amount == 50) {
						$ga_goal = "side-50";
					} else if ($amount == 25) {
						$ga_goal = "side-25";
					}
				}
				system::go("/classifieds/myposts?purchase={$ga_goal}");
			}
		} else if ($row["type"] == "advertise") {
			system::go("/advertise");
		} else if ($row["type"] == "businessowner") {
			businessowner::afterPayment($row["id"]);
		} else {
			system::go("/classifieds/myposts?purchase={$ga_goal}");
		}

		return true;
	}

	/**
	 * This function captures authorised payment
	 * Returns false if capture failed or admin text message when succeeded
	 */
	public static function capture($payment_id, &$error = null) {
		global $db, $account;

		$now = time();
		file_log("payment", "capture: payment_id={$payment_id}");

        $res = $db->q("SELECT result, processor, trans_id, amount FROM payment WHERE id = ?", [$payment_id]);
        if ($db->numrows($res) != 1) {
            file_log("payment", "capture: Error: payment not found !");
            $error = "Invalid payment! Payment not found.";
            return false;
        }
        $row = $db->r($res);
        $result = $row["result"];
        $trans_id = $row["trans_id"];
        $amount = $row["amount"];
		$processor = $row["processor"];

        if ($result != "U") {
            //this payment is not in authorized state, perhaps it is failed or already captured ??
            file_log("payment", "capture: Error: payment #{$payment_id} status is not authorized(U), it is '{$result}'");
            reportAdmin("AS: payment::capture failed !", "payment #{$payment_id} status is not authorized(U), it is '{$result}'");
            return false;
        }

		if ($processor != "securionpay") {
            //not implemented processor for capture
            file_log("payment", "capture: Error: not implemented processor '{$processor}' for payment #{$payment_id}");
            reportAdmin("AS: payment::capture failed !", "Error: not implemented processor '{$processor}' for payment #{$payment_id}");
            return false;
		}

        file_log("payment", "capture: going to capture trans_id='{$trans_id}'");

		$securionpay = new securionpay();
		$ret = $securionpay->capture($trans_id, $error);
		if (!$ret) {
			//capture failed
        	file_log("payment", "capture: Error: capture failed: error='{$error}'");
			return false;
		}

		//update payment
        $res = $db->q("
            UPDATE payment SET result = ?, responded_stamp = ?, trans_id = ? WHERE id = ? LIMIT 1
            ",
            ["A", $now, $trans_id, $payment_id]
            );
        $aff = $db->affected($res);
        if ($aff != 1) {
            file_log("paymnet", "capture: ERROR while updating payment to result 'A' !");
            reportAdmin("Payment succeeded - error: cant update payment", "cant update payment about transaction success, payment_id={$payment_id}, trans_id={$trans_id}, aff={$aff}");
        }

        //add transaction
        $res = $db->q(
            "INSERT INTO transaction (type, payment_id, stamp, trans_id, amount) VALUES (?, ?, ?, ?, ?)",
            ["N", $payment_id, $now, $trans_id, $amount]
            );
        $transaction_id = $db->insertid($res);
        if (!$transaction_id) {
            file_log("payment", "capture: ERROR while inserting transaction: payment_id={$payment_id}, trans_id={$trans_id}, amount={$amount}");
            reportAdmin("AS: Payment succeeded - error: cant insert transaction", "cant insert transaction into db, payment_id={$payment_id}, trans_id={$trans_id}");
        }

		//insert payment event
		$db->q("INSERT INTO payment_event
				(payment_id, transaction_id, author_id, stamp, type, message)
				VALUES
				(?, ?, ?, ?, ?, ?)
				",
				[$payment_id, $transaction_id, $account->getId(), $now, "C", "Captured charge #{$trans_id} in amount {$amount}"]
				);
		$id = $db->insertid();
		if (!$id) {
			//this should not happen
			reportAdmin("AS: payment::capture : cant insert into payment_event", "", ["stamp" => $now, "author_id" => $account->getId(), "transaction_id" => $transaction_id, "payment_id" => $payment_id, "amount" => $amount]);
		}

		$msg = "Transaction '{$trans_id}' successfully captured (\${$amount}).";
		//self::captureReceipt($email, $transaction_id);

		return $msg;
	}


	/**
	 * This function cancels recurring subscription
	 */
	public static function cancel($payment_id, $reason, &$error = null, $send_email_notification = false) {
		global $db, $account;

		file_log("payment", "cancel: payment_id={$payment_id}, reason='{$reason}'");

		//fetch payment/subscription details from db
		$res = $db->q("SELECT p.email, p.subscription_id, p.subscription_status, p.recurring_amount, p.responded_stamp, p.trans_id
			FROM payment p
			WHERE p.id = ?
			LIMIT 1",
			[$payment_id]
			);
		if ($db->numrows($res) != 1) {
			file_log("payment", "cancel: Error fetching payment #{$payment_id} from db !");
			reportAdmin("AS: payment:cancel error", "Error fetching payment #{$payment_id} from db !");
			return false;
		}
		$row = $db->r($res);
		$email = $row["email"];
		$subscription_id = $row["subscription_id"];
		$subscription_status = $row["subscription_status"];
		if (!is_null($subscription_status) && !$subscription_id)
			$subscription_id = $payment_id;
		$amount = $row["recurring_amount"];
		$trans_id = $row["trans_id"];
		$trans_date_time = null;
		if ($row["responded_stamp"])
			$trans_date_time = date("m/d/Y H:i:s", $row["responded_stamp"]);

		$res = $db->q("UPDATE payment SET subscription_status = 2, next_renewal_date = NULL WHERE id = ? LIMIT 1", [$payment_id]);
		$aff = $db->affected($res);
		if ($aff != 1) {
			file_log("payment", "cancel: Error while canceling payment #{$payment_id}: aff={$aff}");
			return false;
		}

		//insert cancel payment event
		$res = $db->q("
			INSERT INTO payment_event 
			(payment_id, author_id, stamp, type, message)
			VALUES
			(?, ?, ?, ?, ?)",
			[$payment_id, $account->getId(), time(), "SC", "Subscription canceled, reason: '{$reason}'"]
			);
		$id = $db->insertid($res);
		if (!$id) {
			file_log("payment", "cancel: Error while inserting cancel payment event #{$payment_id}: id={$id}");
			reportAdmin("AS: payment:cancel succeeded, but payment event insert failed", "payment_id={$payment_id}, id={$id}");
		}

		file_log("payment", "cancel: payment #{$payment_id} successfully cancelled");

		if ($send_email_notification)
			self::cancelReceipt($email, $subscription_id, $amount, $trans_id, $trans_date_time);

		return true;
	}

	/**
	 * This function refunds the transaction at payment processor and updates database accordingly
	 * If transaction_id is null, then it is authorised payment with no transaction yet
	 * Returns false if refund faild or admin text message when succeeded
	 */
	public static function refund($payment_id, $transaction_id = null, $cancel_subscription = false, $p_amount = null, $refund_reason = "", &$error = null) {
		global $db, $account;

		file_log("payment", "refund: payment_id={$payment_id}, transaction_id={$transaction_id}, amount={$p_amount}");

		if ($transaction_id) {
			$res = $db->q("
				SELECT t.payment_id, t.amount, t.stamp, t.trans_id
				, p.email, p.account_id, p.cc_id, p.trans_id as payment_trans_id, p.subscription_status, p.subscription_id, p.processor
				, cc.cc
				FROM transaction t
				INNER JOIN payment p on p.id = t.payment_id
				LEFT JOIN account_cc cc on cc.cc_id = p.cc_id
				WHERE t.id = ?
				LIMIT 1",
				[$transaction_id]
				);
			if ($db->numrows($res) != 1) {
				file_log("payment", "refund: Error: transaction not found !");
				$error = "Can't find transaction #{$transaction_id}";
				return false;
			}
		} else {
			$res = $db->q("
				SELECT p.id as payment_id, p.amount, p.responded_stamp as stamp, p.trans_id
				, p.email, p.account_id, p.cc_id, p.trans_id as payment_trans_id, p.subscription_status, p.subscription_id, p.processor
				, cc.cc
				FROM payment p 
				LEFT JOIN account_cc cc on cc.cc_id = p.cc_id
				WHERE p.id = ? AND p.result = 'U'
				LIMIT 1",
				[$payment_id]
				);
			if ($db->numrows($res) != 1) {
				file_log("payment", "refund: Error: payment not found !");
				$error = "Can't find authorised payment #{$payment_id}";
				return false;
			}	
		}

		$row = $db->r($res);
		$trans_id = $row["trans_id"];
		$trans_date_time = date("m/d/Y H:i:s T", $row["stamp"]);	//for receipt   
		$amount = $row["amount"];
		$payment_id = $row["payment_id"];
		$payment_trans_id = $row["payment_trans_id"];
		$email = $row["email"];
		$account_id = $row["account_id"];
		$cc_id = $row["cc_id"];
		$subscription_id = $row["subscription_id"];
		$subscription_status = $row["subscription_status"];
		$cardno_last_4 = substr($row["cc"], -4);
		$processor = $row["processor"];

		if ($amount < 0.01) {
			//nothing to refund !
			file_log("payment", "refund(): Error: nothing to refund, amount={$amount}");
			reportAdmin("AS: Refund failed - nothing to refund !", "payment_id={$payment_id}, transaction_id={$transaction_id}, p_amount={$p_amount}, amount={$amount}");
			return false;
		}

		if ($p_amount && $p_amount > ($amount + 0.001)) {
			//refund amount too big !
			file_log("payment", "refund(): Error: refund amount too big, amount={$amount}");
			reportAdmin("AS: Refund failed - amount too big !", "payment_id={$payment_id}, transaction_id={$transaction_id}, p_amount={$p_amount}, amount={$amount}");
			return false;
		} else if (!$p_amount) {
			$p_amount = $amount;
		}

		$new_amount = number_format($amount - $p_amount, 2, ".", "");
		if ($new_amount < 0)
			$new_amount = 0;

		$error = null;
		if ($processor == "securionpay") {
			$securionpay = new securionpay();
			$ret = $securionpay->refund_charge($trans_id, $p_amount, $error);
		} else if ($processor == "budget") {
			$budget = new budget();
			$ret = $budget->refund_charge($account_id, $p_amount, $error);
		} else {
			reportAdmin("AS: payment:refund error", "Unimplemented refund for processor '{$processor}' ! payment_id={$payment_id}, transaction_id={$transaction_id}, amount={$amount}");
			$ret = false;
		}

		if (!$ret) {
			//refund failed
			return false;
		}

		//refund succeeded
		$now = time();

		if ($transaction_id) {
			//insert refund into account_refund table
			$db->q("INSERT INTO account_refund 
					(type, stamp, author_id, transaction_id, payment_id, account_id, cc_id, amount, reason)
					VALUES
					(?, ?, ?, ?, ?, ?, ?, ?, ?)",
					array("R", $now, $account->getId(), $transaction_id, $payment_id, $account_id, $cc_id, $p_amount, $refund_reason));
			$id = $db->insertid();
			if (!$id) {
				//this should not happen
				reportAdmin("AS: payment::refund : cant insert into account_refund", "", ["type" => "R", "now" => $now, "author_id" => $account->getId(), "transaction_id" => $transaction_id, "payment_id" => $payment_id, "account_id" => $account_id, "cc_id" => $cc_id, "p_amount" => $p_amount, "refund_reason" => $refund_reason]);
			}
		} else {
			$subscription_status_sql = "";
			if ($subscription_status == 1)
				$subscription_status_sql = ", subscription_status = 1 ";
			$res2 = $db->q("UPDATE payment SET result = 'C' {$subscription_status_sql} WHERE id = ? LIMIT 1", [$payment_id]);
			if ($db->affected($res2) != 1) {
				file_log("payment", "refund: Error: cant update payment from authorised to canceled status, payment_id={$payment_id}");
				reportAdmin("AS: payment::refund : cant update payment from authorised to canceled status", "", ["payment_id" => $payment_id]);
			} else {
				file_log("payment", "refund: Payment updated from authorised to canceled");
			}
		}

		//insert payment event
		$db->q("INSERT INTO payment_event
				(payment_id, transaction_id, author_id, stamp, type, message)
				VALUES
				(?, ?, ?, ?, ?, ?)
				",
				[$payment_id, $transaction_id, $account->getId(), $now, "R", "Refunded transaction #{$transaction_id} in amount {$p_amount}"]
				);
		$id = $db->insertid();
		if (!$id) {
			//this should not happen
			reportAdmin("AS: payment::refund : cant insert into payment_event", "", ["stamp" => $now, "author_id" => $account->getId(), "transactoin_id" => $transaction_id, "payment_id" => $payment_id, "p_amount" => $p_amount]);
		}

		if ($transaction_id) {
			//update transaction table
			$db->q("UPDATE transaction SET amount = ? WHERE id = ? LIMIT 1", [$new_amount, $transaction_id]);
			$aff = $db->affected($res);
			if ($aff != 1) {
				//this should not happen
				reportAdmin("AS: payment::refund : cant update transaction", "", ["new_amount" => $new_amount, "id" => $transaction_id]);
			}
		}

		$msg = "Transaction '{$trans_id}' successfully refunded (\${$p_amount}).";
		self::refundReceipt($email, $trans_id, $trans_date_time, $p_amount, $cardno_last_4, $transaction_id);

		//---------------------------------------------------------
		//if we also want to wish to cancel subscription, cancel it
		if ($transaction_id && $cancel_subscription && $subscription_status == self::SUBSCRIPTION_ACTIVE) {
			file_log("payment", "refund(): cancelling recurring payment #{$payment_id}");
			$ret = self::cancel($payment_id, "refund: '{$refund_reason}'", $error);
			if (!$ret) {
				$error = "Transaction refunded successfully, but subscription cancellation failed: {$error} !";
				return false;
			}
			if ($subscription_id)
				$msg .= " Subscription '{$subscription_id}' cancelled.";
			else
				$msg .= " Recurring payment #{$payment_id} cancelled.";

		}

		return $msg;
	}


	public static function chargeback($transaction_id, $cancel_subscription = false, $p_amount = null, &$error = null) {
		global $db, $account;

		file_log("payment", "chargeback(): purchase_id={$purchase_id}, amount={$amount}, cancel_subscription=".intval($cancel_subscription));

		$res = $db->q("SELECT t.payment_id, t.amount, t.stamp, t.trans_id
						, p.email, p.account_id, p.cc_id, p.trans_id as payment_trans_id, p.subscription_id, p.subscription_status, p.processor
						, cc.cc
						FROM transaction t
						INNER JOIN payment p on p.id = t.payment_id
						INNER JOIN account_cc cc on cc.cc_id = p.cc_id
						WHERE t.id = ?
						LIMIT 1",
						[$transaction_id]);
		if ($db->numrows($res) != 1) {
			$error = "Can't find transaction #{$transaction_id}";
			return false;
		}

		$row = $db->r($res);
		$trans_id = $row["trans_id"];
		$trans_date_time = date("m/d/Y H:i:s T", $row["stamp"]);	//for receipt   
		$amount = $row["amount"];
		$payment_id = $row["payment_id"];
		$payment_trans_id = $row["payment_trans_id"];
		$email = $row["email"];
		$account_id = $row["account_id"];
		$cc_id = $row["cc_id"];
		$subscription_id = $row["subscription_id"];
		$subscription_status = $row["subscription_status"];
		$cardno_last_4 = substr($row["cc"], -4);
		$processor = $row["processor"];

		if ($amount < 0.01) {
			//either this was zero purchase (should not happen ?) - and there is nothing to chargeback
			//or this was already refunded/chargebacked
			//do not chargeback
			$error = "Transaction #{$transaction_id} was already refunded/chargebacked ?";
			return false;
		}

		if (!$p_amount)
			$p_amount = $amount;
		$new_amount = number_format($amount - $p_amount, 2, ".", "");
		if ($new_amount < 0)
			$new_amount = 0;

		if ($processor == "securionpay") {
			//we don't actually do anything with the transaction on securionpay side
		} else {
			//dont so anything here
		}

		//insert chargeback into account_refund table
		$now = time();
		$db->q("INSERT INTO account_refund
				(type, stamp, author_id, transaction_id, payment_id, account_id, cc_id, amount, reason)
				VALUES
				(?, ?, ?, ?, ?, ?, ?, ?, ?)",
				array("CH", $now, $account->getId(), $transaction_id, $payment_id, $account_id, $cc_id, $p_amount, "chargeback")
				);
		$id = $db->insertid();
		if (!$id) {
			//this should not happen
			reportAdmin("AS: payment::chargeback : cant insert into account_refund", "", ["type" => "CH", "now" => $now, "author_id" => $account->getId(), "transaction_id" => $transaction_id, "payment_id" => $payment_id, "account_id" => $account_id, "cc_id" => $cc_id, "amount" => $p_amount, "reason" => "chargeback"]);
		}

		//insert payment event
		$db->q("INSERT INTO payment_event
				(payment_id, transaction_id, author_id, stamp, type)
				VALUES
				(?, ?, ?, ?, ?)
				",
				[$payment_id, $transaction_id, $account->getId(), $now, "CH"]
				);
		$id = $db->insertid();
		if (!$id) {
			//this should not happen
			reportAdmin("AS: payment::chargeback : cant insert into payment_event", "", ["stamp" => $now, "author_id" => $account->getId(), "payment_id" => $payment_id, "type" => "CH", "trans_id" => $trans_id, "amount" => $amount]);
		}

		//update transaction table
		$db->q("UPDATE transaction SET amount = ? WHERE id = ? LIMIT 1", [$new_amount, $transaction_id]);
		$aff = $db->affected($res);
		if ($aff != 1) {
			//this should not happen
			reportAdmin("AS: payment::chargeback : cant update transaction", "", ["new_amount" => $new_amount, "id" => $transaction_id]);
		}

		$msg = "Transaction '{$trans_id}' successfully marked as chargeback (\${$p_amount}).";


		//---------------------------------------------------------
		//if we also want to wish to cancel subscription, cancel it
		if ($cancel_subscription && $subscription_status == self::SUBSCRIPTION_ACTIVE) {
			file_log("payment", "chargeback(): cancelling recurring payment #{$payment_id}");
			$ret = self::cancel($payment_id, "chargeback", $error);
			if (!$ret) {
				$error = "Transaction chargebacked successfully, but subscription cancellation failed: {$error} !";
				return false;
			}
			if ($subscription_id)
				$msg .= " Subscription '{$subscription_id}' cancelled.";
			else
				$msg .= " Recurring payment #{$payment_id} cancelled.";

		}

		//everything allright
		return $msg;
	}

	/**
	 * This function cancels all payments for specified classified ad
	 */
	public static function cancel_for_clad($clad_id) {
		global $db, $account;

		file_log("payment", "cancel_for_clad: clad_id={$clad_id}");

		//fetch payments for this classified id
		$res = $db->q("SELECT DISTINCT p.id
			FROM payment p
			INNER JOIN payment_item pi on pi.payment_id = p.id
			WHERE pi.classified_id = ?",
			[$clad_id]
			);
		while ($row = $db->r($res)) {
			$payment_id = $row["id"];
			$error = null;
			//file_log("payment", "cancel_for_clad: cancelling payment #{$payment_id}");
			self::cancel($payment_id, "delete ad #{$clad_id}", $error);
		}
		return true;
	}

	private static function refundReceipt($email, $trans_id, $trans_date_time, $amount, $cardno_last_4, $transaction_id = false) {

		if (!$email || !$trans_id || !$amount)
			return false;

		$params = [
			"trans_id" => $trans_id,
			"trans_date_time" => $trans_date_time,
			"amount" => $amount,
			"card_label" => "X".$cardno_last_4,
			"transaction_id" => $transaction_id,
			];

		if ($trans_id == "budget")
			$subject = "Transaction #{$transaction_id} refunded - receipt";
		else
			$subject = "Transaction #{$trans_id} refunded - receipt";

		return self::sendPaymentEmail("payment/email_refund", $params, $subject, $email);
	}

	private static function cancelReceipt($email, $subscription_id, $amount, $trans_id, $trans_date_time) {

		if (!$email || !$subscription_id)
			return false;

		$params = [
			"subscription_id" => $subscription_id,
			"amount" => $amount,
			"trans_id" => $trans_id,
			"trans_date_time" => $trans_date_time,
			];

		$subject = "Subscription #{$subscription_id} canceled - receipt";

		return self::sendPaymentEmail("payment/email_cancel", $params, $subject, $email);
	}

	private static function renewalFailedReceipt($email, $subscription_id, $amount, $cardno_last_4, $reason) {

		if (!$email || !$subscription_id)
			return false;

		$params = [
			"subscription_id" => $subscription_id,
			"amount" => $amount,
			"card_label" => ($cardno_last_4) ? "X".$cardno_last_4 : null,
			"reason" => $reason,
			];

		$subject = "Renewal failed for subscription #{$subscription_id} - notification";

		return self::sendPaymentEmail("payment/email_renewal_failed", $params, $subject, $email);
	}

	private static function renewalSuccessReceipt($email, $subscription_id, $trans_id, $amount, $cardno_last_4, $payment_id) {

		if (!$email || !$subscription_id)
			return false;

		$params = [
			"subscription_id" => $subscription_id,
			"trans_id" => $trans_id,
			"amount" => $amount,
			"card_label" => ($cardno_last_4) ? "X".$cardno_last_4 : null,
			"items" => self::getPaymentItems($payment_id),
			];

		$subject = "Renewal successful for subscription #{$subscription_id} - notification";

		return self::sendPaymentEmail("payment/email_renewal_success", $params, $subject, $email);
	}

	public static function getPaymentItems($payment_id) {
		global $db, $account;


		file_log("payment", "getPaymentItems: payment_id={$payment_id}");

		//every payment should have payment items
		$cnt = $db->single("SELECT COUNT(*) as cnt FROM payment_item pi WHERE pi.payment_id = ?", [$payment_id]);
		if (!$cnt) {
			file_log("payment", "get_payment_items: Error: no payment items for payment payment_id={$payment_id}");
			reportAdmin("AS: payment::get_payment_items() error", "payment::get_payment_items() error - no payment items for payment {$payment_id}");
			return [];
		}

		//find items that this payment is for
		$res = $db->q("SELECT * FROM payment_item pi WHERE pi.payment_id = ?", [$payment_id]);
		$items = [];
		while ($row = $db->r($res)) {
			$label = "";

			if ($row["type"] == "classified") {

				$label .= "Classified ad #{$row["classified_id"]}";

				if ($row["sponsor"])
					$label .= " - City thumbnail";
				if ($row["side"])
					$label .= " - Side sponsor";
				if ($row["sticky"])
					$label .= " - Sticky ad";
				if ($row["sponsor_desktop"])
					$label .= " - Sponsor Desktop";
				if ($row["sponsor_mobile"])
					$label .= " - Sponsor Mobile";
				if ($row["daily_repost"])
					$label .= " - Daily repost";

				$res2 = $db->q("SELECT l.loc_name, l.s FROM location_location l WHERE l.loc_id = ?", [$row["loc_id"]]);
				if ($db->numrows($res2)) {
					$row2 = $db->r($res2);
					$label .= " - {$row2["loc_name"]}, {$row2["s"]}";
				}

			} else if ($row["type"] == "businessowner") {
				$label .= " - Business owner";
				//TODO

			} else if ($row["type"] == "advertise") {
				$label .= " - Advertising credit";

			} else if ($row["type"] == "repost") {
				$label .= " - Global top-up credits";

			}

			$items[] = $label;
		}

		return $items;
	}
	
	/**
	 * this function encapsulates old and new way of defining payment items
	 */
	public static function get_payment_items($payment_id) {
		global $db, $account;

		$items = [];

		file_log("payment", "get_payment_items: payment_id={$payment_id}");

		//find items that this payment is for
		$res = $db->q("SELECT pi.id FROM payment_item pi WHERE pi.payment_id = ?", [$payment_id]);
		if ($db->numrows($res) == 0) {
			file_log("payment", "get_payment_items: Error: no payment items for payment payment_id={$payment_id}");
			reportAdmin("AS: payment::get_payment_items() error", "payment::get_payment_items() error - no payment items for payment {$payment_id}");
			return [];
		}

		$res = $db->q("SELECT DISTINCT pi.classified_id FROM payment_item pi WHERE pi.payment_id = ? and pi.type = 'classified'", [$payment_id]);
		while ($row = $db->r($res)) {
			$clad_id = $row["classified_id"];

			file_log("payment", "get_payment_items: New way, clad_id={$clad_id}");
			$res2 = $db->q("SELECT pi.* FROM payment_item pi WHERE pi.payment_id = ? AND pi.classified_id = ? ", [$payment_id, $clad_id]);
			$ar = null;
			$location_count = 0;
			$locations = [];
			while ($row2 = $db->r($res2)) {
				$payment_item_id = $row2["id"];
				$auto_renew = $row2["auto_renew"];
				$loc_id = $row2["loc_id"];
				$sponsor = $row2["sponsor"];
				$side = $row2["side"];

				if (!$loc_id) {
					debug_log("classifieds::get_payment_items: error: loc_id not specified in payment_item #{$payment_item_id}!");
					reportAdmin("as: classifieds::get_payment_items error", "loc_id not specified in payment_item #{$payment_item_id}!", []);
					return false;
				}

				if ($auto_renew) {
					if ($ar == null) {
						$ar = $auto_renew;
					} else if ($ar != $auto_renew) {
						debug_log("classifieds::get_payment_items: error: auto_renew value differs between location ar={$ar} auto_renew={$auto_renew} !");
						reportAdmin("AS: classifieds::get_payment_items error", "auto_renew val differs between location ar={$ar} auto_renew={$auto_renew}");
						return false;
					}
				}

				$locations[] = [
					"loc_id" => $loc_id,
					"sponsor" => $sponsor,
					"side" => $side,
					];
			}

			$items[] = [
				"type" => "classified",
				"clad_id" => $clad_id,
				"locations" => $locations,
				"auto_renew" => $auto_renew,
				];
		}

		//other payment items
		$res = $db->q("SELECT pi.* FROM payment_item pi WHERE pi.payment_id = ? and pi.type <> 'classified'", [$payment_id]);
		while ($row = $db->r($res)) {
			$payment_item_id = $row["id"];
			$type = $row["type"];

			if ($type == "repost") {
				//add global repost credits
				$credits = intval($row["auto_renew"]);
				if (!$credits) {
					file_log("payment", "get_payment_items: Error: zero repost credits for repost type payment item ? payment_item_id={$payment_item_id}");
					reportAdmin("AS: payment:get_payment_items: error", "Error: zero repost credits for repost type payment item ?", ["payment_item_id" => $payment_item_id]);
					continue;
				}
				$items[] = [
					"type" => "repost",
					"credits" => $credits,
					];
			} else {
				file_log("payment", "get_payment_items: Error: Unknown type of payment item renewal: '{$type}' !, payment_item_id={$payment_item_id}");
				reportAdmin("AS: payment:get_payment_items: error", "Unknown type of payment item renewal: '{$type}' !", ["payment_id" => $payment_id, "payment_item_id" => $payment_item_id]);
			}
		}

		file_log("payment", "get_payment_items: items='".print_r($items, true)."'");
		return $items;
	}

	/**
	 * this function encapsulates old and new way of defining payment items
	 */
	public static function get_payment_classified_id($payment_id) {
		global $db, $account;

		$items = [];

		file_log("payment", "get_payment_classified_id: payment_id={$payment_id}");
		//find items that this payment is for
		$res = $db->q("SELECT pi.* FROM payment_item pi WHERE pi.payment_id = ?", [$payment_id]);
		if ($db->numrows($res) > 0) {
			while ($row = $db->r($res)) {
				if ($row["type"] == "classified")
					return $row["classified_id"];
			}
		}

		return null;
	}

	public static function emailRenewalSuccess($transaction_id, $email = null) {
		global $account, $db;
		$smarty = GetSmartyInstance();

		$res = $db->q("
			SELECT t.amount, t.payment_id, t.trans_id, t.subscription_id 
				, p.email
				, cc.type, cc.cc
			FROM transaction t 
			INNER JOIN payment p on p.id = t.payment_id
			LEFT JOIN account_cc cc on cc.cc_id = p.cc_id
			WHERE t.id = ?
			", 
			[$transaction_id]
		);
		if ($db->numrows($res) != 1) {
			reportAdmin("AS: payment::emailRenewalSuccess() error", "cant find transaction for transaction_id#{$transaction_id} !");
			return false;
		}
		$row = $db->r($res);
		$trans_id = $row["trans_id"];
		$amount = $row["amount"];
		$payment_id = $row["payment_id"];
		$card_description = "";
		if ($row["cc"]) {
			$card_description = creditcard::convertType($row["type"])." ".substr($row["cc"], -5);
		}
		
		$smarty->assign("trans_id", $trans_id);
		$smarty->assign("amount", number_format($amount, 2, ".", ""));
		$smarty->assign("card_description", $card_description);

		if (!$email) {
			$email = $row["email"];
		}


		$email_items = [];
		$items = self::get_payment_items($payment_id);
		foreach ($items as $item) {
			if (!array_key_exists("type", $item))
				continue;

			if ($item["type"] == "classified") {

				if (!array_key_exists("clad_id", $item) || !intval($item["clad_id"]))
					continue;
				$clad_id = intval($item["clad_id"]);

				if (!array_key_exists("locations", $item)) {
					$res = $db->q("
						SELECT cl.loc_id, l.loc_name, l.s 
						FROM classifieds_loc cl
						INNER JOIN location_location l on l.loc_id = cl.loc_id
						WHERE cl.post_id = ? AND cl.done >= 0
						",
						[$clad_id]
						);
					$location_count = $db->numrows($res);
					$locations_text = "";
					while ($row = $db->r($res)) {
						$locations_text .= (empty($locations_text)) ? "" : "; ";
						$locations_text .= $row["loc_name"].", ".$row["s"];
					}
					if ($location_count > 1)
						$locations_text = "{$location_count} locations: ";
					$email_items[] = "Classified ad: {$locations_text}";
				} else {

					$location_count = 0;
					$locations_text = "";
					foreach ($item["locations"] as $location) {

						if (!array_key_exists("loc_id", $location) || !intval($location["loc_id"]))
							continue;
						$loc_id = intval($location["loc_id"]);

						$res = $db->q("
							SELECT l.loc_name, l.s 
							FROM location_location l
							WHERE l.loc_id = ?
							",
							[$loc_id]
							);
						if ($db->numrows($res) != 1)
							continue;
						$row = $db->r($res);
						$locations_text .= (empty($locations_text)) ? "" : "; ";
						$locations_text .= $row["loc_name"].", ".$row["s"];
						$location_count++;
						
						if (array_key_exists("sponsor", $location) && $location["sponsor"] == 1)
							$email_items[] = "City Thumbnail ".$row["loc_name"].", ".$row["s"];

						if (array_key_exists("side", $location) && $location["side"] == 1)
							$email_items[] = "Side Sponsor ".$row["loc_name"].", ".$row["s"];

					}
					if ($location_count > 1)
						$locations_text = "{$location_count} locations: ";
					$email_items[] = "Classified ad: {$locations_text}";
				}

				if (array_key_exists("sponsor", $item) && $item["sponsor"] == 1)
					$email_items[] = "City Thumbnail";

				if (array_key_exists("side", $item) && $item["side"] == 1)
					$email_items[] = "Side Sponsor";

				if (array_key_exists("auto_renew", $item) && intval($item["auto_renew"]))
					$email_items[] = intval($item["auto_renew"])." Top Of The List credits";

			} else if ($item["type"] == "repost") {

				if (!array_key_exists("credits", $item) || !intval($item["credits"]))
					continue;
				$credits = intval($item["credits"]);

				$email_items[] = "{$credits} Top Of The List credits";
			}
		}
		$smarty->assign("email_items", $email_items);

		$html = $smarty->fetch(_CMS_ABS_PATH."/templates/payment/renewal_success.html.tpl");
		$text = $smarty->fetch(_CMS_ABS_PATH."/templates/payment/renewal_success.txt.tpl");
		$subject = "Membership Renewal Success";
		$embedded_images = array("logo_email" => _CMS_ABS_PATH."/images/logo_email_vip.png");

		return email_gmail("receipts@viphost.com", "Receipts", $email, $subject, $html, $text, $embedded_images);
	}

	//temp function that is used in adbuild and purchase pages (classifieds/sponsor, ...) to disallow creating more than 1 pending payment
	public static function has_pending_payment($account_id) {
		global $db;
		$payment_id = $db->single("SELECT p.id FROM payment p WHERE p.result = 'P' AND p.account_id = ? LIMIT 1", [$account_id]);
		return ($payment_id) ? true : false;
	}

	public function getCardTypeByName($name) {
		$type = "";
		switch (strtolower($name)) {
			case "visa": $type = "VI"; break;
			case "mastercard": $type = "MC"; break;
			case "discover": $type = "DI"; break;
			case "jcb": $type = "JC"; break;
			case "maestro": $type = "MA"; break;
			case "americanexpress": $type = "AM"; break;
			case "unknown": $type = NULL; break;
			default:
				reportAdmin("AS: payment:getCardTypeByName(): Unknown card type name", "cardType = '{$name}'");
				$type = "OT";
				break;
		}
		return $type;
	}

	public static function rebill($payment_id, &$error) {
		global $db, $account;

		file_log("payment", "rebill: payment_id={$payment_id}");
		$now = time();

		$res = $db->q("
			SELECT p.account_id, p.recurring_amount, p.recurring_period, p.processor, p.cc_id, p.email
				, p.response, p.trans_id, p.next_renewal_date, p.subscription_status
			FROM payment p
			WHERE p.id = ? 
			LIMIT 1",
			[$payment_id]
			);
		if ($db->numrows($res) != 1) {
			file_log("payment", "rebill: Error, can't fetch payment #{$payment_id} from db !");
			$error = "Can't fetch payment #{$payment_id} from database !";
			return false;
		}
		$row = $db->r($res);
		$account_id = $row["account_id"];
		$amount = $row["recurring_amount"];
		$recurring_period = $row["recurring_period"];
		$processor = $row["processor"];
		$cc_id = $row["cc_id"];
		$email = $row["email"];
		$response = $row["response"];
		$trans_id = $row["trans_id"];
		$next_renewal_date = $row["next_renewal_date"];
		$subscription_status = $row["subscription_status"];
		file_log("payment", "rebill: amount={$amount}, period={$recurring_period}, processor={$processor}, cc_id={$cc_id}, subscription_status={$subscription_status}");

		$cardno_last_4 = null;

		//make sure payment is (still) valid - e.g. the classified ad exists in the db etc...
		if (!self::validate($payment_id, $error)) {
			file_log("payment", "rebill: payment #{$payment_id} not valid: '{$error}'");
			reportAdmin("AS: payment:rebill error", "Payment #{$payment_id} not valid: '{$error}'");
			return false;
		}

		if ($processor == "securionpay") {

			$securionpay = new securionpay();

			$card = creditcard::findOneById($cc_id);
			if (!$card) {
				file_log("payment", "rebill: Error, can't fetch cc #{$cc_id} from db !");
				$error = "Can't fetch cc #{$cc_id} from database !";
				return false;
			}
			if ($card->getDeletedStamp()) {
				//this card was (meanwhile) deleted -> cancel payment
				file_log("payment", "rebill: Error, can't fetch cc #{$cc_id} from db !");
				$error = null;
				$ret = self::cancel($payment_id, "Card #{$cc_id} was deleted, canceling payment", $error, false);
				if (!$ret) {
					file_log("payment", "rebill: Error: failed to cancel payment #{$payment_id} after finding out card #{$cc_id} is deleted !");
					$error = "Error: failed to cancel payment #{$payment_id} after finding out card #{$cc_id} is deleted";
					return false;
				}
				$error = "Card #{$cc_id} was deleted, canceling payment";
				return false;
			}
			$cardno_last_4 = substr($card->getCc(), -4);

			$ret = $securionpay->rebill($payment_id, $amount, $card, $error);

		} else if ($processor == "budget") {

			$budget = new budget();
			$ret = $budget->rebill($payment_id, $amount, $account_id, $error);

		} else {
			file_log("payment", "rebill: Error: Unimplemented processor '{$processor}', payment_id={$payment_id} !");
			reportAdmin("AS: Payment rebill error: Unimplemented processor", "payment_id={$payment_id}, processor='{$processor}'");
			return false;
		}

		if (!$ret) {
			//--------------
			//renewal failed
			file_log("pay", "rebill: Error: Failed to rebill, payment_id={$payment_id}, error='{$error}'");

			//update payment
			$res = $db->q(
				"UPDATE payment SET last_rebill_stamp = ?, last_rebill_result = 'D', decline_code = NULL, decline_reason = ? WHERE id = ? LIMIT 1",
				[$now, $error, $payment_id]
				);
			$aff = $db->affected($res);
			if ($aff != 1) {
				file_log("payment", "rebill: Error: Failed to update payment, payment_id={$payment_id}, error='{$error}'");
				reportAdmin("AS: Rebill failed : cant update payment", "cant update payment, error='{$error}', payment_id={$payment_id}");
			}

			//add renewal failed payment event
			$res = $db->q("
				INSERT INTO payment_event 
				(payment_id, stamp, type, message)
				VALUES
				(?, ?, ?, ?)",
				[$payment_id, time(), "RF", "Renewal failed: '{$error}'"]
				);
			$id = $db->insertid($res);
			if (!$id) {
				file_log("payment", "rebill: Error: Failed to insert RF payment event for payment #{$payment_id}");
				reportAdmin("AS: Failed to insert payment event", "payment_id={$payment_id}, id={$id}");
			}

			//cancel subscription
			$error2 = null;
			$ret = self::cancel($payment_id, "Rebill failed", $error2, false);
			if (!$ret) {
				reportAdmin("AS: Renewal failed, but can't cancel subscription", "Error = '{$error2}' payment_id={$payment_id}");
			}

			//renewal failure email/sms notification
			$ret = self::renewalFailedReceipt($email, $payment_id, $amount, $cardno_last_4, $error);
			if (!$ret) {
				file_log("pay", "rebill: Error: renewal failure email failed");
				reportAdmin("AS: Pay::rebill renewal fail email Error", "Error: renewal failure email failed, check error log");
			}

			return false;
		}

		//-----------------
		//renewal succeeded
		$transaction_id = $ret;

		//update payment
		$res = $db->q(
			"UPDATE payment SET last_rebill_stamp = ?, last_rebill_result = 'A', decline_code = NULL, decline_reason = NULL WHERE id = ? LIMIT 1",
			[$now, $payment_id]
			);
		$aff = $db->affected($res);
		if ($aff != 1) {
			file_log("payment", "rebill: Error: Failed to update payment, payment_id={$payment_id}");
			reportAdmin("AS: Rebill succeded, but cant update payment", "cant update payment, payment_id={$payment_id}");
		}

		//add renewal success payment event
		$res = $db->q("
			INSERT INTO payment_event 
			(payment_id, transaction_id, author_id, stamp, type, message)
			VALUES
			(?, ?, ?, ?, ?, ?)",
			[$payment_id, $transaction_id, $account->getId(), time(), "RS", "Renewal success: \${$amount}"]
			);
		$id = $db->insertid($res);
		if (!$id) {
			file_log("payment", "rebill: Error: Failed to insert RS payment event for payment #{$payment_id}");
			reportAdmin("AS: Failed to insert payment event", "type=RS, payment_id={$payment_id}, id={$id}");
		}

		//---------------------------------------------------
		//set next renewal date depending on recurring period
		//we need to keep in mind that right now it might be actually before actual renewal date, because we want to charge few days before
		$today = new \DateTime("today", new \DateTimeZone("America/New_York"));
		$next_renewal_date = \DateTime::createFromFormat("Y-m-d", $next_renewal_date, new \DateTimeZone("America/New_York"));
		if ($today < $next_renewal_date) {
			//we are charging BEFORE actual renewal date, lets compute next renewal date depending on current renewal date
			$new_next_renewal_date = clone $next_renewal_date;
		} else {
			//we are charging ON actual renewal date, or perhaps renewal date was already in the past (this could happen in case something went wrong with rebills...), compute next renewal date from today
			$new_next_renewal_date = clone $today;
		}
		$last_renewal_date = clone $new_next_renewal_date;
		$new_next_renewal_date->add(new \DateInterval("P{$recurring_period}D"));
		$lrd_string = $last_renewal_date->format("Y-m-d");
		$nrd_string = $new_next_renewal_date->format("Y-m-d");
		$res = $db->q("UPDATE payment SET last_renewal_date = ?, next_renewal_date = ? WHERE id = ? LIMIT 1", [$lrd_string, $nrd_string, $payment_id]);
		$aff = $db->affected($res);
		if ($aff != 1) {
			//this should not really happen, something went wrong, notify admin
			file_log("payment", "rebill: Error: Renewal successful, but failed to update next_renewal_date on payment: next_renewal_date={$nrd_string}, payment_id={$payment_id}");
			reportAdmin("AS: Rebill: Failed to update next_renewal_date", "Renewal successful, but failed to update next_renewal_date on payment: next_renewal_date={$nrd_string}, payment_id={$payment_id}");
		}

		
		//----------------------------------------------------------------
		//this handles e.g. activating all profiles, adding repost credits
		self::paymentSuccessful($payment_id, true);


		//--------------------------------------
		//renewal success email/sms notification
		$trans_id = null;
		$res = $db->q("SELECT trans_id FROM transaction WHERE id = ? LIMIT 1", [$transaction_id]);
		if ($db->numrows($res) != 1) {
			file_log("payment", "rebill: Error Failed to fetch transaction from db, payment_id={$payment_id}, transaction_id={$transaction_id}");
			reportAdmin("AS: Rebill - renewal success Error", "Error: Failed to fetch transaction from db, payment_id={$payment_id}, transaction_id={$transaction_id}");
		} else {
			$row = $db->r($res);
			$trans_id = $row["trans_id"];
		}
		$ret = self::renewalSuccessReceipt($email, $payment_id, $trans_id, $amount, $cardno_last_4, $payment_id);
		if (!$ret) {
			file_log("payment", "rebill: Error Failed to send renewal success email notify, payment_id={$payment_id}, transaction_id={$transaction_id}");
			reportAdmin("AS: Rebill - renewal success email Error", "Error: Failed to send renewal success email notify, payment_id={$payment_id}, transaction_id={$transaction_id}");
		}

		return true;
	}

	public static function processRecurringPayments($limit) {
		global $db, $config_ref_timezone;

		$now = time();

		$notify_days_before = 5;	//how many days before renewal date we notify about upcoming charge
		$notify_days_interval = 10; //how many days is the minimum period in between notifications (so we dont notify every day)
		$charge_days_before = 3;	//how many days before renewal date we actually charge the credit card

		//temporary easing in - 20181031
		$notify_days_before = 4;
		$charge_days_before = 2;

		$today = new \DateTime("today", new \DateTimeZone($config_ref_timezone));
		echo "Today is ".$today->format("Y-m-d")." in timezone {$config_ref_timezone}, limit = '{$limit}'\n";
		file_log("recurring", "Today is ".$today->format("Y-m-d")." in our timezone, limit = '{$limit}'");

		//--------------------------------------------------------------
		//charge payments that are due today
		//charge is actually few days (exactly $charge_days_before) BEFORE next_renewal_date (which should normally be the same as expiration stamp)
		// so if charge fails, customer has few days to fix her billing info
		$charge_date = clone $today;
		$charge_date->modify("+ {$charge_days_before} days");
		echo "\n";
		echo "Going to charge all payments that have renewal date on or before ".$charge_date->format("Y-m-d")."\n";
		file_log("recurring", "Going to charge all payments that have renewal date on or before ".$charge_date->format("Y-m-d"));
		$res = $db->q("
			SELECT p.id, p.recurring_amount, p.next_renewal_date, p.processor
			FROM payment p
			WHERE p.result = 'A' AND p.subscription_status = 1 AND p.next_renewal_date <= ?
			ORDER BY p.next_renewal_date ASC, p.id ASC
			",
			[$charge_date->format("Y-m-d")]
			);
		$total = $success = $fail = $total_amount = $success_amount = $fail_amount = 0;
		while ($row = $db->r($res)) {
			$payment_id = $row["id"];
			$recurring_amount = $row["recurring_amount"];
			$next_renewal_date = $row["next_renewal_date"];
			$processor = $row["processor"];

			//double check that none of the ads that the payment is for is deleted
			$continue = false;
			$res2 = $db->q("
				SELECT pi.classified_id, c.deleted 
				FROM payment_item pi 
				INNER JOIN classifieds c on c.id = pi.classified_id
				WHERE pi.payment_id = ? 
				ORDER BY pi.id ASC
				", [$payment_id]
				);
			while ($row2 = $db->r($res2)) {
				$clad_id = $row["classified_id"];
				$deleted = $row["deleted"];
				if ($deleted) {
					//this should not really happen, notify admin
					echo "Error: Clad #{$clad_id} that this payment #{$payment_id} is for is already deleted !\n";
					reportAdmin("AS: rebill error", "Rebill errror: Payment #{$payment_id} has subscription active but ad #{$clad_id} that this payment is for is already deleted ?! -> check & fix the code");
					$continue = true;
					break;
				}
			}
			if ($continue)
				continue;

			echo "Charging payment #{$payment_id}, next_renewal_date={$next_renewal_date}, processor is '{$processor}'\n";
			file_log("recurring", "Charging payment #{$payment_id}, next_renewal_date={$next_renewal_date}");

			$error = null;
			$ret = self::rebill($payment_id, $error);
			if (!$ret) {
				//rebill failed, most probably transaction was declined
				echo "Error: '{$error}': payment_id={$payment_id}\n";
				file_log("recurring", "Error: '{$error}': payment_id={$payment_id}");
				//email admin report is disabled - because this would notify about all declines
				//reportAdmin("AS: Pay:processRecurringPayments failed", "Error processing reccuring payment for payment_id={$payment_id}: '{$error}'");
				$fail++;
				$fail_amount += $recurring_amount;
			} else {
				//rebill succeeded, yay!
				echo "Success: Payment #{$payment_id} rebilled successfully.\n";
				file_log("recurring", "Success: payment_id={$payment_id}");
				$success++;
				$success_amount += $recurring_amount;
			}
			$total++;
			$total_amount += $recurring_amount;

			if ($limit && $total >= $limit)
				break;
		}
		file_log("recurring", "Tried to charge {$total} payments ({$total_amount}), {$success} payments succeeded ({$success_amount}), {$fail} failed ({$fail_amount}).");
		echo "--------------------\n";
		if ($total == 0)
			echo "No more recurring charges to process today.\n";
		else
			echo "Tried to charge {$total} payments ({$total_amount}), {$success} payments succeeded ({$success_amount}), {$fail} failed ({$fail_amount}).\n";

		//if there is less than 50% of successful charges, send total report to admin
		if ($success < ($total/2))
			reportAdmin("AS: Rebill stats", "Tried to charge {$total} payments ({$total_amount}), {$success} payments succeeded ({$success_amount}), {$fail} failed ({$fail_amount}).<br />\nPlease check if processing is going well.");

		//--------------------------------------------------------------
		//send email notifications about upcoming recurring charge
		$notify_date = clone $today;
		$notify_date->modify("+ {$notify_days_before} days");
		echo "\n";
		echo "Notifying all unnotified recurring pays that are going to happen on or before ".$notify_date->format("Y-m-d")."\n";
		file_log("recurring", "Notifying all unnotified recurring pays that are going to happen on or before ".$notify_date->format("Y-m-d"));
		$notify_stamp_limit = time() - $notify_days_interval*86400;
		$res = $db->q("
			SELECT p.id, p.next_renewal_date
			FROM payment p 
			WHERE p.result = 'A' 
					AND p.subscription_status = 1 
					AND p.next_renewal_date <= ?
					AND ( p.last_pre_charge_notify_stamp IS NULL OR p.last_pre_charge_notify_stamp < ? )
				ORDER BY p.next_renewal_date ASC
				",
				[$notify_date->format("Y-m-d"), $notify_stamp_limit]
				);
		$i = 0;
		while ($row = $db->r($res)) {
			$payment_id = $row["id"];
			$next_renewal_date = $row["next_renewal_date"];

			$charge_date = new \DateTime($next_renewal_date);
			$charge_date->modify("- {$charge_days_before} days");
			if ($charge_date <= $today) {
				echo "Not notifying payment #{$payment_id}, next_renewal_date={$next_renewal_date} - as the charge is going to happen today.\n";
				file_log("recurring", "Not notifying payment #{$payment_id}, next_renewal_date={$next_renewal_date} - the charge is going to happen today.");
				continue;
			}
			echo "Notifying payment #{$payment_id}, next_renewal_date={$next_renewal_date} about upcoming recurring charge on ".$charge_date->format("Y-m-d")."\n";
			file_log("recurring", "Notifying payment #{$payment_id}, next_renewal_date={$next_renewal_date} about upcoming recurring charge on ".$charge_date->format("Y-m-d"));

			$ret = self::sendUpcomingChargeNotification($payment_id, $charge_date);
			if (!$ret) {
				file_log("recurring", "Failed to send upcoming charge notification, payment_id={$payment_id}");
				reportAdmin("AS: upcoming charge notification error", "Failed to send upcoming charge notification, payment_id={$payment_id}");
			} else {
				$db->q("UPDATE payment SET last_pre_charge_notify_stamp = ? WHERE id = ? LIMIT 1", [$now, $payment_id]);
				$i++;
			}
		}
		echo "--------------------\n";
		echo "Sent out {$i} upcoming charge notifications\n";
		file_log("recurring", "Sent out {$i} upcoming charge notifications");
	
		return true;
	}

	public static function getPaymentSummary($payment_id) {
		$summary = "";

		$items = self::getPaymentItems($payment_id);
		foreach ($items as $item)
			$summary .= $item."\n";

		return $summary;
	}

	/**
	 * Sends out upcoming charge notification.
	 * Charge date is the date we will try to charge their credit card, it is usually few days before renewal date
	 */
	public static function sendUpcomingChargeNotification($payment_id, $charge_date, $email = null) {
		global $db, $account, $config_site_url;

		$res = $db->q("
			SELECT p.account_id, p.email, p.processor, p.recurring_amount, p.next_renewal_date 
				, cc.type, cc.cc
				, ab.budget
			FROM payment p
			LEFT JOIN advertise_budget ab on ab.account_id = p.account_id
			LEFT JOIN account_cc cc on cc.cc_id = p.cc_id 
			WHERE p.id = ?", 
			[$payment_id]
			);
		if ($db->numrows($res) != 1) {
			file_log("recurring", "Failed to send upcoming charge notification, can't fetch payment #{$payment_id} from db!");
			error_log("Failed to send upcoming charge notification, can't fetch payment #{$payment_id} from db!");
			return false;
		}
		$row = $db->r($res);
		$account_id = $row["account_id"];
		$processor = $row["processor"];
		$recurring_amount = $row["recurring_amount"];
		$next_renewal_date = $row["next_renewal_date"];
		$card_label = null;
		if ($row["cc"])
			$card_label = $row["type"]." ".substr($row["cc"], -5);
		$budget = $row["budget"];

		if (!$email)
			$email = $row["email"];

		if (!$email) {
			file_log("recurring", "Failed to send upcoming charge notification, email not set for payment #{$payment_id}!");
			error_log("Failed to send upcoming charge notification, email not set for payment #{$payment_id}!");
			return false;
		}

		$now_dt = new \DateTime("now");
		$diff_in_days = intval($charge_date->diff($now_dt)->format("%a")) + 1;

		$cancel_link = $config_site_url."/account/payments?action=subscription_cancel&payment_id={$payment_id}";
		$cancel_link = $account->createautologin($account_id, $cancel_link);

		$params = [
			"payment_summary" => self::getPaymentSummary($payment_id),
			"recurring_amount" => $recurring_amount,
			"next_renewal_date" => $next_renewal_date,
			"diff_in_days" => $diff_in_days,
			"cancel_link" => $cancel_link,
			"processor" => $processor,
			"card_label" => $card_label,
			"budget" => $budget,
			];
		
		$subject = "Upcoming charge";

		return self::sendPaymentEmail("payment/email_upcoming_charge", $params, $subject, $email);
	}

	/**
	 * Sends out payment notification email 
	 */
	public static function sendPaymentEmail($template_filename_base, $params, $subject, $email) {
		global $twig;

		if (!$template_filename_base || !$subject || !$email) {
			reportAdmin("AS: Error sending payment email", "template_filename_base='{$template_filename_base}', subject='{$subject}', email='{$email}'", $params);
			return false;
		}

		$template = $twig->load("{$template_filename_base}.html.twig");
		$html = $template->render($params);

		$template = $twig->load("{$template_filename_base}.txt.twig");
		$text = $template->render($params);

		//$embedded_images = array("logo_email" => _CMS_ABS_PATH."/images/logo_email_vip.png");
		$embedded_images = [];

		$error = null;
		$ret = send_email([
			"from" => "receipts@yeobill.com",
			"to" => $email,
//		  "bcc" => ADMIN_EMAIL,
			"subject" => $subject,
			"html" => $html,
			"text" => $text,
			"embedded_images" => $embedded_images
			], $error);
		if (!$ret) {
			reportAdmin("AS: Error sending payment email", "template_filename_base='{$template_filename_base}', subject='{$subject}', email='{$email}', error='{$error}'", $params);
		}
		return $ret;
	}


}

//END 
