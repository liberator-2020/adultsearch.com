<?php

class permission {

	private $id = NULL,
			$email = NULL;
			
	function __construct() {
	}

	public static function has($permission_name) {
		global $account, $mcache;

		if ($account->isrealadmin() || $account->isadmin())
			return true;

		$per_row = $mcache->get("SQL:PER-NAM", array($permission_name));
		if ($per_row === false)
			return false;		//we didnt find permission by name ? this should not happen, return false
		$permission_id = intval($per_row["id"]);
		if (!$permission_id)
			return false;

		$permissions = $account->getPermissions();

		if (in_array($permission_id, $permissions))
			return true;

		return false;
	}

	/**
	 * This makes sure:
	 * - user is logged in and if not then he's redirected to login page
	 * - user has the specified permission, if not it return false
	 */
	public static function access($permission_name) {
		global $account;

		if (!$account->isMember()) { 
			$account->asklogin(); 
			return false; 
		}

		return self::has($permission_name);
	}

}

