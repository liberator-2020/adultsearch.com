<?php

use App\Entity\Message;
use App\Repository\PhoneVerificationRepository;
use App\Service\Sms\Sms;

/**
 * Phone verification functions
 */
class phone {

	const SMS_VERIFICATION_TIMEOUT = 15; // min;
	const MAX_ATTEMPTS = 3; // three times to try phone verification

	public static function computeBogusAccountId(int $classifiedId) {
		return 10000000 + $classifiedId;
	}

	public static function getAccountIdClassifiedId() {
		global $account;

		$accountId = $classifiedId = null;
		if (!account::isLoggedIn()) {
			//we are probably in the process of posting new classified ad without being logged in, get classified_id we are posting 
			$classifiedId = classifieds::get_cl_owner();
			if (!$classifiedId) {
				return false;
			}
			$accountId = self::computeBogusAccountId($classifiedId);
		} else {
			$accountId = $account->getId();
		}

		return [$accountId, $classifiedId];
	}

	/**
	 * Check if the phone is not validated
	 *
	 * Search in account table
	 * @param string $phone normalized phone
	 * @param int $account_id
	 * @return boolean
	 */
	public static function isVerified($phone) {
		global $db, $account;

		if (!account::isLoggedIn()) {
			//we are probably in the process of posting new classified ad without being logged in, get classified_id we are posting 
			$classified_id = classifieds::get_cl_owner();
			if (!$classified_id)
				return false;
			$verified = $db->single(
				"SELECT count(*) FROM phone_verification WHERE phone = ? AND classified_id = ? AND verified = 1 LIMIT 1", 
				[$phone, $classified_id]
				);
			return ($verified) ? true : false;
		}

		$account_id = $account->getId();

		// select from verified table (only normalized)
		$result = $db->single('SELECT verified FROM phone_verification WHERE phone =? AND account_id=? AND verified=1 LIMIT 1', array($phone, $account_id));
		if( (int)$result === 1 ) {
			return true;
		}
		// check account
		$account = account::findOneById($account_id);
		if ($account->getPhoneVerified() && $account->getNormalizedPhone() === $phone) {
			debug_log("isVerified: phone $phone verified from account table" );
			return true;
		}

		return false;
	}

	/**
	 * Check if validation process for the phone has started
	 *
	 * @param string $phone normalized phone
	 * @param int $account_id
	 * @return boolean true if code is active
	 */
	public static function isWaitingForCode($phone, $account_id ) {
		global $db;

		$timeout = time() - (60*self::SMS_VERIFICATION_TIMEOUT);

		$result = $db->single('
			SELECT verified FROM phone_verification
			WHERE phone =? AND account_id=?
			AND verified=0 AND created_dts > ?
			LIMIT 1
			', array($phone, $account_id, $timeout));

		if( (int)$result === 0 ) {
			debug_log("isWaitingForCode: phone $phone verification process is active" );
			return true;
		} else {
			debug_log("isWaitingForCode: phone $phone verification expired or not started, call startVerification(...)" );
			return false;
		}
	}

	/**
	 * Generate verification token, store it in table and send by SMS
	 * returns verification ID hash
	 *
	 * @param string $phone
	 * @param int	$accountId
	 *
	 * @return bool|string
	 */
	public static function startVerification($phone) {
		global $db, $account;

		$smsCode = self::createCode();
		$hash	= self::createHash($smsCode);

		$phoneVerificationRepository = new PhoneVerificationRepository();

		$now	 = time();
		$timeout = $now - (60 * self::SMS_VERIFICATION_TIMEOUT);

		file_log("phone_verification", "startVerification: phone={$phone}, smsCode={$smsCode}");

		$ret = self::getAccountIdClassifiedId();
		if (!$ret) {
			file_log("phone_verification", "startVerification: ERROR: Can't get account_id nor classified_id !");
			return false;
		}
		list($accountId, $classifiedId) = $ret;

		file_log("phone_verification", "startVerification: account_id={$accountId}, classified_id={$classifiedId}");

		$phoneVerificationRepository->deleteExpiredRecords($timeout);
		$phoneVerificationRepository->deleteAccountUnverifiedRecords($accountId, $classifiedId, $phone);
		$phoneVerificationRepository->insertAccountVerificationCode(
			$accountId,
			$classifiedId,
			$phone,
			$smsCode,
			self::MAX_ATTEMPTS,
			$now
		);

		$affected = $db->affected();

		file_log("phone_verification", "startVerification: Inserted {$affected} rows to phone_verification table");

		$sms = new Sms();

		$message = new Message();
		$message->setToNumber($phone);
		$message->setImportant(true);
		$message->setText("AS verification code: {$smsCode}");

		$success = $sms->send($message);

		if (!$success) {
			file_log("phone_verification", "startVerification: ERROR: SMS send failed !");
			return false;
		}

		return $hash;
	}

	/**
	 * Generate a number 4 characters long
	 *
	 * @return string
	 */
	public static function createCode() {
		global $config_env;

		if ($config_env == "dev"
			|| $config_env == "test"
			|| strpos($config_env, "uat") === 0
		) {
			file_log("phone_verification", "createCode: DEV-like environment ('{$config_env}') -> code is 1111");
			return "1111";
		}

		srand(time());
		return rand(1,9).rand(0,9).rand(0,9).rand(0,9);
	}

	/**
	 * Generate hash of SMS code
	 *
	 * @return string
	 */
	public static function createHash($code) {
		return md5(time().$code.'as');
	}

	/**
	 * Update verification.
	 *
	 * if validation successful returns true.
	 * If there's a problem returns false . Need to startVerification again.
	 * Otherwise returns a number between 0-3 - number of attempts left.
	 *
	 * @global \PDOConnect $db
	 * @param string $verify_token
	 * @param string $phone
	 * @param int $account_id
	 * @return mixed
	 */
	public static function updateVerification($verify_token, $phone) {
		global $db, $account;

		$now = time();
		//todo  add hash of verify token

		file_log("phone_verification", "updateVerification: verify_token='{$verify_token}', phone='{$phone}'");

		$ret = self::getAccountIdClassifiedId();
		if (!$ret) {
			file_log("phone_verification", "updateVerification: Error: can't get accountId and classifiedId");
			return false;
		}
		list($accountId, $classifiedId) = $ret;
		file_log("phone_verification", "updateVerification: accountId={$accountId}, classifiedId={$classifiedId}");

		$res = $db->q("
			SELECT verified, verified_dts, sms_code, attempts 
			FROM phone_verification 
			WHERE account_id = ? AND phone = ?
			", 
			[$accountId, $phone]
		);
		if ($db->numrows($res) != 1) {
			file_log("phone_verification", "updateVerification: Error: can't find verification accountId={$accountId}, classifiedId={$classifiedId}, phone={$phone}");
			return false;
		}

		$row = $db->r($res);
		$verified = $row["verified"];
		$sms_code = $row["sms_code"];
		$attempts = intval($row["attempts"]);

		if ($verified) {
			//this phone is already verified for this account
			file_log("phone_verification", "updateVerification: Error? Phone is already verified ?");
			return true;
		}

		if ($attempts < 1) {
			//we should have never gotten here, 0 attempts left
			file_log("phone_verification", "updateVerification: Failed, less than 1 attempt left");
			return false;
		}

		if ($verify_token == $sms_code) {
			//successful verification
			file_log("phone_verification", "updateVerification: Success");
			$res = $db->q("UPDATE phone_verification SET verified = 1, verified_dts = ? WHERE account_id = ? AND phone = ?", [$now, $accountId, $phone]);
			if ($db->affected($res) != 1) {
				reportAdmin("AS: phone:updateVerification error", "not affected - verified ? accountId='{$accountId}' phone='{$phone}'");
			}
			return true;
		}

		//bad try, decrement attempts
		$attemptsLeft = $attempts - 1;
		if ($attemptsLeft < 0)
			$attemptsLeft = 0;
		$res = $db->q("UPDATE phone_verification SET attempts = ? WHERE account_id = ? AND phone = ?", [$attemptsLeft, $accountId, $phone]);
		if ($db->affected($res) != 1) {
			reportAdmin("AS: phone:updateVerification error", "not affected - {$attemptsLeft} attempts left ? accountId='{$accountId}' phone='{$phone}'");
		}
		return $attemptsLeft;
	}

	/**
	 * Checks if the phone was used to verify another account
	 *
	 * @global \PDOConnect $db
	 * @param string $phone
	 * @param int $account_id
	 * @return bool
	 */
	public static function usedInAnotherAccount($phone) {
		global $db, $account;

		if (!account::isLoggedIn()) {
			//we might be in adbuild process, not yet logged in, then we always suppose phone is not used in another account
			return false;
		}
		
		$a1 = $db->single("SELECT account_id FROM phone_verification WHERE phone = ? AND account_id <> ? LIMIT 1", [$phone, $account->getId()]);
		if ($a1) {
			debug_log("Error: Phone number found in validation table for another account, phone={$phone}, another_account_id={$a1}");
		}

		// find phones validated in old format
		if( substr($phone, 0, 2) === '+1' ) {
			$phone1 = substr($phone,2);
		} else {
			$phone1 = $phone;
		}

		$a2 = $db->single("
			SELECT account_id FROM account
			WHERE account_id <> ? AND ( phone = ? OR phone = ? ) AND phone_verified = 1
			LIMIT 1",
			[$account->getId(), $phone1, $phone]);
		if ($a2) {
			debug_log("Error: Phone number found in accounts table for another account, phone={$phone}, another_account_id={$a2}");
		}

		return ($a1 || $a2);
	}

	/**
	 * validate phone number-like input string
	 * @param string $phone
	 * @return false if phone invalid, normalized phone if OK
	 */
	public static function checkPhoneNumber($phone, $country=null) {
		$errors = array();

		if (preg_match('/[^0-9\-\s\+\(\)]/',  $phone)) {
			debug_log("Phone number contains invalid characters " . escapeshellarg($phone));
			return false;
		}

		if (account::isImpersonatedAdmin()) {
			//if we are impersonated admin, allow admin to enter non-valid numbers (e.g. for sponsor ads with direct links)
			return $phone;
		}

		$phone_util = \libphonenumber\PhoneNumberUtil::getInstance();

		$phone_valid = false;
		try {
			$num = $phone_util->parse($phone, $country);
			$phone_valid = $phone_util->isValidNumber($num);
		} catch (\libphonenumber\NumberParseException $e) {
			debug_log($e->getMessage());
			$phone_valid = false;
		}

		if ($phone_valid) {
			// phone is valid
			return $phone_util->format($num, \libphonenumber\PhoneNumberFormat::E164);
		} else {
			debug_log("Phone number is invalid: " . escapeshellarg($phone));
			return false;
		}
	}

	/**
	 * Clean phone number-like input string
	 * @param string $phone
	 * @return string
	 */
	public static function normalizePhoneNumber($phone) {
		return preg_replace('/[^0-9\+]/', '', $phone);
	}

	public static function updateVerificationForClassified($clad) {
		global $db;

		$now = time();
		$bogusAccountId = self::computeBogusAccountId($clad->getId());
		$accountId = $clad->getAccountId();
		$cladId = $clad->getId();
		$phone = $clad->getPhone();
		$updated = false;

		file_log("phone_verification", "updateVerificationForClassified: clad_id={$cladId}, phone={$phone}, bogus_account_id={$bogusAccountId}, real_account_id={$accountId}");

		$res = $db->q("
			SELECT created_dts 
			FROM phone_verification 
			WHERE account_id = ? AND classified_id = ? AND phone = ? AND verified = 1", 
			[$bogusAccountId, $cladId, $phone]
			);
		while ($row = $db->r($res)) {
			//check if this account has already verified this phone
			$res2 = $db->q("SELECT verified FROM phone_verification WHERE account_id = ? AND phone = ?",
				[$accountId, $phone]
				);
			if ($db->numrows($res2)) {
				$row2 = $db->r($res2);
				$verified = $row2["verified"];
				if (!$verified) {
					$res2 = $db->q(
						"UPDATE phone_verification SET verified = 1, verified_dts = ? WHERE account_id = ? AND phone = ?", 
						[$now, $accountId, $phone]
						);
				}
				$res3 = $db->q("DELETE FROM phone_verification WHERE account_id = ? AND phone = ?", [$bogusAccountId, $phone]);
				if ($db->affected($res2) != 1) {
					file_log("phone_verification", "updateVerificationForClassified: ERROR deleting bogus account_id verification entry: clad_id={$cladId}, phone={$phone}, bogus_account_id={$bogusAccountId}, real_account_id={$accountId} !");
				}
			} else {
				//phone was not verified before for this account, lets change bogus account id to real account id
				$res2 = $db->q("
					UPDATE phone_verification 
					SET account_id = ?, classified_id = NULL 
					WHERE account_id = ? AND phone = ?
					",
					[$accountId, $bogusAccountId, $phone]
					);
				if ($db->affected($res2) == 1) {
					file_log("phone_verification", "updateVerificationForClassified: Successfully updated bogus account_id to real account_id: clad_id={$cladId}, phone={$phone}, bogus_account_id={$bogusAccountId}, real_account_id={$accountId}");
				} else {
					file_log("phone_verification", "updateVerificationForClassified: ERROR updating bogus account_id to real account_id: clad_id={$cladId}, phone={$phone}, bogus_account_id={$bogusAccountId}, real_account_id={$accountId} !");
				}
			}
			$updated = true;
		}

		if ($updated) {
			//we have successfully updated verification for this classified
			//let's delete other verifications for this ad (unverified or other phone numbers)
			$res = $db->q(
				"DELETE FROM phone_verification WHERE classified_id = ? AND (phone <> ? OR verified = 0)", 
				[$cladId, $phone]
				);
		}

		return true;
	}
}
