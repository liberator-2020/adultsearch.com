<?php

/*
 * post = forum post
 */
class post {

	private $id = NULL,
			$topic_id = NULL,
			$account_id = NULL,
			$posted = NULL,
			$forum_id = NULL,
			$loc_id = NULL,
			$post_edited = NULL,
			$post_lastedit_date = NULL,
			$post_edit_count = NULL,
			$post_edit_reason = NULL,
			$post_text = NULL;

	private $topic = NULL;
	private $account = NULL;
	private $location = NULL;

	function __construct() {
	}

	public static function findOneById($id) {
		global $db;

		$res = $db->q("SELECT * FROM forum_post WHERE post_id = '".intval($id)."'");
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

		return self::withRow($row);
	}
	
	public static function withRow($row) {

		$post = new post();
		$post->setId($row["post_id"]);
		$post->setTopicId($row["topic_id"]);
		$post->setAccountId($row["account_id"]);
		$post->setPosted($row["posted"]);
		$post->setForumId($row["forum_id"]);
		$post->setLocId($row["loc_id"]);
		$post->setPostEdited($row["post_edited"]);
		$post->setPostLasteditDate($row["post_lastedit_date"]);
		$post->setPostEditCount($row["post_edit_count"]);
		$post->setPostEditReason($row["post_edit_reason"]);
		$post->setPostText($row["post_text"]);

		return $post;
	}

	//getters and setters
	public function getId() {
		return $this->id;
	}
	public function getTopicId() {
		return $this->topic_id;
	}
	public function getAccountId() {
		return $this->account_id;
	}
	public function getPosted() {
		return $this->posted;
	}
	public function getForumId() {
		return $this->forum_id;
	}
	public function getLocId() {
		return $this->loc_id;
	}
	public function getPostEdited() {
		return $this->post_edited;
	}
	public function getPostLasteditDate() {
		return $this->post_lastedit_date;
	}
	public function getPostEditCount() {
		return $this->post_edit_count;
	}
	public function getPostEditReason() {
		return $this->post_edit_reason;
	}
	public function getPostText() {
		return $this->post_text;
	}

	public function setId($id) {
		$this->id = $id;
	}
	public function setTopicId($topic_id) {
		$this->topic_id = $topic_id;
	}
	public function setAccountId($account_id) {
		$this->account_id = $account_id;
	}
	public function setPosted($posted) {
		$this->posted = $posted;
	}
	public function setForumId($forum_id) {
		$this->forum_id = $forum_id;
	}
	public function setLocId($loc_id) {
		$this->loc_id = $loc_id;
	}
	public function setPostEdited($post_edited) {
		$this->post_edited = $post_edited;
	}
	public function setPostLasteditDate($post_lastedit_date) {
		$this->post_lastedit_date = $post_lastedit_date;
	}
	public function setPostEditCount($post_edit_count) {
		$this->post_edit_count = $post_edit_count;
	}
	public function setPostEditReason($post_edit_reason) {
		$this->post_edit_reason = $post_edit_reason;
	}
	public function setPostText($post_text) {
		$this->post_text = $post_text;
	}

	//relationships
	public function getAccount() {
		if ($this->account)
			return $this->account;
		$acc = account::findOneById($this->account_id);
		if (!$acc)
			return NULL;
		$this->account = $acc;
		return $this->account;
	}

	public function getTopic() {
		if ($this->topic)
			return $this->topic;
		$top = topic::findOneById($this->topic_id);
		if (!$top)
			return NULL;
		$this->topic = $top;
		return $this->topic;
	}	

	public function getLocation() {
		global $db;

		if ($this->location)
			return $this->location;

		//first check county
		$res = $db->q("SELECT * from location_location WHERE county_id = ? ORDER BY significant DESC, has_place_or_ad DESC LIMIT 1", array($this->loc_id));
		if ($db->numrows($res) != 1) {
			$loc = location::findOneById($this->loc_id);
			if (!$loc)
				return NULL;
		} else {
			$row = $db->r($res);
			$loc = location::withRow($row);
		}
		$this->location = $loc;
		return $this->location;
	}	

	//CRUD
	public function add() {
		global $db, $account;

		//some default values
		$now = time();
		$this->setPosted($now);

		//first store into our db
		$res = $db->q("INSERT INTO forum_post 
						(topic_id, account_id, posted, forum_id, loc_id, post_text) 
						VALUES
						(?, ?, ?, ?, ?, ?);",
						array($this->topic_id, $this->account_id, $this->posted, $this->forum_id, $this->loc_id, $this->post_text));
		$newid = $db->insertid($res);
		if ($newid == 0)
			return false;
		$this->id = $newid;

		//audit::log("FOP", "New", $this->id, "", $account->getId());

		forum::updateStats($this->forum_id, $this->loc_id);
		//burak's integrity updates
		//TODO fix these
		$db->q("UPDATE account 
				SET forum_post = forum_post + 1 
				WHERE account_id = ?",
				array($this->account_id));

		//burak's spam fighting
		if (strstr($this->post_text, "www.") || strstr($this->post_text, "http://"))
			$_SESSION["forumspam"] = $_SESSION["forumspam"] ? $_SESSION["forumspam"]++ : 1;

		//if this post is not being posted by adimn, send admin eamil notification
		if (!$account->isadmin())
			$this->emailAdminNotify();

		return true;
	}

	//update function
	public function update() {
		global $account, $db;

		if (!$this->id)
			return false;

		$original = post::findOneById($this->id);
		if (!$original)
			return false;

		$update_fields = array();
		if ($this->topic_id != $original->getTopicId())
			$update_fields["topic_id"] = $this->topic_id;
		if ($this->account_id != $original->getAccountId())
			$update_fields["account_id"] = $this->account_id;
		if ($this->posted != $original->getPosted())
			$update_fields["posted"] = $this->posted;
		if ($this->forum_id != $original->getForumId())
			$update_fields["forum_id"] = $this->forum_id;
		if ($this->loc_id != $original->getLocId())
			$update_fields["loc_id"] = $this->loc_id;
		if ($this->post_edited != $original->getPostEdited())
			$update_fields["post_edited"] = $this->post_edited;
		if ($this->post_lastedit_date != $original->getPostLasteditDate())
			$update_fields["post_lastedit_date"] = $this->post_lastedit_date;
		if ($this->post_edit_count != $original->getPostEditCount())
			$update_fields["post_edit_count"] = $this->post_edit_count;
		if ($this->post_edit_reason != $original->getPostEditReason())
			$update_fields["post_edit_reason"] = $this->post_edit_reason;
		if ($this->post_text != $original->getPostText())
			$update_fields["post_text"] = $this->post_text;

		$changed_fields = "";
		$update = "";
		$update_arr = array();
		foreach ($update_fields as $key => $val) {
			$changed_fields .= (empty($changed_fields)) ? "" : ", ";
			$update .= (empty($update)) ? "SET " : ", ";
			$changed_fields .= $key;
			$update .= "{$key} = ?";
			$update_arr[] = $val;
		}
		$update_arr[] = $this->id;
		$update = "UPDATE forum_post {$update} WHERE post_id = ? LIMIT 1";

		$db->q($update, $update_arr);
		$aff = $db->affected();

		if (!$aff)
			return false;

		//audit::log("FOP", "Edit", $this->id, "Changed fields: {$changed_fields}", $account->getId());
		return true;
	}


	//additional functions
	public function remove() {
		global $account, $db;

		//check permissions
		if (!$account->isadmin() && $this->getAccountId() != $account->getId())
			return false;

		$topic_id = $this->getTopicId();

		$db->q("DELETE FROM forum_post WHERE post_id = ?", array($this->id));
		$aff = $db->affected();

		//explicitely check if topic has other posts now after deleting
		$res = $db->q("SELECT p.post_id, p.posted FROM forum_post p WHERE p.topic_id = ? ORDER BY p.post_id DESC", array($topic_id));
		$num = $db->numrows($res);
		if ($num == 0) {
			//topic has no other posts, delete whole topic
			$db->q("DELETE FROM forum_watch WHERE topic_id = ?", array($topic_id));
	        $db->q("DELETE FROM forum_topic WHERE topic_id = ? LIMIT 1", array($topic_id));
		}

		forum::updateStats($this->forum_id, $this->loc_id);

		if ($aff > 0)
			return true;
		return false;
	}

	public function emailAdminNotify($email = NULL) {
		global $db;

		if (!$this->getTopic())
			return false;		//can't get topic ? this should not happen
		if (!$this->getAccount())
			return false;		//can't get account ? this should not happen
		if (!$this->getLocation())
			return false;		//can't get location ? this should not happen

		$location = $this->getLocation();
		$topic = $this->getTopic();

		$topic_link = $topic->getUrl();
		$delete_post_link = $location->getUrl()."/sex-forum/delpost/{$this->id}";

		$smarty = GetSmartyInstance();
		$smarty->assign("topic_title", $this->getTopic()->getTitle());
		$smarty->assign("topic_link", $topic_link);
		$smarty->assign("username", $this->getAccount()->getUsername());
		$smarty->assign("post_text", $this->getPostText());
		$smarty->assign("delete_post_link", $delete_post_link);

		$html = $smarty->fetch(_CMS_ABS_PATH."/templates/sex-forum/email/reply_admin_notify.html.tpl");
		$text = $smarty->fetch(_CMS_ABS_PATH."/templates/sex-forum/email/reply_admin_notify.txt.tpl");

		if (!$email) {
			$email = "dallas@adultsearch.com";
			//$email = ADMIN_EMAIL;
		}

		return send_email(array(
			"from" => "support@adultsearch.com",
			"to" => $email,
//			"bcc" => ADMIN_EMAIL,
			"subject" => "Forum New Reply",
			"html" => $html,
			"text" => $text,
			));
	}
}

//END
