<?php

use SecurionPay\Connection\Connection;
use SecurionPay\Exception\ConnectionException;
use GuzzleHttp\Client as GuzzleClient;

/**
 * Implementation of SecurionPay\Connection class, that forwards Secuionpay via proxy machine
 */
class ProxyConnection extends Connection {

    public function __construct() {
    }

    public function get($url, $headers) {
        return $this->httpRequest('GET', $url, $headers);
    }

    public function post($url, $requestBody, $headers) {
        return $this->httpRequest('POST', $url, $headers, $requestBody);
    }

	public function delete($url, $headers) {
        return $this->httpRequest('DELETE', $url, $headers);
    }

    private function httpRequest($httpMethod, $url, $headers = array(), $requestBody = null) {
		global $config_securionpay_proxy_url;

        $params = [
            "client" => "adultsearch",
            "password" => "fh93j90kd3",
            "httpMethod" => $httpMethod,
            "url" => $url,
            "headers" => json_encode($headers),
            "requestBody" => $requestBody,
            ];

        file_log("proxy_connection", "httpRequest: url='{$config_securionpay_proxy_url}', params=".print_r($params, true));

        $client = new GuzzleClient([
            "base_uri" => $config_securionpay_proxy_url,
            "timeout" => 20.0,
        ]);

        try {
            $response = $client->request('POST', $config_securionpay_proxy_url, [
                "form_params" => $params,
                "allow_redirects" => true
                ]);
        } catch (\Exception $e) {
            $code = $e->getCode();
            $reason = $e->getMessage();
            file_log("proxy_connection", "httpRequest: Error: Forward failed: code={$code}, reason={$reason}");
            throw new ConnectionException("{$code}: {$reason}");
        }

        $response_body = (string)$response->getBody();
        file_log("proxy_connection", "httpRequest: response='{$response_body}'");
		$response_arr = json_decode($response_body, true);

        if ($response_arr["result"] != "success") {
            file_log("proxy_connection", "httpRequest: Proxy Failed: '{$response_arr["reason"]}'");
            throw new ConnectionException($response_arr["reason"]);
        }

        file_log("proxy_connection", "httpRequest: Proxy Success");
        return $response_arr;
    }

}

//END
