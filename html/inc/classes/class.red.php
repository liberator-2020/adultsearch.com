<?php

/**
 * Wrapper for redis caching system (https://redis.io/)
 *
 * Uses phpredis php extension (https://github.com/phpredis/phpredis)
 * This extension needs to be installed: https://github.com/phpredis/phpredis/blob/develop/INSTALL.markdown
 */
class red {

	var $redis_host = null;
	var $redis_port = null;
	var $redis = null;

	public function __construct($redis_host = null, $redis_port = null) {
		global $config_redis_host, $config_redis_port;
		if (is_null($redis_host))
			$this->redis_host = $config_redis_host;
		else
			$this->redis_host = $redis_host;
		if (is_null($redis_port))
			$this->redis_port = $config_redis_port;
		else
			$this->redis_port = $redis_port;
	}

	public function getClient() {

		if (!is_null($this->redis))
			return $this->redis;

		try {
			$this->redis = new Redis();
			$this->redis->connect($this->redis_host, $this->redis_port, 3); // 3 sec timeout
		} catch(\Exception $e) {
			error_log("redis connect exception: ".$e->getMessage()." ".$e->getCode()." file=".$e->getFile()." line=".$e->GetLine()." trace=".$e->getTraceAsString());
			return false;
		}

		return $this->redis;
	}
	
	public function set($key, $value) {

		$redis = $this->getClient();
		if (!$redis)
			return false;

		try {
			$ret = $redis->set($key, $value);
		} catch(\Exception $e) {
            return false;
        }

		return ($ret == "OK") ? true : false;
	}

	public function get($key) {

		$redis = $this->getClient();
		if (!$redis)
			return false;

		try {
			$ret = $redis->get($key);
		} catch(\Exception $e) {
            return false;
        }

		return $ret;
	}

	public function incr($key) {

		$redis = $this->getClient();
		if (!$redis)
			return false;

		try {
			$ret = $redis->incr($key);
		} catch(\Exception $e) {
            return false;
        }

		return $ret;
	}

	public function decrBy($key, $decrement) {

		$redis = $this->getClient();
		if (!$redis)
			return false;

		try {
			$ret = $redis->decrBy($key, $decrement);
		} catch(\Exception $e) {
            return false;
        }

		return $ret;
	}

}

