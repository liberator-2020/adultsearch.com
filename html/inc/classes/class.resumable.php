<?php
/**
 * resumable upload widget functions
 * Class 
 * @author jay
 */
class resumable {

	public static $file_param_name = 'file';

	public static function getHtml() {

		video::storeVideoInfoToSession();
	
		$html = "";
		
$html .= <<<END
<script src="/js/resumable.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
jQuery(document).ready(function() {
	var r = new Resumable({
		target: '/resumable/uploadHandler', 
	});
	
	if(!r.support) location.href = '?uploader=flash';	// Resumable.js isn't supported, fall back on a different method
  
	r.assignBrowse(document.getElementById('upl_vid_sel_browse'));
	r.assignDrop(document.getElementById('content'));

	r.on('fileAdded', function(file) {
		upload_video_resumable_add(file);
	});
	r.on('fileProgress', function(file) {
		upload_video_resumable_file_progress(file);
	});
	r.on('fileSuccess', function(file, message) {
		upload_video_resumable_ok(file, message);
	});
	r.on('fileError', function(file, message) {
		upload_video_resumable_error(file, message);
	});
	
});

function upload_video_resumable_add(file) {
	$('div#upl_vid_sel_wrap').hide();
	$('div#upl_vid_upl_progress_name').html(file.fileName);
	$('div#upl_vid_upl_wrap').show();
	file.resumableObj.upload();
}
function upload_video_resumable_file_progress(file) {
	var progress_decimal = file.resumableObj.progress();
	var progress_percentage = Math.round(progress_decimal * 100);
	//console.log('total progress = ' + total_progress);
	$('div#upl_vid_upl_wrap progress').val(progress_percentage);
	$('div#upl_vid_upl_wrap span#text_progress').html(progress_percentage + '%');
}
function upload_video_resumable_ok(file, message) {
	//console.log('upload finished.');
	$("div#upl_vid_upl_progress img").attr("src","/images/upload_video_done.png");
	$('span#upl_vid_upl_msg').html('Upload finished.');
	_ajax('post', '/files/uploadVideoResult', '', 'upl_vid_result');
}
function upload_video_resumable_error(file, message) {
	//console.log('error : ' + message);
	$('div#upl_vid_error .error_detail').html(error_msg);
	$('div#upl_vid_error').show();
	//_ajaxs('post', '/files/errorUploadVideo', debug_info);
}
</script>
END;
//TODO comment up

$html .= <<<END
<style type="text/css">
div#upl_vid_sel_wrap {
	width: 600px;
	text-align: center;
	margin: auto;
}
div#upl_vid_sel_wrap * {
	margin: auto;
}
div#upl_vid_sel_browse {
	width: 300px;
	height: 200px;
	margin: auto;
	margin-bottom: 10px;
	border: 1px solid white;
	padding: 5px;
}
div#upl_vid_sel_browse:hover {
	border: 1px solid #ddd;
	background-color: #f5f5f5;
}
div#upl_vid_sel_browse_button {
	width: 250px;
	margin: auto;
	border: 1px solid #bbb;
	background-color: #ddd;
	padding: 5px;
	font-size: 1.2em;
}
div#upl_vid_sel_wrap span {
	display: block;
	font-weight: bold;
	font-size: 1.4em;
}
div#upl_vid_sel_dd_msg {
	margin-top: 10px;
	border: 3px dashed #0B4599;
	width: 90%;
	padding: 20px;
	font-size: 1.2em;
}
div#upl_vid_sel_dd_msg:hover {
	background-color: #f5f5f5;
}

div#upl_vid_upl_wrap {
	width: 600px;
	text-align: center;
	margin: auto;
}
div#upl_vid_upl_wrap * {
	margin: auto;
}
div#upl_vid_upl_progress {
	width: 300px;
	margin: auto;
	margin-bottom: 20px;
	border: 1px solid #ccc;
	padding: 5px;
	font-size: 1.2em;
}
div#upl_vid_upl_progress_name {
	font-weight: bold;
}
div#upl_vid_upl_progress progress {
	width: 220px;
}
div#upl_vid_upl_wrap span#text_progress {
	display: inline-block;
	width: 60px;
	font-weight: bold;
	color: #48BC24;
	text-align: center;
}

div#upl_vid_error {
	display: none;
	border: 2px solid red;
	padding: 10px;
	font-size: 1.2em;
}
div#upl_vid_error .error_detail {
	font-size: 0.8em;
}

div#upl_vid_result {
	width: 800px;
	margin: auto;
}

</style>
	
<div id="upl_vid_sel_wrap">
<div id="upl_vid_sel_browse">
<img src="/images/upload_video.png" />
<div id="upl_vid_sel_browse_button">Select files from your computer</div>
</div>
<span>OR</span>
<div id="upl_vid_sel_dd_msg">Drag and drop video files anywhere on this page</div>
<br />
</div>

<div id="upl_vid_upl_wrap" style="display: none;">
<div id="upl_vid_upl_progress">
<div id="upl_vid_upl_progress_name"></div><br />
<img src="/images/ajax_loader_64.gif" /><br />
<span id="upl_vid_upl_msg">Uploading in progress.<br />Please wait.<br /></span>
<progress max="100" value="0"></progress><span id="text_progress">0%<span>
</div>
</div>

<div id="upl_vid_error"><strong>Video upload has failed !</strong><br /><span class="error_detail"></span><br /><br />Please contact us at <a href="mailto:support@adultsearch.com">support@adultsearch.com</a></div>

<div id="upl_vid_result"></div>

END;

		return $html;
	}

	private static function haveFilePart($id, $filename, $part_no, $chunk_size) {
		$file_part_path = video::getStageDir().$id."/".$filename.".part".$part_no;
		debug_log("UP resumable: haveFilePart($id, $filename, $part_no, $chunk_size): file_part_path='$file_part_path'");
		if (!file_exists($file_part_path)) {
			debug_log("UP resumable: Error: file part '$file_part_path' not found");
			return false;
		}
		if (!is_file($file_part_path)) {
			debug_log("UP resumable: Error: file part '$file_part_path' is directory ???");
			return false;
		}
		if (filesize($file_part_path) != $chunk_size) {
			debug_log("UP resumable: have file part, but size differs: path='$file_part_path' chunk_size='$chunk_size' real_size='".filesize($file_part_path)."'");
			unlink($file_part_path);
			return false;
		}
		return true;
	}

	private static function chunkTest() {
		if (self::haveFilePart(GetGetParam("resumableIdentifier"), GetGetParam("resumableFilename"), GetGetParam("resumableChunkNumber"), GetGetParam("resumableChunkSize"))) {
			header('HTTP/1.1 200 OK');
		} else {
			header('HTTP/1.1 404 Not Found');
		}
		header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Pragma: no-cache');
		die;
	}

	public static function uploadHandler() {
		debug_log('UP resumable');
		debug_log("GET=".print_r($_GET,true)." POST=".print_r($_POST,true)." FILES=".print_r($_FILES,true));

		if (isset($_GET["resumableFilename"]))
			self::chunkTest();

		if (empty($_FILES))
			return;
		
		foreach ($_FILES as $file) {
			// check the error status
			if ($file['error'] != 0) {
				debug_log('UP resumable error '.$file['error'].' in file '.$_POST['resumableFilename']);
				continue;
			}

			//temp dir is $TMP_FOLDER/identifier
			$temp_dir = video::getStageDir().$_POST['resumableIdentifier'];
			// init the destination file (format <filename.ext>.part<#chunk>
			$dest_file = $temp_dir.'/'.$_POST['resumableFilename'].'.part'.$_POST['resumableChunkNumber'];

			// create the temporary directory
			@mkdir($temp_dir, 0777, true);

			// move the temporary file
			if (!@move_uploaded_file($file['tmp_name'], $dest_file)) {
				debug_log('UP resumable: Error saving (move_uploaded_file) chunk '.$_POST['resumableChunkNumber'].' for file '.$_POST['resumableFilename'].'. src=\''.$file['tmp_name'].'\' dst=\''.$dest_file.'\'');
			} else {
				// check if all the parts present, and create the final destination file
				self::createFileFromChunks($temp_dir, $_POST['resumableFilename'], $_POST['resumableTotalSize']);
			}
		}
	}

	private static function createFileFromChunks($temp_dir, $filename, $total_size) {
		//debug_log("UP resumable: createFileFromChunks($temp_dir, $filename, $total_size)");
		
		// count all the parts of this file
		$total_files = 0;
		$parts_size = 0;
		foreach(scandir($temp_dir) as $file) {
			//debug_log("UP resumable: file={$file} total_files=$total_files");
			if (stripos($file, $filename) !== false) {
				$total_files++;
				$parts_size += filesize($temp_dir."/".$file);
			}
		}

		//debug_log("UP resumable: total_files={$total_files} parts_size={$parts_size} total_size={$total_size}");

		// check that all the parts are present
		if ($parts_size !=  $total_size)
			return;

		// create the final destination file
		
		if ($_SESSION["upl_vid_module"] == "place") {	
			$dir = $_SESSION["upl_vid_country"];
		} else {
			$dir = $_SESSION["upl_vid_module"];
		}

		$dest_filename = video::getUniqueFilename($dir, $filename);
		$dest_filepath = video::getDestFilePath($dir, $dest_filename);
		if ($dest_filepath === false) {
			debug_log('UP resumable: Error: cannot create the destination folder.');
						return false;
		}
		debug_log('UP resumable: Creating final file at \''.$dest_filepath.'\'');
		if (($fp = @fopen($dest_filepath, 'w')) !== false) {
			for ($i=1; $i<=$total_files; $i++) {
				@fwrite($fp, file_get_contents($temp_dir.'/'.$filename.'.part'.$i));
				debug_log('UP resumable: writing chunk '.$i);
			}
			@fclose($f);
		} else {
			debug_log('UP resumable: Error: cannot create the destination file');
			return false;
		}

		// rename the temporary directory (to avoid access from other 
		// concurrent chunks uploads) and than delete it
		if (@rename($temp_dir, $temp_dir.'_UNUSED')) {
			video::rrmdir($temp_dir.'_UNUSED');
		} else {
			video::rrmdir($temp_dir);
		}

		//store data about successful upload to session
		$_SESSION["upl_vid_filename"] = $dest_filename;
		$_SESSION["upl_vid_filepath"] = $dest_filepath;

		//remove old files from stage directory
				video::removeOldStageFiles();
	}

}

//END
