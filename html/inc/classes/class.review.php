<?php

class review {
	var $row, $id, $name, $title, $link, $account_id, $review_id, $edit = false, $module = NULL;
	var $spam_protection = false, $donot_allow_url = false;
	var $reviewwhat = NULL; // to change the word on the review page.. default is 'place'

	/* provider stuff */
	var $provider = false,
		$provider_name_question = "Providers Name";

	var $mp_types = array(
		"1" => "Store Front", 
		"2"=>"Apartment", 
		"3"=>"House", 
		"4"=>"Outcall"
		);

	var $lineupArray = array("1" => "Yes", "2" => "No");

	var $ageArray = array("1"=>"18-20", "2"=>"21-24", "3"=>"25-29", "4"=>"30-37", "5"=>"38-49", "6"=>"50+");
	var $buildArray = array("1"=>"Petite/Tiny", "2"=>"Slim/Slender", "3"=>"Athletic","4"=>"Average", "5"=>"Curvy",  "6"=>"Whale", "7"=>"Chubby");
	var $heightArray = array("1"=>"Less than 5'1''", "2"=>"5'1'' to 5'3''", "3"=>"5'4'' to 5'6''","4"=>"5'7'' to 5'9''", "5" => "5'10'' to 5'11''", "6"=>"6' and up");
	var $hairlengthArray = array("Short" => "Short", "Shoulder Length" => "Shoulder", "Midback"=>"Midback", "Super Long"=>"Super Long");
	var $cupArray = array("1"=>"A", "2"=>"B", "3"=>"C", "4"=>"D", "5"=>"Unknown");
	var $assArray = array("1"=>"Nice and Round", "2"=>"Flat", "3"=>"Big");

	var $sexArray = array("1"=>"Yes", "3"=>"Yes, w/out Condom", "0"=>"No", "2"=>"Didn't Ask");
	var $lickArray = array("1"=>"Yes", "0"=>"No", "2"=>"Didn't Ask");
	var $kissArray = array("1"=>"Yes, no tongue", "2"=>"Yes, with tongue", "0"=>"No", "3"=>"Didn't Ask");
	var $handjobArray = array("1"=>"Yes", "2"=>"Yes, w/condom", "3"=>"Yes, w/latex glove", "4"=>"Cum on body", "0"=>"No", "5"=>"Didn't Ask");
	var $fingerArray = array("1"=>"Outside only", "2"=>"Yes, inside", "0"=>"No", "3"=>"Didn't Ask");
	var $blowjobArray = array("1"=>"Yes, w/condom", "2"=>"Yes, swallows", "3"=>"Cum in mouth &amp; spits out", "4"=>"Yes, allows facial", "5"=>"Yes, cum on body", "7"=>" W/O Condom but covers for sex", "0"=>"No", "6"=>"Didn't Ask");
	var $breastplayArray = array("1"=>"Didn't Ask", "6"=>"No", "2"=>"Over Clothes", "3"=>"Quick Feel", "5"=>"Feel under Clothes", "4"=>"Full Contact");
	var $assplayArray = array("1"=>"Outside Only", "2"=>"Yes, finger the inside", "3"=>"Didn't Ask", "4"=>"No");

	var $tattoosArray = array("0"=>"No","3"=>"A few", "4"=> "A lot", "2"=>"Unknown");

	public function __construct($table, $id, $title, $link, $module = '', $acc_id = NULL, $review_id = NULL) {
		global $account;

		if (!$acc_id)
			$this->account_id = $account->isloggedin();
		else
			$this->account_id = $acc_id;
		
		if ($review_id)
			$this->review_id = $review_id;

		$this->id = $id;
		$this->title = $title;
		$this->link = $link;
		if (empty($module)) {
			echo "Error: not module specified for review constructor!<br />\n";
			die;
		}
		$this->module = $module;

		if( !in_array($module, array('adultwebsite', 'adultstore')) ) {
			switch($module) {
				case 'stripclub':
					$this->provider_name_question = "Strippers Name";
			}
			$this->provider = true;
		}

		if( isset($_REQUEST["reviewcomment"]) ) {
			$this->comment();
			die;
		}

		if( isset($_REQUEST["commentremove"]) ) {
			$this->commentremove();
			die;
		}

	}

	public static function findOneById($id) {
		global $db;
		
		$res = $db->q("SELECT * FROM place_review WHERE review_id = ?", array($id));
		if ($db->numrows($res) != 1)
			return false;
		$row = $db->r($res);

		$link = "";
		switch ($row["module"]) {
			case "adultstore": $link = "/sex-shops/store"; break;
			case "sexclub": $link = "/sex-clubs/club"; break;
			case "lingeriemodeling1on1": $link = "/lingerie-modeling-1on1/place"; break;
			case "stripclub": $link = "/strip-clubs/club"; break;
			case "brothel": $link = "/brothels/club"; break;
			case "lifestyle": $link = "/lifestyle/club"; break;
			case "gay": $link = "/gay/place"; break;
			case "gaybath": $link = "/gay-bath-houses/place"; break;
			case "eroticmassage": $link = "/erotic-massage/parlor"; break;
			case "place":
			default:
				$link = "/dir/place"; break;
		}
		$rev = new review("place", $row["id"], "", $link, "place", $row["account_id"], $id);

		return $rev;	
	}

	public function getAccountId() {
		return $this->account_id;
	}

	/*
	 * removes review with comments from database
	 */
	public function remove($quiet = false) {
		global $db;

		//remove comments
		$sql = "DELETE FROM place_review_comment WHERE review_id = ?";
		$db->q($sql, array($this->review_id));
		$comments_deleted = $db->affected();
		
		//remove review
		$sql = "DELETE FROM place_review WHERE review_id = ?";
		$db->q($sql, array($this->review_id));
		$reviews_deleted = $db->affected();

		if ($reviews_deleted == 1) {
			if (!$quiet)
				flash::add(flash::MSG_SUCCESS, "Successfully deleted review id #{$this->review_id}, deleted also {$comments_deleted} comments.");
			return true;
		} else {
			if (!$quiet)
				flash::add(flash::MSG_ERROR, "Error: Review id #{$this->review_id} not deleted (affected={$reviews_deleted}), deleted also {$comments_deleted} comments.");
			return false;
		}
	}

	function process() {
		global $db, $account, $smarty, $type_array;

		$form = new form();

		/* i guess we have to make them register first in order to fight with the spam rather then giving option to register while leaving the review...*/
		if( !$this->account_id || !$account->isMember() ) {
			$account->asklogin();
			return false;
		} 

		if( is_array($type_array) ) {
			foreach($type_array as $t) {
				if( stristr($t, "erotic massage") ) {
					$this->provider = true; break;
				}
			}
		}

		$blocked = $this->isblocked();

		if(isset($_REQUEST["edit"]) && $this->account_id ) {
			$this->review_id = intval($_REQUEST["edit"]);

			$sql = "SELECT r.*, p.name
					FROM place_review r 
					INNER JOIN place p ON p.place_id = r.id
					WHERE r.review_id = ?";
			$res = $db->q($sql, [$this->review_id]); 

			if( $db->numrows($res) ) {
				$this->edit=true;
				$smarty->assign("edit", $this->review_id);
				$this->row=$db->r($res);

				if (isset($_GET["remove"]) && permission::has("edit_all_places")) {
					$ret = $this->remove();
					if ($ret) {
						$sql = "UPDATE place SET recommend = recommend - ?, review = review - 1, overall = recommend/review, 
						last_review_id = (select review_id from place_review where id = ? and module = ? order by review_id desc limit 1) 
						WHERE place_id = ?";
						$db->q($sql, [$this->row["recommend"], $this->row["id"], $this->module, $this->row["id"]]);
					}
					system::moved($this->getPlaceLink());
				}

				if (!permission::has("edit_all_places") && $this->account_id != $this->row["account_id"] ) {
					system::moved($this->getPlaceLink());
				}
			} else {
				reportAdmin("AS: Hack");
				system::moved("/");
			}
		} else if( isset($_GET["makeforum"]) && intval($_GET["makeforum"]) && $account->isadmin() ) {
			$this->review_id = intval($_REQUEST["makeforum"]);

			global $ctx;
			$loc_sql = $ctx->international ? "p.loc_id" : "p.state_id";
			$res = $db->q("SELECT r.*, p.name, $loc_sql
							FROM place_review r
							INNER JOIN place p ON p.place_id = r.id
							WHERE r.review_id = ? and r.module = ?",
							[$this->review_id, $this->module]);
			if( !$db->numrows($res) )
				system::moved("/");
			$this->row = $db->r($res);
			$smarty->assign("forummove", true);
			if (isset($_POST["topic_title"]))
				$smarty->assign("topic_title", $_POST["topic_title"]);
		} else if( isset($_GET["move"]) && intval($_GET["move"]) && $account->isadmin() ) {
			global $ctx;

			$this->review_id = intval($_REQUEST["move"]);
			$res = $db->q("SELECT r.*, p.name, p.loc_id
							FROM place_review r
							INNER JOIN place p ON p.place_id = r.id
							WHERE r.review_id = ? and r.module = ?",
							[$this->review_id, $this->module]);
			if( !$db->numrows($res) )
				system::moved("/");
			$this->row = $db->r($res);
			$smarty->assign("move", true);

			if ($ctx->node_id) {
				$id = $ctx->node_id;
			} else { 
				$id = intval($_GET['id']);
			}

			$rex = $db->q("select place_id, name from place where place_id != ? and loc_id = (select loc_id from place where place_id = ?) order by name",
				[$id, $id]);
			while($rox = $db->r($rex)) {
				$new_place_list[] = array($rox['place_id']=>"{$rox['name']} - #{$rox['place_id']}");
			}

			$npl = $form->select_add_select() . $form->select_add_array($new_place_list);
			$smarty->assign("new_place_list", $npl);

		} else {
			$sql = "select name, review from place where place_id = ?";

			$res = $db->q($sql, [$this->id]);
			if (!$db->numrows($res))
				system::moved("/");
			$this->row = $db->r($res);
		}

		if( isset($_POST["id"]) && !$blocked ) {
			$this->post();
		}

		if( isset($_GET['from_forum_post']) && intval($_GET['from_forum_post']) && $account->isadmin() ) {
			$post_id = intval($_GET['from_forum_post']);
			$res = $db->q("select * from forum_post where post_id = ?", [$post_id]);
			if( $db->numrows($res) ) {
				$row = $db->r($res);
				$_POST['comment'] = $row['post_text'];
			}
		}

		$recommend = $form->select_add_select();
		$recommend .= $form->select_add_yesno(isset($_POST["recommend"])? $_POST["recommend"]:($this->edit?$this->row["recommend"]:""));

		$recommendp = $form->select_add_select();
		$recommendp .= $form->select_add_yesno(isset($_POST["recommendp"])? $_POST["recommendp"]:($this->edit?$this->row["recommendp"]:""));

		$smarty->assign("title", $this->title);
		$smarty->assign("recommend", $recommend);
		$smarty->assign("star", isset($_POST["star"])? $_POST["star"]:($this->edit?$this->row["star"]:0));
		$smarty->assign("recommendp", $recommendp);
		$smarty->assign("name", $this->row["name"]);
		$smarty->assign("id", $this->id);
		$smarty->assign("comment", isset($_POST["comment"])?$_POST["comment"]:($this->edit?$this->row["comment"]:""));
		$smarty->assign("link", $this->getPlaceLink());
		if ($blocked)
			$smarty->assign("blocked", true);
		if (!$this->account_id )
			$smarty->assign("register", true);

		if( $this->provider ) {
			$smarty->assign("provider", true);
			$smarty->assign("provider_name_question", $this->provider_name_question);

			$providers[] = array("0"=>"Don't Know");
			$res = $db->q("SELECT provider_id, provider_name
							FROM place_review_provider
							WHERE module = ? and place_id = ?
							ORDER BY provider_name", [$this->module, $this->id]);
			while($rox=$db->r($res)) {
				$providers[] = array($rox['provider_id']=>$rox['provider_name']);
			}
			$providers[] = array("-1"=>"Name is not on the list, I would like to enter it");

			$provider_names = $form->select_add_array($providers, $_POST['provider_id']?$_POST['provider_id']:$this->row['provider_id']);
			$smarty->assign("provider_names", $provider_names);
			$smarty->assign("provider_id", isset($_POST['provider_id'])?$_POST['provider_id']:$this->row['provider_id']);
			$smarty->assign("provider_name", $_POST['provider_name']);
		}

		if( isset($_GET['popup']) ) $smarty->assign("popup", true);
		if( !is_null($this->reviewwhat) ) $smarty->assign("reviewwhat", $this->reviewwhat);
		$smarty->display(_CMS_ABS_PATH."/templates/place_review_add.tpl");
		if( isset($_GET['popup']) ) die;

		return true;
	}

	function post() {
		global $db, $account, $smarty;

		if( isset($_GET["makeforum"]) && $account->isadmin() ) {
			$forum_id = intval($_POST["forum_id"]);
			$topic_title = GetPostParam("topic_title");
			if (!$forum_id) {
				$error = true;
				$smarty->assign("error", "Pick the forum");
			} elseif ( empty($topic_title) ) {
				$error = true;
				$smarty->assign("error", "You need to type a title for the topic");
			} else {
				$db->q("INSERT INTO forum_topic
						(topic_title, item_id, account_id, topic_time, topic_last_post_nick, forum_id, loc_id, topic_last_post_time) 
						values 
						(?, ?, ?, now(), ?, ?, ?, unix_timestamp(now()))",
						[$topic_title, $this->row["id"], $this->row["account_id"], $this->row["account_id"], $forum_id, $this->row["loc_id"]]);
				$topic_id = $db->insertid();
				if( !$topic_id )
					die("Error occurs".mysql_error());
				$res = $db->q("
					INSERT INTO forum_post (topic_id, account_id, posted, forum_id, loc_id, post_text)
					VALUES
					(?, ?, ?, ?, ?, ?)",
					[$topic_id, $this->row["account_id"], time(), $forum_id, $this->row["loc_id"], $this->row["comment"]]
					);
				$post_id = $db->insertid();
				if( !$post_id )
					die("Error occurs 2: ".mysql_error());
				$db->q("UPDATE forum_topic SET topic_last_post_id = ? WHERE topic_id = ?", [$post_id, $topic_id]);
				$db->q("update account set forum_post = forum_post + 1 where account_id = ?", [$this->row["account_id"]]);

				$_REQUEST["edit"] = $_GET["makeforum"];
				$_GET["remove"] = true;
				unset($_GET["makeforum"]);
				$this->process();
				system::moved("/forum/viewtopic?topic_id=".$topic_id);

			}

			return;
		} elseif( isset($_GET["move"]) && $account->isadmin() ) {
			$new_place_id = intval($_POST['new_place_id']);
			if( !$new_place_id ) die("no place is picked");
			$db->q("update place_review set id = ? where review_id = ?", [$new_place_id, $this->review_id]);
			if ($this->row["provider_id"])
				$db->q("update place_review_provider set place_id = ? where provider_id = ?", [$new_place_id, $this->row["provider_id"]]);

			$db->q("update place set review = review + 1, recommend = recommend + ?, overall = recommend/review where place_id = ?",
				[$this->row["star"], $new_place_id]);
			$db->q("update place set review = review - 1, recommend = recommend - ?, overall=recommend/review where place_id = ?",
				[$this->row["star"], $this->row['id']]);

			system::moved($this->getPlaceLink());
			return;
		}

		$recommend = GetPostParam("recommend"); 
		$recommendp = intval(GetPostParam("recommendp")); 
		$star = GetPostParam("star");
		$comment = $_REQUEST["comment"];

		if ($recommend == "")
			$recommend = NULL;

		if( $star < 1 || $star > 5 ) { $star = NULL; }
		if( $star >= 3 ) $recommend = 1; elseif( $star && $star < 3 ) $recommend = 0;

		//spam blacklist
		if ($this->containsSpam($comment))
			system::moved($this->getPlaceLink());

		//check if there are any links in the review text
		if ($this->spam_protection && (strstr($comment,'http:') || strstr($comment,'www.'))) {
			$smarty->assign("spam_protection", true);
			$spam = true;
		} else
			$spam = false;

		if( $spam ){
			if( strcmp(strtolower($_POST['spam']),strtolower($_SESSION["captcha_str"])) || empty($_SESSION["captcha_str"]) ) {
				$error = true;
				$smarty->assign("error", "ERROR: You have to verify the picture in order to leave this review.");
			} else {
				$smarty->assign("captcha_ok", true); 
			}
			$smarty->assign("spam", $_POST['spam']);
		} else if (!$this->account_id || (strstr($comment, 'http:')||strstr($comment, 'www.'))) {
			if ($this->donot_allow_url) {
				$error = true;				
				$smarty->assign("error", "ERROR: You may not post website addresses in the review.");
			}
		}

		//rate limits
		$dailyLimit = 2;
		if (!$account->isWorker() && !$account->isAdmin()) {
			//check that 1 account cant post more than 2 reviews in 1 day
			$count = $db->single("SELECT COUNT(*) FROM place_review WHERE account_id = ? AND reviewdate = DATE(NOW())", [$account->getId()]);
			if ($count >= $dailyLimit) {
				$smarty->assign("error", "ERROR: You may not post more than {$dailyLimit} reviews per day.");
				return false;
			}
			//check that 1 IP address cant post more than 2 reviews in 1 day
			$count = $db->single("SELECT COUNT(*) FROM place_review WHERE ip_address = ? AND reviewdate = DATE(NOW())", [account::getUserIp()]);
			if ($count >= $dailyLimit) {
				$smarty->assign("error", "ERROR: You may not post more than {$dailyLimit} reviews per day.");
				return false;
			}
		}

		if( !isset($error) && !$this->account_id ) {
			$email = GetPostParam("email");
			$password = GetPostParam("password");
			$username = GetPostParam("username");
			$account_id = $account->auto_register($email, $username, $password, 1, $error);
			if( $account_id > 0 ) {
				$account->login($email, $password, $error);
				$this->account_id = $account->isloggedin();
			} else {
				$smarty->assign("email", $_POST["email"]);
				$smarty->assign("password", $_POST["password"]);
				$smarty->assign("username", $_POST["username"]);
				if( isset($error) )
					$smarty->assign("error", $error);
			}
		}

		if( !isset($error) && is_null($recommend)) {
			$error = true; $smarty->assign("error", "Please rate this {$this->title}");
		} else if (!isset($error) && empty($comment)) {
			$error = true; $smarty->assign("error", "Please comment your experience for ".$this->row["name"]);
		} elseif( is_null($star)) {
			$error = true; $smarty->assign("error", "Please rate this place");
		}

		if( !isset($error) && $this->provider ) {
			$provider_id = intval($_POST['provider_id']);
			if( $provider_id == -1 ) {
				$provider_name = GetPostParam("provider_name");
				if( empty($provider_name) ) {
					$error = true; $smarty->assign("error", "You did not type the {$this->provider_name_question}");
				} else {
					$db->q("insert into place_review_provider (provider_name, module, place_id) values (?, ?, ?)",
						[$provider_name, $this->module, $this->id]
						);
					$provider_id = $db->insertid;
				}
			} elseif( $provider_id ) {
				$res = $db->q("select provider_name from place_review_provider where provider_id = ? and module = ? and place_id = ?",
					[$provider_id, $this->module, $this->id]
					);
				if( !$db->numrows($res) ) {
					reportAdmin("provider_id is not right for the review");
					$error = true;
					$smarty->assign("error", "ERROR! Please select the {$this->provider_name_question} again.");
					unset($_POST['provider_id']);
				} $rox = $db->r($res); $provider_name = $rox[0];
			}

			if( $provider_id ) {
				$this->uploadproviderpic($provider_id);
			}

		} else {
			$provider_id = 0;
		}

		$recommend = intval($recommend);

		if (isset($error) || !$this->account_id)
			return;

		if( isset($_GET['account_id']) && intval($_GET['account_id']) && $account->isadmin() ) 
			$account_id = intval($_GET['account_id']);
		else
			$account_id = $this->account_id;

		if( $this->edit ) {

			$db->q("update place set recommend = recommend - ? + ?, overall=recommend/review where place_id = ?",
					[$this->row["star"], $star, $this->row["id"]]);

			$db->q("UPDATE place_review SET provider_id = ?, recommend = ?, recommendp = ?, `comment` = ?, star = ? WHERE review_id = ?",
					array($provider_id, $recommend, $recommendp, $comment, $star, $this->review_id)
					);
		} else {
			$ip = preg_replace('/[^0-9a-zA-Z:.]/', '', account::getUserIp());
			$db->q("INSERT INTO place_review 
					(id, module, account_id, provider_id, recommend, recommendp, reviewdate, comment, ip_address, star, done) 
					values 
					(?, ?, ?, ?, ?, ?, CURDATE(), ?, ?, ?, 0)",
					array($this->id, $this->module, $account_id, $provider_id, $recommend, $recommendp, $comment, $ip, $star)
					);
			//jay
			//$insertid = $db->insertid;
			$this->review_id = $db->insertid;

			$db->q("update place set review = review + 1, recommend = recommend + ?, overall=recommend/review, last_review_id = ? where place_id = ?",
				[$star, $this->review_id, $this->id]);

			//store review pictures
			for ($i=1; $i<5; $i++) {
				files::uploadImage(0, "review", $this->review_id, "review_pic{$i}");
			}

			if (!$account->isrealadmin()) {
				$this->notifyNewReview($this->row['review'] + 1, $provider_name, $account_id, $_SESSION['username'], $comment, array(SUPPORT_EMAIL));
			}

			$db->q("insert into classifieds_stat (time, review) values (curdate(), '1') on duplicate key update review=review+1");

			rotate_index("dir");

			$db->q("INSERT INTO place_review_log
					(id, module, account_id, provider_id, recommend, reviewdate, comment, ip_address, star) 
					values 
					(?, ?, ?, ?, ?, curdate(), ?, ?, ?)",
					array($this->id, $this->module, $account_id, $provider_id, $recommend, $comment, $ip, $star));
		}

		if( isset($_POST['popup']) ) die("Success");

		$res = $db->q("select * from place where place_id = ?", [$this->id]);

		if( $db->numrows($res) ) {
			$row = $db->r($res);
			if( $row['loc_id'] ) {
				$twitter = new twitter;
				$url = $this->getPlaceLink();
				$twitter->queue($row['loc_id'], "New review left for {$this->title} in {$row['loc_name']}. {$url}");
			}
		}

		system::moved($this->getPlaceLink());
	}

	private function containsSpam($text) {

		$blacklistedPhrases = [
			"4gosex.com",
			"pussyliker.com",
			];

		foreach ($blacklistedPhrases as $pattern) {
			if (stristr($text, $pattern) !== false) {
				debug_log("new review contains spam: pattern='{$pattern}' text='{$text}'");
				return true;
			}
		}
		
		return false;
	}

	/**
	 * notify about new review
	 */
	public function notifyNewReview($nr, $provider_name, $account_id, $username, $comment, $emails) {
		global $db, $mobile;

		$nr = $this->row['review'] + 1;
		$m = "";
		if (!empty($provider_name))
			$m .= "provider: <b>{$provider_name}</b><br/>";
		$m .= "user_id: <b>$account_id</b><br>username: <b>{$_SESSION['username']}</b><br>total review: <b>$nr</b><br>mobile: <b>$mobile</b><br>";
		$m .= "#{$this->id} <a href='".$this->getPlaceLink()."'>".$this->getPlaceLink()."</a><br><br>$comment<br /><br />";
		$m .= "<a href=\"https://adultsearch.com/review/remove?id={$this->review_id}\">remove review</a><br />";
		$m .= "<a href=\"https://adultsearch.com/review/remove_ban?id={$this->review_id}\">remove review &amp; ban user who wrote review</a><br />";
		foreach ($emails as $email) {
			sendEmail(SUPPORT_EMAIL, "new review", $m, $email);
		}
	}

	public function reviews() {
		global $db, $smarty, $account, $ctx, $config_image_server, $page;

		$dictionary = new dictionary();

		//paging
		$items_per_page = 10;
		$p = intval($page);
		if ($p < 1)
			$p = 1;
		$start = ($p * $items_per_page) - $items_per_page;

		//get total number of reviews
		$rex = $db->q("SELECT count(*) as cnt FROM place_review r WHERE r.id = ? AND r.module = ?", array($this->id, $this->module));
		$rox = $db->r($rex);
		$total_review = $rox["cnt"];

		//get reviews
		$res = $db->q("
			SELECT r.*, date_format(reviewdate, '%m-%d-%Y') reviewdate, 
				a.avatar, a.username, 
				p.provider_name, group_concat(pp.filename) as pfilename, p.ethnicity, p.age_group, p.height_range, p.build, p.hair_color, p.hair_length, p.eye_color, p.ass, p.cup, 
					p.breast_implants, p.kitty, p.tattoos,  
				pic.picture as video_id, pic.filename as video_filename, pic.thumb as video_thumb 
			FROM place_review r 
			LEFT JOIN account a using (account_id) 
			LEFT JOIN (place_review_provider p left join place_review_provider_pic pp using (provider_id)) using (provider_id) 
			LEFT JOIN place_picture pic on r.review_id = pic.id and pic.place_file_type_id = 2 and pic.module = 'review' and pic.visible = 1
			WHERE r.id = ? AND r.module = ? 
			GROUP BY review_id 
			ORDER BY review_id desc 
			LIMIT $start, $items_per_page",
			array($this->id, $this->module));

		$quickread=array();
		while($row2 = $db->r($res)) {

			if ($row2["ip"] && (!isset($ips[$row2["ip"]]) || !in_array($row2["username"], $ips[$row2["ip"]])))
				$ips[$row2["ip"]][] = $row2["username"];

			//get comments
			$c = NULL;
			if ($row2["c"] > 0) {
				$rex = $db->q("SELECT c.comment_id, c.`comment`, date_format(c.date, '%m-%d-%Y %r') date, c.account_id, a.username, a.avatar 
					FROM place_review_comment c 
					LEFT JOIN account a using (account_id) 
					WHERE c.review_id = ? 
					ORDER BY c.comment_id",
					array($row2["review_id"]));
				while($rox = $db->r($rex)) {
					$c[] = array(
						"comment_id" => $rox["comment_id"],
						"date" => $rox["date"],
						"comment" => $dictionary->process(nl2br(htmlspecialchars($rox["comment"]))),
						"username" => $rox["username"],
						"account_id" => $rox["account_id"],
						"avatar" => $rox["avatar"],
						"owner" => $this->account_id==$rox["account_id"]||$account->isadmin()?true:false
					);
				}
			}

			$pfilename = !empty($row2['pfilename']) ? explode(',', $row2['pfilename']) : NULL;

			//jay - images
			$image = array();
			$rest = array();
			$resi = $db->q("select picture, filename, thumb from place_picture where module = 'review' and visible = 1 and id = ?", [$row2["review_id"]]);
			while ($rowi = $db->r($resi)) {
				$im = array("big" => $config_image_server."/review/".$rowi['filename'],
							"thumb" => $config_image_server."/review/t/".$rowi['thumb'],
							"id" => $rowi['picture'],
							);
				if (empty($image)) {
					$image = $im;
				} else {
					$rest[] = $im;
				}
			}
			if (!empty($image))
				$image['rest'] = $rest;

			//jay - videos
			$videos = array();
			//TODO - support for multiple videos for one review ??
			if (($row2["video_id"] != "") && ($row2["video_filename"] != "")) {
				$videos[] = video::withAll($row2["video_id"], 'review', NULL, $ctx->country, $row2["video_filename"], $row2["video_thumb"], '', '', 1);
			}

			$qr = $row2;
			$qr["review"] = $this->locallink($dictionary->process(nl2br(htmlspecialchars($row2["comment"], ENT_NOQUOTES))));
			$qr["detail_provider"] = $this->locallink($dictionary->process(nl2br(htmlspecialchars($row2["detail_provider"], ENT_NOQUOTES))));
			$qr["comment"] = $c;
			$qr["ips"] = &$ips[$row2["ip"]];
			$qr["pfilename"] = $pfilename;
			$qr["videos"] = $videos;
            $qr["image"] = $image;
			$qr["provider_name"] = $row2["provider_name"];

			$qr["visit_time"] = $this->minutes_to_time($row2["visit_time"]);

			$qr["private"] = $this->yesno($row2["private"]);
			$qr["clean"] = $this->yesno($row2["clean"]);
			$qr["lineup"] = $this->yesno($row2["lineup"]);
			$qr["tip"] = $this->yesno($row2["tip"]);

			$qr["type"] = $this->enum($row2["type"], $this->mp_types);
			$qr["fee"] = "\${$row2["fee"]}";
			$qr["extra_paid"] = "\${$row2["extra_paid"]}";
			$qr["ethnicity"] = ucfirst($row2["ethnicity"]);

			$qr["breast_implants"] = $this->yesno($row2["breast_implants"]);
			$qr["recommend"] = $this->yesno($row2["recommend"]);
			$qr["recommendp"] = $this->yesno($row2["recommendp"]);
			$qr["age_group"] = $this->enum($row2["age_group"], $this->ageArray);
			$qr["build"] = $this->enum($row2["build"], $this->buildArray);
			$qr["height_range"] = $this->enum($row2["height_range"], $this->heightArray);
			$qr["hair_length"] = $this->enum($row2["hair_length"], $this->hairlengthArray);
			$qr["cup"] = $this->enum($row2["cup"], $this->cupArray);
			$qr["ass"] = $this->enum($row2["ass"], $this->assArray);

			$qr["kissing"] = $this->enum($row2["kissing"], $this->kissArray);
			$qr["sex"] = $this->enum($row2["sex"], $this->sexArray);
			$qr["handjob"] = $this->enum($row2["handjob"], $this->handjobArray);
			$qr["blowjob"] = $this->enum($row2["blowjob"], $this->blowjobArray);
			$qr["lick"] = $this->enum($row2["lick"], $this->lickArray);
			$qr["finger"] = $this->enum($row2["finger"], $this->fingerArray);
			$qr["assplay"] = $this->enum($row2["assplay"], $this->assplayArray);
			$qr["breastplay"] = $this->enum($row2["breastplay"], $this->breastplayArray);

			$qr["tattoos"] = $this->enum($row2["tattoos"], $this->tattoosArray);

			//jay
			if ($ctx->page_type != NULL) {
				$qr['link_edit'] = $ctx->node_id."/add_review?edit=".$row2["review_id"];
				$qr['link_remove'] = $ctx->node_id."/add_review?edit=".$row2["review_id"]."&amp;remove=true";
				$qr['link_makeforum'] = $ctx->node_id."/add_review?makeforum=".$row2["review_id"];
				$qr['link_move'] = $ctx->node_id."/add_review?move=".$row2["review_id"];
			} else {
				$qr['link_edit'] = "review-add?id=".$this->id."&amp;edit=".$row2["review_id"];
				$qr['link_remove'] = "review-add?id=".$this->id."&amp;edit=".$row2["review_id"]."&amp;remove=true";
				$qr['link_makeforum'] = "review-add?id=".$this->id."&amp;makeforum=".$row2["review_id"];
				$qr['link_move'] = "review-add?id=".$this->id."&amp;move=".$row2["review_id"];
			}

			$quickread[] = $qr;
		}

		$paging_review = $db->paging($total_review, $items_per_page, false, true, "?id=".$this->id, NULL);

		$smarty->assign("paging_review", $paging_review);
		$smarty->assign("provider_name_question", $this->provider_name_question);

		include_html_head("css", "/css/slider.css");

		return $quickread;
	}

	private function yesno($val) {
		return ($val) ? "Yes" : "No";
	}
	private function enum($val, $arr) {
		if (array_key_exists($val, $arr))
			return $arr[$val];
		return "";
	}
	private function minutes_to_time($mins) {
		return date('g:i A', mktime(0,$mins));
	}

	function locallink($link) {
		$m = NULL;
		return preg_replace('/\[link=(http:\/\/(.*)?\.adultsearch\.com\/([^\]]*))\](.*)\[\/link\]/sm', '<a href="$1">$4</a>', $link);
	}

	function comment() {
		global $db, $account, $smarty, $gItemID, $mobile;

		$c = trim($_REQUEST["c"]);

		$redirect = GetPostParam("redirect");
		if( empty($redirect) )
			$redirect = "?";

		$this->account_id = $account->isloggedin();
		if (!$this->account_id)
			die;

		$review_id = intval($_POST["review_id"]);
		if (empty($c) || !$review_id || !$this->account_id) {
			if ($this->account_id && !empty($c))
				reportAdmin("AS: New Review Comment Fails", "", array("c" => $c, "review_id" => $review_id, "account_id" => $this->account_id));
			ob_clean();

			header('Cache-Control: no-cache, must-revalidate'); header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
			header('Content-type: application/json; charset=utf-8');
			echo json_encode(array());
			die;
		}

		$db->q("INSERT INTO place_review_comment 
				(review_id, account_id, date, comment) 
				values 
				(?, ?, NOW(), ?)",
				array($review_id, $this->account_id, $c)
				);
		$id = $db->insertid();
		if (!$id)
			die;

		$db->q("update place_review set c = c + 1 where review_id = ?", array($review_id));

		$db->q("insert into classifieds_stat (time, reviewc) values (curdate(), '1') on duplicate key update reviewc=reviewc+1");

		$res = $db->q("SELECT c.comment_id, c.`comment`, date_format(c.date, '%m-%d-%Y %r') date, c.account_id, a.username, a.avatar 
						FROM place_review_comment c
						LEFT JOIN account a using (account_id)
						WHERE c.comment_id >= ? and c.review_id = ?
						ORDER BY c.comment_id",
						array($id, $review_id)
						);

		while ($row = $db->r($res)) {
			$comment[] = array(
				"comment_id"=>$row["comment_id"],
				"date"=>$row["date"],
				"comment"=>iconv('ISO-8859-9','utf-8', nl2br($row["comment"])),
				"username"=>iconv('ISO-8859-9','utf-8', $row["username"]),
				"account_id"=>$row["account_id"],
				"avatar"=>$row["avatar"],
				"owner"=>$this->account_id==$row["account_id"]?true:false
			);
		}

		$smarty->assign("comment", $comment);
		if ($gItemID)
			$smarty->assign("id", $gItemID);
		$result = $smarty->fetch(_CMS_ABS_PATH."/templates/place/ajax_comment_place.tpl");

		//send new comment email to admin
		if(!$account->isadmin() ) {
			$this->commentAdminNotify($c, $this->getPlaceLink(), $review_id, $this->account_id);
		}

		//notify author of review about new comment
		$res = $db->q("SELECT r.account_id, a.email, a.username, o.opt 
						FROM place_review r 
						INNER JOIN account a on r.account_id = a.account_id 
						LEFT JOIN account_opt o on r.account_id = o.account_id and o.x = 'newcomment'
						WHERE r.review_id = ?",
						array($review_id)
						);
		if( $db->numrows($res) ) {
			$row = $db->r($res);
			$review_account_id = $row["account_id"];
			if( $this->account_id != $row["account_id"] && $row["opt"] !== 0) {
				$m = "Dear {$row["username"]},<br><br>" . $_SESSION["username"] . " has left a new comment on one of your reviews. To read the new comment, please click on the following link.<br><br><a href='http://adultsearch.com/account/review-redirect?review_id=$review_id'>http://adultsearch.com/account/review-redirect?review_id=$review_id</a><br><br>You may change your email notification settings at <a href='http://adultsearch.com/account/notifications'>http://adultsearch.com/account/notifications</a> anytime you want. <br><br>adultsearch.com";
				sendEmail(SUPPORT_EMAIL, "AdultSearch.com - Comment Notification", $m, $row["email"]);
			}
		}

		//notify previously commenting members
		$res = $db->q("SELECT r.account_id, a.email, a.username, o.opt 
						FROM place_review_comment r 
						INNER JOIN account a on r.account_id = a.account_id 
						LEFT JOIN account_opt o on r.account_id = o.account_id and o.x = 'newcommentme'
						WHERE r.review_id = ?",
						array($review_id)
						);
		while($row = $db->r($res)) {
			if( $this->account_id != $row["account_id"] && $this->account_id != $review_account_id ){
				$m = "Dear {$row["username"]},<br><br>" . $_SESSION["username"] . " has left a new comment on one of the review you commented earlier. To read the new comment, please click on the following link.<br><br><a href='http://adultsearch.com/account/review-redirect?review_id=$review_id'>http://adultsearch.com/account/review-redirect?review_id=$review_id</a><br><br>You may change your email notification settings at <a href='http://adultsearch.com/account/notifications'>http://adultsearch.com/account/notifications</a> anytime you want. <br><br>adultsearch.com";
				sendEmail(SUPPORT_EMAIL, "AdultSearch.com - Comment Notification", $m, $row["email"]);
			}
		}

		ob_clean();

		header('Cache-Control: no-cache, must-revalidate'); header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json; charset=utf-8');
		echo json_encode($result);
		die;
	}

	function commentremove() {
		global $db, $account;

		$comment_id = intval($_GET["commentremove"]);

		if( !$comment_id || (!$this->account_id && !$account->isadmin()) ) die;

		$res = $db->q("select review_id, account_id from place_review_comment where comment_id = ?", [$comment_id]);
		if( !$db->numrows($res) ) die;
		$row = $db->r($res);
		$review_id = $row[0];

		if( $row[1] != $this->account_id && !$account->isadmin() ) die;
		$db->q("delete from place_review_comment where comment_id = ?", [$comment_id]);
		if( $db->affected ) {
			$db->q("update place_review set c = c - 1 where review_id = ?", [$review_id]);
			$db->q("update classifieds_stat set reviewc = reviewc - 1 where time = curdate()");
		}
		ob_clean();
		header('Content-type: application/javascript');
		echo("jQuery('#co{$comment_id}').remove();");
	}

	function isblocked() {
		global $db;

		$ip = account::getUserIp();
		if (!$ip)
			return false;

		$res = $db->q("SELECT ip_address FROM place_review_block WHERE ip_address = ? LIMIT 1", [$ip]);
		if( $db->r($res) ) {
			reportAdmin("AS: Blocked IP review attempt");
			return true;
		}

		return false;
	}

	function uploadproviderpic($provider_id) {
		global $db, $config_image_path, $account;

		if( !$provider_id ) return;

		require_once(_CMS_ABS_PATH."/inc/classes/class.upload.php");

		$system = new system;
		for($i=1;$i<5;$i++){
			$handle = new Upload($_FILES["provider_pic{$i}"]);
			if (!$handle->uploaded)
				continue;

			list($width, $height, $t, $attr) = getimagesize($handle->file_src_pathname);
			if( $height > $width && $height > 632 ) { $handle->image_resize = true; $handle->image_ratio_x = true; $handle->image_y = 632; }
			else if( $width > $height && $width > 600 ) { $handle->image_resize = true; $handle->image_ratio_y = true;$handle->image_x = 600;}
			$handle->image_convert = "jpg";
	
			$text = $system->_makeText(20);
			$upload_dir = "{$config_image_path}/rprovider/";
	
			$account_id = $account->isloggedin();
			$handle->file_new_name_body = $provider_id . "_" . $text;
	
			$handle->Process($upload_dir);
			if($handle->processed) {
				if( $width > 136 ) $handle->image_resize = true;
				$handle->image_x = 136; $handle->image_ratio = true; $handle->image_y = NULL;
				$handle->file_new_name_body = $provider_id . "_" . $text;
				$handle->Process($upload_dir."t");
				$db->q("insert into place_review_provider_pic (provider_id, filename, byuser, time) values (?, ?, ?, now())",
					[$provider_id, $handle->file_dst_name, $account_id]);
				$db->q("update place_review_provider set pic = pic + 1 where provider_id = ?", [$provider_id]);
			}
			$handle->Clean();
		}
	}

	//temporary helper functions until full unification
	private function getPlaceLink() {
		return dir::getPlaceLinkById($this->id);
	}
	
	public static function commentAdminNotify($comment, $place_link, $review_id, $account_id, $email = NULL) {
		global $db;

		$acc = account::findOneById($account_id);
		if (!$acc)
			return false;

		$smarty = GetSmartyInstance();
		$smarty->assign("comment", $comment);
		$smarty->assign("account_id", $acc->getId());
		$smarty->assign("account_email", $acc->getEmail());
		$smarty->assign("account_mng_link", "http://adultsearch.com/mng/accounts?account_id={$acc->getId()}");
		$smarty->assign("comment_link", $place_link."#review{$review_id}");

		$html = $smarty->fetch(_CMS_ABS_PATH."/templates/review/email/comment_admin_notify.html.tpl");
		$text = $smarty->fetch(_CMS_ABS_PATH."/templates/review/email/comment_admin_notify.txt.tpl");

		if (!$email) {
			$email = SUPPORT_EMAIL;
		}

		return send_email(array(
			"from" => WEB_EMAIL,
			"to" => $email,
//			"bcc" => ADMIN_EMAIL,
			"subject" => "New Review Comment",
			"html" => $html,
			"text" => $text,
			));
	}

	public static function refreshComputedColumns() {
		global $db;

		$res = $db->q("
			SELECT p.place_id, p.review, p.recommend, p.overall, p.last_review_id 
			FROM place p 
			ORDER BY p.place_id ASC", []);
		$i = 0;
		while ($row = $db->r($res)) {
			$place_id = $row["place_id"];
			$review = $row["review"];
			$recommend = $row["recommend"];
			$overall = $row["overall"];
			$last_review_id = $row["last_review_id"];
			
			$rev = $rec = $totstar = $ove = $las = 0;
			$res2 = $db->q("
				SELECT pr.review_id, pr.recommend, pr.star 
				FROM place_review pr 
				WHERE pr.id = ? AND pr.module = 'place' 
				ORDER BY pr.review_id ASC
				", 
				[$place_id]
				);
			while ($row2 = $db->r($res2)) {
				$rev++;
				$rec += intval($row2["recommend"]);
				$totstar += intval($row2["star"]);
				$las = $row2["review_id"];
			}
			if ($totstar)
				$ove = round(($totstar / $rev), 2);
			if ($review != $rev || $recommend != $rec || $overall != $ove || $last_review_id != $las) {
				echo "Updating #{$place_id} rev={$rev} rec={$rec} ove={$ove} las={$las}\n";
				$res2 = $db->q("
					UPDATE place 
					SET review = ?, recommend = ?, overall = ?, last_review_id = ?
					WHERE place_id = ?
					LIMIT 1
					",
					[$rev, $rec, $ove, $las, $place_id]
					);
				if ($db->affected($res2) != 1)
					die("EA");
			}
			$i++;
		}
		echo "Done, updated {$i} places.\n";
	}
}

//END
