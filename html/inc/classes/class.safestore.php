<?php

define("SAFESTORE_DIR", _CMS_ABS_PATH."/../data/safestore");
define("PUBLIC_KEY_FILENAME", "safestore.public.key");

/**
 * Class safestore handles safe (encrypted) file storage of data (e.g. cc numbers)
 * to generate public&private key pair:
 * $ openssl genrsa -out web.safestore.private.key 2048
 * $ openssl rsa -in web.safestore.private.key -outform PEM -pubout -out web.safestore.public.key
 */
class safestore {

	var $error = NULL;
	//const CONST = 1;

	function __construct() {
	}

	/**
	 * Adds entry to safestore
	 * Returns true in case of success, false after failure (error variable is filled with error message)
	 */
	public static function addEntry($storage_code, $id, $item) {
		//check if safestore exists
		if (!is_dir(SAFESTORE_DIR)) {
			$ret = mkdir(SAFESTORE_DIR);
			if (!$ret) {
				reportAdmin("AS: safestore::addEntry error", "Can't create safestore directory: '".SAFESTORE_DIR."' !", array("directory" => SAFESTORE_DIR));
				return false;
			}
			reportAdmin("AS: safestore::addEntry directory created", "Safestore directory '".SAFESTORE_DIR."' has been created.", array("directory" => SAFESTORE_DIR));
		}

		//check public key file
		if (!is_readable(SAFESTORE_DIR."/".PUBLIC_KEY_FILENAME)) {
			reportAdmin("AS: safestore::addEntry error", "Can't read safestore public key: '".SAFESTORE_DIR."/".PUBLIC_KEY_FILENAME."' !", 
				array("public_key_filepath" => SAFESTORE_DIR."/".PUBLIC_KEY_FILENAME));
			return false;
		}

		//get public key
		$public_key = openssl_pkey_get_public(file_get_contents(SAFESTORE_DIR."/".PUBLIC_KEY_FILENAME));
		if ($public_key === false) {
			reportAdmin("AS: safestore::addEntry error", "Public key can't be read or is incorrect !", array("public_key_filepath" => SAFESTORE_DIR."/".PUBLIC_KEY_FILENAME));
			return false;
		}

		//json encode data into string
		$data = json_encode($item);
		if ($data === false) {
			reportAdmin("AS: safestore::addEntry error", "Failed to json_encode item !", array());
			return false;
		}

		//encrypt
		$encrypted = $env = NULL;
		$ret = openssl_seal($data, $encrypted, $env_keys, array($public_key));
		if ($ret === false) {
			reportAdmin("AS: safestore::addEntry error", "Failed to encrypt data !", array());
			return false;
		}

		//base64 encode
		$sealed_data = base64_encode($encrypted);
		$envelope = base64_encode($env_keys[0]);

		//and append to storage file
		$storage_filepath = SAFESTORE_DIR."/{$storage_code}.bin";
		$handle = fopen($storage_filepath, "a");
		if ($handle === false) {
			reportAdmin("AS: safestore::addEntry error", "Failed to open storage file '{$storage_filepath}' for appending !", array("storage_filepath" => $storage_filepath));
			return false;
		}

		$id = intval($id);
		$line = "{$id},{$sealed_data},{$envelope}\n";
		$ret = fwrite($handle, $line);
		if ($ret === false || $ret != strlen($line)) {
			reportAdmin("AS: safestore::addEntry error", "Failed to write line into storage file !", array("storage_filepath" => $storage_filepath, "line" => $line));
			return false;
		}

		//all succeeded
		fclose($handle);

		return true;		
	}
	
	/**
	 * Uodates entry in safestore
	 * Returns true in case of success, false after failure (error variable is filled with error message)
	 */
	public static function updateEntry($storage_code, $id, $item) {
		//check if safestore exists
		if (!is_dir(SAFESTORE_DIR)) {
			$ret = mkdir(SAFESTORE_DIR);
			if (!$ret) {
				reportAdmin("AS: safestore::updateEntry error", "Can't create safestore directory: '".SAFESTORE_DIR."' !", array("directory" => SAFESTORE_DIR));
				return false;
			}
			reportAdmin("AS: safestore::updateEntry directory created", "Safestore directory '".SAFESTORE_DIR."' has been created.", array("directory" => SAFESTORE_DIR));
		}

		//check public key file
		if (!is_readable(SAFESTORE_DIR."/".PUBLIC_KEY_FILENAME)) {
			reportAdmin("AS: safestore::updateEntry error", "Can't read safestore public key: '".SAFESTORE_DIR."/".PUBLIC_KEY_FILENAME."' !", 
				array("public_key_filepath" => SAFESTORE_DIR."/".PUBLIC_KEY_FILENAME));
			return false;
		}

		//get public key
		$public_key = openssl_pkey_get_public(file_get_contents(SAFESTORE_DIR."/".PUBLIC_KEY_FILENAME));
		if ($public_key === false) {
			reportAdmin("AS: safestore::updateEntry error", "Public key can't be read or is incorrect !", array("public_key_filepath" => SAFESTORE_DIR."/".PUBLIC_KEY_FILENAME));
			return false;
		}

		//json encode data into string
		$data = json_encode($item);
		if ($data === false) {
			reportAdmin("AS: safestore::updateEntry error", "Failed to json_encode item !", array());
			return false;
		}

		//encrypt
		$encrypted = $env = NULL;
		$ret = openssl_seal($data, $encrypted, $env_keys, array($public_key));
		if ($ret === false) {
			reportAdmin("AS: safestore::updateEntry error", "Failed to encrypt data !", array());
			return false;
		}

		//base64 encode
		$sealed_data = base64_encode($encrypted);
		$envelope = base64_encode($env_keys[0]);

		//prepare line
		$id = intval($id);
		$new_line = "{$id},{$sealed_data},{$envelope}\n";
		$start = "{$id},";
		$storage_filepath = SAFESTORE_DIR."/{$storage_code}.bin";
		$storage_filepath_tmp = SAFESTORE_DIR."/{$storage_code}.bin.tmp";

		//and update in storage file
		$reading = fopen($storage_filepath, "r");
		$writing = fopen($storage_filepath_tmp, "w");
		if ($reading === false || $writing === false) {
			reportAdmin("AS: safestore::updateEntry error", "Failed to open storage file '{$storage_filepath}' or tmp file !", array("storage_filepath" => $storage_filepath, "storage_filepath_tmp" => $storage_filepath_tmp));
			return false;
		}
		$replaced = false;
		while (!feof($reading)) {
			$line = fgets($reading);
			if (substr($line, 0, strlen($start)) == $start) {
				$line = $new_line;
				$replaced = true;
			}
			if (fputs($writing, $line) === false) {
				reportAdmin("AS: safestore::updateEntry error", "Failed put line into tmp file !", array("storage_filepath_tmp" => $storage_filepath_tmp, "line" => $line));
				return false;
			}
		}
		fclose($reading);
		fclose($writing);
		if ($replaced) {
			//we only overwrite the file if we did actual replacement
			rename($storage_filepath_tmp, $storage_filepath);
		} else {
			unlink($storage_filepath_tmp);
		}

		if (!$replaced) {
			reportAdmin("AS: safestore::updateEntry error", "Didnt do any replacement ?", array("storage_filepath" => $storage_filepath, "line" => $line));
			return false;
		}

		//all succeeded
		return true;		
	}

	/**
	 * Returns true if we already have entry with this id, returns false otherwise
	 */
	public function hasEntryById($storage_code, $id) {
		//check if safestore exists
		if (!is_dir(SAFESTORE_DIR)) {
			reportAdmin("AS: safestore::hasEntryById safestore dir does not exist", "Safestore directory '".SAFESTORE_DIR."' does not exist.", array("directory" => SAFESTORE_DIR));
			return false;
		}

		//open storage file
		$storage_filepath = SAFESTORE_DIR."/{$storage_code}.bin";
		$handle = fopen($storage_filepath, "r");
		if ($handle === false) {
			reportAdmin("AS: safestore::hasEntryById error", "Failed to open storage file '{$storage_filepath}' for reading !", array("storage_filepath" => $storage_filepath));
			return false;
		}

		//scan storage file and find line
		$id = intval($id);
		$encrypted_data = $envelope = NULL;
		while (!feof($handle)) {
			$line = fgets($handle);
			$arr = explode(",", $line);
			if (count($arr) != 3)
				continue;
			if (intval($arr[0]) == intval($id)) {
				$encrypted_data = base64_decode($arr[1]);
				$envelope = base64_decode($arr[2]);
				break;
			}
		}
		if ($encrypted_data === false || $envelope === false) {
			//error while base_64 decoding ?  - this should not happen
			reportAdmin("AS: safestore::hasEntryById error", "Failed to base64_decode data or envelope !", []);
			return false;
		}
		if ($encrypted_data == NULL || $envelope == NULL) {
			//item with such ID not found
			return false;
		}
		//item found
		fclose($handle);
		return true;
	}
	
	/**
	 * Finds entry by id, and returns decrypted item
	 * Returns true in case of success, false after failure (error variable is filled with error message)
	 */
	public static function getEntryById($storage_code, $id, $private_key_text, &$item) {
		//check if safestore exists
		if (!is_dir(SAFESTORE_DIR)) {
			$ret = mkdir(SAFESTORE_DIR);
			if (!$ret) {
				reportAdmin("AS: safestore::getEntryById error", "Can't create safestore directory: '".SAFESTORE_DIR."' !", array("directory" => SAFESTORE_DIR));
				return false;
			}
			reportAdmin("AS: safestore::getEntryById directory created", "Safestore directory '".SAFESTORE_DIR."' has been created.", array("directory" => SAFESTORE_DIR));
		}

		//get private key
		$private_key = openssl_pkey_get_private($private_key_text);
		if ($private_key === false) {
			reportAdmin("AS: safestore::getEntryById error", "Private key can't be read or is incorrect !", array("length(private_key)" => strlen($private_key_text)));
			return false;
		}

		//open storage file
		$storage_filepath = SAFESTORE_DIR."/{$storage_code}.bin";
		$handle = fopen($storage_filepath, "r");
		if ($handle === false) {
			reportAdmin("AS: safestore::getEntryById error", "Failed to open storage file '{$storage_filepath}' for reading !", array("storage_filepath" => $storage_filepath));
			return false;
		}

		//scan storage file and find line
		$id = intval($id);
		$encrypted_data = $envelope = NULL;
		while (!feof($handle)) {
			$line = fgets($handle);
			$arr = explode(",", $line);
			if (count($arr) != 3)
				continue;
			if (intval($arr[0]) == intval($id)) {
				$encrypted_data = base64_decode($arr[1]);
				$envelope = base64_decode($arr[2]);
				break;
			}
		}
		if ($encrypted_data === false || $envelope === false) {
			//error while base_64 decoding ?	- this should not happen
			reportAdmin("AS: safestore::getEntryById error", "Failed to base64_decode data or envelope !", array());
			return false;
		}
		if ($encrypted_data == NULL || $envelope == NULL) {
			//item with such ID not found
			return false;
		}
		//item found !
		fclose($handle);

		//decrypt
		$plaintext = NULL;
		$ret = openssl_open($encrypted_data, $data, $envelope, $private_key);
		if ($ret === false) {
			reportAdmin("AS: safestore::getEntryById error", "Failed to decrypt data !", array());
			return false;
		}

		//json decode data into item
		$item = json_decode($data);
		if ($item === NULL) {
			reportAdmin("AS: safestore::getEntryById error", "Failed to json_decode item !", array());
			return false;
		}

		return true;		
	}

}

//END
