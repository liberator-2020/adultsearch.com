<?php

use SecurionPay\SecurionPayGateway;
use SecurionPay\Exception\SecurionPayException;


class securionpay {

	private $proxyConnection = null;

	public function __construct() {
		$this->proxyConnection = new ProxyConnection();
	}

	public function get_name() {
		return "securionpay";
	}

	public function get_template_path() {
		return _CMS_ABS_PATH."/templates/payment_gen_securionpay.tpl";
	}

	public function export_to_template() {
		global $smarty, $config_securionpay_public_key;
		$smarty->assign("config_securionpay_public_key", $config_securionpay_public_key);
	}

	//payment needs to contain unobfuscated card number and csc
	public function charge($payment, $cardno_real, $csc_real, $captured = true, &$error) {
		global $db, $config_securionpay_secret_key;

		$now = time();

		$payment_id = $payment["payment_id"];
		$account_id = $payment["account_id"];

		$card = $payment["card"];

		$firstname = $card->getFirstname();
		$lastname = $card->getLastname();
		$address = $card->getAddress();
		$city = $card->getCity();
		$zip = $card->getZipcode();
		$state = $card->getState();
		$country = $card->getCountryCode();
		$card_no = $card->getCc();
		$exp_month = $card->getExpMonth();
		$exp_year = $card->getExpYear();
		$csc = $card->getCvc2();
		$token = $card->getSecurionpayToken();
		$email = $payment["email"];
		$cc_id = $card->getId();
		$amount = $payment["amount"];
		$recurring_amount = $payment["recurring_amount"];

		file_log("securionpay", "securionpay::charge(): payment_id={$payment_id}, account_id={$account_id}, card_id={$card->getId()}, amount={$amount}");

		//doublecheck completeness of params
		if (!$firstname
			|| !$lastname
			|| !$address
			|| !$city
			|| !$zip
			|| !$cardno_real
			|| !$exp_month
			|| !$exp_year
			|| !$csc_real
			|| !$email
			) {
			file_log("securionpay", "securionpay::charge: mandatory fields not filled: payment_id={$payment_id}, cc_id='{$cc_id}', firstname='{$firstname}', lastname='{$lastname}', address='{$address}', city='{$city}', zip='{$zip}', email='{$email}', cardno='{$card_no}', exp_month='{$exp_month}', exp_year='{$exp_year}', csc='{$csc}'");
			reportAdmin("AS: Payment failed - mandatory fields not filled.", "payment_id={$payment_id}, cc_id='{$cc_id}', firstname='{$firstname}', lastname='{$lastname}', address='{$address}', city='{$city}', zip='{$zip}', email='{$email}', cardno='{$card_no}', exp_month='{$exp_month}', exp_year='{$exp_year}', csc='{$csc}'");
			$error = "Something is missing.";
			return false;
		}

		$res = $db->q("UPDATE payment SET result = 'S', processor = 'securionpay', sent_stamp = ? WHERE id = ? LIMIT 1", [$now, $payment_id]);

		//----------------
		//SecurionPay code
		//----------------

		$gateway = new SecurionPayGateway($config_securionpay_secret_key, $this->proxyConnection);

		//$csc_real ?
		$currency = "USD";
		$amount = number_format($amount, 2, ".", "");
		$amount_minor = round($amount * 100); //Charge amount in minor units of given currency (for USD in cents)

		$request_card = [];
		if ($token) {
			//we have card token from custom form (possible with 3d secure auth too)
			$request_card = $token;
		} else {
			$request_card = [
				"number" => $cardno_real,
				"expMonth" => $exp_month,
				"expYear" => $exp_year,
				"cvc" => $csc_real,
			];
		}

		$request = [
			"amount" => $amount_minor,
			"currency" => $currency,
			"card" => $request_card,
			"billing" => [
				"name" => trim($firstname." ".$lastname),
				"address" => [
					"line1" => $address,
					"zip" => $zip,
					"city" => $city,
					"state" => $state,
					"country" => $country,
					],
				],
			"captured" => $captured,
			"metadata" => [
				"site" => "AS",
				],
		];

		$request_log = $request;
		if (array_key_exists("card", $request_log) && is_array($request_log["card"]) && array_key_exists("number", $request_log["card"]))
			$request_log["card"]["number"] = "X".substr($request_log["card"]["number"], -4);
		if (array_key_exists("card", $request_log) && is_array($request_log["card"]) && array_key_exists("cvc", $request_log["card"]))
			$request_log["card"]["cvc"] = "XX".substr($request_log["card"]["cvc"], -1);
		file_log("securionpay", "securionpay::charge(): gonna charge, request='".print_r($request_log, true)."'");

		try {
			$charge = $gateway->createCharge($request);

		} catch (\Exception $e) {
			file_log("securionpay", "securionpay::charge(): API failed ! ".self::getErrorAdminDescription($e));

			$res = $db->q(
				"UPDATE payment SET result = ?, responded_stamp = ?, decline_code = ?, decline_reason = ? WHERE id = ? LIMIT 1",
				["D", $now, $e->getCode(), $e->getMessage(), $payment_id]
				);
			$aff = $db->affected($res);
			//if ($aff != 1) {
			//	reportAdmin("Payment failed - error: cant update payment", "cant update payment about transaction decline, payment_id={$payment_id}, now={$now}, aff={$aff}");
			//}
			$db->q("INSERT INTO account_cc_fail (account_id, stamp, payment_id) values (?, ?, ?)", [$account_id, $now, $payment_id]);

			$error = self::getErrorUserDescription($e);
			return false;
		}

		//charge succeeded
		$trans_id = $charge_id = $charge->getId();
		file_log("securionpay", "securionpay::charge(): success, charge_id='{$charge_id}'");

		if ($captured != $charge->getCaptured()) {
			file_log("securionpay", "securionpay::charge(): Error: requested captured=".intval($captured).", returned captured=".intval($charge->getCaptured())." !");
			reportAdmin("AS: securionpay::charge() Error", "payment_id={$payment_id}, requested captured=".intval($captured).", returned captured=".intval($charge->getCaptured())." !");
		}

		//processing response
		//https://securionpay.com/docs/api#charge-object
		$card_brand = null;
		if ($charge->getCard() && $charge->getCard()->getBrand())
			$card_brand = $charge->getCard()->getBrand();
		$resp = json_encode([
			"charge_id" => $charge->getId(),
			"created" => $charge->getCreated(),
			"card" => [
				"card_id" => $charge->getCard()->getId(),
				"created" => $charge->getCard()->getCreated(),
				"first6" => $charge->getCard()->getFirst6(),
				"last4" => $charge->getCard()->getLast4(),
				"fingerprint" => $charge->getCard()->getFingerprint(),
				"customerId" => $charge->getCard()->getCustomerId(),
				"brand" => $charge->getCard()->getBrand(),
				"type" => $charge->getCard()->getType(),
				],
			"customerId" => $charge->getCustomerId(),
			"captured" => $charge->getCaptured(),
			"metadata" => $charge->getMetadata(),
			]);

		//-------------------------
		//API requst was successful
		// handling is different depending whether it was recurrint or non-recurrent transaction
		$now = time();
		$ip_address = account::getUserIp();

		//store cc into safestore
		if (!safestore::hasEntryById("cc", $cc_id)) {
			$ret = safestore::addEntry("cc", $cc_id, ["cc" => $cardno_real, "cvc2" => $csc_real]);
			if (!$ret) {
				//notify admin, but dont panic
				file_log("securionpay", "securionpay::charge(): Error: Failed to store CC into safestore ! cc_id={$cc_id}");
				reportAdmin("AS: Failed to store CC into safestore", "cc_id={$cc_id}");
			}
		}


		//update card - making sure we store securionpay card id and securionpay customer id
		$securionpay_card_id = $charge->getCard()->getId();
		if (!$card->getSecurionpayCardId()) {
			$card->setSecurionpayCardId($securionpay_card_id);
		}
		$customer = null;
		if (!$card->getSecurionpayCustomerId()) {
			$customer = $this->getCustomerByEmail($email, $charge_id, $error);
			if ($customer)
				$card->setSecurionpayCustomerId($customer["id"]);
		} else {
			$customer = $this->getCustomerById($card->getSecurionpayCustomerId());
		}
		if (!$customer) {
			//this error is not critical at this point - just notify admin
			file_log("securionpay", "securionpay::charge(): Error: Failed to get securionpay customer: payment_id={$payment_id}, charge_id='{$charge_id}', email='{$email}', securionpay_customer_id='{$card->getSecurionpayCustomerId()}', error='{$error}'");
			reportAdmin("AS: Securionpay error - cant get customer", "Failed to get securionpay customer: payment_id={$payment_id}, charge_id='{$charge_id}', email='{$email}', securionpay_customer_id='{$card->getSecurionpayCustomerId()}', error='{$error}'");
		} else {
			//make sure customer has this card on file
			if (!$this->customerHasCard($customer, $securionpay_card_id, $charge_id)) {
				//this error is not critical at this point - just notify admin
				file_log("securionpay", "securionpay::charge(): Error: Failed to add securionpay card to securionpay customer: payment_id={$payment_id}, charge_id='{$charge_id}', email='{$email}', securionpay_customer_id='{$card->getSecurionpayCustomerId()}', card_id='{$securionpay_card_id}', error='{$error}'");
				reportAdmin("AS: Securionpay error - cant assign card to customer", "Failed to add securionpay card to securionpay customer: payment_id={$payment_id}, charge_id='{$charge_id}', email='{$email}', securionpay_customer_id='{$card->getSecurionpayCustomerId()}', card_id='{$securionpay_card_id}', error='{$error}'");
			}
		}
		$card->setType($this->getCardTypeByName($card_brand));
		$card->update();

		//update payment	
		$result = null;
		if ($charge->getCaptured())
			$result = "A";
		else
			$result = "U";
		$res = $db->q(
			"UPDATE payment 
			SET result = ?, response = ?, responded_stamp = ?, decline_code = null, decline_reason = null, ip_address = ?, trans_id = ?
			WHERE id = ? 
			LIMIT 1",
			[$result, $resp, $now, $ip_address, $trans_id, $payment_id]
			);
		$aff = $db->affected($res);
		if ($aff != 1) {
			reportAdmin("Payment succeeded - error: cant update payment", "cant update payment about transaction success, payment_id={$payment_id}, now={$now}, aff={$aff}");
		}

		//add transaction as well if charge was captured
		if ($charge->getCaptured()) {
			$res = $db->q(
				"INSERT INTO transaction (type, payment_id, stamp, trans_id, amount) VALUES (?, ?, ?, ?, ?)",
				["N", $payment_id, $now, $trans_id, $amount]
				);
			$transaction_id = $db->insertid($res);
			if (!$transaction_id) {
				reportAdmin("AS: Payment succeeded - error: cant insert transaction", "cant insert transaction into db, payment_id={$payment_id}, trans_id={$trans_id}");
			}
		}

		return true;
	}

	public function capture($charge_id, &$error) {
		global $db, $config_securionpay_secret_key;

		$now = time();

		file_log("securionpay", "capture: going to capture charge_id='{$charge_id}'");

		//----------------
		//SecurionPay code
		//----------------

		$gateway = new SecurionPayGateway($config_securionpay_secret_key, $this->proxyConnection);

		$request = array(
			'chargeId' => $charge_id,
		);

		try {
			$charge = $gateway->captureCharge($request);
		} catch (\Exception $e) {
			file_log("securionpay", "capture: API failed ! ".self::getErrorAdminDescription($e));
			reportAdmin("AS: Securionpay API failed !", self::getErrorAdminDescription($e));
			$error = self::getErrorUserDescription($e);
			return false;
		}

		//capture succeeded
		$trans_id = $charge_id = $charge->getId();
		file_log("securionpay", "capture: success, charge_id='{$charge_id}'");

		return true;
	}

	public function retrieve_charge($charge_id, &$error) {
		global $config_securionpay_secret_key;

		file_log("securionpay", "securionpay::retrieve_charge(): charge_id={$charge_id}");

		//----------------
		//SecurionPay code
		//----------------

		$gateway = new SecurionPayGateway($config_securionpay_secret_key, $this->proxyConnection);

		try {
			$charge = $gateway->retrieveCharge($charge_id);
		} catch (\Exception $e) {
			file_log("securionpay", "securionpay::retrieve_charge(): API failed ! ".self::getErrorAdminDescription($e));
			reportAdmin("AS: Securionpay API failed !", "retrieve_charge(): charge_id={$charge_id}, ".self::getErrorAdminDescription($e));
			$error = self::getErrorUserDescription($e);
			return false;
		}
		
		//request succeeded
		$refund_charge_id = $charge->getId();
		file_log("securionpay", "securionpay::retrieve_charge(): success, captured=".$charge->getCaptured().", refunded=".$charge->getRefunded().", disputed=".$charge->getDisputed());

		return $charge;
	}

	public function refund_charge($charge_id, $amount = null, &$error) {
		global $config_securionpay_secret_key;

		file_log("securionpay", "securionpay::refund_charge(): charge_id={$charge_id}, amount={$amount}");

		//first check if charge is not disputed (chargeback)
		$charge = $this->retrieve_charge($charge_id, $error);
		if (!$charge) {
			file_log("securionpay", "securionpay::refund_charge(): retrieve_charge failed: error='{$error}'");
			return false;
		}
		if ($charge->getDisputed()) {
			file_log("securionpay", "securionpay::refund_charge(): cannot refund charge, because it is already disputed (chargeback)");
			$error = "Cannot refund charge, because it is already disputed (chargeback)";
			return false;
		}

		//----------------
		//SecurionPay code
		//----------------

		$gateway = new SecurionPayGateway($config_securionpay_secret_key, $this->proxyConnection);

		$request = [
			"chargeId" => $charge_id,
			];
		if ($amount) {
			$amount_minor = round($amount * 100);   //Charge amount in minor units of given currency (for USD in cents)
			$request["amount"] = $amount_minor;
		}

		try {
			$charge = $gateway->refundCharge($request);
		} catch (\Exception $e) {
			file_log("securionpay", "securionpay::refund_charge(): API failed ! ".self::getErrorAdminDescription($e));
			reportAdmin("AS: Securionpay API failed !", "refund_charge(): charge_id={$charge_id}, amount={$amount}, ".self::getErrorAdminDescription($e));
			$error = self::getErrorUserDescription($e);
			return false;
		}
		
		//request succeeded
		$refund_charge_id = $charge->getId();
		file_log("securionpay", "securionpay::refund_charge(): success, refund_charge_id={$refund_charge_id}");

		return [
			"refund_transaction_id" => $refund_charge_id,
			];
	}

	/**
	 * As a card_token, actually charge_id can be used too
	 */
	public function getCustomerByEmail($email, $card_token_or_charge_id, &$error) {
		global $db, $config_securionpay_secret_key;

		file_log("securionpay", "getCustomerByEmail: email={$email}, card_token_or_charge_id={$card_token_or_charge_id}");

		//----------------
		//SecurionPay code
		//----------------
		$gateway = new SecurionPayGateway($config_securionpay_secret_key, $this->proxyConnection);
		$request = [
			"email" => $email
		];
		try {
			$list = $gateway->listCustomers($request);
		} catch (\Exception $e) {
			file_log("securionpay", "getCustomerByEmail: API failed ! ".self::getErrorAdminDescription($e));
			reportAdmin("AS: Securionpay API failed !", "securionpay::getCustomerByEmail: ".self::getErrorAdminDescription($e));
			$error = self::getErrorUserDescription($e);
			return false;
		}

		//request succeeded
		file_log("securionpay", "getCustomerByEmail: resp=".print_r($list, true));
		$list = $list->toArray();
		$list = $list["list"];
		$count=count($list);
		
		if ($count > 1) {
			//more than one customer with such email ?
			//i *THINK* this should not happen, lets notify admin to fix/adjust the code
			file_log("securionpay", "getCustomerByEmail: Error: Too many results: {$count}");
			reportAdmin("AS: Securionpay API failed !", "securionpay::getCustomerByEmail: error: too many results, email={$email}, count={$count}");
			$error = "Can not find customer, please contact administrator of the website";
			return false;
		} else if ($count == 1) {
			//customer found
			$customer = array_shift($list);
			$customer_id = $customer["id"];
			file_log("securionpay", "getCustomerByEmail: found: customer_id={$customer_id}");
			return $customer;
		}

		//customer with such email not found
		//create customer
		$error = null;
		$customer = $this->createCustomer($email, $card_token_or_charge_id, $error);
		if (!$customer)
			return false;
		return $customer;	
	}

	public function getCustomerById($securionpay_customer_id) {
		global $db, $config_securionpay_secret_key;

		file_log("securionpay", "getCustomerById: securionpay_customer_id='{$securionpay_customer_id}'");

		//----------------
		//SecurionPay code
		//----------------
		$gateway = new SecurionPayGateway($config_securionpay_secret_key, $this->proxyConnection);
		//$request = [
		//	"customerId" => $securionpay_customer_id
		//];
		try {
			$customer = $gateway->retrieveCustomer($securionpay_customer_id);
		} catch (\Exception $e) {
			file_log("securionpay", "getCustomerById: API failed ! ".self::getErrorAdminDescription($e));
			reportAdmin("AS: Securionpay API failed !", "securionpay::getCustomerById(): ".self::getErrorAdminDescription($e));
			$error = self::getErrorUserDescription($e);
			return false;
		}

		//request succeeded
		$customer = $customer->toArray();
		file_log("securionpay", "getCustomerById: success");
		return $customer;
	}

	public function createCustomer($email, $card_token_or_charge_id, &$error) {
		global $config_securionpay_secret_key;

		file_log("securionpay", "createCustomer: email={$email}, card_token_or_charge_id={$card_token_or_charge_id}");

		//----------------
		//SecurionPay code
		//----------------
		$gateway = new SecurionPayGateway($config_securionpay_secret_key, $this->proxyConnection);
		$request = [
			"email" => $email,
			"card" => $card_token_or_charge_id,
			"metadata" => [
				"site" => "AS",
				],
		];

		try {
			$customer = $gateway->createCustomer($request);

		} catch (\Exception $e) {
			file_log("securionpay", "createCustomer: API failed ! ".self::getErrorAdminDescription($e));
			reportAdmin("AS: Securionpay API failed !", "createCustomer failed: email='{$email}', card_token_or_charge_id='{$card_token_or_charge_id}', ".self::getErrorAdminDescription($e));
			$error = "Can't create customer, please contact website administrator";
			return false;
		}

		//customer created
		$customer = $customer->toArray();
		$customer_id = $customer["id"];
		file_log("securionpay", "createCustomer: customer_id={$customer_id}, resp=".print_r($customer, true));

		return $customer;
	}

	public function customerHasCard($customer, $securionpay_card_id, $charge_id) {
		global $db, $config_securionpay_secret_key;

		if (!is_array($customer) || !array_key_exists("id", $customer) || !$securionpay_card_id) {
			//invalid input
			file_log("securionpay", "customerHasCard: Error: invalid_input: customer=\n".var_export($customer, true)."\n, securionpay_card_id='{$securionpay_card_id}', charge_id='{$charge_id}'");
			reportAdmin("AS: Securionpay:customerHasCard error", "Error: invalid_input: customer=<br />".var_export($customer, true)."<br />\n, securionpay_card_id='{$securionpay_card_id}', charge_id='{$charge_id}'");
			return false;
		}

		file_log("securionpay", "customerHasCard: securionpay_customer_id={$customer["id"]}, securionpay_card_id='{$securionpay_card_id}', charge_id='{$charge_id}'");

		if (array_key_exists("cards", $customer)) {
			foreach ($customer["cards"] as $card_array) {
				if ($card_array["id"] == $securionpay_card_id) {
					//card is already on file with this customer
					file_log("securionpay", "customerHasCard: result=true");
					return true;
				}
			}
		}

		file_log("securionpay", "customerHasCard: card not on customer's file (yet)");

		//card is not on file with this customer -> let's add this card to customers file
		$customer2 = $this->updateCustomer($customer["id"], null, $charge_id);

		//let's doublecheck card is there now
		foreach ($customer2["cards"] as $card_array) {
			if ($card_array["id"] == $securionpay_card_id) {
				file_log("securionpay", "customerHasCard: result=true");
				return true;
			}
		}

		//this should not happen
		file_log("securionpay", "customerHasCard: Error: cant put card '{$securionpay_card_id}' on file with customer '{$customer2["id"]}' !");
		reportAdmin("AS: Securionpay:customerHasCard error", "Error: cant put card '{$securionpay_card_id}' on file with customer '{$customer2["id"]}'");
		return false;
	}

	/**
	 * Either $email or $charge_id needs to be filled
	 */
	public function updateCustomer($securionpay_customer_id, $email, $charge_id) {
		global $db, $config_securionpay_secret_key;

		file_log("securionpay", "updateCustomer: securionpay_customer_id='{$securionpay_customer_id}', email='{$email}', charge_id='{$charge_id}'");

		//----------------
		//SecurionPay code
		//----------------
		$gateway = new SecurionPayGateway($config_securionpay_secret_key, $this->proxyConnection);
		$request = [
			"customerId" => $securionpay_customer_id,
			"metadata" => [
				"site" => "AS",
				],
		];
		if ($email)
			$request["email"] = $email;
		if ($charge_id)
			$request["card"] = $charge_id;

		try {
			$customer = $gateway->updateCustomer($request);
		} catch (\Exception $e) {
			file_log("securionpay", "updateCustomer: API failed ! ".self::getErrorAdminDescription($e));
			reportAdmin("AS: Securionpay API failed !", "updateCustomer failed: customerId='{$securionpay_customer_id}', email='{$email}', charge_id='{$charge_id}', ".self::getErrorAdminDescription($e));
			return false;
		}

		//all should be good, customer updated
		$customer = $customer->toArray();
		return $customer;
	}

	public function rebill($payment_id, $amount, $card, &$error) {
		global $db, $config_securionpay_secret_key;

		$now = time();
		
		file_log("securionpay", "rebill(): payment_id={$payment_id}, card_id={$card->getId()}, securionpay_card_id='{$card->getSecurionpayCardId()}', amount={$amount}");

		//check if we have recurring amount specified and card has card id and customer id assigned
		if (!$amount) {
			file_log("securionpay", "rebill: Error: Payment is not specified as recurring, payment_id={$payment_id}");
			reportAdmin("AS: Rebill Error", "Payment is not specified as recurring, payment_id={$payment_id}");
			$error = "Payment is not specified as recurring";
			return false;
		}

		//check if card has card_id and customer_id
		//this could only happen for historic purchases, in case card doesnt have these set, we need to do it now
		$customer = $charge_id = null;
		if (!$card->getSecurionpayCardId() || !$card->getSecurionpayCustomerId()) {

			$res = $db->q("
				SELECT p.email, p.response, t.trans_id
				FROM payment p
				INNER JOIN transaction t on t.payment_id = p.id
				WHERE p.id = ? 
				ORDER BY t.id DESC
				LIMIT 1",
				[$payment_id]
				);
			if ($db->numrows($res) != 1) {
				file_log("securionpay", "rebill: Error, can't fetch payment #{$payment_id} from db !");
				$error = "Can't fetch payment #{$payment_id} from database !";
				return false;
			}
			$row = $db->r($res);
			$email = $row["email"];
			$response = $row["response"];
			$charge_id = $row["trans_id"];

			if (!$card->getSecurionpayCardId()) {
				//filling securionpay card id
				if (!$response) {
					$error = "securionpay_card_id not set but payment resp also not set";
					reportAdmin("AS: securionpay::rebill: Error", "payment_id={$payment_id}, securionpay_card_id not set but payment resp also not set");
					return false;
				}
				$resp_arr = json_decode($response, true);
				if (!$resp_arr) {
					$error = "securionpay_card_id not set but payment resp invalid JSON";
					reportAdmin("AS: securionpay::rebill: Error", "payment_id={$payment_id}, securionpay_card_id not set but payment resp invalid JSON");
					return false;
				}
				if (!array_key_exists("card", $resp_arr) || !array_key_exists("card_id", $resp_arr["card"])) {
					$error = "securionpay_card_id not set but payment resp invalid JSON 2";
					reportAdmin("AS: securionpay::rebill: Error", "payment_id={$payment_id}, securionpay_card_id not set but payment resp invalid JSON 2");
					return false;
				}
				$securionpay_card_id = $resp_arr["card"]["card_id"];
				if (!$securionpay_card_id) {
					$error = "securionpay_card_id not set but payment resp invalid JSON 3";
					reportAdmin("AS: securionpay::rebill: Error", "payment_id={$payment_id}, securionpay_card_id not set but payment resp invalid JSON 3");
					return false;
				}
				$card->setSecurionpayCardId($securionpay_card_id);
			}
	
			if (!$card->getSecurionpayCustomerId()) {
				$customer = $this->getCustomerByEmail($email, $charge_id, $error);
				if ($customer)
					$card->setSecurionpayCustomerId($customer["id"]);
			}
		}
		$card->update();

		if (!$customer && $card->getSecurionpayCustomerId())
			$customer = $this->getCustomerById($card->getSecurionpayCustomerId());

		if (!$customer) {
			//this error is critical at this point
			file_log("securionpay", "rebill: Error: Failed to get securionpay customer: payment_id={$payment_id}, securionpay_customer_id='{$card->getSecurionpayCustomerId()}', error='{$error}'");
			reportAdmin("AS: Securionpay error - cant get customer", "securionpay::rebill(): Failed to get securionpay customer: payment_id={$payment_id}, securionpay_customer_id='{$card->getSecurionpayCustomerId()}', error='{$error}'");
			$error = "cant get securionpay customer";
			return false;
		}

		//make sure customer has this card on file
		if (!$this->customerHasCard($customer, $card->getSecurionpayCardId(), $charge_id)) {
			//this error is critical at this point
			file_log("securionpay", "rebill: Error: Failed to add securionpay card to securionpay customer: payment_id={$payment_id}, charge_id='{$charge_id}', email='{$email}', securionpay_customer_id='{$card->getSecurionpayCustomerId()}', card_id='{$securionpay_card_id}', error='{$error}'");
			reportAdmin("AS: Securionpay error - cant assign card to customer", "Failed to add securionpay card to securionpay customer: payment_id={$payment_id}, charge_id='{$charge_id}', email='{$email}', securionpay_customer_id='{$card->getSecurionpayCustomerId()}', card_id='{$securionpay_card_id}', error='{$error}'");
			$error = "cant assign card '{$card->getSecurionpayCardId()}' to customer '{$customer["id"]}' !";
			return false;
		}

		//----------------
		//SecurionPay code
		//----------------
		$gateway = new SecurionPayGateway($config_securionpay_secret_key, $this->proxyConnection);

		$currency = "USD";
		$amount_minor = round($amount * 100);	//Charge amount in minor units of given currency (for USD in cents)
		$amount = number_format($amount, 2, ".", "");

		$request = [
			"amount" => $amount_minor,
			"currency" => $currency,
			"customerId" => $card->getSecurionpayCustomerId(),
			"card" => $card->getSecurionpayCardId(),
			"billing" => [
				"name" => trim($card->getFirstname()." ".$card->getLastname()),
				"address" => [
					"line1" => $card->getAddress(),
					"zip" => $card->getZipcode(),
					"city" => $card->getCity(),
					"state" => $card->getState(),
					"country" => $card->getCountry(),
					],
				],
			"captured" => true,
			"metadata" => [
				"site" => "AS",
				],
		];

		file_log("securionpay", "rebill(): gonna charge, request='".print_r($request, true)."'");

		try {
			$charge = $gateway->createCharge($request);

		} catch (\Exception $e) {
			file_log("securionpay", "rebill: API failed ! ".self::getErrorAdminDescription($e));

			//update decline code/message
			$errorCode = $e->getCode();
			$errorMessage = $e->getMessage();
			$resp = "errorCode='{$errorCode}', errorMessage='{$errorMessage}'";
			$res = $db->q("
				UPDATE payment 
				SET subscription_status = 2, decline_code = ?, decline_reason = ?, last_rebill_response = ? 
				WHERE id = ? 
				LIMIT 1",
				[$errorCode, $errorMessage, $resp, $payment_id]
				);
			$aff = $db->affected($res);
			if ($aff != 1)
				reportAdmin("AS: Securionpay rebill failed - cant update payment", "cant update payment, resp='{$resp}', payment_id={$payment_id}, ".self::getErrorAdminDescription($e));

			$error = self::getErrorUserDescription($e);
			return false;
		}

		//charge succeeded
		$trans_id = $charge_id = $charge->getId();

		file_log("securionpay", "rebill: Success, charge_id='{$charge_id}'");

		//processing response
		//https://securionpay.com/docs/api#charge-object
		$resp = json_encode([
			"charge_id" => $charge->getId(),
			"created" => $charge->getCreated(),
			"card" => [
				"card_id" => $charge->getCard()->getId(),
				"created" => $charge->getCard()->getCreated(),
				"first6" => $charge->getCard()->getFirst6(),
				"last4" => $charge->getCard()->getLast4(),
				"fingerprint" => $charge->getCard()->getFingerprint(),
				"customerId" => $charge->getCard()->getCustomerId(),
				"brand" => $charge->getCard()->getBrand(),
				"type" => $charge->getCard()->getType(),
				],
			"customerId" => $charge->getCustomerId(),
			"captured" => $charge->getCaptured(),
			"metadata" => $charge->getMetadata(),
			]);

		//-------------------------
		//API requst was successful
		// handling is different depending whether it was recurrint or non-recurrent transaction

		//update payment
		$res = $db->q("UPDATE payment SET subscription_status = 1, last_rebill_response = ? WHERE id = ? LIMIT 1", [$resp, $payment_id]);
		$aff = $db->affected($res);
		if ($aff != 1)
			reportAdmin("AS: Payment succeess but cant update payment", "cant update payment, resp='{$resp}', payment_id={$payment_id}");

		//add transaction
		$res = $db->q(
			"INSERT INTO transaction (type, payment_id, stamp, trans_id, amount) VALUES (?, ?, ?, ?, ?)",
			["N", $payment_id, $now, $trans_id, $amount]
			);
		$transaction_id = $db->insertid($res);
		if (!$transaction_id)
			reportAdmin("AS: Payment succeess but cant insert transaction", "cant insert transaction in db, payment_id={$payment_id}, trans_id={$trans_id}");

		return $transaction_id;
	}

	public function getCardTypeByName($name) {
		$type = "";
		switch (strtolower($name)) {
			case "visa": $type = "VI"; break;
			case "mastercard": $type = "MC"; break;
			case "discover": $type = "DI"; break;
			case "jcb": $type = "JC"; break;
			case "maestro": $type = "MA"; break;
			case "americanexpress":
			case "american express":
				$type = "AM";
				break;
			case "unknown": $type = NULL; break;
			default:
				reportAdmin("AS: securionpay:getCardTypeByName(): Unknown card type name", "cardType = '{$name}'");
				$type = "OT";
				break;
		}
		return $type;
	}

	/**
	 * returns admin description of error exception
	 * see https://securionpay.com/docs/api#error-object
	 */
	public static function getErrorAdminDescription(\Exception $e) {
		$desc = get_class($e).": ";
		$desc .= "code='{$e->getCode()}' message='{$e->getMessage()}' ";
		if ($e instanceof \SecurionPay\Exception\SecurionPayException) {
			$desc .= "type='{$e->getType()}' chargeId='{$e->getChargeId()}' blacklistRuleId='{$e->getBlacklistRuleId()}' ";
		}
		return $desc;
	}

	/**
	 * Returns user description of error exception
	 * see https://securionpay.com/docs/api#error-object
	 */
	public static function getErrorUserDescription(\Exception $e) {
		$code = $e->getCode();
		$message = $e->getMessage();

		if ($e instanceof \SecurionPay\Exception\ConnectionException)
			return "Connection error, please try again later";

		if ($code == "card_declined" || $code == "suspected_fraud")
            return "Your card was declined. It is very likely that your bank declined the transaction because they suspect that someone else is using your card.  This is because the credit card processor that we are using is based outside of the USA.  You need to either call your bank or login to your online banking and confirm that everything is ok with your card and that you confirm the charges are accepted.  After contacting your bank, you will need to go through the payment process again.";

        if ($code == "processing_error") {
            if (preg_match('/^Your account is not configured.*$/', $message)) {
                if (preg_match('/AMEX/', $message))
                    return "We don't process American Express credit cards. Please use different credit card.";
                return "We don't process this type of credit cards. Please use different credit card.";
            }
            return "An error occured while processing your credit card. Please try again later and/or contact us at support@adultsearch.com.";
        }

        if ($code == "incorrect_cvc" || $code == "invalid_cvc")
            return "The card's security code failed verification. Please use correct CVC security code and submit payment again.";

        if ($code == "lost_or_stolen")
            return "The card is marked as lost or stolen. Please use different credit card.";

        return $message;
    }

}

//END 
