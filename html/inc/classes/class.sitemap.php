<?php

class sitemap {

	var $mapdir = "sitemap";
	var $sitemap = "sitemap.xml";
	var $file_limit = 10000; // google's link limit per sitemap 

	var $map;
	var $total_item = 0;
	var $file_number = 0; // how many total file we are creating

	public function __construct() {
	}

	// should only be called when we need to create the new files...
	private function cleancache() {
		$exec = "rm -f ". _CMS_ABS_PATH."/{$this->mapdir}/*.xml";
		exec($exec);
	}

	private function addToMap($link, $lastmod) {
		$this->map[] = array("link" => $link, "lastmod" => $lastmod);
		$this->total_item++;
		if (count($this->map) % $this->file_limit == 0)
			$this->createmap();
	}
	
	private function createmap() {
		global $smarty;

		$dir = _CMS_ABS_PATH."/{$this->mapdir}";
		if (!is_dir($dir)) {
			$ret = mkdir($dir);
			if (!$ret) {
				reportAdmin("inc/classes/class.sitemap.php createmap() create dir '{$dir}' failed...");
				die("inc/classes/class.sitemap.php createmap() create dir '{$dir}' failed...");
			}
		}

		$smarty->assign("map", $this->map);
		$this->file_number++;
		$cache_file2 = "{$dir}/sitemap_{$this->file_number}.xml";
		$fetch = $smarty->fetch(_CMS_ABS_PATH."/templates/sitemap/sitemap_item.tpl");
		$handle = fopen($cache_file2, "w");
		if (!$handle) {
			reportAdmin("inc/classes/class.sitemap.php createmap() fopen failed...");
			die("inc/classes/class.sitemap.php createmap() fopen failed...");
		}
		fputs($handle, $fetch);
		fclose($handle);
		$this->map = array();
	}
	

	public function createsitemap() {
		global $db; 
		$classifieds = new classifieds;

		$this->cleancache();
		$this->map = array();

		$count = 0;
		$res = $db->q("SELECT l.country_sub, l.loc_url, l.dir FROM location_location l WHERE l.has_place_or_ad = 1");
		while ($row=$db->r($res)) {
			$link = rtrim(location::getUrlStatic($row['country_sub'], $row['loc_url'], $row['dir']), "/");
			$this->addToMap($link, date("c"));
			$count++;
		}
		echo "locations: {$count}\n";

		// classifieds...
		$count = 0;
		$res = $db->q("SELECT c.id, c.type, date_format(cl.updated, '%Y-%m-%d') as lastmod, l.country_sub, l.loc_url, l.dir
						FROM `classifieds_loc` cl
						INNER JOIN classifieds c on cl.post_id = c.id 
						INNER JOIN location_location l on l.loc_id = cl.loc_id
						WHERE cl.done = 1 AND c.deleted IS NULL");
		while ($row=$db->r($res)) {
			$cat_url = $classifieds->getModuleByType($row['type']);
			$link = location::getUrlStatic($row['country_sub'], $row['loc_url'], $row['dir'])."{$cat_url}/{$row['id']}";
			$this->addToMap($link, $row['lastmod']);
			$count++;
		}
		echo "classified ad pages: {$count}\n";

		// places...
		$count = 0;
		$res = $db->q("
			SELECT p.place_id, p.name, pt.url, l.country_sub, l.loc_url, l.dir,
				CASE 
					WHEN p.updated <> NULL THEN DATE_FORMAT(FROM_UNIXTIME(p.updated), '%Y-%m-%d') 
					WHEN p.created <> NULL THEN DATE_FORMAT(p.created, '%Y-%m-%d') 
					ELSE '2014-01-01' END AS lastmod
			FROM place p
			INNER JOIN place_type pt on pt.place_type_id = p.place_type_id
			INNER JOIN location_location l ON l.loc_id = p.loc_id
			WHERE p.edit = 1 AND p.deleted IS NULL");
		while ($row = $db->r($res)) {
			$link = location::getUrlStatic($row['country_sub'], $row['loc_url'], $row['dir'])."{$row["url"]}/".name2url($row['name'])."/{$row['place_id']}";
			$this->addToMap($link, $row['lastmod']);
			$count++;
		}
		echo "place view pages: {$count}\n";

		if (count($this->map) > 0)
			$this->createmap();

		$this->finishup();

		echo "total item wrote: {$this->total_item} into {$this->file_number} files...\n";

		//mark in operation log
		$now = time();
		$op_msg = "Total urls: {$this->total_item} (in {$this->file_number} files)";
		$db->q("INSERT INTO admin_operation_log (code, stamp, msg) values ('last_sitemap_generated', ?, ?) on duplicate key update stamp = ?, msg = ?", array($now, $op_msg, $now, $op_msg));
	}

	private function finishup() {
		global $smarty;
		$map = NULL;
		$id = 1;
		while($id <= $this->file_number ) {
			$map[] = array("map"=>"/{$this->mapdir}/sitemap_{$id}.xml");
			$id++;
		}

		if ($map) {
			$smarty->assign("map", $map);
			$fetch = $smarty->fetch(_CMS_ABS_PATH."/templates/sitemap/sitemap_map.tpl");
			$handle = fopen(_CMS_ABS_PATH."/{$this->mapdir}/{$this->sitemap}", "w");
			if( !$handle ) {
				reportAdmin("inc/classes/class.sitemap.php finishup() fopen failed...");
				die("inc/classes/class.sitemap.php finishup() fopen failed...");
			}
			fputs($handle, $fetch);
			fclose($handle);
		} else {
			reportAdmin("sitemap class finishup no actual map file ?");
			die("sitemap class finishup no actual map file ?");
		}
	}
}

//END
