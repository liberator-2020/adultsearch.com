<?php

class sms {
	var $api = "8bb617c04aae74a228c0f359c476d897";
	var $list = "AS";

	function send($phone, $msg) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);

		$id = $this->addtolist($phone, $error);
		if( $id < 1 ) return -1;

		$param = "APIKEY=" . urlencode($this->api);
		$param .= "&SMSMode=" . urlencode("TRUE");
		$param .= "&EmailMode=" . urlencode("FALSE");
		$param .= "&IMMode=" . urlencode("FALSE");
		$param .= "&SBMode=" . urlencode("FALSE");
		$param .= "&Description=" . urlencode("AdultSearch.com");
		$param .= "&SMSMessage=" . urlencode($msg);
		$param .= "&ContactIDs=" . urlencode($id);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
		curl_setopt($ch, CURLOPT_URL, "http://trumpia.com/api/sendtocontact.php");
		$xml = simplexml_load_string(curl_exec($ch));

		if (current($xml->xpath("/TRUMPIA/STATUSCODE")) == "1") {
			return current($xml->xpath("/TRUMPIA/STATUSCODE"));
		} else {
			return current($xml->xpath("/TRUMPIA/STATUSCODE")) . " - " . current($xml->xpath("/TRUMPIA/ERRORCODE")) . " - " . current($xml->xpath("/TRUMPIA/ERRORMESSAGE"));
		}
	}

	function addtolist($phone, &$error = NULL) {
	  	$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		$param = "APIKEY=" . urlencode($this->api);
		$param .= "&FirstName=" . urlencode("a");
		$param .= "&LastName=" . urlencode("b");
		$param .= "&Email=" . urlencode("");
		$param .= "&CountryCode=" . urlencode("1");
		$param .= "&MobileNumber=" . urlencode($phone);
		$param .= "&ListName=" . urlencode($this->list);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
		curl_setopt($ch, CURLOPT_URL, "http://trumpia.com/api/addcontact.php");
		
		$xml = simplexml_load_string(curl_exec($ch));

		if (current($xml->xpath("/TRUMPIA/STATUSCODE")) == "1") {
			return current($xml->xpath("/TRUMPIA/CONTACTID"));
			}
		else {
			if( current($xml->xpath("/TRUMPIA/ERRORCODE")) == 11 ) { $error = "This number is blocked"; return -1; }
			return $this->getcontactid($phone, $error);
		}
	}

	function getcontactid($phone, &$error) {
			$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER , TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		$param = "APIKEY=" . urlencode($this->api);
		$param .= "&ToolType=2";
		$param .= "&ToolData=" . urlencode($phone);
		$param .= "&ListName=" . urlencode($this->list);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
		curl_setopt($ch, CURLOPT_URL, "http://trumpia.com/api/getcontactid.php");
		
		$xml = simplexml_load_string(curl_exec($ch));

		if (current($xml->xpath("/TRUMPIA/STATUSCODE")) == "1") {
			return current($xml->xpath("/TRUMPIA/CONTACTID"));
			}
		else {
			$error = current($xml->xpath("/TRUMPIA/ERRORCODE")) . " - " . current($xml->xpath("/TRUMPIA/ERRORMESSAGE"));
			return -1;
		}
	}
}

//END
