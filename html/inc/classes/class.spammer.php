<?php

class spammer {

	private $id = NULL,
			$email = NULL,
			$password = null,
			$ip_address= null;
			
	function __construct() {
	}

	public static function findOneById($id) {
		global $db;

		$res = $db->q("SELECT * FROM admin_spammer WHERE id = ?", array(intval($id)));
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

 		return self::withRow($row);
	}

	public static function findOneByEmail($email) {
		global $db;

		$res = $db->q("SELECT * FROM admin_spammer WHERE email like ?", [$email]);
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

 		return self::withRow($row);
	}

	public static function findOneByIp($ip_address) {
		global $db;

		$res = $db->q("SELECT * FROM admin_spammer WHERE ip_address like ?", [$ip_address]);
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

 		return self::withRow($row);
	}

	public static function findOneByPassword($password) {
		global $db;

		$res = $db->q("SELECT * FROM admin_spammer WHERE password like ?", [$password]);
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

 		return self::withRow($row);
	}

	public static function withRow($row) {

		$as = new spammer();
		$as->setId($row["id"]);
		$as->setEmail($row["email"]);
		$as->setIpAddress($row["ip_address"]);
		$as->setPassword($row["password"]);
		
		return $as;
	}

	//getters and setters
	public function getId() {
		return $this->id;
	}
	public function getEmail() {
		return $this->email;
	}
	public function getIpAddress() {
		return $this->ip_address;
	}
	public function getPassword() {
		return $this->password;
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	public function setEmail($email) {
		$this->email = $email;
	}
	public function setIpAddress($ip_address) {
		$this->ip_address = $ip_address;
	}
	public function setPassword($password) {
		$this->password = $password;
	}

	
	public function add() {
		global $db, $account;

		$res = $db->q("INSERT INTO admin_spammer (email, ip_address, password) VALUES (?, ?, ?);", 
			array($this->email, $this->ip_address, $this->password));
		$newid = $db->insertid($res);

		if ($newid == 0)
			return false;

		$msg = [];
		if ($this->email)
			$msg[] = "Email={$this->email}";
		if ($this->ip_address)
			$msg[] = "IP={$this->ip_address}";
		if ($this->password)
			$msg[] = "Password={$this->password}";
		$msg = implode(", ", $msg);

		$this->id = $newid;
		audit::log("SPA", "New", $this->id, $msg, $account->getId());

		return true;
	}

	public function update() {
		global $account, $db;

		if (!$this->id)
			return false;

		$original = spammer::findOneById($this->id);
		if (!$original)
			return false;

		$audit_msg = "";

		//update ad fields
		$update_fields = array();
		if ($this->email != $original->getEmail())
			$update_fields["email"] = $this->email;
		if ($this->ip_address != $original->getIpAddress())
			$update_fields["ip_address"] = $this->ip_address;
		if ($this->password != $original->getPassword())
			$update_fields["password"] = $this->password;
		if (!empty($update_fields)) {
			$changed_fields = "";
			$update = "";
			$update_arr = array();
			foreach ($update_fields as $key => $val) {
				$changed_fields .= (empty($changed_fields)) ? "" : ", ";
				$update .= (empty($update)) ? "SET " : ", ";
				$changed_fields .= $key;
				$update .= "{$key} = ?";
				$update_arr[] = $val;
			}
			$update_arr[] = $this->id;
			$update = "UPDATE admin_spammer {$update} WHERE id = ? LIMIT 1";

			$db->q($update, $update_arr);
			$aff = $db->affected();

			if (!$aff)
				return false;

			$audit_msg = "Changed fields: {$changed_fields} ";
		}

		//if we changed anything, lets store audit message
		if (!empty($audit_msg))
			audit::log("SPA", "Edit", $this->id, $audit_msg, $account->getId());

		return true;
	}

	
	public function remove() {
		global $db, $account;

		$res = $db->q("DELETE FROM admin_spammer WHERE id = ? LIMIT 1", array($this->id));
		$aff = $db->affected($res);
		if ($aff == 1) {
			audit::log("SPA", "Remove", $this->id, "", $account->getId());
			return true;
		} else {
			return false;
		}
	}

	// user@domain.com -> @domain.com
	public static function getDomainPart($email) {
		$pos = strpos($email, "@");
		if ($pos === false)
			return false;
		$email_domain = substr($email, $pos);
		return $email_domain;
	}

	public static function isCurrentSpammer() {
		global $account, $db;

		$email_domain = self::getDomainPart($account->getEmail());
		if ($email_domain === false) {
			//this should not happen
			reportAdmin("AS: hammer::isCurrentHammer: Error: cant find @ in email of current user", "", 
				array("account_id" => $account->getId(), "email" => $account->getEmail()));
			return false;
		}

		$res = $db->q("SELECT id FROM admin_spammer WHERE email LIKE ?", array($email_domain));
		if ($db->numrows($res) > 0) {
			$row = $db->r($res);
			debug_log("Spammer test: TRUE ({$account->getEmail()}, spammer_id={$row["id"]})");
			reportAdmin("AS: hammer::isCurrentHammer: Caught hammer !", "", array("account_id" => $account->getId(), "email" => $account->getEmail()));
			return true;
		}

		return false;
	}

	public static function isEmailOnSpamList($email) {
		global $db;

		$email_domain = self::getDomainPart($email);
		if ($email_domain === false)
			return false;

		$res = $db->q("SELECT email FROM admin_spammer WHERE email like ? OR email like ?", [$email, $email_domain]);
		if ($db->numrows($res) > 0) {
			$row = $db->r($res);
			return $row["email"];
		}

		return false;
	}

	public static function isIpOnSpamList($ip_address) {
		global $db;

		$res = $db->q("SELECT * FROM admin_spammer WHERE ip_address like ?", [$ip_address]);
		if ($db->numrows($res) > 0)
			return true;

		return false;
	}

	public static function isPasswordOnSpamList($password) {
		global $db;

		$res = $db->q("SELECT * FROM admin_spammer WHERE password like ?", [$password]);
		if ($db->numrows($res) > 0)
			return true;

		return false;
	}

}

