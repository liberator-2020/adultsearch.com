<?php

/**
 * sphinx API wrapper
 */
class sphinx {

	var $sphinx_client = NULL;

	function __construct() {
	}

	private function init() {
		//if already initialized, dont do anything
		if ($this->sphinx_client != NULL)
			return;

		//include sphinx API file
		if (!is_object('SphinxClient'))
			require_once(_CMS_ABS_PATH."/inc/classes/class.sphinxapi.php");

		//create new instance of sphinx client
		$this->sphinx_client = new SphinxClient();

		//set some default values
		//connect timeout 5 seconds - this overrides default 60 sec timeout of php fsockopen - so webpage doesnt "hang" if sphinx server is maxed out
		$this->sphinx_client->SetConnectTimeout(5);
		$this->sphinx_client->SetLimits(0, 1000, 1000);
	}
	
	function __destruct() {
		if ($this->sphinx_client) {
			//close possible persistent connection
			$this->sphinx_client->Close();
		}
		$this->sphinx_client = NULL;
	}


	//----------------------
	// wrapped methods

	public function ResetFilter($name) {
		$this->init();
		return $this->sphinx_client->ResetFilter($name);
	}

	public function ResetFilters() {
		$this->init();
		return $this->sphinx_client->ResetFilters();
	}

	public function ResetGroupBy() {
		$this->init();
		return $this->sphinx_client->ResetGroupBy();
	}

	public function ResetAnchor() {
		$this->init();
		$this->sphinx_client->_anchor = array();
	}

	public function reset() {
		$this->init();
		$this->ResetFilters();
		$this->ResetGroupBy();
		$this->ResetAnchor();
		$this->SetSortMode(SPH_SORT_RELEVANCE, "");
		$this->SetLimits(0, 1000, 1000);
	}

	public function SetLimits($a, $b, $c) {
		$this->init();
		return $this->sphinx_client->SetLimits($a, $b, $c);
	}

	public function SetArrayResult($arr_result) {
		$this->init();
		return $this->sphinx_client->SetArrayResult($arr_result);
	}

	public function SetFilter($name, $values, $exclude = false) {
		$this->init();
		return $this->sphinx_client->SetFilter($name, $values, $exclude);
	}
	
	public function SetFilterRange($name, $min, $max, $exclude = false) {
		$this->init();
		return $this->sphinx_client->SetFilterRange($name, $min, $max, $exclude);
	}

	public function SetFilterFloatRange($name, $min, $max, $exclude = false) {
		$this->init();
		return $this->sphinx_client->SetFilterFloatRange($name, $min, $max, $exclude);
	}

	public function SetGeoAnchor($attr_lat, $attr_lon, $lat, $lon) {
		$this->init();
		return $this->sphinx_client->SetGeoAnchor($attr_lat, $attr_lon, $lat, $lon);
	}
	
	public function SetGroupBy($name, $type, $groupby) {
		$this->init();
		return $this->sphinx_client->SetGroupBy($name, $type, $groupby);
	}

	public function SetSortMode($type, $sortby) {
		$this->init();
		return $this->sphinx_client->SetSortMode($type, $sortby);
	}

	public function Query($query, $index) {
		$this->init();
		$this->sphinx_client->Open();
		return $this->sphinx_client->Query($query, $index);
	}

	public function AddQuery($query, $index) {
		$this->init();
		return $this->sphinx_client->AddQuery($query, $index);
	}
	
	public function RunQueries() {
		$this->init();
		$this->sphinx_client->Open();
		return $this->sphinx_client->RunQueries();
	}

	public function getLastError() {
		$this->init();
		return $this->sphinx_client->getLastError();
	}

	public function debugFilters() {
		foreach ($this->sphinx_client->_filters as $f) {
			_darr($f);
		}
	}
}

//END
