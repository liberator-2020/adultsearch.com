<?php

class stripe {

	private $stripeSuccessPayment = 0;
	private $tokenArr = [];
	private $errorType = '';
	private $errorCode;
	private $errorMessage = '';

	public function __construct() {
		global $config_stripe_secret_key;
		\Stripe\Stripe::setApiKey($config_stripe_secret_key);
	}

	public function get_name() {
		return "stripe";
	}

	public function get_template_path() {
		return _CMS_ABS_PATH."/templates/payment_gen_stripe.tpl";
	}

	public function export_to_template() {
		global $smarty, $config_stripe_public_key;
		$smarty->assign("stripe_public_key", $config_stripe_public_key);
	}

	//payment needs to contain unobfuscated card number and csc
	public function charge($params, $cardno_real, $csc_real, $captured = true, &$error) {

		global $db, $config_stripe_secret_key;

		$now = time();

		$payment_id = $params["payment_id"];
		$account_id = $params["account_id"];

		$card = $params["card"];

		$firstname = $card->getFirstname();
		$lastname = $card->getLastname();
		$address = $card->getAddress();
		$city = $card->getCity();
		$zip = $card->getZipcode();
		$state = $card->getState();
		$country = $card->getCountry();
		$card_no = $card->getCc();
		$exp_month = $card->getExpMonth();
		$exp_year = $card->getExpYear();
		$csc = $card->getCvc2();
		$token = $card->getStripeToken();
		$email = $params["email"];
		$cc_id = $card->getId();
		$amount = $params["amount"];
		$recurring_amount = $params["recurring_amount"];

		file_log("stripe", "stripe::charge(): payment_id={$payment_id}");

		//doublecheck completeness of params
		if (!$firstname
			|| !$lastname
			|| !$address
			|| !$city
			|| !$zip
			|| !$cardno_real
			|| !$exp_month
			|| !$exp_year
			|| !$csc_real
			|| !$email
		) {
			file_log("stripe", "stripe::charge: mandatory fields not filled: payment_id={$payment_id}, cc_id='{$cc_id}', firstname='{$firstname}', lastname='{$lastname}', address='{$address}', city='{$city}', zip='{$zip}', email='{$email}', cardno='{$card_no}', exp_month='{$exp_month}', exp_year='{$exp_year}', csc='{$csc}'");
			reportAdmin("AS: Payment failed - mandatory fields not filled.", "payment_id={$payment_id}, cc_id='{$cc_id}', firstname='{$firstname}', lastname='{$lastname}', address='{$address}', city='{$city}', zip='{$zip}', email='{$email}', cardno='{$card_no}', exp_month='{$exp_month}', exp_year='{$exp_year}', csc='{$csc}'");
			$error = "Something is missing.";
			return false;
		}

		$res = $db->q("UPDATE payment SET result = 'S', processor = 'stripe', sent_stamp = ? WHERE id = ? LIMIT 1", [$now, $payment_id]);

		//----------------
		//Stripe code
		//----------------

		$amount = number_format($amount, 2, ".", "");

		$this->tokenArr = json_decode(urldecode($_REQUEST['tokenObj']), true);

		// possible errors
		$cvc_check = $this->tokenArr['card']['cvc_check'] === 'pass';
		if(! $cvc_check){
			file_log("stripe", "stripe::pay(): card cvc check failed: payment_id={$payment_id}");
			//reportAdmin("AS: stripe:charge: Payment failed - card cvc check failed: payment_id={$payment_id}");
			$error = "Error in payment. CVC code check failed";
		}

		$address_zip_check = $this->tokenArr['card']['address_zip_check'] === 'pass';
		if(! $address_zip_check){
			file_log("stripe", "stripe::pay(): card address zip check failed: payment_id={$payment_id}");
			//reportAdmin("AS: stripe:charge: Payment failed - card address and zip code check failed: payment_id={$payment_id}");
			$error = "Error in payment. Address and ZIP code check failed";
		}

		// charge
		$charge = $this->capture($params, $error);

		// if payment didn't succeeded
		if(! $this->stripeSuccessPayment || ! $charge->paid){
			file_log("stripe", "stripe::charge(): API failed ! type='{$this->errorType}', code='{$this->errorCode}', message='{$this->errorMessage}'");

			$res = $db->q(
				"UPDATE payment SET result = ?, responded_stamp = ?, decline_code = ?, decline_reason = ? WHERE id = ? LIMIT 1",
				["D", $now, $this->errorCode, $this->errorMessage, $payment_id]
			);
			$aff = $db->affected($res);
			if ($aff != 1) {
				reportAdmin("Payment failed - error: cant update payment", "cant update payment about transaction decline, payment_id={$payment_id}, now={$now}, aff={$aff}");
			}
			$db->q("INSERT INTO account_cc_fail (account_id, stamp, payment_id) values (?, ?, ?)", [$account_id, $now, $payment_id]);

			// user friendly error comes from stripe in $charge->failure_message
			$error = ! empty($charge->failure_message) ? $charge->failure_message : 'An error occurred. Please contact us at support@adultsearch.com';
			return false;
		}

		//charge succeeded
		$trans_id = $charge_id = $charge->balance_transaction;
		file_log("stripe", "stripe::charge(): success, charge_id='{$charge_id}'");

		if ($captured != $charge->captured) {
			file_log("stripe", "stripe::charge(): Error: requested captured=".intval($captured).", returned captured=".intval($charge->getCaptured())." !");
			reportAdmin("AS: stripe::charge() Error", "payment_id={$payment_id}, requested captured=".intval($captured).", returned captured=".intval($charge->getCaptured())." !");
		}

		//processing response
		//https://stripe.com/docs/api#charge_object
		$card_brand = null;
		if (! empty($charge->source->brand))
			$card_brand = $charge->source->brand;
		$resp = json_encode($this->tokenArr);

		//-------------------------
		//API requst was successful
		// handling is different depending whether it was recurrint or non-recurrent transaction
		$now = time();
		$ip_address = account::getUserIp();

		//store cc into safestore
		if (!safestore::hasEntryById("cc", $cc_id)) {
			$ret = safestore::addEntry("cc", $cc_id, ["cc" => $cardno_real, "cvc2" => $csc_real]);
			if (!$ret) {
				//notify admin, but dont panic
				file_log("stripe", "stripe::charge(): Error: Failed to store CC into safestore ! cc_id={$cc_id}");
				reportAdmin("AS: Failed to store CC into safestore", "cc_id={$cc_id}");
			}
		}


		//update payment
		$card->setType($this->getCardTypeByName($card_brand));
		$card->update();

		$result = null;
		if ($charge->captured)
			$result = "A";
		else
			$result = "U";
		$res = $db->q(
			"UPDATE payment 
			SET cc_id = ?, result = ?, response = ?, responded_stamp = ?, decline_code = null, decline_reason = null, ip_address = ?, trans_id = ?
			WHERE id = ? 
			LIMIT 1",
			[$cc_id, $result, $resp, $now, $ip_address, $trans_id, $payment_id]
		);
		$aff = $db->affected($res);
		if ($aff != 1) {
			reportAdmin("AS: stripe:charge: Payment succeeded - error: cant update payment", "cant update payment about transaction success, payment_id={$payment_id}, now={$now}, aff={$aff}");
		}

		//add transaction as well if charge was captured
		if ($charge->captured) {
			$res = $db->q(
				"INSERT INTO transaction (payment_id, stamp, trans_id, amount) VALUES (?, ?, ?, ?)",
				[$payment_id, $now, $trans_id, $amount]
			);
			$transaction_id = $db->insertid($res);
			if (!$transaction_id) {
				reportAdmin("AS: stripe:charge: Payment succeeded - error: cant insert transaction", "cant insert transaction into db, payment_id={$payment_id}, trans_id={$trans_id}");
			}
		}

		//reportAdmin("AS: checkme stripe:charge: Payment succeeded - payment_id: {$payment_id}");

		return true;
	}


	/**
	 * @param $params = [
				"payment_id" => $payment_id,
				"account_id" => $account_id,
				"card" => $cre,
				"email" => $email,
				"amount" => $amount, // required **
				"recurring_amount" => $recurring_amount,
				];
	 *
	 * @param $error
	 * return charge stripe object // https://stripe.com/docs/api#charge_object
	 */
	public function capture($params, &$error, $capture = true) {
		global $config_stripe_secret_key;

		file_log("stripe", "stripe::charge(): gonna charge, request='".print_r($this->tokenArr, true)."'");

		\Stripe\Stripe::setApiKey($config_stripe_secret_key);

		$charge = false;
		$logStripe = '';
		$amount_minor = round($params['amount'] * 100); //Charge amount in minor units of given currency (for USD in cents)

		try {
			$charge = \Stripe\Charge::create(
				array(
					'amount' => $amount_minor,
					'currency' => 'usd',
					'source' => GetPostParam('stripeToken'),
					'capture' => $capture
				)
			);

			$this->stripeSuccessPayment = 1;

		} catch(\Stripe\Error\Card $e) {
			// Since it's a decline, \Stripe\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$this->errorType = $err['type'];
			$this->errorCode = $err['code'];
			$this->errorMessage = $err['message'];

			$logStripe .= "Status is:" . $e->getHttpStatus() . "\n";
			$logStripe .= "Type is:" . $err['type'] . "\n";
			$logStripe .= "Code is:" . $err['code'] . "\n";
			// param is '' in this case
			$logStripe .= "Param is:" . $err['param'] . "\n";
			$logStripe .= "Message is:" . $err['message'] . "\n";


		} catch (\Stripe\Error\RateLimit $e) {
			// Too many requests made to the API too quickly
			$logStripe .= $e->getMessage() . "\n";
		} catch (\Stripe\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Stripe's API
			$logStripe .= $e->getMessage() . "\n";
		} catch (\Stripe\Error\Authentication $e) {
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			$logStripe .= $e->getMessage() . "\n";
		} catch (\Stripe\Error\ApiConnection $e) {
			// Network communication with Stripe failed
			$logStripe .= $e->getMessage() . "\n";
		} catch (\Stripe\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			$userErrorMsg = 'Payment error. Please contact support';
			$logStripe .= $e->getMessage() . "\n";
		} catch (Exception $e) {
			// Something else happened, completely unrelated to Stripe
			$logStripe .= $e->getMessage() . "\n";
		}

		file_log("stripe", "Stripe payment request log: " . $logStripe);

		return $charge;
	}

	public function refund_transaction($transaction, $amount = null, &$error) {
		return false;
	}

	// process refund or cancel authorization (release funds)
	// https://stripe.com/docs/api#create_refund
	public function refund_charge($charge_id, $amount = null, &$error) {

		$refund = \Stripe\Refund::create(array(
			"charge" => $charge_id
		));

		if($refund->status != 'succeeded'){
			$error = 'Refund issue. Charge obj: ' . $charge_id;
			reportAdmin('AS: stripe:refund_charge(): Refund not succeeded. Stripe Charge obj: ' . $charge_id);
			file_log('stripe', 'Refund not succeeded. Stripe Charge obj: ' . $charge_id);
			return false;
		}

		return $refund->id;
	}

	public function getCardTypeByName($name) {
		$type = "";
		switch (strtolower($name)) {
			case "visa": $type = "VI"; break;
			case "mastercard": $type = "MC"; break;
			case "discover": $type = "DI"; break;
			case "jcb": $type = "JC"; break;
			case "maestro": $type = "MA"; break;
			case "americanexpress":
			case "american express":
				$type = "AM"; break;
			case "unknown": $type = NULL; break;
			default:
				reportAdmin("AS: stripe:getCardTypeByName(): Unknown card type name", "cardType = '{$name}'");
				$type = "OT";
				break;
		}
		return $type;
	}

	public function change_card(&$error) {
		global $account, $db;

		$params['amount'] = 1; // authorize card for 1$
		$charge = $this->capture($params, $error, false);
		$account_id = $account->getId();

		// if new cc auth succeeded, delete the old one and insert new one
		if (!$charge->paid) {
			$error = 'Card not authorized';
			return false;
		}

		// cancel authorization and release funds
		$this->refund_charge($charge->id, null, $error);

		// soft delete old cc
		$cre_id = intval($_REQUEST['ccid']);
		$cre = creditcard::findOneById($cre_id);
		if ($cre && !$cre->getDeletedStamp() && $account->getId() == $cre->getAccountId())
			$cre->remove();

		// new cc
		$tokenArr = json_decode(urldecode($_REQUEST['tokenObj']), true);
		$nameSplit = explode(' ', $tokenArr['card']['name']);
		$lastname = array_pop($nameSplit);
		$firstname = implode(' ', $nameSplit);
		$address = $tokenArr['card']['address_line1'] . "\n" . $tokenArr['card']['address_line2'];
		$zipcode = $tokenArr['card']['address_zip'];
		$city = $tokenArr['card']['address_city'];
		$state = $tokenArr['card']['address_state'];
		$country = $tokenArr['card']['address_country'];
		$cc_month = $tokenArr['card']['exp_month'];
		$cc_year = $tokenArr['card']['exp_year'];
		$token = GetPostParam("stripeToken");
		$cc_cc = 'xxxxxxxxxxxx' . $tokenArr['card']['last4'];
		$cc_cvc2 = ' ';

		$new_cre = new creditcard();
		$new_cre->setAccountId($account_id);
		$new_cre->setCc($cc_cc);
		$new_cre->setExpMonth($cc_month);
		$new_cre->setExpYear($cc_year);
		$new_cre->setCvc2($cc_cvc2);
		$new_cre->setFirstName($firstname);
		$new_cre->setLastName($lastname);
		$new_cre->setAddress($address);
		$new_cre->setZipcode($zipcode);
		$new_cre->setCity($city);
		$new_cre->setStripeToken($token);
		$new_cre->setApprovedStamp(time());
		$new_cre->setCountry($country);
		$new_cre->setType($this->getCardTypeByName($charge->source->brand));

		if (empty($state))
			$state = ''; // col can't be null
		$new_cre->setState($state);

		$ret = $new_cre->add();
		$new_cre_id = $new_cre->getId();

		if (!$ret) {
			reportAdmin("AS: stripe change_card() error", "Cannot insert cc in db.", [
				"cc" => creditcard::ccnoFirstLast($cc_cc),
				"cvc" => creditcard::obfuscateCvc2($cc_cvc2),
				"cc_month" => $cc_month,
				"cc_year" => $cc_year,
				"firstname" => $firstname,
				"lastname" => $lastname,
				"address" => $address,
				"city" => $city,
				"state" => $state,
				"zipcode" => $zipcode,
				"country" => $country,
			]);
			$error = "Error in payment, please contact support.";
			return false;
		}

		// update payments with active subscription
		if(GetPostParam('subscription') == payment::SUBSCRIPTION_ACTIVE){
			$update_payments = $db->q('UPDATE payment SET cc_id = ? WHERE cc_id = ?', [$new_cre_id, $cre_id]);
			$aff = $db->affected($update_payments);
			if ($aff != 1) {
				file_log("stripe", "stripe:change_card: Error updating payment with new card, new_cc_id={$new_cre_id}, old_cc_id={$cre_id}");
				reportAdmin("AS: stripe:change_card: Error updating payment with new card, new_cc_id={$new_cre_id}, old_cc_id={$cre_id}");
			}
		}

		file_log("payment", "stripe::change_card(): ok, CC inserted, cc_id={$new_cre_id}, stripe_token={$new_cre->getStripeToken()}");
		reportAdmin("AS: checkme stripe:change_card: User #{$account->getId()} has changed his payment method old_card_id: {$cre_id}, new_card_id: {$new_cre_id}");

		return true;

	}

}

//END 
