<?php

/**
 * swfupload upload widget functions
 * Class 
 * @author jay
 */
class swfupload {

	public static function getHtml() {

		video::storeVideoInfoToSession();

		$html = "";
		
$html .= <<<END
<script src="/js/swfupload.js" type="text/javascript"></script>
<script type="text/javascript">
END;

global $account;
if ($account->isadmin())
	$html .= "var worker = true;";
else
	$html .= "var worker = false;";

$html .= <<<END
function upl_vid_file_add(fileName) {
	$('div#upl_vid_progress_name').html(fileName);
	$('div#upl_vid_progress_name').css('display', 'block');
}
function upl_vid_start() {
	$('span#upl_vid_upl_msg').css('display', 'block');
		$('progress').css('display', 'inline-block');
		$('span#text_progress').css('display', 'inline-block');
		$('div#upl_vid_browse').css('border', '0px none');
		$('div#upl_vid_browse').css('border-bottom', '1px dashed #f5f5f5');
		$('div#upl_vid_browse').hover(
			   function() {
					   $(this).css({'border':'0px none', 'border-bottom':'1px dashed #f5f5f5', 'background-color':'transparent'});
			   },
			   function() {
					   $(this).css({'border':'0px none', 'border-bottom':'1px dashed #f5f5f5', 'background-color':'transparent'});
			   }
		);
		$('div#upl_vid_browse').css('height', '150px');
	$('div#upl_vid_wrap img').replaceWith('<img src="/images/ajax_loader_64.gif" />');
}
function upl_vid_progress(ratio) {
	var progress_percentage = Math.round(ratio * 100);
	$('progress').val(progress_percentage);
	$('span#text_progress').html(progress_percentage + '%');
}
function upl_vid_ok() {
	$("div#upl_vid_browse > img").attr("src","/images/upload_video_done.png");
	$('span#upl_vid_upl_msg').html('Upload finished.');
	_ajax('post', '/files/uploadVideoResult', '', 'upl_vid_result');
}
function upl_vid_error(message) {
	$("div#upl_vid_browse > img").attr("src","/images/upload_video_error.png");
	$('span#upl_vid_upl_msg').html('Upload error.');
	$('div#upl_vid_error').show();
	$('div#upl_vid_error .error_detail').html(message);
	_ajaxs('post', '/files/errorUploadVideo', message);
}

function fileError(file, errorCode, message) {
//	alert('fileError: ' + 'errorCode=' + errorCode + ' message=' + message);
	if (errorCode == SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT) {
		if (worker)
			location.href = '?uploader=jumploader';
		var size_mbytes = Math.round(10 * file.size / 1048576)/10;
		var allowed_size = swfu.customSettings.file_size_limit;
		swfu_error = "File size " + size_mbytes + " MB exceeds allowed limit " + allowed_size;
	} else {
		swfu_error = message;
	}		
}

function fileDialogComplete(numFilesSelected, numFilesQueued) {
	if (numFilesSelected == 0)
		return;
	try {
		upl_vid_start();
		this.startUpload();
		this.setButtonDisabled(true);
		swfu.getMovieElement().style.visibility = "hidden";
	} catch (ex)  {
			this.debug(ex);
	}

	//check if there were some errors
	var stats = swfu.getStats();
	if (stats.queue_errors > 0) {
		upl_vid_error(swfu_error);	
	}
}

function uploadStart(file) {
	upl_vid_file_add(file.name);
	return true;
}

function uploadProgress(file, bytesLoaded, bytesTotal) {
	upl_vid_progress(bytesLoaded / bytesTotal);
}

function uploadSuccess(file, serverData) {
	if (serverData.substring(0,6) == 'Error:') {
		upl_vid_error(serverData.substring(6).trim());
	} else {
		upl_vid_ok();
	}
}

function uploadError(file, errorCode, message) {
	switch (errorCode) {
		case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:
			upl_vid_error('HTTP Error (' + message +').');
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:
			upl_vid_error('Upload Failed (' + message +').');
			break;
		case SWFUpload.UPLOAD_ERROR.IO_ERROR:
			upl_vid_error('IO Error (' + message +').');
			break;
		case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:
			upl_vid_error('Security Error (' + message +').');
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
			upl_vid_error('Upload limit exceeded. (' + message +').');
			break;
		case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:
			upl_vid_error('Failed Validation.  Upload skipped. (' + message +').');
			break;
		case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
			upl_vid_error('Cancelled. (' + message +').');
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
			upl_vid_error('Stopped. (' + message +').');
			break;
		default:
			upl_vid_error('Unhandled Error: ' + errorCode + ' (' + message +').');
			break;
	}
	upl_vid_error('Error: ' + errorCode + ' (' + message +').');
}

var swfu;
var swfu_error;

jQuery(document).ready(function() {
	var settings = {
		flash_url : "/js/swfupload/swfupload.swf",
		upload_url: "http://philippines.adultsearch.com/swfupload/uploadHandler",
		file_types : "*.*",
		file_types_description : "All Files",
		debug: false,
END;

//debug_log("Exporting session_id to web session_id=".session_id());
//debug_log("upload_max_filesize=".ini_get("upload_max_filesize"));
$html .= "		post_params: {\"PHPSESSID\" : \"".session_id()."\"},";
$max_fs = ini_get("upload_max_filesize");
$html .= "			  file_size_limit : \"{$max_fs}\",";
$html .= "			  custom_settings : { progressTarget : \"fsUploadProgress\", file_size_limit : \"{$max_fs}\", },";

$html .= <<<END

		// Button settings
		button_image_url: "/images/upload_btn_bg.png",
		button_width: "260",
		button_height: "30",
		button_placeholder_id: "spanButtonPlaceHolder",
		button_text: '<span class="theFont">Select files from your computer</span>',
		button_text_style: ".theFont { text-align: center; font-family: arial, verdana, serif; font-size: 16; }",
		button_text_left_padding: 0,
		button_text_top_padding: 3,
		button_cursor: SWFUpload.CURSOR.HAND,
		button_action: SWFUpload.BUTTON_ACTION.SELECT_FILE,
			
		// The event handler functions are defined in handlers.js
		file_queue_error_handler : fileError,
		file_dialog_complete_handler : fileDialogComplete,
		upload_start_handler : uploadStart,
		upload_progress_handler : uploadProgress,
		upload_error_handler : uploadError,
		upload_success_handler : uploadSuccess,
	};
	swfu = new SWFUpload(settings);
});

</script>
END;
//TODO comment up - philippines !!!!

$html .= <<<END
<style type="text/css">
div#upl_vid_wrap {
	width: 600px;
	text-align: center;
	margin: auto;
}
div#upl_vid_wrap * {
	margin: auto;
}
div#upl_vid_browse {
	width: 300px;
	height: 220px;
	margin: auto;
	margin-bottom: 10px;
	border: 1px solid white;
	padding: 5px;
}
div#upl_vid_browse:hover {
	border: 1px solid #ddd;
	background-color: #f5f5f5;
}
div#upl_vid_browse_button {
	width: 250px;
	margin: auto;
	border: 1px solid #bbb;
	background-color: #ddd;
	padding: 5px;
	font-size: 1.2em;
}
span#spanButtonPlaceHolder {
	display: block;
}

div#upl_vid_progress_name {
	display: none;
	font-weight: bold;
}
progress {
	display: none;
	width: 220px;
}
span#upl_vid_upl_msg {
	display: none;
	height: 35px;
}
span#text_progress {
	display: none;
	width: 60px;
	font-weight: bold;
	color: #48BC24;
	text-align: center;
}

div#upl_vid_error {
	display: none;
	border: 2px solid red;
	background-color: #FF8D8D;
	padding: 10px;
	font-size: 1.2em;
}
div#upl_vid_error .error_detail {
	font-size: 0.8em;
}

div#upl_vid_result {
	width: 600px;
	margin: auto;
}
</style>
	
<div id="upl_vid_wrap">
<div id="upl_vid_browse">
<div id="upl_vid_progress_name"></div><br />
<img src="/images/upload_video.png" /><br />
<span id="upl_vid_upl_msg">Upload in progress.<br />Please wait.<br /></span>
<progress max="100" value="0"></progress><span id="text_progress">0%</span>
<span id="spanButtonPlaceHolder"></span>
<!--<div id="upl_vid_browse_button">Select files from your computer</div>-->
</div>
</div>

<div id="upl_vid_error"><strong>Video upload has failed !</strong><br /><span class="error_detail"></span><br /><br />Please contact us at <a href="mailto:support@adultsearch.com">support@adultsearch.com</a></div>

<div id="upl_vid_result"></div>

END;

		return $html;
	}

	public static function uploadHandler() {
		//debug_log('UP swfupload');
		//debug_log("GET=".print_r($_GET,true)." POST=".print_r($_POST,true)." FILES=".print_r($_FILES,true));
		
		// Code for Session Cookie workaround
		session_destroy();
		if (isset($_POST["PHPSESSID"])) {
			session_id($_POST["PHPSESSID"]);
		} else if (isset($_GET["PHPSESSID"])) {
			session_id($_GET["PHPSESSID"]);
		}
		session_start();

		// Check post_max_size (http://us3.php.net/manual/en/features.file-upload.php#73762)
		$POST_MAX_SIZE = ini_get('post_max_size');
		$unit = strtoupper(substr($POST_MAX_SIZE, -1));
		$multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

		if ((int)$_SERVER['CONTENT_LENGTH'] > $multiplier*(int)$POST_MAX_SIZE && $POST_MAX_SIZE) {
			header("HTTP/1.1 500 Internal Server Error"); // This will trigger an uploadError event in SWFUpload
			self::handleError("POST exceeded maximum allowed size.");
			exit(0);
		}

		// Settings
		$upload_name = "Filedata";
		$max_file_size_in_bytes = 2147483647;				// 2GB in bytes
		$MAX_FILENAME_LENGTH = 260;
		$uploadErrors = array(
			0=>"There is no error, the file uploaded with success",
			1=>"The uploaded file exceeds the upload_max_filesize directive in php.ini",
			2=>"The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
			3=>"The uploaded file was only partially uploaded",
			4=>"No file was uploaded",
			6=>"Missing a temporary folder"
		);

		// Validate the upload
		if (!isset($_FILES[$upload_name])) {
			self::handleError("No upload found in \$_FILES for ".$upload_name);
			exit(0);
		} else if (isset($_FILES[$upload_name]["error"]) && $_FILES[$upload_name]["error"] != 0) {
			self::handleError($uploadErrors[$_FILES[$upload_name]["error"]]);
			exit(0);
		} else if (!isset($_FILES[$upload_name]["tmp_name"]) || !@is_uploaded_file($_FILES[$upload_name]["tmp_name"])) {
			self::handleError("Upload failed is_uploaded_file test.");
			exit(0);
		} else if (!isset($_FILES[$upload_name]['name'])) {
			self::handleError("File has no name.");
			exit(0);
		}

		// Validate the file size (Warning: the largest files supported by this code is 2GB)
		$file_size = @filesize($_FILES[$upload_name]["tmp_name"]);
		if (!$file_size || $file_size > $max_file_size_in_bytes) {
			self::handleError("File exceeds the maximum allowed size");
			exit(0);
		}

		if ($file_size <= 0) {
			self::handleError("File size outside allowed lower bound");
			exit(0);
		}

		// create the final destination file

		if ($_SESSION["upl_vid_module"] == "place") {   
			$dir = $_SESSION["upl_vid_country"];
		} else {
			$dir = $_SESSION["upl_vid_module"];
		}

		$filename = $_FILES[$upload_name]['name'];
		if (!video::validateFilename($filename)) {
			self::handleError("Invalid filename.");
						exit(0);
		}
		$dest_filename = video::getUniqueFilename($dir, $filename);
		$dest_filepath = video::getDestFilePath($dir, $dest_filename);
		debug_log('UP swfupload: Creating final file at \''.$dest_filepath.'\'');
		
		if (!@move_uploaded_file($_FILES[$upload_name]["tmp_name"], $dest_filepath)) {
			self::handleError("File could not be saved.");
			exit(0);
		}

		//store data about successful upload to session
		$_SESSION["upl_vid_filename"] = $dest_filename;
				$_SESSION["upl_vid_filepath"] = $dest_filepath;

		exit(0);

		// create the temporary directory
		@mkdir($temp_dir, 0777, true);

	}

	private static function handleError($msg) {
		echo "Error: ".$msg;
		die;
	}

}

//END
