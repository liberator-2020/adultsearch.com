<?php

/**
 * Sending SMS through https://secure.smsgateway.ca gateway
 */
class swiftsms {

	private static $account_key = "MLC810wg66";
	public $response = NULL;
	public $error = NULL;

	public function send($phone, $message) {
		global $config_env;

        if (in_array($config_env, ["dev", "test"]) || strpos($config_env, "uat") === 0) {
            //on DEV, TEST and UAT environments send email instead of SMS (that ends up in virtual mailbox)
            $text = "Phone: {$phone}\nText: {$message}\n";
            $error = null;
            send_email([
                "from" => WEB_EMAIL,
                "to" => WEB_EMAIL,
                "subject" => "SMS to {$phone}",
                "html" => nl2br($text),
                "text" => $text,
                ], $error);
            debug_log("swiftsms: env={$config_env} -> SMS sent as email to ".WEB_EMAIL.", error='{$error}'");
            return true;
        }

		$this->response = NULL;
		$this->error = NULL;

		$url = "http://smsgateway.ca/sendsms.aspx?CellNumber=".urlencode($phone)."&MessageBody=".urlencode($message)."&AccountKey=".self::$account_key;

		debug_log("swiftsms: url={$url}");

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //return response instead of printing out
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30); //timeout 30 seconds
		curl_setopt($ch, CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)"); 
		curl_setopt($ch, CURLOPT_URL, $url);
		$return = curl_exec($ch);

		$err_no = curl_errno($ch);
		if ($err_no != 0) {
			$err = curl_error($ch);
			reportAdmin("Unable to send SMS through Swift SMS gateway !", "", array("err_no" => $err_no, "err" => $err, "phone" => $phone, "message" => $message, "return" => $return));
			debug_log("swiftsms: Error: curl_err_no={$err_no}, curl_err={$err}, return={$return}");
			curl_close($ch);
			return false;
		} 

		curl_close($ch);
		debug_log("swiftsms: return={$return}");
		$this->response = $return;

		if ($return == "Message queued successfully")
			return true;


		$this->error = $return;
		return false;
	}

}

//END
