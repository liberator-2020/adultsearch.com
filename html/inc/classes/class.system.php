<?php

/**
 * class system
 *
 * Commonly used functions defined in that class. So no need to write them again and again if we need them.
 *
 */
class system {

	function fileLog($file, $log) {
		$file = fopen($file, "a");
		if($file) {
			fwrite($file, $log . "\n");
			fclose($file);
		}
	}

	private static function preserve_tracking($url) {
		$addition = "";

		if (array_key_exists("utm_source", $_REQUEST))
			$addition = "utm_source={$_REQUEST["utm_source"]}";

		if (array_key_exists("utm_medium", $_REQUEST)) {
			$addition .= ($addition) ? "&" : "";
			$addition .= "utm_medium={$_REQUEST["utm_medium"]}";
		}
						
		if (array_key_exists("utm_campaign", $_REQUEST)) {
			$addition .= ($addition) ? "&" : "";
			$addition .= "utm_campaign={$_REQUEST["utm_campaign"]}";
		}
	
		if ($addition) {
			if (strpos($url, "?") !== false)
				$url .= "&".$addition;
			else
				$url .= "?".$addition;
		}

		return $url;					
	}

	public static function go($url, $alert = '') {
		if( strstr($url,"www.") && isset($_SESSION["coresub"]) )
			$url = str_replace("www.", $_SESSION["coresub"].".", $url);

		$url = self::preserve_tracking($url);

		echo "<script type=\"text/javascript\">";
			if( $alert )
				echo "alert('".addslashes($alert)."');";
			
		echo "window.location = '".$url."'; </script>";
		echo "You are being redirected to another page. <a href='$url'>Click here</a> to go right away.";
		die;
	}

	public static function moved($url) {
		ob_end_clean();
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: ".$url);
		self::go($url);
		exit();
		return;
	}
	
	public static function redirect($url) {
		ob_end_clean();
		header("HTTP/1.1 302 Found");
		header("Location: ".$url);
		self::go($url);
		exit();
		return;
	}

	public static function goModuleBase() {
		global $gModule, $account;
		if (empty($gModule))
			$gModule = "/";
		if (!is_array($account->core_loc_array))
			self::moved("/$gModule/");
		if (!isset($account->core_loc_array['loc_url']) || empty($account->core_loc_array['loc_url']))
			$account->core_loc_array['loc_url'] = "/";
		self::moved($account->core_loc_array['loc_url'].$gModule."/");
	}

	public static function reload($filter = []) {

		$arr = parse_url($_SERVER["REQUEST_URI"]);
		
		$newUrl = "https://".$_SERVER["HTTP_HOST"].$arr["path"];

		$query = $arr["query"];
		if ($query || $filter) {
			$new_query_params = [];
			$arr2 = explode("&", $query);
			foreach($arr2 as $item) {
				$arr3 = explode("=", $item);
				$param_name = $arr3[0];
				$param_value = $arr3[1];
				foreach ($filter as $key => $val) {
					if ($param_name == $key) {
						$param_value = $val;
						break;
					}						
				}
				$new_query_params[$param_name] = $param_value;
			}
			foreach ($filter as $key => $val) {
				if (!array_key_exists($key, $new_query_params))
					$new_query_params[$key] = $val;
			}
			$new_query = http_build_query($new_query_params);
			$newUrl .= "?".$new_query;
		}

		ob_end_clean();
		header("HTTP/1.1 302 Moved Permanently");
		header("Location: ".$newUrl);
		exit();
	}

	public static function _makeText($limit = 20, $intonly = 0) {
		if( $intonly )
			$m = "1234567890";
		else
			$m = "abcdefghijklmnoprstuwyxzABCDEFGHIJKLMOPRSTUYW1234567890";

		$s = "";
		for($i=0;$i<$limit;$i++)
			$s .= $m[rand(0, strlen($m))];
		return $s;
	}
}

//END
