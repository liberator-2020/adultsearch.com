<?php

/*
 * topic = forum topic
 */
class topic {

	private $id = NULL,
			$title = NULL,
			$place_id = NULL,
			$account_id = NULL,
			$created = NULL,
			$last_post_id = NULL,
			$forum_id = NULL,
			$loc_id = NULL;

	private $account = NULL;
	private $location = NULL;
	private $forum_url = NULL;

	function __construct() {
	}

	public static function findOneById($id) {
		global $db;

		$res = $db->q("SELECT * FROM forum_topic WHERE topic_id = '".intval($id)."'");
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

		return self::withRow($row);
	}
	
	public static function withRow($row) {

		$topic = new topic();
		$topic->setId($row["topic_id"]);
		$topic->setTitle($row["topic_title"]);
		$topic->setPlaceId($row["item_id"]);
		$topic->setAccountId($row["account_id"]);
		$topic->setCreated($row["created"]);
		$topic->setLastPostId($row["topic_last_post_id"]);
		$topic->setForumId($row["forum_id"]);
		$topic->setLocId($row["loc_id"]);

		return $topic;
	}

	//getters and setters
	public function getId() {
		return $this->id;
	}
	public function getTitle() {
		return $this->title;
	}
	public function getPlaceId() {
		return $this->place_id;
	}
	public function getAccountId() {
		return $this->account_id;
	}
	public function getCreated() {
		return $this->created;
	}
	public function getLastPostId() {
		return $this->last_post_id;
	}
	public function getForumId() {
		return $this->forum_id;
	}
	public function getLocId() {
		return $this->loc_id;
	}

	public function setId($id) {
		$this->id = $id;
	}
	public function setTitle($title) {
		$this->title = $title;
	}
	public function setPlaceId($place_id) {
		$this->place_id = $place_id;
	}
	public function setAccountId($account_id) {
		$this->account_id = $account_id;
	}
	public function setCreated($created) {
		$this->created = $created;
	}
	public function setLastPostId($last_post_id) {
		$this->last_post_id = $last_post_id;
	}
	public function setForumId($forum_id) {
		$this->forum_id = $forum_id;
	}
	public function setLocId($loc_id) {
		$this->loc_id = $loc_id;
	}

	//relationships
	public function getAccount() {
		if ($this->account)
			return $this->account;
		$acc = account::findOneById($this->account_id);
		if (!$acc)
			return NULL;
		$this->account = $acc;
		return $this->account;
	}	

	public function getLocation() {
		global $db;

		if ($this->location)
			return $this->location;

		//first check county
		//2019-08-05 forums on counties are not anymore
		/*
		$res = $db->q("SELECT * from location_location WHERE county_id = ? ORDER BY significant DESC, has_place_or_ad DESC LIMIT 1", array($this->loc_id));
		if ($db->numrows($res) != 1) {
			$loc = location::findOneById($this->loc_id);
			if (!$loc)
				return NULL;
		} else {
			$row = $db->r($res);
			$loc = location::withRow($row);

		}
		*/
		$loc = location::findOneById($this->loc_id);
		if (!$loc)
			return NULL;

		$this->location = $loc;
		return $this->location;
	}

	public function getForumUrl() {
		global $db;

		if ($this->forum_url)
			return $this->forum_url;

		$res = $db->q("SELECT forum_name FROM forum_forum WHERE forum_id = ? AND loc_id = ? LIMIT 1", [$this->forum_id, $this->loc_id]);
		if ($db->numrows($res) != 1)
			return null;
		$row = $db->r($res);

		$this->forum_url = name2url($row["forum_name"]);
		return $this->forum_url;
	}	

	//additional functions
	public function remove() {
		global $db;
		$db->q("DELETE FROM forum_post WHERE topic_id = ?", array($this->id));
		$aff = $db->affected();
		$db->q("DELETE FROM forum_watch WHERE topic_id = ?", array($this->id));
		$db->q("DELETE FROM forum_topic WHERE topic_id = ? LIMIT 1", array($this->id));
		$aff2 = $db->affected();
		forum::updateStats($this->forum_id, $this->loc_id);
		if ($aff > 0 && $aff2 == 1)
			return true;
		return false;
	}

	public function getUrl() {
		$location = $this->getLocation();
		$url = $location->getUrl()."/sex-forum/".$this->getForumUrl()."/topic/{$this->id}";
		return $url;
	}

	public function emailAdminNotify($email = NULL) {
        global $db;

		//get last post message
		$res = $db->q("SELECT post_text FROM forum_post WHERE post_id = ?", array($this->getLastPostId()));
		if ($db->numrows($res) != 1) {
			return false;
		}
		$row = $db->r($res);
		$message = $row["post_text"];
		if ($message == NULL) {
			return false;
		}

		$place_link = (intval($this->getPlaceId()) > 0) ? "Yes" : "No";
		$acc = $this->getAccount();

        $smarty = GetSmartyInstance();
        $smarty->assign("title", $this->getTitle());
        $smarty->assign("message", $message);
		$smarty->assign("account_id", $acc->getId());
		$smarty->assign("account_email", $acc->getEmail());
		$smarty->assign("account_mng_link", "http://adultsearch.com/mng/accounts?account_id={$acc->getId()}");
		$smarty->assign("place_link", $place_link);
		$smarty->assign("view_link", $this->getUrl());
		$smarty->assign("delete_link", "http://adultsearch.com/sex-forum/topic_remove?id={$this->id}");
		$smarty->assign("delete_ban_link", "http://adultsearch.com/sex-forum/topic_remove_ban?id={$this->id}&account_id={$this->account_id}");

        $html = $smarty->fetch(_CMS_ABS_PATH."/templates/sex-forum/email/topic_admin_notify.html.tpl");
        $text = $smarty->fetch(_CMS_ABS_PATH."/templates/sex-forum/email/topic_admin_notify.txt.tpl");

        if (!$email) {
            $email = "support@adultsearch.com";
            //$email = ADMIN_EMAIL;
        }

        return send_email(array(
            "from" => "support@adultsearch.com",
            "to" => $email,
//            "bcc" => ADMIN_EMAIL,
            "subject" => "Forum New Topic",
            "html" => $html,
            "text" => $text,
            ));
	}

}

//END
