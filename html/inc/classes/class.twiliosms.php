<?php

require_once(_CMS_ABS_PATH."/inc/classes/twilio/Services/Twilio.php");

/**
 * Sending SMS through Twilio
 */
class twiliosms {

	private static $sid = "AC85780ceadc80307b260381d720254f8f";	//Account SID from https://www.twilio.com/console
	private static $token = "bf10b7169b82163385073abe248decdb"; //Auth Token from https://www.twilio.com/console
	private static $from = "+17029351688";	//Our phone number purchased in Twilio

	public $message = NULL;
	public $error = NULL;

	public function send($to, $txt) {

		//normalizing phone number
		$to = preg_replace('/[^0-9+]/', '', $to);

		$this->message = NULL;
		$this->error = NULL;

		debug_log("twiliosms: to='{$to}' txt='{$txt}'");

		try {
			$client = new Services_Twilio(self::$sid, self::$token);
			$this->message = $client->account->messages->sendMessage(
				self::$from,
				$to,
				$txt
				);
		} catch (\Exception $e) {
			debug_log("twiliosms: Exception: '{$e->getMessage()}' !");
			$this->error = $e->getMessage();
			return false;
		}

		debug_log("twiliosms: message: sid='{$this->message->sid}', status='{$this->message->status}', error_code='{$this->message->error_code}', error_message='{$this->message->error_message}'");

		//if error_code and error_message are empty, then SMS was sent successfully
		if (!$this->message->error_code && !$this->message->error_message)
			return true;

		//SMS failed
		$this->error = $this->message->error_message;
		return false;
	}

	//this webhook is called when someone replies to our SMS
	//webhook is set up here: https://www.twilio.com/console/phone-numbers/PNeae4e7949cbc4d4923c85de490967018
	public function sms_webhook() {
   		debug_log("twiliosms: sms_webhook: ".print_r($_REQUEST, true));
		reportAdmin("AS: Twilio SMS reply", "", $_REQUEST);

		//return empty TwiML response (so twilio doesnt panic)
		ob_clean();
		header("Content-type: text/xml");
		echo "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<Response>\n</Response>\n";
		die;
	}

}

//END
