<?php

class twitter {

	private $to; // sub twitter object thats linked to oauth class
	private $auth = NULL; // twitter account info for the given location

	public function __construct() {
		require_once(_CMS_ABS_PATH.'/inc/classes/twitter/EpiCurl.php');
		require_once(_CMS_ABS_PATH.'/inc/classes/twitter/EpiOAuth.php');
		require_once(_CMS_ABS_PATH.'/inc/classes/twitter/EpiTwitter.php');
	}

	private function post($msg) { 
		if (is_null($this->auth))
			return;
		try {
			$update_status = $this->to->post_statusesUpdate(array('status' => $msg));
		} catch(Exception $e) {
			$msg = "Exception message: ".$e->getMessage()."<br />\nFile: {$e->getFile()}<br />\nLine: {$e->line()}<br />\n";
			$auth = $this->auth;
			$params = array(
				"tweet_log.id" => $auth["id"],
				"tweet_log.tweet" => $auth["tweet"],
				"tweet_data.username" => $auth["username"],
				"this-auth" => print_r($auth),
				);
			reportAdmin("twitter:post() failed", $msg, $params);
		}
		$temp = $update_status->response;
		return $temp;
	}

	public function tweet() {
		global $db;
		$res = $db->q("SELECT l.id, l.tweet, d.*
						FROM `tweet_log` l
						INNER JOIN twitter_data d using (loc_id)
						WHERE l.sent = 0 and d.status = 1
						LIMIT 1");
		if (!$db->numrows($res))
			return;

		while ($this->auth = $db->r($res)) {
			$this->to = new EpiTwitter($this->auth['consumer_key'], $this->auth['consumer_secret']);
			$this->to->setToken($this->auth['access_token'], $this->auth['access_token_secret']);
			$db->q("update tweet_log set sent = 1 where id = '{$this->auth['id']}'");
			$db->q("update twitter_data set status=0 where loc_id = '{$this->auth['loc_id']}' limit 1");
			$result = $this->post($this->auth['tweet']);
			if( $result['id'] ) {
				$db->q("update twitter_data set total_tweet = total_tweet + 1, status = 1 where loc_id = '{$this->auth['loc_id']}' limit 1");
			} else {
				$m = "";
				foreach($result as $key=>$value)
					$m .= "$key: $value<br>";
				reportAdmin('could not tweet', $m, 'burak');
				$db->q("update tweet_log set sent = '-1' where id = '{$this->auth['id']}'");
			}
		}
	}

	public function queue($loc_id, $tweet) {
		global $db;
		$res = $db->q("select loc_id from twitter_data where loc_id = '$loc_id' and status = '1'");
		if (!$db->numrows($res))
			return;
		$db->q("insert into tweet_log (loc_id, date, tweet) values (?, curdate(), ?)", [$loc_id, $tweet]);
	}
}

//END
