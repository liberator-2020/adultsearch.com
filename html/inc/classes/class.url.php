<?php

/**
 * Processing of URL
 * Class 
 * @author jay
 */
class url {

	var $env_arr = array("dev", "test", "uat1", "uat2", "uat3", "uat4", "uat5", "staging");	//environments other than live site
	var $domain_name = "adultsearch";	//TODO should be set from configuration
	var $domain_tld = array("com", "xxx");		//TODO should be set from configuration

	//TODO this is only for now in array, probably we should make this smarter	
	var $primary_exceptions = array("ad", "all_locations", "dir", "fix", "geo_redirect", "submit", "map", "popup_reset", "search", "takeover", "upload", "uploadVideo", "worker", "place"); // 1ts part of url (for example '/place/*')

	//these are hack attempts, instanly dies
	var $primary_dies = array("administrator", "wp-login.php");

	//URLs that are going to search in _cms_files/<dir>/<file>.php
	var $primary_legacy = array("_ajax", "account", "adbuild", "advertise", "affiliate", "ajax", "api", "brothels", "businessowner", "classifieds", "contact", "ebiz", "emp", "emp_import", "erotic-massage", "escorts", "landing", "gay", "gay-bath-houses", "geo", "hot", "hotleads", "int", "lifestyle", "lingerie-modeling-1on1", "m", "male-revues", "male-gay-revues", "jay", "mng", "mobile", "payment", "pm", "poster", "privacy-policy", "promo", "publisher", "report-trafficking", "review", "search_location", "seo", "sex-forum", "sex-shops", "stats", "strip-clubs", "law-enforcement", "subpoena.html", "terms-of-service", "twilio", "vip", "vpn", "worker", "our-banners");

	var $secondary_exceptions = array("rss", "sex-forum", "uploadVideo", "submit", "map");				// 3th part of url (for example '/georgia/atlanta/map')

	//TODO the same thing
	var $erotic_services = array(
		"female-escorts",
		"tstv-shemale-escorts",
		"body-rubs",
		"bdsm-fetish-escorts",
		"strippers-for-hire",
		"male-escorts-for-female",
		"male-for-male-escorts",
		"male-for-male-body-rubs",
		"adult-help-wanted",
		"escort-agencies",
		"phone-and-web"
		);

	var $erotic_services_redirect = array(
		"AdultHelpWanted" => "adult-help-wanted",
		"BDSMFetish" => "bdsm-fetish-escorts",
		"BodyRubs" => "body-rubs",
		"FemaleEscorts" => "female-escorts",
		"MaleforFemale" => "male-escorts-for-female",
		"MaleforMaleBodyRubs" => "male-for-male-body-rubs",
		"MaleforMaleEscorts" => "male-for-male-escorts",
		"StrippersForHire" => "strippers-for-hire",
		"TSTVShemaleEscorts" => "tstv-shemale-escorts",
		"escortagencies" => "escort-agencies"
		);

	var $loc_redirect = array(
		"malate-manila" => "manila",
		"makati-city-manila" => "manila",
		"ermita-manila" => "manila",
		"koh-samui-bo-phut" => "koh-samui",
		"koh-samui-chaweng" => "koh-samui",
		"koh-samui-chaweng-beach" => "koh-samui",
		"koh-samui-lamai-beach" => "koh-samui",
		"koh-samui-mae-nam" => "koh-samui",
		"phuket-kamala" => "phuket",
		"phuket-karon" => "phuket",
		"phuket-kata" => "phuket",
		"phuket-patong-beach" => "phuket",
		);

	var $old_subdomains = array(
		"alabama","alaska","arizona","arkansas","california","scalifornia","colorado","connecticut","delaware","florida",
		"georgia","hawaii","idaho","illinois","indiana","iowa","kansas","kentucky","louisiana","maine",
		"maryland","massachusetts","michigan","minnesota","mississippi","missouri","montana","nebraska","nevada","newhampshire",
		"newjersey","newmexico","newyork","northcarolina","northdakota","ohio","oklahoma","oregon","pennsylvania","rhodeisland",
		"southcarolina","southdakota","tennessee","texas","utah","vermont","virginia","washington","westvirginia","wisconsin",
		"wyoming","alberta","britishcolumbia","manitoba","newbrunswick","newfoundland","novascotia","ontario","princeedwardisland","quebec",
		"saskatchewan","yukon","washingtondc"
		);

	var $legacy_categories = array(
		"erotic-massage",
		"strip-clubs",
		"male-strip-clubs",
		"tstv-strip-clubs",
		"male-revues",
		"male-gay-revues",
		"sex-shops",
		"swinger-clubs",
		"bdsm",
		"topless-pools",
		"nudist-colonies",
		"gay-bath-houses",
		"gay",
		"brothels",
		"lingerie-modeling-1on1"
		);

	var $old_legacy_categories = array(
		"eroticmassage" => "erotic-massage",
		"stripclubs" => "strip-clubs",
		"adult-stores" => "sex-shops",
		"adultstores" => "sex-shops",
		"SwingerClubs" => "swinger-clubs",
		"lifestyle-clubs" => "swinger-clubs",
		"BDSM" => "bdsm",
		"lingeriemodeling1on1" => "lingerie-modeling-1on1",
		"strip-club-revues" => "male-revues"
		);

	var $category_in_url = false;		//set to true if category url name is part of URL

	/**
	 * Returns true if URL was successfully processed. False if not.
	 */
	public function process_url() {
		global $ctx, $smarty, $config_site_url;
	
		//TODO should be removed after total unification		
		$a = explode(".", $_SERVER['HTTP_HOST']);
		//old URL host, e.g.: michigan.adultsearch.com
		//we dont want to bother with search of these in database
		//return immediate false (those URLs will be redirected anyway bu Burak's code, so we dont need to set any contextg variables)
		if (in_array($a[0], $this->old_subdomains)) {
			//2019-07-19 googlebot and bing still have these urls indexed ...
			$this->redirect("https://".str_replace($a[0].".", "", $_SERVER["HTTP_HOST"]).$_SERVER["REQUEST_URI"]);
			die;
		}

		//process host name
		if (!$this->process_url_host()) {
			//probably wrong hostname, redirect to homepage
			$this->redirect($config_site_url);
			die;
		}

		$ctx->domain_base_link = "https://".$ctx->domain_base;
		$ctx->domain_link = "https://".$ctx->domain;
		
		//if no country set in subdomain, it is United states
		if (!$ctx->country) {
			$ctx->country = "united-states";
			$ctx->country_id = 16046;
			$ctx->country_name = "United States";		//TODO take from location_location table instead ?
		}

		$ctx->international = true;

		//process path
		if (!$this->process_url_path()) {
			//we were not able to decode url path of requested page, redirect to homepage
			$this->redirect($config_site_url);
		}

		//if it is directory page, but we the category is not set (was wrong or misspelled), redirect to /location
		if ($this->category_in_url && $ctx->category_id == NULL && ($_SERVER["REQUEST_URI"] != $ctx->location_path))
			$this->redirect($ctx->location_path);

		if (intval($ctx->page) == 0)
			$ctx->page = 1;

		//TODO - for debug reasons
		//debug_log("URL processing SUCCESS, URL = '".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]."'");
		//$this->debugAccount();

		//the URL processing is successful - fill old global account variables - will be removed in future
		$this->fillAccountVariables();

		//TODO - for debug reasons
		if ($_SESSION['account'] == 3974) {
//		$this->debugAccount();
//		$this->debugContext();
		}

		$smarty->assign("page_type", $ctx->page_type);

		return true;
	}
	
	/**
	 * Process host part of URL.
	 * Returns true in success.
	 */
	private function process_url_host() {
		global $ctx;

		$http_host = strtolower($_SERVER['HTTP_HOST']);
		if (substr($http_host, -3) == ":81")
			$http_host = substr($http_host, 0, -3);

		$host_arr = explode(".", $http_host);

		$tld = array_pop($host_arr);
		//if ($tld != $this->domain_tld) return $this->invalid_url("TLD is '$tld', not '$this->domain_tld' !");		//TODO maybe we dont need these sanity checks
		if (!in_array($tld, $this->domain_tld)) return $this->invalid_url("TLD is '$tld', not '$this->domain_tld' !");		//TODO maybe we dont need these sanity checks
		
		$dom = array_pop($host_arr);
		if ($dom != $this->domain_name) return $this->invalid_url("Domain name is '$dom', not '$this->domain_name' !");	//TODO maybe we dont need these sanity checks

		$ctx->domain_base = "$dom.$tld";
		$ctx->domain = $ctx->domain_base;

		if (empty($host_arr))
			return true;

		if ($host_arr[0] == "www") {
			//redirect to non-www URL
			$original = "https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
			$newHost = str_replace("www.", "", $_SERVER['HTTP_HOST']);
			$new = "https://{$newHost}{$_SERVER['REQUEST_URI']}";
			system::moved($new);
			die;
		}
		
		if (empty($host_arr))
			return true;
	
		$url_part = array_pop($host_arr);
		if (in_array($url_part, $this->env_arr)) {	//processing of "dev" / "test" URL part
			$ctx->env = "$url_part";
			$ctx->domain_base = $ctx->env.".".$ctx->domain_base;
			$ctx->domain = $ctx->env.".".$ctx->domain;
			if (empty($host_arr))
				return true;
			$url_part = array_pop($host_arr);
		}

		//subdomain is (country) location, search for it
		$country_row = $this->fetchLocation($url_part, 0);
		if ($country_row === false) {
			//we have not found country for this subdomain
			//probably its very old URL host, e.g.: detroit-mi.adultsearch.com
			//we dont support those anymore, strip it from url and redirect
			$this->redirect("https://".str_replace($url_part.".", "", $_SERVER["HTTP_HOST"]).$_SERVER["REQUEST_URI"]);
			die;
			//return false;
		}
		$ctx->country = $country_row["dir"];
		$ctx->country_id = $country_row["loc_id"];
		$ctx->country_name = $country_row["loc_name"];
		$ctx->country_code = $country_row["s"];
		if (in_array($ctx->country_id, array(16046, 16047, 41973, 41756))) {
			//USA,Canada,UK,Australia
			$ctx->country_has_states = true;
		} else {
			$ctx->country_has_states = false;
		}
		$ctx->domain = $ctx->country.".".$ctx->domain;

		//TODO sanity check - do we need it ?
		if (!empty($host_arr)) return $this->invalid_url("Too many URL parts in host ? The unprocessed rest is : '".print_r($host_arr, true)."' !");

		return true;
	}

	/**
	 * Process path part of URL.
	 * Returns true in success.
	 */
	private function process_url_path() {
		global $account, $ctx, $smarty, $gIncFilename, $gFilename, $gModule, $config_site_url;
	
		$path_query_arr = explode("?", $_SERVER['REQUEST_URI']);
		$ctx->path_original = rtrim($path_query_arr[0], '/');

		//by using array_filter we filter out empty elements (that happens with multiple // in the url)
		$path_arr = array_filter(explode("/", trim($path_query_arr[0], '/')), 'strlen');

		$a = explode("/page/", $path_query_arr[0]);
		$ctx->path_no_page = $a[0];				//TODO performance - it is necessary to process this every page load ?

		if (!$path_arr || empty($path_arr) || $path_arr[0] == "") {
			if ($ctx->country == "united-states") {
				//return false;
				$gIncFilename = _CMS_ABS_PATH."/_cms_files/homepage.php";
				$gFilename = "homepage";
				$gModule = "homepage";
				$ctx->page_type = "landing";
				return true;
			}
			//if international country and no location specified, redirect to TOP city (city with most businesses)
			$this->redirect_to_biggest_city();
			die;
		}

		$part = array_shift($path_arr);

		if (in_array($part, $this->primary_dies)) {
			die("Master password = turkish delight");
		}

		if ($part == "homepage") {
			// /homepage -> delete defualt location cookie and redirect to /
			$account->setcookie("def_loc_id", '', -30);
			system::moved($config_site_url);
			die;
		}

		//exceptions from location:
		if (in_array($part, $this->primary_exceptions)) {
			switch ($part) {
				case 'ad':
					$ctx->page_type = "ad";
					return true;
					break;
				case 'submit':
					//user submitting place for this country/module
					$ctx->page_type = "submit";
					return true;
					break;
				case 'fix':
					$ctx->page_type = "fix";
					return true;
					break;
				case 'geo_redirect':
					$ctx->page_type = "geo_redirect";
					return true;
					break;
				case 'map':
					//user correcting gps position for a place
					$ctx->page_type = "map";
					return true;
					break;
				case 'popup_reset':
					$ctx->page_type = "popup_reset";
					return true;
					break;
				case 'search':
					//legacy search - short form in header
					$ctx->page_type = "legacy_search";
					return true;
					break;
				case 'upload':
					//general upload photo page for all visitors
					$ctx->page_type = "upload";
					return true;
					break;
				case 'uploadVideo':
					//general upload video page for workers and admins
					$ctx->page_type = "upload_video";
					return true;
					break;
				case 'worker':
					//worker edit screen
					$ctx->page_type = "worker";
					return true;
					break;
				case 'place':
					$part = array_shift($path_arr);
					if ($part == "map" && is_file(_CMS_ABS_PATH."/_cms_files/dir/list_map.php")) {
						global $gModule, $gIncFilename, $gFilename;
						$gModule = "place";
						$gIncFilename = "./_cms_files/dir/list_map.php";
						$gFilename = "place";
						return true;
					}
					break;
				case 'dir':
					$part = array_shift($path_arr);
					if ($part == "place" && is_file(_CMS_ABS_PATH."/_cms_files/dir/place.php")) {
						global $gModule, $gIncFilename, $gFilename;
						$gModule = "place";
						$gIncFilename = "./_cms_files/dir/place.php";
						$gFilename = "place";
						return true;
					}
					if ($part == "updatemap" && is_file(_CMS_ABS_PATH."/_cms_files/dir/updatemap.php")) {
						global $gModule, $gIncFilename, $gFilename;
						$gModule = "updatemap";
						$gIncFilename = "./_cms_files/dir/updatemap.php";
						$gFilename = "updatemap";
						return true;
					}
					if ($part == "edit" && is_file(_CMS_ABS_PATH."/_cms_files/dir/edit.php")) {
						global $gModule, $gIncFilename, $gFilename;
						$gModule = "edit";
						$gIncFilename = "./_cms_files/dir/edit.php";
						$gFilename = "edit";
						return true;
					}
					if ($part == "owner" && is_file(_CMS_ABS_PATH."/_cms_files/dir/owner.php")) {
						global $gModule, $gIncFilename, $gFilename;
						$gModule = "owner";
						$gIncFilename = "./_cms_files/dir/owner.php";
						$gFilename = "owner";
						return true;
					}
					if ($part == "coupon" && is_file(_CMS_ABS_PATH."/_cms_files/dir/coupon.php")) {
						global $gModule, $gIncFilename, $gFilename;
						$gModule = "coupon";
						$gIncFilename = "./_cms_files/dir/coupon.php";
						$gFilename = "coupon";
						return true;
					}
					break;
				default:
					break;
			}
			//TODO process the rest of the URL
			return false;
		}

		//exceptions - legacy handler
		if (in_array($part, $this->primary_legacy)) {
			if ($this->process_url_legacy($part, $path_arr))
				return true;
		}

		//general rule:
		// URL /ajax/class_name/par1/par2?par3=val3&par4=val4
		//  -> /inc/classes/class.class_name.php -> static method ajax(arg1, arg2)
		//	   arg1 = array('par1', 'par2')
		//	   arg2 = array('par3' => 'val3', 'par4' => 'val4')
		if ($part == "ajax" && (count($path_arr) >= 1)) {
			$class_name = $path_arr[0];
			if (class_exists($class_name) && method_exists($class_name, "ajax")) {
				$arg1 = $path_arr;
				array_shift($arg1);
				$ajax_handler = new $class_name;
				$arg2 = GetRequestParams();
				$ajax_handler->ajax($arg1, $arg2);
				die;	//the ajax handler should die by himself
				//} else {
				//		die("class or method not exist");
				} else {
					debug_log("class $class_name or method ajax() not exist");
				}
		}

		//general rule:
		// URL /class_name/method_name
		//  -> /inc/classes/class.class_name.php -> static method method_name()
		if (count($path_arr) == 1) {
			$class_name = $part;
			$method_name = $path_arr[0];
			if (class_exists($class_name) && method_exists($class_name, $method_name)) {
				//debug_log("URL: calling class-\>method handler: clas_name=$class_name method_name=$method_name");
				$class_handler = new $class_name;
				if (count($path_query_arr) > 1) {
					$arg2 = GetRequestParams();
					//debug_log("direct class name call, ".count($arg2)." request arguments: ".print_r($arg2, true));
					$ret = $class_handler->$method_name($arg2);
				} else {
					//debug_log("direct class name call, no request arguments");
					$ret = $class_handler->$method_name();
				}
				if ($ret === TRUE)
					return true;
				die;	//the class handler should die by himself
			}
//			debug_log("unsuccessful class-method call: class=$class_name method=$method_name");
		}

		//location redirects
		if (array_key_exists($part, $this->loc_redirect)) {
			$redirect_path = "/".$this->loc_redirect[$part]."/".implode("/", $path_arr);
			if (!empty($path_query_arr[1]))
				 $redirect_path .= "?".$path_query_arr[1];
			//2019-07-19 google bot still have these pages indexed
			$this->redirect($redirect_path);
			return false;
		}

		//search for location
		$ctx->location_path = "";
		$location_row = $this->fetchLocation($part, 1, $ctx->country_id);
		if ($location_row === false) {
			//didnt find primary location, redirect to biggest city
			$this->redirect_to_biggest_city();
			return false;
		}
		$ctx->location_type = intval($location_row["loc_type"]);
		//if ($location_row["loc_type"] == 2) {
		if ($ctx->location_type == 2) {
			//this is state
		   	$ctx->state = $location_row["dir"];
			$ctx->state_id = $location_row["loc_id"];
   			$ctx->state_name = $location_row["loc_name"];
			$ctx->state_lat = $location_row["loc_lat"];
			$ctx->state_long = $location_row["loc_long"];
			$ctx->state_loc_url = $location_row["loc_url"];
			
			$ctx->location = $location_row["dir"];
			$ctx->location_id = $location_row["loc_id"];
			$ctx->location_name = $location_row["loc_name"];
			$ctx->location_lat = $location_row["loc_lat"];
			$ctx->location_long = $location_row["loc_long"];
			$ctx->location_path .= "/$part";
			//next one could be city
			$location_row = false;
			if (empty($path_arr)) {
				//return $this->invalid_url("There is state '${part}' defined, but no city follows in URL path !");
			} else {
				$test_for_city = $path_arr[0];
				$location_row = $this->fetchLocation($test_for_city, 1, $ctx->state_id);
//				if ($_SESSION["account"] == 3974)
//					die("es, test_for_city='{$test_for_city}', state_id='{$ctx->state_id}', loc_row='".print_r($location_row, true)."'");
				if ($location_row)
					$part = array_shift($path_arr);
			}
		} else {
			$smarty->assign("cur_loc_id", $location_row["loc_id"]);
			$smarty->assign("cur_loc_name", $location_row["loc_name"]);
			$smarty->assign("cur_loc_url", $location_row["loc_url"]);
		}
		if ($location_row !== false) {
			//this is city
			$ctx->location_type = intval($location_row["loc_type"]);

			$ctx->city = $location_row["dir"];
			$ctx->city_id = $location_row["loc_id"];
			$ctx->city_name = $location_row["loc_name"];
			$ctx->city_lat = $location_row["loc_lat"];
			$ctx->city_long = $location_row["loc_long"];
			$ctx->city_loc_url = $location_row["loc_url"];
			$ctx->city_s = $location_row["s"];

			$ctx->county_id = $location_row["county_id"];

			$ctx->location = $location_row["dir"];
			$ctx->location_id = $location_row["loc_id"];
			$ctx->location_name = $location_row["loc_name"];
			$ctx->location_lat = $location_row["loc_lat"];
			$ctx->location_long = $location_row["loc_long"];
			$ctx->location_path .= "/$part";
		}

		$smarty->assign("city", $ctx->city);
		$smarty->assign("city_name", $ctx->city_name);
		$smarty->assign("state", $ctx->state);
		$smarty->assign("state_name", $ctx->state_name);

		//international city or domestic state, parent is country
		$ctx->parent = $ctx->country;
		$ctx->parent_id = $ctx->country_id;
		$ctx->parent_name = $ctx->country_name;
		$ctx->parent_lat = $ctx->country_lat;
		$ctx->parent_long = $ctx->country_long;
		$ctx->parent_loc_url = $ctx->country_loc_url;

		if ($this->process_url_order($path_arr) || empty($path_arr)) {
			// listing page with location selected and no category selected
			$ctx->page_type = "city";
			return true;
		}

		if ($this->process_url_page($path_arr) || empty($path_arr)) {
			// listing page with location selected and no category selected
			$ctx->page_type = "city";
			return true;
		}
		
		//after location there can be search	
		if ($this->process_url_search($path_arr))
			return true;	//after search there can't be anything else

		$part = array_shift($path_arr);

		//after location info there can be "forum" (or other secondary)or category	
		if (in_array($part, $this->secondary_exceptions)) {
			switch ($part) {
				case 'submit':
					//user submitting place for this country/module
					$ctx->page_type = "submit";
					return true;
					break;
				case 'sex-forum':
					return $this->process_url_forum($path_arr);
					break;
				case 'uploadVideo':
					$ctx->page_type = "upload_video";
					return true;
					break;
				case 'rss':
					global $gFilename, $gIncFilename, $gModule;
					$gModule = "rss";	
					$gFilename = "rss";
					$gIncFilename = _CMS_ABS_PATH."/_cms_files/female-escorts/rss.php";
					return true;
					break;
				case 'map':
					if (is_file(_CMS_ABS_PATH."/_cms_files/dir/list_map.php")) {
						global $gModule, $gIncFilename, $gFilename;
						$gModule        = "map";
						$gIncFilename   = "./_cms_files/dir/list_map.php";
						$gFilename      = "place";
						$ctx->page_type = "map";
						return true;
					}
					break;
				default:
					return false;
					break;
			}
			return false;
		}

		//old erotic services name redirect
		if (array_key_exists($part, $this->erotic_services_redirect)) {
			$category = $this->erotic_services_redirect[$part];
			$path = str_replace("/{$part}", "/{$category}", $_SERVER['REQUEST_URI']);
			//2019-07-19 google bot and bing still have these pages indexed...
			$this->redirect($path);
		}

		//part can be erotic service
		if (in_array($part, $this->erotic_services)) {
			global $gFilename, $gIncFilename, $gModule;

			$gModule = $part;

			require_once(_CMS_ABS_PATH."/_cms_files/classifieds/array.php");
			$classifieds = new classifieds;
			$ctx->category_id = $classifieds->getTypeByModule($gModule);
			$ctx->category = $part;
			$ctx->category_name = $type_array_[$ctx->category_id];

			//if ($_SESSION["account"] == 3974)
			//	debug_log("category_id={$ctx->category_id}, category={$ctx->category}, category_name={$ctx->category_name}");

			if (!empty($path_arr)) {
				$part2 = array_shift($path_arr);

				if (is_numeric($part2)) {
					//viewing concrete listing
					$ctx->node_id = intval($part2);
					$gFilename = "look";
					$gIncFilename = _CMS_ABS_PATH."/_cms_files/female-escorts/look.php";
					$ctx->page_type = "erotic_services_look";
					return true;
				}

				if ($part2 == "rss") {
					global $gFilename, $gIncFilename, $gModule;
					$gModule = "rss";	
					$gFilename = "rss";
					$gIncFilename = _CMS_ABS_PATH."/_cms_files/female-escorts/rss.php";
					return true;
				}
			}

			//list page
			$gFilename = "index";
			$gIncFilename = _CMS_ABS_PATH."/_cms_files/female-escorts/index.php";
			$ctx->page_type = "erotic_services_index";

			global $page;
			$page = intval($_REQUEST["page"]);
			if ($page == 0)
				$page = 1;

			return true;
		}

		//part MUST BE (!) category

		//we dont quit here in case category name was mispelled
		//if (!$this->fetchCategory($part)) return false;
		$this->fetchCategory($part);
		
		//after category there can be search	
		if ($this->process_url_search($path_arr))
			return true;	//after search there can't be anything else

		if ($this->process_url_order($path_arr) || (empty($path_arr))) {
			// listing page with both location and category selected
			$ctx->page_type = "list";
			return true;
		}

		if ($this->process_url_page($path_arr) || (empty($path_arr))) {
			// listing page with both location and category selected
			$ctx->page_type = "list";
			return true;
		}


		$part = array_shift($path_arr);
		//if the next part is numeric - it is id of personal listing (erotic services: female-escorts, body-rubs, ...)
		if (is_numeric($part)) {
			//TODO check if this category belongs to personal listings - TODO - do we need this ?
			
			//assign item id
			$ctx->node_id = intval($part);
			$ctx->page_type = "item";

			//map page for item
			if (!empty($path_arr) && $path_arr[0] == "map") {
				$ctx->page_type = "map";
				array_shift($path_arr);
			}

			//TODO 
			//if (!empty($path_arr))
			//	return $this->invalid_url("Excess URL path for personal listing: '".print_r($path_arr, true)."' !");
			//there are some places, which name is number, for example https://singapore.adultsearch.com/singapore/whore-house/69/196
			//therefore, if there is anything behind, we treat it as a business listing
			if (empty($path_arr))
				return true;
		}

		if ($this->process_url_business_listing($part, $path_arr))
			return true;	//after business listing there can't be anything else
		
		//TODO the rest of types of URL ?

		$this->cant_resolve();
		die();
	}

	private function redirect_to_biggest_city() {
		global $config_site_url;

		if (($redirect_path = dir::getBiggestCityInCountry()) !== false) {
			$this->redirect($redirect_path);
		}

		//sadly, something went wrong and we didnt find biggest city, redirect to homepage
		$this->redirect($config_site_url);
	}

	private function process_url_legacy_category($part, &$path_arr) {
		/*
		global $gFilename, $gIncFilename, $gModule, $gLocation, $gItemID, $page, $ctx;

		if (array_key_exists($part, $this->old_legacy_categories)) {
			//redirect
			$category = $this->old_legacy_categories[$part];
			$path = str_replace("/{$part}", "/{$category}", $_SERVER['REQUEST_URI']);
			//debug_log("old legacy category redirect: part={$part}, category={$category}, path={$path}");
			$this->redirect($path);
		}

		if (in_array($part, $this->legacy_categories)) {
			$gModule = $part;

			$dir = $gModule;
			if (in_array($gModule, array("swinger-clubs", "bdsm", "topless-pools", "nudist-colonies")))
				$dir = "swinger-clubs";
			if (in_array($gModule, array("male-revues", "male-gay-revues")))
				$dir = "male-revues";
			if (in_array($gModule, array("strip-clubs", "tstv-strip-clubs", "male-strip-clubs")))
				$dir = "strip-clubs";

			if (empty($path_arr)) {
				$gFilename = "index";
				$gIncFilename = "./_cms_files/{$dir}/index.php";
				$ctx->page_type = "list";

				$page = intval($_REQUEST["page"]);
				if ($page == 0)
					$page = 1;

				return true;
			}

			if (count($path_arr) == 1) {
				if ($path_arr[0] == "rss") {
	 				$gFilename = "rss";
					$gIncFilename = "./_cms_files/{$dir}/rss.php";
					return true;
				}
				if ($path_arr[0] == "submit") {
	 				$gFilename = "submit";
					$gIncFilename = "./_cms_files/{$dir}/submit.php";
					return true;
				}
			}

			if (count($path_arr) >= 2) {
				$gLocation = array_shift($path_arr);
				$id_or_action = array_shift($path_arr);
				if ($id_or_action == "review-add") {
					$gFilename = "review-add";
					$gIncFilename = "./_cms_files/{$dir}/review-add.php";
					return true;
				}
				if ($id_or_action == "billing") {
	 				$gFilename = "billing";
					$gIncFilename = "./_cms_files/{$dir}/billing.php";
					return true;
				}
				if ($id_or_action == "club") {
	 				$gFilename = "club";
					$gIncFilename = "./_cms_files/{$dir}/club.php";
					return true;
				}
				if ($id_or_action == "coupon") {
	 				$gFilename = "coupon";
					$gIncFilename = "./_cms_files/{$dir}/coupon.php";
					return true;
				}
				if ($id_or_action == "cover") {
	 				$gFilename = "cover";
					$gIncFilename = "./_cms_files/{$dir}/cover.php";
					return true;
				}
				if ($id_or_action == "fix") {
	 				$gFilename = "fix";
					$gIncFilename = "./_cms_files/{$dir}/fix.php";
					return true;
				}
				if ($id_or_action == "look") {
	 				$gFilename = "look";
					$gIncFilename = "./_cms_files/{$dir}/look.php";
					$ctx->page_type = "item";
					return true;
				}
				if ($id_or_action == "of") {
	 				$gFilename = "of";
					$gIncFilename = "./_cms_files/{$dir}/of.php";
					return true;
				}
				if ($id_or_action == "owner") {
	 				$gFilename = "owner";
					$gIncFilename = "./_cms_files/{$dir}/owner.php";
					return true;
				}
				if ($id_or_action == "parlor") {
	 				$gFilename = "parlor";
					$gIncFilename = "./_cms_files/{$dir}/parlor.php";
					return true;
				}
				if ($id_or_action == "revue") {
	 				$gFilename = "revue";
					$gIncFilename = "./_cms_files/{$dir}/revue.php";
					return true;
				}
				$gItemID = intval($id_or_action);
				if ($gItemID == 0) {
					//redirect
					$this->redirect($ctx->location_path."/".$gModule."/");
					return false;
				}
				if (!empty($path_arr)) {
					//redirect
					$this->redirect($ctx->location_path."/".$gModule."/".$gItemID);
					return false;
				}
				$gFilename = "look";
				$gIncFilename = "./_cms_files/{$dir}/look.php";
				return true;
			}
			//redirect
			$this->redirect($ctx->location_path."/".$gModule."/");
			return false;
		}
		*/
	}
	
	/**
	 * process legacy URLs
	 */
	private function process_url_legacy($dir, &$path_arr) {
		global $gFilename, $gIncFilename, $gModule;

		if ($dir == "subpoena.html")
			$dir = "law-enforcement";

		if (empty($path_arr)) {

			if (substr($dir, -5) == ".html")
				$dir = substr($dir, 0, -5);

			//first try php file directly in _cms_files directory
			$handler_path = _CMS_ABS_PATH."/_cms_files/{$dir}.php";
			if (is_file($handler_path)) {
				$gFilename = $dir;
				$gIncFilename = $handler_path;
				$gModule = $dir;
				return true;
			}
			
			//then try index.php file in directory
			$handler_path = _CMS_ABS_PATH."/_cms_files/{$dir}/index.php";
			if (is_file($handler_path)) {
				$gFilename = "index";
				$gIncFilename = $handler_path;
				$gModule = $dir;
				return true;
			}
			return false;
		}

		$file = $path_arr[0];
		$handler_path = _CMS_ABS_PATH."/_cms_files/{$dir}/{$file}.php";
		
		if (!is_file($handler_path))
			return false;

		$gFilename = $file;
		$gIncFilename = $handler_path;
		$gModule = $dir;
		
		//debug_log("process_url_legacy gFilename={$gFilename}, gIncFilename={$gIncFilename} gModule={$gModule}");
	
		return true;
	}

	/**
	 * process search part of url path
	 */
	private function process_url_search(&$path_arr) {
		global $ctx;

		if ((count($path_arr) < 2) || ($path_arr[0] != "search"))
			return false;
		//ok, this is search
		array_shift($path_arr);
		$ctx->page_type = "search";

		//search keys and values are always in pair
		while (count($path_arr) >= 2) {	//search keys and values are always in pair
		
			//check if these are not order paraemters, if so, quit processing
			if ($this->process_url_order($path_arr))
				break;

			//check if these are not page paraemters, if so, quit processing
			if ($this->process_url_page($path_arr))
				break;

			//TODO uniform sanitizing ?
			$search_key = preg_replace('/[^A-Za-z0-9_-]+/', '', array_shift($path_arr));
			$search_value = preg_replace('/[^A-Za-z0-9_\- ]+/', '', urldecode(array_shift($path_arr)));
			//debug_log("search_key = '$search_key', search_value = '".$search_value."'");
			$ctx->search[$search_key] = $search_value;
		}

		if (count($path_arr) == 1 && $path_arr[0] == "query") {
			//user submitted empty query
			$ctx->search["query"] = "";
		}

		//TODO should this be penalized ?
		//if (count($path_arr) != 0) return $this->invalid_url("Excess URL path for search: '".print_r($path_arr, true)."' !");

		return true;
	}

	/**
	 * Process order part of URL path
	 */
	private function process_url_order(&$path_arr) {
		global $ctx;
		if ((count($path_arr) < 4) || ($path_arr[0] != "order") || ($path_arr[2] != "orderway")) return false;
		array_shift($path_arr);
		$ctx->order_by = array_shift($path_arr);
		array_shift($path_arr);
		$ctx->order_way = intval(array_shift($path_arr));
		
		//after order there can be only page
		$this->process_url_page($path_arr);
		
		return true;
	}

	/**
	 * Process page part of URL path
	 */
	private function process_url_page(&$path_arr) {
		global $ctx;
		if ((count($path_arr) < 2) || ($path_arr[0] != "page") || (!is_numeric($path_arr[1]))) return false;
		array_shift($path_arr);
		$ctx->page = intval(array_shift($path_arr));
		//if URL is /some/thing/page/1 redirect to /some/thing
		if ($ctx->page == 1) {
			$this->redirect($ctx->path_no_page);
			return false;
		}
		return true;
	}

	/**
	 * Process business listing part of URL path
	 */
	private function process_url_business_listing($url_name, &$path_arr) {
		global $ctx, $dir;

		//debug_log("process_url_business_listing, url_name='{$url_name}'");

		if (empty($path_arr) || !is_numeric($path_arr[0]))
			return false;	//this is not business listing, must be in form /url-name-of-business/<numeric_id_of_business>

		//it is business listing
		$ctx->node_id = intval(array_shift($path_arr));
		$ctx->page_type = "item";

		if (!isset($dir))
			$dir = new dir;

		$res = $dir->loadNode();
	
		//we have not found this place by id
		if ($res === false) {
			//first check if it is not old place number
			$row = dir::isOldNode($ctx->node_id, $ctx->location_id, $url_name);
			if ($row !== false) {
				//ok it is old node, redirect to correct URL
				//debug_log("OLD id, row = ".print_r($row, true));
				$correct_url = $dir->getPlaceLink($row);
				//debug_log("OLD id, redirecting to: {$correct_url}");
				$this->redirect($correct_url);
				return false;
			}

			//ok we dont have this place in db, lets redirect to city page
			$this->redirect($ctx->location_path);
			return false;
		}

		//if wrong url name of business place, redirect to correct location
		//TODO should add rest of the path , like /map ... ?
		if (($correct_url_name = $dir->getPlaceUrlName()) != $url_name) {
			//second check if it is old place number with exist duplicate
			$row = dir::isOldNode($ctx->node_id, $ctx->location_id, $url_name);
			if ($row !== false) {
				//ok it is old node, redirect to correct URL
				$correct_url = $dir->getPlaceLink($row);
				$this->redirect($correct_url);
				return false;
			}

			$correct_url = $dir->getPlaceLink(NULL, true);
			//debug_log("misspelled place url name, redirecting to: {$correct_url}");
			$this->redirect($correct_url);
			return false;
		}
	
		//in URL was wrong or mispelled category, redirect to correct URL
		$cat = intval($ctx->item['place_type_id']);
		if ($ctx->category_id == NULL || $ctx->category_id != $cat) {
	
			if (($correct_category_url_name = dir::getCategoryUrlNameById($cat)) !== false) {
				$this->redirect("{$ctx->location_path}/{$correct_category_url_name}/{$correct_url_name}/{$ctx->node_id}");
				return false;
			} else {
				$this->redirect($ctx->location_path);
				return false;
			}
		}

		//review paging
		if ($this->process_url_page($path_arr)) {
			return true;
		}
	
		if (empty($path_arr))
			return true;

		$action = array_shift($path_arr);
		switch ($action) {
			case 'add_review':
				$ctx->page_type = "add_review";
				break;
			case 'map':
				$ctx->page_type = "map";
				break;
			case 'uploadVideo':
				$ctx->page_type = "upload_video";
				break;
			case 'review':
				//TODO redo this part, all the actions with review should be /BUSINESS_LISTING_URL/review/id_of_review/action
				if (empty($path_arr))
								return false;
				$review_id = intval(array_shift($path_arr));
				if ($review_id == 0)
					return false;
				$ctx->review_id = $review_id;
				if (empty($path_arr))
								return false;
				$subaction = array_shift($path_arr);
				if ($subaction == "uploadVideo")
					$ctx->page_type = "upload_video";
				break;
			default:
				return $this->invalid_url("Unknown action '$action' for business listing id = '$ctx->node_id' rest of URL array=".print_r($path_arr, true)."' !");
				break;
		}

		if (empty($path_arr))
			return true;
		
		//subaction ? for now no such thing supported
		return $this->invalid_url("Excess URL path for business listing: '".print_r($path_arr, true)."' !");
	
		return false;
	}
	
	/**
	 * Process forum part of URL path
	 */
	private function process_url_forum(&$path_arr) {
		global $ctx, $gModule, $gFilename, $gIncFilename;

		if (empty($path_arr)) {
			$ctx->page_type = "forum_index";
			return true;
		}
		$part = array_shift($path_arr);

		if ($part == "forum") {
			if (empty($path_arr)) {
				$ctx->page_type = "forum_index";
				return true;
			}
			$part = array_shift($path_arr);
		}

		switch ($part) {
			case "delpost":
				$part2 = array_shift($path_arr);
				if (intval($part2) == 0) {
					return false;
				}
				//delete post
				$ctx->page_type = "forum_reply";
				$ctx->forum_action = "delpost";
				$ctx->forum_post_id = intval($part2);
				return true;
				break;
			case "forum_search":
				//forum search
				$ctx->page_type = "forum_search";
				return true;
				break;

			default:
				$part2 = $part;

				// burak - forum url name support
				if (intval($part2) == 0) {
					if (($fid = forum::getForumIdByUrlName(preg_replace('/[^A-Za-z0-9_-]+/', '', $part2))) !== false)
						$part2 = intval($fid);
				}

				//display forum
				$ctx->forum_id = intval($part2);
				if (empty($path_arr)) {
					$ctx->page_type = "forum";
					return true;
				}

				//new topic
				$part3 = array_shift($path_arr);
				if ($part3 == "new_topic") {
					$ctx->page_type = "forum_topic_new";
					return true;
				}

				//display topic
				if ($part3 == "topic") {
					if (empty($path_arr)) {
						//topic but no topic id ? ->  display whole forum
						$ctx->page_type = "forum";
						return true;
					}
					$part4 = array_shift($path_arr);
					if (intval($part4) == 0)
						return false;

					$ctx->forum_topic_id = intval($part4);
					if (empty($path_arr)) {
						$ctx->page_type = "forum_topic";
						return true;
					}

					$part5 = array_shift($path_arr);
					switch ($part5) {
						case "reply":
							$ctx->page_type = "forum_reply";
							return true;
							break;
						case "watch":
							$ctx->page_type = "forum_reply";
							$ctx->forum_action = "watch";
							return true;
							break;
						case "unwatch":
							$ctx->page_type = "forum_reply";
							$ctx->forum_action = "unwatch";
							return true;
							break;
						case "move":
							$ctx->page_type = "forum_move";
							return true;
							break;
						case "delete":
							$ctx->page_type = "forum_reply";
							$ctx->forum_action = "deltopic";
							return true;
							break;
						case "quote":
							//replying with a quote
							$part6 = array_shift($path_arr);
							if (intval($part6) == 0)
								return false;
							$ctx->forum_post_id = intval($part6);
							$ctx->page_type = "forum_quote";
							return true;
							break;
						case "edit":
							//editing of a post
							$part6 = array_shift($path_arr);
							if (intval($part6) == 0)
								return false;
							$ctx->forum_post_id = intval($part6);
							$ctx->page_type = "forum_edit";
							return true;
							break;
						default:
							return false;
							break;
					}
				}
				return false;
		}
		return false;		
	}

	private function fetchLocation($loc_url_orig, $subdomain_or_path, $parent_id = NULL) {
		global $mcache;
		$loc_url = str_replace(array('"', "'", "\n", "\r"), "", $loc_url_orig);
		if ($loc_url != $loc_url_orig)
			return false;
		if (!$subdomain_or_path) {
			//$key = "SQL:LOC-SUB";
			$key = "SQL:LOC-DIR-CTR";
			$params = array($loc_url);
		} else if ($parent_id == NULL) {
			$key = "SQL:LOC-DIR";
			$params = array($loc_url);
		} else {
			$key = "SQL:LOC-DIR-PAR";
			$params = array($loc_url, $parent_id);
		}

		$res = $mcache->get($key, $params, true);
//		if ($_SESSION["account"] == 3974)
//			echo "key='{$key}', params='".print_r($params, true)."', res=i'".(($res === false)?"false":$res)."'<br />";

		if ($res === false) {
			if ($mcache->multiple_results) {
				debug_log("url::fetchLocation() : Multiple locations found, key='{$key}', params=".print_r($params, true));
				reportAdmin("AS: url::fetchLocation() : Multiple locations found", "", array("key" => $key, "params" => print_r($params, true)));
			}
			return $this->invalid_url("memcache get failed, luo='{$loc_url_orig}', sop='{$subdomain_or_path}', pi='{$parent_id}'");
		} else
			return $res;
	}

	/*
	 * Set up module/category in context based on url-name (e.g. /erotic-massage/)
	 */
	private function fetchCategory($cat) {
		global $ctx, $db, $mcache;

		$this->category_in_url = true;

		//if not unified, check backward compatible category finder. this will be removed after we are unified
		if ($_SESSION["account"] != 3974) {
			// now data for all countries  are unified and data process equal
//		if (in_array($ctx->country_id, array(16046, 16047, 41973))) {
//			//US, Canada, UK have category module
//			$cat_directory = _CMS_ABS_PATH."/_cms_files/".$cat;
//					if (!file_exists($cat_directory))
//				return $this->invalid_url("Category lookup failed: could not category directory '$cat_directory' !");
//			$ctx->module = $cat;
//			return true;
//		}
		}

		//international countries
		$res = $mcache->get("SQL:PTY-URL", array($cat));
		if ($res === false)
			return false;
		$ctx->category_id   = $res['place_type_id'];
		$ctx->category	  = $cat;
		$ctx->category_name = $res['name'];
		return true;
	}

	/*
	 * Function for backward compatibility (will be deleted in future) - setting important global variables
	 */
	private function fillAccountVariables() {
		global $ctx, $account, $gFilename, $gIncFilename, $gModule, $gLocation, $gItemID, $page, $smarty;
	
		$account->core_sub = ($ctx->country == "united-states") ? "" : $ctx->country;
		
		if ($ctx->location_type == 3) {
			//city
			$account->core_loc_id = $ctx->city_id;
			$account->core_state_id = ($ctx->state_id != NULL) ? $ctx->state_id : $ctx->country_id;	//domestic vs. international
			$account->core_loc_type = 3;
			$account->core_loc_array = array(
				"loc_id"	=> $ctx->city_id, 
				"loc_parent"	=> ($ctx->state_id != NULL) ? $ctx->state_id : $ctx->country_id, 
				"loc_name"	=> $ctx->city_name,
				"loc_url"	=> $ctx->city, 
				"loc_type"	=> 3, 
				"lp_loc_name"	=> ($ctx->state_id != NULL) ? $ctx->state_name : $ctx->country_name, 
				"lp_loc_url"	=> ($ctx->state_id != NULL) ? $ctx->state : $ctx->country, 
				"loc_lat"	=> $ctx->city_lat,
				"loc_long"	=> $ctx->city_long,
				"country_sub" => $account->core_sub
				);

			$smarty->assign("cur_loc_id", $ctx->city_id);
			$smarty->assign("cur_loc_name", $ctx->city_name);
			$smarty->assign("cur_loc_url", $ctx->city_loc_url);
			$smarty->assign("cur_lp_loc_name", $ctx->state_name);
			$smarty->assign("cur_lp_loc_url", $ctx->state_loc_url);

			//$gLocation = $ctx->city;	//TODO is this correct ?
		} else if ($ctx->location_type == 2) {
			//state
			$account->core_loc_id = $ctx->state_id;
			$account->core_state_id = $ctx->state_id;
			$account->core_loc_type = 2;
			$account->core_loc_array = array(
				"loc_id"	=> $ctx->state_id, 
				"loc_parent"	=> $ctx->country_id, 
				"loc_name"	=> $ctx->state_name,
				"loc_url"	=> $ctx->state, 
				"loc_type"	=> 2, 
				"lp_loc_name"	=> $ctx->country_name, 
				"lp_loc_url"	=> $ctx->country, 
				"loc_lat"	=> $ctx->state_lat,
				"loc_long"	=> $ctx->state_long,
				"country_sub" => $account->core_sub
				);

			$smarty->assign("cur_loc_id", $ctx->state_id);
			$smarty->assign("cur_loc_name", $ctx->state_name);
			$smarty->assign("cur_loc_url", $ctx->state_loc_url);

		} else {
			//country
			$account->core_loc_id = $ctx->country_id;
			$account->core_loc_type = 1;
			$account->core_loc_array = array(
				"loc_id"	=> $ctx->country_id, 
				"loc_parent"	=> 0, 
				"loc_name"	=> $ctx->country_name,
				"loc_url"	=> $ctx->country, 
				"loc_type"	=> 1, 
				"country_sub" => $account->core_sub
				);
		}

		if ($ctx->node_id != NULL) $gItemID = $ctx->node_id;

		//if ($_SESSION["account"] == 3974)
		//	debug_log("page_type='{$ctx->page_type}', gFilename='{$gFilename}', gIncFilename='{$gIncFilename}', gModule='{$gModule}'");

		//debug_log("gFilename=$gFilename gIncFilename=$gIncFilename gModule=$gModule");
		//if variables are already set by some class, dont process directory things...
		if (isset($gFilename) && isset($gIncFilename) && isset($gModule)) {
			return true;
		}

		$account->international = 1;
		switch ($ctx->page_type) {
			case 'city':
				$gIncFilename = "./_cms_files/dir/city.php";
				$gFilename = "city";
				$gModule = $ctx->country;
				break;
			case 'list':
				$gIncFilename = "./_cms_files/dir/home.php";
				$gFilename = "home";
				$gModule = $ctx->country;
				break;
			case 'item':
				$gIncFilename = "./_cms_files/dir/look.php";
				$gFilename = "look";
				$gModule = $ctx->country;
				break;
			case 'add_review':
				$gIncFilename = "./_cms_files/dir/review-add.php";
				$gFilename = "review-add";
				$gModule = "review-add";
				break;
			case 'fix':
				$gIncFilename = "./_cms_files/dir/fix.php";
				$gFilename = "fix";
				$gModule = "fix";
				break;
			case 'submit':
				$gIncFilename = "./_cms_files/dir/submit.php";
				$gFilename = "submit";
				$gModule = $ctx->country;
				break;
			case 'geo_redirect':
				$gIncFilename = "./_cms_files/geo_redirect.php";
				$gFilename = "geo_redirect";
				$gModule = "geo_redirect";
				break;
			case 'map':
			case 'global_map':
				$gIncFilename = "./_cms_files/map.php";
				$gFilename = "map";
				$gModule = $ctx->country;
				break;
			case 'updatemap':
				$gIncFilename = "./_cms_files/dir/updatemap.php";
				$gFilename = "updatemap";
				$gModule = $ctx->country;
				break;
			case 'forum_index':
				$gIncFilename = "./_cms_files/sex-forum/index.php";
				$gFilename = "index";
				$gModule = "sex-forum";
				break;
			case 'forum':
				$gIncFilename = "./_cms_files/sex-forum/viewforum.php";
				$gFilename = "viewforum";
				$gModule = "sex-forum";
				break;
			case 'forum_topic_new':
				$gIncFilename = "./_cms_files/sex-forum/forum-new-topic.php";
				$gFilename = "forum-new-topic";
				$gModule = "sex-forum";
				break;
			case 'forum_topic':
				$gIncFilename = "./_cms_files/sex-forum/viewtopic.php";
				$gFilename = "viewtopic";
				$gModule = "sex-forum";
				break;
			case 'forum_reply':
				$gIncFilename = "./_cms_files/sex-forum/forum_reply.php";
				$gFilename = "forum_reply";
				$gModule = "sex-forum";
				break;
			case 'forum_quote':
				$gIncFilename = "./_cms_files/sex-forum/forum_quote.php";
				$gFilename = "forum_quote";
				$gModule = "sex-forum";
				break;
			case 'forum_edit':
				$gIncFilename = "./_cms_files/sex-forum/forum_edit.php";
				$gFilename = "forum_edit";
				$gModule = "sex-forum";
				break;
			case 'forum_move':
				$gIncFilename = "./_cms_files/sex-forum/forum_move.php";
				$gFilename = "forum_move";
				$gModule = "sex-forum";
				break;
			case 'forum_search':
				$gIncFilename = "./_cms_files/sex-forum/forum_search.php";
				$gFilename = "forum_search";
				$gModule = "sex-forum";
				break;
			case 'ad':
				$gIncFilename = "./_cms_files/ad.php";
				$gFilename = "ad";
				$gModule = "ad";
				break;
			case 'legacy_search':
				$gIncFilename = "./_cms_files/search.php";
				$gFilename = "search";
				$gModule = "search";
				break;
			case 'upload':
				$gIncFilename = "./_cms_files/upload.php";
				$gFilename = "upload";
				$gModule = "upload";
				break;
			case 'upload_video':
				$gIncFilename = "./_cms_files/dir/upload_video.php";
				$gFilename = "upload_video";
				$gModule = "upload_video";
				break;
			case 'worker':
				$gIncFilename = "./_cms_files/dir/worker.php";
				$gFilename = $ctx->country;
				$gModule = $ctx->country;
				$ctx->node_id = (intval($_REQUEST["id"]) > 0) ? intval($_REQUEST["id"]) : NULL;
				break;
			case 'owner':
				$gIncFilename = "./_cms_files/dir/owner.php";
				$gFilename = $ctx->country;
				$gModule = $ctx->country;
				$ctx->node_id = (intval($_REQUEST["id"]) > 0) ? intval($_REQUEST["id"]) : NULL;
				break;
			default:
				//sane default values ? - home.php
				$gIncFilename = "./_cms_files/dir/home.php";
				$gFilename = "home";
				$gModule = $ctx->country;
				break;
		}

		if ($ctx->page != NULL)	$page = $ctx->page;
	
		if ($ctx->search != NULL && isset($ctx->search['query']))
			$_GET["query"] = $ctx->search['query'];

		//admin overrides
		if (isset($_REQUEST["gIncFilename"]) && $account->isrealadmin()) {
			$gIncFilename = preg_replace('/[^A-Za-z0-9_\-.\/]+/', '', $_REQUEST["gIncFilename"]);
		}

		return;
	}

	public function debugAccount() {
		global $account, $gFilename, $gIncFilename, $gModule, $gLocation, $gItemID, $page;
		debug_log("debug Account: international={$account->international}, core_loc_type={$account->core_loc_type}, core_loc_id={$account->core_loc_id}, core_state_id={$account->core_state_id}, core_loc_array=".print_r($account->core_loc_array, true));
		debug_log("debug Global: gFilename={$gFilename}, gIncFilename={$gIncFilename}, gModule={$gModule}, gLocation={$gLocation}, gItemID={$gItemID}, page={$page}");
	}
	
	public function debugContext() {
		global $ctx;
		debug_log("debug Context: ".print_r($ctx, true));
	}

	/*
	 * Invalid URL handler.
	 * Should log (?) the invalid URL and reason of invalidity.
	 * Should redirect to some sane location
	 */
	private function invalid_url($reason) {

//TODO enable this again when fighting excessive SQL loction lookups
//	if ($_SESSION["account"] == 3974)
//		debug_log("URL error: $reason, URL = '".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]."', referer='".$_SERVER['HTTP_REFERER']."'");
		
		//TODO smart redirect to close location (preserve location if possible)_

		return false;
	}

	/**
	 * This function is called when we cant resolve the URL
	 */
	private function cant_resolve() {
		if (is_bot()) {
			//dont report these errors by bots
			//reportAdmin("AS: url::process_path() - did not find path handler", "", array("URL" => $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]));
		} else {
			//reportAdmin("AS: url::process_path() - did not find path handler", "", array("URL" => $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]));
			//debug_log("URL: Can't resolve '".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."', referer='".$_SERVER["HTTP_REFERER"]."', uagent='".$_SERVER["HTTP_USER_AGENT"]."'");
		}
		//display404();
		$this->redirect("/");
		die();
	}

	private function redirect($path) {
		system::moved($path);
	}

	//empty constructor
	public function __construct() {
	}
}

//END
