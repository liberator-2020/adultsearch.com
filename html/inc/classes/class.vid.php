<?php

/**
 * Classified ad video's handling class
 * classified_video table
 */
class vid {

	private $id = NULL,
			$account_id = null,
			$classified_id = null,
			$filename = null,
			$thumbnail = null,
			$width = null,
			$height = null,
			$converted = null,
			$public_verification = null,
			$created_stamp = null,
			$deleted_stamp = null,
			$deleted_by = null;
			
	function __construct() {
	}

	public static function findOneById($id) {
		global $db;

		$res = $db->q("SELECT * FROM classified_video WHERE id = ?", array(intval($id)));
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

		return self::withRow($row);
	}

	public static function withRow($row) {

		$v = new vid();
		$v->setId($row["id"]);
		$v->setAccountId($row["account_id"]);
		$v->setClassifiedId($row["classified_id"]);
		$v->setFilename($row["filename"]);
		$v->setThumbnail($row["thumbnail"]);
		$v->setWidth($row["width"]);
		$v->setHeight($row["height"]);
		$v->setConverted($row["converted"]);
		$v->setPublicVerification($row["public_verification"]);
		$v->setCreatedStamp($row["created_stamp"]);
		$v->setDeletedStamp($row["deleted_stamp"]);
		$v->setDeletedBy($row["deleted_by"]);

		return $v;
	}

	//getters and setters
	public function getId() {
		return $this->id;
	}
	public function getAccountId($account_id) {
		return $this->account_id;
	}
	public function getClassifiedId($classified_id) {
		return $this->classified_id;
	}
	public function getFilename($filename) {
		return $this->filename;
	}
	public function getThumbnail($thumbnail) {
		return $this->thumbnail;
	}
	public function getWidth($width) {
		return $this->width;
	}
	public function getHeight($height) {
		return $this->height;
	}
	public function getConverted($converted) {
		return $this->converted;
	}
	public function getPublicVerification($public_verification) {
		return $this->public_verification;
	}
	public function getCreatedStamp($created_stamp) {
		return $this->created_stamp;
	}
	public function getDeletedStamp($deleted_stamp) {
		return $this->deleted_stamp;
	}
	public function getDeletedBy($deelted_by) {
		return $this->deleted_by;
	}

	public function setId($id) {
		$this->id = $id;
	}
	public function setAccountId($account_id) {
		$this->account_id = $account_id;
	}
	public function setClassifiedId($classified_id) {
		$this->classified_id = $classified_id;
	}
	public function setFilename($filename) {
		$this->filename = $filename;
	}
	public function setThumbnail($thumbnail) {
		$this->thumbnail = $thumbnail;
	}
	public function setWidth($width) {
		$this->width = $width;
	}
/*
	public function set($) {
		$this-> = $;
	}
	public function set($) {
		$this-> = $;
	}
	public function set($) {
		$this-> = $;
	}
	public function set($) {
		$this-> = $;
	}
	public function set($) {
		$this-> = $;
	}
*/



	//other methods

	/** Return web URI for thumbnails */
	public static function thumbnailUri($thumbnail_file, $with_url=false) {
		global $config_image_server;
		return (($with_url)?$config_image_server:'') . '/classifieds/video/'.$thumbnail_file;
	}
	/** Return web URI for videos */
	public static function videoUri($video_file, $with_url=false) {
		global $config_image_server;
		return (($with_url)?$config_image_server:'') . '/classifieds/video/'.$video_file;
	}
	/** Return full path for thumbnails 
	 * @return string
	 */
	public static function thumbnailPath($thumbnail_file) {
		global $config_image_path;
		return $config_image_path . '/classifieds/video/'.$thumbnail_file;
	}
	/** Returns full path to video
	 *	@return string
	 */
	public static function videoPath($video_file) {
		global $config_image_path;
		return $config_image_path . '/classifieds/video/'.$video_file;
	}


	private static function json_response($arr) {
		header('Content-Type: application/json');
		die(json_encode($arr));
	}

	/**
	 * Return list of videos
	 * @global type $account
	 * @global type $db
	 * @global type $config_image_path
	 */
	public static function test() {
		global $account, $db, $config_image_path;
		$res = $db->q('SELECT * FROM classified_video WHERE account_id=?', array($account->getId()));
		while( $row = $db->r($res) ) {
			$response[] = $row;
		}
		json_response($response);

	}


	/**
	 * Upload advertisement videos, rename into target upload folder, create database entry.
	 *
	 * @global mixed $account
	 * @global PDOConn $db
	 * @global string $config_image_path
	 *
	 * 
	 */
	public static function upload() {
		global $account, $db, $config_image_path;

		//file_log("vid", "GET='".print_r($_GET, true)."', POST='".print_r($_POST, true)."', FILES='".print_r($_FILES, true));

		//fetch clad
		$clad_id = $_REQUEST["ad_id"];
		$clad = clad::findOneById($clad_id);
		if (!$clad)
			die("Error: can't find clad by id '{$clad_id}' !");

		//target video folder for uploaded , but not processed videos
		$tgt_path = $config_image_path."/classifieds/uploaded/";
		if (!is_dir($tgt_path))
			mkdir($tgt_path, 0755, true);

		//upload handler uploads video file(s) into tmp folder: _CMS_ABS_PATH."/tmp/vid"
		$handler = new uploadhandler();
		$handler->initialize("video");
		$response = $handler->get_response();
		if (is_array($response) && array_key_exists("video", $response) && count($response["video"]) > 0) {
			$items = $response["video"];
		}

		foreach ($items as $file) {
			file_log("vid", "jay: file");
			if (property_exists($file, "error") && $file->error) {
				//this is error, for example image does not meet minimum width/height requirements
				file_log("vid", "jay error: '{$file->error}'");
				$files[] = $file;
				continue;
			}

			$filename = $file->name;
			$file->path = "/tmp/vid/{$file->name}";	//url path
			$filepath = _CMS_ABS_PATH."/tmp/vid/".$filename;	//absolute path on disk

			//assure we have correct mime type
			$mime_type = mime_content_type($filepath);
			if (substr($mime_type, 0, 6) != "video/") {
				file_log("vid", "jay: Invalid file mime type: mime_type='{$mime_type}', type='{$type}' !");
				unlink($filepath);
				$file->error = "Invalid file type";
				$files[] = $file;
				continue;
			}

			file_log("vid", "jay: filename='{$filename}', filepath='{$filepath}', mime_type='{$mime_type}'");
			

			//succesfully uploaded video file
			file_log("vid", "jay: ProfileController:upload: video uploaded, filename='{$filename}'");
			unset($file->deleteUrl);
			unset($file->deleteType);
			unset($file->url);
			unset($file->path);

			//make new filename character safe
			$new_filename = "{$clad_id}_{$filename}";
			$new_filename = preg_replace('/[^a-zA-Z0-9_\-\.]/', '', preg_replace('/ /', '_', $new_filename));
			$tgt_filepath = "{$tgt_path}/{$new_filename}";
			// @todo Oleg
			file_log("vid", "Renaming '{$filepath}' to temp folder '{$tgt_filepath}' ....");
			$ret = rename($filepath, $tgt_filepath);
			if (!$ret) {
				file_log("vid", "can't move uploaded video '{$filepath}' to temp folder '{$tgt_filepath}' !");
			} else {
				//create image entry in DB

				$width = (property_exists($file, "width") && intval($file->width)) ? intval($file->width) : null;
				$height = (property_exists($file, "height") && intval($file->height)) ? intval($file->height) : null;
				$public_verification = ($_REQUEST["public_verification"] == "1") ? 1 : null;

				$res = $db->q("INSERT INTO classified_video(
					account_id, classified_id, filename,
					width, height,
					converted, public_verification, created_stamp
					) VALUES (
					?, ?, ?,
					?, ?,
					?, ?, ?)",
					[$clad->getAccountId(), $clad->getId(), $new_filename, 
						$width, $height,
						0, $public_verification, time()]
					);
				$video_id = $db->insertid($res);
				if (!$video_id) {
					file_log("vid", "vid:upload: Error can't insert video into db: {$clad->getAccountId()}, {$clad_id}, {$new_filename}, {$width}, {$height}, {$public_verification} !");
					reportAdmin("AS: vid:upload: Error", "can't insert video into db: {$clad->getAccountId()}, {$clad_id}, {$new_filename}, {$width}, {$height}, {$public_verification} !");
					//TODO
					die;
				}
				$file->video_id = $video_id;
				file_log("vid", "vid:upload: Video #{$video_id} uploaded successfully.");
			}

			$files[] = $file;
		}

		$response = array("files" => $files, "profile_id" => $id);
		json_response($response);
	}

	public static function delete() {
		global $account, $db, $config_image_path;

		$video_id = intval($_REQUEST["video_id"]);
		if (!$video_id) {
			$video_id = 0;
		}

		$res = $db->q('SELECT id, filename, thumbnail FROM classified_video WHERE id=? AND account_id=?', array($video_id, $account->getId()));

		$row = $db->r($res);

		if( !$row ) {
			json_response(['status_code'=>404, 'deleted'=>false,'message'=>'Image not found']);
		}

		$b1 = @unlink( self::videoPath( $row['filename'] ));
		$b2 = @unlink( self::thumbnailPath( $row['thumbnail']));

		$res = $db->q('DELETE FROM classified_video WHERE id=? AND account_id=?', array($video_id, $account->getId()));

		file_log("vid", "vid:delete: Video #{$video_id} deleted successfully. $b1 $b2");

		json_response(['status_code'=>200, 'deleted'=>true,'message'=>'Deleted video file']);
	}

	/**
	 * Check status of video thumbnail production.
	 *
	 * respond to AJAX request /vid/check?video_id=
	 *
	 * Returns 0 if the job is not completed.
	 * Returns 1 if there was an error
	 * Returns 2 if video thumbnail has been created successfully.
	 *
	 * @global type $account
	 * @global type $db
	 * @global type $config_image_path
	 * @throws \Exception
	 */
	public static function check() {
		global $account, $db, $config_image_path;

		//find image for which we requested to check the video
		$video_id = intval($_REQUEST["video_id"]);
		if (!$video_id) {
			json_response(["status" => 1]);
		}

		$res = $db->q('SELECT * FROM classified_video WHERE id=? AND account_id=?', array($video_id, $account->getId()));

		$row = $db->r($res);

		$status = 0;
		$path = $thumb_path = null;

		if( !$row ) {
			$status = 0;
			$path = $thumb_path = null;
		} else {

			$status = (int)$row['converted'];
			$thumb_path = self::thumbnailUri($row['thumbnail']);
			$path = self::videoUri($row['filename']);
		}

		//send success message
		json_response(["status" => $status, "path" => $path, "thumb_path" => $thumb_path]);
	}

}

