<?php

define('VIDEO_TYPE_PLACE', 1);
define('VIDEO_TYPE_LOCATION', 2);
define('VIDEO_TYPE_REVIEW', 3);

/**
 * common video functions
 * Class 
 * @author jay
 */
class video {
	private $file_id = NULL,
			$module = NULL,
			$type = NULL,
			$country = NULL,
			$filename = NULL,
			$thumb = NULL,
			$name = NULL,
			$description = NULL,
			$visible = NULL,
			$place_id = NULL,
			$orden = NULL,
			$place_name = NULL,
			$location_name = NULL;

	private static $extension_whitelist = array("flv", "mp4", "mov", "m4v", "f4v");	//formats supported by flowplayer (http://flowplayer.org/documentation/technical-facts.html)

	//constructors
	function __construct($file_id = null) {
		$this->file_id = intval($file_id);
	}
	
	public static function withAll($file_id, $module, $loc_id, $country, $filename, $thumb, $name, $description, $visible) {
		$instance = new self($file_id);
		$instance->setModule($module);
		$type = ($module == "review") ? VIDEO_TYPE_REVIEW : (($loc_id == NULL) ? VIDEO_TYPE_PLACE : VIDEO_TYPE_LOCATION);
		$instance->setType($type);
		$instance->setCountry($country);
		$instance->setFilename($filename);
		$instance->setThumb($thumb);
		$instance->setName($name);
		$instance->setDescription($description);
		$instance->setVisible($visible);
		return $instance;
	}
	
	public static function withRow($row) {
		$instance = new self($row["picture"]);
		$instance->setModule($row["module"]);
		$type = ($row['module'] == "review") ? VIDEO_TYPE_REVIEW : (($row["loc_id"] == NULL) ? VIDEO_TYPE_PLACE : VIDEO_TYPE_LOCATION);
		$instance->setType($type);
		$instance->setCountry($row["country"]);
		$instance->setFilename($row["filename"]);
		$instance->setThumb($row["thumb"]);
		$instance->setName($row["name"]);
		$instance->setDescription($row["description"]);
		$instance->setVisible($row["visible"]);
		$instance->setPlaceId($row["place_id"]);
		$instance->setOrden($row["orden"]);
		$instance->setPlaceName($row["place_name"]);
		$instance->setLocationName($row["location_name"]);
		return $instance;
	}

	public function fetch() {
		global $db;

		if (intval($this->file_id) == 0)
			return false;

		$sql = "SELECT pp.*
						, (CASE WHEN pp.id = 0 THEN lc.dir WHEN pp.module = 'place' THEN lc1.dir WHEN pp.module = 'review' THEN lc2.dir ELSE NULL END) as country
						, (CASE WHEN pp.id = 0 THEN l.loc_name WHEN pp.module = 'place' THEN l1.loc_name WHEN pp.module = 'review' THEN l2.loc_name ELSE NULL END) as loc_name
				FROM place_picture pp
			
				LEFT JOIN location_location l on l.loc_id = pp.loc_id
				LEFT JOIN location_location lc on l.country_id = lc.loc_id

				LEFT JOIN place p1 on pp.id = p1.place_id
				LEFT JOIN location_location l1 on p1.loc_id = l1.loc_id
				LEFT JOIN location_location lc1 on l1.country_id = lc1.loc_id

				LEFT JOIN place_review pr on pp.id = pr.review_id
				LEFT JOIN place p2 on pr.id = p2.place_id
				LEFT JOIN location_location l2 on p2.loc_id = l2.loc_id
				LEFT JOIN location_location lc2 on l2.country_id = lc2.loc_id

				WHERE picture = '{$this->file_id}' and place_file_type_id = 2";

		$res = $db->q($sql);
		if ($db->numrows($res) != 1)
			return false;
		$row = $db->r($res);

		$this->module	= $row['module'];
		$this->type		= ($row['module'] == "review") ? VIDEO_TYPE_REVIEW : (($row["loc_id"] == NULL) ? VIDEO_TYPE_PLACE : VIDEO_TYPE_LOCATION);
		$this->filename = $row['filename'];
		$this->thumb	= $row['thumb'];
		$this->name		= $row['name'];
		$this->description = $row['description'];
		$this->visible	= ($row['visible']) ? true : false;
		$this->orden    = $row["orden"];
		$this->country  = $row["country"];
		$this->location_name = $row['loc_name'];
		return true;
	}

	//getters
	public function getFileId() {
		return $this->file_id;
	}
	public function getModule() {
		return $this->module;
	}
	public function getType() {
		return $this->type;
	}
	public function getCountry() {
		return $this->country;
	}
	public function getFilename() {
		return $this->filename;
	}
	public function getThumb() {
		return $this->thumb;
	}
	public function getName() {
		return $this->name;
	}
	public function getDescription() {
		return $this->description;
	}
	public function getVisible() {
		return $this->visible;
	}
	public function isVisible() {
		return $this->visible;
	}
	public function getPlaceId() {
		return $this->place_id;
	}
	public function getOrden() {
		return $this->orden;
	}
	public function getPlaceName() {
		return $this->place_name;
	}
	public function getLocationName() {
		return $this->location_name;
	}
	public function isExternal() {
		return (substr($this->filename, 0, 4) == "http") ? true : false;
	}
	
	//setters
	public function setModule($module) {
		$this->module = $module;
	}
	public function setType($type) {
		$this->type = $type;
	}
	public function setCountry($country) {
		$this->country = $country;
		}
	public function setFilename($filename) {
		$this->filename = $filename;
	}
	public function setThumb($thumb) {
		$this->thumb = $thumb;
	}
	public function setName($name) {
		$this->name = $name;
	}
	public function setDescription($description) {
		$this->description = $description;
	}
	public function setVisible($visible) {
		$this->visible = ($visible) ? true : false;
	}
	public function setPlaceId($place_id) {
		$this->place_id = $place_id;
	}
	public function setOrden($orden) {
		$this->orden = (intval($orden) > 0) ? intval($orden) : NULL;
	}
	public function setPlaceName($place_name) {
		$this->place_name = $place_name;
	}
	public function setLocationName($location_name) {
		$this->location_name = $location_name;
	}

	//"configuration"
	public static function getStageDir() {
		return $_SERVER['DOCUMENT_ROOT']."/tmp/upload_stage/";
	}
	public static function getStageUrlFolder() {
		return "/tmp/upload_stage";
	}
	public static function getVideoFolder($module = "") {
		global $config_image_path;

		$dir = $config_image_path."videos";
		if ($module != "")
			$dir .= "/".$module;
		return $dir;
	}
	public static function getVideoUrlFolder($module = "") {
		global $config_image_server, $ctx;
		$dir = "https:";
		$dir .= $config_image_server."/videos/";
		if ($module != "")
			$dir .= "$module"."/";
		return $dir;
	}

	//methods
	public function getFolderAbsPath() {
		if ($this->module == "place")
			$dir = $this->country;
		else
			$dir = $this->module;
		return self::getVideoFolder($dir);
	}

	public function getVideoAbsPath() {
		if ($this->module == "place")
			$dir = $this->country;
		else
			$dir = $this->module;
		return self::getVideoFolder($dir)."/".$this->filename;
	}
	
	public function getVideoUrlPath() {
		if ($this->module == "place")
			$dir = $this->country;
		else
			$dir = $this->module;
		return self::getVideoUrlFolder($dir).$this->filename;
	}
	
	public function getThumbAbsPath() {
		if (!$this->getThumb())
			return false;
		if ($this->module == "place")
			$dir = $this->country;
		else
			$dir = $this->module;
		return self::getVideoFolder($dir)."/".$this->thumb;
	}

	public function getThumbUrlPath() {
		global $config_dev_server;

		$default_sshot_filename = "default_screenshot.jpg";
		$thumb_url = "";
		if ($this->module == "place")
			$dir = $this->country;
		else
			$dir = $this->module;

		if ($config_dev_server) {
			if ($this->thumb == "")
				return self::getVideoUrlFolder()."/".$default_sshot_filename;
			else
				return self::getVideoUrlFolder($dir).$this->thumb;
		}

		if (($ext_pos = strrpos($this->filename, ".")) !== false) {
			if ($this->thumb != NULL)
				$sshot_filename = $this->thumb;
			else
				$sshot_filename = substr($this->filename, 0, $ext_pos)."_screenshot.jpg";
			$sshot_path = self::getVideoFolder($dir)."/".$sshot_filename;
			$default_sshot_path = self::getVideoFolder()."/".$default_sshot_filename;
			if (file_exists($sshot_path)) {
				$thumb_url = self::getVideoUrlFolder($dir).$sshot_filename;
			} else if (file_exists($default_sshot_path)) {
				$thumb_url = self::getVideoUrlFolder().$default_sshot_filename;
			}
		}

		return $thumb_url;
	}

	public function getTemplateArray() {
		return array(
			"file_id" => $this->getFileId(),
			"name"    => $this->getName(),
			"url"     => $this->getVideoUrlPath(),
			"thumb"   => $this->getThumbUrlPath(),
			"place_link" => $this->getPlaceLink(),
		);
	}

	public static function getAllTemplateArrays($input_array) {
		$output_array = array();
		foreach ($input_array as $item) {
			$output_array[] = $item->getTemplateArray();
		}
		return $output_array;
	}

	public function getPlaceLink() {
		global $ctx;
		if (!$this->place_id)
			return false;
		$item_bak = $ctx->item;
		$ctx->item = NULL;
		$node_id_bak = $ctx->node_id;
		$ctx->node_id = $this->place_id;
		$place_link = dir::getPlaceLink();
		$ctx->item = $item_bak;
		$ctx->node_id = $node_id_bak;
		return $place_link;
	}

	/*
	 * /www/virtual/adts/img.adultsearch.net/html/videos/philippines/Carousel.mp4 -> philippines/Carousel.mp4
	 */
	public static function getVideoRelativePath($abs_fpath) {
		//debug_log("1 abs_fpath = $abs_fpath");
		$video_folder = self::getVideoFolder();
		if (strpos($abs_fpath, $video_folder) === 0)
			$rel_fpath = substr($abs_fpath, strlen($video_folder) + 1);	   // + 1 because of additional /
		else
			$rel_fpath = basename($abs_fpath);		//safe fallback
		//debug_log("2 rel_fpath = $rel_fpath");
		return $rel_fpath;
	}

	public static function storeVideoInfoToSession() {
		global $ctx;
		//TODO (country vs domestic modules)
		if ($ctx->review_id != NULL) {
			$_SESSION["upl_vid_type"] = "review";
			$_SESSION["upl_vid_module"] = "review";
			$_SESSION["upl_vid_id"] = $ctx->review_id;
			$_SESSION["upl_vid_place_link"] = dir::getPlaceLink();
		} else if ($ctx->node_id != NULL) {
			$_SESSION["upl_vid_type"] = "place";
			$_SESSION["upl_vid_module"] = "place";
			$_SESSION["upl_vid_country"] = $ctx->country;
			$_SESSION["upl_vid_id"] = $ctx->node_id;
			$_SESSION["upl_vid_place_link"] = dir::getPlaceLink();
		} else if ($ctx->location_id != NULL) {
			$_SESSION["upl_vid_type"] = "location";
			$_SESSION["upl_vid_module"] = "place";
			$_SESSION["upl_vid_country"] = $ctx->country;
			$_SESSION["upl_vid_id"] = $ctx->location_id;
			$_SESSION["upl_vid_place_link"] = $ctx->location_path;
		}

		//debug_log("Storing video info to session. session_id=".session_id()." sess_arr=".print_r($_SESSION, true));
	}

	public static function validateFilename($filename) {
		// Validate file extension
		$path_info = pathinfo($filename);
		$file_extension = $path_info["extension"];
		$is_valid_extension = false;
		foreach (self::$extension_whitelist as $extension) {
			if (strcasecmp($file_extension, $extension) == 0) {
				$is_valid_extension = true;
				break;
			}
		}
		if (!$is_valid_extension) {
			debug_log("class video:validateFilename: extension '$file_extension' not valid!");
			return false;
		}
		return true;
	}

	public static function getUniqueFilename($module, $filename) {
		//TODO replace weird characters
		$filename = preg_replace('/[^A-Za-z0-9-.]/', '.', $filename);
		$filename = preg_replace('/\.+/', '.', $filename);
		$filename = trim($filename, '.');
		if (empty($filename))
			$filename = date("YmdHis");

		//make sure file does not exist, so we dont overwrite other file
		//if fiel exists, add number at the end of file, e.g. filename1.ext, filename2.ext, ...
		$i = 0;
		do {
			if ($i > 0) {
				$pos = strrpos($filename, '.');
				if ($pos === false)
					$new_filename = $filename.$i;
				else
					$new_filename = substr($filename, 0, $pos).$i.substr($filename, $pos);
			} else {
				$new_filename = $filename;
			}
			$i++;
		} while (file_exists(self::getDestFilePath($module, $new_filename)));

		return $new_filename;
	}

	public static function getDestFilePath($module, $filename) {
		$dir_path = self::getVideoFolder($module);
		if (!file_exists($dir_path)) {
			debug_log("video: creating folder '$dir_path'");
			if (!mkdir($dir_path))
				return false;
		}
		return $dir_path."/".$filename;
	}

	public static function getDefaultVideoFilename() {
		global $ctx;

		$video_filename = $ctx->country.".mp4";
		if (file_exists(self::getVideoFolder($ctx->country)."/".$video_filename))
			return $video_filename;

		$video_filename = $ctx->country.".flv";
		if (file_exists(self::getVideoFolder($ctx->country)."/".$video_filename))
			return $video_filename;

		return false;
	}

	public static function removeOldStageFiles() {
		//remove old files from stage directory
		if ($handle = opendir(self::getStageDir())) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry == "." || $entry == "..")
					continue;
				$mtime = filemtime(video::getStageDir() . $entry);
				if ((time() - $mtime) > 86400) {
					$path = video::getStageDir().$entry;
					if (is_dir($path))
						self::rrmdir($path);
					else
						unlink($path);
				}
			}
			closedir($handle);
		}
	}

	public function agetExternalVideoHtmlCode($width = 260, $height = 220) {
		$url = $this->filename;
		if (strpos($url, "http") !== 0)
			return false;
		
		//youtube
		if (preg_match('@youtube.com/v/([a-zA-Z0-9\-]+)$@i', $url, $matches) === 1) {
			$yt_code = $matches[1];
			$video_code = "<iframe width=\"$width\" height=\"$height\" src=\"https://www.youtube.com/embed/{$yt_code}?wmode=transparent&rel=0\" frameborder=\"0\" wmode=\"Opaque\" allowfullscreen></iframe>";
			return $video_code;
		}

		//unknown video url format
		return "";
	}

	public function agetInternalVideoHtmlCode() {
		return $this->agetInternalVideoHtmlCodeAux("vid_".$this->getFileId());
	}

	public function agetInternalVideoHtmlCodeAux($identifier) {

		//check if internal video file exists
		$video_path = $this->getVideoAbsPath();
		if (!file_exists($video_path))
			return false;
		$video_url_path = $this->getVideoUrlPath();

		//screenshot
		$thumb_url = $this->getThumbUrlPath();

/*
		include_html_head("js", "/js/flowplayer.js");
		include_html_head("js", "/js/flowplayer.ipad.js");

		$video_html .= "<a href=\"{$video_url_path}\" style=\"display: block; width: 261px; height: 168px; position: relative;\" id=\"flow_{$identifier}\" class=\"".substr($video, 0, $ext_pos)."\" data-video-file-id=\"{$file_id}\"><img src=\"{$thumb_url}\" style=\"width: 100%; height: 100%; position: relative; z-index: 99;\" /><img src=\"/images/play_50.png\" style=\"display: block; top: 59px; left: 105px; position: absolute; z-index: 100;\" /></a>";

		$video_html .= self::getFlowplayerJsCode($identifier);
*/

		$video_html .= '<link rel="stylesheet" href="//releases.flowplayer.org/7.0.2/skin/skin.css?b"><script src="//releases.flowplayer.org/7.0.2/flowplayer.min.js"></script>';
		$video_html .= "<div class=\"flowplayer\"><video poster=\"{$thumb_url}\"><source type=\"video/mp4\" src=\"{$video_url_path}\"></video></div>";
		return $video_html;
	}

	public static function getThumbUrl($module, $video, $thumb = NULL) {
		$default_sshot_filename = "default_screenshot.jpg";
		$thumb_url = "";

		if (($ext_pos = strrpos($video, ".")) !== false) {
			if ($thumb != NULL)
				$sshot_filename = $thumb;
			else
				$sshot_filename = substr($video, 0, $ext_pos)."_screenshot.jpg";
			$sshot_path = self::getVideoFolder($module)."/".$sshot_filename;
			$default_sshot_path = self::getVideoFolder()."/".$default_sshot_filename;
			if (file_exists($sshot_path)) {
				$thumb_url = self::getVideoUrlFolder($module).$sshot_filename;
			} else if (file_exists($default_sshot_path)) {
				$thumb_url = self::getVideoUrlFolder().$default_sshot_filename;
			}
		}

		return $thumb_url;
	}

	public static function getFlowplayerJsCode($identifier) {
		global $account;
		$onstart_hook = "";

		//default valus
		$controls = ", time: false, stop: true";

//		if ($identifier == "vid_city")
			$controls .= ", 
bufferGradient: 'none', 
callType: 'default',
tooltipTextColor: '#D00000',
sliderGradient: 'none',
buttonOverColor: '#5eabe5',
autoHide: 'never',
progressColor: '#112233',
buttonColor: '#FFFFFF',
timeBgColor: '#262626',
backgroundColor: '#222222',
timeColor: '#B1E0FC',
timeBorder: '1px solid rgba(0, 0, 0, 0.3)',
bufferColor: '#445566',
disabledWidgetColor: '#555555',
progressGradient: 'none',
durationColor: '#ffffff',
volumeSliderColor: '#D00000',
volumeSliderGradient: 'none',
tooltipColor: '#C9C9C9',
sliderColor: '#C9C9C9',
backgroundGradient: 'high',
borderRadius: '10',
buttonOffColor: 'rgba(90,90,90,1)',
height: 20,
opacity: 1.0";

		if (!$account->isWorker())
			$onstart_hook = ", onStart: function(clip) { $.get('/video/played?fid=' + $('#flow_{$identifier}').attr('data-video-file-id'))}";

		return "<script language=\"JavaScript\">flowplayer(\"flow_{$identifier}\", \"/js/flowplayer.swf\", {'key': '#$9790c83cc945fb375bb', clip: {autoPlay: true, autoBuffering: true$onstart_hook}, plugins: {controls: {url: 'flowplayer.controls.swf'{$controls}}}}).ipad();</script>";
	}

	public static function played($args) {
		global $db, $account;

		if ($account->isWorker()) {
			echo "Error: admin!<br />";
			die;
		}

		$fid = intval($args['fid']);
		if ($fid == 0) {
			echo "Error: wrong fid!<br />";
			die;
		}
		
		$sql = "update place_picture set count_play = count_play + 1 where picture = '{$fid}'";
		$db->q($sql);
		if ($db->affected() != 1) {
			echo "Error: sql error!<br />";
			die;
		}

		echo "OK";
		die;
	}

	/*
	 * time_in_seconds 1 or 2 or 1.5
	 */
	public function grabThumbnail($time) {
		$time = preg_replace('/[^0-9\.]+/', '', $time);	//safety
		$video_path = $this->getVideoAbsPath();
		$sshot_tmp_name = "sstmp_".$this->getFilename()."_{$time}.jpg";
		$sshot_tmp_path = self::getStageDir().$sshot_tmp_name;
		$command = "/usr/bin/ffmpeg -y -itsoffset -{$time} -i '{$video_path}' -vcodec mjpeg -vframes 1 -f rawvideo '$sshot_tmp_path'";
		$line = system($command, $ret);
		if ($ret != 0)
			return false;
		else
			return $sshot_tmp_name;
	}
	
	public function getInfo() {
		$error = "";

		//video
		if (($ret = self::getInfoVideo($this->getVideoAbsPath(), $video_codec, $video_resolution, $audio_codec, $output, $err, $err_det)) === false) {
			if ($err_det == "")
				$err_det = "Ret value = $ret, output = $output";
			$error .= "<span style=\"color:red;font-weight:bold;\" title=\"File={$this->getModule()}/{$this->getFilename()} - {$err_det}\">{$err}</span><br />";
		} else {
			$video_info = "File={$this->getModule()}/{$this->getFilename()} - codec=$video_codec, resolution=$video_resolution";
			$audio_info = "Audio: codec=$audio_codec";
		}

		//thumbnail
		if ($this->getThumb() == "") {
			$error .= "<span style=\"color:red;font-weight:bold;\" title=\"{$video_info} - {$audio_info}\">No thumbnail!</span><br />";
		} else if (($ret = self::getInfoThumbnail($this->getThumbAbsPath(), $thumb_size, $thumb_format, $thumb_resolution, $output, $err, $err_det)) === false) {
			if ($err_det == "")
				$err_det = "Ret value = $ret, output = $output";
			$error .= "<span style=\"color:red;font-weight:bold;\" title=\"Thumbnail={$this->getModule()}/{$this->getThumb()} - {$err_det}\">{$err}</span><br />";
		} else {
				$thumb_info = "Thumbnail={$this->getModule()}/{$this->getThumb()} - size=$thumb_size kB, format=$thumb_format, resolution=$thumb_resolution";
		}

		if (!empty($error))
			return $error;

		$info = "$video_info - $audio_info - $thumb_info";

		if ($video_codec != "h264")
				return "<span style=\"color:red;font-weight:bold;\" title=\"$info\">Incorrect video codec: $video_codec !</span><br />";

		if ($video_resolution != $thumb_resolution) {
				return "<span style=\"color:red;font-weight:bold;\" title=\"$info\">Video and thumbnail resolution dont match !</span><br />";
		}

		if ($audio_codec != "aac")
				return "<span style=\"color:red;font-weight:bold;\" title=\"$info\">Incorrect audio codec: $audio_codec !</span><br />";

		if ($thumb_size > 100) {
				return "<span style=\"color:red;font-weight:bold;\" title=\"$info\"><a href=\"{$this->getThumbUrlPath()}\" target=\"_new\">Thumbnail</a> is too big ($thumb_size kB) !</span><br />";
		}

		return "<span title=\"$info\">$video_resolution</span>";
	}

	public static function getInfoVideo($filepath, &$video_codec, &$video_resolution, &$audio_codec, &$output, &$error, &$error_detail) {
		$error = "";
		$error_detail = "";
		$output = "";

		if (empty($filepath)) {
			$error = "No video!";
			return false;
		}
		if (!file_exists($filepath)) {
			$error = "Video file does not exists!";
			return false;
		}
		$out = array();
		$command = "/usr/bin/ffmpeg -i '{$filepath}' 2>&1 | egrep \"Video:|Audio:\" | cut -d ':' -f 3- | sort";
		//_d("command = '{$command}'");
		exec($command, $out, $ret);
		if (($ret != 0) || (count($out)!=2)) {
			$error_detail = "Return value = $ret, output = ".print_r($out, true);
			$error = "Error getting video info!";
			return false;
		}
		if (preg_match('/^.*Video: ([^, ]*)[, ].* ([0-9]*x[0-9]*).*$/', $out[1], $matches) == 1) {
			$video_codec = $matches[1];
			$video_resolution = $matches[2];
		} else {
			$error_detail = $out[0];
			$error = "Error getting video stream info! ";
		}
		if (preg_match('/^.*Audio: ([^, ]*)[, ].*$/', $out[0], $matches) == 1) {
			$audio_codec = $matches[1];
		} else {
			$error_detail .= $out[1];
			$error .= "Error getting audio stream info!";
		}
		if (!empty($error))
			return false;
		return true;
	}


	public static function getInfoThumbnail($filepath, &$size, &$format, &$resolution, &$output, &$error, &$error_detail) {
		$error = "";
		$error_detail = "";
		$output = "";

		if (empty($filepath)) {
			$error = "No thumbnail!";
			return false;
		}
		if (!file_exists($filepath)) {
			$error = "Thumbnail file does not exists!";
			return false;
		}
		$size = round(filesize($filepath) / 1024);

		//this was not working in some cases, i dont know why... (jay)
/*
		$out = array();
		$command = "identify '{$filepath}'";
		//debug_log("iv: command = $command");
		$str = exec($command, $out, $ret);
		if (($ret != 0) || (count($out)!=1)) {
			$error = "Error getting thumbnail info!";
			$error_detail = "str = '{$str}' Return value = $ret, output = ".print_r($out, true);
			return false;
		}
		$output = $out[0];
		if (preg_match('/^.* ([A-Z]*) ([0-9]*x[0-9]*) .*$/', $output, $matches) == 1) {
			$format = $matches[1];
			$resolution = $matches[2];
			return true;
		}
*/
		if (($img = getimagesize($filepath)) === false) {
			$error = "Error getting thumbnail info!";
			$error_detail = "getimagesize() failed";
			return false;
		}
		$format = $img["mime"];
		$resolution = $img[0]."x".$img[1];
		return true;

		$error = "Error getting image info!";
		return false;
	}

	public function convert() {
		echo "";
	}

	public static function rrmdir($dir) {
		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (filetype($dir . "/" . $object) == "dir") {
						self::rrmdir($dir . "/" . $object); 
					} else {
						unlink($dir . "/" . $object);
					}
				}
			}
			reset($objects);
			rmdir($dir);
		}
	}

}

//END
