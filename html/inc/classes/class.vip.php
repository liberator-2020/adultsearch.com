<?php

class vip {

	var $price_normal = 19.99;
	var $price_recurring = 9.99;

	public function __construct() {
	}

	/**
	 * This is executed from payline class, as soon as purchase is successful
	 */
	public static function purchaseSuccessful($purchase_id, $processor) {
		global $db;

		$system = new system();

		if ($processor == "ccbill") {
			$res = $db->q("SELECT p.account_id, p.total, p.cc_id, pp.p2
						FROM account_purchase p
						INNER JOIN ccbill_post pp on pp.id = p.ccbill_post_id
						WHERE p.id = ?",
						array($purchase_id));
		} else if ($processor == "anet") {
			$res = $db->q("SELECT p.account_id, p.total, p.cc_id, pp.p2
						FROM account_purchase p
						INNER JOIN payment pp on pp.id = p.payment_id
						WHERE p.id = ?",
						array($purchase_id));
		} else {
			reportAdmin("AS: ERR: vip:purchaseSuccessful()", "uknown processor '{$processor}'!", []);
			return false;
		}

		if ($db->numrows($res) != 1) {
			debug_log("Error in vip::purchaseSuccessful(): cant find purchase & post, purchase_id={$purchase_id}");
			reportAdmin("AS: vip::purchaseSuccessful(): Error cant find purchase & post", "", array("purchase_id" => $purchase_id));
			return false;
		}
		$row = $db->r($res);
		$account_id = $row["p2"];
		$payment_id = $row["p3"];
		$total = $row["total"];

		$now = time();

		$acc = account::findOneById($account_id);
		if (!$acc) {
			debug_log("Error in vip::purchaseSuccessful(): cant find account, account_id={$account_id}");
			reportAdmin("AS: vip::purchaseSuccessful(): Error cant find account", "", array("account_id" => $account_id));
			return false;
		}

		//update vip_payment table
		$db->q("UPDATE vip_payment SET paid_stamp = ?, purchase_id = ? WHERE id = ?", array($now, $purchase_id, $payment_id));

		//update account table
		$new_vip_end = ($acc->isVip()) ? $acc->getVipEnd() + 30*86400 : $now + 30*86400;
		$res = $db->q("UPDATE account SET vip = 1, vip_end = ? WHERE account_id = ? LIMIT 1", array($new_vip_end, $account_id));
		if ($db->affected($res) != 1) {
			debug_log("Error in vip::purchaseSuccessful(): update account didnt affect row ?, new_vip_end={$new_vip_end}, account_id={$account_id}");
			reportAdmin("AS: vip::purchaseSuccessful(): update account didnt affect row", "", array("new_vip_end" => $new_vip_end, "account_id" => $account_id));
		}
		//TODO this is not nice here
		$_SESSION["vip"] = 1;
		$_SESSION["vip_end"] = $new_vip_end;


		file_log("vip", "Purchase successful, account_id={$account_id}, total={$total}");
		system::go("/", "Thank You!, You are now VIP member.");
		die;
	}

	public static function purchaseFailed($post_id, $processor = "ccbill") {
		global $db;

		if ($processor == "ccbill") {
			$res = $db->q("SELECT * FROM ccbill_post WHERE id = ?", array($post_id));
		} else if ($processor == "anet") {
			$res = $db->q("SELECT * FROM payment WHERE id = ?", array($post_id));
		} else {
			reportAdmin("AS: ERR: vip:purchaseSuccessful()", "uknown processor '{$processor}'!", []);
			return false;
		}

		if ($db->numrows($res) != 1) {
			debug_log("Error in vip::purchaseFailed(): cant find post, post_id={$post_id}");
			reportAdmin("AS: vip::purchaseFailed(): Error cant find post", "", array("post_id" => $post_id));
			return false;
		}
		$row = $db->r($res);
		$account_id = $row["p2"];
		$payment_id = $row["p3"];

		$post_xml = @new \SimpleXMLElement((string)$row["post"]);

		//try to get amount value from post
		$amount = "";
		if ($post_xml) {
			$amt = intval($post_xml->amount);
			if ($amt)
				$amount = "&amount={$amt}";
		}

		$reason = "";
		if ($row["response"])
			$reason = "&reason=".urlencode($row["response"]);


		//go back to vip add funds page
		system::go("/vip/step2?error=1{$amount}{$reason}");
		die;
	}

}

