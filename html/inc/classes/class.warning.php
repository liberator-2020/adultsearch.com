<?php
/*
 * popup warning
 */
class warning {

	private $a = NULL;

	function __construct() {
	}

	/*
	 * This needs to be called early (before any output), because it sets cookie
	 */
	public static function set() {
		if ($_REQUEST["hidewarning"] == "1")
			self::agreed();
		return;
	}

	/*
	 * This needs to be called early (before any output), because it sets cookie
	 */
	public static function agreed() {
		setcookie('warning', "1", time() + 31536000, '/', '.adultsearch.com', false, true);
		$_SESSION["warning"] = 1;
		return false;
	}

	public static function check() {
		global $db, $smarty, $account, $ctx;

		if ($_REQUEST["showwarning"] == "1" && $_REQUEST["hidewarning"] != "1")
			return true;

		//2019-08-02 Zach: disable this
		return false;

		//bots don't get warning
		if (is_bot())
			return false;

		//popups from other websites don't get warning
		if (stripos($_REQUEST["utm_medium"], "pop") !== false 
			|| stripos($_REQUEST["utm_medium"], "link") !== false
			|| stripos($_REQUEST["utm_medium"], "banner") !== false
			|| stripos($_REQUEST["utm_campaign"], "pop") !== false
			|| stripos($_REQUEST["utm_campaign"], "link") !== false
			|| stripos($_REQUEST["utm_campaign"], "banner") !== false
			)
			return false;

		//only these pages get warning: homepage, city page, escort list, escort look, place list, place look
		// !!!
		// !!! BE CAREFUL NOT TO REMOVE THIS CHECK AND DISPLAY SITE-WIDE AS THAT WOULD DISABLE ADVERTISING SYSTEM, AND OTHER STUFF !!!
		// !!!
		if (!in_array($ctx->page_type, ["landing", "city", "erotic_services_index", "erotic_services_look", "list", "item"]))
			return false;

		if ($_COOKIE["warning"] == "1")
			return false;

		if ($_SESSION["warning"] == "1")
			return false;

//		if ($account->getId() == 3974)
//			return true;

//		return false;
		return true;
	}

	public static function checkAndDisplay() {
		global $db, $smarty, $account;

		if (!self::check()) {
			$smarty->assign("warning", false);
			return false;
		}

		//agree link so it works also without javascript 
		$agree_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if (strlen($_SERVER["QUERY_STRING"]) > 0)
			$agree_link .= "&hidewarning=1";
		else
			$agree_link .= "?hidewarning=1";
		$smarty->assign("agree_link", $agree_link);

		$smarty->assign("warning", true);
		return true;
	}

}

