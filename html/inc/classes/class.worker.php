<?php

class worker {

	public function __construct() {
	}
	
	public static function isWorkItem($module, $id) {
		global $db, $account;

		$res = $db->q("SELECT i.* FROM work_item i WHERE i.module = ? AND i.module_id = ?", [$module, $id]);
		if ($db->numrows($res) == 0)
			return false;

		$row = $db->r($res);
		return $row["id"];
	}

	public static function getReassignName($wi_id) {
		global $db, $account;

		$res = $db->q("SELECT i.* FROM work_item i WHERE i.id = ?", [$wi_id]);
		if ($db->numrows($res) == 0)
			return false;

		//TODO this is hardcoded
		$row = $db->r($res);
		$account_id = $row["account_id"];
		if ($account_id == 226368) {
			return "reassign_30220";
		} else if ($account_id == 30220) {
			return "reassign_226368";
		}

		return "";
	}
	public static function getReassignLabel($wi_id) {
		global $db, $account;
		
		$res = $db->q("SELECT i.* FROM work_item i WHERE i.id = ?", [$wi_id]);
		if ($db->numrows($res) == 0)
			return false;

		//TODO this is hardcoded
		$row = $db->r($res);
		$account_id = $row["account_id"];
		if ($account_id == 226368) {
			return "Do Not Save and Reassign to Maarten";
		} else if ($account_id == 30220) {
			return "Do Not Save and Reassign to Rachel";
		}

		return "";
	}

	public static function getNextItemEditUrl($wi_id) {
		global $db, $account;
		
		$res = $db->q("SELECT i.* FROM work_item i WHERE i.id = ?", [$wi_id]);
		if ($db->numrows($res) == 0)
			return false;
		
		$row = $db->r($res);
		$account_id = $row["account_id"];
		$orden = $row["orden"];

		$res = $db->q("SELECT i.* FROM work_item i WHERE i.account_id = ? AND i.orden > ? LIMIT 1", [$account_id, $orden]);
		if ($db->numrows($res) == 0)
			return false;

		$row = $db->r($res);
		//TODO unify with /mng/work_items code
		$edit_url = false;
		switch ($row["module"]) {
			case "eroticmassage": $edit_url = "/worker/emp?id={$row["module_id"]}"; break;
			default: break;
		}

		return $edit_url;
	}

	public static function reassignMaarten($wi_id) {
		global $db, $account;
		
		$res = $db->q("UPDATE work_item SET account_id = ? WHERE id = ? LIMIT 1", [30220, $wi_id]);
		if ($db->affected($res) == 1) {
			flash::add(flash::MSG_SUCCESS, "Work item #{$wi_id} reassigned to Maarten.");
			return true;
		}
		return false;
	}

	public static function reassignRachel($wi_id) {
		global $db, $account;
		
		$res = $db->q("UPDATE work_item SET account_id = ? WHERE id = ? LIMIT 1", [226368, $wi_id]);
		if ($db->affected($res) == 1) {
			flash::add(flash::MSG_SUCCESS, "Work item #{$wi_id} reassigned to Rachel.");
			return true;
		}
		return false;
	}

	public static function markCompleted($wi_id) {
		global $db, $account;
		
		$res = $db->q("UPDATE work_item SET completed = 1, completed_stamp = ? WHERE id = ? LIMIT 1", [time(), $wi_id]);
		if ($db->affected($res) == 1) {
			flash::add(flash::MSG_SUCCESS, "Work item #{$wi_id} marked as completed.");
			return true;
		}
		return false;
	}

}

//END
