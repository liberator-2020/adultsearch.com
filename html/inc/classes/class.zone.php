<?php
/*
 * zone = advertise zone (advertise section)
 */
class zone {

	private $id = NULL,
			$type = NULL,
			$account_id = NULL,
			$status = NULL,
			$name = NULL,
			$nickname = NULL;

	private $account = NULL;

	function __construct() {
	}

	public static function findOneById($id) {
		global $db;

		$res = $db->q("SELECT * FROM advertise_section WHERE id = ?", array(intval($id)));
		if (!$db->numrows($res))
			return NULL;
		$row = $db->r($res);

		return self::withRow($row);
	}
	
	public static function withRow($row) {

		$zone = new zone();
		$zone->setId($row["id"]);
		$zone->setType($row["type"]);
		$zone->setAccountId($row["account_id"]);
		$zone->setStatus($row["status"]);
		$zone->setName($row["name"]);
		$zone->setNickname($row["nickname"]);

		return $zone;
	}
	
	//getters and setters
	public function getId() {
		return $this->id;
	}
	public function getType() {
		return $this->type;
	}
	public function getAccountId() {
		return $this->account_id;
	}
	public function getStatus() {
		return $this->status;
	}
	public function getName() {
		return $this->name;
	}
	public function getNickname() {
		return $this->nickname;
	}

	public function setId($id) {
		$this->id = $id;
	}
	public function setType($type) {
		$this->type = $type;
	}
	public function setAccountId($account_id) {
		$this->account_id = $account_id;
	}
	public function setStatus($status) {
		$this->status = $status;
	}
	public function setName($name) {
		$this->name = $name;
	}
	public function setNickname($nickname) {
		$this->nickname = $nickname;
	}

	//relationships
	public function getAccount() {
		if ($this->account)
			return $this->account;
		$acc = account::findOneById($this->account_id);
		if (!$acc)
			return NULL;
		$this->account = $acc;
		return $this->account;
	}	


	//CRUD functions
	function update($audit = false) {
		global $account, $db;

		if (!$this->id)
			return false;

		$original = zone::findOneById($this->id);
		if (!$original)
			return false;

		$update_type = false;
		$update_fields = array();

		if ($this->type != $original->getType())
			$update_fields["type"] = $this->type;
		$update_account = false;
        if ($this->account_id != $original->getAccountId()) {
            $update_fields["account_id"] = $this->account_id;
            $update_account = true;
        }
		$update_status = false;
		if ($this->status != $original->getStatus()) {
			$update_fields["status"] = $this->status;
			$update_status = true;
		}

		if ($this->name != $original->getName())
			$update_fields["name"] = $this->name;
		if ($this->nickname != $original->getNickname())
			$update_fields["nickname"] = $this->nickname;

		if (empty($update_fields))
			return true;	//nothing to update

		$changed_fields = "";
		$update = "";
		$update_arr = array();
		foreach ($update_fields as $key => $val) {
			$changed_fields .= (empty($changed_fields)) ? "" : ", ";
			$update .= (empty($update)) ? "SET " : ", ";
			$changed_fields .= "{$key}";
			if (method_exists($original, get_getter_name($key)))
				$changed_fields .= ": ".call_user_func(array($original, get_getter_name($key)))."->".call_user_func(array($this, get_getter_name($key)));
			$update .= "`{$key}` = ?";
			$update_arr[] = $val;
		}
		$update_arr[] = $this->id;
		$update = "UPDATE advertise_section {$update} WHERE id = ? LIMIT 1";

		$db->q($update, $update_arr);
		$aff = $db->affected();

		if (!$aff)
			return false;

		if ($audit)
			audit::log("AZO", "Edit", $this->id, "Changed fields: {$changed_fields}", $account->getId());

		return true;
	}

	//additional functions

	//this function changes done to 1 for classified
/*
	public function makeLive() {
		global $db, $account;

		$res = $db->q("UPDATE advertise_section SET done = 1 WHERE id = ? LIMIT 1", array($this->id));
		$aff = $db->affected($res);

		$res2 = $db->q("UPDATE advertise_section_loc SET done = 1 WHERE post_id = ?", array($this->id));
		$aff2 = $db->affected($res2);

		$res3 = $db->q("UPDATE advertise_section_sponsor SET done = 1 WHERE post_id = ?", array($this->id));
		$res3 = $db->q("UPDATE advertise_section_side SET done = 1 WHERE post_id = ?", array($this->id));

		debug_log("zone:makeLive(): id={$this->id}, who={$account->getId()}");
		audit::log("CLA", "MakeLive", $this->id, "", $account->getId());

		advertise_section::rotateIndex();

		if ($aff == 1 && $aff2 > 0)
			return true;

		return false;
	}
*/
	
}

