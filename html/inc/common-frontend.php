<?php
/*
 * common-frontend.php
 *
 * Common functions for webpage frontend
 *
 */

/**
 * Title of Page (titlebar in browser window), can be set from anywhere in CMS
 * @global string $GLOBALS['gTitle'] 
 */ 
$GLOBALS['gTitle'] = '';
/**
 * Contents of meta-description HTML HEAD tag, can be set from anywhere in CMS
 * @global string $GLOBALS['gDescription']
 */ 
$GLOBALS['gDescription'] = '';
/**
 * Contents of meta-keywords HTML HEAD tag, can be set from anywhere in CMS
 * @global string $GLOBALS['gKeywords']
 */ 
$GLOBALS['gKeywords'] = '';
/**
 * HTML code, that will be added inside HTML HEAD tags. Useful for adding <script> or <style> tags to header. Only append code to this code, don't overwrite contents !
 * @global string $GLOBALS['gHtmlHeadInclude']
 */ 
$GLOBALS['gHtmlHeadInclude'] = '';
/**
 * Main configuration array where all configuration options are stored. Do not access directly, but use function GetConfig, SaveConfig !
 * @global array $GLOBALS['config']
 */ 
$GLOBALS['config'] = array();
/**
 * Main smarty template filled from index.php
 * @global string $GLOBALS['gIndexTemplate'] 
 */ 
$GLOBALS['gIndexTemplate'] = '';


// -------------------------
// -------------------------
// -------------------------


// killer function. each individual php file be able to use its own css or js file included properly.
function include_html_head($type, $file, $iehack = NULL) {
	global $gHtmlHeadInclude;
	$line = "";

	if( $type == 'css' ) {
		if( $file == "/css/info.css" ) $file = "{$config_site_url}/css/info.css?";
		$line .= "<link href=\"{$file}\" rel=\"stylesheet\" type=\"text/css\" />\n";
		// ie6 and lower could not see this css, not sure about 7 yet, mostly times 7 is good for definitions
		if( $iehack ) {
			$line .= "<style type=\"text/css\">@import url(\"{$iehack}\") all;</style>\n";
		}
	} else if( $type == 'js' )
		$line .= "<script type=\"text/javascript\" src=\"{$file}\"></script>\n";
		
	//check if this file was not already included, prevents double includes
	if (strpos($gHtmlHeadInclude, $line) === false)
		$gHtmlHeadInclude .= $line;
}

function name2url($name) {
	$name = preg_replace('/[^a-z0-9\-\s]/', '', strtolower($name));
	$name = preg_replace('/\s+/', '-', $name);
	return $name;
}

function limit_text($text, $limit) {
	if (str_word_count($text) > $limit) {
		$words = str_word_count($text, 2);
		$pos = array_keys($words);
		$text = substr($text, 0, $pos[$limit]);
	}
	return $text;
}

function display404() {
	global $ctx;
	header("HTTP/1.0 404 Not Found");
	$gTitle = "404 Not Found";
	echo "<h1>404 Not Found</h1>";
	echo "<h2>The page cannot be found.</h2>";
	echo "<p>It seems the page you were trying to find on our site isn't around anymore. <a href=\"{$ctx->domain_link}\">Back to homepage</a>.</p>";
}

?>
