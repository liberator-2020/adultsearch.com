<?php
/**
 * common.php
 *
 * Common functions for both frontend and backend of CMS
 *
 */

global $config_site_abspath, $config_site_url, $site_domain;

/**
 * _CMS_ABS_PATH constant contains absolute server path to web-root of CMS (usually '/var/www/html')
 */
define('_CMS_ABS_PATH', $config_site_abspath);

//create adultsearch.com from http://adultsearch.com
$pos = strpos($config_site_url, "//");
if ($pos === false)
	$pos = 0;
else
	$pos+=2;
$site_domain = substr($config_site_url, $pos);

// -----------------------
// set up class autoloader
// -----------------------
function classLoader($class) {
	$class_file = _CMS_ABS_PATH."/inc/classes/class.".strtolower($class).".php";
	if (!file_exists($class_file))
		return false;
	include_once($class_file);
}
spl_autoload_register(null, false);			 //nullify any existing autoloads
spl_autoload_extensions('.php');				//specify extensions that may be loaded
spl_autoload_register('classLoader');   //register loader function

require_once(_CMS_ABS_PATH."/inc/classes/Map/MapQuestMapGenerator.php");

//composer 3rd party libraries autoloader
require_once(_CMS_ABS_PATH."/../vendor/autoload.php");

// ----------------------
// --- Error handling ---
// ----------------------
define('_CMS_PHP_ERROR_LOG_FILE', _CMS_ABS_PATH."/../data/log/php_error_log");	//php error log file
define('_CMS_PHP_DEBUG_LOG_FILE', _CMS_ABS_PATH."/../data/log/php_debug_log");	//php debug log file
ini_set('log_errors', true);
ini_set('error_log', _CMS_PHP_ERROR_LOG_FILE);
error_reporting(E_ALL & ~E_NOTICE);

//get error log mode, default is "production" mode (don't display errors)
global $config_dev_server;
if ($config_dev_server) {
	ini_set('display_errors', true);
} else {
	ini_set('display_errors', false);
}

function CmsErrorHandler($errno, $errstr, $errfile, $errline) {
	//initialize variables
	$stdlog = true;
	$display = false;
	$halt_script = false;

	//error-dependent processing
	switch($errno) {
		case E_USER_NOTICE:
		case E_NOTICE:
		case E_STRICT:
		case E_DEPRECATED:
		case E_USER_DEPRECATED:
			$type = "Notice";
			$stdlog = false;	//don't log notices
			$display = false;
			break;
		  
		case E_USER_WARNING:
		case E_COMPILE_WARNING:
		case E_CORE_WARNING:
		case E_WARNING:
			$type = "Warning";
			$stdlog = false;	//don't log warnings
			break;
		  
		case E_USER_ERROR:
		case E_COMPILE_ERROR:
		case E_CORE_ERROR:
		case E_RECOVERABLE_ERROR:
		case E_ERROR:
			$type = "Error";
			break;
	   
		case E_PARSE:
			//we should never get here :)
			$type = "Parse Error";
			break;
	   
		default:
			$type = "Unknown Error";
		break;
	}

	$errstr = replaceIpInStr($errstr);

	//create error message
	$error_msg = "{$errstr} occured in {$errfile} on line {$errline}";
	$display_msg = "[".date("D j-M-Y G:i:s T")."] <strong>{$type}</strong> ".$error_msg."<br />\n";
	$log_msg = $type." ".$error_msg;

	//notification and termination
	if ($display)
		echo $display_msg;
	if ($stdlog) {
		//error_log($log_msg, 0);
		error_log("[".date("d-m-Y G:i:s T")."] [".account::getUserIp()."] [{$errfile}:{$errline}] {$type}: {$error_msg}\n", 3, _CMS_PHP_ERROR_LOG_FILE);
	}
	
	if ($halt_script)
		exit -1;

	return true;	//Don't execute PHP internal error handle
}

$old_error_handler = set_error_handler("CmsErrorHandler");

function debug_log($str, $only_admin = false) {
	global $account;
	if ($only_admin && !$account->isrealadmin())
		return;
	error_log(date("[d-m-Y G:i:s T]")." [".account::getUserIp()."] ".replaceIpInStr($str)."\n", 3, _CMS_PHP_DEBUG_LOG_FILE);
}

//common function for file logging
function file_log($log_file, $str) {
	if (in_array($log_file, [
		"account", "advertise", "anet", "api", "api_test", 
		"bp_promo", "budget", "businessowner", 
		"classifieds", 
		"dir", 
		"feed", 
		"import_ts_profiles_from_json",
		"login", 
		"mapquest", 
		"notification", 
		"pay", "payline", "payment", "phone_carrier_verification", "phone_verification", "profile_importer", "proxy_connection", 
		"recurring", 
		"save_images", "securionpay", "sms", "spool", "stripe", 
		"vid", "vip", "vpn", 
		"waiting_list",
		])) {
		$file = _CMS_ABS_PATH."/../data/log/{$log_file}.log";
		error_log(date("[d-m-Y G:i:s T]")." [".account::getUserIp()."] ".replaceIpInStr($str)."\n", 3, $file);
		return true;
	}
	return false;
}

global $db, $account, $smarty, $ctx, $url, $mcache, $sphinx, $mobile, $advertise, $twig;

//MOBILE FLAG
if ($_GET['mobile'] == 2 || (array_key_exists("mobile", $_SESSION) && $_SESSION["mobile"] == 2 && !isset($_GET['mobile']))) {
	$mobile = $_SESSION['mobile'] = 2;
} else {
	if ((array_key_exists("mobile", $_SESSION) && $_SESSION['mobile'] == 1) || $_GET['mobile'] == 1) {
		$mobile = $_SESSION['mobile'] = 1;
	} else {
		//2019-08-21 mobile_detect replaced with browscap, because it was not detecting Samsung Galaxy Tab S5 and S4 as tablets
		/*
		//TODO move to composer.json
		require_once(_CMS_ABS_PATH.'/inc/classes/mobile_detect/Mobile_Detect.php');
		$detect = new Mobile_Detect;
		if ($detect->isMobile() && !$detect->isTablet()) {
			$mobile = $_SESSION['mobile'] = 1;
		}
		*/
		try {
 			$cacheDir = _CMS_ABS_PATH."/../vendor/browscap/browscap-php/resources";
			$fileCache = new \Doctrine\Common\Cache\FilesystemCache($cacheDir);
			$cache = new \Roave\DoctrineSimpleCache\SimpleCacheAdapter($fileCache);
			$logger = new \Monolog\Logger('name');
			$bc = new \BrowscapPHP\Browscap($cache, $logger);
			$browser = $bc->getBrowser();
			if (($browser->ismobiledevice == 1) && ($browser->istablet != 1)) {
				$mobile = $_SESSION['mobile'] = 1;
			}
		} catch (\Exception $e) {
			debug_log("Browscap not working: {$e}\n");
		}
	}
}

//anti-scrape
if (array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
	if (substr($_SERVER['HTTP_USER_AGENT'], 0, 6) == "Scrapy")
		die("");
}

$db = new db('utf8mb4');
$smarty = GetSmartyInstance();
$account = account::init($db);
$advertise = new advertise;
$ctx = new context;
$url = new url;
$mcache = new mcache($db);
$sphinx = new sphinx();


//Twig templates initialization (https://twig.symfony.com/doc/2.x/api.html)
$twig_loader = new Twig_Loader_Filesystem(_CMS_ABS_PATH."/templates/");
$twig_params = ["cache" => _CMS_ABS_PATH."/tmp/twig_cache"];
if ($config_dev_server)
	$twig_params["debug"] = true;
$twig = new Twig_Environment($twig_loader, $twig_params);


if (strpos($_SERVER["REQUEST_URI"], "/advertise") === 0)
	advertise::init();


// -------------------
// enable time measure
// -------------------
// (this code was moved from index.php)
$startarray = explode(" ", microtime());
$_starttime = $startarray[1] + $startarray[0];
function get_page_load_time() {
	global $_starttime;
	$endarray = explode(" ", microtime());
	$endtime = $endarray[1] + $endarray[0];
	$totaltime = $endtime - $_starttime;
	return round($totaltime,5);
}


// ------------------------------
// --- URL HANDLING FUNCTIONS ---
// ------------------------------

//common function for GetRequestParam, GetGetParam, GetPostParam, GetCookieParam
//don't use for another arrays, because magic_quotes_gpc is valid only for $_GET, $_POST and $_COOKIE
function _GetArrayParam($array, $name) {
	if (isset($array[$name])) {
		if (!get_magic_quotes_gpc()){
			$return=$array[$name];
			if(is_array($return)){	
				$tempArray=array();
				foreach($return as $arrayValue){
					$tempArray[]=addslashes($arrayValue);
				}
				return $tempArray;
			}
			else 
				return addslashes($array[$name]);
		}
		else
			return $array[$name];
	}
	return null;
}

//the same but with whole array
function _GetArrayParams($array) {
	return addslashes_recursive($array);
}

/**
 * Method GetRequestParam() returns slashed $_REQUEST parameter, so we can use it in SQL statements
 *
 * @access public
 * @param string $name name of $_REQUEST parameter
 * @return string 
 */
function GetRequestParam($name) {
	return _GetArrayParam($_REQUEST, $name);
}
/**
 * Method GetRequestParamStripped() returns unslashed $_REQUEST parameter, so we can use it (after using htmlspecialchars() function on it) in webpage
 *
 * @access public
 * @param string $name name of $_REQUEST parameter
 * @return string 
 */
function GetRequestParamStripped($name) {
	return stripslashes(GetRequestParam($name));
}

/**
 * Method GetRequestParams() returns slashed whole array of $_REQUEST parameters (GET and POST)
 * 
 * Enter description here ...
 * @return array
 */
function GetRequestParams() {
		return _GetArrayParams($_REQUEST);
}

/**
 * Method GetGetParam() returns slashed $_GET parameter, so we can use it in SQL statements
 *
 * @access public
 * @param string $name name of $_GET parameter
 * @return string 
 */
function GetGetParam($name) {
	return _GetArrayParam($_GET, $name);
}
// return unslashed $_GET parameter
/**
 * Method GetGetParamStripped() returns unslashed $_GET parameter, so we can use it (after using htmlspecialchars() function on it) in webpage
 *
 * @access public
 * @param string $name name of $_GET parameter
 * @return string 
 */
function GetGetParamStripped($name) {
	return stripslashes(GetGetParam($name));
}

/**
 * Method GetPostParam() returns slashed $_POST parameter, so we can use it in SQL statements
 *
 * @access public
 * @param string $name name of $_POST parameter
 * @return string 
 */
function GetPostParam($name) {
	return _GetArrayParam($_POST, $name);
}
/**
 * Method GetPostParamStripped() returns unslashed $_POST parameter, so we can use it (after using htmlspecialchars() function on it) in webpage
 *
 * @access public
 * @param string $name name of $_POST parameter
 * @return string 
 */
function GetPostParamStripped($name) {
	return stripslashes(GetPostParam($name));
}

/**
 * Method GetCookieParam() returns slashed $_COOKIE parameter, so we can use it in SQL statements
 *
 * @access public
 * @param string $name name of $_COOKIE parameter
 * @return string 
 */
function GetCookieParam($name) {
	return _GetArrayParam($_COOKIE, $name);
}



// -----------------------------
// String manipulation functions
// -----------------------------


/**
* Function adds slashes for everything inside:
*	   strings
*   keys and values in array
*   even nested arrays...
* @access public
*/
function addslashes_recursive($parameter) {
	if (!is_array($parameter))
		return addslashes($parameter);
	$ret = array();
	foreach ($parameter as $key => $val) {
		$ret[addslashes_recursive($key)] = addslashes_recursive($val);
	}
	return $ret;
}

//writes debug info
function _d($debug_message, $die = false) {
	global $account, $config_dev_server;
	if ($config_dev_server||$account->isrealadmin()) 
		echo "Debug: {$debug_message}<br />\n";
	if ($die)
		die;
}

//writes debug array
function _darr($array) {
	global $account, $config_dev_server;
	if ($config_dev_server||$account->isrealadmin()) {
		echo "<pre style=\"text-align: left;\">\n";
		print_r($array);
		echo "</pre>\n";
	}
}



// ----------------------------------------------------------
// returns latitude, longitude, city and zip of given address
// ----------------------------------------------------------
function get_coord($Address,$City,$State,$Zip) {
	global $config_mapquest_key, $config_dev_server;

	$Address = urlencode($Address);
	$City = urlencode($City);
	$State = urlencode($State);
	$strZip = urlencode($Zip);

	// Mapquest geocoding API: initiate cURL w/ protocol & URL of remote host
	$url = "http://www.mapquestapi.com/geocoding/v1/address?key={$config_mapquest_key}&street={$Address}&city={$City}&state={$State}&postalCode={$strZip}&maxResults=5";
	//file_log("mapquest", "get_coord: url='{$_SERVER["REQUEST_URI"]}', street={$Address}&city={$City}&state={$State}&postalCode={$strZip}");

	$ch = curl_init($url);

	curl_setopt($ch,CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.6.3");
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);

	if ($config_dev_server) {
		debug_log("common:get_coord: Mapquest geocoding API request : ".$url);
	}

	$response = curl_exec($ch);
	$info     = curl_getinfo($ch);
	curl_close($ch);

	if (curl_errno($ch)) {
		$error_msg = curl_errno($ch)."#".curl_error($ch);
		debug_log("common:get_coord: Mapquest geocoding API error CURL : {$error_msg}");
		return array("error","Mapquest geocoding API error CURL : {$error_msg}","",$response);
	}

	if ($info['http_code'] !== 200) {
		$error_msg = $info['http_code']."#".$response;
		debug_log("common:get_coord: Mapquest geocoding API error: {$error_msg}");
		return array("error","Mapquest geocoding API error: {$error_msg}","",$response);
	}

	$resp = json_decode($response, true);

	if ($resp === null && json_last_error() !== JSON_ERROR_NONE) {
		$code    = json_last_error();
		$message = json_last_error_msg();
		debug_log("common:get_coord: Mapquest invalid json response: {$response}; code: {$code}; message: {$message}");
		return array("error","Mapquest geocoding API error","",$response);
	}

	if (!is_array($resp)) {
		debug_log("common:get_coord: Mapquest invalid response : ".$response);
		return array("error","Mapquest geocoding API error","",$response);
	}

	if ($resp["info"]["statuscode"] != 0) {
		//return error
		debug_log("common:get_coord: Mapquest geocoding API error: ".$resp["info"]["messages"]);
		return array("error","Mapquest geocoding API error: ".$resp["info"]["messages"],"",$response);
	}

	if (count($resp["results"]) == 0) {
		//return error
		return array("error","No result from Mapquest geocoding API","",$response);
	}

	if (count($resp["results"][0]["locations"]) == 1) {
		//exactly one result
		if (!array_key_exists("locations", $resp["results"][0])) {
			//this should not happen
			return array("error","No location in result from Mapquest geocoding API","",$response);
		}

		$lat = $resp["results"][0]["locations"][0]["latLng"]["lat"];
		$lon = $resp["results"][0]["locations"][0]["latLng"]["lng"];
		$zipcode = $resp["results"][0]["locations"][0]["postalCode"];
		$cityname = $resp["results"][0]["locations"][0]["adminArea5"];

		//TODO redo this function - do we need to return city name ? do we use it somewhere ?
		return array($lat,$lon,$cityname,$zipcode);
	}

	if (!$Zip) {
		//if we dont have ZIP in our DB, we can't decide from multiple results, return error
		debug_log("common:get_coord: Multiple results from Mapquest geocoding API, but no zip specified in DB, cant decide : ".$response);
		return array("error","Multiple results from Mapquest geocoding API, but no zip specified in DB, cant decide","",$response);
	}

	if (count($resp["results"][0]["locations"]) > 1) {
		//multiple results, lets check if there is exactly one with correct ZIP

		$return_array = NULL;
		foreach ($resp["results"][0]["locations"] as $result) {

			if ($result["postalCode"] != $Zip)
				continue;

			if ($return_array) {
				//this is already second result with zipcde we are interested in, return too many results error
				debug_log("common:get_coord: Too many results from Mapquest geocoding API: ".count($resp["results"][0]["locations"]).$response);
				return array("error","Too many results from Mapquest geocoding API: ".count($resp["results"][0]["locations"]),"",$response);
			}

			if (!array_key_exists("latLng", $result)) {
				//this should not happen
				debug_log("common:get_coord: Mapquest geocoding API error: No location in result from Mapquest geocoding API ".$response);
				return array("error","No location in result from Mapquest geocoding API","",$response);
			}

			$lat      = $result["latLng"]["lat"];
			$lon      = $result["latLng"]["lng"];
			$zipcode  = $result["postalCode"];
			$cityname = $result["adminArea5"];

			$return_array = array($lat,$lon,$cityname,$zipcode);
		}

		if ($return_array) {
			//TODO redo this function - do we need to return city name ? do we use it somewhere ?
			return $return_array;
		}

		//return no valid result error, perhaps zipcode is incorrrect in our DB ?
		debug_log("common:get_coord: No valid result from Mapquest geocoding API, perhaps zip is incorrect in our DB ? ".$response);
		return array("error","No valid result from Mapquest geocoding API, perhaps zip is incorrect in our DB ?","",$response);
	}
   
	//return error
	debug_log("common:get_coord: Mapquest geocoding API error: ".$response);
	return array("error","Mapquest geocoding API error","",$response);
}

function distance($lat1, $lon1, $lat2, $lon2, $unit) {
	$theta = $lon1 - $lon2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;
	$unit = strtoupper($unit);

	if ($unit == "K") {
		return ($miles * 1.609344);
	} else if ($unit == "N") {
		return ($miles * 0.8684);
	} else {
		return $miles;
	}
}


// parses php(html) file and returns output in string
// used in templates
function parse_php_file($filename) {
	//_d("parsed php filename=".$filename);
	//file existency check
	if (!file_exists($filename)) {
		_d("File for include '{$filename}' doesn't exists !");
		return false;
	}
	//read contents into string
	$str = file_get_contents($filename);
	// turn on output buffering
	ob_start();
	// this parses the php code in out $str variable
	//_d("STR='{$str}'");

require $filename;
	
	// store the final parsed output in a variable $output
	$output = ob_get_contents();
	// clear buffer and stop buffering
	ob_end_clean();

	//return output
	return $output;
}


//sort array by certain key in inner arrays
function usort_cmp_key($a, $b) {
	global $usort_cmp_index;
	$aval = $a[$usort_cmp_index];
	$bval = $b[$usort_cmp_index];

	//string values
	if (is_string($aval) || is_string($bval))
		return strcmp($aval, $bval);

	//other (number) values
	if ($aval == $bval)
		return 0;
	return ($aval > $bval) ? +1 : -1;
}

//sort array greater than smaller 3, 2, 1, 0...
function usort2_cmp_key($a, $b) {
	global $usort_cmp_index;
	$aval = $a[$usort_cmp_index];
	$bval = $b[$usort_cmp_index];

	//string values
	if (is_string($aval) || is_string($bval))
		return strcmp($aval, $bval);

	//other (number) values
	if ($aval == $bval)
		return 0;
	return ($aval < $bval) ? +1 : -1;
}


// ------------------------
// --- Smarty shortcuts ---
// ------------------------
/**
 * Function GetSmartyInstance() creates new smarty object instance with pre-defined settings.
 *
 * @access public
 * @param string $template_dir Path to directory, where to look for templates later when display() method is called on smarty object. By default it will try to determine directory automatically - default path is _CMS_ABS_PATH."/templates/".<current_module_code>
 * @return object Newly created instance of Smarty class.
 */
function GetSmartyInstance() {
	global $config_site_url, $site_domain, $gCanonicalUrl, $mobile, $ctx, $account, $config_dev_server, $config_env, 
			$config_image_server,  $config_mapquest_key, $config_direct_call, $config_recaptcha_site_key;

	require_once(_CMS_ABS_PATH.'/inc/classes/Smarty/SmartyBC.class.php');
	$smarty_instance = new SmartyBC();

	//default template directory
	$smarty_instance->template_dir  = _CMS_ABS_PATH."/templates";
	$smarty_instance->compile_dir   = _CMS_ABS_PATH.'/tmp/smarty/templates_c';
	$smarty_instance->cache_dir	 = _CMS_ABS_PATH.'/tmp/smarty/cache';
	$smarty_instance->config_dir	= _CMS_ABS_PATH.'/inc/classes/Smarty/configs';

	if (isset($_REQUEST["_d"]))
		$smarty_instance->debugging = true;

	//some common global variables

	if (array_key_exists('HTTP_HOST', $_SERVER)) {
		$gCanonicalUrl = "https://{$_SERVER["HTTP_HOST"]}";
		$urlPath = rtrim(strtok($_SERVER["REQUEST_URI"], "?"), "/");
		$gCanonicalUrl .= $urlPath;
	}

	$current_url = "";
	if (array_key_exists("HTTP_HOST", $_SERVER) && array_key_exists("REQUEST_URI", $_SERVER))
		$current_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER["HTTP_HOST"]}{$_SERVER["REQUEST_URI"]}";
	$smarty_instance->assign("current_url", $current_url);
	$smarty_instance->assign("site_domain", $site_domain);
	$smarty_instance->assign("https", (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']!='off') ? true : false);
	if (array_key_exists('REQUEST_URI', $_SERVER))
		$smarty_instance->assign("request_uri", urlencode($_SERVER["REQUEST_URI"]));

	$iphone = false;
	$ipad = false;
	if (array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
		if (strpos($_SERVER['HTTP_USER_AGENT'],"iPhone"))
			$iphone = true;
		if (strpos($_SERVER['HTTP_USER_AGENT'],"iPad"))
			$ipad = true;
	}
	$smarty_instance->assign("iphone", $iphone);
	$smarty_instance->assign("ipad", $ipad);

	$smarty_instance->assign("current_timestamp", time());	//was used in index.tpl
	$smarty_instance->assign("csm4", date("s")%4);	//current second in minute modulo by 4		//use in mobile_index.tpl
	$smarty_instance->assign("rand3", rand(1,3));	//random number 1-3							//use in mobile_index.tpl

	if (isset($_SESSION) && array_key_exists("mobile", $_SESSION) && $_SESSION["mobile"] == 1) {
		$smarty_instance->assign("mobile", 1);
	}

	if (isset($_SESSION)) {
		if (!array_key_exists("pages_visited", $_SESSION)) {
			$_SESSION["pages_visited"] = 0;
		} else {
			$_SESSION["pages_visited"] = intval($_SESSION["pages_visited"]) + 1;
		}
		$smarty_instance->assign("pages_visited", $_SESSION["pages_visited"]);
		//if ($_SESSION["pages_visited"] < 3) {
		//	$smarty_instance->assign("exit_pop", 1);
		//}
	}

	if (array_key_exists("nobanner", $_REQUEST) && $_REQUEST["nobanner"] == "1")
		$smarty_instance->assign("nobanner", true);

	$smarty_instance->assign("page_type", $ctx->page_type);

	$smarty_instance->assign("config_env", $config_env);
	$smarty_instance->assign("config_site_url", $config_site_url);
	$smarty_instance->assign('config_image_server', $config_image_server);
	$smarty_instance->assign("config_dev_server", $config_dev_server);
	//$smarty_instance->assign("config_mapquest_key", $config_mapquest_key);
	$smarty_instance->assign('config_direct_call', $config_direct_call);
	$smarty_instance->assign("config_recaptcha_site_key", $config_recaptcha_site_key);

	return $smarty_instance;
}



function sendEmail($notification_from_email, $subject, $mail_content, $to, $reply = NULL) {
	$params = array(
		"from" => $notification_from_email,
		"to" => $to,
		"subject" => $subject,
		"html" => $mail_content,
		"text" => $mail_content
	);
	if ($reply)
		$params["reply_to"] = $reply;
	$error = "";
	return send_email($params, $error);
}

/**
 * This method logs outgoing email into CSV file
 * This logging is used on DEV environment (instead sending email out via STMP,...)
 */
function email_log($from, $to, $subject, $text, $html) {

	$text = preg_replace('/"/', '""', preg_replace('/\n/', '<EOL>', preg_replace('/\r/', '', $text)));
	$html = preg_replace('/"/', '""', preg_replace('/\n/', '<EOL>', preg_replace('/\r/', '', $html)));

	$line = time().",\"{$from}\",\"{$to}\",\"{$subject}\",\"{$text}\",\"{$html}\"\n";

	$email_log_filepath = _CMS_ABS_PATH."/../data/log/email.log";
	
	$new_content = $line;
	$handle = @fopen($email_log_filepath, "r");
	if ($handle) {
		$i = 0;
		while (($buffer = fgets($handle, 4096)) !== false) {
			$new_content .= $buffer."\n";
			$i++;
			if ($i > 100)
				break;
		}
		fclose($handle);
	}
	
	file_put_contents($email_log_filepath, $new_content);
}

/**
 * Sends out email
 * Return true in success, false in error (and fills $error variable)
 */
function send_email($params, &$error = null, $debug = false) {
	global $config_env, $config_dev_server, $config_email_override, $config_smtp_host, $config_smtp_port;
	global $config_yeobill_smtp_host, $config_yeobill_smtp_port, $config_yeobill_smtp_user, $config_yeobill_smtp_password, $config_yeobill_smtp_encryption;

	if (!array_key_exists("from", $params))
		$params["from"] = SUPPORT_EMAIL;

	if (!array_key_exists("from", $params)
		|| !array_key_exists("to", $params)
		|| !array_key_exists("subject", $params)
		|| !array_key_exists("html", $params) && !array_key_exists("text", $params)
		) {
		$error = "Missing parameter(s)!";
		return false;
	}

	if ($config_dev_server)
		email_log($params["from"], $params["to"], $params["subject"], $params["text"], $params["html"]);

	if ($config_email_override)
		$params["to"] = $config_email_override;

	if ($config_env == "prod" || strstr($params["from"], "@yeobill.com")) {
		return do_email_sendgrid($params["from"], $params["from"], $params["to"], $params["subject"], $params["html"], $params["text"], $params["embedded_images"]);
	}

	$mail = new PHPMailer();
	$mail->IsSmtp();

	if ($config_smtp_host)
		$mail->Host = $config_smtp_host;
	if ($config_smtp_port)
		$mail->Port = $config_smtp_port;

	if ($debug)
		$mail->SMTPDebug = 2;
//  $mail->SMTPDebug  = 2; // 1 = errors and messages, 2 = messages only

	$mail->From  = $params["from"];
	if (array_key_exists("from_name", $params))
		$mail->FromName = $params["from_name"];
	else
		$mail->FromName = $params["from"];
	if (array_key_exists("reply_to", $params))
		$mail->addReplyTo($params["reply_to"]);
	$mail->Subject = $params["subject"];
	if (array_key_exists("html", $params)) {
		$mail->IsHTML(true);
		$mail->Body = $params["html"];
		if (array_key_exists("text", $params))
			$mail->AltBody = $params["text"];
	} else {
		$mail->Body = $params["text"];
	}
	if (is_array($params["to"])) {
		foreach ($params["to"] as $to)
			$mail->addAddress($to);
	} else {
		$mail->addAddress($params["to"]);
	}
	if (array_key_exists("cc", $params)) {
		if (is_array($params["cc"])) {
			foreach ($params["cc"] as $cc)
				$mail->addCC($cc);
		} else {
			$mail->addCC($params["cc"]);
		}
	}
	if (array_key_exists("bcc", $params)) {
		if (is_array($params["bcc"])) {
			foreach ($params["bcc"] as $bcc)
				$mail->addBCC($bcc);
		} else {
			$mail->addBCC($params["bcc"]);
		}
	}

	if (array_key_exists("embedded_images", $params)) {
		foreach($params["embedded_images"] as $name => $path) {
			$mail->AddEmbeddedImage($path, $name);
		}
	}

	if (array_key_exists("attachments", $params)) {
		foreach($params["attachments"] as $name => $path) {
			$mail->AddAttachment($path, $name);
		}
	}

	$mail->XMailer = ' ';	//remove email header X-Mailer: PHPMailer 5.2.9 (https://github.com/PHPMailer/PHPMailer/) - some MTAs use this to mark email as spam

	if ($mail->Send()) {
        $mail->ClearAddresses();
		return true;
	} else {
		$error = $mail->ErrorInfo;
		return false;
	}
}

/**
 * Sends out email using YAHOO SMTP
 * Return true in success, error_message otherwise
 */
function email_yahoo($from, $from_name, $password, $to, $subject, $html, $text) {
	global $config_dev_server, $config_email_override;

	if ($config_dev_server) {
		email_log($from, $to, $subject, $text, $html);
		return true;
	}

	if ($config_email_override)
		$to = $config_email_override;

	$mail = new PHPMailer();

	$mail->From  = $from;
	$mail->FromName = $from_name;

	$mail->IsSMTP();

	$mail->SMTPAuth = true;
	$mail->Username = $from;
	$mail->Password = $password;
	$mail->SMTPSecure = "ssl";
	//$mail->SMTPSecure = "tls";

	$mail->Host = "smtp.mail.yahoo.com";
	$mail->Port = 465;

	$mail->SMTPDebug  = 4; // 1 = errors and messages, 2 = messages only

	$mail->Sender   =  $from;
	$mail->ConfirmReadingTo  = $from;

	$mail->AddReplyTo($from);
	$mail->IsHTML(true);
	$mail->Subject = $subject;

	$mail->Body = $html;
	$mail->AltBody = $text; //txt body

	$mail->AddAddress($to, $to);

	if ($mail->Send()) {
		$mail->ClearAddresses();
		return true;
	} else {
		return $mail->ErrorInfo;
	}
}

/**
 * Called from cron, sends out emails spooled in db, currently supporting only gmail
 */
function send_out_spooled_emails() {
	global $db;

	$in_progress_file = _CMS_ABS_PATH."/sending_spooled_emails_in_progress.flag";

	if (file_exists($in_progress_file)) {

		//check if this is not by any chance old in_progress touch file (e.g. when we rebooted server, file might have stayed)
		$too_old = time() - 3600;	//too old = more than 1 hour old
		$mtime = filemtime($in_progress_file);
		if ($mtime < $too_old) {
			file_log("spool", "In progress file exists, but too old (timestamp={$mtime}), deleting in_progress file.");
			@unlink($in_progress_file);
		} else {
			file_log("spool", "In progress file already exists, skipping");
			return true;
		}
	}

	file_put_contents($in_progress_file, "Started on: ".date("Y-m-d H:i:s T"));

	$res = $db->q("SELECT * FROM spool_gmail WHERE succeeded IS NULL AND sent_stamp IS NULL LIMIT 100");
	while ($row = $db->r($res)) {
		$start = time();
		//$ret = do_email_gmail($row["from"], $row["from_name"], $row["to"], $row["subject"], $row["html"], $row["text"], json_decode($row["embedded_images"], true));
		$ret = do_email_sendgrid($row["from"], $row["from_name"], $row["to"], $row["subject"], $row["html"], $row["text"], json_decode($row["embedded_images"], true));
		$end = time();
		if ($ret === true) {
			$db->q("UPDATE spool_gmail SET sent_stamp = ?, succeeded = 1 WHERE id = ? LIMIT 1", [$end, $row["id"]]);
		} else {
			$db->q("UPDATE spool_gmail SET sent_stamp = ?, succeeded = 0, error = ? WHERE id = ? LIMIT 1", [$end, $ret, $row["id"]]);
		}
		file_log("spool", "Sent out: #{$row["id"]} to {$row["to"]}, took ".round($end-$start)." seconds, success=".intval($ret));
	}

	unlink($in_progress_file);
	return true;
}

/**
 * Sends out email using GMAIL - adds to spool table in db - email is later sent by cron
 */
function email_gmail($from, $from_name, $to, $subject, $html, $text, $embedded_images = []) {
	global $db;

	debug_log("email_gmail(): to={$to}, subject={$subject}");

	$res = $db->q("INSERT INTO spool_gmail
		(`from`, from_name, `to`, subject, html, `text`, embedded_images, created_stamp)
		VALUES
		(?, ?, ?, ?, ?, ?, ?, ?)",
		[$from, $from_name, $to, $subject, $html, $text, json_encode($embedded_images), time()]
		);
	$id = $db->insertid($res);
	debug_log("email_gmail(): id={$id}");
	if ($id)
		return true;
	else
		return false;
}

/**
 * Sends out email using GMAIL SMTP
 * Return true in success, error_message otherwise
 */
function do_email_gmail($from, $from_name, $to, $subject, $html, $text, $embedded_images = []) {
	global $config_dev_server, $config_email_override;

	if ($config_dev_server) {
		email_log($from, $to, $subject, $text, $html);
		return true;
	}

	if ($config_email_override)
		$to = $config_email_override;

	$password = null;
	if ($from == "receipts@viphost.com")
		$password = "HoffaTrix3";
	else if ($from == "joemolson84@gmail.com")
		$password = "JoeEme84";
	else
		return false;

	//new google xoauth: https://github.com/PHPMailer/PHPMailer/wiki/Using-Gmail-with-XOAUTH2
	if ($from == "receipts@viphost.com") {

		$mail = new PHPMailerOAuth;
	
		$mail->IsSMTP();
		$mail->SMTPDebug = 3;
		$mail->Debugoutput = 'add_to_global_email_log';

		$mail->Host = "smtp.gmail.com";
		$mail->Port = 587;
		$mail->SMTPSecure = "tls";
		$mail->AuthType = 'XOAUTH2';
		$mail->SMTPAuth = true;

		$mail->oauthUserEmail = "receipts@viphost.com";
		$mail->oauthClientId = "722255413444-t76amt0re95d7846ef182ub9qivkmm9a.apps.googleusercontent.com";
		$mail->oauthClientSecret = "3_8wPQopiUYFgi--EZxVnYPP";
		$mail->oauthRefreshToken = "1/xkNDtEKUosK430DAy9T53JbSbJLEzg5sZme1BGRuKzM";

	} else {
	
		$mail = new PHPMailer();

		$mail->IsSMTP();
		$mail->SMTPDebug = 3;
		$mail->Debugoutput = 'add_to_global_email_log';

		$mail->Username = $from;
		$mail->Password = $password;
		$mail->Host = "smtp.gmail.com";
		$mail->Port = 465;
		$mail->SMTPSecure = "ssl";
		$mail->SMTPAuth = true;
	}

	$mail->From  = $from;
	$mail->FromName = $from_name;

	$mail->Sender   =  $from;
	$mail->ConfirmReadingTo  = $from;

	$mail->AddReplyTo($from);
	$mail->IsHTML(true);
	$mail->Subject = $subject;

	$mail->Body = $html;
	$mail->AltBody = $text; //txt body

	$mail->AddAddress($to, $to);
	//$mail->AddBCC("admin@adultsearch.com", "admin");

	foreach($embedded_images as $name => $path) {
		$mail->AddEmbeddedImage($path, $name);
	}

	if ($mail->Send()) {
		$mail->ClearAddresses();
		return true;
	} else {
		return $mail->ErrorInfo;
	}
}

/**
 * Sends out email using SENDGRID WEB API
 * Return true in success, error_message otherwise
 * Reference: https://sendgrid.com/docs/API_Reference/Web_API_v3/index.html
 */
function do_email_sendgrid($from, $from_name, $to, $subject, $html, $text, $embedded_images = []) {
	global $config_sendgrid_api_key, $config_dev_server, $config_email_override;

	if ($config_dev_server) {
		email_log($from, $to, $subject, $text, $html);
		return true;
	}

	if ($config_email_override)
		$to = $config_email_override;

	//if (substr($from, -12) != "@viphost.com")
	//	return "From not @viphost.com, can't use sendgrid as sender";

	$email = new \SendGrid\Mail\Mail();
	$email->setFrom($from, $from_name);
	$email->setSubject($subject);
	$email->addTo($to, $to);
	$email->addContent("text/plain", $text);
	$email->addContent(
		"text/html", $html
	);

	foreach ($embedded_images as $name => $filepath) {
		$file_encoded = base64_encode(file_get_contents($filepath));
		$mime = mime_content_type($filepath);
		if (!$file_encoded || !$mime)
			continue;
		$email->addAttachment($file_encoded, mime_content_type($filepath), $name, "inline", $name);
	}

	$sendgrid = new \SendGrid($config_sendgrid_api_key);
	try {
		$response = $sendgrid->send($email);
		$code = $response->statusCode();
		$headers = $response->headers();
		$body = $response->body();
		//debug_log("code={$code}, body='{$body}', headers=".print_r($headers, true)."<br />\n");
		if ($code >= 400) {
			$body_arr = json_decode($body, true);
			if ($body_arr
				&& array_key_exists("errors", $body_arr)
				&& is_array($body_arr["errors"])
				&& array_key_exists("message", $body_arr["errors"][0])
				)
				return "SG Error: ".$body_arr["errors"][0]["message"];
			return "SG Error: {$code}";
		}
	} catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
		return "SG Error";
	}

	return true;
}

global $global_email_log;
function add_to_global_email_log($str, $level) {
	global $global_email_log;
	if ($level <= 3)
		$global_email_log .= $str;
}

function reportAdmin($subject, $message = "", $params = NULL) {
	$message = "<style type=\"text/css\">table {border-spacing: 0px; border-collapse: collapse;} table td, table th {border: 1px solid #999; padding: 1px;} table tr:nth-child(odd) td {background-color: #eee;} table th {text-align: left;}</style>\n{$message}";
	if (!empty($params)) {
		$message .= "<br /><br />\n<table>\n";
		$message .= _getInfoTableAux("Params", $params);
		$message .= "</table>\n<br />\n";
	}
	$message .= _getInfoTable();
	$ret = sendEmail(WEB_EMAIL, $subject, $message, ADMIN_EMAIL);
	if ($ret === false)
		debug_log("reportAdmin(): sendEmail failed: subject={$subject}, message={$message}, params=".print_r($params, true).".");
}

function makeOnlyDigitPhoneNumber($phone) {
	return preg_replace("/[^0-9]/", "", $phone);
}

function makeOnlyDigitPhoneNumberWithPlus($phone) {
	return preg_replace("/[^0-9+]/", "", $phone);
}

function phoneNiceFormat($phone) {
	$phone = preg_replace('/[^0-9\+]/', '', $phone);

	$country = null;
	if (substr($phone, 0, 1) != "+" && strlen($phone) == 10)
		$country = "US";

	$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
	$phoneNumberObject = null;
	try {
		$phoneNumberObject = $phoneUtil->parse($phone, $country);
	} catch (\libphonenumber\NumberParseException $e) {
		debug_log("makeProperPhoneNumber: Error : {$e->getCode()} phone {$phone} ". $e->getMessage());
		return $phone;
	}

	$phone_e164 = $phoneUtil->format($phoneNumberObject, \libphonenumber\PhoneNumberFormat::E164);
	if (substr($phone_e164, 0, 2) == "+1")
		return "(".substr($phone_e164, 2, 3).") ".substr($phone_e164, 5, 3)."-".substr($phone_e164, 8);

	return $phoneUtil->format($phoneNumberObject, \libphonenumber\PhoneNumberFormat::INTERNATIONAL);	
}

function makeProperPhoneNumber($phone, $format_e164 = false) {
	global $ctx;

	if(!$phone) {
		return $phone;
	}

	$number_format = \libphonenumber\PhoneNumberFormat::NATIONAL;
	switch ($ctx->country) {
		case 'uk':
			$region_code = 'GB';
			break;
		case 'ca':
			$region_code = 'CA';
			break;
		case 'united-states':
			$region_code = 'US';
			break;
		default:
			$region_code = $ctx->country_code;
			$number_format = \libphonenumber\PhoneNumberFormat::INTERNATIONAL;
	}

	$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
	try {
		$phone_proto = $phoneUtil->parse($phone, $region_code);
	} catch (\libphonenumber\NumberParseException $e) {
		debug_log("makeProperPhoneNumber: Error : {$e->getCode()} phone {$phone} ". $e->getMessage());
		return $phone;
	}

	if($format_e164) {
		return $phoneUtil->format($phone_proto, \libphonenumber\PhoneNumberFormat::E164);
	}
	return $phoneUtil->format($phone_proto, $number_format);
}

function prepareMap($id, $table, $row, $section, $module) {
	global $db, $config_mapquest_key, $config_dev_server;

	if (is_null($row["loc_lat"]) || is_null($row["loc_long"])) {
		list($lat, $lon, $city, $zip) = get_coord($row["address1"], $row["loc_name"], $row["s"],
			$row["zipcode"]);

		if ($lat != "error") {
			$row["loc_lat"]  = $lat;
			$row["loc_long"] = $lon;
		} else {
            $row["loc_lat"]  = 0.0;
            $row["loc_long"] = 0.0;
        }
        $db->q("update place set loc_lat = '$lat', loc_long = '$lon' where place_id = '$id'");
	}

    if (is_null($row["loc_lat"]) || is_null($row["loc_long"]) || $row["loc_lat"] == 0 || $row["loc_long"] == 0) {
        return null;
    }

	$map = array(
		"loc_lat"    => $row["loc_lat"],
		"loc_long"   => $row["loc_long"],
		"link"       => "/map?id={$id}&lightbox=1&section={$section}",
		'public_key' => $config_mapquest_key,
	);

	$map["map_image_update_url"] = dir::get_map_image_update_url($module, $id, $row["loc_lat"], $row["loc_long"]);
//caching of the map images
	if ($row["map_image"] && file_exists(dir::get_map_image_filepath($module, $row["map_image"]))) {
		$map["map_image_url"] = dir::get_map_image_url($module, $row["map_image"]);
	} else {
		if($config_dev_server) {
			debug_log("common:prepareMap: Mapquest static Map request : ".$map["map_image_update_url"]);
		}
	}

//Get direct link in case if we don't have both saved image and update link
	if (!$map['map_image_update_url'] && !$map['map_image_url']) {
		$mapGenerator             = new MapQuestMapGenerator($map['public_key']);
		$map['map_static_direct'] = $mapGenerator->getStaticImageUrlForCoordinates($row["loc_lat"], $row["loc_long"]);
		if($config_dev_server) {
			debug_log("common:prepareMap: Mapquest static Map request (may be): ".$map["map_image_update_url"]);
		}
	}

	return $map;
}

//validates phone number and converts to international format
function phone_to_international($phone, &$error = null) {
	$phone = preg_replace('/[^0-9\+]/', '', $phone);

	if (strlen($phone < 10)) {
		$error = "Phone number too short";
		return false;
	}

	if (substr($phone, 0, 1) == "+")
		return $phone;

	if (substr($phone, 0, 2) == "00")
		return "+".substr($phone, 2);

	if (strlen($phone) == 10 && substr($phone, 0, 1) != "0")
		return "+1".$phone;	//U.S. / Canada phone

	if (strlen($phone) == 11 && substr($phone, 0, 1) == "1")
		return "+".$phone;	//U.S. / Canada

	if (strlen($phone) == 12 && substr($phone, 0, 2) == "44")
		return "+".$phone;	//U.K. phone

	if (strlen($phone) == 11 && substr($phone, 0, 2) == "07")
		return "+44".substr($phone, 1);	//U.K. phone

	//unknown format
	$error = "Unknown format";
	return false;
}

/**
 * Returns true if it is US/Canada number and it has invalid are code
 * Otherwise returns false
 */
function area_code_not_valid($phone) {
	$valid_area_codes = [201, 202, 203, 204, 205, 206, 207, 208, 212, 213, 214, 215, 216, 217, 218, 301, 302, 303, 304, 305, 306, 307, 312, 313, 314, 315, 316, 317, 319, 401, 402, 403, 404, 405, 406, 412, 413, 414, 415, 416, 418, 419, 501, 502, 503, 504, 505, 512, 513, 514, 515, 517, 518, 601, 602, 603, 604, 605, 612, 613, 614, 616, 617, 618, 701, 702, 703, 704, 712, 713, 715, 716, 717, 801, 802, 803, 812, 814, 815, 816, 901, 902, 913, 914, 915, 916, 219, 417, 516, 714, 519, 813, 817, 918, 308, 507, 606, 607, 615, 912, 919, 506, 608, 209, 309, 318, 509, 705, 805, 806, 808, 819, 907, 609, 809, 408, 707, 906, 709, 807, 903, 904, 800, 900, 804, 706, 905, 903, 619, 409, 700, 710, 718, 818, 407, 508, 719, 708, 903, 310, 410, 510, 908, 706, 905, 210, 600, 706, 909, 917, 456, 810, 905, 910, 610, 334, 352, 360, 423, 441, 500, 520, 540, 541, 770, 860, 864, 941, 954, 970, 242, 246, 250, 268, 281, 320, 345, 561, 573, 630, 664, 757, 758, 773, 787, 847, 869, 888, 937, 972, 228, 240, 248, 253, 254, 264, 267, 284, 330, 336, 340, 425, 435, 440, 443, 473, 530, 562, 580, 626, 649, 650, 660, 732, 734, 740, 760, 765, 767, 781, 785, 830, 850, 867, 868, 870, 876, 920, 931, 940, 956, 973, 978, 225, 252, 256, 323, 450, 559, 570, 651, 678, 720, 724, 727, 775, 784, 786, 828, 831, 843, 877, 925, 949, 231, 262, 270, 321, 337, 347, 361, 469, 480, 484, 623, 631, 636, 646, 661, 662, 780, 832, 856, 858, 863, 865, 229, 234, 478, 571, 641, 682, 763, 845, 859, 866, 936, 952, 971, 979, 989, 251, 276, 289, 339, 351, 386, 434, 551, 563, 586, 620, 647, 731, 754, 774, 848, 857, 862, 878, 928, 939, 980, 985, 224, 260, 269, 479, 567, 574, 585, 772, 947, 239, 325, 430, 432, 951, 769, 829, 226, 424, 438, 331, 575, 762, 779, 581, 587, 657, 778, 385, 442, 475, 533, 681, 747, 872, 343, 458, 470, 534, 544, 579, 938, 249, 531, 539, 721, 855, 929, 431, 566, 667, 669, 873, 984, 236, 272, 365, 437, 639, 737, 844, 346, 364, 577, 725, 782, 930, 959, 220, 588, 622, 628, 629, 854, 380, 463, 522, 548, 743, 825, 934, 223, 332, 521, 564, 680, 726, 833, 838, 986, 456, 279, 367, 445, 640, 658, 820];

	$err = null;
	$phone = phone_to_international($phone, $err);
	if ($phone === false)
		return false;
	if (substr($phone, 0, 2) != "+1" || strlen($phone) != 12)
		return false;

	//this is US/Canada number
	$area_code = substr($phone, 2, 3);

	if (in_array($area_code, $valid_area_codes))
		return false;
	return true;
}

function _pagingQuery($arr = NULL, $hidden = 0, $forurl = 0) {
	$link = "";
	$and = $forurl ? "&" : "&amp;";
	foreach($_GET as $key => $value) {
		if( $key != "filename" && $key != "page" && (is_null($arr)||(is_array($arr)&&!in_array($key,$arr))) ) {
			if( is_array($value) ) {
				foreach($value as $v) {
					if( !$hidden ) $link .= $link ? ("{$and}{$key}[]={$v}") : ("?{$key}[]={$v}");
					else $link .= "<input type=\"hidden\" name=\"{$key}[]\" value=\"$v\" />";
				}
			}
			elseif( !empty($value) ) {
				if( !$hidden ) $link .= $link ? ("{$and}{$key}={$value}") : ("?{$key}={$value}");
				else $link .= "<input type=\"hidden\" name=\"{$key}\" value=\"$value\" />";
			}
		}
	}
	return $link;
}

function _pagingQueryFilter($link, &$filter) {
		if(!is_array($filter)) return;
		foreach($filter as $key=>&$value) {
		if( !isset($value["link"]) || empty($value["link"]) ) {
			if( is_array($value["type"]) ) {
				for($i=0;$i<count($value["type"]);$i++) {
					$value["link"] = preg_replace('@(&amp;)?'.$value["type"][$i].'(\[[^\]]*\])?='.preg_quote($value["id"][$i], '@').'@', '', empty($value["link"])?$link:$value["link"]);
				}
			} else $value["link"] = preg_replace('@(&amp;)?'.$value["type"].'(\[[^\]]*\])?='.preg_quote($value["id"], '@').'@', '', $link);
		}
		else $value["link"] .= preg_replace('@(&amp;|\?)?'.$value["type"].'(\[[^\]]*\])?='.preg_quote($value["id"], '@').'@', '', $link);
		if( empty($value["link"]) ) $value["link"] = "?";
		}

}

function _getInfoAccount() {
	global $account;
	$html = "<hr/>Account info:<br />";
	if (!$account->IsLoggedIn()) {
		$html .= "<strong>Anonymous user</strong><br />";
	} else {
		if ($account->isrealadmin()) {
			$html .= "Superadmin ";
		} else if ($account->isrealadmin()) {
			$html .= "Admin ";
		} else {
			$html .= "User ";
		}
		$user = $account->getUserDetail();
		if (!is_null($user))
			$html .= "<strong>{$user["username"]}</strong> - email={$user["email"]}";
			if ($user["worker"])
				$worker = ", worker";
			$html .= " (account_id={$user["account_id"]}, account_level={$user["account_level"]}{$worker})";
	}
	$html .= "<br />";
	if (account::getUserIp())
		$html .= "IP: ".account::getUserIp().", ";
	$html .= "Client: ".$_SERVER['HTTP_USER_AGENT']."<br />";
	return $html;
}

function _getInfoTableAux($title, $arr) {
	$html = "";
	$html .= "<tr><th colspan=\"2\">{$title}</th></tr>\n";
	reset($arr);
	foreach($arr as $key => $value) {
		if ($key == "cc_cc")
			$value = "xxxxxxxxxxxxxxxx";
		else if ($key == "cc_cvc2")
			$value = "xxx";
		$html .= "<tr><td>{$key}</td><td>{$value}</td></tr>\n";
	}
	return $html;
}

function _getInfoTable() {
	global $account;

	$msg = "";
	$msg .= "<table>\n";
	$msg .= "<tr><th colspan=\"2\">URL</th></tr>\n";
	$url = $_SERVER["HTTP_HOST"].$_SERVER['REQUEST_URI'];//.$_SERVER["QUERY_STRING"];
	if ($_SERVER["HTTPS"])
		$url = "https://{$url}";
	else
		$url = "http://{$url}";
	$msg .= "<tr><td>URL</td><td>{$_SERVER["REQUEST_METHOD"]} <a href=\"{$url}\">{$url}</a></td></tr>\n";
	if (!empty($_SERVER["HTTP_REFERER"]))
		$ref = "<a href=\"{$_SERVER["HTTP_REFERER"]}\">{$_SERVER["HTTP_REFERER"]}</a>";
	else
		$ref = "";
	$msg .= "<tr><td>Referer:</td><td>{$ref}</td></tr>\n";
	$msg .= "<tr><th colspan=\"2\">Account</th></tr>\n";
	if ($account && ($account_id = $account->isMember())) {
		$row = $account->getUserDetail($account_id);
		$acc = "account_level: '{$row["account_level"]}', username: '{$row["username"]}', account_id: '{$row["account_id"]}', email: '{$row["email"]}'</td></tr>\n";
	} else {
		$acc = "anonymous";
	}
	$msg .= "<tr><td colspan=\"2\">{$acc}</td></tr>\n";
	$msg .= _getInfoTableAux("SESSION", $_SESSION);
	$msg .= _getInfoTableAux("GET", $_GET);
	$msg .= _getInfoTableAux("POST", $_POST);
	$msg .= _getInfoTableAux("COOKIE", $_COOKIE);
	$msg .= "<tr><th colspan=\"2\">Client</th></tr>\n";
	$msg .= "<tr><td>IP address</td><td>".account::getUserIp()."</td></tr>\n";
	$msg .= "<tr><td>User agent</td><td>".$_SERVER['HTTP_USER_AGENT']."</td></tr>\n";
	$msg .= "</table>\n";

	return $msg;
}

function getMonthRange(&$start_date, &$end_date, $offset=0) {
	$start_date = '';
	$end_date = '';
	$date = date('Y-m-d');

	list($yr, $mo, $da) = explode('-', $date);
	$start_date = date('Y-m-d', mktime(0, 0, 0, $mo - $offset, 1, $yr));

	$i = 2;

	list($yr, $mo, $da) = explode('-', $start_date);

	while(date('d', mktime(0, 0, 0, $mo, $i, $yr)) > 1) {
		$end_date = date('Y-m-d', mktime(0, 0, 0, $mo, $i, $yr));
		$i++;
	}
}

function time_to_human($etime, $addition = "") {
	if ($etime < 1)	{
		return '0 seconds';
	}

	$a = array( 12 * 30 * 24 * 60 * 60	=>  'year',
				30 * 24 * 60 * 60		=>  'month',
				24 * 60 * 60			=>  'day',
				60 * 60					=>  'hour',
				60						=>  'minute',
				1						=>  'second'
				);

	foreach ($a as $secs => $str) {
		$d = $etime / $secs;
		if ($d >= 1) {
			$r = round($d);
			return $r . ' ' . $str . ($r > 1 ? 's' : '') . $addition;
		}
	}
}

function time_elapsed_string($ptime) {
	$addition = "";
	if (time() > $ptime) {
		$etime = time() - $ptime;
		$addition = " ago";
	} else {
		$etime = $ptime - time();
	}

	if ($etime < 1)	{
		return '0 seconds';
	}

	return time_to_human($etime, $addition);
/*
	$a = array( 12 * 30 * 24 * 60 * 60	=>  'year',
				30 * 24 * 60 * 60		=>  'month',
				24 * 60 * 60			=>  'day',
				60 * 60					=>  'hour',
				60						=>  'minute',
				1						=>  'second'
				);

	foreach ($a as $secs => $str) {
		$d = $etime / $secs;
		if ($d >= 1) {
			$r = round($d);
			return $r . ' ' . $str . ($r > 1 ? 's' : '') . $addition;
		}
	}
*/
}

function float_equal($val1, $val2) {
	if (abs($val1 - $val2) > 0.001)
		return false;
	return true;
}

function is_bot() {
	if (!array_key_exists("HTTP_USER_AGENT", $_SERVER))
		return false;
	$user_agent = $_SERVER["HTTP_USER_AGENT"];
	if (strstr($user_agent, "Googlebot")
		|| strstr($user_agent, "bingbot")
		|| strstr($user_agent, "msnbot")
		|| strstr($user_agent, "Baiduspider")
		|| strstr($user_agent, "TinEye-bot")
		|| strstr($user_agent, "ia_archiver")
		|| strstr($user_agent, "HotSearch")
		|| strstr($user_agent, "SISTRIX")
		|| strstr($user_agent, "YandexBot")
		|| strstr($user_agent, "Spyder")
		|| strstr($user_agent, "MJ12bot")
		|| strstr($user_agent, "CCBot")
		|| strstr($user_agent, "DotBot")
		|| strstr($user_agent, "AhrefsBot")
		|| strstr($user_agent, "SearchToolbar")
		|| strstr($user_agent, "Yahoo! Slurp")
		|| stristr($user_agent, "Crawler")
		|| stristr($user_agent, "SemrushBot")
		)
		return true;
	return false;
}

function get_device_type($agent = null) {
	$http_accept = $x_wap_profile = $http_profile = $http_x_operamini_phone_ua = $http_device_stock_ua = null;
	if (!$agent) {
		if (!array_key_exists("HTTP_USER_AGENT", $_SERVER))
			return false;
		$agent = $_SERVER["HTTP_USER_AGENT"];
	}
	if (array_key_exists("HTTP_ACCEPT", $_SERVER))
		$http_accept = $_SERVER["HTTP_ACCEPT"];
	if (array_key_exists("HTTP_X_WAP_PROFILE", $_SERVER))
		$x_wap_profile = $_SERVER["HTTP_X_WAP_PROFILE"];
	if (array_key_exists("HTTP_PROFILE", $_SERVER))
		$http_profile = $_SERVER["HTTP_PROFILE"];
	if (array_key_exists("HTTP_X_OPERAMINI_PHONE_UA", $_SERVER))
		$http_x_operamini_phone_ua = $_SERVER["HTTP_X_OPERAMINI_PHONE_UA"];
	if (array_key_exists("HTTP_DEVICE_STOCK_UA", $_SERVER))
		$http_device_stock_ua = $_SERVER["HTTP_DEVICE_STOCK_UA"];

	$tablet_browser = 0;
	$mobile_browser = 0;

	if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($agent))) {
		$tablet_browser++;
	}

	if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($agent))) {
		$mobile_browser++;
	}

	if ((strpos(strtolower($http_accept),'application/vnd.wap.xhtml+xml') > 0) or ((isset($x_wap_profile) or isset($http_profile)))) {
		$mobile_browser++;
	}

	$mobile_ua = strtolower(substr($agent, 0, 4));
	$mobile_agents = array(
		'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
		'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
		'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
		'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
		'newt','noki','palm','pana','pant','phil','play','port','prox',
		'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
		'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
		'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
		'wapr','webc','winw','winw','xda ','xda-');

	if (in_array($mobile_ua,$mobile_agents)) {
		$mobile_browser++;
	}

	if (strpos(strtolower($agent),'opera mini') > 0) {
		$mobile_browser++;
		//Check for tablets on opera mini alternative headers
		$stock_ua = strtolower(isset($http_x_operamini_phone_ua) ? $http_x_operamini_phone_ua : (isset($http_device_stock_ua) ? $http_device_stock_ua : ''));
		if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
		  $tablet_browser++;
		}
	}

	if ($tablet_browser > 0)
		return "Tablet";
	else if ($mobile_browser > 0)
		return "Mobile";
	else
		return "Desktop";
}

function get_os($agent = null) {
	if (!$agent) {
		if (!array_key_exists("HTTP_USER_AGENT", $_SERVER))
			return false;
		$agent = $_SERVER["HTTP_USER_AGENT"];
	}
	if (stripos($agent, 'Android') !== false)
		return "Android";
	else if (stripos($agent, 'Linux') !== false)
		return "Linux";
	else if (stripos($agent, 'Mac OS') !== false)
		return "iOS";
	else if (stripos($agent, 'Windows') !== false)
		return "Windows";
	return "Other";
}

function get_browser_name($agent = null) {
	if (!$agent) {
		if (!array_key_exists("HTTP_USER_AGENT", $_SERVER))
			return false;
		$agent = $_SERVER["HTTP_USER_AGENT"];
	}
	if (stripos($agent, 'Firefox') !== false)
		return "Firefox";
	else if (stripos($agent, 'Edge') !== false)
		return "Edge";
	else if (stripos($agent, 'MSIE') !== false)
		return "MSIE";
	else if (stripos($agent, 'SamsungBrowser') !== false)
		return "SamsungBrowser";
	else if (stripos($agent, 'Chrome') !== false)
		return "Chrome";
	else if (stripos($agent, 'Safari') !== false)
		return "Safari";
	else if (stripos($agent, 'Opera') !== false)
		return "Opera";
	return "Other";
}

/**
 * Returns name of getter for specified property
 * E.g. for "auto_renew" returns "getAutoRenew"
 */
function get_getter_name($attr_name) {
	return "get".str_replace(" ", "", ucwords(str_replace("_", " ", $attr_name)));
}

//converts real utf8 string to mysql ut8 string
//(removes 4-bytes UTF8 characters and UTF8 control characters)
function str_to_utf8($str) {
	//this removes 4byte UTF-8 characters (because we have ut8 everywhere in db, and not utf8mb4)
	$str = preg_replace('/[\xF0-\xF7].../s', '', $str);
	//removes UTF8 control characters
	$str = preg_replace('/[\x00-\x1F\x80-\x9F]/u', '', $str);   // http://stackoverflow.com/questions/1176904/php-how-to-remove-all-non-printable-characters-in-a-string
	return $str;
}

//ui time select array generator, used in backend
function get_time_array() {
	$time_first = true;
	$a_r_t = array();
	for($i = 0, $s = 12, $x = "AM"; $i < 24; $i++) {
		for($l=0; $l<60; $l=$l+30) {
			$t = (($i*60)+$l);
			if ($l == 0)
				$l = "00";
			$a_r_t[$t] = "{$s}:{$l} {$x}";
		}
		if( $s == 12 && $time_first ) {
			$s = 1;
			$time_first = false;
		} else if( $s == 12 ) {
			$s = 1;
		} else if( $s == 11 && !$time_first ) {
			$s++;
			$x = "PM";
		} else
			$s++;
	}
	return $a_r_t;
}

function error_redirect($error_msg, $path = null) {
	flash::add(flash::MSG_ERROR, $error_msg);
	echo "<br /><strong>Please wait...</strong><br />";
	if (!$path)
		$path = "/";
	system::go($path);
	return false;
}

function success_redirect($msg, $path = null) {
	flash::add(flash::MSG_SUCCESS, $msg);
	echo "<br /><strong>Please wait...</strong><br />";
	if (!$path)
		$path = "/";
	system::go($path);
	return false;
}

function base64url_encode($data) { 
	return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
} 

function base64url_decode($data) { 
	return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
}

function rotate_index($index_name) {
	global $db;
	$db->q("INSERT IGNORE INTO cache_update (index_name, create_stamp) VALUES (?, ?)", [$index_name, time()]);
}

function formatDateDiff($start, $end=null) {
	if(!($start instanceof \DateTime)) { 
		$start = new \DateTime($start); 
	} 
	
	if($end === null) { 
		$end = new \DateTime(); 
	} 
	
	if(!($end instanceof \DateTime)) { 
		$end = new \DateTime($start); 
	} 
	
	$interval = $end->diff($start); 
	$doPlural = function($nb,$str){return $nb>1?$str.'s':$str;}; // adds plurals

	$format = array(); 
	if($interval->y !== 0) { 
		$format[] = "%y ".$doPlural($interval->y, "year"); 
	} 
	if($interval->m !== 0) { 
		$format[] = "%m ".$doPlural($interval->m, "month"); 
	} 
	if($interval->d !== 0) { 
		$format[] = "%d ".$doPlural($interval->d, "day"); 
	} 
	if($interval->h !== 0) { 
		$format[] = "%h ".$doPlural($interval->h, "hour"); 
	} 
	if($interval->i !== 0) { 
		$format[] = "%i ".$doPlural($interval->i, "minute"); 
	} 
	if($interval->s !== 0) { 
		if(!count($format)) { 
			return "less than a minute ago"; 
		} else { 
			$format[] = "%s ".$doPlural($interval->s, "second"); 
		} 
	}

	// We use the two biggest parts 
	if(count($format) > 1) { 
		$format = array_shift($format)." and ".array_shift($format); 
	} else { 
		$format = array_pop($format); 
	} 
	
	// Prepend 'since ' or whatever you like 
	return $interval->format($format); 
}

/**
 * This function converts UTC timestamp into date/time string (in specified format) in admin timezone (currently EST)
 */
function get_datetime_admin_timezone($stamp, $format = "Y-m-d H:i:s T") {
	if (!$stamp)
		return "";
	$dt = new \DateTime("", new \DateTimeZone("UTC"));
	$dt->setTimestamp($stamp);
	$dt->setTimezone(new \DateTimeZone("America/New_York"));
	return $dt->format($format);
}

function br2nl($string) {
	return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
}

function json_response($response_array) {
	header('Content-Type: application/json');
	die(json_encode($response_array));
}

function process_videos($verification_id = null) {
	global $db, $config_image_path;

	if ($verification_id)
		$res = $db->q("SELECT v.* FROM verification v WHERE v.status = 0 OR v.status IS NULL AND v.id = ?", [$verification_id]);
	else
		$res = $db->q("SELECT v.* FROM verification v WHERE v.status = 0 OR v.status IS NULL OR v.status = 1 LIMIT 5");

	while ($row = $db->r($res)) {
		$id = $row["id"];
		$filename = $row["filename"];
		
		$src_dir = $config_image_path."/verification/uploaded";
		$src_filepath = $src_dir."/".$filename;

		$arr = process_video("verification", $row);

		if ($arr["status"] == 1) {
			$db->q("UPDATE verification SET status = 1 WHERE id = ? LIMIT 1", [$id]);

		} else if ($arr["status"] == 2) {
			//update
			$db->q(
				"UPDATE verification SET filename = ?, thumbnail = ?, width = ?, height = ?, status = 2 WHERE id = ? LIMIT 1", 
				[$arr["filename"], $arr["thumbnail"], $arr["width"], $arr["height"], $id]
				);
			@unlink($src_filepath);
		}
	}

	return true;
}

/**
 * Convert video, create thumbnail
 * 
 * @global type $config_image_path
 * @param string $type "verification" or "advertising"
 * @param array $row contains "id" and "filename"
 * @return array
 */
function process_video($type, $row) {
	global $config_image_path, $config_site_abspath; //$config_image_path = root of img.adultsearch.net

	//configuration
	$tmp_dir_base = _CMS_ABS_PATH."/tmp/video_processing";

	$subdir = uniqid();
	$tmp_dir = $tmp_dir_base."/".$subdir;
	if (!mkdir($tmp_dir, 0777, true)) {
		file_log("vid", "process_video: Error: can't create tmp dir '{$tmp_dir}'");
		return ["status" => 1];
	}

	//changing directory to video tmp dir, so temp ffmpeg process files are created here and not anywhere in filesystem
	$old_dir = getcwd();
	chdir($tmp_dir);

	if ($type == "verification") {
		$src_dir = $config_image_path."/verification/uploaded";
		$tgt_dir = $config_image_path."/verification";

		//get source params
		$id = $row["id"];
		$src_filename = $row["filename"];

	} else if($type == "advertising") {
		$src_dir = $config_image_path . "/classifieds/uploaded";
		$tgt_dir = $config_image_path . "/classifieds/video";

		$id = $row["id"];
		$src_filename = $row["filename"];

	}

	$max_width = 622;
	$max_height = 350;
	$max_duration = 20;	 //in seconds
	$maxrate = "300k";	  //modify this with max_width and max_height in regard ...
	$bufsize = "700k";	  //modify this with max_width and max_height in regard ...

//	  echo "type={$type}, id={$id}, src_filename = '{$src_filename}'\n";

	$src_filepath = $src_dir."/".$src_filename;
	if (!file_exists($src_filepath)) {
		error_log("System:processVideos(): Video source file '{$src_filepath}' not found for {$type} id#{$id}");
		file_log("vid", "System:processVideos(): Video source file '{$src_filepath}' not found for {$type} id#{$id}");
		return ["status" => 1];
	}
	file_log("vid", "type = '{$type}', src_filepath = '{$src_filepath}'");

	//get width, height and length
	$width = $height = $duration = $rotation = null;
	$command = "ffprobe -i '{$src_filepath}' -show_streams";
	$output = $ret = null;
	
	exec($command, $output, $ret);
	if ($ret) {
		error_log("System:processVideos(): ffprobe failed: command='{$command}', type={$type}, id={$id}");
		return ["status" => 1];
	}
	foreach ($output as $line) {
//		echo "line='{$line}'\n";
		if (substr($line, 0, 6) == "width=")
			$width = intval(substr($line, 6));
		if (substr($line, 0, 7) == "height=")
			$height = intval(substr($line, 7));
		if (is_null($duration) && substr($line, 0, 9) == "duration=")
			$duration = floatval(substr($line, 9));
		if (substr($line, 0, 11) == "TAG:rotate=")
			$rotation = intval(substr($line, 11));
	}
//		  echo "width={$width}, height={$height}, duration={$duration}, rotation={$rotation}\n";

	$w_h_swapped = false;
	if ($rotation == 90 || $rotation == 270) {
		//video is rotated, swap width and height
		$w_h_swapped = true;
		$swap = $width; $width = $height; $height = $swap;
	}
	file_log("vid", "width={$width}, height={$height}, duration={$duration} rotation={$rotation}");
	if (!$width || !$height) {
		error_log("System:processVideos(): invalid width or height: width='{$width}', height='{$height}', type={$type}, id={$id}");
		return ["status" => 1];
	}

	//determine target width & height
	$scale = "";
	if (($width > $max_width) || ($height > $max_height)) {
		$w_ratio = $width / $max_width;
		$h_ratio = $height / $max_height;
//			  echo "w_ratio={$w_ratio}, h_ratio={$h_ratio}\n";
		file_log("vid", "w_ratio={$w_ratio}, h_ratio={$h_ratio}");
		if ($w_ratio > $h_ratio) {
//				  echo "case 1)\n";
			$new_width = $max_width;
			$new_height = round($height / $w_ratio);
			if ($new_height % 2 == 1)
				$new_height -= 1;
		} else if ($h_ratio > $w_ratio) {
//				  echo "case 2)\n";
			$new_width = round($width / $h_ratio);
			if ($new_width % 2 == 1)
				$new_width -= 1;
			$new_height = $max_height;
		} else {
//				  echo "case 3)\n";
			$new_width = $max_width;
			$new_height = $max_height;
		}
//			  echo "new_width={$new_width}, new_height={$new_height}\n";
		file_log("vid", "new_width={$new_width}, new_height={$new_height}");
		$width = $new_width;
		$height = $new_height;
		if ($w_h_swapped)
			$scale = " -vf scale={$new_height}:{$new_width}";
		else
			$scale = " -vf scale={$new_width}:{$new_height}";


	}
	$duration_options = "";
	if ($duration > $max_duration)
		$duration_options = " -t {$max_duration} ";

	//convert & optimize
	//file_log('vid','Skipping First pass as it does not do anything');
	
	$first_pass = "ffmpeg -y -i \"{$src_filepath}\" -vcodec libx264 -vprofile main -preset slow {$scale} -b:v {$maxrate} -maxrate {$maxrate} -bufsize {$bufsize} {$duration_options} -threads 0 -pass 1 -an -f mp4 /dev/null 2>&1";
	file_log("vid", "First pass: {$first_pass}");
	$output = $ret = null;
	exec($first_pass, $output, $ret);
	if ($ret) {
		file_log("vid", "process_video: Error: ffmpeg failed: first pass, command='{$first_pass}', output='".print_r($output, true)."', type={$type}, id={$id}");
		error_log("process_videos: Error: ffmpeg failed: first pass, command='{$first_pass}', output='".print_r($output, true)."', type={$type}, id={$id}");
		return ["status" => 1];
	}
	//file_log("vid", "output=".print_r($output, true));
	
	//determine filename_base
	$filename_base = preg_replace('/[^a-zA-Z0-9\._\-]/', '', preg_replace('/ /', '_', $src_filename));
	if (($pos = strrpos($src_filename, ".")) !== false) {
		$filename_base = substr($src_filename, 0, $pos);
	}
	if ($filename_base == "")
		$filename_base = system::_makeText(8);
	file_log("vid", "filename_base = '{$filename_base}'");

	//determine target filename
	$tmp_filename = "tmp_{$filename_base}.mp4";
	$tmp_filepath = "{$tmp_dir}/{$tmp_filename}";

	$second_pass = "ffmpeg -y -i \"{$src_filepath}\" -vcodec libx264 -vprofile main -preset slow {$scale} -b:v {$maxrate} -maxrate {$maxrate} -bufsize {$bufsize} {$duration_options} -threads 0 -pass 2 -acodec aac -ar 44100 -f mp4 -strict -2 \"{$tmp_filepath}\" 2>&1";
	file_log("vid", "Second pass: {$second_pass}");
	$output = $ret = null;
	exec($second_pass, $output, $ret);
	if ($ret) {
		file_log("vid", "process_video: Error: ffmpeg failed: second pass, command='{$second_pass}', output='".print_r($output, true)."', type={$type}, id={$id}");
		error_log("process_video: ffmpeg failed: second pass, command='{$second_pass}', output='".print_r($output, true)."', type={$type}, id={$id}");
		return ["status" => 1];
	}
	//file_log("vid", "Second pass output='".print_r($output, true)."'");

	//deleting ffmpeg tmp files
	@unlink("./ffmpeg2pass-0.log");
	@unlink("./ffmpeg2pass-0.log.mbtree");

	//determine target filename
	$tgt_filename = "{$filename_base}.mp4";
	$tgt_filepath = "{$tgt_dir}/{$tgt_filename}";

	//fromcess file with qt-faststart nad store in final video directory
	$command = "qt-faststart \"{$tmp_filepath}\" \"{$tgt_filepath}\" > /dev/null";
	file_log("vid", "Running qt-faststart: '{$command}'");
	$output = $ret = null;
	exec($command, $output, $ret);
	if ($ret) {
		file_log("vid", "process_video: Error: qt-faststart failed: command='{$command}', output='".print_r($output, true)."', type={$type}, id={$id}");
		error_log("System:processVideos(): qt-faststart failed: command='{$command}', output='".print_r($output, true)."', type={$type}, id={$id}");
		return ["status" => 1];
	}
	//file_log("vid", "qt-faststart output=".print_r($output, true)."");

	//create screenshot
	$ss_filename = "{$filename_base}.jpg";
	$ss_filepath = "{$tgt_dir}/{$ss_filename}";
//	$rotate_params = "";		//taken from https://stackoverflow.com/a/9570992
//	if ($rotation == 90)
//		$rotate_params = " -vf \"transpose=1\" ";
//	else if ($rotation == 180)
//		$rotate_params = " -vf \"transpose=2,transpose=2\" ";
//	else if ($rotation == 270)
//		$rotate_params = " -vf \"transpose=2\" ";
	
	// Create thumbnail from source video that has correct rotation metadata.
	//
	//TODO if we have ffmpegversion > 2.7 it autorotates the photos - we should remove our rotation code or add param '-noautorotate' !

	//$command = "ffmpeg -y -ss 1 -i \"{$tgt_filepath}\" -vframes 1 -q:v 2 {$rotate_params} \"{$ss_filepath}\"";
	$command = "ffmpeg -y -ss 1 -i \"{$tgt_filepath}\" -vframes 1 -q:v 2 {$scale} \"{$ss_filepath}\" 2>&1";
	file_log("vid", "Creating screenshot: '{$command}'");
	$output = $ret = null;
	exec($command, $output, $ret);
	if ($ret) {
		file_log("vid", "process_video: Error: creating screenshot failed: command='{$command}', output='".print_r($output, true)."', type={$type}, id={$id}");
		error_log("System:processVideos(): creating screenshot failed: command='{$command}', output='".print_r($output, true)."', type={$type}, id={$id}");
		return ["status" => 1];
	}
	//file_log("vid", "Screenshot output=".print_r($output, true)."");

	//TODO archive src file ?
	//delete tmp file
	@unlink($tmp_filepath);
	@unlink($src_filepath); // delete source file

	//changing directory to original dir and removing tmp dir
	chdir($old_dir);
	@rmdir($tmp_dir);

	return [
		"filename" => $tgt_filename,
		"thumbnail" => $ss_filename,
		"width" => $width,
		"height" => $height,
		"status" => 2,
		];
}


/**
 * Sends out agency notification email
 */
function sendSupportEmail($template_filename_base, $params, $subject, $email) {
	global $twig;

	if (!$template_filename_base || !$subject || !$email) {
		reportAdmin("AS: Error sending agency email", "template_filename_base='{$template_filename_base}', subject='{$subject}', email='{$email}'", $params);
		return false;
	}

	$template = $twig->load("{$template_filename_base}.html.twig");
	$html = $template->render($params);

	$template = $twig->load("{$template_filename_base}.txt.twig");
	$text = $template->render($params);

	$error = null;
	$ret = send_email([
		"from" => SUPPORT_EMAIL,
		"to" => $email,
//		"bcc" => ADMIN_EMAIL,
		"subject" => $subject,
		"html" => $html,
		"text" => $text,
	], $error);
	if (!$ret) {
		reportAdmin("AS: Error sending agency email", "template_filename_base='{$template_filename_base}', subject='{$subject}', email='{$email}', error='{$error}'", $params);
	}
	return $ret;
}

/**
 * Added to replace admin ip if logging string has it
 * @param $string
 * @return string
 */
function replaceIpInStr($string) {
	if (account::getRealIp() !== account::getUserIp())	{
		$string = str_replace(account::getRealIp(), account::getUserIp(), $string);
	}
	return $string;
}

function getRandomString($len = 40) {
	//$scr = preg_replace('/[^a-zA-Z0-9]/i', '', base64_encode(random_bytes($len + 10)));
	//PHP 5.6 workaround
	$m = "abcdefghijklmnoprstuwyxzABCDEFGHIJKLMOPRSTUYW1234567890";
	$s = "";
	for($i=0;$i<$len;$i++)
		$s .= $m[rand(0, strlen($m))];
	if (strlen($s) > $len)
		return substr($s, 0, $len);
	else
		return str_pad($s, $len, "j59834nfh39chg94mg10bn2lkf7j34");
}

/**
 * Returns an array of latitude and longitude from the Image file
 *
 * @param $filename
 * @return mixed:number |boolean
 */
function read_geo($filename) {
	if (!is_file($filename)) {
		return false;
	}

	if (!function_exists('exif_read_data')) {
		debug_log('Not exist "exif_read_data" function');
		return false;
	}

	$info = exif_read_data($filename);
	if (isset($info['GPSLatitude']) && isset($info['GPSLongitude'])
		&& isset($info['GPSLatitudeRef'])
		&& isset($info['GPSLongitudeRef'])
		&& in_array($info['GPSLatitudeRef'], array('E', 'W', 'N', 'S'))
		&& in_array($info['GPSLongitudeRef'], array('E', 'W', 'N', 'S'))
	) {

		$GPSLatitudeRef  = strtolower(trim($info['GPSLatitudeRef']));
		$GPSLongitudeRef = strtolower(trim($info['GPSLongitudeRef']));

		$lat_degrees_a = explode('/', $info['GPSLatitude'][0]);
		$lat_minutes_a = explode('/', $info['GPSLatitude'][1]);
		$lat_seconds_a = explode('/', $info['GPSLatitude'][2]);
		$lng_degrees_a = explode('/', $info['GPSLongitude'][0]);
		$lng_minutes_a = explode('/', $info['GPSLongitude'][1]);
		$lng_seconds_a = explode('/', $info['GPSLongitude'][2]);

		$lat_degrees = $lat_degrees_a[0] / $lat_degrees_a[1];
		$lat_minutes = $lat_minutes_a[0] / $lat_minutes_a[1];
		$lat_seconds = $lat_seconds_a[0] / $lat_seconds_a[1];
		$lng_degrees = $lng_degrees_a[0] / $lng_degrees_a[1];
		$lng_minutes = $lng_minutes_a[0] / $lng_minutes_a[1];
		$lng_seconds = $lng_seconds_a[0] / $lng_seconds_a[1];

		$lat = (float)$lat_degrees + ((($lat_minutes * 60) + ($lat_seconds)) / 3600);
		$lng = (float)$lng_degrees + ((($lng_minutes * 60) + ($lng_seconds)) / 3600);

		//If the latitude is South, make it negative.
		//If the longitude is west, make it negative
		$GPSLatitudeRef == 's' ? $lat *= -1 : '';
		$GPSLongitudeRef == 'w' ? $lng *= -1 : '';
		return array(
			'lat' => $lat,
			'lng' => $lng,
		);
	}
	return false;
}

function deprecated_controller() {
	if (!is_bot())
		reportAdmin("AS: deprecated controller called", "request_uri={$_SERVER["REQUEST_URI"]}");
	die;
}

//END
