<?php
/*
if ($_SERVER["REMOTE_ADDR"] != "24.200.148.88") {
$site = "AdultSearch.com";
echo "<html><head><title>{$site} - Maintenance break</title></head><body style=\"border: 10px solid #296FA5; padding: 10px;\" >
<h1>Thank you for visiting. I am currently undergoing my annual physical checkup. Will be back soon.<h1>
<h2>Your {$site}</h2>
</body></html>";
die;
}
*/

if (array_key_exists("REMOTE_ADDR", $_SERVER) && ($_SERVER["REMOTE_ADDR"] == "179.43.134.204"))
	die;

if( isset($_SERVER["HTTP_HOST"]) && strstr($_SERVER["HTTP_HOST"], "adultsearch.com") ) ini_set("session.cookie_domain",".adultsearch.com");
ob_start();
ini_set('session.cookie_httponly', 1);
$ret  = session_start();
define( '_CMS_FRONTEND', 1);

require_once('./inc/config.php');
require_once('./inc/common.php');
require_once('./inc/common-frontend.php');

// PROCESSING ACTION
if (isset($_REQUEST["action"]))
	$GLOBALS["gAction"] = GetRequestParam("action");

global $mobile, $config_image_server, $gTitle, $gDescription, $gCanonicalUrl, $ctx, $gIndexTemplate, $advertise, $config_site_url;

$a = $ctx->getGeoLocation();

warning::set();

// NEW URL PROCESSING
$url_successfully_processed = false;
$url_successfully_processed = $url->process_url();
if (!$url_successfully_processed)
	system::moved($config_site_url);

//export navlink advertising campaigns directly into smarty template
$advertise->navlinks_as();

$smarty->assign('img_server', $config_image_server);
$smarty->assign('filename', $gFilename);

if ($gIncFilename != '') {
	$smarty->assign('content', $gContent.parse_php_file($gIncFilename));
}

//-----------------------------------
//choose which layout template to use
if ((isset($_REQUEST["template"])) && (file_exists("./templates/".$_REQUEST["template"])) && !strstr($_REQUEST["template"], "..") ) {
	//override from URL
	$gIndexTemplate = $_REQUEST["template"];
} else if (empty($gIndexTemplate)) {
	//if not set by controller, use default template index.tpl
	$gIndexTemplate = "index.tpl";
}
//check if index template file exists
if (!is_file(_CMS_ABS_PATH."/templates/{$gIndexTemplate}")) {
	//it doesnt, lets check if default index template file exists
	if (!file_exists(_CMS_ABS_PATH."/templates/index.tpl")) {
		//should never happen
		echo "Default template does not exists !<br />\n";
		die();
	}
	//template file does not exists, lets use default one
	$gIndexTemplate = "index.tpl";
}
if (isset($_REQUEST["templates"])) {
	if ($_REQUEST["templates"]) {
		$_SESSION["templates"] = $_REQUEST["templates"];
	} else {
		unset($_SESSION["templates"]);
	}
}

require_once(_CMS_ABS_PATH."/inc/classes/class.navigation.php");
$navigation = new navigation;
$navigation->quickcategory();

/* assume that, $gTitle has been always set */
if (($gDescription == ""))
	$gDescription = $gTitle;

$smarty->assign('html_head_include', $gHtmlHeadInclude);
$smarty->assign('html_bottom_include', $gHtmlBottomInclude);
$smarty->assign('title', $gTitle);
$smarty->assign('keywords', $gKeywords);
$smarty->assign('description', $gDescription);
$smarty->assign('canonical_url', $gCanonicalUrl);

//jay
if ($ctx->page_type != NULL) {
	require_once(_CMS_ABS_PATH."/inc/classes/class.dir.php");
	if (($breadcrumb = dir::getBreadcrumb()) !== FALSE)
		$smarty->assign("breadcrumb", $breadcrumb);
}	
global $gBreadPlace, $gBreadCatId, $gBreadCatName, $gBreadCat;
if (!empty($gBreadPlace))
	$smarty->assign('breadplace', htmlspecialchars($gBreadPlace));

if( empty($gBreadCat) && $gModule && !strstr($account->core_loc_array['loc_url'], strtolower($gModule)) && isset($account->core_loc_array['loc_url']) ) {
	$cat_name = ucwords(str_replace("-", " ", $gModule));
	$url = $account->core_loc_type == 1 ? "/{$gModule}/" : "{$account->core_loc_array['loc_url']}{$gModule}/";
	$smarty->assign("breadcat", "<a href='$url' title='$cat_name'>$cat_name</a>");
} else if (!empty($gBreadCatId) && !empty($gBreadCatName)) {
	$gBreadCatName = trim($gBreadCatName);
	if (substr($gBreadCatName, -1) != "s")
		$gBreadCatName .= "s";
	$smarty->assign("breadcat", "<a href='{$account->core_loc_array['loc_url']}?type=".$gBreadCatId."/' title='$gBreadCatName'>$gBreadCatName</a>");
}
$smarty->assign("request_uri", rawurlencode($_SERVER["REQUEST_URI"]));

//jay flash messages
$smarty->assign("flash_msg", flash::getDisplayHtml());
$smarty->assign("location_name", $ctx->location_name);


//for admin, write number of unread emails into breadcrumb
//if ($account->isAdmin()) {
if (permission::has("mail_manage")) {
	$smarty->assign("isadmin", true);
	$res = $db->q("select count(*) from contact where `read` = 0 and (assigned = 0 or assigned = '{$_SESSION['account']}')");
	$row = $db->r($res);
	$smarty->assign("newemail", $row[0]);
	if (permission::has("classified_manage")) {
		$pecl_count = $db->single("SELECT count(*) as cnt FROM classifieds WHERE deleted IS NULL AND done = 3");
    	$smarty->assign("pecl_count", $pecl_count);
	}
}



if ($account->core_host == "dev.adultsearch.com")
	$smarty->assign("devserver", true);

if ($account->isrealadmin()) {
	$smarty->assign("sqls", $db->sqls);
}

//for admins, display some debug info in footer
if( isset($_SESSION["account_level"]) && $_SESSION["account_level"] == 3 ) {
	$page_load_time = get_page_load_time();
	$smarty->assign('totaltime', $page_load_time);
	$smarty->assign('total_sql', $db->total_sql);
	$smarty->assign('total_time', number_format($db->total_time, 4));
	if (isset($_GET['debug']))
		_darr($db->sqls);
	$smarty->assign('hostname', php_uname("n"));
}

warning::checkAndDisplay();

if ($mobile == 1 && file_exists(_CMS_ABS_PATH."/templates/mobile/mobile_{$gIndexTemplate}")) {
	//automatic use of mobile index template for mobile devices
	$_SERVER["REQUEST_URI"] = preg_replace('@(&amp;|&)?mobile=(1|2)@', '', $_SERVER["REQUEST_URI"]);
	$full_version_link = $_SERVER['REQUEST_URI'];
//	echo "'".$full_version_link."'<br />";
	if ($full_version_link == "/m/location?")
		$full_version_link = "/homepage";
	$full_version_link .= strchr($full_version_link, '?') ? '&mobile=2' : '?mobile=2';
	if (strstr($full_version_link, '//'))
		$full_version_link = str_replace('//', '/', $full_version_link);
	$smarty->assign('full_version_link', $full_version_link);
//	if (array_key_exists("REMOTE_ADDR", $_SERVER) && ($_SERVER["REMOTE_ADDR"] == "66.130.199.89" || $_SERVER["REMOTE_ADDR"] == "89.102.25.94"))
//		$smarty->display(_CMS_ABS_PATH."/templates/mobile/mobile_{$gIndexTemplate}.new");
//	else
	if (array_key_exists("templates", $_SESSION) && ($_SESSION["templates"] == "jay") && file_exists(_CMS_ABS_PATH."/templates/mobile/mobile_{$gIndexTemplate}.jay"))
		$smarty->display(_CMS_ABS_PATH."/templates/mobile/mobile_{$gIndexTemplate}.jay");
	else
		$smarty->display(_CMS_ABS_PATH."/templates/mobile/mobile_{$gIndexTemplate}");
} else {
	if( $mobile == 2 ) {
		$_SERVER["REQUEST_URI"] = preg_replace('@(&amp;|&)?mobile=(1|2)@', '', $_SERVER["REQUEST_URI"]);
		$mobile_text = $_SERVER['REQUEST_URI'] . (strchr($_SERVER['REQUEST_URI'], '?') ? '&mobile=1' : '?mobile=1');
		$mobile_text = preg_replace("/\/+/", "/", $mobile_text);
		$smarty->assign("mobile_text", $mobile_text);
	}
	if (array_key_exists("templates", $_SESSION) && ($_SESSION["templates"] == "jay") && file_exists(_CMS_ABS_PATH."/templates/{$gIndexTemplate}.jay"))
		$smarty->display(_CMS_ABS_PATH."/templates/{$gIndexTemplate}.jay");
	else
		$smarty->display(_CMS_ABS_PATH."/templates/".$gIndexTemplate);
}

?>
