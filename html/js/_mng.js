var timeout    = 5000;
var closetimer = 0;
var ddmenuitem = 0;

function jsddm_open()
{  jsddm_canceltimer();
   ddmenuitem = $(this).find('ul').css('visibility', 'visible');}

function jsddm_close()
{  if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}

function jsddm_timer()
{  closetimer = window.setTimeout(jsddm_close, timeout);}

function jsddm_canceltimer()
{  if(closetimer)
   {  window.clearTimeout(closetimer);
      closetimer = null;}}

$(document).ready(function()
{  $('#jsddm li').bind('mouseover', jsddm_open)
});

document.onclick = jsddm_close;

var mng_first = false;
function _checkContact() {
	if( mng_first === false ) mng_interval = 5000; 
	else mng_interval = 10000;
	setInterval(_checkContactCount, mng_interval);
}
function _checkContactCount() {
	var m = new $.ajax("/mng/?count=true", {
		method: 'get', 
		success:function(t) { 
			if( t == -1 ) return;
			$("#mng_contact").html(t + ' Email').fadeIn('slow');
		}
	});
}

jQuery(document).ready(function(){_checkContact()});
