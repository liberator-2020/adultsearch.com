/* 
 * used in /account/verify
 */

$(document).ready(function () {
    // init  phone
	options = {
		allowDropdown: true,
		nationalMode: true,
		preferredCountries: ["us","ca","gb"],
		separateDialCode: true,
		utilsScript: "/js/intl-tel-input/js/utils.js"
	};

	$('#phone').intlTelInput(options);

    $('#phone').on("countrychange", function() {
		updatePhoneHint();
    });
    
    $('#form1').on('submit', function() {
		return submit1(); 
	});

	updatePhoneHint();
});

function updatePhoneHint() {
	var cntry = $("#phone").intlTelInput("getSelectedCountryData");
	if (!cntry['iso2']) {
		$('#sms_phone_hint').html('Choose a country');
    }
    var msg;
    switch(cntry['iso2']) {
    	case 'us':
        	msg = 'Enter a 10-digit U.S. phone number<br />(With area code, without leading zero)';
            break;
        case 'ca':
            msg = 'Enter a 10-digit Canada phone number<br />(With area code, without leading zero)';
            break;
        default:
            msg = 'Enter local phone number in ';
            if(cntry['name']) {
                msg = msg + cntry['name'] ;
            } else {
                msg = msg + ' your country';
            }
    }
    $('#sms_phone_hint').html(msg);
}

function submit1() {
    $('#phone').val( $('#phone').intlTelInput("getNumber") );
    return true;    
}

// end of verify.js
