function toggle(divId) {
	var divArray = document.getElementsByTagName("div");
	for(i = 0; i < divArray.length; i++){
		if(divArray[i].id == divId){
			if(divArray[i].style.display != 'none'){
				divArray[i].style.display = 'none';
			}else{
				divArray[i].style.display = '';
			}
		}
	}
}

$(function() {
	$( "#visiting_from" ).datepicker({
		showOn: "button",
		buttonImage: "/images/adbuild/calendar.png",
		buttonImageOnly: true,
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 1,
		onSelect: function( selectedDate ) {
			$( "#visiting_to" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#visiting_to" ).datepicker({
		showOn: "button",
		buttonImage: "/images/adbuild/calendar.png",
		buttonImageOnly: true,
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 1,
		onSelect: function( selectedDate ) {
			$( "#visiting_from" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});	


$(document).ready(function(){	
	//default usage
	//$("#message1").charCount();
	//custom usage
	$("#message2").charCount({
		allowed: 70,		
		warning: 20,
		counterText: ''	
	});
	$("#message3").charCount({
		allowed: 30,		
		warning: 10,
		counterText: ''	
	});
	$("#locationdeet").charCount({
		allowed: 25,		
		warning: 10,
		counterText: ''	
	});
});


function autotab(original,destination){
if (original.getAttribute&&original.value.length==original.getAttribute("maxlength"))
destination.focus()
}
/*<![CDATA[*/
function NewEmail(sel,id,nu){
 document.getElementById(id).style.display=sel.selectedIndex==nu?'block':'none';
}
/*]]>*/

 
(function($) {

	$.fn.charCount = function(options){
	  
		// default configuration properties
		var defaults = {	
			allowed: 1500,		
			warning: 25,
			css: 'counter',
			counterElement: 'span',
			cssWarning: 'warning',
			cssExceeded: 'exceeded',
			counterText: ''
		}; 
			
		var options = $.extend(defaults, options); 
		
		function calculate(obj){
			var count = $(obj).val().length;
			var available = options.allowed - count;
			if(available <= options.warning && available >= 0){
				$(obj).next().addClass(options.cssWarning);
			} else {
				$(obj).next().removeClass(options.cssWarning);
			}
			if(available < 0){
				$(obj).next().addClass(options.cssExceeded);
			} else {
				$(obj).next().removeClass(options.cssExceeded);
			}
			$(obj).next().html(options.counterText + available);
		};
				
		this.each(function() {  			
			$(this).after('<'+ options.counterElement +' class="' + options.css + '">'+ options.counterText +'</'+ options.counterElement +'>');
			calculate(this);
			$(this).keyup(function(){calculate(this)});
			$(this).change(function(){calculate(this)});
		});
	  
	};

})(jQuery);

 function removeloc(loc,ad) {
	var val = "removeloc=" + loc + "&ad_id=" + ad;
	 jQuery.post("/adbuild/loc", val, function(data){ jQuery("#calculator").html(data); }, "json");
	 $("input.pickloc[value="+loc+"]").attr("checked", false);
 }

function cart_process(type,loc,ad) {
	var val = '';
	if( type == "loc" ) {
		val = "removeloc=" + loc + "&ad_id=" + ad;
		$("input.pickloc[value="+loc+"]").attr("checked", false);
	} else if( type == "cover" ) {
		val = "removecover=" + loc + "&ad_id=" + ad;
		$("input[name=homesp"+loc+"]").attr("checked", false);
	} else if( type == "cancelrepost" ) {
		$("input[name=repost]").attr("checked", false);
		val = "cancelrepost=" + loc + "&ad_id=" + ad;
	} else if( type == 'coupon' ) {
		val = "cancelcoupon=1&ad_id=" + ad;
		$("#promo").val("");
	} else if (type == 'side') {
		val = "removeside=" + loc + "&ad_id=" + ad;
		$('input[name=side_sponsor_'+loc+']').attr("checked", false);
	} else if (type == 'sticky') {
		val = "removesticky=" + loc + "&ad_id=" + ad;
		$('input[name=sticky_sponsor_'+loc+']').attr("checked", false);
	}
	if (val != '')
		jQuery.post("/adbuild/loc", val, function(data){ jQuery("#calculator").html(data); }, "json");
}
