// config
var config_cms_site_url = 'http://adultsearch.com';
var config_cms_folder_name = 'cms';


// Function adds event handler for event 'evType' on element 'elm'
// Event handler function is 'fn'
// 'useCapture' is usually false
function addEvent(elm, evType, fn, useCapture) {
	if (elm.addEventListener) {
		elm.addEventListener(evType, fn, useCapture);
		return true;
	}
	else if (elm.attachEvent) {
		var r = elm.attachEvent('on' + evType, fn);
		return r;
	}
	else {
		elm['on' + evType] = fn;
	}
}

function AddLoadEvent(func) {
	addEvent(window, 'load', func, false);
}

// Function creates filename from name
function createFilenameFromName(form_name, name_input_name, filename_input_name) {
	var fnam = eval('document.'+form_name+'.'+filename_input_name);
	var nam = eval('document.'+form_name+'.'+name_input_name);
	if (fnam.value.length == 0) {
		var fname = nam.value.replace(/ /g, "-");
		fname = fname.replace(/&/g, "And");
		fname = fname.replace(/\?/g, "");
		fname = fname.replace(/\'/g, "");
		fname = fname.replace(/\./g, "");
		fname = fname.replace(/,/g, "");
		fname = fname.replace(/\"/g, "");
		fnam.value = fname;
	}
}

// Function return array of elements with tag 'tag' which have class 'searchClass' under node 'node'
function getElementsByClass(searchClass, node, tag) {
	var classElements = new Array();
	if ( node == null )
		node = document;
	if ( tag == null )
		tag = '*';
	var els = node.getElementsByTagName(tag);
	var elsLen = els.length;
	var pattern = new RegExp('(^|\\s)'+searchClass+'(\\s|$)');
	for (i = 0, j = 0; i < elsLen; i++) {
		if ( pattern.test(els[i].className) ) {
			classElements[j] = els[i];
			j++;
		}
	}
	return classElements;
}


//function logs string to console
function LogToConsole(str) {
	if (window.console) {
		//Firebug debugging console for Firefox
		console.log(str);
	}
}


// Javascript version of PHP in_array function
//Array.prototype.in_array = function(search_term) {
//	var i = this.length;
//	if (i > 0) {
//		do {
//			if (this[i] === search_term)
//				return true;
//		} while (i--);
//	}
//	return false;
//}

// get index of item
//Array.prototype.get_index = function(search_term) {
//	var len = this.length;
//	for (var i = 0; i < len; i++) {
//		if (this[i] === search_term)
//			return i;
//	}
//	return null;
//}

// Javascript version of PHP in_array function
function in_array(search_array, search_term) {
	var i = search_array.length;
	if (i > 0) {
		do {
			if (search_array[i] === search_term)
				return true;
		} while (i--);
	}
	return false;
}

// get index of item
function get_index(search_array, search_term) {
	var len = search_array.length;
	for (var i = 0; i < len; i++) {
		if (search_array[i] === search_term)
			return i;
	}
	return null;
}

// ------------
// --- AJAX ---
// ------------

// Function creates and returns XMLHTTP Request object (AJAX)
function createRequestObject() {
	var ro;
	var browser = navigator.appName;
	if(browser == "Microsoft Internet Explorer") {
		ro = new ActiveXObject("Microsoft.XMLHTTP");
	} else {
		ro = new XMLHttpRequest();
	}
	return ro;
}


// ---------------------------------------------------------------
// univesal AJAX send_request and handle_response global functions
var http = createRequestObject();

function sndReq(url) {
	http.open('GET', url);
	http.onreadystatechange = handleResponse;
	http.send(null);
}

function handleResponse() {
	if(http.readyState == 4){
		var response = http.responseText;
		var update = new Array();
		var commands = new Array();
		commands=response.split('%%');
		
		for (key in commands) {
	          	if ((typeof(commands[key]) == 'string') && (commands[key].indexOf('|') != -1)) {
        	  		update = commands[key].split('|');
				if (update[0]!='javascript') {
					var elem = document.getElementById(update[0]);
					if (elem == null) {
						//alert('Non-existent element !');
					} else {
						elem.innerHTML = update[1];
					}
				} else {
					eval(update[1]);
				}
			}
		}
	}
}



function ajaxFilenameExists() {
	var http1 = '';
	var form_name = '';
	var filename_input = '';
	var result_element_id = '';
	
	this.init = function (fn, fi, rei) {
		http1 = createRequestObject();
		form_name = fn;
		filename_input = fi;
		result_element_id = rei;
	}
	this.sndReq = function () {
		var uri = '/'+config_cms_folder_name+'/ajax/getsubcat.php?action=filename_exists';
		uri += '&filename='+encodeURI(eval('document.forms[\''+form_name+'\'].'+filename_input).value);

		http1.open('get', uri);
		http1.onreadystatechange = this.handleResponse;
		http1.send(null);
	}
	this.handleResponse = function () {
		if(http1.readyState == 4) {
			var elem = document.getElementById(result_element_id);
			var response = http1.responseText;
			if(response == "1") {
				elem.innerHTML = '<span style="font-weight: bold; color: red;">This filename is occupied ! Please choose another filename.</span>'; 
			} else {
				elem.innerHTML = 'Filename OK'; 
			}
		}
	}
	this.checkFilename = function () {
		var elem = document.getElementById(result_element_id);
		elem.innerHTML = '';
		this.sndReq();
	}
}

function ajaxStoreClick() {
	var http1 = '';

	this.init = function () {
		http1 = createRequestObject();
	}

	this.sndReq = function (id, type) {
		var uri = '/'+config_cms_folder_name+'/ajax/getsubcat.php?action=ad_click&type=' + encodeURI(type) + '&id=' + encodeURI(id);
		http1.open('get', uri);
		http1.onreadystatechange = this.handleResponse;
		http1.send(null);
		//alert('URI='+uri);
	}

	this.handleResponse = function () {}
}

function ajaxCategoryHierarchyCombo() {
	var http1 = '';
	var http2 = '';
	var which = '';
	var form_name = 'Form1';
	var select_list_name = '';
	var info_id = 'where_info';
	var value_input_name = '';

	this.init = function (wh, fn, sln, ii, vin) {
		http1 = createRequestObject();
		http2 = createRequestObject();
		which = wh;
		form_name = fn;
		select_list_name = sln;
		info_id = ii;
		value_input_name = vin;
	}

	this.sndReq = function (cat) {
		http1.open('get', '/'+config_cms_folder_name+'/ajax/getsubcat.php?action=getsubcat&cat='+cat);
		http1.onreadystatechange = this.handleResponse;
		http1.send(null);

		http2.open('get', '/'+config_cms_folder_name+'/ajax/getsubcat.php?action=breadcrumb_links&cat='+cat+'&which='+which);
		http2.onreadystatechange = this.handleResponse2;
		http2.send(null);
	}

	this.handleResponse = function () {
		//alert('handle-response, state='+http1.readyState);
		var select_list = eval('document.forms[\''+form_name+'\'].'+select_list_name);
		if(http1.readyState == 4) {
			var response = http1.responseText;
			var opts = new Array();
			var temp = new Array();

			//alert(response);

			if(response.indexOf('|' != -1)) {
				//empty select
				for (var i = select_list.options.length; i > 0; i--) {
					select_list.options[i] = null;
				}
		
		    		//fill select with options
				select_list.options[0] = new Option('-- Choose subcategory --', '');
				opts = response.split('$');
				for (var i = 1; i < opts.length; i++) {
					temp = opts[i-1].split('|');
					//alert('i='+i+'    name='+temp[1]);
					select_list.options[i] = new Option(temp[1], temp[0]);
				}
			}
		}
	}

	this.handleResponse2 = function () {
		//alert('handle-response2, state='+http2.readyState);
		if (http2.readyState == 4) {
			var element = document.getElementById(info_id);
			var response = http2.responseText;
			element.innerHTML = response;
		}
	}

	this.change = function (cat) {
		if (cat == '')
			return false;
		this.sndReq(cat);
		var value_input = eval('document.forms[\''+form_name+'\'].'+value_input_name);
		value_input.value = cat;
		
		var name_input = eval('document.forms[\''+form_name+'\'].'+value_input_name+'_name');
		value_input.value = cat;
	}

	this.change2 = function (elem) {
		var sind = elem.selectedIndex;
		var cat = elem.value;
		if (cat == '')
			return false;
		var value_input = eval('document.forms[\''+form_name+'\'].'+value_input_name);
		value_input.value = cat;
		
		var name_input = eval('document.forms[\''+form_name+'\'].'+value_input_name+'_name');
		name_input.value = elem.options[sind].text;
		
		this.sndReq(cat);
	}
}


function ajaxScCategoryHierarchyCombo() {
	var http1 = '';
	var http2 = '';
	var which = '';
	var form_name = 'Form1';
	var select_list_name = '';
	var info_id = 'where_info';
	var value_input_name = '';
	var name_input_name = '';

	this.init = function (wh, fn, sln, ii, vin, nin) {
		http1 = createRequestObject();
		http2 = createRequestObject();
		which = wh;
		form_name = fn;
		select_list_name = sln;
		info_id = ii;
		value_input_name = vin;
		name_input_name = nin;
	}

	this.sndReq = function (cat) {
		//alert('cat='+cat);
		http1.open('get', '/'+config_cms_folder_name+'/ajax/getsubcat.php?action=sc_getsubcat&sc_cat='+cat);
		http1.onreadystatechange = this.handleResponse;
		http1.send(null);

		http2.open('get', '/'+config_cms_folder_name+'/ajax/getsubcat.php?action=sc_breadcrumb_links&sc_cat='+cat+'&which='+which);
		http2.onreadystatechange = this.handleResponse2;
		http2.send(null);
	}

	this.handleResponse = function () {
		//alert('handle-response, state='+http1.readyState);
		var select_list = eval('document.forms[\''+form_name+'\'].'+select_list_name);
		if(http1.readyState == 4) {
			var response = http1.responseText;
			var opts = new Array();
			var temp = new Array();

			//alert(response);

			if(response.indexOf('|' != -1)) {
				//empty select
				for (var i = select_list.options.length; i > 0; i--) {
					select_list.options[i] = null;
				}
		
		    		//fill select with options
				select_list.options[0] = new Option('-- Choose subcategory --', '');
				opts = response.split('$');
				for (var i = 1; i < opts.length; i++) {
					temp = opts[i-1].split('|');
					if (i==1) {
						//this is name of parent category
						//alert('parent_name = ' + temp[1]);
						var value_input = eval('document.forms[\''+form_name+'\'].'+name_input_name);
						value_input.value = temp[1];
					} else {
						//alert('i='+i+'    name='+temp[1]);
						select_list.options[i-1] = new Option(temp[1], temp[0]);
					}
				}
			}
		}
	}

	this.handleResponse2 = function () {
		//alert('handle-response2, state='+http2.readyState);
		if (http2.readyState == 4) {
			var element = document.getElementById(info_id);
			var response = http2.responseText;
			element.innerHTML = response;
		}
	}

	this.change = function (cat) {
		if (cat == '')
			return false;
		this.sndReq(cat);
		var value_input = eval('document.forms[\''+form_name+'\'].'+value_input_name);
		value_input.value = cat;
	}
}

function ajaxScCategoryParents() {
	var http1 = '';

	this.sndReq = function (cat) {
		http1 = createRequestObject();
		var uri = '/'+config_cms_folder_name+'/ajax/getsubcat.php?action=sc_parents&sc_cat='+cat;
		//alert('uri=\''+uri+'\'');
		http1.open('get', uri);
		http1.onreadystatechange = this.handleResponse;
		http1.send(null);
	}

	this.handleResponse = function () {
		//alert('handle-response, state='+http1.readyState);
		if(http1.readyState == 4) {
			var response = http1.responseText;
			var opts = new Array();
			var temp = new Array();
			//alert('Response='+response);
			if(response.indexOf('|' != -1)) {
				opts = response.split('$');
				cat_parents = new Array(opts.length - 1);
				for (var i = 1; i < opts.length; i++) {
					temp = opts[i-1].split('|');
					//alert('Parent: id='+temp[0]+' name='+temp[1]);
					cat_parents[i-1] = new Array(2);
					cat_parents[i-1][0] = temp[0];
					cat_parents[i-1][1] = temp[1];
				}
				GenerateHiddenFields();
			}
		}
	}
}

function ajaxAlphabetCombo() {
	var http1 = '';
	var which = '';
	var form_name = '';
	var select_list_name = '';
	var value_input_name = '';
	var letter = '';
	var chosen_id = '';

	this.init = function (wh, fn, sln, vin) {
		//alert('ajaxAlphabetCombo:init()');
		http1 = createRequestObject();
		which = wh;
		form_name = fn;
		select_list_name = sln;
		value_input_name = vin;
	}

	this.sndReq = function (let) {
		//alert('sned req letter = '+let);
		if (which == 'company') {
			http1.open('get', '/'+config_cms_folder_name+'/ajax/getsubcat.php?action=getcompletter&letter='+let);
		} else if (which == 'filename') {
			http1.open('get', '/'+config_cms_folder_name+'/ajax/getsubcat.php?action=getfilletter&letter='+let);
		} else if (which == 'filename_str') {
			http1.open('get', '/'+config_cms_folder_name+'/ajax/getsubcat.php?action=getfilstrletter&letter='+let);
		} else {
			http1.open('get', '/'+config_cms_folder_name+'/ajax/getsubcat.php?action=getlocletter&letter='+let);
		}
		http1.onreadystatechange = this.handleResponse;
		http1.send(null);
	}

	this.handleResponse = function () {
		//alert('handle-response, letter='+letter+'  state='+http1.readyState);
		var select_list = eval('document.forms[\''+form_name+'\'].'+select_list_name);
		if(http1.readyState == 4) {
			var response = http1.responseText;
			var opts = new Array();
			var temp = new Array();

			//alert(response);

			if(response.indexOf('~' != -1)) {
				//empty select
				for (var i = select_list.options.length; i > 0; i--) {
					select_list.options[i] = null;
				}
		
		    		//fill select with options
				if (which == 'company') {
					select_list.options[0] = new Option('-- Choose company -- ' + letter, '');
				} else if (which == 'filename') {
					select_list.options[0] = new Option('-- Choose filename -- ' + letter, '');
				} else if (which == 'filename_str') {
					select_list.options[0] = new Option('-- Choose filename -- ' + letter, '');
				} else {
					select_list.options[0] = new Option('-- Choose location -- ' + letter, '');
				}
				opts = response.split('|');
				for (var i = 1; i < opts.length; i++) {
					temp = opts[i-1].split('~');
					//alert('i='+i+'    name='+temp[1]);
					select_list.options[i] = new Option(temp[1], temp[0]);
					if ((chosen_id != '') && (chosen_id == temp[0])) {
						//alert('found !!');
						select_list.options[i].selected = 1;
					}
				}
			}
		}
	}

	this.LetterChosen = function (let, id) {
		if (let == '') {
			var select_list = eval('document.forms[\''+form_name+'\'].'+select_list_name);
			if (which == 'company') {
				select_list.options[0] = new Option('-- Choose company -- ' + letter, '');
			} else if (which == 'filename') {
				select_list.options[0] = new Option('-- Choose filename -- ' + letter, '');
			} else {
				select_list.options[0] = new Option('-- Choose location -- ' + letter, '');
			}
		} else {
			letter = let;
			chosen_id = id;
			//alert('Letter chosen: '+let+'  id='+id);
			this.sndReq(let);
		}
	}

	this.optionChosen = function (id) {
		//alert('Option chosen');
		//document.forms[form_name].eval(value_input_name).value = id;
		var value_input = eval('document.forms[\''+form_name+'\'].'+value_input_name);
		value_input.value = id;
	}
}


function ajaxGetCoord() {
	var http1 = '';
	var form_name = '';
	var address_name = '';
	var city_name = '';
	var state_name = '';
	var zip_name = '';
	var latitude_name = '';
	var longitude_name = '';
	
	this.init = function (fn, ad, ci, st, zi, la, lo) {
		//alert('ajaxGetCoord:init()');
		http1 = createRequestObject();
		form_name = fn;
		address_name = ad;
		city_name = ci;
		state_name = st;
		zip_name = zi;
		latitude_name = la;
		longitude_name = lo;
	}

	this.sndReq = function () {
		var form = eval('document.forms[\''+form_name+'\']');
		var uri = '/'+config_cms_folder_name+'/ajax/getsubcat.php?action=getcoord';
		uri += '&address='+encodeURI(eval('document.forms[\''+form_name+'\'][\'' + address_name + '\'].value'));
		uri += '&city='+encodeURI(eval('document.forms[\''+form_name+'\'][\'' + city_name + '\'].value'));
		uri += '&state='+encodeURI(eval('document.forms[\''+form_name+'\'][\'' + state_name + '\'].value'));
		uri += '&zip='+encodeURI(eval('document.forms[\''+form_name+'\'][\'' + zip_name + '\'].value'));

		//alert('get: uri='+uri);
		http1.open('get', uri);
		http1.onreadystatechange = this.handleResponse;
		http1.send(null);
	}

	this.handleResponse = function () {
		//alert('handle-response, state='+http1.readyState);
		var form = document.forms[form_name];
		if(http1.readyState == 4) {
			var response = http1.responseText;
			//alert(response);
			var temp = new Array();

			if(response.indexOf('|' != -1)) {
				temp = response.split('|');
				if (temp[0] == "error") {
					alert('Yahoo API Error: \''+temp[1]+'\'');
				} else {
					eval('document.forms[\''+form_name+'\'][\'' + latitude_name + '\']').value = temp[0];
					eval('document.forms[\''+form_name+'\'][\'' + longitude_name + '\']').value = temp[1];
					eval('document.forms[\''+form_name+'\'][\'' + city_name + '\']').value = temp[2];
					eval('document.forms[\''+form_name+'\'][\'' + zip_name + '\']').value = temp[3];
				}
			}
		}
	}

	this.getcoord = function () {
		//alert('Getcoord');
		this.sndReq();
	}
}

function ajaxGetFillCompanyAddress() {
	var http1 = '';
	var form_name = '';
	var address1_name = '';
	var address2_name = '';
	var city_name = '';
	var state_name = '';
	var zip_name = '';
	
	this.init = function (fn, ad1, ad2, ci, st, zi) {
		http1 = createRequestObject();
		form_name = fn;
		address1_name = ad1;
		address2_name = ad2;
		city_name = ci;
		state_name = st;
		zip_name = zi;
	}

	this.sndReq = function (id_company) {
		var form = eval('document.forms[\''+form_name+'\']');
		var uri = '/'+config_cms_folder_name+'/ajax/getsubcat.php?action=company_address';
		uri += '&id_company='+encodeURI(id_company);

		http1.open('get', uri);
		http1.onreadystatechange = this.handleResponse;
		http1.send(null);
	}

	this.handleResponse = function () {
		//alert('handle-response, state='+http1.readyState);
		var form = document.forms[form_name];
		if(http1.readyState == 4) {
			var response = http1.responseText;
			//alert(response);
			var temp = new Array();

			if(response.indexOf('|' != -1)) {
				temp = response.split('|');
				eval('document.forms[\''+form_name+'\'][\'' + address1_name + '\']').value = temp[0];
				eval('document.forms[\''+form_name+'\'][\'' + address2_name + '\']').value = temp[1];
				eval('document.forms[\''+form_name+'\'][\'' + city_name + '\']').value = temp[2];
				eval('document.forms[\''+form_name+'\'][\'' + state_name + '\']').value = temp[3];
				eval('document.forms[\''+form_name+'\'][\'' + zip_name + '\']').value = temp[4];
				agc.getcoord();
			}
		}
	}

	this.getaddress = function (id_company) {
		//alert('Getaddress');
		this.sndReq(id_company);
	}
}


function ajaxGetSimilarCompanies() {
	var http1 = '';
	var form_name = '';
	var company_name = '';
	var info_name = '';
	var reference_uri = '';
	
	this.init = function (fn, cn, inn, ru) {
		http1 = createRequestObject();
		form_name = fn;
		company_name = cn;
		info_name = inn;
		reference_uri = ru;
	}

	this.sndReq = function () {
		var form = eval('document.forms[\''+form_name+'\']');
		var uri = '/'+config_cms_folder_name+'/ajax/getsubcat.php?action=similar_companies';
		uri += '&company_name='+encodeURI(eval('document.forms[\''+form_name+'\'][\'' + company_name + '\'].value'));

		http1.open('get', uri);
		http1.onreadystatechange = this.handleResponse;
		http1.send(null);
	}

	this.handleResponse = function () {
		//alert('handle-response, state='+http1.readyState);
		var form = document.forms[form_name];
		if(http1.readyState == 4) {
			var response = http1.responseText;
			//alert('Response='+response);
			var comps = new Array();
			var temp = new Array();
			var warning_text = '';

			if(response.indexOf('|' != -1)) {
				comps = response.split('|');
				var i = 0;
				for (i = 1; i < comps.length; i++) {
					temp = comps[i-1].split('~');
					if (warning_text == '') {
						warning_text += '<strong>These companies with similar name already exists in DB.<br />\nWould you want to edit one of them instead?</strong><br />\n<ul>\n';
					}
					warning_text += '<li><a href="'+reference_uri+'com='+temp[0]+'&fill=yes" title="edit '+temp[1]+'">'+temp[1]+'</a></li>\n';
				}
				if (i > 0) {
					warning_text += '</ul>\n';
					var element = document.getElementById(info_name);
					element.innerHTML = warning_text;
					element.style.visibility = "visible";
				}
			}
		}
	}

	this.checkSimilarCompanies = function () {
		//alert('GetSimilarCompanies');
		this.sndReq();
	}
}


// ------------------
// --- POPUP MENU ---
// ------------------

var timerID = new Array();		//used for storing timer IDs for individual menu levels
var timerElement = new Array();		//used for storing menu div elements for individual levels
var timerIframeElement = new Array();	//used for storing iframe elements for each menu div element (MSIE select box bug fix)


//returns name of iframe element 
function GetHoverIframeId(div_id) {
	var iframe_id = div_id + '_hvrifrm';
	return iframe_id;
}

//this function inserts iframe element after drawing of each menu floating div
function DrawHoverIframe(elem, level) {
	var iframe_id = GetHoverIframeId(elem.id);
	elem.insertAdjacentHTML("afterEnd", '<iframe style="display: hidden; position: absolute; z-index:4;" src="javascript:\'&lt;html&gt;&lt;/html&gt;\';" frameBorder="0" scrolling="no" id="'+iframe_id+'" />');
	var iframe_elem = document.getElementById(iframe_id);
	iframe_elem.style.top	= findPosY(elem) + 'px';
	iframe_elem.style.left	= findPosX(elem) + 'px';
	iframe_elem.style.width = elem.offsetWidth;
	iframe_elem.style.height= elem.offsetHeight;
	timerIframeElement[level] = iframe_elem;
}

//function returns x position of element relatively to browser window
function findPosX(obj) {
	var curleft = 0;
	if (obj.offsetParent) {
		while (obj.offsetParent) {
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;
	return curleft;
}

//function returns y position of element relatively to browser window
function findPosY(obj) {
	var curtop = 0;
	var printstring = '';
	if (obj.offsetParent) {
		while (obj.offsetParent) {
			printstring += ' element ' + obj.tagName + ' has ' + obj.offsetTop;
			curtop += obj.offsetTop;
			if (document.all)
				curtop += obj.clientTop;
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
		curtop += obj.y;
	return curtop;
}

function HideMenuElement(level) {
	if (timerElement[level] != null) {
		timerElement[level].style.visibility="hidden";
	}
	if (document.all) {
		if (timerIframeElement[level] != null) {
			var parent = timerIframeElement[level].parentNode;
			parent.removeChild(timerIframeElement[level]);
			timerIframeElement[level] = null;
		}
	}
}

function HideMenuElements(level) {
	for (i = level; i < 7; i++) {
		HideMenuElement(i);
	}
}

function TimerHandler(level) {
	HideMenuElement(level);
	timerID[level] = 0;
}

function ClearTimeouts(level) {
	for (i = level; i >= 0; i--) {
		if (timerID[i] != 0) {
			clearTimeout(timerID[i]);
			timerID[i] = 0;
		}
	}
}

function SetTimeouts(level) {
	for (i = level; i >= 0; i--) {
		if (timerID[level] == 0) {
			timerID[i] = setTimeout("TimerHandler("+i+")", 500);
		} else {
			clearTimeout(timerID[i]);
			timerID[i] = setTimeout("TimerHandler("+i+")", 500);
		}
	}
}

function showIdOffset(obj, element, xoffset, yoffset, level) {

	if (timerID[level] != 0) {
		ClearTimeouts(level);
		HideMenuElements(level);
	}

	var elem = document.getElementById(element);
	if (elem != null) {
		if (level == 0) {
			//menu on level 0 is underneath parent menu link
			elem.style.top = findPosY(obj) + yoffset + 'px';
			elem.style.left = findPosX(obj) + xoffset + 'px';
		} else {
			//menus on level 1 and higher are on the right side of parent menu link
			elem.style.top = findPosY(obj) + 'px';
			elem.style.left = findPosX(obj) + obj.offsetWidth + 'px';
		}
		var wid = elem.offsetWidth;
		elem.style.visibility="visible";
		if (document.all) {
			//LogToConsole('setting width to '+wid+' px');
			elem.style.width = wid + 'px';		//we re-set width of element, f**king  MSIE stretches the element in some weird cases
		}
		
		timerElement[level] = elem;

		if (document.all) {		// This is workaround for nasty nasty select box bug of evil MSIE 6
			DrawHoverIframe(elem, level);
		}
	}
}

function hideSubMenu(level) {
	//we leave menu div at level 'level', set timeouts for all levels equal or lower than 'level'
	SetTimeouts(level);
}

function subMenuOver(level) {
	//we hover over menu div at level 'level', clear all timeouts for all levels equal or lower than 'level'
	ClearTimeouts(level);
}


// End of common.js

