//currency_convert.js

function CurrencyConvertHideConverter() {
	$('#xe_currency_convert').hide();
}

function CurrencyConvertToggleConverter() {
	if ($('#xe_currency_convert').css('display') == 'none') {
		var preset = $.cookie("preferredCurrency");
		if (preset != '') {
			//highlight current setting
			$('#xe_currency_convert li').each(
					function(ind,elem){
						if (this.getAttribute("data-currency") == preset)
							$(this).addClass('currency_preset');
						else
							$(this).removeClass('currency_preset');
					});
		}		
	}
	$('#xe_currency_convert').toggle();
	return false;
}

function CurrencyConvertChangeCurrency(li_elem) {
	var currency_to = li_elem.getAttribute("data-currency");
	//alert('Changing currency to ' + currency_to + '.');
	var par = ''; 
	$('.currency_convertable').each(
			function(ind,elem){
				p = this.id + '|' + this.getAttribute("data-original");
				if (par !== "")
					par += '@';
				par += p;
			});
	par = 'to=' + currency_to + '&what=' + par; 
	$.post("/ajax/currency_convert", par, CurrencyConvertProcess, 'text');  
}

function CurrencyConvertDisable() {
	$.post("/ajax/currency_convert", 'to=DISABLE', function(){}, 'text');
	$('.currency_convertable').each(
			function(ind,elem){
				parts = this.getAttribute("data-original").split('|');
				if (parts.length != 3)
					return;
				$(this).html(parts[2]);
			});
	CurrencyConvertHideConverter();
}

function CurrencyConvertProcess(converted) {
	//alert('received = ' + converted);
	var toks = converted.split('@');
	for(var i in toks) {
	    var parts = toks[i].split('|');
		if (parts.length != 3)
			continue;
		var id = parts[0];
		var amt_display = parts[1];
		var amt_original = parts[2];
		CurrencyConvertSetAmount(id, amt_display, amt_original);
	}
}

function CurrencyConvertSetAmount(id, amt_display, amt_original) {
	if (amt_display != amt_original)
    	var innerHtml = amt_display + ' <span class="currency_original">(' + amt_original + ')</span>';
	else
    	var innerHtml = amt_display;
	$('#' + id).html(innerHtml);
	CurrencyConvertHideConverter();
}
