function _ajax(method, url, data, div) {$.ajax({type: method, url: url, data: data, success: function(msg){ jQuery('#'+div).html(msg); }});}
function _ajaxc(method, url, data, div) {var c = confirm('Are you sure?');if(c){$.ajax({type: method, url: url, data: data, success: function(msg){ jQuery('#'+div).html(msg); }});}}
function _ajaxs(method, url, data) {$.ajax({type: method, url: url, data: data});}
function _ajaxsc(method, url, data) {var c = confirm('Are you sure?');if(c){$.ajax({type: method, url: url, data: data});}}

function _ajaxj(method, url, data) {$.ajax({type: method, url: url, data: data, dataType: "script", success: function(code){ eval(code); }});}
function _ajaxjc(method, url, data) {var c = confirm('Are you sure?');if(c){$.ajax({type: method, url: url, data: data, dataType: "script", success: function(code){ eval(code); }});}}

function warning_agreed() {
	jQuery.post('/warning/agreed');
}

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).ready(function() {
	jQuery("#city").autocomplete({
	source: function(request, response) {
		jQuery.ajax({
		url: "/_ajax/city",
		dataType: "json",
		data: {
			search_string: request.term
		},
		success: function(data) {
			response(jQuery.map(data, function(item) {
			return {
				url: item.url,
				value: item.name
			}
			}))
		}
		})
	},
	create: function() {
		$(this).attr("autocomplete", "off");
	},
	focus: function() {
		$(this).attr("autocomplete", "off");
	},
	search: function() {
		$(this).attr("autocomplete", "off");
	 },
	select: function( event, ui ) {
		window.location.href = ui.item.url;
	},
	minLength: 2
	});
});

function addFilter(filterName,filterValue) {
	var vars = "";
	var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	 
	for(var i = 0; i < hashes.length; i++) {
		hash = hashes[i].split('=');
		if (hash[1]) {
			if (vars == "") { vars = vars + "?"; } else { vars = vars + "&"; }
			vars = vars + hash[0] + "=" + hash[1];
		}
	}

	if (vars == "") { vars = vars + "?"; } else { vars = vars + "&"; }
	vars = vars + filterName + "=" + filterValue;

	urls = document.location.href.split("?");
	document.location.href = urls[0] + vars;
}

// mobile location change toggle

function toggle_visibility(id) {
	var e = document.getElementById(id);
	if(e.style.display == 'block')
		e.style.display = 'none';
	else
		e.style.display = 'block';
}

// mobile refine toggle
function toggleDiv(id1,id2) {
	var tag = document.getElementById(id1).style;
	var tagicon = document.getElementById(id2);

	if(tag.display == "none") {
		tag.display = "block";
		tagicon.innerHTML = "[close]";
	} else {
		tag.display = "none";
		tagicon.innerHTML = "[expand]";
	}
}

function expandAll(cnt) {
	for(var x=1; x<=cnt; x++) {
		document.getElementById('content'+x).style.display="block";
		document.getElementById('icon'+x).innerHTML="[close]";
	}
}

function collapseAll(cnt) {
	for(var x=1; x<=cnt; x++) {
		document.getElementById('content'+x).style.display="none";
		document.getElementById('icon'+x).innerHTML="[expand]";
	}
}

function cookie_set(value,exdays) {
	var cookie_name = "as_mobile_pop";
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var cookie_value=escape(value) + ((exdays===null) ? "" : "; expires="+exdate.toUTCString() + "; path=/" );
	document.cookie=cookie_name + "=" + cookie_value;
}

function cookie_get(){
	var cookie_name = "mrpop";
	var i,x,y,cookie=document.cookie.split(";");
	for (i=0;i<cookie.length;i++) {
		x=cookie[i].substr(0,cookie[i].indexOf("="));
		y=cookie[i].substr(cookie[i].indexOf("=")+1);
		x=x.replace(/^\s+|\s+$/g,"");
		if (x==cookie_name) {
			return unescape(y);
		}
	}
}

function firepop( uri ){
	if (!uri) {
		uri = "http://adultsearch.com";
	}
	if (cookie_get()) {
	} else {
		cookie_set(true, 1);
		document.location.assign( uri );
		return true;
	}

}

/**
 * Popunder script for Mobile devices
 * 1. creates new tab
 * 2. loads original link in new tab
 * 3. loads ad in original tab
 * 4. changes focus to new tab
 *
 * This needs to be set:
 * _popunderSettings	= {
 *	 "url":	 [string],
 * }
 *
 * Example of use:
 * $(document).ready(_PopunderMobile.attach);
 */
var _PopunderMobile = {

	attach:	 function() {
		// Attach click callback to all anchors
		$('body').on('click', 'a', _PopunderMobile.click);
	},

	click:	function(ev) {
		var link	= $(this);
		//only fire popunder on links with defined href and not defined target
		if(!link.attr('target') && link.attr('href') !== 'undefined' && link.attr('href') !== undefined) {
			window.open(link.attr('href'), '');
			setTimeout('document.location.assign("' + _popunderSettings.url + '")', 50);
			ev.preventDefault();
		}
	},

};

/** Interstitial ads display hook **/
function interstitial_init(impression_url) {
    $(window).scroll(function () {
        if ($(window).scrollTop() > $('body').height() / 3) {
            interstitial_show(impression_url);
        }
    });
}
function interstitial_show(impression_url) {
    $('#interstitial_overlay').fadeIn(300);
    $(window).unbind('scroll');
    $('#interstitial_overlay').click(function(e) {
        interstitial_close();
    });
    $('#interstitial_ad').click(function(e) {
        e.stopPropagation();
    });
    $('#interstitial_overlay .close').click(function(e) {
        interstitial_close();
    });
    $('#interstitial_overlay a').click(function(e) {
        interstitial_close();
    });
	$.post(impression_url);
}
function interstitial_close() {
    $('#interstitial_overlay').hide();
}
