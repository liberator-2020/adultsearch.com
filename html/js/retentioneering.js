;(function($) {
if (typeof $ === 'undefined') { console.warn('jQuery not found'); return; }
var CONFIG =
{
  "source": "adultsearch",
  "events": [
    {
      "comment": "Click on link from services panel",
      "name": "services_click",
      "selector": ".services a"
    },
    {
      "comment": "Click on link from Adult Search Metropolitan Areas",
      "name": "select_metropolitan_area",
      "selector": ".roundeddiv2 a"
    },
    {
      "comment": "Click on link from AdultSearch International Top Cities",
      "name": "select_international_top_cities",
      "selector": "#blueLeft a"
    },
    {
      "comment": "Click on link from services panel",
      "name": "select_city_main",
      "selector": ".geoUnit a"
    },
    {
      "comment": "Click on go to top button",
      "name": "anchor_click",
      "selector": "#anchorTop a"
    },
    {
      "comment": "Click on escorts: post button",
      "name": "post_add",
      "selector": ".postTitle"
    },
	{
      "comment": "Click on escorts POST ESCORT AD button",
      "name": "post_add_services",
      "selector": ".post_btn"
    },
    {
      "comment": "Click on escorts post button at main screen",
      "name": "post_add_main",
      "selector": "#post"
    },
    {
      "comment": "Click on offer",
      "name": "select_offer",
      "selector": ".Listing"
    },
    {
      "comment": "Change view to list",
      "name": "change_view_list",
      "selector": ".viewRow #view-list"
    },
    {
      "comment": "Change view to photo",
      "name": "change_view_photo",
      "selector": ".viewRow #view-photo"
    },
    {
      "comment": "Change number of listed offers",
      "name": "change_view_page_limit",
      "selector": ".viewRow #ipp"
    },
    {
      "comment": "Remove filter",
      "name": "filter_remove",
      "selector": "#ListLeft #refineResults"
    },
    {
      "comment": "Add filter",
      "name": "filter_add",
      "selector": "#ListLeft #refine"
    },
    {
      "comment": "Focus on city search form",
      "name": "city_search_start",
      "eventType": "focus",
      "selector": "#city_search #city"
    },
    {
      "comment": "Blur from city search form",
      "name": "city_search_end",
      "eventType": "blur",
      "selector": "#city_search #city"
    },
    {
      "comment": "Submit city search form",
      "name": "city_search_submit",
      "eventType": "submit",
      "selector": "#city_search #city"
    },
    {
      "comment": "Submit city search form using click on ->",
      "name": "city_search_submit_click",
      "selector": "#city_search .selectLg"
    },
    {
      "comment": "Click on scroll to photo button (at profile)",
      "name": "click_scroll_to_photo",
      "selector": "#foto a"
    },
    {
      "comment": "Click on phone number from name (at profile)",
      "name": "phone_name",
      "selector": ".name a"
    },
    {
      "comment": "Click on links under name(at profile)",
      "name": "click_links",
      "selector": ".linkbutton a"
    },
    {
      "comment": "Recommend profile at fb",
      "name": "recommend_fb",
      "selector": ".place_fb"
    },
    {
      "comment": "Phone / email from body (at profile)",
      "name": "phone_body",
      "selector": "#stats a"
    },
    {
      "comment": "Breadcrumbs click",
      "name": "breadcrumb",
      "selector": ".breadcrumbs a"
    },
    {
      "comment": "Precontent pause",
      "name": "precontent_pause",
      "selector": ".precontent input[value='Pause']"
    },
    {
      "comment": "Precontent resume",
      "name": "precontent_resume",
      "selector": ".precontent input[value='Resume']"
    },
    {
      "comment": "Precontent back to click",
      "name": "precontent_back",
      "selector": ".precontent td:first a"
    },
    {
      "comment": "Precontent marquee click",
      "name": "precontent_click",
      "selector": ".marquee a"
    },
    {
      "comment": "Precontent marquee click",
      "name": "select_city_footer",
      "selector": "#footer_city_links a"
    },
    {
      "comment": "Search form at header focus",
      "name": "select_city_header_focus",
      "eventType": "focus",
      "selector": ".changeBlock #city"
    },
    {
      "comment": "Search form at header blur",
      "name": "select_city_header_blur",
      "eventType": "blur",
      "selector": ".changeBlock #city"
    },
    {
      "comment": "Search form at header submit",
      "name": "select_city_header_submit",
      "eventType": "submit",
      "selector": ".changeBlock #city"
    },
    {
      "comment": "Search form at header click on submit button",
      "name": "select_city_header_click",
      "selector": ".changeBlock #headGo2"
    },
    {
      "comment": "Search form at header click on all cities link",
      "name": "select_city_header_all",
      "selector": ".changeBlock a"
    },
    {
      "comment": "Click on login buttons",
      "name": "top_right_click",
      "selector": ".navlogin a"
    },
    {
      "comment": "Top advertising links",
      "name": "top_left_click",
      "selector": "#topnav a"
    }
  ]
}
/* global $ */

if (typeof CONFIG === 'undefined') {
  console.warn('Rete:config not found');
  CONFIG = {
    events: [],
    dynamicEvents: [],
    ignoredEvents: [],
    forbiddenNames: [],
    source: 'test' //document.location.host
  };
}

/**
 * Get cookie by name
 * @param  {String} name
 * @return {String|Null}
 */
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    '(?:^|; )' + name.replace(/([.$?*|{}()[\]\\/+^])/g, '\\$1') + '=([^;]*)'
  ));
  return matches ? decodeURIComponent(matches[1]) : null;
}

/**
 * If cookie is not set - get user id from API and set it to cookie. Invoke callback anyway.
 * @param {Function} callback
 */
function setCookie(callback) {
  if (getCookie('reuserid')) {
    if (callback) callback();
    return;
  }
  fetch('https://api.retentioneering.com/get_cookie')
    .then(function (res) { return res.text(); })
    .then(function (text) {
      document.cookie = 'reuserid=' + text;
      document.cookie = 'retm=' + (new Date()).getTime();
      if (callback) callback();
    });
}

/**
 * Returns object with search params as key-value
 * @return {Object}
 */
function getParamsFromURL(prefix) {
  if (!document.location.search) return {};
  var getParams = document.location.search.slice(1);
  var paramsArray = getParams.split('&');
  var paramsMap = {};
  paramsArray.forEach(function(el) {
    var keyVal = el.split('=');
    var key = (prefix || '') + keyVal[0];
    var value = keyVal.length === 2 ? keyVal[1] : null
    paramsMap[key] = value;
  });
  return paramsMap;
}

/**
 * Skip personal data
 * @param  {Object} params
 * @return {void}
 */
function clean(params) {
  for (var key in params) {
    if (CONFIG.forbiddenNames && CONFIG.forbiddenNames.indexOf(key) !== -1) delete params[key];
  }
}

/**
 * Create event name based on its params
 * @param  {Object} item
 * @return {String}
 */
function getEventName(item) {
  if (item.name) return item.name;
  else return item.selector.replace(/[^a-zA-Z_-]/g, '') + '_' + (item.eventType || 'click');
}

/**
 * Create event type with namespace
 * @param  {Object} item
 * @return {String}
 */
function getEventType(item) {
  return (item.eventType || 'click') + '.rete';
}

function sanitizeUrl(url) {
  return encodeURIComponent(url.replace('#', '[hash]').replace('?', '[params]'));
}

/**
 * Send event data to API
 * @param  {Object} params
 * @return {void}
 */
function sendEvent(params) {
  var persistentParams = {
    source: CONFIG.source,
    user_id: getCookie('reuserid'),
    user_id_front: getCookie('retm'),
    url: sanitizeUrl(document.location.href)
  };
  var mergedParams = Object.assign({}, persistentParams, params, getParamsFromURL('query_'));
  clean(mergedParams);
  var img = new Image();
  if (window.DEBUG_MODE) {
    console.info('***', mergedParams);
  } else {
    if (false && $.post) { // POST
      $.post({
        contentType: 'application/json',
        dataType: 'json',
        url: 'https://api.retentioneering.com/pixel',
        data: JSON.stringify({ json: mergedParams })
      });
    } else { // GET
      // supports jQuery slim version
      if (mergedParams.url) mergedParams.url = sanitizeUrl(mergedParams.url);
      if (mergedParams.reference_address) mergedParams.reference_address = sanitizeUrl(mergedParams.reference_address);
      img.src = 'https://api.retentioneering.com/pixel.gif?data=' + encodeURIComponent(JSON.stringify(mergedParams));
    }
  }
}


// BASIC EVENTS

// Page load; start tracking
setCookie(function() {
  sendEvent({
    event_name: 'pageload'
  });
});

var defaultConfig = {
  "events": [
    {
      "selector": "button",
    },
    {
      "selector": "a",
      "name": "link_click"
    },
    {
      "selector": "a",
      "name": "link_rightclick",
      "eventType": "contextmenu"
    },
    {
      "selector": "input",
      "eventType": "focus"
    },
    {
      "selector": "input",
      "eventType": "blur"
    },
    {
      "selector": "select",
      "eventType": "change"
    }
  ]
}


// CONFIG

/**
 * Collect payload for event
 * @param  {Event} e     - original event
 * @param  {Object} item - config
 * @return {void}
 */
function handleSingleEvent(e, item) {
  var $this = $(this);
  var event_name = getEventName(item);
  if (CONFIG.ignoredEvents && CONFIG.ignoredEvents.indexOf(event_name) !== -1) {
    console.warn('*** Rete: ignored event', e);
    return;
  }
  var payload = {
    event_name: event_name
  };
  if (item.event_value) payload.event_value = item.event_value;

  if (item.eventType === 'submit') { // form
    payload.reference_address = e.currentTarget.action;
    payload.reference_text = $this.find('button[type=submit]').text();
  } else if (item.eventType === 'change' && $this.is('select')) { // select
    payload.reference_text = $this.find('option:selected').text();
    payload.reference_id = $this.val();
  } else { // other
    var text = e.currentTarget.innerText;
    var href = e.currentTarget.href;
    payload.reference_text = text.length > 20 ? (text.slice(0,20) + '…') : text;
    if (href) payload.reference_url = sanitizeUrl(href);
  }
  var id = $this.get(0).id;
  var name = $this.get(0).name;
  if (name || id) payload.reference_id = name || id;

  sendEvent(payload);
}

/**
 * Set handler for element specified in config
 * @param {Object} item
 * @param {String=} rootSelector - for dynamic elements
 */
function setSingleEvent(item, rootSelector) {
  // prevent tracking form submit twice
  if (item.eventType === 'submit') {
    $(item.selector).find('button').off('click.rete');
  }
  var eventType = getEventType(item);
  if (rootSelector) {
    $(rootSelector)
      .find(item.selector)
      .off(eventType);
    $(rootSelector)
      .off(eventType, item.selector)
      .on(eventType, item.selector, function(e) {
        handleSingleEvent.call(this, e, item);
      });
  } else {
    $(item.selector)
      .off(eventType)
      .on(eventType, function(e) {
        handleSingleEvent.call(this, e, item);
      });
  }
}

// default elements
defaultConfig.events.forEach(function(item) {
  setSingleEvent(item);
});

// static elements
if (CONFIG.events) CONFIG.events.forEach(function(item) {
  setSingleEvent(item);
});

// dynamic elements
if (CONFIG.dynamicEvents) CONFIG.dynamicEvents.forEach(function(parent) {
  parent.events.forEach(function(item) {

    setSingleEvent(item, parent.rootSelector)
  });
});

// EXTRA SCRIPTS (if exist)
function getParentText(x) {
    return x.currentTarget.parentElement.children[0].innerText;
}

$('.contentStyle a').on('click.rete', function (e) {
    var item = {
        "event_value": { "parent_name": getParentText(e) },
        "name": "select_service",
    };
    handleSingleEvent.call(this, e, item);
});

$('input:radio').on('click.rete', function (e) {
    var item = {
        "event_value": {
            "field_name": e.currentTarget.name,
            "value": e.currentTarget.value
        },
        "name": "input_radio",
    };
    handleSingleEvent.call(this, e, item);
});

$('input:checkbox').on('click.rete', function (e) {
    var item = {
        "event_value": {
            "field_name": e.currentTarget.name,
            "value": e.currentTarget.value
        },
        "name": "input_checkbox",
    };
    handleSingleEvent.call(this, e, item);
});

$('.question').on('hover.rete', function (e) {
    var item = {
        "event_value": {
            "value": e.currentTarget.textContent
        },
        "name": "input_hint",
    };
    handleSingleEvent.call(this, e, item);
});

$('.cke_wysiwyg_div').on('click.rete', function (e) {
    var item = {
        "name": "input_text",
    };
    handleSingleEvent.call(this, e, item);
});


$('input:file').on('click.rete', function (e) {
    var item = {
        "event_value": {
            "field_name": e.currentTarget.name
        },
        "name": "input_files",
    };
    handleSingleEvent.call(this, e, item);
});




console.log('Rete:loaded');
})(jQuery);
