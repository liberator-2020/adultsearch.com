<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

session_start();
if (!array_key_exists("p", $_POST) || $_POST["p"] != "snail") {
	if (array_key_exists("p", $_POST) && $_POST["p"] != "")
		sleep(5);
	$_SESSION["atest"] = "1";
	echo "<form method=\"post\"><input type=\"text\" name=\"p\" /><input type=\"submit\" value=\"S\" /><input type=\"hidden\" name=\"session_test\" value=\"1\" /></form>";die;
}
?>
<style type="text/css">
h6 {
	color: red;
	font-size: 14px;
	margin-bottom: 5px;
}
</style>
<?php

define( '_CMS_FRONTEND', 1);
$start = microtime(true);
require_once(__DIR__."/inc/config.php");
require_once(__DIR__."/inc/common.php");
require_once(__DIR__."/inc/common-frontend.php");

echo "Running from dir '".dirname(__FILE__)."'<br />\n";
echo "IP = '".$_SERVER['REMOTE_ADDR']."'<br />\n";
echo "Session = <br /><pre>".print_r($_SESSION, true)."</pre><br />\n";

$end = microtime(true);
echo "diff = ".number_format($end - $start, 3)."<br />\n";

//sessions
echo "<h6>Sessions</h6>";
if ($_REQUEST["session_test"] == "1") {
	if($_SESSION['atest'] == "1") {
		echo "PHP Sessions working <span style=\"font-weight: bold; color: green;\">OK</span><br />";
		echo "session_id = ".session_id()."<br />\n";
	} else {
		echo "PHP Sessions <span style=\"font-weight: bold; color: red;\">NOT</span> working !<br />";
	}
}
$_SESSION["atest"] = "1";
echo "<form action=\"\" method=\"post\">";
echo "<input type=\"hidden\" name=\"p\" value=\"{$_POST["p"]}\" />";
echo "<input type=\"hidden\" name=\"session_test\" value=\"1\" />";
echo "<input type=\"submit\" name=\"submit\" value=\"Test\" />";
echo "</form>\n";

//smarty
echo "<h6>Smarty</h6>";
$smarty = GetSmartyInstance();
$smarty->testInstall();

//db
$start = microtime(true);
global $config_db_host, $config_db_name, $config_db_user, $config_db_pass;
echo "<h6>DB</h6>\nhost=$config_db_host db_name=$config_db_name db_user=$config_db_user<br />";
$con = mysqli_connect($config_db_host, $config_db_user, $config_db_pass, $config_db_name);
if (!$con) {
	echo "Error: Could not connect: ".mysqli_error()."<br />";
} else {
	echo "DB connection Ok.<br />\n";
}
$end = microtime(true);
echo "diff = ".number_format($end - $start, 3)."<br />\n";

//sphinx
$start = microtime(true);
require_once(_CMS_ABS_PATH."/inc/classes/class.sphinxapi.php");
$sc = new SphinxClient ();
echo "<h6>Sphinx</h6>\nserver: host={$sc->_host} port={$sc->_port}<br />";
$sc->SetFilter('loc_id', array(42038)); //Angeles City, Philipines
$sc->SetGroupBy("place_type_id", SPH_GROUPBY_ATTR, '@count desc');
$results = $sc->Query("", "dir");
if ($sc->_error != "") {
	echo "Sphinx error: ".$sc->_error."<br />";
} else {
	$cnt = count($results["matches"]);
	echo "Sphinx connection OK, $cnt types in Angeles City<br />";
}

$sc = new SphinxClient ();
$sc->SetFilter('type', array(1));
$sc->SetFilter('loc_id', array(10991));
$sc->SetSortMode(SPH_SORT_EXTENDED, "sponsor DESC, bp ASC, eccie_promo ASC, updated DESC");
$sc->setGroupBy('cid', SPH_GROUPBY_ATTR, "sponsor DESC, bp ASC, eccie_promo ASC, updated DESC");
$results = $sc->Query("", "escorts");
if ($sc->_error != "") {
	echo "Sphinx error: ".$sc->_error."<br />";
} else {
	$cnt = count($results["matches"]);
	echo "Sphinx connection OK, $cnt escorts in Las Vegas,NV<br />";
}
$end = microtime(true);
echo "diff = ".number_format($end - $start, 3)."<br />\n";

//memcached
global $config_memcached_server, $config_memcached_port;
$start = microtime(true);
$mc_ips = [$config_memcached_server];
$mc_port = $config_memcached_port;

foreach ($mc_ips as $mc_ip) {

	$mc = new Memcached();
	$mc->addServer($mc_ip, $mc_port);
	$stats = array_shift($mc->getStats());
	echo "<h6>Memcache @IP {$mc_ip}:{$mc_port}</h6>";

	if (array_key_exists("memcached_flush", $_POST) && $_POST["memcached_flush"] == "1") {
		echo "<br /><strong>Flushing memcached...</strong><br />\n";
		$command = "echo 'flush_all' | nc {$mc_ip} {$mc_port}";
		echo "<pre>";
		$line = system($command, $ret);
		echo "</pre>";
		if ($ret != 0) {
			echo "Command '{$command}' failed!<br >";
			return false;
		}
	}

	echo "<table border='1'>"; 
	echo "<tr><td>Memcache Server version:</td><td> ".$stats["version"]."</td></tr>"; 
	echo "<tr><td>Process id of this server process </td><td>".$stats["pid"]."</td></tr>"; 
	echo "<tr><td>Number of seconds this server has been running </td><td>".$stats["uptime"]."</td></tr>"; 
	echo "<tr><td>Curr/Total items  </td><td>".$stats["curr_items"]." / ".$stats["total_items"]."</td></tr>"; 
	echo "<tr><td>Bytes  </td><td>".$stats["bytes"]."</td></tr>"; 
	echo "<tr><td>Limit maxbytes  </td><td>".$stats["limit_maxbytes"]."</td></tr>"; 
	$all_keys = $mc->getAllKeys();
	echo "<tr><td>count(getallKeys()) </td><td>".count($all_keys)."</td></tr>"; 
	echo "</table>";

	echo "PTIds: ";
	$ptids = array();
	foreach ($all_keys as $key) {
		if (substr($key, 0, 10) != "SQL:PTY-ID")
			continue;
		$ptids[] = substr($key, 11);
	}
	sort($ptids);
	foreach ($ptids as $ptid) {
		echo $ptid.", ";
	}
	echo "<br />";
}

//class memcached
echo "Class mcache test...<br />\n";
require_once(_CMS_ABS_PATH.'/inc/classes/class.mcache.php');
$db = new db();
$mcache = new mcache($db);
$row = $mcache->get("SQL:PTY-ID", 1);
echo "row: ".print_r($row, true)."<br />\n";
$row = $mcache->get("SQL:PTY-ID", 1);
echo "row: ".print_r($row, true)."<br />\n";
echo "log: ".print_r($mcache->getLog(), true)."<br />\n";

echo "<form action=\"\" method=\"post\">";
echo "<input type=\"hidden\" name=\"p\" value=\"{$_POST["p"]}\" />";
echo "<input type=\"hidden\" name=\"memcached_flush\" value=\"1\" />";
echo "<input type=\"submit\" name=\"submit\" value=\"Flush memcached\" />";
echo "</form>\n";

$end = microtime(true);
echo "diff = ".number_format($end - $start, 3)."<br />\n";

//redis
global $config_redis_real_host, $config_redis_real_port, $config_redis_host, $config_redis_port, $config_nutcracker_stats_host, $config_nutcracker_stats_port;
function redis_test($red) {
	$start = microtime(true);
	$ret = $red->set("as_monitor_test", 2);
	if ($ret) {
		echo "SET <span style=\"font-weight: bold; color: green;\">OK</span><br />";
	} else {
		echo "ERROR: redis SET returns '{$ret}' !<br />";
	}
	$ret = $red->get("as_monitor_test");
	if ($ret == 2) {
		echo "GET <span style=\"font-weight: bold; color: green;\">OK</span><br />";
	} else {
		echo "ERROR: redis GET returns '{$ret}' !<br />";
	}
	$ret = $red->incr("as_monitor_test");
	if ($ret == 3) {
		echo "INCR <span style=\"font-weight: bold; color: green;\">OK</span><br />";
	} else {
		echo "ERROR: redis INCR returns '{$ret}' !<br />";
	}
	$ret = $red->decrBy("as_monitor_test", 2);
	if ($ret == 1) {
		echo "DECRBY <span style=\"font-weight: bold; color: green;\">OK</span><br />";
	} else {
		echo "ERROR: redis DECRBY returns '{$ret}' !<br />";
	}
	$end = microtime(true);
	echo "diff = ".number_format($end - $start, 3)."\n";
}
echo "<br /><h6>Redis - real</h6>";
echo "Host: {$config_redis_real_host}, Port: {$config_redis_real_port}<br />";
$red = new red($config_redis_real_host, $config_redis_real_port);
redis_test($red);
echo "<h6>Redis - via proxy</h6>";
echo "Host: {$config_redis_host}, Port: {$config_redis_port}<br />";
$red = new red();
redis_test($red);
echo "<h6>Redis - nutcracker proxy</h6>";
$errno = null;$errstr = null;
$fp = fsockopen($config_nutcracker_stats_host, $config_nutcracker_stats_port, $errno, $errstr, 30);
if (!$fp) {
	echo "$errstr ($errno)<br />\n";
} else {
	$stats = fgets($fp, 2000);
	echo "Stats = '{$stats}'<br />\n";
	fclose($fp);
}

//dns resolver
echo "<br /><h6>DNS Resolver</h6>";
$host = "secure.paylinedatagateway.com";
$ip = gethostbyname($host);
echo "IP for '{$host}': '{$ip}'<br />\n";

//shared memory
echo "<br /><h6>Shared memory</h6>";
// Create 100 byte shared memory block with system id of 0xff3
$shm_id = shmop_open(0xff3, "c", 0644, 100);
if (!$shm_id) {
	echo "Couldn't create shared memory segment\n";
}
// Get shared memory block's size
$shm_size = shmop_size($shm_id);
echo "SHM Block Size: " . $shm_size . " has been created.<br />\n";
// Lets write a test string into shared memory
$test_string = "my shared memory block";
$shm_bytes_written = shmop_write($shm_id, $test_string, 0);
if ($shm_bytes_written != strlen("my shared memory block")) {
	echo "Couldn't write the entire length of data\n";
}
// Now lets read the string back
$test_string_2 = trim(shmop_read($shm_id, 0, $shm_size));
if (!$test_string_2) {
	echo "Couldn't read from shared memory block\n";
}
echo "The data inside shared memory was: {$test_string_2}<br />\n";
if ($test_string == $test_string_2) {
	echo "<span style=\"font-weight: bold; color: green;\">OK</span><br />";
} else {
	echo "ERROR: '{$test_string}' != '{$test_string_2}' !<br />";
}
//Now lets delete the block and close the shared memory segment
if (!shmop_delete($shm_id)) {
	echo "Couldn't mark shared memory block for deletion.";
}
shmop_close($shm_id);


//device detection
echo "<br /><h6>Device Detection</h6>";
echo "User-Agent: ".$_SERVER['HTTP_USER_AGENT']."<br />\n";
echo "<strong><u>Mobile Detect</u></strong><br />\n";
require_once(_CMS_ABS_PATH.'/inc/classes/mobile_detect/Mobile_Detect.php');
$detect = new Mobile_Detect;
echo "<strong>Mobile: ".($detect->isMobile() ? "Yes" : "No")."</strong><br />\n";
echo "<strong>Tablet: ".($detect->isTablet() ? "Yes" : "No")."</strong><br />\n";
echo "<small>";
echo "Phone: ".($detect->isPhone() ? "Yes" : "No")."<br />\n";
echo "iPhone: ".($detect->isIphone() ? "Yes" : "No")."<br />\n";
echo "iPad: ".($detect->isIpad() ? "Yes" : "No")."<br />\n";
echo "Android: ".($detect->isAndroid() ? "Yes" : "No")."<br />\n";
echo "Samsung: ".($detect->isSamsung() ? "Yes" : "No")."<br />\n";
echo "</small>";
echo "<strong><u>Browscap</u></strong><br />\n";
try {
	$cacheDir = _CMS_ABS_PATH."/../vendor/browscap/browscap-php/resources";
	//echo "browscap cache dir = '{$cacheDir}'<br />\n";
	$fileCache = new \Doctrine\Common\Cache\FilesystemCache($cacheDir);
	$cache = new \Roave\DoctrineSimpleCache\SimpleCacheAdapter($fileCache);
	$logger = new \Monolog\Logger('name');
	$bc = new \BrowscapPHP\Browscap($cache, $logger);
	$browser = $bc->getBrowser();
	echo "<strong>Mobile: ".($browser->ismobiledevice == 1 ? "Yes" : "No")."</strong><br />\n";
	echo "<strong>Tablet: ".($browser->istablet == 1 ? "Yes" : "No")."</strong><br />\n";
	echo "<small><pre>".print_r($browser, true)."</pre></small><br />\n";
} catch (\Exception $e) {
	echo "Browscap not working!<br />\n";
	throw $e;
}

//debug log
debug_log("monitor");

//phpinfo
echo "<h6>Phpinfo</h6>";
phpinfo();
?>
