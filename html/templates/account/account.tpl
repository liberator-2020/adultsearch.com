<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/account/account.css?20190430">

<div class="container account">

	{include "account/menu.tpl" ebiz_login_url=$ebiz_login_url ebiz_available=$ebiz_available}

	<div class="wrap">

		{if $agency}
		<div class="row">
			<label class="col-sm-3 control-label">Agency Guidelines</label>
			<div class="col-sm-9">
				{include "wire_info.tpl" account_id=$account_id}
			</div>
		</div>
		{/if}

		<div class="row">
			<label class="col-sm-3 control-label">Login</label>
			<div class="col-sm-9">
				<div >
					{$username}<br />
					<a href="/account/change-username" class="btn btn-xs btn-default">Change username</a><br />
					<a href="/account/change-password" class="btn btn-xs btn-default">Change password</a><br />
				</div>
			</div>
		</div>

		<div class="row">
			<label class="col-sm-3 control-label">Email Address</label>
			<div class="col-sm-9">
				<div >
					{$email}<br />
					<a href="/account/change-email" class="btn btn-xs btn-default">Change email</a><br />
				</div>
			</div>
		</div>

		<div class="row">
			<label class="col-sm-3 control-label">Email Notifications</label>
			<div class="col-sm-9">
				When someone leaves a comment on one of your reviews - {if $newcomment_checked}YES{else}NO{/if}<br />
				When someone leaves a comment on a review you commented on - {if $newcommentme_checked}YES{else}NO{/if}<br />
				<a href="/account/change-settings" class="btn btn-xs btn-default">Change settings</a><br />
			</div>
		</div>

		<div class="row">
			<label class="col-sm-3 control-label">Number of Forum Posts</label>
			<div class="col-sm-9 ">
				{$forum_post}
			</div>
		</div>

		<div class="row">
			<label class="col-sm-3 control-label">Number of Reviews</label>
			<div class="col-sm-9 ">
				{$review_cnt}
			</div>
		</div>

		<div class="row">
			<label class="col-sm-3 control-label">Avatar</label>
			<div class="col-sm-9">
				<span id="currenta" style="margin-right: 20px; vertical-align:left;">
				<img src="{$config_image_server}/avatar/{if $avatar}{$avatar}{else}no_avatar.png{/if}" width="100" height="100" />
				<br />
				<a href="/account/change-avatar" class="btn btn-xs btn-default">Change avatar</a><br />
			</div>
		</div>

	   {if $ebiz_login_url}
		<div class="row">
			<label class="col-sm-3 control-label">Escorts.biz</label>
			<div class="col-sm-9">
				<a href="{$ebiz_login_url}" target="_blank">Login to Escorts.biz</a>
			</div>
		</div>
		{/if}

		<br style="clear: both;" />
		{include "account/help.tpl" agency=$agency}

	</div>

</div>
