<h2>You didnt get your confirm email yet?</h2>
We can send you your confirm code to an email address you've given us during the creation of an account. Just fill the following form:<br />
<div style="width: 40em; text-align: center;padding-bottom:40px;">
	<form method="post">
		<input type="hidden" name="id" value="{$account_id}" />
		<img src="/captcha.php?{$time}" border="1" alt="" /><br />
		Please enter the code shown above:<input type="text" maxlength="8" size="8" name="captcha_string" value="" /><br />
		<input type="submit" name="submit" value="Submit" />
	</form>
</div>
