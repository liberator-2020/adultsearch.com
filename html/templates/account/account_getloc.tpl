<table width="100%" border="0" cellspacing="0" cellpadding="0" height="500">
  <tr>
    <td valign="top">
<form action="" method="post">
<input type="hidden" name="core_loc_update" value="1" />
<table width="99%" border="0" align="center" cellpadding="3" cellspacing="3" bgcolor="#ffffff" id="signupform">
          <tbody>
            <tr>
              <td colspan="3" valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td width="11%" valign="top" nowrap="nowrap"><div class="createAccountTitles"><strong><span class="gaia sub el">ZIP code: </span><span 
class="reqired">*</span></strong></div></td>
              <td width="33%" valign="top"><input name="zipcode" class="formHeight" value="{$zipcode}" size="10" maxlength="10" /></td>

              <td width="56%" rowspan="4" valign="top"><strong class="ContentlinksTitle">Please pick your location first.</strong> <br />
                <br />
Adult Search has thousands of location filled with information. To serve you better, we need to know which location you are near. <br/><br/>Please enter your zip 
code 
or 
type the city name that you would like to see information about.</td>
            </tr>

	<tr>
	  <td nowrap="nowrap" colspan="2">- OR -</td>
	</tr>

      <tr>
        <td nowrap="nowrap" valign="top"><div class="createAccountTitles"><strong><span class="gaia sub pl">City name:</span> <span 
class="reqired">*</span></strong></div></td>
        <td valign="top"><input name="loc_name" type="text" class="formHeight" value="{$loc_name}" size="30" maxlength="50" /></td>
        </tr>
      <tr>
        <td valign="top">&nbsp;</td>
        <td valign="top"><label>

          <input name="button" type="submit" class="button" id="button" value="Submit" />
        </label></td>
        </tr>
   

    </tbody></table>
	</form>
   </td>
  </tr>

{if $error||$cities}
  <tr>
   <td colspan="2" height="100" valign="top">
{if $error}<div class="error">{$error}</div>{/if}

{if $cities}
<form method="post" action="">
<div style="font-size:16px;line-height:200%;">
There are more than one city found, Please make your selection below;<br/>
<ul>
{section name=s loop=$cities}
{assign var=c value=$cities[s]}
<li><a href="{$c.link}">{$c.loc_name}, {$c.s}</a></li>
{/section}
</ul>
<input type="submit" value="Continue" />
</div>
</form>
{/if}
   </td>
  </tr>
{/if}

{if $state_columns}
  <tr>
   <td colspan="2" height="300" valign="top">
	<div style="text-align:center;font-size:16px"> -- OR pick your state below -- </div>
	<div class="classifiedloc">
	{section name=s loop=$state_columns}{assign var=s value=$state_columns[s]}
	<ul class="updown">
	{section name=s2 loop=$s}{assign var=l value=$s[s2]}
	{* <li><a href="//{$l.loc_name}.adultsearch.com{$smarty.server.REQUEST_URI}" title="">{$l.real_name}</a></li> *}
	<li><a href="{$l.link}" title="">{$l.real_name}</a></li>
	{/section}
	</ul>
	{/section}
	</div>
   </td>
  </tr>
{/if}

</table>
