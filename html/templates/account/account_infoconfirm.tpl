<div id="container">
	<div style="padding-top:9px; padding-left:20px;"></div>
	<div style="padding-top:9px; padding-left:20px;"></div>
</div>
<div id="mainContent">
	<div style="padding-left:0px;">
		<table width="100%" cellpadding="0" cellspacing="0" summary="page">
		<tr>
			<td valign="top">
				<table width="100%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td>
						<table width="98%" height="101" align="center" cellpadding="0" cellspacing="0">
						<tbody>
						<tr>
							<td width="100%" height="35" style="font-size:16px;">
								Thank you {$username} . We have sent an email message to the address <strong>{$email}</strong>.	Please access your email and visit the verification url provided in	order to activate your AdultSearch Account. If you do not receive this	email, please check your Spam or Junk email folder.
							</td>
						</tr>
						<tr>
							<td height="5"></td>
						</tr>
						<tr>
							<td height="46">&nbsp;</td>
						</tr>
						</tbody>
						</table>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</div>
</div>
<div style="clear:both"></div>

