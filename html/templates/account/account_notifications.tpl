<form method="post">

{if $done || $newmperror }
<br />
<table class="tblMessage">
	<tr>
		<td class="SettingsHelp">{if $newmperror}Please Select a Specific Location for the email alerts.{else}Notification settings successfully changed.{/if}</td>
	</tr>
</table>
{/if}

<div class="granularSettingsTable">
 <div>
	<div class="settingsList" >
		<h3 class="SubSettingsTitle">Email Notifications</h3>
		<hr class="settingsSeparator" />
	</div>
 </div>
</div>

<div class="granularSettingsTable">
	<div>
		<div class="notificationsRowHeader"></div>
		<div class="submitButtonContainer">
			<div class="notificationsRowText"></div>
			<div class="notificationsRowCheckbox"><span class="boldText">Send Email?</span></div>
		</div>
	</div>

	<div class="notificationRow">
		<div class="notificationsRowHeader"><span>Alerts:</span></div>
		<div class="rowContainer">
			<div class="notificationsRowText"><span>When someone leaves a comment on one of your reviews</span></div>
			<div class="notificationsRowCheckbox">{$newcomment}</div>
		</div>

		<div class="rowContainer">
			<div class="notificationsRowText"><span>When someone leaves a comment on a review which you left comment(s) too</span></div>
			<div class="notificationsRowCheckbox">{$newcommentme}</div>
		</div>
	</div>

	<div class="submitButtonContainer">
		<div class="ar"><input type="submit" name="Save" value="Save Changes"  /></div>
	</div>


</div>

</form>
