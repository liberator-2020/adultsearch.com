This message is to confirm that an order{if $order|@count>1}(s){/if} has been placed with Adult Search.<br><br>

Please do not reply to this message. This is simply a courtesy confirmation for<br>
your records and security.<br><br>

Order{if $order|@count>1}(s){/if} information is as follows:<br><br>

{section name=s1 loop=$order}{assign var=o value=$order[s1]}
Total Amount: ${$o.total}<br>
Approval Code: APPROVED {$o.t}<br>
  Invoice Num: {$o.id}<br>
         Name: {$o.name}<br>
      Address: {$o.address}<br>
          Zip: {$o.zip}<br>
        EMail: {$o.email}<br>
	   CC: **{$o.cc}<br>
{if !$smarty.section.s1.last}
-------------------------------------<br><br>
{/if}
{/section}
<br><br>
Sincerely,<br>
adultsearch.com
