{literal}
<style>
#logoModule #header { position: inherit; }
#mainContent {
	text-align: center;
	margin: 0 auto;
	height: 700px;
	padding-top: 40px;
	width: 760px;

}
#content {
	background: url("/images/login_blue_bg.jpg") no-repeat scroll center top #19639b;
	height: 700px;
	width: 100%;
	
}
#loginMain {
	width: 400px;
	background: #FFF;
	padding: 10px;
	font-size: 12px;
	text-align: left;
	border-radius: 6px;
	border: solid 3px #999;
	float: left;
}
#loginMain a {
	font-size: 11px;
	color: #186299;
}
#loginMain #password, #loginMain #email {
	background: none repeat scroll 0 0 #D9E0E5;
	border-radius: 5px;
	font-size: 16px;
	margin-right: 10px;
	padding: 5px;
	width: 370px;
}
#loginMain form {
	margin-left: 10px;
}
#loginAd a {
	float: left;
	width: 620px;
	height: 680px;
}
#loginMain #button {
	font-size: 16px;
	padding: 5px;
	border-radius: 5px;
	color: #FFFFFF;
	background: #ff740e;
	cursor: pointer;
	border: solid 1px #000000;
	box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.6);
}
#loginMain #button:hover {
	color: #000000;
}
.title {
	font-size: 14px;
	font-weight: bold;
	color: #FFF;
	background: #186299;
	padding: 2px 6px;
	margin-bottom: 15px;
	clear: both;
	text-transform: capitalize;
	border-radius: 6px;
}
.breadcrumbs { display: none; }

.registerCTA {
	width: 100%;
	display: inline-block;
}
.registerCTA a, .registerCTA a:visited {
	background: url(/products/images/register_cta.png) no-repeat 0 0;
	display: block;
	text-align: center;
	margin: 0px 110px;

	font-size: 20px !important;
	padding: 5px;
	border-radius: 5px;
	color: #FFFFFF !important;
	background: #ff740e;
	cursor: pointer;
	border: solid 1px #000000;
	box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.6);
}
.registerCTA a:hover {
	background-position: 0px -55px;
	color: black !important;
	text-decoration: none;
}
.bannerSquare {
	float: right;
}
</style>
{/literal}

<div id="mainContent"> 

	<div id="loginMain">
		<div class="title">Login to AdultSearch</div>
		<form method="post" action="">
			<input type="hidden" name="login" value="1" />

			{if $origin}<input type="hidden" name="origin" value="{$origin}" />{/if}
			
			{if $login_error}
				<h3>{$login_error}</h3><br />
			{/if}

			{if $message}
				<div class="login_message">
					{$message_full}
				</div>
				<input type="hidden" name="message" value="{$message}" />
			{/if}

			Email Address<br /><input name="login_email" id="email" /><br /><br />
			Password<br /><input name="login_password" id="password" type="password" />
			<label><p><input name="saveme" value="1" type="checkbox" /> Keep this computer logged in</p></label>
			{$csrf}
			<input name="submit" id="button" value="Login" type="submit" />
			<br />
			<br />
			<a href="/account/signup" rel="nofollow">Register</a> | <a href="/account/reset" rel="nofollow">Password Help</a>
		</form>
		<br /><br />
		<div class="title">Register for AdultSearch</div>
		<div class="registerCTA"><a href="/account/signup">Register</a></div>
		<br /><br />
	</div>
   
	<div class="bannerSquare">
		<script src="{$config_site_url}/js/publisher/p.js?20180922" type="text/javascript"></script>
		<script type="text/javascript">as_show_banner(10106, 300, 250);</script>
	</div>

</div>
