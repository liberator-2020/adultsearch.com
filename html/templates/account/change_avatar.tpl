<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/account/account.css?20190430">

<div class="container account">

	{include "account/menu.tpl" ebiz_login_url=$ebiz_login_url ebiz_available=$ebiz_available}

	<div class="wrap">

		<form method="post" action="" enctype="multipart/form-data" class="form-horizontal">

			<div class="form-group">
				<label class="col-sm-3 control-label">&nbsp;</label>
				<div class="col-sm-9">
					<h1>Change avatar</h1>
				</div>
			</div>

			{if $error}<span class="error">{$error}</span>{/if}

			<div class="form-group">
				<label class="col-sm-3 control-label">Current Avatar</label>
				<div class="col-sm-9">
					<img src="{$config_image_server}/avatar/{if $avatar}{$avatar}{else}no_avatar.png{/if}" width="100" height="100" />
                    {if $avatar}
						<br />
			  			<input type="submit" name="delavatar" class="btn btn-danger btn-xs" value="Delete avatar" />
					{/if}
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Upload New Avatar</label>
				<div class="col-sm-9">
					<br />
                    <input name="avatar" type="file" />
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
			  		<input type="submit" class="btn btn-primary" value="Submit" name="submit" />
					<div class="clear"></div>
				</div>
			</div>

		</form>

		{include "account/help.tpl" agency=$agency}

	</div>

</div>
