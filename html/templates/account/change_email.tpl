<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/account/account.css?20190430">

<div class="container account">

	{include "account/menu.tpl" ebiz_login_url=$ebiz_login_url ebiz_available=$ebiz_available}

	<div class="wrap">

		<form method="post" action="" enctype="multipart/form-data" class="form-horizontal">

			<div class="form-group">
				<label class="col-sm-3 control-label">&nbsp;</label>
				<div class="col-sm-9">
					<h1>Change email</h1>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">&nbsp;</label>
				<div class="col-sm-9">
					Your current email is <strong>{$currentEmail}</strong>.
				</div>
			</div>

			{if $error}<span class="error">{$error}</span>{/if}

			<div class="form-group">
				<label class="col-sm-3 control-label">New email</label>
				<div class="col-sm-9">
					<input type="text" size="10" value="{$email}" name="email" class="form-control" />
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
			  		<input type="submit" class="btn btn-primary" value="Submit" />
					<div class="clear"></div>
				</div>
			</div>

		</form>

		{include "account/help.tpl" agency=$agency}

	</div>

</div>
