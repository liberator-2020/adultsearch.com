
{if $account_id}
	<h1>Close Account #{$account_id}</h1>
{else}
	<h1>Close Your Account</h1>
{/if}

{if $ads}
	<h2>Classified ads</h2>
	<ul>
	{foreach from=$ads item=c}
		<li></li>
	{/foreach}
	</ul>
{/if}
<div id="profile" style="padding-top:30px;">

{if $show_password_form}

	{if isset($error2)}<span style="color: red; font-weight: bold;">{$error2}</span><br />{/if}
	<h2>Set a new password for your account</h2>
	<form name="Form1" action="" method="post">
	<input type="hidden" name="account_id" value="{$account_id}" />
	<input type="hidden" name="code" value="{$code}" />
	<table>
		<tr>
			<th class="createAccountTitles">Password:</th>
			<td><input type="password" name="password" tabindex="10" value="" class="formHeight" /></td>
		</tr>
		<tr>
			<th class="createAccountTitles">Password again:</th>
			<td><input type="password" name="password2" tabindex="11" value="" class="formHeight" /></td>
		</tr>
		<tr>
			<th></th>
			<td><input type="submit" name="reset_submit" value="Set password"  class="button" onclick="return CheckFields();"/></td>
		</tr>
	</table>
	</form>
{/if}

{if isset($success)}
<table class="user" cellspacing="0" cellpadding="0"><tr><td>
 <tr>
        <td class="userhd" colspan="2"><h3>Please check your email inbox</h3></td>
 </tr>

 <tr>
        <td class="midmid">
	<span style="font-weight: bold;">{$success}</span><br />
	{if $cclink}<a href="/passwordreset-account.htm?account_id={$account_id}">Click here</a> if you want to reset your password by using your credit card number.{/if}
</td></tr></table>
{/if}

	{if isset($error)}<span style="color: red; font-weight: bold;">{$error}</span><br />{/if}

{if $show_form}

<table class="user" cellspacing="0" cellpadding="0">
 <tr>
	<td class="userhd" colspan="2"><h3>Have you forgotten your password?</h3></td>
 </tr>

 <tr>
	<td class="midmid">
		<form action="" method="post" class="tbuser">
		<input type="hidden" name="action" value="passforgot" />
		<table class="content2" cellpadding="5" cellspacing="5">
		 <tr>
			<td align="right" class="createAccountTitles">E-mail address:</td>
			<td class="right"><input type="text" name="email" value="{$email}" class="formHeight" /></td>
		</tr>

		<tr>
			<td align="right" valign="top" class="createAccountTitles">Security check:</th>
			<td class="right createAccountTitles"><img src="/captcha.php?{$time}" border="1" alt="" /><br /><br />
		Text in the box: <input type="text" maxlength="8" size="8" 
name="captcha_string" value="" class="formHeight" /></td>
		</tr>

		<tr>
			<th></th>
			<td><input type="submit" name="submit" value="Submit" class="btn" /></td>
		</tr>
		<tr>
			<td class="spacer24" colspan="2"> </td>
		</tr>	
		</table>	
		</form>
	</td>
  </tr>
</table>


{/if}

</div>
