<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>AdultSearch.com - Account Confirmation</title>
</head>
<body>
Welcome to AdultSearch.com.<br /><br />

Your Details:<br /><br />
Email-address: {$email}<br />
Password: {$password}<br /><br />

In order to use your membership, email approval is needed. So please click on the below link (if the address is not clickable, copy it and past it into the address bar of your browser then press enter to go) to confirm your account.<br /><br />

<a href="{$config_site_url}/account/confirm?h={$hash}&id={$account_id}">{$config_site_url}/account/confirm?h={$hash}&amp;id={$account_id}</a><br /><br />

Thank you,<br />
AdultSearch.com Administrator.
</body>
</html>
