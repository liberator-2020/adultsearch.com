Welcome to AdultSearch.com.

Your Details:
Email-address: {$email}
Password: {$password}

In order to use your membership, email approval is needed. So please click on the below link (if the address is not clickable, copy it and past it into the address bar of your browser then press enter to go) to confirm your account.

{$config_site_url}/account/confirm?h={$hash}&id={$account_id}

Thank you,
AdultSearch.com Administrator.
