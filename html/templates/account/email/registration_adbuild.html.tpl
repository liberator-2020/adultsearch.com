<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Welcome to AdultSearch, save this email!</title>
</head>
<body>
Your account on AdultSearch is now active and ready for use. Save this email so you always have access to your login information.<br /><br />

<span style="font-weight: bold; text-decoration: underline;">Your AdultSearch Account Details</span><br />
Email: {$email}<br />
Password: {$password}<br /><br />

Please respond to this email if you have any questions or concerns.<br /><br />

Sincerely,<br />
AdultSearch.com<br />
<a href="mailto:support@adultsearch.com">support@adultsearch.com</a><br />
</body>
</html>
