Your account on AdultSearch is now active and ready for use. Save this email so you always have access to your login information.

Your AdultSearch Account Details
Email: {$email}
Password: {$password}

Please respond to this email if you have any questions or concerns.

Sincerely,
AdultSearch.com
support@adultsearch.com
