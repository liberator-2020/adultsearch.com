<div style="padding: 15px; border-radius: 10px; margin-top: 10px; margin-bottom: 10px; background-color: #ffffb9; border: 1px solid #ccc;">
	For help please contact:<br />
	U.S. &amp; Canada: <a href="tel:+17029351688">1-702-935-1688</a><br />
	{if $agency}
		<a href="mailto:agency@adultsearch.com">agency@adultsearch.com</a>
	{else}
		<a href="mailto:support@adultsearch.com">support@adultsearch.com</a>
	{/if}
</div>
