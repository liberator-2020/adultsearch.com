<div id="status">
	<span><a href="/adbuild/" class="active reddish">Post An Ad</a></span>
	<span><a href="/classifieds/myposts" class="active">My Ads</a></span> 
	<span><a href="/account/mypage" class="active">My Account</a></span>
	{if $agency}
	<span><a href="/account/wire" class="active reddish">Upload wire info</a></span>
	{/if}
	<span><a href="/account/payments" class="active">My Payments</a></span>
	<span><a href="/pm/index" class="active">My Private Messages</a></span>
	{if $ebiz_login_url || $ebiz_available}
	<span style="position: relative;"><a href="{if $ebiz_login_url}{$ebiz_login_url}{else}/account/mywebsite{/if}" class="active">My Website</a><img src="/images/new.png" style="position: absolute; top: -10px; left: 70px;"/></span>
	{/if}
</div>
