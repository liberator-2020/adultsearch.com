{literal}
<style type="text/css">
.ebiz-title{font-size:25px;text-align:center;padding-top:20px;padding-bottom:30px}
.ebiz-title span{color:#FF831A}
.ebiz-left{display:block;width:50%;float:left;text-align:center}
.ebiz-right{display:block;width:50%;float:right}
.ebiz-clear{clear:both}
.ebiz-bullet{display:block;padding-bottom:12px;padding-left:20px;font-size:17px;background-image:url(//www.escorts.biz/assets/images/adultsearch/ebiz-bullet.gif);background-repeat:no-repeat;background-position:left 3px}
a#create_website {
	display:block;
	width:277px;
	padding-left:70px;
	height:44px;
	background-image: url(//www.escorts.biz/assets/images/adultsearch/ebiz-button.png);
	font-size: 18px !important;
	font-weight:700;
	color:#FFF;
	text-decoration:none;
	margin-top:15px
}
.ebiz-signup-button a:hover{-moz-opacity:.75;filter:alpha(opacity:75);opacity:.75}
.ebiz-easy{display:block;font-size:12px;color:#666;text-align:center}
.ebiz-easy span{font-size:24px;color:#1E76B7;padding-right:20px}
#ebiz-loading-wrapper {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 150;
	background-color: white;
	filter:alpha(opacity=60);
	opacity:0.6;
}
#ebiz-loading-wrapper img {
	overflow: auto;
	margin: auto;
	position: absolute;
	top: 0; left: 0; bottom: 0; right: 0;
}
#ebiz-result {
	padding: 5px;
	font-size: 1.2em;
}
#ebiz-result.success {
	background-color: #EEFFEE;
	border: 1px solid green;
}
#ebiz-result.fail {
	background-color: #FFEEEE;
	border: 1px solid red;
}
</style>
{/literal}


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/account/account.css?20190430">

<div class="container account">

	{include "account/menu.tpl" ebiz_login_url=$ebiz_login_url ebiz_available=$ebiz_available}

	<div class="wrap">

		{if $ebiz_login_url}
			<p style="height: 350px;">
				You already have escort.biz website.<br />
				You can login <a href="{$ebiz_login_url}">here</a>.
			</p>
		{else}
			<div class="ebiz-title">You qualify for a <strong>free offshore website and anonymous domain name</strong> <span>(www.yourname.com)</span></div>
			<div class="ebiz-left"><img src="//www.escorts.biz/assets/images/adultsearch/ebiz-laptop.jpg" alt="Screenshot" /></div>
			<div class="ebiz-right">
				<div class="ebiz-bullet">build your website in 30 seconds</div>
				<div class="ebiz-bullet">easily update your own website</div>
				<div class="ebiz-bullet">customize your website design</div>
				<div class="ebiz-bullet">24/7 technical support to help you</div>
				<div class="ebiz-signup-button"><a href="#" id="create_website">Start my Free Website</a></div>
			</div>
			<div class="ebiz-clear"></div>
			<div id="ebiz-result"></div>
			<div class="ebiz-easy">
				<p><span>Easy to customize</span> Choose your fonts, colors, patterns and upload your photos!</p>
				<p><img src="//www.escorts.biz/assets/images/adultsearch/ebiz-tablet.jpg" alt="Screenshot" /></p>
			</div>
		{/if}

		{include "account/help.tpl" agency=$agency}

	</div>

</div>

<div id="ebiz-loading-wrapper" style="display: none;"><img src="/images/ajax_loader_64.gif" /></div> 

<script type="text/javascript">
var ad_id = {$ebiz_ad_id};

{literal}
// General tracking code. Tracks page view
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-48537132-4', 'adultsearch.com');
ga('send', 'pageview');

function redirectTo(url) {
	window.location.replace(url);
}

function ebiz_reset() {
	$('#ebiz-result').html('');
	$('#ebiz-result').removeClass('fail');
	$('#ebiz-result').removeClass('success');
}

function ebiz_success(login_url) {
	var res = '<strong>Website was successfully created!</strong><br />';
	res += 'You are beeing automatically redirected to your new website. If nothing happes in a while, please click <a href="'+login_url+'">here</a>.';
	$('#ebiz-result').html(res);
	$('#ebiz-result').addClass('success');
	setTimeout(function () { redirectTo(login_url);}, 200);
}

function ebiz_fail(details) {
	var res = '<strong>We are sorry, but creating of your website failed.</strong><br />';
	if (details)
		res += 'Details: ' + details;
	$('#ebiz-result').html(res);
	$('#ebiz-result').addClass('fail');
}

$('#create_website').click(function(e) {
	e.preventDefault();
	$('#ebiz-loading-wrapper').show();
	ebiz_reset();
	// Track click
	ga('send', 'event', 'outbound', 'click', 'create_website_tab', {
		'nonInteraction': 1
	});
	var request = $.ajax({
		url: '/ajax/escortsbiz/create_json',
		type: 'GET',
		data: { ad_id : ad_id },
		dataType: 'json'
	});
	request.done(function(data) {
		if (data.status == 'success')
			ebiz_success(data.link);
		else
			ebiz_fail(data.detail);
	});
	request.fail(function(jqXHR, textStatus) {
		ebiz_fail(textStatus)
	});
	request.always(function(jqXHR, textStatus) {
		$('#ebiz-loading-wrapper').hide();
	});
});

{/literal}
</script>

