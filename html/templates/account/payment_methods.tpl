<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/account/account.css?20190430">

<link rel="stylesheet" href="/js/tools/jquery-confirm/css/jquery-confirm.css">
<script src="/js/tools/jquery-confirm/js/jquery-confirm.js"></script>
<script src="https://checkout.stripe.com/checkout.js"></script>

<div class="container account">

	{include "account/menu.tpl" ebiz_login_url=$ebiz_login_url ebiz_available=$ebiz_available}

	<div class="wrap">
	
		<h2>Saved Credit Cards</h2>
		<table class="table table-striped table-bordered">
			<tr>
				<th>Type</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Details</th>
				<th>Actions</th>
			</tr>
			{foreach from=$cards item=c}
				<tr>
					<td>{$c.type}</td>
					<td>{$c.firstname}</td>
					<td>{$c.lastname}</td>
					<td>{$c.label}</td>
					<td>
						<button type="button" data-cc_type="{$c.cc_type}" data-last4="{$c.last4}" data-ad_id="{$c.ad_id}" data-ad_desc="{$c.ad_desc}" data-next_renewal_date="{$c.next_renewal_date}" data-id="{$c.id}" data-email="{$account->getEmail()}" data-subscription="{$c.subscription_status}" class="btn btn-warning btn-xs change">Change</button>
						<button type="button" data-cc_type="{$c.cc_type}" data-last4="{$c.last4}" data-ad_id="{$c.ad_id}" data-ad_desc="{$c.ad_desc}" data-next_renewal_date="{$c.next_renewal_date}" data-id="{$c.id}" data-subscription="{$c.subscription_status}" class="btn btn-danger btn-xs delete-method">Delete</button>
					</td>
				</tr>
			{/foreach}
		</table>

		{include "account/help.tpl" agency=$agency}

	</div>

</div>

{literal}
<script type="text/javascript">
	$(document).ready(function() {

		var delete_ccid = 0;

		var subscription = 0;

		var key = '{/literal}{$stripe_public_key}{literal}';

		var handler = StripeCheckout.configure({
			key: key
		});

		// close stripe auth form on page navigation
		window.addEventListener('popstate', function () {
			handler.close();
		});

		//replace card
		$('.change').on('click', function (e) {
			console.log('change');
			var self = $(this);
			delete_ccid = self.data('id');
			subscription = parseInt(self.data('subscription'));

			// confirm if card has subscription
			if(subscription === 1){
				confirm_delete(self.data('cc_type'), self.data('last4'), self.data('ad_id'), self.data('ad_desc'), self.data('next_renewal_date'), change_action);
			} else {
				// open card auth form
				change_action();
			}

			e.preventDefault();

		});


		// delete card
		$('.delete-method').on('click', function (e) {
			console.log('delete');
			var self = $(this);
			delete_ccid = self.data('id');

			// confirm if card has subscription
			if(parseInt(self.data('subscription')) === 1){
				confirm_delete(self.data('cc_type'), self.data('last4'), self.data('ad_id'), self.data('ad_desc'), self.data('next_renewal_date'), delete_action);
			} else {
				$.confirm({
					title: 'Are you sure you want to delete credit card ' + self.data('cc_type') + ' ' + self.data('last4') + ' ?',
					content: '',
					useBootstrap: false,
					buttons: {
						confirm: {
							text: 'Yes',
							btnClass: 'btn-red',
							action: function (){
								delete_action();
								// self.closest('tr').remove();
							}
						},
						cancel: {
							text: 'No'
						}
					}
				});
			}

			e.preventDefault();

		});


		// functions
		var confirm_delete = function (cc_type, last4, ad_id, ad_desc, next_renewal_date, action_confirm) {

			cc_type = cc_type || '';
			last4 = last4 || '';
			ad_id = ad_id ? '#' + ad_id : '';
			ad_desc = ad_desc ? '( ' + ad_desc + ' )' : '';
			next_renewal_date = next_renewal_date ? 'on ' + next_renewal_date : '';

			$.confirm({
				title: 'Are you sure you want to delete credit card ' + cc_type + ' ' + last4 + ' ?',
				content: 'If you delete this card, your recurring payment for ad ' + ad_id + ' ' + ad_desc + ' will become inactive and will not prolong ' + next_renewal_date + ' ?',
				useBootstrap: false,
				buttons: {
					confirm: {
						text: 'Yes, delete ' + cc_type + ' ' + last4,
						btnClass: 'btn-red',
						action: action_confirm
					},
					cancel: {
						text: 'No, keep my card ' + cc_type + ' ' + last4 + ' on file'
					}
				}
			});
		};

		var delete_action = function () {
			var delete_form =  '<form action="' + window.location.href + '" method="POST">' +
				'<input type="hidden" name="delete_ccid" value="' + delete_ccid + '" >' +
				'</form>';

			$(delete_form).appendTo('body').submit();
		};

		var change_action = function () {
			handler.open({
				name: 'Yeobill',
				description: 'Add New Payment Method',
				panelLabel: 'Check Card',
				zipCode: true,
				image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
				locale: 'auto',
				billingAddress: true,
				token: function (token) {
					var url = window.location.href;
					var html =  '<form action="' + url + '" method="POST">' +
						'<input type="hidden" name="stripeToken" value="' + token.id + '" >' +
						'<input type="hidden" name="email" value="' + token.email + '" >' +
						'<input type="hidden" name="ccid" value="' + delete_ccid + '" >' +
						'<input type="hidden" name="tokenObj" value="' + encodeURIComponent(JSON.stringify(token)) + '" >' +
						'<input type="hidden" name="submitted" value="1" >' +
						'<input type="hidden" name="subscription" value="' + subscription + '" >' +
						'</form>';

					$(html).appendTo('body').submit();
				}
			});
		};

	});

</script>
{/literal}
