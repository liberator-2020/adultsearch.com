<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/account/account.css?20190430">

<div class="container account">

    {include "account/menu.tpl" ebiz_login_url=$ebiz_login_url ebiz_available=$ebiz_available}

    <div class="wrap">

		<div class="row">
			<div class="col-xs-8">
				<h2>Active subscriptions</h2>
			</div>
			{if $cards|count > 0}
				<div class="col-xs-4">
					<a id="edit-payment-method" class="btn btn-success pull-right" href="/account/payment_methods">Edit Payment Method</a>
				</div>
			{/if}
		</div>
		{if $subscriptions|count > 0}
			<table class="table table-striped table-bordered">
			<tr>
				<th>Subject</th>
				<th>Subscription Id#</th>
				<th>Amount</th>
				<th>Card</th>
				<th>Next renewal date</th>
				<th/>
			</tr>
			{foreach from=$subscriptions item=s}
			<tr>
				<td>{$s.subject}</td>
				<td>{$s.payment_id}</td>
				<td>${$s.amount}</td>
				<td>{$s.card_label}</td>
				<td>{$s.next_renewal_date}</td>
				<td><a href="?action=subscription_cancel&payment_id={$s.payment_id}" class="btn btn-xs btn-warning">Cancel</a></td>
			</tr>
			{/foreach}
			</table>
		{else}
			<em>No active subscriptions.</em>
		{/if}

		<h2>Transactions</h2>
		{if $transactions|count > 0}
			<table class="table table-striped table-bordered">
			<tr>
				<th>Date/Time</th>
				<th>Subject</th>
				<th>Transaction Id#</th>
				<th>Subscription Id#</th>
				<th>Amount</th>
				<th>Card</th>
			</tr>
			{foreach from=$transactions item=t}
			<tr>
				<td>{$t.stamp|date_format:"%m/%d/%Y %l:%M %p %Z"}</td>
				<td>{$t.subject}</td>
				<td>{$t.trans_id}</td>
				<td>{$t.payment_id}</td>
				<td>${$t.amount}</td>
				<td>{$t.card_label}</td>
			</tr>
			{/foreach}
			</table>
		{else}
			<em>No transactions.</em>
		{/if}

		{include "account/help.tpl" agency=$agency}

    </div>

</div>
