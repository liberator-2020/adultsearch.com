<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div id="cl" style="padding-left:0px;">
	<form action='/account/signup?step=1' method='post'>
	<input type="hidden" name="submit" value="1" />
		<table width="100%" cellpadding="0" cellspacing="0" summary="page">
		<tr>
			<td valign="top">
				<table width="99%" class="b" border="0" align="center" cellpadding="3" cellspacing="3" id="signupform">
				<tbody>
				<tr>
					<td colspan="2" valign="top">
						{if $error}<h2 style="color:#ff0000;">{$error}</h2>{/if}
						<span class="ContentlinksTitle">Required information for Adult Search account</span>
						<br /><br />
					</td>
				</tr>
				<tr>
					<td class="first" valign="top" nowrap="nowrap">
						<div class="createAccountTitles"><strong><span>Your email address:</span></strong> <span class="reqired">*</span></div>
					</td>
					<td class="second">
						<div>
							<input type='text' name="email" value='{$email}' id="email" class="searchinput" maxlength="100" size="30" onblur="checkemail();" oncontextmenu="return false" />
							<span id="cem" style="color:#ff0000;"></span>
						</div>
						<span>
							e.g. myname@example.com. This will be used to sign-in to your account.
						</span>
					</td>
				</tr>
				<tr>
					<td class="first">
						<div class="createAccountTitles"><strong><span class="gaia sub el">Confirm your email:</span></strong> <span class="reqired">*</span></div>
					</td>
					<td class="second">
						<input type='text' name="email_conf" value='{$email_conf}' class="searchinput" size="30" oncontextmenu="return false" />
					</td>
				</tr>
				<tr>
					<td class="first">
						<div class="createAccountTitles"><strong><span class="gaia sub pl">Create a Username:</span></strong><span class="reqired"> *</span></div>
					</td>
					<td class="second">
						<input name="username" value='{$username}' id="username" type="text" class="searchinput" maxlength="32" size="30" onblur="checkusername();" />
						<span id="cun" style="color:#ff0000;"></span>
					</td>
				</tr>
				<tr>
					<td class="first">
						<div class="createAccountTitles"><strong><span class="gaia sub pl">Choose a password:</span></strong><span class="reqired"> *</span></div>
					</td>
					<td class="second">
						<input type='password' name="password" value='{$password}' class="searchinput" maxlength="32" size="30" />
						{if $error_password eq "empty"}<strong style='color:#FF0000'>Enter your password please.</strong>{/if}
					</td>
				</tr>
				<tr>
					<td class="first" valign="top">
						<div class="createAccountTitles"> <strong><span class="gaia sub rpl">Re-enter password:	</span></strong> <span class="reqired">*</span></div>
					</td>
					<td class="second">
						<div>
							<input type='password' name="password_conf" value='{$password_conf}' class="searchinput" size="30" >
							{if $error_password eq "not_match"}<strong style='color:#FF0000'>Confirm your password please.</strong>{/if}
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2"><hr></td>
				</tr>

				<tr>
                    <td valign="top" class="first">
                        <span class="createAccountTitles"><strong>Verification: </strong></span><span class="reqired">*</span>
                    </td>
                    <td class="seconds">
						<div class="g-recaptcha" data-sitekey="{$config_recaptcha_site_key}"></div>
					</td>
				</tr>
{*
{if !$captcha_ok}	
				<tr>
					<td valign="top" class="first">
						<span class="createAccountTitles"><strong>Word Verification: </strong></span><span class="reqired">*</span>
					</td>
					<td class="seconds">
						<table>
						<tbody>
						<tr>
							<td width="6" valign="top"></td>
							<td width="312" valign="top">
								<span>Type the characters you see in the picture below.</span>
								<div>
									<img src="/captcha.php?{$time}" border="0" alt="security" width='200' height='70'/><br /><br/>
								</div>
								<input name="spam" class="searchinput" size="30" >
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<span>Letters are not case-sensitive<br /><br /></span>
							</td>
						</tr>
						</tbody>
						</table>
					</td>
				</tr>
{else}
				<input type="hidden" name="spam" value="{$spam}" />
{/if}
*}
				<tr>
					<td class="first" valign="top">
						<div class="createAccountTitles">
						<b>Terms of Service:</b>
						<span class="reqired">*</span></div>
					</td>
					<td class="second" valign="top">
						<font face="Arial, sans-serif" size="2">
						Please check the Adult Search Account information you've entered above (feel
						free to change anything you like), and review the Terms of Service.<br />
						<br />
						</font>
					</td>
				</tr>
				<tr>
					<td rowspan="2"> </td>
					<td valign="top">
						<table width="622" border="0" cellpadding="0" cellspacing="0">
						<tbody>
						<tr>
							<td>
								<font size="2">By clicking on 'I accept' below you are agreeing to the <a href="/privacy-policy" target="_blank">Privacy Policy</a>.</font>
							</td>
						</tr>
						</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td class="second">
						<br /><input type="image" src="/images/account/Btn_CreateAccount_Blu6a.png" value="I accept. Create my account." /><br /><br />
					</td>
				</tr>
				</tbody>
				</table>
			</td>
		</tr>
		</table>

		<input type="hidden" name="advertiser" value="{if $advertiser}1{else}0{/if}" />

	</form>
</div>

{literal}
<script type="text/javascript">
<!--
function checkusername() {
	_ajax("get", "/account/_ajax", "u="+jQuery("#username").val(), "cun")
}
function checkemail() {
	_ajax("get", "/account/_ajax", "e="+jQuery("#email").val(), "cem")
}
-->
</script>
{/literal}
