<div class="mgmt">

{if $error}<span class="error">{$error}</span>{/if}

<form method="post" action="" enctype="multipart/form-data">
<input type="hidden" name="update" value="1" />
	<div id="status">
		<span><a href="/classifieds/myposts" class="active">My Ads</a></span> 
		<span><a href="/account/mypage" class="active">My Account</a></span>
		<span><a href="/account/payments" class="active">Payments</a></span>
		<span><a href="/pm/index" class="active">My Private Messages</a></span>
		{if $ebiz_login_url || $ebiz_available}
		<span style="position: relative;"><a href="{if $ebiz_login_url}{$ebiz_login_url}{else}/account/mywebsite{/if}" class="active">My Website</a><img src="/images/new.png" style="position: absolute; top: -10px; left: 70px;"/></span>
		{/if}
	</div>
  <div class="general">
	<div>
		<h2>Cancel subscription #{$subscription_id} ?</h2>
		<p>
			Do you really want to cancel subscription #{$subscription_id} ?
			{if $message}
				<br />
				{$message}
			{/if}
		</p>
		<input type="hidden" name="action" value="subscription_cancel" />
		<input type="hidden" name="subscription_id" value="{$subscription_id}" />
		<button type="submit" name="go_live" value="1">Yes, cancel this subscription</button>
		<button type="submit" name="go_back" value="1">No, go back</button>
	</div>
	<br />
	<div class="help">
		For help please contact:<br />
		U.S. &amp; Canada: <a href="tel:+17029351688">1-702-935-1688</a><br />
		<a href="mailto:support@adultsearch.com">support@adultsearch.com</a>
	</div>
  </div>
</form>
</div>
