<script type="text/javascript" src="/js/intl-tel-input/js/intlTelInput-jquery.min.js"></script>
<script type="text/javascript" src="/js/account/verify.js"></script>

<link rel="stylesheet" type="text/css" href="/js/intl-tel-input/css/intlTelInput.min.css">
<style type="text/css">
	/* mobile field fix */
	.intl-tel-input {
	width: 100%;
}
</style>

{* Load intl-tel-input plugin  - see gHtmlInclude *}

<style type="text/css">
div.round_white {
	margin: 0px 12px 10px 0px;
	border-radius: 6px;
	background: #FFF;
	padding: 6px 0px 6px 8px;
	min-height: 425px;
}
.infoblock div.intl-tel-input , .infoblock div.intl-tel-input  div {
	min-height: auto;
}
.error {
	color: red;
	font-weight: bold;
}
input, button {
	padding: 2pt 4pt;
	font-size:11pt;
}
.error {
	margin-top:10pt;
}
</style>

<div class="round_white">
	<h1>Phone SMS Verification</h1>

	<p>
		To prevent Human Trafficking, we need you to verify your phone first.
	</p>
	<!-- STEP {$step} of 3 -->
	{if $step == 1 }
	<p>
		After submitting your phone number, we will send you a short text message with a verification code.<br />
		This phone number will not be displayed anywhere on the site nor in your ad unless it's the phone number you are using in your ad.<br />
	</p>
	<form id="form1" method="post" action="/account/verify">
		<input type="hidden" name="step" id="step" value="1" />
		<input type="hidden" name="origin" id="origin" value="{$origin}" />
		<input type="hidden" name="ad_id" id="ad_id" value="{$account_id}" />
		<div id="phone_select">
			<h3>Phone number:</h3>
			<input type="text" id="phone" name="phone" size="25" value=""/><br />
			<span id="sms_phone_hint"></span></br>
			{if $error_message}<div id="sms_error_1" class="error">{$error_message}</div>{/if}
		</div>
		<br style="clear:both">
		<button type="submit" name="request_sms_code">Request verification code</button>
	</form>

	{/if}


	{if $step == 2}
		<p>
			A short text message has been sent to your cellphone <b>{$phone}</b>.<br>
			Please enter the 4-digit verification code in the text message in the field below.
		</p>
		<p>
			To change phone number <a href="/account/verify?origin={$origin|escape:'url'}">click here</a>.
		</p>

		<form id="form2" method="post" action="/account/verify">
			<input type="hidden" name="phone" value="{$phone}">
			<input type="hidden" name="token" value="{$token}">
			<input type="hidden" name="step" value="2">
			<input type="hidden" name="ad_id" id="ad_id" value="{$account_id}" >
			<input type="hidden" name="origin" value="{$origin}">
			<h3>Verification code:</h3>
			<input style="text-align:center" type="text" id="sms_code" name="sms_code" size="10" maxlength="4" value="" autocomplete="off">
			<input type="submit" name="verify_sms_code" value="Verify code">
			{if $error_message}<div id="sms_error_1" class="error">{$error_message}</div>{/if}
		</form>
	{/if}


	{if $step == 3}
		<p>
			Verification was successful, please <a href="{$origin}">click here to continue </a>.
		</p>
		{*
		<form id="form3" method="get" action="">
			<input type="hidden" name="origin" value="{$origin}" />
			<input type="submit" name="submit" value="Continue" />
			<input type="hidden" name="ad_id" value="{$account_id}" />
		</form>
		*}
	{/if}
	

	<br /><br />
	If you have any trouble with phone verification, please  <a href="/contact?where=%2Faccount%2Fverify">contact us</a>. Thanks!
</div>
