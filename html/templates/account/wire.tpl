<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/account/account.css?20190430">

<div class="container account">

    {include "account/menu.tpl" ebiz_login_url=$ebiz_login_url ebiz_available=$ebiz_available}
	
	<div class="wrap">
	
		<h2>Wire sent form</h2>
		
		<h3>Let us know you sent us wire</h3>
		<p>
		After you wire us money, you need to submit form below. Do not send us emails anymore.<br />
		If you don't fill out form below, it might take days or weeks to add your money to the budget.<br />
		All fields are mandatory.
		</p>

		<form method="post" action="" enctype="multipart/form-data" class="form-horizontal" data-ajax="false">

			{if $error}<div style="color: red; font-weight: bold;">{$error}</div>{/if}

			<div class="form-group">
				<label class="col-sm-3 control-label">Senders Name:</label>
				<div class="col-sm-9">
					<input type="text" name="name" id="name" value="{$name}" class="form-control" style="max-width: 400px;" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Account #ID:</label>
				<div class="col-sm-9">
					<input type="text" name="account_id" id="account_id" value="{$account_id}" disabled="disabled" class="form-control" style="max-width: 100px;"/>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Wire Amount:</label>
				<div class="col-sm-9">
					<input type="text" name="amount" id="amount" value="{$amount}" class="form-control" style="max-width: 200px;" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Bank sent from:</label>
				<div class="col-sm-9">
					<input type="text" name="bank_name" id="bank_name" value="{$bank_name}" class="form-control" style="max-width: 400px;" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Wire receipt image</label>
				<div class="col-sm-9">
					<input type="file" name="image" id="image" class="form-control" style="max-width: 400px;" />
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-9 col-sm-offset-3">
					<input type="submit" name="submit" value="Submit" class="btn btn-primary"/>
		    		<a href="/account/mypage" class="btn btn-default" data-ajax="false">Cancel</a>
				</div>
			</div>

		</form>

		{include "account/help.tpl" agency=$agency}

	</div>

</div>
