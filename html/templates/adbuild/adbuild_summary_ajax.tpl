<div id="absa" {if $localert}data-localert="{$localert}"{/if}>
	
{if !$editonly}
	<div class="title">Summary</div><br />
	{$cat_name}, 30-days
	<ul>
	{section name=s loop=$selectedloc}
	{assign var=sl value=$selectedloc[s]}
		<li>
			<span class="remove"><a href="#" rel="{{$sl.loc_id}}" class="cart" onclick="cart_process('{$sl.type}', '{$sl.loc_id}', '{$ad_id}'); return false;">X</a></span> 
			{if $sl.post_price>0}${$sl.post_price}/month {/if}{$sl.loc_name}
		</li>
	{sectionelse}
		<span class="error">A Location Is Required</span>
	{/section}
	</ul>
	<br />

	{if $total}
		<div class="title">Total: ${$total}/month{if $exchange} ({$exchange}){/if}<br /></div>
	{/if}
	<br />

	{if $total && $adbuild_step && $adbuild_step < 3}
		<span class="continue">
			<a href="step{$adbuild_step+1}{if $ad_id}?ad_id={$ad_id}{/if}" onclick="window.location=$('#nextstep').attr('href'); return false;">
				<img src="/images/adbuild/btn_continue.png" width="136" height="40" />
			</a>
		</span>
	{/if}

	<br /><br />
{/if}

	<div style="width: 180px; font-size: 1.2em; font-weight: bold;">
            For help please contact:<br /><br />
			U.S. &amp; Canada:<br />
            1-702-935-1688<br />
			<span style="font-weight: normal; font-size: 0.9em;">(9am to 5pm EST Mon - Fri)</span>
			<br /><br />
			<img src="/images/email_address.png" style="width: 180px; height: 20px;"/>
    </div>
</div>
<br />
