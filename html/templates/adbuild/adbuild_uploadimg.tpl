<!DOCTYPE HTML>
<html lang="en">
<head>
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
<meta charset="utf-8">
<title>jQuery File Upload</title>
<meta name="viewport" content="width=device-width">
<script type="text/javascript" src="/js/jquery.js?183"></script>
<script type="text/javascript" src="/js/ui/jquery-ui-1.9.1.custom.min.js"></script>
<link href="/js/ui/start/jquery-ui-1.9.1.custom.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/css/adbuild/bootstrap.min.css">
<link rel="stylesheet" href="/css/adbuild/bootstrap-responsive.min.css">
<link rel="stylesheet" href="/css/adbuild/bootstrap-image-gallery.min.css">
<link rel="stylesheet" href="/css/adbuild/jquery.fileupload-ui.css">
<!--[if lt IE 9]><script src="html5.js"></script><![endif]-->
<style>
{literal}
.name {
        max-width: 80px;
        overflow: hidden;
        text-overflow: ellipsis;
}
.fade.in {
        background: #FFFFB9;
}
.text {
        font-family: Verdana, Geneva, sans-serif;
        color: #000000;
        margin-bottom: 10px;
}
{/literal}

{if !$ie}
{literal}
body{ background: url(/images/adbuild/bg.png) no-repeat #FFFFB9; }
{/literal}
{else}
{literal}
body { background-color:#FFFFB9; }
{/literal}
{/if}
</style>

</head>
<body>

<div>

<div> 
  <br>
  <form id="fileupload" action="/adbuild/upload?ad_id={$ad_id}" method="POST" enctype="multipart/form-data">
 <input type="hidden" name="ad_id" value="{$ad_id}" /> 
   <div class="text">
   {if $ie}Your first image will also be your thumbnail. You can edit it after submitting your ad. 
Browse & upload up to 8 images below.{else}
   Your <strong>first</strong> image will also be your <strong>thumbnail</strong>. You can edit it after submitting your ad.<br>
 Drag &amp; Drop up to 8 images below - OR - Click the green button to browse &amp; select.{/if}<br>
    </div>
     <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
<div class="row fileupload-buttonbar">
            <div class="span7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="icon-plus icon-white"></i>
                    <span>Browse For Files</span>
                    <input type="file" name="files[]" multiple>
                </span>
                <!-- <button type="submit" class="btn btn-primary start">
                    <i class="icon-upload icon-white"></i>
                    <span>Upload All</span>
                </button> -->
               <!--  <button type="reset" class="btn btn-warning cancel">
                    <i class="icon-ban-circle icon-white"></i>
                    <span>Cancel All</span>
                </button>
                <button type="button" class="btn btn-danger delete">
                    <i class="icon-trash icon-white"></i>
                    <span>Delete Selected</span>
                </button> 

                <input type="checkbox" class="toggle">
                Select All My  Images -->
</div>
            <div class="span5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="bar" style="width:0%;"></div>
                </div>
                <!-- The extended global progress information -->
                <!-- <div class="progress-extended">&nbsp;</div> -->
            </div>
        </div>
        <!-- The loading indicator is shown during file processing -->
        <div class="fileupload-loading"></div>
        <br>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped"><tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery"></tbody></table>
    </form>
</div>

</div>

<!-- modal-gallery is the modal dialog used for the image gallery -->
<div id="modal-gallery" class="modal modal-gallery hide fade" data-filter=":odd">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h3 class="modal-title"></h3>
    </div>
    <div class="modal-body"><div class="modal-image"></div></div>
    <div class="modal-footer">
        <a class="btn modal-download" target="_blank">
            <i class="icon-download"></i>
            <span>Download</span>
        </a>
{*        <a class="btn btn-success modal-play modal-slideshow" data-slideshow="5000">
            <i class="icon-play icon-white"></i>
            <span>Slideshow</span>
        </a> *}
        <a class="btn btn-info modal-prev">
            <i class="icon-arrow-left icon-white"></i>
            <span>Previous</span>
        </a>
        <a class="btn btn-primary modal-next">
            <span>Next</span>
            <i class="icon-arrow-right icon-white"></i>
        </a>
    </div>
</div>

<!-- The template to display files available for upload -->
{literal}
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="preview"><span class="fade"></span></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        {% if (file.error) { %}
            <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
            <td>
                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
            </td>
            <td class="start">{% if (!o.options.autoUpload) { %}
                <button class="btn btn-primary">
                    <i class="icon-upload icon-white"></i>
                    <span>Start</span>
                </button>
            {% } %}</td>
        {% } else { %}
            <td colspan="2"></td>
        {% } %}
        <td class="cancel">{% if (!i) { %}
            <button class="btn btn-warning">
                <i class="icon-ban-circle icon-white"></i>
                <span>Cancel</span>
            </button>
        {% } %}</td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        {% if (file.error) { %}
            <td></td>
            <td class="name"><span>{%=file.name%}</span></td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else { %}
            <td class="preview">{% if (file.thumbnail_url) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" rel="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}" width="75"></a>
            {% } %}</td>
            <td class="name">
                <a href="{%=file.url%}" title="{%=file.name%}" rel="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
            </td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            <td colspan="2"></td>
        {% } %}
        <td class="delete">
            <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}">
                <i class="icon-trash icon-white"></i>
                <span>Delete</span>
            </button>
           
        </td>
    </tr>
{% } %}
</script>
{/literal}

{*
<script src="http://blueimp.github.com/jQuery-File-Upload/js/vendor/jquery.ui.widget.js"></script>
<script src="/js/adbuild/js/test/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="/js/adbuild/js/test/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="/js/adbuild/js/test/canvas-to-blob.min.js"></script>
<script src="/js/adbuild/js/test/bootstrap-image-gallery.min.js"></script>

<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/js/adbuild/js/test/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/js/adbuild/js/test/jquery.fileupload.js"></script>
<!-- The File Upload file processing plugin -->
<script src="/js/adbuild/js/test/jquery.fileupload-fp.js"></script>
<!-- The File Upload user interface plugin -->
<script src="/js/adbuild/js/test/jquery.fileupload-ui.js"></script>
<!-- The main application script -->



<script src="http://blueimp.github.com/JavaScript-Templates/tmpl.min.js"></script>
<script src="http://blueimp.github.com/JavaScript-Load-Image/load-image.min.js"></script>
<script src="http://blueimp.github.com/JavaScript-Canvas-to-Blob/canvas-to-blob.min.js"></script>
<script src="http://blueimp.github.com/cdn/js/bootstrap.min.js"></script>
<script src="http://blueimp.github.com/Bootstrap-Image-Gallery/js/bootstrap-image-gallery.min.js"></script>
*}

<script src="/js/adbuild/js/jquery.ui.widget.js"></script>
<script src="/js/adbuild/js/tmpl.min.js"></script>
<script src="/js/adbuild/js/load-image.min.js"></script>
<script src="/js/adbuild/js/canvas-to-blob.min.js"></script>
<script src="/js/adbuild/js/bootstrap.min.js"></script>
<script src="/js/adbuild/js/bootstrap-image-gallery.min.js"></script>
<script src="/js/adbuild/js/jquery.iframe-transport.js"></script>
<script src="/js/adbuild/js/jquery.fileupload.js"></script>
<script src="/js/adbuild/js/jquery.fileupload-fp.js"></script>
<script src="/js/adbuild/js/jquery.fileupload-ui.js"></script>



<script type="text/javascript">
$(function () {
    'use strict';

    $('#fileupload').fileupload({ 'url':'/adbuild/upload?ad_id={$ad_id}' });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );


        $.ajax({
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).done(function (result) {
            if (result && result.length) {
                $(this).fileupload('option', 'done')
                    .call(this, null, { result: result});
            }
        });
});

</script>

<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
<!--[if gte IE 8]><script src="js/adbuild/js/cors/jquery.xdr-transport.js"></script><![endif]-->
</body>
</html>
