{include file="adbuild/adbuild_status.tpl"}

<style type="text/css">
div.loginblock input {
	width: 200px;
}
</style>

<div id="build1">
	<div id="calculator">
		{include file="adbuild/adbuild_summary_ajax.tpl"}
	</div>

	<form method="post" action="">

		<input type="hidden" name="login" value="1" />
		<input type="hidden" name="origin" value="{$config_site_url}/adbuild/step4?ad_id={$ad_id}" />

		<div class="infoblock loginblock">
			<span class="title1">Login Or Register to Checkout &amp; Publish Your Ad</span>
			<br /><br />
			{if $error}<span class="error">{$error}</span>{/if}
			{if !$error && $login_error}<span class="error">{$login_error}</span>{/if}
			<div>
			<span class="title2">Login to Your Account<br /></span>
				<input value="{$login_email}" placeholder="Email or Username" name="login_email" />
				<br />
				<input type="password" value="{$login_password}" name="login_password" />
				<br />
				<a href="http://adultsearch.com/account/reset" target="_blank">Lost Password </a>
			</div>
			<div class="register">
				<span class="title2">Or Create Your Account<br /></span>
				<label><span class="question"><span class="hint">This is only for your account admin and will not be displayed on your ad.<span class="hint-pointer">&nbsp;</span></span></span>Your Email</label>
				<input name="account_email" value="{$account_email}" />
				<br />
				<label><span class="question"><span class="hint">Maximum 32 characters. <span class="hint-pointer">&nbsp;</span></span></span>Create Username</label>
				<input name="account_username" value="{$account_username}" />
				<br />
				<label><span class="question"><span class="hint">Maximum 32 characters.<span class="hint-pointer">&nbsp;</span></span></span>Create Password</label>
				<input type="password" name="account_password" />
				<br />
				<label>Retype Password</label>
				<input type="password" name="account_password2" />
				<br />
			</div>
			<br style="clear: both;" />

			<button type="submit" class="btn_adbuild_continue" style="display: block; margin: auto; margin-top: 20px;">Continue</button>
		</div>
	</form>

</div>

<br style="clear: both;" /><br /><br />
