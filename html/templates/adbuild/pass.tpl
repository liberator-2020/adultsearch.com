<div class="confirmation">

	<div class="title">Pass ad to other person</div>

{if $error}
	<div style="color: red;">{$error}</div>
{/if}

	<ul>
	<li style="width: 100% !important;">
	{if !$passed}
		<h1>New ad owner:</h1>
		Please put in email address or account #id and pick from suggested options:<br />
		<form action="" method="post">
		<input type="search" name="new_ad_owner" id="new_ad_owner" value="" size="80" style="background-color: white;"/><br />
		<input type="hidden" name="new_account_id" id="new_account_id" value="" />
		<input type="submit" name="submit" id="submit" value="Submit" />
		</form>
	{else}
		You succesfully chanegd ownership of the ad {$ad_id} to account {$account_label}.<br />
		{if $step4_link}
			<br />
			<textarea cols="100" rows="5" style="background-color: white;">
You can purchase your ad by clicking on following link, filling out your personal details, clicking continue button and then entering payment information on the secured website of our payment processor:
{$step4_link}
			</textarea>
		{/if}
	{/if} 
	</li>
	</ul>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$('input[type="submit"]').prop('disabled', true);
	$('#new_ad_owner').autocomplete({
      source: '/_ajax/account_search',
      minLength: 2,
      select: function( event, ui ) {
//		console.log(ui);
		if (ui.item.id) {
			$('#new_account_id').val(ui.item.id);
			$('input[type="submit"]').prop('disabled', false);
		} else {
			alert('Error!');
		}
      }
    });	
});
</script>
