<form method="get" action="/adbuild/step2" id="step1">
{if $ad_id}<input type="hidden" name="ad_id" value="{$ad_id}" />{/if}

{include file="adbuild/adbuild_status.tpl"}

{if $error}
	<br /><br />
	<div class="error">{$error}</div>
{/if}

<div id="build1" class="build2">
	<div id="category">

{*
<div><span class="error">Due to maintenance of our servers, it is not possible post a new ad today. Please come back tomorrow</span></div>
*}

		<div class="title">1. Select A Section For Your Ad: <br /><br />
			<div id="pickcategory" class="error" style="display:none">You must select a category to continue.</div>
		</div>
		<div>
			Escorts:<br />
			<input name="category" type="radio" value="1" {if $adbuild_type==1}checked="checked"{/if}/>	Female Escort<br />
			<input name="category" type="radio" value="2" {if $adbuild_type==2}checked="checked"{/if}/>	TS / TV Shemale Escort<br />
			<br />
		</div>
		<div>
			Or Other Adult Services:<br />
			<input name="category" type="radio" value="6" {if $adbuild_type==6}checked="checked"{/if}/>	Female Bodyrubs<br />
		</div>
		<div>
			&nbsp;&nbsp;
		</div>
		<div style="width: 180px;">
			<span style="font-size: 1.2em; font-weight: bold;">
			For help please contact:<br /><br />
			</span>
			<span style="font-size: 0.9em; font-weight: bold;">
			U.S. &amp; Canada:<br />
        	1-702-935-1688<br />
			(9am to 5pm EST Mon - Fri)
			</span>
			<br /><br />
			<img src="/images/email_address.png" style="width: 180px; height: 20px;"/>
			<br /><br />
		</div>
		
		<button type="submit" class="btn_adbuild_continue" style="display: block; margin: auto; margin-bottom: 10px;">Agree &amp; Continue</button>
		<span style="display: block; margin: auto; text-align: center; font-weight: bold; text-transform: uppercase;">By continuing, you agree to the terms below.</span>

	</div>
	<div id="rules"> <br />
		<br />
        
    <div class="title">You Agree to the Following When Posting on AdultSearch:</div>
		<br>
		<ul>
		<li>I will not post obscene or lewd and lascivious graphics or	 photographs which depict genitalia or actual or simulated sexual acts;</li>
		<li>I will not post any solicitation directly or in "coded"	 fashion for any illegal service exchanging sexual favors for money or	 other valuable	 
consideration;</li>
		<li>I will not post any material on the Site that exploits minors in any way;</li>
		<li>I will not post any material on the Site that in any way constitutes or assists in human	 trafficking;</li>
		<li>I am <strong>at least 21 years of age</strong> or older and not considered to be a minor in my state of	 residence</li>
		</ul>

		<div class="title"> Rules: </div>
		<p>Any post containing images or contact information that we deem to be misleading or invalid for the category in which it was posted will be removed with no refund.</p>
		<p style="color: red;">We &#39;Do Not&#39; allow any ads of a spamming nature. We will share data with Slixa and Eros regarding any violators of these rules.</p>

		<div id="ssl">
			
			<span><img width="15" height="18" id="lock" alt="SSL" src="{$config_site_url}/images/i_secure.png" />
			All transactions are 			discreet &amp; secured with SSL Certificate encryption.</span><br /><br />
      <div>
				<img width="46" height="25" id="mc" alt="Mastercard" src="{$config_site_url}/images/i_mastercard.png" />
				<img width="46" height="25" id="visa" alt="Visa" src="{$config_site_url}/images/i_visa.png" />
{*
  				<img width="46" height="25" id="amex" alt="American Express" src="{$config_site_url}/images/i_amex.png" />
				<img width="46" height="25" id="discover" alt="Discover" src="{$config_site_url}/images/i_discover.png" />
*}
			</div>
			<div class="cards">
				<span id="siteseal">
					<script type="text/javascript" src="https://seal.starfieldtech.com/getSeal?sealID=6pvUdl4f9Rtkdbd9ezSTPMwOCVzfH3x9ZkWQUkJFk3UajmExXMp4hYJ9"></script>
				</span>
			</div>
			<br />
		</div>

		<p>Any post exploiting a minor in any way will be subject to criminal prosecution and will be reported to the <a href="http://www.cybertipline.com" target="_blank" rel="nofollow">CyberTipline</a>. The poster will be caught and the police will prosecute the poster to the full extent of the law.</p>
		<p>Any post with terms or misspelled versions of terms implying an illegal service will be rejected. Examples of such terms include without limitation: 
		'greek', ''gr33k", bbbj', 'blow', 'trips to greece', etc. </p>
		<p>You may not post ads including following content; "exchange of sexual favors for money", "use code words such as 'greek', 'bbbj', 'blow', 'trips to greece', etc.", "post obscene images, e.g. explicit genitalia, sex acts, erect penises, etc.", "post content which advertises an illegal service".</p>
	  <p>Postings violating these rules and our Terms of Use are subject to removal without refund.</p>
		<p><strong>By clicking &quot;continue,&quot; you agree to abide by these rules as well as AdultSearch's terms of use. {if !$ismember}<br>You will need to <a href="https://adultsearch.com/account/signin">login</a> or <a href="https://adultsearch.com/account/signup">register</a> prior to completing your ad.{/if}</strong></p>
		<p>&nbsp;</p>

	</div>
	<button type="submit" class="btn_adbuild_continue" style="display: block; margin: auto;">Agree &amp; Continue</button>
	<div style="clear:both"></div>

</div>

</div>

{$csrf}
</form>

<script type='text/javascript'>
var cat;
$('#step2').click(function(event) { 
	event.preventDefault();
	cat = $("input[name=category]:checked", "#step1").val();
	if( cat === undefined ) {
		$('#pickcategory').show();
		return false;
	}
	$("#step1").submit();
});
</script>
