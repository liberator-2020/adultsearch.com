{include file="adbuild/adbuild_status.tpl"}
	
<div id="build1"> 
	<div id="calculator">
		{include file="adbuild/adbuild_summary_ajax.tpl"}
	</div>

	<div id="geo">
		<div class="title">2. Select Location(s) For Your Ad:<br /> </div>
		<ul>
			<li>All prices are for a 30-day	period.	Payments are recurring, so will charge you every 30 days for a new 30 days period. You can cancel this subscription anytime (for example right after posting an ad - so your payment will effectively be non-recurring).</li>
			<li>Maximum 2 locations. You can choose multiple states/countries from the dropdown.</li>
			<li>You can add visiting dates for chosen locations in the next section.</li>
		</ul>
		<br />
 
		<div id="geoLinks">
			<script> function showcities(id) { $("div.cities").hide(); $("#_city"+id).show(); } </script>
			<div>
				<select name="navigation" id="navigation" onchange="showcities($(this).val());">
					<option selected="selected">-Choose Location-</option>
					{foreach from=$sel item=foo}
						<option disabled="disabled">{$foo.name}</option>
						{section name=s1 loop=$foo.list}{assign var=s value=$foo.list[s1]}
						<option value="{$s.loc_id}"{if $curloc==$s.loc_id} selected="selected"{/if}>{$s.loc_name}</option>
						{/section}
						<option disabled="disabled"></option>
					{/foreach}
				</select>
			</div>

			{foreach from=$hide item=foo key=key}
			{assign var=half value=$foo|@count}
				{section name=h1 loop=$foo}
				{assign var=a value=$foo[h1]}
					{if $smarty.section.h1.index == 0}<div {if $curloc!=$a.state}style="display:none"{/if} id="_city{$a.state}" class="cities"><ul>{/if}
					<li>
						<input type="checkbox" id="loc_{$a.loc_id}" name="loc_id" value="{$a.loc_id}" class="pickloc" />
						<a href="https://{$a.sub}adultsearch.com{$a.loc_url}" target="_blank" style="color: #186299;">{$a.loc_name}</a> (${$a.post_price} USD/month)
					</li>
					{if $half % 2 == 0 && $smarty.section.h1.index == $half/2-1}
						</ul><ul>
					{elseif $half % 2 == 1 && $smarty.section.h1.index == ($half-1)/2}
						</ul><ul>
					{/if}
				{/section}
				</ul>
				</div>
			{/foreach}

		</div>

		<br style="clear: both;"/>

		<div>
			<a id="nextstep" href="step3?ad_id={$ad_id}" data-href="step3?ad_id={$ad_id}" class="btn_adbuild_continue" style="display: block; margin: auto; margin-bottom: 10px;">Continue</a>
			<div style="color: red; font-size: 11px; font-weight: normal; text-align: center; text-transform: none;">
			NOTE: We &#39;Do Not&#39; allow any ads of a spamming nature. We will share data with Slixa and Eros regarding any violators of these rules.
			</div>
		</div>

	</div> 

</div>

<script type="text/javascript">
<!--
function toggle_visibility(id) {
	var e = document.getElementById(id);
	if(e.style.display == 'block')
		e.style.display = 'none';
	else
		e.style.display = 'block';
}

function toggle_continue() {
	var location_picked = ($(':checkbox:checked').length > 0);
	if (!location_picked) {
		$('#nextstep').removeAttr('href');
		$('#nextstep').attr('onclick', "location_alert(); return false;");
	} else {
		$('#nextstep').attr('href', $('#nextstep').attr('data-href'));
		$('#nextstep').attr('onclick', '');
	}
}
function location_alert() {
	alert('You need to select location first');
	return false;
}

$("input.pickloc").click(function() { 
	var what = $(this).attr("checked") ? "addloc=" : "removeloc=";
	var val = $(this).val();
	jQuery.post(
		"/adbuild/loc?ad_id={$ad_id}", 
		what + val,
		function(data){
			jQuery("#calculator").html(data);
			var localert = $('#absa').attr('data-localert');
			if (localert) {
				if (localert == 1)
					alert('You are allowed to post one ad only in 1 location, unless you are proven advertiser. Please log in to your account or contact us at support@adultsearch.com');
				else if (localert == 2)
                    alert('You are allowed to post one ad only in 1 location, unless you are proven advertiser. If you need to post in more locations, please contact us at support@adultsearch.com');
				else if (localert == 3)
                    alert('You are allowed to post one ad only in 2 different states. If you need to post in more locations, please contact us at support@adultsearch.com');
				$('#loc_'+val).attr("checked", false);
			}
		}, 
		"json"
	);
	toggle_continue();
});

$(document).ready(function() { 
	$("#calculator a.cart").each(function() { 
		$("input.pickloc[value="+$(this).attr('rel')+"]").attr("checked", true);
	});
	toggle_continue();
});

//-->
</script>

