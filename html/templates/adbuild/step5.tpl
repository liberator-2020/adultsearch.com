<style type="text/css">
.wide_yellow_box {
	background-color: #FFFFB9;
	border-radius: 10px;
	margin: 10px 65px 10px 65px;
	padding: 15px;
	font-size: 16px;
}
.post_warning {
	display: block;
	text-align: center;
	font-weight: bold;
	color: #ff0000;
	text-shadow: rgba(148, 2, 2, 0.4) 1px 1px;
	font-size: 20px;
}
</style>

<div class="confirmation">

	{if $done == 3}
		<div class="title">Confirmation: Your Ad Is Going To Be Live Soon</div>
	    <div class="wide_yellow_box">
			<span class="post_warning">Your ad can take up to 24 hours to go live.</span>
			<script type="text/javascript">
			var blinked = 0;
			var timer = null;
			function blinker() {
				$('.post_warning').fadeOut(100);
				$('.post_warning').fadeIn(100);
				blinked++;
				if (blinked > 4) {
					window.clearInterval(timer);
				}
			}
			timer = setInterval(blinker, 300);
			</script>
		</div>
	
		<div class="wide_yellow_box">	
			<b>We have NOT yet charged your credit card</b>, we put only temporary authorisation on your card while we review your ad.<br />
			If your ad is acceptable with our terms we will make it live and visible to other users. Only when your ad is live will you be charged the full amount.<br />
			We usually require 24 hours to review the ad, or you can contact customer service on the live chat below for a faster approval. You may be asked to provide ID to prove your age. If your verification will not be resolved in 5 days, the authorisation will be removed by your bank automatically.
		</div>
	{else}
		<div class="title">Confirmation: Your Ad Is Now Live</div>
	{/if}
	<ul>
	<li>
		<h1>View Your Ad</h1>
		Thank you for posting your ad on AdultSearch.com!<br />
		<a href="/classifieds/look?id={$ad_id}" target="_blank">View Your Ad</a></li>
	<li class="edit">
		<h1>Edit Your Ad</h1>
	<img src="/images/adbuild/conf_edit.png" alt="" width="211" height="84" /> You can always login to edit your ad later. <br />
		<a href="/adbuild/step3?ad_id={$ad_id}">Edit Now</a><br />
	</li>

	{if $thumb}
	<li class="thumbnail">
		<h1>Edit Your Thumbnail</h1>
		<a href="/classifieds/thumbnail?id={$ad_id}">
			<img src="{$config_image_server}/classifieds/{$thumb}?{$current}" width="75" height="75" /> <br />Edit This Thumbnail
		</a>
	</li>
	{/if}

	{if $smarty.session.account==3974 && $ebiz_show}
	<li>
		<h1>Create your website in 1-click!</h1>
		<div id="ebiz_response">
			<p>We offer you to create website for you at our partner <a href="http://escorts.biz" target="_blank">Escorts.biz</a>. All the data you submitted to the classified ad will be automatically used in your website (description, phone number, photos, etc...). You will be automatically registered with the same email and password you have at adultsearch.com. So easy! Just click on link below.</p>
			<a href="#" onclick="ebizCreateWebsite({$ad_id}); return false;">Create my website at Escorts.biz</a>
		</div>
	</li>
	{/if}

	{if $done == 1}
	<li>
		<h1>Share Your Ad </h1>
		Post your ad right now to your social media.
		<br />
		<br />
	 <a href="http://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fadultsearch.com%2Fclassifieds%2Flook?id={$ad_id}" onclick="javascript:window.open(this.href, '',
'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/images/classifieds/facebook_32.png" alt="My AdultSearch on Facebook"
width="32" height="32" /></a>

	<a href="https://twitter.com/share?url=http%3A%2F%2Fadultsearch.com%2Fclassifieds%2Flook?id={$ad_id}" onclick="javascript:window.open(this.href, '',
'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="custom-tweet-button"> <img src="/images/classifieds/twitter_32.png" alt="My
AdultSearch on Twitter" width="32" height="32" /></a>

<a href="https://plus.google.com/share?url=http://adultsearch.com/classifieds/look?id={$ad_id}" onclick="javascript:window.open(this.href,	'',
'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/images/classifieds/gplus_32.png" alt="Share on Google+"	/></a>

 <a href="mailto:?subject=My AdultSearch Post&amp;body=Check out my ad on AdultSearch http://adultsearch.com/classifieds/look?id={$ad_id}"
onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" title="Share by Email"><img
src="/images/classifieds/email_32.png" alt="My AdultSearch on Email" width="32" height="32" /></a>
	</li>
	{/if}

	<li>
		<h1>For help please contact:</h1>
		U.S. &amp; Canada:<br />
		<strong>1-702-935-1688</strong> (9am to 5pm EST Mon - Fri)<br />
		<img src="/images/email_address.png" style="width: 180px; height: 20px;"/>
	</li>

	</ul>
		
</div>

<script type="text/javascript">
function ebizCreateWebsite(ad_id) {
	_ajax('GET', '/_ajax/ebiz_create', 'ad_id='+ad_id, 'ebiz_response');	
}
</script>
