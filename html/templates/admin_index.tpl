<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>AS Admin</title>
	<link rel="icon" type="image/png" href="/favicon.ico">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="/js/ui/jquery-ui-1.9.1.custom.min.js"></script>
	<link href="{$config_site_url}/js/ui/start/jquery-ui-1.9.1.custom.min.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	<script src="/js/main.js"></script>
	<script src="/js/admin.js"></script>
	<link href="/css/admin.css?20190909" rel="stylesheet" type="text/css" />
	{$html_head_include}
	{if $isworker}
    <script>
        window.isWorker      = true;
        window.directCallUrl = '{$config_direct_call}';
    </script>
	{/if}
</head>

<body>

	<!-- Fixed navbar -->
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/"><img src="/images/mobile/as_logo_mobile_short.png" /></a>
		</div>

<ul class="nav navbar-nav">
	{if permission::has("access_admin")}<li><a href="/mng/home" style="padding-left: 0px; padding-right: 0px;"><i class="glyphicon glyphicon-home"></i></a></li>{/if}
	{if permission::has("access_admin_accounts")}<li><a href="/mng/accounts">Accounts</a></li>{/if}
	{if permission::has("")}<li><a href="/advertise">Advertising</a></li>{/if}

{*
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onclick="return false;">Advertisers <span class="caret"></span></a>
	<ul class="dropdown-menu">
		{if permission::has("")}<li><a href="/advertise/advertisers">Advertisers</a></li>{/if}
		{if permission::has("")}<li><a href="/advertise/zones">Advertise sections</a></li>{/if}
		{if permission::has("")}<li><a href="/advertise/stats">Popunder stats</a></li>{/if}
	</ul>
</li>
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onclick="return false;">BP Promo <span class="caret"></span></a>
	<ul class="dropdown-menu">
		{if permission::has("")}<li><a href="/mng/bppromo_adslot">Ad slots</a></li>{/if}
		{if permission::has("bppromo_manage")}<li><a href="/mng/bppromo_ad">Ads</a></li>{/if}
		{if permission::has("bppromo_manage")}<li><a href="/mng/bppromo_images">Ad Images</a></li>{/if}
		{if permission::has("bppromo_manage")}<li><a href="/mng/bppromo_stats">Stats</a></li>{/if}
		{if permission::has("bppromo_manage")}<li><a href="/mng/bppromo_ukraine">Old images</a></li>{/if}
	</ul>
</li>
*}
<li class="dropdown">
	<a href="/mng/classifieds" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onclick="location.href = '/mng/classifieds'">Classifieds <span class="caret"></span></a>
	<ul class="dropdown-menu">
		{if permission::has("access_admin_classifieds")}<li><a href="/mng/classifieds">Classifieds</a></li>{/if}
		{if permission::has("classified_manage")}<li><a href="/mng/classifieds?status=3">Pending classifieds</a></li>{/if}
		{if permission::has("promocode_manage")}<li><a href="/mng/promocodes">Promocodes</a></li>{/if}
		{if permission::has("access_admin_cl_stats")}<li><a href="/mng/cl_report">CL Stats</a></li>{/if}
		{if permission::has("access_admin_classifieds")}<li><a href="/mng/clad_city_thumbnails">City thumbnails stats</a></li>{/if}
		{if permission::has("access_admin_classifieds")}<li><a href="/mng/clad_side_sponsors">Side sponsor stats</a></li>{/if}
		{if permission::has("access_admin_classifieds")}<li><a href="/mng/clad_sticky_sponsors">Sticky sponsor stats</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/clad_clicks">Click stats</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/bwlist?type=0">Blacklist</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/bwlist?type=1">Whitelist</a></li>{/if}
		{if permission::has("classified_manage")}<li><a href="/mng/clexport">Export</a></li>{/if}
		{if permission::has("classified_ter_check")}<li><a href="/mng/clad_ter_check">TER Check</a></li>{/if}
		{if permission::has("classified_ter_check")}<li><a href="/mng/ebiz_ter_check">EBIZ TER Check</a></li>{/if}
		{if permission::has("classified_ter_check")}<li><a href="/mng/ts_ter_check">TS TER Check</a></li>{/if}
		{if permission::has("classified_managek")}<li><a href="/mng/ad_mass_update">Ad Mass Update</a></li>{/if}
		<li class="separator"></li>
		{if permission::has("access_admin_cl_stats")}<li><a href="/mng/clrep_paidvsfree">Paid vs. Free</a></li>{/if}
		{if permission::has("access_admin_cl_stats")}<li><a href="/mng/clrep_promocodes">Report Promocodes</a></li>{/if}
		<li class="separator"></li>
		{if permission::has("classified_manage")}<li><a href="/mng/phone_whitelist">Phone whitelist</a></li>{/if}
	</ul>
</li>
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onclick="return false;">Directory <span class="caret"></span></a>
	<ul class="dropdown-menu">
		{if permission::has("edit_all_places")}<li><a href="/mng/unidir_explorer">Places</a></li>{/if}
		{if permission::has("edit_all_places")}<li><a href="/mng/place_duplicities">Place duplicities</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/attributes">Attributes</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/location_attributes">Location Attributes</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/unify_db">Unify DB</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/videos">Videos</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/forums">Forums</a></li>{/if}
		{if permission::has("forum_topic_manage")}<li><a href="/mng/forum_topics">Forum Topics</a></li>{/if}
		{if permission::has("edit_all_places")}<li><a href="/mng/forum_posts">Forum Posts</a></li>{/if}
		{if permission::has("edit_all_places")}<li><a href="/mng/reviews">Reviews</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/place_mass_upload">Place mass upload</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/image_mass_upload">Image mass upload</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/image_identify">Image place identify</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/empty_latlong">Empty latlong</a></li>{/if}
	</ul>
</li>
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onclick="return false;">Locations <span class="caret"></span></a>
	<ul class="dropdown-menu">
		{if permission::has("location_manage")}<li><a href="/mng/locations">Locations</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/loc_utils">Location utils</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/bp_url">BP Urls</a></li>{/if}
	</ul>
</li>
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onclick="location.href = '/mng/sales';">Sales <span class="caret"></span></a>
	<ul class="dropdown-menu">
		{if permission::has("access_admin_sales")}<li><a href="/mng/sales">Sales</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/uncaptured_payments">Uncaptured payments</a></li>{/if}
		{if permission::has("access_admin_sales")}<li><a href="/mng/creditcards">Credit Cards</a></li>{/if}
		{if permission::has("access_admin_sales")}<li><a href="/mng/bannedcreditcards">Banned Credit Cards</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/admin_watches">Watches</a></li>{/if}
		{if permission::has("account_ban")}<li><a href="/mng/chargeback_report">Chargeback Report</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/report_recurring_expected">Expected Recurring Report</a></li>{/if}
	</ul>
</li>
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onclick="return false;">Workers <span class="caret"></span></a>
	<ul class="dropdown-menu">
		{if permission::has("work_assign")}<li><a href="/mng/work_items">Work Items</a></li>{/if}
	</ul>
</li>
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onclick="return false;">Admin <span class="caret"></span></a>
	<ul class="dropdown-menu">
		{if permission::has("mail_manage")}<li><a href="/mng/">Mail</a></li>{/if}
		{if permission::has("settings_manage")}<li><a href="/mng/admin_settings">Settings</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/admin_important">Important items</a></li>{/if}
		{if permission::has("account_manage")}<li><a href="/mng/admin_spammers">Spam List</a></li>{/if}
		{if permission::has("access_admin_audit")}<li><a href="/mng/audit">History</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/revenue">Revenues</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/promo_girls_validate">Promo girls validate</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/promo_girls_preview">Promo girls preview</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/promo_girls_upload">Promo girls upload</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/permission">Permissions</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/account_permission">Account permissions</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/manual_email">Manual email</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/manual_sms">Manual SMS</a></li>{/if}
		{if permission::has("access_admin")}<li><a href="/mng/email_settings">Email client set-up</a></li>{/if}
		{if permission::has("access_admin")}<li><a href="/mng/vpn_setup">VPN Setup</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/ebiz_newsletter">E.biz newsletter</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/sms_blast">SMS Blast</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/banners">Banners</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/export_cities">Export City List</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/admin_geolocation">IP Geolocation</a></li>{/if}
		{if permission::has("")}<li><a href="/mng/advertiser_notify">Advertiser notify</a></li>{/if}
		{if $config_dev_server}<li><a href="/mng/dev_email_log">DEV Email Log</a></li>{/if}
	</ul>
</li>
</ul>

<ul class="nav navbar-nav navbar-right">
	{if permission::has("classified_manage") && $pecl_count}
		<li>
			<a id="clpe_btn" href="/mng/classifieds?status=3">
				<span class="badge"><span class="glyphicon glyphicon-star" aria-hidden="true"></span> {$pecl_count}</span>
			</a>
		</li>
	{/if}
	{if permission::has("mail_manage") && $newemail}
		<li>
			<a id="email_btn" href="/mng/">
				<span class="badge"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> {$newemail}</span>
			</a>
		</li>
	{/if}
	{if $account_level > 2 && $unread_messages > 0}
		<li>
			<a id="pm_btn" href="/pm/">
				<span class="badge"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> {$unread_messages}</span>
			</a>
		</li>
	{/if}
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onclick="return false;"><i class="glyphicon glyphicon-user"></i> {$smarty.session.username} <span class="caret"></span></a>
		<ul class="dropdown-menu">
			<li><a href="/">Adultsearch frontend</a></li>
			{if $smarty.session.classified_poster}
				<li><a href="/classifieds/myposts">My Classified Ads</a></li>
			{/if}
			<li><a href="http://{$site_domain}/account/mypage">My Profile</a></li>
			{if $smarty.session.worker||$account_level==3}
				<li><a href="http://{$site_domain}/mng/home">Manage</a></li>
			{/if}
			<li><a href="http://{$site_domain}/?logoff=1&redirect={$request_uri}">Logout</a></li>
		</ul>
	</li>
	<li>
</ul>

	</div>
</nav>

<div class="container-fluid">
	{if $flash_msg}
		<br style="clear: both;" />
		{$flash_msg}
	{/if}

	{$content}
</div>
<br />

<footer class="footer">
	<div class="container">
	{if $totaltime}
		<b>{$totaltime}</b>. total sql: <b>{$total_sql}</b>. sql time: <b>{$total_time}</b>
	{/if}
	</div>
</footer>

{literal}
<script type="text/javascript">
$(document).ready(function() {
	$('input[name=master]').change(function() {
		var checked = $(this).is(':checked');
		if (checked) {
			$(this).parents('table').children('tbody').children('tr').addClass('highlight');
		} else {
			$(this).parents('table').children('tbody').children('tr').removeClass('highlight');
		}
		$("input[name='action_id[]']").each(function() {
			$(this).prop('checked', checked);
		});
	});
	$('table').on('change', ':checkbox', function(obj) {
		if ($(obj.target).is(':checked')) {
			$(this).parents('tr:first').addClass('highlight');
		} else {
			$(this).parents('tr:first').removeClass('highlight');
		}
	});
});
</script>
{/literal}

</body>
</html>

