<style type="text/css">
tr td:nth-child(1) {
	vertical-align: top;
}
tr td:nth-child(2) {
	padding-left: 20px;
}
</style>

<h1>Funds Manager</h1>

<div class="well well-sm">Account balance: <b>${$budget}</b></div>

<form method="post" action="" name="f">
<table border="0" cellspacing="0" cellpadding="0" class="b">

{if $error}
<tr>
	<td colspan="2" style="color: red; font-weight: bold;">
		{$error}
	</td>
</tr>
{/if}

{*
<tr>
	<td colspan="2">
		<hr size="1" />
		<h2>Make a One Time Deposit</h2>
	</td>
</tr>
<tr>
	<td>Deposit amount:</td>
	<td>
		<select name="amount" class="form-control" style="width: auto;">
			<option value="100">$100.00</option>
			<option value="200">$200.00</option>
			<option value="300">$300.00</option>
			<option value="500">$500.00</option>
		</select>
		<br />
	</td>
</tr>
*}
<tr>
	<td style="vertical-align: top;">Note:</td>
	<td>
		{include "wire_info.tpl" account_id=$account_id}
	</td>
</tr>
{*
<tr>
	<td colspan="2">
		<input type="submit" class="btn btn-primary" name="submit" value="Next" />
		<a class="btn btn-default" href="/advertise/" />Cancel</a>
	</td>
</tr>
*}

</table>
</form>
<br />
