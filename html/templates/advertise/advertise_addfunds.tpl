<style type="text/css">
tr td:nth-child(1) {
	vertical-align: top;
}
tr td:nth-child(2) {
	padding-left: 20px;
}
</style>

<h1>Funds Manager</h1>

<div class="well well-sm">Account balance: <b>${$budget}</b></div>

<form method="get" action="" name="f">
<table border="0" cellspacing="0" cellpadding="0" class="b">

{if $error}
<tr>
	<td colspan="2" style="color: red; font-weight: bold;">
		{$error}
	</td>
</tr>
{/if}

{*
<tr>
	<td>Recurring:</td>
	<td>
		{if $recurring>=1}<span class="status_active">On</span>{else}<span class="status_paused">Off</span>{/if} [ <a href='/advertise/recurring'>configure</a> ]
	</td>
</tr>

{if $recurring < 1}
<tr>
	<td>Bugdet Notify:</td>
	<td><b>${$budget_notify}</b> [ <a href='/advertise/notify'>configure</a> ]</td>
</tr>
{/if}
*}

<tr>
	<td colspan="2">
		<hr size="1" />
		<h2>Make a One Time Deposit</h2>
	</td>
</tr>
<tr>
	<td>Deposit amount:</td>
	<td>
		<select name="amount" class="form-control" style="width: auto;">
			<option value="100">$100.00</option>
{*
			<option value="150">$150.00</option>
			<option value="200">$200.00</option>
			<option value="250">$250.00</option>
			<option value="300">$300.00</option>
			<option value="400">$400.00</option>
			<option value="500">$500.00</option>
*}
		</select>
		<br />
	</td>
</tr>
<tr>
	<td style="vertical-align: top;">Note:</td>
	<td>
		If you want to buy advertising credits in larger amount, you need to send us a wire.<br />
        The wire details are following:<br />
<hr style="margin-top: 5px; margin-bottom: 5px;"/>
		<p>
{*
        EMP Media, Inc<br />
        6130 Flamingo Rd<br />
        Las Vegas, NV 89103<br />
        Account number: 501015392477<br />
        Routing number: 026009593<br />
        Swift Code: bofaus3n<br />
*}
		Mosquito Marketing<br />
		2764 N. Green Valley Pkwy<br />
		Henderson, NV 89014<br />
		Account number: 501019478290<br />
		Routing number: 026009593 (if you use ACH, then routing number needs to be 122400724)<br />
		Swift Code: bofaus3n<br />
		</p>
		<br />
	</td>
</tr>
<tr>
	<td colspan="2">
		<button class="btn btn-primary" type="submit" value="Next"/>Next</button>
		<button class="btn btn-default" type="button" value="Cancel" onclick="window.location='http://{$site_domain}/advertise/'"/>Cancel</button>
	</td>
</tr>

</table>
</form>
<br />
