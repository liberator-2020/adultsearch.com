{if $error}<div class="error">{$error}</div>{/if}

<div id="ad_preview">
 <b>Ad Preview</b>
 <div id="ad_preview_box">
  <div id="ad_preview_title"></div>
  <div id="ad_preview_line1"></div>
  <div id="ad_preview_line2"></div>
  <div id="ad_preview_displayurl"></div>
 </div>
{literal}<span class="ex">Customize variable is {location}</span>{/literal}
</div>

<h1>{$h1title}</h1>
<form method="post" action="">
{if $new}<input type="hidden" name="new" value="1" />{/if}
<table class="b" cellspacing="0" cellpadding="0" id="ad_form">

 <tr>
  <td class="first">Campaign Name:</td>
  <td class="second"><input type="text" name="name" value="{$name}" style="width:280px; background-color: #FFFFCC" /> <span class="ex">(ex: Campaign #1)</span></td>
 </tr>

 <tr>
  <td class="first">Ad Title:</td>
  <td class="second"><input type="text" name="ad_title" id="ad_title" value="{$ad_title}" style="width:280px" />
	</td>
 </tr>

 <tr>
  <td class="first">Ad Description Line 1:</td>
  <td class="second"><input type="text" name="ad_line1" id="ad_line1" value="{$ad_line1}" style="width:280px" />
	</td>
 </tr>

 <tr>
  <td class="first">Ad Description Line 2:</td>
  <td class="second"><input type="text" name="ad_line2" id="ad_line2" value="{$ad_line2}" style="width:280px" />
	</td>
 </tr>

 <tr>
  <td class="first">Display URL:</td>
  <td class="second"><input type="text" name="ad_displayurl" id="ad_displayurl" value="{$ad_displayurl}" style="width:280px" />
	<span class="ex">(the URL shown in the ad - www.yoursite.com)</span>
	</td>
 </tr>

 <tr>
  <td class="first">Default Destination URL:</td>
  <td class="second">
	<select name="ad_prefix" id="ad_prefix"><option value="http://">http://</option>
		<option value="https://"{if $ad_prefix=='https://'} selected="selected"{/if}>https://</option></select>
	<input type="text" name="ad_url" value="{$ad_url}" id="ad_url" style="width:280px" />
	<div class="ex">(the web page that customers go to when they click your ad)</div>
	</td>
 </tr>

{* if $appneeded}
 <tr>
  <td class="second" colspan="2"><div class="error">If you make changes to your campaign, some of the publishers need to revise your ad again in order to show your ad on 
their 
websites.</div>
	</td>
 </tr>
{/if *}

</table>


<hr />
<h1>Approximate daily budget and default CPC for this campaign</h1>
<table class="b" cellspacing="0" cellpadding="0">
 <tr>
  <td class="first">Daily Budget $</td>
  <td class="second"><input type="text" name="budget" id="budget" value="{$budget}" style="width:50px" maxlength="5" /> / day
	<div class="ex">Your campaign will stop when you reach your daily budget.</div>
  </td>
 </tr>
 <tr>
  <td class="first">Default CPC per section $</td>
  <td class="second"><input type="text" name="dcpc" id="dcpc" value="{$dcpc}" style="width:50px" maxlength="5" />
	<div class="ex">You may set individual CPC for each category from campaign page.</div>
  </td>
 </tr>
</table>

<hr />
<h1>Target your advertising categories</h1>

<span style="display: inline-block; border: 1px solid red; margin: 10px; padding: 10px; background-color: #F2DDD8;">
To purchase text ads in Female Escorts and Bodyrubs or to purchase banners on our website please create an account at <a href="http://www.hottraffic.com">www.HotTraffic.com</a>
</span>
<br />

<div id="ad_target">

{section name=s2 loop=$cat_ad}
{assign var=ac value=$cat_ad[s2]}
	<div class="ad_target">
		<div class="ad_title">
			<input type="checkbox" name="ad_cat[]" value="{$ac.id}" id="{$ac.section}" {if $ac.checked}checked="checked"{/if} rel="mc"/>{$ac.name}{if $ac.appneeded}<b>(URL Approval Needed)</b>{/if}
		</div>
		{if $ac.d}<p>{$ac.d}</p>{/if}
		{if $ac.defaultbid}Your Bid for {$ac.name}: $<input type="text" name="_db_{$ac.id}" value="{$ac.bid}" size="4" /> / Per Visitor (Minimum bid is <b>${$ac.defaultbid}</b>){/if}
		{if $ac.sub||$ac.sub2}
			<div style="padding-left:50px">
				<table>
				<tr>
					<td>
						<b><span id="ava{$ac.id}">{$ac.sub|@count}</span> Do not advertise subsections</b><br />
						<select multiple="multiple" name="_select{$ac.id}[]" style="height:150px;width:250px;" id="select{$ac.id}">
						{section name=s3 loop=$ac.sub}
							<option value="{$ac.sub[s3].id}">{$ac.sub[s3].name}</option>
						{/section}
						</select>
					</td>
					<td>
						<input type="button" value="Add >" onclick="processMultiBox('{$ac.id}', 1);" class="btn" style="width:80px;" /><br /><br />
						<input type="button" value="&lt; Remove" onclick="processMultiBox('{$ac.id}', 0);" class="btn" style="width:80px;" />
					</td>
					<td>
						<b><span id="sel{$ac.id}">{$ac.sub2|@count}</span> Advertise subsections</b><br />
						<select size="4" name="selected{$ac.id}[]" multiple="multiple" id="selected{$ac.id}" style="height:150px;width:250px;">
						{section name=s4 loop=$ac.sub2}
							<option value="{$ac.sub2[s4].id}">{$ac.sub2[s4].name}</option>
						{/section}
						</select>
					</td>
				</tr>
				</table>
			</div>
		{/if}
		
		{if $ac.hasloc}
			<div>
				<div class="ad_location" id="{$ac.section}16046">
					<div class="ad_main">
						<p>Your ad will be published on every location <b>except</b> the ones you pick below</p>
						<input type="hidden" name="{$ac.section}_dont" id="{$ac.section}_dont" value="{$ac.dont}" />
						<span id="{$ac.section}_added" class="textboxlist">
							<ul class="textboxlist-bits">
							{section name=s loop=$loc_dont}{assign var=d value=$loc_dont[s]}
								{if $d.section==$ac.section}
									<li rel="{$d.loc}" class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">{$d.loc_name}<a href="#" class="textboxlist-bit-box-deletebutton" onclick="update_dont('{$d.loc}', '{$ac.section}', false); return false;"></a></li>
								{/if}
							{/section}
							</ul>
						</span>
						<a href="" onclick="ad_alt_loc('16046', '{$ac.section}'); return false;"><img src='/images/advertise/plus.gif' alt='' rel="closed" border="0" /></a>
						<input type="checkbox" name="{$ac.section}_ad_loc[]" value="16046" onclick="ad_check_relatives('16046', '{$ac.section}');" /> <label>United States</label>
						<div id="ad_loc_alt_{$ac.section}16046"></div>
					</div>
				</div>
				<div class="ad_location" id="{$ac.section}16047">
					<div class="ad_main">
						<a href="" onclick="ad_alt_loc('16047', '{$ac.section}'); return false;"><img src='/images/advertise/plus.gif' alt='' rel="closed" border="0" /></a>
						<input type="checkbox" name="{$ac.section}_ad_loc[]" value="16047" onclick="ad_check_relatives('16047', '{$ac.section}');" /> <label>Canada</label>
						<div id="ad_loc_alt_{$ac.section}16047"></div>
					</div>
				</div>
			</div>
		{/if}
	</div>
	<hr style="color:#EFEFEF"/>
{/section}


<h1>Target your advertising on external websites</h1>


<div class="ad_target">
 <div class="ad_title"> <input type="checkbox" name="external" id="external" value="1"{if $external} checked="checked"{/if}/> Enable External Website Advertising
   <div id="subofexternal" style="padding-left:50px;">
<table>
 <tr>
 <td>
  <b><span id="avaexternal">{$cat_ad_ex.sub|@count}</span> Do not advertise subsections</b><br />
 <select multiple="multiple" name="_selectexternal[]" style="height:150px;width:450px;" id="selectexternal">
 {section name=s6 loop=$cat_ad_ex.sub}{assign var=ac value=$cat_ad_ex.sub[s6]}
	<option value="{$ac.id}">{$ac.parent} - {$ac.name}{if $ac.defaultbid!=0.2} (Default bid 
${$ac.defaultbid}){/if}</option>
 {/section}
 </select>
 </td>
 <td>
<input type="button" value="Add &gt;" onclick="processMultiBox('external', 1);" class="btn" style="width:80px;" /><br /><br />
<input type="button" value="&lt; Remove" onclick="processMultiBox('external', 0);" class="btn" style="width:80px;" />
 </td>
 <td>
  <b><span id="selexternal">{$cat_ad_ex.sub2|@count}</span> Advertise subsections</b><br />
<select size="4" name="selectedexternal[]" multiple="multiple" id="selectedexternal" style="height:150px;width:450px;">
 {section name=s6 loop=$cat_ad_ex.sub2}{assign var=ac value=$cat_ad_ex.sub2[s6]}
	<option value="{$ac.id}">{$ac.parent} - {$ac.name}</option>
 {/section}
</select>
 </td>
 </tr>
</table>
New added external websites will be activated for your campaign unless you blocked the source URL. <a href="/advertise/exception?{if $id}id={$id}{else}new=1{/if}" 
class="customurl">{$exception|@count}</a> URL 
has 
been 
blocked.
</div>
</div>
</div>

</div>

{*
<hr style="color:#EFEFEF"/>

<h1>Target your customers location (GEO Targeting model)</h1>
<div style="padding-left:50px;">
<table>
 <tr>
 <td>
  <b><span id="avageo">{$cat_ad_geo|@count}</span> Do not advertise subsections</b><br />
 <select multiple="multiple" name="_selectgeo[]" style="height:150px;width:450px;" id="selectgeo">
 {section name=s7 loop=$cat_ad_geo}{assign var=geo value=$cat_ad_geo[s7]}
	<option value="{$geo.id}">{$geo.name}</option>
 {/section}
 </select>
 </td>
 <td>
<input type="button" value="Add &gt;" onclick="processMultiBox('geo', 1);" class="btn" style="width:80px;" /><br /><br />
<input type="button" value="&lt; Remove" onclick="processMultiBox('geo', 0);" class="btn" style="width:80px;" />
 </td>
 <td>
  <b><span id="selgeo">{$cat_ad_geo2|@count}</span> Advertise subsections</b><br />
<select size="4" name="selectedgeo[]" multiple="multiple" id="selectedgeo" style="height:150px;width:450px;">
 {section name=s7 loop=$cat_ad_geo2}{assign var=geo value=$cat_ad_geo2[s7]}
	<option value="{$geo.id}">{$geo.name}</option>
 {/section}
</select>
 </td>
 </tr>
</table>
</div>
*}

<span class="bsubmit"><button type="submit" value="Submit" class="bsubmit-r" onclick="return adformcheck();"><span>Submit</span></button></span>
<span class="bsubmit"><button type="button" value="Submit" onclick="window.location='/advertise/';" class="bsubmit-r"><span>Cancel</span></button></span>
<hr/>
</div>
</form>

<div id="example" style="display:none"></div>

<script type="text/javascript">
<!--
var sc = false, as = false, emp = false, cls = false, pop = false;
{literal}
$(document).ready(function(){
	var v, v2;
	var options1 = {'maxCharacterSize': 25,'originalStyle': 'ex','warningStyle': '','displayFormat': '#input chars entered | #left chars Left'};  
	var options2 = {'maxCharacterSize': 35,'originalStyle': 'ex','warningStyle': '','displayFormat': '#input chars entered | #left chars Left'};  
	$('#ad_title').textareaCount(options1);  
	$('#ad_line1').textareaCount(options2);  
	$('#ad_line2').textareaCount(options2);  
	$('#ad_displayurl').textareaCount(options2);  
	$("#ad_form input").keyup(updateAdPreview);
	$("#budget").autotab({format: 'numeric'});

	sc = $("#sc").is(":checked");
	as = $("#as").is(":checked");
	emp = $("#emp").is(":checked");
	cls = $("#cls").is(":checked");
	pop = $("#pop").is(":checked");
	$("#sc").click(function(){sc=$(this).is(":checked");adlocreadonly();});
	$("#as").click(function(){as=$(this).is(":checked");adlocreadonly();});
	$("#emp").click(function(){emp=$(this).is(":checked");adlocreadonly();});
	$("#cls").click(function(){cls=$(this).is(":checked");adlocreadonly();});
	$("#pop").click(function(){pop=$(this).is(":checked");adlocreadonly();});

	$("#external").click(function(){adlocreadonly();});

	$("input[rel=mc]").click(function(){adlocreadonly();});

{/literal}
{section name=s6 loop=$cat_ad_ex}{assign var=ac value=$cat_ad_ex[s6]}
{if $ac.sub||$ac.sub2}
	$("#{$ac.section}"){literal}.click(function(){adlocreadonly();});
	{/literal}
{/if}
{/section}


	adlocreadonly();
	updateAdPreview();

{*
	v = $("#sc_dont"); v2 = v.val().split('|');for(i=0;i<v2.length;i++) $("input[type=checkbox][name^='sc_ad_loc'][value="+v2[i]+"]").attr("checked", true);
	v = $("#as_dont"); v2 = v.val().split('|');for(i=0;i<v2.length;i++) $("input[type=checkbox][name^='as_ad_loc'][value="+v2[i]+"]").attr("checked", true);
	v = $("#emp_dont"); v2 = v.val().split('|');for(i=0;i<v2.length;i++) $("input[type=checkbox][name^='emp_ad_loc'][value="+v2[i]+"]").attr("checked", true);
	v = $("#cls_dont"); v2 = v.val().split('|');for(i=0;i<v2.length;i++) $("input[type=checkbox][name^='cls_ad_loc'][value="+v2[i]+"]").attr("checked", true);
*}

{literal}


	$("#ava15").text($("#select15").children("option").length);
	$("#sel15").text($("#selected15").children("option").length);

var dialogOpts = {
      modal: true,
      bgiframe: true,
      autoOpen: false,
      height: 200,
      width: 590,
      draggable: true,
      resizeable: true,
   };
$("#example").dialog(dialogOpts);   //end dialog

   $('a.customurl').click(
      function() {
         $("#example").load($(this).attr("href")+"&pop=1", "", function(){
               $("#example").dialog("open");
            }
         );
         return false;
      }
   );

	return;
});

function adlocreadonly() {
	if( sc === true ) {
		$("input[name^='sc_ad_loc']").attr("disabled", false);
	} else {
		$("input[name^='sc_ad_loc']").attr("disabled", true);
	}
	if( as === true ) {
		$("input[name^='as_ad_loc']").attr("disabled", false);
	} else {
		$("input[name^='as_ad_loc']").attr("disabled", true);
	}
	if( emp === true ) {
		$("input[name^='emp_ad_loc']").attr("disabled", false);
	} else {
		$("input[name^='emp_ad_loc']").attr("disabled", true);
	}
	if( cls === true ) {
		$("input[name^='cls_ad_loc']").attr("disabled", false);
		$("#select"+$("#cls").attr("value")).attr("disabled", false);
		$("#selected"+$("#cls").attr("value")).attr("disabled", false);
	} else {
		$("input[name^='cls_ad_loc']").attr("disabled", true);
		$("#select"+$("#cls").attr("value")).attr("disabled", true);
		$("#selected"+$("#cls").attr("value")).attr("disabled", true);
	} 


	if( pop === true ) {
		$("#select"+$("#pop").attr("value")).attr("disabled", false);
		$("#selected"+$("#pop").attr("value")).attr("disabled", false);
	} else {
		$("#select"+$("#pop").attr("value")).attr("disabled", true);
		$("#selected"+$("#pop").attr("value")).attr("disabled", true);
	}

	$("#select"+$("#sextourism").attr("value")).attr("disabled", $("#sextourism").is(":checked")?false:true);
	$("#selected"+$("#sextourism").attr("value")).attr("disabled", $("#sextourism").is(":checked")?false:true);

	$("#select"+$("#aw").attr("value")).attr("disabled", $("#aw").is(":checked")?false:true);
	$("#selected"+$("#aw").attr("value")).attr("disabled", $("#aw").is(":checked")?false:true);

	var val_ = $("#external").is(":checked");
	$("#selectexternal").attr("disabled", val_?false:true);
	$("#selectedexternal").attr("disabled", val_?false:true);

}

function ad_alt_loc(loc_id, cat) {
	if( $("#"+cat+loc_id+" img").attr("rel") == "closed" ) {
		if( sc === true || as === true || emp === true || cls === true ) {
			var readonly = $("input[type=checkbox][value="+loc_id+"]").attr("disabled");
		}
		else var readonly = true;
		var v = $("#"+cat+"_dont").val();
		var param = "loc_id="+loc_id+"&cat="+cat+"&readonly="+readonly+"&sc="+sc+"&emp="+emp+"&as="+as+"&cls="+cls+"&v="+v;
		_ajax('get', '/_ajax/aa', param, 'ad_loc_alt_'+cat+loc_id);
		$("#"+cat+loc_id+" img").attr("rel", "open");
		$("#"+cat+loc_id+" img").attr("src", "/images/advertise/minus.gif");
	} else  {
		$("#ad_loc_alt_"+cat+loc_id).html("");
		$("#"+cat+loc_id+" img").attr("rel", "closed");
		$("#"+cat+loc_id+" img").attr("src", "/images/advertise/plus.gif");
	}	
}

function update_dont(loc, section, type) {
	var vals = $("#"+section+"_dont");
	val = vals.val().split('|');
	if( type === true ) {
		if ($.inArray(loc, val) != -1) {
			return;
		}
		if ( val == '') {
			vals.val(loc);
			var loc_name = $("input[name="+section+"_ad_loc[]][value="+loc+"]").next().text();
			$("#"+section+"_added > ul").append('<li rel="'+loc+'" class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+loc_name+'<a href="#" class="textboxlist-bit-box-deletebutton" onclick="update_dont(\''+loc+'\', \''+section+'\', false); return false;"></a></li>');		
		} else {
			var loc_name = $("input[name="+section+"_ad_loc[]][value="+loc+"]").next().text();
			vals.val(val.join('|') + '|' + loc);
			$("#"+section+"_added > ul").append('<li rel="'+loc+'" class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+loc_name+'<a href="#" class="textboxlist-bit-box-deletebutton" onclick="update_dont(\''+loc+'\', \''+section+'\', false); return false;"></a></li>');		
		}
	} else {
		if ($.inArray(loc, val) == -1) return;
		var index = $.inArray(loc, val);
		if( index >-1 ) {
			val.splice(index, 1);
			vals.val(val.join('|'));
			$("#"+section+"_added > ul > li").each(function() { if($(this).attr("rel")==loc) $(this).remove(); });
		}
		if( $("input[type=checkbox][name="+section+"_ad_loc[]][value="+loc+"]").attr("disabled") === false ) {
			$("input[type=checkbox][name="+section+"_ad_loc[]][value="+loc+"]").attr("checked", false);
			ad_check_relatives(loc, section);
		}
	}
}

function ad_check_relatives(id, cat) {
	var dis = $("#"+cat+id+" input[value="+id+"]").is(":disabled");
	var con = $("#"+cat+id+" input[value="+id+"]").is(":checked");
	$("#ad_loc_alt_"+cat+id+" input[type=checkbox]").each(function() {
		$(this).attr("checked", con);
		$(this).attr("disabled", con);
		if( con === true ) { update_dont($(this).attr("value"), cat, false); }
	});
	if( dis ) return;
	update_dont(id, cat, con);
}

function updateAdPreview() {
    var title = $('#ad_title').val();
    var line1 = $('#ad_line1').val();
    var line2 = $('#ad_line2').val();
    var displayUrl = $('#ad_displayurl').val();
    var clickUrl = $('#ad_url').val();
    var ad_prefix = $('#ad_prefix').val();
    
    if (title == "")
        title = "Ad Title ...";
    else
        title = title.replace(/[<>]/g,'');
        
    if (line1 == "")
        line1 = "Ad Description Line 1 ...";
    else
        line1 = line1.replace(/[<>]/g,'');

    if (line2 == "")
        line2 = "Ad Description Line 2 ...";
    else
        line2 = line2.replace(/[<>]/g,'');


    if (displayUrl == "")
        displayUrl = "Display URL...";
        
    if (clickUrl == "")
        clickUrl = "#";
            
    $("#ad_preview_title").html('<a href="' + ad_prefix + clickUrl + '" target="_blank">' + title + '</a>');
    $("#ad_preview_line1").html(line1);
    $("#ad_preview_line2").html(line2);
    $("#ad_preview_displayurl").html(displayUrl);
}

function adformcheck() {
	var r = true, r2, error, checked;

	$("table.b input").each(function() { if( $(this).val() == "" ) { $(this).closest('tr').addClass('fieldmissing'); r = false; } 
	else $(this).closest('tr').removeClass('fieldmissing'); });
	if( r === false ) { alert('Please field the boxed with red background.'); return r; }
	
	r = r2 = false;
	$("input[name^='ad_cat']").each(function() { 
		if( $(this).is(":checked") ) { 
			var value = $(this).val();
			if( $("input[name^='"+value+"_ad_loc']").length > 0) {
				r2 = false;
				$("input[name^='"+value+"_ad_loc']").each(function() { checked = $(this).attr("checked"); if( checked === false ) { r = r2 = true;} });
				if( r2 === false ) { 
					error = "You need to leave a targeting location unchecked for the category that has location underneath.";
					r = false;
					return false;
				}
			} else r = true; 
		}
	});
	if( r === false ) { 
		if( $("#external").is(":checked") && $("#selectedexternal option").length>0 ) {
			r = true;
		}
	}

	if( r === false ) { 
		if( error ) alert(error);
		else alert("Please pick a category to advertise."); $("#ad_target").addClass('fieldmissing'); 
		return false;
	}
	$("#ad_target").removeClass('fieldmissing');
	$("#selected15 option").attr("selected", "selected");
	$("select[multiple=multiple] > option").attr("selected", true);
{/literal}
	return r;
}

function processMultiBox(id, add) {
	if( add == 1 ) {
		$("#select"+id).children("option").each(function(){
			if( $(this).attr('selected') == 'selected') {
				$(this).appendTo("#selected"+id);
			}
		});
	} else {
		$("#selected"+id).children("option").each(function(){
			if( $(this).attr('selected') == 'selected') {
				$(this).appendTo("#select"+id);
			}
		});
	}
	$("#ava"+id).text($("#select"+id).children("option").length);
	$("#sel"+id).text($("#selected"+id).children("option").length);
}

-->
</script>
