<div class="ar">Your budget is <b>${$budget}</b> <a href='https://adultsearch.com/advertise/addfunds'>Add Funds &gt;&gt;</a></div>

{if $fundneeded}<div class="paylink"><a href="https://adultsearch.com/advertise/addfunds" class="paylink">You must have enough funds in your account in order to run 
your ads. Click here to add more funds now.</a></div>{/if}

<form action="" method="get">
<table width="100%" cellpadding="3" style="background-color:#FFFFFF;border-width:0;color:#000000;font-family:verdana,sans-serif;font-size:12px;">
 <tr>
  <td width="130"><b>Select Report Date:</b></td>
  <td>
		<input type="radio" name="range_or_period" value="1" class="input" id="periodd" {if $range_or_period==1 || !$range_or_period}checked="checked"{/if} />
		<select name="period" class="input" onfocus="this.form.periodd.checked=true">
		<option value="0"{if $period==0} selected="selected"{/if}>Today</option>
		<option value="2"{if $period==2} selected="selected"{/if}>Yesterday</option>
		<option value="1"{if $period==1} selected="selected"{/if}>Last 7 Days</option>
		<option value="3"{if $period==3} selected="selected"{/if}>Last 30 Days</option>
		<option value="4"{if $period==4} selected="selected"{/if}>This Month</option>
		<option value="5"{if $period==5} selected="selected"{/if}>Last Month</option>
		</select>&nbsp;&nbsp;&nbsp;
		<i>or</i>&nbsp;

		<input type="radio" name="range_or_period" value="2" class="input" {if $range_or_period==2}checked="checked"{/if} id="range" />
		<select name="monthFrom" class="input" onfocus="this.form.range.checked=true">
		<option value="1"{if $monthFrom==1} selected="selected"{/if}>Jan</option>
		<option value="2"{if $monthFrom==2} selected="selected"{/if}>Feb</option>
		<option value="3"{if $monthFrom==3} selected="selected"{/if}>Mar</option>
		<option value="4"{if $monthFrom==4} selected="selected"{/if}>Apr</option>
		<option value="5"{if $monthFrom==5} selected="selected"{/if}>May</option>
		<option value="6"{if $monthFrom==6} selected="selected"{/if}>Jun</option>
		<option value="7"{if $monthFrom==7} selected="selected"{/if}>Jul</option>
		<option value="8"{if $monthFrom==8} selected="selected"{/if}>Aug</option>
		<option value="9"{if $monthFrom==9} selected="selected"{/if}>Sep</option>
		<option value="10"{if $monthFrom==10} selected="selected"{/if}>Oct</option>
		<option value="11"{if $monthFrom==11} selected="selected"{/if}>Nov</option>
		<option value="12"{if $monthFrom==12} selected="selected"{/if}>Dec</select>

		<input type="text" name="dayFrom" size="2" value="{$dayFrom}" maxlength="2" class="input" onfocus="this.form.range.checked=true" />

		<input type="text" name="yearFrom" size="4" value="{$yearFrom}" maxlength="4" class="input" onfocus="this.form.range.checked=true" />&nbsp;&nbsp;-&nbsp;&nbsp;

		<select name="monthTo" class="input" onfocus="this.form.range.checked=true">
		<option value="1"{if $monthTo==1} selected="selected"{/if}>Jan</option>
		<option value="2"{if $monthTo==2} selected="selected"{/if}>Feb</option>
		<option value="3"{if $monthTo==3} selected="selected"{/if}>Mar</option>
		<option value="4"{if $monthTo==4} selected="selected"{/if}>Apr</option>
		<option value="5"{if $monthTo==5} selected="selected"{/if}>May</option>
		<option value="6"{if $monthTo==6} selected="selected"{/if}>Jun</option>
		<option value="7"{if $monthTo==7} selected="selected"{/if}>Jul</option>
		<option value="8"{if $monthTo==8} selected="selected"{/if}>Aug</option>
		<option value="9"{if $monthTo==9} selected="selected"{/if}>Sep</option>
		<option value="10"{if $monthTo==10} selected="selected"{/if}>Oct</option>
		<option value="11"{if $monthTo==11} selected="selected"{/if}>Nov</option>
		<option value="12"{if $monthTo==12} selected="selected"{/if}>Dec</option>
		</select>
		<input type="text" name="dayTo" size="2" value="{$dayTo}" maxlength="2" class="input" onfocus="this.form.range.checked=true" />
		<input type="text" name="yearTo" size="4" value="{$yearTo}" maxlength="4" class="input" onfocus="this.form.range.checked=true" />
		<span class="bsubmit"><button class="bsubmit-r" type="submit" value="Show Reports"/><span>Show Reports</span></button></span>
  </td>
 </tr>
</table>
</form>


<form method="post" action="">
<table width="100%">
<tr>
	<td>
		<a href="/advertise/edit"><b>Create a new campaign</b></a>
		<a href="/advertise/edit?type=B"><b>Create new banner</b></a>
	</td>
</tr>

 <tr>
  <td class="al">
<input type="submit" name="action" value="Pause" class="btn" />
<input type="submit" name="action" value="Activate" class="btn" />
<input type="submit" name="action" value="Delete" class="btn" onclick="return confirm('You may not reverse this process, are you sure ?');" />
<hr size="1"></td>
 </tr>

 <tr>
  <td>
	{if $campaign}
	<table width="100%" cellspacing="0" cellpadding="0" class="grid" border="0" style="border-collapse: collapse;">
	 <tr class="header">
	<td style="width:10px"><input type="checkbox" onclick="checkall(this);" /></td>
	   <td>Campaign Name</td>
	   <td style="width:50px">Status</td>
	   <td style="width:50px">Daily Budget</td>
	   <td style="width:50px">Categories</td>
	   <td style="width:100px">Impressions</td>
	   <td style="width:100px">Clicks</td>
	   <td style="width:100px">Cost</td>
	   <td style="width:100px">Av. CPC</td>
	   <td style="width:30px">&nbsp;</td>
	 </tr>

	{section name=c loop=$campaign}
	{assign var=c value=$campaign[c]}
	 <tr class={cycle values="r0,r1"}>
	<td><input type="checkbox" name="chk[]" value="{$c.id}" /></td>
	   <td><a href='/advertise/detail?id={$c.id}'>{$c.name}</a></td>
	   <td class="ac">{if $c.status==1&&$c.budgetx==0}<span class="status_active">Live</span>{elseif $c.status==1&&$c.budgetx==1}<span class="status_paused">Budget Reached</span>{elseif $c.status==0}<span 
class="status_paused">Paused</span>{elseif $c.status==-1}<span class="status_deleted">&nbsp;</span>{/if}</td>
	<td>{$c.budget}{*<a href="/advertise/add?id={$c.id}">${$c.budget}</a>*}</td>
	   <td class="ac">{if $c.type != "B"}{$c.category}{/if}</td>
	   <td class="ar">{$c.impression}</td>
	   <td class="ar">{if $c.type != "B"}{$c.click}{/if}</td>
	   <td class="ar">{if $c.cost}${$c.cost}{/if}</td>
	   <td class="ar">{if $c.type != "B" && $c.acpc}${$c.acpc}{/if}</td>
	   <td class="ac"><a href="{if $c.type != "B"}/advertise/add?id={$c.id}{else}/advertise/edit?id={$c.id}{/if}">Edit</a></td>
	 </td>
   </tr>
	{/section}

	<tr class="total">
	<td colspan="5"><b>Totals:</b></td><td>{$total_i}</td><td>{$total_c}</td><td>${$total_cost}</td><td>${$total_a}</td><td>&nbsp</td>
	</tr>
  </table>
  {/if}
  </td>
 </tr>
</table>
</form>


{literal}
<script type="text/javascript">
<!--
function checkall(t) {
	$("input[type=checkbox]").each(function() { $(this).attr("checked", t.checked); } );
}
-->
</script>
{/literal}
