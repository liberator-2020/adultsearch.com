{literal}
<style type="text/css">
.gaia_loginbox {border-left:1px solid #C0C0C0;}
.subttl_blu {color:#206AB6;font-size:14px;font-weight:bold;line-height:22px;}
</style>
{/literal}

   <table width="100%" cellspacing="1" cellpadding="0" border="0" align="center">
  <tbody><tr>
  <td valign="top" style="width: 50%">
<div class="ac">
  <h1>
  Advertise your business
  <br /> on {if $section}{$name}{else}AdultSearch{/if}

</h1>
  <p>
  <span class="subtext_gry">No matter what your budget, you can display your ads on {if $section}{$name}{else}AdultSearch{/if}
  <br /> and our advertising network. Pay only if people click your ads</span>.
  </p>
  <img alt="Your ads appear beside related search results... People click your ads... And connect to your business" src="{if 
$thumb}/images/advertise/thumb/{$thumb}{else}/images/advertise/your-ad-here.gif{/if}" />
</div>
  </td>
  <td valign="top" align="center" style="padding-left: 10px;">
<form method="get" action="{if !$ismember}http://adultsearch.com/account/signup?advertiser=1{else}http://adultsearch.com/advertise/edit{/if}">

<div id="gaia_loginbox" class="gaia_loginbox">
<table width="100%" cellspacing="3" cellpadding="5" border="0" class="form-noindent">
  <tbody><tr>
  <td valign="top" bgcolor="white" style="text-align: left;">
<br />
 <div>
  <div><span class="subttl_blu">Create your own ad</span><br />You can describe 
	your business or service to attract customers.</div>

  <div style="padding-top:6px"><span class="subttl_blu">Your ad displays next to 
	related content</span><br/>When people browse on {if $section}{$name}{else}AdultSearch{/if}, your ad 
	will appear beside the content. Since you are advertising to an audience that's 
	already interested in your category, they are more likely to click on your ad.</div>

  <div style="padding-top:6px"><span class="subttl_blu">Attract customers</span><br/>People can simply click your ad to visit your website and learn more about your 
products 
	or services. You only pay when someone clicks on your advertisement. There are no impression limits.</div>
 </div>


  </td>
  </tr>
</tbody></table>

  <br />

  <table width="100%" cellspacing="3" cellpadding="6" border="0">
  <tbody><tr>
  <td align="center" style="font-size: 83%;">
  
  <div>
 {if !$ismember}
  <span class="bsubmit"><button class="bsubmit-r" onclick="window.location='http://adultsearch.com/account/signup?advertiser=1'" type="submit" value="Sign Up Now"/><span>Sign Up 
Now</span></button></span>
 {else}
  <span class="bsubmit"><button class="bsubmit-r" onclick="window.location='http://adultsearch.com/advertise/edit'" type="submit" value="Start Now"/><span>Start 
Now</span></button></span>
 {/if}
   </div>
  </td>
  </tr>

  </tbody></table>
</div>
</form>
  </td>
  </tr>
  </tbody>
</table>
