{literal}
<style type="text/css">
.subttl_blu {
	color:#206AB6;
	font-size:14px;
	font-weight:bold;
	line-height:22px;
}
h1 {
	height: 55px;
}
</style>
{/literal}

<div class="row">
	<div class="col-md-6">
		<h1>Business Owners, Pay Per Click, Banners, Popups</h1>
		<img src="/images/advertise/Clipboard2.jpg" alt="" width="416" height="162"/>

		<p>
		<span class="subttl_blu">Create your own ad</span><br>
		You can describe  your business or service to attract customers.
		</p>

		<p>
		<span class="subttl_blu">Budget Friendly</span><br>
		No matter what your budget, you can display your ads on AdultSearch. Pay only if people click your ads. There are no impression limits.
		</p>

		<p>
		<span class="subttl_blu">Your ad displays next to related content</span><br>
		When people browse on AdultSearch, your ad will appear beside the content. Since you are advertising to an audience that's already interested in your category, they are more likely to click your ad.
		</p>

		<p>
		<a href="{if !$ismember}http://adultsearch.com/account/signup?advertiser=1{else}http://adultsearch.com/advertise/edit{/if}" rel="nofollow" class="btn btn-primary center-block" style="width: 150px;">Sign Up Now !</a>
		</p>

	</div>

	<div class="col-md-6">
		<h1>Escort Advertisers</h1> 
		<img src="/images/advertise/classified.jpg" /> 

		<p>
		<span class="subttl_blu">Create your own ad</span><br>
		You can describe your business or service to attract customers.
		</p>

		<p>
		<span class="subttl_blu">Flat Price</span><br>
		You only pay flat price per ad no matter how many clicks it gets. There is no hidden fees.
		</p>

		<p>
		<span class="subttl_blu">Your ad displays in classifieds section </span><br >
		When people browse AdultSearch classifieds section, your ad will appear among the other ads.
		</p>

		<p>
		<span class="subttl_blu">Attract customers</span><br>
		People can simply click your ad to learn more about your services.
		</p>

		<p>
		<a href="http://adultsearch.com/classifieds/posttype" rel="nofollow" class="btn btn-primary center-block" style="width: 150px;">Sign Up Now !</a>
		</p>

	</div>
</div>		


