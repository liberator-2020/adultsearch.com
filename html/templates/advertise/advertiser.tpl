
<div class="row">
	<div class="col-sm-12">
		<h1>
			Advertiser #{$advertiser.account_id} - {$advertiser.email}
			{if !$advertiser.banned}
				<a href="/mng/accounts?action=edit&action_id={$advertiser.account_id}" class="btn btn-sm btn-primary" style="margin-left: 30px;">edit account</a>
				<a href="/advertise/advertiser?action=edit&id={$advertiser.account_id}" class="btn btn-sm btn-primary" style="margin-left: 30px;">edit advertiser info</a>
				<a href="/advertise/advertiser?action=addfunds&id={$advertiser.account_id}" class="btn btn-sm btn-primary" style="margin-left: 30px;">add funds</a>
			{/if}
			<a href="/advertise/stats_advertiser?id={$advertiser.account_id}" class="btn btn-sm btn-primary" style="margin-left: 30px;">stats</a>
			{if $account_level == 3}
				<a href="/account/impersonate?id={$advertiser.account_id}" class="btn btn-sm btn-primary" style="margin-left: 30px;">impersonate</a>
			{/if}
		</h1>
	</div>
</div>

{if $advertiser.banned}
<div class="row">
	<div class="col-sm-12">
		<div class="alert alert-danger">
			{if $advertiser.ban_reason|substr:0:11 == "integrate #"}
				This account was merged into account #{$advertiser.ban_reason|substr:11}.<a class="btn btn-danger" href="/advertise/advertiser?id={$advertiser.ban_reason|substr:11}">View advertiser #{$advertiser.ban_reason|substr:11}</a>
			{else}
				<strong>This account is banned.</strong><br />
				Ban reason: {$advertiser.ban_reason}<br />
			{/if}
		</div>
	</div>
</div>
{/if}

<div class="row">
	<div class="col-sm-2">
		<label>Current budget:</label>
	</div>
	<div class="col-sm-10">
		{if $advertiser.budget > 0}
			<strong>${$advertiser.budget|string_format:"%.2f"}</strong><a href="/advertise/advertiser?action=budget_upped&id={$advertiser.account_id}" class="btn btn-xs btn-default">budgetUpped</a>
		{else}
			${$advertiser.budget|string_format:"%.2f"}<a href="/advertise/advertiser?action=budget_zero&id={$advertiser.account_id}" class="btn btn-xs btn-default">budgetZero</a>
		{/if}	
		<br />
	</div>
</div>
<div class="row">
	<div class="col-sm-2">
		<label>Last budget changes:</label>
	</div>
	<div class="col-sm-10">
		<table class="table table-striped table-condensed" style="max-width: 500px;">
		<tr><th>Date/time</th><th style="text-align: right;">Balance</th><th style="text-align: right;">Purchase amount</th></tr>
		{foreach from=$budget_changes item=change}
			<tr><td>{$change.stamp|date_format:"%Y-%m-%d %H:%M"}</td><td style="text-align: right;">{$change.balance}</td><td style="text-align: right; font-style: bold;">{$change.total}</td></tr>
		{/foreach}
		</table>
	</div>
</div>

<div class="row">
	<div class="col-sm-2">
		<label>Notes:</label>
	</div>
	<div class="col-sm-10">
		{$advertiser.notes|nl2br}<br />
	</div>
</div>

{if $deals}
	<div class="row">
		<div class="col-sm-2">
			<label>Flat deals:</label>
		</div>
		<div class="col-sm-10" style="max-height: 300px; overflow: auto;">
			<table class="table table-striped trable-condensed table-bordered">
			<thead>
				<tr>
					<th>#Id</th>
					<th>Status</th>
					<th>Zone</th>
					<th>Description</th>
				</tr>
			</thead>
			<tbody>
			{section name=idx loop=$deals}
			{assign var=d value=$deals[idx]}
				<tr>
					<td>{$d.id}</td>
					<td>{if !$d.started}not started yet{elseif !$d.finished}in progress{else}finished{/if}</td>
					<td>{$d.zone}<br />(TODO: this doesnt reflect new deal<->zone db structure)</td>
					<td>{$d.description}</td>
				</tr>
			{/section}
			</tbody>
			</table>
		</div>
	</div>
{/if}

{if $campaigns}
	<div class="row">
		<div class="col-sm-2">
			<label>Campaigns:</label>
		</div>
		<div class="col-sm-10">
			{section name=ci loop=$campaigns}
			{assign var=c value=$campaigns[ci]}
				{if $c.status == 1}
					<a href="/advertise/campaign_detail?id={$c.id}">#{$c.id} - {$c.name}</a> 
					<span class="label label-success">live
						{if $c.flat_deal_status == 1} - flat deal on {$c.flat_deal_count} placements{/if}
					</span><br />
				{elseif $c.status == 0}
					<a href="/advertise/campaign_detail?id={$c.id}">#{$c.id} - {$c.name}</a> <span class="label label-default">paused</span><br />
				{/if}
			{/section}
		</div>
	</div>
{/if}

