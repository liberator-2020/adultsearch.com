
<form action="" method="post" class="form-inline" id="form_advertiser_addfunds">

<h1>Add funds to advertiser's account</h1>

{if $errors}
	<div style="color: red;">
		<ul>
		{foreach from=$errors item=error}
			<li>{$error}</li>			
		{/foreach}
		</ul>
	</div>
{/if}

Advertiser <strong>{$advertiser.email}</strong> (username = {$advertiser.username}, #{$advertiser.account_id}) has current budget ${$advertiser.budget|string_format:"%.2f"}.
<br /><br />

<label style="width: 120px;">Add:</label>
<div class="input-group">
	<span class="input-group-addon">$</span>
	<input type="text" name="amount" id="amount" class="form-control" value="{$amount}" />
	<span class="input-group-addon">.00</span>
</div>
<small>(e.g. 10000)</small> dollars.
<br /><br />

<label style="width: 120px;">Number of ads:</label>
<input type="text" name="ads_limit" id="ads_limit" class="form-control" value="{$amount}" style="width: 100px;"/>
<small>If you leave this field empty, escort agency will have default number of ads allowed, which is <strong>10</strong></small>
<br /><br />

<label>
	<input type="checkbox" name="email_notify" id="email_notify" class="form-control" value="1" checked="checked" />
	Send out email notification to {$advertiser.email}
</label>
<br /><br />

{if $wire_id}
<input type="hidden" name="wire" value="{$wire_id}" />
{/if}
{if $wire_id and $wire_row}
<img src="/advertise/wire?action=image&id={$wire_id}" style="max-width: 100px; max-height: 100px;" /><br />
<label>
	<input type="checkbox" name="wire_process" id="wire_process" class="form-control" value="1" checked="checked" />
	Mark wire #{$wire_id} of amount {$wire_row.amount} as processed
</label>
<br /><br />
{/if}

<a href="javascript:history.go(-1)" class="btn btn-default">Back</a>&nbsp;&nbsp;&nbsp;&nbsp;
<input type="submit" name="submit" value="Submit" class="btn btn-primary" />


<input type="hidden" name="action" value="addfunds" />
<input type="hidden" name="account_id" value="{$advertiser.account_id}" />

</form>

<script type="text/javascript">
$(document).ready(function() {
	$('#amount').keyup(function() {
		var ads_limit = $('#ads_limit').val();
		console.log('ads_limit='+ads_limit);
		if (ads_limit == '') {
			var i = Math.floor($('#amount').val() / 3000);
			if (i > 1)
				$('#ads_limit').val(i * 10);
		}
	});
	$('form#form_advertiser_addfunds').submit(function() {
		if ($('#amount').val() > 5000)
			return confirm('Are you sure you want to add '+$('#amount').val()+' ?');
	});
});
</script>
