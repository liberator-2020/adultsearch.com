
<form action="" method="post" class="form-horizontal">

<div class="form-group">
	<div class="col-sm-12">
		{if $new}
			<h1>New Advertiser</h1>
		{else}
			<h1>Edit Advertiser #{$advertiser.account_id} - {$advertiser.email}</h1>
		{/if}
	</div>
</div>

{if $errors}
<div class="form-group">
	<div class="col-sm-12" style="color: red;">
		<ul>
		{foreach from=$errors item=error}
			<li>{$error}</li>			
		{/foreach}
		</ul>
	</div>
</div>
{/if}

{if $new}
<div class="form-group">
	<div class="col-sm-2">
		<label>Email:</label>
	</div>
	<div class="col-sm-10">
		<input type="text" name="email" class="form-control" value="{$email}" />
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2">
		<label>Username:</label>
	</div>
	<div class="col-sm-10">
		<input type="text" name="username" class="form-control" value="{$username}" />
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2">
		<label>Password:</label>
	</div>
	<div class="col-sm-10">
		<input type="password" name="password" class="form-control" value="" />
	</div>
</div>
<div class="form-group">
	<div class="col-sm-2">
		<label>Password again:</label>
	</div>
	<div class="col-sm-10">
		<input type="password" name="password2" class="form-control" value="" />
	</div>
</div>

{/if}

<div class="form-group">
	<div class="col-sm-2">
		<label>Name:</label>
	</div>
	<div class="col-sm-10">
		<input name="name" class="form-control" value="{$advertiser.name}" />
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2">
		<label>Phone:</label>
	</div>
	<div class="col-sm-10">
		<input name="phone" class="form-control" value="{$advertiser.phone}" />
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2">
		<label>Company:</label>
	</div>
	<div class="col-sm-10">
		<input name="company" class="form-control" value="{$advertiser.company}" />
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2">
		<label>Nickname:</label>
	</div>
	<div class="col-sm-10">
		<input name="nickname" class="form-control" value="{$advertiser.nickname}" />
		<small>Short name for advertiser, only visible in admin panel for adminstrators</small>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2">
		<label>New Note:</label>
	</div>
	<div class="col-sm-10">
		<small>
			Please enter complete description of any flat deal or any information regarding this advertiser for future reference.<br />
			If there is a new deal - enter date/time, what was the deal about, CPM/CPC, value ($) of the deal, number of pops/impressions, etc.. all info<br />
			Example: [04/11/2017] - email: Advertiser paid $40K (wire on 04/10) for 2 zones: XX, YY and pops @0.05 CPC.
		</small>
		<textarea id="new_note" name="new_note" class="form-control" style="height: 50px;">{$new_note}</textarea>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2">
		<label>Notes:</label>
	</div>
	<div class="col-sm-10">
		<textarea id="notes" name="notes" class="form-control" style="height: 200px;" readonly="readonly">{$advertiser.notes}</textarea>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-12">
		<a href="javascript:history.go(-1)" class="btn btn-default">Back</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
	</div>
</div>

</form>

<script type="text/javascript">
$(document).ready(function() {
	var notes = $('#notes');
	if (notes.length)
		notes.scrollTop(notes[0].scrollHeight - notes.height());
});
</script>
