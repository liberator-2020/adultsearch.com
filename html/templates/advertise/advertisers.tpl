<h1>
	Advertisers
	<a href="/advertise/advertiser?action=new" class="btn btn-sm btn-primary" style="margin-left: 30px;">new</a>
</h1>

<form action="" method="get" class="form-inline">
<label>Id:</label>
<input type=text" name="account_id" class="input-sm form-control" value="{$account_id}" size="7" />
<label>Type:</label>
<select name="type" class="input-sm form-control">
	<option value="">All</option>
	<option value="agency" {if $type == "agency"}selected="selected"{/if}>Escort Agencies</option>
	<option value="advertiser" {if $type == "advertiser"}selected="selected"{/if}>CPC/CPM Advertisers</option>
</select>
<label>Username:</label>
<input type=text" name="username" class="input-sm form-control" value="{$username}" size="10" />
<label>Email:</label>
<input type=text" name="email" class="input-sm form-control" value="{$email}" size="25" />
<input type="submit" name="submit" value="Search" class="btn btn-default btn-sm" />
<input type="submit" name="reset" value="Reset" class="btn btn-default btn-sm" {literal}onclick="$('form input').each(function(){$(this).val('');});"{/literal} />
</form>

<table class="table table-striped table-condensed">
<thead>
<tr>
	<th>#Id</th>
	<th>Type</th>
	<th>Username</th>
	<th>Email</th>
	<th>Password</th>
	<th># live campaigns</th>
	<th>Budget</th>
	<th/>
</tr>
</thead>
<tbody>
{section name=ai loop=$advertisers}
{assign var=a value=$advertisers[ai]}
	<tr>
		<td>{$a.account_id}</td>
		<td>{if $a.agency}Escort Agency{else}Advertiser{/if}</td>
		<td>{$a.username}</td>
		<td>{$a.email}</td>
		<td><span class="click_to_see"><span class="msg">Click to see</span><span class="content">{$a.password}</span></span></td>
		<td>{$a.live_campaigns}</td>
		<td>${$a.budget}</td>
		<td>
			<a href="/advertise/advertiser?id={$a.account_id}" class="btn btn-default btn-sm">Detail</a>
			<a href="/advertise/advertiser?action=edit&id={$a.account_id}" class="btn btn-default btn-sm">Edit</a>
		</td>
	</tr>
{/section}
</tbody>
</table>

<style type="text/css">
.click_to_see .content {
	display: none;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
	$('.click_to_see').each(function(ind,obj) {
		$(this).find('.msg').click(function() {
			$(this).parent().find('.content').show();
			$(this).remove();
		});
	});
});
</script>
