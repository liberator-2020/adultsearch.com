
<form action="" method="post" class="form-horizontal">

<input type="hidden" name="c_id" value="{$campaign.id}" />

<div class="form-group">
	<div class="col-sm-12">
		<h1>
			Do you want to clone campaign #{$campaign.id} - {$campaign.name} 
		</h1>
	</div>
</div>

{if $error}
<div class="form-group">
	<div class="col-sm-12">
		<div class="alert alert-danger">
			{$error}
		</div>
	</div>
</div>
{/if}

<div class="form-group">
	<div class="col-sm-2">
		<label>Name of new campaign:</label>
	</div>
	<div class="col-sm-10">
		<input type="text" name="new_campaign_name" value="{$campaign.name}" class="form-control"/>	
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2">
		<label>Status of new campaign:</label>
	</div>
	<div class="col-sm-10">
		<select name="status" class="form-control">
			<option value="0">Paused</option>
			<option value="1">Live</option>
		</select>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-10 col-sm-offset-2">
		<a href="javascript:history.go(-1);" class="btn btn-default">Go back</a>
		<input type="submit" name="submit" value="Submit" class="btn btn-default" />
	</div>
</div>

<br /><br /><br />

</form>

<script type="text/javascript">
</script>
