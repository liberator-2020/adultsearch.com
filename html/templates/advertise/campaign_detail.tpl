
<div class="row">
	<div class="col-sm-12">
		<h1>
			Campaign #{$campaign.id} - {$campaign.name} 
			{if $account_level == 4}
				&nbsp;&nbsp;<a href="/advertise/campaign_edit?id={$campaign.id}" class="btn btn-sm btn-primary">edit</a>	
			{/if}
			&nbsp;&nbsp;<a href="/advertise/stats_campaign?id={$campaign.id}" class="btn btn-sm btn-primary">stats</a>	
			{if $account_level == 4}
				&nbsp;&nbsp;<a href="/advertise/campaign_clone?id={$campaign.id}" class="btn btn-sm btn-primary">clone</a>	
			{/if}
		</h1>
	</div>
</div>

<div class="row">
	<div class="col-sm-2">
		<label>Advertiser:</label>
	</div>
	<div class="col-sm-10">
		<a href="/advertise/advertiser?id={$campaign.account_id}">#{$campaign.account_id} - {$campaign.email}</a>
	</div>
</div>

<div class="row">
	<div class="col-sm-2">
		<label>Parameters:</label>
	</div>
	<div class="col-sm-10">
		Status: 
		{if $campaign.status == 1}
			<span class="label label-success">live</span>
		{elseif $campaign.status == 0}
			<span class="label label-default">paused</span>
		{elseif $campaign.status == -1}
			<span class="label label-danger">deleted</span>
		{else}
			<span style="font-weight: bold; color: red;">UNKNOWN??? - please contact administrator</span>
		{/if}
		{if $campaign.ad_title}
			<br />
			Ad Title: {$campaign.ad_title}
		{/if}
		{if $campaign.ad_url}
			<br />
			Ad URL: {$campaign.ad_url}
		{/if}
	</div>
</div>

{if $campaign.dcpc}
<div class="row">
	<div class="col-sm-2">
		<label>Default Bid:</label>
	</div>
	<div class="col-sm-10">
		$ {$campaign.dcpc}
	</div>
</div>
{/if}

{if $campaign.budget}
<div class="row">
	<div class="col-sm-2">
		<label>Daily budget:</label>
	</div>
	<div class="col-sm-10">
		$ {$campaign.budget}
	</div>
</div>
{/if}
	
<div class="row">
	<div class="col-sm-2">
		<label>Placement summary:</label>
	</div>
	<div class="col-sm-10">
		Not approved: <strong>{$not_approved}</strong>&nbsp;&nbsp;Out of budget: <strong>{$out_of_budget}</strong>&nbsp;&nbsp;Paused: <strong>{$paused}</strong>&nbsp;&nbsp;Live: <strong>{$live}</strong>&nbsp;&nbsp;Total: <strong>{$total}</strong>
	</div>
</div>

<div class="row">
	<div class="col-sm-2">
		Placements:
	</div>
	<div class="col-sm-10">
		{section name=l loop=$placements}
		{assign var=p value=$placements[l]}
			{if $p.flat_deal_status == 1}
				<span class="label label-danger">flat deal</span>
			{elseif $p.cs_status==0}
				<span class="label label-default">Funds needed</span>
			{elseif $p.cs_approved==0}
				<span class="label label-default">waiting for approval</span>
			{elseif $p.cs_approved==-1}
				<span class="label label-default">declined</span>
			{elseif $p.cs_budget_available==0}
				<span class="label label-default">daily budget reached</span>
{*
			{elseif $c.visible==0}
				<span class="label label-default">min. bid {if $type == "B"}${$c.cpm_minimum}{else}${$c.defaultbid}{/if}</span>
			{elseif $campaign.type == "T" && $c.impression==0 && $c.mbid >= $c.bid}
				<span class="label label-default">Increase CPC</span>
			{elseif $campaign.type == "B" && $c.impression==0 && $c.cpm_minimum > $c.cpm}
				<span class="label label-default">Increase CPM</span>
*}
			{else}
				<span class="label label-success">enabled</span>
			{/if}
			{if $campaign.type == 'B'}
				CPM: {$p.cpm}
			{else}
				CPC: {$p.bid}
			{/if}
			<a href="/advertise/zone_detail?id={$p.s_id}">#{$p.s_id} - {$p.s_name}</a>
			{if $p.test_link}
				<a href="{$p.test_link}" target="_blank" class="btn btn-xs btn-warning" style="margin-left: 20px;">Test display</a>
			{/if}
			<br />
		{/section}
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<h3>Impressions</h3>
		{if $impressions}
			<div id="flot-chart-1" style="width: 1150px; height: 350px; font-size: 1.2em;"></div>
		{else}
			No impressions recorded in last week.
		{/if}
		<h3>Costs</h3>
		{if $costs}
			<div id="flot-chart-2" style="width: 1150px; height: 350px; font-size: 1.2em;"></div>
		{else}
			No costs recorded in last week.
		{/if}
	</div>
</div>

<script type="text/javascript">
var data_impressions = [
{foreach from=$impressions key=cs_id item=i1}
	{ label: '{$i1.s_name}', data: [
		{foreach from=$i1.data key=day item=imp}
			[{$day_indexes[$day]}, {$imp}],	
		{/foreach}
	] },
{/foreach}
];

var data_costs = [
{foreach from=$costs key=cs_id item=c1}
	{ label: '{$c1.s_name}', data: [
		{foreach from=$c1.data key=day item=cost}
			[{$day_indexes[$day]}, {$cost}],	
		{/foreach}
	] },
{/foreach}
];

var day_ticks = [
{foreach from=$day_legend key=day_num item=day_str}
	[{$day_num}, '{$day_str}'],	
{/foreach}
];

thousandFormatter = function(val, axis) {
	return val.toString().replace(/./g, function(c, i, a) {
		return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
	});
}
dollarFormatter = function(val, axis) { return "$" + val;}

$(function() {
	{if $impressions}
		$.plot("#flot-chart-1", data_impressions, { xaxis: { ticks: day_ticks}, yaxis: { tickFormatter: thousandFormatter}});
	{/if}
	{if $costs}
		$.plot("#flot-chart-2", data_costs, { xaxis: { ticks: day_ticks}, yaxis: { tickFormatter: dollarFormatter}});
	{/if}
});
</script>
