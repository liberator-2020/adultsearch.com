<form action="" method="post" class="form-inline">

<div class="row">
	<div class="col-sm-12">
		<h1>Edit Campaign #{$campaign.id} - {$campaign.name}</h1>
	</div>
</div>

{if $errors}
<div class="row">
	<div class="col-sm-12" style="color: red;">
		<strong>Errors:</strong><br />
		<ul>
		{foreach from=$errors item=err}
			<li>{$err}</li>
		{/foreach}
		</ul>
	</div>
</div>
{/if}

<div class="row">
	<div class="col-sm-2">
		<label>Status:</label>
	</div>
	<div class="col-sm-10">
		<select id="status" name="status" class="form-control">
			<option value="">- Please select -</option>
			<option value="-1" {if $campaign.status == -1}selected="selected"{/if}>Deleted</option>
			<option value="0" {if $campaign.status == 0}selected="selected"{/if}>Paused</option>
			<option value="1" {if $campaign.status == 1}selected="selected"{/if}>Live</option>
		</select>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<a href="javascript:history.go(-1)" class="btn btn-default">Back</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
	</div>
</div>

</form>

<script type="text/javascript">
$(document).ready(function() {
});
</script>
