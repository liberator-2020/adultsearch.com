<h1>Dashboard<small> - Deals</small> <a href="/advertise/dashboard_detail" class="btn btn-default btn-sm">Show zone utilization</a></h1>

<form action="" method="get" class="form-inline">
<label>Zone group:</label>
<select name="zone_group_id" class="input-sm form-control">
    <option value="">All</option>
    {foreach $zone_groups as $group_id => $group_name}
        <option value="{$group_id}" {if $group_id == $zone_group_id} selected="selected"{/if}>{$group_name}</option>
    {/foreach}
</select>
<label>Zone type:</label>
<select name="zone_type" class="input-sm form-control">
	<option value="">-</option>
    <option value="T"{if $zone_type == 'T'} selected="selected"{/if}>Text Ads</option>
    <option value="B"{if $zone_type == 'B'} selected="selected"{/if}>Banners</option>
    <option value="P"{if $zone_type == 'P'} selected="selected"{/if}>Popups</option>
    <option value="A"{if $zone_type == 'A'} selected="selected"{/if}>Sponsor Ads</option>
    <option value="L"{if $zone_type == 'L'} selected="selected"{/if}>Nav Links</option>
</select>
<input type="submit" name="submit" value="Show" class="btn btn-default btn-sm" />
</form>

<table class="table table-striped table-condensed">
<thead>
<tr>
	<th>Zone</th>
	<th>Advertiser</th>
	<th>Deal Started</th>
	<th>Volume</th>
	<th>Paid</th>
	<th>Deal end</th>
	<th/>
</tr>
</thead>
<tbody>
{foreach from=$zones key=key item=deal}
<tr>
	<td>
		{if $deal.zone_type == 'B'}
			<span class="glyphicon glyphicon-picture" aria-hidden="true" style="color: green;"></span>
		{elseif $deal.zone_type == 'T'}
			<span class="glyphicon glyphicon-font" aria-hidden="true" style="color: blue;"></span>
		{elseif $deal.zone_type == 'P'}
			<span class="glyphicon glyphicon-fullscreen" aria-hidden="true" style="color: orange;"></span>
		{elseif $deal.zone_type == 'A'}
			<span class="glyphicon glyphicon-file" aria-hidden="true" style="color: purple;"></span>
		{elseif $deal.zone_type == 'L'}
			<span class="glyphicon glyphicon-link" aria-hidden="true" style="color: red;"></span>
		{/if}
		{$deal.zone_label}
	</td>
	{if $deal.occupied == "flat_deal"}
		<td>{$deal.advertiser}&nbsp;<a href="/advertise/advertiser?action=edit&id={$deal.advertiser_id}" class="btn btn-default btn-xs">edit</a></td>
		<td>{$deal.deal_start}</td>
		<td>
			{if $deal.deal_type != "ZT"}
				<span title="Left: {$deal.volume_left}">{$deal.volume}</span>
			{/if}
		</td>
		<td>{$deal.paid}</td>
		<td>{$deal.deal_end}</td>
		<td>
			{if $account_level == 3}
				<a href="/advertise/deal?action=edit&id={$deal.id}" class="btn btn-default btn-xs">Edit...</a>
				{if !$deal.next_deal_id}
					<a href="/advertise/deal_renew?id={$deal.id}" class="btn btn-default btn-xs">Renew...</a>
				{/if}
			{/if}
		</td>
	{else}
		<td colspan="5">
			In the SYSTEM, {if $deal.cnt_advertisers == 0}<span style="font-weight: bold; color: darkorange;">No advertiser</span>{else}{$deal.cnt_advertisers} advertisers{/if}
		</td>
		<td />
	{/if}
</tr>
{/foreach}
</tbody>
</table>

