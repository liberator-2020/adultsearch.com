<style type="text/css">
.alert {
	max-width: 400px;
}
</style>

<h1>Dashboard<small> - Zone Utilization</small> <a href="/advertise/dashboard" class="btn btn-default btn-sm">Show deals</a></h1>

<form action="" method="get" class="form-inline">
<label>Zones:</label>
<select name="fdfree" class="input-sm form-control">
    <option value="">All</option>
    <option value="nfd" {if $fdfree == "nfd"} selected="selected"{/if}>No flat deals</option>
    <option value="free" {if $fdfree == "free"} selected="selected"{/if}>Free zones</option>
</select>
<button type="submit" class="btn btn-sm btn-default" name="show" value="1">Show Report</button>
<button type="submit" class="btn btn-sm" name="export" value="1">Export to CSV</button>
</form>

<table class="table table-striped table-condensed">
<thead>
<tr>
	<th>Id</th>
	<th>Name</th>
	{if $fdfree != "nfd"}
		<th>System</th>
		<th>Flat Deals</th>
	{/if}
	<th>Utilization</th>
	<th>Advertiser Info</th>
	<th>Add.Info</th>
</tr>
</thead>
<tbody>
{foreach from=$zones key=key item=zone}
<tr>
	<td>#{$zone.id}</td>
	<td>
		{if $zone.type == 'B'}
			<span class="glyphicon glyphicon-picture" aria-hidden="true" style="color: green;"></span>
		{elseif $zone.type == 'T'}
			<span class="glyphicon glyphicon-font" aria-hidden="true" style="color: blue;"></span>
		{elseif $zone.type == 'P'}
			<span class="glyphicon glyphicon-fullscreen" aria-hidden="true" style="color: orange;"></span>
		{elseif $zone.type == 'A'}
			<span class="glyphicon glyphicon-file" aria-hidden="true" style="color: purple;"></span>
		{elseif $zone.type == 'L'}
			<span class="glyphicon glyphicon-link" aria-hidden="true" style="color: red;"></span>
		{/if}
		{$zone.zone}&nbsp;
		{$zone.label}
		<a href="/advertise/zone?id={$zone.id}" class="btn btn-default btn-xs">edit</a>
	</td>
	{if $fdfree != "nfd"}
		<td>{$zone.system_html}</td>
		<td>{$zone.flat_deals_html}</td>
	{/if}
	<td>
		{if $zone.type == "A"}
			{if $zone.free}
				<span style="color: red;">Free</span>
			{else}
				<span style="color: green;">Occupied</span>
			{/if}
		{else}
			{if $zone.utilization_today > 0.95}
				<span style="color: green;" title="Yesterday: {$zone.utilization_yesterday*100}% full">Fully occupied</span>
			{else if $zone.utilization_today < 0.01}
				<span style="color: red;" title="Yesterday: {$zone.utilization_yesterday*100}% full">Free</span>
			{else}
				<span style="color: orange;" title="Yesterday: {$zone.utilization_yesterday*100}% full">Today: {$zone.utilization_today*100}% full</span>
			{/if}
		{/if}
	</td>
	<td>
	</td>
	<td>
		{$zone.warnings}
		<a href="/advertise/zone_detail?id={$zone.id}" class="btn btn-default btn-xs">Detail</a
	</td>
</tr>
{/foreach}
</tbody>
</table>

