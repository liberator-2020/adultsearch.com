
<form action="" method="post" class="form-horizontal">

<div class="form-group">
	<div class="col-sm-12">
		{if $new}
			<h1>New Flat deal</h1>
		{else}
			<h1>Flat deal #{$id}</h1>
		{/if}
	</div>
</div>

{if $errors}
<div class="form-group">
	<div class="col-sm-12" style="color: red;">
		<ul>
		{foreach from=$errors item=error}
			<li>{$error}</li>			
		{/foreach}
		</ul>
	</div>
</div>
{/if}

<div class="form-group">
	<div class="col-sm-2">
		<label>Type:</label>
	</div>
	<div class="col-sm-10">
		<select id="type" name="type" class="form-control">{html_options options=$type_options selected=$type}</select>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2">
		<label>Advertiser:</label>
	</div>
	<div class="col-sm-10">
		<input type="search" placeholder="Type advertiser account id, username or email" id="account_suggest" name="account_suggest"  class="form-control ac-input" value="{$account_suggest}" />
		<input type="hidden" id="account_id" name="account_id" value="{$account_id}">
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2">
		<label>Zone:</label>
	</div>
	<div class="col-sm-10">
		<input type="search" placeholder="Type zone id or name " id="zone_suggest" name="zone_suggest"  class="form-control ac-input" value="{$zone_suggest}" />
		<ul id="zone_list">
		{foreach from=$zones item=z}
			<li>{$z.label}<input type="hidden" name="zone_ids[]" value="{$z.id}" /><button type="button" class="zone_remove_btn">Remove</button></li>
		{/foreach}
		</ul>
	</div>
</div>

<div id="zizp" class="form-group" {if $type == "ZT"}style="display:none;"{/if}>
	<div class="col-sm-2">
		<label>Impressions/Pops deal:</label>
	</div>
	<div class="col-sm-10">
		<input type="text" name="deal" class="form-control" value="{$deal}" /><small>Enter number of impressions / pops, e.g. 900000</small>
	</div>
</div>

<div id="clad_1" class="form-group" {if $type != "ZT"}style="display:none;"{/if}>
	<div class="col-sm-2">
		<label>Classifieds sponsor position:</label>
	</div>
	<div class="col-sm-10">
		<input type="text" name="clad_sponsor_position" class="form-control" value="{$clad_sponsor_position}" /><small>Enter small number, e.g. 1,2,3</small>
	</div>
</div>

<div id="clad_2" class="form-group" {if $type != "ZT"}style="display:none;"{/if}>
	<div class="col-sm-2">
		<label>Classifieds #id:</label>
	</div>
	<div class="col-sm-10">
		<input type="text" name="clad_id" class="form-control" value="{$clad_id}" /><small>Enter classified #id</small>
	</div>
</div>

<div id="zt" class="form-group" {if $type != "ZT"}style="display:none;"{/if}>
	<div class="col-sm-2">
		<label>Deal until:</label>
	</div>
	<div class="col-sm-10">
		<input type="date" name="to_date" id="to_date" class="form-control" value="{$to_date}" />
		<small>The deal will end at the end of this day (23:59 PST)</small>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2">
		<label>Status:</label>
	</div>
	<div class="col-sm-10">
		<div class="checkbox"><label><input type="checkbox" id="status" name="status" value="1" {if $status}checked="checked"{/if} /> Active</label></div>
		<small>If deal is set as active, it starts right now</small>
	</div>
</div>

<div id="fs" class="form-group" {if $status == 1}style="display: none;"{/if}>
	<div class="col-sm-2">
		<label>Deal starts on:</label>
	</div>
	<div class="col-sm-10">
		<input type="date" name="fs_date" id="fs_date" class="form-control" value="{$fs_date}" />
		<small>If you select date, deal will start automatically on this date at the beginning of the day (00:01am PST)<br />If you don't select date, deal will be saved into database as not active and you need to make it active later whenever you want</small>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2">
		<label>Description:</label>
	</div>
	<div class="col-sm-10">
		<textarea name="description" class="form-control" style="width: 100%; height: 100px;">{$description}</textarea><small>e.g. [MM/DD/YYYY] Dallas email: Give XXX 900K impressions in zone YYY</small>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2">
		<label>Paid:</label>
	</div>
	<div class="col-sm-10">
		<input type="text" name="paid" class="form-control" value="{$paid}" /><small>Enter how much advertiser paid, e.g. 20K</small>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-12">
		<a href="javascript:history.go(-1)" class="btn btn-default">Back</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
	</div>
</div>

</form>

{literal}
<script type="text/javascript">
function zone_picked(item) {
	$('#zone_suggest').val('');
	if ($('#zone_list_item_'+item.id).length > 0)
		return true;
	$('#zone_list').append('<li id="zone_list_item_'+item.id+'">'+item.label+'<input type="hidden" name="zone_ids[]" value="'+item.id+'" /><button type="button" class="zone_remove_btn">Remove</button></li>');
	if (item.type == 'A') {
		$('#clad_1').show();
		$('#clad_2').show();
	} else {
		$('#clad_1').hide();
		$('#clad_2').hide();
	}
	attach_zone_remove_btn_handlers();
}
function attach_zone_remove_btn_handlers() {
	$('.zone_remove_btn').each(function(ind, obj) {
		$(obj).click(function () {
			$(this).closest('li').remove();
		});
	});
}
$(document).ready(function() {
	$('#type').change(function() {
		if ($('#type').val() == 'ZT') {
			$('#zt').show();
			$('#zizp').hide();
			$('#clad_1').show();
			$('#clad_2').show();
		} else {
			$('#zt').hide();
			$('#zizp').show();
			$('#clad_1').hide();
			$('#clad_2').hide();
		}
	});
	$('#status').change(function() {
		if ($('#status').is(':checked')) {
			$('#fs').hide();
		} else {
			$('#fs').show();
		}
	});
	$('#account_suggest').autocomplete({
		source: function(request, response) {
			$.get('/_ajax/account_search', { advertiser: 1, term: $('#account_suggest').val() }, 
				response);
		},
		minLength: 2,
		select: function( event, ui ) {
			$("#account_suggest").val(ui.item.label);
			$("#account_id").val(ui.item.id);
		},
		change: function( event, ui ) {
			$("#account_suggest").val(ui.item.label);
			$("#account_id").val(ui.item.id);
		}
	});
	$('#zone_suggest').autocomplete({
		source: '/_ajax/zone_search',
		minLength: 2,
		select: function( event, ui ) {
			zone_picked(ui.item);
			return event.stopImmediatePropagation() || false;
		},
		change: function( event, ui ) {
			zone_picked(ui.item);
			return event.stopImmediatePropagation() || false;
		}
	});
	attach_zone_remove_btn_handlers();
});
</script>
{/literal}
