
<h1>Flat deal #{$id} Arrangements</h1>

<h3>Flat deal details</h3>
Flat deal #{$id},status = {$status} ({if $status == 1}active{else if $status == 0}not active{/if})<br />
Advertiser: #{$account_id} - {$account_email}<br />
Deal type: {$type_label}<br />
<br />

<form action="" method="post" class="form-horizontal">

{if $errors}
<div class="form-group">
	<div class="col-sm-12" style="color: red;">
		<ul>
		{foreach from=$errors item=error}
			<li>{$error}</li>			
		{/foreach}
		</ul>
	</div>
</div>
{/if}

{foreach from=$zones item=z}

	<h2>Zone: #{$z.zone_id} - {$z.zone_label}, exclusive access: '{$z.exclusive_access}'</h2>

	{if $z.exclusive_message}
		<div class="form-group">
			<div class="col-sm-12">
				<h3>Zone</h3>
				<div class="alert alert-warning">{$z.exclusive_message}</div>
				{if $z.exclusive_prompt}
					<strong>{$z.exclusive_prompt}</strong>
					<div class="radio">
					{foreach from=$z.exclusive_options key=v item=l}
						<label><input type="radio" name="{$z.exclusive_select}" value="{$v}" />{$l}</label>
					{/foreach}
					</div>
				{/if}
			</div>
		</div>
	{/if}

	{if $z.campaigns}
		<div class="form-group">
			<div class="col-sm-12">
				<h3>Campaigns</h3>
				{if !$z.all_live_campaigns_activated}
					<div class="alert alert-info">You probably want to activate flat deal on all placements of live campaigns</div>
				{/if}
				{foreach from=$z.campaigns item=c}
					{if $c.status == 1}
						<a href="/advertise/campaign_detail?id={$c.id}">#{$c.id} - {$c.name}</a>
						<span class="label label-success">live
						</span><br />
					{elseif $c.status == 0}
						<a href="/advertise/campaign_detail?id={$c.id}">#{$c.id} - {$c.name}</a> <span class="label label-default">paused</span><br />
					{else}
						<a href="/advertise/campaign_detail?id={$c.id}">#{$c.id} - {$c.name}</a> <span class="label label-danger">unknown status</span><br />
					{/if}
					<div class="checkbox" style="margin-left: 30px; margin-bottom: 20px;">
						<label>
							<input type="checkbox" name="p_{$c.placement_id}" value="1" {if $c.flat_deal_status}checked="checked"{/if} {if $c.status <> 1}disabled{/if} />Placement #{$c.placement_id} flat deal status
						</label>
					</div>
				{/foreach}
			</div>
		</div>
	{/if}

{/foreach}

<div class="form-group">
	<div class="col-sm-12">
		<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
	</div>
</div>

</form>

{literal}
<script type="text/javascript">
$(document).ready(function() {
});
</script>
{/literal}
