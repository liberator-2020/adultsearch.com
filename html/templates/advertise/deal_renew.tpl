
<script type="text/javascript" src="/js/moment.js"></script>

<form action="" method="post" class="form-horizontal">

<div class="form-group">
	<div class="col-sm-12">
		<h1>Renew flat deal #{$deal.id}</h1>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2"><label>Advertiser</label></div>
	<div class="col-sm-10">{$deal.advertiser}</div>
</div>
<div class="form-group">
	<div class="col-sm-2"><label>Zones</label></div>
	<div class="col-sm-10">{$deal.zones}</div>
</div>

<div class="form-group">
	<div class="col-sm-12">
		<h2>Current deal</h2>
	</div>
</div>

<div class="form-group">
	<div class="col-sm-2"><label>Length</label>
	</div><div class="col-sm-10">{$deal.length}</div>
</div>
<div class="form-group">
	<div class="col-sm-2"><label>Paid</label></div>
	<div class="col-sm-10">{$deal.paid}</div>
</div>
<div class="form-group">
	<div class="col-sm-2"><label>Ending</label></div>
	<div class="col-sm-10">{$deal.ending}</div>
</div>

<div class="form-group">
	<div class="col-sm-12">
		<h2>New deal</h2>
	</div>
</div>

{if $errors}
<div class="form-group">
	<div class="col-sm-12" style="color: red;">
		<ul>
		{foreach from=$errors item=error}
			<li>{$error}</li>			
		{/foreach}
		</ul>
	</div>
</div>
{/if}

<div class="form-group">
	<div class="col-sm-2"><label>Start</label></div>
	<div class="col-sm-10">Immediately after old deal is finished. (If you need to set different date/time here, please contact administrator)</div>
</div>
<div class="form-group">
	<div class="col-sm-2"><label>Paid</label></div>
	<div class="col-sm-10"><input class="form-control" name="paid" value="{$paid}" style="display: inline-block !important; width: 200px;" /> <small>Enter how much advertiser paid in dollars, e.g. 4500 or 20K</small></div>
</div>
{if $deal.type == "ZT"}
<div class="form-group">
	<div class="col-sm-2"><label>Length</label></div>
	<div class="col-sm-10">
		<input type="hidden" id="to_stamp" value="{$deal.to_stamp}" />
		<select id="length" name="length" class="form-control">{html_options options=$length_options selected=$length}</select>
	</div>
</div>
<div class="form-group">
	<div class="col-sm-2"><label>End</label></div>
	<div class="col-sm-10" id="div_end"></div>
</div>
{else}
<div class="form-group">
	<div class="col-sm-2"><label>Volume</label></div>
	<div class="col-sm-10"><input type="text" name="volume" value="" class="form-control" style="display: inline-block !important; width: 200px;" /> {if $deal.type == "ZI"}impressions{else}pops{/if}</div>
</div>
{/if}
<div class="form-group">
	<div class="col-sm-2"><label>Description</label></div>
	<div class="col-sm-10"><textarea name="description" class="form-control">{$description}</textarea><small>Please put in the details about transaction (who paid how for the deal, was deal confirmed via phone/email/skype?, ...) - so deal can be looked up in future</small></div>
</div>

<div class="form-group">
	<div class="col-sm-12">
		<a href="javascript:history.go(-1)" class="btn btn-default">Back</a>&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
	</div>
</div>

</form>

{literal}
<script type="text/javascript">
function length_changed() {
	var length = $('#length').val();
	if (!length) {
		$('#div_end').html('');
		return;
	}
	var months = 0;
	switch (length) {
		case '1M': months = 1; break;
		case '2M': months = 2; break;
		case '3M': months = 3; break;
		case '6M': months = 6; break;
		case '12M': months = 12; break;
		case 'end': $('#div_end').html('Please select end of deal date in future: <input type="date" name="end" id="end" value="" class="form-control" />');
		default: months = 0; break;
	}
	if (months) {
		var m = moment($('#to_stamp').val(), "X");
		m.add(months, "M");
		$('#div_end').html(m.format('MMM D YYYY'));
	}
}
$(document).ready(function() {
	$('#length').change(length_changed);
});
</script>
{/literal}
