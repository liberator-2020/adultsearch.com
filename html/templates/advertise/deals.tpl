<h1>
	Flat deals
	<a href="/advertise/deal?action=new" class="btn btn-sm btn-primary" style="margin-left: 30px;">new</a>
</h1>

<form action="" method="get" class="form-inline">
<label>Status:</label>
<select name="status" class="input-sm form-control">
	<option value="all">All</option>
	<option value="active"{if $status == 'active'} selected="selected"{/if}>Active</option>
	<option value="not_active"{if $status == 'not_active'} selected="selected"{/if}>Not Active</option>
</select>
<label>Advertiser:</label>
<select name="advertiser" class="input-sm form-control">
	<option value="all">All</option>
	{foreach $advertisers as $account_id => $advertiser_name}
		<option value="{$account_id}" {if $account_id == $advertiser} selected="selected"{/if}>{$advertiser_name}</option>
	{/foreach}
</select>
<input type="submit" name="submit" value="Show" class="btn btn-default btn-sm" />
</form>

<table class="table table-striped table-condensed">
<thead>
<tr>
	<th>#Id</th>
	<th>Type</th>
	<th>Active</th>
	<th>Advertiser</th>
	<th>Zone(s)</th>
	<th>Deal</th>
	<th>Left</th>
	<th>Campaigns advertising</th>
	<th>Description</th>
	<th/>
</tr>
</thead>
<tbody>
{section name=de loop=$deals}
{assign var=d value=$deals[de]}
	<tr>
		<td>{$d.id}</td>
		<td style="min-width: 100px;">{$d.type_name}</td>
		<td>{if $d.status == 1}
				<span class="glyphicon glyphicon-ok" aria-hidden="true" style="color: green;"></span>
			{else}
				<span class="glyphicon glyphicon-minus" aria-hidden="true" style="color: red;"></span>
			{/if}
		</td>
		<td><a href="/advertise/advertiser?id={$d.account_id}">#{$d.account_id} - {$d.username} - {$d.email}</a></td>
		<td style="min-width: 150px;">
			{foreach from=$d.zones item=z}
				<a href="/advertise/zone_detail?id={$z.zone_id}">
					{if $z.zone_type == 'B'}
						<span class="glyphicon glyphicon-picture" aria-hidden="true" style="color: green;"></span>
					{elseif $z.zone_type == 'T'}
						<span class="glyphicon glyphicon-font" aria-hidden="true" style="color: blue;"></span>
					{elseif $z.zone_type == 'P'}
						<span class="glyphicon glyphicon-fullscreen" aria-hidden="true" style="color: orange;"></span>
					{elseif $z.zone_type == 'A'}
						<span class="glyphicon glyphicon-file" aria-hidden="true" style="color: purple;"></span>
					{elseif $z.zone_type == 'L'}
						<span class="glyphicon glyphicon-link" aria-hidden="true" style="color: red;"></span>
					{/if}
					{$z.zone_name}<br />
				</a>
			{/foreach}
		</td>
		<td>
			{if $d.type == 'ZI'}
				{$d.impressions_deal}
			{else if $d.type == 'ZP'}
				{$d.pops_deal}
			{else if $d.type == 'ZT'}
				{$d.time_until}
			{/if}
		</td>
		<td>
			{if $d.type == 'ZI'}
				{$d.impressions_left}<br />
			{else if $d.type == 'ZP'}
				{$d.pops_left}<br />
			{/if}
			{if $d.time_left}<span {if $d.time_left_warning}style="color: red; font-weight: bold;"{/if}>(~{$d.time_left|replace:' ':'&nbsp;'})</span>{/if}
		</td>
		<td>{$d.campaigns_advertising}</td>
		<td>{$d.description}</td>
		<td>
			{if $account_level == 3}
				<a href="/advertise/deal_detail?id={$d.id}" class="btn btn-default btn-xs">Deal Placements</a>
				<a href="/advertise/deal?id={$d.id}&action=edit" class="btn btn-default btn-xs">Edit</a>
				<a href="/advertise/deal?id={$d.id}&action=cancel" class="btn btn-default btn-xs">Cancel</a>
			{/if}
			{foreach from=$d.zones item=z}
				<a href="/advertise/stats_zone_account?id={$z.zone_id}" class="btn btn-default btn-sm">{$z.zone_name} Stats</a>
			{/foreach}
		</td>
	</tr>
{/section}
</tbody>
</table>
