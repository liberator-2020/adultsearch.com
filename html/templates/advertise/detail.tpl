
<form action="" method="get" class="form-inline">
<input type="hidden" name="id" value="{$id}" />
<table width="100%" cellpadding="3" style="background-color:#FFFFFF;border-width:0;color:#000000;font-family:verdana,sans-serif;font-size:12px;">
<tr>
	<td colspan="3">
		<a href="/advertise/" class="btn btn-primary btn-sm">Back to the campaigns</a>
		<hr size="1">
	</td>
</tr>
<tr>
	<td width="130"><b>Select Report Date:</b></td>
	<td>
		<input type="radio" name="range_or_period" value="1" class="input form-control input-sm" id="periodd" {if $range_or_period==1 || !$range_or_period}checked="checked"{/if} /> 
		<select name="period" class="input form-control input-sm" onfocus="this.form.periodd.checked=true">
		<option value="0"{if $period==0} selected="selected"{/if}>Today</option>
		<option value="2"{if $period==2} selected="selected"{/if}>Yesterday</option>
		<option value="1"{if $period==1} selected="selected"{/if}>Last 7 Days</option>
		<option value="3"{if $period==3} selected="selected"{/if}>Last 30 Days</option>
		<option value="4"{if $period==4} selected="selected"{/if}>This Month</option>
		<option value="5"{if $period==5} selected="selected"{/if}>Last Month</option>
		</select>&nbsp;&nbsp;&nbsp;
		<i>or</i>&nbsp;
		<input type="radio" name="range_or_period" value="2" class="input form-control input-sm" {if $range_or_period==2}checked="checked"{/if} id="range" /> 
		
		<select name="monthFrom" class="input form-control input-sm" onfocus="this.form.range.checked=true">
		<option value="1"{if $monthFrom==1} selected="selected"{/if}>Jan</option>
		<option value="2"{if $monthFrom==2} selected="selected"{/if}>Feb</option>
		<option value="3"{if $monthFrom==3} selected="selected"{/if}>Mar</option>
		<option value="4"{if $monthFrom==4} selected="selected"{/if}>Apr</option>
		<option value="5"{if $monthFrom==5} selected="selected"{/if}>May</option>
		<option value="6"{if $monthFrom==6} selected="selected"{/if}>Jun</option>
		<option value="7"{if $monthFrom==7} selected="selected"{/if}>Jul</option>
		<option value="8"{if $monthFrom==8} selected="selected"{/if}>Aug</option>
		<option value="9"{if $monthFrom==9} selected="selected"{/if}>Sep</option>
		<option value="10"{if $monthFrom==10} selected="selected"{/if}>Oct</option>
		<option value="11"{if $monthFrom==11} selected="selected"{/if}>Nov</option>
		<option value="12"{if $monthFrom==12} selected="selected"{/if}>Dec</select> 
		<input type="text" name="dayFrom" size="2" value="{$dayFrom}" maxlength="2" class="input form-control input-sm" onfocus="this.form.range.checked=true" /> 
		<input type="text" name="yearFrom" size="4" value="{$yearFrom}" maxlength="4" class="input form-control input-sm" onfocus="this.form.range.checked=true" />&nbsp;&nbsp;-&nbsp;&nbsp;

		<select name="monthTo" class="input form-control input-sm" onfocus="this.form.range.checked=true">
		<option value="1"{if $monthTo==1} selected="selected"{/if}>Jan</option>
		<option value="2"{if $monthTo==2} selected="selected"{/if}>Feb</option>
		<option value="3"{if $monthTo==3} selected="selected"{/if}>Mar</option>
		<option value="4"{if $monthTo==4} selected="selected"{/if}>Apr</option>
		<option value="5"{if $monthTo==5} selected="selected"{/if}>May</option>
		<option value="6"{if $monthTo==6} selected="selected"{/if}>Jun</option>
		<option value="7"{if $monthTo==7} selected="selected"{/if}>Jul</option>
		<option value="8"{if $monthTo==8} selected="selected"{/if}>Aug</option>
		<option value="9"{if $monthTo==9} selected="selected"{/if}>Sep</option>
		<option value="10"{if $monthTo==10} selected="selected"{/if}>Oct</option>
		<option value="11"{if $monthTo==11} selected="selected"{/if}>Nov</option>
		<option value="12"{if $monthTo==12} selected="selected"{/if}>Dec</option>
		</select>
		<input type="text" name="dayTo" size="2" value="{$dayTo}" maxlength="2" class="input form-control input-sm" onfocus="this.form.range.checked=true" /> 
		<input type="text" name="yearTo" size="4" value="{$yearTo}" maxlength="4" class="input form-control input-sm" onfocus="this.form.range.checked=true" />
		<button class="btn btn-default btn-sm" type="submit" value="Show Reports"/>Show Reports</button>
	</td>
</tr>
</table>
</form>

{if $report_error}<div style="text-align:center;color:red;font-weight:bold;">{$report_error}</div>{/if}

<form action="" method="post">

<br />
{if $type != "B"}
	<button type="button" name="addcu" value="Update Custom URLs at once" class="btn btn-default btn-xs" onclick="$('.customurls').toggle();">Update Custom URLs at once</button>
	<div class="customurls" style="display:none;">
		<input type="textbox" size="75" name="customurls" value="" />
		<input type="submit" value="Update Selected Section(s)" {literal}onclick="var v=false;$('input[name=chk[]]').each(function(){ if( !v ) v = $(this).attr('checked'); }); if( !v ) {alert('You did not pick any section.'); return false; }"{/literal} />
	</div>
	<br />
{/if}

<br />

{if $campaign}
	<table class="table table-striped table-condensed">
	<tbody>
	<tr class="header">
		<td style="width: 10px;display:none;" class="customurls"><input type="checkbox" onclick="checkall(this);" /></td>
		<td>Section</td>
		{if $type == "B" }
			<td style="width:50px">CPM</td>
		{else}
			<td style="width:50px">Custom URL</td>
			<td style="width:50px">MAX CPC</td>
		{/if}
		<td style="width:50px">Status</td>
		<td style="width:100px">Impressions</td>
		{if $type != "B" && $type != "P"}
			<td style="width:100px">Clicks</td>
		{/if}
		<td style="width:100px">Cost</td>
		{if $type != "B" }
			<td style="width:100px">Av. CPC</td>
		{/if}
	</tr>

	{section name=c loop=$campaign}
	{assign var=c value=$campaign[c]}
	<tr class={cycle values="r0,r1"}>
		<td style="display:none" class="customurls"><input type="checkbox" name="chk[]" value="{$c.id}" /></td>
		<td>{$c.name}{if $c.approved==-1}<br>(Decline reason: <font color="red">{$c.reason}</font>){/if}</td>
		{if $type == "B"}
			<td nowrap="nowrap">
				{if $c.flat_deal_status}
					{$c.cpm}
				{else}
					$<input type="text" name="cpm_{$c.s_id}" value="{$c.cpm}" size="4" />{if $c.cpm_minimum}<br /><span class="ex">(min. ${$c.cpm_minimum})</span>{/if}
				{/if}
			</td>
		{else}
			<td><a href="/advertise/customurl?id={$c.id}" class="customurl">{if $c.customurl}Yes{else}No{/if}</a></td>
			<td nowrap="nowrap">
				{if $c.flat_deal_status}
					{$c.bid}
				{else}
					$<input type="text" name="bid_{$c.s_id}" value="{$c.bid}" size="4" />{if $c.defaultbid!=0.2}<br><span class="ex">(min. ${$c.defaultbid})</span>{/if}
				{/if}
			</td>
		{/if}
		<td class="ac" data-jay="{$c.status}">
			{if $c.flat_deal_status}
				<span style="color: orange;">Flat deal active</span>
			{elseif $c.status != 1}
				Funds Needed
			{elseif $c.approved==0}
				Waiting for approval
			{elseif $c.budget_available==0}
				Daily budget reached
			{elseif $c.visible==0}
				min. bid {if $type == "B"}${$c.cpm_minimum}{else}${$c.defaultbid}{/if}
			{elseif $c.approved==-1}
				Declined
			{elseif $type == "T" && $c.impression==0 && $c.mbid >= $c.bid}
				Increase CPC
			{elseif $type == "B" && $c.impression==0 && $c.cpm_minimum > $c.cpm}
				Increase CPM
			{else}
				Live
			{/if}
		</td>
		<td class="ar">{if $type == "P"}{$c.click}{else}{$c.impression}{/if}</td>
		{if $type != "B" && $type != "P"}
			<td class="ar">{$c.click}</td>
		{/if}
		<td class="ar">{if $type != "B" }${$c.cost}{else}${$c.cost}{/if}</td>
		{if $type != "B" }
			<td class="ar">${$c.acpc}</td>
		{/if}
		</td>
	</tr>
	{/section}

	<tr class="total">
		<td colspan="{if $type == "B"}3{else}4{/if}"><b>Totals:</b></td>
		<td>{$total_i}</td>
		{if $type != "B" && $type != "P"}
			<td>{$total_c}</td>
		{/if}
		<td>${$total_cost}</td>
		{if $type != "B"}
			<td>${$total_a}</td>
		{/if}
	</tr>

	</table>
{/if}

<div class="ar">
	<button class="btn btn-primary" type="submit" value="Save Bids"/>Save Bids</button>
</div>

</form>

<div id="example" style="display:none"></div>

{literal}
<script type="text/javascript">
function checkall(t) {
	$("input[type=checkbox]").each(function() { $(this).attr("checked", t.checked); } );
}
$(document).ready(function(){
	var dialogOpts = {
		modal: true,
		bgiframe: true,
		autoOpen: false,
		height: 200,
		width: 590,
		draggable: true,
		resizeable: true,
	};
	$("#example").dialog(dialogOpts);//end dialog
	$('a.customurl').click(
		function() {
			 $("#example").load($(this).attr("href"), "", function(){
				$("#example").dialog("open");
				}
			 );
			 return false;
		}
	);
}); 
</script>
{/literal}

