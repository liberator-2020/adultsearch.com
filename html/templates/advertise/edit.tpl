<style type="text/css">
table.b {
	width: 642px;
}
hr {
	margin-top: 5px;
	margin-bottom: 5px;
}
.btn-default.active, .btn-default:active, .open>.dropdown-toggle.btn-default {
	color: #222;
	background-color: #FFFF71 !important;
}
#manage_images_table tr {
	cursor: pointer;
}
.img_selected {
	background-color: #D9EDF7 !important;
}
</style>

{if $error}<div class="error">{$error}</div>{/if}

<div id="ad_preview" {if $type != "T"}style="display: none;"{/if}>
	<b>Ad Preview</b>
	<div id="ad_preview_box">
		<div id="ad_preview_title"></div>
		<div id="ad_preview_line1"></div>
		<div id="ad_preview_line2"></div>
		<div id="ad_preview_displayurl"></div>
	</div>
	{literal}<span class="ex">Customize variable is {location}</span>{/literal}
</div>

<div id="banner_preview" {if $type != "B"}style="display: none;"{/if}>
	<b>Banner Preview</b>
	<div id="banner_preview_box" style="width: 900px; height: 250px; overflow: hidden;">{if $admin && $code}{$code}{/if}</div>
</div>

<div id="link_preview" {if $type != "L"}style="display: none;"{/if}>
	<b>Link Preview</b>
	<div id="link_preview_box" style="border: 1px solid #333; padding: 10px; margin: 10px;"><a id="link_preview_link" href="#" target="_blank"></a></div>
</div>

<h1>{$h1title}</h1>

<form class="form-horizontal" method="post" action="">

{if $new}<input type="hidden" name="new" value="1" />{/if}

<div id="basic_params">

<div class="form-group">
	<label for="" class="col-sm-2 control-label">Campaign Type:</label>
	<div class="col-sm-10">
		<div id="type_buttons" class="btn-group" data-toggle="buttons">
			<label class="btn btn-default{if $type == "T"} active{/if}">
				<input type="radio" name="type" id="type_T" value="T" {if $type == "T"}checked{/if}> <span class="glyphicon glyphicon-font" aria-hidden="true"></span> Text Ad
			</label>
			<label class="btn btn-default{if $type == "B"} active{/if}">
				<input type="radio" name="type" id="type_B" value="B" {if $type == "B"}checked{/if}> <span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Image Banner
			</label>
			{if $popunder_available || ($type == "P" && !$new)}
			<label class="btn btn-default{if $type == "P"} active{/if}">
				<input type="radio" name="type" id="type_P" value="P" {if $type == "P"}checked{/if}> <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span> Popunder
			</label>
			{/if}
			<label class="btn btn-default{if $type == "L"} active{/if}">
				<input type="radio" name="type" id="type_L" value="L" {if $type == "L"}checked{/if}> <span class="glyphicon glyphicon-link" aria-hidden="true"></span> Nav Link
			</label>
			<label class="btn btn-default{if $type == "I"} active{/if}">
				<input type="radio" name="type" id="type_I" value="I" {if $type == "I"}checked{/if}> <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span> Interstitial Ad
			</label>
		</div>
	</div>
</div>
<div class="form-group">
	<label for="" class="col-sm-2 control-label">Campaign Name:</label>
	<div class="col-sm-10">
		<input type="text" name="name" value="{$name}" class="form-control" style="background-color: #FFFFCC" />
		<span class="ex">(ex: Campaign #1)</span>
	</div>
</div>

</div>

<div id="text_params" {if $type != "T"}style="display: none;"{/if}>

<div class="form-group">
	<label for="" class="col-sm-2 control-label">Ad Title:</label>
	<div class="col-sm-10">
		<input type="text" name="ad_title" id="ad_title" value="{$ad_title}" class="form-control" />
	</div>
</div>
<div class="form-group">
	<label for="" class="col-sm-2 control-label">Ad Description Line 1:</label>
	<div class="col-sm-10">
		<input type="text" name="ad_line1" id="ad_line1" value="{$ad_line1}" class="form-control" />
	</div>
</div>
<div class="form-group">
	<label for="" class="col-sm-2 control-label">Ad Description Line 2:</label>
	<div class="col-sm-10">
		<input type="text" name="ad_line2" id="ad_line2" value="{$ad_line2}" class="form-control" />
	</div>
</div>
<div class="form-group">
	<label for="" class="col-sm-2 control-label">Display URL:</label>
	<div class="col-sm-10">
		<input type="text" name="ad_displayurl" id="ad_displayurl" value="{$ad_displayurl}" class="form-control" />
		<span class="ex">(the URL shown in the ad - www.yoursite.com)</span>
	</div>
</div>
<div class="form-group">
	<label for="" class="col-sm-2 control-label">Default Destination URL:</label>
	<div class="col-sm-2">
		<select name="ad_prefix" id="ad_prefix" class="form-control">
			<option value="http://">http://</option>
			<option value="https://"{if $ad_prefix=='https://'} selected="selected"{/if}>https://</option>
		</select>
	</div>
	<div class="col-sm-8">
		<input type="text" name="ad_url" value="{$ad_url}" id="ad_url" class="form-control" />
		<div class="ex">(the web page that customers go to when they click your ad)</div>
	</div>
</div>

</div>

<div id="popup_params" {if $type != "P"}style="display: none;"{/if}>
<div class="form-group">
	<label for="" class="col-sm-2 control-label">Destination URL:</label>
	<div class="col-sm-10">
		<input type="text" name="popup_url" value="{$popup_url}" id="popup_url" class="form-control" />
		<div class="ex">(the address of the web page that opens in the popup window)</div>
	</div>
</div>
</div>

<div id="link_params" {if $type != "L"}style="display: none;"{/if}>
<div class="form-group">
	<label for="" class="col-sm-2 control-label">Link Title:</label>
	<div class="col-sm-10">
		<input type="text" name="link_title" value="{$ad_title}" id="link_title" class="form-control" />
		<div class="ex">(the text of the link)</div>
	</div>
</div>
<div class="form-group">
	<label for="" class="col-sm-2 control-label">Link URL:</label>
	<div class="col-sm-10">
		<input type="text" name="link_url" value="{$ad_url}" id="link_url" class="form-control" />
		<div class="ex">(the address of the web page that link points to)</div>
	</div>
</div>
</div>

<div id="banner_interstitial_params" {if $type != "B" && $type != "I"}style="display: none;"{/if}>

<div class="form-group">
	<label for="" class="col-sm-2 control-label">Image:</label>
	<div class="col-sm-10">
		<button type="button" class="btn btn-default" data-toggle="modal" data-target="#manage_images"> Manage Images / Upload Image</button>
		<br />
		Or enter directly (external) URL of image:
		<br />
		<input type="text" name="banner_img_src" id="banner_img_src" value="{$banner_img_src}" class="form-control" {if $code}disabled="disabled"{/if} />
		<div class="modal fade" id="manage_images">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">My images</h4>
					</div>
					<div class="modal-body">
<b>Upload:</b> <input id="fileupload" type="file" name="files[]" data-url="{$config_site_url}/advertise/upload" style="display: inline-block">
<script src="/js/jquery_file_upload/vendor/jquery.ui.widget.js"></script>
<script src="/js/jquery_file_upload/jquery.iframe-transport.js"></script>
<script src="/js/jquery_file_upload/jquery.fileupload.js"></script>
<script type="text/javascript">
function hook_select_image() {
	$('#manage_images_table tr').click(function() {
		var id = $(this).attr('data-id');
		console.log(id);
		$('#manage_images_table tr').removeClass('img_selected');
		$(this).addClass('img_selected');
		$('#select_image').prop('disabled', false);	
	});
}

$(document).ready(function() {
	$('#fileupload').fileupload({
		dataType: 'json',
		done: function (e, data) {
			var d = data.result;
			if (d.status == 'ok') {
				$('#no_images').remove();
				$('#manage_images_table tr:last')
					.after('<tr data-id="'+d.id+'"><td>'+d.id+'</td><td><img src="'+d.thumb_url+'"/></td><td>'+d.filename+'</td><td>'+d.width+'</td><td>'+d.height+'</td><td>'+d.uploaded+'</td><td></td></tr>');
				hook_select_image();
			}
		}
	});

	hook_select_image();

	$('#select_image').click(function() {
		if ($('#manage_images_table tr.img_selected td:nth-child(3)').length == 0)
			return;
		var filename = $('#manage_images_table tr.img_selected td:nth-child(3)').html();
		if (filename) {
			$('#banner_img_src').val('{$img_server}/promo/'+filename);
			$('#manage_images').modal('hide');
		}
	});
});
</script>

						<br />
						<div style="max-height: 300px; overflow: auto;">
						<table class="table table-striped table-condensed" id="manage_images_table">
						<thead>
						<tr>
							<th>#</th>
							<th>Thumbnail</th>
							<th>Name</th>
							<th>Width</th>
							<th>Height</th>
							<th>Uploaded on</th>
							<th></th>
						</tr>
						</thead>
						<tbody>
						{foreach item=img from=$advertise_images}
						<tr data-id="{$img.id}">
							<td>{$img.id}</td>
							<td><img src="{$img.thumb_url}" /></td>
							<td>{$img.filename}</td>
							<td>{$img.width}</td>
							<td>{$img.height}</td>
							<td>{$img.uploaded}</td>
							<td></td>
						</tr>
						{foreachelse}
						<tr id="no_images"><td colspan="7">You have uploaded no images yet</td></tr>
						{/foreach}
						</tbody>
						</table>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="select_image" disabled="disabled">Select Image</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="form-group">
	<label for="" class="col-sm-2 control-label">Destination&nbsp;URL (A&nbsp;HREF):</label>
	<div class="col-sm-10">
		<input type="text" name="banner_a_href" id="banner_a_href" value="{$banner_a_href}" class="form-control" {if $code}disabled="disabled"{/if} />
	</div>
</div>

</div>

<div id="banner_params" {if $type != "B"}style="display: none;"{/if}>

<div class="form-group">
	<label for="" class="col-sm-2 control-label">Iframe SRC:</label>
	<div class="col-sm-10">
		<input type="text" name="banner_iframe_src" id="banner_iframe_src" value="{$banner_iframe_src}" class="form-control" {if $code}disabled="disabled"{/if} />
	</div>
</div>
{if $admin || $code}
<div class="form-group">
	<label for="" class="col-sm-2 control-label">Banner HTML code:</label>
	<div class="col-sm-10">
		<textarea name="code" id="code" class="form-control" {if !$admin}disabled="disabled"{/if}>{$code}</textarea>
	</div>
</div>
{/if}

</div>

<hr />
<h2>Approximate daily budget and default CPC/CPM for this campaign</h2>
<div class="form-group" id="budget_params">
	<label for="" class="col-sm-2 control-label">Daily Budget</label>
	<div class="col-sm-10">
		<div class="input-group" style="width:160px">
			<span class="input-group-addon">$</span>
			<input type="text" name="budget" id="budget" value="{$budget}" maxlength="5" class="form-control" aria-label="Daily Budget">
			<span class="input-group-addon">.00</span>
		</div>
		<div class="ex">Your campaign will stop when you reach your daily budget.</div>
	</div>
</div>
<div class="form-group" id="text_params_2" {if $type != "T" && $type != "P" && $type != "L" && $type != "I"}style="display: none;"{/if}>
	<label for="" class="col-sm-2 control-label">Default CPC per section</label>
	<div class="col-sm-10">
		<div class="input-group" style="width:115px">
			<span class="input-group-addon">$</span>
			<input type="text" name="dcpc" id="dcpc" value="{$dcpc}" maxlength="5" class="form-control" aria-label="Daily Budget">
		</div>
		<div class="ex">You may set individual CPC for each category from campaign page.</div>
	</div>
</div>
<div class="form-group" id="banner_params_2" {if $type != "B"}style="display: none;"{/if}>
	<label for="" class="col-sm-2 control-label">Default CPM</label>
	<div class="col-sm-10">
		<div class="input-group" style="width:115px; float: left;">
			<span class="input-group-addon">$</span>
			<input type="text" name="cpm" id="cpm" value="{$cpm}" maxlength="5" class="form-control" aria-label="CPM">
		</div>
		<button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#bidModal" style="float: left; margin: 10px 0px 0px 20px;">How does bidding work?</button>
		<br style="clear: both;" />
		<div class="ex">Cost per 1000 impressions</div>
	</div>
</div>

<hr />

<div id="text_sections" {if $type != "T"}style="display: none;"{/if}>

<h2>Target your advertising categories</h2>

<div id="ad_target">
	{section name=s2 loop=$cat_ad}
	{assign var=ac value=$cat_ad[s2]}
	<div class="ad_target">
		<div class="ad_title">

			<div class="form-group" style="margin-bottom: 5px;">
				<div class="col-sm-12">
					<div class="checkbox">
						<label><input type="checkbox" name="ad_cat[]" value="{$ac.id}" id="{$ac.section}" {if $ac.checked}checked="checked"{/if} rel="mc"> {$ac.name}{if $ac.appneeded}<b>(URL Approval Needed)</b>{/if}</label>
					</div>
				</div>
			</div>

{*
			<input type="checkbox" name="ad_cat[]" value="{$ac.id}" id="{$ac.section}" {if $ac.checked}checked="checked"{/if} rel="mc"/>{$ac.name}{if $ac.appneeded}<b>(URL Approval Needed)</b>{/if}
*}

		</div>
		{if $ac.d}<p>{$ac.d}</p>{/if}
		{if $ac.defaultbid}Your Bid for {$ac.name}: $<input type="text" name="_db_{$ac.id}" value="{$ac.bid}" size="4" /> / Per Visitor (Minimum bid is <b>${$ac.defaultbid}</b>){/if}
		{if $ac.sub||$ac.sub2}
			<div style="padding-left:50px">
				<table>
				<tr>
					<td>
						<b><span id="sel{$ac.id}">{$ac.sub2|@count}</span> Advertise subsections</b><br />
						<select size="4" name="selected{$ac.id}[]" multiple="multiple" id="selected{$ac.id}" style="height:150px;width:250px;">
						{section name=s4 loop=$ac.sub2}
							<option value="{$ac.sub2[s4].id}">{$ac.sub2[s4].name}</option>
						{/section}
						</select>
					</td>
					<td>
						<button type="button" onclick="processMultiBox('{$ac.id}', 0);" class="btn btn-default" style="width: 100px;">Remove <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></button>
						<br /><br />
						<button type="button" onclick="processMultiBox('{$ac.id}', 1);" class="btn btn-default" style="width: 100px;" ><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Add</button>
					</td>
					<td>
						<b><span id="ava{$ac.id}">{$ac.sub|@count}</span> Do not advertise subsections</b><br />
						<select multiple="multiple" name="_select{$ac.id}[]" style="height:150px;width:250px;" id="select{$ac.id}">
						{section name=s3 loop=$ac.sub}
							<option value="{$ac.sub[s3].id}">{$ac.sub[s3].name}</option>
						{/section}
						</select>
					</td>
				</tr>
				</table>
			</div>
		{/if}
		
		{if $ac.hasloc}
			<div>
				<div class="ad_location" id="{$ac.section}16046">
					<div class="ad_main">
						<p>Your ad will be published on every location <b>except</b> the ones you pick below</p>
						<input type="hidden" name="{$ac.section}_dont" id="{$ac.section}_dont" value="{$ac.dont}" />
						<span id="{$ac.section}_added" class="textboxlist">
							<ul class="textboxlist-bits">
							{section name=s loop=$loc_dont}{assign var=d value=$loc_dont[s]}
								{if $d.section==$ac.section}
									<li rel="{$d.loc}" class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">{$d.loc_name}<a href="#" class="textboxlist-bit-box-deletebutton" onclick="update_dont('{$d.loc}', '{$ac.section}', false); return false;"></a></li>
								{/if}
							{/section}
							</ul>
						</span>
						<a href="" onclick="ad_alt_loc('16046', '{$ac.section}'); return false;"><img src='/images/advertise/plus.gif' alt='' rel="closed" border="0" /></a>
						<input type="checkbox" name="{$ac.section}_ad_loc[]" value="16046" onclick="ad_check_relatives('16046', '{$ac.section}');" /> <label>United States</label>
						<div id="ad_loc_alt_{$ac.section}16046"></div>
					</div>
				</div>
				<div class="ad_location" id="{$ac.section}16047">
					<div class="ad_main">
						<a href="" onclick="ad_alt_loc('16047', '{$ac.section}'); return false;"><img src='/images/advertise/plus.gif' alt='' rel="closed" border="0" /></a>
						<input type="checkbox" name="{$ac.section}_ad_loc[]" value="16047" onclick="ad_check_relatives('16047', '{$ac.section}');" /> <label>Canada</label>
						<div id="ad_loc_alt_{$ac.section}16047"></div>
					</div>
				</div>
			</div>
		{/if}
	</div>
	<hr style="color:#EFEFEF"/>
	{/section}

	<h2>Target your advertising on external websites</h2>

	<div class="ad_target">
		<div class="ad_title">

			<div class="form-group" style="margin-bottom: 5px;">
				<div class="col-sm-12">
					<div class="checkbox">
						<label><input type="checkbox" name="external" id="external" value="1"{if $new || $external} checked="checked"{/if}/> Enable External Website Advertising</label>
					</div>
				</div>
			</div>

			<div id="subofexternal" style="padding-left:50px;">
				<table>
				<tr>
					<td>
						<b><span id="selexternal">{$cat_ad_ex.sub2|@count}</span> Advertise subsections</b><br />
						<select size="4" name="selectedexternal[]" multiple="multiple" id="selectedexternal" style="height:150px;width:450px;">
						{section name=s6 loop=$cat_ad_ex.sub2}
						{assign var=ac value=$cat_ad_ex.sub2[s6]}
							<option value="{$ac.id}">{$ac.parent} - {$ac.name}</option>
						{/section}
						</select>
					</td>
					<td>
						<button type="button" onclick="processMultiBox('external', 0);" class="btn btn-default" style="width: 100px;" >Remove <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></button><br /><br />
						<button type="button" onclick="processMultiBox('external', 1);" class="btn btn-default" style="width: 100px;" ><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Add</button>
					</td>
					<td>
						<b><span id="avaexternal">{$cat_ad_ex.sub|@count}</span> Do not advertise subsections</b><br />
						<select multiple="multiple" name="_selectexternal[]" style="height:150px;width:450px;" id="selectexternal">
						{section name=s6 loop=$cat_ad_ex.sub}
						{assign var=ac value=$cat_ad_ex.sub[s6]}
							<option value="{$ac.id}">{$ac.parent} - {$ac.name}{if $ac.defaultbid!=0.2} (Default bid ${$ac.defaultbid}){/if}</option>
						{/section}
						</select>
					</td>
				</tr>
				</table>
				New added external websites will be activated for your campaign unless you blocked the source URL. <a href="/advertise/exception?{if $id}id={$id}{else}new=1{/if}" class="customurl">{$exception|@count}</a> URL has been blocked.
			</div>
		</div>
	</div>
</div>
</div>

<div id="banner_sections" {if $type != "B"}style="display: none;"{/if}>
	<h2>Target your banner on our network</h2>

	{section name=s loop=$cat_ban}
	{assign var=cb value=$cat_ban[s]}
		<div class="form-group" style="margin-bottom: 5px;">
			<div class="col-sm-12">
				{if $cb.type == "zone"}
				<div class="checkbox"{if $cb.in_group} style="margin-left: 20px;"{/if}>
					<label>
						<input type="checkbox" name="cat_ban[]" id="cat_ban_{$cb.id}" value="{$cb.id}" {if $cb.selected}checked="checked"{/if}> {$cb.name}  <span class="cpm">Min. CPM ${$cb.cpm_minimum}</span>
						{if $cb.volume}
							<span class="volume">Volume: {$cb.volume}</span>
						{/if}
						{if $cb.example_screenshot}
							<button type="button" class="btn btn-xs btn-success" data-es="{$cb.example_screenshot}" data-toggle="modal" data-target="#esModal">Example screenshot</button>
						{/if}
						{if $cb.test_link}
							<a href="{$cb.test_link}" class="btn btn-xs btn-warning">Test</a>
						{/if}
					</label>
				</div>
				{else if $cb.type == "group"}
					<h3>{$cb.name}</h3>
					{$cb.short_description}
				{/if}
			</div>
		</div>
	{/section}

	<br /><br />
</div>

<div id="popup_sections" {if $type != "P"}style="display: none;"{/if}>
	<h2>Target your popup on our network</h2>

	{section name=s loop=$cat_pop}
	{assign var=cp value=$cat_pop[s]}
		<div class="form-group" style="margin-bottom: 5px;">
			<div class="col-sm-12">
				<div class="checkbox">
					<label><input type="checkbox" name="cat_pop[]" id="cat_pop_{$cp.id}" value="{$cp.id}" {if $cp.selected}checked="checked"{/if}> {$cp.name}
						{if $cp.defaultbid}  <span class="cpm">Min. bid ${$cp.defaultbid}</span>{/if}
						{if $cp.volume} <span class="volume">Volume: {$cp.volume}</span>{/if}
						{if $cp.id == 79}
							<button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#countryASDModal">Country Breakdown</button>
						{elseif $cp.id == 10086}
							<button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#countryASMModal">Country Breakdown</button>
						{/if}
					</label>
				</div>
			</div>
		</div>
	{/section}

	<br /><br />
</div>

<div id="link_sections" {if $type != "L"}style="display: none;"{/if}>
	<h2>Target your nav link on our network</h2>

	{section name=s loop=$cat_link}
	{assign var=cp value=$cat_link[s]}
		<div class="form-group" style="margin-bottom: 5px;">
			<div class="col-sm-12">
				<div class="checkbox">
					<label><input type="checkbox" name="cat_link[]" id="cat_link_{$cp.id}" value="{$cp.id}" {if $cp.selected}checked="checked"{/if}> {$cp.name}
						{if $cp.defaultbid}  <span class="cpm">Min. bid ${$cp.defaultbid}</span>{/if}
						{if $cp.volume} <span class="volume">Volume: {$cp.volume}</span>{/if}
						{if $cp.example_screenshot}
							<button type="button" class="btn btn-xs btn-success" data-es="{$cp.example_screenshot}" data-toggle="modal" data-target="#esModal">Example screenshot</button>
						{/if}
					</label>
				</div>
			</div>
		</div>
	{/section}

	<br /><br />
</div>

<div id="interstitial_sections" {if $type != "I"}style="display: none;"{/if}>
	<h2>Target your interstitial ad on our network</h2>

	{section name=s loop=$cat_interstitial}
	{assign var=cp value=$cat_interstitial[s]}
		<div class="form-group" style="margin-bottom: 5px;">
			<div class="col-sm-12">
				<div class="checkbox">
					<label><input type="checkbox" name="cat_interstitial[]" id="cat_interstitial_{$cp.id}" value="{$cp.id}" {if $cp.selected}checked="checked"{/if}> {$cp.name}
						{if $cp.defaultbid}  <span class="cpm">Min. bid ${$cp.defaultbid}</span>{/if}
						{if $cp.volume} <span class="volume">Volume: {$cp.volume}</span>{/if}
						{if $cp.example_screenshot}
							<button type="button" class="btn btn-xs btn-success" data-es="{$cp.example_screenshot}" data-toggle="modal" data-target="#esModal">Example screenshot</button>
						{/if}
					</label>
				</div>
			</div>
		</div>
	{/section}

	<br /><br />
</div>

<div class="modal fade" id="esModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Example Screenshot</h4>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
table.how_bid {
	border-collapse: collapse;
	margin-bottom: 3px;
}
table.how_bid td, table.how_bid th {
	padding: 3px;
	border: 1px dotted #aaa;
}
</style>
<div class="modal fade" id="bidModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">How does bidding work ?</h4>
			</div>
			<div class="modal-body">
<p>
Let's say there is only one other advertiser, "Advertiser #1", who is advertising in zone you are interested in. His CPM bid is $1.00. Your bid is $2.00.
</p>
<table class="how_bid">
<tr><th/><th>Advertiser #1</th><th>You</th></tr>
<tr><td>Bid</td><td>$1.00</td><td>$2.00</td></tr>
</table>
<p>
Because your bid is higher, only your ad is going to be displayed in that zone. The banner of Advertiser #1 is not going to be displayed at all. Your ad will be accounted with $1.01 CPM, which means for each 1000 impressions of your ad your advertising balance will be substracted by $1.01 (one dollar and one cent). 
</p>
<p>
This goes for a while, your ad has been displayed 10000 times, and it cost you $10.10. Because you set up your daily limit to $20, your ad is still being displayed.
</p>
<p>But let's say another advertiser wants to advertise in this zone too, his name is "Advertiser #2". He starts his campaign and his bid is $1.50. What happens? Because your bid is still the highest, your ad is still being displayed, but this time it is accounted with CPM $1.51 (because you need to outbid the second advertiser, who is willing to pay $1.50 CPM).
</p>
<table class="how_bid">
<tr><th/><th>Advertiser #1</th><th>Advertiser #2</th><th>You</th></tr>
<tr><td>Bid</td><td>$1.00</td><td>$1.50</td><td>$2.00</td></tr>
</table>
<p>
This goes for some time until 6000 more impressions of your ad are displayed. At that moment you have already spend $19.16 (10 * $1.01 + 6 * $1.51 = $19.16). But now you don't have anymore another $1.51 to spend for advertising, because you have set your daily limit to $20. So your campaign is stopped until the end of the day (and next day it will be automatically resumed). Now there are again only 2 advertisers willing to advertising in this zone:
</p>
<table class="how_bid">
<tr><th/><th>Advertiser #1</th><th>Advertiser #2</th></tr>
<tr><td>Bid</td><td>$1.00</td><td>$1.50</td></tr>
</table>
<p>
So ad of "Advertiser #2" starts to be displayed in the zone, he will be accounted with CPM $1.01.
</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="countryASDModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Country Breakdown - Adultsearch.com Desktop</h4>
			</div>
			<div class="modal-body">
				<table class="table table-condensed table-striped">
					<thead><tr><th>Country</th><th>Percentage</th></tr></thead>
					<tbody>
						<tr><td>United States</td><td>88%</td></tr>
						<tr><td>Canada</td><td>2%</td></tr>
						<tr><td>United Kingdom</td><td>1%</td></tr>
						<tr><td>Rest of the world</td><td>9%</td></tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="countryASMModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Country Breakdown - Adultsearch.com Mobile</h4>
			</div>
			<div class="modal-body">
				<table class="table table-condensed table-striped">
					<thead><tr><th>Country</th><th>Percentage</th></tr></thead>
					<tbody>
						<tr><td>United States</td><td>86%</td></tr>
						<tr><td>Canada</td><td>6%</td></tr>
						<tr><td>United Kingdom</td><td>3%</td></tr>
						<tr><td>Australia</td><td>1%</td></tr>
						<tr><td>South Africa</td><td>1%</td></tr>
						<tr><td>Rest of the world</td><td>3%</td></tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<button type="submit" value="Submit" class="btn btn-primary" id="submit">Submit</button>
<button type="button" value="Submit" class="btn btn-default" onclick="window.location='/advertise/';">Cancel</button>
<br /><br />
</div>
</form>

<div id="example" style="display:none"></div>

<script type="text/javascript">
<!--
var sc = false, as = false, emp = false, cls = false, pop = false;
{if $type == "T"}
	var type = 'T';
{else if $type == "B"}
	var type = 'B';
{else if $type == "P"}
	var type = 'P';
{else if $type == "L"}
	var type = 'L';
{else}
	var type = false;
{/if}

{literal}
$(document).ready(function(){
	var v, v2;
	var options1 = {'maxCharacterSize': 25,'originalStyle': 'ex','warningStyle': '','displayFormat': '#input chars entered | #left chars Left'};	
	var options2 = {'maxCharacterSize': 35,'originalStyle': 'ex','warningStyle': '','displayFormat': '#input chars entered | #left chars Left'};	
	$('#ad_title').textareaCount(options1);	
	$('#ad_line1').textareaCount(options2);	
	$('#ad_line2').textareaCount(options2);	
	$('#ad_displayurl').textareaCount(options2);	
	$("#text_params input").keyup(updateAdPreview);
	$("#banner_interstitial_params input").keyup(updateBannerPreview);
	$("#banner_params input").keyup(updateBannerPreview);
	$("#link_params input").keyup(updateLinkPreview);
	$("#budget").autotab({format: 'numeric'});

	sc = $("#sc").is(":checked");
	as = $("#as").is(":checked");
	emp = $("#emp").is(":checked");
	cls = $("#cls").is(":checked");
	pop = $("#pop").is(":checked");
	$("#sc").click(function(){sc=$(this).is(":checked");adlocreadonly();});
	$("#as").click(function(){as=$(this).is(":checked");adlocreadonly();});
	$("#emp").click(function(){emp=$(this).is(":checked");adlocreadonly();});
	$("#cls").click(function(){cls=$(this).is(":checked");adlocreadonly();});
	$("#pop").click(function(){pop=$(this).is(":checked");adlocreadonly();});

	$("#external").click(function(){adlocreadonly();});

	$("input[rel=mc]").click(function(){adlocreadonly();});

{/literal}
{section name=s6 loop=$cat_ad_ex}{assign var=ac value=$cat_ad_ex[s6]}
{if $ac.sub||$ac.sub2}
	$("#{$ac.section}"){literal}.click(function(){adlocreadonly();});
	{/literal}
{/if}
{/section}

	adlocreadonly();
	updateAdPreview();
{if $type == "B"}
	updateBannerPreview();
{/if}
{if $type == "L"}
	updateLinkPreview();
{/if}

{literal}

	$("#ava15").text($("#select15").children("option").length);
	$("#sel15").text($("#selected15").children("option").length);

	var dialogOpts = {
		modal: true,
		bgiframe: true,
		autoOpen: false,
		height: 200,
		width: 590,
		draggable: true,
		resizeable: true,
	};
	$("#example").dialog(dialogOpts);	 //end dialog

	$('a.customurl').click(function() {
		$("#example").load($(this).attr("href")+"&pop=1", "", function(){
			$("#example").dialog("open");
		});
		return false;
	});

	//$("#type").change(function(){
	$("#type_buttons input").change(function(){
		type = $('input[name=type]:checked').val();
		$('#ad_preview').hide();
		$('#text_params').hide();
		$('#text_params_2').hide();
		$('#text_sections').hide();
		$('#banner_preview').hide();
		$('#banner_interstitial_params').hide();
		$('#banner_params').hide();
		$('#banner_params_2').hide();
		$('#banner_sections').hide();
		$('#popup_params').hide();
		$('#popup_sections').hide();
		$('#link_params').hide();
		$('#link_sections').hide();
		$('#interstitial_sections').hide();
		if (type == 'T') {
			$('#ad_preview').show();
			$('#text_params').show();
			$('#text_params_2').show();
			$('#text_sections').show();
		} else if (type == 'B') {
			$('#banner_preview').show();
			$('#banner_interstitial_params').show();
			$('#banner_params').show();
			$('#banner_params_2').show();
			$('#banner_sections').show();
		} else if (type == 'P') {
			$('#popup_params').show();
			$('#text_params_2').show();
			$('#popup_sections').show();
		} else if (type == 'L') {
			$('#link_params').show();
			$('#text_params_2').show();
			$('#link_sections').show();
		} else if (type == 'I') {
			$('#banner_interstitial_params').show();
			$('#interstitial_sections').show();
		}
	});

	$('#submit').click(function(evt) {
		return adformcheck();
	});

	$('#esModal').on('show.bs.modal', function (e) {
		var es_filename = $(e.relatedTarget).attr('data-es');
		$('#esModal .modal-body').html('<img src="/images/advertise/example/'+es_filename+'" style="max-width: 100%;"/>');
	});
	$('#esModal').on('hidden.bs.modal', function (e) {
		$('#esModal .modal-body').html('');
	});

	return;
});

function adlocreadonly() {
	if( sc === true ) {
		$("input[name^='sc_ad_loc']").attr("disabled", false);
	} else {
		$("input[name^='sc_ad_loc']").attr("disabled", true);
	}
	if( as === true ) {
		$("input[name^='as_ad_loc']").attr("disabled", false);
	} else {
		$("input[name^='as_ad_loc']").attr("disabled", true);
	}
	if( emp === true ) {
		$("input[name^='emp_ad_loc']").attr("disabled", false);
	} else {
		$("input[name^='emp_ad_loc']").attr("disabled", true);
	}
	if( cls === true ) {
		$("input[name^='cls_ad_loc']").attr("disabled", false);
		$("#select"+$("#cls").attr("value")).attr("disabled", false);
		$("#selected"+$("#cls").attr("value")).attr("disabled", false);
	} else {
		$("input[name^='cls_ad_loc']").attr("disabled", true);
		$("#select"+$("#cls").attr("value")).attr("disabled", true);
		$("#selected"+$("#cls").attr("value")).attr("disabled", true);
	} 
	if( pop === true ) {
		$("#select"+$("#pop").attr("value")).attr("disabled", false);
		$("#selected"+$("#pop").attr("value")).attr("disabled", false);
	} else {
		$("#select"+$("#pop").attr("value")).attr("disabled", true);
		$("#selected"+$("#pop").attr("value")).attr("disabled", true);
	}

	$("#select"+$("#sextourism").attr("value")).attr("disabled", $("#sextourism").is(":checked")?false:true);
	$("#selected"+$("#sextourism").attr("value")).attr("disabled", $("#sextourism").is(":checked")?false:true);

	$("#select"+$("#aw").attr("value")).attr("disabled", $("#aw").is(":checked")?false:true);
	$("#selected"+$("#aw").attr("value")).attr("disabled", $("#aw").is(":checked")?false:true);

	var val_ = $("#external").is(":checked");
	$("#selectexternal").attr("disabled", val_?false:true);
	$("#selectedexternal").attr("disabled", val_?false:true);

}

function ad_alt_loc(loc_id, cat) {
	if( $("#"+cat+loc_id+" img").attr("rel") == "closed" ) {
		if( sc === true || as === true || emp === true || cls === true ) {
			var readonly = $("input[type=checkbox][value="+loc_id+"]").attr("disabled");
		}
		else var readonly = true;
		var v = $("#"+cat+"_dont").val();
		var param = "loc_id="+loc_id+"&cat="+cat+"&readonly="+readonly+"&sc="+sc+"&emp="+emp+"&as="+as+"&cls="+cls+"&v="+v;
		_ajax('get', '/_ajax/aa', param, 'ad_loc_alt_'+cat+loc_id);
		$("#"+cat+loc_id+" img").attr("rel", "open");
		$("#"+cat+loc_id+" img").attr("src", "/images/advertise/minus.gif");
	} else	{
		$("#ad_loc_alt_"+cat+loc_id).html("");
		$("#"+cat+loc_id+" img").attr("rel", "closed");
		$("#"+cat+loc_id+" img").attr("src", "/images/advertise/plus.gif");
	}	
}

function update_dont(loc, section, type) {
	var vals = $("#"+section+"_dont");
	val = vals.val().split('|');
	if( type === true ) {
		if ($.inArray(loc, val) != -1) {
			return;
		}
		if ( val == '') {
			vals.val(loc);
			var loc_name = $("input[name="+section+"_ad_loc[]][value="+loc+"]").next().text();
			$("#"+section+"_added > ul").append('<li rel="'+loc+'" class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+loc_name+'<a href="#" class="textboxlist-bit-box-deletebutton" onclick="update_dont(\''+loc+'\', \''+section+'\', false); return false;"></a></li>');		
		} else {
			var loc_name = $("input[name="+section+"_ad_loc[]][value="+loc+"]").next().text();
			vals.val(val.join('|') + '|' + loc);
			$("#"+section+"_added > ul").append('<li rel="'+loc+'" class="textboxlist-bit textboxlist-bit-box textboxlist-bit-box-deletable">'+loc_name+'<a href="#" class="textboxlist-bit-box-deletebutton" onclick="update_dont(\''+loc+'\', \''+section+'\', false); return false;"></a></li>');		
		}
	} else {
		if ($.inArray(loc, val) == -1) return;
		var index = $.inArray(loc, val);
		if( index >-1 ) {
			val.splice(index, 1);
			vals.val(val.join('|'));
			$("#"+section+"_added > ul > li").each(function() { if($(this).attr("rel")==loc) $(this).remove(); });
		}
		if( $("input[type=checkbox][name="+section+"_ad_loc[]][value="+loc+"]").attr("disabled") === false ) {
			$("input[type=checkbox][name="+section+"_ad_loc[]][value="+loc+"]").attr("checked", false);
			ad_check_relatives(loc, section);
		}
	}
}

function ad_check_relatives(id, cat) {
	var dis = $("#"+cat+id+" input[value="+id+"]").is(":disabled");
	var con = $("#"+cat+id+" input[value="+id+"]").is(":checked");
	$("#ad_loc_alt_"+cat+id+" input[type=checkbox]").each(function() {
		$(this).attr("checked", con);
		$(this).attr("disabled", con);
		if( con === true ) { update_dont($(this).attr("value"), cat, false); }
	});
	if( dis ) return;
	update_dont(id, cat, con);
}

function updateAdPreview() {
	var title = $('#ad_title').val();
	var line1 = $('#ad_line1').val();
	var line2 = $('#ad_line2').val();
	var displayUrl = $('#ad_displayurl').val();
	var clickUrl = $('#ad_url').val();
	var ad_prefix = $('#ad_prefix').val();
	
	if (title == "")
		title = "Ad Title ...";
	else
		title = title.replace(/[<>]/g,'');
		
	if (line1 == "")
		line1 = "Ad Description Line 1 ...";
	else
		line1 = line1.replace(/[<>]/g,'');

	if (line2 == "")
		line2 = "Ad Description Line 2 ...";
	else
		line2 = line2.replace(/[<>]/g,'');

	if (displayUrl == "")
		displayUrl = "Display URL...";
		
	if (clickUrl == "")
		clickUrl = "#";
			
	$("#ad_preview_title").html('<a href="' + ad_prefix + clickUrl + '" target="_blank">' + title + '</a>');
	$("#ad_preview_line1").html(line1);
	$("#ad_preview_line2").html(line2);
	$("#ad_preview_displayurl").html(displayUrl);
}

function updateBannerPreview() {
	var html = '';
	if ($('#code').length > 0 && $('#code').val() != '') {
		html = $('#code').val();
	} else {
		if ($('#banner_img_src').val())
			html = '<img src="'+$('#banner_img_src').val()+'" />';
		if ($('#banner_a_href').val())
			html = '<a href="'+$('#banner_a_href').val()+'">'+html+'</a>';
		if ($('#banner_iframe_src').val())
			html = '<iframe src="'+$('#banner_iframe_src').val()+'" width="100%" height="100%" style="border: 0px;" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="no">'+html+'</iframe>';
	}
	$('#banner_preview_box').html(html);
	if (html == '')
		$('#banner_preview_box').hide();
	else
		$('#banner_preview_box').show();
}

function updateLinkPreview() {
	var title = $('#link_title').val();
	var url = $('#link_url').val();
	$("#link_preview_link").html(title);
	$("#link_preview_link").attr('href', url);
}

function check_inputs(selector) {
	var r = true;
	$(selector).each(function() {
		if ($(this).val() == "") {
			$(this).closest('.form-group').addClass('has-error');
			r = false;
		} else {
			$(this).closest('.form-group').removeClass('has-error');
		}
	});
	return r;
}

function adformcheck() {
	var r = true, r2, error, checked;

	var r1 = check_inputs('#basic_params input');

	if ($('input[name=type]:checked').val() == 'T') {
		var r2 = check_inputs('#text_params input');
		var r3 = check_inputs('#text_params_2 input');
	} else if ($('input[name=type]:checked').val() == 'B') {
		if ($('#banner_iframe_src').val() == '' && $('#banner_img_src').val() == '' && $('#code').val() == '') {
			alert('Please fill IFRAME SRC or IMG SRC fields.');
			$('#banner_iframe_src').closest('.form-group').addClass('has-error');
			$('#banner_img_src').closest('.form-group').addClass('has-error');
			return false;
		}
		var r3 = check_inputs('#banner_params_2 input');
	} else if ($('input[name=type]:checked').val() == 'P') {
		var r2 = check_inputs('#popup_params input');
		var r3 = check_inputs('#text_params_2 input');
	}
	var r4 = check_inputs('#budget_params input');
	if (r1 === false || r2 === false || r3 === false || r4 === false) {
		alert('Please field the boxes with red background.');
		return false;
	}
	
	if ($('input[name=type]:checked').val() == 'T') {
		r = r2 = false;
		$("input[name^='ad_cat']").each(function() { 
			if( $(this).is(":checked") ) { 
				var value = $(this).val();
				if( $("input[name^='"+value+"_ad_loc']").length > 0) {
					r2 = false;
					$("input[name^='"+value+"_ad_loc']").each(function() { checked = $(this).attr("checked"); if( checked === false ) { r = r2 = true;} });
					if( r2 === false ) { 
						error = "You need to leave a targeting location unchecked for the category that has location underneath.";
						r = false;
						return false;
					}
				} else r = true; 
			}
		});
		if( r === false ) { 
			if( $("#external").is(":checked") && $("#selectedexternal option").length>0 ) {
				r = true;
			}
		}
		if( r === false ) { 
			if (error)
				alert(error);
			else
				alert("Please pick a category to advertise.");
//			$("#ad_target").addClass('fieldmissing'); 
			return false;
		}
		$("#selected15 option").attr("selected", "selected");
		$("select[multiple=multiple] > option").attr("selected", true);

	} else if ($('input[name=type]:checked').val() == 'B') {
		r = false;
		$("input[name^='cat_ban']").each(function() { 
			if ($(this).is(":checked")) {
				r = true;
			}
		});
		if (r === false) {
			alert("Please pick a category to advertise.");
			return false;
		}
	} else if ($('input[name=type]:checked').val() == 'P') {
		r = false;
		$("input[name^='cat_pop']").each(function() { 
			if ($(this).is(":checked")) {
				r = true;
			}
		});
		if (r === false) {
			alert("Please pick a category to advertise.");
			return false;
		}
	}

{/literal}
	return r;
}

function processMultiBox(id, add) {
	if (add == 1) {
		$("#select"+id).children("option").each(function(){
			if ($(this).is(':selected')) {
				$(this).appendTo("#selected"+id);
			}
		});
	} else {
		$("#selected"+id).children("option").each(function(){
			if ($(this).is(':selected')) {
				$(this).appendTo("#select"+id);
			}
		});
	}
	$("#ava"+id).text($("#select"+id).children("option").length);
	$("#sel"+id).text($("#selected"+id).children("option").length);
}

-->
</script>
