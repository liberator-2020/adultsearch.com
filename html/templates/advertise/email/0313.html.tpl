<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>April 2013 Marketing Email</title>
</head>
<body>

<table width="600" border="0" align="center" cellpadding="0" cellspacing="14" bgcolor="#186299">
<tr>
<td bgcolor="#FFFFFF"><a href="https://adultsearch.com/advertise/addfunds" target="_blank"><img src="email-header.png" width="600" height="193" border="0" /></a>

<blockquote style="font-family: Verdana, Geneva, sans-serif; font-size: 14px; color: #000000;">
 <font style="font-size: 16px; font-weight: bold; color: #186299;">You are eligible to receive 20% extra traffic from the AdultSearch network if you buy during the month of April! </font>
<br />
<br />

We are now offering a special bonus offer to select advertisers who fund their accounts during month of April. <br />
<br />
Simply login to your account at adultclicks.com and navigate to <a href="https://adultsearch.com/advertise/addfunds" target="_blank" style="color: #186299;">adultsearch.com/advertise/addfunds<br />
</a><br />

Once you have funded your account send an email to <a href="mailto:support@adultsearch.com?subject=Special Offer" style="color: #186299;">support@adultsearch.com</a> with the following information: your account email, the amount you added to your account. Within 2 business days your account will be topped off with your bonus! <br />
<br />

For funding up to $1,000 you will receive a 10% bonus. For funding over that amount you will receive a 20% bonus. If you would like to add more than $2,000 to your account please contact <a href="mailto:support@adultsearch.com?subject=Special Offer" style="color: #186299;">support@adultsearch.com</a> for assistance. <br />
<br />
<a href="https://adultsearch.com/advertise/addfunds" target="_blank"><img src="learn.png" alt="Learn More" width="156" height="33" border="0" align="right" /></a>Thank you for your support!<br />

The AdultSearch.com Team
<br />
<br />
<em>For assistance with your campaigns contact your account manager:<br />

 Dena-Renee <br />
adultconsult@gmail.com
<br />
Skype: adultconsult
<br />
415-230-2691</em><br />
<br />
</blockquote>

</td>
</tr>
</table>

</body>
</html>

