*April 2013 Marketing Email*

You are eligible to receive 20% extra traffic from the AdultSearch network if you buy during the month of April!

We are now offering a special bonus offer to select advertisers who fund their accounts during month of April.

Simply login to your account at adultclicks.com and navigate to https://adultsearch.com/advertise/addfunds.

Once you have funded your account send an email to support@adultsearch.com with the following information: your account email, the amount you added to your account. Within 2 business days your account will be topped off with your bonus!

For funding up to $1,000 you will receive a 10% bonus. For funding over that amount you will receive a 20% bonus. If you would like to add more than $2,000 to your account please contact support@adultsearch.com for assistance.

Thank you for your support!

The AdultSearch.com Team

For assistance with your campaigns contact your account manager:
Dena-Renee
adultconsult@gmail.com
Skype: adultconsult
415-230-2691

