<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Your campaigns prepaid balance is low</title>
</head>
<body>
Hello,<br />
<br />
Thank you for advertising with us.<br />
<br />
Your campaigns prepaid balance is low. Please log in to your account and add more funds before your ads stop running.<br />
<br />
Thank you for advertising with AdultSearch.<br />
<br />
Sincerely,<br />
<a href="mailto:advertising@adultsearch.com">advertising@adultsearch.com<a/><br />
<a href="http://adultsearch.com">adultsearch.com</a><br />
</body>
</html>

