<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>AdultSearch Advertising - Prepay Balance Exhausted</title>
</head>
<body>
Hello,<br /><br />

Thank you for advertising with us.<br /><br />

Your campaigns prepaid funds have been exhausted. We have stopped running your ads until you transfer additional funds into your advertising account.<br /><br />

To make a payment, please follow these steps:<br /><br />
1. Log in to your AdultSearch account at <a href='http://adultsearch.com/advertise/'>http://adultsearch.com/advertise/</a><br />
2. Click 'Add Funds' at the top right next to the 'Your budget is' text.<br />
3. Follow the instructions to make a payment.<br /><br />

When we receive the funds, your ads will start running (or continue running if you are renewing your prepay balance). Make more payments as needed.<br /><br />

Thank you for advertising with AdultSearch.<br /><br />

Sincerely,<br /><br />

<a href="mailto:support@adultsearch.com">support@adultsearch.com<a/><br />
<a href="http://adultsearch.com">adultsearch.com</a><br />
</body>
</html>

