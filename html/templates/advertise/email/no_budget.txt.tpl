Hello,

Thank you for advertising with us.

Your campaigns prepaid funds have been exhausted. We have stopped running your ads until you transfer additional funds into your advertising account.

To make a payment, please follow these steps:
1. Log in to your AdultSearch account at http://adultsearch.com/advertise/
2. Click 'Add Funds' at the top right next to the 'Your budget is' text.
3. Follow the instructions to make a payment.

When we receive the funds, your ads will start running (or continue running if you are renewing your prepay balance). Make more payments as needed.

Thank you for advertising with AdultSearch.

Sincerely,

support@adultsearch.com
http://adultsearch.com
