{if $popup}
{$popup_url = "https://adultsearch.com/promo/p?sid={$popup}"}
{else if $url && $s_id}
{$popup_url = "https://adultsearch.com/promo/p?backup_url={$url}&s_id={$s_id}"}
{/if}
{if $popup_url}
{literal}
var _PopunderMobile = {

    attach:   function() {
        // Attach click callback to all anchors
        $('body').on('click', 'a', _PopunderMobile.click);
    },

    click:  function(ev) {
        var link  = $(this);
        //only fire popunder on links with defined href and not defined target
        if(!link.attr('target') && link.attr('href') !== 'undefined' && link.attr('href') !== undefined) {
            window.open(link.attr('href'), '');
            setTimeout('document.location.assign("' + _popunderSettings.url + '")', 50);
            ev.preventDefault();
        }
    },

};
_popunderSettings = { "url": {/literal}'{$popup_url}'{literal} }
$(document).ready(_PopunderMobile.attach);
{/literal}
{else}
var _trst = '{$current_timestamp}';
{/if}
