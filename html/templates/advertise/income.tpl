<h1>Income</h1>

<h3>Today ({$today})</h3>
<strong>Total made:</strong>: ${$today_total_made|string_format:"%.2f"}<br />
<br />
<div id="flot-pie-chart-1" style="width: 800px; height: 150px; font-size: 1.2em;"></div>

<h3>Yesterday ({$yesterday})</h3>
<strong>Total made:</strong>: ${$yesterday_total_made|string_format:"%.2f"}<br />
<br />
<div id="flot-pie-chart-2" style="width: 800px; height: 150px; font-size: 1.2em;"></div>

<script type="text/javascript">
$(function() {
    var data = [
	{section name=tz loop=$today_zones}
    {assign var=z value=$today_zones[tz]}
	{
        label: '{$z.name} (\$'+{$z.cost|string_format:"%.2f"}+')',
        data: {$z.cost},
    },
	{/section}
    ];
    var plotObj = $.plot($("#flot-pie-chart-1"), data, {
        series: {
            pie: {
                show: true
            }
        }
    });

    var data_yesterday = [
	{section name=tz loop=$yesterday_zones}
    {assign var=z value=$yesterday_zones[tz]}
	{
        label: '{$z.name} (\$'+{$z.cost|string_format:"%.2f"}+')',
        data: {$z.cost},
    },
	{/section}
    ];
    var plotObj = $.plot($("#flot-pie-chart-2"), data_yesterday, {
        series: {
            pie: {
                show: true
            }
        }
    });
});
</script>
