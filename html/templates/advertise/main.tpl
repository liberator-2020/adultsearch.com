
<div class="well well-sm" style="margin-bottom: 10px;">
	Your balance is <b>${$budget}</b>&nbsp;&nbsp;
	<a href="http://{$site_domain}/advertise/addfunds" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Funds</a>
	{if $fundneeded}
		&nbsp; &nbsp;<a href="https://adultsearch.com/advertise/addfunds" class="paylink">You must have enough funds in your account in order to run your ads. Click here to add more funds now.</a>
	{/if}
</div>

<div style="margin-bottom: 10px;">
	<a href="/advertise/edit?type=T" class="btn btn-success"><span class="glyphicon glyphicon-font" aria-hidden="true" style="color: #171717;"></span> Create Text Ad</a>
	<a href="/advertise/edit?type=B" class="btn btn-success"><span class="glyphicon glyphicon-picture" aria-hidden="true" style="color: #171717;"></span> Create Banner Ad</a>
	<a href="/advertise/edit?type=P" class="btn btn-success"><span class="glyphicon glyphicon-fullscreen" aria-hidden="true" style="color: #171717;"></span> Create Popup Ad</a>
	<a href="/advertise/edit?type=L" class="btn btn-success"><span class="glyphicon glyphicon-link" aria-hidden="true" style="color: #171717;"></span> Create Nav Link</a>
	<a href="/advertise/edit?type=I" class="btn btn-success"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true" style="color: #171717;"></span> Create Interstitial Ad</a>
</div>

{if !empty($campaign) }
<form action="" method="get" class="form-inline">
<table>
 <tr>
  <td width="160"><b>Select Report Date:</b></td>
  <td>
		<input type="radio" name="range_or_period" value="1" class="input form-control input-sm" id="periodd" {if $range_or_period==1 || !$range_or_period}checked="checked"{/if} />
		<select name="period" class="input form-control input-sm" onfocus="this.form.periodd.checked=true">
		<option value="0"{if $period==0} selected="selected"{/if}>Today</option>
		<option value="2"{if $period==2} selected="selected"{/if}>Yesterday</option>
		<option value="1"{if $period==1} selected="selected"{/if}>Last 7 Days</option>
		<option value="3"{if $period==3} selected="selected"{/if}>Last 30 Days</option>
		<option value="4"{if $period==4} selected="selected"{/if}>This Month</option>
		<option value="5"{if $period==5} selected="selected"{/if}>Last Month</option>
		</select>&nbsp;&nbsp;&nbsp;
		<i>or</i>&nbsp;

		<input type="radio" name="range_or_period" value="2" class="input form-control input-sm" {if $range_or_period==2}checked="checked"{/if} id="range" />
		<select name="monthFrom" class="input form-control input-sm" onfocus="this.form.range.checked=true">
		<option value="1"{if $monthFrom==1} selected="selected"{/if}>Jan</option>
		<option value="2"{if $monthFrom==2} selected="selected"{/if}>Feb</option>
		<option value="3"{if $monthFrom==3} selected="selected"{/if}>Mar</option>
		<option value="4"{if $monthFrom==4} selected="selected"{/if}>Apr</option>
		<option value="5"{if $monthFrom==5} selected="selected"{/if}>May</option>
		<option value="6"{if $monthFrom==6} selected="selected"{/if}>Jun</option>
		<option value="7"{if $monthFrom==7} selected="selected"{/if}>Jul</option>
		<option value="8"{if $monthFrom==8} selected="selected"{/if}>Aug</option>
		<option value="9"{if $monthFrom==9} selected="selected"{/if}>Sep</option>
		<option value="10"{if $monthFrom==10} selected="selected"{/if}>Oct</option>
		<option value="11"{if $monthFrom==11} selected="selected"{/if}>Nov</option>
		<option value="12"{if $monthFrom==12} selected="selected"{/if}>Dec</select>

		<input type="text" name="dayFrom" size="2" value="{$dayFrom}" maxlength="2" class="input form-control input-sm" onfocus="this.form.range.checked=true" />

		<input type="text" name="yearFrom" size="4" value="{$yearFrom}" maxlength="4" class="input form-control input-sm" onfocus="this.form.range.checked=true" />&nbsp;&nbsp;-&nbsp;&nbsp;

		<select name="monthTo" class="input form-control input-sm" onfocus="this.form.range.checked=true">
		<option value="1"{if $monthTo==1} selected="selected"{/if}>Jan</option>
		<option value="2"{if $monthTo==2} selected="selected"{/if}>Feb</option>
		<option value="3"{if $monthTo==3} selected="selected"{/if}>Mar</option>
		<option value="4"{if $monthTo==4} selected="selected"{/if}>Apr</option>
		<option value="5"{if $monthTo==5} selected="selected"{/if}>May</option>
		<option value="6"{if $monthTo==6} selected="selected"{/if}>Jun</option>
		<option value="7"{if $monthTo==7} selected="selected"{/if}>Jul</option>
		<option value="8"{if $monthTo==8} selected="selected"{/if}>Aug</option>
		<option value="9"{if $monthTo==9} selected="selected"{/if}>Sep</option>
		<option value="10"{if $monthTo==10} selected="selected"{/if}>Oct</option>
		<option value="11"{if $monthTo==11} selected="selected"{/if}>Nov</option>
		<option value="12"{if $monthTo==12} selected="selected"{/if}>Dec</option>
		</select>
		<input type="text" name="dayTo" size="2" value="{$dayTo}" maxlength="2" class="input form-control input-sm" onfocus="this.form.range.checked=true" />
		<input type="text" name="yearTo" size="4" value="{$yearTo}" maxlength="4" class="input form-control input-sm" onfocus="this.form.range.checked=true" />
		<button class="btn btn-default btn-sm" type="submit" value="Show Reports"/>Show Reports</button>
  </td>
 </tr>
</table>
</form>
<br />

<form method="post" action="">
<button type="submit" name="action" value="Pause" class="btn btn-default btn-xs">Pause</button>
<button type="submit" name="action" value="Activate" class="btn btn-default btn-xs">Activate</button>
<button type="submit" name="action" value="Archive" class="btn btn-default btn-xs">Archive</button>
{*
<button type="submit" name="action" value="Delete" class="btn btn-default btn-xs" onclick="return confirm('You may not reverse this process, are you sure ?');">Delete</button>
*}

{if $campaign}
	<table class="table table-condensed table-striped">
	<tbody>
	<tr class="header">
		<td style="width:10px"><input type="checkbox" onclick="checkall(this);" /></td>
		<td>Type</p>
		<td>Campaign Name</td>
		<td style="width:50px">Status</td>
		<td style="width:50px">Daily Budget</td>
		<td style="width:50px">Categories</td>
		<td style="width:100px">Impressions</td>
		<td style="width:100px">Clicks</td>
		<td style="width:100px">Cost</td>
		<td style="width:100px">Av. CPC</td>
		<td style="width:30px">&nbsp;</td>
	</tr>

	{section name=c loop=$campaign}
	{assign var=c value=$campaign[c]}
	<tr class={cycle values="r0,r1"}>
		<td><input type="checkbox" name="chk[]" value="{$c.id}" /></td>
		<td>
			{if $c.type == 'B'}
				<span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Banner
			{elseif $c.type == 'T'}
				<span class="glyphicon glyphicon-font" aria-hidden="true"></span> Text
			{elseif $c.type == 'P'}
				<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span> Popup
			{elseif $c.type == 'A'}
                <span class="glyphicon glyphicon-file" aria-hidden="true"></span>&nbsp;Ad
            {elseif $c.type == 'L'}
                <span class="glyphicon glyphicon-link" aria-hidden="true"></span>&nbsp;Nav&nbsp;Link
            {elseif $c.type == 'I'}
                <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>&nbsp;Interstitial&nbsp;Ad
			{/if}
		</td>
		<td><a href='/advertise/detail?id={$c.id}' title="Created on {$c.created_stamp|date_format:'%Y-%m-%d %H:%M:%S'}">{$c.name}</a></td>
		<td class="ac">
			{if $c.status == 1 && $c.budgetx == 0}
				<span class="status_active">Live</span>
			{elseif $c.status==1&&$c.budgetx==1}
				<span class="status_paused">Budget Reached</span>
			{elseif $c.status==0}
				<span class="status_paused">Paused</span>
			{elseif $c.status==-1}
				<span class="status_deleted">&nbsp;</span>
			{/if}
		</td>
		<td>{$c.budget}</td>
		<td class="ac">{if $c.type == "T"}{$c.category}{/if}</td>
		<td class="ar">{if $c.type != "P"}{$c.impression}{/if}</td>
		<td class="ar">{if $c.type == "T" || $c.type == "P" || $c.type == "L" || $c.type == "I"}{$c.click}{/if}</td>
		<td class="ar">{if $c.cost}${$c.cost}{/if}</td>
		<td class="ar">{if $c.type != "B" && $c.acpc}${$c.acpc}{/if}</td>
		<td class="ac"><a href="{if $c.type != "B"}/advertise/edit?id={$c.id}{else}/advertise/edit?id={$c.id}{/if}">Edit</a></td>
		</td>
	</tr>
	{/section}

	<tr class="total">
		<td colspan="6"><b>Totals:</b></td>
		<td>{if $c.type != "P"}{$total_i}{/if}</td>
		<td>{if $c.type == "T" || $c.type == "P" || $c.type == "I"}{$total_c}{/if}</td>
		<td>${$total_cost}</td>
		<td>{if $c.type != "B"}${$total_a}{/if}</td>
		<td>&nbsp;</td>
	</tr>
	</tbody>
	</table>
{/if}
</form>

{/if}

{literal}
<script type="text/javascript">
<!--
function checkall(t) {
	$("input[type=checkbox]").each(function() { $(this).attr("checked", t.checked); } );
}
-->
</script>
{/literal}
