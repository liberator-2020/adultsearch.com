<h1>Placement Stats</h1>

{*
<form action="" method="get" class="form-inline">
<label>Zone group:</label>
<select name="zone_group_id" class="input-sm form-control">
    <option value="">All</option>
    {foreach $zone_groups as $group_id => $group_name}
        <option value="{$group_id}" {if $group_id == $zone_group_id} selected="selected"{/if}>{$group_name}</option>
    {/foreach}
</select>
<label>Zone type:</label>
<select name="zone_type" class="input-sm form-control">
	<option value="">-</option>
    <option value="T"{if $zone_type == 'T'} selected="selected"{/if}>Text Ads</option>
    <option value="B"{if $zone_type == 'B'} selected="selected"{/if}>Banners</option>
    <option value="P"{if $zone_type == 'P'} selected="selected"{/if}>Popups</option>
    <option value="A"{if $zone_type == 'A'} selected="selected"{/if}>Sponsor Ads</option>
    <option value="L"{if $zone_type == 'L'} selected="selected"{/if}>Nav Links</option>
</select>
<input type="submit" name="submit" value="Show" class="btn btn-default btn-sm" />
</form>
*}

<table class="table table-striped table-condensed">
<thead>
<tr>
	<th>Placement #Id</th>
	<th>Type</th>
	<th>Campaign</th>
	<th>Zone</th>
	<th>Impressions Since</th>
	<th/>
</tr>
</thead>
<tbody>
{foreach from=$placements key=key item=p}
<tr>
	<td>{$p.cs_id}</td>
	<td>
		{if $p.c_type == 'B'}
			<span class="glyphicon glyphicon-picture" aria-hidden="true" style="color: green;"></span>
		{elseif $p.c_type == 'T'}
			<span class="glyphicon glyphicon-font" aria-hidden="true" style="color: blue;"></span>
		{elseif $p.c_type == 'P'}
			<span class="glyphicon glyphicon-fullscreen" aria-hidden="true" style="color: orange;"></span>
		{elseif $p.c_type == 'A'}
			<span class="glyphicon glyphicon-file" aria-hidden="true" style="color: purple;"></span>
		{elseif $p.c_type == 'L'}
			<span class="glyphicon glyphicon-link" aria-hidden="true" style="color: red;"></span>
		{/if}
	</td>
	<td>
		#{$p.c_id} - {$p.c_name}
	</td>
	<td>
		#{$p.s_id} - {$p.s_name}
	</td>
	<td>
		{if $p.c_type == 'B'}
		{$p.cs_is} - {$p.cs_is2}
		{/if}
	</td>
	<td />
</tr>
{/foreach}
</tbody>
</table>

