<h1>Stats</h1>

<form action="" method="GET" class="form-inline">

<label>Type:</label>
<select name="type" class="input-sm form-control">
	<option value="">-</option>
	<option value="T"{if $type == 'T'} selected="selected"{/if}>Text Ads</option>
	<option value="B"{if $type == 'B'} selected="selected"{/if}>Banners</option>
	<option value="P"{if $type == 'P'} selected="selected"{/if}>Popups</option>
	<option value="P"{if $type == 'L'} selected="selected"{/if}>Nav Links</option>
</select>

<br />

<strong>Select Report Date:</strong>
	<input type="radio" name="range_or_period" value="1" class="input-sm form-control" id="periodd" {if $range_or_period==1 || !$range_or_period}checked="checked"{/if} />
	<select name="period" class="input-sm form-control" onfocus="this.form.periodd.checked=true">
		<option value="0"{if $period==0} selected="selected"{/if}>Today</option>
		<option value="2"{if $period==2} selected="selected"{/if}>Yesterday</option>
		<option value="1"{if $period==1} selected="selected"{/if}>Last 7 Days</option>
		<option value="3"{if $period==3} selected="selected"{/if}>Last 30 Days</option>
		<option value="4"{if $period==4} selected="selected"{/if}>This Month</option>
		<option value="5"{if $period==5} selected="selected"{/if}>Last Month</option>
	</select>

	&nbsp;&nbsp;&nbsp;<i>or</i>&nbsp;

	<input type="radio" name="range_or_period" value="2" class="input-sm form-control" {if $range_or_period==2}checked="checked"{/if} id="range" />
	<select name="monthFrom" class="input-sm form-control" onfocus="this.form.range.checked=true">
		<option value="1"{if $monthFrom==1} selected="selected"{/if}>Jan</option>
		<option value="2"{if $monthFrom==2} selected="selected"{/if}>Feb</option>
		<option value="3"{if $monthFrom==3} selected="selected"{/if}>Mar</option>
		<option value="4"{if $monthFrom==4} selected="selected"{/if}>Apr</option>
		<option value="5"{if $monthFrom==5} selected="selected"{/if}>May</option>
		<option value="6"{if $monthFrom==6} selected="selected"{/if}>Jun</option>
		<option value="7"{if $monthFrom==7} selected="selected"{/if}>Jul</option>
		<option value="8"{if $monthFrom==8} selected="selected"{/if}>Aug</option>
		<option value="9"{if $monthFrom==9} selected="selected"{/if}>Sep</option>
		<option value="10"{if $monthFrom==10} selected="selected"{/if}>Oct</option>
		<option value="11"{if $monthFrom==11} selected="selected"{/if}>Nov</option>
		<option value="12"{if $monthFrom==12} selected="selected"{/if}>Dec</option>
	</select>

	<input type="text" name="dayFrom" size="2" value="{$dayFrom}" maxlength="2" class="input-sm form-control" onfocus="this.form.range.checked=true" />
	<input type="text" name="yearFrom" size="4" value="{$yearFrom}" maxlength="4" class="input-sm form-control" onfocus="this.form.range.checked=true" />&nbsp;&nbsp;-&nbsp;&nbsp;
	<select name="monthTo" class="input-sm form-control" onfocus="this.form.range.checked=true">
		<option value="1"{if $monthTo==1} selected="selected"{/if}>Jan</option>
		<option value="2"{if $monthTo==2} selected="selected"{/if}>Feb</option>
		<option value="3"{if $monthTo==3} selected="selected"{/if}>Mar</option>
		<option value="4"{if $monthTo==4} selected="selected"{/if}>Apr</option>
		<option value="5"{if $monthTo==5} selected="selected"{/if}>May</option>
		<option value="6"{if $monthTo==6} selected="selected"{/if}>Jun</option>
		<option value="7"{if $monthTo==7} selected="selected"{/if}>Jul</option>
		<option value="8"{if $monthTo==8} selected="selected"{/if}>Aug</option>
		<option value="9"{if $monthTo==9} selected="selected"{/if}>Sep</option>
		<option value="10"{if $monthTo==10} selected="selected"{/if}>Oct</option>
		<option value="11"{if $monthTo==11} selected="selected"{/if}>Nov</option>
		<option value="12"{if $monthTo==12} selected="selected"{/if}>Dec</option>
	</select>
	<input type="text" name="dayTo" size="2" value="{$dayTo}" maxlength="2" class="input-sm form-control" onfocus="this.form.range.checked=true" />
	<input type="text" name="yearTo" size="4" value="{$yearTo}" maxlength="4" class="input-sm form-control" onfocus="this.form.range.checked=true" />
	<button type="submit" class="btn btn-sm" name="show" value="1">Show Reports</button>
	<button type="submit" class="btn btn-sm" name="export" value="1">Export to CSV</button>
</form>

<table class="table table-condensed table-striped">
<thead>
<tr>
	<th>Zone #Id</th>	
	<th>Type</th>	
	<th>Name</th>	
	<th style="text-align: right;">Impressions</th>	
	<th style="text-align: right;">Clicks</th>	
	<th style="text-align: right;">Cost</th>	
	<th></th>
</tr>
</thead>
<tbody>
{foreach from=$zones item=zone}
<tr>
	<td>{$zone.id}</td>
	<td>
		{if $zone.type == 'B'}
			<span class="glyphicon glyphicon-picture" aria-hidden="true" style="color: green;"></span> Banner
		{elseif $zone.type == 'T'}
			<span class="glyphicon glyphicon-font" aria-hidden="true" style="color: blue;"></span> Text
		{elseif $zone.type == 'P'}
			<span class="glyphicon glyphicon-fullscreen" aria-hidden="true" style="color: orange;"></span> Popup
		{elseif $zone.type == 'A'}
			<span class="glyphicon glyphicon-file" aria-hidden="true" style="color: purple;"></span> Ad
		{elseif $zone.type == 'L'}
			<span class="glyphicon glyphicon-link" aria-hidden="true" style="color: red;"></span> Nav Link
		{/if}
	</td>
	<td>{$zone.name}</td>
	<td style="text-align: right;">{$zone.impression}</td>
	<td style="text-align: right;">{$zone.click}</td>
	<td style="text-align: right;">${$zone.cost}</td>
	<td>
		<a href="/advertise/zone_detail?id={$zone.id}" class="btn btn-xs btn-default">Detail</a>
		<a href="/advertise/stats_zone?id={$zone.id}" class="btn btn-xs btn-default">Stats</a>
		<a href="/advertise/stats_zone_account?id={$zone.id}" class="btn btn-xs btn-default">Account Stats</a>
	</td>
</tr>
{/foreach}
</tbody>
</table>

