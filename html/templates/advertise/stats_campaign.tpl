<h1>Stats for campaing #{$campaign.id} - {$campaign.name}</h1>

<form action="" method="GET" class="form-inline">

<strong>Select Report Date:</strong>
	<input type="radio" name="range_or_period" value="1" class="input-sm form-control" id="periodd" {if $range_or_period==1 || !$range_or_period}checked="checked"{/if} />
	<select name="period" class="input-sm form-control" onfocus="this.form.periodd.checked=true">
		<option value="1"{if $period==1} selected="selected"{/if}>Last 7 Days</option>
		<option value="3"{if $period==3} selected="selected"{/if}>Last 30 Days</option>
		<option value="4"{if $period==4} selected="selected"{/if}>This Month</option>
		<option value="5"{if $period==5} selected="selected"{/if}>Last Month</option>
	</select>

	&nbsp;&nbsp;&nbsp;<i>or</i>&nbsp;

	<input type="radio" name="range_or_period" value="2" class="input-sm form-control" {if $range_or_period==2}checked="checked"{/if} id="range" />
	<select name="monthFrom" class="input-sm form-control" onfocus="this.form.range.checked=true">
		<option value="1"{if $monthFrom==1} selected="selected"{/if}>Jan</option>
		<option value="2"{if $monthFrom==2} selected="selected"{/if}>Feb</option>
		<option value="3"{if $monthFrom==3} selected="selected"{/if}>Mar</option>
		<option value="4"{if $monthFrom==4} selected="selected"{/if}>Apr</option>
		<option value="5"{if $monthFrom==5} selected="selected"{/if}>May</option>
		<option value="6"{if $monthFrom==6} selected="selected"{/if}>Jun</option>
		<option value="7"{if $monthFrom==7} selected="selected"{/if}>Jul</option>
		<option value="8"{if $monthFrom==8} selected="selected"{/if}>Aug</option>
		<option value="9"{if $monthFrom==9} selected="selected"{/if}>Sep</option>
		<option value="10"{if $monthFrom==10} selected="selected"{/if}>Oct</option>
		<option value="11"{if $monthFrom==11} selected="selected"{/if}>Nov</option>
		<option value="12"{if $monthFrom==12} selected="selected"{/if}>Dec</option>
	</select>

	<input type="text" name="dayFrom" size="2" value="{$dayFrom}" maxlength="2" class="input-sm form-control" onfocus="this.form.range.checked=true" />
	<input type="text" name="yearFrom" size="4" value="{$yearFrom}" maxlength="4" class="input-sm form-control" onfocus="this.form.range.checked=true" />&nbsp;&nbsp;-&nbsp;&nbsp;
	<select name="monthTo" class="input-sm form-control" onfocus="this.form.range.checked=true">
		<option value="1"{if $monthTo==1} selected="selected"{/if}>Jan</option>
		<option value="2"{if $monthTo==2} selected="selected"{/if}>Feb</option>
		<option value="3"{if $monthTo==3} selected="selected"{/if}>Mar</option>
		<option value="4"{if $monthTo==4} selected="selected"{/if}>Apr</option>
		<option value="5"{if $monthTo==5} selected="selected"{/if}>May</option>
		<option value="6"{if $monthTo==6} selected="selected"{/if}>Jun</option>
		<option value="7"{if $monthTo==7} selected="selected"{/if}>Jul</option>
		<option value="8"{if $monthTo==8} selected="selected"{/if}>Aug</option>
		<option value="9"{if $monthTo==9} selected="selected"{/if}>Sep</option>
		<option value="10"{if $monthTo==10} selected="selected"{/if}>Oct</option>
		<option value="11"{if $monthTo==11} selected="selected"{/if}>Nov</option>
		<option value="12"{if $monthTo==12} selected="selected"{/if}>Dec</option>
	</select>
	<input type="text" name="dayTo" size="2" value="{$dayTo}" maxlength="2" class="input-sm form-control" onfocus="this.form.range.checked=true" />
	<input type="text" name="yearTo" size="4" value="{$yearTo}" maxlength="4" class="input-sm form-control" onfocus="this.form.range.checked=true" />
<br />

	<label>Group by:</label>
	<select name="group" class="input-sm form-control">
		<option value="daily"{if $group == 'daily'} selected="selected"{/if}>Day</option>
		<option value="monthly"{if $group == 'monthly'} selected="selected"{/if}>Month</option>
		<option value="yearly"{if $group == 'yearly'} selected="selected"{/if}>Year</option>
	</select>
	<br />

	<input type="hidden" name="id" value="{$campaign.id}" />
	<button type="submit" class="btn btn-sm" name="show" value="1">Show Reports</button>
	<button type="submit" class="btn btn-sm" name="export" value="1">Export to CSV</button>
</form>

<table class="table table-condensed table-striped">
<thead>
<tr>
	{if $group == "yearly" || $group == "monthly" || $group == "daily"}
		<th>Year</th>	
	{/if}
	{if $group == "monthly" || $group == "daily"}
		<th>Month</th>	
	{/if}
	{if $group == "daily"}
		<th>Day</th>	
	{/if}
	<th style="text-align: right;">Impressions</th>
	{if $campaign.type == "T"}
		<th style="text-align: right;">Clicks</th>	
		<th style="text-align: right;">CTR</th>	
	{/if}
	<th style="text-align: right;">Profit</th>	
</tr>
</thead>
<tbody>
{foreach from=$stats item=stat}
<tr>
	{if $group == "yearly" || $group == "monthly" || $group == "daily"}
		<td>{$stat.year}</td>	
	{/if}
	{if $group == "monthly" || $group == "daily"}
		<td>{$stat.month}</td>	
	{/if}
	{if $group == "daily"}
		<td>{$stat.day}</td>	
	{/if}
	<td style="text-align: right;">{$stat.impressions}</td>
	{if $campaign.type == "T"}
		<td style="text-align: right;">{$stat.clicks}</td>
		<td style="text-align: right;">{$stat.ctr}</td>
	{/if}
	<td style="text-align: right;">{$stat.profit}</td>
</tr>
{/foreach}
</tbody>
</table>

