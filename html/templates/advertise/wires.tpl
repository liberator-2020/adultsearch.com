<style type"text.css">
tr.mobile, tr.mobile td {
	background-color: cornsilk;
}
table.borderless>tbody>tr>td, table.borderless>tbody>tr>th {
	border: none;
}
table.borderless>tbody>tr.inner_first>td, table.borderless>tbody>tr.inner_first>th {
	border-top: 1px solid #ccc;
}
</style>
<h1>
	Wires
</h1>

<form action="" method="get" class="form-inline">
<label>Status:</label>
<select name="status" class="input-sm form-control">
	<option value="">All</option>
	<option value="0"{if $status == '0'} selected="selected"{/if}>Unprocessed</option>
	<option value="1"{if $status == '1'} selected="selected"{/if}>Processed</option>
</select>
<input type="submit" name="submit" value="Show" class="btn btn-default btn-sm" />
</form>

<table class="table table-condensed borderless">
<thead>
<tr>
	<th>#Id</th>
	<th>Date/Time</th>
	<th>Account</th>
	<th>Name</th>
	<th>Amount</th>
	<th>Bank Name</th>
	<th>Image</th>
	<th>Status</th>
	<th/>
</tr>
</thead>
<tbody>
{foreach from=$wires item=aw}
	{cycle values="white,#f5f5f5" assign="rowcolor"}
	{foreach from=$aw item=w name=inner}
		<tr style="background-color: {$rowcolor};"{if $smarty.foreach.inner.index == 0} class="inner_first"{/if} >
			<td>{$w.id}</td>
			<td>{$w.created_stamp|date_format:"m/d/Y g:i a"}</td>
			<td>#{$w.account_id}<br />{$w.email}</td>
			<td>{$w.name}</td>
			<td>{$w.amount}</td>
			<td>{$w.bank_name}</td>
			<td>
				{if $w.thumbnail}
					<a href="/advertise/wire?action=image&id={$w.id}" target="_blank"><img src="/advertise/wire?action=thumbnail&id={$w.id}" style="max-width: 100px; max-height: 100px;" /></a>
				{else}
					<span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>
				{/if}
			</td>
			<td>
				{if $w.status == 1}
					Processed
				{else}
					Unprocessed
				{/if}
			</td>
			<td>
				<a href="/advertise/wire?action=process&id={$w.id}" class="btn btn-default btn-xs">Mark as processed</a>
				<a href="/advertise/advertiser?action=addfunds&id={$w.account_id}&wire={$w.id}" class="btn btn-default btn-xs">Add funds</a>
			</td>
		</tr>
	{/foreach}
{/foreach}
</tbody>
</table>
