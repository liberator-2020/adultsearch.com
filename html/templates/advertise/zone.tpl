
<form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			{if $new}
				<h1>Add new zone</h1>
			{else}
				<h1>Edit Zone #{$zone.id}</h1>
			{/if}
		</div>
	</div>

{if $isadmin}
	<div class="form-group">
		<label for="" class="col-sm-2 control-label">Owner:</label>
		<div class="col-sm-10">
			<div id="change_owner_current" style="margin-top: 7px;">
				{$account} <a onclick="change_owner(); return false;" class="btn btn-default btn-xs">Change owner</a>
			</div>
			<div id="change_owner_ac" style="display: none;">
				<input type="search" placeholder="Type account id, username or email" id="account_suggest" name="account_suggest"  class="form-control ac-input" value="" />
			</div>
			<input type="hidden" id="account_id" name="account_id" value="{$account_id}">
		</div>
	</div>
{/if}

	<div class="form-group">
		<label for="" class="col-sm-2 control-label">Type:</label>
		<div class="col-sm-10">
			<select name="type" id="type" class="form-control" style="width: 130px;" >
				<option value="T" {if $zone.type == 'T'}selected="selected"{/if}>Text Ad</option>
				<option value="B" {if $zone.type == 'B'}selected="selected"{/if}>Banner</option>
				<option value="P" {if $zone.type == 'P'}selected="selected"{/if}>Popup</option>
				<option value="A" {if $zone.type == 'A'}selected="selected"{/if}>Sponsor Ad</option>
				<option value="L" {if $zone.type == 'L'}selected="selected"{/if}>Nav Link</option>
				<option value="I" {if $zone.type == 'I'}selected="selected"{/if}>Interstitial</option>
			</select>
		</div>
	</div>

	<div class="form-group">
		<label for="" class="col-sm-2 control-label">Status:</label>
		<div class="col-sm-10">
			<select name="status" id="statu" class="form-control" style="width: 120px;" >
				<option value="-1" {if $zone.status == -1}selected="selected"{/if}>Delete</option>
				<option value="0" {if $zone.status == 0}selected="selected"{/if}>Not Active</option>
				<option value="1" {if $zone.status == 1}selected="selected"{/if}>Active</option>
			</select>
		</div>
	</div>

	<div class="form-group row">
		<label for="" class="col-sm-2 control-label">Group:</label>
		<div class="col-sm-10">
			<select id="group_id" name="group_id" class="form-control">{html_options options=$group_options selected=$zone.group_id}</select>
		</div>
	</div>

	<div class="form-group row">
		<label for="" class="col-sm-2 control-label">Nickname:</label>
		<div class="col-sm-10">
			<input type="text" name="nickname" id="nickname" class="form-control" value="{$zone.nickname}" />
			<small>This will be used only in admin panel, not visible to advertisers, e.g. 'AS Mobile POP'</small>
		</div>
	</div>

	<div class="form-group row">
		<label for="" class="col-sm-2 control-label">Name:</label>
		<div class="col-sm-10">
			<input type="text" name="name" id="name" class="form-control" value="{$zone.name}" />
			<small>This will be displayed to advertisers, needs to be descriptive, for example 'www.adultsearch.com - Mobile footer banner 300x250'</small>
		</div>
	</div>

	<div class="form-group">
		<label for="" class="col-sm-2 control-label">Minimum CPC bid:</label>
		<div class="col-sm-10">
			<div class="input-group">
				<span class="input-group-addon">$</span>
				<input type="text" name="defaultbid" id="defaultbid" class="form-control" value="{$zone.defaultbid}" style="width: 100px;" />
			</div>
			<small>For text ads, it is usually $0.20, for popups around $0.06</small>
		</div>
	</div>

	<div class="form-group">
		<label for="" class="col-sm-2 control-label">Minimum CPM:</label>
		<div class="col-sm-10">
			<div class="input-group">
				<span class="input-group-addon">$</span>
				<input type="text" name="cpm_minimum" id="cpm_minimum" class="form-control" value="{$zone.cpm_minimum}" style="width: 100px;" />
			</div>
			<small>Cost per 1000 impressions, usually around $4.50</small>
		</div>
	</div>

	<div class="form-group">
		<label for="" class="col-sm-2 control-label">Example screenshot:</label>
		<div class="col-sm-10">
			{if $zone.example_screenshot}
				<img src="/images/advertise/example/{$zone.example_screenshot}" style="max-width: 100%;" />
				<br /><br />
			{/if}
			<input type="file" name="example_screenshot_file" class="form-control" />
			<small>Upload image, which shows to advertiser, how is his ad going to be displayed. Supports JPG images.</small>
		</div>
	</div>

	<div class="form-group">
		<label for="" class="col-sm-2 control-label" id="backuphtml_label">Backup HTML:</label>
		<div class="col-sm-10">
			<textarea name="backuphtml" class="form-control">{$zone.backuphtml}</textarea>
			<small>This HTML will be used, if no advertiser is found for this zone.</small>
		</div>
	</div>

	<div class="form-group row">
		<label for="" class="col-sm-2 control-label">Volume:</label>
		<div class="col-sm-10">
			<input type="text" name="volume" id="volume" class="form-control" value="{$zone.volume}" />
			<small>This will be displayed to advertisers, text description of the volume of the zone, e.g. '100K impressions / day'</small>
		</div>
	</div>

	<div class="form-group row">
		<label for="" class="col-sm-2 control-label">Exclusive access:</label>
		<div class="col-sm-10">
			<input type="text" name="exclusive_access" id="exclusive_access" class="form-control" value="{$zone.exclusive_access}" />
			<small>Coma separated list of account ids of advertiser, who have exclusive access to this zone</small>
		</div>
	</div>

	<div class="form-group">
		<label for="" class="col-sm-2 control-label">Admin notes:</label>
		<div class="col-sm-10">
			<textarea name="notes" class="form-control">{$zone.notes}</textarea>
			<small>These notes are for admin use only.</small>
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit" />
		</div>
	</div>

	{if $new}
		<input type="hidden" name="new" value="1" />
	{/if}

</form>

{literal}
<script type="text/javascript">
function show_proper_attributes() {
	var type = $('#type').val();
	$('#defaultbid').closest('div.form-group').hide();
	$('#cpm_minimum').closest('div.form-group').hide();
	if (type == 'T' || type == 'P' || type == 'L') {
		$('#defaultbid').closest('div.form-group').show();
	} else if (type == 'B') {
		$('#cpm_minimum').closest('div.form-group').show();
	}

	if (type == 'P') {
		$('#backuphtml_label').html('Default POP URL:');
	} else {
		$('#backuphtml_label').html('Backup HTML:');
	}
}

function change_owner() {
	$('#change_owner_current').hide();
	$('#change_owner_ac').show();
}

$(document).ready(function() {
	$('#type').change(function() {show_proper_attributes()});
	show_proper_attributes();
	$('#account_suggest').autocomplete({
		source: '/_ajax/account_search',
		minLength: 2,
		select: function( event, ui ) {
			$("#account_suggest").val(ui.item.label);
			$("#account_id").val(ui.item.id);
		},
		change: function( event, ui ) {
			$("#account_suggest").val(ui.item.label);
			$("#account_id").val(ui.item.id);
		}
	});
});
</script>
{/literal}
