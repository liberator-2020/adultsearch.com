
	<div class="row">
		<div class="col-sm-12">
			<h1>
				Zone #{$zone.id} - {$zone.name}
				<a href="/advertise/stats_zone_account?id={$zone.id}" class="btn btn-sm btn-primary" style="margin-left: 30px;">account stats</a>
			</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-2">
			<label>Placement summary:</label>
		</div>
		<div class="col-sm-10">
			Not approved: <strong>{$not_approved}</strong>&nbsp;&nbsp;Out of budget: <strong>{$out_of_budget}</strong>&nbsp;&nbsp;Paused: <strong>{$paused}</strong>&nbsp;&nbsp;Live: <strong>{$live}</strong>&nbsp;&nbsp;Total: <strong>{$total}</strong>
		</div>
	</div>

{if $not_approved}
	<div class="row">
		<div class="col-sm-2">
			Not approved placements:
		</div>
		<div class="col-sm-10">
			TODO
		</div>
	</div>
{/if}

{if $out_of_budget}
	<div class="row">
		<div class="col-sm-2">
			Out of budget placements:
		</div>
		<div class="col-sm-10">
			TODO
		</div>
	</div>
{/if}

{if $paused}
	<div class="row">
		<div class="col-sm-2">
			Paused placements ({$paused}):
		</div>
		<div class="col-sm-10" style="max-height: 180px; overflow: auto;">
			{foreach from=$placements key=cs_id item=p}
			{if $p.status == "paused"}
				<a href="/advertise/campaign_detail?id={$p.c_id}">#{$p.c_id} - {$p.c_name}</a> 
				 (<a href="/advertise/advertiser?id={$p.account_id}">#{$p.account_id} - {$p.email}</a>)
				<br />&nbsp;&nbsp;Impressions: First on <strong>{$p.imp_date_first}</strong>, Last on <strong>{$p.imp_date_last}</strong>, Total: <strong>{$p.imp_total}</strong>, Today so far: <strong>{$p.imp_today_so_far}</strong>
				<br />
			{/if}
			{/foreach}
		</div>
	</div>
{/if}

{if $live}
	<div class="row">
		<div class="col-sm-2">
			<label>Live placements:</label>
		</div>
		<div class="col-sm-10">
			{foreach from=$placements key=cs_id item=p}
			{if $p.status == "live"}
				<a href="/advertise/campaign_detail?id={$p.c_id}">#{$p.c_id} - {$p.c_name}</a> 
				 (<a href="/advertise/advertiser?id={$p.account_id}">#{$p.account_id} - {$p.email}</a>)
				<br />
				&nbsp;&nbsp;{if $zone.type == "B"}CPM: <strong>{$p.cpm}</strong>{else}Bid: <strong>{$p.bid}</strong>{/if} #{$p.cs_id}
				<br />
				&nbsp;&nbsp;Impressions: First on <strong>{$p.imp_date_first}</strong>, Last on <strong>{$p.imp_date_last}</strong>, Total: <strong>{$p.imp_total}</strong>, Yesterday: <strong>{$p.imp_yesterday}</strong>, Today so far: <strong>{$p.imp_today_so_far}</strong>
				<br />
				<hr style="margin: 0px;"/>
			{/if}
			{/foreach}
		</div>
	</div>
{/if}

	<div class="row">
		<div class="col-sm-12">
			<h3>Impressions</h3>
			{if $impressions}
				<div id="flot-chart-1" style="width: 1150px; height: 350px; font-size: 1.2em;"></div>
			{else}
				No impressions recorded in last month.
			{/if}
			<h3>Costs</h3>
			{if $costs}
				<div id="flot-chart-2" style="width: 1150px; height: 350px; font-size: 1.2em;"></div>
			{else}
				No costs recorded in last month.
			{/if}
		</div>
	</div>

<script type="text/javascript">
var data_impressions = [
{foreach from=$impressions key=cs_id item=i1}
	{ label: '{$i1.c_name}', data: [
		{foreach from=$i1.data key=day item=imp}
			[{$day_indexes[$day]}, {$imp}],	
		{/foreach}
	] },
{/foreach}
];

var data_costs = [
{foreach from=$costs key=cs_id item=c1}
	{ label: '{$c1.c_name}', data: [
		{foreach from=$c1.data key=day item=cost}
			[{$day_indexes[$day]}, {$cost}],	
		{/foreach}
	] },
{/foreach}
];

var day_ticks = [
{foreach from=$day_legend key=day_num item=day_str}
	[{$day_num}, '{$day_str}'],	
{/foreach}
];

thousandFormatter = function(val, axis) {
	return val.toString().replace(/./g, function(c, i, a) {
		return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
	});
}
dollarFormatter = function(val, axis) { return "$" + val;}

$(function() {
	{if $impressions}
		$.plot("#flot-chart-1", data_impressions, { xaxis: { ticks: day_ticks}, yaxis: { tickFormatter: thousandFormatter}});
	{/if}
	{if $costs}
		$.plot("#flot-chart-2", data_costs, { xaxis: { ticks: day_ticks}, yaxis: { tickFormatter: dollarFormatter}});
	{/if}
});
</script>
