<style type"text.css">
tr.mobile, tr.mobile td {
	background-color: cornsilk;
}
</style>
<h1>
	Zones
	<a href="/advertise/zone?new" class="btn btn-sm btn-primary" style="margin-left: 30px;">new</a>
</h1>

<form action="" method="get" class="form-inline">
<label>Zone group:</label>
<select name="zone_group_id" class="input-sm form-control">
    <option value="">All</option>
    {foreach $zone_groups as $group_id => $group_name}
        <option value="{$group_id}" {if $group_id == $zone_group_id} selected="selected"{/if}>{$group_name}</option>
    {/foreach}
</select>
<label>Zone type:</label>
<select name="zone_type" class="input-sm form-control">
    <option value="">-</option>
    <option value="T"{if $zone_type == 'T'} selected="selected"{/if}>Text Ads</option>
    <option value="B"{if $zone_type == 'B'} selected="selected"{/if}>Banners</option>
    <option value="P"{if $zone_type == 'P'} selected="selected"{/if}>Popups</option>
    <option value="A"{if $zone_type == 'A'} selected="selected"{/if}>Sponsor Ads</option>
    <option value="L"{if $zone_type == 'L'} selected="selected"{/if}>Nav Links</option>
    <option value="I"{if $zone_type == 'I'} selected="selected"{/if}>Interstitial</option>
</select>
<label>D/M:</label>
<select name="desktop_mobile" class="input-sm form-control">
    <option value="">-</option>
    <option value="D"{if $desktop_mobile == 'D'} selected="selected"{/if}>Desktop</option>
    <option value="M"{if $desktop_mobile == 'M'} selected="selected"{/if}>Mobile</option>
</select>
<input type="submit" name="submit" value="Show" class="btn btn-default btn-sm" />
</form>

<table class="table table-striped table-condensed">
<thead>
<tr>
	<th>#Id</th>
	<th>Type</th>
	<th>Name</th>
	<th>Default bid /<br />Min. CPM</th>
	<th>Volume</th>
	<th>Campaigns<br />advertising</th>
	<th/>
</tr>
</thead>
<tbody>
{section name=zi loop=$zones}
{assign var=z value=$zones[zi]}
	<tr {if $z.mobile == 1}class="mobile"{/if}>
		<td data-group-id="{$z.group_id}">{$z.id}</td>
		<td>
			{if $z.type == 'B'}
            <span class="glyphicon glyphicon-picture" aria-hidden="true" style="color: green;"></span> Banner
	        {elseif $z.type == 'T'}
            	<span class="glyphicon glyphicon-font" aria-hidden="true" style="color: blue;"></span> Text
        	{elseif $z.type == 'P'}
    	        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true" style="color: orange;"></span> Popup
        	{elseif $z.type == 'A'}
    	        <span class="glyphicon glyphicon-file" aria-hidden="true" style="color: purple;"></span> Ad
        	{elseif $z.type == 'L'}
    	        <span class="glyphicon glyphicon-link" aria-hidden="true" style="color: red;"></span>&nbsp;Nav&nbsp;Link
	        {/if}
		</td>
		<td>{$z.name}</td>
		<td>{if $z.type == 'B'}{$z.cpm_minimum}{else}{$z.defaultbid}{/if}</td>
		<td>{$z.volume}</td>
		<td>{$z.campaigns_advertising}</td>
		<td>
			<a href="/advertise/zone_detail?id={$z.id}" class="btn btn-default btn-xs">Detail</a>
			{if $account_level == 3}
				<a href="/advertise/zone?id={$z.id}" class="btn btn-default btn-xs">Edit</a>
			{/if}
			<a href="/advertise/stats_zone?id={$z.id}" class="btn btn-xs btn-default">Stats</a>
			<a href="/advertise/stats_zone_account?id={$z.id}" class="btn btn-xs btn-default">Account Stats</a>
		</td>
	</tr>
{/section}
</tbody>
</table>
