<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="google-site-verification" content="mqImYun7BuewEVh_ALw82P9EZUyvfyjGWw3x7eKy_vM" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="RATING" content="RTA-5042-1996-1400-1577-RTA" />
<title>Adultsearch Advertising</title>
<link rel="icon" type="image/png" href="/favicon.ico">
<meta name="description" content="{$description}" />
{if $devserver}<meta name="robots" content="nofollow, noindex" />{/if}
{if $keywords}<meta name="keywords" content="{$keywords}" />{/if}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="//adultsearch.com/js/ui/jquery-ui-1.9.1.custom.min.js"></script>
<link href="{$config_site_url}/js/ui/start/jquery-ui-1.9.1.custom.min.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700' rel='stylesheet' type='text/css'>
<link href="{$config_site_url}/css/advertise.css?3" rel="stylesheet" type="text/css" />

{$html_head_include}
{if !$devserver&&$smarty.server.HTTP_HOST=="adultsearch.com"}<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-5124307-2']);
  _gaq.push(['_setDomainName', '.adultsearch.com']);
  _gaq.push(['_trackPageview']);
 {if $adultsearchnet} _gaq.push(['_setCustomVar', 1, 'Visitor Type', 'ASNET', 1]);{/if}
  (function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
{/if}
</head>

<body>

	<!-- Fixed navbar -->
	<nav class="navbar navbar-inverse navbar-static-top">
		<div class="container-fluid">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="/advertise">{if $smarty.session.account != 3974}<span class="logo_1">AdultSearch</span> {/if}<span class="logo_2">Advertising</span></a>
			</div>

			{if $account_level == 3}
				<p class="navbar-btn pull-right">
					<a href="/" title="Back to AdultSearch.com" class="btn btn-default btn-sm" style="margin: 3px 0px 0px 20px;">Back to AS</a>
				</p>
			{/if}
			<ul class="nav navbar-nav" style="font-size: 1.3em;">
			{if permission::has("advertise_manage")}
				<li><a href="/advertise/dashboard" style="color: #ccc;"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
				<li><a href="/advertise/advertisers" style="color: #ccc;"><span class="glyphicon glyphicon-user"></span> Advertisers</a></li>
				<li><a href="/advertise/wires" style="color: #ccc;"><span class="glyphicon glyphicon-usd"></span> Wires</a></li>
				<li><a href="/advertise/deals" style="color: #ccc;"><span class="glyphicon glyphicon-piggy-bank"></span> Flat deals</a></li>
			{/if}
			{if permission::has("advertise_manage") || $smarty.session.advertise_publisher}
				<li><a href="/advertise/zones" style="color: #ccc;"><span class="glyphicon glyphicon-th-large"></span> Zones</a></li>
			{/if}
			{if permission::has("advertise_manage")}
				<li><a href="/advertise/stats" style="color: #ccc;"><span class="glyphicon glyphicon-stats"></span> Stats</a></li>
			{/if}
			{if $smarty.session.account == 3974}
				<li><a href="/advertise/admin" style="color: #ccc;"><span class="glyphicon glyphicon-wrench"></span> Admin</a></li>
			{/if}
			</ul>

				{if !$ismember}
					<ul class="nav navbar-nav navbar-right">
						<li><a href="/account/signup" rel="nofollow">Sign Up</a></li>
						<li><a href="/account/signin?origin={$request_uri}" rel="nofollow">Member Login</a></li>
						<li><a href="/" title="Back to AdultSearch.com">Back to AdultSearch.com</a></li>
					</ul>
				{else}
					<ul class="nav navbar-nav navbar-right">
						<p class="navbar-text">{$smarty.session.username}</p>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onclick="return false;">My Account <span class="caret"></span></a>
							<ul class="dropdown-menu">
								{if $smarty.session.worker||$account_level==3}
									<li><a href="http://{$site_domain}/mng/home">Manage</a></li>
								{/if}
								{if account::is_impersonated() }
									<li><a href="/account/impersonate_return">Return to admin</a></li>
								{/if}
								<li><a href="http://{$site_domain}/?logoff=1&redirect={$request_uri}">Logout</a></li>
							</ul>
						</li>
						<li>
					</ul>
				{/if}
			</div><!--/.nav-collapse -->
		</div>
	</nav>

	<!-- Begin page content -->
	<div {if $account_level == 3}class="container-fluid"{else}class="container"{/if}>
		{if $flash_msg}
			<br style="clear: both;" />
			{$flash_msg}
		{/if}

   		{$content}
	</div>
	<div id="filler" {if $account_level == 3}class="container-fluid"{else}class="container"{/if} style="display:block;	position:absolute;	height:100%;   bottom:0;	top:0;	left:0;	right:0;	z-index: -9999;">
	</div>

	<!-- footer -->
	<footer class="footer">
	  <div class="container">
	
		<p>
			<a href="http://{$site_domain}/advertise/" class="o" title="Advertising">Advertising</a> |
			<a title="Privacy Policy" href="/privacy-policy">Privacy Policy</a> |
			<a href="/contact?where={$request_uri}">Contact Us</a> |
			<a title="adult search - business owners" href="/businessowner/">Business Owners</a> |
			<a title="Our Banners" href="/our-banners">Grab Our Banner</a> |
			<a rel="nofollow" target="_blank" title="Protect Children Online" href="http://www.asacp.org/">RCA</a>
			{if $totaltime}
				<br/>
				<b>{$totaltime}</b>. total sql: <b>{$total_sql}</b>. sql time: <b>{$total_time}</b>
			{/if}
		</p>
	  </div>
	</footer>

{$html_bottom_include}
</body>
</html>

