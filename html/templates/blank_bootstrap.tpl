<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>{$title}</title>

	<link rel="icon" type="image/png" href="/favicon.ico">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="/js/ui/jquery-ui-1.9.1.custom.min.js"></script>
	<link href="{$config_site_url}/js/ui/start/jquery-ui-1.9.1.custom.min.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

	{$html_head_include}

	{if !$config_dev_server}
	{literal}
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117338291-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-117338291-1');
	</script>
	{/literal}
	{/if}

</head>

<body>

<div class="container">
	{if $flash_msg}
		<br style="clear: both;" />
		{$flash_msg}
	{/if}

	{$content}
</div>


{literal}
<script type="text/javascript">
</script>
{/literal}

</body>
</html>

