<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$title}</title>
<link rel="icon" type="image/png" href="/favicon.ico">
<meta name="description" content="{$description}" />
{if $devserver}<meta name="robots" content="nofollow, noindex" />{/if}
{if $keywords}<meta name="keywords" content="{$keywords}" />{/if}
{if $canonical_url}<link rel="canonical" href="{$canonical_url}"/>{/if}
<link href="/css/as16.css?" rel="stylesheet" type="text/css" />
{if $canonical}<link rel="canonical" href="{$canonical}" />{/if}
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/tools/fancybox/jquery.fancybox-1.3.1.pack.js"></script>
{if $account_level>2}<script type="text/javascript" src="/js/_mng.js"></script>{/if}
<link href="/css/wrapper.css?20160715" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/main.js"></script>
{$html_head_include}
{if !$devserver && $smarty.server.HTTP_HOST == "adultsearch.com"}
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-5124307-2']);
  _gaq.push(['_setDomainName', '.adultsearch.com']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
{/if}
<meta name="viewport" content="width=device-width">
</head>
<body style="background: none;">
	<div>
	   {$content}
	</div>
</body>
</html>

