<form method="post" action="" name="f">
{$ccform_hidden}

<div id="cl">

<p>You will be charged : <b>${$total}</b></p>

<div class="payment">
<table border="0" cellspacing="0" cellpadding="0" class="b">

{if $error}
<tr>
 <td colspan="2" class="error" align="center">
	{$error}
</td>
</tr>
{/if}

 <tr>
  <td align="center" valign="middle" colspan="2"><font style="font-size:18px">Payment Details</font>
<script type="text/javascript" 
src="https://seal.verisign.com/getseal?host_name=adultsearch.com&amp;size=S&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=en"></script><br />
  </td>
 </tr>


 <tr>
  <td class="first">First Name:</td>
  <td class="second">
	<input type="text" name="cc_firstname" size="40" value="{$cc_firstname}" maxlength="60" />
  </td>
 </tr>

 <tr>
  <td class="first">Last Name:</td>
  <td class="second">
	<input type="text" name="cc_lastname" size="40" value="{$cc_lastname}" />
  </td>
 </tr>

 <tr>
  <td class="first">Address:</td>
  <td class="second">
	<input type="text" name="cc_address" size="40" value="{$cc_address}" />
  </td>
 </tr>

 <tr>
  <td class="first">City:</td>
  <td class="second">
	<input type="text" name="cc_city" size="40" value="{$cc_city}" />
  </td>
 </tr>

 <tr>
  <td class="first">State/Province/Region:</td>
  <td class="second">
	<input type="text" name="cc_state" size="20" value="{$cc_state}" />
  </td>
 </tr>

 <tr>
  <td class="first">ZIP/Postal Code:</td>
  <td class="second">
	<input type="text" name="cc_zipcode" size="10" value="{$cc_zipcode}" />
  </td>
 </tr>

   <input type="hidden" name="captcha_str" value="{$captcha_str}" />

 <tr>
  <td colspan="2" class="submit">
	<span id="submitbutton">
		<span class="bsubmit"><button class="bsubmit-r" type="submit" ><span>Submit</span></button></span>
	</span>
  </td>
</tr>

</table>
</div>

</div>

</form>
