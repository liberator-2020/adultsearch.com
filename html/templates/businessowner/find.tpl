<div id="blue">
 <div class="header">
  <span class="title">Lets See If we already have your business listed</span>
 </div>

<div class="content">

<table border="0" cellspacing="5" cellpadding="5" width="100%">
 <tr>
  <td>

<table width="100%" cellspacing="0" cellpadding="4" class="edit">
 <tr>
   <td>
	<span class="edittitle">Search for your business!</span>

{if $error}<div class="error">{$error}</div>{/if}

<table class="b" width="600">
 <tr>
  <td>What is your businesses main phone number ?</td>
  <td class="second"><form method="post">
	<input id="phone1id" size="3" maxlength="3" type="text" name="phone1" value="{$phone1}" />
	<input id="phone2id" size="3" maxlength="3" type="text" name="phone2" value="{$phone2}" />
	<input id="phone3id" size="4" maxlength="4" type="text" name="phone3" value="{$phone3}" />
	<span class="bsubmit"><button class="bsubmit-r" type="submit" value="search"/><span>Search</span></button></span>
	</form>
  </td>
 </tr>

{if $place}
 <tr>
  <td colspan="2">
	<p>We found <b>{$place.name} {$place.address} {$place.loc_name}, {$place.s} {$place.zipcode}</b>. If you want to advertise this place, <a href="/businessowner/pick?id={$place.place_id}&from=find1">click here now.</a>
	</p>
  </td>
 </tr>
{elseif $notfound}
 <tr>
  <td colspan="2"><p>We do not have any place using {$phone1}-{$phone2}-{$phone3} phone number. Please <a href="/place/submit?ref=%2Fbusinessowner%2Ffind">click here</a> to add your business into our system.</p>
  </td>
 </tr>
{/if}

 <tr>
  <td colspan="2" style="height:40px"> <em> - OR - </em> </td>
 </tr>

 <tr>
  <td colspan="2">Pick your businesses location;</td>
 </tr>

 <tr>
  <td colspan="2">
<select onchange="$('#findBState').html(''); $('#findBCity').html('');
_ajax('get','/_ajax/advertise', 'parent_name='+this.options[this.selectedIndex].value+'&amp;select=State&amp;onChangeCity=test&city_select=findBCity', 'findBState');" name="findBCountry">
<option value="">-- country --</option>
<option value="16047">Canada</option>
<option value="16046">United States</option>
</select>
<span id="findBState"></span>
<span id="findBCity"></span>
  </td>
 </tr>

{if $places}
 <tr>
  <td colspan="2">
	<ul>
	{foreach $places as $place}
	<li style="padding:5px;list-style-type: circle;">
		<a href="/businessowner/pick?id={$place.place_id}&from=find2">{$place.name}, {$place.address} {$place.loc_name}, {$place.s} {$place.zipcode}</a>
	</li>
	{/foreach}
	</ul>

	<p>If your business is not listed here, <a href="/place/submit?ref=%2Fbusinessowner%2Ffind">click here</a> to add it to our database.</p>
  </td>
 </tr>
{/if}

</table>

   </td>
 </tr>
</table>
 </td>
 </tr> 
 </table>
</div>
</div>

<script type="text/javascript">
<!--
_ajax('get','/_ajax/advertise', 'parent_name={$Country}&select_name=State&State={$State}&onChangeCity=test&city_select=findBCity', 'findBState');
{if $State}
_ajax('get','/_ajax/advertise', 'parent_name={$State}&select_name=City&City={$City}&onChangeCity=test', 'findBCity');
{/if}

{literal}
function test() {
	var new_loc = '/businessowner/find?City=' + $('#City').val();
	window.location = new_loc;
}

$(document).ready(function() {
	$('#phone1id').autotab({ target: 'phone2id', format: 'numeric' });
	$('#phone2id').autotab({ target: 'phone3id', format: 'numeric', previous: 'phone1id' });
	$('#phone3id').autotab({ previous: 'phone2id', format: 'numeric' });
});
{/literal}
-->
</script>
