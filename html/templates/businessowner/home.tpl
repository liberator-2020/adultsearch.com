<script type="text/javascript" src="/js/flowplayer.js"></script>
<script type="text/javascript" src="/js/flowplayer.ipad.js"></script>

<div id="blue">
 <div class="header">
  <span class="title">Adult Business Owners</span>
 </div>

<div class="content">

<table border="0" cellspacing="5" cellpadding="5" width="100%">
 <tr>
  <td align="center">
	<div style="width: 640px; height: 360px; position: relative;">
	<a href="{$config_site_url}/images/videos/adultsearch_business_owner.mp4" id="flow_vid_city" title="Adultsearch - Business owners">
		<img src="{$config_site_url}/images/videos/adultsearch_business_owner.png" class="vid_thumb_img">
		<img src="/images/city_play_50.png" style="display: block; top: 160px; left: 290px; position: absolute; z-index: 100;">
	</a>
	{literal}
	<script type="text/javascript" language="JavaScript">
flowplayer(
	"flow_vid_city", 
	"/js/flowplayer.swf", 
	{
		'key': '#$9790c83cc945fb375bb', 
		clip: {
			autoPlay: true, 
			autoBuffering: true, 
			plugins: {
				controls: {
					url: 'flowplayer.controls.swf', 
					time: false, 
					stop: true, 
					bufferGradient: 'none', 
					callType: 'default',
					tooltipTextColor: '#D00000',
					sliderGradient: 'none',
					buttonOverColor: '#5eabe5',
					autoHide: 'never',
					progressColor: '#112233',
					buttonColor: '#FFFFFF',
					timeBgColor: '#262626',
					backgroundColor: '#222222',
					timeColor: '#B1E0FC',
					timeBorder: '1px solid rgba(0, 0, 0, 0.3)',
					bufferColor: '#445566',
					disabledWidgetColor: '#555555',
					progressGradient: 'none',
					durationColor: '#ffffff',
					volumeSliderColor: '#D00000',
					volumeSliderGradient: 'none',
					tooltipColor: '#C9C9C9',
					sliderColor: '#C9C9C9',
					backgroundGradient: 'high',
					borderRadius: '10',
					buttonOffColor: 'rgba(90,90,90,1)',
					height: 20,
					opacity: 1.0
				}
			}
		}
	}).ipad();
		</script>
		{/literal}
	</div>
	<br />
	<a href="/businessowner/pick?section={$section}&id={$id}&from=home" rel="nofollow">
		<img src="{$config_site_url}/images/businessowner/get-started.png" alt="" width="254" height="35" border="0" />
	</a>
	</td>
 </tr>
 
 </table>

</div>
</div>

