<div id="blue">
	<div class="header">
	<span class="title">Choose an Owner Package to Process</span>
	</div>

<div class="content">

{if $error}
	<div style="color: red; font-weight: bold; margin: 20px; padding: 20px;">
		There has been error with your payment. Please repeat the purchase process and pay attention to your credit card and billing details.
	</div>
{/if}

<table border="0" cellspacing="5" cellpadding="5" width="100%">
<tr>
<td>

<table width="100%" cellspacing="0" cellpadding="4" class="edit">
<tr>
<td valign="top">

<table>
<tr>
	<td colspan="2">
	<b>{$place.name} {$place.street} {$place.loc_name} {$place.zipcode}</b>
	{if $place.thumb}<p><img src="http://img.adultsearch.com/place/t/{$place.thumb}" alt="" /></p>{/if}
	</td>
</tr>
</table>
</td>

 <td valign="top">
	{if $pick}<div class="error">Please pick the business owner package that you want to purchase</div>{/if}
	<span class="edittitle">Choose Advertising Package</span>

	{if $error}<div class="error">{$error}</div>{/if}

<form method="post" data-ajax="false">

<input type="hidden" name="pay" value="1" />
<table class="b" width="600">
<tr>
	<td class="second">
	<input type="radio" name="p" value="1" /> Premium owner package. {$option_premium}
	</td>
</tr>
<tr>
	<td class="second">
	<input type="radio" name="p" value="3" /> Premium owner package. {$option_premium_year}
	</td>
</tr>
<tr>
	<td class="second">
	<input type="radio" name="p" value="2" /> Regular owner package. {$option_normal}
	</td>
</tr>
<tr>
	<td class="second" align="center">
	<span class="bsubmit"><button type="submit" class="bsubmit-r"><span>Go to the payment page</span></button></span>
	</td>
</tr>
</table>

</form>

</td>
</tr>
</table>


</td>
</tr>
</table>

</div>
</div>
