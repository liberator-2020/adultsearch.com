<!--[if IE]><link rel="stylesheet" href="/css/ie.css" type="text/css" media="screen, projection" /><![endif]-->

<div id="contentCity">

{*
{if !$video && $account_level>1}<div style="position: absolute; z-index: 100; top: 105px; "><a href="{$city_video_upload_link}" id="videoUpload">Upload A Video</a></div>{/if}
*}

{if $video}
<div id="cityVideo" class="box clearVideo">
	<a href="{$city_video_upload_link}" id="videoUpload">Upload A Video</a>
	<div class="title">
		<h2>{if $video_pl}{$video_count} Exclusive videos{else}Exclusive video{/if}</h2>
	</div>
	<div class="main_video">
		{if $video.place_link}<a id="city_video_link" href="{$video.place_link}"><h3 id="city_video_name">{$video.name}</h3></a>{else}<h3 id="city_video_name">{$video.name}</h3>{/if}
		{if $account_level>2}<div style="position: absolute; z-index: 110;"><a href="/ajax/files/place/{$loc_id}/edit/{$video.file_id}" style="color: yellow; background-color: green; padding: 1px;">* edit</a></div>{/if}
		<div class="flowplayer"><video poster="{$video.thumb}" data-index="0"><source type="video/mp4" src="{$video.url}"></video></div>
	</div>
{if $video_pl}
<div class="playlist">
<a class="prev browse left"></a>
<div id="scrollable" class="scrollable">
		<div class="items">
		{section name=vp loop=$video_pl}
		{assign var=video_pl_group value=$video_pl[vp]}
				<div>
			{section name=vpi loop=$video_pl_group}
			{assign var=video_pl_item value=$video_pl_group[vpi]}
						<a href="{$video_pl_item.url}" title="{$video_pl_item.name}" data-video-id="{$video_pl_item.file_id}" data-video-link="{$video_pl_item.place_link}" data-video-name="{$video_pl_item.name}" onclick="return false;"><img src="{$video_pl_item.thumb}" alt=""/></a>
			{/section}
				</div>
		{/section}
		</div>
</div>
<a class="next browse right"></a>
</div>

<script type="text/javascript" language="JavaScript">
$(document).ready(function() {
	$("div#scrollable div.items a").click(function() {
		var vid_id = $(this).attr("data-video-id");
		var vid_name = $(this).attr("data-video-name");
		var place_url = $(this).attr("data-video-link");
		var vid_url = $(this).attr("href");
		$("h3#city_video_name").html(vid_name);
		$("a#flow_vid_city").attr("title", vid_name);
		$('a#flow_vid_city').attr("data-video-file-id", vid_id);
		$('a#flow_vid_city').attr("href", vid_url);
		$('a#city_video_link').attr("href", place_url);
	});
});
$(function () {
	var playlist = [],
		videos = $(".scrollable a"),
		scrollable,
		player,
		i;
 
	$('.scrollable a').each(function(i,obj) {
		var video_url = $(obj).attr('href');
		playlist.push({
			sources: [
				{ type: "video/mp4",	src: video_url },
			]
		});
 	});

	$(".scrollable").scrollable({
		circular: true
	});
 
	scrollable = $(".scrollable").data("scrollable");
 
	player = flowplayer($("div.flowplayer"), {
		// loop the playlist in a circular scrollable
		loop: true,
		ratio: 9/16,
		splash: true,
		bgcolor: "#333333",
		customPlaylist: true,
		playlist: playlist
 
	}).on("ready", function (e, api, video) {
		videos.each(function (i) {
			var active = i == video.index;
			$(this).toggleClass("is-active", active)
				 .toggleClass("is-paused", active && api.paused);
		});
 
	}).on("pause resume", function (e, api) {
		videos.eq(api.video.index).toggleClass("is-paused", e.type == "pause");
 
	}).on("finish", function (e, api) {
		var vindex = api.video.index,
			currentpage = Math.floor(vindex / 2),
			scrollindex = scrollable.getIndex(),
			next;
		// advance scrollable every 2nd playlist item
		if (vindex % 2 != 0 && scrollindex == currentpage) {
			// prefer circular movement when current item is visible
			scrollable.next();
		} else if (scrollindex != currentpage) {
			// scroll to next item if not visible
			next = vindex < playlist.length - 1 ? vindex + 1 : 0;
			scrollable.seekTo(Math.floor(next / 2));
		}
	});

	player.setPlaylist(playlist);
 
	videos.click(function () {
		var vindex = videos.index(this);
 		if (player.video.index === vindex) {
			player.toggle();
		} else {
			player.play(vindex);
		}
	});
});
</script>
{/if}
</div>
{elseif $youtube_video}
<div id="cityVideo" class="clearVideo">
	<div class="title"><h1>Exclusive AdultSearch Video - {$loc_name}</h1></div>
	<div class="main_video">
	{$youtube_video}
	</div>
</div>
{/if}

{if $country != "cambodia" && $country != "southafrica"}
	{include "city_eroticservices.tpl" eroticservices=$eroticservices loc_name=$loc_name}
{/if}

{section name=i loop=$categories}
	{include "city_intl_unit.tpl" category=$categories[i] loc_name=$loc_name}
{/section}

{if $forum.forums}
	{include "city_forums.tpl" forum=$forum loc_name=$loc_name}
{/if}

<div style="clear:both"></div>

{if $location_description || $location_images || $location_video_html}
	<div id="location_description">
		<!-- {$location_lat} - {$location_lng} -->
		<h3>{$location_label}</h3>
		{if $location_lat && $location_lng}
			<a href="https://www.google.com/maps?ll={$location_lat},{$location_lng}&z=12" target="_blank">
				<img src="//maps.google.com/maps/api/staticmap?center={$location_lat},{$location_lng}&amp;zoom=10&amp;size=200x200&amp;maptype=roadmap&amp;markers=color:red|label:A|{$location_lat},{$location_lng}&amp;sensor=false" alt="" style="float: right; margin-right: 10px; margin-left: 10px;"/>
			</a>
		{/if}
		{$location_description}
		{foreach from=$location_images item=img}
			<img src="{$config_image_server}/location/{$img}" style="float: left; margin: 10px;" />
		{/foreach}
		<br style="clear: both;" />
		{$location_video_html}
	</div>
{/if}

</div>
