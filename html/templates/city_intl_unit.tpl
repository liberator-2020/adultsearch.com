{* $category array passed from parent template city_intl.tpl *}
<div class="box col1 height contentStyle {$category.addl_classes}">
	<div class="title"><h2>{$loc_name} {$category.name}<super>({$category.count})</super></h2></div>
	{section name=sc loop=$category.subcats}
	{assign var=sc value=$category.subcats[sc]}
		<h3><a href="{$sc.url}" title="{$sc.title}">{$loc_name} {$sc.name}<super>({$sc.count})</super></a></h3>
	{sectionelse}
		<h3><a href="{$category.url}" title="{$category.title}">{$loc_name} {$category.name}<super>({$category.count})</super></a></h3>
	{/section}
{if $category.populars}
	<div class="recent"></div>
	<div id="reviewsThumb">
		{section name=pop loop=$category.populars}
		{assign var=popular value=$category.populars[pop]}
		<a href="{$popular.url}" title="">
			<img src="{$popular.thumb}" alt="" border="0" />
			<div class="name">{$popular.name|truncate:20}</div>
			{if $popular.date}<li>[on {$popular.date|date_format:'%m/%d/%Y'}]</li>{/if}
		</a>
          	{/section}
	</div>
	<div class="submitCTA"><a href="{$category.url}" title="{$category.title}"></a></div>
{/if}
</div>

