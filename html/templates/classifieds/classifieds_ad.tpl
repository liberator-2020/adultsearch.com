<link href="/css/services.css?20180114" rel="stylesheet" type="text/css">
<a name="anchorLand"></a>

{if $marquee}
<div class="precontent">
<table  cellpadding="0" cellspacing="0"><tr><td>{if $marquee|@count>12} <input type="button" onclick="pause('#marquee');" value="Pause"/> <input type="button" onclick="resume('#marquee');" value="Resume"/>{/if} <a href="{$cat_link}" title="{$cat_name}">Back To All 
{$cat_name}</a></td></tr>
<tr><td>
<ul id="marquee" class="marquee"> 
  <li>
	<table><tr>
	{section name=m loop=$marquee}{assign var=m value=$marquee[m]}
		<td><a href="{$m.link}" title="{$m.title}"><img src="{$m.thumb}" alt="" width="75" border="0" /></a></td>
	{/section}
	</tr></table>
  </li> 
</ul>
</td></tr></table>
</div>
{/if}

{if $adpreview}
<a onclick="$('#preview').dialog('close'); return false;" href="#"><img src="/images/adbuild/btn_edit.png" alt="" border="0" /></a>
{if $post.done < 1}<a href="/adbuild/step4?ad_id={$post.id}"><img src="/images/adbuild/btn_submitcontinue.png" alt="" /></a>{/if}
{/if}

<!-- end pre-content -->

<div id="ad">

<!-- buttons - admin -->   
<span class="linkbutton">
	{if $isadmin || $isworker}
		{if $bp}
			<span class="bpbutton">BP</span>
		{/if}
		{if $paid}
			<span class="paidbutton">$</span>
		{/if}
	{/if}
	{if $adexpired && ($isadmin || $isworker)}
		<span style="color: red; float: left; display: inline-block;">Ad is expired!</span>	
	{elseif $adexpiredbp}
		<a class="paylink" rel="nofollow" href="/classifieds/renew?id={$post.id}">Renew This Expired Ad</a>
	{/if}

	{if $canedit}
		<a class="editlink" href="/adbuild/step3?ad_id={$post.id}">Edit Ad</a>
	{/if}
	{if $isadmin}
		<a class="editlink" href="/mng/classifieds?action=manage&action_id={$post.id}">Manage Ad</a>
		<a class="editlink" href="/mng/classifieds?action=delete&action_id={$post.id}">Delete Ad or Ban user ...</a>
		<a class="editlink" href="/classifieds/sponsor?id={$post.id}">Make This Ad A Homepage Thumbnail</a>
		{if $isrealadmin}
			<a class="editlink" href="/mng/classifieds?cid={$post.id}">Mng</a>
			{if $type == 2}
				<a class="editlink" href="http://tsescorts.com/admin/import_as.php?clad_id={$post.id}&submit=submit">Import to TS</a>
			{/if}
		{/if}
	{/if}

	{if $expired}
		<a class="paylink" href="https://adultsearch.com/adbuild/step3?ad_id={$post.id}">Pay to Publish Ad</a>
	{/if}

	{if $clad_ter_check}
		<a class="paylink" href="/mng/clad_ter_check?cid={$post.id}">TER Check</a>
	{/if}
</span>

<!-- heading -->
  {if $images}
  <div id="foto"><a href="#uploads"> {$images|@count} Photos</a></div>
  {/if}
  <h1 class="name">
	  {$post.firstname}
	  {if $post.phone} -
		  <a href="tel:{if $post.phoneInternational}{$post.phoneInternational}{else}{$post.phone}{/if}">{$post.phone}</a>
	  {/if}
  </h1>
	{*
	{if !$adpreview}<div class="posteddate"> Posted: {$post.date2} PST</div>{/if}
	*}
  <div class="summary">
{*
	{if $post.visiting} Visiting, {/if}
*}
	{if $post.pregnant} Pregnant, {/if}
	{if $post.pornstar} Porn Star, {/if}
{$cat_name}{if $post.gfe_limited} &amp; Limited GFE{/if}{if $post.gfe} &amp; GFE{/if} {if $post.available}available for {if $post.available==1}Incall{elseif $post.available==2}Outcall{else}Incall and Outcall{/if}{/if}.</div>
  <div class="tagline">{$post.title}</div>
  
	<div class="linkbutton">
	{if $post.reply==1} <a href="/classifieds/reply?id={$post.id}" rel="nofollow" title="" target="_blank">email this provider</a>{/if}
	{if !$adpreview}<a href="#" title="" onclick="$('#linktothis').toggle('slow'); return false;">copy link</a>
	<a href="/contact?where={php}echo rawurlencode($_SERVER["REQUEST_URI"]);{/php}" rel="nofollow">Report An Error</a>
	<span class="place_fb"><fb:like layout="button_count" show_faces="true" width="450" action="recommend" font="arial"></fb:like></span>{/if}
	</div>
	
	
   <div id="linktothis" style="display:none"><input type="text" value="http://{$smarty.server.HTTP_HOST}{$state_link}{$gModule}/{$post.id}" onclick="this.select();" size="100" readonly="readonly" /><br /><br /></div>

<!-- ad content -->

<div id="stats">
<!-- contact -->
	  <div class="title">Contact</div>
		{*
		{if $post.vt && $post.visiting}
			<div><label>Visiting</label>{if $post.vf!="00/00/0000"}{$post.vf} - {$post.vt}{else}Yes{/if}</div>
		{/if}
		*}

	{if $post.phone}
		<div>
			<label>Phone</label>
			<a href="tel:{if $post.phoneInternational}{$post.phoneInternational}{else}{$post.phone}{/if}">{$post.phone}</a>
		</div>
	{/if}
	{if $post.reply==1}
		<div>
			<label>Email</label>
			<a href="/classifieds/reply?id={$post.id}" rel="nofollow" title="">Send A Reply</a>
		</div>
	{/if}
	
	{if $post.reply==3}
		<div>
			<label>Email</label>
			<a href="mailto:{$post.email}">{$post.email}</a>
		</div>
	{/if}

	  {if $post.website}
	  <div>
		<label>Website</label>
	   <a href="{$post.website}" target="_blank" rel="nowfollow">{$post.website}</a></div>
	   {/if}
	  
	{* 
	 {if $post.ter}
	 <div>
		<label>TER</label>
	 <a href="http://www.theeroticreview.com/reviews/show.asp?id={$post.ter}" target="_blank" rel="nofollow">Read My Reviews on TER</a>
	 </div>
	 {/if}
	*}

	 {if $post.ECCASapi_revsearch_url}
	 <div>
		<label>Eccie Review</label>
	 <a href="{$post.ECCASapi_revsearch_url}" target="_blank" rel="nofollow">Read My Reviews on Eccie</a>
	 </div>
	 {/if}

	 {if 0&&$post.bigdoggie}
	 <div>
	 <label>BigDoggie</label>
	 <a href="http://www.bigdoggie.net/escorts/by-id/{$post.bigdoggie}.html" target="_blank" rel="nofollow">Read My Reviews on Bigdoggie.net</a>
	 </div>
	 {/if}

{*
	 {if $post.eccie}
	 <div>
	 <label>Eccie</label>
	 <a href="/ad?goo=1&h=eccie.net&l={$post.eccie}" target="_blank" rel="nofollow">Eccie.net discussion about this ad</a>
	 </div>
	 {/if}
*}	   
	   
	{if $post.facebookHandle}
		<div><label>&nbsp;</label><img src="/images/icons/facebook_75.png" style="width: 20px; height: 20px; vertical-align: middle;" /> <a href="{$post.facebookUrl}" target="_blank" rel="nofollow">{$post.facebookHandle}</a></div>
	{/if}
	{if $post.twitterHandle}
		<div><label>&nbsp;</label><img src="/images/icons/twitter_75.png" style="width: 20px; height: 20px; vertical-align: middle;" /> <a href="{$post.twitterUrl}" target="_blank" rel="nofollow">{$post.twitterHandle}</a></div>
	{/if}
	{if $post.google}
		<div><label>&nbsp;</label><img src="/images/icons/google_75.png" style="width: 20px; height: 20px; vertical-align: middle;" /> <a href="{$post.google}" target="_blank" rel="nofollow">{$post.google}</a></div>
	{/if}
	{if $post.instagramHandle}
		<div><label>&nbsp;</label><img src="/images/icons/instagram_75.png" style="width: 20px; height: 20px; vertical-align: middle;" /> <a href="{$post.instagramUrl}" target="_blank" rel="nofollow">{$post.instagramHandle}</a></div>
	{/if}

	<div> Mention you saw this ad on AdultSearch.com </div>

<!-- stats -->

	  <h2 class="title">About {$post.firstname}</h2>
	  
  {*	
	  {if $post.loc_name}
	  <div>
		<label>Location</label>
	   {$post.loc_name}{if $post.location} ({$post.location}){/if} 
		</div>
	  {/if}
*}
	  {if $post.location}
	  <div>
		<label>Location</label>
	   {$post.location} 
		</div>
	  {/if}
	  
	{if $post.age}
	  <div>
		<label>Stats</label>
		{if $post.age}{$post.age} years old{/if}{if $post.ethnicity}, {$post.ethnicity}{/if}{if $post.height}, {$post.height}{/if}{if $post.weight}, {$post.weight}{/if}{if $post.eyecolor}, {$post.eyecolor} Eyes{/if}{if $post.haircolor}, {$post.haircolor} Hair{/if}{if $post.build},  {$post.build}{/if}{if $post.measurements}, {$post.measurements}{/if}{if $post.cupsize}, {$post.cupsize} Cup{/if}{if $post.penis_size}, {$post.penis_size} (inches){/if}{if $post.kitty}, {$post.kitty} Kitty{/if}
	   </div>
	{/if}
	  
	{if $post.language}
	<div>
		<label>Languages</label>
	   {$post.language}
	</div>
	{/if}

	   {if $post.avl_men||$post.avl_women||$post.avl_couple}
	 <div>
		<label>Available To</label>
		{if $post.avl_men}Men{if $post.avl_women||$post.avl_couple}, {/if}{/if} {if $post.avl_women}Women{if $post.avl_couple}, {/if}{/if} {if 
$post.avl_couple}Couples{/if}
	 </div>
	 {/if}
		
	 
	 {if $post.tantra}
	   <div>
		<label>Massage</label>
	  Tantra Massage
	 </div>
	   {/if}

		
	   {if $post.fetish}
	 <div>
		<label>Fetish Session</label>
		{$post.fetish} 
	 </div>
	 {/if}

	   {if $post.bdsm}
	 <div>
		<label>BDSM Activities</label>
		{$post.bdsm} 
	 </div>
	 {/if}

	<div>
		<label>Incall</label>
	   {if $post.incall}
			{if $post.incall_rate}
				{if $post.incall_rate_hh}{$post.incall_rate_hh}/half hour, {/if}
				{$post.incall_rate}/hour
				{if $post.incall_rate_2h}, {$post.incall_rate_2h}/2 hours{/if}
				{if $post.incall_rate_day}, {$post.incall_rate_day}/overnight{/if}
			{else}Yes
			{/if}
		{else}No
		{/if}
	 </div>

	<div>
		<label>Outcall</label>
	   {if $post.outcall}
			{if $post.outcall_rate}
				{if $post.outcall_rate_hh}{$post.outcall_rate_hh}/half hour, {/if}
				{$post.outcall_rate}/hour
				{if $post.outcall_rate_2h}, {$post.outcall_rate_2h}/2 hours{/if}
				{if $post.outcall_rate_day}, {$post.outcall_rate_day}/overnight{/if}
			{else}Yes
			{/if}
		{elseif $post.incall}No
		{else}Yes
		{/if}
	 </div>

	{if $currency_button&&($post.incall_rate||$post.outcall_rate)}<div>{$currency_button}</div>{/if} 
			
	  {if $post.payment_visa or $post.payment_amex or $post.payment_dis}
	  <div id="cc">
		<label>Credit Cards</label>
		{if $post.payment_visa}<img width="46" height="25" alt="Visa" src="{$config_site_url}/images/i_visa.png"><img width="46" height="25" alt="Mastercard" src="{$config_site_url}/images/i_mastercard.png">{/if}
		{if $post.payment_amex}<img width="46" height="25" alt="American Express" src="{$config_site_url}/images/i_amex.png">{/if}
		{if $post.payment_dis}<img width="46" height="25" alt="Discover" src="{$config_site_url}/images/i_discover.png">{/if}
	</div>
	{/if}
</div>

<!-- bio and photos -->
<div id="BioID">
	<div class="title">A Little More...</div>
	{$post.content}

	<div id="gallery" style="margin-top:20px">
	{if $videos}
		<div><a id="video-uploads" name="video-uploads"></a>
		{section name=i loop=$videos}
		<div class="flowplayer is-splash"
			 style="width:{$videos[i].width}px;height:{$videos[i].height}px;background-color:#777; background-image:url('{$videos[i].thumbnail_url}');">
			<video width="{$videos[i].width}" height="{$videos[i].height}" controls><source src="{$videos[i].file_url}" type="video/mp4"/></video>
		</div>
		{/section}
		</div>
	{/if}

	{if $images}
		<a id="uploads" name="uploads"></a>
		{section name=i loop=$images}
			{assign var=i value=$images[i]}
				<img src="{$config_image_server}/classifieds/{$i.filename}" alt="" /><br />
		{/section}
	{/if}
	</div>
	<br /><br />
	<div id="anchorTop"><a href="#anchorLand">top</a></div>
</div>
</div>

<!-- right column -->
<div id="ppc">

{if $place}
	{section name=sec loop=$place}
	{assign var=p value=$place[sec]}
	 	{if $smarty.section.sec.index==0}
			<h2 class='nopm'>Erotic Massage Parlors near you</h2> 
		{elseif $smarty.section.sec.index==3}
			<h2 class='nopm'>Strip Clubs near you</h2>
		{elseif $smarty.section.sec.index==6}
			<h2 class='nopm'>Sex Shops near you</h2>
		{/if}
		<table width="200">
			<tr>
				<td><a href="{$p.link}" title="{$p.loc_name}"><img src="{if $p.thumb}{$p.thumb}{else}/images/placeholder.gif{/if}" alt="" width="90" border="0" /></a></td>
				<td valign="top">
					<span class="clpromo"><a href="{$p.link}" title="{$p.loc_name}">{$p.title}</a></span><br>
					{if $p.review}<font class='error'>({$p.review} review{if $p.review>1}s{/if})</font><br>{/if}
					{$p.loc_name}
				</td>
			</tr>
		</table>
	{/section}
{/if}
		
{if !$adpreview}
	{include file='page_ads.tpl'}
	{include file='page_ads_2.tpl'}
{/if} 

</div>
 



{if $refers}
<br style="clear: both;" />
<table width="100%"><tr><th>Time</th><th>Link({$post.hit})</th></tr>
{section name=s1 loop=$refers}
{assign var=r value=$refers[s1]}
<tr bgcolor="{cycle values="#ffffff,#cccccc"}">
<td style="font-size:11px;">{$r.time}</td><td style="font-size:11px;"><a href="{$r.referer}" title="{$r.referer}"
target="_blank">{$r.referer|truncate:100}</a></td></tr>
{/section}
</table>
{/if}

{if !$previewad}
<script src="//connect.facebook.net/en_US/all.js#xfbml=1"></script>
{/if}

{if $marquee}
{literal}
<script type="text/javascript" src="{/literal}{$config_site_url}{literal}/js/tools/jquery.marquee.js"></script>
<script type="text/javascript"> 
$(document).ready(function (){ $("#marquee").marquee();}); 
function pause(selector){$(selector).marquee('pause');}
function resume(selector){$(selector).marquee('resume');}
</script>
{/literal}
{/if}

{if $videos}
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/flowplayer/6.0.5/flowplayer.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flowplayer/6.0.5/skin/minimalist.css">
{/if}

