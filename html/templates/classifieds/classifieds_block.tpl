<div id="blockcnt">
<h2 class="nopm">Blocked users</h2>
{if $blockmsg}<p class="error">{$blockmsg}</p>{/if}
<p>Block a user name: <input type="text" name="new" id="newblock" value="" /> <input type="button" value="Add" onclick="return newblock();" /></p>
{section name=blk loop=$blocked}{assign var=b value=$blocked[blk]}
<p>User Name: <b>{$b.username}</b> IP Address: <i><b>{$b.ip}</b></i> <a href="#" onclick="return removeblock('{$b.i}');">Remove this block</a></p>
{/section}
</div>

{literal}
<script type="text/javascript">
 function newblock() {
	var m = new $.ajax("?blocked=true&newblock="+$('#newblock').val(), {method: 'get', success:
        	function(t) { $("#blockcnt").html(t); } 
	});
 }
 function removeblock(i) {
	var m = new $.ajax("?blocked=true&removeblock="+i, {method: 'get', success:
        	function(t) { $("#blockcnt").html(t); } 
	});
	return false;
 }
</script>
{/literal}

