<form method="post" action="" name="f">

<div id="cl">

<h1>Please fill the form below to change the login information of the ad ID #{$id}</h1>

<table border="0" cellspacing="0" cellpadding="0" class="b">

{if $error}
<tr>
 <td colspan="2" class="error">
	{$error}
</td>
</tr>
{/if}

 <tr>
  <td class="first">Create a User Name:</td>  
  <td class="second">
	<input type="text" name="username" value="{$username}" size="50" maxlenght="30" id="username" onblur="checkusername();" /><div id="cun"></div>
  </td>
 </tr>

 <tr>
  <td class="first">Type your current E-Mail Address:</td>  
  <td class="second">
	<input type="text" name="email" value="{$email}" size="50" maxlenght="50">
  </td>
 </tr>

 <tr>
  <td class="first">Create a Password:</td>  
  <td class="second">
	<span id="c"><input type="password" name="password" value="{$password}" size="50" maxlenght="50" id="password"></span><br/>
	<input type="checkbox" id="vp" value="1" /> Show the password <span id="sp"></span>
  </td>
 </tr>

 <tr>
  <td colspan="2" class="submit">
	<span class="bsubmit"><button class="bsubmit-r" type="submit" value="Post My Ad"/><span>{if $button_post}{$button_post}{else}Submit{/if}</span></button></span>
  </td>
</tr>

</table>

</div>
</form>

{literal}
<script type="text/javascript">

var oldp, newp;
$(document).ready(function() {

$("#vp").click(function() { 
	if( $(this).is(":checked") ) { 
		var newp = $("#password").clone();
		newp.attr("type", "text");
		$("#c").html(newp);
		newp.attr("id", "password");
	} else {
		var oldp = $("#password").clone();
		oldp.attr("type", "password");
		$("#c").html(oldp);
		oldp.attr("id", "password");
	}
	});
});

function checkusername() {
        _ajax("get", "/account/_ajax", "u="+jQuery("#username").val(), "cun")
}
</script>
{/literal}
