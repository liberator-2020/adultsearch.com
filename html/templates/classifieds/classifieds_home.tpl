<link href="/css/listings.css?20190814" rel="stylesheet" type="text/css">

<div id="container" class="clear">

{if !$sc}
	<div style="padding: 10px 5px; background: #FFF; width: 990px; float: left; border-radius: 6px; margin-bottom: 10px; font-size: 14px;">
		Currently, there is 0 {$cat_name} {$whereas} {$loc_name}.<br /><br />
		You can post your ad <a href="/adbuild/" style="font-size: 14px;" rel="nofollow">here</a>.
	</div>
{else}

  <div id="ListLeft"> {if $filter}
	<div id="refineResults">
	  <h4>You Searched For</h4>
	  {section name=filter loop=$filter}
	  {assign var=f value=$filter[filter]} <span class="action"> <a href="{$f.link}" rel="nofollow">X</a> {$f.name}<br />
	  </span> {/section} </div>
	{/if}
	
	{if $refine}
	<div id="refine">
	{section name=r loop=$refine}
	{assign var=r value=$refine[r]}
	  <h4 id="r1" rel="1">{$r.name}</h4>
	  <ul>
		{if $r.type=='manual'}
			{$r.text}
		{else}
			{section name=a loop=$r.alt}
			{assign var=a value=$r.alt[a]}
			
			{if $a.link}
				<li{$class}><a href="{$a.link}{if $link}{$link}{/if}"{if $a.title} title="{$a.title}"{/if}>{$a.name}</a> ({$a.c})</li>
			{else}
				<li><a href="{if $link}{$link}&amp;{else}?{/if}{$a.type}={$a.id}" rel="nofollow">{$a.name}</a> ({$a.c})</li>
			{/if}
			
			
			{if $smarty.section.a.index > 5 && $smarty.section.a.total>15}
			{assign var=class value=' class="hidden"'}
			{if $smarty.section.a.last}
			<li><a href="#refine" onclick="$('li.hidden').toggle('slow');if( this.text.indexOf('More') != -1 ) this.text = '&lt;&lt; Less Cities';else this.text = 'More Cities &raquo;';return false;">More Option &raquo;</a></li>
			{/if}
			{else}{assign var=class value=''}
			{/if}
			{/section}
		{/if}
	</ul>
	{/section} <br />
	Only advertisers who voluntarily selected specific options will display in your results. 
	</div>

	{*
	{if $forum}
	<div id="forum">
	  <h4>Recent Forum Topics</h4>
	  <ul>
		{section name=f loop=$forum.posts}{assign var=f value=$forum.posts[f]}
		<li><a href="{$f.link}" title="{$f.name}">{$f.name|truncate:25}</a>{if $f.c} ({$f.c}){/if}</li>
		{sectionelse}There are no forum posts yet.
		{/section} <span class="action"><a href="{$forumlink|replace:'/viewforum':'/forum-new-topic'}">Start a new topic</a></span>
	  </ul>
	  <div class="clear"></div>
	</div>
	{/if}
	*}

	{/if} </div>

  <div id="ListMid">
	<div id="results">
		<div class="ResultsTitle">
			<span class="action postTitle"><a href="https://adultsearch.com/adbuild/" rel="nofollow">Escorts: Post</a></span>
			<div class="h1title">{$total} {$h1title}</div>
		</div>
		 <div id="sorting" class="viewRow">
			<select id="ipp" name="ipp">
				<option value="30">30 per page</option>
				<option value="60"{if $smarty.get.ipp==60} selected="selected"{/if}>60 per page</option>
				<option value="90"{if $smarty.get.ipp==90} selected="selected"{/if}>90 per page</option>
			</select>
			<div id="views">
				<div id="view-list"><a href="#"></a></div>
				<div id="view-photo"><a href="#"></a></div>
			</div>
		</div>

		{if $sc}
	  {assign var=day value=""}
	  {section name=sc loop=$sc}
	  {assign var=c value=$sc[sc]}
	  	<!-- INDIVIDUAL LISTING -->
		<li class="Listing">
			<a href="{if $c.direct_link}{$c.direct_link}{else}{$c.link}{/if}" title="{$c.name}"{if $c.direct_link} target="_blank"{/if}> 
				{if $c.sponsor} <span class="sponsor">Sponsor</span>{/if}
				{if $c.thumb} <img src="{$config_image_server}/{$image_path}/{$c.thumb}{if $c.owner}?{$current}{/if}" alt="{$c.name}" border="0" width="75" height="75" /> {else} <img src="/images/placeholder.gif" alt="AdultSearch" border="0" width="75" /> {/if}
				<h2 class="ListingTitle"> {if $c.firstname != ""}{$c.firstname}{else}{$c.name|truncate:110}{/if}</h2>
				<div class="ListingRight"> 
					{if $c.incall_rate}Incall - {$currency_sign} {$c.incall_rate}<br />{/if}
					{if $c.outcall_rate}Outcall - {$currency_sign} {$c.outcall_rate}{/if}
				</div>
				<h3 class="ListingTagline">{if $c.firstname != ""}{$c.name|truncate:110}{/if}</h3>
				{if $c.age}{$c.age} years old, {/if}
				{if $c.ethnicity}{$c.ethnicity}, {/if}
				{if $c.haircolor}{$c.haircolor} Hair, {/if}
				{if $c.eyecolor}{$c.eyecolor} Eyes, {/if}
				{if $c.pregnant}Pregnant, {/if}
				{if $c.pornstar}Pornstar, {/if}
				<!-- {if $c.available}{$c.available}, {/if} -->
				{if $c.visiting}Visiting{/if} <br />
				{if $c.location}{$c.location}{/if}
			</a> 
			{if $account_level>2} <a class="edit" href="/adbuild/step3?ad_id={$c.id}" title="{$c.account_id}">Edit</a> {/if}
	  </li>
	  <!-- /INDIVIDUAL LISTING -->
	  {assign var=day value=$c.day}
	  {/section}
	  {else} <span class="error">{$error}</span> {/if}
	<!-- pagination -->
	<div class="pagelist">
	  <div id="sort"> <a href="{$category_path}/rss?type={$type}&loc_id={$loc_id}" title="{$loc_name} escorts rss field" id="rssico"></a>
		<select id="ipp2" name="ipp">
		  <option value="30">30 per page</option>
		  <option value="60"{if $smarty.get.ipp==60} selected="selected"{/if}>60 per page</option>
		  <option value="90"{if $smarty.get.ipp==90} selected="selected"{/if}>90 per page</option>
		</select>
		</div>
		{$paging}
	  <!-- /pagination -->
	</div>
  </div>

	<div id="list_location_video">
		{*
		{if $category_description}
			{if $account_level > 0 && $cur_loc_id && $type}
				<a class="pm_alert" href="/seo/edit?location_id={$cur_loc_id}&category={$type}">Edit SEO description</a>
			{/if}
			{$category_description}
		{/if}
		*}

		{if $type == 1}
<h1>{$loc_name} Escorts - Female Escorts in {$loc_name}</h1>
<p>
Escorts can often get a bum rap, however a number of the ladies are professional Models, pageant winners and physical fitness lovers from around the USA. Not everyone would think about working with a <a href="">{$loc_name} escort</a> for anything besides an bachelor party or erotic dance night.
</p>
<p>
That is just one scenario, and not even the most lucrative, part of the escort business. It is popular that males with a lady on the arm, can be thought of as better off than males without, and as such, working with an escort to go to a service function is not an unusual practice, as a beautiful lady is most likely to stand out and make discussion more likely between guys.
</p>

<h2>{$loc_name} Independent Escorts</h2>
<p>
There are both independent escorts and {$loc_name} Escort Agencies. 
</p>
<p>
An escort service hires ladies for adult entertainment and as regional tourist guide usually, but there is the periodic opportunity that a guy might require a lady on his arm for a function that he may not wish to go to alone. 
</p>
<p>
To that end, it is essential that he find a reputable firm to connect him with a woman to fill his needs, in whatever non-sexual manner in which may be. 
</p>
<p>
Being assured that when working with an escort one will not be consulted with any legal problem is of utmost value, as is made clear to anybody looking for escorts. 
</p>
<p>
These ladies are of the greatest quality and can be relied on to provide more than adequate companionship or home entertainment for any occasion either public or private.
</p>
<p>
{$loc_name} Female Escorts live an exciting lifestyle, and take pleasure in every minute of it. With guys happy to luxurious every high-end on them, and treat them to a few of the very best celebrations in the city, <a href="{$city_url}">{$loc_name}</a> affiliated or independent escorts offer the best adult entertainment for a fee, and are worth every penny.
</p>
<p>
Beautiful Independent Escorts are easily offered all over Phoenix it holds true, however just through respectable companies will you discover professional, certified women ready to meet your every non-sexual fantasy through live stripteases. 
</p>
<p>
You can schedule one woman or multiple, change your mind about the female you worked with and discover another, as well as discover ladies going to engage in your preferred fetish or fantasy, all with the comfort of knowing that you are protected legally so long as you hire from a <a href="{$loc_parent_url}">{$loc_parent_name}</a> escort firm or independent service provider. 
</p>
<p>
Have a look at the listings we provide and call the provider directly to ask exactly what {$loc_name} services they provide.
</p>
<p>
------------------------
</p>
<p>
Posting your adult service On AdultSearch.com - Are you thinking about publishing your adult business on adult search?
</p>

<h3>What do we imply by adult business?</h3>
<p>
This includes massage services, escort services, strippers, adult shops that sell adult toys, part plans that offer adult toys, and so on. This is exactly what we are talking about when we refer to adult organisations.
</p>
<p>
Now that we are all on the exact same page and understand exactly what it is adults are offering, this is ways to promote it. Also, if it is possible that your company could in the least bit be illegal, do not post it.Adultsearch.com does not tolerate prohibited publishing or advertisements. You must be a legal aged adult to promote or run any kind of advertisement or discount on this site.
</p>

<h3>Should you publish a picture?</h3>

<p>
That is completely as much as you. What and who you photograph might make a big difference in the number of sees your ad gets. Also, some advertisements may not require a photo. Its as much as you, just no apparent pornography is allowed.
</p>
<p>
There are various classifications in the adult classification. You will find them under services then look for sexual. All the sensual advertisements are grouped together. You will need to page through a great deal of interesting titles in order to discover what you are searching for. When you post your ad, You will desire it to stand out. This may be harder than you think because of all of the other advertisements. Make sure to state precisely what you are using. If it is toys, then say that. There are lots of massage ads, So the technique of deleting and reporting every 2 days is important.
</p>
<p>
Getting traffic to your ad should not be a problem as our website is a very popular one. The trick is finding the best ways to get your advertisement to stand out from everyone else. Be sure to have great pictures as that constantly assists. If it is possible to photograph what you are offering you constantly are in a better position. Another technique is to have an excellent title. It has to catch somebody's attention. Discover a method to make you title scream out above the others. Then write extremely fascinating ad copy. This is where you want your ad to be. So try to get the very best advertisement copy possible. It might even deserve your while to pay for someone to compose the advertisement for you. These are just a couple of ideas for making money with an adult business on adultsearch.com
</p>
<p>
You may want to know if all this is legal. Yes it is. You can post any legal business in this classification. It remains in your benefit to make sure that you keep it legal. All of us prefer to have a good time, however there are rules.
</p>
<p>
You can think about many other ways to make your advertisement to stand apart. The more imaginative your are, the better your chances of making money will be.</p>
<p>
<a href="/adbuild">Click here for more info {$loc_name} Escort Advertising</a>
</p>
		{/if}
		{if $location_video_html}
			<div class="video">
				{$location_video_html}
			</div>
		{/if}
	</div>

  </div>

	<div id="ListRight">
		{*
		<div class="forumCrumb">{if $forumlink}<a href="{$forumlink}">{$cat_name} Sex Forums</a>{/if}</div>
		*}

		{include file='page_ads.tpl'}

		{include file='page_side_sponsor.tpl'}

		{if $featuredads}
			<div id="servicesThumb">
				<div class="featTitle">{$loc_name} Escorts</div>
				{section name=cl loop=$featuredads}
				{assign var=p value=$featuredads[cl]}
					<a href="{$p.link}"><img src="{if $p.avatar}{$p.avatar}{else}http://{$core_host}/images/icons/User_1.jpg{/if}" alt="" border="0" />
						<div class="category">{$p.category}</div>
						<li>{$p.info}</li>
					</a>
				{/section}
			</div>
		{/if}

		{include file='page_ads_2.tpl'}

	</div>

{/if}

</div>

<script type='text/javascript'>
{literal}
var view_current = 'list';
$("#ipp, #ipp2").change(function() {
	var qp = {}, qs = location.search.substring(1), re = /([^&=]+)=([^&]*)/g, m;
	while (m = re.exec(qs)) {
		if (decodeURIComponent(m[1]) == 'view')
			continue;
		if (decodeURIComponent(m[1]) == 'page')
			continue;
		qp[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
	}
	if (view_current == 'photo')
		qp['view'] = 'photo';
	qp['ipp'] = $(this).val();
	location.search = $.param(qp);
});
function add_photo_view(link) {
	var url = link.attr('href');
	if (typeof url === "undefined")
		return;
	if (url.indexOf('&view=photo') == -1)
		url = url+'&view=photo';
	link.attr('href', url);
}
function remove_photo_view(link) {
	var url = link.attr('href');
	if (typeof url === "undefined")
		return;
	url = url.replace('&view=photo', '');
	link.attr('href', url);
}
function view_photo() {
	if (view_current == 'photo')
		return;
	$('#results').addClass('gallery');
	$('ul.paging a').each(function() {add_photo_view($(this));});
	$('#refine a').each(function() {add_photo_view($(this));});
	$('#refineResults a').each(function() {add_photo_view($(this));});
	if (!document.getElementById('search_view')) {
		$('#search').append('<input id="search_view" type="hidden" name="view" value="photo" />');
	}
	view_current = 'photo';
}
function view_list() {
	if (view_current == 'list')
		return;
	$('#results').removeClass('gallery');
	$('ul.paging a').each(function() {remove_photo_view($(this));});
	$('#refine a').each(function() {remove_photo_view($(this));});
	$('#refineResults a').each(function() {remove_photo_view($(this));});
	if (document.getElementById('search_view')) {
		$('#search_view').remove();
		$('#search input[name=view]').remove();
	}
	view_current = 'list';
}
$('#view-photo a').click(view_photo);
$('#view-list a').click(view_list);
$(document).ready(function() {
	if (getParameterByName('view') == 'photo') {
		view_photo();
	}
});
{/literal}
</script>

