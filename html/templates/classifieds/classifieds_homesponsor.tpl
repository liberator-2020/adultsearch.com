<form method="post" action="">
<input type="hidden" name="id" value="{$id}"/>

<div class="infoblock upgrade">
	<h2 style="margin-top: 0px;">Be A Cover Star...for ${$home_price_month} a month</h2>
	Be the viewer's first fruit!<br />
	Post your photo on your city's cover for ${$home_price_month} a month.<br />

	{if $error}
		<div class="error_msg">{$error}</div>
	{/if}

	<div id="cover">
		<br /><br />

		<div class="intro">
			<img src="/images/adbuild/sales_city_thumb2.png" alt="" width="294" height="289" hspace="0" border="0" align="right" />
			<strong></b>Post your photo and link <br /> on your city's cover page -</strong><br />

			<blockquote>
			{section name=hl loop=$homesp}{assign var=home value=$homesp[hl]}
				{if $home.not_available}
					<span style="color: red;">City thumbnail in {$home.loc_name} not available<span><br />
				{else}
					<input type="checkbox" value="{$home.loc_id}" rel="{$home.price}" name="locs[]" {if $home.checked}checked="checked"{/if}/> {$home.loc_name} ${$home_price_month}/per month<br />
				{/if}
			{/section}
			</blockquote>

			<br />
			Upload Your Photo:<br />
			{if $sponsorpic}<img src="{$sponsorpic}" /> <a href="/classifieds/sponsor?id={$id}&removepic=true">Remove this picture</a><br/>{/if}
			<input type="file" name="sponsorpic" />
		</div>
	</div>

	<input type="submit" name="submit" value="Submit" class="blue_btn"/>
	<a href="/classifieds/myposts" class="blue_btn">Cancel</a>
</div>

</form>
