{literal}
<style type="text/css">
#total, #sloc {font-weight:bold;}
</style>
{/literal}
<form method="post" action="">
<input type="hidden" name="step" value="1" />
<h2 id="h2">Choose Metro Area(s)</h2>

{if !$ismember}<div id="centrallogin"><p><a href="/account/signin?origin={php}echo urlencode($_SERVER['REQUEST_URI']);{/php}">Login to your account</a></p></div>{/if}

{if $error}<p class="error">ERROR: {$error}</p>{/if}

<div id="cart" style="padding-left:5px;display:inline-block;border:1px solid #000000;background-color:#D5ECFF;height:150px;width:250px">
{if $smarty.session.account_level>1||$type==12}<p>Select: <a href="" onclick="showprice(1); return false;">All</a> || <a href="" onclick="showprice(2); return 
false;">None</a> || 
<a href="" 
onclick="pickbigmarket(); return false;">Big Markets</a></p>{/if}
<p class='error' id='lmterr'>You may post an ad in up to 10 locations. <a href="" onclick="showprice(2); return false;">Cancel Selections</a></p>

<p>Total Location Selected: <span id="sloc">0</span></p>
Total Price: <span id="total">$0.00</span>
<span class="cntbtn"><input type="submit" value="Continue" /></span>
<p>Customer Service;<br />
{*
US: <strong>702-472-7242</strong><br />
Canada: <strong>778-819-8510</strong><br />
UK: <b>44-203-318-9833</b>
*}
<a href="mailto:support@adultsearch.com">support@adultsearch.com</a>
</p>
</div>

<p>Go to: <a href="#US" onclick="$('#US').remove().prependTo('#country'); return false;">United States</a>, <a href="#CA" onclick="$('#CA').remove().prependTo('#country'); 
return false;">Canada</a>, <a href="#UK" 
onclick="$('#UK').remove().prependTo('#country'); return false;">United Kingdom</a></p>

<div id='country'>
{section name=l loop=$list}{assign var=l value=$list[l]}
<a name="{$l.name}"></a>
<div class="loc_section clearfix" id="{$l.name}">
<h1 {* class="homeloctitle active hot" *}>{$l.title}</h5>
<div{if 0&&!$l.active} style='display:none'{/if}>
{section name=us loop=$l.list}
{assign var=tt value=$l.list[us]}
<ul class="states">
{section name=us2 loop=$tt}
{assign var=t value=$tt[us2]}
 <li class="state">
  <a name="{$t.real_name}"></a>
  <div>{$t.real_name}</div>
   <ul class="cities">
        {section name=s3 loop=$t.city}{assign var=c value=$t.city[s3]}
        <li><input type="checkbox" name="loc_id[]" value="{$c.loc_id}" id="loc_id{$c.loc_id}" onclick="calculatetotal();" ref="{$c.price}" {if
$c.checked}checked="checked"{/if} {if $c.bold}class="bold"{/if} > {if $c.bold}<b>{$c.loc_name}</b>{else}{$c.loc_name}{/if} <span>(${$c.price})</span></li>
	 {section name=s4 loop=$c.ne}{assign var=n value=$c.ne[s4]}
	   <li><input type="checkbox" name="loc_id[]" value="{$n.loc_id}" id="loc_id{$n.loc_id}" onclick="calculatetotal();" ref="{$n.price}" {if
$n.checked}checked="checked"{/if} {if $n.bold}class="bold"{/if} > {if $n.bold}<b>{$n.loc_name}</b>{else}{$n.loc_name}{/if} <span>(${$n.price})</span></li>
	 {/section}
       {/section}
   </ul>
 </li>
{/section}
</ul>
{/section}
</div>
</div> 
{/section}
</div>

<div style="clear:both;padding-top:15px" class="cntbtn"><input type="image" src="{$config_site_url}/images/cl/continue-button.png" value="Continue" /></div>
</form>

{if $local}
<div style="padding-bottom:10px;clear:both;">
<a href="" onclick="$('#addnew').show(); return false;">Click here to add a city which is not listed above.</a>
<div style="" id="addnew">You may type the city names to add them individually: <select id="addcity" name="addcity"></select></div>
<div id="extracity"></div>
</div>
{/if}

<script type='text/javascript'>
<!--
 function formatUSD (n) {
    var s = "" + (Math.round(n*100)/100);
    var i = s.indexOf('.');
    if (i < 0) return "$" + s + ".00";
    if (i + 2 == s.length) s += "0";
    return "$" + s;
  }

function showprice(way) {
	if( way == 1 ) jQuery('input[name^="loc_id"]').not(':disabled').attr('checked', true);
	else if( way == 2 ) jQuery('input[name^="loc_id"]').attr('checked', false);
	calculatetotal();
}

function calculatetotal() {
	var x = 0.0; var y, t = 0;
	$("#sloc").html('');
	jQuery('input[name^="loc_id"]:checked').not(':disabled').each(function() { 
		x += parseFloat($(this).attr("ref")); t++;
	});

	{if $smarty.session.account_level>2||$type==12}
		$(".cntbtn").show(); $("#lmterr").hide();
	{else}
		if( t > 10 ) {
			$(".cntbtn").hide(); $("#lmterr").show();
		} else {
			$(".cntbtn").show(); $("#lmterr").hide();
		}
	{/if}

	$("#sloc").html(t);
	y = formatUSD(x);
	jQuery("#total").html(y);
}

function pickbigmarket() {
	jQuery('input[name^="loc_id"]').attr('checked', false);
	jQuery('input[name^="loc_id"][class=bold]').not(':disabled').attr('checked', true);
	calculatetotal();
	return false;
}

-->
</script>

{literal}
<script type="text/javascript">
<!--

$(document).ready(function($) 
{
 showprice(0);
 $('h5',this).click(function(e){
 e.stopImmediatePropagation();
 var theElement=$(this).next();
 $(theElement).slideToggle('normal', function() {calculatetotal();});
   if( $(this).hasClass('active') ) {
	$(this).removeClass('active'); 
	$(theElement).find('input').attr('disabled', 'disabled');
	calculatetotal();
   } else {
	$(this).addClass('active');
	$(theElement).find('input').attr('disabled', false);
    }
  });

	var name = "#cart";
	$(name).css("z-index", "100");
	if ( $(name).css("top") == null) { 
		$(name).css("top", "10px");
	}

	$(window).scroll(function () {
		offset = jQuery(document).scrollTop() + 115;
		if( offset <= 130 ) {
			$(name).css("position", "relative");
			$(name).css("top", "0px");
			$(name).css("left", "0px");
			return;
		} else {
			$(name).css("position", "absolute");
			$(name).css({top:offset+'px'});
		}
	});
});

-->
</script>
{/literal}
