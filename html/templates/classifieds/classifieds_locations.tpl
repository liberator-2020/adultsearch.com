{literal}
<style type="text/css">
#cart {padding:5px;display:inline-block;border:1px solid #000000;background-color:#D5ECFF;height:80px;}
#cart p {margin-top: 0.7em; margin-bottom: 0.7em;}
#total, #sloc {font-weight:bold;}
</style>
{/literal}

<form method="post" action="">
	{if $ref}<input type="hidden" name="ref" value="{$ref}" />{/if}

	<h2 id="h2">Change ad locations</h2>

	{if $error}<p class="error">ERROR: {$error}</p>{/if}

	<div id="cart">
		<p>
			Select: 
			<a href="" onclick="pickall(); return false;">All</a> | 
			<a href="" onclick="picknone(); return false;">None</a> |
			<a href="" onclick="pickbigmarket(); return false;">Big Markets</a> | 
			<a href="" onclick="pickoriginal(); return false;">Original</a>
		</p>
		<p>
			Total locations selected: <span id="sloc">0</span>
		</p>
		<p>
			<input type="submit" name="submit" value="Save locations" />
			<input type="button" value="Back" onClick="history.go(-1);return true;" />
		</p>
	</div>

	<p>Go to: 
		<a href="#US" onclick="$('#US').remove().prependTo('#country'); return false;">United States</a>, 
		<a href="#CA" onclick="$('#CA').remove().prependTo('#country'); return false;">Canada</a>, 
		<a href="#UK" onclick="$('#UK').remove().prependTo('#country'); return false;">United Kingdom</a>
	</p>

	<div id='country'>
		{section name=l loop=$list}
		{assign var=l value=$list[l]}
			<a name="{$l.name}"></a>
			<div class="loc_section clearfix" id="{$l.name}">
				<h1>{$l.title}</h1>
				<div>
					{section name=us loop=$l.list}
					{assign var=tt value=$l.list[us]}
						<ul class="states">
							{section name=us2 loop=$tt}
							{assign var=t value=$tt[us2]}
								<li class="state">
									<a name="{$t.real_name}"></a>
									<div>{$t.real_name}</div>
									<ul class="cities">
										{section name=s3 loop=$t.city}
										{assign var=c value=$t.city[s3]}
											<li><input type="checkbox" name="loc_id[]" value="{$c.loc_id}" id="loc_id{$c.loc_id}" {if $c.checked}data-original="1" checked="checked"{/if} {if $c.bold}class="bold"{/if} onclick="calculate();"> {if $c.bold}<b>{$c.loc_name}</b>{else}{$c.loc_name}{/if}</li>
										{/section}
									</ul>
								</li>
							{/section}
						</ul>
					{/section}
				</div>
			</div> 
		{/section}
	</div>

	<div style="clear:both;padding-top:15px"><input type="submit" name="submit" value="Save locations" /></div>
</form>

{literal}
<script type='text/javascript'>
<!--
function pickall() {
	jQuery('input[name^="loc_id"]').not(':disabled').attr('checked', true);
	calculate();
}
function picknone() {
	jQuery('input[name^="loc_id"]').attr('checked', false);
	calculate();
}
function pickbigmarket() {
	jQuery('input[name^="loc_id"]').attr('checked', false);
	jQuery('input[name^="loc_id"][class=bold]').not(':disabled').attr('checked', true);
	calculate();
}
function pickoriginal() {
	jQuery('input[name^="loc_id"]').attr('checked', false);
	jQuery('input[name^="loc_id"][data-original=1]').not(':disabled').attr('checked', true);
	calculate();
}

function calculate() {
	var t = 0;
	$("#sloc").html('');
	jQuery('input[name^="loc_id"]:checked').not(':disabled').each(function() {
		t++;
	});
	$("#sloc").html(t);
}

$(document).ready(function($) {
	var name = "#cart";
	$(name).css("z-index", "100");
	if ( $(name).css("top") == null) { 
		$(name).css("top", "10px");
	}

	$(window).scroll(function () {
		offset = jQuery(document).scrollTop() + 115;
		if( offset <= 130 ) {
			$(name).css("position", "relative");
			$(name).css("top", "0px");
			$(name).css("left", "0px");
			return;
		} else {
			$(name).css("position", "absolute");
			$(name).css({top:offset+'px'});
		}
	});
	
	calculate();
});

-->
</script>
{/literal}

