<form method="post" action="">
	<div id="cl">

		{if $error}<div class="error">{$error}</div>{/if}

		{if $current_loc|@count>1}
			Your ad is posted in more than one location. First pick the location that you want to move your ad from:<br/>
			<select id="from" name="from">
			{html_options options=$current_loc_options selected=$from}
			</select>
		{else}
			<input type="hidden" id="from" name="from" value="{$current_loc[0].loc_id}" />
			Your ad is currently located in <b>{$current_loc[0].loc_name}</b>
		{/if}

		<hr/>
		Pick state and city that you want to move your ad to:<br/>
		<span id="state_select"><select id="state" name="State"></select></span>
		<span id="city_select"><select id="city" name="City"></select></span>
		<input type="submit" name="move" value="Move My Ad" /> | 
		{if $smarty.session.account_level>1} <input type="submit" value="or Add this city" name="add"/ > |{/if} 
		<input type="button" value="Cancel - Go Back" onclick="window.location='/classifieds/myposts';" />
	</div>
</form>

{if $smarty.session.account_level>1}
	<hr/>
	<h2>Remove the post from a specific location</h2>
	<form action="" method="post">
	<select name="remove">
	{html_options options=$current_loc_options}
	<input type="submit" value="Remove"/ >
	</form>
{/if}

<script type="text/javascript">
function state_changed(state_id) {
	if (state_id == undefined)
		state_id = $('#state').val();
	_ajax("get", "/classifieds/chgloc", 'parent_id=' + state_id + '&city=' + $('#from').val() + '&move=1', 'city');
}

$('#state').on('change', function() {
	state_changed();
});

$(document).ready(function() {
	_ajax("get", "/classifieds/chgloc","states=1&state={$state_id}&city={$loc_id}&move=1","state");
	state_changed({$state_id});
	_ajax("get", "/classifieds/chgloc","states=1&state={$state_id}&city={$loc_id}&move=1","state_select_add");
	_ajax("get", "/classifieds/chgloc","parent_id={$state_id}&city={$loc_id}&move=1","city_select_add");
});
</script>
