<form method="get" action="" name="f">
{if $id}
	<input type="hidden" name="id" value="{$id}" />
{/if}

	<div class="infoblock upgrade">
		<h2 style="margin-top: 0px;">Schedule your reposts, hassle-free.</h2>
		Every time your ad is reposted, it appears at the top of its category list. <br />
		Maximize your exposure exactly when you tell us to.<br />

		<div id="repost">
			<br/>

			<div class="intro">
				<span class="title">
					<input name="recurring" type="checkbox" value="1" {if $recurring}checked="checked"{/if}/>
					Hassle Free Daily Reposting
				</span>
				<blockquote>Every day at
					<select name="auto_renew_time2">
						{html_options values=$art_values output=$art_names selected=$auto_renew_time2}
					</select>
					{if $timezone}{$timezone}{else}PST{/if}
					for <b>${$total}</b> per month. <br />
					<br />
					Price includes all of your  locations. <span class="orange">70%</span> less per month as compared to daily posting rates! This is a monthly recurring charge. You may cancel this at any time by clicking the &quot;My Ads&quot; link at the top of	this page.
				</blockquote>
			</div>

			<span class="bsubmit"><button class="bsubmit-r" type="submit" value="Upgrade My Ad"/><span>Upgrade My Ad</span></button></span>
			<span class="bsubmit"><button class="bsubmit-r" type="button" value="Cancel" onclick="window.location='/classifieds/myposts'"/><span>Cancel</span></button></span>

		</div>
	</div>
</form>
