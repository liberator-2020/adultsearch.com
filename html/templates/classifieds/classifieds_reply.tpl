<form id="emailReplyForm" name="formEmail" action="" method="post" >
	{if $error}<div class="error">{$error}</div>{/if}
	<div style="margin-bottom:1.5em;"><b>Subject:</b> {$title}</div>
	<div style="float:left;margin-right:1em;">
		<b>Your email address:</b><br>
		<input type="text" name="from" size="30" value="{$from}"/>
	</div>
	<div style="margin-bottom:1.5em;padding-left:1em;">
		<b>Confirm your email address:</b><br>
		<input type="text" name="fromVerify" size="30" value="{$fromVerify}"/>
	</div>
	<div>
		<b>Your message:</b><br>
		<textarea cols="60" rows="18" name="m">{$m}</textarea>
	</div>
	{if !$captcha_ok}
		<tr>
			<td class="first">Security Control: <br/></td>
			<td class="second">
				<input type="text" name="captcha_str" value="" autocomplete="off" />
				<span class="smallr">Type the text on the image into the above box.<br/><img src="/captcha.php?{$time}" alt="" /></span>
			</td>
		</tr>
	{else}
		<input type="hidden" name="captcha_str" value="{$captcha_str}" />
	{/if}
	<input type="submit" value="Send Reply" class="button" id="submit_button" onclick='$("#submit_button").value = "Processing..."; $("#submit_button").attr("disabled", true); $("#emailReplyForm").submit(); return true;' style="margin-padding:4px;font-size:1.2em;">
</form>
<a href="/classifieds/look?id={$id}">Cancel - Go Back</a>
