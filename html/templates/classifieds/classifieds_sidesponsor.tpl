<form method="post" action="">
<input type="hidden" name="id" value="{$id}"/>

<div class="infoblock upgrade">
	<h2 style="margin-top: 0px;">Appear on every page ... for ${$side_sponsor_price} a month</h2>
	Show up in sponsored ads section on the right of every page!<br />
	Buy a side sponsor ad upgrade for just ${$side_sponsor_price} a month.<br />

	<div id="side">
		<br /><br />

		{if $error}
			<div style="color: red; font-weight: bold;">{$error}</div>
		{/if}

		<div class="intro">
			<img src="/images/adbuild/as_ss_example.png" alt="" width="429" height="183" hspace="10" border="0" align="right" />
			{foreach from=$locations key=loc_id item=location}
				<input type="checkbox" value="{$loc_id}" name="loc_id[]" {if $loc_id|in_array:$selected_loc_ids}checked="checked"{/if} style="vertical-align: middle;"/>{$location->getName()} ${$side_sponsor_price}/per month<br />
			{/foreach}
		</div>
	</div>

	{if !$error}
		<input type="submit" name="submit" value="Submit" class="blue_btn"/>
	{/if}
    <a href="/classifieds/myposts" class="blue_btn">Cancel</a>
</div>

</form>
