<form method="post" action="">

{if $id}<input type="hidden" name="id" value="{$id}" />{/if}

{if $error}<span class="error">{$error}</span>{/if}

<div class="infoblock upgrade">
	<h2 style="margin-top: 0px;">Schedule your reposts, hassle-free.</h2>
	Every time your ad is reposted, it appears at the top of its category list. <br />
	Maximize your exposure exactly when you tell us to.<br />

	<div id="repost">
		<br/>
		<div class="intro">
		<span class="title">Repost Exactly When You Want You To</span>
		<blockquote> Every
			<select name="auto_renew_fr">
				{html_options values=$arf_values output=$arf_names selected=$auto_renew_fr}
			</select>
			at
			<select name="auto_renew_time">
				{html_options values=$art_values output=$art_names selected=$auto_renew_time}
			</select>
			{if $timezone}{$timezone}{else}PST{/if}
			<select name="auto_renew">
				{html_options values=$ar_values output=$ar_names selected=$auto_renew}
			</select>
			<br />
			<br />
			Price includes all of your  locations. <span class="orange">50%</span> less per day as compared to daily posting rates! This is a single, non-recurring charge. You may purchase additional reposts at any time by clicking the &quot;My Ads&quot; link at the top of this page.
		</blockquote>
		</div>
	</div>

	<input type="submit" name="submit" value="Submit" class="blue_btn"/>
    <a href="/classifieds/myposts" class="blue_btn">Cancel</a>

</div>

</form>
