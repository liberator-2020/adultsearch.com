<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$subject}</title>
<style type="text/css">
body {
	font-size: 10pt;
}
.cap {
	font-size: 0.9em;
	font-weight: bold;
}
table {
	font-size: 1em;
}
td {
	vertical-align: top;
}
</style>
</head>
<body>
<div>
	{if $edit}Ad #{$id} edited:&nbsp;{else}New ad #{$id}:&nbsp;{/if}
	<a href="{$url}" target="_blank">view on AS</a>&nbsp;&middot;&nbsp;
	<a href="http://adultsearch.com/mng/classifieds?cid={$id}" target="_blank">manage</a>&nbsp;&middot;&nbsp;
	<a href="http://adultsearch.com/mng/classifieds?action=delete&action_id={$id}" target="_blank">delete and/or ban</a>
	<br />
	{if $admin}<span style="color: green; font-weight: bold;">Posted by AS administrator!</span><br />{/if}
	<table>
	<tr>
		<td>
			<img src="cid:thumb_{$thumb}" />
		</td>
		<td>
			<span class="font-size: 0.9em; font-weight: bold;">Title:</span> {$title}<br />
			<span style="font-size: 0.9em; font-weight: bold;">Type:</span> {$category}<br />
			<span class="font-size: 0.9em; font-weight: bold;">Phone:</span> 
				{if !$phone}<span style="font-weight: bold; color: red;">No phone!</span>{else}{$phone} 
					{if $is_phone_in_whitelist}
						<span style="color: green;">{$is_phone_in_whitelist}</span>
					{else}
						(phone not in whitelist)
					{/if}
					{if $area_code_not_valid}
						<span style="font-weight: bold; color: red;">Phone area code is not valid (NANP)</span>
					{/if}
				{/if}
				<br />
			<span style="font-size: 0.9em; font-weight: bold;">{$locations|@count} location{if $locations|@count > 1}s{/if}:</span> 
			{foreach from=$locations item=loc}
				{$loc}; 
			{/foreach}<br />
			<span style="font-size: 0.9em; font-weight: bold;">Paid:</span> ${$paid}{if $cc_info} ({$cc_info}){/if}
			{if $city_cover_loc_cnt}
				<br /><span style="font-size: 0.9em; font-weight: bold;">City cover purchased!</span> ({$city_cover_loc_cnt} location(s) for {$city_cover_days} days)
			{/if}
		</td>
	</tr>
	</table>
	Ad Owner: account <a href="http://adultsearch.com/mng/accounts?account_id={$account_id}">#{$account_id}</a> - {$acc_email}</a> {if $whitelisted} is <span style="color: green; font-weight: bold;">whitelisted</span>, {/if}has {$user_ads_count} live classified ads.
	{if $ip_address_text}
		Logged in from {$ip_address_text}.
	{/if}
	<a href="https://adultsearch.com/mng/accounts?account_id={$account_id}&action=whitelist&action_id={$account_id}">Whitelist account #{$account_id}</a>
	<br />
	{if $images}
		<hr />
		{$images|@count} images{if $images|@count gt 5}, first 5 images:{else}:{/if}
		{if $images}
			<br />
			{foreach $images as $image}
				{if $image@index >= 5}{break}{/if}
				<img src="cid:image_{$image}" style="max-width: 80px; max-height: 80px;" />
			{/foreach}
		{/if}
	{/if}
	<hr />
	Content:<br />
	{$content}
</div>
</body>
</html>
