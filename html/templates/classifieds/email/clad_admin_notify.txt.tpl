{if $edit}Ad #{$id} edited:&nbsp;{else}New ad #{$id}{/if}
------------------
View on AS: {$url}
Manage: http://adultsearch.com/mng/classifieds?cid={$id}
Delete and/or ban: http://adultsearch.com/mng/classifieds?action=delete&action_id={$id}
------------------
{if $admin}
*** Posted by AS administrator ! ***
------------------
{/if}
Title: {$title}
Type: {$category}
Phone: {if !$phone}No phone!{else}{$phone} {if $is_phone_in_whitelist}({$is_phone_in_whitelist}){else}(phone not in whitelist){/if}{if $area_code_not_valid} - Phone area code is not valid (NANP) !{/if}{/if}
{$locations|@count} locations:
{foreach from=$locations item=loc}
	{$loc}; 
{/foreach}
Paid: ${$paid}{if $cc_info} ({$cc_info}){/if}
{if $city_cover_loc_cnt}City cover purchased! ({$city_cover_loc_cnt} location(s) for {$city_cover_days} days){/if}
------------------
Account {$acc_email} {if $whitelisted}*whitelisted*{/if} {$user_ads_count} live classified ads. {if $ip_address_text}Logged in from {$ip_address_text}{/if}
Whitelist account #{$account_id}: https://adultsearch.com/mng/accounts?account_id={$account_id}&action=whitelist&action_id={$account_id}
------------------
{$images|@count} images{if $images|@count gt 5}, first 5 images:{else}:{/if}
{if $images}
	{foreach $images as $image}
		{if $image@index >= 5}{break}{/if}
		<img src="{$config_image_server}/classifieds/t/{$image}" style="max-width: 80px; max-height: 80px;"/>
	{/foreach}
{/if}
------------------
Content:
{$content}
