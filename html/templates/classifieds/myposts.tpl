<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/account/account.css?20190430">

<div class="container account">

    {include "account/menu.tpl" ebiz_login_url=$ebiz_login_url ebiz_available=$ebiz_available}

    <div class="wrap">
		Your Account Id#: {$account_id}<br />
		You have <strong>{$count_total}</strong> classified ads{if $count_total > 0} (<strong>{$count_live}</strong> live ads and <strong>{$count_expired}</strong> expired ads){/if}.
		{if $repost}
		<br />
		You have <strong>{$repost}</strong> top of the list credits. <a href="/classifieds/topup" class="btn btn-xs btn-success">Buy top-ups</a>
		{/if}
		{if $budget > 1}
			<br />
			You have <strong>${$budget|round}</strong> left in your advertising budget balance.
		{/if}
		{if $escorts[0].type == 2}
			<br /><br />
				We recommend the following site to advertise on: <a href="https://www.tsescorts.com/advertising?utm_source=as&utm_medium=link&utm_campaign=recommend_advertise" class="btn btn-info" target="_blank">TSescorts.com</a>
		{else if $eccieLinkDisplay}
			<br /><br />
			We recommend the following site to advertise on: <a href="https://eccie.net" target="_blank"><img src="/images/eccie.jpg" width="80" style="border-radius: 4px;" /></a>
		{/if}
	</div>

	{section name=s1 loop=$escorts}
	{assign var=c value=$escorts[s1]}
	<div class="wrap row" style="margin-left: 0px; margin-right: 0px;">

		<div class="summary">
			<div class="pull-right">
			{if $c.done>0}
				<a href="/classifieds/look?id={$c.id}" target="_blank" class="btn btn-xs btn-success">View Ad</a>
				<a href="/adbuild/step3?ad_id={$c.id}" class="btn btn-xs btn-success">Edit Ad</a>
			{else}
				<a href="/adbuild/step3?ad_id={$c.id}" class="btn btn-xs btn-success">Renew</a>
			{/if}
			</div>
		  <strong>Id #{$c.id}  - {$c.firstname} in {$c.category}</strong><br />
		  &quot;{$c.name|truncate:100}&quot;
			{if $c.done == 3}
				<br /><span style="color: red;">This ad is waiting for approval.</span>
			{/if}
		</div>

		<div class="col-sm-6 actions">
			Manage Your Ad:<br />
			<form name="dropdown" id="dropdown">
			<select name="jumpMenu" onchange="jump(this.options[this.selectedIndex].value)" class="form-control">
			<option value="" selected="selected">&nbsp;</option>
			{if $c.done > 0}
				<option value="/classifieds/look?id={$c.id}">View Ad</option>
				<option value="/adbuild/step3?ad_id={$c.id}">Edit Ad</option>
		  		<option value="/classifieds/thumbnail?id={$c.id}">Edit Thumbnail</option>
				<option value="/classifieds/duplicate?id={$c.id}">Duplicate Ad</option>
				<option value="/classifieds/move?id={$c.id}">Change Location</option>
			{/if}
			{if $c.done < 1}
				<option value="/adbuild/step3?ad_id={$c.id}">Renew Ad</option>
			{/if}
			<option value="/classifieds/myposts?remove={$c.id}">Delete Ad</option>
			</select>
			</form>
			
			<br />

			{if $c.done != 3}
				Manage Reposts &amp; Ad Promotion:<br />
				<form name="dropdown2" id="dropdown2">
					<select name="jumpMenu2" onchange="jump(this.options[this.selectedIndex].value)" class="form-control">
						<option value="" selected="selected">&nbsp;</option>
						{if $c.done > 0}
							{if $repost || $c.auto_renew}
								<option value="/classifieds/repost?id={$c.id}">Put Ad At The Top</option>
							{/if}
							<option value="/classifieds/topup">Buy Top-Up Credits</option>
							{if $c.total_loc < 11}
								<option value="/classifieds/sponsor?id={$c.id}">Buy City Thumbnail</option>
								<option value="/classifieds/side?id={$c.id}">Buy Side Sponsor</option>
								{if $c.type == 2 || $agency}
									<option value="/classifieds/sticky?id={$c.id}">Buy Sticky Upgrade</option>
								{/if}
							{/if}
						{else}
							<option value="/adbuild/step3?ad_id={$c.id}">Renew Ad</option>
						{/if}
					</select>
				</form>
			{/if}
		</div>

		<div class="col-sm-6" style="padding-left: 20px;">
			<a href="/classifieds/thumbnail?id={$c.id}" title="Edit Your Thumbnail"><img src="{$c.thumb}?{$current}" alt="" id="thumb{$c.id}" height="75" width="75" style="border: solid 1px #000000;" /></a>

			<div>
				<label>City</label>
				{if $c.total_loc==1}{$c.location}{else}<a href='/classifieds/showloc?id={$c.id}' class='customurl'>Multiple</a>{/if}
			</div>
		
			{if $c.done==1}
				<div>
					<label>Post Date</label>
					{$c.updated}
					<a href="/classifieds/topups?id={$c.id}" class="btn btn-xs btn-default">Top Ups</a>
				</div>
				{if $c.recurring!=1}
				<div>
					<label>Expires in</label>
					{$c.expires_proper}
				</div>
				{/if}
				{if !$repost}
				<div>
					<label>Auto Repost Remaining</label>
					{$c.auto_renew}
				</div>
				{/if}
				{if $c.recurring==1}
				<div>
					<label>Next Billing Date</label>
					{$c.nextbill}</div>
				{/if}
				{if $c.auto_renew}
				<div>
					<label>Next Repost Date</label>
					{$c.time_next_proper}</div>
				{/if}
			{/if}

			{if $c.sponsor==1}<div><label>City Thumbnail</label> {$c.sponsor_expires_proper}</div>{/if}

			{if $c.side==1}<div><label>Side Sponsor</label> {$c.side_expires_proper}</div>{/if}

			{if $c.sticky==1}<div><label>Sticky Ad</label> {$c.sticky_expires_proper}</div>{/if}

			<div>
				<label>Share</label>
				Post My Ad to My<br />
				<a href="http://www.facebook.com/sharer/sharer.php?u=http%3A//adultsearch.com/classifieds/look?id={$c.id}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/images/classifieds/facebook_32.png" alt="My AdultSearch on Facebook" height="32" width="32" /></a> <a href="https://twitter.com/share?url=http%3A%2F%2Fadultsearch.com%2Fclassifieds%2Flook?id={$c.id}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="custom-tweet-button"> <img src="/images/classifieds/twitter_32.png" alt="My AdultSearch on Twitter" height="32" width="32" /></a> 
				<a href="https://plus.google.com/share?url=http://adultsearch.com/classifieds/look?id={$c.id}" onclick="javascript:window.open(this.href,  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/images/classifieds/gplus_32.png" alt="Share on Google+" /></a> 
				<a href="mailto:?subject=My%20AdultSearch%20Post&amp;body=Check%20out%20my%20ad%20on%20AdultSearch%20http://adultsearch.com/classifieds/look?id={$c.id}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" title="Share by Email"><img src="/images/classifieds/email_32.png" alt="My AdultSearch on Email" height="32" width="32" /></a>
			</div>
			
		</div>
	</div>
	{sectionelse}
	<br /><br />You have no classified ads.<br />
	{/section}

</div>

{literal}
<script type="text/javascript">
function jump(val){ 
	if (val.indexOf("remove=") > 0) {
		if( confirm('Are you sure you want to delete this ad ? Click "OK" to continue or "Cancel" to take no action.') ) {
			window.location = val;
		}
		return;
	} else if(val.indexOf("/repost") > 0) {
		if( confirm('Are you sure you want to re-post this ad now ? Click "OK" to continue or "Cancel" to take no action.') ) {
			window.location = val;
		}
		return;
	} else if(val!='') {
		window.location = val;
	}
} 
</script>
{/literal}
