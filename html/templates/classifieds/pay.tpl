<style type="text/css">
div.alert {
	padding: 3px !important;
}
table {
	border-collapse: collapse;
}
table td {
	padding: 5px;
	vertical-align: top;
}
</style>

<h2>Pay for ad(s)</h2>

<form action="" method="post" id="pay_form">
<input type="hidden" name="action_id[]" value="{$action_ids|implode:','}" />

"Pay for these {$cnt} ads ?:<br />
<table>
<tr>
	<th>Id</th>
	<th>Thumb</th>
	<th>Type</th>
	<th>Title</th>
	<th>Location count</th>
	<th>Last payments</th>
	<th>Options</th>
</tr>
{foreach from=$clads item=clad}
<tr>
	<td>{$clad.id}</td>
	<td><img src="{$config_image_server}/classifieds/{$clad.thumb}" /></td>
	<td>{$clad.type}</td>
	<td>{$clad.title}</td>
	<td>
		<span id="location_count">{$clad.locations|count}</span>
		<input type="hidden" name="clad_{$clad.id}_loc_ids" id="clad_{$clad.id}_loc_ids" value="{foreach from=$clad.locations item=loc name=clids}{if $smarty.foreach.clids.index > 0},{/if}{$loc->getId()}{/foreach}" />
	</td>
	<td>
		{foreach from=$clad.payments item=p}
			{$p.last_transaction_stamp|date_format:'%m/%d/%Y'} - #{$p.last_transaction_id} - ${$p.last_transaction_amount} {if $p.subscription_status == 1 && $p.next_renewal_date}(next renewal on {$p.next_renewal_date}){else}(one-time payment){/if}<br />
			<div class="alert alert-warning">Please prorate this already paid amount into new payment amount. {if $p.subscription_status == 1 && $p.next_renewal_date}<strong>This subscription will be canceled</strong>{/if}</div>
		{/foreach}
		<a href="/mng/sales?item_id={$clad.id}">All sales transactions for this ad</a>
	</td>
	<td>
		{foreach from=$clad.locations item=location}
			<input type="checkbox" name="city_thumbnail_{$clad.id}_{$location->getId()}" id="city_thumbnail_{$clad.id}_{$location->getId()}" value="1" data-price="{$city_thumbnail_price}" class="recompute"/>City thumbnail in {$location->getLabel()} - (${$city_thumbnail_price}/month)<br />
		{/foreach}
		<hr style="margin: 0px;"/>
		{foreach from=$clad.locations item=location}
			<input type="checkbox" name="side_sponsor_{$clad.id}_{$location->getId()}" id="side_sponsor_{$clad.id}_{$location->getId()}" value="1" data-price="{$side_sponsor_price}" class="recompute"/>Side sponsor in {$location->getLabel()} - (${$side_sponsor_price}/month)<br />
		{/foreach}
		<hr style="margin: 0px;"/>
{*
		<input type="checkbox" name="recurring_repost_{$clad.id}" id="recurring_repost_{$clad.id}" value="1" class="recompute" />Recurring daily repost ($49.99/location)<br />
		#Reposts ($4.99/repost): <input type="text" name="auto_renew_{$clad.id}" id="auto_renew_{$clad.id}" value="" size="1" class="recompute" /><br />
		Repost freq: <select name="auto_renew_fr_{$clad.id}">
			<option value="1">Every 1 day</option>
			<option value="2">Every 2 days</option>
			<option value="3">Every 3 days</option>
			<option value="4">Every 4 days</option>
			<option value="5">Every 5 days</option>
		</select>
		at: <select name="auto_renew_fr_{$clad.id}" id="auto_renew_fr_{$clad.id}" class="recompute" >
		{html_options values=$art_values output=$art_names selected=$auto_renew_time}
        </select>
		{if $timezone}{$timezone}{else}PST{/if}
		<hr style="margin: 0px;"/>
*}
		{foreach from=$clad.locations item=location}
			<input type="checkbox" name="sticky_{$clad.id}_{$location->getId()}" id="sticky_{$clad.id}_{$location->getId()}" value="1" data-price="{$location->getStickyPrice($clad.type, 30)}" class="recompute"/>Sticky in {$location->getLabel()} - (${$location->getStickyPrice($clad.type, 30)}/month)<br />
		{/foreach}
{*
		<input type="checkbox" name="sponsor_desktop_{$clad.id}" id="sponsor_desktop_{$clad.id}" value="1" class="recompute"/>Desktop position ($200)<br />
		<input type="checkbox" name="sponsor_mobile_{$clad.id}" id="sponsor_mobile_{$clad.id}" value="1" class="recompute"/>Mobile position ($200)<br />
		Position in {$clad.locations}: <input type="text" name="sponsor_position_{$clad.id}" id="sponsor_position_{$clad.id}" value="" style="max-width: 30px;"/>
*}
	</td>
</tr>
{/foreach}
</table>
<hr/>
Global reposts: <input type="text" name="global_reposts" id="global_reposts" value="" class="recompute" /><br />

<hr style="border-top: 1px solid #666;"/>
<table>
	<tr>
		<th>Suggested Amount:</th>
		<td>
			<input type="text" id="computed_amount" value="" style="max-width: 60px;" readonly="readonly"/> 
			<button type="button" id="copy_amounts_btn">Copy suggested amount to pay amount</button>
		</td>
	</tr>
	<tr>
		<th />
		<td><input type="checkbox" name="recurring" id="recurring" value="1" checked="checked" class="recompute" /> Recurring payment</td>
	</tr>
	<tr>
		<th>Amount:</th>
		<td><input type="text" name="amount" id="amount" value="" style="max-width: 60px;"/><span id="amount_after"> to pay now.</td>
	</tr>
	<tr>
		<th>Rebill Amount:</th>
		<td><input type="text" name="amount_recurring" id="amount_recurring" value="" style="max-width: 60px;"/><span id="amount_after"></span> to pay monthly</td>
	</tr>
	<tr>
		<td/>
		<td><input type="submit" name="submit" value="Submit" /></td>
	</tr>
</table>

{$action_hidden_inputs}

</form>

{literal}
<script type="text/javascript">
var city_thumbnail_price = {/literal}{$city_thumbnail_price}{literal};
var side_sponsor_price = {/literal}{$side_sponsor_price}{literal};

function roundToTwo(num) {    
    return +(Math.round(num + "e+2")  + "e-2");
}
function recompute() {
	var clad_ids = $('input[name="action_id[]"]').val();
	var clad_ids = clad_ids.split(',');
	var total = 0;
	for (var i = 0; i < clad_ids.length; i++) {
		var clad_total = 0;
		var clad_id = clad_ids[i];
		var clad_loc_ids = $('#clad_'+clad_id+'_loc_ids').val().split(',');
		var location_count = clad_loc_ids.length;
		if ($('#recurring_repost_'+clad_id).is(":checked")) {
			clad_total += location_count * 49.99;
		} else {
			var auto_renew = parseInt($('#auto_renew_'+clad_id).val());
			if (auto_renew)
				clad_total += location_count * 4.99*auto_renew;
		}
		if ($('#sponsor_desktop_'+clad_id).is(":checked"))
			clad_total += location_count * 500;
		if ($('#sponsor_mobile_'+clad_id).is(":checked"))
			clad_total += location_count * 500;

		for (var j = 0; j < clad_loc_ids.length; j++) {
			var loc_id = clad_loc_ids[j];
			if ($('#city_thumbnail_'+clad_id+'_'+loc_id).is(":checked"))
				clad_total += city_thumbnail_price;
			if ($('#side_sponsor_'+clad_id+'_'+loc_id).is(":checked"))
				clad_total += side_sponsor_price;
			if ($('#sticky_'+clad_id+'_'+loc_id).is(":checked"))
				clad_total += $('#sticky_'+clad_id+'_'+loc_id).data('price');
		}

		clad_total = roundToTwo(clad_total);
		console.log('Clad #'+clad_id+' location_count='+location_count+' total='+clad_total);
		total += clad_total;
	}
	if (total < 1)
		total = location_count * 9.99;
	total = roundToTwo(total);
	console.log('Total = '+total);
	$('#computed_amount').val(total);
}
function copy_amounts() {
	$('#amount').val($('#computed_amount').val());
	if ($('#recurring').is(":checked"))
		$('#amount_recurring').val($('#computed_amount').val());
}
function recurring_change() {
	if ($('#recurring').is(":checked"))
		$('#amount_recurring').val($('#amount').val());
	else
		$('#amount_recurring').val('');
}
function validate() {
	var clad_ids = $('input[name="action_id[]"]').val();
	var clad_ids = clad_ids.split(',');
	var error = '';
	for (var i = 0; i < clad_ids.length; i++) {
		var clad_id = clad_ids[i];
		var clad_loc_ids = $('#clad_'+clad_id+'_loc_ids').val().split(',');
		for (var j = 0; j < clad_loc_ids.length; j++) {
			var loc_id = clad_loc_ids[j];
			if ($('#sponsor_desktop_'+clad_id).is(":checked") && !$('#sponsor_position_'+clad_id).val())
				error = error+'Enter sponsor desktop position for ad #'+clad_id+' !\n';
			else if ($('#sponsor_mobile_'+clad_id).is(":checked") && !$('#sponsor_position_'+clad_id).val())
				error = error+'Enter sponsor mobile position for ad #'+clad_id+' !\n';
		}
	}
	if (!$('#amount').val())
		error = error+'Empty amount to pay !\n';
	if ($('#recurring').is(":checked") && !$('#amount_recurring').val())
		error = error+'Empty rebill amount !\n';
	if (error) {
		alert(error);
		return false;
	}
	return true;
}
$(document).ready(function() {
	$('input.recompute, select.recompute').change(function(ev) {
		recompute();
	});
	$('#recurring').change(function(ev) {
		recurring_change();
	});
	$('#copy_amounts_btn').click(function(ev) {
		copy_amounts();
	});
	$('#pay_form').submit(function () {
		return validate();
	});
});
</script>
{/literal}
