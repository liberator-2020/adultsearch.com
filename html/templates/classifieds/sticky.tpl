<style>
.city_options{
	padding-left: 15px;
}
</style>
<form method="post" action="">
<input type="hidden" name="id" value="{$id}"/>

<div class="infoblock upgrade">
	<h2 style="margin-top: 0px;">Appear always on the top of the list ... from ${$sticky_min_monthly_price} a month</h2>
	<p>
		Show up at the very top of the list, and be ahead of your competition.<br />
		Ads on top of the list have 500% more views than average ad. Your ad will be always at the beginning of the first page and will not be moved to second or subsequent pages.
	</p>

	<div id="side">
		<br /><br />

		<div class="intro">
			<img src="/images/adbuild/as_sticky_example.png" alt="Sticky Ad Example" style="width: 40%; height: auto;" hspace="10" border="0" align="right"/>
            {assign var=subscribedStatusMessage value="– You are subscribed to the waiting list for a sticky upgrade in this location only"}

			{foreach from=$locations key=key item=location}
				<input type="checkbox" value="{$location['loc']->getId()}" name="loc_id[]" class="city {if $location['has_sticky'] || $location['not_available']}not-allowed{/if}" id="loc_id_{$location['loc']->getId()}" {if $location['has_sticky'] || $location['not_available']}disabled="disabled"{/if}/>
				{$location['loc']->getName()}{if $location['has_sticky']}<span> (already purchased)</span>{/if}

                {if in_array($location['loc']->getId(), $awaiting_locations_ids) && !$location['has_sticky'] && $location['not_available']}
					<span style="color: #2E8B57;">
						 {$subscribedStatusMessage}
					</span>
                {elseif !$location['has_sticky'] && $location['not_available']}
					<span data-location-id="{$location['loc']->getId()}" data-location-name="{$location['loc']->getName()}" data-type-id="{$clad->getType()}" data-classified-id="{$id}">
						<span style="color: red;">Sticky upgrade in {$location['loc']->getName()} not available - sold out</span>
						<a style="cursor: pointer;" class="js-waiting-list-add">Click here and join the waiting list for a sticky ad upgrade in this location only</a>
					</span>
                {/if}

				<br /><br />
				<div id="period_{$location['loc']->getId()}" class="days-container">
					<input type="radio" name="days[{$location['loc']->getId()}]" class="days" value="7">${$location['price_weekly']} for 1 Week - One-Time payment
					<br /><br />
					<input type="radio" name="days[{$location['loc']->getId()}]" class="days" value="30">${$location['price_monthly']} for 1 Month - One-Time payment
					<br /><br />
				</div>
			{/foreach}

			<br /><br />
			<div id="recurring_div" style="display: none;">
				<input type="checkbox" name="recurring" id="recurring" value="1">Make this payment recurring - you will be charged automatically every month
			</div>
		</div>
	</div>

	{if !$error}
		<input type="submit" name="submit" value="Submit" class="blue_btn"/>
	{/if}
	<a href="/classifieds/myposts" class="blue_btn">Cancel</a>
</div>

</form>

{literal}
<script type="text/javascript">
function city_changed() {
	var at_least_one = false;
	$('.city').each(function(ind,obj) {
		var loc_id = $(obj).val();
		//console.log('loc_id='+loc_id+', checked='+$(obj).is(':checked'));
		if ($(obj).is(':checked')) {
			$('#period_'+loc_id).show();
			at_least_one = true;
		} else {
			$('#period_'+loc_id).hide();
		}
	});
	if (at_least_one)
		days_changed();
	else
		$('#recurring_div').hide();
}
function days_changed() {
	var some_30_checked = false;
	var some_7_checked = false;
	$('.days').each(function(ind,obj) {
		var loc_id = $(obj).attr('name').substring(5, $(obj).attr('name').length-1);
		//console.log('loc_id='+loc_id+'ind='+ind+',obj=',$(obj),$(obj).val(),$(obj).is(':checked'),$(obj).attr('name'),$(obj).attr('name').length);
		if ($('#loc_id_'+loc_id).is(':checked')) {
			if ($(obj).val() == '30' && $(obj).is(':checked')) {
				some_30_checked = true;
			} else if ($(obj).val() == '7' && $(obj).is(':checked')) {
				some_7_checked = true;
			}
		}
	});
	if (some_30_checked && !some_7_checked) {
		//console.log('all days selected month, display=',$('#recurring_div').css('display'));
		$('#recurring').prop('checked', false);
		$('#recurring_div').show();
	} else {
		$('#recurring_div').hide();
	}
}
$(document).ready(function() {
	$('.city').on('change', function () {
		city_changed();
	});
	$('.days').change(function() {
		days_changed();
	});

	$('.js-waiting-list-add').on('click', function () {
		var $this        = $(this),
			$buttonBlock = $this.parent(),
			typeId       = $buttonBlock.data('type-id'),
			classifiedId = $buttonBlock.data('classified-id'),
			locationId   = $buttonBlock.data('location-id'),
			locationName = $buttonBlock.data('location-name');

		var message = "Do you want to sign up to waiting list for sticky upgrade in " + locationName + "? We will notify you whenever a sticky upgrade will become available in this location.";
		if (confirm(message)) {
			$.post(config_site_url + '/classifieds/waiting-list', {typeId: typeId, locationId: locationId, classifiedId: classifiedId}, function(response) {
				if (response.status !== 'success') {
					return;
				}

				{/literal}
				$buttonBlock.text("{$subscribedStatusMessage}");
                {literal}
				$buttonBlock.css('color', '#2E8B57');
			}, 'json');
		}
	});
});
</script>
{/literal}
