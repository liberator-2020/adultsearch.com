<script type="text/javascript" src="/js/tools/jquery.imgareaselect.min.js"></script>

<style type="text/css">
#thumb_image_select {
	max-width: 800px;
	overflow: auto;
}
#thumb_image_select ul {
	margin: 0px;
	padding: 0px;
}
#thumb_image_select li {
	display: inline-block;
	float: left;
	cursor: pointer;
	padding: 3px;
}
#thumb_image_select li.sel {
	background-color: #FFFFB9;
}
#thumb_image_select ul li:not(:first-child) {
	margin-left: 30px;
}
#thumb_image_select img {
	max-width: 80px;
	max-height: 80px;
}
#tic_preview_div {
	width: 75px;
	height: 75px;
	overflow: hidden;
}


/*
 * imgAreaSelect default style
 */
.imgareaselect-border1 {
	background: url('/css/crop/border-v.gif') repeat-y left top;
}
.imgareaselect-border2 {
    background: url('/css/crop/border-h.gif') repeat-x left top;
}
.imgareaselect-border3 {
    background: url('/css/crop/border-v.gif') repeat-y right top;
}
.imgareaselect-border4 {
    background: url('/css/crop/border-h.gif') repeat-x left bottom;
}
.imgareaselect-border1, .imgareaselect-border2, .imgareaselect-border3, .imgareaselect-border4 {
    filter: alpha(opacity=50);
	opacity: 0.5;
}
.imgareaselect-handle {
    background-color: #fff;
    border: solid 1px #000;
    filter: alpha(opacity=50);
    opacity: 0.5;
}
.imgareaselect-outer {
    background-color: #000;
    filter: alpha(opacity=50);
    opacity: 0.5;
}
.imgareaselect-selection {  
}

</style>

<h1>Change thumbnail for classified ad #{$id}</h1>

{if $nopic}
	First you need to upload a picture for this ad. <a href="/adbuild/step3?ad_id={$id}">Click here</a> to upload a picture.
{else}

<table>
<tr>
	<td width="130" style="border-right: 1px solid #666;">
		Current thumbnail:<br />
		<img id="current" src="{$config_image_server}/classifieds/{$current}?{$current_time}" title="{$current}" />
		<br /><br />
		<div id="status">
			<span><a href="/classifieds/myposts" class="active">Back to My Ads</a></span>
		</div>
	</td>
	<td>
		<strong>1.</strong> Click on image, from which you want to create thumbnail:<br />
		<div id="thumb_image_select">
			<ul>
				<li>
					<button type="button" style="width: 75px; height: 75px;">Upload new image</button>
				</li>
			{foreach from=$images item=image}
				<li data-imageid="{$image.id}" data-filename="{$image.filename}">
					<img src="{$config_image_server}/classifieds/t/{$image.filename}" title="Click to select"/>
				</li>
			{/foreach}
			</ul>
		</div>
		<div id="detail_1" style="display: none;">
			<strong>2.</strong>	Upload image file (JPG/GIF/PNG), idealy 75x75px. Larger images will be cropped to square and resized to 75x75px.<br />
			<form method="post" enctype="multipart/form-data" action="">
				<input type="file" name="thumbnail_file" value="" /><br />
				<input type="submit" name="submit" value="Upload" />
			</form>
		</div>
		<div id="detail_2" style="display: none;">
			<strong>2.</strong>	Move and/or expand the highlighted box to create your preferred thumbnail:<br />
			<div id="thumb_image_crop"> 
				<table>
				<tr>
					<td valign="top">
						<img src="{$config_site_url}/images/placeholder.gif" id="tic_image" />
					</td>
					<td width="20"></td>
					<td valign="top">
						Selection preview:<br />
						<div id="tic_preview_div">
							<img src="{$config_site_url}/images/placeholder.gif" id="tic_preview_img" />
						</div>
						<br/>
						<input type="button" id="button" value="Update" /> 
						<br />
						<div id="thumbchanged" style="display:none">Thumbnail has been successfully updated.<br />If you don't like it, you can submit the form again.<br /><span style="color: red; font-weight: bold;">Please allow 2 minutes to reflect the change on our pages.</span><br /></div>
					</td>
				</tr>
				</table>
			</div>
		</div>
	</td>
</tr>
</table>

<form id="thumbform">
	<input type="hidden" name="id" value="{$id}" />
	<input type="hidden" name="image_id" value="" id="image_id" />
	<input type="hidden" name="x1" value="" id="x1" />
	<input type="hidden" name="y1" value="" id="y1" />
	<input type="hidden" name="x2" value="" id="x2" />
	<input type="hidden" name="y2" value="" id="y2" />
	<input type="hidden" name="w" value="" id="w" />
	<input type="hidden" name="h" value="" id="h" />
</form>

{literal}
<script type="text/javascript">
var ia = null;

function thumb_update() {
	$("#button").val('Please Wait.....').attr("disabled", "disabled");
	new $.ajax({
		url : "/classifieds/thumbnail",
		method : "get",
		data : $("#thumbform").serialize(),
		success : function(result) {
			if (result.substring(0,5) == 'Error') {
				alert(result);
			} else if (0 < result.indexOf("jpg")) {
				$('#current').attr('src', result);
				$("#thumbchanged").show();
			} else {
				console.log(result);
			}
			$("#button").val('Update').attr("disabled", false);
		}
	});
}

function crop_init() {
	if (ia == null) {
		ia = $('#tic_image').imgAreaSelect({
			instance: true,
			aspectRatio: '1:1',
			onSelectChange: preview,
			x1: 0,
			y1: 0,
			x2: 75,
			y2: 75
		});
	} else {
		ia.setSelection(0, 0, 75, 75);
		ia.update();
		preview(null, ia.getSelection());
	}
}

function preview(img, selection) {
	if (!selection.width || !selection.height)
		return;
	var origWidth = $('#tic_image').width();
	var origHeight = $('#tic_image').height();
	var prevWidth = $('#tic_preview_div').width();
	var prevHeight = $('#tic_preview_div').height();
	var scaleX = prevWidth / selection.width;
	var scaleY = prevHeight / selection.height;
	$('#tic_preview_img').css({
		width: Math.round(scaleX * origWidth),
		height: Math.round(scaleY * origHeight),
		marginLeft: -Math.round(scaleX * selection.x1),
		marginTop: -Math.round(scaleY * selection.y1)
	});
	$("#x1").val(selection.x1);
	$("#y1").val(selection.y1);
	$("#x2").val(selection.x2);
	$("#y2").val(selection.y2);
	$("#w").val(selection.width);
	$("#h").val(selection.height);
}

function ti_selected(elem) {
	$('#thumb_image_select li').each(function() {
		$(this).removeClass('sel');
	});
	$('#detail_1').hide();
	$('#detail_2').hide();
	elem.addClass('sel');
	console.log('f ch cnodename='+$(elem).children().first().prop('nodeName'));
	if ($(elem).children().first().prop('nodeName') == 'BUTTON') {
		if (ia != null)
	        ia.remove();
		$('#detail_1').show();
	} else {
		$('#detail_2').show();
		$("#image_id").val(elem.attr('data-imageid'));
		$('#tic_image').attr('src', '{/literal}{$config_image_server}{literal}/classifieds/'+elem.attr('data-filename'));
		$('#tic_preview_img').attr('src', '{/literal}{$config_image_server}{literal}/classifieds/'+elem.attr('data-filename'));
		crop_init();
	}
}

$(document).ready(function() {
	$('#thumb_image_select li').click(function() {
		ti_selected($(this));
	});
	$('#button').click(function() {
		thumb_update();
	});
	//ti_selected($('#thumb_image_select li').first());
});

</script>
{/literal}

{/if}

