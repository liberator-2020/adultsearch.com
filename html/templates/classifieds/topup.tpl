<form method="post" action="">
{if $account_id}
<input type="hidden" name="account_id" value="{$account_id}"/>
{/if}

<div class="infoblock upgrade">
	<h2 style="margin-top: 0px;">Put your ad to the top of the list ... anytime you want</h2>
	Buy top-up credits for just ${$price_topup} per one top up and then you can use this top-up credit to put any of your ads to the top of the list. Anytime you want, even multiple times a day !<br />
	Don't miss your opportunity to be the first one the clients will see in the list.<br />

	<div id="side">
		<br /><br />

		{if $error}
			<div style="color: red; font-weight: bold;">{$error}</div>
		{/if}

		<div class="intro">
			<strong>Choose number of top-up credits:</strong>
			<select name="number" id="number">
				{html_options values=$option_values output=$option_labels selected=$number}
			</select>
		</div>
	</div>

	<input type="submit" name="submit" value="Submit" class="blue_btn"/>
    <a href="/classifieds/myposts" class="blue_btn">Cancel</a>
</div>

</form>
