<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/account/account.css?20190430">

<div class="container account">

    {include "account/menu.tpl" ebiz_login_url=$ebiz_login_url ebiz_available=$ebiz_available}

    <div class="wrap">

		<div class="row">
			<div class="col-xs-12">
				{if $id}
					<h2>Latest top-ups of classified id #{$id}</h2>
					<a href="/classifieds/topups">View top-ups of all my ads</a>
				{else}
					<h2>Latest top-ups</h2>
				{/if}
			</div>
		</div>

		{if $top_ups|count > 0}
			<table class="table table-striped table-bordered">
			<tr>
				<th>Date/Time</th>
				{if !$id}
					<th>Classified Id#</th>
				{/if}
				<th/>
			</tr>
			{foreach from=$top_ups item=t}
			<tr>
				<td>{$t.stamp|date_format:"%m/%d/%Y %l:%M %p %Z"}</td>
				{if !$id}
					<td>{$t.classified_id}</td>
				{/if}
				<td>
					{if $t.author_id == $account_id}
						Manual top-up
					{else}
						Auto top-up
					{/if}
				</td>
			</tr>
			{/foreach}
			</table>
		{else}
			<em>No top_ups.</em>
		{/if}

		{include "account/help.tpl" agency=$agency}

    </div>

</div>
