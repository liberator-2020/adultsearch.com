<div id="cl">

<form action="" method="post">
<input type="hidden" name="where" value="{$where}" />

<h1>Contact Us</h1>

<div style="margin-left: 100px; line-height: 1.2em;">
	In U.S. &amp; Canada you can also contact us via phone:<br />
	<strong>1-702-935-1688</strong> (9am to 5pm EST Mon - Fri)<br />
	or via email:<br />
	<img src="/images/email_address.png" style="width: 180px; height: 20px;"/><br />
	<br />
	Law enforcement inquiries: <img src="/images/email_notrafficking.png" style="width: 180px; height: 18px; vertical-align: middle;"/>
</div>

<p>&nbsp;</p>

{if $error}<div class="error">{$error}</div>{/if}

<table border="0" cellspacing="0" cellpadding="0" class="b">

 <tr>
  <td class="first">Name</td>
  <td class="second">
	<input type="text" name="name" id="name" value="{$name}" class="text" size="40" />
  </td>
 </tr>

 <tr>
  <td class="first">E-Mail Address</td>
  <td class="second">
	<input type="text" name="email" id="email" value="{$email}" class="text" size="40" />
  </td>
 </tr>

 <tr>
  <td class="first">Contact Phone Number</td>
  <td class="second">
	<input type="text" name="phone" value="{$phone}" class="text" size="13" />
  </td>
 </tr>

 <tr>
  <td class="first">Question/Concern/Comment</td>
  <td class="second">
	<textarea name="text" rows="10" cols="50">{$text}</textarea>
  </td>
 </tr>

 <tr>
  <td class="first"></td>
  <td class="second">
	{$captcha}
  </td>
 </tr>

{if !$captcha_ok}
 <tr>
  <td class="first">Security Control: <br/>
  </td>
  <td class="second">
        <input type="text" name="captcha_str" value="" autocomplete="off" />
        <span class="smallr">Type the text on the image into the above box.<br/>
        <img src="/captcha.php?{$time}" alt="" /></span>
  </td>
 </tr>
{else}
   <input type="hidden" name="captcha_str" value="{$captcha_str}" />
 {/if}

 <tr>
  <td class="first"></td>
  <td class="second">
	<span class="bsubmit"><button type="submit" name="submit" class="bsubmit-r" value="Submit"><span>Submit</span></button>
  </td>
 </tr>

</table>

<input type="hidden" name="phone_or_email" value="" />

</form>
</div>
