<h2>Contact messages submitted through webform</h2>

{if $new_contacts}
<table class="table table-striped table-bordered table-condensed">
 <tr style="background-color: #dcdcdc;">
  <th>E-mail / Username</th>
  <th>Text</th>
  <th>Time</th>
  <th>IP</th>
 </tr>

{section name=s1 loop=$new_contacts}
{assign var=n value=$new_contacts[s1]}
 <tr class="{cycle values="n1,n2"}">
  <td>{$n.email}{if $n.username} / {$n.username}{/if}</td>
  <td><a class="read" id="{$n.contact_id}" href="/mng/?read={$n.contact_id}" onclick="return false;">{$n.text|truncate:50}</a></td>
  <td>{$n.time}</td>
  <td>{$n.ip}</td>
 </tr>
{/section}

</table>
{else}
<em>No unread contact submissions.</em>
{/if}

<div id="example" style="display:none"></div>

{literal}
<script type='text/javascript'>
$(document).ready(function() {
	$("a.read").click(function() {
		var dialogOpts = {
			modal: true, 
			bgiframe: true, 
			autoOpen: false,
			height: 580,
			width: 980,
			zIndex: 1200,
			draggable: true,
			resizeable: true, 
			title: '#'+$(this).attr('id')
		};
		$("#example").dialog(dialogOpts);
		$("#example").load($(this).attr("href"), "", function(){
			$("#example").dialog("open");
		});
		return false;
	});

	{/literal}
	{if $open}$("a#{$open}").attr('href', $("a#{$open}").attr('href')+'&error=1').click();{/if}
	{literal}

});
</script>
{/literal}
