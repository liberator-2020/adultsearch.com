{literal}
<style type="text/css">
th {text-align: left; font-size: 90%; font-weight: bold; }
.header {font-size: 150%; font-weight:bold; }
.odd {background-color: #F4F4F4; }
</style>
{/literal}

<span class="header">Data from form {$contact_formname}</span> (<a href="http://{$url}">{$url}</a>)<br />Submitted on: ({$submit})<br /><br />
<table width="100%">
{foreach from=$post item=item key=key}
<tr{cycle values=" class='odd',"}><th>{$key}:</th><td>{$item}</td></tr>
{/foreach}
</table>
