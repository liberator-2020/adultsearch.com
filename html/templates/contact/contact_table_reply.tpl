{literal}
<style type="text/css">
.contact_table_reply th {
	text-align: right;
	font-size: 90%;
	padding-right: 5px;
	vertical-align: top;
}
</style>
{/literal}

{if $error}<p class='error'>{$error}</p>{/if}

<div style="float: right;">Submitted on: ({$submit})</div>
<span class="header">Data from form {$contact_formname}</span> (<a href="http://{$url}" target="_blank">{$url}</a>{if $account_id}{assign var="go" value="http://`$url`"|urlencode} <a href="/account/impersonate?id={$account_id}&go={$go}" target="_blank">impersonate &amp go here</a>{/if})
<br style="clear: both;"/>
<br />

<form method="post" action="">
<input type="hidden" name="contact_id" value="{$contact_id}" />
<table class="contact_table_reply table table-striped table-condensed">
{if $account_id}
	<tr><th>Account:</th><td><a href="/mng/accounts?account_id={$account_id}" target="_blank">#{$account_id}</a> (username: {$username})</td></tr>
{else}
	<tr><th>Name:</th><td>{$name} <span style="color: #999;">(user was not logged in)</span></td></tr>
{/if}
<tr><th>Email:</th><td>{$email}</td></tr>
{if $phone}
	<tr><th>Phone:</th><td>{$phone}</td></tr>
{/if}
<tr><th>IP:</th><td>{$ip_address_label}</td></tr>
{if $device}
	<tr><th>Client device:</th><td>{$device}</td></tr>
{/if}
<tr><th>Text:</th><td>{$text}</td></tr>

{if $ads}
<tr>
	<th>Classifieds:</th>
	<td>
		{$ads}
		<a href="/mng/classifieds" target="_blank">Search classifeds by other parameter (firstname, ...)</a>
	</td>
</tr>
{/if}

{if $attachments|count > 0}
<tr>
	<th>Attachment(s):</th>
	<td>
		{section name=a loop=$attachments}
		{assign var=att value=$attachments[a]}
			<a src="//img.adultsearch.com/_contact/{$att.filename}"><img src="//img.adultsearch.com/_contact/{$att.filename}" alt="" border="0" /></a>
		{/section}
	</td>
</tr>
{/if}

{if $previous}
<tr>
	<th valign="top">Previous Emails:</th>
	<td valign="top">
		{section name=a2 loop=$previous}
		{assign var=p value=$previous[a2]}
		   on <b>{$p.1}</b><br/>{$p.0}
			<hr/>
		{/section}
	</td>
</tr>
{/if}

{if !$third}
	{if $bp_ad_id}
<tr>
	<td colspan="2">
		<a href="" onclick="_ajax('get', '/classifieds/claim', 'set={$bp_ad_id}', 'respond_'); return false;" class="bold">Create a manage link for BP ad #{$bp_ad_id}</a>
	</td>
</tr>
	{/if}
{*
<tr>
	<td colspan="2">
		<div id='assign'>
			{if $assigned}
				This is assigned to you. Noone else sees this message.
			{else}
				<a onclick="_ajax('get', '', 'assign={$contact_id}', 'assign'); return false;">Assign to me</a>
			{/if}
		</div>
	</td>
</tr>
*}
{/if}

<tr>
	<th>Reply:</th>
	<td>
		<textarea name="respond" id="respond_" cols="100" rows="7" style="width: 100%;"></textarea>
		<input type="submit" value="Send Respond to user's email" />
	</td>
</tr>
<tr>
	<th>Other actions:</th>
	<td>
		{if $user_delete_link}
			<a href="{$user_delete_link}" class="btn btn-sm btn-default">Delete user account</a><br />
		{/if}
		<input type="submit" name='read' value="Make This Read" /> --
		<input type="submit" name="toagency" value="Send to Agency" /> -
		<input type="submit" name="toadmin" value="Send to Admin" />
	</td>
</tr>
</table>
</form>

