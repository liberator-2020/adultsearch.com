<style type="text/css">
td, th {
	padding: 3px;
}
th {
	vertical-align: top;
}
</style>

<div style="float: right;">Submitted on: ({$submit})</div>
<span class="header">Data from form {$contact_formname}</span> (<a href="http://{$url}" target="_blank">{$url}</a>)
<br style="clear: both;"/>
<br />

<table width="100%">
<tr><th>Contact id:</th><td>#{$contact_id}</td></tr>
{if $account_id}
	<tr><th>Account:</th><td>#{$account_id} ({$username})</td></tr>
{else}
	<tr><th>Name:</th><td>{$name}</td></tr>
{/if}
<tr><th>Email:</th><td>{$email}</td></tr>
{if $phone}
	<tr><th>Phone:</th><td>{$phone}</td></tr>
{/if}
<tr><th>IP:</th><td>{$ip_address_label}</td></tr>
{if $device}
	<tr><th>Client device:</th><td>{$device}</td></tr>
{/if}
<tr><th>Text:</th><td>{$text}</td></tr>
</table>

