Contact id: #{$contact_id}
Submitted on: {$submit}
URL: {$url}
{if $account_id}
	Account: #{$account_id} (username: {$username})
{else}
	Name:{$name}
{/if}
Email:{$email}
{if $phone}
	Phone: {$phone}
{/if}
IP:{$ip_address_label}
{if $device}
	Client device:{$device}
{/if}
Text:{$text}


