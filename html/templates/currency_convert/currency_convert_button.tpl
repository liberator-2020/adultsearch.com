<a href="javascript:void(0);" onclick="CurrencyConvertToggleConverter();"><img src="/images/btn_xe_currency_convert.png" title="Currency conversion" /></a><br />
<div id="xe_currency_convert">
	<a href="#" onclick="CurrencyConvertHideConverter();" class="closer">close</a>
	<a href="#" onclick="CurrencyConvertDisable();" class="closer">disable conversion</a>
	<br style="clear: both; margin-bottom: 5px;"/>
	<table>
		<tbody>
			<tr>
				<td>
					<ul>
					<li data-currency="USD" onclick="CurrencyConvertChangeCurrency(this);" class="highlight">U.S. Dollar</li>
					<li data-currency="EUR" onclick="CurrencyConvertChangeCurrency(this);" class="highlight">Euro</li>
					<li data-currency="GBP" onclick="CurrencyConvertChangeCurrency(this);" class="highlight">Pound Sterling</li>
					<li data-currency="JPY" onclick="CurrencyConvertChangeCurrency(this);" class="highlight">Japanese Yen</li>
					<li data-currency="AED" onclick="CurrencyConvertChangeCurrency(this);">United Arab Emirates Dirhams</li>
					<li data-currency="AUD" onclick="CurrencyConvertChangeCurrency(this);">Australia Dollars</li>
					<li data-currency="BSD" onclick="CurrencyConvertChangeCurrency(this);">Bahamas Dollars</li>
					<li data-currency="CAD" onclick="CurrencyConvertChangeCurrency(this);">Canada Dollars</li>
					<li data-currency="CHF" onclick="CurrencyConvertChangeCurrency(this);">Switzerland Francs</li>
					<li data-currency="CNY" onclick="CurrencyConvertChangeCurrency(this);">China Yuan Renminbi</li>
					</ul>
				</td>
				<td>
					<ul>
					<li data-currency="CZK" onclick="CurrencyConvertChangeCurrency(this);">Czech Republic Koruny</li>
					<li data-currency="HKD" onclick="CurrencyConvertChangeCurrency(this);">Hong Kong Dollars</li>
					<li data-currency="HUF" onclick="CurrencyConvertChangeCurrency(this);">Hungary Forint</li>
					<li data-currency="KHR" onclick="CurrencyConvertChangeCurrency(this);">Cambodia Riels</li>
					<li data-currency="NZD" onclick="CurrencyConvertChangeCurrency(this);">New Zealand Dollars</li>
					<li data-currency="PHP" onclick="CurrencyConvertChangeCurrency(this);">Philippines Pesos</li>
					<li data-currency="RON" onclick="CurrencyConvertChangeCurrency(this);">Romania New Lei</li>
					<li data-currency="TRY" onclick="CurrencyConvertChangeCurrency(this);">Turkey Lira</li>
					<li data-currency="THB" onclick="CurrencyConvertChangeCurrency(this);">Thailand Baht</li>
					<li data-currency="ZAR" onclick="CurrencyConvertChangeCurrency(this);">South Africa Rand</li>
					</ul>
				</td>
			</tr>
		</tbody>
	</table>
</div>
