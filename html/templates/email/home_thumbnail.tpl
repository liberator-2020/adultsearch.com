Hello!<br><br>

We just wanted to let you know that your Homepage Thumbnail has run out of funds and is no longer featured on the front page of your city. But don't worry - you can put it 
back right now! Just click the link below to add more days to your Homepage Thumbnail and get your best assets in front of more viewers. ;)<br><br>

{$link}<br><br>

Have a great day!<br><br>

Sincerely,<br>
support@adultsearch.com<br>
