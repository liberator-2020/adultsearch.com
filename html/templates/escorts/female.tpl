<style type="text/css">
body > div.container {
	background-color: #f6fafd;
	border-radius: 10px;
	border: 2px solid #e2e3e2;
	margin-top: 20px;
	margin-bottom: 20px;
}
#header_row .col_header {
	padding-left: 8px;
	padding-right: 8px;
}
#header_row h1 {
	font-size: 1.8em;
	color: white;
	background-color: #121d38;
	border-radius: 10px 10px 0px 0px;
	border: 0px none;
	margin-top: 12px;
	margin-bottom: 12px;
	padding-top: 12px;
	padding-bottom: 12px;
	padding-left: 35px;
	background-image: linear-gradient(-90deg, #111c36, #21375e, #111c36);
}
.esc_fem_row .col {
	border-bottom: 2px solid #e2e3e2;
	text-align: center;
}
@media (min-width:768px) {
	.esc_fem_row .col:not(:last-child) {
		border-right: 2px solid #e2e3e2;
	}
}
.esc_fem_row .col a, .esc_fem_row .col a:hover {
	display: block;
	height: 130px;
	text-decoration: none;
	color: black;
	padding-top: 5px;
	padding-bottom: 5px;
	margin-top: 10px;
	margin-bottom: 10px;
}
.esc_fem_row img {
	border-radius: 8px;
}
.esc_fem_row .name {
	font-weight: bold;
	font-size: 1.2em;
	color: #266090;
}
#bottom_row {
	text-align: center;
	padding-top: 12px;
	padding-bottom: 12px;
}
#bottom_row .btn.btn-default {
	color: #266090;
	font-weight: bold;
}
</style>

<div class="row" id="header_row">
	<div class="col-xs-12 col_header">	
		<h1>Featured Escorts</h1>
	</div>
</div>
<div class="esc_fem_row" class="row">
	{foreach from=$clads item=c name=clad}
		<div class="col-sm-6 col-xs-12 col">
			<a href="{$c.link}">
			{if $c.thumb}
				<img src="{$config_image_server}/classifieds/{$c.thumb}" alt="{$c.name}" border="0" width="75" height="75" />
			{else}
				<img src="/images/placeholder.gif" alt="AdultSearch" border="0" width="75" />
			{/if}
			<br />
			<span class="name">{$c.firstname}</span><br />
			{$c.location}
			</a>
		</div>
		{if $smarty.foreach.clad.iteration % 2 == 0 && !$smarty.foreach.clad.last}
</div>
<div class="esc_fem_row" class="row">
		{/if}
	{/foreach}
</div>
<br style="clear: both;" />

<div id="bottom_row" class="row">
	<div class="col-xs-12">
		<a class="btn btn-sm btn-default" href="{$link_view_all}">View all escorts</a>
	</div>
</div>
