<div id="homepage">

<div class="anchorLand"><a name="anchorLand"></a></div>
  <!-- blue box -->
  <div id="top">
	<!-- blue box -->
	<div class="roundeddiv">
	  <div class="column_color">
		<div id="blueLeft">
		  <div id="search">
			<form id="city_search" action="/search_location" method="get">
				<span class="selectLg" onclick="document.getElementById('city_search').submit();"></span>
				<input type="text" id="city" name="city" value="" placeholder="Type your city name" autocomplete="off" />
			</form>
		  </div>
		 <!-- international -->
		<div id="countries">
		<div class="title3">AdultSearch International Top Cities</div>
		<div class="countries">
			<li><a href="#australia" title="Australia">Australia</a></li>
			<li><a href="#latin" title="Brazil">Brazil</a></li>
			<li><a href="#europe" title="Belgium">Belgium</a></li>
			<li><a href="#asia" title="Cambodia">Cambodia</a></li>
			<li><a href="#canada" title="Canada">Canada</a></li>
			<li><a href="#asia" title="China">China</a></li>
			<li><a href="#latin" title="Colombia">Colombia</a></li>
			<li><a href="#latin" title="Costa Rica">Costa Rica</a></li>
			<li><a href="#latin" title="Cuba">Cuba</a></li>
			<li><a href="#mideast" title="Cyprus">Cyprus</a></li>
			<li><a href="#europe" title="Czech Republic">Czech Republic</a></li>
			<li><a href="#latin" title="Dominican Republic">Dominican Republic</a></li>
		</div>
		<div class="countries">
			<li><a href="//uae.{$site_domain}/dubai" title="Dubai, United Arab Emirates">Dubai</a></li>
			<li><a href="#uk" title="England">England</a></li>
			<li><a href="#europe" title="Germany">Germany</a></li>
			<li><a href="//greece.{$site_domain}/athens" title="Athens, Greece">Greece</a></li>
			<li><a href="//guam.{$site_domain}/tamuning/erotic-massage/" title="Tamuning, Guam">Guam</a></li>
			<li><a href="#europe" title="Hungary">Hungary</a></li>
			<li><a href="#asia" title="India">India</a></li>
			<li><a href="#asia" title="Indonesia">Indonesia</a></li>
			<li><a href="#uk" title="Ireland">Ireland</a></li>
			<li><a href="#asia" title="Japan">Japan</a></li>
			<li><a href="//macau.{$site_domain}/macau" title="Macau">Macau</a></li>
		</div>
		<div class="countries">
			<li><a href="#asia" title="Malaysia">Malaysia</a></li>
			<li><a href="#latin" title="Mexico">Mexico</a></li>
			<li><a href="#europe" title="Netherlands">Netherlands</a></li>
			<li><a href="#australia" title="New Zealand">New Zealand</a></li>
			<li><a href="//panama.{$site_domain}/panama-city" title="Panama City, Panama">Panama</a></li>
			<li><a href="#asia" title="Philippines">Philippines</a></li>
			<li><a href="#europe" title="Portugal">Portugal</a></li>
			<li><a href="#latin" title="Puerto Rico">Puerto Rico</a></li>
			<li><a href="//romania.{$site_domain}/bucharest" title="Bucharest, Romania">Romania</a></li>
			<li><a href="#europe" title="Russia">Russia</a></li>
			<li><a href="//saintmaarten.{$site_domain}/saintmaarten" title="Saint Maarten">Saint Maarten</a></li>
		</div>
		<div class="countries">
			<li><a href="#uk" title="Scotland">Scotland</a></li>
			<li><a href="//singapore.{$site_domain}/singapore" title="Singapore">Singapore</a></li>
			<li><a href="#africa" title="South Africa">South Africa</a></li>
			<li><a href="#europe" title="Spain">Spain</a></li>
			<li><a href="#europe" title="Switzerland">Switzerland</a></li>
			<li><a href="#asia" title="Thailand">Thailand</a></li>
			<li><a href="#mideast" title="Turkey">Turkey</a></li>
			<li><a href="#europe" title="Ukraine">Ukraine</a></li>
			<li><a href="#uk" title="UK">United Kingdom</a></li>
			<li><a href="#asia" title="Vietnam">Vietnam</a></li>
			<li><a href="#uk" title="Wales">Wales</a></li>
		</div>
		<!-- end white box - countries -->
		</div>
		<!-- /international -->
		
		<div id="post"><a href="https://{$site_domain}/adbuild/">Escorts: Post Your Ad</a></div>
		 
		</div>
		<!-- white box - metros -->
		<div class="roundeddiv2">
		  <div>
			<div class="title2">Adult Search Metropolitan Areas</div>
			<div class="metros">
			<h1><a href="/georgia/atlanta/" title="Atlanta, GA">Atlanta</a></h1>
			<h1><a href="/texas/austin/" title="Austin, TX">Austin</a></h1>
			<h1><a href="/maryland/baltimore/" title="Baltimore, MD">Baltimore</a></h1>
			<h1><a href="/massachusetts/boston/" title="Boston, MA">Boston</a></h1>
			<h1><a href="/illinois/chicago/" title="Chicago, IL">Chicago</a></h1>
			<h1><a href="/texas/dallas/" title="Dallas, TX">Dallas</a></h1>
			<h1><a href="/colorado/denver/" title="Denver, CO">Denver</a></h1>
			<h1><a href="/michigan/detroit/" title="Detroit, MI">Detroit</a></h1>
			<h1><a href="/florida/fort-lauderdale/" title="Fort Lauderdale, FL">Fort Lauderdale</a></h1>
			<h1><a href="/hawaii/honolulu/" title="Honolulu, HI">Honolulu</a></h1>
			</div>
			<div class="metros">
			<h1><a href="/texas/houston/" title="Houston, TX">Houston</a></h1>
			<h1><a href="/nevada/las-vegas/" title="Las Vegas, NV">Las Vegas</a></h1>
			<h1><a href="/california/los-angeles/" title="Los Angeles, CA">Los Angeles</a></h1>
			<h1><a href="/florida/miami/" title="Miami, FL">Miami</a></h1>
			<h1><a href="/minnesota/minneapolis/" title="Minneapolis, MN">Minneapolis</a></h1>
			<h1><a href="/new-york/new-york-city/" title="New York City, NY">New York City</a></h1>
			<h1><a href="/california/orange-county/" title="Orange County, CA">Orange County</a></h1>
			<h1><a href="/pennsylvania/philadelphia/" title="Philadelphia, PA">Philadelphia</a></h1>
			<h1><a href="/arizona/phoenix/" title="Phoenix, AZ">Phoenix</a></h1>
			<h1><a href="/oregon/portland/" title="Portland, OR">Portland</a></h1>
			</div>
			<div class="metros">
			<h1><a href="/missouri/saint-louis/" title="Saint Louis, MO">Saint Louis</a></h1>
			<h1><a href="/california/sacramento/" title="Sacramento, CA">Sacramento</a></h1>
			<h1><a href="/texas/san-antonio/" title="San Antonio, TX">San Antonio</a></h1>
			<h1><a href="/california/san-diego/" title="San Diego, CA">San Diego</a></h1>
			<h1><a href="/washington/seattle/" title="Seattle, WA">Seattle</a></h1>
			<h1><a href="/california/san-francisco/" title="San Francisco Bay Area, CA">SF Bay Area</a></h1>
			<h1><a href="//australia.{$site_domain}/new-south-wales/sydney/" title="Sydney, Australia">Sydney</a></h1>
			<h1><a href="//ca.{$site_domain}/ontario/toronto/" title="Toronto, ON">Toronto</a></h1>
			<h1><a href="//ca.{$site_domain}/british-columbia/vancouver/" title="Vancouver, BC">Vancouver</a></h1>
			<h1><a href="/washington-dc/washington-dc/" title="Washington, DC">Wash DC</a></h1>
			</div>
		  </div>
		</div>
		<!-- /white box -->
	  </div>
	  <div class="clear"></div>
	</div>
	<!--/blue box -->
  </div>
  <!-- /blue box -->
	<br style="clear: both;" />


  <div id="container">
	<div id="main2">

	<h2><a name="us" id="us"></a>UNITED STATES</h2>
	{include "homepage_maingroup.tpl" main_groups=$main_groups main_group_key='us' all_locations=$all_locations}
	<div class="clear"></div>	

	<div id="anchorTop"><a href="#anchorLand">top</a></div>
	<h2><a name="canada" id="canada"></a>Canada</h2>
	{include "homepage_maingroup.tpl" main_groups=$main_groups main_group_key='ca' all_locations=$all_locations}
	<div class="clear"></div>

	<div id="anchorTop"><a href="#anchorLand">top</a></div>
	<h2><a name="uk" id="uk"></a>United Kingdom</h2>
	{include "homepage_maingroup.tpl" main_groups=$main_groups main_group_key='uk' all_locations=$all_locations}
	<div class="clear"></div>

	<div id="anchorTop"><a href="#anchorLand">top</a></div>
	<h2><a name="europe" id="europe"></a> Europe</h2>
	{include "homepage_maingroup.tpl" main_groups=$main_groups main_group_key='europe'}
	<div class="clear"></div>

	<div id="anchorTop"><a href="#anchorLand">top</a></div>
	<h2><a name="latin" id="latin"></a>Latin America &amp; Caribbean</h2>
	{include "homepage_maingroup.tpl" main_groups=$main_groups main_group_key='latin'}
	<div class="clear"></div>

	<div id="anchorTop"><a href="#anchorLand">top</a></div>
	<h2><a name="midleeast" id="midleeast"></a>The Middle East</h2>
	{include "homepage_maingroup.tpl" main_groups=$main_groups main_group_key='middle_east'}
	<div class="clear"></div>

	<div id="anchorTop"><a href="#anchorLand">top</a></div>
	<h2><a name="asia" id="asia"></a>Asia </h2>
	{include "homepage_maingroup.tpl" main_groups=$main_groups main_group_key='asia'}
	<div class="clear"></div>

	<div id="anchorTop"><a href="#anchorLand">top</a></div>
	<h2><a name="africa" id="africa"></a>Africa</h2>
	{include "homepage_maingroup.tpl" main_groups=$main_groups main_group_key='africa'}
	<div class="clear"></div>

	<div id="anchorTop"><a href="#anchorLand">top</a></div>
	<h2><a name="australia" id="australia"></a>Australia and Oceania</h2>
	{include "homepage_maingroup.tpl" main_groups=$main_groups main_group_key='australia'}
	<div class="clear"></div>

	<div id="anchorTop"><a href="#anchorLand">top</a></div>
	<h2><a name="other" id="other"></a> Other countries</h2>
	{include "homepage_maingroup.tpl" main_groups=$main_groups main_group_key='other'}
	<div class="clear"></div>

	<h3 style="text-align: left;"><a href="/all_locations" rel="nofollow">All locations</a></h3>
</div>

</div>  
<!-- / #container -->

</div>
<!-- homepage -->

{literal}
<script type="text/javascript">
$(document).ready(function() {
	$('a.vm').each(function() {
		$(this).click(function() {
			var ul = $(this).closest('ul');
			if (!ul)
				return false;
			var si = ul.attr('data-state-id');
			if (!si)
				return false;
			$.ajax({type: 'GET', url: '/_ajax/home_locs_in_state', data: 's='+si, success: function(msg){ ul.html(msg); }});
			return false;
		});
	});
});
</script>
{/literal}

