{* $main_groups array, $main_group_key string and $all_locations bool passed from parent template homepage.tpl *}

{assign var='main_group' value=$main_groups.$main_group_key}

{foreach $main_group as $col}
	<div class="content_col geoUnit">
	{foreach $col as $state}
		{if $main_group_key == "us" || $main_group_key == "ca" || $main_group_key == "uk"}
			<h3><a href="{$state.state_url}" title="{$state.state_name}">{$state.state_name}</a></h3>
		{else}
			<h3>{$state.state_name}</h3>
		{/if}
		<ul data-state-id="{$state.state_id}">
		{foreach $state.cities as $city}
			<li><a href="{$city.loc_url}" title="{$city.title}" {if $all_locations}rel="nofollow"{/if}>{$city.loc_name}</a></li>
		{/foreach}
		{if !$all_locations && ( $main_group_key == "us" || $main_group_key == "ca" || $main_group_key == "uk" ) }
			<li class="more"><a href="#" class="vm" title="View more cities in {$state.state_name}" rel="nofollow">View more...</a></li>
		{/if}
		</ul>
	{/foreach}
	</div>
{/foreach}
