<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$title}</title>
<link rel="icon" type="image/png" href="/favicon.ico">
<meta name="description" content="{$description}" />
{if $keywords}<meta name="keywords" content="{$keywords}" />{/if}

{if $canonical_url}<link rel="canonical" href="{$canonical_url}"/>{/if}
{if $canonical}<link rel="canonical" href="{$canonical}" />{/if}

{if $config_env && $config_env != 'prod'}
<!-- for all non-prod environments, export config_site_url to javascript var -->
<script type="text/javascript">
var config_site_url = '{$config_site_url}';
</script>
{/if}
{if $jquery_new}
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
{else}
	<script type="text/javascript" src="{$config_site_url}/js/jquery.js?183"></script>
	<script type="text/javascript" src="{$config_site_url}/js/tools/fancybox134/jquery.fancybox-1.3.4.pack.js"></script>
{/if}
{if $account_level>2}<script type="text/javascript" src="{$config_site_url}/js/_mng.js"></script>{/if}
<link href="/css/wrapper.css?20190805" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{$config_site_url}/js/ui/jquery-ui-1.9.1.custom.min.js"></script>
<link href="{$config_site_url}/js/ui/start/jquery-ui-1.9.1.custom.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{$config_site_url}/js/main.js?20190725"></script>

{if $is_super_admin}
	<script type="text/javascript" src="/js/tippy.js"></script>
{/if}

<link href="{$config_site_url}/css/as16.css?20190801" rel="stylesheet" type="text/css" />
{$html_head_include}

{if $jquery_file_upload}
	<link rel="stylesheet" href="/css/jquery_file_upload/jquery.fileupload.css">
	<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
	<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.5.2/jquery.iframe-transport.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.5.2/jquery.fileupload.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.5.2/jquery.fileupload-process.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.5.2/jquery.fileupload-image.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/blueimp-file-upload/9.5.2/jquery.fileupload-validate.min.js"></script>
{/if}
{if $flowplayer}
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flowplayer/6.0.5/skin/minimalist.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/flowplayer/6.0.5/flowplayer.min.js"></script>
{/if}

{if !$config_dev_server}
	{include "google_analytics.tpl"}
{/if}
{if $isworker}
	<script>
		window.isWorker      = true;
		window.directCallUrl = '{$config_direct_call}';
	</script>
{/if}
</head>
<body class="{if $page_type == "landing"}homepage {/if}">

<div id="logoModule">
	<div id="header">
		<div id="topnav">
			<ul class="left">
				<a href="{if $ts_city_link}{$ts_city_link}{else}https://www.tsescorts.com{/if}" target="_blank" title="{$ts_link_title}">{$ts_link_label}</a>
				{if $backlink}
					<a href="{$backlink.url}" target="_blank" title="{$backlink.hover}">{$backlink.label}</a>
				{/if}
				{$navlink_html}
			</ul>
			{if !$ismember}
				<ul class="navlogin notlogged">
					<a href="/account/signup" class="leftborder navbar_signup" rel="nofollow">Sign Up</a>
					<a href="/account/signin?origin={$request_uri}" rel="nofollow">Member Login</a>
					<a href="/adbuild/" title="Advertise on AdultSearch.com">Escort&nbsp;Advertising</a>
				</ul>
			{else}
				<ul class="navlogin">
					{$smarty.session.username}
					<ul>
						<li><a href="#" class="leftborder" onclick="return false;">My Account</a>
						<ul>
							{if $smarty.session.classified_poster}<li><a href="/classifieds/myposts">My Classified Ads</a></li>{/if}
							<li><a href="/account/mypage">My Profile</a></li>
							<li><a href="/pm/">My Private Messages{if $unread_messages > 0} <span style="color: orange; font-weight: bold;">({$unread_messages})</span>{/if}</a></li>
							{if permission::has("access_admin")}
								<li><a href="/mng/home">Manage</a></li>
							{/if}
							{if $smarty.session.worker||$account_level==3}
								<li><a href="/mng/cl_report">CL Stats</a></li>
							{/if}
							{if $advertiser || $account_level == 3 || permission::has("advertise_manage")}
								<li><a href="/advertise/{if $account_level > 2}dashboard{/if}">Advertising</a></li>
							{/if}
							{if $smarty.session.account_level > 2}
								<!-- {$smarty.session.account_level} {$jay} -->
								{if !$smarty.session.templates}
									<li><a href="?templates=jay" data-ajax="false">.jay templates</a></li>
								{else}
									<li><a href="?templates=" data-ajax="false">Standard templates</a></li>
								{/if}
							{/if}
							{if account::is_impersonated() }
								<li><a href="/account/impersonate_return">Return to admin</a></li>
							{/if}
							<li><a href="/?logoff=1&redirect={$request_uri}">Logout</a></li>
						</ul>
						</li>
						<li><a href="/adbuild/" title="Advertise on AdultSearch.com">Escort Advertising</a></li>
					</ul>
				</ul>
			{/if}
		</div>

		{if $page_type == "landing"}

		{*
		<div class="clear promo" id="slide">
			<li>Over 100,000 escorts in 20+ countries</li>
			<li>Over 5,000 erotic massage parlors &amp; reviews</li>
			<li>Over 4,000 strip clubs &amp; reviews</li>
			<li>Over 5,000 sex shops &amp; reviews</li>
			<li>Thousands of international sex tourism destinations</li>
		</div>
		*}

		{else}

		<div id="logoHome"> <a href="{$homelink}/" title="Adult Search Engine"></a> </div>

		{*
		<!-- headCat -->
		<div id="headCat">
		{if $qslinks}
			<span class="stretch">{$loc_name}</span>
			<form method="post" action="/search" name="hdrsrch">
				<input name="btnG" src="{$config_site_url}/images/selectGoSilver.png" type="image" id="headGo" />
				<select name="section" onchange="if(this.options[this.selectedIndex].value=='webcam') document.hdrsrch.target='_target'; else document.hdrsrch.target='_top';">
				{section name=qs loop=$qslinks}
				{assign var=qs value=$qslinks[qs]}
					<option value="{$qs.link}"{if $qs.selected} selected="selected"{/if}>{$qs.title}</option>
				{/section}
				</select>
			</form>
		{elseif !$clean_header&&$cur_loc_url&&!$international}<span class="stretch">{$cur_loc_name}{if $cur_loc_s}, {$cur_loc_s}{/if}</span>
		  <form method="post" action="/search" name="hdrsrch">
			<input type="hidden" name="url" value="{$cur_loc_url}"/>
		   <input name="btnG" src="{$config_site_url}/images/selectGoSilver.png" type="image" id="headGo" />
	<select name="section" onchange="if(this.options[this.selectedIndex].value=='webcam') document.hdrsrch.target='_target'; else document.hdrsrch.target='_top';">
			{if $quickcategory}
			   {section name=qc loop=$quickcategory}{assign var=qc value=$quickcategory[qc]}
					<option value="{$qc.section}"{if $qc.selected} selected="selected"{/if}>{$qc.title}</option>
			   {/section}
			{else}
			<option value="as"{if $section=="as"} selected="selected"{/if}>Sex Shops</option>
			<option value="sc"{if $section=="sc"} selected="selected"{/if}>Strip Clubs</option>
			<option value="ls"{if $section=="ls"} selected="selected"{/if}>Life Style Clubs</option>
			<option value="emp"{if $section=="emp"} selected="selected"{/if}>Erotic Massage Parlors</option>
			<option value="cl1"{if $section=="cl1"} selected="selected"{/if}>Female Escorts</option>
			{/if}
			</select>
		  </form>
		{/if} 
		</div>
		*}

		<!-- /headCat -->
		{if $cur_loc_url}
			<div class="changeBlock">
				<div id="change">
					<form action="/search_location" method="get">
					<input name="btnG" src="{$config_site_url}/images/selectGoSilver2.png" type="image" id="headGo2" />
					<input name="city" id="city" class="locationHead" value="" placeholder="Type City Name" autocomplete="off" style="border: 2px solid #eee;"/>
					<div class="clear">
						<a href="/homepage" title="Adult Search Engine">All Cities &amp; Countries &raquo;</a> 
					</div>
					</form>
				</div>
			</div>

			<a id="current" title="Adult Search Engine">
				{$cur_loc_name}{if $cur_loc_s}, {$cur_loc_s}{/if}<br />
			</a>
		{/if}

		{/if}

	</div>
</div>

<div id="content">
	<div class="breadcrumbs">
		{if $account_level>1 && $emp_city_link}
		<div style="float: right; margin: 1px 10px 0px 0px;">
			<a href="{$emp_city_link}" title="Erotic Massage Parlors in {$cur_loc_name}" style="color: #186299; font-weight: bold;" target="_new">Erotic Massage Parlors</a>
		</div>
		{/if}
		
		{if $breadcrumb}
			{$breadcrumb}
		{else}
			{if $cur_loc_url}<a href="{$config_site_url}/homepage" title="Adult Search Engine">All Locations</a> &raquo;
					{if $cur_lp_loc_name}
					{if $cur_lp_loc_url}
						<a href="{$cur_lp_loc_url}" title="{$cur_lp_loc_name}">{$cur_lp_loc_name}</a>
					{else}
						{$cur_lp_loc_name}
					{/if} 
					&raquo;
				{/if} 
				<a href="{$cur_loc_url}" title="{$cur_loc_name}">{$cur_loc_name}</a>
				{if $breadcat} &raquo; {$breadcat}{/if}
				{if $breadplace} &raquo; <a href="" title="{$breadplace}" class="bold">{$breadplace}</a>{/if}
			{/if}
		{/if}
		{if $is_super_admin && $cur_loc_id}
			({$cur_loc_id})  
		{/if}
		{if $account_level==3&&$newemail} 
			<span id="mng_contact"><a class="alert" href="/mng/">&nbsp;*{$newemail} E-mail*</a></span> 
		{/if}
		{if $account_level > 2 && $unread_messages > 0}
			&nbsp;<a class="pm_alert" href="/pm/">{$unread_messages} Unread Message{if $unread_messages > 1}s{/if}</a>
		{/if}
		{if $account_level > 0 && $cur_loc_id}
			{if $place_type_id}
				&nbsp;<a class="pm_alert" href="/seo/edit?location_id={$cur_loc_id}&place_type_id={$place_type_id}">Add/Edit SEO</a>
			{elseif $type}
				&nbsp;<a class="pm_alert" href="/seo/edit?location_id={$cur_loc_id}&category={$type}">Add/Edit SEO</a>
			{/if}
		{/if}
	</div>

	{if $flash_msg}
		<br style="clear: both;" />
		{$flash_msg}
	{/if}

	{$content}
</div>

<br style="clear: both;" />
  
<div id="footerx"{if $nobanner} class="nobanner"{/if}>
	{if !$nobanner && !$config_dev_server}
		<br />
		<center>
			<script src="{$config_site_url}/js/publisher/p.js?20180922" type="text/javascript"></script>
			<!-- {$type} -->
			{if $type == 2}
				<script type="text/javascript">as_show_banner(10096, 728, 90);</script>
			{else}
				<script type="text/javascript">as_show_banner(10074, 728, 90);</script>
			{/if}
		</center>
	{/if}

	<div id="footer_city_links">
		<a href="{$config_site_url}/new-mexico/albuquerque/" title="Albuquerque, NM">Albuquerque</a>, 
		<a href="https://philippines.adultsearch.com/angeles-city/" title="angeles City, PH">Angeles City</a>, 
		<a href="{$config_site_url}/maryland/annapolis/" title="Annapolis, MD">Annapolis</a>, 
		<a href="{$config_site_url}/georgia/atlanta/" title="Atlanta, GA">Atlanta</a>, 
		<a href="{$config_site_url}/texas/austin/" title="Austin, TX">Austin</a>, 
		<a href="{$config_site_url}/maryland/baltimore/" title="Baltimore, MD">Baltimore</a>, 
		<a href="{$config_site_url}/montana/billings/" title="Billings, MT">Billings</a>, 
		<a href="{$config_site_url}/massachusetts/boston/" title="Boston, MA">Boston</a>, 
		<a href="{$config_site_url}/new-york/buffalo/" title="Buffalo, NY">Buffalo</a>, 
		<a href="{$config_site_url}/illinois/chicago/" title="Chicago, IL">Chicago</a>, 
		<a href="{$config_site_url}/texas/dallas/" title="Dallas, TX">Dallas</a>, 
		<a href="{$config_site_url}/colorado/denver/" title="Denver, CO">Denver</a>, 
		<a href="{$config_site_url}/michigan/detroit/" title="Detroit, MI">Detroit</a>, 
		<a href="https://uae.adultsearch.com/dubai" title="Dubai, UAE">Dubai</a>, 
		<a href="{$config_site_url}/connecticut/hartford/" title="Hartford, CT">Hartford</a>, 
		<a href="{$config_site_url}/hawaii/honolulu/" title="Honolulu, HI">Honolulu</a>, 
		<a href="{$config_site_url}/texas/houston/" title="Houston, TX">Houston</a>, 
		<a href="{$config_site_url}/florida/jacksonville/" title="Jacksonville, FL">Jacksonville</a>, 
		<a href="{$config_site_url}/missouri/kansas-city/" title="Kansas City, MO">Kansas City</a>, 
		<a href="https://malaysia.adultsearch.com/kuala-lumpur" title="Kuala Lumpur, MY">Kuala Lumpur</a>, 
		<a href="{$config_site_url}/nevada/las-vegas/" title="Las Vegas, NV">Las Vegas</a>, 
		<a href="{$config_site_url}/arkansas/little-rock/" title="Little Rock, AR">Little Rock</a>, 
		<a href="https://uk.adultsearch.com/england/london/" title="London, UK">London</a>, 
		<a href="{$config_site_url}/california/long-beach/" title="Long Beach, CA">Long Beach</a>, 
		<a href="{$config_site_url}/california/los-angeles/" title="Los Angeles, CA">Los Angeles</a>, 
		<a href="{$config_site_url}/florida/miami/" title="Miami, FL">Miami</a>, 
		<a href="{$config_site_url}/wisconsin/milwaukee/" title="Milwaukee, WI">Milwaukee</a>, 
		<a href="{$config_site_url}/minnesota/minneapolis/" title="Minneapolis, MN">Minneapolis</a>, 
		<a href="{$config_site_url}/louisiana/new-orleans/" title="New Orleans, LA">New Orleans</a>, 
		<a href="{$config_site_url}/new-york/new-york-city/" title="New York City, NY">New York City</a>, 
		<a href="{$config_site_url}/oklahoma/oklahoma-city/" title="Oklahoma City, OK">Oklahoma City</a>, 
		<a href="{$config_site_url}/california/orange-county/" title="Orange County, CA">Orange County</a>, 
		<a href="{$config_site_url}/florida/orlando/" title="Orlando, FL">Orlando</a>, 
		<a href="https://thailand.adultsearch.com/pattaya/" title="Pattaya, TH">Pattaya</a>, 
		<a href="{$config_site_url}/pennsylvania/philadelphia/" title="Philadelphia, PA">Philadelphia</a>, 
		<a href="{$config_site_url}/arizona/phoenix/" title="Phoenix, AZ">Phoenix</a>, 
		<a href="{$config_site_url}/oregon/portland/" title="Portland, OR">Portland</a>, 
		<a href="{$config_site_url}/nevada/reno/" title="Reno, NV">Reno</a>, 
		<a href="{$config_site_url}/missouri/saint-louis/" title="Saint Louis, MO">Saint Louis</a>, 
		<a href="{$config_site_url}/california/sacramento/" title="Sacramento, CA">Sacramento</a>, 
		<a href="{$config_site_url}/texas/san-antonio/" title="San Antonio, TX">San Antonio</a>, 
		<a href="{$config_site_url}/california/san-diego/" title="San Diego, CA">San Diego</a>, 
		<a href="{$config_site_url}/california/san-jose/" title="San Jose, CA">San Jose</a>, 
		<a href="{$config_site_url}/washington/seattle/" title="Seattle, WA">Seattle</a>, 
		<a href="{$config_site_url}/california/san-francisco/" title="San Francisco Bay Area, CA">SF Bay Area</a>, 
		<a href="https://china.adultsearch.com/shanghai" title="Shanghai, CN">Shanghai</a>, 
		<a href="https://singapore.adultsearch.com/singapore/" title="Singapore, SG">Singapore</a>, 
		<a href="{$config_site_url}/florida/tampa/" title="Tampa, FL">Tampa</a>, 
		<a href="https://ca.{$site_domain}/ontario/toronto/" title="Toronto, ON">Toronto</a>, 
		<a href="{$config_site_url}/oklahoma/tulsa/" title="Tulsa, OK">Tulsa</a>, 
		<a href="https://ca.{$site_domain}/british-columbia/vancouver/" title="Vancouver, BC">Vancouver</a>, 
		<a href="{$config_site_url}/washington-dc/washington-dc/" title="Washington, DC">Washington DC</a>
		<br />
	</div>
	
	<div id="footer_content">
		<a href="{$config_site_url}/affiliate/" class="o" title="Affiliates Sign Up">Affiliates</a> |
		<a href="{$config_site_url}/advertise/" class="o" title="Advertising">Advertising</a> |
		<a title="Privacy Policy" href="/privacy-policy">Privacy Policy</a> |
		<a title="Law Enforcement" href="/law-enforcement">Law Enforcement</a> |
		<a title="Report Trafficking" href="/report-trafficking">Report Trafficking</a> |
		<a href="/contact?where={$request_uri}">Contact Us</a> |
		<a title="adult search - business owners" href="/businessowner/">Business Owners</a> |
		<a title="Our Banners" href="/our-banners">Grab Our Banner</a> |
		<a rel="nofollow" target="_blank" title="Protect Children Online" href="http://www.asacp.org/">RCA</a><br />
	</div>
</div>

{if $totaltime}
	<br/>
	<div style="text-align:center">
		Page processing time: <b>{$totaltime}</b>,
		Host: {$hostname}, 
		<a href="#" onclick="$('#debug_sqls').toggle(); window.scrollTo(0,document.body.scrollHeight); return false;">Total sqls: <b>{$total_sql}</b></a>, 
		SQL time: <b>{$total_time}</b>, 
		pages_visited: {$pages_visited}, 
		page_type={$page_type},
		geolocation={$geolocation}, 
		popup_decision={$popup_decision}
		<table id="debug_sqls" style="display:none;">
		<tr><th>SQL</th><th>Time in seconds</th></tr>
		{foreach from=$sqls item=sql}
			<tr><td style="text-align: left; max-width: 1400px;">{$sql.sql}</td><td style="text-align: right;">{$sql.time|number_format:4}</td></tr>
		{/foreach}
		</table>
	</div>
{/if}

{if $warning}
	{include "warning.tpl"}
{/if}

{if !$config_dev_server && ($popup || $popup_url)}
<script type="text/javascript">
{literal}
_popunderSettings = { "url": {/literal}{if $popup}'/advertise/popup?sid={$popup}'{else}'{$popup_url}'{/if}{literal} }
$(document).ready(_PopunderMobile.attach);
{/literal}
</script>
{else}
<!-- no as popup -->
{/if}

{if $interstitial_img_src && $interstitial_cs_id}
	<!-- img_src = {$interstitial_img_src}, cs_id = {$interstitial_cs_id} -->
	{include "interstitial.tpl" interstitial_img_src=$interstitial_img_src interstitial_cs_id=$interstitial_cs_id}
{else}
	<!-- no as interstitial -->
{/if}

<script type="text/javascript">
<!--
jQuery(function() {
	jQuery("#city").autocomplete({
		source: function(request, response) {
			jQuery.ajax({
				url: "/_ajax/city",
				dataType: "json",
				data: {
					search_string: request.term
				},
				success: function(data) {
					response(jQuery.map(data, function(item) {
						return {
							url: item.url,
							value: item.name
						}
					}))
				}
			})
		},
		create: function() {
			$(this).attr("autocomplete", "off");
		},
		focus: function() {
			$(this).attr("autocomplete", "off");
		},
		search: function() {
			$(this).attr("autocomplete", "off");
		},
		select: function( event, ui ) {
			window.location.href = ui.item.url;
		},
		minLength: 2
	});
});
-->
</script>

{$html_bottom_include}

{if !$config_dev_server && $ismember && !$isworker && !$isadmin}
	{include "jivochat.tpl"}
{/if}

{if !$config_dev_server && !$isworker && !$isadmin}
	<script src="/js/retentioneering.js" type="text/javascript"></script>
{/if}

</body>
</html>

