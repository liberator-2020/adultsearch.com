<link href="/css/landing/styles.css" rel="stylesheet" type="text/css"/>

<div class="landing-body landing{$pageId}">
    <div class="landing-container container">
        <h1 class="landing-title">
            Find a local escort
            in your city!
        </h1>
        <div class="profiles-container">
            {foreach from=$classifieds item=classified}
                <a href="{$classified['profile_url']}" class="profile" data-ajax="false">
                    <img src="{$classified['photo']}" class="background" alt="photo">
                    <div class="info">
                        <p class="name">{$classified['name']}</p>
                        <p class="location">{$classified['location']}</p>
                    </div>
                </a>
            {/foreach}
        </div>
        <div class="location-url-container">
            <a href="{$urlCity}" data-ajax="false">View All Escorts</a>
        </div>
		<div style="width: 100%; text-align: center; background-color: rgba(0,0,0,0.3); margin-top: 20px; text-shadow: none; color: #aaa; border-radius: 6px;">
			&copy; Adultsearch.com 2019
		</div>
    </div>
</div>
