<h1>Law Enforcement</h1>

<p>
Adultsearch has always been adamantly against illegal prostitution, sex trafficking, and all forms of child abuse worldwide. We only want adults that want to be here for entertainment fantasies and lawful activity.  As we have stated on our website, sex traffickers, illegal prostitutes, pedophiles and child abusers are not welcome on the Adultsearch.com website.  In any effort to curtail these activities, Adultsearch.com voluntarily works with law enforcement to provide them with information regarding any alleged illegal activity and Adultsearch.com immediately removes any posts referring or relating to alleged illegal activity once notified of such by law enforcement.  Any law enforcement officer may email us at <img src="/images/email_notrafficking.png" style="width: 180px; height: 18px; vertical-align: middle;"/> for information.  We will usually get back to you within 2 business days.
</p>
<h2>Subpoena Process</h2>
<p>
That being said, Adultsearch.com is based in The Netherlands.  While we are willing to voluntarily work with law enforcement to provide them with information quickly and efficiently, we do not accept foreign subpoenas directly, nor any service of process, from jurisdictions outside of The Netherlands.  We never accept ANY service of process via e-mail.
</p>
<p>
If you are in North America, South America, Africa, Asia, Antarctica or Australia/Oceana, and your country is a signatory to the Hague Convention, then you need to have a Letters Rogatory prepared in both your native language and in Dutch (along with a an affidavit signed before a notary by your translator), then signed by a judge in your jurisdiction, and forwarded to your state department.  Your state department then needs to forward the Letters Rogatory to The Hague, so that it may be forwarded to the state department of The Netherlands, and served in accordance with Dutch law.  You should check with the state department of your individual country for confirmation its processes.  Please note that the time frame is generally 12-24 months (depending on the state departments and the Hague), and we would prefer to receive notice via the email address listed supra, so that we can help you quickly and effectively stop any illegal activity. 
</p>
<p>
If you are in Europe in a jurisdiction that is part of the EU, then we will accept service of process to the address in the Dutch population register (BRP) in accordance with the EU Directive and Regulation (EC) No. 1393/2007. Please note that the time frame is generally 6-18 months, and we would prefer to receive notice via the email address listed supra, so that we can help you quickly and effectively stop any illegal activity.
</p>

