<style type="text/css">
</style>

<script type="text/javascript">
function email_text_toggle() {
	if ($('#u_send_email').is(':checked'))
		$('#email_text_div').show();
	else
		$('#email_text_div').hide();
}
$(document).ready(function() {
	$('#u_send_email').change(function() {
		console.log('a');
		email_text_toggle();
	});
	email_text_toggle();
});
</script>

<h1>Delete account #{$id} - {$email}</h1>
<form method="post" action="">
<table class="table table-striped table-condensed">
<tbody>
<tr><th width="150">Account ID</th><td>{$id}</td></tr>
<tr><th>Email</th><td>{$email}</td></tr>
<tr><th>Reason</th><td><input type="text" name="u_reason" value="{$reason}" style="width: 100%;" /></td></tr>
<tr>
	<th>Email</th>
	<td>
		<div class="checkbox">
			<label>
				<input type="checkbox" name="u_send_email" id="u_send_email" {if $send_email}checked="checked"{/if} value="1" />
				Send notification email to user
			</label>
		</div>
		<div id="email_text_div">
		Email text:<br />
		<textarea name="u_email_text" rows="5" cols="80">{$email_text}</textarea>
		</div>
	</td>
</tr>
<tr>
	<th></th>
	<td>
		{if $cnt_active_subscriptions > 0}
			<div class="alert alert-danger">
				This account has active subscriptions, please cancel these first before deleting account!<br />
				<a href="/mng/sales?account_id={$id}" target="_blank">View subscriptions</a><br />
				<button type="submit" name="reload" class="btn btn-sm btn-default">Reload</button>
			</div>
		{/if}
		{if $cnt_active_ads > 0}
			<div class="alert alert-danger">
				This account has active ads, please expire/delete these first before deleting account!<br />
				<a href="/mng/classifieds?account_id={$id}" target="_blank">View ads</a><br />
				<button type="submit" name="reload" class="btn btn-sm btn-default">Reload</button>
			</div>
		{/if}
		{if $cnt_active_subscriptions == 0 && $cnt_active_ads == 0}
			<button type="submit" name="submit" class="btn btn-sm btn-danger">Delete</button>
		{/if}
		<button type="button" onclick="javascript:history.go(-1);" class="btn btn-sm btn-default">Go back</button>
	</td>
</tr>
</tbody>
</table>
<input type="hidden" name="action" value="delete" />
<input type="hidden" name="action_id" value="{$id}" />
{if $contact_id}
	<input type="hidden" name="contact_id" value="{$contact_id}" />
{/if}
{$html_hidden_fields}
</form>

