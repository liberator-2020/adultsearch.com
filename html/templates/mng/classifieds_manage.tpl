{if $videos}
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/flowplayer/6.0.5/flowplayer.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flowplayer/6.0.5/skin/minimalist.css">
{/if}
<style type="text/css">
table.clad_manage td {
	vertical-align: top;
}
table.clad_manage legend {
	display: inline;
	width: auto;
	font-size: 1em;
	padding: 2px;
	border-bottom: 0px none;
	color: #111;
	font-size: 1em;
	margin-bottom: 0px;
}
fieldset {
	border: 1px solid #aaa;
	margin-left: 0px;
	padding: 5px;
}
fieldset > select, fieldset > input, fieldset > label {
	margin-bottom: 3px;
}
button:not([disabled]) {
	cursor: pointer;
}
.changed {
	background-color: rgb(255, 255 ,142);
}
div.clvideo_div {
	background-color: #777;
}

div.climg_div {
	width: 100px;
	display: inline-block;
	background-color: #ddd;
	border: 1px solid #ccc;
}
img.climg {
	max-width: 80px; 
	max-height: 80px;
	border: 1px solid #ccc;
}
label {
	display: inline-block;
	padding: 3px;
}
span.help {
	display: inline-block;
	margin-top: 1px;
	cursor: pointer;
	border: 1px solid #A6C9FF;
	border-radius: 2px;
	background-color: #E4EEFF;
	padding: 1px 2px 1px 2px;
}
span.help > span {
	display: none;
}
</style>

<script type="text/javascript">

var data_changed = false;
var url_refresh = '{$url_refresh}';
var url_view = '{$url_view}';
var url_list = '{$url_list}';

{literal}
function refresh(force = false) {
	var answer = true;
	if (!force && data_changed)
		answer = confirm('You have changed data. Are you sure you want to refresh page and discard changes made ?');
	if (answer)
		window.location.href = url_refresh;
}

function view() {
	var answer = true;
	if (data_changed)
		answer = confirm('You have changed data. Are you sure you want to view ad and discard changes made ?');
	if (answer)
		//window.location.href = url_view;
		window.open(url_view,'_blank');
}

function list() {
	var answer = true;
	if (data_changed)
		answer = confirm('You have changed data. Are you sure you want to view classifieds manage list page and discard changes made ?');
	if (answer)
		window.location.href = url_list;
}

function mark_changed() {
	data_changed = false;
	$('*[data-original-value]').each(function() {
		if ($(this).attr('type') == 'checkbox') {
			var curr = $(this).prop('checked') ? 1 : 0;
		} else {
			var curr = $(this).val();
		}
		var orig =  $(this).attr('data-original-value');
		if (curr != orig) {
			data_changed = true;
			if ($(this).attr('type') == 'checkbox') {
				$(this).parent('label').addClass('changed');
			} else {
				$(this).addClass('changed');
			}
		} else {
			if ($(this).attr('type') == 'checkbox') {
				$(this).parent('label').removeClass('changed');
			} else {
				$(this).removeClass('changed');
			}
		}
	});
	if (data_changed) {
		$('.btn_save').each(function() {$(this).removeAttr('disabled');});
		$('.btn_save_next').each(function() {$(this).removeAttr('disabled');});
	} else {
		$('.btn_save').each(function() {$(this).attr('disabled', 'disabled');});
		$('.btn_save_next').each(function() {$(this).attr('disabled', 'disabled');});
	}
	//alert('data_changed:'+data_changed);
}

function field_changed(elem) {
	if (! $(elem).attr('data-original-value'))
		return;
	mark_changed();
}

function whitelist() {
	var jqxhr = $.post('/_ajax/account_whitelist?account_id='+{/literal}{$account_id}{literal}, function(data) {
		console.log('data='+data);
		if (data == 'OK') {
			$('#whitelisted').html('<span style="color: green; font-weight: bold;">YES</span> <a id="btn_unwhitelist" href="#">Unwhitelist</a>');
			$('#btn_unwhitelist').click(function() { return unwhitelist();});
		} else {
			alert('Error whitelisting account, please contact administrator!');
		}
	});
	return false;
}

function unwhitelist() {
	var jqxhr = $.post('/_ajax/account_unwhitelist?account_id='+{/literal}{$account_id}{literal}, function(data) {
		console.log('data='+data);
		if (data == 'OK') {
			$('#whitelisted').html('NO <a id="btn_whitelist" href="#">Whitelist</a>');
			$('#btn_whitelist').click(function() { return whitelist();});
		} else {
			alert('Error unwhitelisting account, please contact administrator!');
		}
	});
	return false;
}

function rotate(image_id, direction) {
	var jqxhr = $.post('/_ajax/clad_image_rotate?id='+image_id+'&direction='+direction, function(data) {
		console.log('data='+data);
		if (data == 'OK') {
			alert('ok');
		} else {
			alert('Error rotating image, please contact administrator!');
		}
	});
	return false;
}

function ct_add(btn) {
	var loc_id = $(btn).attr('data-loc-id');
	var until = $('#expires_date').val();
	console.log('adding ct for loc_id = '+loc_id);
	var jqxhr = $.post('/_ajax/classifieds_ct?id='+{/literal}{$id}{literal}+'&action=ct_add&loc_id='+loc_id+'&until='+until, function(data) {
		console.log('data='+data);
		if (data.status == 'success') {
			var loc_name = $(btn).attr('data-loc-name');
			$('#ct').html(loc_name+' - live - expires '+until+'<br />');
		} else {
			alert('Error adding city thumbnail: '+data.reason);
		}
	});
	return false;
}

function ct_can(btn) {
	var ct_id = $(btn).attr('data-ct-id');
	console.log('cancelling ct '+ct_id);
	var jqxhr = $.post('/_ajax/classifieds_ct?id='+{/literal}{$id}{literal}+'&action=ct_can&ct_id='+ct_id, function(data) {
		console.log('data='+data);
		if (data.status == 'success') {
			refresh(true);
		} else {
			alert('Error cancelling city thumbnail: '+data.reason);
		}
	});
	return false;
}

function ct_chl(btn) {
	var ct_id = $(btn).attr('data-ct-id');
	var new_loc_id = $(btn).attr('data-new-loc-id');
	console.log('changing ct '+ct_id+' to loc_id '+new_loc_id);
	var jqxhr = $.post('/_ajax/classifieds_ct?id='+{/literal}{$id}{literal}+'&action=ct_chl&new_loc_id='+new_loc_id+'&ct_id='+ct_id, function(data) {
		console.log('data='+data);
		if (data.status == 'success') {
			$('#ct').html(data.loc_name+' - live - expires '+data.until+'<br />');
		} else {
			alert('Error changing location for city thumbnail: '+data.reason);
		}
	});
	return false;
}


function ss_add(btn) {
	var loc_id = $(btn).attr('data-loc-id');
	//var until = $('#expires_date').val()+'_'+$('#expires_hour').val()+':'+$('#expires_minute').val();
	var until = $('#expires_date').val();
	console.log('adding ss for loc_id = '+loc_id);
	var jqxhr = $.post('/_ajax/classifieds_ss?id='+{/literal}{$id}{literal}+'&action=ss_add&loc_id='+loc_id+'&until='+until, function(data) {
		console.log('data='+data);
		if (data.status == 'success') {
			var loc_name = $(btn).attr('data-loc-name');
			$('#ss').html(loc_name+' - live - expires '+until+'<br />');
		} else {
			alert('Error adding side sponsor: '+data.reason);
		}
	});
	return false;
}

function st_add(btn) {
	var loc_id = $(btn).attr('data-loc-id');
	var until = $('#expires_date').val();
	var days = $(btn).data('days');
	console.log('adding st for loc_id = '+loc_id);
	var jqxhr = $.post('/_ajax/classifieds_st?id='+{/literal}{$id}{literal}+'&action=st_add&loc_id='+loc_id+'&until='+until+'&days='+days, function(data) {
		if (data.status == 'success') {
			var loc_name = $(btn).attr('data-loc-name');
			$('#st').html(loc_name+' - live - expires '+data.until+'<br />');
		} else {
			alert('Error adding sticky sponsor: '+data.reason);
		}
	});
	return false;
}

function ss_chl(btn) {
	var ss_id = $(btn).attr('data-ss-id');
	var new_loc_id = $(btn).attr('data-new-loc-id');
	console.log('changing ss '+ss_id+' to loc_id '+new_loc_id);
	var jqxhr = $.post('/_ajax/classifieds_ss?id='+{/literal}{$id}{literal}+'&action=ss_chl&ss_id='+ss_id+'&new_loc_id='+new_loc_id, function(data) {
		console.log('data=',data);
		if (data.status == 'success') {
			$('#ss').html(data.loc_name+' - live - expires '+data.until+'<br />');
		} else {
			alert('Error changing location for side sponsor: '+data.reason);
		}
	});
	return false;
}

function st_chl(btn) {
	var st_id = $(btn).attr('data-st-id');
	var new_loc_id = $(btn).attr('data-new-loc-id');
	console.log('changing st '+st_id+' to loc_id '+new_loc_id);
	var jqxhr = $.post('/_ajax/classifieds_st?id='+{/literal}{$id}{literal}+'&action=st_chl&st_id='+st_id+'&new_loc_id='+new_loc_id, function(data) {
		console.log('data=',data);
		if (data.status == 'success') {
			$('#st').html(data.loc_name+' - live - expires '+data.until+'<br />');
		} else {
			alert('Error changing location for sticky upgrade: '+data.reason);
		}
	});
	return false;
}


$(document).ready(function() {
	//add button handlers
	$('.btn_refresh').each(function() {$(this).click(function() {refresh();return false;});});
	$('.btn_view').each(function() {$(this).click(function() {view();return false;});});
	//$('.btn_save').each(function() {$(this).click(function() {save();return false;});});
	$('.btn_list').each(function() {$(this).click(function() {list();return false;});});

	//after each input change, check if current value has changed from original value
	$('input').on('input', function() {field_changed(this);});
	$('select').on('input', function() {field_changed(this);});
	$('input').on('change', function() {field_changed(this);});

	//little helper to change expires date depending on change of classified status
	$('#done').change(function() {
		var val = $('#done').val();
		if (val == 0) {
			var today = new Date();
			var yyyy = today.getFullYear();
			var mm = today.getMonth()+1;
			if (mm < 10)
				mm='0'+mm;
			var dd = today.getDate();
			if (dd < 10)
				dd='0'+dd;
			$('#expires_date').val(yyyy+'-'+mm+'-'+dd);
			$('#expires_hour').val('0');
			$('#expires_minute').val('0');
			field_changed($('#expires_date'));
			field_changed($('#expires_hour'));
			field_changed($('#expires_minute'));
		} else if (val == 1) {
			var date_selected = new Date($('#expires_date').val()+'T'+$('#expires_hour').val()+':'+$('#expires_minute').val()+':00');
			var date_now = new Date();
			if (date_selected < date_now) {
				$('#expires_date').val($('#expires_date').attr('data-original-value'));
				$('#expires_hour').val($('#expires_hour').attr('data-original-value'));
				$('#expires_minute').val($('#expires_minute').attr('data-original-value'));
				field_changed($('#expires_date'));
				field_changed($('#expires_hour'));
				field_changed($('#expires_minute'));
			}
		}
	});

	//init videos
	//$('.flowplayer').flowplayer();
/*
	$('#account_id').on('input', function() {
		if ($('#account_id').val() != $('#account_id').attr('data-original-value'))
			$('#account_move_cc').prop('disabled', false);
		else
			$('#account_move_cc').prop('disabled', true);
	});
*/

	//help hover handlers
	$('span.help').each(function(ind, obj) {
		$(obj).hover(
			function(evt) {
				var help = $(evt.target).find('span');
				if (help) {
					help.css('position', 'absolute');
					help.css('background-color', '#F4FF94');
					help.css('border', '1px solid #444');
					help.css('padding', '3px');
					help.show();
					help.css('display', 'block');
				}
			},
			function(evt, obj) {
				$(evt.target).find('span').hide();
			}
		);
	});

	$('#btn_whitelist').click(function() {
		return whitelist();
	});
	$('#btn_unwhitelist').click(function() {
		return unwhitelist();
	});
	$('.btn_rotate_left').click(function() {
		return rotate($(this).data('image-id'), 'left');
	});
	$('.btn_rotate_right').click(function() {
		return rotate($(this).data('image-id'), 'right');
	});
	$('#btn_add_ct').click(function() {
		return ct_add(this);
	});
	$('#btn_add_ss').click(function() {
		return ss_add(this);
	});
	$('.btn_add_st').click(function() {
		return st_add(this);
	});
	$('.btn_ct_can').click(function() {
		return ct_can(this);
	});
	$('.btn_ct_chl').click(function() {
		return ct_chl(this);
	});
	$('.btn_ss_chl').click(function() {
		return ss_chl(this);
	});
	$('.btn_st_chl').click(function() {
		return st_chl(this);
	});

	mark_changed();

	$('#sticky').on('change', function () {
		$('#days').toggle();
	})
});

{/literal}
</script>

<h1>Manage classified ad #{$id}</h1>
<form method="POST" action="">
<input type="hidden" name="action" value="manage" />
<input type="hidden" name="action_id" value="{$id}" />
{if $ref}
	<input id="ref" type="hidden" name="ref" value="{$ref}" />
{/if}
<button class="btn_refresh" ><img src="/images/control/arrow-circle-double-135.png" /> Refresh</button>
<button class="btn_view"><img src="/images/control/eye.png" /> View as user</button>
<button type="submit" name="edit"><img src="/images/control/pencil.png" /> Edit</button>
<button type="submit" name="save" class="btn_save" disabled="disabled"><img src="/images/control/disk.png" /> Save changes</button>
<button class="btn_list"><img src="/images/control/table-join-row.png" /> Go to list</button>
<button type="submit" name="save_next" class="btn_save_next" disabled="disabled"><img src="/images/control/disk--arrow.png" /> Save changes and display next waiting ad</button>
<button type="submit" name="next"><img src="/images/control/arrow-skip.png" /> Skip to next waiting ad</button>
<table class="clad_manage">
<tr>
	<td width="500">
		<fieldset style="width: 95%;">
			<legend>General information</legend>
			<div style="float: left; margin-right: 5px;">
			{if $multiple_thumbs_urls}
				{foreach from=$multiple_thumbs_urls item=thumb_url}
					<img src="{$thumb_url}" class="climg"/><br />
				{/foreach}
			{else}
				<img src="{$thumb_url}" class="climg"/>
			{/if}
			</div>
			Title: <strong>{$title}</strong><br />
			Type: <select name="type" data-original-value="{$type_selected}">{html_options values=$type_values output=$type_names selected=$type_selected}</select><br/>
			Status: 
			{if $deleted}
				<span style="color: red; font-weight: bold;">DELETED!</span>
			{else}
				<select id="done" name="done" data-original-value="{$done_selected}">{html_options values=$done_values output=$done_names selected=$done_selected}</select>
			{/if}
			<br />
			Expires:
			<input type="date" name="expires_date" id="expires_date" data-original-value="{$expires_date}" value="{$expires_date}" />&nbsp;&nbsp;
			<input type="text" name="expires_hour" id="expires_hour" data-original-value="{$expires_hour}" value="{$expires_hour}" size="1" style="height: 18px;" />:
			<input type="text" name="expires_minute" id="expires_minute" data-original-value="{$expires_minute}" value="{$expires_minute}" size="1" style="height: 18px;" />
			<br />
			Account #:
			<input type="text" name="account_id" id="account_id" data-original-value="{$account_id}" value="{$account_id}" size="5" />
			<input type="checkbox" name="account_move_cc" id="account_move_cc" value="1" disabled="disabled" />Move CC info as well
			<br />
			Email: {$email}, Whitelisted: <span id="whitelisted">
				{if $whitelisted}
					<span style="color: green; font-weight: bold;">YES</span> <a id="btn_unwhitelist" href="#">Unwhitelist</a>
				{else}
					NO <a id="btn_whitelist" href="#"><button type="button">Whitelist</button></a></span>
				{/if}
			<br />
			CC: <select name="cc_id" data-original-value="{$cc_id}">{html_options values=$cc_ids output=$cc_labels selected=$cc_id}</select><br/>
		</fieldset>
		<fieldset style="width: 95%;">
			<legend>Videos</legend>
			{if !$videos}
				No videos.
			{else}
				{foreach from=$videos key=video_id item=video}
					<div class="clvideo_div">
							<div class="flowplayer is-splash" style="
								 width:{$video.width}px;height:{$video.height}px;background-color:#777;
								 background-image:url('{$video.thumbnail_uri}');">
								<video controls><source src="{$video.file_uri}" type="video/mp4"/></video>
							</div>
					</div>
				<div class="clvideo_status">Status: <b>{$video.converted}</b> Created: <b>{$video.created}</b></div>
					<br style="clear: both;" />
				{/foreach}
			{/if}
		</fieldset>
		<fieldset style="width: 95%;">
			<legend>Images</legend>
			{if !$images}
				No images.
			{else}
				{foreach from=$images key=image_id item=image}
					<div class="climg_div">
						<span style="display: block; float: left;">
						<img src="{$config_image_server}/classifieds/t/{$image}" class="climg"/>
						</span>
						<a href="#" class="btn_rotate_right" data-image-id="{$image_id}" style="float: right;"><img src="/images/control/rotate-right.png" alt="rotate right"></a>
						<a href="#" class="btn_rotate_left" data-image-id="{$image_id}" style="float: right;"><img src="/images/control/rotate-left.png" alt="rotate left"></a>
						<br style="clear: both;" />
					</div>
				{/foreach}
			{/if}
		</fieldset>
		<fieldset style="width: 95%;">
			<legend>Content</legend>
			{$content}
		</fieldset>
	</td>
	<td width="600">
		<fieldset style="width: 95%;">
			<legend>Upgrades</legend>

			<label style="margin-right: 10px;">City thumbnail:</label>
			<span id="ct" style="max-height: 100px; overflow: auto; vertical-align: middle;">
			{foreach from=$city_thumbnails item=ct}
				{$ct.location} - 
				{if $ct.done == 1}
					live - expires {$ct.expire_stamp|date_format:"%m/%d/%Y"}
				{elseif $ct.done == 2}
					invisible - expires {$ct.expire_stamp|date_format:"%m/%d/%Y"}
				{else}
					expired - expired {$ct.expire_stamp|date_format:"%m/%d/%Y %H:%i"}
				{/if}
				{if $ct.done == 1}
					<button type="button" class="btn btn-default btn-xs btn_ct_can" data-ct-id="{$ct.id}">Cancel city thumbnail in {$ct.location}</button>
				{/if}
				{if $location_count == 1 && $ct.loc_id != $location_1_id}
					<button type="button" class="btn btn-default btn-xs btn_ct_chl" data-ct-id="{$ct.id}" data-new-loc-id="{$location_1_id}">Change Location to {$locations_text}</button>
				{/if}
				<br />
			{foreachelse}
				<span id="ct_none" style="vertical-align: middle;">
				No city thumbnails
				{if $location_count == 1}
					<button type="button" class="btn btn-default btn-xs" id="btn_add_ct" data-loc-id="{$location_1_id}" data-loc-name="{$locations_text}" class="btn btn-default btn-xs">Add city thumbnail for {$locations_text} until {$expires_date}</button>
				{else}
					<i>TODO add city thumbnails for more locations</i>
				{/if}
				</span>
				<br />
			{/foreach}
			</span>

			<label style="margin-right: 10px;">Side sponsor:</label>
			<span id="ss" style="max-height: 100px; overflow: auto; vertical-align: middle;">
			{foreach from=$side_sponsors item=ss}
				{$ss.location} - 
				{if $ss.done == 1}
					live - expires {$ss.expire_stamp|date_format:"%m/%d/%Y"}
				{elseif $ss.done == 2}
					invisible - expires {$ss.expire_stamp|date_format:"%m/%d/%Y"}
				{else}
					expired - expired {$ss.expire_stamp|date_format:"%m/%d/%Y %H:%I"}
				{/if} 
				{if $location_count == 1 && $ss.loc_id != $location_1_id}
					<button type="button" class="btn btn-default btn-xs btn_ss_chl" data-ss-id="{$ss.id}" data-new-loc-id="{$location_1_id}">Change Location to {$locations_text}</button>
				{/if}
				<br />
			{foreachelse}
				<span id="ss_none" style="vertical-align: middle;">
				No side sponsors
				{if $location_count == 1}
					<button type="button" class="btn btn-default btn-xs" id="btn_add_ss" data-loc-id="{$location_1_id}" data-loc-name="{$locations_text}">Add side sponsor for {$locations_text} until {$expires_date}</button>
				{else}
					<i>TODO add side sponsors for more locations</i>
				{/if}
				<br />
				</span>
			{/foreach}
			</span>

			<label style="margin-right: 10px;">Sticky Upgrade:</label>
			<span id="st" style="max-height: 100px; overflow: auto; vertical-align: middle;">
			{foreach from=$stickies item=st}
				{$st.location} - 
				{if $st.done == 1}
					live - expires {$st.expire_stamp|date_format:"%m/%d/%Y"}
				{elseif $st.done == 2}
					invisible - expires {$st.expire_stamp|date_format:"%m/%d/%Y"}
				{else}
					expired - expired {$st.expire_stamp|date_format:"%m/%d/%Y %H:%I"}
				{/if}
				{if $st.done == 1 && $location_count == 1 && $st.loc_id != $location_1_id}
					<button type="button" class="btn btn-default btn-xs btn_st_chl" data-st-id="{$st.id}" data-new-loc-id="{$location_1_id}">Change Location to {$locations_text}</button>
				{else if $st.done == 0}
					<button type="button" class="btn btn-default btn-xs btn_add_st" data-days="30" data-loc-id="{$st.loc_id}" data-loc-name="{$locations_text}">Add sticky upgrade for {$locations_text} until {$expires_date}</button>
				{/if}
				<br />
			{foreachelse}
				<span id="st_none" style="vertical-align: middle;">
				No sticky upgrade
				{if $location_count == 1}
					<button type="button" class="btn btn-default btn-xs btn_add_st" data-days="30" data-loc-id="{$location_1_id}" data-loc-name="{$locations_text}">Add sticky upgrade for {$locations_text} until {$expires_date}</button>
					<button type="button" class="btn btn-default btn-xs btn_add_st" data-days="7" data-loc-id="{$location_1_id}" data-loc-name="{$locations_text}" style="margin-left: 212px">Add sticky upgrade for {$locations_text} until {$expires_in_one_week}</button>
				{else}
					<i>TODO add sticky upgrade for more locations</i>
				{/if}
				<br />
				</span>
			{/foreach}
			</span>

			<label style="margin-right: 10px;">
				Sponsor desktop:
				<input type="checkbox" name="sponsor" value="1" data-original-value="{$sponsor}" {if $sponsor > 0}checked="checked"{/if} />
				<span class="help">?<span>If checked, ad will be displayed at the top of the list in desktop version of page (wont be displayed at all in mobile version, unless also 'Mobile sponsor' is checked)</span></span>
			</label>
			<label style="margin-right: 10px;">
				Sponsor mobile:
				<input type="checkbox" name="sponsor_mobile" value="1" data-original-value="{$sponsor_mobile}" {if $sponsor_mobile > 0}checked="checked"{/if} />
				<span class="help">?<span>If checked, ad will be displayed at the top of the list in mobile version of page (wont be displayed at all in desktop version, unless also 'Sponsor' is checked)</span></span>
			</label>
			Sponsor position:
			<input type="text" name="sponsor_position" value="{$sponsor_position}" data-original-value="{$sponsor_position}" style="width: 20px;"/>
			<br />

			{*<label style="margin-right: 10px;">*}
				{*Sticky Upgrade*}
				{*<input type="checkbox" id="sticky" name="sticky" value="{$sticky}" data-original-value="{$sticky}"*}
					   {*style="width: 20px;" {if $sticky}checked="checked"{/if}/>*}
				{*<span class="help">?<span>If checked, ad will be displayed at the top of the list with a grey "SPONSOR" badge</span></span>*}
				{*<span id="days" style="display: {if $sticky}block{else}none{/if}">*}
					{*<input type="radio" name="days" value="30" {if $days == 30}checked="checked"{/if}> Monthly*}
					{*<input type="radio" name="days" value="7" {if $days == 7}checked="checked"{/if}> Weekly*}
				{*</span>*}
			{*</label>*}


			<label style="margin-right: 20px;">
				Auto Repost: 
				<input type="checkbox" name="auto_repost" value="1" data-original-value="{$auto_repost}" {if $auto_repost > 0}checked="checked"{/if} />
				<span class="help">?<span>Auto repost means the ad is reposted automatically in selected time and frequency.</span></span>
			</label>
			<label>
				Top-Up credits: {$repost}
			</label>
			<br />
			<label>
				Repost frequency:
				<select name="auto_renew_fr" data-original-value="{$auto_renew_fr}">{html_options values=$arf_values output=$arf_names selected=$auto_renew_fr}</select>
				<select name="auto_renew_time" data-original-value="{$auto_renew_time}">{html_options values=$art_values output=$art_names selected=$auto_renew_time}</select>
				{$timezone}
			</label>
			<br />
			<label>
				Next repost: {$next_repost}
			</label>
			<br />
			<label>
				Last repost: {$last_repost}
			</label>
			<a href="/classifieds/topups?account_id={$account_id}">view&nbsp;latest&nbsp;top-ups</a>
			<br />

		</fieldset>

		<fieldset style="width: 95%;">
			<legend>Locations</legend>
			<label># of posted locations: {$location_count}: {$locations_text}</label><br />
			<label><a href="/classifieds/move?id={$id}">Change location(s)</a></label><br />
			<label><a href="/classifieds/locations?id={$id}&ref=/mng/classifieds?action=manage&action_id={$id}">Mass Update posted locations</a></label><br />
		</fieldset>

		<fieldset style="width: 95%;">
			<legend>Payments</legend>
			{if !$payment}
				<em>No payment</em>
			{else}
				{if $last_payment_event}
					{$last_payment_event.stamp|date_format:"%m/%d/%Y %H:%I"}: 
					{if $last_payment_event.type == "RF"}<span style="color: red;">Renewal failure</span>
					{else if $last_payment_event.type == "RS"}<span style="color: green;">Renewal success</span>
					{else}{$last_payment_event.type} - {$last_payment_event.message}
					{/if}
					<hr style="border: 0px none; border-top: 1px dashed #aaa;"/>
				{/if}
				Original payment: 
				#{$payment.id} from {$payment.created_stamp|date_format:"%m/%d/%Y %H:%I"} - ${$payment.amount} - {if $payment.result == "A"}<span style="color: green;">successful</span>{else}<span style="color: red;">failed</span>{/if}
			{/if}
		</fieldset>

		<fieldset style="width: 95%;">
			<label>
				Direct link(s): 
				<input type="text" name="direct_link" value="{$direct_link}" data-original-value="{$direct_link}" size="60" />
			</label>
			<br />

			<legend>Other</legend>
			<label>
				Allow links: 
				<input type="checkbox" name="allowlinks" value="1" data-original-value="{$allowlinks}" {if $allowlinks > 0}checked="checked"{/if} />
			</label>
			<br />

			<label>
				Eccie reviews link: 
				<input type="text" name="eccie_reviews_link" value="{$eccie_reviews_link}" data-original-value="{$eccie_reviews_link}" size="40" />
			</label>
			<br />
			<label>Hits:</label> {$hit}<br />
			{if !$account_id}
			<label>
				Control link:
				<div id="claim"></div>
				<a href="#" onclick="_ajax('get', '/classifieds/claim', 'set={$id}', 'claim'); return false;">create a new link</a>
			</label>
			{/if}
		</fieldset>
	</td>
</tr>
</table>
<br /><br />

<fieldset style="width: 95%;">
	<legend>Other ads of this account</legend>
	{if !$other_ads}
	{else}
		{foreach $other_ads as $ad}
			{$ad}
			<hr style="clear: both;"/>
		{/foreach}
	{/if}
</fieldset>

<br /><br />

<button type="submit" name="delete" class="control_btn {if $important || $whitelisted}disabled{/if}"><img src="/images/control/cross.png" /> Delete</button>
<button type="submit" name="delete_ban" class="control_btn {if $important || $whitelisted}disabled{/if}"><img src="/images/control/cross.png" /> Delete ad &amp; <img src="/images/control/user-red.png" /> Ban account</button>
<button type="submit" name="delete_all_ban" class="control_btn {if $important || $whitelisted || $other_ads_important}disabled{/if}"><img src="/images/control/cross-two.png" /> Delete *ALL* ads of this account &amp; <img src="/images/control/user-red.png" /> Ban this account</button>

<input type="hidden" name="submit" value="clad_manage" />
</form>

