<h2>Classifieds report - Paid vs. Free</h2>
<form action="" method="get">
<table width="800" cellpadding="3" style="background-color:#FFFFFF;border-width:0;color:#000000;font-family:verdana,sans-serif;font-size:12px;">
<tr>
	<td colspan="2">
		<button type="submit" name="show" value="show" style="cursor: pointer; font-weight: bold;"/><img src="/images/control/eye.png" />&nbsp;Show Report</button>
{*
		&nbsp;&nbsp;&nbsp;<button type="submit" name="export" value="export" style="cursor: pointer;"/><img src="/images/csv_icon_16.gif" />&nbsp;Export to CSV</button>
*}
	</td>
</tr>
</table>
</form>

<table width="800">
<tr>
	<th>City</th>
	<th># Free ads</th>
	<th># Paid ads</th>
	<th>% of paid ads</th>
	<th>Total # ads in location</th>
</tr>

{foreach $data as $item}
<tr style="background-color:#{cycle values="eeeeee,ffffff"}">
	<td>{$item.location}</td>
	<td>{$item.free}</td>
	<td>{$item.paid}</td>
	<td>{$item.paid_percent}</td>
	<td>{$item.total}</td>
</tr>
{/foreach}

</table>
