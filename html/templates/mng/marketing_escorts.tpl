{literal}
<style type='text/css'>
.contact {border:1px solid #D0D0D0;border-bottom:0}
.contact .h {line-height:32px;background-color:#DCDCDC;text-transform:uppercase}
.contact tr {padding:5px;line-height:24px}
.contact td {border-bottom:1px solid #D0D0D0}
.contact .n1 {background-color:#F0F0F0}
.contact .n2 {background-color:#FFFFFF}
</style>
{/literal}

{if $news}
<table width='100%' class="contact">
 <tr class='h'>
  <th>Name</th>
  <th>E-Mail</th>
  <th>Location</th>
  <th>Phone</th>
  <th>Has Live Ad</th>
  <th>Contacted on</th>
 </tr>

{section name=s1 loop=$news}{assign var=n value=$news[s1]}
 <tr class="{cycle values="n1,n2"}">
  <td><a class="read" id="{$n.account_id}" href="?show={$n.account_id}" onclick="return false;">{$n.name}</a></td>
  <td>{$n.email}</td>
  <td>{$n.loc_name}</td>
  <td>{$n.phone}</td>
  <td>{if $n.active==1}<span class="status_active">Yes</span>{else}<span class="status_deleted">No</span>{/if}</td>
  <td>{$n.contacted}</td>
 </tr>
{/section}

</table>
{/if}

<div id="example" style="display:none"></div>

{literal}
<script type='text/javascript'>
var dialogOpts = {modal: true, bgiframe: true, autoOpen: false,height: 350,width: 640,draggable: true,resizeable: true};
$(document).ready(function(){
 $("a.read").click(function() {
	$("#example").dialog(dialogOpts);
	$("#example").load($(this).attr("href"), "", function(){
		$("#example").dialog("option", "width", 640);
		$("#example").dialog("option", "height",550);
		$("#example").dialog("open");
	});
	return false;
  });

  {/literal}
  {if $open}$("a#{$open}").attr('href', $("a#{$open}").attr('href')+'&error=1').click();{/if}
  {literal}

});

</script>
{/literal}
