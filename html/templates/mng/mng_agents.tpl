<style>
.col1{ background-color:#F0B699}
.totals{ background-color:#FBA800}
</style>

<form method="get">
Look up <b>promo codes, email, phone numbers or ad ID(with '#' character before the ID)</b>: <input type="text" name="lookupcode" value=""/> <input type="submit" 
value="Show" />
</form>

<form method="get">
 <select name="date">
  {section name=s loop=$m}{assign var=d value=$m[s]}
   <option value="{$d.sql}"{if $d.selected} selected="selected"{/if}>{$d.txt}</option>
  {/section}
 </select>
 <select name='group'>{$group}</select>
<input type="submit" value="Show" />
</form>

<table width='1000'>
<tbody>
<tr>
 <th>Agent</th>
 <th>Free Ads</th>
 <th>High. Ads</th>
 <th>30% Ads</th>
 <th>50% Ads</th>
 <th>Rec. Ads</th>
 <th>Refunds</th>
 <th>Charge back</th>
 <th>Made Us</th>
 <th>Share</th>
 <th>Recurring Sales</th>
 <th>Recurring Made</th>
 <th>Total</th>
</tr>
</tbody>


{section name=s loop=$agent}{assign var=a value=$agent[s]}
{assign var=freead value=$freead+$a.freead}
{assign var=highlight value=$highlight+$a.highlight}
{assign var=percent30 value=$percent30+$a.percent30}
{assign var=percent50 value=$percent50+$a.percent50}
{assign var=recurring value=$recurring+$a.recurring}
{assign var=refund value=$refund+$a.refund}
{assign var=chargeback value=$chargeback+$a.chargeback}
{assign var=made value=$made+$a.made}
{assign var=com value=$com+$a.com}
{assign var=recurring2 value=$recurring2+$a.recurring2}
{assign var=made2 value=$made2+$a.made2}
{assign var=total value=$total+$a.total}
{/section}
<tr class="totals">
 <td>Totals...:</td>
 <td>{$freead}</td>
 <td>{$highlight}</td>
 <td>{$percent30}</td>
 <td>{$percent50}</td>
 <td>{$recurring}</td>
 <td>{$refund}</td>
 <td>{$chargeback}</td>
 <td><b>${$made}</b></td>
 <td>${$com}</td>
 <td>${$recurring2}</td>
 <td>${$made2}</td>
 <td><b>${$total}</b></td>
</tr>

{section name=s loop=$agent}{assign var=a value=$agent[s]}
 <tr class='{cycle values="col1,col2"}'>
  <td><a href="?agent={$a.user}">{$a.user}</a></td>
  <td>{$a.freead}</td>
  <td>{$a.highlight}</td>
  <td>{$a.percent30}</td>
  <td>{$a.percent50}</td>
  <td>{$a.recurring}</td>
  <td>{$a.refund}</td>
  <td>{$a.chargeback}</td>
  <td>${$a.made}</td>
  <td>${$a.com}</td>
  <td>${$a.recurring2}</td>
  <td>${$a.made2}</td>
  <td><b>${$a.total}</b></td>
 </tr>

{assign var=freead value=$freead+$a.freead}
{assign var=highlight value=$highlight+$a.highlight}
{assign var=percent30 value=$percent30+$a.percent30}
{assign var=percent50 value=$percent50+$a.percent50}
{assign var=recurring value=$recurring+$a.recurring}
{assign var=refund value=$refund+$a.refund}
{assign var=chargeback value=$chargeback+$a.chargeback}
{assign var=made value=$made+$a.made}
{assign var=com value=$com+$a.com}
{assign var=recurring2 value=$recurring2+$a.recurring2}
{assign var=made2 value=$made2+$a.made2}
{assign var=total value=$total+$a.total}
{/section}

</table>
