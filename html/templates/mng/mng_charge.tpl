<form id="marketing">
<input type="hidden" name="ad" value="{$id}"/>
{if $cc_add}<input type="hidden" name="cc_add" value="1"/>{/if}
<div class="cl">
<table class="b" width="100%">
 <tr>
  <td class="first">Promo Code:</td>
  <td class="second"><select name="package" id="package" class="refreshme">{$package_select}</select></td>
 </tr>

{if $auto_renew}
 <tr>
  <td class="first">Auto Re-Post to the Top:</td>
  <td class="second"><select name="auto_renew" id="autorenew" class="refreshme">{$auto_renew}</select></td>
 </tr>
{/if}

{if $option_recurring}
 <tr>
  <td class="first">Recurring:</td>
  <td class="second"><input type='checkbox' name='recurring' value='1' id="recurring" class="refreshme"{if $recurring} checked="checked"{/if}/></td>
 </tr>
{/if}

{if $option_renewtime}
 <tr>
  <td class="first">Post this ad:</td>
	<td class="second">
		<select name="auto_renew_time" class="refreshme">
		{html_options values=$art_values output=$art_names selected=$auto_renew_time}
		</select>
		{if $timezone}{$timezone}{else}PST{/if}
	</td>
 </tr>
{/if}

{if $cc}
 <tr>
  <td class="first">Credit Card on file:</td>
  <td class="second"><select name="cc_id" id="cc_id">
	<option value="">-- use selected cc --</option>
	{section name=s loop=$cc}{assign var=c value=$cc[s]}
	<option value="{$c.cc_id}">{$c.cc} - {$c.firstname} {$c.lastname} - {$c.expmonth}/{$c.expyear} - {$c.zipcode}</option>
	{/section}
	</select><br/><a href="#" class="ccadd">Add another credit card</a></td>
 </tr>
{elseif $cc_add}
 <tr>
  <td class="first">First Name:</td>
  <td class="second">
        <input type="text" name="cc_firstname" size="40" value="{$cc_firstname}" maxlength="60" />
  </td>
 </tr>

 <tr>
  <td class="first">Last Name:</td>
  <td class="second">
        <input type="text" name="cc_lastname" size="40" value="{$cc_lastname}" />
  </td>
 </tr>

 <tr>
  <td class="first">Address:</td>
  <td class="second">
        <input type="text" name="cc_address" size="40" value="{$cc_address}" />
  </td>
 </tr>

 <tr>
  <td class="first">ZIP/Postal Code:</td>
  <td class="second">
        <input type="text" name="cc_zipcode" size="10" value="{$cc_zipcode}" />
  </td>
 </tr>

 <tr>
  <td class="first">Credit Card Number:</td>
  <td class="second">
        <input type="text" name="cc_cc" size="20" value="{$cc_cc}" /> <img class="img1" src="/images/classifieds/Credit_card_logos_small.png" alt="" />
  </td>
 </tr>

 <tr>
  <td class="first">Expiration Date:</td>
  <td class="second">
          <select name="cc_month">
<option value="">-- Select --</option>
<option value="1"{if $cc_month==1} selected="selected"{/if}>January (01)</option>
<option value="2"{if $cc_month==2} selected="selected"{/if}>February (02)</option>
<option value="3"{if $cc_month==3} selected="selected"{/if}>March (03)</option>
<option value="4"{if $cc_month==4} selected="selected"{/if}>April (04)</option>
<option value="5"{if $cc_month==5} selected="selected"{/if}>May (05)</option>
<option value="6"{if $cc_month==6} selected="selected"{/if}>June (06)</option>
<option value="7"{if $cc_month==7} selected="selected"{/if}>July (07)</option>
<option value="8"{if $cc_month==8} selected="selected"{/if}>August (08)</option>
<option value="9"{if $cc_month==9} selected="selected"{/if}>September (09)</option>
<option value="10"{if $cc_month==10} selected="selected"{/if}>October (10)</option>
<option value="11"{if $cc_month==11} selected="selected"{/if}>November (11)</option>
<option value="12"{if $cc_month==12} selected="selected"{/if}>December (12)</option>
</select>

  <select name="cc_year"><option value="">-- Select --</option>
        {php}for($i=date("Y");$i<date("Y")+11;$i++){ $ii = substr($i, -2); $selected = isset($_POST["cc_year"]) && $_POST["cc_year"] == $ii ? ' selected="selected"': '';
echo "<option value=\"$ii\"$selected>$i</option>";}{/php}</select>
  </td>
 </tr>

 <tr>
  <td class="first">Security Code (CVC2):</td>
  <td class="second">
        <input type="text" name="cc_cvc2" value="{$cc_cvc2}" size="5" />
  </td>
 </tr>
{/if}

 <tr>
  <td class="first">Total:</td>
  <td class="second"><span id="totalcost" class="orange">{$total}</span></td>
 </tr>

 <tr>
  <td class="first"></td>
  <td class="second"><span id="chargemebtn"><input type="button" name="submit" value="Submit" onclick="chargeme();" /></span><span id="chargemebtn2" 
style="display:none">wait...</span>
	</td>
 </tr>

 <tr>
  <td class="first" colspan="2"> <div id="result"></div></td>
 </tr>

</table>
</div>
</form>


<script type="text/javascript">
var base_price = {$base_price};
var price_per_repost = {$price_per_repost};
var price_per_recurring = {$price_per_recurring};
var total_loc = {$total_loc};
var id = {$id};

{* 'chr=true&id='+id+'&cc_id='+cc_id,  *}


function chargeme() {
	var cc_id = $("#cc_id").val();
	if( cc_id == '' ) {
		alert('Pick a CC please.'); return;
	}
	$("#chargemebtn,#chargemebtn2").toggle();
	var param = $("#marketing").serialize() + "&chr=true";
	$.ajax({ 
		type: 'get', url: '/mng/charge', data: param, 
		success: function(msg){ 
			jQuery('#result').html(msg);
			if( msg.indexOf("uccess") < 0 ) $("#chargemebtn,#chargemebtn2").toggle(); 
		}
	});
}

function updatePrice() {
	$("#totalcost").html(moneyUSA(base_price+$("#autorenew").val()*price_per_repost*total_loc));
}


function moneyUSA (n) {
   var s = "" + (Math.round(n*100)/100);
   var i = s.indexOf('.');
   if (i < 0) return "$" + s + ".00";
   if (i + 2 == s.length) s += "0";
   return "$" + s;
}

var url = "/mng/charge";

$(document).ready(function(){
	$(".refreshme").change(function(){
		var param = $("#marketing").serialize();
		$("#example").load(url, param, function(){ });
	});

	$(".ccadd").click(function(event){
		var param = $("#marketing").serialize() + "&cc_add=1";
		$("#example").load(url, param, function(){ });
		event.preventDefault();
		return false;
	});
});

</script>
