<h3>{$total} total record found</h3>

{if $total}
{if $total<20000}<a href="{$q}excel=1">export as excel</a>{else}<b>To export to Excel, there should be less than 20,001 records. (Filter locations to 
lower the number.)</b>{/if}
{/if}

<form name="FilterForm" method="post" action="/mng/cl">
<table class="datatable">
<thead>
<tr><td colspan="12">{$paging}</td></tr>
<tr>
<th><a title="Click to order by ID">Ad ID</a></th>
<th><a title="Click to order by Account ID">Account ID</a></th>
<th><a title="Click to order by E-Mail">E-Mail</a></th>
{* <th><a title="">Last/First Name</a></th> *}
<th><a title="Click to order by Location">Location</a></th>
<th><a>State</a></th>
<th><a title="Click to order by Phone">Phone</a></th>
<th><a>Recurring</a></th>
<th><a title="Click to order by Live">Live</a></th>
<th><a title="Click to order by posted">posted</a></th>
<th><a title="Click to order by auto renew">auto repost</a></th>
<th><a title="Click to order by Promo">Promo</a></th>
<th>&nbsp;</th>
</tr>
<tr>
<td><input size="3" type="text" name="id" id="filter0" value="{$id}" tabindex="1" autocomplete="off" 
style="width: 50px; "><ul id="dlsg_0" class="dlsg"></ul>
</td>
<td><input size="3" type="text" name="account_id" id="filter1" value="{$account_id}" tabindex="2" autocomplete="off" style="width: 
82px; "><ul id="dlsg_1" class="dlsg"></ul><input type="checkbox" name="acctgrp" value="1" {if $acctgrp}checked="checked"{/if}/> Group by Accounts
</td>
<td><input size="25" type="text" name="cl_email" id="filter1" value="{$cl_email}" tabindex="3" autocomplete="off"><ul id="dlsg_2" class="dlsg"></ul></td>
{* <td><input size="25" type="text" name="cl_firstname" id="filter11" value="{$cl_firstname}" tabindex="3" autocomplete="off"><ul id="dlsg_11" 
class="dlsg"></ul></td> *}
<td><input size="3" type="text" name="loc_name" id="filter3" value="{$loc_name}" tabindex="4" autocomplete="off" style="width: 
67px; "><ul id="dlsg_3" class="dlsg"></ul>	
</td>
<td><input size="3" type="text" name="state" value="{$state}" tabindex="4" autocomplete="off"></td>
<td><input size="10" type="text" class="filter_set" name="phone" value="{$phone}" id="filter4" tabindex="5" 
autocomplete="off"><ul id="dlsg_4" class="dlsg"></ul>
</td>
<td><input size="3" type="text" name="recurring" id="filter5" value="{$recurring}" tabindex="6" autocomplete="off" style="width: 50px; ">
</td>
<td><input size="1" type="text" name="done" id="filter6" value="{$done}" tabindex="7" autocomplete="off"><ul id="dlsg_6" class="dlsg"></ul>
</td>
<td><table class="filter_date" cellspacing="0"><tbody><tr><td><input size="3" type="text" id="filter7_s" name="filter7_s" value=" - From -" class="filter_date_input" 
tabindex="8" onfocus="if (this.value == ' - From -') this.value = '';"></td></tr>

<tr><td><input size="3" type="text" id="filter7_e" name="filter7_e" value=" - To -" class="filter_date_input" tabindex="9"></td></tr></tbody></table></td>
<td><input size="3" type="text" name="auto_renew" id="filter8" value="" tabindex="10" autocomplete="off" ><ul id="dlsg_8" class="dlsg"></ul>
</td>
<td><input size="3" type="text" name="promo" id="filter9" value="{$promo}" tabindex="11" autocomplete="off" style="width: 50px; "><ul id="dlsg_9" class="dlsg"></ul>
</td>
<td><input type="reset" tabindex="12" value="Reset" onclick="window.location='/mng/cl';">
<input type="submit" value="Fetch" tabindex="13"></td>
</tr>

</thead>
<tbody>
{section name=l loop=$listing}{assign var=l value=$listing[l]}
<tr onclick="ListingRowClicked(this);">
<td>{$l.id}</td>
<td>{if $l.account_id}<a href="/mng/accountdetails?account_id={$l.account_id}">{$l.account_id}</a>{else}{$l.account_id}{/if}</td>
<td>{$l.cl_email}</td>
{* <td>{$l.cl_lastname}{if $l.cl_lastname}, {/if}{$l.cl_firstname}</td> *}
<td>{$l.loc_name}</td>
<td>{$l.state}</td>
<td>{$l.phone}</td>
<td>{$l.recurring}</td>
<td>{$l.done}</td>
<td>{$l.date}</td>
<td>{$l.auto_renew}</td>
<td>{$l.promo}</td>
<td><a href="{$config_site_url}/classifieds/look?id={$l.id}" target="_blank">Show</a></td>
</tr>
{sectionelse}<tr><td colspan="10">No rows has been found. Try to change search criteria.</td></tr>{/section}
</tbody>
<tfoot>
<tr><td colspan="12">{$paging}</td></tr>
</tfoot>

</table>
</form>
