<form action="" method="get">

<table width="800" cellpadding="3" style="background-color:#FFFFFF;border-width:0;color:#000000;font-family:verdana,sans-serif;font-size:12px;">
<tr>
	<td width="130"><b>Select Report Date:</b></td>
	<td>
		<input type="radio" name="range_or_period" value="1" class="input" id="periodd" {if $range_or_period==1 || !$range_or_period}checked="checked"{/if} />
		<select name="period" class="input" onfocus="this.form.periodd.checked=true">
		<option value="3"{if $period==3} selected="selected"{/if}>Today</option> 
		<option value="2"{if $period==2} selected="selected"{/if}>Yesterday</option>  
		<option value="1"{if $period==1} selected="selected"{/if}>Last 7 Days</option> 
		<option value="0"{if $period==0} selected="selected"{/if}>Last 30 Days</option>
		<option value="4"{if $period==4} selected="selected"{/if}>This Month</option>
		<option value="5"{if $period==5} selected="selected"{/if}>Last Month</option>
		</select>&nbsp;&nbsp;&nbsp;
		<i>or</i>&nbsp;

		<input type="radio" name="range_or_period" value="2" class="input" {if $range_or_period==2}checked="checked"{/if} id="range" />
		<select name="monthFrom" class="input" onfocus="this.form.range.checked=true">
		<option value="1"{if $monthFrom==1} selected="selected"{/if}>Jan</option>   
		<option value="2"{if $monthFrom==2} selected="selected"{/if}>Feb</option>
		<option value="3"{if $monthFrom==3} selected="selected"{/if}>Mar</option>
		<option value="4"{if $monthFrom==4} selected="selected"{/if}>Apr</option>
		<option value="5"{if $monthFrom==5} selected="selected"{/if}>May</option>
		<option value="6"{if $monthFrom==6} selected="selected"{/if}>Jun</option>
		<option value="7"{if $monthFrom==7} selected="selected"{/if}>Jul</option>
		<option value="8"{if $monthFrom==8} selected="selected"{/if}>Aug</option>
		<option value="9"{if $monthFrom==9} selected="selected"{/if}>Sep</option>
		<option value="10"{if $monthFrom==10} selected="selected"{/if}>Oct</option>
		<option value="11"{if $monthFrom==11} selected="selected"{/if}>Nov</option>
		<option value="12"{if $monthFrom==12} selected="selected"{/if}>Dec</select>
		<input type="text" name="dayFrom" size="2" value="{$dayFrom}" maxlength="2" class="input" onfocus="this.form.range.checked=true" />
		<input type="text" name="yearFrom" size="4" value="{$yearFrom}" maxlength="4" class="input" onfocus="this.form.range.checked=true" />

		&nbsp;&nbsp;-&nbsp;&nbsp;
 
		<select name="monthTo" class="input" onfocus="this.form.range.checked=true">
		<option value="1"{if $monthTo==1} selected="selected"{/if}>Jan</option>
		<option value="2"{if $monthTo==2} selected="selected"{/if}>Feb</option>
		<option value="3"{if $monthTo==3} selected="selected"{/if}>Mar</option>
		<option value="4"{if $monthTo==4} selected="selected"{/if}>Apr</option>
		<option value="5"{if $monthTo==5} selected="selected"{/if}>May</option>
		<option value="6"{if $monthTo==6} selected="selected"{/if}>Jun</option>
		<option value="7"{if $monthTo==7} selected="selected"{/if}>Jul</option>
		<option value="8"{if $monthTo==8} selected="selected"{/if}>Aug</option>
		<option value="9"{if $monthTo==9} selected="selected"{/if}>Sep</option>
		<option value="10"{if $monthTo==10} selected="selected"{/if}>Oct</option>
		<option value="11"{if $monthTo==11} selected="selected"{/if}>Nov</option>
		<option value="12"{if $monthTo==12} selected="selected"{/if}>Dec</option>
		</select>
		<input type="text" name="dayTo" size="2" value="{$dayTo}" maxlength="2" class="input" onfocus="this.form.range.checked=true" />
		<input type="text" name="yearTo" size="4" value="{$yearTo}" maxlength="4" class="input" onfocus="this.form.range.checked=true" /> 
 	</td>
</tr>	   
<tr>
	<td width="130"><b>Agency:</b></td>
	<td>
		<select name="agency" class="input">
			<option value="">All Ads</option>
			<option value="na" {if $agency === 'na'} selected="selected"{/if}>Only Non-Agency Ads</option>
			<option value="a" {if $agency === 'a'} selected="selected"{/if}>Only Agency Ads</option>
		</select>
	</td>
</tr>
{*
<tr>
	<td width="130"><b>Promocode:</b></td>
	<td><input type="text" name="promocode" value="{$promocode}" class="input" id="promocode" /></td>
</tr>
<tr>
	<td width="130"><b>Paid ads:</b></td>
	<td>
		<select name="paid" class="input">
			<option value="">All Ads</option>
			<option value="0" {if $paid === '0'} selected="selected"{/if}>Only Free Ads</option>
			<option value="1" {if $paid === '1'} selected="selected"{/if}>Only Paid Ads</option>
		</select>
	</td>
</tr>
<tr>
	<td width="130"><b>Location:</b></td>
	<td>
		<select name="location" class="input">
			<option value="">All Locations</option>
			<option value="us" {if $location == "us"} selected="selected"{/if}>U.S.</option>
			<option value="ca" {if $location == "ca"} selected="selected"{/if}>Canada</option>
			<option value="uk" {if $location == "uk"} selected="selected"{/if}>U.K.</option>
			<option value="do" {if $location == "do"} selected="selected"{/if}>Domestic (US, Canada, UK)</option>
			<option value="in" {if $location == "in"} selected="selected"{/if}>International (everything except US, Canada, UK)</option>
		</select>
	</td>
</tr>
*}
<tr>
	<td colspan="2">
		<button type="submit" name="show" value="show" style="cursor: pointer; font-weight: bold;"/><img src="/images/control/eye.png" />&nbsp;Show Report</button>
		&nbsp;&nbsp;&nbsp;<button type="submit" name="export" value="export" style="cursor: pointer;"/><img src="/images/csv_icon_16.gif" />&nbsp;Export to CSV</button>
	</td>
</tr>
</table>
</form>

<table width="800">
<tr>
	<th></th>
	<th>City</th>
	<th style="text-align: right;">New ads count</th>
	<th style="text-align: right;">New ads amount</th>
	<th style="text-align: right;">Renewal count</th>
	<th style="text-align: right;">Renewal amount</th>
	<th style="text-align: right;">Upgrade count</th>
	<th style="text-align: right;">Upgrade amount</th>
	<th style="text-align: right;">Total count</th>
	<th style="text-align: right;">Total amount</th>
</tr>

{foreach from=$location_stats item=r}
<tr style="background-color: {if $r.loc_label == "Total"}khaki{else}#{cycle values="eeeeee,ffffff"}{/if};">
	<td></td>
	<td><strong>{$r.loc_label}</strong></td>
	<td style="text-align: right;">{$r.new_count}</td>
	<td style="text-align: right;">{$r.new_amount|@number_format:2:".":","}</td>
	<td style="text-align: right;">{$r.renewal_count}</td>
	<td style="text-align: right;">{$r.renewal_amount|@number_format:2:".":","}</td>
	<td style="text-align: right;">{$r.upgrade_count}</td>
	<td style="text-align: right;">{$r.upgrade_amount|@number_format:2:".":","}</td>
	<td style="text-align: right;">{$r.location_total_count}</td>
	<td style="text-align: right;">{$r.location_total_amount|@number_format:2:".":","}</td>
</tr>
{/foreach}

</table>
