<style type="text/css">
table.report_table td, table.report_table th {
	padding: 2px 5px 2px 5px;
}
table.report_table th {
	border-bottom: 1px solid #ccc;
}
table.report_table tr:nth-child(odd) td {
	background-color: #f0f0f0;
}
table.report_table tr:nth-child(even) td {
	background-color: white;
}
</style>

<h1>Promocodes usage report</h1>

<form action="" method="get">
<table cellpadding="3" style="background-color:#FFFFFF;border-width:0;color:#000000;font-family:verdana,sans-serif;font-size:12px;">
<tr>
	<td width="130"><b>Since:</b></td>
	<td>
		<select name="since" class="input">
			<option value="">All Time</option>
			<option value="beginning_year" {if $since == 'beginning_year'} selected="selected"{/if}>Beginning Of This Year</option>
		</select>
	</td>
</tr>
<tr>
	<td width="130"><b>Promocode Created By:</b></td>
	<td>
		<select name="promocode_created_by" class="input">
			<option value="">All users</option>
			{html_options options=$promocode_created_by_options selected=$promocode_created_by}
		</select>
	</td>
</tr>
<tr>
	<td width="130"><b>Promocode:</b></td>
	<td><input type="text" name="promocode" value="{$promocode}" class="input" id="promocode" /></td>
</tr>
<tr>
	<td width="130"><b>Only ads posted for free:</b></td>
	<td><input type="checkbox" name="ads_for_free" value="1" class="input" checked="checked" disabled="disabled" /></td>
</tr>
<tr>
	<td width="130"><b>Group by:</b></td>
	<td>
		<select name="group_by">
			<option value="">No grouping</option>
			<option value="account"{if $group_by == "account"}selected="selected"{/if}>By Account</option>
			<option value="promocode"{if $group_by == "promocode"}selected="selected"{/if}>By Promocode</option>
		</select>
	</td>
</tr>
<tr>
	<td colspan="2">
		<button type="submit" name="show" value="show" style="cursor: pointer; font-weight: bold;"/><img src="/images/control/eye.png" />&nbsp;Show Report</button>
		&nbsp;&nbsp;&nbsp;<button type="submit" name="export" value="export" style="cursor: pointer;"/><img src="/images/csv_icon_16.gif" />&nbsp;Export to XLS</button>
	</td>
</tr>
</table>
</form>

<table class="report_table">
<tr>
	<th>Date/Time</th>
	<th>Cl.ad #id</th>
	<th>Promocode</th>
	<th>Promocode created by</th>
	<th>Posted by</th>
	{if $group_by == "account" || $group_by == "promocode"}
		<th>#Purchases</th>
	{/if}
	<th>Price Paid</th>
	<th>Price without promo</th>
	<th>Discount</th>
</tr>

{section name=s loop=$report}
{assign var=r value=$report[s]}
<tr>
	<td>{$r.datetime}</td>
	<td>{$r.clad_id}</td>
	<td>{$r.promocode}</td>
	<td>{$r.promocode_created_by}</td>
	<td>{$r.posted_by}</td>
	{if $group_by == "account" || $group_by == "promocode"}
		<td style="text-align: right;">{$r.post_cnt}</td>
	{/if}
	<td style="text-align: right;">${$r.paid}</td>
	<td style="text-align: right;">${$r.should_have_paid}</td>
	<td style="text-align: right;">${$r.discount}</td>
</tr>
{/section}
</table>
<br />

<table class="report_table" style="border: 1px solid #aaa;">
	<tr><td width="130">Total purchases:</td><td style="text-align: right;">{$total}</td></tr>
	<tr><th width="130">Total discount:</td><th style="text-align: right;">${$total_discount}</td></tr>
	<tr><td width="130">Average discount:</td><td style="text-align: right;">${$average_discount}</td></tr>
</table>
