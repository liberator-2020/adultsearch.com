<div id="example"></div>

<script type="text/javascript">
$(document).ready(function(){
	var dialogOpts = { modal: true, bgiframe: true, autoOpen: false,height: 350,width: 640,draggable: true,resizeable: true};
	$("#example").dialog(dialogOpts);
});


function namethis(id, u) {
	var url = $("form#"+id).serialize() + "&popup=1";
	var li = $("#"+u).val();
	$("#example").load($("#"+u).val(), url, function(){
		$("#example").dialog("option", "width", 890);
		$("#example").dialog("option", "height", 450);
		$("#example").dialog("open");
	});
	return;
}

function takeout(id) {
	$.ajax({
		type: 'get', url: '/mng/review', data: 'out='+id,
		success: function(msg){
			jQuery('#result').html(msg);
			if( msg.indexOf("1") < 0 )
				alert(msg);
			else
				$("#d"+id).remove();
			}
		});
}

function resendEmail(id) {
	$.ajax({
		type: 'get', url: '/mng/review', data: 'resendEmail='+id,
		success: function(msg){
			if( msg.indexOf("1") < 0 )
				alert("Wrong");
			else
				alert("Success");
			}
		});
}
</script>


{section name=s loop=$review}
{assign var=r value=$review[s]}
	<div id="d{$r.review_id}">
		<form method="get" action="" id="r{$r.review_id}">
			<input type="hidden" name="url" value="{$r.url}" id="url{$r.review_id}" />
			<input type="hidden" name="id" value="{$r.id}" />
			<input type="hidden" name="module" value="{$r.module}" />
			<p><b>#{$r.review_id}</b> {$r.comment}</p>
			<input type="button" value="Add Name" onclick="namethis('r{$r.review_id}', 'url{$r.review_id}');" />
			<a href="{$r.edit}" target="_blank">Edit This place</a>
			<input type="button" value="Take this out from the list" onclick="takeout('{$r.review_id}');" />
			{if $admin}
				<input type="button" value="Resend new review email to admin" onclick="resendEmail('{$r.review_id}');" />
			{/if}
		</form>
		<hr/>
	</div>
{/section}


