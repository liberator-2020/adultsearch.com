{if $addnew}
<form method="post" action="">
<table>
 <tr>
  <td>Location:</td>
  <td><select name="loc_id">{$loc_id}</select></td>
 </tr>

 <tr>
  <td>username</td>
  <td><input type="text" name="username" value="{$loc.username}" size="75" /></td>
 </tr>

 <tr>
  <td>password</td>
  <td><input type="text" name="password" value="{$loc.password}" size="75" /></td>
 </tr>

 <tr>
  <td>Consumer Key</td>
  <td><input type="text" name="consumer_key" value="{$loc.consumer_key}" size="75" /></td>
 </tr>

 <tr>
  <td>Consumer Secret:</td>
  <td><input type="text" name="consumer_secret" value="{$loc.consumer_secret}" size="75" /></td>
 </tr>

 <tr>
  <td>Access Token:</td>
  <td><input type="text" name="access_token" value="{$loc.access_token}" size="75" /></td>
 </tr>

 <tr>
  <td>Access Token Secret:</td>
  <td><input type="text" name="access_token_secret" value="{$loc.access_token_secret}" size="75" /></td>
 </tr>

 <tr>
  <td><input type="submit" value="submit"/> - <input type="button" value="cancel" onclick="window.location='/mng/tweet';"></td>
 </tr>
</table></form>
{else}
<a href="?addnew">Add a new location</a>
<table width='750' border=1 style='border-collapse:collapse'>

 <tr>
  <th>Location</th>
  <th>username</th>
  <th>password</th>
  <th>Total Tweet</th>
  <th>Link</th>
  <th>Action</th>
 </tr>

{section name=s loop=$tweet}{assign var=t value=$tweet[s]}
 <tr{if $t.status!=1} style="background-color:red"{/if}>
  <td>{$t.loc_name}, {$t.s}</td>
  <td>{$t.username}</td>
  <td>{$t.password}</td>
  <td>{$t.total_tweet}</td>
  <td><a href='https://twitter.com/{$t.username}' target='_blank'>{$t.username}</a></td>
  <td><a href="?addnew&loc_id={$t.loc_id}">Edit</a> | <a href="?status={if $t.status==1}0{else}1{/if}&loc_id={$t.loc_id}">{if 
$t.status==1}Disable{else}Enable{/if}</a> | 
<a href="?remove={$t.loc_id}" onclick="return confirm('Are you sure ?');">Remove</a></td>
 </tr>
{/section}

</table>
{/if}

