<div data-role="navbar"><ul>
		<li><a href="/adbuild/" data-ajax="false" data-theme="c">Post An Ad</a></li
		><li><a href="/classifieds/myposts" data-ajax="false" data-theme="c">My Ads</a></li
		><li><a href="/account/mypage" class="ui-btn-active" data-ajax="false" data-theme="c">Profile</a></li
		><li><a href="/account/payments" data-ajax="false" data-theme="c">Payments</a></li
		><li><a href="/pm/index" data-ajax="false" data-theme="c">PM</a></li
		><li><a href="{if $ebiz_login_url}{$ebiz_login_url}{else}/account/mywebsite{/if}" data-ajax="false" data-theme="c">Website</a></li
	></ul>
</div>

<h2>Profile</h2>

{if $error}<span class="error">{$error}</span>{/if}

{if $agency}
<div>
	<strong>Agency Guidelines</strong>
	{include "wire_info.tpl" account_id=$account_id}
</div>
{/if}

<label>Username:</label><br />
{$username}<br />
<a href="/account/change-username" class="pm_btn pm_btn_white">Change username</a><br />
<a href="/account/change-password" class="pm_btn pm_btn_white">Change password</a><br />

<label>Email Address</label><br />
{$email}<br />
<a href="/account/change-email" class="pm_btn pm_btn_white">Change email</a><br />

<label>Email Notifications</label><br />
When someone leaves a comment on one of your reviews - {if $newcomment_checked}YES{else}NO{/if}<br />
When someone leaves a comment on a review you commented on - {if $newcommentme_checked}YES{else}NO{/if}<br />
<a href="/account/change-settings" class="pm_btn pm_btn_white">Change settings</a><br />

<label>Number of Forum Posts:</label>{$forum_post}<br />

<label>Number of Reviews: </label>{$review_cnt}<br />

<label>Avatar</label><br />
<img src="{$config_image_server}/avatar/{if $avatar}{$avatar}{else}no_avatar.png{/if}" width="100" height="100" />
<br />
<a href="/account/change-avatar" class="pm_btn pm_btn_white">Change avatar</a><br />

{if $ebiz_login_url}
<label>Escorts.biz</label>
<div>
	<a href="{$ebiz_login_url}" class="pm_btn pm_btn_white" target="_blank">Login to Escorts.biz</a>
</div>
{/if}

<hr />

<div>
	For help please contact:<br />
	{*
	U.S.: 1-702-472-7242<br />
	Canada: 1-778-819-8510<br />
	*}
	<a href="mailto:support@adultsearch.com">support@adultsearch.com</a><br />
</div>
