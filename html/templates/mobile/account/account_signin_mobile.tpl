<form method="post" action="" data-ajax="false" novalidate>
	<input type="hidden" name="login" value="1" />
	{if $message}
		<div class="login_message">
			{$message_full}
		</div>
		<input type="hidden" name="message" value="{$message}" />
	{/if}

	{if $login_error}
		<strong>{$login_error}</strong>
	{/if}
	<div class="ui-field-contain">
		<label for="email">Email:</label>
		<input type="email" name="login_email" id="email" value="" required="required">

		<label for="password">Password:</label>
		<input type="password" name="login_password" id="password" required="required">

		<fieldset data-role="controlgroup" style="width: 100%;" data-mini="true">
			<legend></legend>
			<label for="remember_me">Remember me on this device</label><input type="checkbox" value="1" id="remember_me" name="saveme" checked/>
		</fieldset>
	</div>
	<input type="submit" id="submit" data-theme="b" name="submit" value="Submit">
	<a href="/account/signup" rel="nofollow" data-role="button" data-theme="a" data-ajax="false">I want to register</a>
	<a href="/account/reset" rel="nofollow" data-role="button" data-theme="a" data-ajax="false">I forgot my password</a>
</form>
