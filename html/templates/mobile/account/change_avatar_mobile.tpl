<div data-role="navbar"><ul>
		<li><a href="/adbuild/" data-ajax="false" data-theme="c">Post An Ad</a></li
		><li><a href="/classifieds/myposts" data-ajax="false" data-theme="c">My Ads</a></li
		><li><a href="/account/mypage" class="ui-btn-active" data-ajax="false" data-theme="c">Profile</a></li
		><li><a href="/account/payments" data-ajax="false" data-theme="c">Payments</a></li
		><li><a href="/pm/index" data-ajax="false" data-theme="c">PM</a></li
		><li><a href="{if $ebiz_login_url}{$ebiz_login_url}{else}/account/mywebsite{/if}" data-ajax="false" data-theme="c">Website</a></li
	></ul>
</div>

<h2>Change avatar</h2>

<form method="post" action="" enctype="multipart/form-data" data-ajax="false" novalidate>

{if $error}<span class="error">{$error}</span><br /><br />{/if}

<div class="ui-field-contain">
	<label>Current avatar:</label>
	<img src="{$config_image_server}/avatar/{if $avatar}{$avatar}{else}no_avatar.png{/if}" width="100" height="100" />
	{if $avatar}
		<br />
		<label></label>
		<button type="submit" name="delavatar" class="ui-btn ui-btn-inline">Delete avatar</button>
	{/if}
</div>

<div class="ui-field-contain">
	<label>New avatar:</label>
	<input name="avatar" type="file" />
</div>

<input type="submit" data-theme="b" name="submit" value="Submit">

</form>

{include "mobile/account/footer_mobile.tpl"}
