<div data-role="navbar"><ul>
		<li><a href="/adbuild/" data-ajax="false" data-theme="c">Post An Ad</a></li
		><li><a href="/classifieds/myposts" data-ajax="false" data-theme="c">My Ads</a></li
		><li><a href="/account/mypage" class="ui-btn-active" data-ajax="false" data-theme="c">Profile</a></li
		><li><a href="/account/payments" data-ajax="false" data-theme="c">Payments</a></li
		><li><a href="/pm/index" data-ajax="false" data-theme="c">PM</a></li
		><li><a href="{if $ebiz_login_url}{$ebiz_login_url}{else}/account/mywebsite{/if}" data-ajax="false" data-theme="c">Website</a></li
	></ul>
</div>

<h2>Change email</h2>

<form method="post" action="" data-ajax="false" novalidate>

{if $error}<span class="error">{$error}</span><br /><br />{/if}

Your current email is <strong>{$currentEmail}</strong>.<br />

<div class="ui-field-contain">
	<label>New email:</label>
	<input type="text" name="email" value="{$email}" required="required">
</div>

<input type="submit" data-theme="b" name="submit" value="Submit">

</form>

{include "mobile/account/footer_mobile.tpl"}
