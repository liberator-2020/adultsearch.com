<div data-role="navbar"><ul>
		<li><a href="/adbuild/" data-ajax="false" data-theme="c">Post An Ad</a></li
		><li><a href="/classifieds/myposts" data-ajax="false" data-theme="c">My Ads</a></li
		><li><a href="/account/mypage" class="ui-btn-active" data-ajax="false" data-theme="c">Profile</a></li
		><li><a href="/account/payments" data-ajax="false" data-theme="c">Payments</a></li
		><li><a href="/pm/index" data-ajax="false" data-theme="c">PM</a></li
		><li><a href="{if $ebiz_login_url}{$ebiz_login_url}{else}/account/mywebsite{/if}" data-ajax="false" data-theme="c">Website</a></li
	></ul>
</div>

<h2>Change settings</h2>

<form method="post" action="" data-ajax="false" novalidate>

{if $error}<span class="error">{$error}</span><br /><br />{/if}

<label>Email Notifications</label>

<div class="ui-field-contain">
	<fieldset data-role="controlgroup" style="width: 100%;">
		<label for="newcomment">When someone leaves a comment on one of your reviews</label><input type="checkbox" id="newcomment" name="newcomment" value="1" {if $newcomment_checked}checked="checked"{/if} />
	</fieldset>

	<fieldset data-role="controlgroup" style="width: 100%;">
		<label for="newcommentme">When someone leaves a comment on a review you commented on</label><input type="checkbox" id="newcommentme" name="newcommentme" value="1" {if $newcommentme_checked}checked="checked"{/if} />
	</fieldset>
</div>

<input type="submit" data-theme="b" name="submit" value="Submit">

</form>

{include "mobile/account/footer_mobile.tpl"}
