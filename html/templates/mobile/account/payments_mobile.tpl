<style type="text/css">
	#edit-payment-method{
		background-color: #5bb75b;
		width: 50%;
		color: #FFFFFF;
		margin: 10px auto 0 auto;
	}
</style>
<div data-role="navbar"><ul>
		<li><a href="/adbuild/" data-ajax="false" data-theme="c">Post An Ad</a></li
		><li><a href="/classifieds/myposts" data-ajax="false" data-theme="c">My Ads</a></li
		><li><a href="/account/mypage" data-ajax="false" data-theme="c">Profile</a></li
		><li><a href="/account/payments" class="ui-btn-active" data-ajax="false" data-theme="c">Payments</a></li
		><li><a href="/pm/index" data-ajax="false" data-theme="c">PM</a></li
		><li><a href="{if $ebiz_login_url}{$ebiz_login_url}{else}/account/mywebsite{/if}" data-ajax="false" data-theme="c">Website</a></li
	></ul>
</div>

{if $error}<span class="error">{$error}</span>{/if}

<form method="post" action="" enctype="multipart/form-data" data-ajax="false">
	{if $cards|count > 0}
		<div>
			<a id="edit-payment-method" class="ui-btn" data-ajax="false"  href="/account/payment_methods">Edit Payment Method</a>
		</div>
	{/if}
	 <div>
		  <h2>Active subscriptions</h2>
		  {if $subscriptions|count > 0}
				<table>
				<tr>
					 <th>Subject</th>
					 <th>Subscription Id#</th>
					 <th>Amount</th>
					 <th>Card</th>
					 <th>Next renewal date</th>
					 <th/>
				</tr>
			{foreach from=$subscriptions item=s}
				<tr>
					 <td>{$s.subject}</td>
					 <td>{$s.subscription_id}</td>
					 <td>${$s.amount}</td>
					 <td>{$s.card_label}</td>
					 <td>{$s.next_renewal_date}</td>
					 <td><a href="?action=subscription_cancel&subscription_id={$s.subscription_id}" class="pm_btn pm_btn_white" data-ajax="false">Cancel</a></td>
				</tr>
				{/foreach}
				</table>
		  {else}
				<em>No active subscriptions.</em>
		  {/if}
	 </div>
	 <div>
		  <h2>Transactions</h2>
		  {if $transactions|count > 0}
				<table>
				<tr>
					 <th>Date/Time</th>
					 <th>Subject</th>
					 <th>Transaction Id#</th>
					 <th>Subscription Id#</th>
					 <th>Amount</th>
					 <th>Card</th>
				</tr>
			{foreach from=$transactions item=t}
				<tr>
					 <td>{$t.stamp|date_format:"%m/%d/%Y %l:%M %p %Z"}</td>
					 <td>{$t.subject}</td>
					 <td>{$t.trans_id}</td>
					 <td>{$t.subscription_id}</td>
					 <td>${$t.amount}</td>
					 <td>{$t.card_label}</td>
				</tr>
				{/foreach}
				</table>
		  {else}
				<em>No transactions.</em>
		  {/if}
	 </div>
	 <br />
	 <div class="help">
		  For help please contact:<br />
		  U.S. &amp; Canada: <a href="tel:+17029351688">1-702-935-1688</a><br />
		{if $agency}
				<a href="mailto:agency@adultsearch.com">agency@adultsearch.com</a>
		  {else}
			  <a href="mailto:support@adultsearch.com">support@adultsearch.com</a>
		{/if}
	 </div>
</form>

