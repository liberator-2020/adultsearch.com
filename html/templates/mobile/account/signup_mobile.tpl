<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<h1>Sign Up</h1>

{if $error}
	<strong style="color:#ff0000;">{$error}</strong>
{/if}

<form action="signup?step=1" method="post" data-ajax="false">
    <input type="hidden" name="submit" value="1" />
	<div class="ui-field-contain">
        <label for="email">Your email: <span class="required">*</span></label>
        <input type="email" name="email" id="email" value="{$email}" required="required">
		e.g. myname@example.com. This will be used to sign-in to your account.
		<br style="clear: both;" />

		<label for="email_confirm">Confirm email: <span class="required">*</span></label>
		<input type="text" name="email_conf" id="email_confirm" value="{$email_conf}" />
		<br style="clear: both;" />

		<label for="username">Create username: <span class="required">*</span></label>
		<input name="username" value="{$username}" id="username" type="text" >
		<br style="clear: both;" />

		<label for="password">Choose password: <span class="required">*</span></label>
		<input type='password' name="password" id="password" value="{$password}" >
		<br style="clear: both;"/>

		{if $error_password eq "empty"}
			<br /><strong style='color:#FF0000'>Enter your password please.</strong>
		{/if}

		<label for="password_conf">Re-enter password: <span class="required">*</span></label>
		<input type='password' name="password_conf" id="password_conf" value="{$password_conf}" />
		<br style="clear: both;" />

		{if $error_password eq "not_match"}<br /><strong style='color:#FF0000'>Confirm your password please.</strong>{/if}

		<hr size='1' />
		<strong>Verification:</strong><span class="reqired">*</span>
		<div class="g-recaptcha" data-sitekey="{$config_recaptcha_site_key}"></div>

		{*
		{if !$captcha_ok}
			<hr size='1' />
			<strong>Word Verification:</strong><span class="required">*</span>
			<br /><span> Type the characters you see in the picture below. </span>
			<br /><img src="/captcha.php?{$time}" border="0" alt="security" width='200'  height='70'/><br />
			<input name="spam" class="searchinput" /><br style="clear: both;" />
			Letters are not case-sensitive
			<br style="clear: both;" />
		{else}
			<input type="hidden" name="spam" value="{$spam}" />
		{/if} 
		*}

		<br />
		<hr size='1' />
		<strong>Terms of Service:<span class="required">*</span></strong><br />
		Please check the Adult Search Account information you've entered above (feel free to change anything you like), and review the Terms of Service.
		<br /><br />
		By clicking on 'I accept' below you are agreeing to the <a href="/privacy-policy" target="_blank" data-ajax="false">Privacy Policy</a>.

		<br /><br />
		<input type="submit" id="submit" data-theme="b" name="submit" value="I accept. Create my account.">
	</div>
</form>

<br />
