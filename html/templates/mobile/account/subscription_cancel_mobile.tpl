<div data-role="navbar"><ul>
		<li><a href="/classifieds/myposts" data-ajax="false" data-theme="c">My Ads</a></li
		><li><a href="/account/mypage" data-ajax="false" data-theme="c">Profile</a></li
		><li><a href="/account/payments" class="ui-btn-active" data-ajax="false" data-theme="c">Payments</a></li
		><li><a href="/pm/index" data-ajax="false" data-theme="c">PM</a></li
		><li><a href="{if $ebiz_login_url}{$ebiz_login_url}{else}/account/mywebsite{/if}" data-ajax="false" data-theme="c">Website</a></li
	></ul>
</div>

{if $error}<span class="error">{$error}</span>{/if}

<form method="post" action="" enctype="multipart/form-data" data-ajax="false">
	<h2>Cancel subscription #{$subscription_id} ?</h2>
	<p>
		Do you really want to cancel subscription #{$subscription_id} ?
		{if $message}
			<br />
			{$message}
		{/if}
	</p>
	<input type="hidden" name="action" value="subscription_cancel" />
	<input type="hidden" name="subscription_id" value="{$subscription_id}" />
	<button type="submit" name="go_live" value="1">Yes, cancel this subscription</button>
	<button type="submit" name="go_back" value="1">No, go back</button>
    <br />
    <div class="help">
        For help please contact:<br />
        U.S. &amp; Canada: <a href="tel:+17029351688">1-702-935-1688</a><br />
        <a href="mailto:support@adultsearch.com">support@adultsearch.com</a>
    </div>
</form>

