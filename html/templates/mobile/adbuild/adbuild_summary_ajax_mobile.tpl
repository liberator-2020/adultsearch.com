<div id="absa_mobile" {if $localert}data-localert="{$localert}"{/if}>
	
{if !$editonly}
	<h3>Summary</h3>
	{$cat_name}, 30-days
	<ul>
	{section name=s loop=$selectedloc}
	{assign var=sl value=$selectedloc[s]}
		<li>
			<span class="remove"><a href="#" rel="{{$sl.loc_id}}" class="cart pm_btn pm_btn_coral" onclick="cart_process('{$sl.type}', '{$sl.loc_id}', '{$ad_id}'); return false;">X</a></span> 
			{if $sl.post_price>0}${$sl.post_price} {/if}{$sl.loc_name}
		</li>
	{sectionelse}
		<span class="error">A Location Is Required</span>
	{/section}
	</ul>

	{if $total}
		<strong style="font-size: 1.2em;">Total: ${$total}{if $exchange} ({$exchange}){/if}</strong><br />
	{/if}
	<br />
{/if}

</div>
