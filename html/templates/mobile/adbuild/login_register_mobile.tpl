<h2>Login Or Register to Checkout &amp; Publish Your Ad</h2>

<form method="post" action="" data-ajax="false">

    <input type="hidden" name="login" value="1" />
    <input type="hidden" name="origin" value="{$config_site_url}/adbuild/step4?ad_id={$ad_id}" />

	{if $error}<span class="error">{$error}</span>{/if}
    {if !$error && $login_error}<span class="error">{$login_error}</span>{/if}

	1. If you already have account at AdultSearch, please login in the form below:<br />
	<h3>Login</h3>

	<span class="bluelabel">Email</span>
	<input value="{$login_email}" name="login_email"/>
	
	<span class="bluelabel">Password</span>
	<input type="password" value="{$login_password}" name="login_password" />

	<button type="submit" class="pm_btn pm_btn_coral" data-role="none">Continue</button>&nbsp;&nbsp;
	<a href="http://adultsearch.com/account/reset" class="pm_btn_small pm_btn_white" data-ajax="false">Forgot Password?</a><br />

	<br />	
	2. If you don't yet have an account at AdultSearch, please fill in form below:
	<br /><br />
	
	<span class="bluelabel">Your Email</span><br />
	This email is only for your account administration, it will not be displayed on your ad
	<input value="{$account_email}" name="account_email"/>

	<span class="bluelabel">Pick a Username</span>
	<input value="{$account_username}" name="account_username"/>

	<span class="bluelabel">Create Password</span>
	<input type="password" name="account_password"/>

	<span class="bluelabel">Retype Password</span>
	<input type="password" name="account_password2"/>

	<button type="submit" class="pm_btn pm_btn_coral" data-role="none">Continue</button>

	<div id="calculator">
		{include file="mobile/adbuild/adbuild_summary_ajax_mobile.tpl"}
	</div>

</form>
