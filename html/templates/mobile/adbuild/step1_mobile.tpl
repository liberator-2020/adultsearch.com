<h2>Posting new ad - step 1</h2>

{*
<div><span class="error">Due to maintenance of our servers, it is not possible post a new ad today. Please come back tomorrow</span></div>
*}

<form method="post" action="/adbuild/step2" id="step1" data-ajax="false">
{if $ad_id}<input type="hidden" name="ad_id" value="{$ad_id}" />{/if}

{if $error}
    <br />
    <div class="error">{$error}</div>
    <br />
{/if}

<div id="pickcategory" class="error" style="display:none">You must select a category to continue.</div>
<fieldset data-role="controlgroup">
	<legend>Select A Section For Your Ad:</legend>
	<input type="radio" name="category" id="radio-1" value="1" {if $adbuild_type == 1}checked="checked"{/if} />
	<label for="radio-1">Female Escort</label>
	<input type="radio" name="category" id="radio-2" value="2" {if $adbuild_type == 2}checked="checked"{/if} />
	<label for="radio-2">TS / TV Shemale Escort</label>
	<input type="radio" name="category" id="radio-6" value="6" {if $adbuild_type == 6}checked="checked"{/if} />
	<label for="radio-6">Female Bodyrubs</label>
</fieldset>
<br />

<div class="ui-body ui-body-a">
<h4>You Agree to the Following When Posting on AdultSearch:</h4>
<ul>
	<li>I will not post obscene or lewd and lascivious graphics or photographs which depict genitalia or actual or simulated sexual acts</li>
	<li>I will not post any solicitation directly or in "coded" fashion for any illegal service exchanging sexual favors for money or other valuable consideration</li>
	<li>I will not post any material on the Site that exploits minors in any way</li>
	<li>I will not post any material on the Site that in any way constitutes or assists in human trafficking</li>
	<li>I am <strong>at least 21 years of age</strong> or older and not considered to be a minor in my state of residence</li>
	<li>Any post containing images or contact information that we deem to be misleading or invalid for the category in which it was posted will be removed with no refund</li>
	<li>Any post exploiting a minor in any way will be subject to criminal prosecution and will be reported to the <a href="http://www.cybertipline.com" target="_blank" rel="nofollow">CyberTipline</a>. The poster will be caught and the police will prosecute the poster to the full extent of the law</li>
	<li>Any post with terms or misspelled versions of terms implying an illegal service will be rejected. Examples of such terms include without limitation: 'greek', ''gr33k", bbbj', 'blow', 'trips to greece', etc. You may not post ads including following content; "exchange of sexual favors for money", "use code words such as 'greek', 'bbbj', 'blow', 'trips to greece', etc.", "post obscene images, e.g. explicit genitalia, sex acts, erect penises, etc.", "post content which advertises an illegal service"</li>
	<li>Postings violating these rules and our Terms of Use are subject to removal without refund</li>
	<li style="color: red;">We do not allow any ads of a spamming nature. We will share data with Slixa and Eros regarding any violators of these rules</li>
</ul>
</div>
<br />

<div class="ui-body ui-body-a">
	<span><img width="15" height="18" id="lock" alt="SSL" src="{$config_site_url}/images/i_secure.png" />
	All transactions are discreet &amp; secured with SSL Certificate encryption.</span><br />
	<div>
		<img width="46" height="25" id="visa" alt="Visa" src="{$config_site_url}/images/i_visa.png" />
		<img width="46" height="25" id="mc" alt="Mastercard" src="{$config_site_url}/images/i_mastercard.png" />
{*
  		<img width="46" height="25" id="amex" alt="American Express" src="{$config_site_url}/images/i_amex.png" />
		<img width="46" height="25" id="discover" alt="Discover" src="{$config_site_url}/images/i_discover.png" />
*}
	</div>
	<div class="cards">
		<span id="siteseal">
			<script type="text/javascript" src="https://seal.starfieldtech.com/getSeal?sealID=6pvUdl4f9Rtkdbd9ezSTPMwOCVzfH3x9ZkWQUkJFk3UajmExXMp4hYJ9"></script>
		</span>
	</div>
</div>
<br />

<strong>By clicking &quot;Agree and Continue&quot;, you agree to abide rules above as well as AdultSearch's terms of use.</strong><br />
<input type="submit" class="pm_btn pm_btn_coral" name="submit" value="Agree and Continue" data-role="none" />
<br />

<hr />
<div>
For help please contact:<br /><br />
<a href="mailto:support@adultsearch.com">support@adultsearch.com</a><br />
</div>
		
{$csrf}
</form>

<script type='text/javascript'>
</script>
