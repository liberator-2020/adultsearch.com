<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>
<link rel="stylesheet" type="text/css" href="/js/intl-tel-input/css/intlTelInput.min.css?20190917">
<script type="text/javascript" src="/js/intl-tel-input/js/intlTelInput.min.js?20190917"></script>

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script>
	CKEDITOR.env.isCompatible = true;	//enabling ckeditor on android browser
</script>
<style type="text/css">
.error a {
	color: red;
}
/* mobile field fixes */
.intl-tel-input {
	width: 100%;
}
div.iti {
	width: 100%;
}
/* bootstrap and jquery mobile fix */
input[type=checkbox], input[type=radio] {
	margin: 0px;
}
select option {
	color: black !important;
}
</style>

{if $editonly}
	<h2>Edit ad #{$ad_id}</h2>
{else}
	<h2>Posting new ad - step 3</h2>
{/if}

<div>
	{if $spam}
	<span class="error">
	You can't post an ad since you are using blacklisted contact information.	
	</span>
	{elseif $error}
		<span class="error">
		Oops! Correct the errors in these fields to continue: 
		{section name=r loop=$error}{assign var=e value=$error[r]}
			<li>{$e}</li>
		{/section}
		</span>
	{elseif $updated}
		<p class="error">Your edits have been saved</p>
	{/if}

	<span class="required"></span>Fields are required. All optional info will <strong>help you be found faster.</strong> We allow your client to search by hair color, services, rates, etc.	If you want to show up more often in client results, fill in the optional fields.
</div>

<form name="page3" action="" method="post" data-ajax="false">
	<input type="hidden" name="updatethead" value="1"/>	
	<input type="hidden" name="ad_id" value="{$ad_id}"/>

	<div{if $erroravl} id="error-available">{/if}
		<label class="bluelabel">Available To</label>
		{if $erroravl}<span class="error">{/if}
		<label><input type="checkbox" name="avl_men" value="1"{if $avl_men} checked="checked"{/if}/> Men</label>
 		<label><input type="checkbox" name="avl_women" value="1"{if $avl_women} checked="checked"{/if}/> Women</label>
		<label><input type="checkbox" name="avl_couple" value="1"{if $avl_couple} checked="checked"{/if}/> Couples</label>
		{if $erroravl}<br/>You have to pick at least 1 option</span>{/if}
	</div>

	<div{if $errorincall} id="error-call"{/if}> 
		<label class="bluelabel required">Incall/Outcall</label>
		You must indicate incall, outcall or both.	Rates are optionally encouraged; our web users can search by rates.<br />
		{if $errorincall}<span class="error">{/if}
		<label><input type="checkbox" value="1" onClick="$('#ratein').toggle();" name="incall" {if $incall}checked="checked"{/if}/> Incall</label>
		<label><input type="checkbox" value="2" onClick="$('#rateout').toggle();" name="outcall" {if $outcall}checked="checked"{/if}/>Outcall</label>

		<div class="alt" id="ratein" {if !$incall}style="display: none;"{/if}> Incall Rates (optional)<br />
		{*
		Half hour: {if $currency_sign}{$currency_sign}{else}${/if}<input maxlength="4" type="text" name="incall_rate_hh" value="{$incall_rate_hh}" />
		*}
		Hour: {if $currency_sign}{$currency_sign}{else}${/if}<input maxlength="4"type="text" name="incall_rate_h" value="{$incall_rate_h}" />
		{*
		2 hours: {if $currency_sign}{$currency_sign}{else}${/if}<input maxlength="4"type="text" name="incall_rate_2h" value="{$incall_rate_2h}" />
		*}
		Overnight: {if $currency_sign}{$currency_sign}{else}${/if}<input maxlength="5" type="text" name="incall_rate_day" value="{$incall_rate_day}" />
		</div>

		<div class="alt" id="rateout" {if !$outcall}style="display: none;"{/if}> Outcall Rates (optional)<br />
		{*
		Half hour: {if $currency_sign}{$currency_sign}{else}${/if}<input maxlength="4" type="text" name="outcall_rate_hh" value="{$outcall_rate_hh}" />
		*}
		Hour: {if $currency_sign}{$currency_sign}{else}${/if}<input maxlength="4"type="text" name="outcall_rate_h" value="{$outcall_rate_h}" />
		{*
		2 hours: {if $currency_sign}{$currency_sign}{else}${/if}<input maxlength="4"type="text" name="outcall_rate_2h" value="{$outcall_rate_2h}" />
		*}
		Overnight: {if $currency_sign}{$currency_sign}{else}${/if}<input maxlength="5" type="text" name="outcall_rate_day" value="{$outcall_rate_day}" />
		</div>
		{if $errorincall}
			You must select either outcall and/or incall.</span>
		{/if}
	</div>

	<div{if $errorvisiting} id="error-visiting"{/if}>
		<label class="bluelabel">Visiting? </label>
		Dates are optional. If you have specific dates per location, include them in your ad text.
		{if $errorvisiting}<span class="error">{/if}
		<label><input id="visiting" name="visiting" value="1" type="checkbox" onClick="$('#visitdates').toggle();" {if $visiting}checked="checked"{/if}/>Yes, you are visiting this location.</label>
		<div class="alt" id="visitdates" {if !$visiting}style="display: none;"{/if}> Dates (optional):<br />
			<input type="text" id="visiting_from" name="visiting_from" value="{$visiting_from}" />
			-
			<input type="text" id="visiting_to" name="visiting_to" value="{$visiting_to}" />
			<br />
		</div>
		{if $errorvisiting}</span>{/if}
	</div>

	<div>
		<label class="bluelabel">Location Detail</label>
		e.g. Downtown
		<input name="location" maxlength="30" type="text" value="{$location}" />
	</div>

	{if !in_array($cat_id, array(3,4,5,7,12,13,14))}
		<div>
			<label class="bluelabel">Girlfriend Experience</label>
			<label><input type="checkbox" name="gfe" value="1" {if $gfe}checked="checked"{/if}/>Yes, I offer Girlfriend Experience</label>
			<label><input type="checkbox" name="gfe_limited" value="1" {if $gfe_limited}checked="checked"{/if}/>Yes, I offer Limited Girlfriend Experience</label>
		</div>
	{/if}

	{if $cat_id == 6}
		<div>
		<label><input type="checkbox" name="tantra" value="1" {if $tantra}checked="checked"{/if} />	 Yes, I provide tantra massage</label>
		</div>
	{/if}

	{if $cat_id != 14}
		<div>
			<label class="bluelabel">Fetish Session Activities</label>
			Optional, select all that apply
			<label><input type="checkbox" name="fetish_dominant" value="1" {if $fetish_dominant}checked="checked"{/if} /> Dominant</label>
			<label><input type="checkbox" name="fetish_submissive" value="1" {if $fetish_submissive}checked="checked"{/if} /> Submissive</label>
			<label><input type="checkbox" name="fetish_swith" {if $fetish_swith}checked="checked"{/if} value="1" /> Switch</label>
		</div>
	{/if}

	{if $cat_id == 13}
		<div>
			<label class="bluelabel">BDSM	Session Activities</label>
			Optional, select all that apply
			<select multiple="multiple" name="fetish[]" size="5">
				{$fetish}
			</select>
		</div>
	{/if}
		

	{if !in_array($cat_id, array(5,14))}
		<div>
			<label class="bluelabel required">Age</label>
			{if $errorage}<span id="error-age" class="error">{/if}
				<select name="age" id="quick_info_age">{html_options options=$age_options selected=$age}</select>
			{if $errorage}Must be over 21.</span>{/if}
		</div>

		{if $cat_id != 12}
			<div>
				<label class="bluelabel">Ethnicity</label>
				<select name="ethnicity">
					{$ethnicity}
				</select>
			</div>

			<div>
				<label class="bluelabel">Language(s) Spoken</label>
				{if $errorlanguage}<span id="error-language" class="error">{/if}
				<select name="language[]" style="margin-bottom: 10px;">
					{html_options options=$language_options selected=$lang}
				</select>
				{if $errorlanguage}You can pick maximum 4 languages.</span>{/if}
			</div>
		
			<div>
				<label class="bluelabel">Height ({if $metric}meter{else}feet{/if})</label>
				<span id="height0">
					<select name="height_feet">
						{$height_feet}
					</select>
				<label class="bluelabel">Height ({if $metric}Centimeter{else}Inches{/if})</label>
					<select name="height_inches">
						{$height_inches}
					</select>
				<span id="height1" style="display:none;">
					<input name="quick_info_height_sm" value="168" style="width:35px;" type="text" />cm
				</span>
			</div>

			<div>
				<label class="bluelabel">Weight ({if $metric}kg{else}lbs{/if})</label>
				<input maxlength="3" type="text" name="weight" value="{$weight}" />
			</div>

			<div>
				<label class="bluelabel">Eye Color</label>
				<select name="eyecolor">
					{$eyecolor}
				</select>
			</div>

			<div>
				<label class="bluelabel">Hair color</label>
				<select name="haircolor">
					{$haircolor}
				</select>
			</div>

			<div>
				<label class="bluelabel">Build</label>
				<select name="build">
					{$build}
				</select>
			</div>

			{if !in_array($cat_id, array(3,4))}
				<div>
					<label class="bluelabel">Measurements</label>
					<input maxlength="3" metric="0" type="text" name="measure_1" value="{$measure_1}" />
					-
					<input maxlength="3" type="text" name="measure_2" value="{$measure_2}" />
					-
					<input maxlength="3" type="text" name="measure_3" value="{$measure_3}" />
					({if $metric}cm{else}inches{/if})
				</div>
				<div>
					<label class="bluelabel">Cup size</label>
					<select name="cupsize">
						{$cupsize}
					</select>
				</div>
			{/if}

			{if in_array($cat_id, array(2,3,4,7))}
				<div>
					<label class="bluelabel">Penis size (inches)</label>
					<select name="penis_size">
						{$penis_size}
					</select>
				</div>
			{/if}

			{if in_array($cat_id, array(1,6))}
				<div>
					<label class="bluelabel">Kitty</label>
					<select name="kitty">
						{$kitty}
					</select>
				</div>
			{/if}

			<div>
				<label class="bluelabel">Are you a porn star?</label>
				<label><input type="checkbox" name="pornstar" value="1" {if $pornstar}checked="checked"{/if} />Yes, I am.</label>
			</div>

			{if in_array($cat_id, array(1,6))}
				<div>
					<label class="bluelabel">Currently Pregnant?</label>
					<label><input type="checkbox" name="pregnant" value="1" {if $pregnant}checked="checked"{/if} />Yes, I am.</label>
				</div>
			{/if}
		{/if}
	{/if}		

	<div{if $errorname} id="error-name" {/if}>
		<label class="bluelabel required">Name on Ad</label>
		Working name, 40 characters max
		{if $errorname}<span class="error">{/if}
		<input name="firstname" maxlength="40" type="text" value="{$firstname}" />
		{if $errorname}You must include your working name</span>{/if}
	</div>

	<div{if $errortitle} id="error-introduction"{/if}>
		<label class="bluelabel required">Introduction</label>
		Your "tagline"
		{if $errortitle}<span class="error">{/if}
		<input maxlength="70" size="40" name="title" type="text" value="{$title}" />
		{if $errortitle}<br />You must include an introduction</span>{/if}
	</div>

	<div{if $errorcontent} id="error-adtext"{/if}>
		<label class="bluelabel required">Ad text</label>
		You may not post ads stating an exchange of sexual favors for money or use code words such as 'greek', 'bbbj', 'blow', 'trips to greece', etc. You may not post content which advertises an illegal service.
		{if $errorcontent}<span class="error">You must include some ad text</span>{/if}
		<textarea class="bio ckeditor" id="message1" name="content" rows="24" cols="38">{$content}</textarea>
	</div>

	<div id="email"{if $reply==2} style="display:none"{/if}>
		<label class="bluelabel required">Email Address on Your Ad</label>
		{if $erroremail}<span id="error-email" class="error">{/if}
		<input maxlength="70" size="30" name="email" type="text" value="{$email}" />
		{if $erroremail} You must include an email</span>{/if}
	</div>
	<div>
		<label class="bluelabel">Receive email?</label>
		We can make your email anonymous and forward you viewer responses or post your email address. If you do not want to receive email, be sure to include a phone number in your ad.
		<select name="reply" id="reply">
			<option value="1">Yes, make my email anonymous and	forward	inquiries to me.</option>
			<option value="3"{if $reply==3} selected="selected"{/if}>Yes, include the email address above.</option>
			<option value="2"{if $reply==2} selected="selected"{/if}>No, I don't want to receive any email inquiries.</option>
		</select>
	</div>
	 
	<div{if $errorphone} id="error-phone"{/if}>
		<label class="bluelabel">Phone Number</label>
		Customer contact number. 444-555-6666 format.
		<input id="phone1full" type="hidden" name="phone_full" value="">
		<input class="phone" id="phone1id" size="15" maxlength="18" type="text" name="phone1" value="{$phone1}" />
		<div id="phone_verify_result" style="display: inline-block;"></div>
		{if $errorphone}<span id="error-phone" class="error">You must enter valid phone number.</span>{/if}
		<input type="hidden" name="phone_verify_result_val" id="phone_verify_result_val" value="" />
	</div>

	{if $phone_validation_needed}
	<div id="error-validation">
		<label class="bluelabel">SMS Phone verification code</label>
		Enter code from SMS message.
		<span class="error" style="background-color: #F60; color: white;">
			<input size="6" maxlength="6" type="text" id="sms_code"  name="sms_code" value="" />
			Enter code from SMS message.
		</span>
	</div>
	{/if}

	<div>
		<label class="bluelabel">Your Website</label>
		e.g. MyWebsite.com. You do not need to include http:// or www
		<input maxlength="70" size="50" name="website" id="ad_website" type="text" value="{$website}"/><br />
	</div>

	<div>
		<label class="bluelabel">Credit Cards You Accept</label>
		<label><input type="checkbox" name="payment_visa" value="1" {if $payment_visa}checked="checked"{/if}/>Visa/MasterCard</label>
		<label><input type="checkbox" name="payment_amex" value="1" {if $payment_amex}checked="checked"{/if} />American Express</label>
		<label><input type="checkbox" name="payment_dis" value="1" {if $payment_dis}checked="checked"{/if}/>Discover Card</label>
	</div>

	<div>
		<label class="bluelabel">TER ID</label>
		If you have been reviewed on TheEroticReview.com, include your ID and we'll link you.
		<input type="text" name="ter" value="{$ter}"/>
	</div>

	<div>
		<label class="bluelabel">Facebook Profile Link</label>
		If you have profile on Facebook, include link to your profile and we'll display it on your ad.
		<input type="text" name="facebook" value="{$facebook}" size="50"/>
	</div>

	<div>
		<label class="bluelabel">Twitter Profile Link</label>
		If you have profile on Twitter, include link to your profile and we'll display it on your ad.
		<input type="text" name="twitter" value="{$twitter}" size="50"/>
	</div>

	<div>
		<label class="bluelabel">Google Profile Link</label>
		If you have profile on Google, include link to your profile and we'll display it on your ad.
		<input type="text" name="google" value="{$google}" size="50"/>
	</div>

	<div>
		<label class="bluelabel">Instagram Profile Link</label>
		If you have profile on Instagram, include link to your profile and we'll display it on your ad.
		<input type="text" name="instagram" value="{$instagram}" size="50"/>
	</div>

	<div>		
		<label class="bluelabel">Upload Your Photos</label>
		<iframe src ="/adbuild/upload?ad_id={$ad_id}&frame=true" width="100%" height="600"></iframe>
		<input type="hidden" name="last_image_stamp" value="{$last_image_stamp}" />
	</div>


	<div class="ui-body ui-body-a">
		<h3>Upload Your Videos</h3>
		<span style="color: red; font-weight: bold; font-size: 1.1em;">Naked videos &amp; genitalia are NOT ALLOWED. This includes topless video.</span>
		<div id="video_preview_div" style="display: none;"></div>
		<div id="video_processing_div" style="display:none;">
			<img src="/images/processing_50.gif" alt="Processing" style="float:left"/>
			<div>
				Your video is being optimized for mobile devices. It might take up to 1 minute.<br />
				Meanwhile you can continue with editing your profile or save the changes. Video will automatically display when the processing is finished.
			</div>
			<br style="clear: both;" />
		</div>
		<div id="video_processing_error" style="display:none;">
			<img src="/images/upload_video_error.png" alt="Error uploading video"  style="float:left;">
			<div style="padding-top:15px">
				Error uploading video. Please try again.
			</div>
		</div>
		<br style="clear: both;" />
		<div id="video_upload_div">
			<p style="font-size: 0.9em; margin-bottom: 2px;">
				Max. video length is 20 sec or 10MB. This only uploads video files.<br />
			</p>
			<span class="btn btn-success fileinput-button">
				<i class="glyphicon glyphicon-plus"></i>
				<span>Upload Video</span>
				<input type="file" id="video" name="video" accept="video/*" data-role="none">
			</span>
			<div id="progress_video" class="progress" style="display: none;"><div class="progress-bar progress-bar-success"></div></div>
		</div>
	</div>

	{if !$editonly}

		{*
		<div class="ui-body ui-body-a">
			<h3>Schedule your reposts, hassle-free.</h3>
			Every time your ad is reposted, it appears at the top of its category list. Maximize your exposure exactly when you tell us to.<br />
			<div id="repost">
				<span class="title2">Select the option that works for you:</span>

				<fieldset data-role="controlgroup">
					<input name="repost" type="radio" value="0" {if !$repost}checked="checked"{/if}/>
					<label>No reposting</label>

					<input name="repost" type="radio" value="1" {if $recurring}checked="checked"{/if}/>
					<label>Hassle Free Daily Reposting</label>
						<blockquote>
							Every day at
							<select name="auto_renew_time2">
								{html_options values=$art_values output=$art_names selected=$auto_renew_time}
							</select>
							{if $timezone}{$timezone}{else}PST{/if}
							for $49.99 per month per city. <br />
							<br />
						Price includes all of your	locations. <span class="orange">70%</span> less per month as compared to daily posting rates! This is a monthly recurring charge.	You may cancel this at any time by clicking the &quot;My Ads&quot; link at the top of this page.
						</blockquote>

					<input name="repost" type="radio" value="2" {if $repost&&!$recurring}checked="checked"{/if}/>
					<label>Or Repost Exactly When You Want You To</label> 
					<blockquote> Every
					<select name="auto_renew_fr" onchange="document.f.auto_renew.checked=true;">
					{$auto_renew_fr}
					</select>
					at
					<select name="auto_renew_time" onchange="document.f.auto_renew.checked=true;">
						{html_options values=$art_values output=$art_names selected=$auto_renew_time}
					</select>
					{if $timezone}{$timezone}{else}PST{/if}
					<select name="auto_repost" id="autoRepostn">
					{$autoRepostS}
					</select>
					<br />
					<br />
					Price includes all of your	locations. <span class="orange">50%</span> less per day as compared to daily posting rates! This is a single, non-recurring charge. You may purchase additional reposts at any time by clicking the &quot;My Ads&quot; link at the top of this page.
					</blockquote>
				</fieldset>
			</div>
		</div>
		<br />
		*}

		<div class="ui-body ui-body-a">	
			<h3>Be A Cover Star...for ${$home_price_month} a month</h3>
			Be the viewer's first fruit! Post your photo on your city's cover for ${$home_price_month} a month.
			<br />

			<div id="cover">
				<img src="/images/adbuild/sales_city_thumb2.png" alt="" width="294" height="289" hspace="10" border="0" align="right" />
				<strong>Post your photo and link <br />	on your city's cover page -</strong><br />
		
				<blockquote>
					{section name=hl loop=$homesp}{assign var=home value=$homesp[hl]}
						{if $home.not_available}
							<span style="color: red;">City thumbnail in {$home.loc_name} not available - sold out<span><br />
						{else}
							<label><input type="checkbox" value="{$home.loc_id}" rel="{$home.price}" name="homesp{$home.loc_id}" {if $home.checked}checked="checked"{/if}/>{$home.loc_name} ${$home_price_month}/per month</label>
						{/if}
					{/section}
					<input type="hidden" name="_homespday" value="30" />
				</blockquote>
				<br />
				Upload Your Photo:<br />
				<input type="file" name="sponsorpic" />
			</div>
		</div>
		<br />

		<div class="ui-body ui-body-a">
			<h3>Appear on every page ... for ${$price_side_sponsor} a month</h3>
			Show up in sponsored ads section on the right of every page! Buy a sponsor ad upgrade for just ${$price_side_sponsor} a month.
			<br /><br />
			<span style="color: red;">Disclaimer: This upgrade is only visible on desktop and tablet version of our website (which is about 30% of our visitors - the rest 70% are users on cell phones)</span><br /><br />

			<div id="side">
				<img src="/images/adbuild/as_ss_example.png" alt="" width="429" height="183" hspace="10" border="0" align="right" />
				{section name=s loop=$sides}
				{assign var=side value=$sides[s]}
					{if $side.not_available}
						<span style="color: red;">Side sponsor in {$side.loc_name} not available - sold out<span><br />
					{else}
						<label><input type="checkbox" value="{$side.loc_id}" name="side_sponsor_{$side.loc_id}" {if $side.checked}checked="checked"{/if} /> {$side.loc_name} ${$price_side_sponsor}/per month</label>
					{/if}
				{/section}
			</div>
		</div>

		{if $is_agency || $cat_id == 2}
		<div class="ui-body ui-body-a">
			<h3>Appear always on the top of the list ... for ${$sticky_min_monthly_price} a month</h3>
			Show up at the very top of the list, and be ahead of your competition.<br/>
			Ads on top of the list have 500% more views than average ad.<br/>
			Your ad will be always at the beginning of the first page and will not be moved to second or subsequent pages.
			<br />

			<div id="sticky">
				<img src="/images/adbuild/as_sticky_example.png" alt="" width="429" height="183" hspace="10" border="0" align="right" />
				{section name=s loop=$stickies}
				{assign var=sticky value=$stickies[s]}  
					{if $sticky.not_available}
						<span style="color: red;">Sticky upgrade in {$sticky.loc_name} not available - sold out<span><br />
					{else}
						<label><input type="checkbox" value="{$sticky.loc_id}" name="sticky_sponsor_{$sticky.loc_id}" class="sticky-city" {if $sticky.checked}checked="checked"{/if} style="vertical-align: middle;"/> {$sticky.loc_name} - ${$sticky.price_monthly} / month</label>
					{/if}
				{/section}
			</div>
		</div>
		{/if}

	{/if}

	{if $ref}
		<input type="hidden" name="ref" value="{$ref}" />
	{/if}

	{if $bp && $account_level > 1}
		<div class="clear ac">This ad is taken from BP. <a href="/classifieds/remove?id={$ad_id}" onclick="return confirm('sure ?')">Delete this ad</a></div>
	{/if}

	{if $preview}
		<div id="preview"></div>
	{/if}

	<div id="calculator">
		{include file="mobile/adbuild/adbuild_summary_ajax_mobile.tpl"}
	</div>

	<input type="submit" name="preview" class="pm_btn pm_btn_white" id="adbuild_preview_btn" value="Preview Ad" data-role="none" />	
	<input type="submit" name="continue" class="pm_btn pm_btn_coral" id="adbuild_continue_btn" value="Save &amp; Continue &gt;" data-role="none" />

</form>

<hr />
<div>
For help please contact:<br />
{*
U.S.: 1-702-472-7242<br />
Canada: 1-778-819-8510
*}
<a href="mailto:support@adultsearch.com">support@adultsearch.com</a>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="phone_verification_modal" data-backdrop="static">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">Phone Verification</h4>
	  </div>
	  <div class="modal-body">
		<div id="phone_verify_error" style="display: none; color: red;"></div>
		<div id="phone_verify_stage_1" style="display: none;">
			<p>Phone number <span class="phone_verification_phone"></span> is not verified</p>
			<p><button type="button" id="btn_phone_verify" data-role="button" data-theme="a">Verify phone <span class="phone_verification_phone"></span></button></p>
		</div>
		<div id="phone_verify_stage_2" style="display: none;">
			<p>
				A 4-digit code has just been sent to your phone <span class="phone_verification_phone"></span>.<br />
				It might take up to 1 minute for the code to arrive.<br />
				Please enter this code:<br />
				<input type="text" name="phone_verify_sms_code" id="phone_verify_sms_code" value="" class="form-control" maxlength="4" /><br />
				<button type="button" id="btn_phone_check_code" data-role="button" data-theme="a">Submit</button>
			</p>
		</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" data-role="button" data-theme="a" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>

<script type="text/javascript">
var iti = null;

$("input[name=repost], select[name=auto_renew_time2], select[name=auto_repost], select[name=auto_renew_time], select[name=auto_renew_fr]").change(function() {
	cart_repost();
});
$("#homespday, input[name^='homesp']").change(function(){
	cart_cover()
});
$("input[name^='side_sponsor_']").change(function(){
	cart_side()
});
$("input[name^='sticky_sponsor_']").change(function(){
	cart_sticky()
});

function cart_repost() {
	if (!$("input[name=repost]").is(":checked")) {
		cart_json('cancelrepost=1');
	} else {
		var val = $("input[name=repost]:checked").val();
		if (val == 0) {
			cart_json('cancelrepost=1');
		} else if (val == 1) {
			var d = $("select[name=auto_renew_time2]").val();
			cart_json('repost=1&art='+d);
		} else if (val == 2) {
			var d = $("select[name=auto_renew_time]").val();
			var ar = $("select[name=auto_repost]").val();
			var fr = $("select[name=auto_renew_fr]").val();
			cart_json('repost=2&art='+d+'&ar='+ar+'&fr='+fr);
		}
	}
}

function cart_cover() {
	var val = "homespx=1&";
	var cnt = 0;
	jQuery('input[name^="homesp"]:checked').not(':disabled').each(function() {
		val += $(this).attr("name") + '=' + $(this).attr('rel') + '&';
		cnt++;
	});
	if (cnt == 0) {
		cart_json('cancelcover=1');
	} else {
		val += "spday=" + $("#homespday").val();
		jQuery.post("/adbuild/loc?ad_id={$ad_id}", val, function(data) {
			 jQuery("#calculator").html(data);
	 	}, "json");
	}
}

function cart_side() {
	var val = "side_sponsor=1&";
	var cnt = 0;
	jQuery('input[name^="side_sponsor_"]:checked').not(':disabled').each(function() {
		val += $(this).attr("name") + '=' + $(this).attr('rel') + '&';
		cnt++;
	});
	if (cnt == 0) {
		cart_json('cancelside=1');
	} else {
		jQuery.post("/adbuild/loc?ad_id={$ad_id}", val, function(data) {
			 jQuery("#calculator").html(data);
	 	}, "json");
	}
}

function cart_sticky() {
	var val = "sticky_sponsor=1&";
	var cnt = 0;
	jQuery('input[name^="sticky_sponsor_"]:checked').not(':disabled').each(function() {
		var loc_id = $(this).val();
		var days = 30;
		val += $(this).attr("name") + '=' + days + '&';
		cnt++;
	});
	if (cnt == 0) {
		cart_json('cancelsticky=1');
	} else {
		jQuery.post("/adbuild/loc?ad_id={$ad_id}", val, function(data) {
			jQuery("#calculator").html(data);
		}, "json");
	}
}

function cart_json(v) { 
	jQuery.post("/adbuild/loc?ad_id={$ad_id}", v, function(data){ jQuery("#calculator").html(data); }, "json");
}

$("#reply").change(function() {
	if ($(this).val() != '2')
		$("#email").show();
	else
		$("#email").hide();
});

{if $preview}
$(document).ready(function(){
var dialogOpts = {
		modal: true,
		bgiframe: true,
		autoOpen: false,
		height: 600,
		width: 890,
		draggable: true,
		resizeable: true,
	title: 'Preview Your Ad - You may close this window to make changes and test them again.'
	 };

	$("#preview").dialog(dialogOpts); 
	$("#preview").load("http://adultsearch.com/adbuild/preview?ad_id={$ad_id}&pop=1", "", function(){
		$("#preview").dialog("open");
	});
}); 
{/if}

{if $isadmin}
	var toolbar1 = [[ 'Bold', 'Italic', 'Strikethrough', '-', 'RemoveFormat' ], ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'], ['Undo', 'Redo'], ['Link', 'Unlink'], [ 'Source' ], [ 'Maximize' ]];
{else}
	var toolbar1 = [[ 'Bold', 'Italic', 'Strikethrough', '-', 'RemoveFormat' ], ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'], ['Undo', 'Redo'], [ 'Source' ], [ 'Maximize' ]];
{/if}

CKEDITOR.replace('message1', { 
	toolbar: toolbar1,
	allowedContent: 'p; br; strong; b; em; i; a[!href];'
});


var CFG_IMG_SERVER = '{$globals.config_image_server}';
var count = 0;
var MAX_RETRIES = 10;
var existing_videos = {$videos|json_encode};
var INTL_MODE = {if $international} true {else} false {/if} ;  // {*International mode from server:*} 
var COUNTRY_ISO = {if $country_iso} '{$country_iso}' {else} false {/if} ;

{literal}
function show_video_upload_error() {
	console.log('Error generating video file');
	$('#video_processing_div').hide();
	$('#video_processing_error').show();
	$('#video_upload_div').show();
	$('#video_preview_div').hide();
	$('.delete_video_btn').each(function(ind,obj) {
		$(obj).click(function() {
			delete_video(obj);
		});
	});

}

function video_processing_check(video_id) {
	if (!video_id)
		return false;
	console.log('video_processing_check, video_id='+video_id);
	count++;
	$('#video_upload_div').hide();
	$.post(
		'/vid/check',
		{ video_id : video_id },
		function(data) {
			if (data.status == '0') {
				if(count<MAX_RETRIES) {
					setTimeout(function() {video_processing_check(video_id);}, 5000);
				} else {
					show_video_upload_error();
				}

			} else if (data.status == '1') {
				console.log('vpc: error');
				show_video_upload_error();
			} else if (data.status == '2') {
				console.log('vpc: success, videopath = \''+data.path+'\' , current src='+$('#video_preview_div video source').attr('src'));
				show_video_preview(video_id, data.thumb_path, data.path);
			}
		},
		'json'
	)
	.fail(function() {
		alert('Video processing failed. Administrator will get back to you shortly.');
		$('#video_processing_div').hide();
	});
}


function show_video_preview(video_id, thumb_path, path) {
	$('#video_preview_div').prepend('<div class="item"><div class="flowplayer is-splash" style="background-color:#777; background-image:url(\''
		+ CFG_IMG_SERVER + thumb_path
		+ '\');"><video controls><source src="'
		+CFG_IMG_SERVER + path+'" type="video/mp4"/></video></div>'
		+'<button type="button" class="btn btn-danger btn-sm delete_video_btn" data-image-id="'+ video_id + '">Delete video</button></div>'
		);
	$('#video_processing_div').hide();
	$('#video_processing_error').hide();
	$('#video_upload_div').hide();
	$('#video_preview_div').show();
	$('.flowplayer').flowplayer();
	$('.delete_video_btn').each(function(ind,obj) {
		$(obj).click(function() {
			delete_video(obj);
		});
	});
	count = 0; // reset count
}

function delete_video(elem) {
	var video_id = $(elem).data('image-id');
	if(!confirm('Do you really want to delete uploaded video?')) return;
	$.post(
		'/vid/delete',
		{ video_id : video_id },
		function(data) {
			if (data.status_code && data.status_code === 200) {
				$(elem).closest('div.item').remove();
				console.log($('#video_preview_div').children().length);
				if ($('#video_preview_div').children().length == 0) {
					$('#video_processing_error').hide();
					$('#video_preview_div').hide();
					$('#video_upload_div').show();
				}
				$('#progress_video').hide();
			} else {
				alert('Deleting video failed. Please contact us.');
			}
		},
		'json'
	)
	.fail(function() {
		alert('Deleting video failed. Please contact us.');
	});
}

function file_upload_error(error_text) {
	alert("Error while uploading video.\nPlease choose another file!\n("+error_text+")  ");
}	

function init_phone(selector, full_elem, country_iso) {
	// lock for specific country
	var options = {
		allowDropdown: true,
		nationalMode: true,
		preferredCountries: ["us","ca","gb"],
		separateDialCode: true,
		utilsScript: "/js/intl-tel-input/js/utils.js"
	};

	if(country_iso) {
		options['initialCountry'] = country_iso;
	}

	iti = window.intlTelInput(document.querySelector(selector), options);
}

function phone_verify_is_verified() {
	var ad_id = $('input[name="ad_id"]').val();
	var phone = iti.getNumber();
	if (phone == '' || phone.length < 9) {
		$('#phone_verify_result').html('');
		$('#phone_verify_result_val').val('');
		return;
	}
	$('#phone_verify_result').html('<span style="color: red;"><i class="glyphicon glyphicon-remove"></i> Not verified</span> <button type="button" id="phone_verify_open" data-role="button" data-theme="a">Verify this phone</button>');
	$('#phone_verify_open').click(function() { $('#phone_verification_modal').modal('show'); });
	$('#phone_verify_result_val').val('0');
	$.post(
		'/_ajax/phone_verification', 
		{action: 'is_verified', ad_id: ad_id, phone: phone },
		function(data) {
			if (data == '0') {
				$('#phone_verification_modal').modal('show');
				$('#phone_verify_error').hide();
				$('#phone_verify_stage_1').show();
				$('#phone_verify_stage_2').hide();
				$('.phone_verification_phone').html(phone);
			} else {
				$('#phone_verify_result').html('<span style="color: green; font-weight: bold;"><i class="glyphicon glyphicon-ok"></i> Verified</span>');
				$('#phone_verify_result_val').val('1');
			}
		}
	);
}

function phone_verify_start_verification() {
	var ad_id = $('input[name="ad_id"]').val();
	var phone = iti.getNumber();
	$('#phone_verify_error').hide();
	$.post(
		'/_ajax/phone_verification', 
		{action: 'start_verification', ad_id: ad_id, phone: phone },
		function(data) {
			if (data == '0') {
				$('#phone_verify_stage_1').hide();
				$('#phone_verify_stage_2').hide();
				$('#phone_verify_error').html('Verification of this phone failed, please contact support');
				$('#phone_verify_error').show();
			} else {
				$('#phone_verify_stage_1').hide();
				$('#phone_verify_stage_2').show();
				$('#phone_verify_error').hide();
			}
		}
	);
}

function phone_verify_code_check() {
	var ad_id = $('input[name="ad_id"]').val();
	var phone = iti.getNumber();
	var code = $('#phone_verify_sms_code').val();
	$.post(
		'/_ajax/phone_verification', 
		{action: 'code_check', ad_id: ad_id, phone: phone, code: code },
		function(data) {
			console.log('data='+data);
			if (data == 'S') {
				$('#phone_verify_result').html('<span style="color: green; font-weight: bold;"><i class="glyphicon glyphicon-ok"></i> Verified</span>');
				$('#phone_verify_result_val').val('1');
				$('#phone_verification_modal').modal('hide');
			} else if (data == 'E') {
				$('#phone_verify_error').html('Phone verification error, please contact support');
				$('#phone_verify_error').show();
				$('#phone_verify_result_val').val('0');
			} else if (data == 'F') {
				$('#phone_verify_sms_code').val('');
				$('#phone_verify_error').html('Phone verification failed, please enter correct verification code');
				$('#phone_verify_error').show();
				$('#phone_verify_result_val').val('0');
			} else if (data == '0') {
				$('#phone_verify_error').html('Phone verification failed, this was the last attempt, you need to start verification again');
				$('#phone_verify_error').show();
				$('#phone_verify_stage_1').show();
				$('#phone_verify_stage_2').hide();
				$('#phone_verify_result_val').val('0');
			} else {
				$('#phone_verify_error').html('Phone verification error, please contact support');
				$('#phone_verify_error').show();
				$('#phone_verify_result_val').val('0');
			}
		}
	);
}

$(document).ready(function() {

	//phone verification and conversions
	init_phone('#phone1id','#phone1full',COUNTRY_ISO);
	$('#phone1id').blur(function () { phone_verify_is_verified(); });
	$('#btn_phone_verify').click(function() { phone_verify_start_verification(); });	
	$('#btn_phone_check_code').click(function() { phone_verify_code_check(); });
	setTimeout(function(){ phone_verify_is_verified(); }, 1000);
	$('form').on('submit',function(ev) {
		$('#phone1full').val(iti.getNumber());
		if ($('#phone_verify_result_val').val() != '1') {
			phone_verify_is_verified();
			alert('Please fill out your phone number and verify it first');
			ev.stopPropagation();
			return false;
		}
	});

	$.each(existing_videos, function(index,obj) {
		video_processing_check(obj.id);
	});

	$('.delete_video_btn').each(function(ind,obj) {
		$(obj).click(function() {
			delete_video(obj);
		});
	});
	$('#video').fileupload({
		url: '/vid/upload',
		dataType: 'json',
		autoUpload: true,
		acceptFileTypes: /(\.|\/)(mp4|avi|wmv)$/i,
		maxFileSize: 10400000,
		add: function(e, data) {
			var uploadErrors = [];
			var acceptFileTypes = /(\.|\/)(mp4|avi|wmv|quicktime|x\-msvideo)$/i;
			if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
				uploadErrors.push('Not an accepted file type:::: \''+data.originalFiles[0]['type']+'\'');
			}
			if(data.originalFiles[0]['size'] && data.originalFiles[0]['size'] > 10400000) {
				uploadErrors.push('Video filesize is too big');
			}
			if(uploadErrors.length > 0) {
				alert(uploadErrors.join("\n"));
			} else {
				data.submit();
			}
		}
	}).on('fileuploadprogressall', function (e, data) {
		var progress = parseInt(data.loaded / data.total * 100, 10);
		$('#video_processing_error').hide();
		$('#progress_video').show();
		$('#progress_video .progress-bar').css(
			'width',
			progress + '%'
		);
	}).on('fileuploaddone', function (e, data) {
		$.each(data.result.files, function (index, file) {
			if (file.error) {
				file_upload_error(file.error);
				//file_upload_error($(data.context.children()[index]), file.error);
			} else {
				$('#video_processing_div').show();
				$('#video_upload_div').hide();
				setTimeout(function() {video_processing_check(file.video_id);}, 5000);
			}
		});
	}).on('fileuploadfail', function (e, data) {
		$.each(data.files, function (index) {
			if (data.jqXHR.responseText == 'You must be logged in!') {
				console.log('fileuploadfail','logged out');
				location.href = '/login';
			} else {
				var error_text = data.jqXHR.statusText || 'File upload failed!';
				show_video_upload_error(error_text);
			}
		});
	}).prop('disabled', !$.support.fileInput)
		.parent().addClass($.support.fileInput ? undefined : 'disabled');	
});
{/literal}

</script>
