{* $forum array passed from parent template city_intl.tpl *}

{section name=for loop=$forum.forums}
{assign var=f value=$forum.forums[for]}
	<li class="ui-nodisc-icon ui-alt-icon"><a data-ajax="false" href="{$f.link}" title="{$loc_name} {$f.name}">{$f.name}<span class="ui-li-count">{$f.count}</span></a></li>
	{if $smarty.section.for.iteration==10}
	{break}
	{/if}
{/section}
