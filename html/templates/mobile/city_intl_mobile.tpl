
<ul data-role="listview" data-divider-theme="a" data-count-theme="b" style="margin-top: 10px !important;">

{if $country != "cambodia" && $country != "southafrica"}
	<li data-role="list-divider">Erotic Services in {$loc_name}</li>
	{include "mobile/city_eroticservices_mobile.tpl" eroticservices=$eroticservices}
{/if}

	<li data-role="list-divider">Adult Places in {$loc_name}</li>
	{section name=i loop=$categories}
		{include "mobile/city_intl_unit_mobile.tpl" category=$categories[i]}
	{/section}

{if $video}

{if $video_pl}
<li data-role="list-divider">Videos</li>
<script src="http://cdn.jquerytools.org/1.2.7/all/jquery.tools.min.js"></script>
<div id="player_wrap">
<div id="player" class="is-splash" {if $video_pl}style="background-image: url('{$video_pl[0][0].thumb}');"{/if}></div>
<div class="scrollable-wrap">
	<a class="prev">prev</a>
	<div class="scrollable">
		<div class="items">
			{foreach from=$video_pl item=it}
				<div>
					<span style="background-image: url('{$it[0].thumb}');" title="{$it[0].name}">{*{$it[0].name}*}</span>
					{if $it|@count > 1 }
						<span style="background-image: url('{$it[1].thumb}');" title="{$it[1].name}">{*{$it[1].name}*}</span>
					{/if}
			</div>
			{/foreach}
		</div>
	</div>
	<a class="next">next</a>
</div>
</div>
<script type="text/javascript">
$(function () {
	var playlist = [],
		videos = $(".scrollable span"),
		scrollable,
		player,
		i;

	// build playlist 
	{foreach from=$video_pl item=it}
		playlist.push({	sources: [ { type: "video/mp4", src: "{$it[0].url}" } ]	});
		{if $it|@count > 1 }
			playlist.push({	sources: [ { type: "video/mp4", src: "{$it[1].url}" } ]	});
		{/if}
	{/foreach}

	$(".scrollable").scrollable({
		circular: true
	});
 
	scrollable = $(".scrollable").data("scrollable");

	player = flowplayer($("#player"), {
		// loop the playlist in a circular scrollable
		loop: true,
		ratio: 9/16,
		bgcolor: "#333333",
		playlist: playlist
 
	}).on("ready", function (e, api, video) {
		videos.each(function (i) {
			var active = i == video.index;
	 
			$(this).toggleClass("is-active", active)
				 .toggleClass("is-paused", active && api.paused);
	});
 
	}).on("pause resume", function (e, api) {
		videos.eq(api.video.index).toggleClass("is-paused", e.type == "pause");
 
	}).on("finish", function (e, api) {
		var vindex = api.video.index,
			currentpage = Math.floor(vindex / 2),
			scrollindex = scrollable.getIndex(),
			next;
 
		// advance scrollable every 2nd playlist item
		if (vindex % 2 != 0 && scrollindex == currentpage) {
			// prefer circular movement when current item is visible
			scrollable.next();
	 
		} else if (scrollindex != currentpage) {
			// scroll to next item if not visible
			next = vindex < playlist.length - 1 ? vindex + 1 : 0;
			scrollable.seekTo(Math.floor(next / 2));
		}
	});
 
	videos.click(function () {
	var vindex = videos.index(this);
 
	if (player.video.index === vindex) {
		player.toggle();
	} else {
		player.play(vindex);
	}
	});
});
</script>

{else}
<li data-role="list-divider">Video</li>
<div id="player_wrap">
<div id="player" class="flowplayer">
	<video {if $video.thumb}poster="{$video.thumb}"{/if} {if $video.name}data-title="{$video.name}"{/if}>
		<source type="video/mp4" src="{$video.url}">
	</video>
</div>
</div>
{/if}

{/if}

	{if $forum.forums}
		<li data-role="list-divider">Sex Forums</li>
		{include "mobile/city_forums_mobile.tpl" forum=$forum}
	{/if}
</ul>

