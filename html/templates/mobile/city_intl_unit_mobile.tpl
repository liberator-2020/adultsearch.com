{* $category array passed from parent template city_intl.tpl *}

{section name=sc loop=$category.subcats}
{assign var=sc value=$category.subcats[sc]}
	<li class="ui-nodisc-icon ui-alt-icon"><a data-ajax="false" href="{$sc.url}" title="{$sc.title}">{$sc.name}<span class="ui-li-count">{$sc.count}</span></a></li>
{sectionelse}
	<li class="ui-nodisc-icon ui-alt-icon"><a data-ajax="false" href="{$category.url}" title="{$category.title}">{$category.name}<span class="ui-li-count">{$category.count}</span></a></li>
{/section}
