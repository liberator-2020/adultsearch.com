<style type="text/css">
h1 {
	font-size: 20px;
	color: #ff740e;
	font-weight: bold;
	font-family: Verdana, Geneva, sans-serif;
	margin: 5px 0px 5px 0px;
}
h1 a {
	color: #ff740e !important;
}
h2 {
	font-weight: bold;
	color: #ff740e;
	border-bottom: solid 1px #CCCCCC;
	font-size: 14px;
	font-family: Verdana, Geneva, sans-serif;
}
.posteddate {
	color: #888;
	margin-bottom: 5px;
}
#stats div span.label {
	display: inline-block;
	width: 100px;
	font-weight: bold;
}
</style>

<link href="/css/currency_convert.css" type="text/css" rel="stylesheet">
<script src="/js/currency_convert.js" type="text/javascript"></script>

{if $adpreview}
<a onclick="$('#preview').dialog('close'); return false;" href="#"><img src="/images/adbuild/btn_edit.png" alt="" border="0" /></a>
{if $post.done < 1}<a href="https://{$site_domain}/adbuild/step4?ad_id={$post.id}" data-ajax="false"><img src="/images/adbuild/btn_submitcontinue.png" alt="" /></a>{/if}
{/if}

<!-- end pre-content -->

<!-- buttons - admin -->	 
<div>
	{if $adexpired}
		{if $isadmin}
			<a href="/classifieds/look?id={$post.id}&adminrepost=1" data-ajax="false">Ad is Expired</a>
		{else}
			<a class="paylink">Ad is Expired</a>
		{/if}
	{elseif $adexpiredbp}
		<a class="paylink" rel="nofollow" href="https://{$site_domain}/classifieds/renew?id={$post.id}" data-ajax="false">Renew This Expired Ad</a>
	{/if}
	{if $canedit}
		<a href="http://{$site_domain}/adbuild/step3?ad_id={$post.id}&mobile=2" class="ui-btn ui-btn-c ui-mini ui-btn-inline" data-ajax="false">Edit Ad</a>
		{if $isadmin}
			<a href="http://{$site_domain}/classifieds/manage?id={$post.id}" class="ui-btn ui-btn-c ui-btn-b ui-mini ui-btn-inline" data-ajax="false">Manage Ad</a>
			<a href="http://{$site_domain}/classifieds/manage?action=delete_ad_ban_user&id={$post.id}" class="ui-btn ui-btn-c ui-mini ui-btn-inline" data-ajax="false">Delete Ad &amp; Ban User</a>
			<a href="http://{$site_domain}/classifieds/sponsor?id={$post.id}" class="ui-btn ui-btn-c ui-mini ui-btn-inline" data-ajax="false">Make This Ad A Homepage Thumbnail</a>
		{/if}
	{/if}
	{if $expired}
		<a class="ui-btn ui-btn-c ui-mini ui-btn-inline" href="https://{$site_domain}/adbuild/step3?ad_id={$post.id}" data-ajax="false">Pay to Publish Ad</a>
	{/if}
</div>

<!-- heading -->
<h1>{$post.firstname}{if $post.phone} - <a href="tel:{if $post.phoneInternational}{$post.phoneInternational}{else}{$post.phone}{/if}" style="white-space: nowrap;">{$post.phone}</a>{/if}</h1>
{*
{if !$adpreview}<div class="posteddate"> Posted: {$post.date2} PST</div>{/if}
*}
<div class="summary">
	{*
	{if $post.visiting} Visiting, {/if}
	*}
	{if $post.pregnant} Pregnant, {/if}
	{if $post.pornstar} Porn Star, {/if}
	{$cat_name}{if $post.gfe_limited} &amp; Limited GFE{/if}{if $post.gfe} &amp; GFE{/if} {if $post.available}available for {if $post.available==1}Incall{elseif $post.available==2}Outcall{else}Incall and Outcall{/if}{/if}.
</div>
<div class="tagline">{$post.title}</div>
	
<div class="linkbutton">
	 <a href="#bio" class="ui-btn ui-btn-c ui-mini ui-btn-inline" data-ajax="false">Jump to Info/Rates</a>	
	 <a href="#contact" class="ui-btn ui-btn-c ui-mini ui-btn-inline" data-ajax="false">Jump to Contact Info</a>
</div>
	
<div id="linktothis" style="display:none"><input type="text" value="http://{$smarty.server.HTTP_HOST}{$state_link}{$gModule}/{$post.id}" onclick="this.select();" size="100" readonly="readonly" /><br /><br /></div>

<!-- photos and videos -->
<div id="BioID">

	{if $videos}
		<div><a id="video-uploads" name="video-uploads"></a>
		{section name=i loop=$videos}
		<div class="flowplayer is-splash"
			 style="width:{$videos[i].width}px;height:{$videos[i].height}px;background-color:#777; background-image:url('{$videos[i].thumbnail_url}');">
			<video width="{$videos[i].width}" height="{$videos[i].height}" controls><source src="{$videos[i].file_url}" type="video/mp4"/></video>
		</div>
		{/section}
		</div>
	{/if}

	{if $images}
		<div class="photo"><a id="uploads" name="uploads"></a></div>
		<div id="gallery">
			{section name=i loop=$images}
			{assign var=i value=$images[i]}
				<img src="{$config_image_server}/classifieds/{$i.filename}" alt="" /><br />
			{/section}
		</div>
	{/if}
</div>

<!-- ad content -->
<div id="stats">
	<h2 class="title">About {$post.firstname}</h2>
	<a name="bio"></a>
	{$post.content}
	<br />

	<h2>Info/Rates</h2>

{*
	{if $post.loc_name}
		<div><span class="label">Location</span>{$post.loc_name}{if $post.location} ({$post.location}){/if}</div>
	{/if}
		
	{if $post.age}
		<div>
			<span class="label">Stats</span>
			{if $post.age}{$post.age} years old{/if}{if $post.ethnicity}, {$post.ethnicity}{/if}{if $post.height}, {$post.height}{/if}{if $post.weight}, {$post.weight}{/if}{if $post.eyecolor}, {$post.eyecolor} Eyes{/if}{if $post.haircolor}, {$post.haircolor} Hair{/if}{if $post.build},	{$post.build}{/if}{if $post.measurements}, {$post.measurements}{/if}{if $post.cupsize}, {$post.cupsize} Cup{/if}{if $post.penis_size}, {$post.penis_size} (inches){/if}{if $post.kitty}, {$post.kitty} Kitty{/if}
		</div>
	{/if}
		
	{if $post.language}
		<div><span class="label">Languages</span>{$post.language}</div>
	{/if}

	{if $post.avl_men||$post.avl_women||$post.avl_couple}
		<div>
			<span class="label">Available To</span>
			{if $post.avl_men}Men{if $post.avl_women||$post.avl_couple}, {/if}{/if} {if $post.avl_women}Women{if $post.avl_couple}, {/if}{/if} {if $post.avl_couple}Couples{/if}
		</div>
	{/if}
	 
	{if $post.tantra}
		<div><span class="label">Massage</span>Tantra Massage</div>
	{/if}

	{if $post.fetish}
		<div><span class="label">Fetish Session</span>{$post.fetish}</div>
	{/if}

	{if $post.bdsm}
		<div><span class="label">BDSM Activities</span>{$post.bdsm}</div>
	{/if}

	<div>
		<span class="label">Incall</span>
		{if $post.incall}
			{if $post.incall_rate}
				{if $post.incall_rate_hh}{$post.incall_rate_hh}/half hour, {/if}
				{$post.incall_rate}/hour
				{if $post.incall_rate_2h}{$post.incall_rate_2h}/2 hours, {/if}
				{if $post.incall_rate_day}, {$post.incall_rate_day}/overnight{/if}
			{else}
				Yes
			{/if}
		{else}
			No
		{/if}
	</div>

	<div>
		<span class="label">Outcall</span>
		{if $post.outcall}
			{if $post.outcall_rate}
				{if $post.outcall_rate_hh}{$post.outcall_rate_hh}/half hour, {/if}
				{$post.outcall_rate}/hour
				{if $post.outcall_rate_2h}{$post.outcall_rate_2h}/2 hours, {/if}
				{if $post.outcall_rate_day}, {$post.outcall_rate_day}/overnight{/if}
			{else}
				Yes
			{/if}
		{elseif $post.incall}
			No
		{else}
			Yes
		{/if}
	</div>

	{if $currency_button&&($post.incall_rate||$post.outcall_rate)}<div>{$currency_button}</div>{/if} 
		{if $post.payment_visa or $post.payment_amex or $post.payment_dis}
		<div>
		<span class="label">Credit Cards</span>
		{if $post.payment_visa}Visa{/if}
		{if $post.payment_amex}American Express{/if}
		{if $post.payment_dis}Discover{/if}.
	</div>
	{/if}
*}

	<table class="slim">
	{if $post.loc_name}
		<tr><td>Location</td><td>{$post.loc_name}{if $post.location} ({$post.location}){/if}</td></tr>
	{/if}
		
	{if $post.age}
		<tr><td>Stats</td><td>
			{if $post.age}{$post.age} years old{/if}{if $post.ethnicity}, {$post.ethnicity}{/if}{if $post.height}, {$post.height}{/if}{if $post.weight}, {$post.weight}{/if}{if $post.eyecolor}, {$post.eyecolor} Eyes{/if}{if $post.haircolor}, {$post.haircolor} Hair{/if}{if $post.build},	{$post.build}{/if}{if $post.measurements}, {$post.measurements}{/if}{if $post.cupsize}, {$post.cupsize} Cup{/if}{if $post.penis_size}, {$post.penis_size} (inches){/if}{if $post.kitty}, {$post.kitty} Kitty{/if}
		</td></tr>
	{/if}
		
	{if $post.language}
		<tr><td>Languages</td><td>{$post.language}</td></tr>
	{/if}

	{if $post.avl_men||$post.avl_women||$post.avl_couple}
		<tr><td>Available To</td><td>
			{if $post.avl_men}Men{if $post.avl_women||$post.avl_couple}, {/if}{/if} {if $post.avl_women}Women{if $post.avl_couple}, {/if}{/if} {if $post.avl_couple}Couples{/if}
		</td></tr>
	{/if}
	 
	{if $post.tantra}
		<tr><td>Massage</td><td>Tantra Massage</td></tr>
	{/if}

	{if $post.fetish}
		<tr><td>Fetish Session</td><td>{$post.fetish}</td></tr>
	{/if}

	{if $post.bdsm}
		<tr><td>BDSM Activities</td><td>{$post.bdsm}</td></tr>
	{/if}

	<tr><td>Incall</td><td>
		{if $post.incall}
			{if $post.incall_rate}
				{if $post.incall_rate_hh}{$post.incall_rate_hh}/half hour, {/if}
				{$post.incall_rate}/hour
				{if $post.incall_rate_2h}{$post.incall_rate_2h}/2 hours, {/if}
				{if $post.incall_rate_day}, {$post.incall_rate_day}/overnight{/if}
			{else}
				Yes
			{/if}
		{else}
			No
		{/if}
	</td></tr>

	<tr><td>Outcall</td><td>
		{if $post.outcall}
			{if $post.outcall_rate}
				{if $post.outcall_rate_hh}{$post.outcall_rate_hh}/half hour, {/if}
				{$post.outcall_rate}/hour
				{if $post.outcall_rate_2h}{$post.outcall_rate_2h}/2 hours, {/if}
				{if $post.outcall_rate_day}, {$post.outcall_rate_day}/overnight{/if}
			{else}
				Yes
			{/if}
		{elseif $post.incall}
			No
		{else}
			Yes
		{/if}
	</td></tr>

	{if $currency_button&&($post.incall_rate||$post.outcall_rate)}
		<tr>
			<td></td>
			<td><div>{$currency_button}</div></td>
		</tr>
	{/if}

	{if $post.payment_visa or $post.payment_amex or $post.payment_dis}
		<tr>
			<td>Credit Cards</td>
			<td>
				{if $post.payment_visa}Visa{/if}
				{if $post.payment_amex}American Express{/if}
				{if $post.payment_dis}Discover{/if}.
			</td>
		</tr>
	{/if}
	</table>
	
	<!-- contact -->
	<a name="contact"></a>
	<h2>Contact</h2>
		{*
		{if $post.vt && $post.visiting}
			<div><span class="label">Visiting</span>{if $post.vf!="00/00/0000"}{$post.vf} - {$post.vt}{else}Yes{/if}</div>
		{/if}
		*}

		{if $post.phone}
			<div><span class="label">Phone</span><a href="tel:{if $post.phoneInternational}{$post.phoneInternational}{else}{$post.phone}{/if}">{$post.phone}</a></div>
		{/if}
		
		{if $post.reply==1}
			<div><span class="label">Email</span><a href="/classifieds/reply?id={$post.id}" rel="nofollow" title=""><u>Send A Reply</u></a></div>
		{/if}
	
		{if $post.reply==3}
			<div><span class="label">Email</span><a href="mailto:{$post.email}">{$post.email}</a></div>
		{/if}

		{if $post.website}
			<div><span class="label">Website</span><a href="{$post.website}" target="_blank" rel="nowfollow">{$post.website}</a></div>
		{/if}
		 
		{if $post.ter}
			<div>
				<span class="label">TER</span>
				<a href="http://www.theeroticreview.com/reviews/show.asp?id={$post.ter}" target="_blank" rel="nofollow">Read My Reviews on TER</a>
			</div>
		{/if}

		{if $post.bigdoggie}
			 <div>
				 <span class="label">BigDoggie</span>
				 <a href="http://www.bigdoggie.net/escorts/by-id/{$post.bigdoggie}.html" target="_blank" rel="nofollow" data-ajax="false">Read My Reviews on Bigdoggie.net</a>
			 </div>
			 {/if}

		{if $post.eccie}
	 		<div><span class="label">Eccie</span><a href="/ad?goo=1&h=eccie.net&l={$post.eccie}" target="_blank" rel="nofollow" data-ajax="false">Eccie.net discussion about this ad</a></div>
		{/if}

	{if $post.facebook}
		<div><span class="label">Facebook</span><a href="{$post.facebook}" target="_blank" rel="nofollow">{$post.facebook}</a></div>
	{/if}
	{if $post.twitter}
		<div><span class="label">Twitter</span><a href="{$post.twitter}" target="_blank" rel="nofollow">{$post.twitter}</a></div>
	{/if}
	{if $post.google}
		<div><span class="label">Google</span><a href="{$post.google}" target="_blank" rel="nofollow">{$post.google}</a></div>
	{/if}
	{if $post.instagram}
		<div><span class="label">Instagram</span><a href="{$post.instagram}" target="_blank" rel="nofollow">{$post.instagram}</a></div>
	{/if}
		 
		<br />
		Mention you saw this ad on AdultSearch.com
</div>

{if $refers}
	<hr size="1" />
	<table data-role="table" data-mode="reflow" class="ui-responsive">
	<thead>
		<tr><th>Time</th><th>Link</th></tr>
	</thead>
	<tbody>
		{section name=s1 loop=$refers}
		{assign var=r value=$refers[s1]}
		<tr>
			<td>{$r.time}</td><td><a href="{$r.referer}" title="{$r.referer}" target="_blank" data-ajax="false">{$r.referer|truncate:100}</a></td>
		</tr>
		{/section}
	</tbody>
	</table>
{/if}

{if $videos}
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/flowplayer/6.0.5/flowplayer.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flowplayer/6.0.5/skin/minimalist.css">
{/if}
