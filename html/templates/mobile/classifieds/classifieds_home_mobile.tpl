{if $filter}
	<div class="ui-corner-all custom-corners">
		<div class="ui-bar ui-bar-a">
			<h3>You Searched For</h3>
		</div>
		<div class="ui-body ui-body-a">
			{section name=filter loop=$filter}
			{assign var=f value=$filter[filter]}
				<a href="{$f.link}" class="ui-btn ui-icon-delete ui-btn-icon-left" data-ajax="false">{$f.name}</a>
			{/section}
		</div>
	</div>
{/if}
	
{if $refine}
	<div data-role="collapsible" data-theme="c">
		<h4>{$r.name} Search Filters</h4>
		{section name=r loop=$refine}
		{assign var=r value=$refine[r]}
			<select name="{$r.alt[0].type}" onChange="javascript:addFilter(this.name,this.value);">
				<option value=""> - {$r.name} - </option>
				{section name=a loop=$r.alt}
					{assign var=a value=$r.alt[a]}
					<option value="{$a.id}">{$a.name} - {$a.c}</option>
				{/section}
			</select>
		{/section}
	</div>
{/if}
	
<!-- content -->	
<div class="title">{$total} {$cat_name}{if $c.day!=$day}- <div class="postDate">{$c.day_title}</div>{/if}</div>
{if $smarty.get.view != "list"}
	<a href="./?view=list" class="ui-btn ui-btn-inline ui-icon-bullets ui-btn-icon-left ui-mini" data-ajax="false">List View</a>
	<a href="./" class="ui-btn ui-btn-inline ui-icon-camera ui-btn-icon-left ui-mini ui-state-disabled" data-ajax="false">Photo View</a>
{else}
	<a href="./?view=list" class="ui-btn ui-btn-inline ui-icon-bullets ui-btn-icon-left ui-mini ui-state-disabled" data-ajax="false">List View</a>
	<a href="./" class="ui-btn ui-btn-inline ui-icon-camera ui-btn-icon-left ui-mini" data-ajax="false">Photo View</a>
{/if}

{if $sc}
	{assign var="ad_count" value=$sc|count}
	<ul id="list-escorts" data-role="listview" data-inset="true" data-ad-count="{$ad_count}">
	{section name=sc loop=$sc}
	{assign var=c value=$sc[sc]}
		
	{if $smarty.get.view != "list"}
		<!-- photo view -->
		<li class="ui-nodisc-icon ui-alt-icon" data-index="{$smarty.section.sc.index}">
			<a href="{if $c.direct_link}{$c.direct_link}{else}{$c.link}{/if}" data-ajax="false">
				{if $c.thumb}
					<img src="{$config_image_server}/{$image_path}/t/{$c.large_thumb}" alt="" border="0" style="max-height: 400px;"/>
				{else}
					<img src="{$config_site_url}/images/placeholder.gif" alt="" border="0" width="75" />
				{/if}
			{if $c.phone}<h2 class="bigPhone">{$c.phone}</h2>{else}No Phone{/if}
			</a>
    	</li>

		{if $ad_count > 1 && $smarty.section.sc.index == 0 && $type == 2}
			<li class="ui-nodisc-icon ui-alt-icon" style="padding: 0px;">{if !$nobanner}
		        <div id="mobile_inlistpromo">
					<center>
        		    {if $type == 2}
                		<script type="text/javascript">as_show_banner(10109, 300, 250);</script>
		            {/if}
					</center>
        		</div>
	        {/if}</li>
		{/if}
	
	 <!-- list view -->				 
	{else}
		<div class="subCategory">
			<a href="{if $c.direct_link}{$c.direct_link}{else}{$c.link}{/if}" title="{$c.name}" align="left">
				{if $c.thumb}
					<img src="{$config_image_server}/{$image_path}/{$c.thumb}" alt="" class="placeThumb" align="left" border="0" width="75" height="75" />
				{else}
					<img src="{$config_site_url}/images/placeholder.gif" align="left" alt="" border="0" width="75" class="placeThumb" />
				{/if}
				<div class="name">{$c.firstname}</div>
				{$c.name|truncate:33}{if $c.age} - {$c.age}{/if}<br/>
				{if $c.ethnicity}{$c.ethnicity}, {/if}
				{if $c.available}{$c.available}, {/if}
				{if $c.haircolor||$c.eyecolor}{if $c.haircolor}{$c.haircolor} Hair{/if}{if $c.eyecolor}{if $c.haircolor}, {/if}{$c.eyecolor} Eyes{/if}, {/if}
				{if $c.pregnant||$c.pornstar||$c.visiting}{if $c.pregnant}Pregnant{/if}{if $c.pornstar}{if $c.pregnant}, {/if}Pornstar{/if}{if $c.visiting}{if $c.pregnant||$c.pornstar}, {/if}Visiting{/if}<br/>{/if}
				{if $c.location}{$c.location}<br/>{/if}
				<br style="clear: both;" />
			</a>
		</div>
	{/if}
	{/section}
	</ul>

	<div data-role="controlgroup" data-type="horizontal">
		{if $pager_array.pager[0].type == "prev"}
			<a class="ui-btn ui-mini ui-corner-all" href="{$pager_array.pager[0].link}" data-ajax="false">Prev</a>
		{else}
			<a class="ui-btn ui-mini ui-corner-all ui-state-disabled" href="{$pager_array.pager[0].link}" data-ajax="false">Prev</a>
		{/if}
		{foreach $pager_array.pager item=page}
			{if $page.type == "current"}
				<a class="ui-btn ui-mini ui-corner-all ui-state-disabled pager-disabled" href="{$page.link}" data-ajax="false">{$page.page}</a>
			{elseif $page.type == "page"}
				<a class="ui-btn ui-mini ui-corner-all" href="{$page.link}" data-ajax="false">{$page.page}</a>
			{/if}
		{/foreach}
		{if $pager_array.pager[($pager_array.pager|@count - 1)].type == "next"}
			<a class="ui-btn ui-mini ui-corner-all" href="{$pager_array.pager[($pager_array.pager|@count - 1)].link}" data-ajax="false">Next</a>
		{else}
			<a class="ui-btn ui-mini ui-corner-all ui-state-disabled" href="{$pager_array.pager[($pager_array.pager|@count - 1)].link}" data-ajax="false">Next</a>
		{/if}
	</div>

{/if}

