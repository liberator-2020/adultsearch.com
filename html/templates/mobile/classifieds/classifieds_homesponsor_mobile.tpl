<form method="post" action="" data-ajax="false">

<input type="hidden" name="id" value="{$id}"/>

<h2>Be A Cover Star...for ${$home_price_month} a month</h2>
Be the viewer's first fruit!<br />
Post your photo on your city's cover for ${$home_price_month} a month.<br />

<img src="/images/adbuild/sales_city_thumb2.png" alt="" style="width:100%; max-width: 294px;" />
<br />
<strong>Post your photo and link on your city's cover page -</strong>
<br />

{if $error}
    <div style="color: red; font-weight: bold;">{$error}</div>
{/if}

{section name=hl loop=$homesp}{assign var=home value=$homesp[hl]}
	{if $home.not_available}
		<span style="color: red;">City thumbnail in {$home.loc_name} not available<span><br />
	{else}
		<label>
			<input type="checkbox" value="{$home.loc_id}" rel="{$home.price}" name="locs[]" {if $home.checked}checked="checked"{/if}/> {$home.loc_name} ${$home_price_month}/per month<br />
		</label>
	{/if}
{/section}
<br />

Upload Your Photo:<br />
{if $sponsorpic}<img src="{$sponsorpic}" /> <a href="/classifieds/sponsor?id={$id}&removepic=true">Remove this picture</a><br/>{/if}
<input type="file" name="sponsorpic" />

<input type="submit" name="submit" class="pm_btn pm_btn_coral" value="Submit" style="width: 100%;" />
<input type="button" name="cancel" class="pm_btn pm_btn_default" value="Cancel" onclick="window.location='/classifieds/myposts'"/>

</form>

