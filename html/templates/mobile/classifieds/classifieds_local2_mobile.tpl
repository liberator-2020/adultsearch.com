<form method="post" action="" data-ajax="false">
<input type="hidden" name="step" value="1" />
<div id="contentWrapper"><div class="mainCategory">Choose Metro Areas</div>
{if $error}<div class="generalRoundBox"><p class="error">ERROR: {$error}</p></div>{/if}
<div class="generalRoundBox">
	<input type="submit" value="Continue" /></div>
	<div class="spacer"></div>
	<div class="generalRoundBox" style="text-align: left;">
{section name=l loop=$list}{assign var=l value=$list[l]}
{section name=us loop=$l.list}
{assign var=tt value=$l.list[us]}
<ul class="states">
{section name=us2 loop=$tt}
{assign var=t value=$tt[us2]}
 <li class="state">
	<div>{$t.real_name}</div>
	 <ul style="list-style-type: none;">
		{section name=s3 loop=$t.city}{assign var=c value=$t.city[s3]}
		<li><input type="checkbox" name="loc_id[]" value="{$c.loc_id}" id="loc_id{$c.loc_id}" {if
$c.checked || $c.loc_id == $smarty.session.def_loc_id}checked="checked"{/if}/ {if $c.bold}class="bold"{/if} > {if $c.bold}<b>{$c.loc_name}</b>{else}{$c.loc_name}{/if} <span>(${$c.price})</span></li>
		 {/section}
	 </ul>
 </li>
{/section}
</ul>
{/section}
{/section}
	</div><div class="spacer"></div>
	<div class="generalRoundBox"><input type="submit" value="Continue" /></form></div>
	{if $local}<div class="spacer"></div>
		<div class="generalRoundBox">
			<a href="" onclick="$('#addnew').show(); return false;">Click here to add a city which is not listed above.</a>
			<div style="" id="addnew">You may type the city names to add them individually: <select id="addcity" name="addcity"></select></div>
			<div id="extracity"></div>
		</div>
	{/if}
</div>
</div>
