<form method="post" action="" name="f" data-ajax="false">
	{if $id}
	<input type="hidden" name="id" value="{$id}" />
	{/if}
	<div id="contentWrapper">
	<div class="generalRoundBox" style="text-align: left;">
		<p>You will be charged : <b>${$total}</b></p>
		<table border="0" cellspacing="0" cellpadding="0" class="b">
		{if $error}
		<tr>
			<td colspan="2" class="error"> {$error} </td>
		</tr>
		{/if}
		<tr>
			<td class="first"><font style="font-size:18px">Payment Details</font></td>
			<td class="second"><img style="padding-left:10px" src="/images/Thawte-secured.jpg" alt="" /></td>
		</tr>
		{if $scc}
		<tr>
			<td class="first">My Cards on my account:</td>
			<td class="second"><select name="cc_id">
				<option value="0">-- select --</option>
				
	{section name=s loop=$scc}{assign var=c value=$scc[s]}
		
				<option value="{$c.cc_id}">{$c.cc} ({$c.expmonth}/{$c.expyear})</option>
				
	{/section}
	
			</select>
			<input type="submit" value="Charge my card on file" /></td>
		</tr>
		{/if}
		<tr>
			<td class="first">First Name:</td>
			<td class="second"><input type="text" name="firstname" id="m-name" size="40" class="input0" value="{$firstname}" maxlength="60" /></td>
		</tr>
		<tr>
			<td class="first">Last Name:</td>
			<td class="second"><input type="text" name="lastname" size="40" class="input0" value="{$lastname}" /></td>
		</tr>
		<tr>
			<td class="first">Address:</td>
			<td class="second"><input type="text" name="address" size="40" class="input0" value="{$address}" /></td>
		</tr>
		<tr>
			<td class="first">City:</td>
			<td class="second"><input type="text" name="city" size="40" class="input0" value="{$city}" /></td>
		</tr>
		<tr>
			<td class="first">State/Province/Region:</td>
			<td class="second"><input type="text" name="state" size="20" class="input0" value="{$state}" /></td>
		</tr>
		<tr>
			<td class="first">ZIP/Postal Code:</td>
			<td class="second"><input type="text" name="zipcode" size="10" class="input0" value="{$zipcode}" /></td>
		</tr>
		<tr>
			<td class="first">Credit Card Number:</td>
			<td class="second"><input type="text" name="cc" size="20" class="input0" value="{$cc}" />
			<img class="img1" src="/images/classifieds/Credit_card_logos_small.png" alt="" /></td>
		</tr>
		<tr>
			<td class="first">Expiration Date:</td>
			<td class="second"><select name="month">
				<option value="">-- Select --</option>
				<option value="1"{if $month==1} selected="selected"{/if}>January (01)</option>
				<option value="2"{if $month==2} selected="selected"{/if}>February (02)</option>
				<option value="3"{if $month==3} selected="selected"{/if}>March (03)</option>
				<option value="4"{if $month==4} selected="selected"{/if}>April (04)</option>
				<option value="5"{if $month==5} selected="selected"{/if}>May (05)</option>
				<option value="6"{if $month==6} selected="selected"{/if}>June (06)</option>
				<option value="7"{if $month==7} selected="selected"{/if}>July (07)</option>
				<option value="8"{if $month==8} selected="selected"{/if}>August (08)</option>
				<option value="9"{if $month==9} selected="selected"{/if}>September (09)</option>
				<option value="10"{if $month==10} selected="selected"{/if}>October (10)</option>
				<option value="11"{if $month==11} selected="selected"{/if}>November (11)</option>
				<option value="12"{if $month==12} selected="selected"{/if}>December (12)</option>
			</select>
			<select name="year">
				{$year}
			</select></td>
		</tr>
		<tr>
			<td class="first">Security Code (CVC2):</td>
			<td class="second"><input type="text" name="cvc2" value="{$cvc2}" size="5" /></td>
		</tr>
		{if !$captcha_ok}
		<tr>
			<td class="first">Security Control: <br/></td>
			<td class="second"><input type="text" name="captcha_str" value="" autocomplete="off" />
			<span class="smallr">Type the text on the image into the above box.<br/>
			<img src="/captcha.php?{$time}" alt="" /></span></td>
		</tr>
		{else}
		<input type="hidden" name="captcha_str" value="{$captcha_str}" />
		{/if}
		<tr>
			<td colspan="2" class="submit"><span id="submitbuttonwait" style="display:none;">Please wait....</span> <span id="submitbutton">
			<input type="image" src="/images/classifieds/Post_Ad_2.png" onclick="$('#submitbutton').hide();$('#submitbuttonwait').show();" />
			</span></td>
		</tr>
		</table>
	</div>
	</div>
	<div class="spacer"></div>
</form>
