<form method="get" action="" data-ajax="false">

<input type="hidden" name="id" value="{$id}" />

<h2>Schedule your reposts, hassle-free</h2>
Every time your ad is reposted, it appears at the top of its category list. <br />
Maximize your exposure exactly when you tell us to.<br />
<br />

{if $error}
	<div style="color: red; font-weight: bold;">{$error}</div>
{/if}

<label>
	<input name="recurring" type="checkbox" value="1" {if $recurring}checked="checked"{/if} />Hassle Free Daily Reposting
</label>

Every day at
<select name="auto_renew_time2">
	{html_options values=$art_values output=$art_names selected=$auto_renew_time2}
</select>
{if $timezone}{$timezone}{else}PST{/if}
	for <b>${$total}</b> per month. <br />
<br />
Price includes all of your  locations. <span class="orange">70%</span> less per month as compared to daily posting rates! This is a monthly recurring charge. You may cancel this at any time by clicking the &quot;My Ads&quot; link at the top of	this page.

{if !$error}
	<input type="submit" name="submit" class="pm_btn pm_btn_coral" value="Upgrade" data-role="none" style="width: 100%;" />
{/if}
<input type="button" name="cancel" class="pm_btn pm_btn_default" value="Cancel" onclick="window.location='/classifieds/myposts'"/>

</form>

