<div id="breadcrumb"><a href="/" data-ajax="false">{$loc_name}</a> : <a href="/{$gModule}/" title="{$cat_name}" data-ajax="false">{$cat_name}</a></div>
<div id="placeList">
	<form id="emailReplyForm" name="formEmail" action="" method="post" data-ajax="false">
		{if $error}<div class="error">{$error}</div>{/if}
		<b>Re:</b> {$title}<br /><br />
		<b>Your email address:</b><br>
		<input type="text" name="from" size="30" value="{$from}"/>
		<br />
		<b>Confirm your email address:</b><br>
		<input type="text" name="fromVerify" size="30" value="{$fromVerify}"/>
		<br />
		<b>Your message:</b><br>
		<textarea cols="40" rows="18" name="m">{$m}</textarea>
		<br />
		{if !$captcha_ok}
			Security Control: <br/>
			<input type="text" name="captcha_str" value="" autocomplete="off" />
			<br />
			<span class="smallr">Type the text on the image into the above box.<br/>
			<img src="/captcha.php?{$time}" alt="" /></span>
		{else}
			 <input type="hidden" name="captcha_str" value="{$captcha_str}" />
		{/if}
		<br />
		<input type="submit" value="Send Reply" class="button" id="submit_button" style="margin-padding:4px;font-size:1.2em;">
	</form>
</div>
