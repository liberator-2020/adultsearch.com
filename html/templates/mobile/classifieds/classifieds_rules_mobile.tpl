<div id="contentWrapper"><div class="generalRoundBox" style="text-align: left;">
      <strong>Posting Rules</strong>
      <p>You agree to the following when posting in this category:</p>
      <ul>
        <li>I will not post obscene or lewd and lascivious graphics or photographs which depict genitalia or actual or simulated sexual acts;</li>
        <li>I will not post any solicitation directly or in "coded" fashion for any illegal service exchanging sexual favors for money or other valuable 
consideration;</li>
        <li><span style="background-color:yellow; padding:0 2px">I will not post any material on the Site that exploits minors in any way;</span></li>

        <li><span style="background-color:yellow; padding:0 2px">I will not post any material on the Site that in any way constitutes or assists in human 
trafficking;</span></li>
        <li><span style="background-color:yellow; padding:0 2px">I am at least 18 years of age or older and not considered to be a minor in my state of 
residence.</span></li>
      </ul>
      <p style="color:red; font-weight:bold">Any post exploiting a minor in any way will be subject to criminal prosecution and will be reported to the <a 
href="http://www.cybertipline.com" target="_blank" style="color:red" rel="nofollow">Cybertipline</a>. The poster will be caught and the police will prosecute the poster 
to the full 
extent of the law.</p>
      <p>Any post with terms or misspelled versions of terms implying an illegal service will be rejected. Examples of such terms include without limitation: 'greek', 
''gr33k", bbbj', 'blow', 'trips to greece', etc.</p>
      <p>Postings violating these rules and our Terms of Use are subject to removal without refund.</p>

      <p><a href="/classifieds/posttype?accept=yes" rel="nofollow">I will abide by these rules and the Site's Terms of Use</a></p>
  </div></div><div class="spacer"></div>