<form method="post" action="" data-ajax="false">

<input type="hidden" name="id" value="{$id}" />

<h2>Appear on every page ... for ${$side_sponsor_price} a month</h2>
Show up in sponsored ads section on the right of every page!<br />
Buy a side sponsor ad upgrade for just ${$side_sponsor_price} a month.<br />
<br />

{if $error}
	<div style="color: red; font-weight: bold;">{$error}</div>
{/if}

<img src="/images/adbuild/as_ss_example.png" style="width:100%; max-width: 429px;" />
{foreach from=$locations key=loc_id item=location}
	<input type="checkbox" value="{$loc_id}" name="loc_id[]" id="check_{$loc_id}" {if $loc_id|in_array:$selected_loc_ids}checked="checked"{/if} />
	<label for="check_{$loc_id}">{$location->getName()} ${$side_sponsor_price}/per month</label>
{/foreach}

<input type="submit" name="submit" class="pm_btn pm_btn_coral" value="Submit" data-role="none" style="width: 100%;" />
<input type="button" name="cancel" class="pm_btn pm_btn_default" value="Cancel" onclick="window.location='/classifieds/myposts'"/>

</form>
