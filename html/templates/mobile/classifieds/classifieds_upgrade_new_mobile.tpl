<form method="post" action="" data-ajax="false">

<input type="hidden" name="id" value="{$id}"/>

<h2>Schedule your reposts, hassle-free.</h2>
Every time your ad is reposted, it appears at the top of its category list. <br />
Maximize your exposure exactly when you tell us to.<br />

<strong>Repost Exactly When You Want You To</strong>
<br />

{if $error}
    <div style="color: red; font-weight: bold;">{$error}</div>
{/if}
<br />

Every
<select name="auto_renew_fr">
{html_options values=$arf_values output=$arf_names selected=$auto_renew_fr}
</select>
at
<select name="auto_renew_time">
{html_options values=$art_values output=$art_names selected=$auto_renew_time}
</select>
{if $timezone}{$timezone}{else}PST{/if}
<select name="auto_renew">
{html_options values=$ar_values output=$ar_names selected=$auto_renew}
</select>
<br />
<br />
Price includes all of your locations. <strong>50%</strong> less per day as compared to daily posting rates! This is a single, non-recurring charge. You may purchase additional reposts at any time by clicking the &quot;My Ads&quot; link at the top of this page.

<input type="submit" name="submit" class="pm_btn pm_btn_coral" value="Submit" style="width: 100%;" />
<input type="button" name="cancel" class="pm_btn pm_btn_default" value="Cancel" onclick="window.location='/classifieds/myposts'"/>

</form>

