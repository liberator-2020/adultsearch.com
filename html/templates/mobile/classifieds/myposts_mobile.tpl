<link href="/css/fa/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<div data-role="navbar">
	<ul>
		<li><a href="/adbuild/" data-ajax="false" data-theme="c">Post An Ad</a></li>
		<li><a href="/classifieds/myposts" class="ui-btn-active" data-ajax="false" data-theme="c">My Ads</a></li>
		<li><a href="/account/mypage" data-ajax="false" data-theme="c">Profile</a></li>
		<li><a href="/account/payments" data-ajax="false" data-theme="c">Payments</a></li>
		<li><a href="/pm/index" data-ajax="false" data-theme="c">PM</a></li>
		<li><a href="{if $ebiz_login_url}{$ebiz_login_url}{else}/account/mywebsite{/if}" data-ajax="false" data-theme="c">Website</a></li>
	</ul>
</div>

<div id="quick_stats_mobile" style="margin-top: 10px; margin-bottom: 10px; padding: 5px; border: 1px solid #666; border-radius: 5px; font-size: 1.1em;">
	Your Account Id#: {$account_id}<br />
	You have <strong>{$count_total}</strong> classified ads{if $count_total > 0} (<strong>{$count_live}</strong> live ads and <strong>{$count_expired}</strong> expired ads){/if}.
	{if $repost}
		<br />
		You have <strong>{$repost}</strong> top of the list credits. <a href="/classifieds/topup" class="pm_btn pm_btn_white" data-ajax="false">Buy top-ups</a>
	{/if}
	{if $escorts[0].type == 2}
		<br /><br />
		We recommend the following site to advertise on: <a href="https://www.tsescorts.com/advertising?utm_source=as&utm_medium=link&utm_campaign=recommend_advertise" class="round_btn" target="_blank">TSescorts.com</a>
	{else if $eccieLinkDisplay}
		<br /><br />
		We recommend the following site to advertise on: <a href="https://eccie.net" target="_blank"><img src="/images/eccie.jpg" width="80" style="border-radius: 4px;" /></a>
	{/if}
</div>

{section name=s1 loop=$escorts}
{assign var=c value=$escorts[s1]}

<div class="ui-body ui-body-a">
	<h3>Id #{$c.id}  - {$c.firstname} in {$c.category} <small>&quot;{$c.name|truncate:100}&quot;</small></h3>
	{if $c.done == 3}
		<span style="color: red;">This ad is waiting for approval.</span><br />
	{/if}

	<img src="{$c.thumb}?{$current}" alt="" id="thumb{$c.id}" height="75" width="75" style="float: left; margin-right: 1em; margin-bottom: 1em;" />

	<p>
	City: {if $c.total_loc==1}{$c.location}{else}<a href="/classifieds/showloc?id={$c.id}" data-ajax="false">Multiple</a>{/if}<br />

	{if $c.done==1}
		Post Date: {$c.updated} <a href="/classifieds/topups?id={$c.id}" data-ajax="false">Top Ups</a><br />
		{if $c.recurring!=1}
			Expires in: {$c.expires_proper}<br />
		{/if}
		{if !$repost}
			Auto Repost Remaining: {$c.auto_renew}<br />
		{/if}
		{if $c.recurring==1}
			Next Billing Date: {$c.nextbill}<br />
		{/if}
		{if $c.auto_renew}
			Next Repost Date: {$c.time_next_proper}<br />
		{/if}
	{/if}
	{if $c.sponsor==1}
		City Thumbnail: {$c.sponsor_expires_proper}<br />
	{/if}
	{if $c.side==1}
		Side Sponsor: {$c.side_expires_proper}<br />
	{/if}

	{if $c.done>0}
		<a href="/classifieds/look?id={$c.id}" class="pm_btn pm_btn_coral" data-ajax="false" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i> View Ad</a>
		<a href="/adbuild/step3?ad_id={$c.id}" class="pm_btn pm_btn_coral" data-ajax="false"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Ad</a>
	{else}
		<a href="/adbuild/step3?ad_id={$c.id}" class="pm_btn pm_btn_coral" data-ajax="false"><i class="fa fa-refresh" aria-hidden="true"></i> Renew</a>
	{/if}
	<br />

	<a href="#popup_manage_{$c.id}" data-rel="popup" data-transition="pop" class="pm_btn pm_btn_white"><i class="fa fa-cog" aria-hidden="true"></i> Manage Your Ad ...</a>

	{if $c.done != 3}
		<a href="#popup_promotion_{$c.id}" data-rel="popup" data-transition="pop" class="pm_btn pm_btn_white"><i class="fa fa-cog" aria-hidden="true"></i> Manage Reposts &amp; Promotion ...</a>
	{/if}

	{strip}
	<div data-role="popup" id="popup_manage_{$c.id}" data-theme="a">
		<ul data-role="listview" data-inset="true" style="min-width:210px;">
			<li data-role="list-divider">Choose an action</li>
			{if $c.done > 0}
				<li><a href="/classifieds/look?id={$c.id}" data-ajax="false">View Ad</a></li>
				<li><a href="/adbuild/step3?ad_id={$c.id}" data-ajax="false">Edit Ad</a></li>
			{/if}
			{if $c.done < 0}
				<li><a href="/adbuild/step3?ad_id={$c.id}" data-ajax="false">Renew Ad</a></li>
			{/if}
			{if $c.done > 0}
				<li><a href="/classifieds/thumbnail?id={$c.id}" data-ajax="false">Edit Thumbnail</a></li>
				<li><a href="/classifieds/duplicate?id={$c.id}" data-ajax="false">Duplicate Ad</a></li>
				<li><a href="/classifieds/move?id={$c.id}" data-ajax="false">Change Location</a></li>
			{/if}
			<li><a href="#" onclick="popup_delete({$c.id});">Delete Ad</a></li>
		</ul>
	</div>

	<div data-role="popup" id="popup_delete_{$c.id}" data-overlay-theme="a" data-theme="a" data-dismissible="false" style="max-width: 320px;">
		<div data-role="header" data-theme="a"><h1>Delete Ad?</h1></div>
		<div role="main" class="ui-content">
			<h3 class="ui-title">Are you sure you want to delete this ad?</h3>
			<a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-a" data-rel="back">Cancel</a>
			<a href="/classifieds/myposts?remove={$c.id}" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-a" data-ajax="false">Delete</a>
		</div>
	</div>
	<div data-role="popup" id="popup_promotion_{$c.id}" data-theme="a">
		<ul data-role="listview" data-inset="true" style="min-width:210px;">
			<li data-role="list-divider">choose an action</li>
			{if $c.done > 0}
				{if $repost || $c.auto_renew}
					<li><a href="/classifieds/repost?id={$c.id}" data-ajax="false">Put Ad At The Top</a></li>
				{/if}
				<li><a href="/classifieds/topup" data-ajax="false">Buy Top-Up Credits</a></li>
				{if $c.total_loc < 11}
					<li><a href="/classifieds/sponsor?id={$c.id}" data-ajax="false">Buy City Thumbnail</a></li>
					<li><a href="/classifieds/side?id={$c.id}" data-ajax="false">Buy Side Sponsor</a></li>
					{if $c.type == 2 || $agency}
						<li><a href="/classifieds/sticky?id={$c.id}" data-ajax="false">Buy Sticky Upgrade</a></li>
					{/if}
				{/if}
{*
				{if $c.total_loc < 11}
					<li><a href="/classifieds/recurring?id={$c.id}" data-ajax="false">Buy Daily Repost</a></li>
				{/if}
*}
			{else}
				<li><a href="/adbuild/step3?ad_id={$c.id}" data-ajax="false">Renew Ad</a></li>
			{/if}
		</ul>
	</div>
	<div data-role="popup" id="popup_repost_{$c.id}" data-overlay-theme="a" data-theme="a" data-dismissible="false" style="max-width: 320px;">
		<div data-role="header" data-theme="a"><h1>Repost Ad?</h1></div>
		<div role="main" class="ui-content">
			<h3 class="ui-title">Are you sure you want to repost this ad now?</h3>
			<a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-a" data-rel="back">Cancel</a>
			<a href="/classifieds/repost?id={$c.id}" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-a" data-ajax="false">Repost</a>
		</div>
	</div>

	<br style="clear: both;" />

	</p>
</div>

{sectionelse}
	<br />You have no classified ads.<br />
{/section}

{literal}
<script type="text/javascript">
function popup_delete(c_id) {
	$('#popup_manage_'+c_id).one("popupafterclose", function(){$('#popup_delete_'+c_id).popup("open")}).popup("close");
}
function popup_repost(c_id) {
	$('#popup_promotion_'+c_id).one("popupafterclose", function(){$('#popup_repost_'+c_id).popup("open")}).popup("close");
}
</script>
{/literal}
