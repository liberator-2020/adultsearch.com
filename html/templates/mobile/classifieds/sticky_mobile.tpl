<style type="text/css">
.days-container {
	display: none;
}
</style>

<form method="post" action="" data-ajax="false">

<input type="hidden" name="id" value="{$id}"/>

<h2>Appear always on the top of the list ... from ${$sticky_min_monthly_price} a month</h2>
Show up at the very top of the list, and be ahead of your competition.<br />
Ads on top of the list have 500% more views than average ad. Your ad will be always at the beginning of the first page and will not be moved to second or subsequent pages.<br />

<img src="/images/adbuild/as_sticky_example.png" alt="" style="width:100%; max-width: 294px;" />
<br />

{if $error}
	<div style="color: red; font-weight: bold;">{$error}</div>
{/if}

{foreach from=$locations key=key item=location}
<label>
	<input type="checkbox" value="{$location['loc']->getId()}" name="loc_id[]" class="city {if $location['has_sticky'] || $location['not_available']}not-allowed{/if}" id="loc_id_{$location['loc']->getId()}" {if $location['has_sticky'] || $location['not_available']}disabled="disabled"{/if}/> {$location['loc']->getName()}{if $location['has_sticky']}<span> (already purchased)</span>{/if}<br />
</label>
<div id="period_{$location['loc']->getId()}" class="days-container">
<label>
	<input type="radio" name="days[{$location['loc']->getId()}]" class="days" value="7">${$location['price_weekly']} for 1 Week - One-Time payment <br />
</label>
<label>
	<input type="radio" name="days[{$location['loc']->getId()}]" class="days" value="30">${$location['price_monthly']} for 1 Month - One-Time payment <br />
</label>
</div>
{/foreach}

<br />

<div id="recurring_div" style="display: none;">
<label>
	<input type="checkbox" name="recurring" id="recurring" value="1">Make this payment recurring - you will be charged automatically every month <br />
</label>
</div>

<input type="submit" name="submit" class="pm_btn pm_btn_coral" value="Submit" style="width: 100%;" />
<input type="button" name="cancel" class="pm_btn pm_btn_default" value="Cancel" onclick="window.location='/classifieds/myposts'"/>

</form>

{literal}
<script type="text/javascript">
function city_changed() {
	var at_least_one = false;
	$('.city').each(function(ind,obj) {
		var loc_id = $(obj).val();
		//console.log('loc_id='+loc_id+', checked='+$(obj).is(':checked'));
		if ($(obj).is(':checked')) {
			$('#period_'+loc_id).show();
			at_least_one = true;
		} else {
			$('#period_'+loc_id).hide();
		}
	});
	if (at_least_one)
		days_changed();
	else
		$('#recurring_div').hide();
}
function days_changed() {
	var some_30_checked = false;
	var some_7_checked = false;
	$('.days').each(function(ind,obj) {
		var loc_id = $(obj).attr('name').substring(5, $(obj).attr('name').length-1);
		//console.log('loc_id='+loc_id+'ind='+ind+',obj=',$(obj),$(obj).val(),$(obj).is(':checked'),$(obj).attr('name'),$(obj).attr('name').length);
		if ($('#loc_id_'+loc_id).is(':checked')) {
			if ($(obj).val() == '30' && $(obj).is(':checked')) {
				some_30_checked = true;
			} else if ($(obj).val() == '7' && $(obj).is(':checked')) {
				some_7_checked = true;
			}
		}
	});
	if (some_30_checked && !some_7_checked) {
		//console.log('all days selected month, display=',$('#recurring_div').css('display'));
		$('#recurring').prop('checked', false);
		$('#recurring_div').show();
	} else {
		$('#recurring_div').hide();
	}
}
$(document).ready(function() {
	$('.city').on('change', function () {city_changed();});
	$('.days').change(function() {days_changed();});
});
</script>
{/literal}
