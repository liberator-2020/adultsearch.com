<form method="post" action="" data-ajax="false">

{if $account_id}
	<input type="hidden" name="account_id" value="{$account_id}"/>
{/if}

<h2>Put your ad to the top of the list ... anytime you want</h2>
Buy top-up credits for just ${$price_topup} per one top up and then you can use this top-up credit to put any of your ads to the top of the list. Anytime you want, even multiple times a day !<br />
Don't miss your opportunity to be the first one the clients will see in the list.<br />
<br /><br />

{if $error}
	<div style="color: red; font-weight: bold;">{$error}</div>
{/if}

<strong>Choose number of top-up credits:</strong>
<select name="number" id="number">
	{html_options values=$option_values output=$option_labels selected=$number}
</select>

<input type="submit" name="submit" class="pm_btn pm_btn_coral" value="Submit" data-role="none" style="width: 100%;" />
<input type="button" name="cancel" class="pm_btn pm_btn_default" value="Cancel" onclick="window.location='/classifieds/myposts'"/>

</form>
