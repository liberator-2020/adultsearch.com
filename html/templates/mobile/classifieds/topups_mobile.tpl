<style type="text/css">
	#edit-payment-method{
		background-color: #5bb75b;
		width: 50%;
		color: #FFFFFF;
		margin: 10px auto 0 auto;
	}
</style>
<div data-role="navbar"><ul>
		<li><a href="/adbuild/" data-ajax="false" data-theme="c">Post An Ad</a></li
		><li><a href="/classifieds/myposts" data-ajax="false" data-theme="c">My Ads</a></li
		><li><a href="/account/mypage" data-ajax="false" data-theme="c">Profile</a></li
		><li><a href="/account/payments" class="ui-btn-active" data-ajax="false" data-theme="c">Payments</a></li
		><li><a href="/pm/index" data-ajax="false" data-theme="c">PM</a></li
		><li><a href="{if $ebiz_login_url}{$ebiz_login_url}{else}/account/mywebsite{/if}" data-ajax="false" data-theme="c">Website</a></li
	></ul>
</div>

{if $error}<span class="error">{$error}</span>{/if}

<form method="post" action="" enctype="multipart/form-data" data-ajax="false">
	 <div>
		{if $id}
			<h2>Latest top-ups of classified id #{$id}</h2>
			<a href="/classifieds/topups" data-ajax="false">View top-ups of all my ads</a>
		{else}
			<h2>Latest top-ups</h2>
		{/if}
		{if $top_ups|count > 0}
			<table>
			<tr>
				<th>Date/Time</th>
				{if !$id}
					<th>Classified Id#</th>
				{/if}
				<th/>
			</tr>
			{foreach from=$top_ups item=t}
			<tr>
				<td>{$t.stamp|date_format:"%m/%d/%Y %l:%M %p %Z"}</td>
				{if !$id}
					<td>{$t.classified_id}</td>
				{/if}
				<td>
					{if $t.author_id == $account_id}
						Manual top-up
					{else}
						Auto top-up
					{/if}
				</td>
			</tr>
			{/foreach}
			</table>
		{else}
			<em>No top ups.</em>
		{/if}
	 </div>
</form>
