<div id="cl">

<form action="" method="post" data-ajax="false">
<input type="hidden" name="where" value="{$where}" />

<h1>Contact Us</h1>

{if $error}<div class="error">{$error}</div>{/if}

<table border="0" cellspacing="0" cellpadding="0" class="b">

 <tr>
	<td class="first">
	Name:<br/>
	<input type="text" name="name" id="name" value="{$name}" class="text" size="40" />
	</td>
 </tr>

 <tr>
	<td class="first">
	E-Mail Address:<br/>
	<input type="text" name="email" id="email" value="{$email}" class="text" size="40" />
	</td>
 </tr>

 <tr>
	<td class="first">
	Contact Phone Number:<br/>
	<input type="text" name="phone" value="{$phone}" class="text" size="13" />
	</td>
 </tr>

 <tr>
	<td class="first">
	Question/Concern/Comment:<br/>
	<textarea name="text" rows="6" cols="35">{$text}</textarea>
	</td>
 </tr>

 <tr>
	<td class="first">{$captcha}</td>
 </tr>

{if !$captcha_ok}
 <tr>
	<td class="first">
	<p>Security Control:</p>
		<p><input type="text" name="captcha_str" value="" autocomplete="off" /></p>
		<span class="smallr">Type the text on the image into the above box.<br/>
		<img src="/captcha.php?{$time}" alt="" /></span>
	</td>
 </tr>
{else}
	 <input type="hidden" name="captcha_str" value="{$captcha_str}" />
 {/if}

 <tr>
	<td class="first">
	<span class="bsubmit"><button type="submit" name="submit" class="bsubmit-r" value="Submit"><span>Submit</span></button>
	</td>
 </tr>

</table>

</form>
</div>
