<div id="interstitial_overlay">
    <div id="interstitial_ad">
        <div id="interstitial_close_1" class="close">[X] Close</div>
        <div id="interstitial_header">Advertisement</div>
        <a href="{$config_site_url}/promo/link_click?cs_id={$interstitial_cs_id}&u={$request_uri}" target="_blank"><img src="{$interstitial_img_src}" /></a>
    </div>
</div>
<script type="text/javascript">
{literal}
$(document).ready(function() {
    interstitial_init({/literal}'{$config_site_url}/promo/interstitial_impression?cs_id={$interstitial_cs_id}'{literal})
});
{/literal}
</script>
