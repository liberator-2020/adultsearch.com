<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>{$title}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="/img/citychix_favicon.ico" />
</head>
<link rel="stylesheet" type="text/css" href="/css/mobile/main.css">
<body>
<div id="wrapper">
<div id="header">{if $smarty.server.REQUEST_URI != "/" && $smarty.server.REQUEST_URI != $core_loc_link}&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:history.back(1);"><img 
src="{$config_image_server}/images/mobile/back-arrow.png" id="backArrow" border="0" /></a>&nbsp;&nbsp;&nbsp;&nbsp;<img src="{$config_image_server}/images/mobile/line.png" width="1" height="45">&nbsp;&nbsp;{/if}&nbsp;&nbsp;<a href="{$homelink}/" data-ajax="false"><img src="{$config_image_server}/images/mobile/logo.png" width="143" height="40" alt="AdultSearch.com Mobile" border="0" 
/></a><div id="headerright">{if $smarty.session.username}{$smarty.session.username}<br/><a href="?logoff=1{php}echo "&amp;redirect=".rawurlencode($_SERVER["REQUEST_URI"]);{/php}" data-ajax="false">Logout</a>{else}<a href="javascript:login();">Login</a>{/if}</div></div>

<script type="text/javascript" src="/js/mobile/mobile.js?5"></script>
<script src="/js/mobile/geo.js"></script>
<script src="/js/mobile/gears_init.js"></script>
<script src="/js/mobile/maps.js"></script>
<script src="/js/mobile/dive.js"></script>
{literal}
<script>
function supports(bool, suffix) {
	var s = "Your browser ";
	if (bool) {
		s += "supports " + suffix + ".";
	} else {
		s += "does not support " + suffix + ". :(";
	}
	return s;
}
function lookup_location() {
	geo_position_js.getCurrentPosition(show_map, show_map_error);
}
function show_map(loc) {
	document.location = "http://adultsearch.com/geo_redirect.php?lat=" + loc.coords.latitude + "&long=" + loc.coords.longitude;
}
function show_map_error() {
	 //location wouldn't work
}
$(function() {
	if (geo_position_js.init()) {
		 lookup_location();
	} else {
		 //some error
	}
})
</script>
{/literal}
  
<div id="contentWrapper">
	<div class="mainCategory">Select Location</div><br>
	<table border="0" cellpadding="0" cellspacing="0" width="100%" class="center">
    <tr>
    	<td class="homeGlobes" width="33%"><a data-ajax="false" href="/m/location?view=16046"><img src="{$config_image_server}/images/mobile/globe-us.png" width="64" height="65" border="0"><br>USA</a></td>
        <td class="homeGlobes" width="33%"><a data-ajax="false" href="/m/location?view=16047"><img src="{$config_image_server}/images/mobile/globe-canada.png" width="64" height="65" border="0"><br>Canada</a></td>
        <td class="homeGlobes" width="33%"><a data-ajax="false" href="/m/location?view=41973"><img src="{$config_image_server}/images/mobile/globe-uk.png" width="64" height="65" border="0"><br>United Kingdom</a></td>
    </tr>
	</table>

{if $locations}
<div style="clear:both;"></div>
<div class="mainCategory"><em>- Or - Select near by location to <b>{$loc_name}</b></em></div>
{section name=sec loop=$locations}
  <div class="subCategory"><a data-ajax="false" href="{if $a}http://{$locations[sec].sub}.adultsearch.com{else}/m/location?view={$locations[sec].loc_id}{/if}"
title="{$locations[sec].loc_name} AdultSearch">{$locations[sec].loc_name}</a></div>
{/section}
{/if}
</div>

<div id="footer"><br /><a data-ajax="false" href="{$homelink}/">Adult Search</a> | <a data-ajax="false" href="{$full_version_link}" rel="nofollow">Full Site</a> | <a data-ajax="false" href="/m/location">Change Location</a>
| <a data-ajax="false" href="/contact{php}echo"?where=".rawurlencode($_SERVER["REQUEST_URI"]);{/php}" rel="nofollow">Contact Us</a>
</div>
</div>
{if !$smarty.session.username}<div id="login"><div style="float: right;"><a data-ajax="false" href="javascript:login();"><img src="{$config_image_server}/images/mobile/x.png" width="17" height="17" border="0" /></a></div>
<div style="background-color: #19639b;"><form action="" method="post"><input type="hidden" name="login" value="1" data-ajax="false"/>
	<table>
    	<tr>
        	<td>E-Mail:</td><td><input id="email" value="" maxLength="200" name="login_email" size="20" /></td>
        </tr>
        <tr>
    		<td>Password:</td><td><input type="password" maxLength="200" name="login_password" /></td>
        </tr>
        <tr>
        	<td></td><td><input type="checkbox" value="1" name="saveme" />Auto-Login from this phone</td>
        </tr>
        <tr>
        	<td></td><td><input type="submit" value="Login" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-ajax="false" href="/account/signup">Signup</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-ajax="false" href="/account/reset">Password Help</a></td>
        </tr>
	</table>
</form></div>
<div style="background-image: url('{$config_image_server}/images/mobile/login_foot.png'); width: 100%; height: 7px;"></div>
</div>{/if}

{literal}
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-5124307-2']);
_gaq.push(['_setDomainName', 'adultsearch.com']);
_gaq.push(['_trackPageview']);
(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
{/literal}

</body>
</html>
