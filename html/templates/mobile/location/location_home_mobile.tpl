<script src="/js/mobile/geo.js"></script>
<link href="/css/ui/jquery-ui-1.8.1.custom.css" rel="stylesheet" type="text/css" />

{literal}
<script type="text/javascript">
function show_map(loc) {
	var lat = loc.coords.latitude;
	var lng = loc.coords.longitude
	if (lat && lng)
		{/literal}document.location = "https://{$site_domain}/geo_redirect?lat="+lat+"&long="+lng;{literal}
}
function show_map_error() {
	//location wouldn't work
}
jQuery(function() {
	var nlr = getParameterByName('nlr');
	if (nlr == '1') {
		//no location redirect
	} else if (geo_position_js.init()) {
		geo_position_js.getCurrentPosition(show_map, show_map_error);
	} else {
		 //some error
	}
});
</script>
{/literal}

<div id="homelocation-mobile">

<div style="width: 295px;">
	<div class="ui-bar ui-bar-a">
		<h3>Type city or choose from below:</h3>
	</div>
	<div class="ui-body ui-body-a" style="padding: 0px; background-color: transparent;">
		<form id="form_search_city" action="/search_location" method="get" data-ajax="false">
			<input type="search" id="city" name="city" value="" placeholder="Your city" autocomplete="off" />
			<input type="submit" name="submit" value="Search" />
		</form>
	</div>
</div>
			
<div data-role="collapsible" data-inset="false">
	<h3>United States</h3>
	<ul data-role="listview">
	{include "mobile/location/location_home_mobile_maingroup.tpl" main_groups=$main_groups main_group_key='us' all_locations=$all_locations}
    </ul>
</div>

<div data-role="collapsible" data-inset="false">
	<h3>Canada</h3>
	<ul data-role="listview">
	{include "mobile/location/location_home_mobile_maingroup.tpl" main_groups=$main_groups main_group_key='ca' all_locations=$all_locations}
    </ul>
</div>

<div data-role="collapsible" data-inset="false">
	<h3>United Kingdom</h3>
	<ul data-role="listview">
	{include "mobile/location/location_home_mobile_maingroup.tpl" main_groups=$main_groups main_group_key='uk' all_locations=$all_locations}
    </ul>
</div>

<div data-role="collapsible" data-inset="false">
	<h3>Europe</h3>
	<ul data-role="listview">
	{include "mobile/location/location_home_mobile_maingroup.tpl" main_groups=$main_groups main_group_key='europe' all_locations=$all_locations}
    </ul>
</div>

<div data-role="collapsible" data-inset="false">
	<h3>Latin America &amp; Caribbean</h3>
	<ul data-role="listview">
	{include "mobile/location/location_home_mobile_maingroup.tpl" main_groups=$main_groups main_group_key='latin' all_locations=$all_locations}
    </ul>
</div>

<div data-role="collapsible" data-inset="false">
	<h3>The Middle East</h3>
	<ul data-role="listview">
	{include "mobile/location/location_home_mobile_maingroup.tpl" main_groups=$main_groups main_group_key='middle_east' all_locations=$all_locations}
    </ul>
</div>

<div data-role="collapsible" data-inset="false">
	<h3>Asia</h3>
	<ul data-role="listview">
	{include "mobile/location/location_home_mobile_maingroup.tpl" main_groups=$main_groups main_group_key='asia' all_locations=$all_locations}
    </ul>
</div>

<div data-role="collapsible" data-inset="false">
	<h3>Africa</h3>
	<ul data-role="listview">
	{include "mobile/location/location_home_mobile_maingroup.tpl" main_groups=$main_groups main_group_key='africa' all_locations=$all_locations}
    </ul>
</div>

<div data-role="collapsible" data-inset="false">
	<h3>Australia &amp; Oceania</h3>
	<ul data-role="listview">
	{include "mobile/location/location_home_mobile_maingroup.tpl" main_groups=$main_groups main_group_key='australia' all_locations=$all_locations}
    </ul>
</div>

</div>

