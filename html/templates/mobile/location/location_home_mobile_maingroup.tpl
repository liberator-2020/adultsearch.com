{* $main_groups array, $main_group_key string and $all_locations bool passed from parent template homepage.tpl *}
  
{assign var='main_group' value=$main_groups.$main_group_key}

{foreach $main_group as $state}
	<li>
		<div data-role="collapsible" data-inset="false">
			<h3 class="nomargin">{$state.state_name}</h3>
			<ul data-role="listview">
			{foreach $state.cities as $city}
				{if $city.loc_name}
					<li class="ui-nodisc-icon ui-alt-icon"><a data-ajax="false" href="{$city.loc_url}">{$city.loc_name}</a></li>
				{/if}
			{/foreach}
			</ul>
		</div>
	</li>
{/foreach}

