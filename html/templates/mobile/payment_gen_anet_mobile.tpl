<form method="post" action="" name="f" data-ajax="false">

	{$ccform_hidden}

	{if !$no_promotion}
	<div>
		<label class="bluelabel">Promocode</label>
		If you have a promocode, enter it in following field and tap 'Apply' button.<br />
		If you don't have any promocode, you can ignore this and fill out the billing details below and tap 'Submit button'.<br />
		Promocode:
		<input name="promo" id="promo" value="{$promo}" size="20" type="text" />
		<input type="submit" name="continue" class="pm_btn pm_btn_coral" id="adbuild_continue_btn" value="Apply" data-role="none" />
	</div>
	{/if}

	<div>
		<h3>Secure Checkout: $<span id="checkouttotal">{$total}</span></h3>
	</div>

	<div>
		{if $error}
			<div style="border: 1px solid red; background-color: white; padding: 5px; color: red;">
				<strong>Error:</span><br />
				<span class="error">{$error}</span>
			</div>
		{/if}
	</div>

	{if $recurring_amount}
		<div id="recurring_confirm">
			<div>
				This is recurring charge. <strong>You can cancel the subscription anytime</strong>.<br />
				<label>
					<input type="checkbox" name="recurring_confirm" value="1" {if $recurring_confirm}checked="checked"{/if}/>
					I confirm this recurring charge.
				</label>
			</div>
		</div>
	{/if}
	<div>
		<label class="bluelabel">First Name:</label>
		<input type="text" name="cc_firstname" size="40" value="{$cc_firstname}" maxlength="60" />
	</div>
	<div>
		<label class="bluelabel">Last Name:</label>
		<input type="text" name="cc_lastname" size="40" value="{$cc_lastname}" />
	</div>
	<div>
		<label class="bluelabel">Address:</label>
		<input type="text" name="cc_address" size="40" value="{$cc_address}" />
	</div>
	<div>
		<label class="bluelabel">ZIP/Postal Code:</label>
		<input type="text" name="cc_zipcode" size="10" value="{$cc_zipcode}" />
	</div>
	{if !$payment_nocity}
	<div>
		<label class="bluelabel">City:</label>
		<input type="text" name="cc_city" size="20" value="{$cc_city}" />
	</div>
	{/if}
	{if !$payment_nostate}
	<div>
		<label class="bluelabel">State/Province:</label>
		<input type="text" name="cc_state" size="20" value="{$cc_state}" />
	</div>
	{/if}
	<div>
		<label class="bluelabel">Country:</label>
		<select name="cc_country">{html_options options=$country_options selected=$cc_country}</select>
	</div>
	<div>
		<label class="bluelabel">Credit Card Number:</label>
		<input type="number" name="cc_cc" size="20" value="{$cc_cc}" />
		<img class="img1" src="/images/classifieds/Credit_card_logos_small_noamex.png" />
	</div>
	<div>
		<label class="bluelabel">Expiration Month:</label>
		<select name="cc_month">
			<option value="">-- Select --</option>
			<option value="1"{if $cc_month==1} selected="selected"{/if}>January (01)</option>
			<option value="2"{if $cc_month==2} selected="selected"{/if}>February (02)</option>
			<option value="3"{if $cc_month==3} selected="selected"{/if}>March (03)</option>
			<option value="4"{if $cc_month==4} selected="selected"{/if}>April (04)</option>
			<option value="5"{if $cc_month==5} selected="selected"{/if}>May (05)</option>
			<option value="6"{if $cc_month==6} selected="selected"{/if}>June (06)</option>
			<option value="7"{if $cc_month==7} selected="selected"{/if}>July (07)</option>
			<option value="8"{if $cc_month==8} selected="selected"{/if}>August (08)</option>
			<option value="9"{if $cc_month==9} selected="selected"{/if}>September (09)</option>
			<option value="10"{if $cc_month==10} selected="selected"{/if}>October (10)</option>
			<option value="11"{if $cc_month==11} selected="selected"{/if}>November (11)</option>
			<option value="12"{if $cc_month==12} selected="selected"{/if}>December (12)</option>
		</select>
	</div>
	<div>
		<label class="bluelabel">Expiration Year:</label>
		<select name="cc_year">
			<option value="">-- Select --</option>
			<option value="17"{if $cc_year==17} selected="selected"{/if}>2017</option>
			<option value="18"{if $cc_year==18} selected="selected"{/if}>2018</option>
			<option value="19"{if $cc_year==19} selected="selected"{/if}>2019</option>
			<option value="20"{if $cc_year==20} selected="selected"{/if}>2020</option>
			<option value="21"{if $cc_year==21} selected="selected"{/if}>2021</option>
			<option value="22"{if $cc_year==22} selected="selected"{/if}>2022</option>
			<option value="23"{if $cc_year==23} selected="selected"{/if}>2023</option>
			<option value="24"{if $cc_year==24} selected="selected"{/if}>2024</option>
			<option value="25"{if $cc_year==25} selected="selected"{/if}>2025</option>
			<option value="26"{if $cc_year==26} selected="selected"{/if}>2026</option>
			<option value="27"{if $cc_year==27} selected="selected"{/if}>2027</option>
			<option value="28"{if $cc_year==28} selected="selected"{/if}>2028</option>
			<option value="29"{if $cc_year==29} selected="selected"{/if}>2029</option>
			<option value="30"{if $cc_year==30} selected="selected"{/if}>2030</option>
		</select>
	</div>
	<div>
		<label class="bluelabel">Security Code (CVC2):</label>
		<input type="number" name="cc_cvc2" value="{$cc_cvc2}" size="5" />
	</div>
	<div>

	{if !$captcha_ok}
	<div>
		<label> Security Image:</label>
		<input type="text" name="captcha_str" value="" autocomplete="off" />
		<div class="alt"> <img src="/captcha.php?{$current}" alt="" /></div>
	</div>
	{else}
		<input type="hidden" name="captcha_str" value="{$captcha_str}" />
	{/if}

	{if $payment_note}
	<div>
		{$payment_note}
	</div>
	{/if}

	<input type="submit" name="submit" class="pm_btn pm_btn_coral" value="Submit" data-role="none" style="width: 100%;"/>

	<hr />

	<div>
		For help please contact:<br />
		U.S./Canada: 1-702-935-1688<br />
		(9am to 5pm EST Mon - Fri)<br />
		<a href="mailto:support@adultsearch.com">support@adultsearch.com</a>
	</div>

</form>
