<style type="text/css">
div#payment_error div {
	border: 1px solid red; background-color: white; padding: 5px; color: red;
}
</style>

<form method="post" action="" name="f" id="payment_form" data-ajax="false">
<input type="hidden" name="submitted" value="1" />
{$ccform_hidden}

	<div>
		<h3>You will be charged : $<span id="checkouttotal">{$total}</span></h3>
	</div>
	
	<div id="payment_error">
		{if $error}
			<div>
				{$error}
			</div>
		{/if}
	</div>

	{if $recurring_amount}
		<div id="recurring_confirm">
			<div>
				This is recurring charge. <strong>You can cancel the subscription anytime</strong>.<br />
				<label>
					<input type="checkbox" name="recurring_confirm" value="1" {if $recurring_confirm}checked="checked"{/if}/>
					I confirm this recurring charge.
				</label>
			</div>
		</div>
	{/if}

	<div>
		<h3>Pay {if $recurring_amount}${$recurring_amount}{else}${$total}{/if} with my advertising balance</h3>
		{if $budget_available}
			Currently you have ${$budget} in your advertising account balance, so you can pay this purchase by substracting that amount from your advertising balance.<br />
			<br />
			<button type="submit" name="pay_budget" value="1" class="pm_btn pm_btn_coral" data-role="none" style="width: 100%;">Pay with advertising budget</button>
			<br />
		{else}
			Currently you have ${$budget} in your advertising account balance.<br />
			<br />
			We are sorry but you don't have enough balance in your budget to pay for this purchase, please contact <a href="mailto:agency@adultsearch.com">agency@adultsearch.com</a> to find out how to recharge your advertising balance.<br />
		{/if}
	</div>

	<hr />

	<div>
		For help please contact:<br />
		U.S./Canada: 1-702-935-1688<br />
		(9am to 5pm EST Mon - Fri)<br />
		<a href="mailto:agency@adultsearch.com">agency@adultsearch.com</a>
	</div>

</form>

