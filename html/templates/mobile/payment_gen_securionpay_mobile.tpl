<style type="text/css">
div#payment_error div {
	border: 1px solid red; background-color: white; padding: 5px; color: red;
}
</style>

<form method="post" action="" name="f" id="payment_form" data-ajax="false">
<input type="hidden" name="submitted" value="1" />
<input type="hidden" name="saved_card_id" id="saved_card_id" value="" />
<input type="hidden" name="image_front_data" id="image_front_data" value="" />
<input type="hidden" name="image_back_data" id="image_back_data" value="" />
{$ccform_hidden}

<input type="hidden" name="amount_minor" id="amount_minor" value="{$amount_minor}" />
<input type="hidden" name="token" id="token" value="{$token}" />


	<div>
		<h3>Secure Checkout: $<span id="checkouttotal">{$total}</span></h3>
	</div>

	<div id="payment_error">
	   	{if $error}
			<div>
				{$error}
			</div>
	   	{/if}
	</div>

	{if !$no_promotion}
	{if $cc_redeemed}
		<label class="bluelabel">Promotion Code Redeemed:</label>
		${$cc_redeemed}
	{else}
		<div>
			<label class="bluelabel">Promocode</label>
			If you have a promocode, enter it in following field and tap 'Apply' button.<br />
			If you don't have any promocode, you can ignore this and fill out the billing details below and tap 'Submit button'.<br />
			Promocode:
			<input name="promo" id="promo" value="{$promo}" size="20" type="text" />
			<button type="submit" name="continue" class="pm_btn pm_btn_coral" id="adbuild_continue_btn" data-role="none">Apply</button>
		</div>
	{/if}
	{/if}

	{if $recurring_amount}
		<div id="recurring_confirm">
			<div>
				This is recurring charge. <strong>You can cancel the subscription anytime</strong>.<br />
				<label>
					<input type="checkbox" name="recurring_confirm" value="1" {if $recurring_confirm}checked="checked"{/if}/>
					I confirm this recurring charge.
				</label>
			</div>
		</div>
	{/if}

	{if $cards}
		<label>Credit card:</label>
		<select id="card" class="form-control" style="width: 295px;">
			<option value="">- Choose remembered card -</option>
			{foreach from=$cards item=card}
			<option value="{$card.id}" data-firstname="{$card.firstname}" data-lastname="{$card.lastname}" data-address="{$card.address}" data-city="{$card.city}" data-state="{$card.state}" data-zipcode="{$card.zipcode}" data-country="{$card.country}" data-month="{$card.month}" data-year="{$card.year}" >{$card.label}</option>
			{/foreach}
		</select>
		<br />
		Or enter new credit card below:
	{/if}

	<div>
		<label class="bluelabel">First Name:</label>
		<input type="text" name="cc_firstname" id="firstname" size="40" value="{$cc_firstname}" maxlength="60" />
	</div>
	<div>
		<label class="bluelabel">Last Name:</label>
		<input type="text" name="cc_lastname" id="lastname" size="40" value="{$cc_lastname}" />
	</div>
	<div>
		<label class="bluelabel">Address:</label>
		<input type="text" name="cc_address" id="address" size="40" value="{$cc_address}" />
	</div>
	<div>
		<label class="bluelabel">City:</label>
		<input type="text" name="cc_city" id="city" size="20" value="{$cc_city}" />
	</div>
	<div>
		<label class="bluelabel">State/Province:</label>
		<input type="text" name="cc_state" id="state" size="20" value="{$cc_state}" />
	</div>
	<div>
		<label class="bluelabel">ZIP/Postal Code:</label>
		<input type="text" name="cc_zipcode" id="zipcode" size="10" value="{$cc_zipcode}" />
	</div>
	<div>
		<label class="bluelabel">Country:</label>
		<select name="cc_country" id="country">{html_options options=$country_code_options selected=$cc_country}</select>
	</div>

	<div>
		<label class="bluelabel">Credit Card Number:</label>
		<input type="number" name="cc_cc" id="cc_cc" size="20" value="{$cc_cc}" data-securionpay="number"/>
		<img width="40" alt="Mastercard" src="{$config_site_url}/images/i_mastercard.png" style="vertical-align: middle;">
		<img width="40" alt="Visa" src="{$config_site_url}/images/i_visa.png" style="vertical-align: middle;">
	</div>
	<div>
		<label>Info about prepaid/gift cards: We do NOT accept Vanilla Cards, we DO accept NetSpend cards</label>
	</div>
	<div>
		<label class="bluelabel">Expiration Month:</label>
		<select name="cc_month"  id="cc_month" data-securionpay="expMonth">
			<option value="">-- Select --</option>
			<option value="1"{if $cc_month==1} selected="selected"{/if}>January (01)</option>
			<option value="2"{if $cc_month==2} selected="selected"{/if}>February (02)</option>
			<option value="3"{if $cc_month==3} selected="selected"{/if}>March (03)</option>
			<option value="4"{if $cc_month==4} selected="selected"{/if}>April (04)</option>
			<option value="5"{if $cc_month==5} selected="selected"{/if}>May (05)</option>
			<option value="6"{if $cc_month==6} selected="selected"{/if}>June (06)</option>
			<option value="7"{if $cc_month==7} selected="selected"{/if}>July (07)</option>
			<option value="8"{if $cc_month==8} selected="selected"{/if}>August (08)</option>
			<option value="9"{if $cc_month==9} selected="selected"{/if}>September (09)</option>
			<option value="10"{if $cc_month==10} selected="selected"{/if}>October (10)</option>
			<option value="11"{if $cc_month==11} selected="selected"{/if}>November (11)</option>
			<option value="12"{if $cc_month==12} selected="selected"{/if}>December (12)</option>
		</select>
	</div>

	<div>
		<label class="bluelabel">Expiration Year:</label>
		<select name="cc_year" id="cc_year" data-securionpay="expYear">
			<option value="">-- Select --</option>
			<option value="18"{if $cc_year==18} selected="selected"{/if}>2018</option>
			<option value="19"{if $cc_year==19} selected="selected"{/if}>2019</option>
			<option value="20"{if $cc_year==20} selected="selected"{/if}>2020</option>
			<option value="21"{if $cc_year==21} selected="selected"{/if}>2021</option>
			<option value="22"{if $cc_year==22} selected="selected"{/if}>2022</option>
			<option value="23"{if $cc_year==23} selected="selected"{/if}>2023</option>
			<option value="24"{if $cc_year==24} selected="selected"{/if}>2024</option>
			<option value="25"{if $cc_year==25} selected="selected"{/if}>2025</option>
			<option value="26"{if $cc_year==26} selected="selected"{/if}>2026</option>
			<option value="27"{if $cc_year==27} selected="selected"{/if}>2027</option>
			<option value="28"{if $cc_year==28} selected="selected"{/if}>2028</option>
			<option value="29"{if $cc_year==29} selected="selected"{/if}>2029</option>
			<option value="30"{if $cc_year==30} selected="selected"{/if}>2030</option>
			<option value="31"{if $cc_year==31} selected="selected"{/if}>2031</option>
		</select>
	</div>
	<div>
		<label class="bluelabel">Security Code (CVC2):</label>
		<input type="number" name="cc_cvc2" id="cc_cvc2" value="{$cc_cvc2}" size="5" data-securionpay="cvc" size="5" />
	</div>
	<div>

{*
<tr id="card_photos_row">
	<td class="first">Credit card photos:</td>
	<td class="second">
		<p style="border-radius: 6px; border: 1px solid #333; padding: 4px; margin-top: 10px; margin-bottom: 10px;">
			To prove that you do have the credit card in posession, please take photo of the front and back of your card.<br />
			We will discard these photos once your credit card is approved.
		</p>
		<label id="image_front_btn_label" for="image_front_btn" class="yellowlink"{if $image_front_thumb_data} style="display:none;"{/if}>
			Upload front photo
			<input type="file" id="image_front_btn" accept="image/*;capture=camera" style="display: none;" onchange="handle_files(this)">
		</label>
		<div id="image_front_set" {if !$image_front_thumb_data}style="display: none;"{/if}>
			Credit card front photo:<br />
			<img src="{if $image_front_thumb_data}{$image_front_thumb_data}{/if}" style="max-width: 150px; max-height: 100px;"/>
			<button type="button" id="image_front_set_delete" class="btn btn-xs btn-warning">Delete</button>
		</div>
		<br />
		<label id="image_back_btn_label" for="image_back_btn" class="yellowlink"{if $image_back_thumb_data} style="display:none;"{/if}>
			Upload back photo
			<input type="file" id="image_back_btn" accept="image/*;capture=camera" style="display: none;" onchange="handle_files(this)">
		</label>
		<div id="image_back_set" {if !$image_back_thumb_data}style="display: none;"{/if}>
			Credit card back photo:<br />
			<img src="{if $image_back_thumb_data}{$image_back_thumb_data}{/if}" style="max-width: 150px; max-height: 100px;"/>
			<button type="button" id="image_back_set_delete" class="btn btn-xs btn-warning">Delete</button>
		</div>
		<br style="clear: both;" />
		<br style="clear: both;" />
	</td>
</tr>
*}

	{if !$captcha_ok && !$isadmin}
	<div>
		<label> Security Image:</label>
		<input type="text" name="captcha_str" id="captcha_str" value="" autocomplete="off" />
		<div class="alt"> <img src="/captcha.php?{$current}" alt="" /></div>
	</div>
	{else}
		<input type="hidden" name="captcha_str" value="{$captcha_str}" />
	{/if}

	{if $payment_note}
	<div>
		{$payment_note}
	</div>
	{/if}

	<button type="submit" class="pm_btn pm_btn_coral" data-role="none" style="width: 100%;">Submit</button>

	<hr />

	<div>
		For help please contact:<br />
		U.S./Canada: 1-702-935-1688<br />
		(9am to 5pm EST Mon - Fri)<br />
		<a href="mailto:support@adultsearch.com">support@adultsearch.com</a>
	</div>

</form>

{literal}
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="https://securionpay.com/js/securionpay.js"></script>

<script type="text/javascript">
function card_changed() {
	var option = $('#card').find(':selected');
	var card_id = $('#card').val();
	console.log('d',card_id,option);
	//console.log('card_id="'+card_id+'"');
	if (!card_id) {
		$('#saved_card_id').val('')
		$('#firstname').val('').prop('disabled', false);
		$('#lastname').val('').prop('disabled', false);
		$('#address').val('').prop('disabled', false);
		$('#city').val('').prop('disabled', false);
		$('#zipcode').val('').prop('disabled', false);
		$('#state').val('').prop('disabled', false);
		$('#country').val('').prop('disabled', false).trigger('change');
		$('#cc_month').val('').prop('disabled', false);
		$('#cc_year').val('').prop('disabled', false);
		//$('#card_photos_row').show();
		return;
	}
	//console.log('card:',card);
	$('#saved_card_id').val(card_id);
	$('#firstname').val($(option).data('firstname')).prop('disabled', true);
	$('#lastname').val($(option).data('lastname')).prop('disabled', true);
	$('#address').val($(option).data('address')).prop('disabled', true);
	$('#city').val($(option).data('city')).prop('disabled', true);
	$('#zipcode').val($(option).data('zipcode')).prop('disabled', true);
	$('#state').val($(option).data('state')).prop('disabled', true);
	$('#country').val($(option).data('country')).prop('disabled', true);
	$('#cc_month').val($(option).data('month')).prop('disabled', true);
	$('#cc_year').val($(option).data('year')).prop('disabled', true);
	//$('#card_photos_row').hide();
}

function handle_files(obj) {
	console.log('handle_files, obj=',obj);
	var filesToUpload = obj.files;
	var file = filesToUpload[0];

	var reader = new FileReader();  
	reader.onload = function(e) {
		var img = document.createElement("img");
		img.src = e.target.result;
		img.addEventListener('load', function () {
			var canvas = document.createElement("canvas");
			var ctx = canvas.getContext("2d");
			ctx.drawImage(img, 0, 0);

			var MAX_WIDTH = 800;
			var MAX_HEIGHT = 600;
			var width = img.width;
			var height = img.height;
			//console.log('width='+width+', height='+height);

			if (width > height) {
				if (width > MAX_WIDTH) {
				height *= MAX_WIDTH / width;
				width = MAX_WIDTH;
				}
			} else {
				MAX_WIDTH = 600;
				MAX_HEIGHT = 800;
				if (height > MAX_HEIGHT) {
				width *= MAX_HEIGHT / height;
				height = MAX_HEIGHT;
				}
			}
			canvas.width = width;
			canvas.height = height;
			var ctx = canvas.getContext("2d");
			ctx.drawImage(img, 0, 0, width, height);
			//console.log('width='+width+', height='+height);

			var dataurl_jpg = canvas.toDataURL("image/jpeg");
			//console.log('dataurl_jpg size = '+dataurl_jpg.length);

			console.log(obj);
			if ($(obj).attr('id') == 'image_front_btn') {
				console.log('front');
				$('#image_front_data').val(dataurl_jpg);
				$('#image_front_set img').attr('src', dataurl_jpg);
				$('#image_front_btn_label').hide();
				$('#image_front_set').show();
			} else if ($(obj).attr('id') == 'image_back_btn') {
				console.log('back');
				$('#image_back_data').val(dataurl_jpg);
				$('#image_back_set img').attr('src', dataurl_jpg);
				$('#image_back_btn_label').hide();
				$('#image_back_set').show();
			}
			$(obj).val('');
		});
	}
	reader.readAsDataURL(file);
	return false;
}

var $form = null;

function validateForm() {
	var errors = Array();
	if ($('#amount_minor').val() > 0) {
		if (!$('#firstname').val())
			errors.push('Firstname is mandatory');
		if (!$('#lastname').val())
			errors.push('Lastname is mandatory');
		if (!$('#address').val())
			errors.push('Address is mandatory');
		if (!$('#city').val())
			errors.push('City is mandatory');
		if (!$('#state').val())
			errors.push('State is mandatory');
		if (!$('#zipcode').val())
			errors.push('Zip is mandatory');
		if (!$('#country').val())
			errors.push('Country is mandatory');
		if (!$('#cc_cc').val())
			errors.push('Card number is mandatory');
		if (!$('#cc_month').val())
			errors.push('Expiration month is mandatory');
		if (!$('#cc_year').val())
			errors.push('Expiration year is mandatory');
		if (!$('#cc_cvc2').val())
			errors.push('Card Security Code (CVC) is mandatory');
		{/literal}{if !$isadmin}{literal}
		if (!$('#captcha_str').val())
			errors.push('Security Control code (captcha) is mandatory');
		{/literal}{/if}{literal}
	}
	if (errors.length > 0) {
		var html = '<div>';
		for (var i = 0; i < errors.length; i++) {
			html += errors[i]+'<br />';
		}
		html += '</div>';
		$('#payment_error').html(html);
		$(window).scrollTo($('#payment_error'));
		return false;
	}
	return true;
}
function paymentFormSubmit(e) {

	if ($('#card').val()) {
		$form.unbind();
		$form.submit();
		return true;
	}

	e.preventDefault();

	if (!validateForm())
		return false;

	$form.find('button#next').prop('disabled', true);   // Disable form submit button to prevent repeatable submits
	$('#payment_error').html('');

	if ($('#amount_minor').val() > 0 && !$('#token').val())
		SecurionPay.createCardToken($form, createCardTokenCallback);
	else
		finishPayment();
};

function createCardTokenCallback(token) {

	console.log('token:',token);

	if (token.error) {
		console.log('tok err');
		$('#payment_error').html(token.error.message);
		$form.find('button#next').prop('disabled', false);
		$(window).scrollTo($('#payment_error'));
	} else {
		$('#token').val(token.id);
		finishPayment();

		//not using 3DSecure
		//SecurionPay.verifyThreeDSecure({
		//  amount: $('#amount_minor').val(),
		//  currency: 'USD',
		//  card: token.id
		//}, verifyThreeDSecureCallback);
	}
}

function verifyThreeDSecureCallback(token) {
	if (token.error) {
		$('#payment_error').html('<div>'+token.error.message+'</div>');
		$form.find('button#next').prop('disabled', false);
		$(window).scrollTo($('#payment_error'));
	} else {
		$('#token').val(token.id);
		finishPayment();
	}
}

function finishPayment() {
	$form.unbind();
	$form.submit();
}

$(document).ready(function() {
	$('#card').change(function() {
		card_changed();
	});

	$('#cc_cc').change(function() {
		$('#token').val('');
	});
	$('#cc_month').change(function() {
		$('#token').val('');
	});
	$('#cc_year').change(function() {
		$('#token').val('');
	});
	$('#cc_cvc2').change(function() {
		$('#token').val('');
	});

	$('#image_front_set_delete').click(function() {
		$('#image_front_data').val('');
		$('#image_front_set img').attr('src', '');
		$('#image_front_set').hide();
		$('#image_front_btn_label').show();
	});
	$('#image_back_set_delete').click(function() {
		$('#image_back_data').val('');
		$('#image_back_set img').attr('src', '');
		$('#image_back_set').hide();
		$('#image_back_btn_label').show();
	});

	Securionpay.setPublicKey('{/literal}{$config_securionpay_public_key}{literal}');

	$form = $('form#payment_form');
	$form.submit(paymentFormSubmit);
});
</script>
{/literal}
