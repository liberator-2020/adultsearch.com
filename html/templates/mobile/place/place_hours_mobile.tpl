{* $hours, $current_weekday passed from parent template place/place_place.tpl *}
{if $hours}
	<table id="hours-mobile">
		<tr {if $current_weekday == "monday"} class="active"{/if}><td>Monday</td><td>{$hours.monday}</td></tr>
		<tr {if $current_weekday == "tuesday"} class="active"{/if}><td>Tuesday</td><td>{$hours.tuesday}</td></tr>
		<tr {if $current_weekday == "wednesday"} class="active"{/if}><td>Wednesday</td><td>{$hours.wednesday}</td></tr>
		<tr {if $current_weekday == "thursday"} class="active"{/if}><td>Thursday</td><td>{$hours.thursday}</td></tr>
		<tr {if $current_weekday == "friday"} class="active"{/if}><td>Friday</td><td>{$hours.friday}</td></tr>
		<tr {if $current_weekday == "saturday"} class="active"{/if}><td>Saturday</td><td>{$hours.saturday}</td></tr>
		<tr {if $current_weekday == "sunday"} class="active"{/if}><td>Sunday</td><td>{$hours.sunday}</td></tr>
	</table>
{/if}
