<link rel="stylesheet" type="text/css" href="{$config_site_url}/css/listings.mobile.css">
<script src="/js/tools/rateit/jquery.rateit.js" type="text/javascript"></script>

{if $display_add_link || $isworker || $isadmin}
	<a href="/worker/{$worker_link}?mobile=2" class="pm_btn pm_btn_coral" rel="nofollow" data-ajax="false">Add {$what}</a>
{/if}

{if $filter}
	<div class="ui-corner-all custom-corners">
		<div class="ui-bar ui-bar-a">
			<h3>You Searched For</h3>
		</div>
		<div class="ui-body ui-body-a">
			{section name=filter loop=$filter}
			{assign var=f value=$filter[filter]}
				<a href="{$f.link}" class="ui-btn ui-icon-delete ui-btn-icon-left" data-ajax="false">{$f.name}</a>
			{/section}
		</div>
	</div>
{/if}


{if $refine}
	<div data-role="collapsible">
		<h4>{$r.name} Search Filters</h4>
		{if $r.name == "Location"}
			{section name=a loop=$r.alt}
			{assign var=a value=$r.alt[a]}
				<a href="{$a.link}{if $link}{$link}{/if}" data-ajax="false">{$a.name}</a> - {$a.c}<br />
			{/section}
		{else}
			{section name=r loop=$refine}
			{assign var=r value=$refine[r]}
				{if $r.alt|@count > 1 && $r.alt[0].id == $r.alt[1].id}
				<select name="{$r.alt[0].type}" onChange="javascript:addFilter(this.value,1);">
					<option value="">{$r.name}</option>
					{section name=a loop=$r.alt}
					{assign var=a value=$r.alt[a]}
						<option value="{$a.type}">{$a.name} - {$a.c}</option>
					{/section}
				</select>
				{else}
				<select name="{$r.alt[0].type}" onChange="javascript:addFilter(this.name,this.value);">
					<option value="">{$r.name}</option>
					{section name=a loop=$r.alt}
					{assign var=a value=$r.alt[a]}
						<option value="{$a.id}">{$a.name} - {$a.c}</option>
					{/section}
				</select>
				{/if}
			{/section}
		{/if}
	</div>
{/if}

<div class="title">{$pager_array.from} - {$pager_array.to} of {$total} {$what}{if $total>1}s{/if}</div>

{if $sc}
	<ul id="list-places" data-role="listview" data-inset="true">
	{section name=sc loop=$sc}
	{assign var=c value=$sc[sc]}
		<li class="ui-nodisc-icon ui-alt-icon" {if $c.sponsor} style="border-color: #ff740e;"{/if}>
			<a href="{$c.url}" title="{$c.name}{if $c.title} {$c.title}{/if}" data-ajax="false">
				{if $c.thumb}
					<img src="{$config_image_server}/place/t/{$c.thumb}" alt="" width="100" height="75" border="0" class="placeThumb" />
				{else}
					<img src="{$config_site_url}/images/placeholder.gif" alt="" border="0" width="100" height="75" class="placeThumb" />
				{/if}
				<strong>{$c.name}</strong><br />
				{if $c.review}<div class="rateit" data-rateit-value="{$c.recommend/$c.review}" data-rateit-ispreset="true" data-rateit-readonly="true"></div><br />{/if}
				{$c.address}<br />
				{if $c.extra}{$c.extra}<br/>{/if} 
				{$c.loc_name} {$c.s} {$c.zipcode} {if $c.distance}<br />({$c.distance} miles){/if}
				<br style="clear: both;" />
				{if $c.phone}<h2 class="bigPhone">{$c.phone}</h2>{/if}
			</a> 
		</li>
	{/section}
	</ul>
{else}
	No Results
{/if}

<div data-role="controlgroup" data-type="horizontal">
	{if $pager_array.pager[0].type == "prev"}
		<a class="ui-btn ui-mini ui-corner-all" href="{$pager_array.pager[0].link}" data-ajax="false">Prev</a>
	{else}
		<a class="ui-btn ui-mini ui-corner-all ui-state-disabled" href="{$pager_array.pager[0].link}" data-ajax="false">Prev</a>
	{/if}
	{foreach $pager_array.pager item=page}
		{if $page.type == "current"}
			<a class="ui-btn ui-mini ui-corner-all ui-state-disabled" href="{$page.link}" data-ajax="false">{$page.page}</a>
		{elseif $page.type == "page"}
			<a class="ui-btn ui-mini ui-corner-all" href="{$page.link}" data-ajax="false">{$page.page}</a>
		{/if}
	{/foreach}
	{if $pager_array.pager[($pager_array.pager|@count - 1)].type == "next"}
		<a class="ui-btn ui-mini ui-corner-all" href="{$pager_array.pager[($pager_array.pager|@count - 1)].link}" data-ajax="false">Next</a>
	{else}
		<a class="ui-btn ui-mini ui-corner-all ui-state-disabled" href="{$pager_array.pager[($pager_array.pager|@count - 1)].link}" data-ajax="false">Next</a>
	{/if}
</div>

