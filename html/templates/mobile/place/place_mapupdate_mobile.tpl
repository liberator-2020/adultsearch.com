{extends file="./../../place/place_mapupdate_base.tpl"}
{block name="table"}
    <table>
        <tr>
            <td colspan="2">
                <b>You can mark the exact location of {$name} by clicking on the map with your mouse.</b>
            </td>
        </tr>
        <tr>
            <td>
                <div id="map" style="width: 95vw; height: 400px;"></div>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <form action="" method="post" data-ajax="false">
                    Latitude: <input type="input" size="10" name="lat" style="margin-bottom: 20px" value=""
                                     id="lat"/><br/>
                    Longitude: <input type="input" size="10" name="long" value="" id="long"/><br/>
                    <input type="submit" name="Submit" value="Submit"/>
                </form>
            </td>
        </tr>
    </table>
{/block}
