<link href="/css/ratings.css" rel="stylesheet" type="text/css">
<link href="/css/places.css?20190709" rel="stylesheet" type="text/css">
<link href="/css/currency_convert.css" type="text/css" rel="stylesheet">
<link href="/css/mobile.css.new" rel="stylesheet" type="text/css">
<link href="/css/wrapper.css?20180315" rel="stylesheet" type="text/css" />
<script src="/js/tools/rateit/jquery.rateit.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.cookie.js"></script>
<script src="/js/currency_convert.js" type="text/javascript"></script>
<script src="/js/jquery.tools.scrollable.js" type="text/javascript"></script>
<script type="text/javascript" src="{$config_site_url}/js/tools/fancybox134/jquery.fancybox-1.3.4.pack.js"></script>

{literal}
<style type="text/css">
#mapcontainer {width: 100%; height: 100%;}
#map_canvas_2 {width: 75vw; height: 400px;}
.mobile-popup-header{display: flex; flex-direction: column;}
#map_canvas img{width: 100%;}
.preloader {
	transform: scale(0.5);
	border: none !important;
	width: 225px;
}
.static-map{
	display: none;
}
#fancybox-title{
    bottom: -20px;
    left: 10px !important;
    color: white;
}
</style>
{/literal}

<h1>{$name}</h1>

{if $overall}
	<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" class="rateOverall">
		<div class="rateit bigstars" data-rateit-value="{$overall}" data-rateit-ispreset="true" data-rateit-readonly="true"	data-rateit-starwidth="32" data-rateit-starheight="32"></div>
		<meta itemprop="ratingValue" content="{$overall}" />
	</div>
{/if}

{$address}
{if $website}
	<a href="http://{$site_domain}/businessowner/{$business_shortcut}" title="adult search - business owners" data-ajax="false">Visit Their Website</a>
{/if}
<br />

{if $c0}
	{$c0}
{else}
	<table id="place_stats">
	<tbody>
	{foreach from=$c1 key=key item=item}
		<tr><td>{$item.label}</td><td>{$item.val}</td></tr>
	{/foreach}
	</tbody>
	</table>
{/if}

{if $currency_button}{$currency_button}{/if}

<div id="summary">
	{if $description}
		{$description}
	{/if}
	{if $info}
		{$info}
	{/if}
	{if $facilities}
		{$facilities}
	{/if}
</div>


{if $account_level>2||$can_edit_place}
	<a href="/worker/{$editlink}?id={$id}" class="ui-btn ui-btn-c ui-mini ui-btn-inline" data-ajax="false">edit</a>
{/if}
{if $isowner}
	<a href="owner?id={$id}" class="ui-btn ui-btn-c ui-mini ui-btn-inline" data-ajax="false">Edit My Business</a>
	<a href="/account/payments" class="ui-btn ui-btn-c ui-mini ui-btn-inline" data-ajax="false">Billing</a>
{/if}
{if $business_link}
	<a href="http://{$site_domain}/businessowner/{$business_shortcut}" class="ui-btn ui-btn-c ui-mini ui-btn-inline" title="adult search - business owners" data-ajax="false">Your Business?</a>
{/if}
<a href="{$state_link}/contact?where={php}echo rawurlencode($_SERVER["REQUEST_URI"]);{/php}" rel="nofollow" class="ui-btn ui-btn-c ui-mini ui-btn-inline" data-ajax="false">Report An Error</a>
<br />


<!-- photos block -->
{if $photos}
<br />
<div style="width: 295px;">
	<div class="ui-bar ui-bar-a">
		<h3>Photos</h3>
	</div>
	<div class="ui-body ui-body-a" style="padding: 0px; background-color: transparent;">
		<a href="{$photos.image}" class="lightbox" id="lightbox0" target="blank" data-ajax="false"><img src="{$photos.thumb}" alt="{$photos.name}" style="width: 100%;"/></a>
	</div>
{/if}
<!-- /photos block -->


<!-- map block -->
{if !$nomap && $map}
<br />
<div style="width: 295px;">
	<div class="ui-bar ui-bar-a">
		<h3>Directions</h3>
	</div>
	<div class="ui-body ui-body-a" style="padding: 0px; background-color: transparent;">
			<div id="map_canvas">
		{if $map.loc_lat && $map.loc_long}
			<a class="maptrg" href="#mapcontainer" title="directions for {$name} {$loc_name}, how to get to the {$name} {$loc_name}">
				{if $map.map_image_url}
					<img src="{$map.map_image_url}" alt="Here should be map with this place" />
				{elseif $map.map_image_update_url}
					<div class="map-container">
						<img src="/images/ajax_loader_128.gif" class="preloader" alt="" />
						<img src="" alt="Here should be map with this place" data-src="{$map.map_image_update_url}" class="static-map">
					</div>
				{else}
					<div class="map-container">
						<img src="/images/ajax_loader_128.gif" class="preloader" alt="" />
						<img src="" alt="Here should be map with this place" class="static-map" data-src="{$map.map_static_direct}">
					</div>
				{/if}
			</a>
		{/if}
	</div>
	<div class="mapLink">
		<a class="maptrg" href="#mapcontainer" title="Directions for {$name} {$loc_name}, how to get to the {$name} {$loc_name}">View Larger Map / Directions / Street View</a>
	</div>

	</div>
</div>
{/if}
<!-- /map block -->


{if $hours}
<br />
<div style="width: 295px;">
	<div class="ui-bar ui-bar-a">
		<h3>Hours</h3>
	</div>
	<div class="ui-body ui-body-a" style="padding: 0px; background-color: transparent;">
		<table id="hours-mobile">
			<tr {if $current_weekday == "monday"} class="active"{/if}><td>Monday</td><td>{$hours.monday}</td></tr>
			<tr {if $current_weekday == "tuesday"} class="active"{/if}><td>Tuesday</td><td>{$hours.tuesday}</td></tr>
			<tr {if $current_weekday == "wednesday"} class="active"{/if}><td>Wednesday</td><td>{$hours.wednesday}</td></tr>
			<tr {if $current_weekday == "thursday"} class="active"{/if}><td>Thursday</td><td>{$hours.thursday}</td></tr>
			<tr {if $current_weekday == "friday"} class="active"{/if}><td>Friday</td><td>{$hours.friday}</td></tr>
			<tr {if $current_weekday == "saturday"} class="active"{/if}><td>Saturday</td><td>{$hours.saturday}</td></tr>
			<tr {if $current_weekday == "sunday"} class="active"{/if}><td>Sunday</td><td>{$hours.sunday}</td></tr>
		</table>
	</div>
</div>
{/if}

<br />
<div style="width: 295px;">
	<div class="ui-bar ui-bar-a">
		<h3>{$totalreview} AdultSearch User Review{if $totalreview!=1}s{/if}</h3>
	</div>
	<div class="ui-body ui-body-a" style="padding: 0px; background-color: transparent;">
		<a class="ui-btn ui-btn-a" href="{if $review_link_add} {$review_link_add} {else} review-add?id={$id} {/if}" title="Write a Review" rel="nofollow" data-ajax="false">Write a review</a>

		<div>
			{section name=q loop=$quickread}
			{assign var=q value=$quickread[q]}
				<!-- single review -->
				<a name="review{$q.review_id}" data-ajax="false"></a>
				<div id="rev{$q.review_id}" class="reviewIndividualMobile">
					<!-- hidden SEO -->
					<span class="rating hidden">{$q.star}</span> <span class="item hidden"> <span class="fn">{$name}</span> </span>
					<!-- account level 2 mgmt -->
					<div class="linkbutton">
						{if $ismember}<a href="/pm/compose?recipient={$q.account_id}">Send PM</a>{/if}
						{if $account_level>2||$q.account_id==$smarty.session.account} | <a href="{$q.link_edit}" data-ajax="false">Edit</a> <a href="{$q.link_remove}" onclick="return confirm('sure?');">Remove</a> <a href="{$q.link_move}" data-ajax="false">Move</a>
					<a href="{$q.link_makeforum}" data-ajax="false">Move to Forum</a>
					<a href="{$id}/review/{$q.review_id}/uploadVideo" data-ajax="false">Upload video</a>
						{/if}
					</div>
					<!-- review intro -->
					<span class="thumb"><img src="{if $q.avatar}{$config_image_server}/avatar/{$q.avatar}{else}/images/icons/User_1.jpg{/if}" alt="" width="40" height="40" align="left" /></span>
					<div class="name">
					<!-- images of provider -->
					{if $q.pfilename}
					<div id="carousel">Provider: {$q.provider_name}
						<div id="slides">
							<ul>
							{section name=s9 loop=$q.pfilename max=1}{assign var=p value=$q.pfilename[s9]}
								<li><a href="//img.adultsearch.com/rprovider/{$p}" class="lightbox" data-ajax="false"><img src="//img.adultsearch.com/rprovider/t/{$p}" /></a></li>
							{/section}
							</ul>
						</div>
					</div>
					{/if}

					{if $smarty.session.account==3}<a href="/user/{$q.username}" class="uprofile" data-ajax="false">{$q.username}</a>{else}{$q.username}{/if}
					<div class="ratings">
						<div class="rateit" data-rateit-value="{$q.star}" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-max="5"></div>
					</div>
					<div class="date">Reviewed {$q.reviewdate} </div>
				</div>
				<!-- review content -->
				{if $q.provider_name}<b>{if $provider_name_question}{$provider_name_question}{else}Provider Name{/if}:</b> {$q.provider_name}<br/>
				{/if}
				{$q.review}
				<!-- IP address -->
				{if $q.ips|@count>1}
					<div id="addressIP"> Users {section name=i loop=$q.ips}{assign var=ip value=$q.ips[i]}<span>{$ip}</span>{if !$smarty.section.i.last}, {/if}{/section} that reviewed this location were using the same IP addresses. </div>
				{/if}
				<!--	-->
				{if $q.comment}
				<div id="revc{$q.review_id}" class="comments">
					<div class="commentsTitle"><a name="comment{$q.review_id}" data-ajax="false"></a> Member Comments </div>
					{section name=q2 loop=$q.comment}
					{assign var=q2 value=$q.comment[q2]}
						<div class="commentSingle" id="co{$q2.comment_id}"{if $smarty.section.q2.index>3} style="display:none"{/if}>
							<img src="{if $q2.avatar}{$config_image_server}/avatar/{$q2.avatar}{else}/images/icons/User_1.jpg{/if}" alt="{$q2.username}">
							<span class="commentDate">{$q2.date}
								{if $isadmin || $q2.owner} | <span id="co{$q2.comment_id}x"> <a href="" onclick="_ajaxsc('get', '', 'id={$id}&commentremove={$q2.comment_id}'); return false;">Delete</a> </span> {/if}
							</span>
							<span class="userName">{$q2.username}</span>
							<div class="userComment">{$q2.comment}</div>
						</div>

						{if $smarty.section.q2.index == 3&& $smarty.section.q2.total>4}
						{assign var=left value=$smarty.section.q2.total-4}
						<div id="moreComments"> <a href="#" onclick="$('#revc{$q.review_id} > div[id^=co]').show();$(this).parent().hide();return false;">Display {$left} Additional Comment{if $left>1}s{/if}</a> </div>
						{/if}
					{/section}
				</div>
				{/if}

				<!-- submit comment -->
				{if !$smarty.session.account}
		            <br /><br />
		            <a href="/account/signin?origin={php}echo rawurlencode($_SERVER["REQUEST_URI"]);{/php}">Please login to add comments to this review</a>
		        {else}
					<div id="{$q.review_id}">
					<form class="form" method="post" action="" id="form{$q.review_id}" data-ajax="false">
						<input type="hidden" name="comment" value="1" />
						<input value="{$q.review_id}" type="hidden" name="review_id" />
						<textarea rows="1"	name="c" style="overflow:hidden;" placeholder="Add a comment or question..."></textarea>
						<input type="submit" value="Submit" onclick="commentreview('{$q.review_id}')" />
					</form>
					</div>
				{/if}
			</div>
			<!-- /single review -->

		{/section}
		<!-- pagination -->
 		{if $review_pager}<div id="page">Page: {$review_pager}</div>{/if}
		</div>
		<!-- /reviewBlock -->
	</div>
</div>

<div style="display:none;">
<div id="mapcontainer">
		<div class="mapinfo">
			<a href="{$link}" title="{$name} {$loc_name}">{$name}</a><br/>
			{$address}{if $phone}<br/>{$phone}{/if}
		</div>
			<form action="#" onsubmit="setDirections();return false" name="directionform" method="get">
				<table>
					<tr style="vertical-align: top;" class="mobile-popup-header">
						<th align="left">From:&nbsp;</th>
						<td><input type="text" size="50" id="fromAddress" name="from" value="{$from}" placeholder="Address, city, state, zip"/></td>
						<th align="left">&nbsp;&nbsp;To:&nbsp;</th>
						<td align="right">
							<input type="text" size="50" id="toAddress" name="to" value="{if $map.loc_lat}{$map.loc_lat},{$map.loc_long}{else}{$to}{/if}" />
							<input name="button" onclick="setDirections(); return false;" type="submit" value="Get Directions!" />
						</td>
					</tr>
				</table>
			</form>
		<br/>
		<table class="directions">
			<tr>
				<td valign="top">
					<div id="directions_2"></div>
					<div id="map_canvas_2" style=""></div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>If the location of this place is wrong on the map, please <a href="{$link_map_update}" rel="nofollow" data-ajax="false">click here</a> to point us the correct location on the map!</b>
				</td>
			</tr>
			{*<tr>*}
				{*<th colspan="2">Street View (if possible)</th>*}
			{*</tr>*}
			{*<tr>*}
				{*<td valign="top" colspan="2"><div id="pano" style="width: 1000px;  height: 400px;";></div></td>*}
			{*</tr>*}
		</table>
	</div>
</div>
{literal}
<script type="text/javascript">
//<![CDATA[

jQuery("textarea").focus(function() {
	jQuery(this).attr("rows",5);
	jQuery(this).next().show();
});
jQuery("textarea").blur(function() {
	if (jQuery(this).val() == "") {
		jQuery(this).attr("rows",1);
		jQuery(this).next().hide();
	}
});

function commentreview(i) {
	console.log('commentreview');
	var id = $("#form"+i+" input[name=review_id]").val();
	var c = $("#form"+i+" textarea[name=c]").val();
	$.post("", { 'id': {/literal}{$id}{literal}, 'reviewcomment': 1, 'comment': 1, 'review_id': id, 'c': c})
	.done(function(data) {
		console.log('.post done');
		var cs = $("#revc"+i);
		if (cs.length > 0)
			cs.append(data);
		else
			$("#"+i).before('<div id="revc'+i+'" class="comments"><div class="commentsTitle"><a name="comment'+i+'" data-ajax="false">Member Comments</a></div>'+data+'</div>');
	});
	$("#form"+i+" > textarea").val($("#form"+i+" > textarea").attr("placeholder"));
}

//]]>
</script>
<script type="text/javascript">

var map, dirLayer;
L.mapquest.key = {/literal}'{$map.public_key}'{literal};

function map_directions_init() {
	map = L.mapquest.map('map_canvas_2', {
		center: [{/literal}{$map.loc_lat}, {$map.loc_long}{literal}],
		layers: L.mapquest.tileLayer('map'),
		zoom:   12
	});

	map.addControl(L.mapquest.control());
	L.marker([{/literal}{$map.loc_lat}, {$map.loc_long}{literal}]).addTo(map);
}

function setDirections() {
	var from = document.directionform.from.value,
		to   = document.directionform.to.value,
		directions = L.mapquest.directions();

	if (from === '') {
		alert("Please type your starting point address");
		return;
	}

	if(dirLayer !== undefined) {
		map.removeLayer(dirLayer);
	}

	directions.route({
		start: getFormattedDirection(from),
		end:   getFormattedDirection(to)
	}, function (err, data) {
		if (err.message) {
			console.log(err);
		} else {
			dirLayer = L.mapquest.directionsLayer({
				directionsResponse: data,
				fitBounds:          false
			}).addTo(map);
		}
	});
}

// Format input value to mapquest valid variable if it is coordinates
function getFormattedDirection(str) {
	var partsOfStr = str.split(','),
		notNumbers = 0;

	partsOfStr.forEach(function (item) {
		if (isNaN(item)) {
			notNumbers++;
		}
	});

	if (notNumbers === 0 && partsOfStr.length === 2) {
		return [partsOfStr[0], partsOfStr[1]];
	}

	return str;
}

$(document).ready(function(){
 $("a.maptrg").fancybox({
  'hideOnContentClick': false, // so you can handle the map
  'overlayColor'	  : 'black',
  'overlayOpacity'	: 0.6,
  'autoDimensions': false,
  'width': '95%',
  'height': '600',
  'onComplete': function(){
	map_directions_init();
	$("#fancybox-close").css({"opacity":0.5});
	$("#fancybox-content").css({"padding":'10px'});
  },
  'onCleanup': function() {
   var myContent = this.href;
   $(myContent).unwrap();
  } // fixes inline bug
 });
});

var preloader = $('.preloader');

if ($(preloader).length){
	$(preloader).each(function(i, item){
		var img = new Image(),
			map = $(this).siblings('.static-map'),
			object = $(this);

		img.onload = function () {
			$(map).attr('src', img.src);
			$(map).show();
			$(object).hide();
		};
		img.src = $(map).data('src');
	});
}

//]]>
</script>
{/literal}
