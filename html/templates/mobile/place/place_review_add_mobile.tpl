You are about to leave a review for <strong>{$name}</strong>
<br /><br />

<form id="review" name="review" method="post" action="" data-ajax="false">
	{if $edit}<input type="hidden" name="edit" value="{$edit}" />{/if}
	<input type="hidden" name="id" value="{$id}" />
	{if $error}<span style="color:red;font-weight:bold;">{$error}</span><br /><br />{/if}
	Would you recommend this {$title}?<br />
	<select name="recommend">{$recommend}</select><br /><br />
	Comments about {$name}:<br />
	<textarea id="detail" name="comment" rows="7" cols="30" {if $blocked}disabled="disabled"{/if}>{$comment}</textarea><br /><br />
	{if $blocked}
		<p>You may not leave review for this place. If you think there is a mistake, please <a href="/contact{php}echo"?where=".rawurlencode($_SERVER["REQUEST_URI"]);{/php}" data-ajax="false">contact us</a> right away.</p>
	{else}
		<input type="submit" class="orangeButton" /><br /><br />
		<a href="{$link}?id={$id}" data-ajax="false">Go Back</a>
	{/if}
	<input type="hidden" name="adding" value="1" />
</form>
