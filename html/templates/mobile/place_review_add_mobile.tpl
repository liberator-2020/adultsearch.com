<script src="/js/tools/rateit/jquery.rateit.min.js" type="text/javascript"></script>
<link href="/js/tools/rateit/rateit.css" rel="stylesheet" type="text/css">
<form id="review" name="review" method="post" action="" data-ajax="false">
	<div id="contentWrapper">
		<div class="generalRoundBox" style="align: left;">
			You are about to leave a review for <strong>{$name}</strong><br />
			{if $edit}<input type="hidden" name="edit" value="{$edit}" />{/if}
			<input type="hidden" name="id" value="{$id}" />
			{if $error}<p style="color:red;font-weight:bold;">{$error}</p>{/if}
			{if $forummove}
				Where would you like to move ?<br />
				<select name="forum_id"><option value="">-- select --</option>
		            <option value="1">General Talk</option>
        		    <option value="2">Strip Clubs &amp; Strippers</option>
		            <option value="3">Massage Parlors &amp; Masseuses</option>
        		    <option value="4">Sex Shops</option>
		            <option value="5">LifeStyle Clubs</option>
        		    <option value="6">Escorts</option>
				</select><br />
				What would be the topic title ?<br />
				<input type="text" name="topic_title" size="75" value="{$topic_title}" />
			{else}
				{if $register}
					<br/>You need to register in order to post a review, if you have a membership, type your email &amp; password below to login, if not you will be registered with the information you provide<br/>
					<br />Your E-Mail Address<br />
					<input type="text" name="email" value="{$email}" /><br/>
					<br />Create a Password:<br/>
					<input type="password" name="password" value="{$password}" /><br/>
				{/if}
				<br />Would you recommend this {$title} ?<br />
				{*<select name="recommend">{$recommend}</select>*}
				<input type="hidden" name="star" value="{$star}" id="star" />
				<div class="rateit bigstars" id="rateit10b" data-rateit-step="1" data-rateit-starwidth="32" data-rateit-starheight="32" data-rateit-backingfld="#star" data-rateit-resetable="false" data-rateit-ispreset="true"></div>
				<br /><br />Comments about {$name}
				<br /><textarea id="detail" name="comment" rows="7" cols="35" {if $blocked}disabled="disabled"{/if}>{$comment}</textarea>
			{/if}
			{if $blocked}
				<p>You may not leave review for this place. If you think there is a mistake, please <a href="/contact{php}echo"?where=".rawurlencode($_SERVER["REQUEST_URI"]);{/php}" data-ajax="false">contact us</a> right away.</p>
			{else}
				<br /><br /><input type="submit" value="Submit" class="orangeButton" />
				<br /><br /><div class="blueButton"><a href="{$link}?id={$id}" data-ajax="false">Go Back</a></div>
			{/if}
			<input type="hidden" name="adding" value="1" />
		</div>
	</div>
</form>
