<link href="/css/fa/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="/css/pm.css" rel="stylesheet" type="text/css" />

<div data-role="navbar"><ul>
        <li><a href="/classifieds/myposts" data-ajax="false" data-theme="c">My Ads</a></li
        ><li><a href="/account/mypage" data-ajax="false" data-theme="c">Profile</a></li
		><li><a href="/account/payments" data-ajax="false" data-theme="c">Payments</a></li
        ><li><a href="/pm/index" class="ui-btn-active" data-ajax="false" data-theme="c">PM</a></li
        ><li><a href="{if $ebiz_login_url}{$ebiz_login_url}{else}/account/mywebsite{/if}" data-ajax="false" data-theme="c">Website</a></li
    ></ul>
</div>

<div data-role="navbar" style="margin-top: 0.5em;"><ul>
        <li><a href="/pm/" data-ajax="false">Inbox{if $unread > 0} <span style="color: coral;">({$unread})</span>{/if}</a></li
        ><li><a href="/pm/sent" data-ajax="false">Sent</a></li
        ><li><a href="/pm/blocked" class="ui-btn-active" data-ajax="false">Blocked Users</a></li
    ></ul>
</div>
<br />

<ul data-role="listview" >
    <li class="ui-nodisc-icon ui-alt-icon msg_list_item">
        <span class="msg_list_author">Username</span> |
        <span class="msg_list_date">Since</span> |
        <span class="msg_list_subject"></span>
    </li>
    {if $blocked|count == 0}
        <li class="ui-nodisc-icon ui-alt-icon msg_list_item">No blocked users</li>
    {else}
        {foreach from=$blocked item=b}
            <li class="ui-nodisc-icon ui-alt-icon msg_list_item">
                <span class="msg_list_author">{$b.username}</span> | 
                <span class="msg_list_date">{$b.stamp|date_format:"M d, g:i a"}</span> | 
                <span class="msg_list_subject"><a class="pm_btn_small pm_btn_white" href="/pm/unblock?id={$b.id}" data-ajax="false"><i class="fa fa-ban" aria-hidden="true"></i> Unblock</a></span>
            </a></li>
        {/foreach}
    {/if}
</ul>

