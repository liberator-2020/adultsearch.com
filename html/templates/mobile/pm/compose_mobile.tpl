<link href="/css/fa/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<div data-role="navbar"><ul>
        <li><a href="/classifieds/myposts" data-ajax="false" data-theme="c">My Ads</a></li
        ><li><a href="/account/mypage" data-ajax="false" data-theme="c">Profile</a></li
		><li><a href="/account/payments" data-ajax="false" data-theme="c">Payments</a></li
        ><li><a href="/pm/index" class="ui-btn-active" data-ajax="false" data-theme="c">PM</a></li
        ><li><a href="{if $ebiz_login_url}{$ebiz_login_url}{else}/account/mywebsite{/if}" data-ajax="false" data-theme="c">Website</a></li
    ></ul>
</div>

<div data-role="navbar" style="margin-top: 0.5em;"><ul>
        <li><a href="/pm/" data-ajax="false">Inbox{if $unread > 0} <span style="color: coral;">({$unread})</span>{/if}</a></li
        ><li><a href="/pm/sent" data-ajax="false">Sent</a></li
        ><li><a href="/pm/blocked" data-ajax="false">Blocked Users</a></li
    ></ul>
</div>


<h2>Compose message</h2>
{if $error}
	<div style="color: red; font-weeight: bold;">{$error}</div>
{/if}

{if $phone_not_verified}
    To prevent sending out spam messages, you need to verify your phone number.<br />
    We will not use this phone number anywhere on our website nor in your private messages, we need it solely to prevent spam submissions.<br />
	<a href="/account/verify?origin={$request_uri}" class="pm_btn pm_btn_coral">Verify phone number</a>
{else}

	{if $recipient_id}
	<form id="pm_compose" method="post" action="/pm/compose" enctype="multipart/form-data" data-ajax="false">
		<div>
			<label>Recipient</label>: {$recipient}
		</div>
		<div>
			<label>Message</label>
			<textarea name="content" rows="8"></textarea>
		</div>
		<div>
			<label>Images</label><br />
			<label>Image 1</label><input type="file" id="file1" class="file" data-num="1" name="file1" />
			<div id="file2_w" style="display:none;"><label>Image 2</label><input type="file" id="file2" class="file" data-num="2" name="file2" /></div>
			<div id="file3_w" style="display:none;"><label>Image 3</label><input type="file" id="file3" class="file" data-num="3" name="file3" /></div>
			<div id="file4_w" style="display:none;"><label>Image 4</label><input type="file" id="file4" class="file" data-num="4" name="file4" /></div>
			<div id="file5_w" style="display:none;"><label>Image 5</label><input type="file" id="file5" class="file" data-num="5" name="file5" /></div>
			<div id="file6_w" style="display:none;"><label>Image 6</label><input type="file" id="file6" class="file" data-num="6" name="file6" /></div>
		</div>
		<input type="hidden" name="recipient" value="{$recipient_id}" />
		<input type="hidden" name="reply" value="{$reply}" />
		<button class="pm_btn pm_btn_coral" type="submit" name="submit" value="submit" data-role="none"><i class="fa fa-check" aria-hidden="true"></i> Send</button>
	</form>
	{/if}

{/if}

{literal}
<script type="text/javascript">
jQuery(function() {
	jQuery('.file').each(function(ind,obj) {
		jQuery(obj).change(function() {
			var next_num = jQuery(this).data('num') + 1;
			jQuery('#file'+next_num+'_w').show();
		});
	});
});
</script>
{/literal}
