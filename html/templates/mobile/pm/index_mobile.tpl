<div data-role="navbar"><ul>
		<li><a href="/adbuild/" data-ajax="false" data-theme="c">Post An Ad</a></li
		><li><a href="/classifieds/myposts" data-ajax="false" data-theme="c">My Ads</a></li
		><li><a href="/account/mypage" data-ajax="false" data-theme="c">Profile</a></li
		><li><a href="/account/payments" data-ajax="false" data-theme="c">Payments</a></li
		><li><a href="/pm/index" class="ui-btn-active" data-ajax="false" data-theme="c">PM</a></li
		><li><a href="{if $ebiz_login_url}{$ebiz_login_url}{else}/account/mywebsite{/if}" data-ajax="false" data-theme="c">Website</a></li
	></ul>
</div>

<div data-role="navbar" style="margin-top: 0.5em;"><ul>
		<li><a href="/pm/" class="ui-btn-active" data-ajax="false">Inbox{if $unread > 0} <span style="color: coral;">({$unread})</span>{/if}</a></li
		><li><a href="/pm/sent" data-ajax="false">Sent</a></li
		><li><a href="/pm/blocked" data-ajax="false">Blocked Users</a></li
	></ul>
</div>
<br />

<ul data-role="listview" >
	<li class="ui-nodisc-icon ui-alt-icon msg_list_item">
		<span class="msg_list_date">Date/Time</span> | 
		<span class="msg_list_author">Author</span> | 
		<span class="msg_list_subject">Subject</span>
	</li>
	{if $messages|count == 0}
		<li class="ui-nodisc-icon ui-alt-icon msg_list_item">No messages</li>
	{else}
		{foreach from=$messages item=message}
			<li class="ui-nodisc-icon ui-alt-icon msg_list_item {if !$message.read_stamp}not_read_yet{/if}"><a data-ajax="false" href="{$message.read_link}">
				<span class="msg_list_date">{$message.sent_stamp|date_format:"M d, g:i a"}</span> | 
				<span class="msg_list_author">{$message.author}</span> | 
				<span class="msg_list_subject">{$message.subject}</span>
			</a></li>
		{/foreach}
	{/if}
</ul>

