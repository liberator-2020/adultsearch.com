<link href="/css/fa/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<div data-role="navbar"><ul>
        <li><a href="/classifieds/myposts" data-ajax="false" data-theme="c">My Ads</a></li
        ><li><a href="/account/mypage" data-ajax="false" data-theme="c">Profile</a></li
		><li><a href="/account/payments" data-ajax="false" data-theme="c">Payments</a></li
        ><li><a href="/pm/index" class="ui-btn-active" data-ajax="false" data-theme="c">PM</a></li
        ><li><a href="{if $ebiz_login_url}{$ebiz_login_url}{else}/account/mywebsite{/if}" data-ajax="false" data-theme="c">Website</a></li
    ></ul>
</div>

<div data-role="navbar" style="margin-top: 0.5em;"><ul>
        <li><a href="/pm/" data-ajax="false">Inbox{if $unread > 0} <span style="color: coral;">({$unread})</span>{/if}</a></li
        ><li><a href="/pm/sent" data-ajax="false">Sent</a></li
        ><li><a href="/pm/blocked" data-ajax="false">Blocked Users</a></li
    ></ul>
</div>

{if $error}
	<div style="color: red; font-weeight: bold;">{$error}</div>
{else}
	<div id="my_view">
		{if $message.author == 'me'}
			<h2>Message sent to {$message.recipient} on {$message.sent_stamp|date_format:"m/d/Y g:i A"}</h2>
		{else if $message.recipient == 'me'}
			<h2>Message received from {$message.author} on {$message.sent_stamp|date_format:"m/d/Y g:i A"}</h2>
		{else}
			<h2>Message from {$message.author} to {$message.recipient} sent on {$message.sent_stamp|date_format:"m/d/Y g:i A"}</h2>
		{/if}
		{if $message.subject}
			<div>
				<label>Subject:</label> {$message.subject}<br />
			</div>
		{/if}
		<div id="msg_content">
			{$message.content}
		</div>

		<div>
			{foreach from=$message.images item=url}
				<img src="{$url}" style="max-width: 200px; max-height: 200px; margin-top: 10px;" /><br />
			{/foreach}
		</div>

		<a class="pm_btn pm_btn_coral" href="/pm/compose?recipient={$message.author_id}&reply={$message.id}" data-ajax="false"><i class="fa fa-reply" aria-hidden="true"></i> Reply</a>
		<a class="pm_btn pm_btn_white" href="/pm/delete?id={$message.id}"><i class="fa fa-trash" aria-hidden="true" data-ajax="false"></i> Delete</a>
		{if $message.author == 'me'}
			<a class="pm_btn pm_btn_white" href="/pm/block?id={$message.recipient_id}" data-ajax="false"><i class="fa fa-ban" aria-hidden="true"></i> Block {$message.recipient}</a>
		{else if $message.recipient == 'me'}
			<a class="pm_btn pm_btn_white" href="/pm/block?id={$message.author_id}" data-ajax="false"><i class="fa fa-ban" aria-hidden="true"></i> Block {$message.author}</a>
		{/if}
	</div>
{/if}
