
<form action="{$action}" method="post" data-ajax="false">
{$hidden}

<div>
	<div class="ui-bar ui-bar-a">
		<h3>Spam Protection</h3>
	</div>
	<div class="ui-body ui-body-a" style="padding: 0px; background-color: transparent;">

		{if $spamerror}
			<div class="error">Wrong Code, try again</div>
		{/if}

		<label>In order to continue, please verify the image code</label>
		<input type="text" name="spam" value="{$spamtext}" />
		<img src="/captcha.php?{$time}" alt="" />
		<input type="submit" value="Submit" name="submit" />

	</div>
</div>

</form>

