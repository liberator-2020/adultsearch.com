<a href="{$loc_path}" data-ajax="false">{$loc_name}</a> &raquo; <a href="{$forum_link}/" data-ajax="false">Forum Home</a> &raquo; {$forum_title} ({$forum_posts})
<br />

{if $states}
<div>
	<div class="ui-bar ui-bar-a">
		<h3>Select a Sub Category</h3>
	</div>
	<div class="ui-body ui-body-a" style="padding: 0px; background-color: transparent;">
		{section name=forum loop=$states}
		{assign var=tt value=$states[forum]}
			{section name=forum2 loop=$tt}
			{assign var=t value=$tt[forum2]}
				<a class="ui-btn ui-btn-a" href="/forum/viewforum?forum_id={$t.forum_id}" title="" data-ajax="false">{$t.forum_name} ({$t.forum_posts}) {$t.paging} {if $t.new} <font style="color:red;font-weight:bold;font-size:9px;">*new post*</font>{/if}</a>
			{/section}
		{/section}
	</div>
</div>
{/if}

{if !$states}
	<a class="ui-btn ui-btn-a" href="./forum-new-topic?forum_id={$forum_id}" rel="nofollow" data-ajax="false">Start New Topic</a>
{/if}

<div>
	<div class="ui-bar ui-bar-a">
		<h3>Search in this forum</h3>
	</div>
	<div class="ui-body ui-body-a" style="padding: 0px; background-color: transparent;">
		<form action="/forum/forum_search" method="get" data-ajax="false">
		<div class="ui-grid-a">
			<div class="ui-block-a">
			<input type="search" name="search" value="{$search}" />
			</div>
			<div class="ui-block-b">
			<input type="submit" value="Search" />
			</div>
		</div>
	</div>
</div>


{if $topics} 
<div>
	<div class="ui-bar ui-bar-a">
		<h3>Forum Topics</h3>
	</div>
	<div class="ui-body ui-body-a" style="padding: 0px; background-color: transparent;">
		{section name=mainforum loop=$topics}
		{assign var=t value=$topics[mainforum]}
			<a class="ui-btn ui-btn-a" href="{$t.topic_url}" data-ajax="false">
				{if $t.important}Sticky: {/if}
				{if $t.moved}<b>MOVED</b>{/if}
				<b>{$t.topic_title}</b>
				{if !$t.viponly || $isvippaid || $account_id == $t.nick}
					{if $t.new} <span class="new">*new*</span>{/if}
				{/if} {$t.paging} {$t.extra} - <i>{if $t.userlink}{$t.userlink}{else}Anonymous{/if}</i>
				<br />
				<b>Replies: {$t.topic_replies}</b>	Last Reply: {assign var="temp" value="<br />"|explode:$t.last}{$temp[0]}
			</a>
		{/section}
	</div>
</div>

{elseif !$states}
	<div class="ui-body ui-body-a ui-corner-all">
		<h3>No forum topics</h3>
		<p>
			No topic found in this section.<br />
			<a class="ui-btn ui-btn-a" href="forum-new-topic?forum_id={$forum_id}" title="" rel="nofollow" data-ajax="false">Click here</a> to create the first forum topic now.
		</p>
	</div>
{/if}

{*
<div id="pager">{$paging}</div>
*}
