<div id="breadcrumb"><a href="{$core_loc_link}">{$loc_name}</a> : {$what}</div>
<div id="contentWrapper">
	{section name=f loop=$forums}
	{assign var=f value=$forums[f]}
		<div class="subCategory" style="background-color: #{cycle values="FFFFFF,DDDDDD"};">
			<a href="{$f.forum_url}" title="{$f.forum_name}"><strong>{$f.forum_name}</strong> ({$f.forum_posts})<br/>{$f.forum_desc}</a>
		</div>
	{/section}
</div>
