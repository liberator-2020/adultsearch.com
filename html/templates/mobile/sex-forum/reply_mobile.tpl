{if $forum_locked == 0 }

<form name="posting" method="post" {if !$noaction}action="{if $forum_reply_link}{$forum_reply_link}{else}forum_reply{/if}"{/if} data-ajax="false">
{if $topic_id}<input type="hidden" name="topic_id" value="{$topic_id}" />{/if}
{if $forum_id}<input type="hidden" name="forum_id" value="{$forum_id}" />{/if}

<div>
	<div class="ui-bar ui-bar-a">
		<h3>{if $new_topic}New Topic{else}Add Reply{/if}</h3>
	</div>
	<div class="ui-body ui-body-a" style="padding: 0px; background-color: transparent;">
		{if $edit_topic_title}
			<label><span>Topic Title</span></label>
			<input type="text" name="topic_title" size="30" maxlength="100" value="{$topic_title}" />
		{/if}

		<label>Write message :</label>
		<textarea name="topictext" rows="8" cols="35">{$topictext}</textarea>
		<label>
			<input type='checkbox' name='watch' value='1' {if $watch_enabled}checked='checked'{/if}/> Send me E-Mail notification when someone replies to this topic.
		</label>
		<p>
			{section name=i loop=$icons}
			{assign var=icon value=$icons[i]}
				<a href="javascript:void(0);" onClick="document.posting.topictext.value+=' {$icon.code} ';"><img src="{$icon.icon}" border="0" alt="{$icon.code}"></a>
			{/section}
		</p>
		<input type="submit" class="ui-btn ui-btn-a" value="Add" name="Submit"{if !$smarty.session.account} disabled="disabled"{/if} />
	
		{if !$smarty.session.account}To post a reply on this topic, <a href='/account/signup' title='AdultSearch sign up' rel='nofollow' data-ajax="false">register</a> free on Adultsearch.com{/if}
		{if $goback}<input type="button" onclick="history.back()" value="Back" class="ui-btn ui-btn-c ui-mini" />{/if}

	</div>
</div>

<table width="1000" border="0" cellspacing="0" cellpadding="0" class="replyform">
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2" class="normal" align="left">
				{if $mp_company_reply}
					<span class="fu-select">If you'd like to talk about a massage parlor not mentioned in this page, select here &gt;&gt;
						<select name="mp" style="width:175px;">
							<option value="">-- select --</option>
							{section name=m loop=$mp_company}
							{assign var=mp value=$mp_company[m]}
								<option value="{$mp.id_company}"{if $mp.selected} selected="selected"{/if}>{$mp.name}</option>
							{/section}
						</select>
					</span>
				{/if}
			</td>
		</tr>

		{if $mp_company}
		{if $thumbnail_filename}
		<tr>
			<td class="normal" colspan="2"><img src="/_mp_pictures/thumb/{$thumbnail_filename}" alt="" /></td>
		</tr>
		{/if}

		<tr {if $mp_company_reply}style="display:none;color: #ffffff;" id="mp_company_e"{else}style="color:#ffffff;"{/if}>
			<td class="normal"><b>{if $mp_company_reply}If you want to refer a MP not related with this topic, select it from the list {else}If you are opening this topic for specific massage parlor, please select from the list:{/if}</b></td>
			<td>
				<span id="mp_for_this_location">
					<select name="mp">
						<option value="">-- select --</option>
						{section name=m loop=$mp_company}
						{assign var=mp value=$mp_company[m]}
							<option value="{$mp.id_company}"{if $mp.selected} selected="selected"{/if}>{$mp.name}</option>
						{/section}
					</select>
					<br />
					{if $forum_parent}
						<a href="javascript:void(0);" onclick="getAjaxUpdater('mp_for_this_location', 'mp_list_loc.htm', 'forum_id={$forum_parent}'); return false;" style="color:#ffffff;">I could not find the massag	parlor in this list, show all the massage parlors for the {$forum_parent_name}.</a>{/if}
				</span>
			</td>
		</tr>
		{/if}

		<tr>
			<td width="200" class="normal" valign="top"></td>
			<td class="normal" width="816">
				{if $inputReason}
					<br />
					<b>Edit Reason<font color="#FF0000">*</font></b> <input type="text" name="editreason" value="{$editreason}" size="100" mexlength="255" />
				{/if}
				{if $error}
					<br /><font color="red"><b>{$error}</b></font><br />
				{/if}
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

</form>

{/if}
