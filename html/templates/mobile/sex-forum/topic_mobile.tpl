<a href="{$loc_path}" data-ajax="false">{$loc_name}</a> &raquo; 
<a href="{$forum_link}" data-ajax="false">Forum Home</a> &raquo; 
<a href="{$forum_link}/viewforum?forum_id={$forum_id}" title="{$forum_name}" data-ajax="false">{$forum_name}</a>

<h1>{$topic_title}</h1>
Total Posts: {$count}<br />
Topic Pages: {$pages}<br />
{if $ismember}
	Subscribe: 
	{if $watch_enabled}
		<a class="ui-btn ui-btn-c" href="forum_reply?unwatch={$topic_id}" data-ajax="false">Unsubscribe to this topic</a>
	{else}
		<a class="ui-btn ui-btn-c ui-mini ui-btn-inline" href="forum_reply?watch={$topic_id}" data-ajax="false">Subscribe to this topic</a>
	{/if}
	<br />
{/if}

{section name=p loop=$posts}
{assign var=post value=$posts[p]}
	<strong>{if $post.username}{$post.username}{else}Anonymous{/if}</strong> {if $post.username}<a class="ui-btn ui-btn-c ui-mini ui-btn-inline" href="/pm/compose?recipient={$post.account_id}" style="padding: 1px 3px 1px 3px;">Send PM</a>{/if} said:<div style="float: right">{$post.post_time}</div><br/>
	{$post.post_text}
	{if ($post.post_edited)}
		<p><i>This message is edited by {$post.post_edited} {if $post.post_edit_count>1}{$post.post_edit_count} times{/if} on {$post.edited_date} for reason: {$post.post_edit_reason}</i></p>
	{/if}
	{if !$smarty.section.p.last}<hr size="1" />{/if}
 {/section}

{$pages}

<br /><br />
{include file="mobile/sex-forum/reply_mobile.tpl"}
