{if $side}
    <div id="side_sponsor">
        {section name=si loop=$side}
            {assign var=ss value=$side[si]}
            <div class="ss">
                <a href="{$ss.link}">{$ss.title}</a><br/>
                <p>{$ss.text}</p>
                {section name=im loop=$ss.images}
                    {assign var=image_url value=$ss.images[im]}
                    <a href="{$ss.link}"><img src="{$image_url}" width="60" style="max-width: 60px; max-height: 100px;"
                                              alt="" border="0"/></a>
                {/section}
            </div>
        {/section}
    </div>
{/if}