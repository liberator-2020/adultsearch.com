<style type="text/css">
.yellow_round {
	background-color: #FFFFB9;
	border-radius: 10px;
	padding: 15px;
	font-size: 14px;
	margin: 10px 0px;
	text-align: left;
}
.title {
	font-size: 22px;
	font-family: Georgia, "Times New Roman", Times, serif;
	color: #000000;
	border-bottom: solid 1px #000000;
}
</style>

<h1 class="title">Thank you for your purchase</h1>

<div class="yellow_round" style="font-size: 1.3em;">
	<p>
	Your payment was authorized successfully, but we are not going to charge your credit card until your purchase is manually verified in the next 10 working days.
	</p>
	<p>
	If you are an escort agency with intent to spend more than $3,000 of advertising, please email us at <a href="mailto:agency@adultsearch.com" style="font-size: 1.1em;">agency@adultsearch.com</a>.
	</p>
	<p>
	In case you have any questions, please emails us at <img src="/images/email_address.png" style="width: 180px; height: 20px; vertical-align: middle;"/>.
	</p>
</div>

<script type="text/javascript">
</script>
