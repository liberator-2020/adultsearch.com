<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Membership Renewal Success</title>
</head>
<body>
<div style="display: block; width: 100% !important; max-width: 640px !important; margin: 0 auto; border: solid 1px black; border-radius: 4px;">
	<table width="100%" align="center" cellpadding="5" cellspacing="0" style="font-family: Verdana, Geneva, sans-serif; font-size: 12px;">
	<tr>
		<td colspan="2" bgcolor="white" align="center"><img src="cid:logo_email" width="57" height="57" /><br /><h1>Yeobill</h1></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<br />
			Hello, Your membership was successfully renewed, this is the receipt from <span style="font-weight: bold; color: black;">Yeobill</span> for this transaction.<br /><br />
		</td>
	</tr>
	{if $trans_id}
	<tr>
		<td align="right"><strong>Transaction ID#</strong></td>
		<td style="padding-left: 5px;">{$trans_id}</td>
	</tr>
	{/if}
	{if $card_description}
	<tr>
		<td align="right"><strong>Credit Card</strong></td>
		<td style="padding-left: 5px;">{$card_description}</td>
	</tr>
	{/if}
	<tr>
		<td align="right"><strong>Amount</strong></td>
		<td style="padding-left: 5px;">${$amount}</td>
	</tr>
	<tr>
		<td align="right" valign="top"><strong>Renewal items</strong></td>
		<td style="padding-left: 5px;">
			{foreach from=$email_items item=it}
				{$it}<br />
			{/foreach}
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding: 30px;">
			Please keep this receipt for future reference. You can cancel your recurring payment anytime.
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center" style="text-align: center;">
			<div style="width: 250px; margin: auto; text-align: center; font-size: 1.2em;">
				<br />
				For help please contact:<br /><br />
				U.S./Canada: <strong>1-702-935-1688</strong><br />
				<a href="mailto:receipts@viphost.com">receipts@viphost.com</a>
				<br />
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding: 30px;">
			Thank you for advertising with us.<br />
			Sincerely,<br />
			<a href="mailto:receipts@viphost.com">receipts@viphost.com</a><br />
		</td>
	</tr>
	</table>
</div>
</body>
</html>
