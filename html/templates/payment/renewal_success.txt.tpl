Hello,

Your membership was successfully renewed, this is the receipt from "Yeobill" for this transaction.

{if $trans_id}Transaction #Id: {$trans_id}{/if}
{if $card_decription}Credit Card: {$card_description}{/if}
Amount: ${$amount}
Renewed items: {foreach from=$email_items item=it name=cycle}{if $smarty.foreach.cycle.first}{$it}{else}; {$it}{/if}{/foreach} 

You can cancel your recurring payment anytime.

For help please contact:
U.S./Canada: 1-702-935-1688
or
receipts@viphost.com

Thank you for advertising with us.

Sincerely,
receipts@viphost.com
