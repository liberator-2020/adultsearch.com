<form method="post" action="" name="f">
{$ccform_hidden}

<div id="cl">

{if $total}<p class="title">You will be charged : <b>${$total}</b></p>{/if}

<div class="payment">
<table border="0" cellspacing="0" cellpadding="0" class="b">

{if $error}
<tr>
 <td colspan="2" class="error" align="center">
	{$error}
</td>
</tr>
{/if}

{if $register}
 <tr>
  <td align="center" valign="middle" colspan="2"><font style="font-size:18px">Account Details</font>
  </td>
 </tr>

 <tr>
  <td class="first">Your E-Mail Address:</td>
  <td class="second">
	<input type="text" name="cc_email" size="40" value="{$cc_email}" />
  </td>
 </tr>

 <tr>
  <td class="first">Create a Password:</td>
  <td class="second">
	<input type="password" name="cc_password" size="40" value="{$cc_password}" />
  </td>
 </tr>
{/if}

{if !$no_promotion}
{if $cc_redeemed}
 <tr>
  <td class="first">Promotion Code Redeemed:</td>
  <td class="second"> <b>${$cc_redeemed}</b>
	<input type="hidden" name="cc_promo" value="{$cc_promo}" />
  </td>
 </tr>
{else}
 <tr>
  <td align="center" valign="middle" colspan="2"><font style="font-size:18px">Promotion Code</font>
  </td>
 </tr>

 <tr>
  <td class="first">Promotion Code:</td>
  <td class="second">
	<input type="text" name="cc_promo" size="40" value="{$cc_promo}" /> <span class="bsubmit"><button class="bsubmit-r" name="redeem"><span>Redeem the 
code</span></button></span>
  </td>
 </tr>
{/if}
{/if}

 <tr>
  <td align="center" valign="middle" colspan="2"><font style="font-size:18px">Payment Details</font>
	<script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=adultsearch.com&amp;size=S&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=en"></script>
	<br />
  </td>
 </tr>

 <tr>
  <td class="first">First Name:</td>
  <td class="second">
	<input type="text" name="cc_firstname" size="40" value="{$cc_firstname}" maxlength="60" />
  </td>
 </tr>

 <tr>
  <td class="first">Last Name:</td>
  <td class="second">
	<input type="text" name="cc_lastname" size="40" value="{$cc_lastname}" />
  </td>
 </tr>

 <tr>
  <td class="first">Address:</td>
  <td class="second">
	<input type="text" name="cc_address" size="40" value="{$cc_address}" />
  </td>
 </tr>

{if !$payment_nocity}
 <tr>
  <td class="first">City:</td>
  <td class="second">
	<input type="text" name="cc_city" size="40" value="{$cc_city}" />
  </td>
 </tr>
{/if}

{if !$payment_nostate}
 <tr>
  <td class="first">State/Province/Region:</td>
  <td class="second">
	<input type="text" name="cc_state" size="20" value="{$cc_state}" />
  </td>
 </tr>
{/if}

 <tr>
  <td class="first">ZIP/Postal Code:</td>
  <td class="second">
	<input type="text" name="cc_zipcode" size="10" value="{$cc_zipcode}" />
  </td>
 </tr>

{if $country_options}
<tr>
	<td class="first">Country:</td>
	<td class="second"><select name="cc_country">{html_options options=$country_options selected=$cc_country}</select></td>
</tr>
{/if}

	<input type="hidden" name="captcha_str" value="{$captcha_str}" />

 <tr>
  <td colspan="2" class="submit">
	<span id="submitbuttonwait" style="display:none;">Please wait....</span>
	<span id="submitbutton">
		<span class="bsubmit"><button class="bsubmit-r" type="submit" onclick="$('#submitbutton').hide();$('#submitbuttonwait').show();"><span>Submit</span></button></span>
		<span class="bsubmit"><button class="bsubmit-r" type="button" onclick="history.back();"><span>Go Back</span></button></span>
	</span>
  </td>
</tr>

{if $payment_note}
 <tr>
  <td colspan="2" class="submit">
	{$payment_note}
  </td>
</tr>
{/if}

</table>
</div>

</div>

</form>
