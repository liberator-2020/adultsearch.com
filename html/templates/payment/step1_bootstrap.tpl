<style type="text/css">
tr td:nth-child(1) {
	vertical-align: top;
}
tr td:nth-child(2) {
	padding-left: 20px;
}
input.form-control {
	margin-bottom: 10px;
}
</style>

<form method="post" action="" name="f">
{$ccform_hidden}

<div id="cl">

{if $total}<p class="title">You will be charged : <b>${$total}</b></p>{/if}

<div class="payment">
<table border="0" cellspacing="0" cellpadding="0" class="b">

{if $error}
<tr>
 <td colspan="2" style="color: red; font-weight: bold;">
	{$error}
</td>
</tr>
{/if}

{if $register}
 <tr>
	<td align="center" valign="middle" colspan="2"><h2>Account Details</h2>
	</td>
 </tr>

 <tr>
	<td>Your E-Mail Address:</td>
	<td>
	<input class="form-control" type="text" name="cc_email" size="40" value="{$cc_email}" />
	</td>
 </tr>

 <tr>
	<td>Create a Password:</td>
	<td>
	<input class="form-control" type="password" name="cc_password" size="40" value="{$cc_password}" />
	</td>
 </tr>
{/if}

{if !$no_promotion}
{if $cc_redeemed}
 <tr>
	<td>Promotion Code Redeemed:</td>
	<td> <b>${$cc_redeemed}</b>
	<input class="form-control" type="hidden" name="cc_promo" value="{$cc_promo}" />
	</td>
 </tr>
{else}
 <tr>
	<td align="center" valign="middle" colspan="2"><h2>Promotion Code</h2>
	</td>
 </tr>

 <tr>
	<td>Promotion Code:</td>
	<td>
	<input class="form-control" type="text" name="cc_promo" size="40" value="{$cc_promo}" />
	<button class="btn btn-default" name="redeem">Redeem the code</button>
	</td>
 </tr>
{/if}
{/if}

 <tr>
	<td align="center" valign="middle" colspan="2"><h2>Payment Details</h2></td>
 </tr>

 <tr>
	<td>First Name:</td>
	<td>
	<input class="form-control" type="text" name="cc_firstname" size="40" value="{$cc_firstname}" maxlength="60" />
	</td>
 </tr>

 <tr>
	<td>Last Name:</td>
	<td>
	<input class="form-control" type="text" name="cc_lastname" size="40" value="{$cc_lastname}" />
	</td>
 </tr>

 <tr>
	<td>Address:</td>
	<td>
	<input class="form-control" type="text" name="cc_address" size="40" value="{$cc_address}" />
	</td>
 </tr>

{if !$payment_nocity}
 <tr>
	<td>City:</td>
	<td>
	<input class="form-control" type="text" name="cc_city" size="40" value="{$cc_city}" />
	</td>
 </tr>
{/if}

{if !$payment_nostate}
 <tr>
	<td>State/Province/Region:</td>
	<td>
	<input class="form-control" type="text" name="cc_state" size="20" value="{$cc_state}" />
	</td>
 </tr>
{/if}

 <tr>
	<td>ZIP/Postal Code:</td>
	<td>
	<input class="form-control" type="text" name="cc_zipcode" size="10" value="{$cc_zipcode}" />
	</td>
 </tr>

{if $country_options}
<tr>
	<td>Country:</td>
	<td><select name="cc_country" class="form-control">{html_options options=$country_options selected=$cc_country}</select></td>
</tr>
{/if}

 <tr>
	<td/>
	<td>
	<span id="submitbuttonwait" style="display:none;">Please wait....</span>
	<span id="submitbutton">
		<button class="btn btn-primary" type="submit" onclick="$('#submitbutton').hide();$('#submitbuttonwait').show();">Submit</button>
		<button class="btn btn-default" type="button" onclick="history.back();">Go Back</button>
	</span>
	</td>
</tr>

{if $payment_note}
 <tr>
	<td colspan="2" class="submit">
	{$payment_note}
	</td>
</tr>
{/if}

</table>
</div>

</div>

</form>
<br />
