<style type="text/css">
.payment_container {
	border: solid 1px #CCCCCC;
	border-radius: 10px;
	padding: 20px;
	font-size: 12px;
}
.payment_container input {
	margin-top: 10px;
}
.post_btn {
	background: #ff740e;
	background-image: -webkit-linear-gradient(top, #FF8A34, #ff740e);
	background-image: -moz-linear-gradient(top, #FF8A34, #ff740e);
	background-image: -ms-linear-gradient(top, #FF8A34, #ff740e);
	background-image: -o-linear-gradient(top, #FF8A34, #ff740e);
	background-image: linear-gradient(to bottom, #FF8A34, #ff740e);
	-webkit-border-radius: 6;
	-moz-border-radius: 6;
	border-radius: 6px;
	font-family: Arial;
	color: #ffffff !important;
	font-size: 17px;
	font-weight: bold;
	padding: 6px 10px 6px 10px;
	border: solid #186299 1px;
	text-decoration: none;
	cursor: pointer;
}
.post_btn:hover {
	text-decoration: none;
	color: black !important;
}
</style>

<form method="post" action="{$form_url}" class="payment_container" onsubmit="return validateForm();">

	<h1>Please enter your credit card information</h1>

	{if $error}
		<div style="color: red; font-weight: bold;">{$error}</div>
		<button type="button" onclick="window.history.go(-1);">Go Back</button>
	{else}

	<table>
	<tr>
		<th style="vertical-align: top;"><label>Billing address</label></th>
		<td>
			{$name}<br />
			{$address}<br />
			{$city}, {$state} {$zip}<br />
			{$country}<br />
			<a href="javascript:history.go(-1);">Change address</a>
		</td>
	</tr>
	<tr>
		<td/>
		<td>
			<div class="card-wrapper" style="margin-left: -17px;"></div>
		</td>
	</tr>
	<tr>
		<th><label>Card number</label></th>
		<td><input type="text" id="number" name="billing-cc-number" value="" class="form-control" autocomplete="cc-number" /></td>
	</tr>
	<tr>
		<th><label>Expiry date</label></th>
		<td><input type="text" id="expiry" name="billing-cc-exp" value="" class="form-control" autocomplete="cc-exp" style="width: 80px;" /></td>
	</tr>
	<tr>
		<th><label>CVV2 code</label></th>
		<td><input type="text" id="cvc" name="billing-cvv" value="" class="form-control" autocomplete="cc-csc" style="width: 80px;" /></td>
	</tr>
	<tr>
		<td />
		<td>
			<br />
			Your purchase will show up as payment to <em><strong>POST MY AD</strong></em> on your bill.<br />
			{*
			Please call us on <strong>702-472-7242</strong>in U.S. or <strong>778-819-8510</strong> in Canada for more information.<br />
			*}
			Please contact us at <a href="mailto:support@adultsearch.com">support@adultsearch.com</a>for more information<br />
			<br />

			<button type="submit" class="post_btn">Purchase now</button>
		</td>

	</tr>
	</table>

	{/if}

</form>

{* https://github.com/jessepollak/card *}
<script src="/js/jquery.card.js"></script>

{literal}
<script type="text/javascript">
function validateForm() {
	var cc = $('input#number').val().replace(/ /g, '');
	var exp = $('input#expiry').val().replace(/ /g, '').replace(/\//g, '');
	if (! /^([0-9]{13,16})$/.test(cc)) {
		alert('Please enter valid credit card number.');
		return false;
	}
	if (! /^([0-9]{4})$/.test(exp)) {
		alert('Please enter valid expiration date in format MMYY.');
		return false;
	}
	$('input#number').val(cc);
	$('input#expiry').val(exp.substring(0,2)+'/'+exp.substring(2,4));
	return true;
}
$(document).ready(function() {
	$('form').card({
		// a selector or DOM element for the container
		// where you want the card to appear
		container: '.card-wrapper', // *required*
		// all of the other options from above
		formSelectors: {
			numberInput: 'input#number', // optional — default input[name="number"]
			expiryInput: 'input#expiry', // optional — default input[name="expiry"]
			cvcInput: 'input#cvc', // optional — default input[name="cvc"]
			nameInput: 'input#name' // optional - defaults input[name="name"]
		},
		placeholders: {
			name: '{/literal}{$name}{literal}',
		},
	});
});
</script>
{/literal}
