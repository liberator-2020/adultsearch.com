<style type="text/css">
.yellow_round {
	background-color: #FFFFB9;
	border-radius: 10px;
	padding: 15px;
	font-size: 14px;
	margin: 10px 0px;
	text-align: left;
}
.title {
	font-size: 22px;
	font-family: Georgia, "Times New Roman", Times, serif;
	color: #000000;
	border-bottom: solid 1px #000000;
}
.confirmation li {
	background-color: #FFFFB9;
	border-radius: 10px 10px 10px 10px;
	float: left;
	padding: 15px;
	width: 350px;
	font-size: 14px;
	margin: 10px 25px;
	height: 150px;
	text-align: left;
}
.confirmation a {
	font-weight: bold;
	font-size: 1.1em;
}
.confirmation h2 {
	font-size: 18px;
	font-family: Georgia, "Times New Roman", Times, serif;
	color: #000000;
	border-bottom: solid 1px #000000;
}
#post_warning {
	font-size: 20px;
	height: 28px;
	text-align: center;
	font-weight: bold;
	color: #ff0000;
	text-shadow: rgba(148, 2, 2, 0.4) 1px 1px;
}
</style>

<h1 class="title">Thank you for your purchase</h1>
<div class="yellow_round">
	{if $amount}
		Your credit card charge for ${$amount} will appear on your statement as "Yeobill". You will receive an email receipt, please keep this for future reference.
	{/if}
	{if $recurring_amount}
		<br />
		Your membership is recurring (you will be charged every 30 days), you can cancel your membership payment anytime.
	{/if}
</div>

{if $ads|@count == 1 && $items|@count == 0}
{assign var=id value=$ads[0].id} 
{assign var=thumb value=$ads[0].thumb} 
<div class="confirmation">

	<div class="yellow_round" style="text-align: center;">
		<span id="post_warning">Your ad can take up to 24 hours to go live.</span>
	</div>

	<ul>
	<li>
		<h2>View Your Ad</h2>
		Thank you for posting your ad on AdultSearch.com!<br /><span style="font-size: 1.4em; font-weight: bold;">Your ad can take up to 24 hours to go live.</span><br />
		<a href="/classifieds/look?id={$id}" target="_blank">View Your Ad</a>
	</li>
	<li>
		<h2>Edit Your Ad</h2>
		<img src="/images/adbuild/conf_edit.png" width="211" height="84" style="float: right; margin-left: 5px; border-radius: 10px;"/>
		You can always login to edit your ad later. <br />
		<a href="/adbuild/step3?ad_id={$id}">Edit Now</a><br />
	</li>
	{if $thumb}
	<li>
		<h2>Edit Your Thumbnail</h2>
		 <a href="/classifieds/crop?id={$id}">
			<img src="{$config_image_server}/classifieds/{$thumb}" width="75" height="75" /><br />
			Edit This Thumbnail
		</a>
	</li>
	{/if}

	{if $ebiz_show}
	<li>
		<h2>Create your website in 1-click!</h2>
		<div id="ebiz_response">
			<p>We offer you to create website for you at our partner <a href="http://escorts.biz" target="_blank">Escorts.biz</a>. All the data you submitted to the classified ad will be automatically used in your website (description, phone number, photos, etc...). You will be automatically registered with the same email and password you have at adultsearch.com. So easy! Just click on link below.</p>
			<a href="#" onclick="ebizCreateWebsite({$id}); return false;">Create my website at Escorts.biz</a>
		</div>
	</li>
	{/if}

	<li>
		<h2>Share Your Ad </h2>
		Post your ad right now to your social media.
		<br /><br />
		<a href="http://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fadultsearch.com%2Fclassifieds%2Flook?id={$id}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/images/classifieds/facebook_32.png" alt="My AdultSearch on Facebook" width="32" height="32" /></a>
		<a href="https://twitter.com/share?url=http%3A%2F%2Fadultsearch.com%2Fclassifieds%2Flook?id={$id}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="custom-tweet-button"> <img src="/images/classifieds/twitter_32.png" alt="My AdultSearch on Twitter" width="32" height="32" /></a>
		<a href="https://plus.google.com/share?url=http://adultsearch.com/classifieds/look?id={$id}" onclick="javascript:window.open(this.href,	'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/images/classifieds/gplus_32.png" alt="Share on Google+" /></a>
		<a href="mailto:?subject=My AdultSearch Post&amp;body=Check out my ad on AdultSearch http://adultsearch.com/classifieds/look?id={$id}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" title="Share by Email"><img src="/images/classifieds/email_32.png" alt="My AdultSearch on Email" width="32" height="32" /></a>
	</li>

	</ul>
</div>
<br style="clear: both;"/>
{else if $ads|@count > 1}
	<div class="yellow_round" style="text-align: center;">
		<span id="post_warning">Your ads can take up to 24 hours to go live.</span>
	</div>
	{foreach from=$ads item=ad}
		<div class="yellow_round">
			<img src="{$config_image_server}/classifieds/{$ad.thumb}" width="75" height="75" />
			<a href="/classifieds/look?id={$ad.id}" target="_blank">View This Ad</a><br />
			<a href="/adbuild/step3?ad_id={$ad.id}">Edit This Ad</a><br />
			<a href="/classifieds/crop?id={$ad.id}" class="crop">Edit This Thumbnail</a><br />
			<strong>Share Your Ad</strong>: Post your ad right now to your social media.
			<a href="http://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fadultsearch.com%2Fclassifieds%2Flook?id={$ad.id}" onclick="javascript:window.open(this.href, '','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/images/classifieds/facebook_32.png" alt="My AdultSearch on Facebook" width="32" height="32" /></a>
			<a href="https://twitter.com/share?url=http%3A%2F%2Fadultsearch.com%2Fclassifieds%2Flook?id={$ad.id}" onclick="javascript:window.open(this.href, '','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="custom-tweet-button"> <img src="/images/classifieds/twitter_32.png" alt="My AdultSearch on Twitter" width="32" height="32" /></a>
			<a href="https://plus.google.com/share?url=http://adultsearch.com/classifieds/look?id={$ad.id}" onclick="javascript:window.open(this.href,  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="/images/classifieds/gplus_32.png" alt="Share on Google+"	/></a>
			<a href="mailto:?subject=My AdultSearch Post&amp;body=Check out my ad on AdultSearch http://adultsearch.com/classifieds/look?id={$ad.id}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" title="Share by Email"><img src="/images/classifieds/email_32.png" alt="My AdultSearch on Email" width="32" height="32" /></a>
		</div>
	{/foreach}
{/if}

{foreach from=$items item=item}
	<div class="yellow_round">
		Your purchase item of type {$item.type}.
	</div>
{/foreach}

<div class="yellow_round">
	<h2>For help please contact:</h2>
	U.S. &amp; Canada:<br />
	<strong>1-702-935-1688</strong> (9am to 5pm EST Mon - Fri)<br />
	<img src="/images/email_address.png" style="width: 180px; height: 20px;"/>
</div>

<script type="text/javascript">
function ebizCreateWebsite(ad_id) {
	_ajax('GET', '/_ajax/ebiz_create', 'ad_id='+ad_id, 'ebiz_response');	
}
var blinked = 0;
var timer = null;
function blinker() {
	$('#post_warning').fadeOut(100);
	$('#post_warning').fadeIn(100);
	blinked++;
	if (blinked > 4) {
		window.clearInterval(timer);
	}
}
timer = setInterval(blinker, 300);
</script>
