<style type="text/css">
.fileinput-button {
	position: relative;
	overflow: hidden;
	display: inline-block;
}
.fileinput-button input {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	opacity: 0;
	-ms-filter: 'alpha(opacity=0)';
	font-size: 200px;
	direction: ltr;
	cursor: pointer;
}
.btn-danger {
	color: #fff;
	background-color: #e32424;
}
.progress {
	height: 20px;
	margin-bottom: 20px;
	overflow: hidden;
	background-color: #f5f5f5;
	border-radius: 4px;
	-webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
	box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
}
.progress-bar {
	float: left;
	width: 0;
	height: 100%;
	font-size: 12px;
	line-height: 20px;
	color: #fff;
	text-align: center;
	background-color: #337ab7;
	-webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
	box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
	-webkit-transition: width .6s ease;
	-o-transition: width .6s ease;
	transition: width .6s ease;
}
.progress-bar-success {
	background-color: #5cb85c !important;
}
</style>
<h1>Video Verification</h1>

<p>
	To prevent Human Trafficking, we need to verify you are the real person in your ad.<br />
	Please upload video with your face clearly visible where you say <strong><em>"Adultsearch verification. I'm over 21 years old and i'm not being sex-trafficked"</em></strong>.<br />
	{*You will also need to upload a photo of the front & back of your ID.<br />*}
	The video {*& ID *}will not be visible on your profile or stored on the internet.
</p>

<form action="" method="post">

<strong>Verification Video:</strong><br />

<div id="video_preview_div" style="display: none; max-width: 300px;">
</div>

<div id="video_upload_div">
	<p style="font-size: 0.9em; margin-bottom: 2px;">
		Max. video length is 20 sec or 10MB. This only uploads video files.<br />
	</p>
	<span class="btn btn-success fileinput-button">
		<i class="glyphicon glyphicon-plus"></i>
		<span>Upload Video</span>
		<input type="file" name="video" id="video" accept="video/*" />
	</span>
	<br /><br />
	<div id="progress_video" class="progress" style="display: none;"><div class="progress-bar progress-bar-success"></div></div>
</div>
<div id="video_processing_div" style="display: none;">
	<img src="/images/processing_50.gif" />
	<div>
		Your video is being processed on our server. It might take up to 1 minute.<br />
		If you want to be sure the video was received correctly, please wait until video shows up.<br />
		Or you can just click "Next" button.
	</div>
	<br style="clear: both;" />
</div>

<button type="submit" name="submit" id="btn_next" disabled="disabled" class="btn">Next</button>

<input type="hidden" name="payment_id" value="{$payment_id}" />
</form>

{literal}
<script type="text/javascript">
function verification_processing_check(verification_id) {
	console.log('verification_processing_check, verification_id='+verification_id);
	$.post(
		'/payment/verification-check',
		{ verification_id : verification_id },
		function(data) {
			if (data.status == '0') {
				console.log('vpc: still processing');
				setTimeout(function() {verification_processing_check(verification_id);}, 1000);
			} else if (data.status == '1') {
				console.log('vpc: error');
			} else if (data.status == '2') {
				console.log('vpc: success, videopath = \''+data.path+'\' , current src='+$('#video_preview_div video source').attr('src'));
//			  $('#video_preview_div > div').remove();
				$('#video_preview_div').prepend('<div class="flowplayer is-splash" style="background-color:#777; background-image:url(\'' + data.thumb_path + '\');"><video controls><source src="'+data.path+'" type="video/mp4"/></video></div><br /><button type="button" class="btn btn-danger btn-sm delete_video_btn" style="margin-bottom: 5px;" data-verification-id="' + verification_id + '">Delete video</button>');
				$('#video_processing_div').hide();
				$('#video_preview_div').show();
				$('.flowplayer').flowplayer();
				$('.delete_video_btn').each(function(ind,obj) {
					$(obj).click(function() {
						delete_video(obj);
					});
				});
			}
		},
		'json'
	)
	.fail(function() {
		alert('Video processing failed. Administrator will get back to you shortly.');
	});
}

function delete_video(elem) {
	var verification_id = $(elem).data('verification-id');
	console.log('delete_video(): verification_id='+verification_id);
	$.post(
		'/payment/verification-delete',
		{ verification_id : verification_id },
		function(data) {
			if (data == 'OK') {
				$('#video_preview_div').html('').hide();
				$('#progress_video').hide();
				$('#video_upload_div').show();
				$('#btn_next').prop('disabled', true).removeClass('btn-success');
			} else {
				alert('Deleting verification video failed. Please contact us.');
			}
		},
		'text'
	)
	.fail(function() {
		alert('Deleting verification video failed. Please contact us.');
	});
}

function file_upload_error(jq_elem, error_text) {
	jq_elem.parent().remove();
	alert(error_text);
}

$(document).ready(function() {
	$('.delete_video_btn').each(function(ind,obj) {
		$(obj).click(function() {
			delete_video(obj);
		});
	});

	$('#video').fileupload({
		url: '/payment/verification-upload',
		dataType: 'json',
		autoUpload: true,
		acceptFileTypes: /(\.|\/)(mp4|avi|wmv)$/i,
		maxFileSize: 10999000,
		add: function(e, data) {
			var uploadErrors = [];
			var acceptFileTypes = /(\.|\/)(mp4|avi|wmv|quicktime)$/i;
			if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
				uploadErrors.push('Not an accepted file type: \''+data.originalFiles[0]['type']+'\'');
			}
			if(data.originalFiles[0]['size'] && data.originalFiles[0]['size'] > 10999000) {
				uploadErrors.push('Video filesize is too big');
			}
			if(uploadErrors.length > 0) {
				alert(uploadErrors.join("\n"));
			} else {
				data.submit();
			}
		}
	}).on('fileuploadprogressall', function (e, data) {
		var progress = parseInt(data.loaded / data.total * 100, 10);
		$('#progress_video').show();
		$('#progress_video .progress-bar').css(
			'width',
			progress + '%'
		);
	}).on('fileuploaddone', function (e, data) {
		$.each(data.result.files, function (index, file) {
			if (file.error) {
				file_upload_error($(data.context.children()[index]), file.error);
			} else {
				console.log(file);
				$('#video_processing_div').show();
				$('#video_upload_div').hide();
				$('#btn_next').prop('disabled', false).addClass('btn-success');
				setTimeout(function() {verification_processing_check(file.verification_id);}, 1000);
			}
		});
	}).on('fileuploadfail', function (e, data) {
		console.log('fileuploadfail');
		$.each(data.files, function (index) {
			var error_text = data.jqXHR.responseText;
			if (error_text == 'You must be logged in!') {
				location.href = '/login';
			} else {
				if (!error_text)
					error_text = 'File upload failed!';
				file_upload_error($(data.context.children()[index]), error_text);
			}
		});
	}).prop('disabled', !$.support.fileInput)
		.parent().addClass($.support.fileInput ? undefined : 'disabled');

});
</script>
{/literal}
