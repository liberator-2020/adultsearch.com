<form method="post" action="" name="f">
{$ccform_hidden}

<div id="cl">

{if $total}<p class="title">You will be charged : <b>${$total}</b></p>{/if}

<div class="payment">
<table border="0" cellspacing="0" cellpadding="0" class="b">

{if $error}
<tr>
 <td colspan="2" class="error" align="center">
	{$error}
</td>
</tr>
{/if}

{if $register}
 <tr>
  <td align="center" valign="middle" colspan="2"><font style="font-size:18px">Account Details</font>
  </td>
 </tr>

 <tr>
  <td class="first">Your E-Mail Address:</td>
  <td class="second">
	<input type="text" name="cc_email" size="40" value="{$cc_email}" />
  </td>
 </tr>

 <tr>
  <td class="first">Create a Password:</td>
  <td class="second">
	<input type="password" name="cc_password" size="40" value="{$cc_password}" />
  </td>
 </tr>
{/if}

{if $scc}
 <tr>
  <td class="first">My Credit Cards:</td>
  <td class="second">
	<select name="cc_id">
	<option value="0">-- select --</option>
	{section name=s loop=$scc}{assign var=c value=$scc[s]}
		<option value="{$c.cc_id}">{$c.cc} ({$c.expmonth}/{$c.expyear})</option>
	{/section}
	</select>
	<span class="bsubmit"><button class="bsubmit-r" type="submit"><span>Charge my credit card on file</span></button></span>
  </td>
 </tr>

 <tr>
  <td class="first">Or add a new credit card</td>
   <td class="second">&nbsp;</td>
 </tr>
{/if}

{if !$no_promotion}
{if $cc_redeemed}
 <tr>
  <td class="first">Promotion Code Redeemed:</td>
  <td class="second"> <b>${$cc_redeemed}</b>
	<input type="hidden" name="cc_promo" value="{$cc_promo}" />
  </td>
 </tr>
{else}
 <tr>
  <td align="center" valign="middle" colspan="2"><font style="font-size:18px">Promotion Code</font>
  </td>
 </tr>

 <tr>
  <td class="first">Promotion Code:</td>
  <td class="second">
	<input type="text" name="cc_promo" size="40" value="{$cc_promo}" /> <span class="bsubmit"><button class="bsubmit-r" name="redeem"><span>Redeem the 
code</span></button></span>
  </td>
 </tr>
{/if}
{/if}

 <tr>
  <td align="center" valign="middle" colspan="2"><font style="font-size:18px">Payment Details</font>
<script type="text/javascript" 
src="https://seal.verisign.com/getseal?host_name=adultsearch.com&amp;size=S&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=en"></script><br />
  </td>
 </tr>

 <tr>
  <td class="first">First Name:</td>
  <td class="second">
	<input type="text" name="cc_firstname" size="40" value="{$cc_firstname}" maxlength="60" />
  </td>
 </tr>

 <tr>
  <td class="first">Last Name:</td>
  <td class="second">
	<input type="text" name="cc_lastname" size="40" value="{$cc_lastname}" />
  </td>
 </tr>

 <tr>
  <td class="first">Address:</td>
  <td class="second">
	<input type="text" name="cc_address" size="40" value="{$cc_address}" />
  </td>
 </tr>

{if !$payment_nocity}
 <tr>
  <td class="first">City:</td>
  <td class="second">
	<input type="text" name="cc_city" size="40" value="{$cc_city}" />
  </td>
 </tr>
{/if}

{if !$payment_nostate}
 <tr>
  <td class="first">State/Province/Region:</td>
  <td class="second">
	<input type="text" name="cc_state" size="20" value="{$cc_state}" />
  </td>
 </tr>
{/if}

 <tr>
  <td class="first">ZIP/Postal Code:</td>
  <td class="second">
	<input type="text" name="cc_zipcode" size="10" value="{$cc_zipcode}" />
  </td>
 </tr>


 <tr>
  <td class="first">Credit Card Number:</td>
  <td class="second">
	<input type="text" name="cc_cc" size="20" value="{$cc_cc}" /> <img class="img1" src="/images/classifieds/Credit_card_logos_small.png" alt="" />
  </td>
 </tr>

 <tr>
  <td class="first">Expiration Date:</td>
  <td class="second">
	  <select name="cc_month">
<option value="">-- Select --</option>
<option value="1"{if $cc_month==1} selected="selected"{/if}>January (01)</option>
<option value="2"{if $cc_month==2} selected="selected"{/if}>February (02)</option>
<option value="3"{if $cc_month==3} selected="selected"{/if}>March (03)</option>
<option value="4"{if $cc_month==4} selected="selected"{/if}>April (04)</option>
<option value="5"{if $cc_month==5} selected="selected"{/if}>May (05)</option>
<option value="6"{if $cc_month==6} selected="selected"{/if}>June (06)</option>
<option value="7"{if $cc_month==7} selected="selected"{/if}>July (07)</option>
<option value="8"{if $cc_month==8} selected="selected"{/if}>August (08)</option>
<option value="9"{if $cc_month==9} selected="selected"{/if}>September (09)</option>
<option value="10"{if $cc_month==10} selected="selected"{/if}>October (10)</option>
<option value="11"{if $cc_month==11} selected="selected"{/if}>November (11)</option>
<option value="12"{if $cc_month==12} selected="selected"{/if}>December (12)</option>
</select>

  <select name="cc_year"><option value="">-- Select --</option>
	{php}for($i=date("Y");$i<date("Y")+11;$i++){ $ii = substr($i, -2); $selected = isset($_POST["cc_year"]) && $_POST["cc_year"] == $ii ? ' selected="selected"': ''; 
echo "<option value=\"$ii\"$selected>$i</option>";}{/php}</select>
  </td>
 </tr>

 <tr>
  <td class="first">Security Code (CVC2):</td>
  <td class="second">
	<input type="text" name="cc_cvc2" value="{$cc_cvc2}" size="5" />
  </td>
 </tr>

{if !$captcha_ok}
 <tr>
  <td class="first">Security Control: <br/>
  </td>
  <td class="second">
        <input type="text" name="captcha_str" value="" autocomplete="off" />
        <span class="smallr">Type the text on the image into the above box.<br/>
        <img src="/captcha.php?{$time}" alt="" /></span>
  </td>
 </tr>
{else}
   <input type="hidden" name="captcha_str" value="{$captcha_str}" />
 {/if}

 <tr>
  <td colspan="2" class="submit">
	<span id="submitbuttonwait" style="display:none;">Please wait....</span>
	<span id="submitbutton">
		<span class="bsubmit"><button class="bsubmit-r" type="submit" 
onclick="$('#submitbutton').hide();$('#submitbuttonwait').show();"><span>Submit</span></button></span>

		<span class="bsubmit"><button class="bsubmit-r" type="button" 
onclick="history.back();"><span>Go Back</span></button></span>
	</span>
  </td>
</tr>

{if $payment_note}
 <tr>
  <td colspan="2" class="submit">
	{$payment_note}
  </td>
</tr>
{/if}

</table>
</div>

</div>

</form>
