<style type="text/css">
.infoblock select:disabled, .infoblock input:disabled, .infoblock textarea:disabled {
	background: #eee;
}
#card_photos_row label {
	display: block;
	width: 150px;
}
</style>

<form method="post" action="" name="f">
<input type="hidden" name="submitted" value="1" />
<input type="hidden" name="saved_card_id" id="saved_card_id" value="" />
<input type="hidden" name="image_front_data" id="image_front_data" value="" />
<input type="hidden" name="image_back_data" id="image_back_data" value="" />
{$ccform_hidden}

<div id="cl">

{if $total}<p class="title">You will be charged : <b>${$total}</b></p>{/if}

<div class="payment">
<table border="0" cellspacing="0" cellpadding="0" class="b">

{if $error}
<tr>
 <td colspan="2" class="error" align="center">
	{$error}
</td>
</tr>
{/if}

{if !$no_promotion}
{if $cc_redeemed}
<tr>
	<td class="first">Promotion Code Redeemed:</td>
	<td class="second"> <b>${$cc_redeemed}</b>
	<input type="hidden" name="cc_promo" value="{$cc_promo}" />
	</td>
</tr>
{else}
<tr>
	<td align="center" valign="middle" colspan="2"><font style="font-size:18px">Promotion Code</font>
	</td>
</tr>

<tr>
	<td class="first">Promotion Code:</td>
	<td class="second">
	<input type="text" name="cc_promo" size="40" value="{$cc_promo}" /> <span class="bsubmit"><button class="bsubmit-r" name="redeem"><span>Redeem the 
code</span></button></span>
	</td>
</tr>
{/if}
{/if}

<tr>
	<td align="center" valign="middle" colspan="2">
		{if $recurring_amount}
			<div id="recurring_confirm">
				<label>&nbsp;<br />&nbsp;</label>
				<div>
					This is recurring charge. <strong>You can cancel the subscription anytime</strong>.<br />
					<input type="checkbox" name="recurring_confirm" value="1" {if $recurring_confirm}checked="checked"{/if}/> I confirm this recurring charge.
				</div>
			</div>
			<br />
		{/if}
	</td>
</tr>

<tr>
	<td align="center" valign="middle" colspan="2">
		<span style="font-size:18px">Payment Details</span>
	</td>
</tr>

{if $budget_available || $account_level == 3}
<tr>
	<td class="first"></td>
	<td class="second">
		<h3>Pay {if $recurring_amount}${$recurring_amount}{else}${$total}{/if} with my advertising balance</h3>
		Currently you have ${$budget} in your advertising account balance, so you can pay this purchase by substracting that amount from your advertising balance.<br />
		<button type="submit" name="pay_budget" value="1" class="btn btn-success"/>Pay with advertising budget</button>
		<br />
		Or you can pay with credit card below:
		<br />
	</td>
</tr>
{/if}

{if $cards}
<tr>
	<td class="first">Credit card:</td>
	<td class="second">
	<select id="card" class="form-control" style="width: 295px;">
		<option value="">- Choose remembered card -</option>
		{foreach from=$cards item=card}
		<option value="{$card.id}" data-firstname="{$card.firstname}" data-lastname="{$card.lastname}" data-address="{$card.address}" data-city="{$card.city}" data-state="{$card.state}" data-zipcode="{$card.zipcode}" data-country="{$card.country}" data-month="{$card.month}" data-year="{$card.year}" >{$card.label}</option>
		{/foreach}
	</select>
	<br />
	Or enter new credit card below:
	</td>
</tr>
{/if}

<tr>
	<td class="first">First Name:</td>
	<td class="second">
	<input type="text" name="cc_firstname" size="40" value="{$cc_firstname}" maxlength="60" />
	</td>
</tr>

<tr>
	<td class="first">Last Name:</td>
	<td class="second">
	<input type="text" name="cc_lastname" size="40" value="{$cc_lastname}" />
	</td>
</tr>

<tr>
	<td class="first">Address:</td>
	<td class="second">
	<input type="text" name="cc_address" size="40" value="{$cc_address}" />
	</td>
</tr>

{if !$payment_nocity}
<tr>
	<td class="first">City:</td>
	<td class="second">
	<input type="text" name="cc_city" size="40" value="{$cc_city}" />
	</td>
</tr>
{/if}

{if !$payment_nostate}
<tr>
	<td class="first">State/Province/Region:</td>
	<td class="second">
	<input type="text" name="cc_state" size="20" value="{$cc_state}" />
	</td>
</tr>
{/if}

<tr>
	<td class="first">ZIP/Postal Code:</td>
	<td class="second">
	<input type="text" name="cc_zipcode" size="10" value="{$cc_zipcode}" />
	</td>
</tr>

<tr>
	<td class="first">Country:</td>
	<td class="second"><select name="cc_country">{html_options options=$country_options selected=$cc_country}</select></td>
</tr>

<tr>
	<td class="first">Credit Card Number:</td>
	<td class="second">
		<input type="text" name="cc_cc" size="20" value="{$cc_cc}" />
{*
		<img class="img1" src="/images/classifieds/Credit_card_logos_small.png" alt="" />
*}
		<img width="40" alt="Mastercard" src="{$config_site_url}/images/i_mastercard.png" style="vertical-align: middle;">
		<img width="40" alt="Visa" src="{$config_site_url}/images/i_visa.png" style="vertical-align: middle;">
	</td>
</tr>
<tr>
	<td class="first"></td>
	<td class="second">
		<p style="border-radius: 6px; border: 1px solid #666; padding: 4px; margin-top: 0px; margin-bottom: 8px;">
			We do NOT accept Vanilla Prepaid cards.
		</p>
	</td>
</tr>

<tr>
	<td class="first">Expiration Date:</td>
	<td class="second">
	  <select name="cc_month">
<option value="">-- Select --</option>
<option value="1"{if $cc_month==1} selected="selected"{/if}>January (01)</option>
<option value="2"{if $cc_month==2} selected="selected"{/if}>February (02)</option>
<option value="3"{if $cc_month==3} selected="selected"{/if}>March (03)</option>
<option value="4"{if $cc_month==4} selected="selected"{/if}>April (04)</option>
<option value="5"{if $cc_month==5} selected="selected"{/if}>May (05)</option>
<option value="6"{if $cc_month==6} selected="selected"{/if}>June (06)</option>
<option value="7"{if $cc_month==7} selected="selected"{/if}>July (07)</option>
<option value="8"{if $cc_month==8} selected="selected"{/if}>August (08)</option>
<option value="9"{if $cc_month==9} selected="selected"{/if}>September (09)</option>
<option value="10"{if $cc_month==10} selected="selected"{/if}>October (10)</option>
<option value="11"{if $cc_month==11} selected="selected"{/if}>November (11)</option>
<option value="12"{if $cc_month==12} selected="selected"{/if}>December (12)</option>
</select>

  <select name="cc_year"><option value="">-- Select --</option>
	{php}for($i=date("Y");$i<date("Y")+11;$i++){ $ii = substr($i, -2); $selected = isset($_POST["cc_year"]) && $_POST["cc_year"] == $ii ? ' selected="selected"': ''; 
echo "<option value=\"$ii\"$selected>$i</option>";}{/php}</select>
	</td>
</tr>

<tr>
	<td class="first">Security Code (CVC2):</td>
	<td class="second">
	<input type="text" name="cc_cvc2" value="{$cc_cvc2}" size="5" />
	</td>
</tr>

<tr id="card_photos_row">
	<td class="first">Credit card photos:</td>
	<td class="second">
		<p style="border-radius: 6px; border: 1px solid #333; padding: 4px; margin-top: 10px; margin-bottom: 10px;">
			To prove that you do have the credit card in posession, please take photo of the front and back of your card.<br />
			We will discard these photos once your credit card is approved.
		</p>
		<label id="image_front_btn_label" for="image_front_btn" class="yellowlink"{if $image_front_thumb_data} style="display:none;"{/if}>
			Upload front photo
			<input type="file" id="image_front_btn" accept="image/*;capture=camera" style="display: none;" onchange="handle_files(this)">
		</label>
		<div id="image_front_set" {if !$image_front_thumb_data}style="display: none;"{/if}>
			Credit card front photo:<br />
			<img src="{if $image_front_thumb_data}{$image_front_thumb_data}{/if}" style="max-width: 150px; max-height: 100px;"/>
			<button type="button" id="image_front_set_delete" class="btn btn-xs btn-warning">Delete</button>
		</div>
		<br />
		<label id="image_back_btn_label" for="image_back_btn" class="yellowlink"{if $image_back_thumb_data} style="display:none;"{/if}>
			Upload back photo
			<input type="file" id="image_back_btn" accept="image/*;capture=camera" style="display: none;" onchange="handle_files(this)">
		</label>
		<div id="image_back_set" {if !$image_back_thumb_data}style="display: none;"{/if}>
			Credit card back photo:<br />
			<img src="{if $image_back_thumb_data}{$image_back_thumb_data}{/if}" style="max-width: 150px; max-height: 100px;"/>
			<button type="button" id="image_back_set_delete" class="btn btn-xs btn-warning">Delete</button>
		</div>
		<br style="clear: both;" />
		<br style="clear: both;" />
	</td>
</tr>

{if !$captcha_ok && !$isadmin}
<tr>
	<td class="first">Security Control: <br/>
	</td>
	<td class="second">
		<input type="text" name="captcha_str" value="" autocomplete="off" />
		<span class="smallr">Type the text on the image into the above box.<br/>
		<img src="/captcha.php?{$time}" alt="" /></span>
	</td>
</tr>
{else}
   <input type="hidden" name="captcha_str" value="{$captcha_str}" />
{/if}

<tr>
	<td colspan="2" class="submit">
		<span id="submitbuttonwait" style="display:none;">Please wait....</span>
		<input type="submit" name="submit" value="Submit" class="btn btn-success" onclick="$('#submitbutton').hide();$('#submitbuttonwait').show();"/>
		<a href="#" onclick="history.back();" class="btn btn-default">Cancel</a>
	</td>
</tr>

{if $payment_note}
<tr>
	<td colspan="2" class="submit">
	{$payment_note}
	</td>
</tr>
{/if}

</table>
</div>

</div>

</form>

{literal}
<script type="text/javascript">
function card_changed() {
	var option = $('#card').find(':selected');
	var card_id = $('#card').val();
	console.log('d',card_id,option);
	//console.log('card_id="'+card_id+'"');
	if (!card_id) {
		$('#saved_card_id').val('')
		$('#firstname').val('').prop('disabled', false);
		$('#lastname').val('').prop('disabled', false);
		$('#address').val('').prop('disabled', false);
		$('#city').val('').prop('disabled', false);
		$('#zipcode').val('').prop('disabled', false);
		$('#state').val('').prop('disabled', false);
		$('#country').val('').prop('disabled', false).trigger('change');
		$('#month').val('').prop('disabled', false);
		$('#year').val('').prop('disabled', false);
		$('#card_photos_row').show();
		return;
	}
	//console.log('card:',card);
	$('#saved_card_id').val(card_id);
	$('#firstname').val($(option).data('firstname')).prop('disabled', true);
	$('#lastname').val($(option).data('lastname')).prop('disabled', true);
	$('#address').val($(option).data('address')).prop('disabled', true);
	$('#city').val($(option).data('city')).prop('disabled', true);
	$('#zipcode').val($(option).data('zipcode')).prop('disabled', true);
	$('#state').val($(option).data('state')).prop('disabled', true);
	$('#country').val($(option).data('country')).prop('disabled', true);
	$('#month').val($(option).data('month')).prop('disabled', true);
	$('#year').val($(option).data('year')).prop('disabled', true);
	$('#card_photos_row').hide();
}

function handle_files(obj) {
	console.log('handle_files, obj=',obj);
	var filesToUpload = obj.files;
	var file = filesToUpload[0];

	var reader = new FileReader();  
	reader.onload = function(e) {
		var img = document.createElement("img");
		img.src = e.target.result;
		img.addEventListener('load', function () {
			var canvas = document.createElement("canvas");
			var ctx = canvas.getContext("2d");
			ctx.drawImage(img, 0, 0);

			var MAX_WIDTH = 800;
			var MAX_HEIGHT = 600;
			var width = img.width;
			var height = img.height;
			//console.log('width='+width+', height='+height);

			if (width > height) {
				if (width > MAX_WIDTH) {
				height *= MAX_WIDTH / width;
				width = MAX_WIDTH;
				}
			} else {
				MAX_WIDTH = 600;
				MAX_HEIGHT = 800;
				if (height > MAX_HEIGHT) {
				width *= MAX_HEIGHT / height;
				height = MAX_HEIGHT;
				}
			}
			canvas.width = width;
			canvas.height = height;
			var ctx = canvas.getContext("2d");
			ctx.drawImage(img, 0, 0, width, height);
			//console.log('width='+width+', height='+height);

			var dataurl_jpg = canvas.toDataURL("image/jpeg");
			//console.log('dataurl_jpg size = '+dataurl_jpg.length);

			console.log(obj);
			if ($(obj).attr('id') == 'image_front_btn') {
				console.log('front');
				$('#image_front_data').val(dataurl_jpg);
				$('#image_front_set img').attr('src', dataurl_jpg);
				$('#image_front_btn_label').hide();
				$('#image_front_set').show();
			} else if ($(obj).attr('id') == 'image_back_btn') {
				console.log('back');
				$('#image_back_data').val(dataurl_jpg);
				$('#image_back_set img').attr('src', dataurl_jpg);
				$('#image_back_btn_label').hide();
				$('#image_back_set').show();
			}
			$(obj).val('');
		});
	}
	reader.readAsDataURL(file);
	return false;
}
$(document).ready(function() {
	$('#card').change(function() {
		card_changed();
	});
	$('#image_front_set_delete').click(function() {
		$('#image_front_data').val('');
		$('#image_front_set img').attr('src', '');
		$('#image_front_set').hide();
		$('#image_front_btn_label').show();
	});
	$('#image_back_set_delete').click(function() {
		$('#image_back_data').val('');
		$('#image_back_set img').attr('src', '');
		$('#image_back_set').hide();
		$('#image_back_btn_label').show();
	});
});
</script>
{/literal}
