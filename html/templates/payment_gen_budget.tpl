<style type="text/css">
.infoblock select:disabled, .infoblock input:disabled, .infoblock textarea:disabled {
	background: #eee;
}
</style>

<form method="post" action="" name="f">
<input type="hidden" name="submitted" value="1" />
{$ccform_hidden}

<div id="cl">

{if $total}<p class="title">You will be charged : <b>${$total}</b></p>{/if}

<div class="payment">
<table border="0" cellspacing="0" cellpadding="0" class="b">

{if $error}
<tr>
<td colspan="2" class="error" align="center">
	{$error}
</td>
</tr>
{/if}

<tr>
	<td align="center" valign="middle" colspan="2">
		{if $recurring_amount}
			<div id="recurring_confirm">
				<label>&nbsp;<br />&nbsp;</label>
				<div>
					This is recurring charge. <strong>You can cancel the subscription anytime</strong>.<br />
					<input type="checkbox" name="recurring_confirm" value="1" {if $recurring_confirm}checked="checked"{/if}/> I confirm this recurring charge.
				</div>
			</div>
			<br />
		{/if}
	</td>
</tr>

<tr>
	<td align="center" valign="middle" colspan="2">
		<span style="font-size:18px">Payment Details</span>
	</td>
</tr>

<tr>
	<td class="first"></td>
	<td class="second">
		<h3>Pay {if $recurring_amount}${$recurring_amount}{else}${$total}{/if} with my advertising balance</h3>
		{if $budget_available}
			Currently you have ${$budget} in your advertising account balance, so you can pay this purchase by substracting that amount from your advertising balance.<br />
			<br />
			<button type="submit" name="pay_budget" value="1" class="btn btn-success"/>Pay with advertising budget</button>
			<br />
		{else}
			Currently you have ${$budget} in your advertising account balance.<br />
			<br />
			We are sorry but you don't have enough balance in your budget to pay for this purchase, please contact <a href="mailto:agency@adultsearch.com">agency@adultsearch.com</a> to find out how to recharge your advertising balance.<br />
		{/if}
	</td>
</tr>

</table>
</div>

</div>

</form>

{literal}
<script type="text/javascript">
$(document).ready(function() {
});
</script>
{/literal}
