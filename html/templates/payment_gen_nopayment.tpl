<form method="post" action="" name="f">
<input type="hidden" name="submitted" value="1" />
{$ccform_hidden}

<div id="cl">

{if $total}<p class="title">You will be charged : <b>${$total}</b></p>{/if}

<div class="payment">
<table border="0" cellspacing="0" cellpadding="0" class="b">

{if $error}
<tr>
<td colspan="2" class="error" align="center">
	{$error}
</td>
</tr>
{/if}

<tr>
	<td align="center" valign="middle" colspan="2">
		{if $recurring_amount}
			<div id="recurring_confirm">
				<label>&nbsp;<br />&nbsp;</label>
				<div>
					This is recurring charge. <strong>You can cancel the subscription anytime</strong>.<br />
					<input type="checkbox" name="recurring_confirm" value="1" {if $recurring_confirm}checked="checked"{/if}/> I confirm this recurring charge.
				</div>
			</div>
			<br />
		{/if}
	</td>
</tr>

<tr>
	<td align="center" valign="middle" colspan="2">
		<span style="font-size:18px">Payment Details</span>
	</td>
</tr>

<tr>
	<td class="first"></td>
	<td class="second" style="font-size: 1.2em;">
		<br />
		We are sorry but currently you can only pay with credit card if you are verified advertiser.<br />
		<br />
		{if $mobile}
		Please select the Live Chat box on the bottom right of the screen to verify your account immediately with one of our customer service operators.
		{else}
		Please contact one of our customers service agents via chat here to be verified immediately:
		{/if}
{*
		You can contact <a href="mailto:support@adultsearch.com">support@adultsearch.com</a> to become verified advertiser.
*}
	</td>
</tr>

</table>
</div>

</div>

</form>
{literal}
<script type="text/javascript">
function jo() {
	//console.log(jivo_api);
	jivo_api.open();
} 
$(document).ready(function() {
	if (typeof jivo_api !== 'undefined')
		setTimeout(jo, 1000);
});
</script>
{/literal}
