<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://checkout.stripe.com/checkout.js"></script>
<style type="text/css">
    .infoblock select:disabled, .infoblock input:disabled, .infoblock textarea:disabled {
        background: #eee;
    }

    #content {
        width: 100%;
    }

    #topnav > ul{
        padding: 12px 0;
    }

   .errors > .alert{
        margin: 10px 0;
        padding: 20px;
        width: auto;
    }
</style>


<div class="container well">

    <form method="post" action="" name="f">
        <input type="hidden" name="submitted" value="1"/>
        <input type="hidden" name="saved_card_id" id="saved_card_id" value=""/>
        <input type="hidden" name="image_front_data" id="image_front_data" value=""/>
        <input type="hidden" name="image_back_data" id="image_back_data" value=""/>
        {$ccform_hidden}

        {if $total}
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h5 class="pull-left">You will be charged : <b>${$total}</b></h5>
                </div>
            </div>
            <hr>
        {/if}

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                {if !$no_promotion}
                    {if $cc_redeemed}
                        Promotion Code Redeemed:
                        <b>${$cc_redeemed}</b>
                        <input type="hidden" name="cc_promo" value="{$cc_promo}"/>
                    {else}
                        <input type="text" class="form-control" id="cc_promo" name="cc_promo" size="40"
                               value="{$cc_promo}" placeholder="Promotion Code">
                    {/if}
                {/if}
            </div>
        </div>

        <br/>

        <div class="row">
            <div class="col-md-6 col-md-offset-3 errors">
                {if $error}
                    <div class="alert alert-danger center-block" role="alert">{$error}</div>
                {/if}
            </div>
        </div>

        {if $recurring_amount}
            <hr/>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div id="recurring_confirm">
                        <label>&nbsp;<br/>&nbsp;</label>
                        <div>
                            <p>This is recurring charge. <strong>You can cancel the subscription
                                    anytime</strong>.</p><br/>
                            <input type="checkbox" name="recurring_confirm" value="1"
                                   {if $recurring_confirm}checked="checked"{/if}/> I confirm this recurring
                            charge.
                        </div>
                    </div>
                    <br/>
                </div>
            </div>
        {/if}

        <br/>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <button id="payWithStripe" type="button" class="btn btn-primary" data-email="{$account->email}" data-total="{$total}">
                    Pay with Card
                </button>
            </div>
        </div>

</div>

</form>

{literal}
<script type="text/javascript">
$(document).ready(function() {

    var key = '{/literal}{$stripe_public_key}{literal}';

    var handler = StripeCheckout.configure({
        key: key
    });

    $('#payWithStripe').on('click', function (e) {
        // Open Checkout with further options:
        var email = $(this).data('email');
        var cc_promo = $('#cc_promo').val();

        // price has to be in cents
        var total = parseFloat($(this).data('total'));

        handler.open({
            name: 'Yeobill',
            description: '',
            amount: total * 100,
            zipCode: true,
            image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
            locale: 'auto',
            email: email,
            billingAddress: true,
			allowRememberMe: false,
            token: function (token) {
                var url = window.location.href;
                var html =  '<form action="' + url + '" method="POST">' +
                    '<input type="hidden" name="stripeToken" value="' + token.id + '" >' +
                    '<input type="hidden" name="email" value="' + token.email + '" >' +
                    '<input type="hidden" name="tokenObj" value="' + encodeURIComponent(JSON.stringify(token)) + '" >' +
                    '<input type="hidden" name="cc_promo" value="' + cc_promo + '" >' +
                    '<input type="hidden" name="submitted" value="1" >' +
                    '</form>';

                $(html).appendTo('body').submit();
            }
        });
        e.preventDefault();
    });

    // Close Checkout on page navigation:
    window.addEventListener('popstate', function () {
        handler.close();
    });
});
</script>
{/literal}

