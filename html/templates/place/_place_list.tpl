	{if $sc}
        <table id="sort" cellspacing="0" cellpadding="0">
                <tr>
                        <td><b>Sort by:</b>
                {section name=s loop=$sort}
                {assign var=s value=$sort[s]}
                <a {if $s.selected}id="{if $s.way==2}up{else}down{/if}"{/if} {if $s.way==2}class="down"{/if} href="{if
$linksort}{$linksort}&amp;{else}?{/if}order={$s.value}&orderway={$s.way}" 
onclick="_respondToMapChange('{$sortquery}&order={$s.value}&orderway={$s.way}'); return false;">{$s.key}</a>
                {/section}
                        </td>
                </tr>
        </table>
	<div class="clear">&nbsp;</div>
	{/if}
                {section name=sc loop=$sc}
                {assign var=c value=$sc[sc]}
<div class="businessresult clearfix{if $c.sponsor} sponsored{/if}" id="place_id_{$c.id}">
<div class="leftcol">
<div class="itemheading">
<a id="place_name_{$c.id}" href="{if $c.linkoverride}{$c.linkoverride}{else}{$place_link}?id={$c.id}{/if}"
title="{$c.name}{if $c.title} {$c.title}{/if}">{$smarty.section.sc.index+1}. {$c.name}</a>{if
$account_level>2||($smarty.session.worker_page&&$smarty.session.worker_page==$worker_link)} <span><a
href="/worker/{$worker_link}?id={$c.id}{if
$ref}&ref={$ref}{/if}" class="empedit">[ Edit ]</a></span>{/if}</div>
{if $c.extra}{$c.extra}{/if}
{$c.address}, {$c.loc_name} {$c.s} {$c.zipcode}
</div>
<div class="rightcol">
{if $c.sponsor}<div>Sponsored Result</div>{/if}
{if $c.review}<div class="reviewLinks""><strong><a href="{if $c.linkoverride}{$c.linkoverride}{else}{$place_link}?id={$c.id}{/if}" title="{$c.name}">{$c.review} Review{if $c.review>1}s{/if}</a>
{if $c.recommend||$c.review-$c.recommend} ({if $c.recommend}{$c.recommend}<img src="/images/Thmb_Up.jpg" alt="" />{/if}{if $c.review-$c.recommend}{if $c.recommend},
{/if}{$c.review-$c.recommend}<img src="/images/Thmb_Down.jpg" alt="" width="14" height="13" />{/if}){/if}</strong><br /></div>{/if}
{if $c.phone}{$c.phone}{/if}
{if $c.distance}<br/><b>{$c.distance} miles</b>{/if}
</div>
{if $c.lr && $c.rc}
<div class="reviewer_info clear review-block">
 <div class="review-avatar">
   <div class="photoBox">
        <a href="{if $c.linkoverride}{$c.linkoverride}{else}{$place_link}?id={$c.id}{/if}">
                <img src="{$c.ra}" alt="Photo of {$c.ru}" height="20" width="20"/>
        </a>
   </div>
 </div>
 <div class="review-story">
        {$c.rc|truncate:200}
 </div>
</div>
{/if}
</div>
                {/section}

	{if $alert}
<div class="alert"><p>There were <strong>no exact matches</strong> found for your search</p>{if $alert.ul}<ul>{section name=al
loop=$alert.ul}<li>{$alert.ul[al]}</li>{/section}</ul>{/if}</div>
{else}<span class="error">{$error}</span>{/if}

	<div class="pagelist">{$paging}</div>
