{section name=c loop=$comment}
{assign var=c value=$comment[c]}
<div class="commentSingle" id="co{$c.comment_id}">
	<img src="{if $c.avatar}{$config_image_server}/avatar/{$c.avatar}{else}/images/icons/User_1.jpg{/if}" alt="{$c.username}">
	<span class="commentDate">
		{$c.date}
		{if $isadmin || $c.owner}
			 | <span id="co{$c.comment_id}x"> <a href="" onclick="_ajaxsc('get', '', 'id={$id}&amp;commentremove={$c.comment_id}'); return false;">Delete</a> </span>
		{/if}
	</span> 
	<span class="userName">{$c.username}</span>
	<div class="userComment">{$c.comment}</div>
</div>
{/section}
