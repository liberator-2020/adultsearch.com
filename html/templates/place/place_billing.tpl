<div id="content">
 <div id="blue">

  <div id="owners">
  <span class="ownermenu">
    <a href="{$item_link}?id={$id}" title="">Business Main Page</a>
  </span>
  <span class="ownermenu">
   <a href="owner?id={$id}" title="">Edit My Business</a>
  </span>
  <span class="ownermenu">
   <a class="active" href="billing?id={$id}" title="">Billing</a>
  </span>
  </div>

<table width="100%" class="b">

{section name=cl loop=$form}{assign var=c value=$form[cl]}
{assign var=include value=$c.include}
<tr style="border: 1px solid #cccccc">
 <td class="first">{$c.name}:</td>
 <td class="second">{if $c.include}{include file="$include"}{else}{$c.value}{/if}</td>
</tr>
{/section}

</table>

 </div>
</div>
