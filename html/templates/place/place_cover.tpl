<table id="coversingle" {if $multicover}style="display:none"{/if}>
 <tr>
  <td width="90">&nbsp;</td>
  <td>Cover (<a href="" onclick="setCSFMon(); return false;">Set Covers From Monday</a>)</td>
 </tr>
 <tr>
  <td width="95">Monday</td>
  <td><select name="_day1">{$_day1}</select></td>
 </tr>
 <tr>
  <td>Tuesday</td>
  <td><select name="_day2">{$_day2}</select></td>
 </tr>
 <tr>
  <td>Wednesday</td>
  <td><select name="_day3">{$_day3}</select></td>
 </tr>
 <tr>
  <td>Thursday</td>
  <td><select name="_day4">{$_day4}</select></td>
 </tr>
 <tr>
  <td>Friday</td>
  <td><select name="_day5">{$_day5}</select></td>
 </tr>
 <tr>
  <td>Saturday</td>
  <td><select name="_day6">{$_day6}</select></td>
 </tr>
 <tr>
  <td>Sunday</td>
  <td><select name="_day7">{$_day7}</select></td>
 </tr>

 <tr>
  <td colspan="2"><b><a href="" onclick="changecoverstyle(); return false;">Click here</a> If you have different cover options for 
different 
hours</b></td>
 </tr>

</table>

<table width="100%" id="covermulti" {if !$multicover}style="display:none"{/if} border="1">
 <tr>
  <td width="90">&nbsp;</td>
  <td>Before <select name="beforetime">{$beforetime}</select><br/>
	(<a href="" onclick="setCMFMon(1); return false;">Set Covers From Sunday</a>)
  </td>
  <td>Between <select name="time1">{$time1}</select> - <select name="time2">{$time2}</select><br/>
	(<a href="" onclick="setCMFMon(2); return false;">Set Covers From Sunday</a>)
   </td>
  <td>After <select name="after">{$after}</select><br/>
	(<a href="" onclick="setCMFMon(3); return false;">Set Covers From Sunday</a>)
   </td>
 </tr>

 <tr>
  <td>Sunday</td>
  <td><select name="_day7_1">{$_day7_1}</select></td>
  <td><select name="_day7_2">{$_day7_2}</select></td>
  <td><select name="_day7_3">{$_day7_3}</select></td>
 </tr>

 <tr>
  <td>Monday</td>
  <td><select name="_day1_1">{$_day1_1}</select></td>
  <td><select name="_day1_2">{$_day1_2}</select></td>
  <td><select name="_day1_3">{$_day1_3}</select></td>
 </tr>

 <tr>
  <td>Tuesday</td>
  <td><select name="_day2_1">{$_day2_1}</select></td>
  <td><select name="_day2_2">{$_day2_2}</select></td>
  <td><select name="_day2_3">{$_day2_3}</select></td>
 </tr>

 <tr>
  <td>Wednesday</td>
  <td><select name="_day3_1">{$_day3_1}</select></td>
  <td><select name="_day3_2">{$_day3_2}</select></td>
  <td><select name="_day3_3">{$_day3_3}</select></td>
 </tr>

 <tr>
  <td>Thursday</td>
  <td><select name="_day4_1">{$_day4_1}</select></td>
  <td><select name="_day4_2">{$_day4_2}</select></td>
  <td><select name="_day4_3">{$_day4_3}</select></td>
 </tr>

 <tr>
  <td>&nbsp;</td>
  <td>Before <select name="beforetime_2">{$beforetime_2}</select></td>
  <td>Between <select name="time1_2">{$time1_2}</select> - <select name="time2_2">{$time2_2}</select></td>
  <td>After <select name="after_2">{$after_2}</select></td>
 </tr>

 <tr>
  <td>Friday</td>
  <td><select name="_day5_1">{$_day5_1}</select></td>
  <td><select name="_day5_2">{$_day5_2}</select></td>
  <td><select name="_day5_3">{$_day5_3}</select></td>
 </tr>

 <tr>
  <td>Saturday</td>
  <td><select name="_day6_1">{$_day6_1}</select></td>
  <td><select name="_day6_2">{$_day6_2}</select></td>
  <td><select name="_day6_3">{$_day6_3}</select></td>
 </tr>

</table>
