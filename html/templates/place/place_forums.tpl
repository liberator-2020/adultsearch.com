{* $forum_topic, $name, $new_topic_link passed from parent template place/place_place.tpl *}
<div id="forums">
	<div id="title"></div>
	{section name=f loop=$forum_topic}
	{assign var=f value=$forum_topic[f]}
		<li>
			<a href="{$f.topic_link}" title="{$f.topic_title}">{$f.topic_title|truncate:50}</a><br>
			<span class="extra">{$f.topic_time} by {$f.username}</span>
		</li>
	{sectionelse}
		<li>There are no discussions on {$name} yet.</li>
	{/section}
	<a href="{$new_topic_link}" rel="nofollow" class="forumBtn"> <img src="/images/btn_forum.png" width="132" height="20" alt="" /></a>
</div>
