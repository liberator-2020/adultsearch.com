{if !$nomap && $map && $map.loc_lat > 1 && $map.loc_long > 1}

{literal}
<style type="text/css">
#mapcontainer {width: 1000px; height: 600px;}
#map_canvas_2 {width: 1000px; height: 400px;}
.preloader {
	transform: scale(0.5);
	border: none !important;
	width: 225px;
}
.static-map{
	display: none;
}
</style>
{/literal}

<div id="directions">
	<div id="title"></div>
	<div id="map_canvas">
		{if $map.loc_lat > 1 && $map.loc_long > 1}
			<a class="maptrg" href="#mapcontainer" title="directions for {$name} {$loc_name}, how to get to the {$name} {$loc_name}">
				{if $map.map_image_url}
					<img src="{$map.map_image_url}" alt="Here should be map with this place" />
				{elseif $map.map_image_update_url}
					<div class="map-container">
						<img src="/images/ajax_loader_128.gif" class="preloader" alt="" />
						<img src="" alt="Here should be map with this place" data-src="{$map.map_image_update_url}" class="static-map">
					</div>
				{else}
					<div class="map-container">
						<img src="/images/no_map.png" />
					</div>
				{/if}
			</a>
		{/if}
	</div>
	<div class="mapLink">
		<a class="maptrg" href="#mapcontainer" title="Directions for {$name} {$loc_name}, how to get to the {$name} {$loc_name}">View Larger Map / Directions / Street View</a>
	</div>
</div>

<div class="" style="display:none;">
	<div id="mapcontainer">
		<div class="mapinfo">
			<a href="{$link}" title="{$name} {$loc_name}">{$name}</a><br/>
			{$address}{if $phone}<br/>{$phone}{/if}
		</div>
			<form action="#" onsubmit="setDirections();return false" name="directionform" method="get">
				<table>
					<tr style="vertical-align: top;">
						<th align="left">From:&nbsp;</th>
						<td><input type="text" size="50" id="fromAddress" name="from" value="{$from}" placeholder="Address, city, state, zip"/></td>
						<th align="right">&nbsp;&nbsp;To:&nbsp;</th>
						<td align="right">
							<input type="text" size="50" id="toAddress" name="to" value="{if $map.loc_lat}{$map.loc_lat},{$map.loc_long}{else}{$to}{/if}" />
							<input name="button" onclick="setDirections(); return false;" type="submit" value="Get Directions!" />
						</td>
					</tr>
				</table>
			</form>
		<br/>
		<table class="directions" width="1000">
			<tr>
				<td valign="top">
					<div id="directions_2"></div>
					<div id="map_canvas_2" style=""></div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<b>If the location of this place is wrong on the map, please <a href="{$link_map_update}" rel="nofollow">click here</a> to point us the correct location on the map!</b>
				</td>
			</tr>
			{*<tr>*}
				{*<th colspan="2">Street View (if possible)</th>*}
			{*</tr>*}
			{*<tr>*}
				{*<td valign="top" colspan="2"><div id="pano" style="width: 1000px;  height: 400px;";></div></td>*}
			{*</tr>*}
		</table>
	</div>
</div>

{literal}
<script type="text/javascript">

var map, dirLayer;
	L.mapquest.key = {/literal}'{$map.public_key}'{literal};

function map_directions_init() {
	map = L.mapquest.map('map_canvas_2', {
		center: [{/literal}{$map.loc_lat}, {$map.loc_long}{literal}],
		layers: L.mapquest.tileLayer('map'),
		zoom:   12
	});

	map.addControl(L.mapquest.control());
	L.marker([{/literal}{$map.loc_lat}, {$map.loc_long}{literal}]).addTo(map);
}

function setDirections() {
	var from = document.directionform.from.value,
		to   = document.directionform.to.value,
		directions = L.mapquest.directions();

	if (from === '') {
		alert("Please type your starting point address");
		return;
	}

	if(dirLayer !== undefined) {
		map.removeLayer(dirLayer);
	}

	directions.route({
		start: getFormattedDirection(from),
		end:   getFormattedDirection(to)
	}, function (err, data) {
		if (err.message) {
			console.log(err);
		} else {
			dirLayer = L.mapquest.directionsLayer({
				directionsResponse: data,
				fitBounds:          false
			}).addTo(map);
		}
	});
}

// Format input value to mapquest valid variable if it is coordinates
function getFormattedDirection(str) {
	var partsOfStr = str.split(','),
		notNumbers = 0;

	partsOfStr.forEach(function (item) {
		if (isNaN(item)) {
			notNumbers++;
		}
	});

	if (notNumbers === 0 && partsOfStr.length === 2) {
		return [partsOfStr[0], partsOfStr[1]];
	}

	return str;
}

$(document).ready(function(){
 $("a.maptrg").fancybox({
  'hideOnContentClick': false, // so you can handle the map
  'overlayColor'	  : 'black',
  'overlayOpacity'	: 0.6,
  'autoDimensions': false,
  'width': '1025',
  'height': '600',
  'onComplete': function(){
	map_directions_init();
	$("#fancybox-close").css({"opacity":0.5});
	$("#fancybox-content").css({"padding":'10px'});
  },
  'onCleanup': function() {
   var myContent = this.href;
   $(myContent).unwrap();
  } // fixes inline bug
 });
});

var preloader = $('.preloader');

if ($(preloader).length){
	$(preloader).each(function(i, item){
		var img = new Image(),
			map = $(this).siblings('.static-map'),
			object = $(this);

		img.onload = function () {
			$(map).attr('src', img.src);
			$(map).show();
			$(object).hide();
		};
		img.src = $(map).data('src');
	});
}

//]]>
</script>
{/literal}

{/if}
