{* $hours, $current_weekday passed from parent template place/place_place.tpl *}
{if $hours}
	<div id="hours">
		<div class="title">Hours</div>
		<label{if $current_weekday == "monday"} class="active"{/if}><span class="day">Monday</span>{$hours.monday}</label>
		<label{if $current_weekday == "tuesday"} class="active"{/if}><span class="day">Tuesday</span>{$hours.tuesday}</label>
		<label{if $current_weekday == "wednesday"} class="active"{/if}><span class="day">Wednesday</span>{$hours.wednesday}</label>
		<label{if $current_weekday == "thursday"} class="active"{/if}><span class="day">Thursday</span>{$hours.thursday}</label>
		<label{if $current_weekday == "friday"} class="active"{/if}><span class="day">Friday</span>{$hours.friday}</label>
		<label{if $current_weekday == "saturday"} class="active"{/if}><span class="day">Saturday</span>{$hours.saturday}</label>
		<label{if $current_weekday == "sunday"} class="active"{/if}><span class="day">Sunday</span>{$hours.sunday}</label>
	</div>
	<br style="clear: both;" />
{/if}
