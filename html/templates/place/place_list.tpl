<link href="/css/listings.css?20190621" rel="stylesheet" type="text/css">
<script src="/js/tools/rateit/jquery.rateit.js" type="text/javascript"></script>

<div class="forumCrumb">{if $forumlink}<a href="{$forumlink}">{$what} Forums</a>{/if}</div>
<div id="container" class="clear">
 
<!-- LEFT COLUMN -->
<div id="ListLeft">
	{if $filter}
	<div id="refineResults">
		<h4>You Searched For</h4>
		{section name=filter loop=$filter}
		{assign var=f value=$filter[filter]}
			<span class="action"> <a href="{$f.link}" rel="nofollow">X</a> {$f.name}<br /></span>
		{/section}
	</div>
	{/if}
	
	<!-- begin search -->
	<form method="get" action="" name="search" id="search" >
		{$search_hidden} Search for...<br />
		 <input type="text" id="query" name="query" size="32" value="{$smarty.get.query}" data-query-link="{$search_query_link}" /><br />
	  <span class="action">
	  <input type="submit" value="search" />
	  </span>
	  <div class="clear"></div>
	</form>
	<!-- end search -->

	<!-- refine -->
	{if $refine}
		<a name="refine"></a>
		<div id="refine">
		{section name=r loop=$refine}
		{assign var=r value=$refine[r]}
			<h4>{$r.name}</h4>
			<ul>
			{assign var=class value=''}
			{section name=a loop=$r.alt}
				{assign var=a value=$r.alt[a]}
				{if $a.link} 
					<li{$class}> <a href="{$a.link}" rel="nofollow" {if $a.title} title="{$a.title}"{/if}>{$a.name}</a> ({$a.c})</li>
				{elseif $a.nonlink} 
					<li{$class}>{$a.name} ({$a.c})</li>
				{else} 
					<li{$class}><a href="{if $link}{$link}&amp;{else}?{/if}{$a.type}={$a.id}" rel="nofollow" {if $a.title} title="{$a.title}"{/if}>{$a.name}</a> ({$a.c})</li>
				{/if}
			{/section}
			</ul>
		{/section} 
		</div>
	{/if}
	<!-- /refine -->

	<!-- forum -->   
	{if $forum}
	<div id="forum">
		<h4>Recent Forum Topics</h4>
		<ul>
			{section name=f loop=$forum.posts}
			{assign var=f value=$forum.posts[f]}
				<li><a href="{$f.link}" title="{$f.name}">{$f.name|truncate:25}</a>{if $f.c} ({$f.c}){/if}</li>
			{sectionelse}
				There are no forum posts yet.
			{/section}
			<span class="action"><a href="{$forumlink}/new_topic">Start a new topic</a></span>
		</ul>
		<div class="clear"></div>
	</div>
	{/if}
	<!-- /forums -->

</div>
<!-- /LEFT COLUMN -->

<!-- MAIN CONTENT -->
<div id="ListMid">
	<div id="results">
		<h1 class="ResultsTitle">
			<span class="action postTitle"><a href="{if $display_add_link || $account_level > 2}/worker/{$worker_link}{else}submit{/if}" rel="nofollow">Add place</a> 
{*
<a style="margin-right: 5px;" href="{$cur_loc_url}map?type={$place_type_id}">Map view</a> 
*}
			</span>{$total} {$what}{if $total>1 && $what|substr:-1 != "s"}s{/if} {if $foundlink} {$foundlink}{/if}
		</h1>
	  
<!-- sort -->
<div id="sorting">
	{section name=i loop=$sort_array}
	{assign var=s value=$sort_array[i]}
		<span><a {if $s.selected_way}id="{$s.selected_way}"{/if} href="{$s.link}" title="Sort by {$s.title}" {if $s.selected_way}class="{$s.selected_way}"{/if} rel="nofollow">{$s.title}</a></span>
	{/section}
</div>
<!-- /sort -->
	  
{if $sc} 
	{section name=sc loop=$sc}
	{assign var=c value=$sc[sc]}

	<!-- INDIVIDUAL LISTING -->
	<li class="Listing places">
		<a href="{$c.url}" title="{$c.name}{if $c.title} {$c.title}{/if}">  
			{if $c.thumb}
				<img src="{$config_image_server}/place/t/{$c.thumb}" alt="{$c.name}" width="131" height="98" border="0" />
			{else}
				<img src="{$config_site_url}/images/placeholder.gif" alt="{$c.name}" border="0" width="131" height="98" />
			{/if}
		  <div class="right{if $c.sponsor} withSponsor{/if}"> 
		  {if $c.sponsor}<div class="sponsor">Sponsor</div>{/if}
		 
			   {if $c.distance}<div>({$c.distance} miles)</div>{/if}
{if $c.video||$c.coupon}<div class="icons">{if $c.coupon}<span id="coupon">Coupon</span>{/if}
{if $c.video}<span id="video">Video</span>{/if}</div>{/if}
		  
		  </div>
  <h2 class="ListingTitle">{$c.name}</h2>

  <div class="ListingRight">
						   {if $c.extra}<b>{$c.extra}</b>{/if}
			   {if $c.phone}{$c.phone}<br />{/if}
			 {if $c.address}{$c.address}<br />{/if}
			  {$c.loc_name} {$c.s} {$c.zipcode} 

			<!-- rating -->			  
			{if $c.review}
			<div class="reviewLinks">
			<div class="rateit" data-rateit-value="{$c.recommend/$c.review}" data-rateit-ispreset="true" data-rateit-readonly="true">
			</div>  
			{$c.review} Review{if $c.review>1}s{/if}
			</div>
			{/if} 
			<!-- /rating -->

			</div>
		</a>

 {if $display_edit_link||$account_level>2||($smarty.session.worker_page&&$smarty.session.worker_page==$worker_link)} 
 <a class="edit" href="/worker/{$worker_link}?id={$c.id}{if $ref}&ref={$ref}{/if}">Edit</a>
	 {if $config_env && $config_env == 'dev'}
	 <a class="edit" href="/mng/dev_download_image?id={$c.id}&return_url={$smarty.server.REQUEST_URI}" style="margin-right:72px;width:185px;">Download images from Prod</a>
	 {/if}
 {/if}

</li>
<!-- /INDIVIDUAL LISTING -->
			  
	  {/section} 

	   {elseif $alert}
	  <div class="zeroResults">
		<p>There were <strong>no exact matches</strong> for "{$smarty.get.query}."</p>
		{if $alert.ul}
		<ul>
		  {section name=al loop=$alert.ul}
		  <li>{$alert.ul[al]}</li>
		  {/section}
		</ul>
		{/if}</div>
	  {else}
	  <span class="error">{$error}</span>
	  {/if} 

   </div>
	<!-- pagination -->
	<div class="pagelist">
		<div id="sort">
			{if $type}
				<a href="rss?type={$type}&loc_id={$loc_id}" title="{$loc_name} escorts rss field" id="rssico"></a>
			{else if $rss_available}
				<a href="rss" title="{$loc_name} RSS feed" id="rssico"></a>
			{/if}
			<select id="ipp2" name="ipp">
				<option value="30">30 per page</option>
				<option value="60"{if $smarty.get.ipp==60 || $ipp==60} selected="selected"{/if}>60 per page</option>
				<option value="90"{if $smarty.get.ipp==90 || $ipp==90} selected="selected"{/if}>90 per page</option>
			</select>
		</div> {$paging}
	</div>
	<!-- /pagination -->

	{if $location_video_html || $category_description}
		<div id="list_location_video">
			{if $category_description}
				{$category_description}
			{/if}
			{if $location_video_html}
				<div class="video">
					{$location_video_html}
				</div>
			{/if}
		</div>
	{/if}

</div>
<!-- /MAIN CONTENT -->

<!-- RIGHT COLUMN -->
<div id="ListRight">

	{include file='page_ads.tpl'}

	{include file='page_side_sponsor.tpl'}

	<!-- featured -->
	{if $featuredads}
		<div id="servicesThumb">
		<div class="featTitle">{$loc_name} Escorts</div>
			{section name=cl loop=$featuredads}
			{assign var=p value=$featuredads[cl]}
				<a href="{$p.link}">
					<img src="{if $p.avatar}{$p.avatar}{else}//adultsearch.com/images/icons/User_1.jpg{/if}" alt="" border="0" />
					<div class="category">{$p.category}</div>
					<li>{$p.info}</li>
				</a>
			{/section}
		</div>
	{/if}
	<!-- /featured -->

	{include file='page_ads_2.tpl'}

	</div>
	<!-- /RIGHT COLUMN -->
</div>

<script type="text/javascript">
$(document).ready(function() {
	$("form#search").submit(function(event) {
		event.preventDefault();
		if ($("input#query").attr("data-query-link") == "")
			window.location.href = "?query=" + $("input#query").val();
		else
		window.location.href = $("input#query").attr("data-query-link") + '/' + $("input#query").val();
		return false;
	});
});
$("select[name=ipp]").change(function() {
	var qp = {}, qs = location.search.substring(1), re = /([^&=]+)=([^&]*)/g, m;
	while (m = re.exec(qs)) {
		qp[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
	}
	qp['ipp'] = $(this).val();
	location.search = $.param(qp);
});
</script>
