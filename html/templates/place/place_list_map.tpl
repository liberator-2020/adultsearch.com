<!-- MAIN CONTENT -->
<div id="container" class="clear bootstrap">

    <!-- FILTER -->
    <div id="filter" style="margin-top: 10px;margin-bottom: 10px;{if $mobile == 1 }/*display:none;*/{/if}" class="">
        <div class="input-group">
            <div class="input-group-btn">
                <button tabindex="-1" class="btn btn-default btn-select" type="button" data-role="none">Select</button>
                <button tabindex="-1" data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"
                        data-role="none">
                    <span class="caret"></span>
                </button>
                <ul role="menu" class="dropdown-menu" style="z-index: 1001;">
                    {section name=category loop=$categories}
                        {assign var=c value=$categories[category]}
                        <li><a href="#" data-role="none">
                                <input type="checkbox" data-role="none"
                                       data-index="{$c.place_type_id}" {if in_array($c.place_type_id,$filter_types)} checked{/if}><span
                                        class="lbl"> {$c.name}</span>
                            </a></li>
                        {sectionelse}
                    {/section}
                </ul>
            </div>
            <input type="text" class="form-control type-name" data-role="none">
            <input type="hidden" class="type-value" data-role="none">
        </div>
    </div>
    <!-- /FILTER -->

    <!-- MAIN CONTENT -->
    <div id="map" style="width: 100%; height: 530px;"></div>

</div>
<!-- /MAIN CONTENT -->

<script type="text/javascript">
	let $map_zoom    = parseInt('{$map_zoom}'),
	    $map_markers = {$map_markers},
	    $map_center  = [{$location_lat}, {$location_long}];

	var markers;
	var redIcon;
	var map;

	function setMarkers($map_markers) {
		console.log('count markers#' + $map_markers.length);

		if (markers) {
			map.removeLayer(markers);
		}

		markers = L.markerClusterGroup();

		$map_markers.forEach(function (obj) {

			var title         = obj.name,
			    customPopup   = "<div class=' text-center'><h4 class=\"ListingTitle\">" + obj.name + "</h4>" +
			                    "<a href=\"" + obj.linkoverride + "\?back_url=" + encodeURIComponent(getUrl(false)) +
			                    "\" rel=\"nofollow\" class='btn btn-default btn-blue' target='_self'>" + "View" + "</a></div>";
			var customOptions =
				    {
					    'maxWidth':  '500',
					    'className': 'customPopup1'
				    };
			// console.log(obj.latitude, obj.longitude);
			var marker        = L.marker(new L.LatLng(obj.latitude, obj.longitude), {
				title: title,
				icon:  redIcon // L.mapquest.icons.marker()
			});
			// marker.on('click', function () {
			// 	window.open(obj.linkoverride, '_self')
			// });
			marker.bindPopup(customPopup, customOptions);
			markers.addLayer(marker);
		});

		map.addLayer(markers);
	}

	function getMarkers() {
		var center = map.getCenter();
		var type   = $('input.type-value').val();
		return $.ajax({
			type:     'GET',
			url:      '/_ajax/markers?type=' + type + '&loc_lat=' + center.lat + '&loc_long=' + center.lng,
			dataType: "json"
		})
	}

	window.onload = function () {
		L.mapquest.key = '{$map_public_key}';

		var baseLayer = L.mapquest.tileLayer('map');

		map = L.mapquest.map('map', {
			center: $map_center,
			layers: baseLayer,
			zoom:   $map_zoom
		}).on('moveend', function () {
			getMarkers().done(function (result) {
				setMarkers(result);
			});
		});


		// function mapMoveEnd(e) {
		// 	map.off('moveend', mapMoveEnd);
		// 	console.log(e);
		// 	getMarkers().done(function (result) {
		// 		setMarkers(result);
		// 	});
		// 	// map.panTo(somewhere, { duration: .25 }); // seconds
		// 	setTimeout(function(){ map.on('moveend', mapMoveEnd); }, 300); // milliseconds
		// }
        //
		// map.on('moveend', mapMoveEnd);

		// https://github.com/pointhi/leaflet-color-markers
		redIcon = new L.Icon({
			iconUrl:     'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
			shadowUrl:   'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
			iconSize:    [25, 41],
			iconAnchor:  [12, 41],
			popupAnchor: [1, -34],
			shadowSize:  [41, 41]
		});

		// setMarkers($map_markers);

		// map.addControl(L.mapquest.navigationControl());
		map.addControl(L.mapquest.locatorControl());
		// map.addControl(L.mapquest.control());

		$('.dropdown-toggle').dropdown();
		$(document).on('click', 'ul.dropdown-menu', function (e) {
			e.stopPropagation();
		});
		checkLine();
		$('ul.dropdown-menu input[type=checkbox]').each(function () {
			$(this).change(checkLine);
		});

		$('button.btn-select').on('click', function () {
			var url = getUrl(true);
			window.open(url, '_self')
		})
	};

	function getUrl(full) {
		var type   = $('input.type-value').val();
		var center = map.getCenter();
		var zoom = map.getZoom();
		var url;
		if (full) {
			url = window.location.origin + window.location.pathname;
		} else {
			url = window.location.pathname;
		}
		if (type.length) {
			url += '?type=' + type + '&loc_lat=' + center.lat + '&loc_long=' + center.lng + '&zoom=' + zoom;
		} else {
			url += '?loc_lat=' + center.lat + '&loc_long=' + center.lng + '&zoom=' + zoom;
		}
		return url;
	}

	function checkLine() {
		var line     = '';
		var lineName = '';
		var item;
		$('ul.dropdown-menu input[type=checkbox]').each(function () {
			item = $(this);
			if (item.is(':checked')) {
				if (line.length) {
					line += ',';
					lineName += ';';
				}
				line += parseInt(item.data('index'));
				lineName += $("+ span", this).text().trim();
			}
		});
		$('input.type-value').val(line);
		$('input.type-name').val(lineName)
		getMarkers().done(function (result) {
			setMarkers(result);
		});
	}
</script>
