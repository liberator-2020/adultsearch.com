{literal}
    <script type="text/javascript">
    //<![CDATA[
 
	var gdir2, map2;

    function initialize2() {
      if (GBrowserIsCompatible()) {      
          var mapOptions = {
            googleBarOptions : {
              style : "new"
            }
          }
        map2 = new Gmap2(document.getElementById("map_canvas2"), mapOptions);
	map2.addControl(new GMapTypeControl());
	map2.addControl(new GSmallMapControl());
        gdir2 = new GDirections(map2, document.getElementById("directions2"));
{/literal}
         map2.setCenter(new GLatLng({$loc_lat}, {$loc_long}), 13);
          var latlng = new GLatLng({$loc_lat}, {$loc_long});
          map2.addOverlay(new GMarker(latlng));
	 map2.enableGoogleBar();


        var fenwayPark = new GLatLng({$loc_lat},{$loc_long});
{literal}
        var panoramaOptions = { latlng:fenwayPark };
        var myPano = new GStreetviewPanorama(document.getElementById("pano"), panoramaOptions);

      }
    }
    
    function setDirections(fromAddress, toAddress) {
	if( fromAddress == "" ) { alert("Please type your starting point address"); return; }
      gdir2.load("from: " + fromAddress + " to: " + toAddress,
                { "locale": "en" });
    }

//]]>
    </script>

{/literal}
  
<div>
<div class="mapinfo">
<a href="{$link}" title="{$name} {$loc_name}">{$name}</a><br/>{$street}{if $phone}<br/>{$phone}{/if}{if $location}<br/>{$location}{/if}</div>

<form action="#" onsubmit="setDirections(this.from.value, this.to.value); return false" name="directionform" method="get">
 <input type="hidden" name="section" value="{$section}" />
 <input type="hidden" name="id" value="{$id}" />

  <table>

   <tr><th align="left">From:&nbsp;</th>

   <td>Address, city, state, zip<br /><input type="text" size="50" id="fromAddress" name="from"
     value="{$from}"/></td>
   <th align="right">&nbsp;&nbsp;To:&nbsp;</th>
   <td align="right"><input type="text" size="50" id="toAddress" name="to"
     value="{if $mp_lat}{$mp_lat},{$mp_long}{else}{$to}{/if}" /> <input name="button" onclick="setDirections(document.directionform.from.value, 
document.directionform.to.value); return false;" 
type="submit" 
value="Get Directions!" /></td></tr>

   </table>
</form>
</div>
<br/>

<table class="directions" width="1000">
<tr>
    <td valign="top">
		<div id="directions2"></div>
		<div id="map_canvas2" style="width: 1000px; height: 400px;"></div>
	</td>
</tr>
<tr>
	<td colspan="2">
		<b>If the location of this place is wrong on the map, please <a href="{$link_map_update}" rel="nofollow">click here</a> to point us the correct location on the map!</b>
	</td>
</tr>
<tr>
	<th colspan="2">Street View (if possible)</th>
</tr>
<tr>
	<td valign="top" colspan="2"><div id="pano" style="width: 1000px;  height: 400px;";></div></td>
</tr>
</table> 

<script type="text/javascript">
<!--
{literal}
jQuery(document).ready(function() {initialize2();});
{/literal}
-->
</script>

