{extends file="./place_mapupdate_base.tpl"}
{block name="table"}
    <table>
    <tr>
        <td colspan="2">
            <b>You can mark the exact location of {$name} by clicking on the map with your mouse.</b>
        </td>
    </tr>
    <tr>
        <td width="800">
            <div id="map" style="width: 800px; height: 500px;"></div>
        </td>
        <td valign="top">
            <form action="" method="post">
                Latitude: <input type="input" size="10" name="lat" value="" id="lat"/><br/>
                Longitude: <input type="input" size="10" name="long" value="" id="long"/><br/>
                <input type="submit" name="Submit" value="Submit"/>
            </form>
        </td>
    </tr>
    </table>
{/block}

