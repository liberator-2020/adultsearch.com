<div class="mapinfo">
    <a href="{$link}?id={$id}" title="{$name} {$loc_name}">{$name}</a><br/>
    {$street}
    {if $phone}<br/>{$phone}{/if}
    {if $location}<br/>{$location}{/if}
</div>

{literal}
<script type="text/javascript">

	var map, geocoding, marker;
	L.mapquest.key  = {/literal}'{$map_public_key}'{literal};
	L.mapquest.open = true;

	function createUpdateMap() {
		geocoding.geocode({/literal}'{$location}'{literal}, createMap);

		function createMap(error, response) {
			if (error.message) {
				console.log(error);
				alert("Address not found");
				return ;
			}

			var location = response.results[0].locations[0],
			    latLng   = location.displayLatLng;

			map = L.mapquest.map('map', {
				center: latLng,
				layers: L.mapquest.tileLayer('map'),
				zoom:   14
			});

			marker = L.marker(latLng, {
				          draggable: true
			          })
			          .addTo(map)
			          .on('dragend', function (e) {
				          showNewLocationPopup(e.target._latlng);
				          setNewLocationInForm(e.target._latlng.lat, e.target._latlng.lng);
			          });

			map.on('click', function (e) {
				marker.setLatLng(e.latlng);
				showNewLocationPopup(e.latlng);
				setNewLocationInForm(e.latlng.lat, e.latlng.lng);
			});
		}
	}

	function showNewLocationPopup(latlng) {
		L.popup({closeButton: true})
		 .setLatLng(latlng)
		 .setContent('You have marked the location of the place. Please submit the form on the right to notify us about this change.')
		 .openOn(map);
	}

	function setNewLocationInForm(lat, lng) {
		document.getElementById('lat').value  = lat;
		document.getElementById('long').value = lng;
	}

	jQuery(document).ready(function () {
		window.onload = function () {
			geocoding = L.mapquest.geocoding();

			createUpdateMap();
		}
	});

</script>
{/literal}
{block name="table"}{/block}