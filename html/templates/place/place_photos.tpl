<!-- photos block -->
<div id="photos"{if not $video and not $youtube_video} class="photosLarge"{/if}>
	  <div id="photoUpload"><a rel="nofollow" href="/upload?ref={$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" id="addphoto">Upload Photo</a></div>
<div id="title"></div>
<a href="{$photos.image}" class="lightbox" id="lightbox0"><img src="{$photos.thumb}" alt="{$photos.name}" /></a>

{if $photos.pics}
<div id="photolist">
    <div id="thumbScroll" class="playlist">
   		<a class="prev browse left"></a>
	    <div class="scrollable" id="scrollable2">
		    <div class="items">
		    {section name=pg loop=$photos.pics}
		    {assign var=pics_group value=$photos.pics[pg]}
		        <div>
		        {section name=pgi loop=$pics_group}
		        {assign var=pic value=$pics_group[pgi]}
					<a href="{$pic.image}" class="lightbox" rel="cimg" id="lightbox{$pic.id}"><img src="{$pic.thumb}" alt="{$pic.name}"/></a>
		        {/section}
		        </div>
		    {/section}
		    </div>
	    </div>
	    <a class="next browse right"></a>
    </div>
</div>
<script language="JavaScript">
$("#scrollable2").scrollable();
</script>
{/if}

</div>
<!-- /photos block -->
