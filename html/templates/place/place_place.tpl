<link href="/css/ratings.css" rel="stylesheet" type="text/css">
<link href="/css/places.css?20190523" rel="stylesheet" type="text/css">
<link href="/css/slider.css" rel="stylesheet" type="text/css">
<script src="/js/tools/rateit/jquery.rateit.js" type="text/javascript"></script>

<div id="places">

	{if not $video and not $youtube_video}
		<div id="lowerRight"> 
			{if $photos and not $video and not $youtube_video}
				{include "place/place_google.tpl" map=$map nomap=$nomap name=$name loc_name=$loc_name} 
			{/if}
			{include "place/place_forums.tpl" forum_topic=$forum_topic name=$name new_topic_link=$new_topic_link}
			{* hide Ads block on dev environment *}
			{if $config_env !== 'dev'}
			<div id="ppc">
				{include file='page_ads.tpl'}
				{include file='page_ads_2.tpl'}
			</div>
			{/if}
		</div>
	{/if}
  
	<div id="topContent"{if not $video and not $youtube_video} class="width"{/if}>
    
	{if $photos}
		{include "place/place_photos.tpl" photos=$photos} 
	{/if}
	
	{if $video}
		<div id="videoBox">
			<div id="placesVideo">
	        <a href="{$link_uploadVideo}" id="videoUpload">Upload A Video</a>
<h3 id="videoName"><!-- {$video_count} -->{$name}</h3>
			{if $video_pl}
			<div class="playlist">
				<a class="prev browse left"></a>
				<div id="scrollable" class="scrollable vertical">
			        <div class="items">
			        {section name=vp loop=$video_pl}
			        {assign var=video_pl_group value=$video_pl[vp]}
		                <div>
			            {section name=vpi loop=$video_pl_group}
			            {assign var=video_pl_item value=$video_pl_group[vpi]}
                        <a href="{$video_pl_item.url}" title="{$video_pl_item.name}" data-video-id="{$video_pl_item.file_id}" data-video-name="{$video_pl_item.name}"><img src="{$video_pl_item.thumb}" alt="" onclick="return false;" /></a>
		    	        {/section}
        	    	    </div>
			        {/section}
			        </div>
				</div>
				<a class="next browse right"></a>
			</div>
			{/if}
  
			<div class="flowplayer" title="{$video.name}" data-video-file-id="{$video.file_id}"><video poster="{$video.thumb}" data-index="0" style="width: 100%; height: auto;"><source type="video/mp4" src="{$video.url}"></video></div>

			{if $video_pl}
				<script  type="text/javascript" language="JavaScript">
$(document).ready(function() {
    $("div#scrollable div.items a").click(function() {
        var vid_id = $(this).attr("data-video-id");
        var vid_name = $(this).attr("data-video-name");
        var place_url = $(this).attr("data-video-link");
        var vid_url = $(this).attr("href");
        $("h3#city_video_name").html(vid_name);
        $("a#flow_vid_city").attr("title", vid_name);
        $('a#flow_vid_city').attr("data-video-file-id", vid_id);
        $('a#flow_vid_city').attr("href", vid_url);
        $('a#city_video_link').attr("href", place_url);
    });
});
$(function () {
    var playlist = [],
        videos = $(".scrollable a"),
        scrollable,
        player,
        i;
 
    $('.scrollable a').each(function(i,obj) {
        var video_url = $(obj).attr('href');
        playlist.push({
            sources: [
                { type: "video/mp4",    src: video_url },
            ]
        });
    });

    $(".scrollable").scrollable({
        circular: true
    });
 
    scrollable = $(".scrollable").data("scrollable");

    player = flowplayer($("div.flowplayer"), {
        // loop the playlist in a circular scrollable
        loop: true,
        ratio: 9/16,
        splash: true,
        bgcolor: "#333333",
        customPlaylist: true,
        playlist: playlist
 
    }).on("ready", function (e, api, video) {
        videos.each(function (i) {
            var active = i == video.index;
            $(this).toggleClass("is-active", active)
                 .toggleClass("is-paused", active && api.paused);
        });
 
    }).on("pause resume", function (e, api) {
        videos.eq(api.video.index).toggleClass("is-paused", e.type == "pause");
 
    }).on("finish", function (e, api) {
        var vindex = api.video.index,
            currentpage = Math.floor(vindex / 2),
            scrollindex = scrollable.getIndex(),
            next;
        // advance scrollable every 2nd playlist item
        if (vindex % 2 != 0 && scrollindex == currentpage) {
            // prefer circular movement when current item is visible
            scrollable.next();
        } else if (scrollindex != currentpage) {
            // scroll to next item if not visible
            next = vindex < playlist.length - 1 ? vindex + 1 : 0;
            scrollable.seekTo(Math.floor(next / 2));
        }
    });

    player.setPlaylist(playlist);

    videos.click(function () {
        var vindex = videos.index(this);
        if (player.video.index === vindex) {
            player.toggle();
        } else {
            player.play(vindex);
        }
    });
});
				</script>
			{/if}
            
        	</div>
    	</div>	
	{elseif $youtube_video}
		<div id="videoBox">
			<div id="placesVideo">
	        <a href="{$link_uploadVideo}" id="videoUpload">Upload A Video</a>
<h3 id="videoName">Videos - {$name}</h3>
			{$youtube_video}
            </div>
        </div>
	{/if}
	<!-- /video -->

	{if not $photos and not $video and not $youtube_video}
		{include  "place/place_google.tpl"  map=$map nomap=$nomap name=$name loc_name=$loc_name}
	{/if}

	{if $page_type=='item' && $back_url}<div><a class="btn back-to-map" href="{$back_url}">Back to map</a></div>{/if}

		<!-- intro info -->
	<div id="intro"> {if $account_level >= 2||$can_edit_place}
	  <div class="linkbutton"> <a href="/worker/{$editlink}?id={$id}">edit</a>
	    {if $config_dev_server}
		<a href="/mng/dev_download_image?id={$id}&return_url={$smarty.server.REQUEST_URI}">Download images from Prod</a>
		{/if}
	  </div>
	  {/if}
	  
	  {if $isowner}
	  <div class="linkbutton"> <a href="/dir/owner?id={$id}" title="">Edit My Business</a> <a href="/account/payments" title="">Billing</a> </div>
	  {/if}
	  <h1 class="name">{$name}</h1>

	{if $overall}
		<div id="overallRate">
			<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" class="rateOverall">
				<div class="rateit bigstars" data-rateit-value="{$overall}" data-rateit-ispreset="true" data-rateit-readonly="true"  data-rateit-starwidth="32" data-rateit-starheight="32"></div>
				<meta itemprop="ratingValue" content="{$overall}" />
			</div>
		</div>
	{/if} 
	  
	<div class="address">{$address}</div>

	{if $c0}
		{$c0}
	{else}
		<div id="stats">
			{foreach from=$c1 key=key item=item}
				<div>
					<label>{$item.label}</label>{$item.val}
				</div>
			{/foreach}
	{/if}

    {if $currency_button}{$currency_button}{/if}
	
	<div class="intro_actions bottom">
		{if $business_link}
			<div id="yourBiz"><a href="{$config_site_url}/businessowner/pick?id={$id}" title="adult search - business owners">Your Business?</a></div>
		{/if}
		<div id="errorReport"><a href="{$state_link}/contact?where={php}echo rawurlencode($_SERVER["REQUEST_URI"]);{/php}" rel="nofollow">Report An Error</a></div>
	</div>

 
	</div>

	<!-- /intro info -->
 	</div>
	<br style="clear: both;" />

	{include "place/place_hours.tpl" hours=$hours current_weekday=$current_weekday}

	<div id="summary">
		{if $description}
			{$description}
		{/if}
		{if $info}
			{$info}
		{/if}
		{if $facilities}
			{$facilities}
		{/if}
	</div>

<!-- /top content -->
</div>

	<!-- lower content -->
	<div id="lowerMain"{if $video or $youtube_video} style="clear:both;"{/if}{if not $video and not $youtube_video} class="width"{/if}>
  
		{if $video or $youtube_video}
			<div id="lowerRight"> 
				{include "place/place_google.tpl" map=$map nomap=$nomap name=$name loc_name=$loc_name} 
				{include "place/place_forums.tpl" forum_topic=$forum_topic name=$name new_topic_link=$new_topic_link}
				<div id="ppc">{include file='page_ads.tpl'}</div>
			</div>
		{/if}
  
		{if $smarty.session.account_level > 2 && $smarty.session.templates}
			{include "place/reviews.tpl.jay"}
		{else}
			{include "place/reviews.tpl"}
		{/if}

</div>
<!-- /lower content -->
</div>

{literal}
<script type="text/javascript">
//<![CDATA[
jQuery("a.lightbox").fancybox({'transitionIn':'elastic','transitionOut':'elastic','speedIn':600,'speedOut':200,'overlayShow':false,'cyclic':true,'titlePosition': 'over','titleFormat':function(title, currentArray, currentIndex, currentOpts) {return '<span id="fancybox-title-over">Image ' +  (currentIndex + 1) + ' / ' + currentArray.length + '</span>';}});

jQuery("a.uprofile").fancybox();

jQuery("textarea").focus(function() {
	jQuery(this).attr("rows",5);
	jQuery(this).next().show(); 
}); 
jQuery("textarea").blur(function() {
	if (jQuery(this).val() == "") { 
		jQuery(this).attr("rows",1); 
		jQuery(this).next().hide(); 
	}
}); 

function commentreview(i) {
	var id = $("#form"+i+" input[name=review_id]").val();
	var c = $("#form"+i+" textarea[name=c]").val();
	$.post('http://{/literal}{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}{literal}', { 'id': {/literal}{$id}{literal}, 'reviewcomment': 1, 'comment': 1, 'review_id': id, 'c': c}).done(function(data) {
		if (data.length == 0)
			return;
		var cs = $("#revc"+i);
		if (cs.length > 0)
			cs.append(data);
		else
			$("#"+i).before('<div id="revc'+i+'" class="comments"><div class="commentsTitle"><a name="comment'+i+'">Member Comments</a></div>'+data+'</div>');
	});
	$("#form"+i+" > textarea").val($("#form"+i+" > textarea").attr("placeholder"));
}
//]]>
</script>
{/literal}

