
	<!--  if video, print video here -->
	{if $video}
		<div id="videoBox">
			<div id="placesVideo">
	        <a href="{$link_uploadVideo}" id="videoUpload">Upload A Video</a>
<h3 id="videoName"><!-- {$video_count} -->{$name}</h3>
			{if $video_pl}
			<div class="playlist">
				<a class="prev browse left"></a>
				<div id="scrollable" class="scrollable vertical">
			        <div class="items">
			        {section name=vp loop=$video_pl}
			        {assign var=video_pl_group value=$video_pl[vp]}
		                <div>
			            {section name=vpi loop=$video_pl_group}
			            {assign var=video_pl_item value=$video_pl_group[vpi]}
                        <a href="{$video_pl_item.url}" title="{$video_pl_item.name}" data-video-id="{$video_pl_item.file_id}" data-video-name="{$video_pl_item.name}"><img src="{$video_pl_item.thumb}" alt="" /></a>
		    	        {/section}
        	    	    </div>
			        {/section}
			        </div>
				</div>
				<a class="next browse right"></a>
			</div>
			{/if}
        
    	    <a href="{$video.url}" id="flow_vid_place" title="{$video.name}" data-video-file-id="{$video.file_id}">
			        <img src="{$video.thumb}" class="vid_thumb_img">
			        <img src="/images/city_play_50.png" class="vid_play_img">
	    	</a>
			{$flowplayer_jscode}

			{if $video_pl}
				<script language="JavaScript">
				flowplayer("flow_vid_place").playlist("div.items");
				$("#scrollable").scrollable({ vertical: true, mousewheel: true });
				</script>
			{/if}
            
        	</div>
    	</div>	
	{elseif $youtube_video}
		<div id="videoBox">
			<div id="placesVideo">
	        <a href="{$link_uploadVideo}" id="videoUpload">Upload A Video</a>
<h3 id="videoName">Videos - {$name}</h3>
			{$youtube_video}
            </div>
        </div>
	{/if}
	<!-- /video -->

