<div id="reviewBlock" data-jay="1">
	<div class="actions">
		{if $link_uploadVideo}<div id="videoUpload"><a href="{$link_uploadVideo}" title="Upload a Video">&nbsp;</a></div>{/if}
		<div id="photoUpload"><a rel="nofollow" href="/upload?ref={$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" title="Upload a Photo" id="addphoto">&nbsp;</a></div>
		<div id="writeReview"><a href="{if $review_link_add} {$review_link_add} {else} review-add?id={$id} {/if}" title="Write a Review" rel="nofollow">&nbsp;</a></div>
	</div>

	<div class="title">{$totalreview} AdultSearch User Review{if $totalreview!=1}s{/if}</div>

	{if $dirty_reviews_link}<a href="{$dirty_reviews_link}" class="emp" target="_new"></a>{/if}

	{section name=q loop=$quickread}
	{assign var=q value=$quickread[q]}
		<!-- single review -->
		<a name="review{$q.review_id}"></a>
		<div id="rev{$q.review_id}" class="reviewIndividual">
		<!-- hidden SEO -->
		<span class="rating hidden">{$q.star}</span> <span class="item hidden"> <span class="fn">{$name}</span> </span>
		<!-- account level 2 mgmt -->
		<div class="linkbutton">
			{if $ismember}
				<a href="/pm/compose?recipient={$q.account_id}">Send PM</a>
			{/if}
			{if permission::has("edit_all_places") || $q.account_id == $smarty.session.account}
				<a href="{$q.link_edit}">Edit</a> 
				<a href="{$q.link_remove}" onclick="return confirm('sure?');">Remove</a> 
				<a href="{$q.link_makeforum}">Move to Forum</a> 
				{*<a href="{$id}/review/{$q.review_id}/uploadVideo">Upload video</a>*} 
			{/if}
		</div>
		<!-- review intro -->
		<span class="thumb"><img src="{if $q.avatar}{$config_image_server}/avatar/{$q.avatar}{else}/images/icons/User_1.jpg{/if}" alt="" width="40" height="40" align="left" /></span>

		<div class="name">
			{if $q.pfilename}
			<div id="carousel">Provider: {$q.provider_name}
				<div id="slides">
					<ul>
					{section name=s9 loop=$q.pfilename max=1}{assign var=p value=$q.pfilename[s9]}
						<li><a href="{$config_image_server}/rprovider/{$p}" class="lightbox"><img src="{$config_image_server}/rprovider/t/{$p}" /></a></li>
					{/section}
					</ul>
				</div>
			</div>
			{/if}

			{if $smarty.session.account==3}<a href="/user/{$q.username}" class="uprofile">{$q.username}</a>{else}{$q.username}{/if}

			<div class="ratings">
				<div class="rateit" data-rateit-value="{$q.star}" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-max="5"></div>
			</div>
			<div class="date">Reviewed {$q.reviewdate} </div>
		</div>

		<!-- review content -->
		{if $q.provider_name}<b>{if $provider_name_question}{$provider_name_question}{else}Provider Name{/if}:</b> {$q.provider_name}<br/>{/if}
		{$q.review}

		<!-- IP address -->
		{if $q.ips|@count>1}
			<div id="addressIP"> Users {section name=i loop=$q.ips}{assign var=ip value=$q.ips[i]}<span>{$ip}</span>{if !$smarty.section.i.last}, {/if}{/section} that reviewed this location were using the same IP addresses. </div>
		{/if}

		{if $q.comment}

		<div id="revc{$q.review_id}" class="comments">
			<div class="commentsTitle"><a name="comment{$q.review_id}"></a> Member Comments </div>
			{section name=q2 loop=$q.comment}
			{assign var=q2 value=$q.comment[q2]}
				<div class="commentSingle" id="co{$q2.comment_id}"{if $smarty.section.q2.index>3} style="display:none"{/if}>
					<img src="{if $q2.avatar}{$config_image_server}/avatar/{$q2.avatar}{else}/images/icons/User_1.jpg{/if}" alt="{$q2.username}">
					<span class="commentDate">{$q2.date}
						{if $isadmin || $q2.owner} | <span id="co{$q2.comment_id}x"> <a href="" onclick="_ajaxsc('get', '', 'id={$id}&commentremove={$q2.comment_id}'); return false;">Delete</a> </span> {/if}
					</span>
					<span class="userName">{$q2.username}</span>
					<div class="userComment">{$q2.comment}</div>
				</div>

				{if $smarty.section.q2.index == 3&& $smarty.section.q2.total>4}
					{assign var=left value=$smarty.section.q2.total-4}
					<div id="moreComments"> <a href="#" onclick="$('#revc{$q.review_id} > div[id^=co]').show();$(this).parent().hide();return false;">Display {$left} Additional Comment{if $left>1}s{/if}</a> </div>
				{/if}

			{/section}
		</div>
		{/if}

		<!-- submit comment -->
		{if !$smarty.session.account}
			<br /><br />
			<a href="/account/signin?origin={php}echo rawurlencode($_SERVER["REQUEST_URI"]);{/php}">Please login to add comments to this review</a>
		{else}
			<div id="{$q.review_id}">
				<form class="form" method="post" action="" id="form{$q.review_id}">
					<input type="hidden" name="comment" value="1" />
					<input value="{$q.review_id}" type="hidden" name="review_id" />
					<textarea rows="1" cols="50" name="c" style="overflow:hidden;" placeholder="Add a comment or question..."></textarea>
					<input type="button" value="Submit" style="display:none" onclick="commentreview('{$q.review_id}')" />
				</form>
			</div>
		{/if}
		</div>
	{/section}

{if $review_pager}<div id="page">Page: {$review_pager}</div>{/if}

</div>
