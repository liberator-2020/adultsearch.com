<?xml version="1.0" encoding="ISO-8859-1" ?> 
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/">
<channel>
<title>AdultSearch.com | {$places} | {$location_label}</title> 
<webMaster>noreply@adultsearch.com</webMaster>
<link><![CDATA[{$link}]]></link>
<description>{$description}</description>
<language>en-us</language>
<lastBuildDate>{$built}</lastBuildDate>
<copyright>International Spirit LTD.</copyright>
<ttl>86400</ttl>
{section name=i loop=$items}
{assign var=item value=$items[i]}
      <item>  
        <title><![CDATA[{$item.title}]]></title>
        <pubDate>{$item.date} -0800</pubDate>
        <link><![CDATA[{$item.link}]]></link>
        <guid>{$item.id}</guid>   
        <description><![CDATA[{$item.content}]]></description>
        <comment>{$item.thumbnail}</comment>
      </item>
{/section}
</channel>
</rss>

