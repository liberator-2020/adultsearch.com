<script src="/js/tools/rateit/jquery.rateit.min.js" type="text/javascript"></script>
<link href="/js/tools/rateit//rateit.css" rel="stylesheet" type="text/css">

<div id="cl">
 <form id="review" name="review" method="post" action="" enctype="multipart/form-data">
 {if $edit}<input type="hidden" name="edit" value="{$edit}" />{/if}
 <input type="hidden" name="adding" value="1" />
 <input type="hidden" name="id" value="{$id}" />
<table border="0" cellspacing="0" cellpadding="0" class="b">
{if $error}<tr><td colspan="2" class="error"><div class="stitle" style="text-align:center">{$error}</div></td></tr>{/if}

<tr>
  <td colspan="2">
        <div class="stitle" style="text-align:center">You are about to leave a review for {$name}</div>
  </td>
</tr>

{if $forummove}
<tr> 
 <td colspan="2">
<div class="col1">Where would you like to move ?</div>
<div class="col2"><span><select name="forum_id"><option value="">-- select --</option>
<option value="1">General Talk</option>
<option value="2">Strip Clubs &amp; Strippers</option>
<option value="3">Massage Parlors &amp; Masseuses</option>
<option value="4">Sex Shops</option>
<option value="5">LifeStyle Clubs</option>
<option value="6">Escorts</option>
</select></span></div>
<div class="col1">What would be the topic title ?</div>
<div class="col2"><input type="text" name="topic_title" size="75" value="{$topic_title}" /></div>
 </td>
</tr>
{elseif $move}
<tr> 
 <td colspan="2">
<div class="col1">Where would you like to move ?</div>
<div class="col2"><span><select name="new_place_id">{$new_place_list}</select></span></div>
 </td>
</tr>
{else}

{if $register}
<tr><td colspan="2">
 <fieldset>
 <legend>Quick Register to post a review:</legend> 
 <table>

<tr>
  <td class="first">Your E-Mail Address:</td>
  <td class="second"><input type="text" name="email" size="40" value="{$email}" /></td>
</tr>

<tr>
  <td class="first">Create a UserName:</td>
  <td class="second"><input type="text" name="username" size="40" value="{$username}" /></td>
</tr>

<tr>
  <td class="first">Create a Password:</td>
  <td class="second"><input type="password" name="password" size="40" value="{$password}" />
    <span class="smallr">If you have a membership, type your email address &amp; password to login.</span>
  </td>
</tr>

 </table>
 </fieldset>
  </td>
</tr>
{/if}
 
{* <tr><td colspan="2"><hr size="1" /></td></tr> *}

{if $provider}
<tr>
  <td class="first">{$provider_name_question}:</td>
  <td class="second"><select name="provider_id" id="provider_id" onchange="providername();">{$provider_names}</select></td>
</tr>

<tr id="provider_name_manual" class="provider_new_only"{if $provider_id!=-1} style="display:none"{/if}>
  <td class="first">Please type {$provider_name_question}:</td>
  <td class="second"><input type="text" name="provider_name" value="{$provider_name}" /></td>
</tr>

<tr class="provider_new_only"{if $provider_id!=-1} style="display:none;"{/if}>
  <td class="first">Picture(s) of the provider (optional):</td>
  <td class="second">
	<input type="file" name="provider_pic1" onclick="showPicInput('provider',2);" />
	<input type="file" name="provider_pic2" style="display: none;" onclick="showPicInput('provider',3);" />
	<input type="file" name="provider_pic3" style="display: none;" onclick="showPicInput('provider',4);" />
	<input type="file" name="provider_pic4" style="display: none;" />
 </td>
</tr>

<tr class="provider_only"{if $provider_id<=0} style="display:none"{/if}>
  <td class="first">Would you recommend this provider?:</td>
  <td class="second"><select name="recommendp">{$recommendp}</select></td>
</tr>

{/if}

<tr>
  <td class="first">Rate this {if $reviewwhat}{$reviewwhat}{else}place{/if}:</td>
  <td class="second"><input type="hidden" name="star" value="{$star}" id="star" /><div class="rateit bigstars" id="rateit10b" data-rateit-step="1" 
data-rateit-starwidth="32" 
data-rateit-starheight="32" data-rateit-backingfld="#star" 
data-rateit-resetable="false" data-rateit-ispreset="true"></div></td>
</tr>

{*
{if $smarty.session.account_level>2}
<tr>
  <td class="first">Review picture(s) (optional):</td>
  <td class="second">
	<input type="file" name="review_pic1" onclick="showPicInput('review',2);" />
	<input type="file" name="review_pic2" style="display: none;" onclick="showPicInput('review',3);" />
	<input type="file" name="review_pic3" style="display: none;" onclick="showPicInput('review',4);" />
	<input type="file" name="review_pic4" style="display: none;" />
 </td>
</tr>
{/if}
*}

<tr>
  <td class="first">Review for {$name}:</td>
  <td class="second"><textarea name="comment" rows="7" cols="55" {if $blocked}disabled="disabled"{/if}>{$comment}</textarea></td>
</tr>

{/if}

{if $spam_protection||$spam}
{if !$captcha_ok}
<tr>
  <td class="first">Spam Protection:</td>
  <td class="second">Type the characters you see in the picture below.<br/>
		<img src="/captcha.php?{$time}" border="0" alt="security" width='150' height='70'/><br />
		<input name="spam" class="searchinput" size="30" ></td>
</tr>
{else}
<input type="hidden" name="spam" value="{$spam}" />
{/if}
{/if}

{if $blocked}
<tr><td colspan="2">
<p>You may not leave review for this place. If you think there is a mistake, please <a href="/contact{php}echo"?where=".rawurlencode($_SERVER["REQUEST_URI"]);{/php}">contact us</a> right away.</p>
</td></tr>
{else}
<tr><td colspan="2"><div class="stitle" style="text-align:center">
	<input type="image" src="/images/But_SendSm_Org.gif" id="reviewsubmit" />
	<!--<a href="{$link}?id={$id}" id="reviewgoback"><img border="0" src="/images/But_Go-BackSm_Blu.gif" width="77" height="18" alt="" /></a>-->
	<a href="{$link}" id="reviewgoback"><img border="0" src="/images/But_Go-BackSm_Blu.gif" width="77" height="18" alt="" /></a>
  </div>
 </td>
</tr>
 {/if}

</table>
</form>
</div>

{if $provider}
<script type="text/javascript">
<!--
function providername() {
	if( $('#provider_id').val() == 0 ) {
		$(".provider_only").hide();
		$(".provider_new_only").hide();
	} else if( $('#provider_id').val() == -1 ) {
		$(".provider_only").show();
		$(".provider_new_only").show();
	} else {
		$(".provider_only").show();
		$(".provider_new_only").hide();
	}
}

function showPicInput(ident, num) {
	$("input[name='"+ident+"_pic"+num+"']").show();
}

{if $popup && $smarty.session.account_level>1}
$(document).ready(function() {
	$("#reviewgoback").click(function(event){ 
		$("#example").dialog("close");
		event.preventDefault();
	});

	$("#reviewsubmit").click(function(event){
		var url = $("form#review").serialize() + "&popup=1";

	        $.ajax({
	                type: 'POST', url: '{$smarty.get.url}', data: url,
	                success: function(msg){
	                        jQuery('#result').html(msg);
	                        if( msg.indexOf("Success") >= 0 ) {
				        $.ajax({
				                type: 'get', url: '/mng/review', data: 'out={$smarty.get.edit}',
				                success: function(msg){
			                	        jQuery('#result').html(msg);
			                	        if( msg.indexOf("1") < 0 ) alert(msg);
			                	        else {
								$("#example").dialog("close");
								$("#d{$smarty.get.edit}").remove();
							}
			        	        }
				        });
				} else alert(msg);
	                }
	        });
		event.preventDefault();
	});
});
{/if}

-->
</script>
{/if}
