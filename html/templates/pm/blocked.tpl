<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/account/account.css?20190430">

<link href="/css/pm.css?20190426" rel="stylesheet" type="text/css" />
<link href="/css/fa/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<div class="container account">

    {include "account/menu.tpl" ebiz_login_url=$ebiz_login_url ebiz_available=$ebiz_available}

    <div id="my_main" class="wrap">

		<div id="my_sidecol">
			<a href="/pm/">Inbox{if $unread_messages > 0} <span style="color: coral;">({$unread_messages})</span>{/if}</a>
			<a href="/pm/sent">Sent</a>
			<a href="/pm/blocked">Blocked Users</a>
		</div>
		<div id="my_maincol">
			<h1>Blocked Users</h1>
			<table class="msg_list">
			<thead>
			<tr>
				<th>Username</th>
				<th>Since</th>
				<th></th>
			</tr>
			</thead>
			</body>
			{if $blocked|count == 0}
				<tr><td colspan="3">No blocked users</td></tr>
			{else}
				{foreach from=$blocked item=b}
					<tr class="tr_msg">
						<td>{$b.username}</td>
						<td>{$b.stamp|date_format:"m/d/Y g:i A"}</td>
						<td><a class="pm_btn_small pm_btn_white" href="/pm/unblock?id={$b.id}"><i class="fa fa-ban" aria-hidden="true"></i> Unblock user {$b.username}</a></td>
					</tr>
				{/foreach}
			{/if}
			</tbody>
			</table>
		</div>
		<br style="clear: both;" />
	</div>
</div>
