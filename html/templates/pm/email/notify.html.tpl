<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>You have new private message</title>
</head>
<body>
<div style="display: block; width: 100% !important; max-width: 640px !important; margin: 0 auto; border: solid 1px #5EABE5;">
	<table width="100%" align="center" cellpadding="5" cellspacing="0" style="font-family: Verdana, Geneva, sans-serif; font-size: 12px;">
	<tr>
		<td colspan="2" bgcolor="#1966a0"><img src="cid:logo_email" width="289" height="86" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<br />
			Hello {$recipient},<br /><br />
			You have new private message from user {$author}, you can <a href="{$read_link}">read it by clicking on this link</a>.
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding: 30px;">
			Sincerely,<br />
			<a href="mailto:support@adultsearch.com">support@adultsearch.com</a><br />
			<a href="http://adultsearch.com">AdultSearch.com</a>
		</td>
	</tr>
	</table>
</div>
</body>
</html>
