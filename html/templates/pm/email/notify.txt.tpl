Hello {$recipient},

You have new private message from user {$author}, you can read it by clicking on following link:
{$read_link}

Sincerely,
support@adultsearch.com
