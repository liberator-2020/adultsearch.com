<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/account/account.css?20190430">

<link href="/css/pm.css?20190426" rel="stylesheet" type="text/css" />

<div class="container account">

    {include "account/menu.tpl" ebiz_login_url=$ebiz_login_url ebiz_available=$ebiz_available}

    <div id="my_main" class="wrap">

		<div id="my_sidecol">
			<a href="/pm/">Inbox{if $unread > 0} <span style="color: coral;">({$unread})</span>{/if}</a>
			<a href="/pm/sent">Sent</a>
			<a href="/pm/blocked">Blocked Users</a>
		</div>
		<div id="my_maincol">
			<h1>Inbox</h1>
			<table class="msg_list">
			<thead>
			<tr>
				<th>Date/Time</th>
				<th>Sender</th>
				<th>Message</th>
			</tr>
			</thead>
			</body>
			{if $messages|count == 0}
				<tr><td colspan="3">No messages</td></tr>
			{else}
				{foreach from=$messages item=message}
					<tr class="tr_msg {if !$message.read_stamp}already_read{/if}">
						<td><a href="{$message.read_link}">{$message.sent_stamp|date_format:"m/d/Y g:i A"}</a></td>
						<td><a href="{$message.read_link}">{$message.author}</a></td>
						<td><a href="{$message.read_link}">{$message.subject}</a></td>
					</tr>
				{/foreach}
			{/if}
			</tbody>
			</table>
		</div>
		<br style="clear: both;" />
	</div>
</div>
