<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/css/account/account.css?20190430">

<link href="/css/pm.css?20190426" rel="stylesheet" type="text/css" />
<link href="/css/fa/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<div class="container account">

    {include "account/menu.tpl" ebiz_login_url=$ebiz_login_url ebiz_available=$ebiz_available}

    <div id="my_main" class="wrap">

		<div id="my_sidecol">
			<a href="/pm/">Inbox{if $unread > 0} <span style="color: coral;">({$unread})</span>{/if}</a>
			<a href="/pm/sent">Sent</a>
			<a href="/pm/blocked">Blocked Users</a>
		</div>
		<div id="my_maincol">
			{if $error}
				<div style="color: red; font-weeight: bold;">{$error}</div>
			{else}
				<div id="my_view">
					{if $message.author == 'me'}
						<h1>Message sent to {$message.recipient} on {$message.sent_stamp|date_format:"m/d/Y g:i A"}</h1>
					{else if $message.recipient == 'me'}
						<h1>Message received from {$message.author} on {$message.sent_stamp|date_format:"m/d/Y g:i A"}</h1>
					{else}
						<h1>Message from {$message.author} to {$message.recipient} sent on {$message.sent_stamp|date_format:"m/d/Y g:i A"}</h1>
					{/if}
					{if $message.subject}
						<div>
							<label>Subject:</label> {$message.subject}<br />
						</div>
					{/if}
					<div id="msg_content">
						{$message.content}
					</div>

					<div>
						{foreach from=$message.images item=url}
							<img src="{$url}" style="max-width: 200px; max-height: 200px; margin-top: 10px;" /><br />
						{/foreach}
					</div>

					<a class="pm_btn pm_btn_coral" href="/pm/compose?recipient={$message.author_id}&reply={$message.id}"><i class="fa fa-reply" aria-hidden="true"></i> Reply</a>
					<a class="pm_btn pm_btn_white" href="/pm/delete?id={$message.id}"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
					{if $message.author == 'me'}
						<a class="pm_btn pm_btn_white" href="/pm/block?id={$message.recipient_id}"><i class="fa fa-ban" aria-hidden="true"></i> Block user {$message.recipient}</a>
					{else if $message.recipient == 'me'}
						<a class="pm_btn pm_btn_white" href="/pm/block?id={$message.author_id}"><i class="fa fa-ban" aria-hidden="true"></i> Block user {$message.author}</a>
					{/if}
				</div>
			{/if}
		</div>
		<br style="clear: both;" />
	</div>
</div>

{literal}
<script type="text/javascript">
jQuery(function() {
});
</script>
{/literal}
