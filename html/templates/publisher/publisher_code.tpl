<a href="/publisher/">&lt;&lt; Back to Publisher Home</a>
<h1>Get Ad Zone HTML</h1>
<table width="100%">
<tr>
	<td width="60%" valign="top">
	<table width="100%" cellpadding="4">
	<tr>
		<td class="lbl">Zone Name:</td>
		<td>{$name}</td>
	</tr>
{if !$popup}
	<tr>
		<td class="lbl">Format:</td>
		<td>
			<select name="ad_format" id="ad_format" onchange="refreshCode();">
				<option value="160x600">(160x600) Vertical</option>
				<option value="120x600">(120x600) Small Vertical</option>
				<option value="180x400">(180x400) Wide Vertical</option>
				<option value="468x60">(468x60) Banner</option>
				<option value="728x90">(728x90) Large Banner</option>
				<option value="900x90">(900x90) XLarge Banner</option>
				<option value="300x250">(300x250) Medium Rectangle</option>
			</select>
			&nbsp;&nbsp;&nbsp;&nbsp;<a href="{$config_site_url}/publisher/format" style="color:blue;" target="adformats">View available formats</a>
		</td>
	</tr>
	<tr>
		<td class="lbl">Customize Colors:</td>
		<td>
			<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top">
					Click the colored square to select a different color.
					<br /><br /><br />
					<table width="100%" cellspacing="0" cellpadding="3">
					<tr>
						<td>Background Color:</td>
						<td>
							<input type="color" name="txtBg" id="txtBg" value="#FFFFFF" style="width:150px;" maxlength="7" class="colorpicker" />
						</td>
					</tr>
					<tr>
						<td>Border Color:</td>
						<td>
							<input type="text" name="txtBorder" id="txtBorder" value="#666666" style="width:150px;" maxlength="7" class="colorpicker" />
						</td>
					</tr>
					<tr>
						<td>Link Color:</td>
						<td>
							<input type="text" name="txtLink" id="txtLink" value="#0000FF" style="width:150px;" maxlength="7" class="colorpicker" />
						</td>
					</tr>
					<tr>
						<td>Text Color:</td>
						<td>
							<input type="text" name="txtText" id="txtText" value="#000000" style="width:150px;" maxlength="7" class="colorpicker" />
						</td>
					</tr>
					<tr>
						<td>URL:</td>
						<td>
							<input type="text" name="txtURL" id="txtURL" value="#0E774A" style="width:150px;" maxlength="7" class="colorpicker" />
						</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>		  
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="100%" cellpadding="2" cellspacing="0">
			<tr>
				<td width="550" valign="top">
					<div style="width:100%;">
					<b class="em2">Copy the code below and paste it into the relevant page on your website.</b><br />
					<textarea name="code" id="code" rows="1" cols="1" style="width:590px;height:240px;font-size:12px;" wrap="off" onclick="this.select()">Code goes here</textarea>
					<br />
					<span class="ex">Click anywhere in the box to select all code.</span><br />
					</div>
				</td>
				<td align="center" valign="top">
					<br />
				</td>
			</tr>
			</table>
		</td>
	</tr>
{else}
	<tr>
		<td colspan="2">
			<b class="em2">Copy the code below and paste it into the relevant page on your website.</b><br />
			<textarea id="popup_code" rows="1" cols="1" style="width:590px;height:240px;font-size:12px;" wrap="off" onclick="this.select()">{$popup_code}</textarea>
			<br />
			<span class="ex">Click anywhere in the box to select all code.</span><br />
		</td>
	</tr>
{/if}
	</table>
	</td>
	<td valign="top">
	{if !$popup}
		<div id="previewAd" style="width:150px;padding:5px;border:1px solid; position:relative;">
			<span id="previewLink"><b><u>Advertisement Title</u></b></span><br /><span id="previewText">Ad text appears here and could be longer.</span>
			<br/><span id="previewURL">www.example.com</span>
		</div>
	{/if}
	</td>
</tr>
</table>

{if !$popup}
{literal}
<script type="text/javascript"><!--
function refreshCode() {
	var vldColor = /^[#0-9a-fA-F]{7}$/;
	var code = "", ad_width, ad_height;
	var ad_format = $("#ad_format").val();
	var ad_bg = "#FFFFFF", ad_border = "#666666", ad_link = "#0000FF", ad_text = "#000000", ad_url = "#0E774A";

	if (ad_format == "180x400") {
		ad_width = "180";
		ad_height = "400";
	} else if (ad_format == "300x250") {
		ad_width = "300";
		ad_height = "250";
	} else if (ad_format == "728x90") {
		ad_width = "728";
		ad_height = "90";
	} else if (ad_format == "900x90") {
		ad_width = "900";
		ad_height = "90";
	} else if (ad_format == "468x60") {
		ad_width = "468";
		ad_height = "60";
	} else if (ad_format == "120x600") {
		ad_width = "120";
		ad_height = "600";
	} else {
		ad_width = "160";
		ad_height = "600";
	}

	if ($("#txtBg") && vldColor.test($("#txtBg").val()))
		ad_bg = $("#txtBg").val();
	if ($("#txtBorder") && vldColor.test($("#txtBorder").val()))
		ad_border = $("#txtBorder").val();
	if ($("#txtLink") && vldColor.test($("#txtLink").val()))
		ad_link = $("#txtLink").val();
	if ($("#txtText") && vldColor.test($("#txtText").val()))
		ad_text = $("#txtText").val();
	if ($("#txtURL") && vldColor.test($("#txtURL").val()))
		ad_url = $("#txtURL").val();

	$("#previewAd").css("background-color", ad_bg);
	$("#previewAd").css("border-color", ad_border);
	$("#previewLink").css("color", ad_link);
	$("#previewText").css("color", ad_text);
	$("#previewURL").css("color", ad_url);

	code += '<!-- AdultSearch.com Publisher -->' + '\r\n';
	code += '<scr' + 'ipt type="text/javascript"><!--' + '\r\n';
	code += 'var aspublisher_id = "{/literal}{$id}{literal}";' + '\r\n';
	code += 'var aspublisher_width = "'+ad_width+'";' + '\r\n';	 
	code += 'var aspublisher_height = "'+ad_height+'";' + '\r\n';	   
	code += 'var aspublis_color_bg = "'+ad_bg.substr(1)+'";' + '\r\n';
	code += 'var aspublis_color_border = "'+ad_border.substr(1)+'";' + '\r\n';
	code += 'var aspublis_color_link = "'+ad_link.substr(1)+'";' + '\r\n';
	code += 'var aspublis_color_text = "'+ad_text.substr(1)+'";' + '\r\n';
	code += 'var aspublis_color_url = "'+ad_url.substr(1)+'";' + '\r\n';
		
	code += '//--></scr' + 'ipt>' + '\r\n';
	code += '<scr' + 'ipt type="text/javascript" src="{/literal}{$config_site_url}{literal}/js/publisher/p.js?20180922"></scr' + 'ipt>' + '\r\n';
	code += '<!-- AdultSearch.com Publisher -->' + '\r\n';

	$("#code").val(code);	

	return;
}

$('#txtBg').simpleColorPicker({'ondone': function(){refreshCode();} });
$('#txtBorder').simpleColorPicker({'ondone': function(){refreshCode();} });
$('#txtLink').simpleColorPicker({'ondone': function(){refreshCode();} });
$('#txtText').simpleColorPicker({'ondone': function(){refreshCode();} });
$('#txtURL').simpleColorPicker({'ondone': function(){refreshCode();} });
$('.colorpicker').change(function(){refreshCode();});
refreshCode();	

-->
</script>
{/literal}
{/if}

