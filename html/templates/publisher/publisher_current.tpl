{literal}
<style type="text/css" rel="stylesheet">
<!--
ol, ul, li {border:0 none;margin:0;padding:0;}
ol li{list-style:none outside none;}
li{line-height:1.3;}
li.g, body, html, .std, .c h2, #mbEnd h2, h1 {font-family:arial,sans-serif;font-size:small;}
#mbEnd {margin:0 0 32px 8px;font-size:12px}
#mbEnd li {margin:12px 8px 0 0;}
cite{font-style:normal;color:#0E774A;}
h3, .med {font-size:13px;font-weight:normal;margin:0;padding:0;}
a:link, a:visited, .w, .q:active, .q:visited, .tbotu {color:#2200C1;cursor:pointer;}
a.as, a.as:visited, a.as:active {color:#767676;font-size:11px}
-->
</style>
{/literal}

{if $error}<div class="error">{$error}</div>{/if}

{if $ads}

<form method="post" action="">
<input type="hidden" name="action" value="1" />
<table width="1000">
 <tr>
  <td>

<ol>
{section name=s1 loop=$ads}{assign var=s value=$ads[s1]}
<fieldset width="1000">
<div style="float:left;">
<fieldset style="height:100px">
<li><h3><a href="#" onclick="return false">{$s.ad_title}</a></h3>
{$s.ad_line1}<br>
{$s.ad_line2}<br>
<cite>{$s.ad_durl}</cite><br>
<cite>{$s.ad_url}</cite>
</li>
</fieldset>
</div>
<div style="float:left;">
<input type="radio" name="action{$s.id}" value="2" ref="{$s.id}" {if $idx==$s.id}checked="checked"{/if}/>Decline this campaign<br/>
<div id="decline{$s.id}" {if $idx!=$s.id}style="display:none;"{/if}>Tell them a reason why ?: <input type="text" name="reason{$s.id}" value="" size="75" maxlength="255" 
/></div>
<div id="button{$s.id}" style="{if $idx!=$s.id}display:none;{/if}text-align:right;"><span class="bsubmit"><button class="bsubmit-r" type="submit" value="Show 
Reports"/><span>Submit</span></button></span></div>
<div>
</fieldset>
{/section}
</ol>

  </td>
 </tr>
</table>
</form>

{literal}
<script type="text/javascript"><!--
$(document).ready(function(){
	$("input[type=radio]").click(function() {processReason($(this).attr("value"), $(this).attr("ref"));});
});

function processReason(v, i) {
	$("#decline"+i).show();
	$("#button"+i).show();
}
--></script>
{/literal}

{else}
<h3>There is no campaign waiting for your approval. <a href="/publisher/">Click here</a> to go back.</h3>
{/if}
