<table width="100%" cellspacing="0" cellpadding="0" class="grid" border="0" style="border-collapse: collapse;">
<tr>
 <td colspan="6"><strong>Your unpaid earnings: ${$income}</strong>
 </td>
</tr>
{if $p}
<tr class="header">
        <td class="tableHeaderLeft">Date</td>
<td class="tableHeaderRight">&nbsp;</td>
<td class="tableHeaderRight">Payment Amount</td>
<td class="tableHeaderRight">To</td>
<td class="tableHeaderRight">Status</td>
<td class="tableHeaderRight">Transaction ID/Tracking #</td>

</tr>
{section name=s1 loop=$p}
{assign var=l value=$p[s1]}
<tr bgcolor="{cycle values="#EEEEEE,#FFFFFF"}">
        <td class="tableContent1Left">{$l.time}</td>
        <td class="tableContent1Left">&nbsp;</td>
        <td class="tableContent1Right">${$l.amount}</td>
        <td class="tableContent1Right">{$l.to}</td>
        <td class="tableContent1Right">{$l.made}</td>
        <td class="tableContent1Right">{$l.track}</td>
</tr>
{/section}
{else}
 <tr>
 <td>There is no payment made to your account.
 </td>
 </tr>
{/if}
</table>

{if $smarty.get.id}
<table class="standard" width="900">
<tr>
 <td colspan="6"><strong>Payout details</strong>
 </td>
</tr>

{if $dis}
<tr>
        <td class="tableHeaderLeft">Sub-User</td>
<td class="tableHeaderRight">Payment Amount</td>
</tr>
{section name=s2 loop=$dis}
{assign var=l value=$dis[s2]}
<tr bgcolor="{cycle values="#EEEEEE,#FFFFFF"}">
        <td class="tableContent1Left">{$l.username}</td>
        <td class="tableContent1Right"><b>${$l.total}</b></td>
</tr>
{/section}
{else}
<tr bgcolor="{cycle values="#EEEEEE,#FFFFFF"}">
        <td class="tableContent1Left" colspan="2">No details found for this payment</td>
</tr>
{/if}
</table>
{/if}
