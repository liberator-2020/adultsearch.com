<div id="cl">

{if isset($message)}
	<span style="display:block; margin: auto; color: red; font-weight: bold;">{$message}</span>
{/if}

<form name="Form0" method="post" enctype="multipart/form-data" action="{$form_action}">
<input type="hidden" name="profile_change" value="1" />
<table class="b">

{if isset($error)}
        <tr><td colspan="2" align="center"><span style="color: red; font-weight: bold;text-align:center;">Error: {$error}</span><br /></td></tr>
{/if}

{if isset($success)}
        <tr><td colspan="2" align="center"><span style="color: black; font-weight: bold;text-align:center;">{$success}</span><br /></td></tr>
{/if}


 <tr>
  <td class="first">
   &nbsp;</td>

  <td class="right_sub" >
   Payout Information</td>
 </tr>

 <tr>
  <td class="first">
        Company Name / Personal Name :<span class="required">*</span>
  </td>
  <td class="second" >
        <input type="text" name="payableto" class="text" size="30" value="{$payableto}" />
  </td>
 </tr>

 <tr>
  <td class="first">
        TAX ID or SSN   :
  </td>
  <td class="second" >
        <input type="text" name="ssn" class="text" size="30" value="{$ssn}" />
	<span class="smallr">(USA ONLY)</span>
  </td>
 </tr>

 <tr>
  <td class="first">
        Address :<span class="required">*</span>
  </td>
  <td class="second" >
        <input type="text" name="billingaddr" class="text" size="60" value="{$billingaddr}" />
  </td>
 </tr>

 <tr>
  <td class="first" valign="top">
   <label>Zipcode: <span class="required">*</span></label>
  </td>
  <td class="second">
        <input type="text" name="zip" value="{$zip}" class="text" size="8" /><br />
	{if $city}<span class="smallr">{$city}, {$state}</span>{/if}
  </td>
 </tr>

<tr>
  <td align="left" style="padding-left:50px;" class="first">
   <label ><input type="radio" name="payout" value="3" {if $payout==3||!$payout}checked="checked"{/if} /> Check</label>
  </td>
   <td class="second">&nbsp;</td>
 </tr>


 <tr>
  <td align="left" style="padding-left:50px;" class="first">
   <label ><input type="radio" name="payout" value="1" {if $payout==1}checked="checked"{/if} /> Paypal</label>
  </td>
   <td class="second">&nbsp;</td>
 </tr>

 <tr>
  <td class="first">
  </td>
  <td class="second" >
        Paypal E-mail: <input type="text" name="paypal" class="text" size="30" value="{$paypal}" />
  </td>
 </tr>

{*
 <tr>
  <td colspan="2" style="padding-left:50px;">
   <label ><input type="radio" name="payout" value="2" {if $payout==2}checked="checked"{/if} /> Bank/Wire Transfer (Must be a US or Canadian bank)</label>
  </td>
 </tr>

 <tr>
  <td class="first">
  </td>
  <td class="second" >
        Bank Name: <input type="text" name="bankname" class="text" size="30" value="{$bankname}" />
  </td>
 </tr>

 <tr>
  <td class="first">
  </td>
  <td class="second" >
        Bank Account Name: <input type="text" name="accname" class="text" size="30" value="{$accname}" />
  </td>
 </tr>

 <tr>
  <td class="first">
  </td>
  <td class="second" >
        Account Number: <input type="text" name="accnumber" class="text" size="30" value="{$accnumber}" />
  </td>
 </tr>

 <tr>
  <td class="first">
  </td>
  <td class="second" >
        Routing Number: <input type="text" name="routenumber" class="text" size="30" value="{$routenumber}" />
  </td>
 </tr>

*}


	{if isset($state_options)}
	<tr class="loc_row">
		<th>State:</th>
		<td class="star"><sup><span style="color: red;">*</span></sup></td>
		<td><select name="State" id="State" >{$state_options}</select></td>
	</tr>
	{/if}

	{if isset($city_options)}
	<tr class="loc_row">
		<th>City:</th>
		<td class="star"><sup><span style="color: red;">*</span></sup></td>
		<td><select name="City" id="City" >{$city_options}</select></td>
	</tr>
	{/if}

 <tr >
  <td class="first">&nbsp;</td>
  <td class="second">
	<span class="bsubmit"><button class="bsubmit-r" type="submit" value="Update"/><span>Update</span></button></span>
	<span class="bsubmit"><button class="bsubmit-r" type="button" value="Go Back" onclick="window.location='/affiliate/';"/><span>Go Back</span></button></span>
  </td>

 </tr>

</table>
</form>
</div>
