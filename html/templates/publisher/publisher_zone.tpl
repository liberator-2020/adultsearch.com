{if $error}<div class="error">{$error}</div>{/if}

<form method="post" action="">
{if $new}<input type="hidden" name="new" value="1" />{/if}
<table class="b" cellspacing="0" cellpadding="0" id="ad_form">

 <tr>
  <td class="first">Main Advertising Area:</td>
  <td class="second">{if $networks}<select name='network' onchange="publisher_type_backup();">{$networks}</select>{else}{$name}{/if}</td>
 </tr>

 <tr>
  <td class="first">Zone Name:</td>
  <td class="second"><input type="text" name="n" value="{$n}" style="width:280px; background-color: #FFFFCC" /> <span class="ex">(ex: All Pages, Home Page, Category 
1)</span></td>
 </tr>

{*
 <tr>
  <td class="first">Zone Description:</td>
  <td class="second"><textarea name="d" style="width:280px" rows="5" />{$d}</textarea>
	</td>
 </tr>
*}

 <tr>
  <td class="first" valign="top">Approval Needed to advertise:</td>
  <td class="second"><input type="checkbox" name="appneeded" value="1" onclick="$('#appemail').toggle();"{if $appneeded} checked="checked"{/if} /> Yes, send me e-mail 
each 
time 
someone 
wants to 
advertise
	<div id="appemail"{if !$appneeded} style="display:none;"{/if}>E-Mail address to send notifications: <input type="text" name="appemail" value="{$appemail}" 
style="width:280px;" /></div>
 </td>
 </tr>

 <tr id="backup_popup" style="display: none;">
  <td class="first" style="width:280px">If no ads currently available for this zone, show this URL:</td>
  <td class="second"><input type="text" name="backupurl" value="{$backupurl}" style="width:280px;" /></td>
 </tr>

 <tr id="backup_text">
  <td class="first" style="width:280px">If no ads currently available for this zone</td>
  <td class="second"><select name="backup" onchange="publisher_backup(this.value);">
	<option value="0">Show advertise here link</option>
	<option value="1"{if $backup==1} selected="selected"{/if}>Show specific HTML code</option>
	<option value="2"{if $backup==2} selected="selected"{/if}>Fill space with a solid color</option></select>
	<div id="backup1"{if $backup!=1} style="display:none;"{/if}><textarea name="backuphtml" style="width:280px;height:101px">{$backuphtml}</textarea></div>
	<div id="backup2"{if $backup!=2} style="display:none;"{/if}><input type="text" name="backupcolor" value="{$backupcolor}" id="backupcolor" style="padding:0" 
/></div>
	</td>
 </tr>

 <tr>
  <td class="first">&nbsp;</td>
  <td class="second">
<span class="bsubmit"><button type="submit" value="Submit" class="bsubmit-r"><span>Submit</span></button></span>
<span class="bsubmit"><button type="button" value="Submit" onclick="window.location='/publisher/';" class="bsubmit-r"><span>Cancel</span></button></span>
 </td>
 </tr>

</table>

</div>
</form>

<script type="text/javascript">
<!--
{literal}
$(document).ready(function(){
	$('#backupcolor').simpleColorPicker();
	return;
});

function publisher_type_backup() {
	var type = $("select[name=network] option:selected").attr("data-zone-type");
	if (type == 'popup') {
		$("#backup_popup").show();
		$("#backup_text").hide();
	} else if (type == 'text') {
		$("#backup_text").show();
		$("#backup_popup").hide();
	}
}

function publisher_backup(v) {
	if( v == 1 ) {
		$("#backup1").show();
		$("#backup2").hide();
	} else if( v == 2 ) {
		$("#backup2").show();
		$("#backup1").hide();
	} else {
		$("#backup1").hide();
		$("#backup2").hide();
	}
}
-->
</script>
{/literal}
