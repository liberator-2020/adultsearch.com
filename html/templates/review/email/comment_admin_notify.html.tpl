<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>New Review Comment</title>
<style type="text/css">
body {
	font-size: 10pt;
}
.cap {
	font-size: 0.9em;
	font-weight: bold;
}
table {
	font-size: 1em;
}
td {
	vertical-align: top;
}
</style>
</head>
<body>
<div>
<strong>Account:</strong> <a href="{$account_mng_link}">#{$account_id} - {$account_email}</a><br />
<br />
<strong>Comment:</strong> {$comment}<br />
<br />
<strong>Comment link:</strong> <a href="{$comment_link}">{$comment_link}</a><br />
</div>
</body>
</html>
