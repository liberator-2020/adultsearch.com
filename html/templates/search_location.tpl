{if $locations}
	<h1>Please select desired location</h1>
	<p>Multiple locations have been found for search query '<strong>{$city}</strong>', please select desired location from list below.</p>
	<ul style="list-style-type: disc;">
	{section name=l loop=$locations}
	{assign var=loc value=$locations[l]}
		<li><a href="{$loc.url}">{$loc.name}</a></li>
	{/section}
	</ul>
{else}
	<h1>No location found</h1>
	<p>We are sorry, but we don't have location '<strong>{$city}</strong>' in our database.</p>
	<p><a href="/">Adultsearch.com homepage</a></p>
{/if}
