<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>

<style type="text/css">
#cke_message1 {
	border: 1px solid #999;
}
</style>

<h1>SEO Edit</h1>

<form action="" method="post">

Location: <strong>{$location_label}</strong><br />
<br />

Type: <strong>{$type_label}</strong><br />
<br />

SEO Description:<br />
<textarea class="bio" id="message1" class="ckeditor" name="description" rows="24" cols="38">{$description}</textarea>
<br />

<input type="hidden" name="type" value="{$type}" />
<input type="hidden" name="place_type_id" value="{$place_type_id}" />
<input type="hidden" name="category" value="{$category}" />
<input type="submit" name="submit" value="Submit" />
{if $id && $smarty.session.account==3974}
&nbsp;&nbsp;&nbsp;<input type="submit" name="submit" value="Remove" />
{/if}
</form>

<script type="text/javascript">
$(document).ready(function(){
}); 

var toolbar1 = [[ 'Bold', 'Italic', 'Strikethrough', '-', 'RemoveFormat' ], ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'], ['Undo', 'Redo'], ['Link', 'Unlink'], [ 'Source' ], [ 'Maximize' ]];
CKEDITOR.replace('message1', { 
    toolbar: toolbar1,
    allowedContent: 'p; br; strong; b; em; i; a[!href];'
});
</script>
