<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Forum New Reply</title>
<style type="text/css">
body {
	font-size: 10pt;
}
.cap {
	font-size: 0.9em;
	font-weight: bold;
}
table {
	font-size: 1em;
}
td {
	vertical-align: top;
}
</style>
</head>
<body>
<div>
<strong>Topic:</strong> <a href="{$topic_link}">{$topic_title}</a><br />
<strong>Username: </strong>{$username}<br />
<strong>Text:</strong><br />
{$post_text}<br />
<br />
<a href="{$delete_post_link}">remove this post</a><br />
</div>
</body>
</html>
