<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Forum New Topic</title>
<style type="text/css">
body {
	font-size: 10pt;
}
.cap {
	font-size: 0.9em;
	font-weight: bold;
}
table {
	font-size: 1em;
}
td {
	vertical-align: top;
}
</style>
</head>
<body>
<div>
<strong>Topic title:</strong> {$title}<br />
<strong>Account:</strong> <a href="{$account_mng_link}">#{$account_id} - {$account_email}</a><br />
<strong>Linked to a place:</strong> {$place_link}<br />
<strong>Message:</strong> {$message}<br />
<a href="{$view_link}">{$view_link}</a><br />
<br />
<a href="{$delete_link}">remove this topic</a><br />
<a href="{$delete_ban_link}">remove this topic and ban author</a><br />
</div>
</body>
</html>
