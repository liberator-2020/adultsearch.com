Topic title: {$title}

Linked to a place: {$place_link}

Account: #{$account_id} - {$account_email} ({$account_mng_link})

Message: {$message}

view topic: {$view_link}

remove this topic: {$delete_link}

remove this topic and ban author: {$delete_ban_link}

