<link href="/css/forums.css" rel="stylesheet" type="text/css" />

<div id="ppc">
	{include file='page_ads.tpl' category='forum'}
	{include file='page_ads_2.tpl'}
</div>

<div id="forum" class="container">

	<div class="forumheader">
		<a class="bigbutton"href="{if $topic_new_link}{$topic_new_link}{else}forum-new-topic?forum_id={$forum_id}{/if}" rel="nofollow"> <span class="iComments"></span> start a discussion </a>
		<h1>{$forum_title}</h1>
		<h2>{$forum_posts} Posts In This Forum</h2>
	</div>

	<div class="pagination">
		<div class="searchForum">
			<form action="{if $forum_search_url}{$forum_search_url}{else}{$core_loc_link}forum/forum_search{/if}" method="get">
				<input type="text" name="search" value="" placeholder="Search Forums" />
				<input type="submit" value="Go" class="go" />
			</form>
		</div>
		{include "sex-forum/pager.tpl" pager=$pager}
	</div>

	<table  border="0" cellspacing="1" cellpadding="8" id="category">
    <tr>
		<td class="title col1">Topic/Author</td>
		<td class="title col2">Replies</td>
		<td class="title col3">Last Post</td>
    </tr>

	{if $topics} 
		{section name=mainforum loop=$topics}
		{assign var=t value=$topics[mainforum]}
		<tr>
			<td class="col1">
				<div class="topic"><a href="{if $t.topic_url}{$t.topic_url}{else}viewtopic?topic_id={$t.topic_id}{/if}" class="normal">{$t.topic_title}</a></div>
				Started by {$t.userlink}
				{if $t.important} {/if}
				{if $t.moved}{/if}
				{if $t.new}{/if}
				{$t.paging} {$t.extra}
			</td>
			<td class="col2"><div class="number">{$t.topic_replies}</div> posts</td>
			<td class="col3">{$t.lastPostDate|date_format:'%B %e, %Y %l:%M%p'}<div class="lastpost">{$t.lastPostUser}</div></td>
		</tr>
		{/section}
	{else}
		<tr><td class="col1" colspan="3">
			No topic found in this section. <a href="{if $topic_new_link}{$topic_new_link}{else}forum-new-topic?forum_id={$forum_id}{/if}" title="" rel="nofollow">Click here</a> to create the first forum topic now.</td></tr>
	{/if}
    
	</table>

	<div class="pagination">
    	<div class="searchForum">
			<form action="{if $forum_search_url}{$forum_search_url}{else}{$core_loc_link}forum/forum_search{/if}" method="get">
				<input type="text" name="search" value="Search Forums" onfocus="javascript:if (this.value == 'Search Forums') this.value='';" onblur="javascript:if (this.value == '') this.value = 'Search Forums';" />
				<input type="submit" value="Go" class="go" />
			</form>
		</div>
		{include "sex-forum/pager.tpl" pager=$pager}
	</div>

<!-- subcategory page -->
{if $states}
<table cellpadding="5" cellspacing="1" width="1000">
	<tr style="font-color: #1975D7; font-weight: bold; background-color: #006699;">
		<td height="28" colspan="5">{if $stitle}{$stitle}{else}Select a Sub Category{/if}</td>
	</tr>
	<tr bgcolor="#dee3e7">
		{section name=forum loop=$states}
		{assign var=tt value=$states[forum]}
		<td valign="top" height="28">
			<table cellspacing="5" cellpadding="5">
				{section name=forum2 loop=$tt}
				{assign var=t value=$tt[forum2]}
				<tr>
					<td>
						<a href="{if $t.forum_link}{$t.forum_link}{else}viewforum?forum_id={$t.forum_id}{/if}" title="">{$t.forum_name} ({$t.forum_posts})</a> {$t.paging} 
						{if $t.new} <font style="color:red;font-weight:bold;font-size:9px;">*new post*</font>{/if}
					</td>
				</tr>
				{if !$smarty.section.forum2.last}<tr>{/if}
				{/section}
				{if $smarty.section.forum.last}
					{section name=miss loop=$missing1}
					<tr>
						<td class="forum-state"></td>
					</tr>
					{/section}
				{/if}
			</table>
		</td>
		{/section}
	</tr>
</table>
{/if}
<!-- end subcategory page -->

</div>
