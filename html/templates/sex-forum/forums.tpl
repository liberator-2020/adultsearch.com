<link href="/css/forums.css" rel="stylesheet" type="text/css" />

<div id="ppc">
	{include file='page_ads.tpl' category='forum'}
	{include file='page_ads_2.tpl'}
</div>

<div id="forum" class="container">
	<div class="forumheader"> 
		<a class="bigbutton"href="{$forum_new_topic_url}" rel="nofollow"><span class="icon-comments"></span> start a discussion </a>
		<h1>AdultSearch Forums</h1>
		<h2>Dish on the Hottest, Naughtiest, Best & Worst of {$location_name}</h2>
	</div>

	<table border="0" cellspacing="1" cellpadding="8" id="drilldown">
		{section name=f loop=$forums}{assign var=f value=$forums[f]}
			<tr>
				<td class="col1">
					{if $f.subforums}
						<div class="count">
						{section name=s loop=$f.subforums}{assign var=s value=$f.subforums[s]}
							{$s.forum_posts} Posts<br />
						{/section}
						</div>
						{section name=s loop=$f.subforums}{assign var=s value=$f.subforums[s]}
							<a href="{$s.forum_url}" title="{$s.forum_name}">{$s.forum_name}</a><br />
						{/section}
					{else}
						<div class="count">{$f.forum_posts} Posts</div>
						<a href="{$f.forum_url}" title="{$f.forum_name}">{$f.forum_name}</a>
					{/if}
				</td>
			</tr>
			{*{$f.forum_desc}*}
		{/section}
	</table>
	<div class="pagination">
		<div class="searchForum">
			<form action="{$forum_search_url}" method="get">
				<input type="text" name="search" placeholder="Search" value="" />
				<input type="submit" value="Go" class="go" />
			</form>
		</div>
	</div>
</div>

