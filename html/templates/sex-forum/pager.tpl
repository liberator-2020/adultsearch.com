{* $pager array passed from parent template *}
{if $pager|count}
	{section name=p loop=$pager}{assign var=p value=$pager[p]}
		{if $p.type == "prev"}
			<a href="{$p.link}" class="smallbutton">&laquo;Prev</a>
		{/if}
		{if $p.type == "page"}
			<a href="{$p.link}" class="smallbutton">{$p.page}</a>
		{/if}
		{if $p.type == "current"}
			<span class="pagination_current">{$p.page}</span>
		{/if}
		{if $p.type == "dots"}
			...
		{/if}
		{if $p.type == "next"}
			<a href="{$p.link}" class="smallbutton">Next&raquo;</a>
		{/if}
	{/section}
{/if}
