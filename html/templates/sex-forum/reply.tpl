{if !$no_wrap}
<link href="/css/forums.css" rel="stylesheet" type="text/css" />
<div id="ppc">{include file='page_ads.tpl' category='forum'}</div>
<div id="forum" class="container">
{/if}

<div class="reply" id="reply">{if $new_topic}New Topic{elseif $edit_post}Edit post{else}<strong>Replying to:</strong>{$topic_title}{/if}</div>

{if $error}<br /><font color="red"><b>{$error}</b></font><br />{/if}

{if $forum_locked == 0 }
	{if $smarty.session.account}
		<form name="posting" method="post" {if !$noaction}action="{if $forum_reply_link}{$forum_reply_link}{else}forum_reply{/if}"{/if}>
		{if $topic_id}<input type="hidden" name="topic_id" value="{$topic_id}" />{/if}
		{if $forum_id}<input type="hidden" name="forum_id" value="{$forum_id}" />{/if}

		<table  border="0" cellspacing="0" cellpadding="8" id="newpost">
		<tr>
			<td class="col1">
				{if $new_topic}
					Category:<br />
					{html_options name="forum_id" options=$categories selected=$selected_forum_id}
				{/if}	
				{if $new_topic || $edit_topic}
					<div id="company_select" data-loc-id="{$loc_id}">
						{if $companies}
							<br /><span id="category_name">Are you talking about a specific {$category_name}?</span><br />
							{html_options name="item_id" options=$companies selected=$item_id}
						{/if}
					</div>
				{/if}
				{if $new_topic}
<script type="text/javascript">
{literal}
function catchange() {
	var catselect = $('select[name="forum_id"]');
	var forum_id = catselect.val();
	var loc_id = $('#company_select').attr('data-loc-id');
	_ajax('GET', '/_ajax/companies', 'forum_id=' + forum_id + '&loc_id=' + loc_id, 'company_select');
}
$(document).ready(function () {
	var catselect = $('select[name="forum_id"]');
	catselect.change(catchange);
	catchange();
});
{/literal}
</script>
				{/if} 

				{if $edit_topic_title}
					<br />Title:
					<input type="text" name="topic_title" maxlength="100" value="{$topic_title}" class="newField" />
				{/if}
			</td>

			<td rowspan="2" valign="bottom" class="col2">
				
				{section name=i loop=$icons}
				{assign var=icon value=$icons[i]}
				<a href="javascript:void(0);" onClick="document.posting.topictext.value+=' {$icon.code} ';"><img src="{$icon.icon}" border="0" width="26" alt="{$icon.code}"></a>
				{/section}
			</td>
		</tr>

		<tr>
			<td class="col1">   
				{if $inputReason}
					<br />
					Edit Reason:
					<input type="text" name="editreason" value="{$editreason}"  mexlength="100" class="newField" />
				{/if}
				<div class="textedit">
					<input type="button" id="addbbcode0" name="addbbcode0" onclick="bbstyle(0);" class="addStyle" title="Bold" style="background-image: url('/images/sex-forum/iBold.png');"/> 
					<input type="button" id="addbbcode2" name="addbbcode2" onclick="bbstyle(2);" class="addStyle" title="Italics" style="background-image: url('/images/sex-forum/iItalic.png');" /> 
					<input type="button" id="addbbcode4" name="addbbcode4" onclick="bbstyle(4);" class="addStyle" title="Underline" style="background-image: url('/images/sex-forum/iUnderline.png');" />
					{*
					<input type="image" name="addbbcode5" value="Photo" onclick="bbstyle(0);" src="/images/iAddPhoto.png" class="addPhoto" title="Insert a Photo" /> 
					<input type="image" name="addbbcode6" value="Video" onclick="bbstyle(0);" src="/images/iAddVideo.png" class="addVideo" title="Insert a Video" /> 
					*}
				</div>   
				Comments:
				<textarea name="topictext" class="newField" >{$topictext}</textarea>
			</td>
		</tr>

		<tr>
			<td class="col1">
				<input type="submit" class="bigbutton"  value="Submit" name="Submit"{if !$smarty.session.account} disabled="disabled"{/if} /> 
				{if $goback}<input type="button" class="bigbutton" onclick="history.back()" value="Back"  />{/if}  
				<div class="follow"><input type='checkbox' name='watch' value='1' {if $watch_enabled}checked='checked'{/if}/>Follow with email notifications</div>
			</td>
			<td class="col2 emoticons">&nbsp;</td>
		</tr>
	</table>

	</form>  
	{/if}
	<!-- smarty.session.account -->
  
	{if !$smarty.session.account}To reply to this topic, <a href='/account/signup' title='AdultSearch sign up' rel='nofollow'>register</a> for free on Adultsearch.com{/if}
{/if}  
 <!-- not locked -->

{if !$no_wrap}
</div>
{/if}

