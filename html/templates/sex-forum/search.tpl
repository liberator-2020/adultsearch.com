<link href="/css/forums.css" rel="stylesheet" type="text/css" />

<div id="ppc">
	{include file='page_ads.tpl' category='forum'}
	{include file='page_ads_2.tpl'}
</div>

<div id="forum" class="container">
	<div class="forumheader">
        <h1>Forum Search</h1>
        <h2>Your searched for: {$search}</h2>
    </div>

	<div class="pagination">
		<div class="searchForum">

		<form action="{if $forum_search_url}{$forum_search_url}{else}{$core_loc_link}forum/forum_search{/if}" method="get">
			 <input type="text" name="search" value="{$search}"  />
				<input type="submit" value="Go" class="go" />
		</form>
</div>
	    {include "sex-forum/pager.tpl" pager=$pager}
	</div>

	<table  border="0" cellspacing="1" cellpadding="8" id="category">
	<tr>
    	<td class="title col1">Topic/Author</td>
		<td class="title col2">Replies</td>
		<td class="title col3">Last Post</td>
	</tr>

	{if $topics} 
		{section name=mainforum loop=$topics}
		{assign var=t value=$topics[mainforum]}
			<tr>
				<td class="col1">
					<div class="topic">
						<a href="{$t.post_link}" class="normal">{$t.topic_title}</a> (<b>{$t.forum_name}</b>)
					</div>
					Started by {$t.userlink}
    
					{if $t.important}{/if}
					{$t.paging} {$t.extra}
					<br />{$t.message|truncate:50}
				</td>
				<td class="col2"><div class="number">{$t.topic_replies}</div> posts</td>
				<td class="col3">{$t.lastPostDate|date_format:'%B %e, %Y %l:%M%p'}<div class="lastpost">{$t.lastPostUser}</div></td>
			</tr>
		{/section}
	{else}
		<tr><td class="col1" colspan="3">No topic found for this search query.</td></tr>
	{/if}
	</table>

	<div class="pagination">
		{include "sex-forum/pager.tpl" pager=$pager}
	</div>
</div>

