<link href="/css/forums.css" rel="stylesheet" type="text/css" />
<link href="/css/pm.css" rel="stylesheet" type="text/css" />
<link href="/css/fa/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<div id="ppc">
	{include file='page_ads.tpl' category='forum'}
	{include file='page_ads_2.tpl'}
</div>

<div id="forum" class="container singleTopic">

	<div class="nav">
		{if $next_topic}<div class="next">Next: {$next_topic} &raquo;</div>{else}<div class="next">&nbsp;</div>{/if}
		{if $previous_topic}&laquo; Previous: {$previous_topic} {else}&nbsp;{/if}
	</div>

	<div class="forumheader"> 
		<h1>{$topic_title}</h1>
		<a class="bigbutton" href="#replyBlock"> <span class="iComments"></span> post a reply </a> 
		{if $ismember}
			{if $watch_enabled}
				<a class="bigbutton" href="{if $unwatch_link}{$unwatch_link}{else}forum_reply?unwatch={$topic_id}{/if}"> <span class="iFollow"></span> Stop Following </a>
			{else}
				<a class="bigbutton" href="{if $watch_link}{$watch_link}{else}forum_reply?watch={$topic_id}{/if}"> <span class="iFollow"></span> Follow </a>
			{/if}
		{/if}   
		<h2><a href="{$forum_url}" title="{$loc_name} {$forum_name}">In "{$loc_name} {$forum_name}"</a></h2>
		ID {$topic_id}: {$count} Posts{if $pages}, {$pages} Pages{/if}
	</div>

	{include "sex-forum/pager.tpl" pager=$pager}
	<br /><br />

	<table border="0" cellspacing="1" cellpadding="8" id="category">
	{section name=p loop=$posts}
	{assign var=post value=$posts[p]}
		<tr>
			<td class="comment" id="p{$post.post_id}">
				<div class="user">
					<span class="name">
						{$post.username} 
						<a class="pm_btn_small pm_btn_white" href="/pm/compose?recipient={$post.account_id}"><i class="fa fa-envelope" aria-hidden="true"></i> Send PM</a>
					</span>
					{$post.post_time}
				</div>
				<span class="avatar">{$post.avatar} Posts: {$post.forum_post}</span>	

				{if $ismember}
				<a href="{if $post.quote_link}{$post.quote_link}{else}{$core_loc_link}forum/forum_quote?post_id={$post.post_id}&amp;topic_id={$post.topic_id}{/if}" class="quote"><span class="iQuote"></span>Quote</a>
				{/if}
				
				{$post.post_text}

				{if $ismember}
					{if $post.can_edit || $can_delete}
					<div class="editPost">	
						<a href="{if $post.edit_link}{$post.edit_link}{else}{$core_loc_link}forum/forum_edit.htm?post_id={$post.post_id}{/if}" class="smallbutton">Edit</a>
						<a href="{if $can_delete}{$delpost_link}{$post.post_id}{/if}" title="" class="smallbutton" onclick="return confirm('Are you sure you want to delete this post ?');">Delete</a>
						</div>
					{/if}
				{/if}
				{if ($post.post_edited)}
					<p style="color: #990000;">This message was edited by {$post.post_edited} {if $post.post_edit_count>1}{$post.post_edit_count} times{/if} on {$post.edited_date} for reason: {$post.post_edit_reason}</p>
				{/if}
			</td>
		</tr>
	{/section}            
	</table>

	<br />
	{include "sex-forum/pager.tpl" pager=$pager}

	<a id="replyBlock"></a>	
	{include file="sex-forum/reply.tpl" nowrap=true}
				
	{if $can_delete}
		<a href="{if $deltopic_link}{$deltopic_link}{else}/sex-forum/forum_reply.htm?deltopic={$topic_id}{/if}">
			<img src="/images/sex-forum/topic_delete.gif" border="0" alt="Do you want to delete entire topic ?" onclick="return confirm('Do you want to delete entire topic ? ');" />
		</a>
		<a href="{if $move_link}{$move_link}{else}/sex-forum/forum_move.htm?topic_id={$topic_id}{/if}"><img border="0" src="/images/sex-forum/topic_move.gif" alt="move" /></a>
	{/if}			

</div>

{if $tooltipScript && $tooltipContent}
{$tooltipContent}

<script type="text/javascript" src="/js/tools/tooltip.js"> </script>
<script type="text/javascript">
{$tooltipScript}
</script>
{/if}


{if $ismember}
<script type="text/javascript">
<!--
var current_page = escape("http://{php}echo $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];{/php}#");
{literal}
function forumSpam(id) {
	postAjaxUpdater("a"+id, '/_contact.htm', 'd=3&page='+current_page+id+'&data=spam:'+id);
}
-->
</script>
{/literal}
{/if}

{literal}
<script src="/js/tools/modernizr-1.7.min.js" type="text/javascript"></script>

<script type="text/javascript"> <!--
        $(document).ready(function() {
	   if (!Modernizr.csscolumns) {
                $('div#detailContainer dl').removeClass('modern').addClass('old');
            }
        });
-->
</script>
{/literal}

