<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
{section name=p loop=$map}
{assign var=m value=$map[p]}
<url><loc>{$m.link}</loc><lastmod>{$m.lastmod}</lastmod><changefreq>weekly</changefreq></url>
{/section}
</urlset>
