If you have a chance to take a picture of this business and are able to <a href="#uploadnow">upload it here</a> or send it to <b>support@adultsearch.com</b> by using your cell phone or computer, we will happily say thank you in return.

<p>
	<h1>Example Picture</h1>
	<img src="http://adultsearch.com/images/upload.jpeg" alt="" width="400" height="300" />
</p>

<p>Please be sure that the picture shows;</p>
<ol>
	<li>The front enterance of the place</li>
	<li>Signs or the name of the place shown on the picture clearly</li>
</ol>

<a name="uploadnow"></a>
<h1>Upload picture now</h1>

<form action="/upload" method="post" enctype="multipart/form-data">
<input type="hidden" name="ref" value="{$ref}" />
<table border="0" cellspacing="0" cellpadding="0" class="b" id="cl">
{if $error}
<tr>
	<td class="first"></td>
	<td class="second"><div class="error">{$error}</div></td>
</tr>
{/if}
<tr>
	<td class="first">E-Mail Address:</td>
	<td class="second">
	<input type="text" name="email" value="{$email}" size="40" /><span class="smallr">In case you think we might want to respond. (Optional)</span>
	</td>
</tr>
<tr>
	<td class="first">Comments/Note:</td>
	<td class="second">
	<textarea rows="5" cols="50" name="note">{$note}</textarea><span class="smallr">If you would like to add note/comment. (Optional)</span>
	</td>
</tr>
<tr>
	<td class="first">Picture:</td>
	<td class="second">
	<input type="file" name="file" />
	</td>
</tr>
<tr>
	<td class="first"></td>
	<td class="second">{$captcha}</td>
</tr>
{if !$captcha_ok}
<tr>
	<td class="first">Security Control:<br/></td>
	<td class="second">
        <input type="text" name="captcha_str" value="" autocomplete="off" />
        <span class="smallr">Type the text on the image into the above box.<br/>
        <img src="/captcha.php?{$time}" alt="" /></span>
	</td>
</tr>
{else}
	<input type="hidden" name="captcha_str" value="{$captcha_str}" />
{/if}
<tr>
	<td class="first">&nbsp;</td>
	<td class="second">
	<input type="submit" name="submit" value="Upload" />
	<input type="button" value="Go Back" onclick="window.location='http://{$ref}';" />
	</td>
</tr>
</table>
</form>
