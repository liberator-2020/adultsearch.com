<form method="post" action="/vip/step1" id="step1">
{if $account_id}<input type="hidden" name="account_id" value="{$account_id}" />{/if}

<div id="build1" class="build2">
	<h1>Purchasing VIP membership</h1>
	<div id="category">
		<div class="title">Select Payment Type: <br /><br />
		</div>
		<div style="min-width: 300px;">
			<input name="type" type="radio" value="1" {if $type==1}checked="checked"{/if}/>	$9.99 Recurring Payment Monthly<br />
			<input name="type" type="radio" value="2" {if $type==2}checked="checked"{/if}/> $19.99 Non-Recurring One Month<br />
			<br />
		</div>
		<div style="width: 180px; font-size: 1.2em; font-weight: bold; float: right;">
			For help please contact:<br /><br />
			{*
			U.S.:<br />
        	1-702-472-7242<br />
    	    Canada:<br />
	        1-778-819-8510<br />
			*}
			<a href="mailto:support@adultsearch.com">support@adultsearch.com</a><br />
		</div>
		<br style="clear: both;" />
	</div>
	<div id="rules"> <br />
		<br />
        
    <div class="title">You Agree to the Following Rules on AdultSearch:</div>
		<br>
		<ul>
		<li>I am <strong>at least 18 years of age</strong> or older and not considered to be a minor in my state of	 residence</li>
		</ul>

		<div>
			
			<span><img width="15" height="18" id="lock" alt="SSL" src="{$config_site_url}/images/i_secure.png" />
			All transactions are 			discreet &amp; secured with SSL Certificate encryption.</span><br /><br />
      <div>
				<img width="46" height="25" id="mc" alt="Mastercard" src="{$config_site_url}/images/i_mastercard.png" />
				<img width="46" height="25" id="visa" alt="Visa" src="{$config_site_url}/images/i_visa.png" />
  				<img width="46" height="25" id="amex" alt="American Express" src="{$config_site_url}/images/i_amex.png" />
				<img width="46" height="25" id="discover" alt="Discover" src="{$config_site_url}/images/i_discover.png" />
			</div>
			<div class="cards">
				<span id="siteseal">
					<script type="text/javascript" src="https://seal.starfieldtech.com/getSeal?sealID=6pvUdl4f9Rtkdbd9ezSTPMwOCVzfH3x9ZkWQUkJFk3UajmExXMp4hYJ9"></script>
				</span>
			</div>
			<br />
		</div>

		<span class="continue"><input id="step2" type="image" src="/images/adbuild/btn_agreecontinue.png" width="195" height="40" /></span>

		<p><strong>By clicking &quot;continue,&quot; you agree to abide by these rules as well as AdultSearch's terms of use. {if !$ismember}<br>You will need to <a href="https://adultsearch.com/account/signin">login</a> or <a href="https://adultsearch.com/account/signup">register</a> prior to completing your ad.{/if}</strong></p>
		<p>&nbsp;</p>

	</div>
	<div style="clear:both"></div>
</div>

{$csrf}
</form>

<script type='text/javascript'>
var cat;
$('#step2').click(function(event) { 
	event.preventDefault();
	cat = $("input[name=type]:checked", "#step1").val();
	if( cat === undefined ) {
		alert('Please choose payment type.');
		return false;
	}
	$("#step1").submit();
});
</script>
