<div id="build1">

<form action="" method="POST">
<div id="build4">

	{if $registered}
		You've successfully registered user <em>{$account_username}</em>.<br />
	{/if}

	<div class="title1">Complete your checkout to get VIP membership.<br /></div>
	
	<div class="infoblock promo">
		<div>
			<label><span class="question"><span class="hint">You must click "Apply" to update your purchase total.<span class="hint-pointer">&nbsp;</span></span></span>Enter a Promo or Coupon Code:</label>
			<input name="promo" id="promo" value="{$promo}" size="20" type="text" style="float: left;" />
			<input type="image" name="pbutton" src="/images/adbuild/btn_apply.png" width="82" height="31" style="margin: -2px 0px 0px 10px; padding: 0px;" />
		</div>
	</div>

	{if $phone_verify}	
		<div class="infoblock promo">
			<span style="font-size: 16px; font-weight: bold;">Please enter your phone number:</span><br /><br />
			{if $phone_verify_error}
			      	<div><span class="error">Please enter your phone number</span></div>
			  	{/if}
			This phone number <strong>will not</strong> be displayed anywhere on the site.<br />
			We will contact you shortly on this number, to prevent Human Trafficking.<br />
			This ad will not be visible on the website until the phone number is verified.<br />
			<input name="phone" id="phone" value="{$phone}" size="20" type="text" style="float: left;" />
		</div>
	{/if}

	<div class="infoblock">
		<span class="title1">Secure Checkout: $<span id="checkouttotal">{$total}</span></span>

		{if $error}
			<div><span class="error">{$error}</span></div>
		{/if}

		<div id="newcc">
			<div>
				<label>First Name:</label>
				<input type="text" name="cc_firstname" size="40" value="{$cc_firstname}" maxlength="60" />
			</div>
			<div>
				<label> Last Name:</label>
				<input type="text" name="cc_lastname" size="40" value="{$cc_lastname}" />
			</div>
			<div>
				<label> Address:</label>
				<input type="text" name="cc_address" size="40" value="{$cc_address}" />
			</div>
			<div>
				<label> ZIP/Postal Code:</label>
				<input type="text" name="cc_zipcode" size="10" value="{$cc_zipcode}" />
			</div>
			<div>
				<label> City:</label>
				<input type="text" name="cc_city" size="20" value="{$cc_city}" />
			</div>
			<div>
				<label> State/Province:</label>
				<input type="text" name="cc_state" size="20" value="{$cc_state}" />
			</div>
			<div>
                <label> Country:</label>
                <select name="cc_country">{html_options options=$country_options selected=$cc_country}</select>
            </div>
			<input type="hidden" name="captcha_str" value="{$captcha_str}" />
			<div>
				<span style="color: black; font-size: 1.2em;">
				<table>
					<tr>
						<td>For help please contact:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						{*
						<td>U.S.: 1-702-472-7242<br />Canada: 1-778-819-8510</td>
						*}
						<td><a href="mailto:support@adultsearch.com">support@adultsearch.com</a></td>
					</tr>
				</table>
				</span>
			</div>
		</div>
	</div>
</div>

<div class="infoblock">
	<div id="ssl">
	    <div class="right">
			<img width="46" height="25" id="mc" alt="Mastercard" src="{$config_site_url}/images/i_mastercard.png" />
			<img width="46" height="25" id="visa" alt="Visa" src="{$config_site_url}/images/i_visa.png" />
			<br />
            <img width="46" height="25" id="amex" alt="American Express" src="{$config_site_url}/images/i_amex.png" />
			<img width="46" height="25" id="discover" alt="Discover" src="{$config_site_url}/images/i_discover.png" />
		</div>
		<span><img width="15" height="18" id="lock" alt="SSL" src="{$config_site_url}/images/i_secure.png" />
		All transactions are <br />discreet &amp; secured with<br />SSL Certificate encryption.</span><br />
		<br />
		<div class="cards">
			{if !$ipad && !$iphone}
				<span id="siteseal">
					<script type="text/javascript" src="{if $https}https{else}http{/if}://seal.starfieldtech.com/getSeal?sealID=6pvUdl4f9Rtkdbd9ezSTPMwOCVzfH3x9ZkWQUkJFk3UajmExXMp4hYJ9"></script>
			    </span>
			{/if}
		</div>
		<br />
	</div>
	<span class="continue"><input type="image" src="/images/adbuild/btn_submit.png" alt="" width="136" height="40" /></span></div>
</div>

</form>

{if $scc}
<script type="text/javascript">
</script>
{/if}

