<p>
Please wire transfer <span style="color: red; font-size: 1.1em;">3,000 USD</span> or more for your account credit to the following Account number. We will no longer be taking credit cards from Agencies. We are a European company not Americain. Therefore we do not bank in the USA.
</p>
<p style="padding: 5px; border: 1px solid #cecaca; border-radius: 10px; background-color: #ffeacc;">
	Bank name: Banco Nacional Ultramarino (BNU)<br />
	Bank address:<br />
	2 Avenida Almeida<br />
	Macau, Macau<br />
	Macau does not use zip codes. Use 00000 if you have to input a zip code<br />
	Account Number: 9015987390<br />
	Swift code: BNULMOMX<br />
	<br />
	Company: VIPMUSIC LIMITED<br />
	Company address:<br />
	Av DA Praia Grande 309<br />
{*
	Nan Yue Commercial Center,7 Andar<br />
*}
	Macau, Macau<br />
</p>
<span style="color: red; font-weight: bold;">
	DO NOT SEND ANY INFO ON THE WIRE AT YOUR BANK EXCEPT YOUR ACCOUNT NUMBER WHICH IS #{$account_id}
</span>
</p>
<p>
We will no longer be taking wire info via email. You will upload the info along with the image of your wire from your account page. <a href="{$site_url}/account/wire" class="btn btn-warning btn-sm" data-ajax="false" >Click this link</a>
</p>
<p>
<span style="color: red; font-size: 1.1em;">BANKS ARE SLOW NOT FAST.</span><br />
If you use Bofa, Chase, Citi to send your wire it will take bout 5 working days before the money is in our account. All other banks can take up to 3 weeks. Other banks use whats called <a href="https://blog.revolut.com/swift-sepa-how-international-money-transfers-actually-work/" target="_blank" class="btn btn-default btn-xs" data-ajax="false">intermediary banks</a>. This means your bank sends your money to another bank &amp; then that bank sends it to our bank which can take up to 3 weeks.<br />
BANKS WORK SLOW NOT FAST.<br />
When we see the funds in our account (We check the account twice daily, Hong Kong time) we will credit your account.
</p>
<p>
The <span style="color: red; font-size: 1.1em;">3,000 USD</span> or any amount you send above that will be credited to your account &amp; used anyway you want when the funds are in your account. Your account will be allowed 10 individual ads &amp; you may buy as many reposts as you like, star covers, side sponsor ads, sticky sponsor ads, or any other type of ads for your 10 individual ads.
</p>
{*
<p>
Allow 10 working days for your wire transfers to arrive in our bank account. We fund your account when the money is in our account not when you send proof. send the senders name &amp; your account number #{$account_id} in every correspondence with us. If you are chinese call your friend who speaks, reads, &amp; writes english to email us.
</p>
*}

<p>
<span style="color :red;">Banks are closed every Saturday, Sunday, the following holidays, &amp; so are we.</span><br />
<br />
Chinese Holidays<br />
<table>
<tr><td>1 May</td><td>Wed</td><td>Labour Day</td></tr>
<tr><td>12 May</td><td>Sun</td><td>Birthday of Buddha</td></tr>
<tr><td>13 May</td><td>Mon</td><td>Birthday of Buddha Holiday</td></tr>
<tr><td>7 Jun</td><td>Fri</td><td>Tuen Ng Festival</td></tr>
<tr><td>1 Jul</td><td>Mon</td><td>HKSAR Establishment Day</td></tr>
<tr><td>14 Sep</td><td>Sat</td><td>The Day Following Mid-Autumn Festival</td></tr>
<tr><td>1 Oct</td><td>Tue</td><td>National Day</td></tr>
<tr><td>7 Oct</td><td>Mon</td><td>Chung Yeung Festival</td></tr>
<tr><td>25 Dec</td><td>Wed</td><td>Christmas Day</td></tr>
<tr><td>26 Dec</td><td>Thu</td><td>The First Weekday After Christmas Day</td></tr>
</table>
</p>

