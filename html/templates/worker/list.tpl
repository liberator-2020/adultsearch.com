<h1>New places to fill data</h1>

<style type="text/css">
table {
	border-collapse: collapse;
}
td, th {
	padding: 3px;
	border: 1px solid #bbb;
}
	
</style>

<table>
<tr>
	<th>Type</th>
	<th>Name</th>
	<th>Phone</th>
	<th>Status</th>
	<th>View link</th>
	<th>Edit link</th>
</tr>

{section name=p loop=$places}
{assign var=place value=$places[p]}
<tr>
	<td>{$place.type}</td>
	<td>{$place.name}</td>
	<td>{$place.phone}</td>
	<td>{$place.status}</td>
	<td><a href="{$place.view_link}" target="_blank">view</a></td>
	<td><a href="{$place.edit_link}" target="_blank">edit</a></td>
</tr>
{/section}

</table>

