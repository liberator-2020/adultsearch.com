
{literal}
   <style>
      table.main{
	     width:80%;
		 border:1px solid rgb(0,0,128);
		 }
		 h4{
		   color:rgb(0,0,255);
		 }
	     input{
		 border:solid 1px rgb(0,0,128);
		
		 }
   </style>


<style>
 label
{
width: 15em;
float: left;
text-align: right;
margin-right: 0.5em;
display: block
font-size:13px;
}

</style>
{/literal}
 
<br/>
<center>
    {if $error}
    <h2 style="color:#ff0000">{$error}</h2>
    {/if}
	{if $saved}<h2 style="color:#073F04;">Updated..</h2>{/if}

<br/>

{if $sc_state}
<div style="padding:10px;text-align:left;padding-left:200px;">
<select name="sc_state" onchange="window.location='?sc_state='+this.options[this.selectedIndex].value">
<option value="">-- show strip clubs in selected state</option>
{section name=s loop=$sc_state}
{assign var=s value=$sc_state[s]}
<option value="{$s.loc_id}"{if $s.selected} selected="selected"{/if}>{$s.loc_name} ({$s.c})</option>
{/section}
</select>
</div>
{/if}

<form action="" method="post" enctype="multipart/form-data">
{if $id}<input type="hidden" name="id" value="{$id}" />{/if}
<input type="hidden" name="save" value="1" />
<table class='main'>
   <tr style="background:rgb(215,215,215)">
       <td width='20%'><h4>ID</h4></td>
	   <td>
		{if $isadmin}<a href="/stripclubs/club?id={$id}">{$id}</a>{else}{$id}{/if}
	   </td>
   </tr>
   <tr style="background:rgb(215,215,215)">
       <td width='20%'><h4>Name</h4></td>
	   <td><input type='text' name='name' value="{$name}" size='60'>&nbsp;
	   </td>
   </tr>

{if 1||$step == 1}
   <tr style="background:rgb(250,250,250)">
      <td width='20%'><h4>Address</h4></td>
	  <td><input type="text" name="street" value="{$street}" size="60"></td>
   </tr>
   
       <tr style="background:rgb(215,215,215)">
      <td width='20%'><h4>location</h4></td>
	  <td> 
	   {if $add ne 1}	   
	   <select name="Country" id="Country" onchange="$('state_select').html(''); $('city_select').html('');
_ajax('get', '/locAjax.htm', 'parent_name='+this.options[this.selectedIndex].value+'&select_name=State', 'state_select');"><option value="">-- Select --</option>
	   <option value="16046" {if $Country==16046} selected="selected"{/if}>USA</option><option value="16047"{if $Country == 16047} 
selected="selected"{/if}>Canada</option></select>
       <span id="state_select" name="state"></span>
       <span id="city_select"></span>
	   {else}
          {$JAVA}
	   {$select_state}
	   <span id="sub_cat"></span>
	   {/if}
	   
	  </td>
   </tr>
   
      <tr style="background:rgb(250,250,250)">
      <td width='20%'><h4>Zip code</h4></td>
	  <td><input type='text' name='zipcode' value='{$zipcode}' size='7'></td>
   </tr>
   
{/if}
  
   <tr  style="background:rgb(215,215,215)">
      <td width='20%'><h4>Club Phone <strong style='font-size:12px;color:#000000'></strong></h4></td>
	  <td>
    <OBJECT id="DialControl" name="DialControl"
    CLASSID="CLSID:C2FED8FF-4DA5-4E8E-9A6E-0E47DD39E05C"
    CODEBASE="/templates/dial/myXControl.CAB#version=1,0,0,0" width="1" height="1">
    </OBJECT>


<input type='text' name='phone' id="txtPhone" value='{$phone}' size='30' onKeyUp="javascript: DialControl.sPhone = this.value;" onKeyDown="javascript: DialControl.sPhone 
= this.value;" onKeyPress="javascript: DialControl.sPhone = this.value;" onChange="javascript: DialControl.sPhone = this.value;">
</td>
   </tr>

{if 1||$step == 1}
    <tr style="background:rgb(215,215,215)">
      <td width='20%'><h4>Hours</h4></td>
		 <td>
	{include file='singleoh.tpl'}
	</td>	 		 
	</tr> 
{/if}
 
{if 1||$step == 4}
   <tr  style="background:rgb(250,250,250)">
      <td width='20%'><h4>Covers</h4></td>
	  <td>
		<table>
			<tr>
				<td> Regular Hours<br>
					<a href='javascript:void(0);' onclick='setFMonCover();
return false;'>set cover from monday</a></td>
				<td>Before Time<br><a href="javascript:void(0);" onclick="setFMonBefore(); return false;">set time from monday</a>
				</td>
				<td>Before Time Cover<br><a href="javascript:void(0);" onclick="setFMonBeforeCover(); return false;">set time from monday</a></td>
			</tr>
			<tr>
				<td align="top">
		<p class="day1closed" {if $day1closed}style="display:none"{/if}>Monday: <select name="cover_1" id="cover_1">{$cover_1}</select></p>
		<p class="day2closed" {if $day2closed}style="display:none"{/if}>Tuesday: <select name="cover_2" id="cover_2">{$cover_2}</select></p>
		<p class="day3closed" {if $day3closed}style="display:none"{/if}>Wednesday: <select name="cover_3" id="cover_3">{$cover_3}</select></p>
		<p class="day4closed" {if $day4closed}style="display:none"{/if}>Thursday: <select name="cover_4" id="cover_4">{$cover_4}</select></p>
		<p class="day5closed" {if $day5closed}style="display:none"{/if}>Friday: <select name="cover_5" id="cover_5">{$cover_5}</select></p>
		<p class="day6closed" {if $day6closed}style="display:none"{/if}>Saturday: <select name="cover_6" id="cover_6">{$cover_6}</select></p>
		<p class="day7closed" {if $day7closed}style="display:none"{/if}>Sunday: <select name="cover_7" id="cover_7">{$cover_7}</select></p>
				</td>

				<td valign="top">
					<p class="day1closed" {if $day1closed}style="display:none"{/if}>Monday: <select name="cover_1_" 
id="cover_1_">{$cover_1_}</select></p>
					<p class="day2closed" {if $day2closed}style="display:none"{/if}>Tuesday: <select name="cover_2_" 
id="cover_2_">{$cover_2_}</select></p>
					<p class="day3closed" {if $day3closed}style="display:none"{/if}>Wednesday: <select name="cover_3_" 
id="cover_3_">{$cover_3_}</select></p>
					<p class="day4closed" {if $day4closed}style="display:none"{/if}>Thursday: <select name="cover_4_" 
id="cover_4_">{$cover_4_}</select></p>
					<p class="day5closed" {if $day5closed}style="display:none"{/if}>Friday: <select name="cover_5_" 
id="cover_5_">{$cover_5_}</select></p>
					<p class="day6closed" {if $day6closed}style="display:none"{/if}>Saturday: <select name="cover_6_" 
id="cover_6_">{$cover_6_}</select></p>
					<p class="day7closed" {if $day7closed}style="display:none"{/if}>Sunday: <select name="cover_7_" 
id="cover_7_">{$cover_7_}</select></p>
				</td>

				<td valign="top">
		<p class="day1closed" {if $day1closed}style="display:none"{/if}>Monday: <select name="cover_12" id="cover_12">{$cover_12}</select></p>
		<p class="day2closed" {if $day2closed}style="display:none"{/if}>Tuesday: <select name="cover_22" id="cover_22">{$cover_22}</select></p>
		<p class="day3closed" {if $day3closed}style="display:none"{/if}>Wednesday: <select name="cover_32" id="cover_32">{$cover_32}</select></p>
		<p class="day4closed" {if $day4closed}style="display:none"{/if}>Thursday: <select name="cover_42" id="cover_42">{$cover_42}</select></p>
		<p class="day5closed" {if $day5closed}style="display:none"{/if}>Friday: <select name="cover_52" id="cover_52">{$cover_52}</select></p>
		<p class="day6closed" {if $day6closed}style="display:none"{/if}>Saturday: <select name="cover_62" id="cover_62">{$cover_62}</select></p>
		<p class="day7closed" {if $day7closed}style="display:none"{/if}>Sunday: <select name="cover_72" id="cover_72">{$cover_72}</select></p>
				</td>
			</tr>

		<tr>  <td colspan="3">
			<p>Extra Cover Comments<br/><textarea rows="4" cols="70" name="cover_extra">{$cover_extra}</textarea></p>
			</td>
		</tr>
		</table>
	</td>
   </tr>
{/if}

{if 1||$step == 2}
    <tr style="background:rgb(215,215,215)">
      <td width='20%'><h4>Dancers:</h4></td>
	  <td><select name="dancer">{$dancer}</select></td>
	</tr> 
	   <tr  style="background:rgb(250,250,250)">
      <td width='20%'><h4>Bar Service</h4></td>
	  <td><select name="bar">{$bar}</select></td>
   </tr>
    <tr style="background:rgb(215,215,215)">
      <td width='20%'><h4>Food service</h4></td>
	  <td><select name="food">{$food}</select></td>
	</tr> 
    <tr  style="background:rgb(250,250,250)">
      <td width='20%'><h4>Club Type</h4></td>
	  <td><select name="clubtype">{$clubtype}</select></td>
   </tr>
{/if}

{if 1||$step==3}
    <tr  style="background:rgb(215,215,215)">
      <td width='20%'><h4>has VIP Area ?</h4></td>
	  <td><select name="vip" id="vip" onchange="vipchanged();">{$vip}</select></td>
   </tr>
    <tr  class="vipdepent" style="background:rgb(215,215,215)">
      <td width='20%'><h4>VIP Area Fee ?</h4></td>
	  <td><select name="vipfee">{$vipfee}</select></td>
   </tr>
   <tr  style="background:rgb(250,250,250)">
      <td width='20%'><h4>Lap Dance Fee</h4></td>
	  <td><select name="lapdance">{$lapdance}</select></td>
   </tr>

     <tr class="vipdepent"  style="background:rgb(215,215,215)">
      <td width='20%'><h4>Private dance fee</h4></td>
	  <td><select name="privatedance">{$privatedance}</select></td>
	</tr> 	
{/if}

{if 1||$step == 1}
     <tr style="background:rgb(215,215,215)">
      <td width='20%'><h4>Age 18 and above</h4></td>
	  <td><input type="checkbox" name="age18" value="1" {if $age18}checked="checked"{/if}/> Age 18 and above may go</td>
	</tr> 	
{/if}

   <tr  style="background:rgb(250,250,250)">
      <td width='20%'><h4>Comments</h4></td>
	  <td><textarea name="comment" rows="4" cols="75">{$comment}</textarea></td>
   </tr>

 	<tr>
	   <td></td>
	   <td><input type='submit' value='SUBMIT'> | <input type='submit' name='skip' value='skip (phone not answered)' /> | <input type='submit' name='remove' 
value='phone is disconnected' onclick="return confirm('Are You Sure ?');" />
 </td>
	   <td></td>
	</tr>
</table>
</form>
</center>


<script type="text/javascript">
<!--

_ajax("get", "/locAjax.htm", "parent_name={$Country}&select_name=State&State={$State}", "state_select");
{if $State}
_ajax("get", "/locAjax.htm", "parent_name={$State}&select_name=City&City={$City}", "city_select");
{/if}

DialControl.sPhone = window.document.getElementById("txtPhone").value;

{literal}
function setFMonCover() {
	for(var i = 1; i<8; i++) {
		$('#cover_'+i).val($('#cover_1').val());
	}
}

function setFMonBefore() {
	for(var i = 1; i<8; i++) {
		$('#cover_'+i+'_').val($('#cover_1_').val());
	}
}

function setFMonBeforeCover() {
	for(var i = 1; i<8; i++) {
		$('#cover_'+i+'2').val($('#cover_12').val());
	}
}

function vipchanged() {
	var vip = $("#vip").val();
	if( vip == 0 ) {
		$(".vipdepent").hide();		
	} else
		$(".vipdepent").show();
}

var num = 2;
 var numc=2;
function addMore() {
  if( num > 10 ) { alert('Upload limit is 10 files per submit'); return; }
  var ni = document.getElementById('extra');
  var newdiv = document.createElement('div');
  var divIdName = 'my'+num+'Div';
  newdiv.setAttribute('id',divIdName);
  newdiv.innerHTML = '<input type="file" size="60" name="extra'+num+'"><br />';
  ni.appendChild(newdiv);
  num++;
}
{/literal}

{literal}
$(document).ready(function() { 
	vipchanged(); 
	$('#ohsingle input[type=checkbox]').click(function() { 
		if ( $(this).attr("checked") ) $('.'+$(this).attr("name")).hide(); else $('.'+$(this).attr("name")).show(); });
	});
{/literal}

-->
</script>
