<form method="get">
<select name="show">
<option value="">-- select --</option>
{section name=sl loop=$state_list}
{assign var=sl value=$state_list[sl]}
{if $sl.loc_id}<option value="{$sl.loc_id}"{if $show==$sl.loc_id} selected="selected"{/if}>{$sl.loc_name} {$sl.total}</option>{/if}
{/section}
</select><input type="submit" value="go" /></form> <a href="?show=all">Show all the places (slows down the browser big time!!)</a>

<table width="100%">

{section name=s1 loop=$state}
{assign var=s value=$state[s1]}
 <tr>
  <td colspan="3"><u><b>{$s.name}</b> total: {$s.total}</u></td>
 </tr>

 <tr>
  <td>ID</td>
  <td>Name</td>
  <td>Phone</td>
  <td>Address</td>
  <td>City</td>
  <td>Zip</td>
  <td>State</td>
  <td>Country</td> 
  <td>&nbsp;</td> 
  <td>Type</td> 
 </tr>

{section name=s2 loop=$state[s1].c}
{assign var=c value=$state[s1].c[s2]}
 <tr id="tr{$c.id}" class="editable" bgcolor="{cycle values="#F0C040,#F8E1A0"}">
{if 1 && $isadmin }
  <td>{$c.id}</td>
  <td width="189">{$c.name}</td>
  <td>{$c.phone}</td>
 <td>{$c.address}</td> 
{else}
  <td width="189">{$c.id}. <input type="text" size="20" name="name" value="{$c.name}" rel="name" /></td>
  <td><input type="text" size="6" name="pic" value="{$c.pic}" /></td>
  <td><input type="text" size="10" name="phone" value="{$c.phone}" /></td>
 <td><input type="text" size="20" name="address" value="{$c.address}" /></td> 
{/if}
 <td>{$c.city}</td> 
 <td>{$c.zip}</td> 
 <td>{$c.state}</td> 
 <td>{$c.country}</td> 
 {if $isadmin}
 <td>&nbsp;</td>
 {else}
 <td><input type="button" name="update" value="update" id="{$c.id}" ref="{if $c.emp}emp{else}sc{/if}" /> || 
<input type="button" name="remove" value="remove" onclick="_ajaxc('get','', 'remove=1&id={$c.id}&type={if $c.emp}emp{else}sc{/if}');" ref="{if $c.emp}emp{else}sc{/if}" /></td>
 {/if}
 <td>{if $c.emp}emp{else}sc{/if}</td>
 </tr>
{/section}

{/section}

</table>

{literal}
<script type="text/javascript">
<!--
-->
</script>
{/literal}
