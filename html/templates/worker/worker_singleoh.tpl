<table>
<tr>
	<td>
		<input type="checkbox" name="always" value="1" {if $always}checked="checked"{/if} onclick="ohsingle_flip(this);"/>open 24 hours a day<br />
	</td>
</tr>
<tr id="ohsingle" {if $always} style="display: none;"{/if}>
	<td>
	monday: 
		from <select name="day1o" id="day1o{$id_company}" class="dayoc" class="dayoc">{$opens[0]}</select> 
		to <select name="day1c" id="day1c{$id_company}" class="dayoc" class="dayoc">{$closes[0]}</select> 
		<input type="checkbox" name="day1allday" id="day1allday" value="1" class="allday" data-day="1" /> All day
		<input type="checkbox" name="day1closed" value="1" {if $closed[0]}checked="checked"{/if} /> Closed 
		<a href="javascript:void(0);" onclick="setFMon('{$id_company}');">set times from monday</a>
	<br />
	<div id="cal{$id_company}">
	tuesday: 
		from <select name="day2o" id="day2o{$id_company}" class="dayoc">{$opens[1]}</select> 
		to <select name="day2c" id="day2c{$id_company}" class="dayoc">{$closes[1]}</select>
		<input type="checkbox" name="day2allday" id="day2allday" value="1" class="allday" data-day="2" /> All day
		<input type="checkbox" name="day2closed" value="1" {if $closed[1]}checked="checked"{/if} /> Closed
	<br />
	wednesday: 
		from <select name="day3o" id="day3o{$id_company}" class="dayoc">{$opens[2]}</select> 
		to <select name="day3c" id="day3c{$id_company}" class="dayoc">{$closes[2]}</select>
		<input type="checkbox" name="day3allday" id="day3allday" value="1" class="allday" data-day="3" /> All day
		<input type="checkbox" name="day3closed" value="1" {if $closed[2]}checked="checked"{/if} /> Closed
	<br />
	thursday: 
		from <select name="day4o" id="day4o{$id_company}" class="dayoc">{$opens[3]}</select> 
		to <select name="day4c" id="day4c{$id_company}" class="dayoc">{$closes[3]}</select>
		<input type="checkbox" name="day4allday" id="day4allday" value="1" class="allday" data-day="4" /> All day
		<input type="checkbox" name="day4closed" value="1" {if $closed[3]}checked="checked"{/if} /> Closed
	<br />
	friday: 
		from <select name="day5o" id="day5o{$id_company}" class="dayoc">{$opens[4]}</select> 
		to <select name="day5c" id="day5c{$id_company}" class="dayoc">{$closes[4]}</select>
		<input type="checkbox" name="day5allday" id="day5allday" value="1" class="allday" data-day="5" /> All day
		<input type="checkbox" name="day5closed" value="1" {if $closed[4]}checked="checked"{/if} /> Closed
	<br />
	saturday: 
		from <select name="day6o" id="day6o{$id_company}" class="dayoc">{$opens[5]}</select> 
		to <select name="day6c" id="day6c{$id_company}" class="dayoc">{$closes[5]}</select>
		<input type="checkbox" name="day6allday" id="day6allday" value="1" class="allday" data-day="6" /> All day
		<input type="checkbox" name="day6closed" value="1" {if $closed[5]}checked="checked"{/if} /> Closed
	<br />
	sunday: 
		from <select name="day7o" id="day7o{$id_company}" class="dayoc">{$opens[6]}</select> 
		to <select name="day7c" id="day7c{$id_company}" class="dayoc">{$closes[6]}</select> 
		<input type="checkbox" name="day7allday" id="day7allday" value="1" class="allday" data-day="7" /> All day
		<input type="checkbox" name="day7closed" value="1" {if $closed[6]}checked="checked"{/if} /> Closed
	</div>
	</td>
</tr>
</table>

<script type="text/javascript">
{literal}
function cal(id) {
	if( $("cal"+id).style.display == "" )
		$("cal"+id).style.display = 'none';
	else
		$("cal"+id).style.display = '';
}

function setFMon(id) {
	for(var i = 2; i<8; i++) {
		$('#day'+i+'o'+id).val($('#day1o'+id).val());
		$('#day'+i+'c'+id).val($('#day1c'+id).val());
	}
}

function allday_changed() {
	if (!this.checked)
		return;
	var day = $(this).data('day');
	$('#day'+day+'o').val(0);
	$('#day'+day+'c').val(1440);
}

function allday_update() {
	for(var i = 1; i < 8; i++) {
		var o = $('#day'+i+'o').val();
		var c = $('#day'+i+'c').val();
		if (o == 0 && c == 1440)
			$('#day'+i+'allday').prop('checked', true);
		else
			$('#day'+i+'allday').prop('checked', false);
	}
}

function ohsingle_flip(elem) {
	if ($(elem).is(':checked')) {
		$('#ohsingle').hide();
	} else {
		$('#ohsingle').show();
	}
}

$(document).ready(function() {
	$('.allday').change(allday_changed);
	allday_update();
	$('.dayoc').change(allday_update);
});
{/literal}
{if $byapponly}
	$('ohmp').hide();
{/if}
-->
</script>


