<form method="get">
<select name="show">
<option value="">-- select --</option>
{foreach from=$state_list item=i}
{if $i.loc_id}<option value="{$i.loc_id}"{if $show==$i.loc_id} selected="selected"{/if}>{$i.loc_name} {$i.total}</option>{/if}
{/foreach}
{*
{section name=sl loop=$state_list}
{assign var=sl value=$state_list[sl]}
{if $sl.loc_id}<option value="{$sl.loc_id}"{if $show==$sl.loc_id} selected="selected"{/if}>{$sl.loc_name} {$sl.total}</option>{/if}
{/section}
*}
</select>
<p><input type="checkbox" name="image" value="1"{if $image} checked="checked"{/if} /> Show places that have picture <p>
<input type="submit" value="go" /></form> <a href="?show=all">Show all the places (slows down the browser big time!!)</a>

<table>

{section name=s1 loop=$state}
{assign var=s value=$state[s1]}
 <tr>
  <td colspan="3"><u><b>{$s.name}</b> total: {$s.total}</u></td>
 </tr>

 <tr>
  <td>Name</td>
  <td>Picture ID</td>
  <td>Phone</td>
  <td>Address</td>
  <td>City</td>
  <td>Zip</td>
  <td>State</td>
  <td>Country</td> 
  <td>Date Added</td> 
  <td>&nbsp;</td> 
  <td>Type</td> 
 </tr>

{section name=s2 loop=$state[s1].c}
{assign var=c value=$state[s1].c[s2]}
 <tr id="tr{$c.id}" class="editable" bgcolor="{cycle values="#F0C040,#F8E1A0"}">
  <td width="189">{$c.id}. <input type="text" size="20" name="name" value="{$c.name}" rel="name" /></td>
  <td><input type="text" size="6" name="pic" value="{$c.pic}" /></td>
  <td><input type="text" size="10" name="phone" value="{$c.phone}" /></td>
 <td><input type="text" size="20" name="address" value="{$c.address}" /></td> 
 <td>{$c.city}</td> 
 <td>{$c.zip}</td> 
 <td>{$c.state}</td> 
 <td>{$c.country}</td> 
 <td>{$c.added}</td> 
 <td><input type="button" name="update" value="update" id="{$c.id}" ref="{if $c.emp}emp{elseif $c.sc}sc{elseif $c.ling}L{else}book{/if}" /> || 
{* <input type="button" name="remove" value="remove" onclick="_ajaxc('get','', 'remove=1&id={$c.id}&type={if $c.emp}emp{else}sc{/if}');" ref="{if $c.emp}emp{else}sc{/if}" 
/>*}
  </td>
 <td>{if $c.emp}emp{elseif $c.sc}sc{elseif $c.ling}L{else}book{/if}</td>
 </tr>
{/section}

{/section}

</table>

{literal}
<script type="text/javascript">
<!--

jQuery(document).ready( function() { jQuery('tr.editable td input[type=button]').click(function() { 
	var id = jQuery(this).attr("id");
	var name = jQuery("#tr"+id+" td input[name=name]").val();
	var pic = jQuery("#tr"+id+" td input[name=pic]").val();
	var phone = jQuery("#tr"+id+" td input[name=phone]").val();
	var address = jQuery("#tr"+id+" td input[name=address]").val();
	var type= jQuery(this).attr("ref");	

	var param = "type="+type+"&update=1&id="+id+"&name="+name+"&pic="+pic+"&phone="+phone+"&address="+address;
	_ajaxs('get', '', param);
} ); });

function submitFuncion(content) {
	alert(jQuery(this).attr("id"));
}
-->
</script>
{/literal}
