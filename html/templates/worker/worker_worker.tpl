<div id="loading" style="display:none;position:absolute;top:200px;color:red"><img src="{$config_site_url}/images/ajax-loader.gif" alt="" /></div>

<div id="blue">

{if $owner_mode}
	<div id="owners" style="clear:both">
		<span class="ownermenu">
			<a href="{$owner_link}" title="">Business Main Page</a>
		</span>
		<span class="ownermenu">
			<a href="owner?id={$id}" class="active" title="">Edit My Business</a>
		</span>
		<span class="ownermenu"><a href="/account/payments" title="">Billing</a></span>
	</div>
{/if}

<table width="100%" class="edit">
<tr>
<td style="text-align:center">
<form action="" method="post" enctype="multipart/form-data" id="wform">
<input type="hidden" name="data_action" value="{$data_action}" />
<input type="hidden" name="data_submit" value="1" />
{if $form_hidden}{$form_hidden}{/if}

{if $browse}
<div>
{section name=l loop=$letters}{assign var=l value=$letters[l]}
<a href="?browse={$l}">{$l}</a>
{/section}
</div>
{/if}

<table width='100%' cellspacing='15' style="text-align:left" class="b">

{section name=cl loop=$form}{assign var=c value=$form[cl]}
{assign var=include value=$c.include}
<tr style="border: 1px solid #cccccc">
 <td class="first">{$c.name}:</td>
 <td class="second">{if $c.include}{include file="$include"}{else}{$c.value}{/if}</td>
</tr>
{/section}

<tr>
	<td>&nbsp;</td>
	  <td>
	<div id="submitlog"></div>

	{if $deleted}
		<br />
		<span style="color: red; font-weight: bold;">This place is deleted</span>
		<br /><br />
	{else}

		{if $work_item && !$owner_mode}
<!--
			Work item: #{$work_item}<br />
-->
			<input type="hidden" name="work_item" value="{$work_item}" />
<!--
			<input type="submit" name="Save_Leave_Uncompleted" value="Save but leave uncompleted" onclick="return checkform();" />
-->
			<input type="submit" name="Save_Completed" value="Save and mark Completed" onclick="return checkform();" />
			<input type="submit" name="Skip" value="Skip to next" />
			{if $reassign_name}
				<input type="submit" name="{$reassign_name}" value="{$reassign_label}" />
			{/if}
			<input type="submit" name="Remove" value="Remove" onclick="return confirm('Are you sure?');" />

			<br /><br />
			<a href="/mng/work_items">Back to my work items list</a>
		{else}
			<input type="submit" name="Save" id="SaveButton" value="{if $save_button_value}{$save_button_value}{else}Save{/if}" onclick="return checkform();" />
			{if $worker_mode}
				&nbsp;&nbsp;&nbsp;<input type="submit" name="Skip" value="Skip to next one" onclick="return confirm('Are you sure to skip this?');"/>
			{elseif !$visitor_mode&&!$owner_mode}
				&nbsp;&nbsp;&nbsp;<input type="submit" name="SaveAddNew" value="Save and add new"/>
				{if $show_remove}
					&nbsp;&nbsp;&nbsp;<input type="submit" name="Remove" value="Remove" onclick="return confirm('Are you sure?');"/>
				{/if}
			{/if}

			{if $cbutton}
				{section name=cb loop=$cbutton}{assign var=cb value=$cbutton[cb]}
					<span class="bsubmit">
						<button class="bsubmit-r" name="{$cb.name}" value="{$cb.value}"{if $cb.onclick} onclick="return confirm('{$cb.onclick}');"{/if}><span>{$cb.value}</span></button>
					</span>
				{/section}
			{/if}
		{/if}
	{/if}

	</td>
 </tr>

{if $worker_note}
<tr>
	<td>&nbsp;</td>
	  <td>
	{$worker_note}
	</td>
 </tr>
{/if}

</table>
</form>	 

</td>
</tr>
</table>

</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
{if $loc_ajax}
<link rel="stylesheet" href="{$config_site_url}/css/token-input.css" type="text/css" />
<script type="text/javascript" src="{$config_site_url}/js/tools/jquery.tokeninput.js"></script>
{/if}

{literal}
<script type="text/javascript">
<!--
 var num = 2;
 var numc=2;
function addMore() {
  if( num > 10 ) { alert('Upload limit is 10 files per submit'); return; }
  var ni = document.getElementById('extra');
  var newdiv = document.createElement('div');
  var divIdName = 'my'+num+'Div';
  newdiv.setAttribute('id',divIdName);
  newdiv.innerHTML = '<input type="file" size="60" name="extra'+num+'"><br />';
  ni.appendChild(newdiv);
  num++;
}

function checkform() {
	var r = true;
	$(".mandatory").each(function() {
		if( $(this).val() == "") { 
			$(this).css("border", "1px solid #ff0000"); r = false;
		} else  $(this).css("border", "1px solid #000000");
	});
	if( !r ) $("#submitlog").html("You have to answer all the fields with the red border around it.").css({"color":"#ff0000"});
	return r;
}
{/literal}

{literal}
function ListToArray(list_string) {
	return list_string.split('|');
}
function ArrayToList(myarray) {
	return myarray.join('|');
}
/*
function MulForeign_UpdateList(list_id, ids_name, names_name) {
	var list_elem = $("#"+list_id);
	var ids_elem = $("input[name="+ids_name+"]");
	var names_elem = $("input[name="+names_name+"]");;
	if (list_elem == null)
		return;
	if (ids_elem == null)
		return;
	if (names_elem == null)
		return;
	ids = ListToArray(ids_elem.val());
	names = ListToArray(names_elem.val());

	var html = '';
	for (var i = 0; i < names.length; i++) {
		if (ids[i] == '')
			continue;
		html += '<li>'+names[i]+'&nbsp;<a href="#" onclick="MulForeign_DelItem(\'' + ids[i] + '\', \'' + list_id + '\', \'' + ids_name + '\', \'' + names_name + '\'); return false;">delete</a></li>';
	}
	list_elem.html(html);
	return true;
}

function MulForeign_AddItem(select_id, list_id, ids_name, names_name) {
	var select_elem = $("#"+select_id);
	var select_elem_name;
	var list_elem = $("#"+list_id);
	var ids_elem = $("input[name="+ids_name+"]");
	var names_elem = $("input[name="+names_name+"]");
	
	if (select_elem == null)
		return;
	if (list_elem == null)
		return;
	if (ids_elem == null)
		return;
	if (names_elem == null)
		return;

	ids = ListToArray(ids_elem.val());
	names = ListToArray(names_elem.val());

	var new_item = select_elem.val();
	var new_item_name = $("#"+select_id+" :selected").text();
	if (new_item == '') {
		alert('Wrong select');
		return;
	}
	if ($.inArray(new_item, ids) != -1) {
		alert(new_item_name + ' is already selected.');
		return;
	}

	if (ids_elem.val() == '') {
		ids_elem.val(new_item);
		names_elem.val(new_item_name);
	} else {
		ids_elem.val(ids_elem.val() + '|' + new_item);
		names_elem.val(names_elem.val() + '|' + new_item_name);
	}
	MulForeign_UpdateList(list_id, ids_name, names_name);
	return true;
}

function MulForeign_DelItem(item_id, list_id, ids_name, names_name) {
	var list_elem = $("#"+list_id);;
	var ids_elem = $("input[name="+ids_name+"]");
	var names_elem = $("input[name="+names_name+"]");
	if (list_elem == null)
		return false;
	if (ids_elem == null)
		return false;
	if (names_elem == null)
		return false;

	ids = ListToArray(ids_elem.val());
	names = ListToArray(names_elem.val());
	if ($.inArray(item_id, ids) == -1) {
		alert('Wrong item for delete ?');
		return false;
	}
	var index = $.inArray(item_id, ids);
	if( index > -1 ) {
		ids.splice(index, 1);
		names.splice(index, 1);
		ids_elem.val(ArrayToList(ids));
		names_elem.val(ArrayToList(names));

		MulForeign_UpdateList(list_id, ids_name, names_name);
	}
	return false;
}
*/

function refreshLoc(x) {
	$("#loading").show();
	var param = 'ajaxdie=1&'+$("#wform").serialize();
	_ajax('get', '', param, 'content');
}


function onChangePlaceType() {
	if ($('select[name="type"]').length > 0)
		var elem = $('select[name="type"]');
	else
		var elem = $('select[name="place_type_id"]');
	var type_val = elem.val();
	if (typeof(allowedQuestionsPerType) != "undefined" && type_val in allowedQuestionsPerType) {
		$('table.b > tbody > tr').hide();
		elem.closest('tr').show();
		var allowedQuestions = allowedQuestionsPerType[type_val];
		for (i in allowedQuestions) {
			var q = allowedQuestions[i];
			$('select[name="'+q+'"], select[name="'+q+'[]"], input[name="'+q+'"], textarea[name="'+q+'"], div#'+q+', table#'+q+'single').closest('table.b > tbody > tr').show();
		};
		$('table.b tr:first').show();
		$('table.b tr:last').show();
	
		$('#type_select_modifier').remove();
		elem.closest('tr').find('td').last().prepend('<div id="type_select_modifier" style="float: right; margin-top: 5px; font-size: 12px;">Showing only questions for type ' + elem.children('option:selected').text() + '. <a href="#" onclick="showAllQuestions();">Show all questions.</a></div>');
	} else {
		$('table.b tr').show();
		$('#type_select_modifier').remove();
	}
	$('span#city_select').closest('tr').show();	//additional because $('select[name="City"]').closest('table.b > tbody > tr').show(); doesnt work
}

function showAllQuestions() {
	if ($('select[name="type"]').length > 0)
		var elem = $('select[name="type"]');
	else
		var elem = $('select[name="place_type_id"]');
	$('table.b tr').show();
	$('#type_select_modifier').html('Showing ALL questions. <a href="#" onclick="onChangePlaceType();">Show only questions for type ' + elem.children('option:selected').text() + '.</a>');
}

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).ready(function() {
	if ($('select[name="type"]').length > 0)
		var elem = $('select[name="type"]');
	else
		var elem = $('select[name="place_type_id"]');
	elem.change(function(e) {
		e.preventDefault();
		onChangePlaceType();
		return false;
	});
	onChangePlaceType();
	if (getParameterByName('savelive') == 1) {
		$('select[name=edit] option').each(function(){
			if ($(this).val() == 2 && $(this).text() == 'Done')
				$(this).prop('selected', true);
			if ($(this).val() == 1 && $(this).text() == 'Live')
				$(this).prop('selected', true);
		});
	}
	$('select.multi-select').select2({
		multiple: true,
		placeholder: "- Select -",
		width: 'element',
	});
});

{/literal}

{$form_javascript}
-->
</script>

