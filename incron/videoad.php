<?php
/* 
 * PHP envelope to create thumbnail and processed video from uploaded video.
 *
 * Usually runs from incron or similar
 *
 * To run:
 *		php /path/to/videoad.php source_dir source_file
 *
 * Where source_file is source file name without path.
 * 
 * This script will produce thumbnail with the same name
 *
 * in TARGET_VIDEO_DIR/XXXXX/YYYYYY.jpg
 *
 * Create folders (@see process_video() @inc/common.php )
 *
 * 		$src_dir = $config_image_path."/classifieds/uploaded/";
 *		$tgt_dir = $config_image_path."/classifieds/video/";
 *
 * Installation:
 *
 * incrontab -e
 *
 * then add
 *
 *   /home/as/dev/html/classifieds/uploaded/ IN_CLOSE_WRITE /usr/bin/php /home/as/dev/incron/videoad.php $@ $#
 *
 */

$olddir = getcwd();

chdir( realpath(dirname(__FILE__). "/../html"));

define( '_CMS_FRONTEND', 1);

require_once('inc/config.php');
require_once('inc/common.php');

global $db;

if (count($argv) < 3) {
	echo "Error: not enough arguments\n";
	error_log("videoad: not enough arguments, exiting");
	exit(1);
}
$source_path = $argv[1];

$filename = $argv[2];


file_log("vid", "videoad: source folder is $source_path , file $filename");

$res = $db->q("SELECT * FROM classified_video WHERE filename=? AND converted=0 LIMIT 1", array($filename));
$row = $db->r($res);
if( !$row ) {
	file_log("vid", "videoad: source picture file not found for file " . $filename);
	exit(1);
}

file_log("vid", "Processing src_filepath = ".$row['filename']);
$result = process_video("advertising", array('id'=>$row['id'], 'filename'=>$row['filename'], 'source_path'=>$source_path));

if ($result['status'] == 2) {
	$db->q(
		'UPDATE classified_video SET filename = ?, converted = 2, thumbnail = ?, width = ?, height = ? WHERE id = ?',
		[$result["filename"], $result['thumbnail'], $result['width'], $result['height'], $row['id']]
	);
	file_log("vid", "Processed  ${row[filename]} success. Thumbnail: ${result[thumbnail]}");
} else {
	$db->q(
		'UPDATE classified_video SET converted=1 WHERE id=?',
		array($row['id'] )
	);
	file_log("vid", "Error converting file ${row[filename]}");
	reportAdmin("Video conversion error", "Error converting video file. Please check logs.", array(
		'id'=>$row['id'],  'filename'=>$row['filename'], 'ad'=> $row['classified_id'], 'account'=>$row['account_id']
	) );

}
chdir($olddir);
