CREATE TABLE IF NOT EXISTS `classified_waiting_list` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int NOT NULL,
  `location_id` mediumint unsigned NOT NULL,
  `status_id` tinyint NOT NULL,
  `classified_type_id` tinyint NOT NULL,
  `updated_stamp` int NOT NULL,
  `created_stamp` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status_id_classified_type_id_location_id_account_id` (`status_id`,`classified_type_id`,`location_id`,`account_id`),
  CONSTRAINT `account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`account_id`),
  CONSTRAINT `location_id` FOREIGN KEY (`location_id`) REFERENCES `location_location` (`loc_id`)
);

INSERT INTO `classified_waiting_list` (
  `account_id`,
  `location_id`,
  `status_id`,
  `classified_type_id`,
  `updated_stamp`,
  `created_stamp`
)
  SELECT
    `account_id`,
    `location_id`,
    `status_id`,
    `classified_type_id`,
    IFNULL(UNIX_TIMESTAMP(last_update_date), UNIX_TIMESTAMP(creation_date)),
    UNIX_TIMESTAMP(creation_date)
  FROM
    `classified_sticky_queue`;
