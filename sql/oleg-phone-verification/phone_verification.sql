DROP TABLE phone_verification ;

CREATE TABLE phone_verification ( 
    account_id INTEGER UNSIGNED NOT NULL,
    phone VARCHAR(24) COMMENT 'International normalized: +xxx',
    verified BOOLEAN DEFAULT false,
    sms_code VARCHAR(10),
    PRIMARY KEY(account_id, phone),
    created_dts INTEGER UNSIGNED NOT NULL,
    verified_dts INTEGER UNSIGNED DEFAULT NULL,
    attempts TINYINT(1) DEFAULT 3,
    token_hash CHAR(32) COMMENT 'MD5 Hash key',    
    UNIQUE( phone )
) COMMENT "Verified phone number";


ALTER TABLE phone_verification
    ADD attempts TINYINT(1) DEFAULT 3,
    ADD token_hash CHAR(32) COMMENT 'MD5 Hash key';
;
