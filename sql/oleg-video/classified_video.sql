-- Run this file once only when installing oleg-video branch.

DROP TABLE IF EXISTS `classified_video`;

CREATE TABLE `classified_video` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) unsigned NOT NULL,
  `classified_id` int(11) unsigned NOT NULL,
  `filename` varchar(255) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `converted` tinyint(3) unsigned DEFAULT NULL COMMENT '0 = not converted yet, 1 = converted, 2 = error',
  `public_verification` tinyint(3) unsigned DEFAULT NULL COMMENT '1 = this is public verification video',
  `created_stamp` int(11) unsigned DEFAULT NULL,
  `deleted_stamp` int(11) unsigned DEFAULT NULL,
  `deleted_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY(`id`)
) ENGINE=InnoDB;

-- thumbnail_path
-- width_for_200